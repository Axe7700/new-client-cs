﻿using Axe_interDT.Shared;
using Axe_interDT.View.Theme;
using Axe_interDT.Views.Theme;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Newtonsoft.Json.Linq;
using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace Axe_interDT
{
    static class Program
    {
        public static log4net.ILog log;
        /// <summary>
        /// The main entry point for the application. 
        /// 
        /// </summary> 
        [STAThread]
        static void Main(string[] args)
        {
            log4net.GlobalContext.Properties["userName"] = Environment.UserName;
            log4net.Config.XmlConfigurator.Configure(new System.IO.FileInfo("log4net.config"));
            log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

            if (General.fncUserName() == "mohammed")
            {
                General._ExecutionMode = General.ExecutionMode.Dev;
            }
            else
            {
                if (args.Length == 0)
                    throw new Exception("Please add running mode to your projet, Debug section, Add (Dev or Prod or Test, Or TestPDA)");

                switch (args[0])
                {
                    //Execute Type
                    case "Dev":
                        General._ExecutionMode = General.ExecutionMode.Dev;
                        break;
                    case "Prod":
                        General._ExecutionMode = General.ExecutionMode.Prod;
                        break;
                    case "Test":
                        General._ExecutionMode = General.ExecutionMode.Test;
                        break;
                    default:
                        throw new Exception("No running mode is added");
                }

                //===> Mondir le 06.01.2021, Check the compnay, and use diff connection string
                if (General._ExecutionMode == General.ExecutionMode.Test)
                    General.cFrMultiSoc += "_Test";

                //Company
                if (args.Length == 2)
                {
                    General.ChangeCompany(args[1]);
                }
                else
                {
                    General.ChangeCompany("DT");
                }
                //===> Fin Modif Mondir
            }

            if (General.fncUserName().ToLower().Contains("mondir"))
            {
                //General._ExecutionMode = General.ExecutionMode.Dev;
                Theme.ActivateSplashScreen = false;
            }


            Theme.guid = Guid.NewGuid().ToString();

            ////=====> Added By Mondir : Intégration continue
            var json = Properties.Resources.projet_info;
            MemoryStream MS = new MemoryStream(json);
            StreamReader sr = new StreamReader(MS);
            var lines = sr.ReadToEnd();
            var jsonObj = JObject.Parse(lines);
            var version = jsonObj["Version"];
            var versionDate = jsonObj["VersionDate"];
            var commit = jsonObj["Commit"];
            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            var showInfo = Convert.ToBoolean(jsonObj["showInfo"]);
            //===> Fin Modif Mondir
            ////

            ////// ==============> Mondir : To Fix This Bug, https://groupe-dt.mantishub.io/view.php?id=1645,
            /////                  need to change a value in the register to force crystal report to render the same font size
            General.ChangeRegValuesFromCrystal();

            General.VersionDate = versionDate.ToString();
            General.Commit = commit.ToString();
            General.Version = version.ToString();
            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            General.showInfo = showInfo;
            //===> Fin Modif Mondir

            if (General._ExecutionMode == General.ExecutionMode.Dev)
            {
                Theme.ActivateSplashScreen = false;
                if (General.fncUserName().Contains("Mondir"))
                    General.DefaultServer = ".\\SQL_AXECIEL";
                else
                    General.DefaultServer = ".";
            }

            //===> Mondir le 06.01.2021 moved to top
            //if (General._ExecutionMode == General.ExecutionMode.Test)
            //    General.cFrMultiSoc += "_Test";
            //===> Fin Modif Mondir

            //===> Mondir le 07.01.2021, check if the user has right to open connection
            General.InitSource_OLEDB();
            if (!General.CheckConnexion())
            {
                CustomMessageBox.Show("Problème de connexion à la base de données.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Application.Exit();
            }
            //===> Fin Modif Mondir
            General.open_conn();

            //if (!General.fncUserName().Contains("Mondir"))
            General.InitOdbc();

            Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            Theme.CreateApplicationFolders();
            Theme.CopyTheme();

            Infragistics.Win.AppStyling.StyleManager.Load($@"{Theme.ThemeFolder}\AeroV2.isl");

            FormUtils.SetDefaultIcon();
            //FormUtils.OnCloseEvent();

            if (Theme.ActivateSplashScreen)
            {
                Theme.SplashScreenThread = new Thread(() =>
                {

                    //=======> Code Updated by Mohammed
                    /*
                     * declare bool variable called "KillSplashScreen" in Theme and set it to false.
                     * in UserDocFichePrinciple instead of abort the thread by calling the abort() method which occurs an exception ,just set KillSplashScreen to true.
                     * in SplashScreen inside timer_tick function. first of all check if the KillSplashScreen is setted to true, if so call hide method of the form and return;
                     */
                    ///=====> i think no need to abort the SplashScreenThread because when the start function finish its work, the thread automatically aborted
                    var splashScreen = new SplashScreen();
                    splashScreen.ShowDialog();

                    //in this function after hiding the SplashScreen the execution will resume (because showDialog() stop the execution of next lines after it) , so we must close the splash Screen and dispose it
                    splashScreen.Close();
                    splashScreen.Dispose();
                });
                Theme.SplashScreenThread.Start();
            }

            var MainForm = new UserDocFichePrincipal();
            Theme.MainForm = MainForm;
            Theme.MainMenu = Theme.MainForm.mainMenu1;

            ModMain.ModMain_Main();

            if (Theme.ActivateSplashScreen)
            {
                Theme.MainForm.Opacity = 0;
                MainForm.Hide();
            }

            Application.Run(MainForm);
            //Application.Run(new frmWhatsNew());
        }

        static void Application_ThreadException(object sender, ThreadExceptionEventArgs e) => SaveException(e.Exception);
        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e) => SaveException(e.ExceptionObject as Exception);
        public static void SaveException(Exception e, string customMessage = "", bool ShowAlert = true)
        {
            var Date = DateTime.Now;
            var User = Environment.UserName;
            var FunctionName = "";
            var LineNumber = 0;
            var FileName = "";
            if (e != null)
            {
                var ExceptionType = e.GetType().ToString();
                var Message = e.Message;
                var innerExceptionMsg = e?.InnerException?.Message; // Ajouté par Ahmed
                var st = new StackTrace(e, true);
                for (int i = 0; i < st.FrameCount; i++)
                {
                    var frame = st.GetFrame(i);
                    if (frame.GetFileLineNumber() != 0)
                    {
                        LineNumber = frame.GetFileLineNumber();
                        FileName = frame.GetFileName();
                        FunctionName = frame.GetMethod().Name;
                    }
                }
                var Log = $"Date : {Date}|" +
                          $"User : {User}|" +
                          $"ExceptionType : {ExceptionType}|" +
                          $"Message : {Message}|" +
                          $"InnerMessage : {innerExceptionMsg}|" +
                          $"FunctionName : {FunctionName}|" +
                          $"FileName : {FileName}|" +
                          $"LineNumber : {LineNumber}|" +
                          $"Product Version : {Application.ProductVersion} - {General.VersionDate} - {General._ExecutionMode}";

                var dt = DateTime.Now.ToString("yyyy-MM-dd");
                log.Error(Log);
                if (ShowAlert)
                    View.Theme.Theme.AfficheAlertDanger();
            }

            //var FilePath = Theme.LogFolder + "\\" + AppDomain.CurrentDomain.FriendlyName + $"-{dt}" + ".txt";

            //====> Mondir le 05.08.2020, Code commented by Mondir, use Theme.sendMessageToLog to add the GUID and Control on top to the LOG
            //if (!String.IsNullOrEmpty(customMessage)) log.Info(customMessage);
            Theme.sendMessageToLog(customMessage);
            //===> Fin Modif Mondir
        }
    }
}
