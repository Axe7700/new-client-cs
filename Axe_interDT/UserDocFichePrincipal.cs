﻿using Axe_interDT.Properties;
using Axe_interDT.Shared;
using Axe_interDT.View.Theme;
using Axe_interDT.Views.Theme;
using Axe_interDT.Views.Theme.CustomContectMenu;
using Axe_interDT.Views.Theme.Menu;
using Axe_interDT.Views.Theme.Menu.MenuCollapsed;
using Newtonsoft.Json;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Axe_interDT
{
    public partial class UserDocFichePrincipal : Form, IMessageFilter
    {
        public bool menuCollapsed { get; set; } = false;
        const int WM_KEYDOWN = 0x100;
        const int WM_KEYUP = 0x101;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private DateTime dateNow;
        public ContextMenuChoixDeSociete ContextMenuChoixDeSociete;
        private PoperContainer PoperContainer;
        public static string AxeInterDt_Bat_File_Path { get; set; } = @"W:\PACKAGES\INTRANET\Axe_interDT_Prod\Axe_interLONG.exe.bat";

        public UserDocFichePrincipal()
        {

            InitializeComponent();

            //===> Mondir le 19.01.2021, https://groupe-dt.mantishub.io/view.php?id=2201
            AxeInterDt_Bat_File_Path = General._Company == General.Company.DT ? @"W:\PACKAGES\INTRANET\Axe_interDT_Prod\Axe_interDT.exe.bat" : @"W:\PACKAGES\INTRANET\Axe_interDT_Prod\Axe_interLONG.exe.bat";
            //===> Fin Modif Mondir


            Application.AddMessageFilter(this);
            this.FormClosed += (o, e) => Application.RemoveMessageFilter(this);

            ContextMenuChoixDeSociete = new ContextMenuChoixDeSociete();
            ContextMenuChoixDeSociete.fc_InitMultiSoc();

            General.open_conn();

            PoperContainer = new PoperContainer(ContextMenuChoixDeSociete);
            this.ResizeBegin += (s, e) => { this.SuspendLayout(); };
            this.ResizeEnd += (s, e) => { this.ResumeLayout(true); };
        }



        public bool PreFilterMessage(ref Message m)
        {
            try
            {
                if (m.Msg == 0x201 || m.Msg == 0x203)
                {
                    // Trap left click + double-click
                    string name = "Unknown";
                    Control ctl = FromHandle(m.HWnd);
                    if (ctl != null)
                    {
                        if (ctl.Name == "" || ctl.Name.ToLower().Contains("_EmbeddableTextBox".ToLower()))
                            name = ctl.Parent?.Name;
                        else
                            name = ctl.Name;

                        if (string.IsNullOrEmpty(name))
                            name = ctl.Parent?.Parent?.Name;
                        if (string.IsNullOrEmpty(name))
                            name = ctl.GetType().ToString();
                    }

                    Point pos = new Point(m.LParam.ToInt32());
                    //Console.WriteLine();

                    if (m.Msg == 0x201) Theme.sendMessageToLog($"CLICK    : {name} at {pos} Text: {ctl?.Text}");
                    if (m.Msg == 0x203) Theme.sendMessageToLog($"DBLCLICK : {name} at {pos} Text: {ctl?.Text}");

                }

                Keys keyCode = (Keys)(int)m.WParam & Keys.KeyCode;
                if (m.Msg == WM_KEYDOWN)
                {
                    Theme.sendMessageToLog($"typed : {keyCode}");


                    //  return true;
                }
            }
            catch
            {

            }
            return false;
        }

        private void UserDocFichePrincipal_Load(object sender, EventArgs e)
        {

            //===> Mondir le 06.01.2020, Change Company Icon
            //===> Mondir le 25.05.2021, add VF, line bellow commented
            //Icon = General._Company == General.Company.DT ? Resources.Intranet : Resources.LONG_ICO;
            switch (General._Company)
            {
                case General.Company.DT:
                    Icon = Resources.Intranet;
                    break;
                case General.Company.LONG:
                    Icon = Resources.LONG_ICO;
                    break;
                case General.Company.VF:
                    Icon = Resources.VF_LOGO1;
                    break;
            }
            //===> Fin Modif Mondir

            PanelMainControls.BackColor = Theme.ControlsBackColor;
            PanelMainControl.BackColor = Theme.ControlsBackColor;

            //var delostalVersionTable = "DelostalVersion";

            //string query = "select case when exists((select * from information_schema.tables where table_name = '" + delostalVersionTable + "')) then 1 else 0 end";
            ////=========> checking if the table exist
            //var cmd = new SqlCommand(query, General.adocnn);
            //if (General.adocnn.State != ConnectionState.Open)
            //    await General.adocnn.OpenAsync();

            //bool exists = (int)cmd.ExecuteScalar() == 1;

            ////=======> if the table doesn't exist, we create it
            //if (!exists)
            //{
            //    query = "create table " + delostalVersionTable + "("
            //            + " id int primary key identity,"
            //            + " version varchar(50),"
            //            + " latestVersion varchar(50),"
            //            + " dateVersion date,"
            //            + " gitCommitVersion varchar(20))";
            //    cmd = new SqlCommand(query, General.adocnn);
            //    cmd.ExecuteNonQuery();
            //}

            ////========> checking if the current product version  exist in table version if doesn't exist we create it and set it to latestVersion
            //query = "select count(*) from " + delostalVersionTable + " where version='" + Application.ProductVersion + "'";
            //cmd = new SqlCommand(query, General.adocnn);
            //if ((int)cmd.ExecuteScalar() == 0)
            //{
            //    query = "insert into " + delostalVersionTable + " values('" + Application.ProductVersion + "','" + Application.ProductVersion + "','" + General.VersionDate + "','" + General.commit + "')";
            //    cmd = new SqlCommand(query, General.adocnn);
            //    cmd.ExecuteNonQuery();
            //}

            dateNow = DateTime.Now;
            TimerLogOppen.Start();

            //===> Mondir le 23.07.2020, 
            Theme.ChangeCompany();

            if (Theme.ActivateSplashScreen)
            {
                Hide();

                foreach (var T in Theme.ControlToLoadOnLunch)
                {
                    Theme.Navigate(T.Value, false);
                    Theme.IsLoading++;
                }

                foreach (Control C in Theme.MainForm.PanelMainControl.Controls)
                {
                    C.Invalidate();
                    C.Update();
                    C.Refresh();
                }
                Theme.killSplashScreen = true;

                //Theme.SplashScreenThread.Abort();

                Show();
                Opacity = 1;
                Activate();

            }
            else
            {
                Theme.Navigate(typeof(Home), false);
            }

            Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocFichePrincipal");


            Theme.MainForm.LabelHeader.Text = $"Bienvenue {getUserFirstName()}!";

            var Date = DateTime.Now;
            var User = Environment.UserName;
            var Log = $"Application Opened On  {Date} User : {User}";
            log.Info(Log);

            if (!string.IsNullOrEmpty(General.NomSousSociete) && General.NomSousSociete != null)
            {
                //TODO : Mondir - Ask Rachid About This
                //LabelCompanyName.Text = General.NomSousSociete;
                System.Windows.Forms.Application.DoEvents();
            }

            // read the notifViewedBy file 
            //if(General._ExecutionMode == General.ExecutionMode.Prod)
            //{
            //CheckNotif();
            //}

            if (General._ExecutionMode != General.ExecutionMode.Prod)
            {
                SAGE.fc_OpenConnSage();
                //====> Mondir le 23.07.2020, ajouté cette condition sur la base P1, long utilise pas la base P1
                //====> Mondir le 25.05.2021, change condition, accept only DT
                //if (!General.cRegInter.Contains("Axe_interLONG"))
                if (General._Company == General.Company.DT)
                    ModP1.fc_OpenConnP1();
                ModParametre.fc_OpenConnGecet();
                var msg = $"Main DataBase | Source = {General.adocnn.DataSource}\nDataBase = {General.adocnn.Database}\n-----\n";
                msg += $"Sage | Source = {SAGE.adoSage.DataSource}\nDataBase = {SAGE.adoSage.Database}\n-----\n";
                //msg += $"AXE_P1 | Source = {ModP1Gaz.adoP1Gaz.DataSource}\nDataBase = {ModP1Gaz.adoP1Gaz.Database}\n-----\n";

                //====> Mondir le 23.07.2020, ajouté cette condition sur la base P1, long utilise pas la base P1
                //====> Mondir le 25.05.2021, change condition, accept only DT
                //if (!General.cRegInter.Contains("Axe_interLONG"))
                if (General._Company == General.Company.DT)
                    msg += $"P1_DT | Source = {ModP1.adoP1.DataSource}\nDataBase = {ModP1.adoP1.Database}\n-----\n";

                msg += $"Gecet | Source = {ModParametre.adoGecet.DataSource}\nDataBase = {ModParametre.adoGecet.Database}\n-----\n";
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(msg);

                new frmWhatsNew().ShowDialog();
            }



            //Thread ATM2 = new Thread(new ThreadStart(ThreadProc));
            //ATM2.Start();


        }

        //Form frm = new Form();

        //private void ThreadProc()
        //{
        //    //if (InvokeRequired)
        //    //{
        //    //    this.Invoke(new Action(() => CreateAndShowForm()));
        //    //    return;
        //    //}
        //    CreateAndShowForm();

        //}

        //private void CreateAndShowForm()
        //{

        //    frm.Size = new Size(300, 300);
        //    frm.Load += (se, ev) =>
        //    {
        //        int i = 0;
        //        while (i < 15000)
        //        {
        //            frm.Text = (i++).ToString();

        //        }
        //    };
        //    frm.ShowDialog();
        //}

        private void CheckNotif()
        {
            try
            {
                string path = "";
                if (General._ExecutionMode == General.ExecutionMode.Dev)
                    path = Theme.DocumentFolderPath + "/notifViewedBy.json";
                else
                    path = Theme.Axe_interDTFolder + "/notifViewedBy.json";

                //create the file if not exist and add the attributes CurrentVersion & ViewedBy 
                if (!File.Exists(path))
                {
                    var fileData = "{\"CurrentVersion\":'',\"ViewedBy\":[]}";
                    var fs = File.Create(path);
                    fs.Close();
                    if (File.Exists(path))
                        File.WriteAllText(path, fileData);
                }
                // read the json file
                var notifViewedByFile = File.ReadAllBytes(path);
                // convert bytes to string .
                string notifViewedByFileStr = Encoding.UTF8.GetString(notifViewedByFile);

                //serialize string to json object
                dynamic notifViewedByObject = JsonConvert.DeserializeObject(notifViewedByFileStr);

                Newtonsoft.Json.Linq.JArray ViewedBy = notifViewedByObject.ViewedBy;

                // check if the version that exist in the file !equal with the current version of app
                if (notifViewedByObject.CurrentVersion.Value + "".ToLower() != General.Version.ToLower())
                    ViewedBy.RemoveAll();// clear the viewedBy array

                notifViewedByObject.CurrentVersion = General.Version;

                //check if the current user already saw the notif
                if (ViewedBy.FirstOrDefault(t1 => t1.ToString().ToLower() == General.fncUserName().ToLower()) == null)
                {
                    // read projet_info file
                    var jsonFile = Properties.Resources.projet_info;
                    string jsonFileStr = Encoding.UTF8.GetString(jsonFile);
                    dynamic jsonObject = JsonConvert.DeserializeObject(jsonFileStr);

                    //Add the name of user who saw the notif
                    ViewedBy.Add(General.fncUserName());

                    var allText = "V" + jsonObject.Version + " du " + jsonObject.VersionDate + "\n";
                    var features = "";

                    //read the new features
                    foreach (dynamic obj in jsonObject.MailNewFeatures)
                    {
                        if (obj.FeatureDesc.Value != "" && obj.FeatureName.Value != "")
                            features += "   - " + obj.FeatureDesc + " \"" + obj.FeatureName + "\" \n";
                    }

                    //read the new bugs
                    var bugs = "";
                    foreach (dynamic obj in jsonObject.FixedBugs)
                    {
                        if (obj.BugId.Value != "")
                            bugs += "   - N° :" + obj.BugId + " ==> nom du bug : " + obj.BugName + " \n";
                    }

                    if (!string.IsNullOrEmpty(features))
                    {
                        allText += "* Features : \n" + features;
                    }
                    if (!string.IsNullOrEmpty(bugs))
                        allText += "-------- \n * bugs fixées :\n" + bugs;

                    //serialize notifViewedByObject 
                    var file = JsonConvert.SerializeObject(notifViewedByObject);

                    //write new data to notifViewedBy.json
                    File.WriteAllText(path, file);

                    //display the notif
                    Views.Theme.CustomMessageBox.CustomMessageBox.Show(allText, "Quoi de neuf sur la nouvelle version?", MessageBoxButtons.OK, MessageBoxIcon.Information, isSmall: false);
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }
        private void buttonBack_Click(object sender, EventArgs e)
        {
            Theme.Previous();
        }

        private void buttonNext_Click(object sender, EventArgs e)
        {
            Theme.Next();

        }

        /// <summary>
        /// GET Name of the connected user using Windows Login and Display the first word as first-Name
        /// </summary>
        string getUserFirstName()
        {

            string req = "SELECT  USR_Name From USR_Users where USR_Nt = '" + General.fncUserName() + "'";
            var ModAdo = new ModAdo();
            string result = ModAdo.fc_ADOlibelle(req);
            if (!string.IsNullOrEmpty(result))
            {
                var words = result.Split(' ');
                return words.Length > 0 ? words[0] : result;
            }
            return "";
        }
        private void pictureBoxLogo_Click(object sender, EventArgs e)
        {
            Theme.Navigate(typeof(Home), false);

            Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocFichePrincipal");

            //===> Mondir le 06.07.2020, Dont change the header if a new update is avariable
            if (!Theme.NewVersionFound)
                Theme.MainForm.LabelHeader.Text = $"Bienvenue {getUserFirstName()}!";
        }

        public void PictureBoxSettings_Click(object sender, EventArgs e)
        {
            //TODO : TO SHOW If Everything is ok
            //if (General._ExecutionMode != General.ExecutionMode.Test)
            //PoperContainer.Show(PictureBoxSettings);
            if (!menuCollapsed)
            {

                foreach (var Control in mainMenu1.PanelItems.Controls.Cast<Control>().Where(item => item.GetType() == typeof(MainMenuItem)))
                    Control.Visible = false;

                buttonBack.Text = "";
                buttonNext.Text = "";
                buttonBack.Width = 33;
                buttonNext.Width = 33;
                tableLayoutPanelMain.ColumnStyles[0].Width = 70;

                foreach (var Control in mainMenu1.PanelItems.Controls.Cast<Control>().Where(item => item.GetType() == typeof(MainMenuItemCollapsed)))
                    Control.Visible = true;
            }
            else
            {
                foreach (var Control in mainMenu1.PanelItems.Controls.Cast<Control>().Where(item => item.GetType() == typeof(MainMenuItemCollapsed)))
                    Control.Visible = false;

                buttonBack.Text = "Précédent";
                buttonNext.Text = "Suivant";
                buttonBack.Width = 147;
                buttonNext.Width = 147;
                //buttonBack.Location = new Point(144, 0);
                buttonBack.Dock = DockStyle.Left;
                buttonNext.Dock = DockStyle.Right;
                tableLayoutPanelMain.ColumnStyles[0].Width = 295;

                foreach (var Control in mainMenu1.PanelItems.Controls.Cast<Control>().Where(item => item.GetType() == typeof(MainMenuItem)))
                    Control.Visible = true;
            }

            menuCollapsed = !menuCollapsed;
        }

        private void TimerLogOppen_Tick(object sender, EventArgs e)
        {
            //var Date = DateTime.Now;
            //var User = Environment.UserName;
            //var Log = $"Application Opened On  {Date} User : {User}";
            //log.Info(Log);
            //if (General._ExecutionMode == General.ExecutionMode.Dev || General.fncUserName().ToLower().Contains("mondir"))
            notifyWhenNewVersionAvailable();
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void notifyWhenNewVersionAvailable()
        {
            try
            {
                //===> Mondir le 02.07.2020, disable the notification for test mode !
                if (General._ExecutionMode == General.ExecutionMode.Test && !General.fncUserName().ToLower().Contains("mondir"))
                    return;

                FileInfo file;

                if (General.fncUserName().ToLower().Contains("mondir"))
                {
                    file = new FileInfo(@"C:\Data\Donnee\delostal\Axe_interDT.exe.bat");
                }
                else if (General._ExecutionMode == General.ExecutionMode.Dev)
                {
                    file = new FileInfo(@"C:\Users\messi\Desktop\bat.exe.bat");
                }
                else
                    file = new FileInfo(AxeInterDt_Bat_File_Path);
                if (file.Exists)
                {
                    if (file.LastWriteTime > dateNow)
                    {
                        notificationIcon.Visible = true;
                        this.Text = $"Une Mise à jour d'intranet est disponible, Veuillez relancer l'application";
                        LabelHeader.Text = "Une Mise à jour d'intranet est disponible, Veuillez relancer l'application";
                        LabelHeader.ForeColor = Color.FromArgb(250, 104, 94);
                        TimerLogOppen.Stop();
                        timerFlashNewVersion.Start();
                        Theme.NewVersionFound = true;
                    }
                    else
                        notificationIcon.Visible = false;
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private void notificationIcon_Click(object sender, EventArgs e)
        {
            try
            {
                //===> Mondir le 16.10.2020, asked by rachid
                new frmWhatsNew().ShowDialog();
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        /// <summary>
        /// Mondir le 02.07.2020, this is added to alert the user that there is another version installed,
        /// the text of the header and the bill icon will flash continusely
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerFlashNewVersion_Tick(object sender, EventArgs e)
        {
            if (!Theme.NewVersionFound)
                return;

            LabelHeader.Visible = !LabelHeader.Visible;
            notificationIcon.Visible = !notificationIcon.Visible;
        }
    }
}