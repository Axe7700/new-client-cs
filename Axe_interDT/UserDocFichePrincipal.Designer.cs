﻿using Axe_interDT.Views.Theme;
using Axe_interDT.Views.Theme.Menu;

namespace Axe_interDT
{
    partial class UserDocFichePrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocFichePrincipal));
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.LabelHeader = new System.Windows.Forms.Label();
            this.notificationIcon = new Infragistics.Win.UltraWinEditors.UltraPictureBox();
            this.info1 = new Axe_interDT.Views.Theme.Info();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBoxSettings = new System.Windows.Forms.PictureBox();
            this.imgLogoSociete = new System.Windows.Forms.PictureBox();
            this.panelNavigationButtons = new System.Windows.Forms.Panel();
            this.buttonNext = new System.Windows.Forms.Button();
            this.buttonBack = new System.Windows.Forms.Button();
            this.PanelMainControls = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panelAlert = new System.Windows.Forms.Panel();
            this.PanelMainControl = new System.Windows.Forms.Panel();
            this.TimerLogOppen = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanelMenu = new System.Windows.Forms.TableLayoutPanel();
            this.mainMenu1 = new Axe_interDT.Views.Theme.Menu.MainMenu();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.timerFlashNewVersion = new System.Windows.Forms.Timer(this.components);
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSettings)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogoSociete)).BeginInit();
            this.panelNavigationButtons.SuspendLayout();
            this.PanelMainControls.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1184, 55);
            this.panel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tableLayoutPanel10);
            this.panel3.Controls.Add(this.panel4);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(1184, 55);
            this.panel3.TabIndex = 0;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 95.05062F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 4.949381F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel10.Controls.Add(this.LabelHeader, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.notificationIcon, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.info1, 2, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(295, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(889, 55);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // LabelHeader
            // 
            this.LabelHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.LabelHeader.Dock = System.Windows.Forms.DockStyle.Fill;
            this.LabelHeader.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.LabelHeader.ForeColor = System.Drawing.Color.White;
            this.LabelHeader.Location = new System.Drawing.Point(3, 0);
            this.LabelHeader.Name = "LabelHeader";
            this.LabelHeader.Size = new System.Drawing.Size(795, 55);
            this.LabelHeader.TabIndex = 4;
            this.LabelHeader.Text = "Bienvenue !";
            this.LabelHeader.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // notificationIcon
            // 
            this.notificationIcon.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.notificationIcon.BorderShadowColor = System.Drawing.Color.Empty;
            this.notificationIcon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.notificationIcon.Image = global::Axe_interDT.Properties.Resources.active;
            this.notificationIcon.Location = new System.Drawing.Point(808, 14);
            this.notificationIcon.Name = "notificationIcon";
            this.notificationIcon.Size = new System.Drawing.Size(26, 26);
            this.notificationIcon.TabIndex = 5;
            this.toolTip1.SetToolTip(this.notificationIcon, "Une nouvelle version est disponible maintenant");
            this.notificationIcon.Visible = false;
            this.notificationIcon.Click += new System.EventHandler(this.notificationIcon_Click);
            // 
            // info1
            // 
            this.info1.BackColor = System.Drawing.Color.Transparent;
            this.info1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("info1.BackgroundImage")));
            this.info1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.info1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.info1.Format = Axe_interDT.Views.Theme.Info.InfoFormat.Rounded;
            this.info1.Link = "http://azdt-iis-01:5000/";
            this.info1.Location = new System.Drawing.Point(847, 13);
            this.info1.Margin = new System.Windows.Forms.Padding(5, 13, 3, 3);
            this.info1.Name = "info1";
            this.info1.Size = new System.Drawing.Size(32, 32);
            this.info1.TabIndex = 6;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel2);
            this.panel4.Controls.Add(this.imgLogoSociete);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(295, 55);
            this.panel4.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.Controls.Add(this.PictureBoxSettings, 1, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(60, 55);
            this.tableLayoutPanel2.TabIndex = 5;
            // 
            // PictureBoxSettings
            // 
            this.PictureBoxSettings.BackColor = System.Drawing.Color.Transparent;
            this.PictureBoxSettings.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBoxSettings.Image = global::Axe_interDT.Properties.Resources.menu;
            this.PictureBoxSettings.Location = new System.Drawing.Point(15, 15);
            this.PictureBoxSettings.Margin = new System.Windows.Forms.Padding(0);
            this.PictureBoxSettings.Name = "PictureBoxSettings";
            this.PictureBoxSettings.Size = new System.Drawing.Size(30, 25);
            this.PictureBoxSettings.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxSettings.TabIndex = 0;
            this.PictureBoxSettings.TabStop = false;
            this.PictureBoxSettings.Click += new System.EventHandler(this.PictureBoxSettings_Click);
            // 
            // imgLogoSociete
            // 
            this.imgLogoSociete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.imgLogoSociete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.imgLogoSociete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgLogoSociete.Image = global::Axe_interDT.Properties.Resources.logodt_295__53;
            this.imgLogoSociete.Location = new System.Drawing.Point(0, 0);
            this.imgLogoSociete.Name = "imgLogoSociete";
            this.imgLogoSociete.Size = new System.Drawing.Size(295, 55);
            this.imgLogoSociete.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.imgLogoSociete.TabIndex = 0;
            this.imgLogoSociete.TabStop = false;
            this.imgLogoSociete.Click += new System.EventHandler(this.pictureBoxLogo_Click);
            // 
            // panelNavigationButtons
            // 
            this.panelNavigationButtons.Controls.Add(this.buttonNext);
            this.panelNavigationButtons.Controls.Add(this.buttonBack);
            this.panelNavigationButtons.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelNavigationButtons.Location = new System.Drawing.Point(0, 0);
            this.panelNavigationButtons.Margin = new System.Windows.Forms.Padding(0);
            this.panelNavigationButtons.Name = "panelNavigationButtons";
            this.panelNavigationButtons.Size = new System.Drawing.Size(295, 33);
            this.panelNavigationButtons.TabIndex = 6;
            // 
            // buttonNext
            // 
            this.buttonNext.BackColor = System.Drawing.Color.Transparent;
            this.buttonNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonNext.Dock = System.Windows.Forms.DockStyle.Right;
            this.buttonNext.FlatAppearance.BorderSize = 0;
            this.buttonNext.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.buttonNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNext.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.buttonNext.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.buttonNext.Image = global::Axe_interDT.Properties.Resources.right_round_24;
            this.buttonNext.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNext.Location = new System.Drawing.Point(147, 0);
            this.buttonNext.Margin = new System.Windows.Forms.Padding(0);
            this.buttonNext.Name = "buttonNext";
            this.buttonNext.Size = new System.Drawing.Size(148, 33);
            this.buttonNext.TabIndex = 2;
            this.buttonNext.Text = "Suivant";
            this.buttonNext.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.buttonNext.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.buttonNext.UseVisualStyleBackColor = false;
            this.buttonNext.Click += new System.EventHandler(this.buttonNext_Click);
            // 
            // buttonBack
            // 
            this.buttonBack.BackColor = System.Drawing.Color.Transparent;
            this.buttonBack.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonBack.Dock = System.Windows.Forms.DockStyle.Left;
            this.buttonBack.FlatAppearance.BorderSize = 0;
            this.buttonBack.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DimGray;
            this.buttonBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBack.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonBack.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.buttonBack.Image = global::Axe_interDT.Properties.Resources.left_round_24;
            this.buttonBack.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBack.Location = new System.Drawing.Point(0, 0);
            this.buttonBack.Margin = new System.Windows.Forms.Padding(0);
            this.buttonBack.Name = "buttonBack";
            this.buttonBack.Size = new System.Drawing.Size(147, 33);
            this.buttonBack.TabIndex = 1;
            this.buttonBack.Text = "Précédent";
            this.buttonBack.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonBack.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.buttonBack.UseVisualStyleBackColor = false;
            this.buttonBack.Click += new System.EventHandler(this.buttonBack_Click);
            // 
            // PanelMainControls
            // 
            this.PanelMainControls.BackColor = System.Drawing.Color.LightGray;
            this.PanelMainControls.Controls.Add(this.tableLayoutPanel1);
            this.PanelMainControls.Controls.Add(this.PanelMainControl);
            this.PanelMainControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelMainControls.Location = new System.Drawing.Point(295, 0);
            this.PanelMainControls.Margin = new System.Windows.Forms.Padding(0);
            this.PanelMainControls.Name = "PanelMainControls";
            this.PanelMainControls.Size = new System.Drawing.Size(889, 687);
            this.PanelMainControls.TabIndex = 3;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.64357F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.71287F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35.64357F));
            this.tableLayoutPanel1.Controls.Add(this.panelAlert, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(5, 653);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(878, 28);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // panelAlert
            // 
            this.panelAlert.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelAlert.Location = new System.Drawing.Point(312, 0);
            this.panelAlert.Margin = new System.Windows.Forms.Padding(0);
            this.panelAlert.Name = "panelAlert";
            this.panelAlert.Size = new System.Drawing.Size(252, 28);
            this.panelAlert.TabIndex = 1;
            this.panelAlert.Visible = false;
            // 
            // PanelMainControl
            // 
            this.PanelMainControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelMainControl.BackColor = System.Drawing.Color.LightGray;
            this.PanelMainControl.Location = new System.Drawing.Point(0, 0);
            this.PanelMainControl.Name = "PanelMainControl";
            this.PanelMainControl.Size = new System.Drawing.Size(883, 650);
            this.PanelMainControl.TabIndex = 0;
            // 
            // TimerLogOppen
            // 
            this.TimerLogOppen.Interval = 5000;
            this.TimerLogOppen.Tick += new System.EventHandler(this.TimerLogOppen_Tick);
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 295F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.Controls.Add(this.tableLayoutPanelMenu, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.PanelMainControls, 1, 0);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 55);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 1;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 687F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(1184, 687);
            this.tableLayoutPanelMain.TabIndex = 7;
            // 
            // tableLayoutPanelMenu
            // 
            this.tableLayoutPanelMenu.ColumnCount = 1;
            this.tableLayoutPanelMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenu.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMenu.Controls.Add(this.mainMenu1, 0, 1);
            this.tableLayoutPanelMenu.Controls.Add(this.panelNavigationButtons, 0, 0);
            this.tableLayoutPanelMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMenu.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMenu.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanelMenu.Name = "tableLayoutPanelMenu";
            this.tableLayoutPanelMenu.RowCount = 2;
            this.tableLayoutPanelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanelMenu.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMenu.Size = new System.Drawing.Size(295, 687);
            this.tableLayoutPanelMenu.TabIndex = 0;
            // 
            // mainMenu1
            // 
            this.mainMenu1.BackColor = System.Drawing.Color.Transparent;
            this.mainMenu1.CommunMenu = null;
            this.mainMenu1.CommunMenuCollapsed = null;
            this.mainMenu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainMenu1.Location = new System.Drawing.Point(0, 33);
            this.mainMenu1.Margin = new System.Windows.Forms.Padding(0);
            this.mainMenu1.Name = "mainMenu1";
            this.mainMenu1.Size = new System.Drawing.Size(295, 654);
            this.mainMenu1.TabIndex = 4;
            // 
            // timerFlashNewVersion
            // 
            this.timerFlashNewVersion.Interval = 2000;
            this.timerFlashNewVersion.Tick += new System.EventHandler(this.timerFlashNewVersion_Tick);
            // 
            // UserDocFichePrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.ClientSize = new System.Drawing.Size(1184, 742);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(1200, 726);
            this.Name = "UserDocFichePrincipal";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Delostal & Thibault V0.1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.UserDocFichePrincipal_Load);
            this.panel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxSettings)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgLogoSociete)).EndInit();
            this.panelNavigationButtons.ResumeLayout(false);
            this.PanelMainControls.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public MainMenu MainMenu;
        public System.Windows.Forms.Panel PanelMainControls;
        public MainMenu mainMenu1;
        public System.Windows.Forms.Label LabelHeader;
        public System.Windows.Forms.Panel PanelMainControl;
        public System.Windows.Forms.Button buttonNext;
        public System.Windows.Forms.Button buttonBack;
        public System.Windows.Forms.PictureBox imgLogoSociete;
        public System.Windows.Forms.Panel panelAlert;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.PictureBox PictureBoxSettings;
        public System.Windows.Forms.Timer TimerLogOppen;
        public System.Windows.Forms.Panel panelNavigationButtons;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanelMenu;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public Infragistics.Win.UltraWinEditors.UltraPictureBox notificationIcon;
        public System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Timer timerFlashNewVersion;
        private Views.Theme.Info info1;
    }
}

