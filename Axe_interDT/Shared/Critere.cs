﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace Axe_interDT.Shared
{
    public class Critere
    {
        private Object _valeur;

        public Object Valeur
        {
            get { return _valeur; }
            set { _valeur = value; }
        }
        private String _operateur;

        public String Operateur
        {
            get { return _operateur; }
            set { _operateur = value; }
        }
        private String _champ;

        public String Champ
        {
            get { return _champ; }
            set { _champ = value; }
        }
        private String _link;

        public String Link
        {
            get { return _link; }
            set { _link = value; }
        }
        private String _dd;

        public String Dd
        {
            get { return _dd; }
            set { _dd = value; }
        }
        private String _df;

        public String Df
        {
            get { return _df; }
            set { _df = value; }
        }

        public Critere(String champ, String operateur, Object valeur, String link, String dd, String df)
        {
            this._champ = champ;
            this._operateur = operateur;
            this._valeur = valeur;
            this._link = link;
            this._dd = dd;
            this._df = df;
        }

        public static String getQueryCritere(HashSet<Critere> criteres)
        {
            String critere = ""; Object valeur; String operateur = ""; String champ = ""; String link = ""; String dd = ""; String df = "";
            Boolean existe = false;
            int i = 1;
            /******inserer les champs*********/
            if (criteres != null)
            {
                foreach (Critere c in criteres)
                {
                    if (c.Valeur != null)
                    {
                        existe = true;
                        valeur = c.Valeur;
                        operateur = c.Operateur;
                        champ = c.Champ;
                        link = c.Link;
                        dd = c.Dd;
                        df = c.Df;
                        if (critere.Equals(""))
                        {
                            critere = critere + dd + champ + " " + operateur + " " + "(@" + i + ")" + df;
                        }
                        else {
                            critere = critere + " " + link + " " + dd + champ + " " + operateur + " " + "(@" + i + ")" + df;
                        }
                        i = i + 1;
                    }
                }
            }
            if (existe == false)
            {
                critere = null;
            }
            else {
                critere = "where " + critere;
            }
            return critere;
        }

        public static void addParametersToQuery(ref SqlCommand req, HashSet<Critere> criteres)
        {
            int i = 1;
            if (criteres != null)
            {
                foreach (Critere c in criteres)
                {
                    if (c.Valeur != null)
                    {
                        req.Parameters.Add(new SqlParameter("@" + i, c.Valeur));
                        i = i + 1;
                    }
                }
            }
        }
    }
}