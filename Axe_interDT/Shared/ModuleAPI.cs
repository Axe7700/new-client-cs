﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Net.NetworkInformation;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public class ModuleAPI
    {
        public const string cWinXP = "Windows XP";
        public const string cWin2000 = "Windows 2000";
        public const string cWinNT4 = "Windows NT 4.0";
        public const string cWin95 = "Windows 95";
        public const string cWin98 = "Windows 98";
        public const string cWinNT351 = "Windows 3.51";
        public const string cWinMe = "Windows Millenium";
        public static string sSysRep;
        public static string sWinRep;



        /**--------------------- new lines Mohammed*--------------------------*/

        private const string cFrModName = "Mod Fichiers";
        private const short SE_ERR_OUT_OF_MEMORY = 0;
        private const short SE_ERR_FNF = 2;
        private const short SE_ERR_PNF = 3;
        private const short ERROR_BAD_FORMAT = 11;
        private const short SE_ERR_ACCESSDENIED = 5;
        private const short SE_ERR_ASSOCINCOMPLETE = 27;
        private const short SE_ERR_DDEBUSY = 30;
        private const short SE_ERR_DDEFAIL = 29;
        private const short SE_ERR_DDETIMEOUT = 28;
        private const short SE_ERR_DLLNOTFOUND = 32;
        private const short SE_ERR_NOASSOC = 31;
        private const short SE_ERR_OOM = 8;
        private const short SE_ERR_SHARE = 26;

        public const short INVALID_HANDLE_VALUE = -1;



        /**--------------------- end*-----------------------------------*/

        public static string gfr_GetComputerName()
        {
            return Environment.MachineName;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="file"></param>
        public static void Ouvrir(string file)
        {
            //===> Mondir le 05.11.2020, pour corrigé le ticket xxx ===> C'est une modif temporaire, j'ai demandé à Rachid de mettre à jour les chemins dans la table
            file = file.Contains(@"\\dt-fic-01\") ? file.Replace(@"\\dt-fic-01\", @"\\azdt-fic-01\") : file;
            //===> Fin Modif Mondir
            if (File.Exists(file))
                Process.Start(file);
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier a été supprimé ou déplacé", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //===> Mondir le 05.11.2020, commented, no need for this
                //Program.SaveException(null, file + " does not exist");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public static string getVersion()
        {
            string functionReturnValue = null;

            // Cette fonction a pour but de récupérer la version de Windows

            var Version = Environment.OSVersion;

            switch (Version.Platform)
            {

                case PlatformID.Win32Windows:

                    switch (Version.Version.Minor)
                    {
                        case 0:
                            functionReturnValue = cWin95;
                            break;
                        case 10:
                            functionReturnValue = cWin98;
                            break;
                        case 90:
                            functionReturnValue = cWinMe;
                            break;
                    }
                    break;

                case PlatformID.Win32NT:
                    switch (Version.Version.Major)
                    {
                        case 3:
                            functionReturnValue = cWinNT351;
                            break;
                        case 4:
                            functionReturnValue = cWinNT4;
                            break;
                        case 5:
                            if (Version.Version.Minor == 0)
                            {
                                functionReturnValue = cWin2000;
                            }
                            else
                            {
                                functionReturnValue = cWinXP;
                            }
                            break;
                    }
                    break;

                default:
                    functionReturnValue = "Failed";
                    break;
            }
            return functionReturnValue;
        }

        public static void SaveBitmap(SqlDataAdapter adors, string strField, string SourceFile)
        {
            //This sub copies the actual file into a byte array.
            //This byte array is then used as the value for
            //the field having an image data type
            //==== enrtigistre une image dans un champs image de la base de donnée.

            //  byte[] Arr = null;
            int Pointer = 0;
            int SizeOfThefile = 0;

            /*   Pointer = lOpen(SourceFile, OF_READ);
               //size of the file

               SizeOfThefile = GetFileSize(Pointer, ref lpFSHigh);
               lclose(Pointer);

               //Resize the array, then fill it with
               //the entire contents of the field

               Arr = new byte[SizeOfThefile + 1];

               FileSystem.FileOpen(1, SourceFile, OpenMode.Binary, OpenAccess.Read);

               FileSystem.FileGet(1, Arr);
               FileSystem.FileClose(1);

               adors.rows[0][strField).value = System.Text.UnicodeEncoding.Unicode.GetString(Arr);
               adors.Update();*/

            DataTable rs = new DataTable();
            SqlCommandBuilder SCBrs;
            adors.Fill(rs);
            FileStream stream = new FileStream(SourceFile, FileMode.Open, FileAccess.Read);
            BinaryReader reader = new BinaryReader(stream);
            byte[] photo = reader.ReadBytes((int)stream.Length);
            reader.Close();
            stream.Close();
            rs.Rows[0][strField] = photo;
            SCBrs = new SqlCommandBuilder(adors);
            adors.Update(rs);

        }


        private static object ErreurDesc(object iVal, object sChemin)
        {
            object functionReturnValue = null;
            // AfficherErreur de ShellExecute pour Ouvrir et Imprimer
            //SE_ERR_ASSOCINCOMPLETE
            //SE_ERR_DDEBUSY
            //SE_ERR_DDEFAIL
            //SE_ERR_DDETIMEOUT
            //SE_ERR_SHARE
            try
            {
                switch ((short)iVal)
                {
                    case SE_ERR_NOASSOC:
                        //  gFr_MsgBox "Pas d'executable associé a ce fichier.", vbInformation
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Windows n'a pas trouvé le programme qui permet d'ouvrir ce fichier.", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        break;
                    case SE_ERR_FNF:
                        // Fichier non trouvé
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier a été déplacé ou supprimé", "", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        break;
                    // gFr_MsgBox Error(53) & vbCrLf & sChemin, vbExclamation
                    case SE_ERR_PNF:
                        // Chemin non trouvé
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier a été déplacé ou supprimé", "", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        break;
                    case SE_ERR_ACCESSDENIED:
                        // accès refusé
                        break;
                    //  gFr_MsgBox Error(70), vbExclamation
                    case SE_ERR_OOM:
                        break;
                    case SE_ERR_OUT_OF_MEMORY:
                        break;
                    //  gFr_MsgBox Error(7), vbExclamation
                    case ERROR_BAD_FORMAT:
                        break;
                    //  gFr_MsgBox "Impossible de lancer l'executable associé à ce fichier.", vbExclamation
                    case SE_ERR_DLLNOTFOUND:
                        break;
                    //  gFr_MsgBox "DLL non trouvée.", vbExclamation
                    default:
                        break;
                        //  gFr_MsgBox "Erreur d'exécution '" & iVal & "'" & vbCrLf & vbCrLf & "Impossible d'ouvrir le fichier " & sChemin, vbExclamation
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, cFrModName + ",ErreurDesc");
                return functionReturnValue;
            }
        }

        /**--------------------- end*-----------------------------------*/

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public static string GetMACAddress()
        {
            //NetworkInterface[] nics = NetworkInterface.GetAllNetworkInterfaces();
            //String sMacAddress = string.Empty;
            //foreach (NetworkInterface adapter in nics)
            //{
            //    if (sMacAddress == String.Empty)// only return MAC Address from first card  
            //    {
            //        IPInterfaceProperties properties = adapter.GetIPProperties();
            //        sMacAddress = adapter.GetPhysicalAddress().ToString();
            //    }
            //}
            //return sMacAddress;
            foreach (var nic in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (nic.OperationalStatus == OperationalStatus.Up)
                    if (nic.GetPhysicalAddress().ToString() != "")
                        return nic.GetPhysicalAddress().ToString();
            }
            return "";
        }
    }
}
