﻿using System;

namespace Axe_interDT.Shared
{
    public static class CModCodeEtat
    {
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sOldEtat"></param>
        /// <param name="sNewEtat"></param>
        /// <param name="bSendPda"></param>
        /// <returns></returns>
        public static bool fc_CtrlCodeEtat(string sOldEtat, string sNewEtat, ref bool bSendPda)
        {
            bool functionReturnValue = false;

            //=== retourne true si la modification du code état est autorisé.
            //=== bSendPda est a true si faut supprimer l'inter envoyé sur un PDA.
            //=== exemple une intervention est envoyé sur le PDA à l'intervenant 003,
            //=== l'opérateur décide d'annuler cette inter, elle change le statut
            //=== 0A (envoyé) en 00 (en attente) automatiquement cette inter doit être supprimé.

            try
            {
                functionReturnValue = true;
                bSendPda = false;

                if (sOldEtat.ToUpper() == "00".ToUpper())
                {
                    if (sNewEtat.ToUpper() == "0A".ToUpper() || sNewEtat.ToUpper() == "01".ToUpper() ||
                        sNewEtat.ToUpper() == "02".ToUpper() || sNewEtat.ToUpper() == "03".ToUpper() || sNewEtat.ToUpper() == "04".ToUpper())
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "0A")
                {
                    if (sNewEtat.ToUpper() == "00".ToUpper() || sNewEtat.ToUpper() == "ZZ".ToUpper())
                    {

                        bSendPda = true;

                    }
                    else if (sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03" || sNewEtat.ToUpper() == "04")
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "01")
                {
                    if (sNewEtat.ToUpper() == "ZZ" || sNewEtat.ToUpper() == "00")
                    {

                        bSendPda = true;

                    }
                    else if (sNewEtat.ToUpper() == "0A" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03" || sNewEtat.ToUpper() == "04")
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "02")
                {
                    if (sNewEtat.ToUpper() == "00")
                    {

                        bSendPda = true;

                    }
                    else if (sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03" || sNewEtat.ToUpper() == "04")
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "03")
                {
                    if (sNewEtat.ToUpper() == "00" || sNewEtat.ToUpper() == "0A" || sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "04" || sNewEtat.ToUpper() == "ZZ")
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "04")
                {
                    if (sNewEtat.ToUpper() == "00" || sNewEtat.ToUpper() == "0A" || sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03")
                    {

                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "ZZ")
                {
                    if (sNewEtat.ToUpper() == "00" || sNewEtat.ToUpper() == "0A" || sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03" || sNewEtat.ToUpper() == "04")
                    {
                        functionReturnValue = false;
                    }
                }
                else if (sOldEtat.ToUpper() == "0A")
                {
                    if (sNewEtat.ToUpper() == "00" || sNewEtat.ToUpper() == "0A" || sNewEtat.ToUpper() == "01" || sNewEtat.ToUpper() == "02" || sNewEtat.ToUpper() == "03" || sNewEtat.ToUpper() == "04")
                    {

                        functionReturnValue = false;
                    }
                }
                //===> Mondir le 16.04.2021, https://groupe-dt.mantishub.io/view.php?id=2376#c5879 added thi condition
                else if (sOldEtat.ToUpper() == "T")
                {
                    functionReturnValue = false;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_CtrlCodeEtat;CModCodeEtat");
                return functionReturnValue;
            }
        }
    }
}
