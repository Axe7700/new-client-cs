﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    class ModGPS
    {
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::                                                                         :::
        //:::  This routine calculates the distance between two points (given the     :::
        //:::  latitude/longitude of those points). It is being used to calculate     :::
        //:::  the distance between two locations using  GeoDataSource(TM) products   :::
        //:::                                                                         :::
        //:::  Definitions:                                                           :::
        //:::    South latitudes are negative, east longitudes are positive           :::
        //:::                                                                         :::
        //:::  Passed to function:                                                    :::
        //:::    lat1, lon1 = Latitude and Longitude of point 1 (in decimal degrees)  :::
        //:::    lat2, lon2 = Latitude and Longitude of point 2 (in decimal degrees)  :::
        //:::    unit = the unit you desire for results                               :::
        //:::           where: 'M' is statute miles (default)                         :::
        //:::                  'K' is kilometers                                      :::
        //:::                  'N' is nautical miles                                  :::
        //:::                                                                         :::
        //:::  Worldwide cities and other features databases with latitude longitude  :::
        //:::  are available at http://www.geodatasource.com                          :::
        //:::                                                                         :::
        //:::  For enquiries, please contact sales@geodatasource.com                  :::
        //:::                                                                         :::
        //:::  Official Web site: http://www.geodatasource.com                        :::
        //:::                                                                         :::
        //:::             GeoDataSource.com (C) All Rights Reserved 2015              :::
        //:::                                                                         :::
        //:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::

        const double pi = 3.14159265358979;

        public static double fc_DistanceGPS(double lat1, double lon1, double lat2, double lon2, string unit)
        {
            double functionReturnValue = 0;
            //=== retourne la distance en deux ponts GPS en DD (dégres décimaux).
            double theta;
            //As Currency
            double dist;
            //As Currency

            try
            {
                theta = lon1 - lon2;


                dist = System.Math.Sin(deg2rad(lat1)) * System.Math.Sin(deg2rad(lat2)) + System.Math.Cos(deg2rad(lat1)) * System.Math.Cos(deg2rad(lat2)) * System.Math.Cos(deg2rad(theta));

                dist = acos( dist);
                dist = rad2deg( dist);

                //=== en MIles
                functionReturnValue = dist * 60 * 1.1515;



                switch (unit.ToUpper())
                {
                    case "K":
                        //== kilomtres.
                        functionReturnValue = functionReturnValue * 1.609344;
                        break;
                    case "N":
                        //== Nautical Miles.
                        functionReturnValue = functionReturnValue * 0.8684;
                        break;
                }

                functionReturnValue = functionReturnValue;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModGPS;fc_DistanceGPS");
                return functionReturnValue;
            }
        }

        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::  This function converts decimal degrees to radians             :::
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double deg2rad(double Deg)
        {
            return Convert.ToDouble(Deg * pi / 180);
        }

        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::  This function get the arccos function using arctan function   :::
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double acos(double Rad)
        {
            double functionReturnValue = 0;
            if (System.Math.Abs(Rad) != 1)
            {
                functionReturnValue = pi / 2 - System.Math.Atan(Rad / System.Math.Sqrt(1 - Rad * Rad));
            }
            else if (Rad == -1)
            {
                functionReturnValue = pi;
            }
            return functionReturnValue;
        }

        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        //:::  This function converts radians to decimal degrees             :::
        //::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
        public static double rad2deg(double Rad)
        {
            return Convert.ToDouble(Rad * 180 / pi);
        }
    }
}