﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    class ModModeleP2
    {
        static string sSQL;
        static DataTable rstmp;
        const string cCOP_NoContrat = "COP_NoContrat";

        public struct idChapitre
        {
            public int lModCos_NpAuto;
            public int lNewCos_Noauto;
        }

        public struct idGamme
        {
            public int lModIIC_NoAuto;
            public int lNewIIC_NoAuto;
        }

        public struct idSousGamme
        {
            public int lMODEqm_NoAuto;
            public int lNEWEqm_NoAuto;
        }

        public struct optContrat
        {
            public int lOptChapitre;
            public int lOptSousChapitre;
            public int lOptMateriel;
            public int lOptGamme;
            public int lOptSousGamme;
            public int lOptNatureOp;
            public int lOptPlanning;
        }
        public static void fc_AddArticle(string scode,int lngNoAuto,int lngNoAutoCat)
        {
            // ajoute la liste d'article liee à une catégorie pour un immeuble donnée.
            
            sSQL = "INSERT INTO IMA_ImmArticle ( CodeArticle, Designation1,ACT_CODE, Designation2,"
                + " Designation3, Designation4, Designation5, Designation6, Designation7, Designation8," 
                + " Designation9, Designation10, FAC_TransKobby, FAC_TransDispapt, FAC_AttentMat," 
                + " FAC_Contratuel, FAC_Facturable, FAC_NonFacturable, CodeImmeuble, CAI_Noauto, CATI_code )"
                + " SELECT FacArticle.CodeArticle, FacArticle.Designation1,ACT_CODE, FacArticle.Designation2,"
                + " FacArticle.Designation3, FacArticle.Designation4, FacArticle.Designation5," 
                + " FacArticle.Designation6, FacArticle.Designation7, FacArticle.Designation8," 
                + " FacArticle.Designation9, FacArticle.Designation10, FacArticle.FAC_TransKobby," 
                + " FacArticle.FAC_TransDispapt, FacArticle.FAC_AttentMat, FacArticle.FAC_Contratuel," 
                + " FacArticle.FAC_Facturable, FacArticle.FAC_NonFacturable,'" 
                + StdSQLchaine.gFr_DoublerQuote(scode) + "','" + lngNoAuto + "'" + " , CATI_code" + " FROM FacArticle where CAI_Noauto =" + lngNoAutoCat;
           General.Execute(sSQL);
        }
    }
}
