﻿
using System;
using System.Data;
using System.Data.SqlClient;
using static Axe_interDT.Shared.Variable;

namespace Axe_interDT.Shared
{
    class ModAnalytique
    {

        public struct Calcaffaire
        {
            public double dbMoprevue;
            public double dbAchatPrevue;
            public double dbSstPrevue;
            public double dbChargePrevue;
            public double dbVentesPrevues;
            public double dbCoefPrevue;
            public double dbMoReel { get; set; }
            public double dbAchatReel;
            public double dbSstReel;
            public double dbChargeReel;
            public double dbVentesreel;
            public double dbCoefReel;
            public double dbHtClient;
            public double dbHTBCD;
            public double dbHTFF;
            public double dbSstCapacite;
            public double dbSstCompetence;
        }
        public static Calcaffaire[] tCalcaffaire;

        public struct AnalyMO
        {
            public double dbMo;
            public int lNointervention;
        }

        static AnalyMO[] tMo;

        public static BCD[] tBCD;

        //===> Mondir le 15.11.2020, modif de la version V12.11.2020 VB6
        public static string sWhereMulti { get; set; }
        //Fin Modif Mondir

        /// <summary>
        /// Mondir le 15.11.2020, modif de la version V12.11.2020 VB6
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        public static string fc_MultiAppel(int lNumficheStandard)
        {
            string sSQL = "";
            DataTable rs = null;
            ModAdo rsModAdo = new ModAdo();
            string sWhere = "";
            int X;

            string returnValue = "";
            try
            {
                sSQL = "SELECT DISTINCT NoFacture From Intervention WHERE  NumFicheStandard = " + lNumficheStandard +
                       " AND NoFacture is not null and NoFacture <> ''";

                rs = rsModAdo.fc_OpenRecordSet(sSQL);
                if (rs == null || rs.Rows.Count == 0)
                {
                    rsModAdo.Dispose();
                    return returnValue;
                }

                foreach (DataRow rsRow in rs.Rows)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE NoFacture ='" + rsRow["NoFacture"] + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " OR NoFacture ='" + rsRow["NoFacture"] + "'";
                    }
                }

                sSQL = "SELECT DISTINCT NumFicheStandard From Intervention " + sWhere;

                rs = rsModAdo.fc_OpenRecordSet(sSQL);

                if (rs == null || rs.Rows.Count == 0)
                {
                    rsModAdo.Dispose();
                    return returnValue;
                }

                X = 0;
                sWhere = "";

                foreach (DataRow rsRow in rs.Rows)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE (Intervention.NumFicheStandard ='" + rsRow["NumFicheStandard"] + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " OR Intervention.NumFicheStandard ='" + rsRow["NumFicheStandard"] + "'";
                    }

                    X = X + 1;
                }

                sWhere = sWhere + ")";

                rsModAdo.Dispose();

                if (X == 1)
                {
                    //=== si il existe une seule fiche d'appel, on considére qu'il faut pas utiliser la clause Where.
                    returnValue = "";
                }
                else
                {
                    returnValue = sWhere;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return returnValue;
        }

        public static string fc_WhereServiceAnalytique(string sWhere)
        {
            string functionReturnValue = null;
            //===  prepare la clause where afin d'empecher l'affichage de
            //=== certains code analytique/
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;

            try
            {

                sSQL = "SELECT     CodeServiceAnalytique From ServiceAnalytique WHERE     (NePasAfficher = 1) ";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);


                foreach (DataRow Row in rs.Rows)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE CA_Num <> '" + Row["CodeServiceAnalytique"] + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  CA_Num <> '" + Row["CodeServiceAnalytique"] + "'";
                    }
                }

                modAdors.Close();

                functionReturnValue = sWhere;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModAnalytque;fc_WhereServiceAnalytique");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="tlAffaire"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sGerant"></param>
        /// <param name="lGrilleSalaire"></param>
        /// <param name="bADDentete"></param>
        /// Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020
        /// Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// <returns></returns>
        public static int fc_AnalyAffaire(int[] tlAffaire, string sCodeImmeuble = "", string sGerant = "", int lGrilleSalaire = 0, bool bADDentete = false, string sDate = null, bool bNotMulti = false, string sSQLWhereStandard = "")
        {
            int functionReturnValue = 0;

            int i = 0;
            int lETAE_NoAuto = 0;
            double dbChargesReel = 0;
            string sNodevis = "";


            //== supprime toutes les données concernant l'utilisateur, afin de preparer
            //== un nouvel état
            fc_SupEtat();//tested

            //== Efface le tableau de type
            tCalcaffaire = null;

            for (i = 0; i <= tlAffaire.Length - 1; i++)
            {

                Array.Resize(ref tCalcaffaire, i + 1);


                lETAE_NoAuto = fc_AddEtat(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant);

                //===>  Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020, ajout du sNodevis
                sNodevis = fc_Devis(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i);
                //===> Fin Modif Mondir

                //===>  Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020
                //=== modif du 29/09/2020, gestion des factures multi-fiche appel sans devis.
                sWhereMulti = "";

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (!string.IsNullOrEmpty(sSQLWhereStandard))
                {
                    sWhereMulti = sSQLWhereStandard;
                }
                else if (string.IsNullOrEmpty(sNodevis) && !bNotMulti)
                {
                    sWhereMulti = fc_MultiAppel(General.nz(tlAffaire[i], 0).ToInt());
                }
                //===> Fin Modif Mondir

                fc_BonDeCommande(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i, sDate, sWhereMulti);

                fc_FactFourn(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i, sDate, sWhereMulti);

                fc_FactSst(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i, sDate, sWhereMulti);

                fc_MO(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i, lGrilleSalaire, sDate, sWhereMulti);

                fc_FactureClient(Convert.ToInt32(General.nz(tlAffaire[i], 0)), sCodeImmeuble, sGerant, lETAE_NoAuto, i, sDate);
                functionReturnValue = lETAE_NoAuto;

                if (bADDentete == true)//tested
                {


                    dbChargesReel = tCalcaffaire[i].dbMoReel + tCalcaffaire[i].dbAchatReel + tCalcaffaire[i].dbSstReel;

                    if (dbChargesReel != 0)
                    {
                        tCalcaffaire[i].dbCoefReel = Convert.ToDouble(tCalcaffaire[i].dbVentesreel) / dbChargesReel;
                    }

                    General.sSQL = "UPDATE    ETAE_AffaireEntete SET" + " ETAE_MoReel =" + General.nz(tCalcaffaire[i].dbMoReel, 0)
                        + " , ETAE_AchatReel = " + General.nz(tCalcaffaire[i].dbAchatReel, 0) + ", EATE_VenteReel = "
                        + General.nz(tCalcaffaire[i].dbVentesreel, 0) + " , ETAE_SstPrevue = " + General.nz(tCalcaffaire[i].dbSstPrevue, 0) +
                        ", ETAE_SstReel =" + General.nz(tCalcaffaire[i].dbSstReel, 0) + ", ETAE_CoefReel = " + General.nz(tCalcaffaire[i].dbCoefReel, 0)
                        + " WHERE     (ETAE_Noauto = " + lETAE_NoAuto + ") ";

                    General.Execute(General.sSQL);
                }

            }
            return functionReturnValue;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <param name="lETAE_NoAuto"></param>
        /// <param name="lindex"></param>
        /// <param name="lGrilleSalaire"></param>
        /// ===>  Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020, ajout du sWhereMulti = ""
        private static void fc_MO(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto, int lindex, int lGrilleSalaire, string sDate = null, string sWhereMulti = "")
        {


            string sqlWhere = null;
            double dbTotHT = 0;
            double dbTempTotMO = 0;
            DataTable rsAdd = default(DataTable);
            DataTable rsMo = default(DataTable);
            ModAdo rsMoAdo = new ModAdo();
            ModAdo rsAddAdo = new ModAdo();
            int X = 0;

            tMo = null;

            General.sSQL = "SELECT     GestionStandard.NumFicheStandard, Intervention.DateRealise,"
                + " Intervention.NoIntervention, Intervention.HeureDebut," + " Intervention.HeureFin, Intervention.Duree, "
                + " Intervention.Debourse AS MO, Intervention.CodeEtat," + " Immeuble.CodeImmeuble, Intervention.Intervenant, Debourse "
                + " FROM         GestionStandard" + " INNER JOIN Intervention " + " ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard "
                + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble";


            // ===>  Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020, ajout du Intervention.CodeEtat =  'TS'
            sqlWhere = " WHERE   (Intervention.CodeEtat = 'AF' OR" + " Intervention.CodeEtat = 'E' OR"
                + " Intervention.CodeEtat = 'S' OR" + " Intervention.CodeEtat = 'T' OR"
                + " Intervention.CodeEtat = 'P3' OR" + " Intervention.CodeEtat =  'D' or Intervention.CodeEtat =  'TS' )";
            //===> Fin Modif

            // ===>  Mondir le 15.11.2020 pour intégrer les modfis de la version V12.11.2020
            if (!string.IsNullOrEmpty(sWhereMulti))
            {
                sqlWhere = sqlWhere + " " + sWhereMulti.Replace("WHERE", "AND");
            }
            else
            {
                if (lnumfichestandard != 0)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";

                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                    }
                }
            }
            //===> Fin Modif Mondir


            if (lnumfichestandard != 0)//tested
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                }

            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sDate) && General.IsDate(sDate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.DateRealise <='" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND  Intervention.DateRealise <='" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                }
            }

            General.sSQL = General.sSQL + sqlWhere + " ORDER BY GestionStandard.NumFicheStandard DESC, Immeuble.CodeImmeuble,Intervention.NoIntervention ";

            rsMo = rsMoAdo.fc_OpenRecordSet(General.sSQL);

            if (lGrilleSalaire == 1)//tested
            {
                //== charge les taux horaires.
                ModCalcul.fc_LoadChargePers("");
            }

            //== calcul du total HT.
            dbTotHT = 0;
            X = 0;
            if (rsMo.Rows.Count > 0)
            {

                foreach (DataRow r in rsMo.Rows)
                {

                    if (!string.IsNullOrEmpty(r["DateRealise"].ToString()) && Convert.ToDouble(General.sGrilleSalaire) == 1)
                    {

                        dbTempTotMO = ModCalcul.fc_calculMo(2, r["Duree"] + "", r["HeureDebut"]
                            + "", r["HeureFin"] + "", "", r["Intervenant"] + "", Convert.ToDateTime(r["DateRealise"] + ""), true);

                        r["Debourse"] = dbTempTotMO;
                        // rsMoAdo.Update();
                        //IIf(chkDeplacement.value = 1, True, False))
                        //            If Not IsNull(rs!Daterealise) Then
                        //                    dbTempTotMO = fc_calculMo(lTypeCalcul, rs!Duree & "", rs!HeureDebut & "", rs!HeureFin & "", _
                        //'                                sDureeFormat, rs!Intervenant & "", rs!Daterealise & "", IIf(chkDeplacement.value = 1, True, False))
                        //            End If
                        dbTotHT = dbTotHT + dbTempTotMO;
                        Array.Resize(ref tMo, X + 1);
                        tMo[X].dbMo = dbTempTotMO;
                        tMo[X].lNointervention = Convert.ToInt32(r["Nointervention"]);
                        X = X + 1;
                    }
                    else
                    {

                        dbTotHT = Convert.ToDouble(dbTotHT) + Convert.ToDouble(General.nz(r["MO"], 0));
                        Array.Resize(ref tMo, X + 1);

                        tMo[X].dbMo = Convert.ToDouble(General.nz(r["MO"], 0));
                        tMo[X].lNointervention = Convert.ToInt32(r["Nointervention"]);
                        X = X + 1;
                    }


                    ///dbTotHT = dbTotHT + dbTempTotMO
                    // rsMo.MoveNext();
                }
            }



            tCalcaffaire[lindex].dbMoReel = General.FncArrondir(dbTotHT, 2);

            General.sSQL = "SELECT ETAE_Noauto, CodeImmeuble, Code1,  Utilisateur, " + " ETAM_NumFicheStandard, ETAM_DateRealise, ETAM_NoIntervention, ETAM_HeureDebut, "
                + " ETAM_HeureFin , ETAM_Duree, ETAM_Mo, ETAM_CodeEtat, ETAM_Intervenant" + " From ETAM_AffaireMo" + " WHERE   ETAM_Noauto = 0";

            rsAdd = rsAddAdo.fc_OpenRecordSet(General.sSQL);
            if (rsMo.Rows.Count > 0)
            {

                foreach (DataRow r in rsMo.Rows)
                {
                    // rsAdd.AddNew();
                    var newRow = rsAdd.NewRow();
                    newRow["ETAE_Noauto"] = lETAE_NoAuto;
                    newRow["CodeImmeuble"] = r["CodeImmeuble"];
                    newRow["Code1"] = sCode1;
                    newRow["Utilisateur"] = General.fncUserName();
                    newRow["ETAM_NumFicheStandard"] = r["Numfichestandard"];
                    newRow["ETAM_dateRealise"] = r["DateRealise"];
                    newRow["ETAM_NoIntervention"] = r["Nointervention"];
                    newRow["ETAM_HeureDebut"] = r["HeureDebut"];
                    newRow["ETAM_HeureFin"] = r["HeureFin"];
                    newRow["ETAM_Duree"] = r["Duree"];

                    if (lGrilleSalaire == 0)
                    {
                        newRow["ETAM_Mo"] = General.nz(r["MO"], 0);
                    }
                    else if (lGrilleSalaire == 1)
                    {
                        newRow["ETAM_Mo"] = 0;

                        for (X = 0; X <= tMo.Length - 1; X++)
                        {
                            if (Convert.ToInt32(r["Nointervention"]) == tMo[X].lNointervention)
                            {
                                newRow["ETAM_Mo"] = tMo[X].dbMo;
                                break;
                            }
                        }
                    }

                    newRow["ETAM_CodeEtat"] = r["CodeEtat"];
                    newRow["ETAM_intervenant"] = r["Intervenant"];
                    rsAdd.Rows.Add(newRow);
                    rsAddAdo.Update();

                    //rsMo.MoveNext();
                }
            }
            rsAdd.Dispose();

            rsAdd = null;

            //ssMO.ReBind
        }
        private static void fc_FactFourn(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto, int lindex, string sDate = null, string sWhereMulti = "")
        {

            string sqlWhere = null;
            double dbTotHT = 0;
            DataTable rsAdd = default(DataTable);
            DataTable rsFF = default(DataTable);
            ModAdo rsFFModAdo = new ModAdo();
            ModAdo rsAddModAdo = new ModAdo();

            General.sSQL = "SELECT FactFournEntete.EC_NoFacture, FactFournEntete.DateSaisie," + " FactFournEntete.DateLivraison, "
                + " fournisseurArticle.Raisonsocial, FactFournEntete.RefFacture, FactFournPied.NoBCmd, " + " FactFournPied.Montant , FactFournPied.CodeImmeuble, FactFournEntete.NoAutoFacture "
                + " FROM         FactFournEntete INNER JOIN" + " FactFournPied " + " ON FactFournEntete.NoAutoFacture = FactFournPied.NoAutoFacture " + " LEFT OUTER JOIN fournisseurArticle "
                + " ON FactFournEntete.CodeFournisseur = fournisseurArticle.Cleauto " + " INNER JOIN BonDeCommande " + " ON FactFournPied.NoBCmd = BonDeCommande.NoBonDeCommande"
                + " INNER JOIN Intervention " + " ON BonDeCommande.NoIntervention = Intervention.NoIntervention" + " INNER JOIN Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

            //Set rsFF = fc_OpenRecordSet(sSQL)

            if (!string.IsNullOrEmpty(General.sWhereFf))
            {
                //===> Mondir le 15.11.2020, pour ajouté les modifs de la version V12.11.2020
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = General.sWhereFf + " " + sWhereMulti.Replace("WHERE", "AND");
                }
                else
                {
                    sqlWhere = General.sWhereFf;
                }
                //===> Fin Modif Mondir
            }
            else
            {
                //===> Mondir le 15.11.2020, pour ajouté les modifs de la version V12.11.2020
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = sWhereMulti;
                }
                else
                {
                    sqlWhere = "";
                }
                //===> Fin Modif Mondir
            }

            //===> Mondir le 15.11.2020, pour ajouté les modifs de la version V12.11.2020
            if (string.IsNullOrEmpty(sWhereMulti))
            {
                if (lnumfichestandard != 0) //tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                }
            }
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }
            if (!string.IsNullOrEmpty(sDate) && General.IsDate(sDate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND  FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                }
            }

            General.sSQL = General.sSQL + sqlWhere;

            rsFF = rsFFModAdo.fc_OpenRecordSet(General.sSQL);

            //== calcul du total HT.
            dbTotHT = 0;

            foreach (DataRow r in rsFF.Rows)//tested
            {
                dbTotHT = dbTotHT + Convert.ToDouble(General.nz(r["Montant"], 0));
                //  rsFF.MoveNext();
            }


            // txtHTFF = FncArrondir(dbTotHT, 2)
            tCalcaffaire[lindex].dbAchatReel = General.FncArrondir(dbTotHT, 2);
            // tCalcaffaire(0).dbHTBCD = FncArrondir(dbTotHT, 2)
            //== insere dans table pour preparation etat.


            General.sSQL = "SELECT     ETAF_NoAuto, ETAE_Noauto, Utilisateur, CodeImmeuble," + " Code1, ETAF_Nofacture, ETAF_DateSaisie, ETAF_RaisonSocial, ETAF_RefFacture, "
                + " ETAF_NoBcmd , ETAF_Mt, ETAF_NoAutoFacture" + " From ETAF_AffaireFactFourn" + " WHERE     (ETAF_NoAuto = 0)";

            rsAdd = rsAddModAdo.fc_OpenRecordSet(General.sSQL);
            if (rsFF.Rows.Count > 0)
            {
                foreach (DataRow r in rsFF.Rows)
                {
                    //rsAdd.AddNew();
                    var newRow = rsAdd.NewRow();
                    newRow["ETAE_Noauto"] = lETAE_NoAuto;
                    newRow["Utilisateur"] = General.fncUserName();
                    newRow["CodeImmeuble"] = r["CodeImmeuble"];
                    newRow["Code1"] = sCode1;
                    newRow["ETAF_Nofacture"] = r["ec_nofacture"];
                    //rsAdd!ETAF_DateSaisie = rsFF!datesaisie
                    newRow["ETAF_DateSaisie"] = r["DateLivraison"];
                    newRow["ETAF_RaisonSocial"] = r["RaisonSocial"];
                    newRow["ETAF_RefFacture"] = r["RefFacture"];
                    newRow["ETAF_NoBcmd"] = r["NoBcmd"];
                    newRow["ETAF_Mt"] = r["Montant"];
                    newRow["ETAF_NoAutoFacture"] = r["NoAutoFacture"];
                    rsAdd.Rows.Add(newRow);
                    rsAddModAdo.Update();
                    // rsFF.MoveNext();
                }
            }

            rsAdd.Dispose();
            rsAdd = null;

            //ssFactFourn.ReBind

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <param name="lETAE_NoAuto"></param>
        /// <param name="lindex"></param>
        /// Mondir le 16.11.2020 pour integrer les modfis de la version V12.11.2020, ajout de sWhereMulti = ""
        private static void fc_FactSst(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto, int lindex, string sDate = null, string sWhereMulti = "")
        {

            string sqlWhere = null;
            double dbTotHT = 0;
            DataTable rsAdd = default(DataTable);
            DataTable rsFF = default(DataTable);
            ModAdo rsFFModAdo = new ModAdo();
            ModAdo rsAddModAdo = new ModAdo();

            General.sSQL = "SELECT FactFournEntete.EC_NoFacture, FactFournEntete.DateSaisie,"
                + " FactFournEntete.DateLivraison, "
                + " fournisseurArticle.Raisonsocial, FactFournEntete.RefFacture, FactFournPied.NoBCmd, "
                + " FactFournPied.Montant , FactFournPied.CodeImmeuble, FactFournEntete.NoAutoFacture,fournisseurArticle.TYFO_Code  "
                + " FROM         FactFournEntete INNER JOIN"
                + " FactFournPied "
                + " ON FactFournEntete.NoAutoFacture = FactFournPied.NoAutoFacture "
                + " LEFT OUTER JOIN fournisseurArticle "
                + " ON FactFournEntete.CodeFournisseur = fournisseurArticle.Cleauto "
                + " INNER JOIN BonDeCommande "
                + " ON FactFournPied.NoBCmd = BonDeCommande.NoBonDeCommande"
                + " INNER JOIN Intervention "
                + " ON BonDeCommande.NoIntervention = Intervention.NoIntervention"
                + " INNER JOIN Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

            //Set rsFF = fc_OpenRecordSet(sSQL)

            sqlWhere = "";

            if (!string.IsNullOrEmpty(General.sWhereSst))
            {
                //===> Mondir le 16.11.2020 pour integrer les modfis de la version V12.11.2020
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = sWhereMulti.Replace("WHERE", "AND");
                    sqlWhere = General.sWhereSst + sqlWhere;
                }
                else
                {
                    sqlWhere = General.sWhereSst;
                }
                //===> Fin Modif Mondir
            }
            else
            {
                //===> Mondir le 16.11.2020 pour integrer les modfis de la version V12.11.2020
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = sWhereMulti;
                }
                else
                {
                    sqlWhere = "";
                }
                //===> Fin Modif Mondir
            }

            //===> Mondir le 16.11.2020 pour integrer les modfis de la version V12.11.2020, add string.IsNullOrEmpty(sWhereMulti)
            if (string.IsNullOrEmpty(sWhereMulti))
            {
                if (lnumfichestandard != 0)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                }
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }
            if (!string.IsNullOrEmpty(sDate) && General.IsDate(sDate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sDate) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND  FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sDate) + "'";
                }
            }
            General.sSQL = General.sSQL + sqlWhere;

            rsFF = rsFFModAdo.fc_OpenRecordSet(General.sSQL);

            //== calcul du total HT.
            dbTotHT = 0;
            tCalcaffaire[lindex].dbSstCapacite = 0;
            tCalcaffaire[lindex].dbSstCompetence = 0;
            if (rsFF.Rows.Count > 0)
            {
                foreach (DataRow r in rsFF.Rows)
                {
                    dbTotHT = dbTotHT + Convert.ToDouble(General.nz(r["Montant"], 0));
                    if (Convert.ToInt32(General.nz(r["TYFO_Code"], 0)) == 4)
                    {
                        // '=== competence
                        tCalcaffaire[lindex].dbSstCompetence = tCalcaffaire[lindex].dbSstCompetence + Convert.ToDouble(General.nz(r["Montant"], 0));
                    }
                    else
                    {
                        tCalcaffaire[lindex].dbSstCapacite = tCalcaffaire[lindex].dbSstCapacite + Convert.ToDouble(General.nz(r["Montant"], 0));
                    }
                    // rsFF.MoveNext();
                }

            }

            // txtHTFF = FncArrondir(dbTotHT, 2)
            tCalcaffaire[lindex].dbSstReel = General.FncArrondir(dbTotHT, 2);
            // tCalcaffaire(0).dbHTBCD = FncArrondir(dbTotHT, 2)
            //== insere dans table pour preparation etat.


            General.sSQL = "SELECT     ETAS_NoAuto, ETAE_Noauto, Utilisateur, CodeImmeuble," + " Code1, ETAS_Nofacture, ETAS_DateSaisie, ETAS_RaisonSocial, ETAS_RefFacture, "
                + " ETAS_NoBcmd , ETAS_Mt, ETAS_NoAutoFacture" + " From ETAS_AffaireFactSst" + " WHERE     (ETAS_NoAuto = 0)";

            rsAdd = rsAddModAdo.fc_OpenRecordSet(General.sSQL);
            if (rsFF.Rows.Count > 0)//tested
            {
                // rsFF.MoveFirst();

                foreach (DataRow r in rsFF.Rows)
                {
                    //rsAdd.AddNew();
                    var newRow = rsAdd.NewRow();
                    newRow["ETAE_Noauto"] = lETAE_NoAuto;
                    newRow["Utilisateur"] = General.fncUserName();
                    newRow["CodeImmeuble"] = r["CodeImmeuble"];
                    newRow["Code1"] = sCode1;
                    newRow["ETAS_Nofacture"] = r["ec_nofacture"];
                    //rsAdd!ETAF_DateSaisie = rsFF!datesaisie
                    newRow["ETAS_DateSaisie"] = r["DateLivraison"];
                    newRow["ETAS_RaisonSocial"] = r["RaisonSocial"];
                    newRow["ETAS_RefFacture"] = r["RefFacture"];
                    newRow["ETAS_NoBcmd"] = r["NoBcmd"];
                    newRow["ETAS_Mt"] = r["Montant"];
                    newRow["ETAS_NoAutoFacture"] = r["NoAutoFacture"];
                    rsAdd.Rows.Add(newRow);
                    rsAddModAdo.Update();
                    //  rsFF.MoveNext();
                }
            }
            rsAdd.Dispose();

            rsAdd = null;

            //ssFactFourn.ReBind

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <param name="lETAE_NoAuto"></param>
        /// <param name="lindex"></param>
        private static void fc_FactureClient(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto, int lindex, string sDate = null)
        {

            string sqlWhere = null;
            double dbTotHT = 0;
            DataTable rsAdd = default(DataTable);
            DataTable rsFClient = default(DataTable);
            ModAdo rsAddModAdo = new ModAdo();
            ModAdo rsFClientModAdo = new ModAdo();

            General.sSQL = "SELECT DISTINCT " + " FactureManuelleEnTete.NoFacture, FactureManuelleEnTete.DateFacture," + " FactureManuellePied.MontantHT AS Ventes, Immeuble.CodeImmeuble,"
                + " Intervention.NumFicheStandard" + " FROM         Intervention " + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble "
                + " LEFT OUTER JOIN FactureManuelleEnTete " + " ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture" + " LEFT OUTER JOIN FactureManuellePied "
                + " ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle";

            sqlWhere = " WHERE  (FactureManuelleEnTete.Facture = 1)";

            if (lnumfichestandard != 0)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                }

            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }
            if (!string.IsNullOrEmpty(sDate) && General.IsDate(sDate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE FactureManuelleEnTete.DateFacture <='" + Convert.ToDateTime(sDate) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND FactureManuelleEnTete.DateFacture <='" + Convert.ToDateTime(sDate) + "'";
                }
            }
            General.sSQL = General.sSQL + sqlWhere + "ORDER BY FactureManuelleEnTete.NoFacture, Immeuble.CodeImmeuble";

            rsFClient = new DataTable();

            rsFClient = rsFClientModAdo.fc_OpenRecordSet(General.sSQL);
            //  Set rsFClient = fc_OpenRecordSet(sSQl)

            //== calcul du total HT.
            dbTotHT = 0;
            foreach (DataRow r in rsFClient.Rows)
            {

                dbTotHT = dbTotHT + Convert.ToDouble(General.nz(r["Ventes"], 0));

                // rsFClient.MoveNext();
            }

            tCalcaffaire[lindex].dbVentesreel = dbTotHT;

            if (rsFClient.Rows.Count > 0)
            {
                // rsFClient.MoveFirst();





                //== insére dans table pour prepa&ration état.

                General.sSQL = "SELECT     ETAC_NoAuto, ETAE_Noauto, Utilisateur, CodeImmeuble," + " Code1, ETAC_Nofacture, ETAC_dateFacture, ETAC_Mtht, ETAC_Numfichestandard"
                    + " From ETAC_AffaireClient" + " WHERE     (ETAC_NoAuto = 0)";

                rsAdd = rsAddModAdo.fc_OpenRecordSet(General.sSQL);

                foreach (DataRow r in rsFClient.Rows)//tested
                {
                    //rsAdd.AddNew();
                    var newRow = rsAdd.NewRow();
                    newRow["ETAE_Noauto"] = lETAE_NoAuto;
                    newRow["Utilisateur"] = General.fncUserName();
                    newRow["CodeImmeuble"] = r["CodeImmeuble"];
                    newRow["Code1"] = sCode1;
                    newRow["ETAC_Nofacture"] = r["NoFacture"];
                    newRow["ETAC_Datefacture"] = r["DateFacture"];
                    newRow["ETAC_MtHt"] = r["Ventes"];
                    newRow["ETAC_Numfichestandard"] = r["Numfichestandard"];
                    rsAdd.Rows.Add(newRow);
                    rsAddModAdo.Update();
                    // rsFClient.MoveNext();
                }
            }
            rsAdd?.Dispose();

            rsAdd = null;

            //  ssFClient.ReBind



        }
        /// <summary>
        /// tested mais TODO LA VALEUR tBCD[i].dbSolde apres la ,
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <param name="lETAE_NoAuto"></param>
        /// <param name="lindex"></param>
        /// Mondir le 15.11.2020 pour integrer les modifs de la version V12.11.2020, ajout du string sWhereMulti = ""
        private static void fc_BonDeCommande(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto, int lindex, string sDate = null, string sWhereMulti = "")
        {

            string sqlWhere = null;
            int i = 0;
            int lNoBonDeCommande = 0;
            string sQte = null;
            double dbTotHT = 0;
            bool bFirst = false;
            DataTable rsAdd = default(DataTable);
            DataTable rsBon = default(DataTable);
            ModAdo rsAddModAdo = new ModAdo();
            ModAdo rsBonModAdo = new ModAdo();

            General.sSQL = "SELECT BonDeCommande.NoBonDeCommande, BonDeCommande.DateCommande," + " fournisseurArticle.RaisonSocial, GestionStandard.NumFicheStandard,"
                + " Intervention.NoIntervention , Immeuble.CodeImmeuble, Intervention.CodeEtat" + " , BCD_Detail.BCD_Quantite, BCD_Detail.BCD_PrixHT, BonDeCommande.ServiceAnalytique"
                + " FROM         fournisseurArticle" + " RIGHT OUTER JOIN GestionStandard INNER JOIN" + " Intervention " + " ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard "
                + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble " + " INNER JOIN BonDeCommande " + " ON Intervention.NoIntervention = BonDeCommande.NoIntervention "
                + " INNER JOIN BCD_Detail " + " ON BonDeCommande.NoBonDeCommande = BCD_Detail.BCD_Cle ON fournisseurArticle.Cleauto = BonDeCommande.CodeFourn";

            sqlWhere = "";

            if (!string.IsNullOrEmpty(sWhereMulti))
            {
                //===> Mondir le 15.11.2020 pour integrer les modifs de la version V12.11.2020
                sqlWhere = sWhereMulti;
                //===> Fin Modif Mondir
            }
            else
            {
                //===> Mondir le 15.11.2020 pour integrer les modifs de la version V12.11.2020
                if (lnumfichestandard != 0)
                {
                    sqlWhere = " WHERE GestionStandard.NumFicheStandard=" + lnumfichestandard + "";
                }
                //===> Fin Modif Mondir
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sDate) && General.IsDate(sDate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = "  WHERE BonDeCommande.DateCommande<='" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND  BonDeCommande.DateCommande<= '" + Convert.ToDateTime(sDate).ToShortDateString() + " 23:59:59" + "'";
                    //'sqlWhere = sqlWhere & " AND BonDeCommande.DateCommande <='" & Format(sDate & " 29:59", FormatDateSQL) & "'"
                }
            }

            General.sSQL = General.sSQL + sqlWhere + " Order by NoBonDeCommande DESC, BonDeCommande.DateCommande DESC";

            rsBon = rsBonModAdo.fc_OpenRecordSet(General.sSQL);

            if (rsBon.Rows.Count == 0)//tested
            {
                return;
            }
            lNoBonDeCommande = 0;
            tBCD = null;
            i = 0;
            bFirst = true;
            foreach (DataRow r in rsBon.Rows)//tested
            {


                if (lNoBonDeCommande != Convert.ToInt32(General.nz(r["NoBonDeCommande"], 0)))
                {
                    if (bFirst == true)
                    {
                        i = 0;
                        bFirst = false;
                    }
                    else
                    {
                        i = i + 1;
                    }
                    Array.Resize(ref tBCD, i + 1);

                    tBCD[i].lNoBonDeCommande = Convert.ToInt32(General.nz(r["NoBonDeCommande"], 0));

                    tBCD[i].sDateCommande = General.nz(r["DateCommande"], "").ToString();

                    tBCD[i].sNomFourn = General.nz(r["RaisonSocial"], "").ToString();

                    tBCD[i].lnumfichestandard = Convert.ToInt32(General.nz(r["Numfichestandard"], 0));

                    tBCD[i].lNoIntervention = Convert.ToInt32(General.nz(r["Nointervention"], 0));

                    tBCD[i].sCodeImmeuble = General.nz(r["CodeImmeuble"], "").ToString();

                    tBCD[i].sCodeEtat = General.nz(r["CodeEtat"], "").ToString();

                    tBCD[i].sServiceAnaly = General.nz(r["ServiceAnalytique"], "").ToString();


                    lNoBonDeCommande = Convert.ToInt32(General.nz(r["NoBonDeCommande"], 0));

                }


                sQte = General.nz(r["BCD_Quantite"], "").ToString().Trim();
                sQte = sQte.Replace(" ", "");

                if (General.IsNumeric(sQte))//tested
                {

                    tBCD[i].dbTotHT = tBCD[i].dbTotHT + Convert.ToDouble(General.nz(sQte, 0)) * Convert.ToDouble(General.nz(r["BCD_PrixHT"], 0));
                }

                // rsBon.MoveNext();
            }

            rsBon.Dispose();

            rsBon = null;

            //== alimente le tableau mode additem.
            General.sSQL = "";
            dbTotHT = 0;
            decimal curtemp = default(decimal);

            for (i = 0; i <= tBCD.Length - 1; i++)//tested
            {
                General.sSQL = "";
                General.sSQL = General.sSQL + "!" + tBCD[i].lNoBonDeCommande + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].sDateCommande + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].sNomFourn + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].lnumfichestandard + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].lNoIntervention + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].sCodeImmeuble + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].sCodeEtat + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].dbTotHT + "!;";
                General.sSQL = General.sSQL + "!" + tBCD[i].sServiceAnaly + "!;";


                //==== modif du 14/02/2017.
                var tmp = new ModAdo();
                General.sSQL = "SELECT     SUM(Montant) AS tot From FactFournPied WHERE NoBCmd = " + tBCD[i].lNoBonDeCommande;
                General.sSQL = tmp.fc_ADOlibelle(General.sSQL);

                tBCD[i].dbTotFacture = Convert.ToDouble(General.nz(General.sSQL, 0));
                General.sSQL = General.sSQL + "!" + General.FncArrondir(tBCD[i].dbTotFacture, 2) + "!;";
                //TODO ERREUR =============> eleminer ToString("#.###")
                tBCD[i].dbSolde = Convert.ToDouble(General.nz(tBCD[i].dbTotHT.ToString("#.###"), 0)) - tBCD[i].dbTotFacture;
                curtemp = (decimal)(tBCD[i].dbSolde);

                //=== modif du 18 09 2017, utilisation d un currency (en double 23690.3 - 23690.29 donne -9.99999999839929E-03w
                //sSQL = sSQL & "!" & FncArrondir(tBCD(i).dbSolde, 2) & "!;"
                General.sSQL = General.sSQL + "!" + curtemp + "!;";



                dbTotHT = dbTotHT + tBCD[i].dbTotHT;

            }


            tCalcaffaire[lindex].dbHTBCD = General.FncArrondir(dbTotHT, 2);

            //== ajout dans table pour preparation etat.

            General.sSQL = "SELECT     ETAE_Noauto, Utilisateur, CodeImmeuble, Code1," + " ETAB_NoBonDeCommande, ETAB_Datecommande, ETAB_RaisonSocial," + " ETAB_NumFicheStandard, "
                + " ETAB_NoIntervention , ETAB_CodeEtat, ETAB_Quantite, ETAB_PrixHT, ETAB_TotFacture, ETAB_Solde " + " From ETAB_AffaireCommande" + " WHERE     (ETAB_Noauto =0)";

            rsAdd = rsAddModAdo.fc_OpenRecordSet(General.sSQL);

            for (i = 0; i <= tBCD.Length - 1; i++)//tested
            {
                // rsAdd.AddNew();
                var newRow = rsAdd.NewRow();
                newRow["ETAE_Noauto"] = lETAE_NoAuto;
                newRow["Utilisateur"] = General.fncUserName();
                newRow["CodeImmeuble"] = tBCD[i].sCodeImmeuble;
                newRow["Code1"] = sCode1;
                newRow["ETAB_Nobondecommande"] = tBCD[i].lNoBonDeCommande;
                newRow["ETAB_DateCommande"] = tBCD[i].sDateCommande;
                newRow["ETAB_RaisonSocial"] = tBCD[i].sNomFourn;
                newRow["ETAB_NumFicheStandard"] = tBCD[i].lnumfichestandard;
                newRow["ETAB_NoIntervention"] = tBCD[i].lNoIntervention;
                newRow["ETAB_CodeEtat"] = tBCD[i].sCodeEtat;
                newRow["ETAB_PrixHT"] = tBCD[i].dbTotHT;
                newRow["ETAB_TotFacture"] = tBCD[i].dbTotFacture;
                newRow["ETAB_Solde"] = tBCD[i].dbSolde;
                rsAdd.Rows.Add(newRow);

                rsAddModAdo.Update();
            }
            rsAdd.Dispose();

            rsAdd = null;

        }
        /// <summary>
        /// tested
        /// ===> Mondir le 15.11.2020, ajout de la version V12.11.2020, changer le type de retour de la fonction, void ==> string
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <param name="lEAD_NoAUto"></param>
        /// <param name="lindex"></param>
        private static string fc_Devis(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lEAD_NoAUto, int lindex)
        {
            string sqlWhere = null;
            double dbTotHT = 0;
            double totMO = 0;
            double totAchat = 0;
            double totSst = 0;
            double dbCoefPrevue = 0;
            DataTable rsAdd = default(DataTable);
            DataTable rsDevis = default(DataTable);

            ModAdo rsAddModAdo = new ModAdo();
            ModAdo rsDevisModAdo = new ModAdo();

            bool bof = false;
            bool bEof = false;

            var returnValue = "";
            //=== retourne le numéro de devis.


            General.sSQL = "SELECT     DevisEnTete.NumeroDevis, DevisEnTete.DateCreation, GestionStandard.NumFicheStandard," + " SUM(DevisDetail.MOd) AS [MO Prevues], "
                + " SUM(DevisDetail.FOd) AS [Achats prévus], SUM(DevisDetail.STd) AS [Sous Traitance]," + " DevisEnTete.TotalHT, Immeuble.Code1," + " Immeuble.CodeImmeuble"
                + " FROM         GestionStandard INNER JOIN" + " Immeuble ON GestionStandard.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN"
                + " DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis LEFT OUTER JOIN" + " DevisDetail ON DevisEnTete.NumeroDevis = DevisDetail.NumeroDevis";

            sqlWhere = "";

            if (lnumfichestandard != 0)//tested
            {
                sqlWhere = " WHERE GestionStandard.NumFicheStandard=" + lnumfichestandard + "";
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            General.sSQL = General.sSQL + sqlWhere;

            General.sSQL = General.sSQL + " GROUP BY GestionStandard.NumFicheStandard, DevisEnTete.TotalHT," + " DevisEnTete.NumeroDevis, DevisEnTete.DateCreation, " + " Immeuble.Code1, Immeuble.CodeImmeuble ";

            rsDevis = new DataTable();

            rsDevis = rsDevisModAdo.fc_OpenRecordSet(General.sSQL);


            //== calcul du total HT.
            dbTotHT = 0;
            totMO = 0;
            totAchat = 0;
            totSst = 0;
            bEof = true;
            if (rsDevis.Rows.Count > 0)//tested
            {
                foreach (DataRow r in rsDevis.Rows)
                {
                    bEof = false;

                    dbTotHT = dbTotHT + Convert.ToDouble(General.nz(r["TotalHT"], 0));

                    totMO = totMO + Convert.ToDouble(General.nz(r["MO Prevues"], 0));

                    totAchat = totAchat + Convert.ToDouble(General.nz(r["Achats prévus"], 0));

                    totSst = totSst + Convert.ToDouble(General.nz(r["Sous Traitance"], 0));
                    // rsDevis.MoveNext();
                }
            }
            tCalcaffaire[lindex].dbMoprevue = totMO;
            tCalcaffaire[lindex].dbAchatPrevue = totAchat;
            tCalcaffaire[lindex].dbSstPrevue = totSst;

            tCalcaffaire[lindex].dbChargePrevue = totMO + totAchat + totSst;
            tCalcaffaire[lindex].dbVentesPrevues = dbTotHT;

            //==== ajout dans table pour état
            General.sSQL = "SELECT ETAD_NoAuto, ETAE_NoAuto, Utilisateur, CodeImmeuble," + " Code1,  ETAD_NumeroDevis, ETAD_DateCreation, ETAD_Numfichestandard, ETAD_MO, "
                + " ETAD_FO , ETAD_ST, ETAD_TotHT" + " From ETAD_AffaireDevis" + " WHERE     (ETAD_NoAuto = 0)";

            rsAdd = rsAddModAdo.fc_OpenRecordSet(General.sSQL);



            if (bEof == false)
            {
                //  rsDevis.MoveFirst();


                foreach (DataRow r in rsDevis.Rows)//tested
                {
                    //rsAdd.AddNew();
                    var newRow = rsAdd.NewRow();
                    newRow["ETAE_Noauto"] = lEAD_NoAUto;
                    newRow["Utilisateur"] = General.fncUserName();
                    var tmp = new ModAdo();
                    newRow["CodeImmeuble"] = tmp.fc_ADOlibelle("select CodeImmeuble " + " FROM gestionstandard Where NumficheStandard =" + General.nz(r["Numfichestandard"], 0));
                    newRow["Code1"] = r["Code1"];
                    newRow["ETAD_NumeroDevis"] = r["numerodevis"];

                    //===> Mondir le 15.11.2020 pour integrer les modifs de la version V12.11.2020
                    returnValue = r["numerodevis"]?.ToString();
                    //===> Fin Modif Mondir

                    newRow["ETAD_DateCreation"] = r["DateCreation"];
                    newRow["ETAD_Numfichestandard"] = r["Numfichestandard"];
                    newRow["Etad_MO"] = r["MO Prevues"];
                    newRow["ETAD_FO"] = r["Achats prévus"];
                    newRow["ETAD_ST"] = r["Sous Traitance"];
                    newRow["ETAD_Totht"] = r["TotalHT"];
                    rsAdd.Rows.Add(newRow.ItemArray);
                    rsAddModAdo.Update();

                    //rsDevis.MoveNext();
                }
            }
            rsAdd.Dispose();

            rsAdd = null;

            if (tCalcaffaire[lindex].dbChargePrevue != 0)//tested
            {

                tCalcaffaire[lindex].dbCoefPrevue = tCalcaffaire[lindex].dbVentesPrevues / tCalcaffaire[lindex].dbChargePrevue;

            }

            tCalcaffaire[lindex].dbCoefPrevue = Convert.ToDouble((General.FncArrondir(tCalcaffaire[lindex].dbCoefPrevue, 3))); //.ToString("### ##0.000");



            tCalcaffaire[lindex].dbHtClient = General.FncArrondir(dbTotHT, 2);

            //== mise à jour de l'entete pour le devis.

            General.sSQL = "UPDATE    ETAE_AffaireEntete SET" + " ETAE_MoPrevue =" + totMO + " , ETAE_AchatPrevue = " + totAchat + ", EATE_VentePrevue = "
                + dbTotHT + ", ETAE_CoefPrevue = " + tCalcaffaire[lindex].dbCoefPrevue + " WHERE     (ETAE_Noauto = " + lEAD_NoAUto + ") ";

            General.Execute(General.sSQL);
            rsDevis.Dispose();
            rsDevis = null;

            //===> Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020, change return type
            return returnValue;
            //===> Fin Modif Mondir
        }
        /// <summary>
        /// tested
        /// </summary>
        private static void fc_SupEtat()
        {
            string sSQL = null;

            sSQL = "DELETE FROM ETAE_AffaireEntete WHERE Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAD_AffaireDevis WHERE Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAB_AffaireCommande WHERE Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAM_AffaireMo WHERE Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAF_AffaireFactFourn WHERE Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAC_AffaireClient Where Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

            sSQL = "DELETE FROM ETAS_AffaireFactSst Where Utilisateur ='" + General.fncUserName() + "'";

            General.Execute(sSQL);

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        /// <returns></returns>
        private static int fc_AddEtat(int lnumfichestandard, string sCodeImmeuble, string sCode1)
        {
            int functionReturnValue = 0;
            string sSQL = null;
            DataTable rsAdd = default(DataTable);
            ModAdo rsAddModAdo = new ModAdo();

            sSQL = "SELECT     codeimmeuble, ETAE_Noauto, Utilisateur, Numfichestandard, Code1" + " From ETAE_AffaireEntete" + " WHERE     (ETAE_Noauto = 0)";

            rsAdd = rsAddModAdo.fc_OpenRecordSet(sSQL);
            int ETAE_Noauto = 0;
            SqlCommandBuilder cb = new SqlCommandBuilder(rsAddModAdo.SDArsAdo);
            rsAddModAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            rsAddModAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            rsAddModAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            rsAddModAdo.SDArsAdo.InsertCommand = insertCmd;

            rsAddModAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {

                if (e.StatementType == StatementType.Insert)
                {

                    ETAE_Noauto =
                            Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());

                }
            });
            //rsAdd.AddNew();
            var newRow = rsAdd.NewRow();
            newRow["CodeImmeuble"] = sCodeImmeuble;
            newRow["Utilisateur"] = General.fncUserName();
            newRow["Numfichestandard"] = lnumfichestandard;
            newRow["Code1"] = sCode1;
            rsAdd.Rows.Add(newRow);
            rsAddModAdo.Update();

            //rsAdd.MoveLast();

            functionReturnValue = ETAE_Noauto;


            rsAdd.Dispose();

            rsAdd = null;
            return functionReturnValue;

        }
    }
}
