﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.View.Theme;
using CrystalDecisions.CrystalReports.Engine;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.Shared;

namespace Axe_interDT.Shared
{
    public static class devModeCrystal
    {
        public struct tpFormulas
        {
            public string sNom;
            public string sCritere;
            //Permet de convertir en numérique les paramètres concernés
            public string sType;
        }

        ///Public tabFormulas()    As tpFormulas

        public struct typeParamReport
        {
            public string sName;
            public object sValue;
            public object sValueDe;
            public object sValueAu;
        }

        public static typeParamReport[] tabParamReports;

        public const string cExportNum = "NUM";
        public const string cExportChaine = "CHAINE";

        public static short AbortPrinting;

        // Flag to indicate whether there is a professional or standard version of Crystal Reports available
        public const bool CRYSTAL_PRO = true;

        // Font constants
        public const short DEFAULT_PITCH = 0;
        public const short ANSI_CHARSET = 1;
        public const short FF_DONTCARE = 0;
        public const short FW_NORMAL = 400;
        public const short FW_BOLD = 700;

        public static string fc_ExportCrystal(string sSelectionFomula, string sReport = "FactureManuelle-SQL-V9.rpt", string sCode = "",
            string sType = cExportChaine, string sFieldSelection = "", string sNomFieldSelection = "",
            System.Windows.Forms.CheckState intNoFieldSelection = System.Windows.Forms.CheckState.Unchecked)
        {
            string functionReturnValue = null;

            string sCodeImmeuble = null;
            string sTitreDevis = null;
            int i = 0;
            int j = 0;
            object x = null;
            string sFiltreDevis1 = null;
            string strTop = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            ReportDocument Report = new ReportDocument();
            object RetVal = null;
            string sNomFic = null;
            string sNomRep = null;
            short intDeroule = 0;
            string[] tabParameterName = null;
            bool bRange = false;
            //Indique qu'une plage de données a été passée en paramètre

            try
            {

                if (!File.Exists(sReport))
                    return "";

                //==============> Mondir 05.06.2020 : Sometimes the RPT file is in use or the app cant reach it, so i added this temporary solution to recheck the file
                //to fix : https://groupe-dt.mantishub.io/view.php?id=1833
                var attempt = 0;
                while (attempt < 3)
                {
                    try
                    {
                        Report.Load(sReport);
                        break;
                    }
                    catch (Exception e)
                    {
                        Thread.Sleep(1000);
                        //Application.DoEvents();
                        attempt++;
                        Program.SaveException(e, "", false);
                    }
                }

                if (attempt == 3)
                    throw new Exception("Echec du chargement du rapport");

                bRange = false;

                ///===================> Tested
                if (!string.IsNullOrEmpty(sCode))
                {
                    sNomFic = sCode.Replace("/", "-") + ".pdf";
                }
                else
                {
                    sNomFic = "Exp_" + General.Left(General.fncUserName(), 6) + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".pdf";
                }

                sNomRep = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + Application.ProductName + "\\Export\\";
                Dossier.fc_ControleAnCreateDossier(sNomRep);
                Dossier.fc_NettoieDossier(sNomRep, "*.pdf");

                ///================> Tested
                if (Dossier.fc_ControleFichier(sNomRep + sNomFic) == true)
                {
                    File.Delete(sNomRep + sNomFic);
                }

                intDeroule = 3;

                Application.DoEvents();

                ///=================> Tested
                if (ModCrystalPDF.tabFormulas != null)
                    for (i = 0; i <= ModCrystalPDF.tabFormulas.Length - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(ModCrystalPDF.tabFormulas[i].sNom))
                        {
                            if (ModCrystalPDF.tabFormulas[i].sType == cExportChaine)
                            {
                                Report.DataDefinition.FormulaFields[ModCrystalPDF.tabFormulas[i].sNom].Text = @"""" + ModCrystalPDF.tabFormulas[i].sCritere + @"""";
                            }
                            else
                            {
                                Report.DataDefinition.FormulaFields[ModCrystalPDF.tabFormulas[i].sNom].Text = ModCrystalPDF.tabFormulas[i].sCritere;
                            }
                        }
                    }

                intDeroule = 4;

                Application.DoEvents();
                if (intNoFieldSelection == 0)
                {
                    if (!string.IsNullOrEmpty(sFieldSelection.Trim()))
                    {
                        //TODO : Mondir - Look On This Commented Liness
                        Report.SetParameterValue(General.nz(sFieldSelection, "NoDevis").ToString(), sFieldSelection);
                    }
                    else
                    {
                        for (i = 0; i <= tabParamReports.Length - 1; i++)
                        {
                            if (!string.IsNullOrEmpty(tabParamReports[i].sName))
                            {
                                for (j = 1; j <= Report.ParameterFields.Count; j++)
                                {

                                    if (Report.ParameterFields[j].Name.Contains("."))
                                    {
                                        tabParameterName = Report.ParameterFields[j].Name.Split('.');
                                        tabParameterName[0] = General.Right(tabParameterName[0], tabParameterName[0].Length - 2) + ".";
                                    }
                                    else
                                    {
                                        tabParameterName = new string[1];
                                        tabParameterName[0] = "";
                                    }
                                    if (Report.ParameterFields[j].Name == tabParameterName[0] + tabParamReports[i].sName | Report.ParameterFields[j].Name == "{?" + tabParameterName[0] + tabParamReports[i].sName + "}")
                                    {
                                        //                                If sType = cExportChaine Then
                                        //                                    Report.ParameterFields(j).AddDefaultValue "" & tabParamReports(i).sValue
                                        //                                Else
                                        //                                    Report.ParameterFields(j).AddDefaultValue CLng(tabParamReports(i).sValue)
                                        //                                End If
                                        if (!string.IsNullOrEmpty(tabParamReports[i].sValue.ToString()))
                                        {
                                            if (sType == cExportChaine)
                                            {
                                                //TODO : Mondir - Look On This Commented Line
                                                //Report.ParameterFields.GetItemByName(tabParameterName[0] + tabParamReports[i].sName).SetCurrentValue("" + tabParamReports[i].sValue);
                                            }
                                            else
                                            {
                                                //TODO : Mondir - Look On This Commented Line
                                                //Report.ParameterFields.GetItemByName(tabParameterName[0] + tabParamReports[i].sName).SetCurrentValue(Convert.ToInt32(tabParamReports[i].sValue));
                                            }
                                        }
                                        if (!string.IsNullOrEmpty(tabParamReports[i].sValueDe.ToString()) && !string.IsNullOrEmpty(tabParamReports[i].sValueAu.ToString()))
                                        {
                                            bRange = true;
                                            //TODO : Mondir - Look On This Commented Line
                                            //Report.ParameterFields.GetItemByName(tabParameterName[0] + tabParamReports[i].sName).DiscreteOrRangeKind = CRAXDRT.CRDiscreteOrRangeKind.crRangeValue;
                                            if (sType == cExportChaine)
                                            {
                                                //TODO : Mondir - Look On This Commented Line
                                                //Report.ParameterFields.GetItemByName(tabParameterName[0] + tabParamReports[i].sName).AddCurrentRange(tabParamReports[i].sValueDe, tabParamReports[i].sValueAu, CRAXDRT.CRRangeInfo.crRangeIncludeLowerBound);
                                            }
                                            else
                                            {
                                                //TODO : Mondir - Look On This Commented Line
                                                //Report.ParameterFields.GetItemByName(tabParameterName[0] + tabParamReports[i].sName).AddCurrentRange(Convert.ToInt32(tabParamReports[i].sValueDe), Convert.ToInt32(tabParamReports[i].sValueAu), CRAXDRT.CRRangeInfo.crRangeIncludeLowerBound);
                                            }
                                        }
                                    }
                                    break;

                                    //                            If InStr(1, Report.ParameterFields(i + 1).Name, ".") Then
                                    //                                tabParameterName = Split(Report.ParameterFields(i + 1).Name, ".")
                                    //                                tabParameterName(0) = Right(tabParameterName(0), Len(tabParameterName(0)) - 2) & "."
                                    //                                Report.ParameterFields.GetItemByName(tabParameterName(0) & tabParamReports(i).sName).SetCurrentValue tabParamReports(i).sValue
                                    //                            Else
                                    //                                If tabParamReports(i).sValue <> "" Then
                                    //                                    If sType = cExportChaine Then
                                    //                                        Report.ParameterFields.GetItemByName(tabParamReports(i).sName).SetCurrentValue "" & tabParamReports(i).sValue
                                    //                                    Else
                                    //                                        Report.ParameterFields.GetItemByName(tabParamReports(i).sName).SetCurrentValue CLng(tabParamReports(i).sValue)
                                    //                                    End If
                                    //                                End If
                                    //                                If tabParamReports(i).sValueDe <> "" And tabParamReports(i).sValueAu <> "" Then
                                    //                                    bRange = True
                                    //                                    Report.ParameterFields.GetItemByName(tabParamReports(i).sName).DiscreteOrRangeKind = crRangeValue
                                    //                                    If sType = cExportChaine Then
                                    //                                        Report.ParameterFields.GetItemByName(tabParamReports(i).sName).AddCurrentRange tabParamReports(i).sValueDe, tabParamReports(i).sValueAu, crRangeIncludeLowerBound
                                    //                                    Else
                                    //                                        Report.ParameterFields.GetItemByName(tabParamReports(i).sName).AddCurrentRange CLng(tabParamReports(i).sValueDe), CLng(tabParamReports(i).sValueAu), crRangeIncludeLowerBound
                                    //                                    End If
                                    //                                End If
                                    //
                                    //                            End If
                                    //                    End If
                                }
                            }
                        }
                    }
                }

                intDeroule = 41;

                Application.DoEvents();
                if (bRange == false)
                {
                    //        If sSelectionFomula <> "" Then
                    //            If Report.RecordSelectionFormula <> "" Then
                    //                Report.RecordSelectionFormula = Report.RecordSelectionFormula & " AND " & sSelectionFomula
                    //            Else
                    //                Report.RecordSelectionFormula = sSelectionFomula
                    //            End If
                    //        End If
                    //    Else
                    Report.RecordSelectionFormula = sSelectionFomula;
                }

                intDeroule = 5;

                //ExportOptions CrExportOptions;
                //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                //CrDiskFileDestinationOptions.DiskFileName = sNomRep + sNomFic;
                //CrExportOptions = Report.ExportOptions;
                //{
                //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                //    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                //}
                //Report.Export();

                Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sNomRep + sNomFic);
                functionReturnValue = sNomRep + sNomFic;
                
                Report.Close();
                Report.Dispose();
                Report = null;
                GC.Collect();

                return functionReturnValue;
            }
            catch (Exception e)
            {
                functionReturnValue = "";

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message);
                Program.SaveException(e);
                return functionReturnValue;
            }
        }

        public static string fc_ExportCrystalSansFormule(string sSelectionFomula, string sReport = "", string sCode = "", string sNomRep = "")
        {
            string functionReturnValue = null;

            string sCodeImmeuble = null;
            string sTitreDevis = null;
            int i = 0;
            int j = 0;
            object x = null;
            string sFiltreDevis1 = null;
            string strTop = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            CrystalReportFormView crAppl;
            ReportDocument Report = null;
            object RetVal = null;
            string sNomFic = null;
            //Dim sNomRep             As String
            short intDeroule = 0;
            string[] tabParameterName = null;
            bool bRange = false;
            //Indique qu'une plage de données a été passée en paramètre

            try
            {

                Report = new ReportDocument();
                Report.Load(sReport);

                bRange = false;

                if (!string.IsNullOrEmpty(sCode))
                {
                    sNomFic = sCode.Replace("/", "-") + ".pdf";
                }
                else
                {
                    sNomFic = "Expt_" + General.Left(General.fncUserName().Replace(" ", ""), 10) + "_" + DateTime.Now.ToString("yyMMdd_hhmmss") + ".pdf";
                }

                if (string.IsNullOrEmpty(sNomRep))
                {
                    sNomRep = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + Application.ProductName + "\\Export\\";

                    intDeroule = 1;
                    //MsgBox intDeroule
                    Dossier.fc_ControleAnCreateDossier(sNomRep);

                    intDeroule = 2;
                    //MsgBox intDeroule
                    Dossier.fc_NettoieDossier(sNomRep, "*.pdf");

                }

                if (Dossier.fc_ControleFichier(sNomRep + sNomFic) == true)
                {
                    intDeroule = 22;
                    //MsgBox intDeroule
                    File.Delete(sNomRep + sNomFic);
                }

                intDeroule = 3;
                //MsgBox intDeroule
                System.Windows.Forms.Application.DoEvents();

                Report.RecordSelectionFormula = sSelectionFomula;

                intDeroule = 5;
                //MsgBox intDeroule

                //ExportOptions CrExportOptions;
                //DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                //PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                //CrDiskFileDestinationOptions.DiskFileName = sNomRep + sNomFic;
                //CrExportOptions = Report.ExportOptions;
                //{
                //    CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                //    CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                //    CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                //    CrExportOptions.FormatOptions = CrFormatTypeOptions;
                //}


                intDeroule = 6;
                //MsgBox intDeroule

                intDeroule = 7;
                //MsgBox intDeroule

                intDeroule = 8;
                //MsgBox intDeroule

                intDeroule = 9;
                //MsgBox intDeroule

                //Report.Export();
                Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sNomRep + sNomFic);
                intDeroule = 10;
                //MsgBox intDeroule

                functionReturnValue = sNomRep + sNomFic;
                intDeroule = 11;
                Report.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                functionReturnValue = "";

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
                Program.SaveException(ex);
                return functionReturnValue;
            }
        }

    }
}
