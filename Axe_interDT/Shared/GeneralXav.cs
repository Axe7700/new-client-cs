﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    public static class GeneralXav
    {
        public static void stockRegParam(string sargdcname, string[][] tabargsetting)
        {
            int i = 0;
            for (i = 0; i <= tabargsetting.Length - 1; i++)
            {
                General.saveInReg(sargdcname, tabargsetting[i][0], tabargsetting[i][1]);
            }
        }

        public static string fct_LookSpace(string sstr, int nstart, int nend = -1)
        {
            string functionReturnValue = null;
            string stemp = null;
            int i = 0;

            i = 0;

            if (nend == -1)
            {
                nend = sstr.Length - nstart + 1;
            }

            functionReturnValue = "";

            if (nstart > sstr.Length)
                return functionReturnValue;
            /*functionReturnValue = General.Mid(sstr, nstart, nend)*/
            functionReturnValue = General.Mid(sstr, nstart);

            if (nstart == 1)
            {
                return functionReturnValue;
            }
           // stemp = General.Mid(sstr, nstart, nend);
            stemp = General.Mid(sstr, nstart);

            while (General.Mid(sstr, nstart - (i + 1), 1) != " ")
            {
                //stemp = General.Mid(sstr, nstart - (i + 1), 1);
                //stemp = General.Mid(sstr, nstart - (i + 1), nend);
                stemp = General.Mid(sstr, nstart - (i + 1));
                stemp = General.Mid(sstr, nstart - (i + 1));
                i = i + 1;
                if (i == nstart - 1)
                    break;
            }

            functionReturnValue = stemp;
            return functionReturnValue;

        }
        public static void Sub_Type(UltraCombo otype, string[] tabrows, int nsel = -1, bool freload = false)
        {
            int i = 0;
            DataTable otypeDT = new DataTable();
            otypeDT.Columns.Add("Type");
            var NewRow = otypeDT.NewRow();
            if (otype.Rows.Count == 0 || freload == true)//tested
            {

                while (i < tabrows.Length)
                {
                    NewRow["Type"] = tabrows[i];
                    otypeDT.Rows.Add(NewRow.ItemArray);
                    otype.DataSource = otypeDT;
                    i = i + 1;
                }
            }
            else
            {
                if (nsel != -1)
                {
                    otype.ActiveRow = otype.Rows[nsel];
                }
                else
                {
                    otype.Text = "";
                }
            }
        }

        public static DateTime fc_DatePieceCLoture(DateTime dtDateFacture, string sJournal)
        {
            string sPer;
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo ModAdors = new ModAdo();

            string sPerMax;
            DateTime dtDate;

            try
            {
                sPer = dtDateFacture.Year.ToString("0000") + dtDateFacture.Month.ToString("00");

                //sSQL = "SELECT     PCF_ID, PCF_Periode, PCF_Libelle, PCF_Date, PCF_Cloture" _
                //& " From PCF_PeriodeClotureFact  " _
                //& " WHERE     (PCF_Periode = '" & sPer & "')" _
                //& " and PCF_Journal ='" & sJournal & "'"

                sSQL = "SELECT     MAX(PCF_Periode) AS MaxPer"
                        + " From PCF_PeriodeClotureFact "
                        + " WHERE     PCF_Journal ='" + sJournal + "' AND PCF_Cloture = 1";
                rs = ModAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    sPerMax = General.nz(rs.Rows[0]["MaxPer"], "").ToString();
                    //=== modif du 24 07 2019
                    if (!string.IsNullOrEmpty(sPerMax) && Convert.ToInt32(sPerMax) >= Convert.ToInt32(sPer))
                    {
                        dtDate = Convert.ToDateTime("01/" + General.Right(sPerMax, 2) + "/" + General.Left(sPerMax, 4));
                        dtDate = dtDate.AddMonths(1);
                        return dtDate;
                    }
                    else
                    {
                        return dtDateFacture;
                    }
                }
                else
                {
                    return dtDateFacture;
                }

                rs.Dispose();
                rs = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "GeneralXav;fc_DatePieceCLoture");
                return dtDateFacture;
            }
        }
    }
}
