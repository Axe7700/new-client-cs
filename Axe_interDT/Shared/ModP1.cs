﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    static class ModP1
    {
        public static SqlConnection adoP1 { get; set; }
        public static bool boolPro;


        //Public Const chaineConnectP1 = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" _
        //& "Persist Security Info=False;" _
        //& "Initial Catalog=P1_DT;" _
        //& "Data Source=" & sServeurSQL

        public static string schaineConnectP1 { get; set; }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public static bool fc_OpenConnP1()
        {
            bool functionReturnValue = false;
            try
            {

                //bP1 = true
                functionReturnValue = true;

                // ouvre une connection avec sage. sOdbc contient le nom de l'odbc Sage
                if (adoP1 == null)
                {
                    adoP1 = new SqlConnection(schaineConnectP1);
                    adoP1.Open();
                }
                else if (adoP1.State == ConnectionState.Closed)
                {
                    adoP1 = new SqlConnection(schaineConnectP1);
                    adoP1.Open();
                    //adoP1.Open cODBCP1
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                functionReturnValue = false;
                Erreurs.gFr_debug(e, "ConnectExterne.bas;fc_OpenConnP1;");
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur est survenue lors de la connexion vers le base de données du P1." + "\n" + "Contrôlez la source de données ainsi que le chemin d'accès à la base de données.", "Connexion impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_CloseConnP1()
        {
            if (adoP1 != null)
            {
                if (adoP1.State == ConnectionState.Open)
                {
                    adoP1.Close();
                    adoP1.Dispose();
                }
            }
            //  bP1 = False
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Total"></param>
        /// <returns></returns>
        public static int fc_GetNumAppereils(int Total)
        {
            int functionReturnValue = 0;
            try
            {
                int i = 0;
                int X = 0;
                DataTable rsAdo = default(DataTable);
                var modAdorsAdo = new ModAdo();

                i = 0;
                boolPro = false;


                for (i = 0; i <= 10; i++)
                {
                    rsAdo = modAdorsAdo.fc_OpenRecordSet("SELECT Autorise, NumAppareil From NumAppareils WHERE Autorise = 0");
                    if (rsAdo.Rows.Count > 0)
                    {
                        rsAdo.Rows[0]["Autorise"] = true;
                        X = Convert.ToInt32(rsAdo.Rows[0]["NumAppareil"]);
                        int xx = modAdorsAdo.Update();
                        var NewRow = rsAdo.NewRow();
                        NewRow["NumAppareil"] = Total + X;
                        NewRow["Autorise"] = false;
                        rsAdo.Rows.Add(NewRow);
                        xx = modAdorsAdo.Update();
                        functionReturnValue = X;
                        modAdorsAdo?.Dispose();
                        break;
                    }
                    modAdorsAdo?.Dispose();
                    boolPro = true;
                }

                rsAdo = null;
                if (boolPro == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention une erreur est survenue sur la numérotation des compteurs libres. Ce dernier ne peut être créé." +
                                    " Veuillez recommencer ultèrieurement.", "Attention création compteur libre annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    functionReturnValue = -1;
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                functionReturnValue = -1;
                Erreurs.gFr_debug(e, "General;fc_GetNumAppereils;");
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message);
                return functionReturnValue;
            }
        }
    }
}
