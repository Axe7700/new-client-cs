﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class ModFinance
    {
        public static double fc_Solde(string sCodeimmeuble, string sCopro, Boolean bEcrituresNonLettre = false)
        {

            DataTable rsFinance = new DataTable();
            ModAdo rsAdoFinance = new ModAdo();
            double MontantCredit = 0;
            double MontantDebit = 0;
            string compteTiers;
            string DateMini;
            string DateMaxi;
            int JourMaxiMois = 0;
            string sDateFacture;
            string sCritere;
            DataTable rsCptGdp = new DataTable();
            double dbCredit = 0;
            double dbDebit = 0;
            string sSQL = "";

            double returnF = 0;
            try
            {
                if (General.UCase(General.adocnn.Database) == General.UCase(General.cNameDelostal))
                {
                    sCritere = General.cCritereFinance;
                }
                else
                {
                    sCritere = "";
                }
                // '===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                    return 0;
                }
                dbCredit = 0;
                dbDebit = 0;
                JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt32(General.nz(General.MoisFinExercice, "12")));

                if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                {
                    DateMini = "01/" + "01" + "/" + DateTime.Now.Year;
                    DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + DateTime.Now.Year;
                }
                else
                {
                    if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= Convert.ToInt16(General.MoisFinExercice))
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year - 1);
                        //MoisFinExercice + 1
                        DateMaxi = JourMaxiMois + "/" + (Convert.ToInt16(General.MoisFinExercice)).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice
                    }

                    else
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice - 1
                        DateMaxi = JourMaxiMois + "/" + (Convert.ToInt16(General.MoisFinExercice)).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year + 1);
                        //MoisFinExercice
                    }
                }

                SAGE.fc_OpenConnSage();
                if (string.IsNullOrEmpty(sCopro))
                {
                    //compteTiers = ComboComptes.Text
                    compteTiers = "";//à traiter plus tard
                    return 0;
                }
                else
                {
                    using (var tmp = new ModAdo())
                        compteTiers = tmp.fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeuble) + "'");
                }

                if (bEcrituresNonLettre == true)
                {
                    sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    sSQL = sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    sSQL = sSQL + "CT_NUM='" + compteTiers + "'";
                    sSQL = sSQL + " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "  )" +
                            " or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

                }
                else
                {
                    sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    sSQL = sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    sSQL = sSQL + "CT_NUM='" + compteTiers + "'";
                    sSQL = sSQL + " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " +
                             " or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

                }

                rsFinance = rsAdoFinance.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);
                if (rsFinance.Rows.Count > 0)
                {
                    dbDebit = Convert.ToDouble(General.nz(rsFinance.Rows[0]["Montant_Total"], 0));
                }
                rsFinance.Dispose();
                rsAdoFinance.Dispose();

                if (bEcrituresNonLettre == true)
                {
                    sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    sSQL = sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    sSQL = sSQL + "CT_NUM='" + compteTiers + "'";
                    sSQL = sSQL + " AND ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + " ) " +
                         " or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }
                else
                {
                    sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    sSQL = sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    sSQL = sSQL + "CT_NUM='" + compteTiers + "'";
                    sSQL = sSQL + " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " +
                    " or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }
                rsFinance = rsAdoFinance.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);
                if (rsFinance.Rows.Count > 0)
                {
                    dbDebit = Convert.ToDouble(General.nz(rsFinance.Rows[0]["Montant_Total"], 0));
                }
                rsFinance.Dispose();
                returnF = Convert.ToDouble(General.nz(dbDebit - dbCredit, 0));
                SAGE.fc_CloseConnSage();
                return returnF;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_RechercheContrat");
                return returnF;
            }
        }
    }
}
