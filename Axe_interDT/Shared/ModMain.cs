﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    static class ModMain
    {
        public static string sUtilisateur;
        public static bool bWindCall;
        public static int londMsgb;
        public static bool bActivate;
        public static int sTest;
        public static string[] stabLien;
        public static int lLien;
        static DataTable rstmp;
        private static ModAdo ModAdorstmp;
        public static string sServiceFact;
        public static string sTVAfactXX;
        public struct droit
        {
            public string sFormulaire;
            public string sName;
            public string sObjet;
            public int lLecture;
            public int lEcriture;
            public int lAjout;
            public int lDelete;
        }
        public static droit[] tpDroit { get; set; }
        public static bool bMain { get; set; }

        /// <summary>
        /// Tested
        /// </summary>
        public static void ModMain_Main()
        {
            int i = 0;

            Array.Resize(ref ModCourrier.sTabAdresseMail, 1);
            ModCourrier.sTabAdresseMail[0].sNom = ModCourrier.cVide;
            ModCourrier.sTabAdresseMail[0].sMail = ModCourrier.cVide;
            bMain = true;
            General.open_conn();

            ModCalcul.fc_LoadChargePers("");
            bMain = false;


            //=== chargement de la feuille de départ.
            Array.Resize(ref stabLien, lLien + 1);
            stabLien[lLien] = General.PROGFICHEPRINCIPAL;
            bActivate = true;


            sUtilisateur = General.fncUserName();
            General.sSQL = "SELECT AUD_Code" + " FROM   AUG_AutoriUtil" + " where AUG_code ='" + sUtilisateur + "'";
            using (var tmpModAdo = new ModAdo())
                General.sDroit = tmpModAdo.fc_ADOlibelle(General.sSQL);
            if (string.IsNullOrEmpty(General.sDroit))
            {
                General.sDroit = General.cUtil;
            }

            //=== 19 oct 2003.
            tpDroit = new droit[1];
            General.sSQL = "SELECT AUT_Formulaire, AUT_Objet, AUT_Nom, AUT_Lecture, AUT_Ecriture" + ", AUT_Ajout, AUT_Delete" + " FROM AUT_Autorisation" + " WHERE AUT_Nom='" + StdSQLchaine.gFr_DoublerQuote(sUtilisateur) + "'";
            ModAdorstmp = new ModAdo();
            rstmp = ModAdorstmp.fc_OpenRecordSet(General.sSQL);
            if (rstmp.Rows.Count > 0)
            {
                tpDroit = new droit[rstmp.Rows.Count];
                i = 0;
                foreach (DataRow Row in rstmp.Rows)
                {
                    tpDroit[i].sFormulaire = General.nz(Row["AUT_Formulaire"], "").ToString();
                    tpDroit[i].sObjet = General.nz(Row["AUT_Objet"], "").ToString();
                    tpDroit[i].sName = General.nz(Row["AUT_Nom"], "").ToString();
                    tpDroit[i].lEcriture = Convert.ToInt32(General.nz(Row["AUT_Ecriture"], 0));
                    tpDroit[i].lLecture = Convert.ToInt32(General.nz(Row["AUT_Lecture"], 0));
                    tpDroit[i].lAjout = Convert.ToInt32(General.nz(Row["AUT_Ajout"], 0));
                    tpDroit[i].lDelete = Convert.ToInt32(General.nz(Row["AUT_delete"], 0));
                    i = i + 1;
                }
            }

            //
            //
            //    'récupére le nom de l'utilisateur.
            //    sUtilisateur = fncUserName
            //
            //    sUtilisateur = fncUserName
            //    sSQL = "SELECT AUD_Code" _
            //'        & " FROM   AUG_AutoriUtil" _
            //'        & " where AUG_code ='" & sUtilisateur & "'"
            //    sDroit = fc_ADOlibelle(sSQL)
            //    If sDroit = "" Then
            //        sDroit = cUtil
            //    End If
            //    '--19 oct 2003
            //    ReDim tpDroit(0)
            //    sSQL = "SELECT AUT_Formulaire, AUT_Objet, AUT_Nom, AUT_Lecture, AUT_Ecriture" _
            //'        & ", AUT_Ajout, AUT_Delete" _
            //'        & " FROM AUT_Autorisation" _
            //'        & " WHERE AUT_Nom='" & gFr_DoublerQuote(sUtilisateur) & "'"
            //    Set rstmp = fc_OpenRecordSet(sSQL)
            //   With rstmp
            //        If Not (.EOF And .BOF) Then
            //             ReDim tpDroit(.RecordCount)
            //             .MoveFirst
            //             i = 0
            //             Do While .EOF = False
            //                tpDroit(i).sFormulaire = nz(!AUT_Formulaire, "")
            //                tpDroit(i).sObjet = nz(!AUT_Objet, "")
            //                tpDroit(i).sName = nz(!AUT_Nom, "")
            //                tpDroit(i).lEcriture = nz(!AUT_Ecriture, 0)
            //                tpDroit(i).lLecture = nz(!AUT_Lecture, 0)
            //                tpDroit(i).lAjout = nz(!AUT_Ajout, 0)
            //                tpDroit(i).lDelete = nz(!AUT_delete, 0)
            //                i = i + 1
            //                .MoveNext
            //             Loop
            //         End If
            //         .Close
            //    End With
            //
            //    sSQL = "SELECT CPTFOURNCHARGE, CPTFOURNTVA, CPTSTCHARGE, CPTSTTVA, VALTVAFOURN, VALTVAST" _
            //'    & " FROM CPT_SAGE"
            //     Set rstmp = fc_OpenRecordSet(sSQL)
            //    With rstmp
            //    If Not (.EOF And .BOF) Then
            //            CPTFOURNCHARGE = nz(!CPTFOURNCHARGE, "")
            //            CPTFOURNTVA = nz(!CPTFOURNTVA, "")
            //            CPTSTCHARGE = nz(!CPTSTCHARGE, "")
            //            CPTSTTVA = nz(!CPTSTTVA, "")
            //            VALTVAFOURN = nz(!VALTVAFOURN, "")
            //            VALTVAST = nz(!VALTVAST, "")
            //    End If
            //    .Close
            //End With
            //    Set rstmp = Nothing
            //    ReDim tabTempTest(0)
            //    '-creation du dossier contenant les paramétres personnels des grilles
            //    fc_CreateDossier CHEMINGRILLE
            //

            //   ' fc_Load
            //    UserDocFichePrincipal.Show
        }
    }
}