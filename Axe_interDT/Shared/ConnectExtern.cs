﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    public class ConnectExtern : IDisposable
    {
        public DataTable rst1 { get; set; }
        public SqlDataAdapter SDArst1 { get; set; }
        public SqlCommandBuilder SCBrst1 { get; set; }

        public void Dispose()
        {
            rst1?.Dispose();
            SDArst1?.Dispose();
            SCBrst1?.Dispose();
        }
        public void Close()
        {
            Dispose();
        }
        public DataTable gFr_ADO_Openrecordset(string sSql, SqlConnection AdoExt = null)
        {
            //If cDebug = "OUI" Then
            //    Debug.Print "**ADO open:" & sSql
            //End If

            if (AdoExt == null)
            {
                AdoExt = General.adocnn;
            }

            rst1 = new DataTable();
            SDArst1 = new SqlDataAdapter(sSql, AdoExt);
            SDArst1.Fill(rst1);

            return rst1;
        }
        public static object gFr_ADO_dLookup(string Expression, string Domaine, string Critere = "", SqlConnection AdoExt = null)
        {
            DataTable objRS;
            string sSQL;
            object FunctionReturn = null;
            //return null;
            try
            {
                sSQL = "select " + Expression + " from " + Domaine;
                if (Critere != "")
                    sSQL = sSQL + " where " + Critere;
                var ModAdo = new ModAdo();
                objRS = ModAdo.fc_OpenRecordSet(sSQL, null, "", AdoExt);
                if (objRS.Rows.Count == 0)
                    return null;
                else
                {
                    return General.nz(objRS.Rows[0][0], "");
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "ConnectExtern;gFr_ADO_dLookup");
                return null;
            }
        }
    }
}
