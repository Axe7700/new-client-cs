﻿using Infragistics.Win;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared.FilterOfGrids
{
    class BooleanColumnDataFilter: IEditorDataFilter
    {
        public object Convert(EditorDataFilterConvertArgs conversionArgs)
        {
            switch (conversionArgs.Direction)
            {
                case ConversionDirection.OwnerToEditor:
                    if (conversionArgs.Value is DBNull || $"{conversionArgs.Value}" == "0")
                    {
                        conversionArgs.Handled = true;
                        return false;
                    }
                    break;
            }

            return conversionArgs.Value;
        }
    }
}
