﻿using Infragistics.Win;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared.FilterOfGrids
{
    class StringColumnDataFilter:IEditorDataFilter
    {
        public object Convert(EditorDataFilterConvertArgs conversionArgs)
        {

            if (conversionArgs.Direction == ConversionDirection.EditorToDisplay)
            {
                if (conversionArgs.Value != null)
                {
                    var value = decimal.TryParse(conversionArgs.Value.ToString(), out var val);
                    if (value)
                    {
                        var decimalValue = val;
                        conversionArgs.Handled = true;
                        conversionArgs.IsValid = true;

                        // You can also use decimalValue.ToString("G29"), but if you have values like the one
                        // in the second row it will cause problems
                        return this.Normalize(decimalValue).ToString();
                    }
                }
            }

            return conversionArgs.Value;
        }

        public decimal Normalize(decimal value)
        {
            return value / 1.000000000000000000000000000000000m;
        }
    }
}
