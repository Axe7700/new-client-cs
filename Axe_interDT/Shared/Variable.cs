﻿namespace Axe_interDT.Shared
{
    static class Variable
    {

        public struct ArtDupli
        {
            public string sUnit;
            public double Qte;
            public string sDesignation;
        }


        //== useranalytique2
        public struct BCD
        {
            public int lNoBonDeCommande;
            public string sDateCommande;
            public string sNomFourn;
            public int lnumfichestandard;
            public int lNoIntervention;
            public string sCodeImmeuble;
            public string sCodeEtat;
            public double dbTotHT;
            public string sServiceAnaly;
            public double dbTotFacture { get; set; }
            public double dbSolde;
        }

        public struct tailleCtl
        {
            public string sName;
            public double dbHeight;
            public double dbLeft;
            public double dbTop;
            public double dbWith;
        }

        //== UserAnalytiqueTravaux
        public struct AnalyTRavaux
        {
            public string sNumFicheStandard;
            public string sNodevis;
            public string sImmeuble;
            public string sGerant;
            public string sMatCommercial;
            public string sNomCommercial;
            public string sMatRespExploit;
            public string sNomRespExploit;
            public string sMatInterv;
            public string sNomInterv;
            public string sMatCDS;
            public string sNomCDS;
            public double dbMoPrevu;
            public double dbAchatPrevu;
            public double dbVentePrevu;
            public double dbVente;
            public double dbMo;
            public double dbAchatReel;
            public double dbCoefPrevu;
            public double dbGainPrevu;
            public double dbCoef;
            public double dbGain;
        }

        public struct AnalyTRavauxImm
        {
            // sNumFicheStandard       As String
            // sNoDevis                As String
            public string sImmeuble;

            public string sGerant;
            public string sMatCommercial;
            public string sNomCommercial;
            public string sMatRespExploit;
            public string sNomRespExploit;
            public string sMatInterv;
            public string sNomInterv;
            public string sMatCDS;
            public string sNomCDS;
            public double dbMoPrevu;
            public double dbAchatPrevu;
            public double dbVentePrevu;
            public double dbVente;
            public double dbMo;
            public double dbAchatReel;
            public double dbCoefPrevu;
            public double dbGainPrevu;
            public double dbCoef;
            public double dbGain;
        }

        public struct AnalyCtr
        {
            public string sNumFicheStandard;
            public string sImmeuble;
            public string sGerant;
            public string sMatCommercial;
            public string sNomCommercial;
            public string sMatRespExploit;
            public string sNomRespExploit;
            public string sMatInterv;
            public string sNomInterv;
            public string sMatCDS;
            public string sNomCDS;
            public double dbVente;
            public double dbMo;
            public double dbAchat;
            public double dbCoef;
            public double dbGain;
            public double dbAchatReel;
        }


        public struct AnalyCtrImm
        {
            // sNumFicheStandard       As String
            public string sImmeuble;

            public string sGerant;
            public string sMatCommercial;
            public string sNomCommercial;
            public string sMatRespExploit;
            public string sNomRespExploit;
            public string sMatInterv;
            public string sNomInterv;
            public string sMatCDS;
            public string sNomCDS;
            public double dbVente;
            public double dbMo;
            public double dbAchat;
            public double dbCoef;
            public double dbGain;
            public double dbAchatReel;
        }


        //==== userdocportefeuillecontrat
        public struct GrpPrt
        {
            public System.DateTime dtDateArret;
            public string sMat;
            public string sNom;
            public double dbTotAvt0;
            public double dbNbAvt0;
            public double dbTotAvAutre;
            public double dbNbAvtAutre;
            public double dbTotHT;
            public double dbTotNb;
            public double dbTotHTP1;
            public double dbTotHTP2;
            public double dbTotHTP3;
            public double dbTotHTP4;
        }

        //==== userdocportefeuillecontrat
        public struct GrpPrtV2
        {
            public System.DateTime dtDateArret;
            public string sMat;
            public string sNom;
            public double dbNbP1;
            public double dbNbP2;
            public double dbNbP2avt;
            public double dbNbP3;
            public double dbNbP4;
            public double dbTotHTP2_P2avt;
            public double dbTotHTP1;
            public double dbTotHTP2;
            public double dbTotHTP2avt;
            public double dbTotHTP3;
            public double dbTotHTP4;
            //===> Mondir le 16.10.2020, requested https://groupe-dt.mantishub.io/view.php?id=1819
            public int nbrImmeuble;
            public int nbrFacturable;
            public int nbrNonFacturable;
            //===> Fin Modif Mondir
        }


        //=== constantes presentes dans la table lien
        //=== permet de gerer des alertes sur les montants des factures
        //=== fournisseurs (numérisé, Read Soft).
        public const string FFAlerteMt = "FFAlerteMt";

        public const string FFAlerteSurHt = "FFAlerteSurHt";


        //=== statut des factures fournisseurs.
        public const string cStatutNV = "NV";

        public const string cStatutAV = "AV";
        public const string cStatutV = "V";
        public const string cStatutAN = "AN";
        public const string cStatutEN = "EA";
        public const string cStatutAR = "AR";

        //=== nom des formulaires vb
        //Public sFormulaire As String
        public const string cUserDocClient = "UserDocClient";

        public const string cUserDocImmeuble = "UserDocImmeuble";
        public const string cUserDocStandard = "UserDocStandard";
        public const string cUserDocArticle = "UserDocArticle";
        public const string cUserIntervention = "UserIntervention";
        public const string cUserDocHistoDevis = "UserDocHistoDevis";
        public const string cUserDocDevis = "UserDocDevis";
        public const string cUserDocDevisV2 = "UserDocDevisV2";

        public const string cUserDocDescriptifChaufferie = "UserDocDescriptifChaufferie";
        public const string cUserDocPlanningCodeJock = "UserDocPlanningCodeJock";


        public const string cUserDocMenu = "UserDocMenu";
        public const string cUserDocParamtre = "UserDocParamtre";
        public const string cUserDocFacManuel = "UserDocFacManuel";
        public const string Cuserdocfourn = "UserDocFourn";
        public const string cUserBCmdHead = "UserBCmdHead";
        public const string cUserBCmdBody = "UserBCmdBody";
        public const string cUserBCLivraison = "UserBCLivraison";
        public const string cUserFactFourn = "UserFactFourn";
        public const string cUderBCmdBody = "UderBCmdBody";
        public const string cUserDispatch = "UserDispatch";
        public const string cUserDispatchDevis = "UserDispatchDevis";
        public const string cUserDocFicheContrat = "UserDocFicheContrat";
        public const string cUserAnalytique = "UserAnalytique";
        public const string cUserDocAnalytique2 = "UserDocAnalytique2";

        //===> Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020
        public const string cUserSyntheseAppel = "UserSyntheseAppel";
        //===> Fin Modif Mondir

        public const string cUserDocP2 = "UserDocP2";
        public const string cUserDocHistoInterv = "UserDocHistoriqueInterv";
        public const string cUserAnalytiqueAffaire = "UserDocAnalytique";
        public const string cUserDocRelTech = "UserDocRelTech";
        public const string cUserDocFactFourn = "UserDocFactFourn";
        public const string cUserDocFichePrincipal = "UserDocFichePrincipal";
        public const string cUserReceptMarchandise = "UserReceptMarchandise";
        public const string cUserInterP2 = "UserInterP2";
        public const string cInterventionFusion = "Intervention";
        public const string cUserDocMail = "UserDocMail";
        public const string cUserPreCommande = "UserPreCommande";
        public const string cUserPreCommande2 = "UserPreCommande2";
        public const string cUserDocAnalyTRavaux = "UserDocAnalyTRavaux";
        public const string cUserHistoReadSoft = "UserHistoReadSoft";
        public const string cUserDocSageScan = "UserDocSageScan";
        public const string cUserDocDevisBis = "UserDocDevisBis";
        public const string cUserDocGroupe = "UserDocGroupe";
        public const string cUserDocGestionAppel = "UserDocGestionAppel";
        //=== variable utilisée dans UserHistoReadSoft.

        public static bool bFactureValide;
        public static bool bFactureRefusee;
        public static bool bFactureEnAttente;
        public static bool bFactureRegule;
        public static string sTempVariable;


        //=== Constantes des noms de sociétés.
        public const string cDelostal = "AXECIEL_DELOSTAL";

        public const string cAmmann = "AXECIEL_AMMANN";


        //=== constantes en rapport avec les droits.
        public const string cStatutIntervention = "StatutIntervention";

        public const string cStatutDevis = "StatutDevis";

        //=== constantes article par defaut du devis pour pezant.
        public const string cArtDef = "DEVISPZ";

        public static bool bHistoBcndAffaire;

        public const string cSaisieReponsable = "SaisieReponsable";
        public const string cSaisieCommercial = "SaisieCommercial";

        public struct NoContrat
        {
            public string sNumContrat;
            public string sAvenant;
        }

        public struct Personnel
        {
            public string sMatricule;
            public string sNom;
            public string sPrenom;
            public string sMatChefSecteur;
        }

        public struct IndiceBis
        {
            public string NomBis;
            public double valeurBis;
            public System.DateTime DateBis;
        }

        public struct Indice
        {
            public string Nom;

            public double valeur;

            public System.DateTime Date_Renamed;
        }


        //=== autoliquidation.
        public const string cContratAutoLiquidation = "AUTO";

        public static int lFactAVoir { get; set; }
        public static string FactToEdit { get; set; } = "";
        public static string sImmFact;
        public static string sDevisFact;

        public static int lFactInterOuDevis;
        public static int lDefOuAcompteOuGarantie;
        public static int lFactNumFicheStandard { get; set; }
        public static double dbAccompteInterv;

    }
}
