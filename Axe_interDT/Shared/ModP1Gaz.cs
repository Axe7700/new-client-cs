﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class ModP1Gaz
    {
        public static SqlConnection adoP1Gaz;

        static DataTable rsAdoP1;
        static SqlDataAdapter SDArsAdoP1;
        static DataTable adoLibP1Gaz;


        //Const METAFIC = "L:\P1\Metafic\"
        public const string cFrNomAppAXE_P1 = "P1_SQL";

        public const string cUoGDP = "02";
        public const string cCodeNumOrdre = "00";

        public static string adoConnetP1Gaz
        {
            get
            {
                if (General._ExecutionMode == General.ExecutionMode.Test)
                    return $"SERVER={General.sServeurSQL};DATABASE=AXE_P1_CSHARP_TEST;INTEGRATED SECURITY=TRUE";
                else
                    return $"SERVER={General.sServeurSQL};DATABASE=AXE_P1;INTEGRATED SECURITY=TRUE";
            }
        }
        public static string fc_InsertAxeP1(string sCodeImmeuble)
        {
            string functionReturnValue = null;
            bool blnSuccess = false;
            int nbRecAff = 0;
            string sCodeAffaire = null;
            string sSQL = null;
            DateTime dtCreeLe = default(DateTime);

            try
            {
                fc_OpConnectP1Gaz();

                //=== recherche automatique du code affaire.
                sSQL = "SELECT     MAX(CodeAffaire) AS CodeAffaire" + " FROM Chantier";
                sCodeAffaire = fc_ADOlibelleP1Gaz(sSQL);
                sCodeAffaire = (Convert.ToInt32(sCodeAffaire) + 1).ToString("00000");
                functionReturnValue = sCodeAffaire;


                blnSuccess = false;

                dtCreeLe = DateTime.Now;

                //=== Création du chantier (Table Chantier).
                sSQL = "INSERT INTO Chantier " + "(CodeUO, CodeAffaire," + " CodeNumOrdre, Libelle," + " CreeLe, CreePar) " + " VALUES" + "('" + cUoGDP + "','" + sCodeAffaire + "'" + ",'" + cCodeNumOrdre + "','" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "','" + dtCreeLe + "','" + General.fncUserName() + "')";

                var SCM = new SqlCommand(sSQL, adoP1Gaz);
                if (adoP1Gaz.State == ConnectionState.Closed)
                    adoP1Gaz.Open();
                nbRecAff = SCM.ExecuteNonQuery();

                if (nbRecAff == 1)
                {
                    blnSuccess = true;
                }
                else
                {
                    blnSuccess = false;
                }

                //=== Création de la chaufferie (table P12000)
                sSQL = "INSERT INTO P12000 " + " (CodeUO, CodeAffaire," + " CodeNumOrdre, Mnemo, " + " NoFichiergraphe, CreeLe," + " CreePar) " + " VALUES " + " ('" + cUoGDP + "','" + sCodeAffaire + "'," + "'" + cCodeNumOrdre + "','" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'" + "," + General.nz(Convert.ToInt32(sCodeAffaire + cCodeNumOrdre), 0) + ",'" + dtCreeLe + "'" + ",'" + General.fncUserName() + "')";

                SCM = new SqlCommand(sSQL, adoP1Gaz);
                if (adoP1Gaz.State == ConnectionState.Closed)
                    adoP1Gaz.Open();
                nbRecAff = SCM.ExecuteNonQuery();

                if (nbRecAff == 1)
                {
                    blnSuccess = true;
                }
                else
                {
                    blnSuccess = false;
                }

                if (fc_AddP1Appareil(sCodeAffaire, cCodeNumOrdre) == true)
                {
                    blnSuccess = true;
                }
                else
                {
                    blnSuccess = false;
                }

                fc_CloseConnectP1Gaz();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "modP1Gaz.fc_InsertAxeP1");
                return functionReturnValue;
            }
        }
        public static void fc_OpConnectP1Gaz()
        {
            //=== ouvre la connection sur la base AXE_P1.
            if (adoP1Gaz == null)
            {
                adoP1Gaz = new SqlConnection(adoConnetP1Gaz);
                adoP1Gaz.Open();
            }
            else if (adoP1Gaz.State == ConnectionState.Closed)
            {
                adoP1Gaz.Open();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="BlnTout"></param>
        /// <param name="adoLibelle"></param>
        /// <returns></returns>
        public static string fc_ADOlibelleP1Gaz(string sSQL, bool BlnTout = false, SqlConnection adoLibelle = null)
        {
            string functionReturnValue = null;
            int i = 0;
            DataTable adoLibP1Gaz = default(DataTable);
            SqlDataAdapter SDAadoLibP1Gaz = default(SqlDataAdapter);
            Cursor.Current = Cursors.WaitCursor;

            try
            {
                fc_OpConnectP1Gaz();

                if (adoLibelle == null)
                {
                    adoLibelle = adoP1Gaz;
                }

                if (!string.IsNullOrEmpty(sSQL))
                {
                    adoLibP1Gaz = new DataTable();
                    SDAadoLibP1Gaz = new SqlDataAdapter(sSQL, adoLibelle);
                    SDAadoLibP1Gaz.Fill(adoLibP1Gaz);

                    if (adoLibP1Gaz.Rows.Count > 0)
                    {
                        if (BlnTout == false)
                        {
                            functionReturnValue = General.nz(adoLibP1Gaz.Rows[0][0], "").ToString();
                        }
                        else
                        {
                            for (i = 0; i <= adoLibP1Gaz.Columns.Count - 1; i++)
                            {
                                functionReturnValue = adoLibP1Gaz.Rows[0][i] + " " + functionReturnValue;
                            }
                        }
                    }
                    else
                    {
                        functionReturnValue = "";
                    }

                }

                Cursor.Current = Cursors.Default;

                if ((adoLibP1Gaz != null))
                {
                    adoLibP1Gaz?.Dispose();
                    SDAadoLibP1Gaz?.Dispose();

                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_ADOlibelle sSQL=" + sSQL, false);
                return functionReturnValue;
            }
        }
        private static bool fc_AddP1Appareil(string sCodeAffaire, string sCodeNumOrdre, string sNumCompteur = "")
        {
            bool functionReturnValue = false;
            string sReq = null;
            string sNumAppareil = null;

            functionReturnValue = false;

            try
            {
                Dossier.fc_CopieFichier(gFr_FichierMetafic() + "DEFAUT", gFr_FichierMetafic() + "" + Convert.ToInt32(sCodeAffaire + sCodeNumOrdre));

                sReq = "INSERT INTO P1_Appareil (";
                sReq = sReq + "NoFichierGraphe,";
                sReq = sReq + "NOrdre,";
                sReq = sReq + "Libelle,";
                sReq = sReq + "Energie,";
                sReq = sReq + "Mixite,";
                sReq = sReq + "CodeUnite,";
                sReq = sReq + "UniteDELecture,";
                sReq = sReq + "AFacturer,";
                sReq = sReq + "Amont,";
                sReq = sReq + "Aval,";
                sReq = sReq + "Sens,";
                sReq = sReq + "CreeLe,";
                sReq = sReq + "CreePAr,";
                sReq = sReq + "Pilote,";
                sReq = sReq + "Actif,";
                sReq = sReq + "NumCompteur";

                sReq = sReq + ") VALUES (";

                sReq = sReq + "" + Convert.ToInt32(sCodeAffaire + sCodeNumOrdre) + ",";
                //NoFichierGraphe"
                sReq = sReq + "1,";
                //NOrdre"
                sReq = sReq + "'GAZ',";
                //Libelle"
                sReq = sReq + "'GAZ',";
                //Energie"
                sReq = sReq + "0,";
                //Mixite"
                sReq = sReq + "1,";
                //CodeUnite"
                sReq = sReq + "'M3',";
                //UniteDELecture"
                sReq = sReq + "1,";
                //AFacturer"
                sReq = sReq + "1,";
                //Amont"
                sReq = sReq + "0,";
                //Aval"
                sReq = sReq + "'+',";
                //Sens"
                sReq = sReq + "'" + DateTime.Now + "',";
                //CreeLe"
                sReq = sReq + "'" + General.fncUserName() + "',";
                //CreePAr"
                sReq = sReq + "1,";
                //Pilote"
                sReq = sReq + "1,";
                //Actif"
                sReq = sReq + "'" + sNumCompteur + "'";
                //NumCompteur"

                sReq = sReq + ")";

                var SCM = new SqlCommand(sReq, adoP1Gaz);
                if (adoP1Gaz.State == ConnectionState.Closed)
                    adoP1Gaz.Open();
                SCM.ExecuteNonQuery();

                General.sSQL = "SELECT P1_Appareil.NumAppareil FROM P1_Appareil" + " WHERE NoFichierGraphe = " + Convert.ToInt32(sCodeAffaire + sCodeNumOrdre) + "";

                sNumAppareil = fc_ADOlibelleP1Gaz(General.sSQL);

                fc_ReplaceTextInFile(gFr_FichierMetafic() + "" + Convert.ToInt32(sCodeAffaire + sCodeNumOrdre), "Data =  48", "Data =  " + sNumAppareil);

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "modP1Gaz;fc_AddP1Appareil;");
                return functionReturnValue;
            }
        }
        public static string gFr_FichierMetafic()
        {
            string functionReturnValue = null;

            object sFicRpt = null;

            try
            {
                sFicRpt = General.getFrmReg(cFrNomAppAXE_P1, "CheminMetafic", "", false);
                if (string.IsNullOrEmpty(sFicRpt.ToString()))
                {
                    sFicRpt = General.METAFIC;
                    General.saveInReg(cFrNomAppAXE_P1, "CheminMetafic", General.METAFIC, false);
                }

                functionReturnValue = sFicRpt.ToString();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "modP1Gaz;gFr_FichierMetafic");

                return functionReturnValue;
            }
        }
        public static void fc_CloseConnectP1Gaz()
        {

            //=== ferme la connection sur la base AXE_P1.
            if (adoP1Gaz != null)
            {
                if (adoP1Gaz.State == ConnectionState.Open)
                {
                    adoP1Gaz.Close();
                    adoP1Gaz?.Dispose();
                }
            }

        }
        public static void fc_ReplaceTextInFile(string sfile, string sSTringSearch, string sStringReplace)
        {

            //écris le contenu le contenu de stexte dans un fichier.
            StreamReader tTs = null;
            string[] tabLines = null;
            string sLine = null;
            int i = 0;
            bool blnFind = false;

            blnFind = false;
            i = 0;
            tabLines = new string[1];
            //
            //    tTs.Close
            //    Set tTs = Nothing
            tTs = new StreamReader(sfile);

            while ((sLine = tTs.ReadLine()) != null)
            {

                if (tabLines.Length < i)
                {
                    Array.Resize(ref tabLines, i + 1);
                }

                sLine = tTs.ReadLine();
                if (sLine.Contains(sSTringSearch))
                {
                    tabLines[i] = sLine.Replace(sSTringSearch, sStringReplace);
                    blnFind = true;
                }
                else
                {
                    tabLines[i] = sLine;
                }

                i = i + 1;
            }

            tTs.Close();
            tTs?.Dispose();



            //Occurence de la chaîne cherchée trouvée
            if (blnFind == true)
            {
                File.WriteAllLines(sfile, tabLines);
            }

            tTs = null;

        }

        public static DataTable fc_OpenRecordSetP1Gaz(string sSQL, int lLecture = 0)
        {
            DataTable functionReturnValue = default(DataTable);

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                fc_OpConnectP1Gaz();
                if (!string.IsNullOrEmpty(sSQL))
                {
                    // If Not rsAdoP1 Is Nothing Then
                    //     If rsAdoP1.State = adStateOpen Then
                    //               rsAdoP1.Close
                    //           End If
                    //       Else
                    rsAdoP1 = new DataTable();
                    //   End If
                    if (lLecture == 1)
                    {
                        SDArsAdoP1 = new SqlDataAdapter(sSQL, adoP1Gaz);
                        SDArsAdoP1.Fill(rsAdoP1);
                    }
                    else
                    {
                        SDArsAdoP1 = new SqlDataAdapter(sSQL, adoP1Gaz);
                        SDArsAdoP1.Fill(rsAdoP1);
                    }
                    if (rsAdoP1.Rows.Count > 0)
                    {
                        functionReturnValue = rsAdoP1;
                    }
                    else
                    {
                        functionReturnValue = rsAdoP1;
                    }

                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return functionReturnValue;
            }
        }
    }
}
