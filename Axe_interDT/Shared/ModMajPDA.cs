﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class ModMajPDA
    {
        public static string sEtatRapportInterventionBIS;


        public static void fc_SaveParamForm(Control C, Control sForm)
        {
            System.Windows.Forms.Control ctrl = null;

            short i = 0;

            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    fc_SaveParamForm(CC, sForm);

            if (C is System.Windows.Forms.CheckBox)
            {
                General.saveInReg(sForm.Name, C.Name, ((CheckBox)C).Checked.ToString());
            }

            if (C is System.Windows.Forms.RadioButton)
            {

                General.saveInReg(sForm.Name, C.Name, ((RadioButton)C).Checked.ToString());
            }


            if (C is System.Windows.Forms.TextBox || C is iTalk.iTalk_TextBox_Small2)
            {
                if (C is iTalk.iTalk_TextBox_Small2)
                    if (!(C as iTalk.iTalk_TextBox_Small2).UseSystemPasswordChar)
                        // Debug.Print ctrl.Name
                        General.saveInReg(sForm.Name, C.Name, C.Text);
                if (C is System.Windows.Forms.TextBox)
                    if (!(C as TextBox).UseSystemPasswordChar)
                        General.saveInReg(sForm.Name, C.Name, C.Text);
            }


            if (C is UltraCombo)
            {
                General.saveInReg(sForm.Name, "Text_" + C.Name, C.Text);

                //            If ctrl.Value <> "" And ctrl.Text <> "" Then
                if (!string.IsNullOrEmpty(C.Text))
                {
                    General.saveInReg(sForm.Name, "Val_" + C.Name, C.Text);

                }
                else
                {
                    General.saveInReg(sForm.Name, "Val_" + C.Name, "");
                }

                var combo = C as UltraCombo;

                if (combo.Rows.Count > 0)
                {
                    if (combo.ActiveRow != null)
                    {
                        for (i = 0; i <= combo.ActiveRow.Cells.Count - 1; i++)
                        {
                            General.saveInReg(sForm.Name, "Text_" + combo.Name + "_Col_" + combo.ActiveRow.Cells[i].Column.Key, combo.ActiveRow.Cells[i].Text);

                            //                    If ctrl.Columns(i).Value <> "" Then
                            //                        SaveSetting cFrNomApp, sForm.Name, "Val_" & ctrl.Name & "_Col_" & ctrl.Columns(i).Name, ctrl.Columns(i).Value
                            //                    Else
                            //                        SaveSetting cFrNomApp, sForm.Name, "Val_" & ctrl.Name & "_Col_" & ctrl.Columns(i).Name, ""
                            //                    End If
                        }
                    }

                }
            }
            if (ctrl is System.Windows.Forms.ComboBox)
            {
                General.saveInReg(sForm.Name, "Text_" + C.Name, C.Text);
            }
        }

    }
}
