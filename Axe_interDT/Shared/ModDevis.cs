﻿using Axe_interDT.View.Theme;
using Axe_interDT.Views.Devis;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using iTalk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public class ModDevis
    {
        private const string ServerUser = "INTRANET_CONCURRENCY";
        private const string Serverpassword = "axeciel2020";

        public struct ControlState
        {
            public Control Control;
            public string Property;
            public object Value;
            public bool IsColumn;
            public string Column;
        }
        List<ControlState> ControlStates = new List<ControlState>();
        public static bool ActivateTransaction { get; set; } = false;

        public string TransactionName { get; set; }

        private SqlConnection Conn { get; set; }

        private string NumeroDevis { get; set; }
        private UserDocDevis UserDocDevis { get; set; }

        public void StartTransaction(string NumeroDevis, UserDocDevis UserDocDevis)
        {
            Program.SaveException(null, $"ModDevis.StartTransaction() - NumeroDevis = {NumeroDevis}");
            if (!General.adocnn.Database.ToUpper().Contains("AXECIEL_DELOSTAL"))
            {
                //Enabled Only for DELOSTAL
                return;
            }
            try
            {
                this.NumeroDevis = NumeroDevis;
                this.UserDocDevis = UserDocDevis;

                TransactionName = $"D_{NumeroDevis}_{Theme.guid}";
                TransactionName = CleanString(TransactionName);
                TransactionName = General.Left(TransactionName, 32);

                Conn = new SqlConnection($"SERVER={General.adocnn.DataSource};DATABASE={General.adocnn.Database};User Id={ServerUser};Password={Serverpassword};");
                var x = Conn.ConnectionTimeout;

                OpenConn();

                var CMD = new SqlCommand($@"BEGIN TRANSACTION {TransactionName}", Conn);
                CMD.ExecuteNonQuery();

                CheckDevisStatus();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public void CommitTransaction()
        {
            Program.SaveException(null, $"ModDevis.CommitTransaction()");

            try
            {
                if (string.IsNullOrEmpty(TransactionName))
                    return;

                var CMD = new SqlCommand($"COMMIT TRANSACTION  {TransactionName}", Conn);
                CMD.ExecuteNonQuery();

                TransactionName = "";

                EnableControls();

                CloseConn();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public void OpenConn()
        {
            Program.SaveException(null, $"ModDevis.OpenConn()");

            try
            {
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public void CloseConn()
        {
            Program.SaveException(null, $"ModDevis.CloseConn()");

            try
            {
                if (Conn.State != ConnectionState.Closed)
                {
                    Conn.Close();
                    Conn.Dispose();
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public string CleanString(string str)
        {
            Program.SaveException(null, $"ModDevis.CleanString() - str = {str}");

            return str.Replace(" ", "")
                .Replace(".", "")
                .Replace("-", "")
                .Replace("/", "");
        }

        public void CheckDevisStatus()
        {
            Program.SaveException(null, $"ModDevis.CheckDevisStatus()");

            try
            {
                if (!ActivateTransaction)
                    return;

                var _devis = CleanString(NumeroDevis);
                //var user = System.Security.Principal.WindowsIdentity.GetCurrent().Name;
                var req = @"SELECT trans.session_id AS [SESSION ID], ESes.host_name AS [HOST NAME],login_name AS [Login_NAME], trans.transaction_id AS [TRANSACTION ID],
                        tas.name AS[TRANSACTION NAME], tas.transaction_begin_time AS[BEGIN_TIME], tds.database_id AS[DATABASE ID], DBs.name AS[DATABASE NAME]
                        FROM sys.dm_tran_active_transactions tas
                        JOIN sys.dm_tran_session_transactions trans ON(trans.transaction_id = tas.transaction_id)
                        LEFT OUTER JOIN sys.dm_tran_database_transactions tds ON(tas.transaction_id = tds.transaction_id)
                        LEFT OUTER JOIN sys.databases AS DBs ON tds.database_id = DBs.database_id
                        LEFT OUTER JOIN sys.dm_exec_sessions AS ESes ON trans.session_id = ESes.session_id
                        WHERE ESes.session_id IS NOT NULL";

                //Get the transaction i have created
                var req_where = $"{req} AND tas.name = '{TransactionName}'";
                var ownTransaction = new ModAdo();
                var ownTransactionDb = ownTransaction.fc_OpenRecordSet(req_where);
                var dateOwn = ownTransactionDb.Rows[0]["BEGIN_TIME"].ToDate();

                //Get all other transaction, Where are started before my transaction, the first in the list is the one has the update rights
                req_where = $"{req} AND tas.name LIKE 'D_{_devis}_%' AND tas.name != '{TransactionName}' AND tas.transaction_begin_time < '{dateOwn}'" +
                            $" ORDER BY tas.transaction_begin_time ASC";
                var othersTransaction = new ModAdo();
                var othersTransactionDb = ownTransaction.fc_OpenRecordSet(req_where);

                if (ownTransactionDb.Rows.Count > 0)
                {
                    if (othersTransactionDb.Rows.Count > 0)
                    {
                        //Dont have Update Rights
                        //Must desable all the controls
                        DesableControls();
                        var user = othersTransactionDb.Rows[0]["Login_NAME"].ToString().Split(new char[] { '\\' })[1];
                        //UserDocDevis.LabelStatus.Text = $"Impossible de modifier le devis {NumeroDevis}, car il est en cours de modification par l'utilisateur : {user}";
                        //UserDocDevis.LabelStatus.Visible = true;
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public void DesableControls()
        {
            Program.SaveException(null, $"ModDevis.DesableControls()");

            try
            {
                if (!ActivateTransaction)
                    return;

                UserDocDevis.Controls.AsEnumerableReq()
                    .Where(c => c is Button || c is UltraCombo || c is CheckBox || c is RadioButton || c is DateTimePicker
                                || c is UltraGrid || c is iTalk_RichTextBox || c is iTalk_TextBox_Small2)
                    .ToList()
                    .ForEach(c =>
                    {
                        if ((c is Button || c is UltraCombo || c is CheckBox || c is RadioButton || c is DateTimePicker) && c.Enabled)
                        {
                            ControlStates.Add(new ControlState
                            {
                                Control = c,
                                Property = "Enabled",
                                Value = c.Enabled
                            });
                            c.Enabled = false;
                        }
                        else if (c is iTalk_TextBox_Small2)
                        {
                            var tmpC = c as iTalk_TextBox_Small2;
                            if (!tmpC.ReadOnly)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "ReadOnly",
                                    Value = false
                                });
                                tmpC.ReadOnly = true;
                            }

                            if (!tmpC.AccReadOnly)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "AccReadOnly",
                                    Value = false
                                });
                                tmpC.AccReadOnly = true;
                            }
                        }
                        else if (c is iTalk_RichTextBox)
                        {
                            var tmpC = c as iTalk_RichTextBox;
                            if (!tmpC.ReadOnly)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "ReadOnly",
                                    Value = false
                                });
                                tmpC.ReadOnly = true;
                            }
                        }
                        else if (c is UltraGrid)
                        {
                            var tmpC = c as UltraGrid;

                            if (tmpC.DisplayLayout.Override.AllowAddNew != AllowAddNew.No)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "AllowAddNew",
                                    Value = (int)tmpC.DisplayLayout.Override.AllowAddNew
                                });
                                tmpC.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                            }

                            if (tmpC.DisplayLayout.Override.AllowUpdate != DefaultableBoolean.False)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "AllowUpdate",
                                    Value = (int)tmpC.DisplayLayout.Override.AllowUpdate
                                });
                                tmpC.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.False;
                            }

                            if (tmpC.DisplayLayout.Override.AllowDelete != DefaultableBoolean.False)
                            {
                                ControlStates.Add(new ControlState
                                {
                                    Control = c,
                                    Property = "AllowDelete",
                                    Value = (int)tmpC.DisplayLayout.Override.AllowDelete
                                });
                                tmpC.DisplayLayout.Override.AllowDelete = DefaultableBoolean.False;
                            }

                            //Sometime the grid is empty, and not yet loaded, so i desable it
                            if (tmpC.DisplayLayout.Bands[0].Columns.Count == 0)
                            {
                                if (c.Enabled)
                                {
                                    ControlStates.Add(new ControlState
                                    {
                                        Control = c,
                                        Property = "Enabled",
                                        Value = c.Enabled
                                    });
                                    c.Enabled = false;
                                }
                            }
                            else
                            {
                                foreach (var col in tmpC.DisplayLayout.Bands[0].Columns)
                                {
                                    if (col.CellActivation != Activation.NoEdit)
                                    {
                                        ControlStates.Add(new ControlState
                                        {
                                            Control = c,
                                            Property = "CellActivation",
                                            Value = (int)col.CellActivation,
                                            IsColumn = true,
                                            Column = col.Key
                                        });
                                        col.CellActivation = Activation.NoEdit;
                                    }
                                }
                            }
                        }
                    });
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        public void EnableControls()
        {
            Program.SaveException(null, $"ModDevis.EnableControls()");

            try
            {
                if (!ActivateTransaction)
                    return;

                foreach (var controlState in ControlStates)
                {
                    var control = controlState.Control;
                    if (controlState.IsColumn)
                    {
                        var grid = control as UltraGrid;
                        grid.DisplayLayout.Bands[0].Columns[controlState.Column].CellActivation =
                            (Activation)controlState.Value;
                    }
                    else if (controlState.Property == "Enabled")
                    {
                        control.Enabled = true;
                    }
                    else if (controlState.Property == "AllowDelete")
                    {
                        var grid = control as UltraGrid;
                        grid.DisplayLayout.Override.AllowDelete = (DefaultableBoolean)controlState.Value;
                    }
                    else if (controlState.Property == "AllowUpdate")
                    {
                        var grid = control as UltraGrid;
                        grid.DisplayLayout.Override.AllowUpdate = (DefaultableBoolean)controlState.Value;
                    }
                    else if (controlState.Property == "AllowAddNew")
                    {
                        var grid = control as UltraGrid;
                        grid.DisplayLayout.Override.AllowAddNew = (AllowAddNew)controlState.Value;
                    }
                    else if (control is iTalk_RichTextBox)
                    {
                        var rich = control as iTalk_RichTextBox;
                        rich.ReadOnly = true;
                    }
                    else if (control is iTalk_TextBox_Small2)
                    {
                        var text = control as iTalk_TextBox_Small2;
                        text.ReadOnly = false;
                        text.AccReadOnly = false;
                    }
                }
                ControlStates.Clear();

                //UserDocDevis.LabelStatus.Visible = false;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }
    }
}
