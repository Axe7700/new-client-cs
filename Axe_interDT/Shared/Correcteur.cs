﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Axe_interDT.Shared
{
    static class Correcteur
    {
        ///these windwo apis added by mohammed to close error dialogBox on word to solve "correcteur Exception"
        ///========> BEGIN
        [DllImport("user32.dll", SetLastError = true)]
        static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        // Find window by Caption only. Note you must pass IntPtr.Zero as the first parameter.

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        static extern IntPtr FindWindowByCaption(IntPtr ZeroOnly, string lpWindowName);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);


        [return: MarshalAs(UnmanagedType.Bool)]
        [DllImport("user32.dll", SetLastError = true)]
        static extern bool PostMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        static extern IntPtr SendMessage(IntPtr hWnd, UInt32 Msg, IntPtr wParam, IntPtr lParam);

        static uint WM_CLOSE = 0x10;
        
        //static bool CloseWindow(IntPtr hWnd)
        //{
        //    // Win 32 API being called through PInvoke
        //    SendMessage(hWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
        //    return true;
        //}

        static bool CloseWindow2(IntPtr hWnd)
        {
            // Win 32 API being called through PInvoke
            PostMessage(hWnd, WM_CLOSE, IntPtr.Zero, IntPtr.Zero);
            return true;

        }

        ///=======> END
        

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sChaine"></param>
        /// <param name="ShowWordInTaskBar"></param>
        /// <param name="oWord"></param>
        /// <returns></returns>
        public static string WSpellCheck(string sChaine, bool ShowWordInTaskBar, ref Word.Application oWord)
        {
            string functionReturnValue = null;
            int hResult = 0, nTime = 0;
            try
            {
                Word.Document oTmpDoc = null;
                int lOrigTop = 0;

                if (string.IsNullOrEmpty(sChaine))
                    return functionReturnValue;

                // Create a Word document object... Creer un document Word
                oTmpDoc = oWord.Documents.Add();
                //if you choose to not show word in the taskbar, the spelling may stay hidden behind
                //your application and it is impossible to switch back to it.
                //Si vous choisisez de ne pas rendre visible word dans la barre des taches, le correcteur
                //d'ortographe peux se retrouver deriere votre application et il est impossible d'y acceder
                //car votre app est en attente et ne peux etre reduite.

                if (ShowWordInTaskBar == false)
                {
                    oWord.Visible = false;
                }
                else
                {
                    oWord.Visible = true;
                    // oWord.Visible = False
                    // Position Word off screen to avoid having document visible...
                    lOrigTop = oWord.Top;
                    oWord.WindowState = 0;

                    //j'ai commenté cette ligne pour apparaitre le fichier word en premier plan directement
                    //0001259: Fiche devis - correcteur fait planter INTRANET
                    // oWord.Top = -3000; 
                }
                //copier le contenu de la chaîne
                Clipboard.Clear();
                Clipboard.SetText(sChaine);
                //on colle le text dans le doc word et on verifie l'orthographe
                var _with1 = oTmpDoc;
                goto essayer;
                essayer:
                try
                {
                    ///==========> modified by mohammed to solve this exception caused by thread Exception Description ==> System.Runtime.InteropServices.COMException|Message : L’appel a été rejeté par l’appelé. (Exception de HRESULT : 0x80010001 (RPC_E_CALL_REJECTED))
                    ///==========> when the exception occurs (the thread cause this exception) i try to close the error dialogBox on word and  retry to paste the text to the document for one time. if the exception still occurs we log the error on Error File and display the error message .
                    _with1.Content.Paste();
                    _with1.Activate();
                    _with1.CheckSpelling(true);
                    //Lance le correcteur d'orthographe : ici l'appli est en attente

                    //on copie la chaîne corrigée pour la renvoyée
                    _with1.Content.Copy();
                    functionReturnValue = Clipboard.GetText();

                    // Fermeture du document après lui avoir dit qu'il est déjà enregistré
                    _with1.Saved = true;
                    _with1.Close();
                    oTmpDoc = null;
                }
                catch (Exception ex)
                {
                    nTime++;
                    hResult = ex.HResult;
                    if(nTime>1)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur est apparue lors de la correction de l'orthographe : " + "\n" + ex.Message);
                        Program.SaveException(ex);
                    }
                }
                if (hResult == -2147418111 && nTime == 1)
                {
                    //close the error window on word and retry again
                    hResult = 0;
                    CloseErrorWindowOfWord();
                    Thread.Sleep(350);
                    goto essayer;
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur est apparue lors de la correction de l'orthographe : " + "\n" + e.Message);
                Program.SaveException(e);
                return functionReturnValue;
            }
        }
        private static void CloseErrorWindowOfWord()
        {
            string caption = "Compléments Office";
            string className = "NUIDialog";
            IntPtr hWnd = (IntPtr)(0);

            // Win 32 API being called through PInvoke
            hWnd = FindWindow(className, caption);

            if ((int)hWnd != 0)
            {
                CloseWindow2(hWnd);
            }
        }

        public static void closeWindowOfExcel(string titre,string classe= "TabWindowClass")
        {
            // - Adobe Acrobat Reader DC
            string caption = titre;
            string className = classe;
            IntPtr hWnd = (IntPtr)(0);

            // Win 32 API being called through PInvoke
            hWnd = FindWindow(className, caption);

            if ((int)hWnd != 0)
            {
                CloseWindow2(hWnd);
            }
        }
    }
}
