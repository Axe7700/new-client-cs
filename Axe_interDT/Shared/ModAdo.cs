﻿using Axe_interDT.View.Theme;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public class ModAdo : IDisposable
    {
        //Public adocnn       As ADODB.Connection
        public DataTable rsAdo { get; set; }
        public SqlDataAdapter SDArsAdo { get; set; }
        public SqlCommandBuilder SCBrsAdo { get; set; }

        public SqlTransaction Tran { get; set; }

        //===> Mondir le 16.04.2021 https://groupe-dt.mantishub.io/view.php?id=2403
        public static bool LogActive { get; set; } = true;


        public static void LogModAdo(DataTable rsAdo)
        {
            var log = "";
            try
            {
                var DT = rsAdo.GetChanges(DataRowState.Modified);
                if (DT != null)
                {
                    log += $"Modified Rows = {DT.Rows.Count}{Environment.NewLine}";
                    foreach (DataRow row in DT.Rows)
                    {
                        foreach (DataColumn col in DT.Columns)
                        {
                            log += $"{col.ColumnName} = {row[col.ColumnName]} - ";
                        }

                        log += Environment.NewLine;
                    }
                }

                DT = rsAdo.GetChanges(DataRowState.Deleted);
                if (DT != null)
                {
                    log += $"Deleted Rows = {DT.Rows.Count}{Environment.NewLine}";
                    foreach (DataRow row in DT.Rows)
                    {
                        foreach (DataColumn col in DT.Columns)
                        {
                            log += $"{col.ColumnName} = {row[col.ColumnName]} - ";
                        }

                        log += Environment.NewLine;
                    }
                }

                DT = rsAdo.GetChanges(DataRowState.Added);
                if (DT != null)
                {
                    log += $"Added Rows = {DT.Rows.Count}{Environment.NewLine}";
                    foreach (DataRow row in DT.Rows)
                    {
                        foreach (DataColumn col in DT.Columns)
                        {
                            log += $"{col.ColumnName} = {row[col.ColumnName]} - ";
                        }

                        log += Environment.NewLine;
                    }
                }
            }
            catch
            {

            }
            Program.SaveException(null, $"ModAdo.Update() - Updating {Environment.NewLine}");
            Program.SaveException(null, log);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public int Update(string AI_Field = "", DataRow newRow = null)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            if (LogActive)
            {
                LogModAdo(rsAdo);
            }
            //===> Fin Modif Mondir

            try
            {
                SCBrsAdo = new SqlCommandBuilder(SDArsAdo);

                if (!AI_Field.IsNullOrEmpty() && newRow != null)
                {
                    SqlCommandBuilder cb = SCBrsAdo;
                    SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                    SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                    SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                    insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                    insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                    SDArsAdo.InsertCommand = insertCmd;

                    SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                    {

                        if (e.StatementType == StatementType.Insert)
                        {
                            newRow[AI_Field] = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                        }
                    });
                }

                if (Tran != null)
                {
                    if (SDArsAdo.InsertCommand != null)
                        SDArsAdo.InsertCommand.Transaction = Tran;
                    if (SDArsAdo.DeleteCommand != null)
                        SDArsAdo.DeleteCommand.Transaction = Tran;
                    if (SDArsAdo.UpdateCommand != null)
                        SDArsAdo.UpdateCommand.Transaction = Tran;
                }

                var result = SDArsAdo.Update(rsAdo);

                if (Tran != null)
                    Tran.Commit();

                return result;
            }
            catch (Exception ex)
            {
                if (General._ExecutionMode == General.ExecutionMode.Dev)
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show($"Error While Updating Database !{Environment.NewLine}{Environment.NewLine}{ex.Message}");
                Program.SaveException(ex, SDArsAdo.SelectCommand.CommandText);
                if (SDArsAdo.InsertCommand != null)
                    Program.SaveException(null, SDArsAdo.InsertCommand.CommandText);
                if (SDArsAdo.UpdateCommand != null)
                    Program.SaveException(null, SDArsAdo.UpdateCommand.CommandText);

                if (Tran != null)
                    Tran.Rollback();

                //string all = "";
                //string query = "Values(";
                //foreach (SqlParameter row in SDArsAdo.InsertCommand.Parameters)
                //{
                //    query += "'" + row.Value + "',";
                //    all+=row.ParameterName+"==>'"+row.Value+"'";
                //}
                return -1;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="AdoExt"></param>
        /// <returns></returns>
        public DataTable fc_OpenRecordSet(string sSQL = "", UltraGrid Grid = null, string AI_Field = "", SqlConnection Conn = null, bool ChangeCursor = true, DataRow newRow = null)
        {
            Program.SaveException(null, $"ModAdo.fc_OpenRecordSet() - Enter In The Function - sSQL = {sSQL}");
            try
            {
                if (ChangeCursor)
                    Theme.MainForm.Cursor = Cursors.WaitCursor;

                if (!string.IsNullOrWhiteSpace(sSQL))
                {
                    if (Conn == null)
                        Conn = General.adocnn;

                    if (Conn.State != ConnectionState.Open)
                        Conn.Open();

                    rsAdo = new DataTable();
                    SDArsAdo = new SqlDataAdapter(sSQL, Conn == null ? General.adocnn : Conn);

                    if (Grid != null || newRow != null)
                    {
                        SqlCommandBuilder cb = new SqlCommandBuilder(SDArsAdo);
                        SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                        SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                        SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                        insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                        insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                        SDArsAdo.InsertCommand = insertCmd;

                        SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                        {

                            if (e.StatementType == StatementType.Insert)
                            {
                                if (Grid != null)
                                {
                                    if (Grid.ActiveRow != null && Grid.ActiveRow.Cells[AI_Field].Value.ToString() == "")
                                    {
                                        if (e.Command.Parameters["@ID"].Value.ToString() != "")
                                            Grid.ActiveRow.Cells[AI_Field].Value = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                                        else
                                            Grid.ActiveRow.CancelUpdate();
                                    }
                                }
                                else if (newRow != null)
                                {
                                    newRow[AI_Field] = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                                }
                            }
                        });
                    }

                    SDArsAdo.SelectCommand.CommandTimeout = 3 * 60;
                    SDArsAdo.Fill(rsAdo);

                    if (ChangeCursor)
                        Theme.MainForm.Cursor = Cursors.Default;
                    return rsAdo;
                }
                else
                {
                    if (SDArsAdo == null)
                        throw new Exception("Adapter is NULL");

                    rsAdo = new DataTable();
                    SDArsAdo.Fill(rsAdo);
                    if (ChangeCursor)
                        Theme.MainForm.Cursor = Cursors.Default;
                    return rsAdo;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_Openrecordset");
                Program.SaveException(e, sSQL);
            }
            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="BlnTout"></param>
        /// <param name="AdoExt"></param>
        /// <returns></returns>
        public string fc_ADOlibelle(string sSQL, bool BlnTout = false, SqlConnection adoLibelle = null, bool ChangeCursor = true)
        {
            if (Theme.MainForm != null)
                if (ChangeCursor)
                    Theme.MainForm.Cursor = Cursors.WaitCursor;
            bool bNewConnect = false;

            if (adoLibelle == null)
                //adoLibelle = General.adocnn;
                bNewConnect = true;

            var fc_ADOlibelle = "";

            try
            {
                if (!string.IsNullOrWhiteSpace(sSQL))
                {
                    rsAdo = new DataTable();
                    if (bNewConnect == true)
                    {
                        SDArsAdo = new SqlDataAdapter(sSQL, General.adocnn);
                    }
                    else
                    {
                        SDArsAdo = new SqlDataAdapter(sSQL, adoLibelle);
                    }

                    SDArsAdo.Fill(rsAdo);

                    if (rsAdo.Rows.Count > 0)
                    {
                        if (!BlnTout)
                            fc_ADOlibelle = General.nz(rsAdo.Rows[0][0], "").ToString();
                        else
                            for (var i = rsAdo.Columns.Count - 1; i >= 0; i--)
                                fc_ADOlibelle += rsAdo.Rows[0][i] + " ";
                    }
                    else
                        fc_ADOlibelle = "";
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_Adolibelle");
                Program.SaveException(null, sSQL);
            }

            if (Theme.MainForm != null)
                if (ChangeCursor)
                    Theme.MainForm.Cursor = Cursors.Default;

            return fc_ADOlibelle;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="rsCloseado"></param>
        public static void fc_CloseRecordset(DataTable rsCloseado)
        {
            if (rsCloseado != null)
            {
                rsCloseado.Dispose();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        public void Dispose()
        {
            if (Tran != null)
            {
                Update();
                //Tran.Commit();
            }
            rsAdo?.Dispose();
            SDArsAdo?.Dispose();
            SCBrsAdo?.Dispose();
        }
        /// <summary>
        /// Tested
        /// </summary>
        public void Close()
        {
            Dispose();
        }
    }
}
