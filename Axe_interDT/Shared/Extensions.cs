﻿using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class Extensions
    {
        public static IEnumerable<UltraTreeNode> AsEnumerableReq(this TreeNodesCollection collection)
        {
            var controls = collection.Cast<UltraTreeNode>();

            return controls.SelectMany(ctrl => AsEnumerableReq(ctrl.Nodes))
                .Concat(controls);
        }

        public static bool IsDate(this string text)
        {
            return General.IsDate(text);
        }

        public static DateTime ToDate(this string text)
        {
            return Convert.ToDateTime(text);
        }

        public static int ToInt(this string text)
        {
            return Convert.ToInt32(text);
        }

        public static int ToInt(this object text)
        {
            return Convert.ToInt32(text);
        }

        public static double ToDouble(this string text)
        {
            //===> Mondir le 13.03.2021, if the text is empty return 0
            if (text.IsNullOrEmpty())
                return 0;
            return Convert.ToDouble(text);
        }

        public static double ToDouble(this object text)
        {
            return Convert.ToDouble(text);
        }

        public static bool IsNullOrEmpty(this string text)
        {
            return string.IsNullOrEmpty(text);
        }

        public static DateTime ToDate(this object obj)
        {
            return Convert.ToDateTime(obj);
        }

        public static bool IsDate(this object obj)
        {
            return General.IsDate(obj);
        }

        public static DateTime? GetCloseDateTime(this string text)
        {
            if (!Regex.IsMatch(text, @"\d\d/\d\d/\d\d\d\d"))
                return null;
            var splited = text.Split('/');
            var day = splited[0].ToInt();
            var month = splited[1].ToInt();
            var year = splited[2].ToInt();
            if (day == 0)
                day = 31;
            if (month == 0)
                month = 12;
            if (year == 0)
                year = 2020;
            var y = 0;
            while (true)
            {
                for (int i = 0; i <= 12; i++)
                {
                    for (int j = 0; j <= 31; j++)
                    {
                        var newDate = $"{day - j}/{month - i}/{year - y}";
                        if (newDate.IsDate())
                            return newDate.ToDate();
                    }
                }
                y++;
            }

        }

        public static void AutoValidateDate(this UltraDateTimeEditor picker)
        {
            picker.BeforeExitEditMode += (se, ev) =>
            {
                var date = picker.Text;
                if (date.IsDate())
                    return;
                var newDate = date.GetCloseDateTime();
                if (newDate != null)
                {
                    picker.Value = newDate.Value;
                }
            };
        }

        public static bool FileExists(this string path, bool create = false)
        {
            if (!File.Exists(path) && create)
                File.Create(path).Dispose();
            return File.Exists(path);
        }

        public static bool DirectoryExists(this string path)
        {
            return Directory.Exists(path);
        }

        public static IEnumerable<Control> AsEnumerableReq(this Control.ControlCollection collection)
        {
            var controls = collection.Cast<Control>();

            return controls.SelectMany(ctrl => AsEnumerableReq(ctrl.Controls))
                .Concat(controls);
        }

        public static T FindControlByName<T>(this Control form, string name)
        {
            return (T)(object)form.Controls.AsEnumerableReq().FirstOrDefault(c => c.Name == name);
        }
    }
}
