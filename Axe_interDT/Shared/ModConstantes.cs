﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    static class ModConstantes
    {
        public const string cUserInterP2V2 = "UserInterP2V2";
        public const string cInterventionP2 = "InterventionP2";
        public const string cCreationVisiteP2 = "CreationVisiteP2";
        public const string cCreationVisiteP2Button = "CreationVisiteP2Button";
        public const string cCreationVisiteP1Compt = "CreationVisiteP1Compt";
        public const string cfrmPDAv2P2 = "frmPDAv2P2";
        public const string cUserDocTriContrat = "UserDocTriContrat";

        public const string cCodeEnAttente = "00";
        public const string cCodeAValiderAffilie = "01";
        public const string cCodeVerifieAenvoye = "02";
        public const string cCodeEnvoye = "04";
        public const string cCodeSansSuite = "SS";

        public const string cUserDocLocalisation = "UserDocLocalisation";

    }
}
