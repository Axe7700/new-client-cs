﻿using System.Data;
using System.Data.SqlClient;

namespace Axe_interDT.Shared
{
    class ReadSoft
    {

        public static SqlConnection adoInvoice;

        //===> Mondir le 29.11.2020 pour ajouté les modifs de la version V29.11.2020
        public static string cBaseInv { get; set; } = "KOFAX_INVOICES_PROD";
        //This line is commented in this version
        //public static string cBaseInv { get; set; } = "INVOICES";
        //===> Fin Modif Mondir

        public static string sConnInvoice { get; set; }

        public static void fc_ConnectInvoices()
        {
            //=== connexion à la base INVOICES (read soft).

            if (General._ExecutionMode == General.ExecutionMode.Test)
                cBaseInv = "invoices_CSHARP_TEST";
            if (string.IsNullOrEmpty(sConnInvoice))
            {
                sConnInvoice = $"SERVER = {ModParametre.NomServeurRD}; DATABASE = {cBaseInv}; INTEGRATED SECURITY = TRUE;";
            }

            adoInvoice = new SqlConnection(sConnInvoice);//verifier
            adoInvoice.Open();

            if (adoInvoice.State == ConnectionState.Closed)
            {
                adoInvoice.Open();
            }


        }
        public static void fc_CloseConnInvoices()
        {
            //==== deconnextion de la base INVOICES.

            if ((adoInvoice != null))
            {
                if (adoInvoice.State == ConnectionState.Open)
                {
                    adoInvoice.Close();
                    adoInvoice?.Dispose();

                }
            }

        }

    }
}
