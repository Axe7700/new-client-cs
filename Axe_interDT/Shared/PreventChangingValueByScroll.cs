﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class PreventChangingValueByScroll
    {
        private object mUddSavedValueForMouseWheelHack;
        public UltraGrid mUgrid;

        public static void InitGrid(UltraGrid ultraGrid)
        {
            PreventChangingValueByScroll PreventChangingValueByScroll = new PreventChangingValueByScroll();
            PreventChangingValueByScroll.mUgrid = ultraGrid;
            PreventChangingValueByScroll.InitUltraGridUddMouseWheelHack();
        }

        public void InitUltraGridUddMouseWheelHack()
        {
            this.mUgrid.ControlAdded += new ControlEventHandler(this.mUGrid_ControlAdded);
        }

        private void mUGrid_ControlAdded(object sender, ControlEventArgs e)
        {
            UltraGrid uGrid = (UltraGrid)sender;
            if (uGrid.ActiveCell != null) {

                UltraCombo udd = uGrid.ActiveCell.ValueListResolved as UltraCombo;
                UltraDropDown udd2 = uGrid.ActiveCell.ValueListResolved as UltraDropDown;

                if (udd != null)
                {
                    e.Control.MouseWheel += new MouseEventHandler(this.mUGrid_uddControl_MouseWheel);
                    e.Control.KeyUp += new KeyEventHandler(this.mUGrid_uddControl_KeyUp);
                    e.Control.Enter += new EventHandler(this.mUGrid_uddControl_Enter);
                    udd.AfterCloseUp += Udd_AfterCloseUp;                  
                }
                else if (udd2 != null)
                {
                    e.Control.MouseWheel += new MouseEventHandler(this.mUGrid_uddControl_MouseWheel);
                    e.Control.KeyUp += new KeyEventHandler(this.mUGrid_uddControl_KeyUp);
                    e.Control.Enter += new EventHandler(this.mUGrid_uddControl_Enter);
                    udd.AfterCloseUp += Udd_AfterCloseUp;
                }
            }
            
        }

        private void Udd_AfterCloseUp(object sender, EventArgs e)
        {
            this.SaveUltraDropDownSelectedValue();
        }

        private void mUGrid_uddControl_Enter(object sender, EventArgs e)
        {
            Control senderControl = (Control)sender;
            UltraGrid uGrid = senderControl.Parent as UltraGrid;
            if (uGrid != null)
            {
                UltraDropDown dropdown = uGrid.ActiveCell.ValueListResolved as UltraDropDown;
                UltraCombo combo = uGrid.ActiveCell.ValueListResolved as UltraCombo;
                if (dropdown != null || combo!=null)
                {
                    this.mUddSavedValueForMouseWheelHack = uGrid.ActiveCell.Value;
                }
            }
        }

        private void mUGrid_uddControl_KeyUp(object sender, KeyEventArgs e)
        {
            this.SaveUltraDropDownSelectedValue();
        }

        private void mUGrid_uddControl_MouseWheel(object sender, MouseEventArgs e)
        {
            Control conSender = (Control)sender;
            UltraGrid ugSender = (UltraGrid)conSender.Parent;
            if (ugSender != null)
            {
                UltraDropDown dropdown = ugSender.ActiveCell.ValueListResolved as UltraDropDown;
                UltraCombo combo = ugSender.ActiveCell.ValueListResolved as UltraCombo;
                if (((dropdown != null) && (!dropdown.IsDroppedDown)) || ((combo != null) && (!combo.IsDroppedDown)) )
                {
                    ugSender.ActiveCell.Value = this.mUddSavedValueForMouseWheelHack;
                    ugSender.PerformAction(UltraGridAction.ExitEditMode, false, false);
                }
            }
        }

        private void SaveUltraDropDownSelectedValue()
        {
            if ((this.mUgrid.ActiveCell != null) && (this.mUgrid.ActiveCell.ValueListResolved != null))
            {
                this.mUddSavedValueForMouseWheelHack = this.mUgrid.ActiveCell.ValueListResolved.GetValue(this.mUgrid.ActiveCell.ValueListResolved.SelectedItemIndex);
            }
        }
    }
}
