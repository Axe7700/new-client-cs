﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    public static class ModCrystalPDF
    {
        //=== MODULE AJOUTE POUR MAJ DU P2 GESTEN.

        public struct tpFormulas
        {
            public string sNom;
            public string sCritere;
            //Permet de convertir en numérique les paramètres concernés
            public string sType;
        }

        public static tpFormulas[] tabFormulas { get; set; }

        public const string cExportNum = "NUM";
        public const string cExportChaine = "CHAINE";
    }
}
