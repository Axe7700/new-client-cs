﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    class ModCalcul
    {
        public struct ChargePers
        {
            public string sMat;
            public double dbCharge;
            public int lMois;
            public int lAnnee;
            public int LID;
        }

        public static ChargePers[] tChargePers;
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWhereMat"></param>
        /// <returns></returns>
        public static bool fc_LoadChargePers(string sWhereMat = "")
        {
            DataTable rstmp = default(DataTable);
            var ModAdorstmp = new ModAdo();
            int i = 0;

            tChargePers = new ChargePers[1];
            i = 0;

            General.sSQL = "SELECT CHP_Charge, matricule, CHP_Mois, CHP_Annee, CHP_NoAuto " + " From CHP_ChargePers";
            General.sSQL = General.sSQL + sWhereMat;
            General.sSQL = General.sSQL + " order by  matricule ,CHP_ANNEE desc, CHP_Mois desc";

            rstmp = ModAdorstmp.fc_OpenRecordSet(General.sSQL);

            if (rstmp.Rows.Count > 0)
            {
                foreach (DataRow Row in rstmp.Rows)
                {
                    Array.Resize(ref tChargePers, i + 1);
                    tChargePers[i].sMat = Row["Matricule"].ToString();
                    tChargePers[i].dbCharge = Convert.ToDouble(General.nz(Row["CHP_Charge"], 0));
                    tChargePers[i].lAnnee = Convert.ToInt32(General.nz(Row["CHP_Annee"], 0));
                    tChargePers[i].lMois = Convert.ToInt32(General.nz(Row["CHP_Mois"], 0));
                    tChargePers[i].LID = Convert.ToInt32(General.nz(Row["CHP_Noauto"], 0));
                    i = i + 1;
                }
            }
            return true;
        }

        public static double fc_ChargePers( string sMat,  int lMois,  int lAnnee, ref int LID )
        {
            double functionReturnValue = 0;
            int i = 0;
            bool bFindMat = false;
            //Dim bFindYear           As Boolean

            try
            {

                //== teste si le tableau est vide.
                i = tChargePers.Length;

                //== flag servant à identifier si l'employé existe dans le tableau.
                bFindMat = false;
                //== flag servant à identifier si l'année du travail réalisé par l'intervenant,
                //== figurant sur l'intervention, a une correpondance au niveau du montant de sa charge.
                //bFindYear = False

                for (i = 0; i <= tChargePers.Length - 1; i++)
                {

                    if (tChargePers[i].sMat.ToUpper() == sMat.ToUpper())
                    {
                        bFindMat = true;

                        if (tChargePers[i].lAnnee == lAnnee)
                        {

                            if (tChargePers[i].lMois <= lMois)
                            {
                                functionReturnValue = Convert.ToDouble(General.nz(tChargePers[i].dbCharge, 0));
                                LID = Convert.ToInt32(General.nz(tChargePers[i].LID, 0));
                                return functionReturnValue;
                            }

                        }
                        else if (tChargePers[i].lAnnee < lAnnee)
                        {

                            functionReturnValue = Convert.ToDouble(General.nz(tChargePers[i].dbCharge, 0));
                            LID = Convert.ToInt32(General.nz(tChargePers[i].LID, 0));
                            return functionReturnValue;

                        }
                    }

                    if (bFindMat == true && tChargePers[i].sMat.ToUpper() != sMat.ToUpper())
                    {
                        break;
                    }

                }

                //== si l'employé existe dans le tableau mais qu'aucune valeur avec l'année n'a été
                //== trouvé alors on récupére la derniére valeur.
                if (bFindMat == true)
                {
                    functionReturnValue = Convert.ToDouble(General.nz(tChargePers[i - 1].dbCharge, 0));
                    LID = Convert.ToInt32(General.nz(tChargePers[i - 1].LID, 0));
                    return functionReturnValue;
                }
            }
            catch(Exception e)
            {
                Program.SaveException(e);
                functionReturnValue = 0;
                return functionReturnValue;
            }
            return functionReturnValue;

        }
        public static double fc_CalculTauxTech( bool bCtlErr, double dbSalAnnuel,  double dbKCongesPayee, 
                                               double dbFraisDivers,  double dbFraisTrans, double dbNbHAnnuel)
        {
            try
            {
                if (dbKCongesPayee == 0)
                    dbKCongesPayee = 1;
                if (dbNbHAnnuel == 0)
                {
                    return 0;
                }
             var fc = General.FncArrondir(((dbSalAnnuel * dbKCongesPayee) + dbFraisDivers + dbFraisTrans) / dbNbHAnnuel, 2);
               
                    return fc;
               
            }
            catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_CalculTauxTech");
                return 0;
            }
        }
      
      
        public static double fc_calculMo(int lTypeCalcul = 0,  string sDuree = "",  string sHeureDeb = "", string sHeureFin = "",
            string sDureeDefaut = "",  string sMat = "" ,  DateTime dtDateRealisee= default(System.DateTime),bool bDeplacement = true)
        {

            double dbChargeMat = 0;
            string sDureeFormat = null;
            string sDebourse = null;

            //== deux types de calcul possibles à partir d'un taux horaire par defaut pour
            //== tout le monde ou bien à partir du tableaux des salaires du personnel.

            //== par defaut
           
            if (lTypeCalcul == 1)
            {

                if (sDuree != "0" && !string.IsNullOrEmpty(sDuree))
                {

                    sDureeFormat =Convert.ToDateTime(sDuree).ToString("HH:mm:ss");
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat),  sMat,  dtDateRealisee,  bDeplacement);

                }
                else if (General.IsDate(sHeureDeb) && General.IsDate(sHeureFin))
                {

                    sDureeFormat = General.fc_calcDuree(Convert.ToDateTime(sHeureDeb), Convert.ToDateTime(sHeureFin));
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat),  sMat,  dtDateRealisee,  bDeplacement);

                }
                else if (!string.IsNullOrEmpty(sDureeDefaut))
                {

                    sDureeFormat =  Convert.ToDateTime(sDureeDefaut).ToString("HH:mm:ss"); 
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat), sMat, dtDateRealisee, bDeplacement);

                }

                
            }
            else if (lTypeCalcul == 2)
            {

                if (sDuree != "0" && !string.IsNullOrEmpty(sDuree))//tested
                {

                    sDureeFormat = Convert.ToDateTime(sDuree).ToString("HH:mm:ss");
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat), sMat,  dtDateRealisee,  bDeplacement);

                }
                else if (General.IsDate(sHeureDeb) && General.IsDate(sHeureFin))
                {

                    sDureeFormat = General.fc_calcDuree(Convert.ToDateTime(sHeureDeb), Convert.ToDateTime(sHeureFin));
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat), sMat,  dtDateRealisee,  bDeplacement);

                }
                else if (!string.IsNullOrEmpty(sDureeDefaut))
                {

                    sDureeFormat = Convert.ToDateTime(sDureeDefaut).ToString("HH:mm:ss");
                    sDebourse = General.fc_CalcDebourse( Convert.ToDateTime(sDureeFormat), sMat,  dtDateRealisee,  bDeplacement);

                }

            }

          
            return Convert.ToDouble(General.nz(sDebourse, 0));

            //dbChargeMat = fc_ChargePers(smaT, lMonth, lYear)

        }
    }
}
