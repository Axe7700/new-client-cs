﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class ModLog
    {
        public const string cFileLogPDA = "LogMobilite.txt";

        public const string cMobilite = "Mobilite";

        public static object fc_Log(string sNameLog, string sContents, string sID = "")
        {
            object functionReturnValue = null;

            short iNumfichier = 0;
            string sText = null;
            string sComputer = null;
            string sPathLog = null;

            try
            {
                //=== Identifie le nom de l'ordinateur.
                sComputer = ModuleAPI.gfr_GetComputerName();

                //=== Chemin du fichier de Log.
                sPathLog = View.Theme.Theme.LogFolder + "\\";
                // gFr_Chemin(gfr_liaison("Base"))

                sText = DateTime.Now.ToString("dd/MM/yyyy HH:mm") + " => Nom App.: " + Application.ProductName + " => ComputerName: " + sComputer + " => Nom du log: " + General.fncUserName();
                if (!string.IsNullOrEmpty(sID))
                {
                    sText = sText + " => ID: " + sID;
                }

                if (sNameLog.ToUpper() == cMobilite.ToUpper())
                {
                    //=== nom et chemin du fichier du Log.
                    sPathLog = sPathLog + "\\" + Application.ProductName + "_" + cFileLogPDA;
                    //=== conenu du log.
                    sText = "\n" + sText + "==> Log: " + sContents;
                }

                if (!string.IsNullOrEmpty(sPathLog))
                {
                    //=== Écrit le texte dans le fichier.
                    File.AppendAllText(sPathLog, sText,Encoding.Unicode);
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Log: " + sPathLog, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return functionReturnValue;
            }
        }

        //'Private Sub fc_LoadDataPlanning(ByVal sDateDeb As String, ByVal sDateFin As String, Optional ByVal sCodeEtat As String, _
        //'        Optional bFistLoad As Boolean = False)
        //'Dim sSQL            As String
        //'
        //'Dim sWhere          As String
        //'Dim sWhere2         As String
        //'Dim sOrderBy        As String
        //'Dim sIntervenant    As String
        //'Dim sUser           As String
        //'Dim sWhereStatut    As String
        //'
        //'Dim bCreateResNull  As Boolean
        //'Dim bFirstSetData   As Boolean
        //'Dim bFirsCodeEtat   As Boolean
        //'
        //'Dim rs              As ADODB.Recordset
        //''Dim rsP             As ADODB.Recordset
        //'
        //'
        //'Dim dtDateRVend         As Date
        //'Dim dtDateHeureRv       As Date
        //'Dim dtDate              As Date
        //'Dim dtDateDeb           As Date
        //'Dim dtDateFin           As Date
        //'Dim dtTemp              As Date
        //'Dim dtTempX             As Date
        //'
        //'Dim pEvent          As CalendarEvent
        //'Dim lEventID        As Long
        //'
        //'Dim iRes            As Long
        //'Dim lIdShedule      As Long
        //'Dim i               As Long
        //'Dim iTab            As Long
        //'Dim xi              As Long
        //'Dim lViewType       As Long
        //'
        //'Dim arResources As New CalendarResources
        //'
        //'Dim pRes() As CalendarResource
        //'
        //'Dim pSchedules As CalendarSchedules
        //'
        //'Dim pSchedule() As CalendarSchedule
        //'
        //'Dim tabStatut() As String
        //'
        //'On Error GoTo erreur
        //'
        //''fc_PropertyCalandar
        //'
        //'If IsDate(sDateDeb) Then
        //'    dtDateDeb = CDate(sDateDeb)
        //'Else
        //'    MsgBox "Date Incorrecte", vbInformation, ""
        //'    Exit Sub
        //'End If
        //'
        //'If IsDate(sDateFin) Then
        //'    dtDateFin = CDate(sDateFin)
        //'Else
        //'    MsgBox "Date Incorrecte", vbInformation, ""
        //'    Exit Sub
        //'End If
        //'
        //''DateCal.MonthDelta = Month(DTDateRealiseDe.value)
        //'
        //'
        //'If txtIntervenant = "" And CmbGroupe = "" Then
        //'    lblMes.Visible = True
        //'    If bFistLoad = True Then
        //'        lblMes = "Vous devez sélectionner à minima un intervenant ou un groupe d'intervenant."
        //'        lblTot = "Total : 0"
        //'        Exit Sub
        //'    Else
        //'        lblMes = "Vous devez sélectionner à minima un intervenant ou un groupe d'intervenant."
        //'        lblTot = "Total : 0"
        //'        Exit Sub
        //'    End If
        //'End If
        //'lblMes.Visible = False
        //'
        //'If sCodeEtat <> "" Then
        //'    sCodeEtat = Trim(sCodeEtat)
        //'    sCodeEtat = Replace(sCodeEtat, " ", "")
        //'    If InStr(1, sCodeEtat, ";") > 0 Then
        //'            tabStatut = Split(sCodeEtat, ";")
        //'    Else
        //'            ReDim tabStatut(0)
        //'            tabStatut(0) = sCodeEtat
        //'    End If
        //'Else
        //'        ReDim tabStatut(0)
        //'        tabStatut(0) = ""
        //'End If
        //'
        //'
        //'DateCal.BoldDaysWithEvents = True
        //'
        //''==== modif du 28 03 2019, place ce code si eof = false
        //''=== supprime les ressources si existantes
        //''i = Cal.MultipleResources.Count
        //''
        //''Do While i > 0
        //''    Cal.MultipleResources.Remove i - 1
        //''    i = i - 1
        //''Loop
        //'
        //''=== supprime tous les events
        //''Cal.DataProvider.RemoveAllEvents
        //'
        //'sUser = fncUserName
        //'dtDate = Now
        //'Erase tpEvent
        //'ReDim Preserve tpEvent(0)
        //'
        //'Screen.MousePointer = 11
        //'
        //'sSQL = "DELETE FROM PLANM_PLanMemoire WHERE  PLANM_Utilisateur = '" & gFr_DoublerQuote(sUser) & "'"
        //'
        //'adocnn.Execute sSQL
        //'
        //''Cal.DataProvider.RemoveAllEvents
        //'
        //'Screen.MousePointer = 11
        //'
        //'sSQL = "SELECT     Intervention.NumFicheStandard, Intervention.NoIntervention, Intervention.CodeImmeuble, intervention.DateSaisie, Intervention.Article," _
        //'    & " Intervention.Designation, " _
        //'    & " Intervention.Commentaire, Intervention.Intervenant, Intervention.DateSaisie, Intervention.DatePrevue, Intervention.HeurePrevue, Intervention.DureePrevue " _
        //'    & " ,Intervention.DateRealise, " _
        //'    & " Intervention.HeureDebut , Intervention.HeureFin, Intervention.Duree, Intervention.CodeEtat, Personnel.Nom, Personnel.Prenom " _
        //'    & " , intervention.NoMaj, TypeCodeEtat.CouleurStatut, Intervention.CodeEtat " _
        //'    & " FROM  Intervention LEFT OUTER JOIN " _
        //'    & " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat LEFT OUTER JOIN " _
        //'    & " Personnel ON Intervention.Intervenant = Personnel.Matricule "
        //'
        //'sWhere = ""
        //'
        //''If DTDatePrevueDe <> "" Then
        //''    If sWhere = "" Then
        //''            sWhere = " WHERE Intervention.DatePrevue >='" & DTDatePrevueDe & "'"
        //''    Else
        //''            sWhere = sWhere & " AND Intervention.DatePrevue >='" & DTDatePrevueDe & "'"
        //''    End If
        //''End If
        //''If DTDatePrevueAu <> "" Then
        //''    If sWhere = "" Then
        //''            sWhere = " WHERE Intervention.DatePrevue <='" & DTDatePrevueAu & "'"
        //''    Else
        //''            sWhere = sWhere & " AND Intervention.DatePrevue <='" & DTDatePrevueAu & "'"
        //''    End If
        //''End If
        //'
        //'
        //'If IsDate(sDateDeb) And IsDate(sDateFin) Then
        //'    '=== reprend la prémière date du date picker.
        //'    'dtDateDeb = DateCal.FirstVisibleDay    'DateCal.Selection.Blocks(0).DateBegin
        //'
        //'
        //'    If sWhere = "" Then
        //'           ' sWhere = " WHERE ((Intervention.DateRealise >='" & dtDateDeb & "'" _
        //'                  & "AND  Intervention.DateRealise <='" & dtDateFin & "')" _
        //'                  & " OR " _
        //'                  & " (Intervention.DatePrevue  >='" & dtDateDeb & "'" _
        //'                  & " AND Intervention.DatePrevue  <='" & dtDateFin & "'" _
        //'                  & " AND Intervention.DateRealise IS NULL) " _
        //'                  & " OR " _
        //'                  & " (Intervention.DateSaisie  >='" & dtDateDeb & "'" _
        //'                  & "AND Intervention.DateSaisie  <='" & dtDateFin & "'" _
        //'                  & " AND Intervention.DateRealise IS NULL " _
        //'                  & " AND Intervention.DatePrevue IS NULL)) " _
        //'
        //'             sWhere = " WHERE ((Intervention.DateRealise >='" & dtDateDeb & "'" _
        //'                  & "AND  Intervention.DateRealise <='" & dtDateFin & "')" _
        //'                  & " OR " _
        //'                  & " (Intervention.DatePrevue  >='" & dtDateDeb & "'" _
        //'                  & " AND Intervention.DatePrevue  <='" & dtDateFin & "'" _
        //'                  & " AND Intervention.DateRealise IS NULL)) " _
        //'
        //'    Else
        //'           ' sWhere = " AND ((Intervention.DateRealise >='" & dtDateDeb & "'" _
        //'                  & "AND  Intervention.DateRealise <='" & dtDateFin & "')" _
        //'                  & " OR " _
        //'                  & " (Intervention.DatePrevue  >='" & dtDateDeb & "'" _
        //'                  & " AND Intervention.DatePrevue  <='" & dtDateFin & "')" _
        //'                  & " OR " _
        //'                  & " (Intervention.DateSaisie  >='" & dtDateDeb & "'" _
        //'                  & "AND Intervention.DateSaisie  <='" & dtDateFin & "'))"
        //'
        //'            sWhere = " AND ((Intervention.DateRealise >='" & dtDateDeb & "'" _
        //'                  & "AND  Intervention.DateRealise <='" & dtDateFin & "')" _
        //'                  & " OR " _
        //'                  & " (Intervention.DatePrevue  >='" & dtDateDeb & "'" _
        //'                  & " AND Intervention.DatePrevue  <='" & dtDateFin & "'))"
        //'
        //'
        //'            'sWhere = sWhere & " AND (Intervention.DateRealise >='" & dtDateDeb & "'" _
        //'                & " or Intervention.DatePrevue  >='" & dtDateDeb & "'" _
        //'                & " or Intervention.DateSaisie  >='" & dtDateDeb & "')"
        //'    End If
        //'End If
        //'
        //'
        //''If IsDate(sDateFin) Then
        //''    '=== reprend la derniére date du date picker.
        //''    'dtDateFin = DateCal.LastVisibleDay   'DateCal.Selection.Blocks(0).DateEnd
        //''    If sWhere = "" Then
        //''            sWhere = " WHERE (Intervention.DateRealise <='" & dtDateFin & "'" _
        //''                & " or Intervention.DatePrevue  <='" & dtDateFin & "'" _
        //''                & " or Intervention.DateSaisie  <='" & dtDateFin & "')"
        //''
        //''    Else
        //''            sWhere = sWhere & " AND (Intervention.DateRealise <='" & dtDateFin & "'" _
        //''                & " or Intervention.DatePrevue  <='" & dtDateFin & "'" _
        //''                & " or Intervention.DateSaisie  <='" & dtDateFin & "')"
        //''    End If
        //''End If
        //'
        //''==== Gestion des statuts multiples.
        //'bFirsCodeEtat = True
        //'sWhereStatut = ""
        //'
        //'For xi = 0 To UBound(tabStatut)
        //'    If tabStatut(xi) <> "" And tabStatut(xi) <> ";" Then
        //'            If sWhere = "" And sWhereStatut = "" Then
        //'                    sWhereStatut = " WHERE ( Intervention.CodeEtat  ='" & gFr_DoublerQuote(tabStatut(xi)) & "'"
        //'            Else
        //'                    If sWhere = "" And sWhereStatut <> "" Then
        //'                            sWhereStatut = sWhereStatut & " OR Intervention.CodeEtat ='" & gFr_DoublerQuote(tabStatut(xi)) & "'"
        //'                    Else
        //'                            If bFirsCodeEtat = True Then
        //'                                    bFirsCodeEtat = False
        //'                                    sWhereStatut = sWhereStatut & " AND ( Intervention.CodeEtat ='" & gFr_DoublerQuote(tabStatut(xi)) & "'"
        //'                            ElseIf bFirsCodeEtat = False Then
        //'                                    sWhereStatut = sWhereStatut & " or Intervention.CodeEtat ='" & gFr_DoublerQuote(tabStatut(xi)) & "'"
        //'                            End If
        //'                    End If
        //'            End If
        //'    End If
        //'Next
        //'
        //'If sWhereStatut <> "" Then
        //'    sWhereStatut = sWhereStatut & ")"
        //'    sWhere = sWhere & sWhereStatut
        //'End If
        //'
        //'If CmbGroupe <> "" Then
        //'
        //'    sWhere2 = "SELECT     GRID_GroupeIntervenantDetail.GRID_Mat" _
        //'            & " FROM         GRI_GroupeIntervenant INNER JOIN " _
        //'            & " GRID_GroupeIntervenantDetail ON GRI_GroupeIntervenant.GRI_ID = GRID_GroupeIntervenantDetail.GRI_ID " _
        //'            & " WHERE  GRI_GroupeIntervenant.GRI_Nom = '" & gFr_DoublerQuote(CmbGroupe) & "'"
        //'
        //'
        //'    If sWhere = "" Then
        //'            sWhere = " WHERE (Intervention.Intervenant  IN (" & sWhere2 & ") "
        //'    Else
        //'            sWhere = sWhere & " AND (Intervention.Intervenant  IN (" & sWhere2 & ") "
        //'    End If
        //'
        //'    If CmbGroupe <> "" And txtIntervenant <> "" Then
        //'            sWhere = sWhere & " OR Intervention.Intervenant ='" & gFr_DoublerQuote(txtIntervenant) & "'"
        //'    End If
        //'
        //'     sWhere = sWhere & ")"
        //'
        //'End If
        //'
        //'
        //'If CmbGroupe = "" And txtIntervenant <> "" Then
        //'    If sWhere = "" Then
        //'            sWhere = " WHERE Intervention.Intervenant  ='" & gFr_DoublerQuote(txtIntervenant) & "'"
        //'    Else
        //'            sWhere = sWhere & " AND Intervention.Intervenant ='" & gFr_DoublerQuote(txtIntervenant) & "'"
        //'    End If
        //''    If sWhere = "" Then
        //''            sWhere = " WHERE (Intervention.Intervenant  ='XX1' OR Intervention.Intervenant  ='XX2')"
        //''    Else
        //''            sWhere = sWhere & " AND (Intervention.Intervenant  ='XX1' OR Intervention.Intervenant  ='XX2')"
        //''    End If
        //'
        //'
        //'ElseIf CmbGroupe = "" And txtIntervenant = "" Then
        //'    If sWhere = "" Then
        //'            sWhere = " WHERE Intervention.Intervenant  is not null"
        //'    Else
        //'            sWhere = sWhere & " AND  (Intervention.Intervenant is not null and  Intervention.Intervenant <> '')"
        //'    End If
        //'
        //'
        //'End If
        //'
        //'
        //'sOrderBy = " ORDER BY Intervention.Intervenant, Intervention.DateRealise "
        //'
        //'fc_SetWherePlanOut sWhere
        //'Screen.MousePointer = 11
        //'Set rs = fc_OpenRecordSet(sSQL & sWhere & sOrderBy)
        //''Debug.Print sSQL & sWhere & sOrderBy
        //'
        //'lblTot = "Total : " & rs.RecordCount
        //'
        //''DateCal.Selection(0).DateBegin = dtDateDeb
        //'
        //''DateCal.Selection(0).DateEnd = dtDateFin
        //'
        //'sIntervenant = ""
        //'bCreateResNull = False
        //'iRes = 0
        //'Set pSchedules = Nothing
        //'
        //''Cal.MultipleSchedulesMode = True
        //'
        //''Cal.ColorScheduleMode = True
        //'
        //'bFirstSetData = True
        //'
        //'If Not (rs.EOF And rs.bof) Then
        //'        ' DateCal.SelectRange dtDateDeb, dtDateFin
        //'        ' DateCal.ResyncCalendar
        //'        '  DateCal.AttachToCalendar Cal
        //'
        //'        '=== modif du 28 03 2019, ajout de.
        //'        i = Cal.MultipleResources.Count
        //'
        //'        Do While i > 0
        //'            Cal.MultipleResources.Remove i - 1
        //'            i = i - 1
        //'        Loop
        //'        '=== modif du 28 03 2019, fin ajout.
        //'Else
        //'       '=== modif du 28 03 2019, ajout de.
        //'
        //'        fc_selectRange dtDateDeb, dtDateFin
        //'
        //'        DateCal.SelectRange dtDateDeb, dtDateFin
        //'
        //'        Cal.Populate
        //'        Cal.RedrawControl
        //'        DateCal.ResyncCalendar
        //'
        //'        If bload = False Then
        //'            FC_SaveParam
        //'        End If
        //'        '=== modif du 28 03 2019, fin ajout.
        //'End If
        //'
        //'Do While rs.EOF = False
        //'            Screen.MousePointer = 11
        //'            '=== cree une ressoucre pour chaque intervenant, dans le cas au une intervention n'est pas affacté à un intervenant
        //'            '=== on créer une resource avec la constante cSansIntervenant.
        //'            If UCase(sIntervenant) <> UCase(nz(rs!Intervenant, "")) Or (nz(rs!Intervenant, "") = "" And bCreateResNull = False) Then
        //'
        //'                    If nz(rs!Intervenant, "") = "" Then
        //'                        bCreateResNull = True
        //'                    End If
        //'
        //'                    sIntervenant = nz(rs!Intervenant, "")
        //'
        //'                    ReDim Preserve pRes(iRes)
        //'                   ' pRes(iRes).SetDataProvider Cal.DataProvider, False
        //'                    Set pRes(iRes) = New CalendarResource
        //'                    'pRes(iRes).SetDataProvider2 cCalProvider, False
        //'                    'pRes(iRes).SetDataProvider Cal.DataProvider, False
        //'                    If bFirstSetData = True Then
        //'                        pRes(iRes).SetDataProvider2 cCalProvider, True
        //'                        bFirstSetData = False
        //'                    Else
        //'                        pRes(iRes).SetDataProvider pRes(0).DataProvider, False
        //'                    End If
        //'
        //'                    If Not pRes(iRes).DataProvider.Open Then
        //'                        If Not pRes(iRes).DataProvider.Create Then
        //'                           MsgBox "Erreur sur connection.", vbInformation, ""
        //'                            Exit Sub
        //'                        End If
        //'                    End If
        //'
        //'
        //'
        //'                    '=== // ** schedules.
        //'                    If pSchedules Is Nothing Then
        //'                        Set pSchedules = pRes(iRes).DataProvider.Schedules
        //'                     End If
        //'
        //'                    If pSchedules Is Nothing Then
        //'                            MsgBox "Erreur sur calendrier.", vbInformation, ""
        //'                            Exit Sub
        //'                    End If
        //'
        //'                    '=== charge les shedules.
        //'                    pSchedules.AddNewSchedule rs!Intervenant & ""
        //'
        //'                    pRes(iRes).DataProvider.Save
        //'
        //'                    '===// ** resources.
        //'                    pRes(iRes).Name = pSchedules.Item(iRes).Name
        //'                    pRes(iRes).ScheduleIDs.Add pSchedules.Item(iRes).Id
        //'                     lIdShedule = pSchedules.Item(iRes).Id
        //'                    'Cal.Options.DayViewCurrentTimeMarkVisible
        //'
        //'                    arResources.Add pRes(iRes)
        //'
        //'                    Cal.SetMultipleResources arResources
        //'
        //'                    iRes = iRes + 1
        //'
        //'            End If
        //'
        //'
        //'            'If nz(rs!DateRealise, 0) <> 0 Then
        //'            fc_MajList "", nz(rs!CouleurStatut, 0)
        //'
        //'            fc_InsertEventADO rs, lIdShedule, True, Cal, dtDateDeb, dtDateFin
        //'            ' DateCal.BoldDaysWithEvents = True
        //'
        //'            'End If
        //'SUITE:
        //'
        //'            rs.MoveNext
        //'Loop
        //'
        //'If Not (rs.EOF And rs.bof) Then
        //'
        //'    fc_selectRange dtDateDeb, dtDateFin
        //'    DateCal.SelectRange dtDateDeb, dtDateFin
        //'
        //'   ' Cal.ViewType = xtpCalendarWorkWeekView
        //'    DateCal.ResyncCalendar
        //'    Cal.Populate
        //'    Cal.RedrawControl
        //'
        //'
        //'End If
        //'
        //'If bload = False Then
        //'    FC_SaveParam
        //'End If
        //'
        //'Screen.MousePointer = 1
        //'
        //'rs.Close
        //'Set rs = Nothing
        //'
        //'Timer1.Enabled = True
        //'
        //''    'DateCal.ResyncCalendar
        //''    Cal.Populate
        //''
        //''    Dim X As CalendarEvent
        //''
        //''    'For Each x In Cal.DataProvider.GetAllEventsRaw
        //''
        //''
        //''
        //''    Dim x1   As CalendarEvents
        //''    Dim p   As CalendarEvent
        //''
        //''    Set x1 = Cal.DataProvider.GetAllEventsRaw()
        //''
        //''    For Each p In x1
        //''        Debug.Print p.Body
        //''
        //''    Next
        //'
        //'    'Cal.RefreshCaptionBar
        //'    'DateCal.ResyncCalendar
        //'
        //'    '=== modif du 28 03 2019, ajout de.
        //'    'lViewType = CalendarControl.ViewType = xtpCalendarDayView
        //'
        //'    'DateCal.SelectRange dtDateDeb, dtDateFin
        //'    'CalendarControl.ViewType = lViewType
        //'    '=== modif du 28 03 2019, fin ajout.
        //'   ' Cal.Populate
        //'   ' Cal.RedrawControl
        //'    'DateCal.ResyncCalendar
        //''    DateCal.BoldDaysWithEvents = True
        //'
        //'
        //''   Cal.RedrawControl
        //''    Cal.ZOrder 1
        //''
        //''    Cal.Visible = False
        //''    Cal.Visible = True
        //'
        //'Exit Sub
        //'erreur:
        //'    gFr_debug Me.Name & ";fc_LoadDataPlanning"
        //'End Sub
        //'
    }
}
