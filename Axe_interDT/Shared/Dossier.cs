﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    static class Dossier
    {
        public struct WIN32_FIND_DATA
        {
            public int dwFileAttributes;
            public FILETIME ftCreationTime;
            public FILETIME ftLastAccessTime;
            public FILETIME ftLastWriteTime;
            public int nFileSizeHigh;
            public int nFileSizeLow;
            public int dwReserved0;
            public int dwReserved1;
            public string cFileName;
            public string cAlternate;
        }
        public struct FILETIME
        {
            public int dwLowDateTime;
            public int dwHighDateTime;
        }
        public struct ListeFichier
        {
            public WIN32_FIND_DATA[] Fichiers;
            public string[] Chemin;
            public int Nombre;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sPathFolder"></param>
        /// <param name="blMessage"></param>
        /// <returns></returns>
        /// 
        public static bool fc_ControleFichier(string sPathFolder, bool blMessage = false)
        {
            bool functionReturnValue = false;
            // mettre un chemin de dossier dans sPathFolder
            // retourne true si celui ci éxiste.
            // si blMessage = true affichage du message ci dessous.
            var blFileExist = false;

            try
            {
                blFileExist = File.Exists(sPathFolder);
                if (blFileExist == false)
                {
                    functionReturnValue = false;
                }
                else
                {
                    functionReturnValue = true;
                }

                if (blMessage == true && blFileExist == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier " + sPathFolder + " n'éxiste pas");
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_ControleFichier");
            }

            return functionReturnValue;
        }

        public static bool fc_ControleDossier(string sPathFolder, bool blMessage = false)
        {
            bool functionReturnValue = false;
            // mettre un chemin de dossier dans sPathFolder
            // retourne true si celui ci éxiste.
            // si blMessage = true affichage du message ci dessous.
            bool blFoldersExist = false;
            try
            {
                blFoldersExist = Directory.Exists(sPathFolder);
                if (blFoldersExist == false)
                {
                    functionReturnValue = false;
                }
                else
                {
                    functionReturnValue = true;
                }

                if (blMessage == true && blFoldersExist == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le Dossier " + sPathFolder + " n'éxiste pas", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_ControleDossier_moduleDossier");
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sPathFolder"></param>
        /// <returns></returns>
        public static int fc_CreateDossier(string sPathFolder)
        {
            int functionReturnValue = -1;
            // mettre un chemin de dossier dans sPathFolder
            // si celui ci n'existe pas le crée.
            try
            {
                if (Directory.Exists(sPathFolder) == false)
                {
                    Directory.CreateDirectory(sPathFolder);
                    functionReturnValue = 1;
                }
                else
                {
                    functionReturnValue = 0;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_CreateDossier_moduleDossier");
                functionReturnValue = -1;
            }
            return functionReturnValue;
        }

        public static void fc_WriteInFile(string sPathFile, string sTexte)
        {
            //écris le contenu le contenu de stexte dans un fichier.
            File.WriteAllText(sPathFile, sTexte);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sPathFile"></param>
        /// <returns></returns>
        public static bool fc_SupprimeFichier(string sPathFile)
        {
            //detruit le fichier
            if (File.Exists(sPathFile))
            {
                File.Delete(sPathFile);
                return true;
            }
            return false;
        }
        public static bool fc_CopieFichier(string sSourcePathFile, string sDestPathFile
            , bool blMessage = false, bool blnSupprDoublon = false)
        {
            //deplace le fichier et renvoie true si l'operation s"est bien passée.
            try
            {
                File.Copy(sSourcePathFile, sDestPathFile,true);
                return true;
            }
            catch (Exception e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur lors de la copie du fichier : \nde l'emplacement " + sSourcePathFile + "\n" + "Vers l'emplacement " + sDestPathFile, "Copie annulée",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                Program.SaveException(e);
                return false;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sPathFolder"></param>
        /// <returns></returns>
        public static bool fc_ControleAnCreateDossier(string sPathFolder)
        {
            bool functionReturnValue = false;
            // mettre un chemin de dossier dans sPathFolder
            // retourne true si celui ci éxiste.
            // si blMessage = true affichage du message ci dessous.
            object fs = null;
            bool blFoldersExist = false;
            string[] tabFolders = null;
            short i = 0;
            short j = 0;
            string sFolder = null;

            try
            {
                functionReturnValue = false;

                if (string.IsNullOrEmpty(sPathFolder))
                    return functionReturnValue;


                blFoldersExist = Directory.Exists(sPathFolder);
                if (blFoldersExist == false)
                {
                    tabFolders = sPathFolder.Split('\\');
                    for (i = 0; i <= tabFolders.Length - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(tabFolders[i]))
                        {
                            sFolder = "";
                            for (j = 0; j <= i; j++)
                            {
                                sFolder = sFolder + tabFolders[j];
                                if (j < i)
                                {
                                    sFolder = sFolder + "\\";
                                }
                            }
                            if (sFolder.Contains("\\\\") && i > 2)
                            {
                                if (fc_ControleDossier(sFolder) == false)
                                {
                                    fc_CreateDossier(sFolder);
                                }
                            }
                            else if (i > 0)
                            {
                                if (fc_ControleDossier(sFolder) == false)
                                {
                                    fc_CreateDossier(sFolder);
                                }
                            }
                        }
                    }
                }
                else
                {

                }
                fs = null;
                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Dossier.bas;fc_ControleAnCreateDossier;");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNomDossier"></param>
        /// <param name="sExtention"></param>
        /// <returns></returns>
        public static object fc_NettoieDossier(string sNomDossier, string sExtention = "*.*")
        {
            object functionReturnValue = null;
            try
            {

                if (string.IsNullOrEmpty(sExtention))
                    sExtention = "*.*";

                if (fc_ControleDossier(sNomDossier) == true)
                {
                    if (Directory.GetFiles(sNomDossier, sExtention).Length == 0)
                        return functionReturnValue;
                    foreach (string sFic in Directory.GetFiles(sNomDossier, sExtention))
                    {
                        if (Convert.ToDateTime(fc_DateFile(sFic)) < DateTime.Today)
                        {
                            fc_SupprimeFichier(sFic);
                        }
                    }
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sPathFile"></param>
        /// <returns></returns>
        public static string fc_DateFile(string sPathFile)
        {
            string functionReturnValue = null;
            // retourne la taille d un fichier.
            if (fc_ControleFichier(sPathFile) == true)
            {
                functionReturnValue = Convert.ToString(File.GetLastWriteTime(sPathFile));
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public static int fc_CtrlFileIn(string Path)
        {
            int functionReturnValue = 0;
            //=== controle dans un dossier si il existe au moins 1 fichier.
            //=== envoyer en parametre 'chemindosiier\*.*'
            int hFindFile = 0;
            string filename = null;

            functionReturnValue = Directory.GetFiles(Path).Length > 0 ? 1 : 0;

            //foreach (string file in Directory.GetFiles(Path))
            //{
            //    filename = General.Mid(file, 1, file.IndexOf((char)0) - 1);
            //    if (filename != "." && filename != "..")
            //    {
            //        functionReturnValue = 1;
            //        break;
            //    }
            //}
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Path"></param>
        /// <returns></returns>
        public static string fc_ListDirectoryName(string Path)
        {
            string functionReturnValue = null;
            //=== retourne les nom des fichiers.
            //=== envoyer en parametre 'chemindosiier\*.*'
            int hFindFile = 0;
            string filename = null;

            functionReturnValue = "";

            foreach (string file in Directory.GetFiles(Path))
            {
                //filename = General.Mid(file, 1, file.IndexOf((char)0) - 1);
                filename = System.IO.Path.GetFileName(file);
                if (filename != "." && filename != "..")
                {
                    functionReturnValue = functionReturnValue + filename + ";";
                }
            }
            return functionReturnValue;
        }

        public static int fc_ListDirectory(string Path)
        {
            int functionReturnValue = 0;
            //=== retourne le nombre de fichier present dans un dossier.
            //=== envoyer en parametre 'chemindosiier\*.*'
            int hFindFile = 0;
            string filename = null;

            functionReturnValue = 0;

            foreach (string file in Directory.GetFiles(Path))
            {
                filename = General.Mid(file, 1, file.IndexOf((char)0) - 1);
                if (filename != "." & filename != "..")
                {
                    functionReturnValue = functionReturnValue + 1;
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sSourcePathFile"></param>
        /// <param name="sDestPathFile"></param>
        /// <param name="blMessage"></param>
        /// <returns></returns>
        public static bool fc_DeplaceFichier(string sSourcePathFile, string sDestPathFile, bool blMessage = false)
        {
            bool functionReturnValue = false;
            // deplace le fichier et renvoie true si l'operation s"est bien passée.
            DialogResult sMsg;

            try
            {
                // controle si le fichier existe
                if (fc_ControleFichier(sDestPathFile) == true)
                {
                    sMsg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le fichier " + sSourcePathFile + " éxiste déjà voulez vous le remplacer", "", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                    if (sMsg == DialogResult.No)
                    {
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                    else if (sMsg == DialogResult.Yes)
                    {
                        // supprime fichier
                        fc_SupprimeFichier(sDestPathFile);
                    }
                }

                // Mondir commenté ces deux ligne !
                //sSourcePathFile = sSourcePathFile.Replace("\\\\", "\\");
                //sDestPathFile = sDestPathFile.Replace("\\\\", "\\");
                try
                {
                    File.Move(sSourcePathFile, sDestPathFile);
                }catch(Exception exc)
                {
                    if(exc.HResult== -2147024864)
                    {
                        sMsg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le fichier " + sSourcePathFile + " est encours d'utilisation ou est peut-etre déjà ouvert", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                }
 
                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_DeplaceFichier");
                return functionReturnValue;
            }
        }

        public static string ExtractFileName(string sFullPath)
        {
            string functionReturnValue = null;
            if (!sFullPath.Contains("\\") || General.Right(sFullPath, 1) == "\\")
            {
                functionReturnValue = "";
                return functionReturnValue;
            }
            functionReturnValue = General.Mid(sFullPath, sFullPath.LastIndexOf("\\") + 2);
            return functionReturnValue;
        }
        public static int Rechercher(string Chemin, string FichierR, ref ListeFichier ResultatRecherche)
        {

            int functionReturnValue = 0;

            //---Déclaration des variables---
            WIN32_FIND_DATA lpFindFileData = default(WIN32_FIND_DATA);
            int hFindFile = 0;
            int lgRep = 0;
            string CheminRep = null;

            //---Recherche tous les fichiers demandés dans le répertoire Chemin---
            lpFindFileData.dwFileAttributes = 0x10;

            string FicTmp = null;


            //---récupération du premier fichier du répertoire
            //hFindFile = FindFirstFile(Chemin + FichierR, ref lpFindFileData);//TODO

            var FileInfo = new DirectoryInfo(Chemin);
            var Files = FileInfo.GetFiles(FichierR);           
            if (Files.Length > 0)
            {
                foreach (var file in Files)
                {
                    lpFindFileData.cFileName = file.Name;
                    //---on récupère le nom du fichier
                    FicTmp = file.Name /*General.Mid(lpFindFileData.cFileName, 1, lpFindFileData.cFileName.IndexOf(Strings.Chr(0)) - 1)*/;
                    //if (FicTmp != ".." && FicTmp != "." &&
                    //    (lpFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
                    //{
                    //---si on tombe sur un fichier , on mémorise le nom du du fichier dans un tableau
                    //---ainsi que le répertoire ou il se situe
                    ResultatRecherche.Nombre = ResultatRecherche.Nombre + 1;
                    //---un fichier ajouté
                    Array.Resize(ref ResultatRecherche.Chemin, ResultatRecherche.Nombre + 1);
                    Array.Resize(ref ResultatRecherche.Fichiers, ResultatRecherche.Nombre + 1);
                    ResultatRecherche.Chemin[ResultatRecherche.Nombre] = Chemin;
                    //---chemin du fichier
                    ResultatRecherche.Fichiers[ResultatRecherche.Nombre] = lpFindFileData;
                    //}
                }
            }

            FileInfo = new DirectoryInfo(Chemin);
            var Directories = FileInfo.GetDirectories();
            if (Directories.Length > 0)
            {
                foreach (var direcetory in Directories)
                {
                    //---on cherche un sous-répertoire
                    //---on cherche un sous-répertoire
                    //if ((lpFindFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == FILE_ATTRIBUTE_DIRECTORY)
                    //{
                    //---un sous-répertoire a été trouvé
                    //---on récupère le nom du sous répertoire
                    //CheminRep = General.Mid(lpFindFileData.cFileName, 1,
                    //    lpFindFileData.cFileName.IndexOf(Strings.Chr(0), 1) - 1);
                    CheminRep = direcetory.Name;
                    //---attention il ne faut pas prendre en compte les valeurs "." et ".."
                    //---considérées comme des répertoires mais qui ne nous intéressent pas
                    if ((CheminRep != ".") && (CheminRep != ".."))
                        //{
                        CheminRep = Chemin + CheminRep + "\\";
                    //---on relance la fonction Rechercher pour récupérer les fichiers du sous répertoire
                    //---voici la récurcivité!!!!
                    //---on va alors récupérer tous les fichiers du sous répertoire mais on va alors rechercher
                    //---si dans ce sous répertoire il y a d'autres sous répertoires etc...
                    functionReturnValue = Rechercher(CheminRep, FichierR, ref ResultatRecherche);
                    //on récupère la main après avoir vérifier le contenu du sous répertoire
                    //}
                    //}
                }

            }

            //---on retourne le nombre de fichiers trouvées
            functionReturnValue = ResultatRecherche.Nombre;
            return functionReturnValue;
        }
        public static string ExtractNameDossier(string sFullPath)
        {
            long i;
            long Y;
            string sT;
            string Return = "";
            // == retourne le nom du dossier.
            try
            {
                sT = ExtractFileName(sFullPath);
                i = sT.Length;
                Y = sFullPath.Length;
                Return = sFullPath.Substring(0, Convert.ToInt16(Math.Abs(Y - i)));
                return Return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";ExtractNameDossier");
                return Return;
            }

        }

        public static string ExtractFileWhihoutExt(string sFullPath)
        {
            string sName;
            string stemp;
            int i;
            string functionReturnValue = null;

            try { 
            //'== retourne le nom du fichier sans l'extension.

            sName = ExtractFileName(sFullPath);
            if (!sName.Contains("."))
            {
                functionReturnValue = "";
                return functionReturnValue;
            }
            else
            {
                stemp = General.Mid(sName, sName.LastIndexOf(".") + 1);
                i = stemp.Length + 1;//' le + 1 correspond au point precedent l'extension.
                i = Math.Abs(i - sName.Length);
                functionReturnValue = General.Mid(sName, 1, i);
                return functionReturnValue;
            }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";ExtractFileWhihoutExt");
                return functionReturnValue;
            }


        }
        public static string fc_ReturnPathTemp(string sNameFile, bool bCreateDir, string sExt)
        {
            //'===  retourne le chemin et le nom complet du fichier.
            string sNomRep;     
            string sNomFic;
            string functionReturnValue = null;

            try
            {
                sNomFic = sNameFile + "_" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + sExt;

                sNomRep = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + Application.ProductName + "\\Export\\";

                if (bCreateDir == true)
                {
                    fc_ControleAnCreateDossier(sNomRep);
                }
                functionReturnValue= sNomRep + sNomFic;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";fc_ReturnPathTemp");
                return functionReturnValue;
            }
        }

        public static string ExtractFileExt(string sFullPath)
        {
            string sName;
          
            string functionReturnValue = null;

            try
            {
                
                sName = ExtractFileName(sFullPath);
                if (!sName.Contains("."))
                {
                    functionReturnValue = "";
                    return functionReturnValue;
                }
                else
                {
                    functionReturnValue = General.Mid(sName, sName.LastIndexOf(".") + 1);                  
                    return functionReturnValue;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";ExtractFileWhihoutExt");
                return functionReturnValue;
            }


        }

    }
}
