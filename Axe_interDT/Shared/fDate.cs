﻿
using System;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class fDate
    {
        public static System.DateTime fc_DateFinMois(System.DateTime dtDate, bool blnSansWeekEnd = true)
        {
            System.DateTime functionReturnValue = default(System.DateTime);
            System.DateTime sDateDernierJour = default(System.DateTime);

            try
            {
                sDateDernierJour = fc_FinDeMois(Convert.ToString(dtDate.Month), Convert.ToString(dtDate.Year));

                var sJour = sDateDernierJour.DayOfWeek;

                if (blnSansWeekEnd == true)
                {
                    //Dimanche
                    if (sJour == DayOfWeek.Sunday)
                    {
                        functionReturnValue = sDateDernierJour.AddDays(-2);
                        //Samedi
                    }
                    else if (sJour == DayOfWeek.Saturday)
                    {
                        functionReturnValue = sDateDernierJour.AddDays(-1);
                    }
                    else
                    {
                        functionReturnValue = sDateDernierJour;
                    }
                }
                else
                {
                    functionReturnValue = sDateDernierJour;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fDate;fc_DateFinMois;");
                return functionReturnValue;
            }
        }

        public static System.DateTime fc_FinDeMois(string sMois, string sAnnee)
        {
            System.DateTime functionReturnValue = default(System.DateTime);
            //--A partir d'un mois et une année donnée en paramétre retourne la fin de mois
            string sMoisAdd = null;
            System.DateTime dtFinDeMois = default(System.DateTime);
            int i = 0;


            try
            {
                dtFinDeMois = Convert.ToDateTime("01/" + sMois + "/" + sAnnee);
                for (i = 0; i <= 32; i++)
                {
                    dtFinDeMois = dtFinDeMois.AddDays(1);
                    sMoisAdd = Convert.ToString(dtFinDeMois.Month);
                    //If UCase(sMoisAdd) <> UCase(sMois) Then
                    if (Convert.ToInt32(sMoisAdd) != Convert.ToInt32(sMois))
                    {
                        dtFinDeMois = dtFinDeMois.AddDays(-1);
                        functionReturnValue = dtFinDeMois;
                        break;
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
                return functionReturnValue;
            }
        }
        public static string fc_ValideDate(string sdate, bool blnHeures = false, bool blnOnlyHours = false)
        {
            string functionReturnValue = null;
            string sPartieDate = null;
            string sPartieHeure = null;
            string[] tabHeure = null;

            if (sdate == "__:__:__")
            {
                sdate = "00:00:00";
            }

            //    If blnHeures = False Then
            //        If IsNumeric(sDate) And Len(sDate) = 6 Then
            //            sDate = Left(sDate, 2) & "/" & Mid(sDate, 3, 2) & "/" & Right(sDate, 2)
            //        ElseIf IsNumeric(sDate) And Len(sDate) = 8 Then
            //            sDate = Left(sDate, 2) & "/" & Mid(sDate, 3, 2) & "/" & Right(sDate, 4)
            //        End If
            //    Else
            //        If IsNumeric(sDate) And Len(sDate) = 6 Then
            //            sDate = Left(sDate, 2) & "/" & Mid(sDate, 3, 2) & "/" & Right(sDate, 2)
            //        ElseIf IsNumeric(sDate) And Len(sDate) = 8 Then
            //            sDate = Left(sDate, 2) & "/" & Mid(sDate, 3, 2) & "/" & Right(sDate, 4)
            //        ElseIf InStr(1, sDate, " ") <> 0 Then
            //            sPartieDate = Left(sDate, InStr(1, sDate, " ") - 1)
            //            sPartieHeure = Right(sDate, Len(sDate) - InStr(1, sDate, " "))
            //            If IsNumeric(sPartieDate) Then
            //                If Len(sPartieDate) = 6 Then
            //                    sPartieDate = Left(sPartieDate, 2) & "/" & Mid(sPartieDate, 3, 2) & "/" & Right(sPartieDate, 2)
            //                ElseIf Len(sPartieDate) = 8 Then
            //                    sPartieDate = Left(sPartieDate, 2) & "/" & Mid(sPartieDate, 3, 2) & "/" & Right(sPartieDate, 4)
            //                End If
            //            End If
            //            If IsDate(sPartieDate) Then
            //                sPartieDate = Format(sPartieDate, "dd/mm/yyyy")
            //            End If
            //
            //            If sPartieHeure <> "" Then
            //                If IsNumeric(sPartieHeure) Then
            //                    If Len(sPartieHeure) = 2 Then
            //                        sPartieHeure = sPartieHeure & ":00:00"
            //                    ElseIf Len(sPartieHeure) = 4 Then
            //                        sPartieHeure = Left(sPartieHeure, 2) & ":" & Right(sPartieHeure, 2) & ":00"
            //                    ElseIf Len(sPartieHeure) = 6 Then
            //                        sPartieHeure = Left(sPartieHeure, 2) & ":" & Mid(sPartieHeure, 3, 2) & ":" & Right(sPartieHeure, 2)
            //                    End If
            //                End If
            //            End If
            //
            //            If IsDate(sPartieHeure) Then
            //                sDate = sPartieDate & " " & sPartieHeure
            //            Else
            //                sDate = sPartieDate
            //            End If
            //
            //        End If
            //    End If


            if (blnOnlyHours == false)
            {

                if (blnHeures == false)
                {
                    if (General.IsNumeric(sdate) && General.Len(sdate) == 6)
                    {
                        sdate = General.Left(sdate, 2) + "/" + General.Mid(sdate, 3, 2) + "/" + General.Right(sdate, 2);
                    }
                    else if (General.IsNumeric(sdate) & General.Len(sdate) == 8)
                    {
                        sdate = General.Left(sdate, 2) + "/" + General.Mid(sdate, 3, 2) + "/" + General.Right(sdate, 4);
                    }
                }
                else
                {
                    if (General.IsNumeric(sdate) && General.Len(sdate) == 6)
                    {
                        sdate = General.Left(sdate, 2) + "/" + General.Mid(sdate, 3, 2) + "/" + General.Right(sdate, 2);
                    }
                    else if (General.IsNumeric(sdate) && General.Len(sdate) == 8)
                    {
                        sdate = General.Left(sdate, 2) + "/" + General.Mid(sdate, 3, 2) + "/" + General.Right(sdate, 4);
                    }
                    else if (sdate.IndexOf(" ") != 0)
                    {
                        sPartieDate = General.Left(sdate, sdate.IndexOf(" ") - 1);

                        sPartieHeure = General.Right(sdate, General.Len(sdate) - sdate.IndexOf(" "));

                        if (General.IsNumeric(sPartieDate))
                        {
                            if (General.Len(sPartieDate) == 6)
                            {
                                sPartieDate = General.Left(sPartieDate, 2) + "/" + General.Mid(sPartieDate, 3, 2) + "/" + General.Right(sPartieDate, 2);
                            }
                            else if (General.Len(sPartieDate) == 8)
                            {
                                sPartieDate = General.Left(sPartieDate, 2) + "/" + General.Mid(sPartieDate, 3, 2) + "/" + General.Right(sPartieDate, 4);
                            }
                        }
                        if (General.IsDate(sPartieDate))
                        {
                            sPartieDate = Convert.ToDateTime(sPartieDate).ToString("dd/MM/yyyy");
                        }

                        if (!string.IsNullOrEmpty(sPartieHeure))
                        {
                            if (General.IsNumeric(sPartieHeure))
                            {
                                if (General.Len(sPartieHeure) == 2)
                                {
                                    sPartieHeure = sPartieHeure + ":00:00";
                                }
                                else if (General.Len(sPartieHeure) == 4)
                                {
                                    sPartieHeure = General.Left(sPartieHeure, 2) + ":" + General.Right(sPartieHeure, 2) + ":00";
                                }
                                else if (General.Len(sPartieHeure) == 6)
                                {
                                    sPartieHeure = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":" + General.Right(sPartieHeure, 2);
                                }
                            }
                        }

                        if (General.IsDate(sPartieHeure))
                        {
                            sdate = sPartieDate + " " + sPartieHeure;
                        }
                        else
                        {
                            sdate = sPartieDate;
                        }

                    }
                }

            }
            else
            {
                if (General.IsNumeric(sdate))
                {
                    sPartieHeure = sdate;
                    switch (General.Len(sPartieHeure))
                    {
                        case 1:
                            if (Convert.ToInt16(sPartieHeure) > 2)
                            {
                                sdate = "0" + sPartieHeure + ":00:00";
                            }
                            else
                            {
                                sdate = sPartieHeure + "0:00:00";
                            }
                            break;
                        case 2:
                            sdate = sPartieHeure + ":00:00";
                            break;
                        case 3:
                            sdate = General.Left(sPartieHeure, 2);
                            if (Convert.ToInt16(General.Right(sPartieHeure, 1)) > 2)
                            {
                                sdate = sdate + ":0" + sPartieHeure + ":00";
                            }
                            else
                            {
                                sdate = sdate + ":" + sPartieHeure + "0:00";
                            }
                            break;
                        case 4:
                            sdate = General.Left(sPartieHeure, 2) + ":" + General.Right(sPartieHeure, 2) + ":00";
                            break;
                        case 5:
                            sdate = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":";
                            if (Convert.ToInt16(General.Right(sPartieHeure, 1)) > 2)
                            {
                                sdate = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":0" + General.Right(sPartieHeure, 1);
                            }
                            else
                            {
                                sdate = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":" + General.Right(sPartieHeure, 1) + "0";
                            }
                            break;
                        case 6:
                            sdate = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":" + General.Right(sPartieHeure, 2);
                            break;
                        default:
                            sdate = General.Left(sPartieHeure, 2) + ":" + General.Mid(sPartieHeure, 3, 2) + ":" + General.Mid(sPartieHeure, 5, 2);
                            break;
                    }
                }
                else
                {
                    sPartieHeure = sdate;
                    if (sPartieHeure.Contains(":"))
                    {
                        sPartieHeure = General.Replace(sPartieHeure, "_", "");
                        if (General.IsNumeric(General.Replace(sPartieHeure, ":", "")))
                        {
                            tabHeure = sPartieHeure.Split(':');

                            if (tabHeure.Length == 0)
                            {
                                sdate = Convert.ToDateTime(General.nz(tabHeure[0], 0)).Hour.ToString("00") + ":00:00";
                            }
                            else if (tabHeure.Length == 1)
                            {

                                sdate = Convert.ToDateTime(General.nz(tabHeure[0], 0)).Hour.ToString("00") + ":" + Convert.ToDateTime(General.nz(tabHeure[1], 0)).Hour.ToString("00") + ":00";
                            }
                            else if (tabHeure.Length == 2)
                            {

                                sdate = Convert.ToDateTime(General.nz(tabHeure[0], 0)).Hour.ToString("00") + ":" + Convert.ToDateTime(General.nz(tabHeure[1], 0)).Hour.ToString("00")
                                    + ":" + Convert.ToDateTime(General.nz(tabHeure[2], 0)).Hour.ToString("00");
                            }
                            else
                            {

                                sdate = Convert.ToDateTime(General.nz(tabHeure[0], 0)).Hour.ToString("00") + ":" + Convert.ToDateTime(General.nz(tabHeure[1], 0)).Hour.ToString("00")
                                    + ":" + Convert.ToDateTime(General.nz(tabHeure[2], 0)).Hour.ToString("00");
                            }
                        }
                        else
                        {
                            sdate = sPartieHeure;
                        }
                    }
                    else
                    {
                        sdate = sPartieHeure;
                    }
                }
            }

            if (General.IsDate(sdate))
            {
                if (blnOnlyHours == true)
                {
                    functionReturnValue = Convert.ToDateTime(sdate).ToString("HH:mm:ss");
                }
                else
                {
                    if (blnHeures == false)
                    {
                        functionReturnValue = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy");
                    }
                    else
                    {
                        functionReturnValue = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy HH:mm:ss");
                    }
                }
            }
            else if (!string.IsNullOrEmpty(sdate))
            {
                if (blnOnlyHours == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date valide.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une heure valide.", "Heure non valide", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                functionReturnValue = "";
            }



            //    If IsDate(sDate) Then
            //        If blnHeures = False Then
            //            fc_ValideDate = Format(sDate, "dd/mm/yyyy")
            //        Else
            //            fc_ValideDate = Format(sDate, "dd/mm/yyyy hh:mm:ss")
            //        End If
            //    ElseIf sDate <> "" Then
            //        MsgBox "Vous devez saisir une date valide.", vbInformation, "Date non valide"
            //        fc_ValideDate = ""
            //    End If

            System.Windows.Forms.Application.DoEvents();
            return functionReturnValue;

        }
        public static double fc_GetMinFromHourCentesimales(double dbDuree)
        {
            //=== retourne les minutes à partir d'une durée centisémales, exemple 1,50(= 1h 30) retourne 90 minutes.
            //=== pour info
            //===    15 minutes = 25
            //===    30 minutes = 50
            //===    60 minutes = 100
            double dbTemp;
            string stemp;
            int i;
            double lNumLeft;
            double lNumRight = 0;
            double lTot;
            try
            {
                //=== recherche si il existe un séparateur.
                stemp = Convert.ToString(dbDuree);
                if (stemp.Contains("."))
                {
                    //===> Mondir le 09.02.2021 https://groupe-dt.mantishub.io/view.php?id=2193 commented line is old
                    stemp = dbDuree.ToString("0.00");
                    //stemp = string.Format(dbDuree.ToString(), "0.00");

                    i = stemp.IndexOf('.');
                    //'=== extrait les nombres de gauches.
                    lNumLeft = General.Left(stemp, i).ToDouble();
                    //lNumLeft = General.Left(stemp, i - 1).Length;

                    //'=== extrait les nombres de droite.
                    lNumRight = General.Right(stemp, stemp.Length - 2).ToDouble();
                    //lNumRight = General.Right(stemp, stemp.Length - i).Length;
                    //===> Fin Modif Mondir

                }
                else
                {
                    lNumLeft = Convert.ToInt32(dbDuree);
                }
                //'=== convertit en minutes.
                lNumLeft = lNumLeft * 60;
                lNumRight = General.FncArrondir((lNumRight / 100) * 60, 2);
                lTot = lNumLeft + lNumRight;
                return lTot;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fDate;fc_GetMinFromHourCentesimales");
                //throw;
                return 0;
            }
        }
        public static System.DateTime fc_ReturnDayPrevious(System.DateTime dtDate, string sDay)
        {
            System.DateTime functionReturnValue = default(System.DateTime);
            //=== retourne la date du jour souhaitée precendent ou égale à la date envoyé en paraméte.
            //=== exemple dtdate = 16/01/2018, sDay = Lundi retourne 15/01/2018.
            //=== attention dans cette fonction la fonction weekday considere que le dimache est le premier jour de la semaine.

            System.DateTime dtDateReturn = default(System.DateTime);
            int lWeekDay = 0;
            int lWeekDaySearch = 0;


            try
            {
                lWeekDay = (int)dtDate.DayOfWeek;

                dtDateReturn = dtDate;

                if (General.UCase(sDay) == General.UCase(modP2.cSDimanche))
                    lWeekDaySearch = modP2.cDimanche;

                if (General.UCase(sDay) == General.UCase(modP2.cSLundi))
                    lWeekDaySearch = modP2.cLundi;

                if (General.UCase(sDay) == General.UCase(modP2.cSMardi))
                    lWeekDaySearch = modP2.cMardi;

                if (General.UCase(sDay) == General.UCase(modP2.cSMercredi))
                    lWeekDaySearch = modP2.cMercredi;

                if (General.UCase(sDay) == General.UCase(modP2.cSJeudi))
                    lWeekDaySearch = modP2.cJeudi;

                if (General.UCase(sDay) == General.UCase(modP2.cSVendredi))
                    lWeekDaySearch = modP2.cVendredi;

                if (General.UCase(sDay) == General.UCase(modP2.cSSamedi))
                    lWeekDaySearch = modP2.cSamedi;



                do
                {
                    if (lWeekDay == lWeekDaySearch)
                    {
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                    else
                    {
                        dtDateReturn = dtDateReturn.AddDays(-1);
                        lWeekDay = (int)dtDateReturn.DayOfWeek;
                    }
                } while (true);

                functionReturnValue = dtDateReturn;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fDate;fc_ReturnDayPrevious");
                return functionReturnValue;
            }


        }
        public static System.DateTime fc_ReturnDayNext(System.DateTime dtDate, string sDay)
        {
            System.DateTime functionReturnValue = default(System.DateTime);
            //=== retourne la date du jour souhaitée prochain ou égale à la date envoyé en paraméte.
            //=== exemple dtdate = 16/01/2018, sDay = Dimanche retourne 21/01/2018.
            //=== attention dans cette fonction la fonction weekday considere que le dimache est le premier jour de la semaine.

            System.DateTime dtDateReturn = default(System.DateTime);
            int lWeekDay = 0;
            int lWeekDaySearch = 0;


            try
            {
                //lWeekDay = DateAndTime.WeekDay(dtDate, FirstDayOfWeek.Sunday);
                lWeekDay = Convert.ToInt32(dtDate.DayOfWeek);
                dtDateReturn = dtDate;

                if (General.UCase(sDay) == General.UCase(modP2.cSDimanche))
                    lWeekDaySearch = modP2.cDimanche;

                if (General.UCase(sDay) == General.UCase(modP2.cSLundi))
                    lWeekDaySearch = modP2.cLundi;

                if (General.UCase(sDay) == General.UCase(modP2.cSMardi))
                    lWeekDaySearch = modP2.cMardi;

                if (General.UCase(sDay) == General.UCase(modP2.cSMercredi))
                    lWeekDaySearch = modP2.cMercredi;

                if (General.UCase(sDay) == General.UCase(modP2.cSJeudi))
                    lWeekDaySearch = modP2.cJeudi;

                if (General.UCase(sDay) == General.UCase(modP2.cSVendredi))
                    lWeekDaySearch = modP2.cVendredi;

                if (General.UCase(sDay) == General.UCase(modP2.cSSamedi))
                    lWeekDaySearch = modP2.cSamedi;


                do
                {
                    if (lWeekDay == lWeekDaySearch)
                    {
                        break;
                    }
                    else
                    {
                        //dtDateReturn = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Day, 1, dtDateReturn);
                        dtDateReturn = dtDateReturn.AddDays(1);
                        lWeekDay = Convert.ToInt32(dtDateReturn.DayOfWeek);
                        //lWeekDay = DateAndTime.WeekDay(dtDateReturn, FirstDayOfWeek.Sunday);
                    }
                } while (!(lWeekDay == lWeekDaySearch));

                functionReturnValue = dtDateReturn;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fDate;fc_ReturnDayNext");
                return functionReturnValue;
            }
        }

        public static bool IsValidMonthAndYear(int month, int year)
        {
            if (year < 1 || year > 9999) { return false; }
            if (month < 1 || month > 12) { return false; }

            return true;
        }

        public static double fc_GetSecondToHourBase100(double dbSecond)
        {
            double functionReturnValue = 0;
            double dbMin = 0;
            try
            {
                //'== convertit en minutes
                dbMin = dbSecond / 60;

                dbMin = (int)(dbMin);

                //'=== convertit en min base 100, regle de trois.
                dbMin = (dbMin * 100) / 60;

                //'=== convertit en h base 100.
                dbMin = dbMin / 100;

                functionReturnValue = General.FncArrondir(dbMin, 2);
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fDate;fc_ReturnDayNext");
                return functionReturnValue;
            }
        }
    }
}
