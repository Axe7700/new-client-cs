﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace Axe_interDT.Shared
{
    class modSaveDevis
    {
        public static void fc_SaveDevis(string sNodevis)
        {
            string sSQL = null;
            string sDB = null;
            string sDBup = null;
            string sUser = null;
            SqlConnection adoBUp;
            DateTime dtDate;
            int i = 0;
            DataTable rs = new DataTable();
            ModAdo modAdors = new ModAdo();

            try
            {
                if (General.UCase(General.fncUserName()) == General.UCase("rachid abbouchi") && DateTime.Now.ToShortDateString() == "23/07/2020")
                    return;


                sDB = General.adocnn.Database;
                sUser = General.fncUserName();
                dtDate = DateTime.Now;
                sDBup = "AXECIEL_BACKUP";
                if (General._ExecutionMode == General.ExecutionMode.Dev)
                    adoBUp = new SqlConnection($"SERVER =.; DATABASE = {sDBup}; INTEGRATED SECURITY = Yes;");
                else
                    adoBUp = new SqlConnection($"SERVER = {General.DefaultServer}; DATABASE = {sDBup}; INTEGRATED SECURITY = Yes;");

                //if(sUser.ToLower().Contains("mondir"))
                //    adoBUp = new SqlConnection($"SERVER =.\\SQL_AXECIEL; DATABASE = {sDBup}; INTEGRATED SECURITY = Yes;");

                adoBUp.Open();

                sSQL = "SELECT  " + sDBup + ".dbo.DevisEnTete.BaseDeDonnee," + sDBup + ".dbo.DevisEnTete.DateSave," + sDBup + ".dbo.DevisEnTete.NumeroDevis"
                    + " From " + sDBup + ".dbo.DevisEnTete "
                    + " WHERE     " + sDBup + ".dbo.DevisEnTete.BaseDeDonnee = '" + sDB + "'"
                    + " AND " + sDBup + ".dbo.DevisEnTete.DateSave = '" + dtDate + "' "
                    + " AND " + sDBup + ".dbo.DevisEnTete.NumeroDevis = '" + sNodevis + "'";

                rs = modAdors.fc_OpenRecordSet(sSQL, null, "", adoBUp);
                if (rs.Rows.Count > 0)
                {
                    modAdors.Dispose();
                    rs = null;
                    adoBUp.Close();
                    adoBUp = null;
                    return;
                }
                modAdors.Dispose();
                rs = null;
                adoBUp.Close();
                adoBUp = null;
                sSQL = "INSERT INTO " + sDBup + ".dbo.DevisEnTete"
                        + " (" + sDBup + ".dbo.DevisEnTete.BaseDeDonnee, " + sDBup + ".dbo.DevisEnTete.Utilisateur, " + sDBup + ".dbo.DevisEnTete.DateSave, " + sDBup + ".dbo.DevisEnTete.NumeroDevis, " + sDBup + ".dbo.DevisEnTete.CodeEtat, "
                        + sDBup + ".dbo.DevisEnTete.Annee, " + sDBup + ".dbo.DevisEnTete.Mois, " + sDBup + ".dbo.DevisEnTete.Ordre, " + sDBup + ".dbo.DevisEnTete.TitreDevis, " + sDBup + ".dbo.DevisEnTete.DateCreation, " + sDBup + ".dbo.DevisEnTete.NumFicheStandard, " + sDBup + ".dbo.CodeDeviseur, "
                        + " " + sDBup + ".dbo.DevisEnTete.CodeCopieA, " + sDBup + ".dbo.DevisEnTete.Observations, " + sDBup + ".dbo.DevisEnTete.DateAcceptation, " + sDBup + ".dbo.DevisEnTete.CodePresentation, "
                        + sDBup + ".dbo.DevisEnTete.NbreExemplaire, " + sDBup + ".dbo.DevisEnTete.AdrEnvoi1, " + sDBup + ".dbo.DevisEnTete.AdrEnvoi2, " + sDBup + ".dbo.DevisEnTete.CPEnvoi, " + sDBup + ".dbo.DevisEnTete.VilleEnvoi, "
                        + sDBup + ".dbo.DevisEnTete.DateMAJ, " + sDBup + ".dbo.DevisEnTete.TypeEnvoi, "
                        + " " + sDBup + ".dbo.DevisEnTete.NbreDeplacement, " + sDBup + ".dbo.DevisEnTete.PrixDeplacement, " + sDBup + ".dbo.DevisEnTete.CodeTVA, " + sDBup + ".dbo.DevisEnTete.TotalHT, " + sDBup + ".dbo.DevisEnTete.TotalTVA, "
                        + sDBup + ".dbo.DevisEnTete.TotalTTC, " + sDBup + ".dbo.DevisEnTete.TotalDeplacement, " + sDBup + ".dbo.DevisEnTete.kDivers, " + sDBup + ".dbo.DevisEnTete.TypePaiement, " + sDBup + ".dbo.DevisEnTete.EnteteDevis, "
                        + " " + sDBup + ".dbo.DevisEnTete.ModifApresAccord, " + sDBup + ".dbo.DevisEnTete.CodeImmeuble, " + sDBup + ".dbo.DevisEnTete.Codetitre, " + sDBup + ".dbo.DevisEnTete.ResponsableTravaux, "
                        + sDBup + ".dbo.DevisEnTete.DateImpression, " + sDBup + ".dbo.DevisEnTete.IntervenantCreat, " + sDBup + ".dbo.DevisEnTete.IntervenantModif, " + sDBup + ".dbo.DevisEnTete.DEV_CoefMOdefaut, "
                        + " " + sDBup + ".dbo.DevisEnTete.DEV_CoefFOdefaut1, " + sDBup + ".dbo.DevisEnTete.DEV_CoefSTdefaut2, " + sDBup + ".dbo.DevisEnTete.DEV_CoefPrixdefaut2, " + sDBup + ".dbo.DevisEnTete.DEV_LIbelleClient, "
                        + sDBup + ".dbo.DevisEnTete.DossierDevis, " + sDBup + ".dbo.DevisEnTete.Commentaire, " + sDBup + ".dbo.DevisEnTete.ExportPDF, " + sDBup + ".dbo.DevisEnTete.Cloture, " + sDBup + ".dbo.DevisEnTete.TotalAvantCoef, "
                        + " " + sDBup + ".dbo.DevisEnTete.TotalMO, " + sDBup + ".dbo.DevisEnTete.TotalFO, " + sDBup + ".dbo.DevisEnTete.TotalST, " + sDBup + ".dbo.DevisEnTete.VerifAchat, " + sDBup + ".dbo.DevisEnTete.CodeBE, "
                        + sDBup + ".dbo.DevisEnTete.TextePaiement, " + sDBup + ".dbo.DevisEnTete.AvctChantier, " + sDBup + ".dbo.DevisEnTete.AvctFacturation, " + sDBup + ".dbo.DevisEnTete.AvctReglement, " + sDBup + ".dbo.DevisEnTete.ConditionPaiement, " + sDBup + ".dbo.DevisEnTete.Situation, " + sDBup + ".dbo.DevisEnTete.Acompte, "
                        + " " + sDBup + ".dbo.DevisEnTete.DelaisReglmt, " + sDBup + ".dbo.DevisEnTete.TypeDevis, " + sDBup + ".dbo.DevisEnTete.CoefArchi, " + sDBup + ".dbo.DevisEnTete.RenovationChauff, " + sDBup + ".dbo.DevisEnTete.fichier, "
                        + sDBup + ".dbo.DevisEnTete.AfficheNoDevis, " + sDBup + ".dbo.DevisEnTete.DateNodevis, " + sDBup + ".dbo.DevisEnTete.NoEnregistrement, " + sDBup + ".dbo.DevisEnTete.DateMajCodeimmeuble, "
                        + " " + sDBup + ".dbo.DevisEnTete.Receptionne, " + sDBup + ".dbo.DevisEnTete.ComSurRecept, " + sDBup + ".dbo.DevisEnTete.ClotureDevis, " + sDBup + ".dbo.DevisEnTete.ComSurCloture, " + sDBup + ".dbo.DevisEnTete.DateReception, "
                        + sDBup + ".dbo.DevisEnTete.ReceptionnePar, " + sDBup + ".dbo.DevisEnTete.ClotureLe, " + sDBup + ".dbo.DevisEnTete.AffichePart, " + sDBup + ".dbo.DevisEnTete.TelPart, " + sDBup + ".dbo.DevisEnTete.EmailPart, " + sDBup + ".dbo.DevisEnTete.CLoturerPar, "
                        + " " + sDBup + ".dbo.DevisEnTete.CodeReglement, " + sDBup + ".dbo.DevisEnTete.AcompteRecuLe, " + sDBup + ".dbo.TYD_Code, " + sDBup + ".dbo.DevisEnTete.CoefMontantSouhaite, " + sDBup + ".dbo.DevisEnTete.MontantSouhaite) ";




                sSQL = sSQL + " SELECT   '" + sDB + "' as BaseDeDonnee, '" + sUser + "' as Utilisateur, '" + dtDate + "' as DateSav, " + sDB + ".dbo.DevisEnTete.NumeroDevis, " + sDB + ".dbo.DevisEnTete.CodeEtat, " + sDB + ".dbo.DevisEnTete.Annee, " + sDB + ".dbo.DevisEnTete.Mois, " + sDB + ".dbo.DevisEnTete.Ordre, "
                        + sDB + ".dbo.DevisEnTete.TitreDevis, " + sDB + ".dbo.DevisEnTete.DateCreation, " + sDB + ".dbo.DevisEnTete.NumFicheStandard, " + sDB + ".dbo.DevisEnTete.CodeDeviseur,"
                        + " " + sDB + ".dbo.DevisEnTete.CodeCopieA, " + sDB + ".dbo.DevisEnTete.Observations, " + sDB + ".dbo.DevisEnTete.DateAcceptation, " + sDB + ".dbo.DevisEnTete.CodePresentation, "
                        + sDB + ".dbo.DevisEnTete.NbreExemplaire, " + sDB + ".dbo.DevisEnTete.AdrEnvoi1, " + sDB + ".dbo.DevisEnTete.AdrEnvoi2, " + sDB + ".dbo.DevisEnTete.CPEnvoi, " + sDB + ".dbo.DevisEnTete.VilleEnvoi, " + sDB + ".dbo.DevisEnTete.DateMAJ, " + sDB + ".dbo.DevisEnTete.TypeEnvoi, "
                        + " " + sDB + ".dbo.DevisEnTete.NbreDeplacement, " + sDB + ".dbo.DevisEnTete.PrixDeplacement, " + sDB + ".dbo.DevisEnTete.CodeTVA, " + sDB + ".dbo.DevisEnTete.TotalHT, " + sDB + ".dbo.DevisEnTete.TotalTVA, "
                        + sDB + ".dbo.DevisEnTete.TotalTTC, " + sDB + ".dbo.DevisEnTete.TotalDeplacement, " + sDB + ".dbo.DevisEnTete.kDivers, " + sDB + ".dbo.DevisEnTete.TypePaiement, " + sDB + ".dbo.DevisEnTete.EnteteDevis, "
                        + " " + sDB + ".dbo.DevisEnTete.ModifApresAccord, " + sDB + ".dbo.DevisEnTete.CodeImmeuble, " + sDB + ".dbo.DevisEnTete.Codetitre, " + sDB + ".dbo.DevisEnTete.ResponsableTravaux, "
                        + sDB + ".dbo.DevisEnTete.DateImpression, " + sDB + ".dbo.DevisEnTete.IntervenantCreat, " + sDB + ".dbo.DevisEnTete.IntervenantModif, " + sDB + ".dbo.DevisEnTete.DEV_CoefMOdefaut, "
                        + " " + sDB + ".dbo.DevisEnTete.DEV_CoefFOdefaut1, " + sDB + ".dbo.DevisEnTete.DEV_CoefSTdefaut2, " + sDB + ".dbo.DevisEnTete.DEV_CoefPrixdefaut2, " + sDB + ".dbo.DevisEnTete.DEV_LIbelleClient, "
                        + sDB + ".dbo.DevisEnTete.DossierDevis, " + sDB + ".dbo.DevisEnTete.Commentaire, " + sDB + ".dbo.DevisEnTete.ExportPDF, " + sDB + ".dbo.DevisEnTete.Cloture, " + sDB + ".dbo.DevisEnTete.TotalAvantCoef,"
                        + " " + sDB + ".dbo.DevisEnTete.TotalMO, " + sDB + ".dbo.DevisEnTete.TotalFO, " + sDB + ".dbo.DevisEnTete.TotalST, " + sDB + ".dbo.DevisEnTete.VerifAchat, " + sDB + ".dbo.DevisEnTete.CodeBE, "
                        + sDB + ".dbo.DevisEnTete.TextePaiement, " + sDB + ".dbo.DevisEnTete.AvctChantier, " + sDB + ".dbo.DevisEnTete.AvctFacturation, " + sDB + ".dbo.DevisEnTete.AvctReglement, " + sDB + ".dbo.DevisEnTete.ConditionPaiement, " + sDB + ".dbo.DevisEnTete.Situation, " + sDB + ".dbo.DevisEnTete.Acompte, "
                        + " " + sDB + ".dbo.DevisEnTete.DelaisReglmt, " + sDB + ".dbo.DevisEnTete.TypeDevis, " + sDB + ".dbo.DevisEnTete.CoefArchi, " + sDB + ".dbo.DevisEnTete.RenovationChauff, "
                        + sDB + ".dbo.DevisEnTete.fichier, " + sDB + ".dbo.DevisEnTete.AfficheNoDevis, " + sDB + ".dbo.DevisEnTete.DateNodevis, " + sDB + ".dbo.DevisEnTete.NoEnregistrement, " + sDB + ".dbo.DevisEnTete.DateMajCodeimmeuble, "
                        + " " + sDB + ".dbo.DevisEnTete.Receptionne, " + sDB + ".dbo.DevisEnTete.ComSurRecept, " + sDB + ".dbo.DevisEnTete.ClotureDevis, " + sDB + ".dbo.DevisEnTete.ComSurCloture, "
                        + sDB + ".dbo.DevisEnTete.DateReception, " + sDB + ".dbo.DevisEnTete.ReceptionnePar, " + sDB + ".dbo.DevisEnTete.ClotureLe, " + sDB + ".dbo.DevisEnTete.AffichePart, " + sDB + ".dbo.DevisEnTete.TelPart, " + sDB + ".dbo.DevisEnTete.EmailPart, " + sDB + ".dbo.DevisEnTete.CLoturerPar, "
                        + " " + sDB + ".dbo.DevisEnTete.CodeReglement , " + sDB + ".dbo.DevisEnTete.AcompteRecuLe, " + sDB + ".dbo.DevisEnTete.TYD_Code, " + sDB + ".dbo.DevisEnTete.CoefMontantSouhaite, " + sDB + ".dbo.DevisEnTete.MontantSouhaite "
                        + " FROM         " + sDB + ".dbo.DevisEnTete WHERE " + sDB + ".dbo.DevisEnTete.NumeroDevis ='" + sNodevis + "'";

                General.Execute(sSQL);

                sSQL = "INSERT INTO " + sDBup + ".dbo.DevisDetail "
                            + " (" + sDBup + ".dbo.DevisDetail.BaseDeDonnee, " + sDBup + ".dbo.DevisDetail.Utilisateur, " + sDBup + ".dbo.DevisDetail.DateSave, " + sDBup + ".dbo.DevisDetail.NumeroDevis, " + sDBup + ".dbo.DevisDetail.NumeroLigne, " + sDBup + ".dbo.DevisDetail.CodeFamille, "
                            + sDBup + ".dbo.DevisDetail.CodeSousFamille, " + sDBup + ".dbo.DevisDetail.CodeArticle, " + sDBup + ".dbo.DevisDetail.CodeSousArticle, " + sDBup + ".dbo.DevisDetail.NumOrdre, "
                            + " " + sDBup + ".dbo.DevisDetail.TexteLigne, " + sDBup + ".dbo.DevisDetail.Quantite, " + sDBup + ".dbo.DevisDetail.MOhud, " + sDBup + ".dbo.DevisDetail.PxHeured, " + sDBup + ".dbo.DevisDetail.FOud, "
                            + sDBup + ".dbo.DevisDetail.STud, " + sDBup + ".dbo.DevisDetail.MOhd, " + sDBup + ".dbo.DevisDetail.MOd, " + sDBup + ".dbo.DevisDetail.FOd, " + sDBup + ".dbo.DevisDetail.STd, " + sDBup + ".dbo.DevisDetail.TotalDebours, "
                            + sDBup + ".dbo.DevisDetail.kMO, " + sDBup + ".dbo.DevisDetail.MO, " + sDBup + ".dbo.DevisDetail.kFO, " + sDBup + ".dbo.DevisDetail.FO, " + sDBup + ".dbo.DevisDetail.kST, " + sDBup + ".dbo.DevisDetail.ST, "
                            + sDBup + ".dbo.DevisDetail.TotalVente, " + sDBup + ".dbo.DevisDetail.Coef1, " + sDBup + ".dbo.DevisDetail.Coef2, "
                            + " " + sDBup + ".dbo.DevisDetail.Coef3, " + sDBup + ".dbo.DevisDetail.TotalVenteApresCoef, " + sDBup + ".dbo.DevisDetail.CodeTVA, " + sDBup + ".dbo.DevisDetail.Facturer, " + sDBup + ".dbo.DevisDetail.DateFacture, "
                            + sDBup + ".dbo.DevisDetail.NFamille, " + sDBup + ".dbo.DevisDetail.NSFamille, " + sDBup + ".dbo.DevisDetail.NArticle, " + sDBup + ".dbo.DevisDetail.NSousArticle, " + sDBup + ".dbo.DevisDetail.Fournisseur, "
                            + sDBup + ".dbo.DevisDetail.CodeMO, " + sDBup + ".dbo.DevisDetail.Unite, " + sDBup + ".dbo.DevisDetail.SautPage ) ";

                sSQL = sSQL + " SELECT     '" + sDB + "' as BaseDeDonnee, '" + sUser + "' as Utilisateur, '" + dtDate + "' as DateSav, "
                            + sDB + ".dbo.DevisDetail.NumeroDevis, " + sDB + ".dbo.DevisDetail.NumeroLigne, " + sDB + ".dbo.DevisDetail.CodeFamille, " + sDB + ".dbo.DevisDetail.CodeSousFamille, " + sDB + ".dbo.DevisDetail.CodeArticle, " + sDB + ".dbo.DevisDetail.CodeSousArticle, " + sDB + ".dbo.DevisDetail.NumOrdre, "
                            + " " + sDB + ".dbo.DevisDetail.TexteLigne, " + sDB + ".dbo.DevisDetail.Quantite, " + sDB + ".dbo.DevisDetail.MOhud, " + sDB + ".dbo.DevisDetail.PxHeured, " + sDB + ".dbo.DevisDetail.FOud, " + sDB + ".dbo.DevisDetail.STud, "
                            + sDB + ".dbo.DevisDetail.MOhd, " + sDB + ".dbo.DevisDetail.MOd, " + sDB + ".dbo.DevisDetail.FOd, " + sDB + ".dbo.DevisDetail.STd, " + sDB + ".dbo.DevisDetail.TotalDebours, " + sDB + ".dbo.DevisDetail.kMO, "
                            + sDB + ".dbo.DevisDetail.MO, " + sDB + ".dbo.DevisDetail.kFO, " + sDB + ".dbo.DevisDetail.FO, " + sDB + ".dbo.DevisDetail.kST, " + sDB + ".dbo.DevisDetail.ST, " + sDB + ".dbo.DevisDetail.TotalVente, "
                            + sDB + ".dbo.DevisDetail.Coef1, " + sDB + ".dbo.DevisDetail.Coef2, "
                            + " " + sDB + ".dbo.DevisDetail.Coef3, " + sDB + ".dbo.DevisDetail.TotalVenteApresCoef, " + sDB + ".dbo.DevisDetail.CodeTVA, " + sDB + ".dbo.DevisDetail.Facturer, " + sDB + ".dbo.DevisDetail.DateFacture, "
                            + sDB + ".dbo.DevisDetail.NFamille, " + sDB + ".dbo.DevisDetail.NSFamille, " + sDB + ".dbo.DevisDetail.NArticle, " + sDB + ".dbo.DevisDetail.NSousArticle, " + sDB + ".dbo.DevisDetail.Fournisseur, "
                            + sDB + ".dbo.DevisDetail.CodeMO, " + sDB + ".dbo.DevisDetail.Unite, "
                            + " " + sDB + ".dbo.DevisDetail.SautPage "
                            + " FROM         " + sDB + ".dbo.DevisDetail WHERE " + sDB + ".dbo.DevisDetail.NumeroDevis ='" + sNodevis + "'";
                General.Execute(sSQL);

                // oConn.Close
                //'Set oConn = Nothing
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModSaveDEvis;fc_SaveDevis");
            }
        }

    }
}
