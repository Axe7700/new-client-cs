﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    static class ModDiv
    {
        public static object fc_AlerteMailDevis(string sNoDevis)
        {
            object functionReturnValue = null;
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors;
            string sCode = null;
            string sCodeImm = null;
            string sSujet = null;
            string sMessage = null;

            try
            {
                sSQL = "SELECT     NumeroDevis, CodeDeviseur, codeimmeuble " + " from DevisEnTete " + " WHERE     NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNoDevis) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                ///===============> Tested
                if (rs.Rows.Count > 0)
                {
                    sCode = rs.Rows[0]["CodeDeviseur"] + "";
                    sCodeImm = rs.Rows[0]["CodeImmeuble"] + "";
                }
                else
                {
                    modAdors.Dispose();
                    return functionReturnValue;
                }

                sSQL = "SELECT     Personnel_1.Email, Personnel_1.Nom, Personnel_1.Prenom" + " FROM         Personnel INNER JOIN " + " Personnel AS Personnel_1 ON Personnel.RespService =" +
                       " Personnel_1.Initiales " + " WHERE     Personnel.Matricule = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(General.nz(rs.Rows[0]["Email"], "").ToString()))
                    {
                        sSujet = "Devis n°" + sNoDevis + " réceptionné, immeuble" + sCodeImm;

                        sMessage = "Bonjour M." + rs.Rows[0]["Nom"] + "," + "\n" + "\n";
                        sMessage = sMessage + "Le devis numéro " + sNoDevis + ", immeuble " + sCodeImm + " a été réceptionné," + "\n";
                        sMessage = sMessage + "merci de procéder à la clôture du devis.";
                        sMessage = sMessage + "\n";
                        sMessage = sMessage + "\n";
                        sMessage = sMessage + "Cet e-mail vous a été envoyé par un automate, merci de ne pas répondre.";

                        ModCourrier.CreateMail(rs.Rows[0]["Email"] + "", "", sSujet, sMessage);
                    }
                }

                modAdors.Dispose();
                rs = null;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "ModDiv;fc_AlerteMailDevis");
            }
            return functionReturnValue;
        }

        public static object fc_AlerteMailClotureDevis(string sNoDevis, string sCodeImmeuble)
        {
            object functionReturnValue = null;

            string sSujet = null;
            string sMessage = null;


            try
            {
                sSujet = "Devis n°" + sNoDevis + " réceptionné, immeuble" + sCodeImmeuble;

                sMessage = "Bonjour," + "\n\n";
                sMessage = sMessage + "-   Pour information le devis numéro " + sNoDevis + ", immeuble " + sCodeImmeuble + " a été clôturé." + "\n";

                sMessage = sMessage + "\n";
                // sMessage = sMessage & vbCrLf
                sMessage = sMessage + "Cet e-mail vous a été envoyé par un automate, merci de ne pas répondre.";

                ModCourrier.CreateMail("lm.gresle@groupe-dt.fr", "", sSujet, sMessage);

                ModCourrier.CreateMail("F.LEBORGNE@groupe-dt.fr", "", sSujet, sMessage);
                return functionReturnValue;
            }
            catch (Exception e)
            {

                Erreurs.gFr_debug(e, "ModDiv;fc_AlerteMailDevis");
                return functionReturnValue;
            }
        }
    }
}
