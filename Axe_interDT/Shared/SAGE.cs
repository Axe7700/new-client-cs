﻿using System;
using System.Data;
using System.Data.Odbc;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public struct SoldeClient
    {
        public double Credit;
        public double Debit;
        public double Solde;
    }

    public struct SoldeClientSearchParams
    {
        public string DateMini;
        public string DateMaxi;
        public int JourMaxiMois;
    }

    static class SAGE
    {
        public static string cODBC { get; set; }
        static DataTable rsSage;
        static SqlDataAdapter SDArsSage;
        public static SqlConnection adoSage { get; set; }
        static string sSQL;
        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_OpenConnSage()
        {
            var ConnectionString =
                $"Dsn={cODBC};" +
                "Trusted_Connection=Yes;";

            OdbcConnection con = new OdbcConnection(ConnectionString);

            con.Open();

            var db = con.Database;
            var server = con.DataSource;
            con.Close();

            // ouvre une connection avec sage. sOdbc contient le nom de l'odbc Sage
            adoSage = new SqlConnection($"SERVER = {server}; DATABASE = {db}; INTEGRATED SECURITY = Yes;");
            if (adoSage.State == ConnectionState.Closed)
                adoSage.Open();
            //    adoSage.Close
            //    Set adoSage = Nothing
        }
        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_CloseConnSage()
        {
            if (adoSage != null)
            {
                if (adoSage.State == ConnectionState.Open)
                {
                    adoSage.Close();
                    adoSage?.Dispose();
                }
            }
        }

        public static string fc_prcAjoutTiers(int lchoix, ref string sCompteTiers, ref string sintitule, ref string sClassement, string sAdresse, string sComplement,
             string sCP, string sVille, string sCT_Contact, string sCT_telephone = "",
            string sCT_fax = "", object sCT_Type = null, object sCT_Email = null, string sCodeTable = "", string sCodeImm = "", string sID = "", string codeImmeubleCurrent = "")
        {
            string functionReturnValue = null;
            object sNCompte = null;
            object sct_comp = null;

            bool bNew = false;
            string sCodeImmeuble = null;
            string strNumPrinc = null;
            string PresenceCpteAnalytique = null;
            short i = 0;
            int iLensNcompte = 0;
            int nAdjust = 0;
            int lngType = 0;
            DataTable rsBaseSage = default(DataTable);
            SqlDataAdapter SDArsBaseSage = default(SqlDataAdapter);
            string sCA_NUMcopro = null;
            DateTime dtDate = default(DateTime);

            const string c0 = "0";
            const string c1 = "1";
            const string c2 = "2";
            const string c3 = "3";

            int nbRecAff = 0;
            string sCtrlCtp = null;

            try
            {
                ///===============> Tested
                if (General.sDesActiveSage == "1")
                {
                    return functionReturnValue;
                    //MsgBox "Liaison comptabilité inexistante.."
                }
                Program.SaveException(null, "fc_prcAjoutTiers : params => choix=>" + lchoix + " // scomptiers => " + sCompteTiers + " // sinititule => " + sintitule + " // sClassement => " + sClassement + " // sAdresse => " + sAdresse + " // sComplement => " + sComplement + " // sCodeTable => " + sCodeTable + " // sCodeImm=> " + sCodeImm + " // txtCodeImmeuble => " + codeImmeubleCurrent);

                // Ouvre la connection SAGE.
                ////==================> Tested
                if (General.UtiliseSage.ToUpper() != "0")
                {
                    fc_OpenConnSage();
                }

                dtDate = DateTime.Now.Date;

                ///==================> Tested
                if (lchoix == 1)
                {
                    using (var tmpModAdo = new ModAdo())
                        sCodeImmeuble =
                            tmpModAdo.fc_ADOlibelle(
                                "SELECT ImmeublePart.CodeImmeuble FROM ImmeublePart WHERE CodeParticulier='" + StdSQLchaine.gFr_DoublerQuote(sCodeTable) + "' AND CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImm) + "'");
                }
                else
                {
                    sCodeImmeuble = sCompteTiers;
                }
                sintitule = General.Left(sintitule, 35);
                sClassement = General.Left(sClassement, 17);
                sct_comp = General.Left(GeneralXav.fct_LookSpace(sAdresse, 36, 71), 35);
                //sAdresse = General.Left(sAdresse, 35 - (sct_comp.ToString().Length - General.Mid(sAdresse, 36, 71).Length));
                sAdresse = General.Left(sAdresse, 35 - (sct_comp.ToString().Length - General.Mid(sAdresse, 36).Length));
                sComplement = General.Left(sComplement, 35);
                sCP = General.Left(sCP, 9);
                sVille = General.Left(sVille, 35);
                sCT_Contact = General.Left(sCT_Contact, 35);
                sCT_telephone = General.Left(sCT_telephone, 21);
                sCT_fax = General.Left(sCT_fax, 21);

                //sNCompte = Trim(Left(gFr_DoublerQuote(sNCompte), 13))
                if (sNCompte != null)
                    iLensNcompte = sNCompte.ToString().Length;

                //recherche de l'existence de ce code dans les comptes tiers SAGE
                if (General.UtiliseSage.ToUpper() != "0")
                {
                    if (lchoix == 0)
                        sSQL = "SELECT CT_Num From F_COMPTET WHERE CT_NUM='" + StdSQLchaine.gFr_DoublerQuote(fc_FormatCompte(sCompteTiers, 0)) + "'";
                    if (lchoix == 1)
                        sSQL = "SELECT CT_Num From F_COMPTET WHERE CT_NUM='" +
                               StdSQLchaine.gFr_DoublerQuote(fc_FormatCompte(sCompteTiers, 1)) + "'";
                    if (lchoix == 2)
                        sSQL = "SELECT CT_Num From F_COMPTET WHERE CT_Num='" +
                               StdSQLchaine.gFr_DoublerQuote(fc_FormatCompte(sCompteTiers, 2)) + "'";

                    bNew = false;
                    ///=======================> Tested
                    if (!string.IsNullOrEmpty(sID))
                    {
                        //=== modif du 19 02 2012, si pas de compte tiers lié à l'immeuble, on force la création du compte.
                        if (lchoix == 0)
                        {
                            sCtrlCtp = "SELECT NCompte FROM Immeuble WHERE  CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sID) + "'";
                            using (var tmpModAdo = new ModAdo())
                                sCtrlCtp = tmpModAdo.fc_ADOlibelle(sCtrlCtp);
                            if (string.IsNullOrEmpty(sCtrlCtp))
                            {
                                bNew = true;
                            }
                        }
                        else if (lchoix == 1)
                        {
                            sCtrlCtp = "SELECT NCompte FROM ImmeublePart WHERE  CodeImmeuble ='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCodeImm) + "' AND CodeParticulier ='" +
                                       StdSQLchaine.gFr_DoublerQuote(sID) + "'";
                            using (var tmpModAdo = new ModAdo())
                                sCtrlCtp = tmpModAdo.fc_ADOlibelle(sCtrlCtp);
                            if (string.IsNullOrEmpty(sCtrlCtp))
                            {
                                bNew = true;
                            }
                        }
                    }

                    //lchoix = 0 : Immeuble
                    //lchoix = 1 : Copropriétaire
                    //lchoix = 2 : Fournisseur

                    rsSage = new DataTable();
                    SDArsSage = new SqlDataAdapter(sSQL, adoSage);
                    SDArsSage.Fill(rsSage);

                    if (rsSage.Rows.Count == 0 || bNew)
                    {

                        // si blCient  = true alors c'est un compte client
                        // sinon comte tiers
                        //0 : immeuble et 1 : copro

                        //====================> Tested First
                        if (lchoix == 0 || lchoix == 1)
                        {
                            if (lchoix == 0)
                            {
                                sCompteTiers = fc_FormatCompte(sCompteTiers, 0);
                            }
                            else if (lchoix == 1)
                            {
                                sCompteTiers = fc_FormatCompte(sCompteTiers, 1);
                            }
                            lngType = 0;

                            if (!string.IsNullOrEmpty(General.sCptGeneralClient))
                            {
                                strNumPrinc = General.sCptGeneralClient;
                            }
                            else
                            {
                                strNumPrinc = "41100000";
                            }
                            if (lchoix == 1)
                            {
                                lngType = 0;
                                //                strNumPrinc = "40100000"
                            }

                            sCompteTiers = fc_RechercheSiDoublon(sCompteTiers);

                            General.SQL = "INSERT INTO F_COMPTET (CT_Num,CT_NumPayeur,CT_Intitule, " +
                                          "CT_Type,CG_NumPrinc, CT_Classement," +
                                          "CT_Adresse, CT_Complement, CT_CodePostal, CT_Ville, CT_Contact," +
                                          "N_Risque, N_Cattarif, N_Catcompta, N_Period,N_Expedition, " +
                                          "N_Condition, CT_Facture, " + " CT_SAUT,CT_Sommeil, CT_DateCreate, CT_Prospect )" +
                                          "VALUES (" + "'" + StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "','" +
                                          StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sintitule) + "'," + lngType + ", " + "'" +
                                          strNumPrinc + "', " + "'" + StdSQLchaine.gFr_DoublerQuote(sClassement) +
                                          "', " + "'" + StdSQLchaine.gFr_DoublerQuote(sAdresse) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sComplement) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCP) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sVille) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Contact) + "', " + "" + c1 + "," + c1 +
                                          "," + c1 + "," + c1 + "," + c1 + "," + c1 + "," + c1 + ", " + "1,0 ,'" +
                                          dtDate.ToString(General.FormatDateSansHeureSQL) + "','" + 0 + "')";

                            var SCM = new SqlCommand(General.SQL, adoSage);
                            if (adoSage.State == ConnectionState.Closed)
                                adoSage.Open();
                            int x = SCM.ExecuteNonQuery();

                            if (lchoix == 0)
                            {
                                sSQL = " UPDATE immeuble SET Ncompte ='" + StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "',comptedivers ='" +
                                       sClassement + "' where codeimmeuble = '" +
                                       StdSQLchaine.gFr_DoublerQuote(sintitule) + "'";
                                x = General.Execute(sSQL);
                            }
                            functionReturnValue = sCompteTiers;

                            //                sCompteTiers = "D"&&Right(sCompteTiers, Len(sCompteTiers) - 1)
                            //
                            //                SQL = "INSERT INTO F_COMPTET (CT_Num,CT_NumPayeur,CT_Intitule, " _
                            //'               &&"CT_Type,CG_NumPrinc, CT_Classement," _
                            //'               &&"CT_Adresse, CT_Complement, CT_CodePostal, CT_Ville, CT_Contact," _
                            //'               &&"N_Risque, N_Cattarif, N_Catcompta, N_Period,N_Expedition, " _
                            //'               &&"N_Condition, CT_Facture, " _
                            //'               &&" CT_SAUT)" _
                            //'               &&"VALUES (" _
                            //'               &&"'"&&gFr_DoublerQuote(sCompteTiers)&&"','"&&gFr_DoublerQuote(sCompteTiers)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sintitule)&&"',"&&lngType&&", " _
                            //'               &&"'"&&strNumPrinc&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sClassement)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sAdresse)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sComplement)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sCP)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sVille)&&"', " _
                            //'               &&"'"&&gFr_DoublerQuote(sCT_Contact)&&"', " _
                            //'               &&""&&c1&&","&&c1&&","&&c1&&","&&c1&&","&&c1&&","&&c1&&","&&c1&&", " _
                            //'               &&"1)"
                            //                adoSage.Execute SQL
                            //                sSql = " update immeuble set comptedivers ='"&&sClassement&&"' where codeimmeuble = '"&&gFr_DoublerQuote(sintitule)&&"'"
                            //                adocnn.Execute sSql


                            //Si immeuble, vérification de la présence du code analytique
                            if (lchoix == 0)
                            {
                                rsBaseSage = new DataTable();
                                sSQL = "SELECT CA_Num,N_Analytique FROM F_COMPTEA WHERE CA_Num='" +
                                       General.Left(sCompteTiers, 13) + "' AND N_Analytique='" + c3 + "'";
                                SDArsBaseSage = new SqlDataAdapter(sSQL, adoSage);
                                SDArsBaseSage.Fill(rsBaseSage);

                                if (rsBaseSage.Rows.Count > 0)
                                {
                                    PresenceCpteAnalytique = General.nz(rsBaseSage.Rows[0]["ca_num"], "").ToString();
                                }

                                rsBaseSage?.Dispose();
                                SDArsBaseSage?.Dispose();

                            }

                            if (lchoix == 0 && string.IsNullOrEmpty(PresenceCpteAnalytique))
                            {

                                //Création du compte analytique propre à l'immeuble
                                sSQL = "";
                                sSQL = "INSERT INTO F_COMPTEA (N_Analytique,CA_Num,CA_Intitule,CA_Classement, " +
                                       "CA_Type,CA_Report,n_Analyse ) " + "VALUES (" + "'" + c3 + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteTiers), 13) + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble), 35) + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sClassement), 17) + "', " + "" + c0 +
                                       ", " + "" + c0 + ", " + "" + c1 + "" + ")";

                                SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                int xx = SCM.ExecuteNonQuery();

                                //Mise à jour du code analytique dans le compte immeuble
                                sSQL = "";
                                sSQL = "UPDATE F_COMPTET SET CA_Num='" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteTiers), 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";

                                SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                xx = SCM.ExecuteNonQuery();

                                //choix immeuble
                            }
                            else if (lchoix == 0)
                            {

                                sSQL = "";
                                sSQL = "UPDATE F_COMPTET SET CA_Num='" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteTiers), 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";
                                //choix copro

                                SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                x = SCM.ExecuteNonQuery();
                            }
                            else if (lchoix == 1)
                            {
                                sCA_NUMcopro = "SELECT NCOMPTE FROM IMMEUBLE WHERE CODEIMMEUBLE='" +
                                               StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                                using (var tmpModAdo = new ModAdo())
                                    sCA_NUMcopro = tmpModAdo.fc_ADOlibelle(sCA_NUMcopro);

                                sCA_NUMcopro = "SELECT CA_NUM FROM F_COMPTET WHERE CT_NUM='" +
                                               StdSQLchaine.gFr_DoublerQuote(sCA_NUMcopro) + "'";

                                rsBaseSage = new DataTable();
                                SDArsBaseSage = new SqlDataAdapter(sCA_NUMcopro, adoSage);
                                SDArsBaseSage.Fill(rsBaseSage);

                                if (rsBaseSage.Rows.Count > 0)
                                {
                                    sCA_NUMcopro = rsBaseSage.Rows[0]["ca_num"] + "";
                                }
                                else
                                {
                                    sCA_NUMcopro = "";
                                }
                                rsBaseSage?.Dispose();

                                //sSQL = "UPDATE F_COMPTET SET CA_Num='"&&Left(gFr_DoublerQuote(fc_FormatCompte(sCodeImmeuble, 0)), 13)&&"',N_Analytique='"&&c3&&"' WHERE CT_Num='"&&gFr_DoublerQuote(sCompteTiers)&&"'"
                                sSQL = "UPDATE F_COMPTET SET CA_Num='" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCA_NUMcopro), 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";

                                SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                int xx = SCM.ExecuteNonQuery();

                                if (!string.IsNullOrEmpty(sCodeTable))
                                {
                                    General.Execute("UPDATE ImmeublePart SET ImmeublePart.NCompte='" +
                                                           StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "' WHERE ImmeublePart.CodeParticulier='" +
                                                           StdSQLchaine.gFr_DoublerQuote(sCodeTable) + "'"
                                                           //====> 10.03.2020 Ajouté par Mondir : Condition sur le 'CodeImmeuble'
                                                           + $" AND CodeImmeuble='{sCodeImmeuble}'"
                                                           );
                                }

                            }

                            // Fournisseur
                        }
                        else if (lchoix == 2)
                        {
                            sCompteTiers = fc_FormatCompte(sCompteTiers, 2);
                            lngType = 1;

                            if (!string.IsNullOrEmpty(General.sCptGeneralFourn))
                            {
                                strNumPrinc = General.sCptGeneralFourn;
                            }
                            else
                            {
                                strNumPrinc = Convert.ToString(40100000);
                            }

                            General.SQL = "INSERT INTO F_COMPTET (CT_Num,CT_NumPayeur,CT_Intitule, " +
                                          "CT_Type,CG_NumPrinc, CT_Classement," +
                                          "CT_Adresse, CT_Complement, CT_CodePostal, CT_Ville, CT_Contact," +
                                          "N_Risque, N_Cattarif, N_Catcompta, N_Period,N_Expedition, " +
                                          "N_Condition, CT_Facture, " +
                                          " CT_SAUT,CT_TELEPHONE,CT_TELECOPIE,CT_EMAIL,CT_Sommeil, CT_DateCreate, CT_Prospect)" +
                                          "VALUES (" + "'" + StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "','" +
                                          StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sintitule) + "'," + lngType + ", " + "'" +
                                          strNumPrinc + "', " + "'" + StdSQLchaine.gFr_DoublerQuote(sClassement) +
                                          "', " + "'" + StdSQLchaine.gFr_DoublerQuote(sAdresse) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sct_comp.ToString()) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCP) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sVille) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Contact) + "', " + "" + c1 + "," + c1 +
                                          "," + c1 + "," + c1 + "," + c1 + "," + c1 + "," + c1 + ",1," + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_telephone) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_fax) + "', " + "'" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Email.ToString()) + "',0,'" + dtDate + "','" + 0 + "')";

                            var SCM = new SqlCommand(General.SQL, adoSage);
                            if (adoSage.State == ConnectionState.Closed)
                                adoSage.Open();
                            int cc = SCM.ExecuteNonQuery();

                            functionReturnValue = sCompteTiers;
                        }

                        //====Le compte passé en paramètre existe.
                    }
                    else
                    {
                        ///===================> Tested First
                        if (lchoix == 0 || lchoix == 1)
                        {
                            if (lchoix == 0)
                            {
                                sCompteTiers = fc_FormatCompte(sCompteTiers, 0);
                                //strNumPrinc = 41100000
                                //lngType = 0
                            }
                            else if (lchoix == 1)
                            {
                                sCompteTiers = fc_FormatCompte(sCompteTiers, 1);
                                //strNumPrinc = 41100000
                                //lngType = 0
                            }

                            General.SQL = "UPDATE F_COMPTET SET CT_Intitule='" + sintitule + "', " + "CT_Classement='" +
                                          StdSQLchaine.gFr_DoublerQuote(sClassement) + "', " + "CT_Adresse='" +
                                          StdSQLchaine.gFr_DoublerQuote(sAdresse) + "', " + "CT_Complement='" +
                                          StdSQLchaine.gFr_DoublerQuote(sComplement) + "', " + "CT_CodePostal='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCP) + "', " + "CT_Contact='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Contact) + "', " + "CT_Prospect=0 " + "WHERE CT_Num='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";
                            //'=== modif du 16 01 2019, ajout du execute.
                            General.Execute(General.SQL, adoSage);
                            functionReturnValue = sCompteTiers;

                            //===Vérification de l'existance du code analytique.

                            if (lchoix == 0)
                            {
                                sSQL = " UPDATE immeuble SET CompteDivers ='" +
                                       StdSQLchaine.gFr_DoublerQuote(sClassement) + "' WHERE CodeImmeuble = '" +
                                       StdSQLchaine.gFr_DoublerQuote(sintitule) + "'";
                                int xx = General.Execute(sSQL);

                                rsBaseSage = new DataTable();
                                sSQL = "SELECT CA_Num,N_Analytique FROM F_COMPTEA WHERE CA_Num='" +
                                       General.Left(sCompteTiers, 13) + "' AND N_Analytique='" + c3 + "'";

                                SDArsBaseSage = new SqlDataAdapter(sSQL, adoSage);
                                SDArsBaseSage.Fill(rsBaseSage);


                                if (rsBaseSage.Rows.Count > 0)
                                {
                                    PresenceCpteAnalytique = General.nz(rsBaseSage.Rows[0]["ca_num"], "").ToString();
                                }

                                rsBaseSage?.Dispose();
                            }

                            if (lchoix == 0 && string.IsNullOrEmpty(PresenceCpteAnalytique))
                            {
                                sSQL = "";
                                sSQL = "INSERT INTO F_COMPTEA (N_Analytique,CA_Num,CA_Intitule,CA_Classement, " +
                                       "CA_Type,CA_Report,n_Analyse ) " + "VALUES (" + "'" + c3 + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteTiers), 13) + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble), 35) + "', " + "'" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sClassement), 17) + "', " + "" + c0 +
                                       ", " + "" + c0 + ", " + "" + c1 + "" + ")";

                                var SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                int x = SCM.ExecuteNonQuery();

                                //Mise à jour du code analytique dans le compte immeuble
                                sSQL = "";
                                sSQL = "UPDATE F_COMPTET SET CA_Num='" + General.Left(sCompteTiers, 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";

                                SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                x = SCM.ExecuteNonQuery();

                                //Si le code analytique existe, mise à jour dans la table Compte Tiers de l'immeuble
                            }
                            else if (lchoix == 0)
                            {
                                sSQL = "";

                                sSQL = "UPDATE F_COMPTET SET CA_Num='" + General.Left(sCompteTiers, 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";

                                var SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                int xx = SCM.ExecuteNonQuery();

                                //Si le code analytique existe, mise à jour dans la table Compte Tiers du particulier
                            }
                            else if (lchoix == 1)
                            {
                                // sSQL = ""
                                // sSQL = "UPDATE F_COMPTET SET CA_Num='"&&Left(gFr_DoublerQuote(fc_FormatCompte(sCodeImmeuble)), 13)&&"',N_Analytique='"&&c3&&"' WHERE CT_Num='"&&gFr_DoublerQuote(sCompteTiers)&&"'"
                                //  adoSage.Execute sSQL, nbRecAff
                                //=== modif du 02 03 2012
                                sCA_NUMcopro = "SELECT NCOMPTE FROM IMMEUBLE WHERE CODEIMMEUBLE='" +
                                               StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                                using (var tmpModAdo = new ModAdo())
                                    sCA_NUMcopro = tmpModAdo.fc_ADOlibelle(sCA_NUMcopro);


                                sCA_NUMcopro = "SELECT CA_NUM FROM F_COMPTET WHERE CT_NUM='" +
                                               StdSQLchaine.gFr_DoublerQuote(sCA_NUMcopro) + "'";

                                rsBaseSage = new DataTable();
                                SDArsBaseSage = new SqlDataAdapter(sCA_NUMcopro, adoSage);
                                SDArsBaseSage.Fill(rsBaseSage);

                                if (rsBaseSage.Rows.Count > 0)
                                {
                                    sCA_NUMcopro = rsBaseSage.Rows[0]["ca_num"] + "";
                                }
                                else
                                {
                                    sCA_NUMcopro = "";
                                }
                                rsBaseSage?.Dispose();

                                //sSQL = "UPDATE F_COMPTET SET CA_Num='"&&Left(gFr_DoublerQuote(fc_FormatCompte(sCodeImmeuble, 0)), 13)&&"',N_Analytique='"&&c3&&"' WHERE CT_Num='"&&gFr_DoublerQuote(sCompteTiers)&&"'"
                                sSQL = "UPDATE F_COMPTET SET CA_Num='" +
                                       General.Left(StdSQLchaine.gFr_DoublerQuote(sCA_NUMcopro), 13) +
                                       "',N_Analytique='" + c3 + "' WHERE CT_Num='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";

                                var SCM = new SqlCommand(sSQL, adoSage);
                                if (adoSage.State == ConnectionState.Closed)
                                    adoSage.Open();
                                int xx = SCM.ExecuteNonQuery();

                                if (!string.IsNullOrEmpty(sCodeTable))
                                {
                                    xx = General.Execute("UPDATE ImmeublePart SET ImmeublePart.NCompte='" +
                                                           StdSQLchaine.gFr_DoublerQuote(sintitule) + "',N_Analytique='" + c3 +
                                                           "' WHERE ImmeublePart.CodeParticulier='" + StdSQLchaine.gFr_DoublerQuote(sCodeTable) + "'"
                                                           //====> 10.03.2020 Ajouté par Mondir : Condition sur le 'CodeImmeuble'
                                                           + $" AND CodeImmeuble='{sCodeImmeuble}'"
                                                           );

                                }

                            }

                            //Sauvegarde des infos du compte fournisseur
                        }
                        else if (lchoix == 2)
                        {
                            sCompteTiers = fc_FormatCompte(sCompteTiers, 2);
                            General.SQL = "UPDATE F_COMPTET SET CT_Intitule='" + sintitule + "', " + "CT_Classement='" +
                                          StdSQLchaine.gFr_DoublerQuote(sClassement) + "', " + "CT_Adresse='" +
                                          StdSQLchaine.gFr_DoublerQuote(sAdresse) + "', " + "CT_Complement='" +
                                          StdSQLchaine.gFr_DoublerQuote(sct_comp.ToString()) + "', " + "CT_CodePostal='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCP) + "', " + "CT_Contact='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Contact) + "', " + "CT_Telephone='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_telephone) + "', " + "CT_Telecopie='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_fax) + "', " + "CT_Email='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCT_Email.ToString()) + "', " + "CT_Prospect= 0" + " " + " WHERE CT_Num='" +
                                          StdSQLchaine.gFr_DoublerQuote(sCompteTiers) + "'";
                            functionReturnValue = sCompteTiers;

                        }


                        var SCM2 = new SqlCommand(General.SQL, adoSage);
                        if (adoSage.State == ConnectionState.Closed)
                            adoSage.Open();
                        int xxx = SCM2.ExecuteNonQuery();
                    }
                }


                if ((rsSage != null))
                {
                    rsSage?.Dispose();
                }

                fc_CloseConnSage();
                return functionReturnValue;
            }
            catch (SqlException e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Insertion dans SAGE IMPOSSIBLE, Plan tiers ouvert sur un autre poste." + " " + e.Message);
                //Resume Next
                Erreurs.gFr_debug(e, ";prcAjoutSage_errInert_errInsertTiersSage", false);
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur Sage : " + e.Message + "\n" + e.Source, "Erreur Sage", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Erreurs.gFr_debug(e, ";prcAjoutSage", false);
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCompteTiers"></param>
        /// <param name="lchoix"></param>
        /// <returns></returns>
        public static string fc_FormatCompte(string sCompteTiers, int lchoix = 0)
        {
            //formate le compte tiers n'est pas neccessaire si le compte est definis.
            //sinon modifier ici le format desirée.
            // immeuble
            if (lchoix == 0)
            {
                sCompteTiers = "C" + SuppCar(sCompteTiers, " ");

                // Copropriétaire
            }
            else if (lchoix == 1)
            {
                sCompteTiers = "C" + SuppCar(sCompteTiers, " ");

                // Fournisseur
            }
            else if (lchoix == 2)
            {
                if (General.Left(sCompteTiers, 1) != "F")
                {
                    sCompteTiers = "F" + SuppCar(sCompteTiers, " ");
                }
                else
                {
                    sCompteTiers = SuppCar(sCompteTiers, " ");
                }

                // En liaison avec l'analytique appel
            }
            else if (lchoix == 3)
            {
                sCompteTiers = "AF" + SuppCar(sCompteTiers, " ");

            }

            sCompteTiers = SuppCar(sCompteTiers, ".");
            sCompteTiers = SuppCar(sCompteTiers, "(");
            sCompteTiers = SuppCar(sCompteTiers, ")");
            sCompteTiers = SuppCar(sCompteTiers, "-");
            sCompteTiers = SuppCar(sCompteTiers, "'");
            sCompteTiers = SuppCar(sCompteTiers, "/");
            sCompteTiers = SuppCar(sCompteTiers, "\\");
            sCompteTiers = SuppCar(sCompteTiers, ";");

            //===> Mondir le 15.11.2020 pour ajouter lees modfis de la version V12.11.2020
            //=== modif du 02 11 2020
            sCompteTiers = SuppCar(sCompteTiers, "]");
            sCompteTiers = SuppCar(sCompteTiers, "[");
            sCompteTiers = SuppCar(sCompteTiers, "}");
            sCompteTiers = SuppCar(sCompteTiers, "{");
            sCompteTiers = SuppCar(sCompteTiers, "#");
            sCompteTiers = SuppCar(sCompteTiers, "~");
            sCompteTiers = SuppCar(sCompteTiers, "|");
            sCompteTiers = SuppCar(sCompteTiers, "*");
            sCompteTiers = SuppCar(sCompteTiers, "¤");
            sCompteTiers = SuppCar(sCompteTiers, ":");
            sCompteTiers = SuppCar(sCompteTiers, "!");
            sCompteTiers = SuppCar(sCompteTiers, "µ");
            sCompteTiers = SuppCar(sCompteTiers, "@");
            sCompteTiers = SuppCar(sCompteTiers, "°");
            sCompteTiers = SuppCar(sCompteTiers, "&");
            sCompteTiers = SuppCar(sCompteTiers, "%");
            sCompteTiers = SuppCar(sCompteTiers, "£");
            sCompteTiers = SuppCar(sCompteTiers, "$");
            sCompteTiers = SuppCar(sCompteTiers, "^");
            sCompteTiers = SuppCar(sCompteTiers, "¨");
            //===> Fin Modif Mondir

            //======> Mohammed 09.07.2020 ===> Modifs of V09.07.2020
            sCompteTiers = SuppCar(sCompteTiers, "+");

            sCompteTiers = General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteTiers), 17).Trim().ToUpper();
            //Debug.Print Len(sCompteTiers)
            return sCompteTiers;
            // placer votre code ici pour personnaliser les informations pour le compte tiers
            //  'recherche des infos sur le gérant
            // Set rsSage = New ADODB.Recordset

            // SQL = "SELECT * FROM Table1 WHERE Code1='"&&infogerant&&"'"
            // Set rs = gdb.OpenRecordset(SQL)
            // If Not (rs.BOF Or rs.EOF) Then
            //     sClassement = rs(0)
            //     sAdresse = rs(4)
            //     sComplement = rs(2)
            //     sCP = rs(8)
            //     sVille = rs(7)
            // End If

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="vVar"></param>
        /// <param name="sCAR"></param>
        /// <returns></returns>
        public static string SuppCar(string vVar, string sCAR)
        {
            //' cette fonction renvoie une chaine de caractere épurée du caractere indesiré.
            //' vVar doit contenir une chaine de caractere.
            //' sCar Le caractere a enlever dans vVar.
            return vVar.Replace(sCAR, "");
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sintitule"></param>
        /// <returns></returns>
        public static string fc_RechercheSiDoublon(string sintitule)
        {
            //recherche de l'existence de ce code dans les comptes tiers SAGE
            string sORIGINAL = null;
            int i = 0;
            DataTable rstmp = default(DataTable);
            sSQL = "SELECT  CT_num" + " FROM F_COMPTET where CT_num ='" + sintitule + "'";

            rstmp = new DataTable();
            SqlDataAdapter SDArstmp = new SqlDataAdapter(sSQL, adoSage);
            SDArstmp.Fill(rstmp);
            if (rstmp.Rows.Count > 0)
            {
                sORIGINAL = rstmp.Rows[0][0].ToString();
                i = 1;
                while (sintitule == rstmp.Rows[0][0].ToString())
                {
                    if (i > 9)
                    {
                        sintitule = General.Left(sORIGINAL, sORIGINAL.Length - 1);
                    }
                    else if (i > 99)
                    {
                        sintitule = General.Left(sORIGINAL, sORIGINAL.Length - 2);
                    }
                    sintitule = General.Left(sintitule, sintitule.Length - 1) + i;
                    General.SQL = "SELECT CT_num FROM F_COMPTET where CT_num='" + sintitule + "'";
                    rstmp = new DataTable();
                    SDArstmp = new SqlDataAdapter(General.SQL, adoSage);
                    SDArstmp.Fill(rstmp);

                    if (rstmp.Rows.Count == 0)
                    {
                        break;
                    }
                    //If sNCompte <> rsTmp(0) Then
                    //    Exit Do
                    //End If
                    i = i + 1;

                }

            }
            return sintitule;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNumFicheStandard"></param>
        /// <param name="sIntitulle"></param>
        /// <param name="sClassement"></param>
        /// <param name="bNotOpenSage"></param>
        /// <returns></returns>
        public static object VerifCreeAnalytique(string sNumFicheStandard, string sIntitulle, string sClassement, bool bNotOpenSage = false)
        {
            object functionReturnValue = null;
            short ErrAnalytique = 0;
            try
            {
                DataTable rsBaseSage = default(DataTable);
                SqlDataAdapter SDArsBaseSage = null;
                SqlCommand SCDrsBaseSage = null;
                string sCompteAnalytique = null;
                const string c0 = "0";
                const string c1 = "1";
                const string c2 = "2";
                //Numéro correspondant à l'analytique affaire 2 (N_Analytique)
                const string c3 = "3";


                //sNumFicheStandard doit contenir uniquement le numéro de la fiche d'appel
                //sans format particulier.
                //sIntitulle contient le numéro de fiche d'appel
                //sClassement contient le code Immeuble

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    return functionReturnValue;
                    //  MsgBox "Liaison comptabilité inexistante.."
                }


                ErrAnalytique = 0;
                if (bNotOpenSage == false)
                {
                    fc_OpenConnSage();
                }

                ErrAnalytique = 1;
                sCompteAnalytique = fc_FormatCompte(sNumFicheStandard, 3);

                rsBaseSage = new DataTable();

                sSQL = "SELECT CA_Num,N_Analytique FROM F_COMPTEA WHERE CA_Num='" + General.Left(sCompteAnalytique, 13) + "' AND N_Analytique='" + c2 + "'";
                SDArsBaseSage = new SqlDataAdapter(sSQL, adoSage);
                SDArsBaseSage.Fill(rsBaseSage);

                ErrAnalytique = 2;

                if (rsBaseSage.Rows.Count == 0)
                {

                    //Création du compte analytique propre à la fiche d'appel
                    sSQL = "";
                    sSQL = "INSERT INTO F_COMPTEA (N_Analytique,CA_Num,CA_Intitule,CA_Classement, " + "CA_Type,CA_Report,n_Analyse ) " + "VALUES (" +
                        "'" + c2 + "', " + "'" + General.Left(StdSQLchaine.gFr_DoublerQuote(sCompteAnalytique), 13) + "', " + "'" + General.Left(StdSQLchaine.gFr_DoublerQuote(sIntitulle), 35) +
                        "', " + "'" + General.Left(StdSQLchaine.gFr_DoublerQuote(sClassement), 17) + "', " + "" + c0 + ", " + "" + c0 + ", " + "" + c1 + "" + ")";

                    SCDrsBaseSage = new SqlCommand(sSQL, adoSage);
                    if (adoSage.State != ConnectionState.Open)
                        adoSage.Open();

                    int xx = SCDrsBaseSage.ExecuteNonQuery();

                }

                ErrAnalytique = 3;

                //if (adoSage.State != ConnectionState.Closed)
                //    adoSage.Close();

                rsBaseSage?.Dispose();
                SDArsBaseSage?.Dispose();
                SCDrsBaseSage?.Dispose();

                if (bNotOpenSage == false)
                {
                    fc_CloseConnSage();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                switch (ErrAnalytique)
                {
                    case 0:
                        //Erreur lors de l'ouverture de la connection vers sage
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur lors de l'ouverture de la connection vers sage", "Erreur Sage", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Erreurs.gFr_debug(ex, "Fonction VerifCreeAnalytique() : ouverture de la connection vers sage", false);
                        break;
                    case 1:
                        //Erreur sur la vérification de la présence du compte analytique
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur sur la vérification de la présence du compte analytique", "Erreur Sage", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Erreurs.gFr_debug(ex, "Fonction VerifCreeAnalytique() : vérification de la présence du compte analytique", false);
                        break;
                    case 2:
                        //Erreur d'insertion du compte analytique
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur d'insertion du compte analytique", "Erreur Sage", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Erreurs.gFr_debug(ex, "Fonction VerifCreeAnalytique() : insertion du compte analytique", false);
                        break;
                    case 3:
                        //Erreur sur la fermeture de la connection Sage
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur lors de la fermeture de la connection Sage", "Erreur Sage", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Erreurs.gFr_debug(ex, "Fonction VerifCreeAnalytique() : fermeture de la connection Sage", false);
                        break;
                }
                return functionReturnValue;
            }
        }

        public static double GerantGetCredit(bool EcrituresNonLetrees, string DateMini, string DateMaxi, string compteTiers, string sCritere)
        {
            double credit = 0;

            try
            {
                if (EcrituresNonLetrees)
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "  )" + " or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }
                else
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }

                using (var tmpModAdo = new ModAdo())
                {
                    var dt = tmpModAdo.fc_OpenRecordSet(General.sSQL, Conn: adoSage);
                    credit = General.nz(dt.Rows[0]["Montant_Total"], 0).ToDouble();
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return credit;
        }

        /// <summary>
        /// Mondir le 02.04.2021 ===> https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <param name="EcrituresNonLetrees"></param>
        /// <param name="DateMini"></param>
        /// <param name="DateMaxi"></param>
        /// <param name="compteTiers"></param>
        /// <param name="sCritere"></param>
        /// <returns></returns>
        public static double GerantGetDebit(bool EcrituresNonLetrees, string DateMini, string DateMaxi, string compteTiers, string sCritere)
        {
            double Debit = 0;

            try
            {
                if (EcrituresNonLetrees)
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + " ) " + " or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }
                else
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                }

                using (var tmpModAdo = new ModAdo())
                {
                    var dt = tmpModAdo.fc_OpenRecordSet(General.sSQL, Conn: adoSage);
                    Debit = General.nz(dt.Rows[0]["Montant_Total"], 0).ToDouble();
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return Debit;
        }

        /// <summary>
        /// Mondir le 02.04.2021 to fix https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <returns></returns>
        public static SoldeClientSearchParams GetSoldeClientSearchParams()
        {
            var dateMini = "";
            int jourMaxiMois = 0;
            var dateMaxi = "";

            if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
            {
                dateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Today.Year);
                jourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year);
                dateMaxi = jourMaxiMois + "/" + General.MoisFinExercice + "/" + Convert.ToString(DateTime.Today.Year);
            }
            else
            {
                if (DateTime.Today.Month >= 1 && DateTime.Today.Month <= Convert.ToInt16(General.MoisFinExercice))
                {
                    dateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year - 1);
                    //MoisFinExercice + 1
                    jourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year);
                    dateMaxi = jourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //MoisFinExercice
                }
                else
                {
                    dateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //MoisFinExercice - 1
                    jourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year + 1);
                    dateMaxi = jourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year + 1);
                    //MoisFinExercice
                }
            }

            return new SoldeClientSearchParams { DateMaxi = dateMaxi, DateMini = dateMini, JourMaxiMois = jourMaxiMois };
        }

        /// <summary>
        /// Mondir le 02.04.2021 to fix https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <param name="EcrituresNonLetrees"></param>
        /// <param name="DateMini"></param>
        /// <param name="DateMaxi"></param>
        /// <param name="compteTiers"></param>
        /// <param name="sCritere"></param>
        /// <returns></returns>
        public static SoldeClient GetSoldeClient(bool EcrituresNonLetrees, string DateMini, string DateMaxi, string compteTiers, string sCritere)
        {
            var solde = new SoldeClient();

            try
            {
                fc_OpenConnSage();

                var credit = GerantGetCredit(EcrituresNonLetrees, DateMini, DateMaxi, compteTiers, sCritere);
                var debit = GerantGetDebit(EcrituresNonLetrees, DateMini, DateMaxi, compteTiers, sCritere);

                solde.Credit = credit;
                solde.Debit = debit;
                solde.Solde = solde.Credit - solde.Debit;

                return solde;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return solde;
        }

        /// <summary>
        /// Mondir le 02.04.2021 to fix https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <returns></returns>
        public static string GetCritere()
        {
            var sCritere = "";
            try
            {
                if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                {
                    sCritere = General.cCritereFinance;
                }
                else
                {
                    sCritere = "";
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return sCritere;
        }
    }
}
