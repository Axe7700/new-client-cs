﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Axe_interDT.Shared
{
    public static class modP2
    {
        public const string cCPREST = "CPREST";

        public const string contratA = "A";
        public const string contratAR = "AR";

        //=== constantes des paramétres GMAO.
        public const string cParamPaques = "Paques";
        public const string cParamJoursFériées = "JoursFériées";
        public const string cParamSamedi = "Samedi";
        public const string cParamDimanche = "Dimanche";
        public const string cParamPentecote = "Pentecôte";

        public struct typeParamGmao
        {
            public int lPaques;
            public int lJoursFeriees;
            public int lSamedi;
            public int lDimanche;
            public int lPentecote;
        }

        public const string clibNiv0 = "Equipements";
        public const string clibNiv1 = "Composant";
        public const string cLIbNiv2 = "Contrôle/Opération";
        public const string cLibNv2bis = "Opération";
        public const string cLibNv2Tier = "Prestation";
        public const string cLibNiv3 = "Nature des prestations";
        public const string cLibNiv4 = "Planification";
        public const string cLibEtatDetailOp = "Détail des visites";
        //Public Const cLibNiv5 = "Matériels"

        public static typeParamGmao tGmao;

        public const string cCodeImmeuble = "Immeuble";


        public const string cSelSousGam = "SELECTION";
        public const string cSupSousGam = "SUPPRESSION";

        public const string cHebdo = "HEBDOMADAIRE";
        public const string cMensuel = "MENSUEL";
        public const string cBiMensuel = "BIMENSUEL";
        public const string cBiMestriel = "BIMESTRIEL";
        public const string cTriMestriel = "TRIMESTRIEL";
        public const string cSemestriel = "SEMESTRIEL";
        public const string cQuotidien = "QUOTIDIEN";
        public const string cAnnuel = "ANNUEL";
        public const string cCycleAnnuel = "Cycle année";
        public const string cCycleJournee = "Cycle Journée";
        public const string cCycleMensuel = "Cycle mois";


        public const string cGammeFille = "Prestation";
        public const string cGammeLib = "Motif";
        public const string cTacheLib = "Nature Des Opérations";
        public const string cChapitre = "Chapitres";
        public const string cMateriel = "Matériels";
        public const string cNbeGammeFille = "Prestations";


        public const short cDimanche = 0;
        public const string cSDimanche = "Dimanche";

        public const short cLundi = 1;
        public const string cSLundi = "Lundi";

        public const short cMardi = 2;
        public const string cSMardi = "Mardi";

        public const short cMercredi = 3;
        public const string cSMercredi = "Mercredi";

        public const short cJeudi = 4;
        public const string cSJeudi = "Jeudi";

        public const short cVendredi = 5;
        public const string cSVendredi = "Vendredi";

        public const short cSamedi = 6;
        public const string cSSamedi = "Samedi";

        public static string cHeureDebut = "8:00:00 AM";

        public static System.DateTime[] tabDPaque;
        static DataTable rs;
        static ModAdo modAdors = null;
        const string cCodeP2 = "PO1";
        const string cDesignationP2 = "Visite d'entretien";
        public static DataRow[] tlJourFeriee;

        static DataTable rstmp;
        static ModAdo modAdorstmp = null;

        /// <summary>
        /// tested
        /// </summary>
        public static void fc_ChargeJourFeriee()
        {
            DataTable rsJFer = default(DataTable);
            ModAdo modAdorsJFer = null;
            int i = 0;
            object ok = null;
            // teste si le tableau est déja alimenté.
            if ((tlJourFeriee == null))
            {

                General.sSQL = "SELECT JFE_Jour, JFE_Mois" + " FROM JFE_Feriee";

                modAdorsJFer = new ModAdo();
                rsJFer = modAdorsJFer.fc_OpenRecordSet(General.sSQL);

                tlJourFeriee = rsJFer.Rows.Cast<DataRow>().ToArray();

                modAdorsJFer.Close();

                // For i = 0 To UBound(tlJourFeriee, 2)
                //
                //Next
            }
        }

        public static string FC_intervenantImm(string sCodeImmeuble)
        {
            string functionReturnValue = null;
            //=== modif du 30 08 2010 ajout de la catégorie de l'intervenant.
            DataTable rs = default(DataTable);
            ModAdo modAdors = new ModAdo();
            string sSQL = "";
            //sSQL = "SELECT  " _
            //& " IntervenantImm_INM.intervenant_INM " _
            //& " FROM IntervenantImm_INM " _
            //& " where Codeimmeuble_IMM='" & gFr_DoublerQuote(sCodeimmeuble) & "'" _
            //& " AND CATI_CODE ='" & cIntervEntretien & "'" _
            //& " order by Nordre_INM"

            if (General.sRamoneurP2particulier == "1")
            {
                sSQL = "SELECT     Immeuble.CodeImmeuble, Immeuble.CodeDepanneur, Qualification.CodeQualif, "
                       + " Immeuble.CodeRamoneur"
                       + " FROM         Qualification LEFT OUTER JOIN"
                       + " Gestionnaire ON Qualification.CodeQualif = Gestionnaire.CodeQualif_Qua RIGHT OUTER JOIN "
                       + " Immeuble ON Gestionnaire.Code1 = Immeuble.Code1 "
                       + " AND Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire "
                       + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                //===> Mondir le 05.01.2021, change General.sSQL by sSQL
                rs = modAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(General.nz(rs.Rows[0]["CodeQualif"], "").ToString()))
                    {
                        if (General.UCase(rs.Rows[0]["CodeQualif"].ToString()) == General.UCase("PART"))
                        {
                            functionReturnValue = General.nz(rs.Rows[0]["CodeRamoneur"], "").ToString();
                        }
                        else
                        {
                            functionReturnValue = General.nz(rs.Rows[0]["CodeDepanneur"], "").ToString();
                        }
                    }
                    else
                    {
                        functionReturnValue = General.nz(rs.Rows[0]["CodeDepanneur"], "").ToString();
                    }
                    rs.Dispose();
                    rs = null;
                }
            }
            else
            {
                General.sSQL = "SELECT     CodeDepanneur"
                + " From Immeuble "
                + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                rs = modAdors.fc_OpenRecordSet(General.sSQL);
                if (rs.Rows.Count > 0)
                {
                    functionReturnValue = General.nz(rs.Rows[0]["CodeDepanneur"], "").ToString();
                }
                rs.Dispose();
                rs = null;
            }



            modAdors.Close();
            return functionReturnValue;

        }

        public static int fc_ReturnDay(string sDay)
        {
            int functionReturnValue = 0;
            //=== sDay contient le jour en lettre
            //=== la fonction retourne le jour en nombre.
            //=== attention dimanche est le premier jour de la semaine dans la fonction weekday par défaut

            try
            {
                functionReturnValue = 0;

                if (sDay.ToUpper() == cSDimanche.ToUpper())
                {
                    functionReturnValue = cDimanche;
                }
                else if (sDay.ToUpper() == cSLundi.ToUpper())
                {
                    functionReturnValue = cLundi;
                }
                else if (sDay.ToUpper() == cSMardi.ToUpper())
                {
                    functionReturnValue = cMardi;
                }
                else if (sDay.ToUpper() == cSMercredi.ToUpper())
                {
                    functionReturnValue = cMercredi;
                }
                else if (sDay.ToUpper() == cSJeudi.ToUpper())
                {
                    functionReturnValue = cJeudi;
                }
                else if (sDay.ToUpper() == cSVendredi.ToUpper())
                {
                    functionReturnValue = cVendredi;
                }
                else if (sDay.ToUpper() == cSSamedi.ToUpper())
                {
                    functionReturnValue = cSamedi;
                }

                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2;fc_ReturnDay");
                return functionReturnValue;
            }
        }

        public static bool fPaques(DateTime dDate)
        {
            bool functionReturnValue = false;
            System.DateTime Paques = default(System.DateTime);
            System.DateTime LunPaq = default(System.DateTime);
            System.DateTime Ascension = default(System.DateTime);
            System.DateTime LunPent = default(System.DateTime);
            //Calcule le jour de Pâques en fonction
            //de l'année

            int a = 0;
            int b = 0;
            int C = 0;
            int d = 0;
            int e = 0;
            int f = 0;
            int g = 0;
            int h = 0;
            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int p = 0;
            int An = 0;

            An = dDate.Year;
            a = An % 19;
            b = An / 100;
            C = An % 100;
            d = b / 4;
            e = b % 4;
            f = (b + 8) / 25;
            g = (b - f + 1) / 3;
            h = (19 * a + b - d - g + 15) % 30;
            i = C / 4;
            k = C % 4;
            l = (32 + 2 * e + 2 * i - h - k) % 7;
            m = (a + 11 * h + 22 * l) / 451;
            n = (h + l - 7 * m + 114) / 31;
            p = (h + l - 7 * m + 114) % 31;

            tabDPaque = new DateTime[3];

            Paques = new DateTime(An, n, p + 1);
            LunPaq = DateTime.FromOADate(Paques.ToOADate() + 1);
            //En déduit
            Ascension = DateTime.FromOADate(Paques.ToOADate() + 39);
            //les autres
            LunPent = DateTime.FromOADate(Paques.ToOADate() + 50);
            //jours fériés
            tabDPaque[0] = LunPaq;
            tabDPaque[1] = Ascension;
            tabDPaque[2] = LunPent;

            if (dDate == LunPaq)
            {
                functionReturnValue = true;
            }
            else if (dDate == Ascension)
            {
                functionReturnValue = true;
            }
            else if (dDate == LunPent)
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }
            return functionReturnValue;
            //Paques = fPaques(An%)   'Cherche le jour de Pâques

        }

        public static void fc_ParamGmao()
        {
            using (var tmpAdo = new ModAdo())
            {
                General.sSQL = "SELECT GMAP_Valeur FROM GAMP_ParamGmao WHERE GMAP_TypeParam ='" + cParamDimanche + "'";
                General.sSQL = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
                tGmao.lDimanche = Convert.ToInt16(General.nz(Convert.ToInt32(General.sSQL), 0));
                General.sSQL = "SELECT GMAP_Valeur FROM GAMP_ParamGmao WHERE GMAP_TypeParam ='" + cParamSamedi + "'";
                General.sSQL = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
                tGmao.lSamedi = Convert.ToInt16(General.nz(Convert.ToInt32(General.sSQL), 0));
                General.sSQL = "SELECT GMAP_Valeur FROM GAMP_ParamGmao WHERE GMAP_TypeParam ='" + cParamJoursFériées + "'";
                General.sSQL = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
                tGmao.lJoursFeriees = Convert.ToInt16(General.nz(Convert.ToInt32(General.sSQL), 0));
                General.sSQL = "SELECT GMAP_Valeur FROM GAMP_ParamGmao WHERE GMAP_TypeParam ='" + cParamPaques + "'";
                General.sSQL = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
                tGmao.lPaques = Convert.ToInt16(General.nz(Convert.ToInt32(General.sSQL), 0));
                General.sSQL = "SELECT GMAP_Valeur FROM GAMP_ParamGmao WHERE GMAP_TypeParam ='" + cParamPentecote + "'";
                General.sSQL = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
                tGmao.lPentecote = Convert.ToInt16(General.nz(Convert.ToInt32(General.sSQL), 0));
            }
        }


        public static void fc_AddGamme(int lGAi_ID, string sCodeImmeuble, int lGAM_ID, int lCop_NoAuto, int lPDAB_Noauto)
        {
            DataTable rs = default(DataTable);
            DataTable rsAdd = default(DataTable);
            string sSQL = null;
            sSQL = "SELECT     CodeImmeuble, CAI_Code, CAI_Libelle, ICC_Noauto, ICC_NoLigne, "
                + " COP_Noauto, ICC_Cout, COS_NoAuto, ICC_Affiche, PDAB_Noauto, ICC_Select, " + " GAi_ID "
                + " From ICC_IntervCategorieContrat " + " WHERE     GAi_ID = " + lGAi_ID;
            var tmpAdorsAdd = new ModAdo();

            rsAdd = tmpAdorsAdd.fc_OpenRecordSet(sSQL);

            if (rsAdd.Rows.Count > 0)
            {
                //=== si ces gammes existent, ne pas les rajouter à nouveau.
                return;

            }

            sSQL = "SELECT  CAI_Noauto, CAI_Code, CAI_Libelle, CAI_NoLigne, CAI_Releve, GAM_ID  FROM  CAI_CategoriInterv ";
            sSQL = sSQL + " WHERE GAM_ID =" + lGAM_ID;

            var tmpAdors = new ModAdo();
            rs = tmpAdors.fc_OpenRecordSet(sSQL);
            var rsAddRow = rsAdd.NewRow();



            SqlCommandBuilder cb = new SqlCommandBuilder(tmpAdorsAdd.SDArsAdo);
            tmpAdorsAdd.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            tmpAdorsAdd.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            tmpAdorsAdd.SDArsAdo.InsertCommand = insertCmd;

            tmpAdorsAdd.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {

                if (e.StatementType == StatementType.Insert)
                {
                    if ((string.IsNullOrEmpty(rsAddRow["ICC_Noauto"].ToString())))
                    {

                        rsAddRow["ICC_Noauto"] =
                            Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                    }
                }
            });




            foreach (DataRow rsRows in rs.Rows)
            {

                rsAddRow["CodeImmeuble"] = sCodeImmeuble;
                rsAddRow["CAI_Code"] = rsRows["CAI_Code"];
                rsAddRow["CAI_Libelle"] = rsRows["CAI_Libelle"];
                rsAddRow["ICC_NoLigne"] = rsRows["CAI_NoLigne"];
                rsAddRow["COP_Noauto"] = lCop_NoAuto;
                rsAddRow["PDAB_Noauto"] = lPDAB_Noauto;
                rsAddRow["GAi_ID"] = lGAi_ID;

                rsAdd.Rows.Add(rsAddRow.ItemArray);

                tmpAdorsAdd.Update();


                fc_AddOp(rsRows["CAI_Noauto"].ToString(), sCodeImmeuble, lCop_NoAuto, Convert.ToInt32(rsAddRow["ICC_Noauto"].ToString()));


            }



        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lCAI_NoAuto"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="lCop_NoAuto"></param>
        /// <param name="lICC_Noauto"></param>
        public static void fc_AddOp(string lCAI_NoAuto, string sCodeImmeuble, int lCop_NoAuto, int lICC_Noauto)
        {

            rs = default(DataTable);
            DataTable rsAdd = default(DataTable);
            string sDateDebutPlanif = null;


            try
            {

                if (Convert.ToDouble(lCAI_NoAuto) == 0)
                {
                    return;
                }

                General.sSQL = "SELECT EQM_NoAuto,EQM_Code, EQM_Libelle,  CodeImmeuble," + "  EQM_Observation, EQM_NoLigne, EQU_NoAuto,"
                    + " COP_NoAuto, ICC_Noauto,Duree, DureePlanning, EQU_TypeIntervenant , CompteurObli, EQU_P1Compteur, "
                    + " CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle "
                    + " From EQM_EquipementP2Imm"
                    + " WHERE  ICC_Noauto = " + lICC_Noauto;
                var tmpAdorsAdd = new ModAdo();
                rsAdd = tmpAdorsAdd.fc_OpenRecordSet(General.sSQL);
                if (rsAdd.Rows.Count > 0)
                {
                    //=== si ces gammes existent, ne pas les rajouter à nouveau.
                    return;
                }

                var tmpAdo = new ModAdo();
                sDateDebutPlanif = tmpAdo.fc_ADOlibelle("SELECT COP_DateSignature "
                                 + " FROM COP_ContratP2 WHERE COP_NoAuto = " + General.nz(lCop_NoAuto, 0));

                General.sSQL = "SELECT  EQU_Code, EQU_Libelle, EQU_NoAuto, EQU_NoLigne,"
                    + " EQU_Observation, Duree, DureePlanning, CompteurObli, EQU_P1Compteur,  "
                    + " CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle "
                    + " From EQU_EquipementP2" + " WHERE  CAI_Noauto = " + lCAI_NoAuto;

                rs = tmpAdo.fc_OpenRecordSet(General.sSQL);

                var NewRow = rsAdd.NewRow();


                SqlCommandBuilder cb = new SqlCommandBuilder(tmpAdorsAdd.SDArsAdo);
                tmpAdorsAdd.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                tmpAdorsAdd.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                tmpAdorsAdd.SDArsAdo.InsertCommand = insertCmd;

                //  int EQM_NoAuto = 0;

                tmpAdorsAdd.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                  {
                      if (e.StatementType == StatementType.Insert)
                      {
                          NewRow["EQM_NoAuto"] = e.Command.Parameters["@ID"].Value;

                      }
                  });


                //while (rs.EOF == false)
                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow rsRows in rs.Rows)
                    {

                        // rsAdd.AddNew();
                        NewRow["EQM_CODE"] = rsRows["EQU_Code"];
                        NewRow["EQM_Libelle"] = rsRows["EQU_Libelle"];
                        NewRow["EQU_NoAuto"] = rsRows["EQU_NoAuto"];
                        NewRow["EQM_Noligne"] = rsRows["EQU_NoLigne"];
                        NewRow["EQM_Observation"] = rsRows["EQU_Observation"];
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["CompteurObli"] = rsRows["CompteurObli"];
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        NewRow["EQU_NoAuto"] = General.nz(rsRows["EQU_NoAuto"], 0);
                        NewRow["ICC_Noauto"] = lICC_Noauto;
                        NewRow["Duree"] = General.nz(rsRows["Duree"], 0);
                        NewRow["DureePlanning"] = General.nz(rsRows["DureePlanning"], 0);
                        NewRow["EQU_P1Compteur"] = rsRows["EQU_P1Compteur"];
                        NewRow["CTR_ID"] = rsRows["CTR_ID"];
                        NewRow["CTR_Libelle"] = rsRows["CTR_Libelle"];
                        NewRow["MOY_ID"] = rsRows["MOY_ID"];
                        NewRow["MOY_Libelle"] = rsRows["MOY_Libelle"];
                        NewRow["ANO_ID"] = rsRows["ANO_ID"];
                        NewRow["ANO_Libelle"] = rsRows["ANO_Libelle"];
                        NewRow["OPE_ID"] = rsRows["OPE_ID"];
                        NewRow["OPE_Libelle"] = rsRows["OPE_Libelle"];
                        rsAdd.Rows.Add(NewRow);
                        // rsAdd!EQU_TypeIntervenant = nz(rs!TypeIntervenant, "EXP")
                        tmpAdorsAdd.Update();
                        //rsAdd.MoveLast();//Pour avoir le derneir ID inserer

                        fc_AddTaches(Convert.ToInt32(General.nz(NewRow["EQU_NoAuto"], 0)), General.nz(NewRow["EQM_NoAuto"], 0), lCop_NoAuto, sCodeImmeuble);

                        fc_AddPlanning(Convert.ToInt32(General.nz(NewRow["EQU_NoAuto"], 0)), General.nz(NewRow["EQM_NoAuto"], 0), lCop_NoAuto, sCodeImmeuble,
                            Convert.ToDouble(General.nz(rsRows["Duree"], 0)), Convert.ToDouble(General.nz(rsRows["DureePlanning"], 0)), sDateDebutPlanif);

                        // rs.MoveNext();
                    }
                }
                rs.Dispose();
                rsAdd.Dispose();
                rsAdd = null;
                rs = null;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2;fc_FindOp");
            }
        }

        public static void fc_AddTaches(int lEQU_Noauto, object lEQM_NoAuto, int lCop_NoAuto, string sCodeImmeuble)
        {
            DataTable rs = default(DataTable);
            DataTable rsAdd = default(DataTable);
            string sSQL = null;
            try
            {
                if (lEQU_Noauto == 0)
                {
                    return;
                }

                sSQL = "SELECT     NAP2_Noauto, NAP2_Libelle, MAP2_NoLigne, EQU_NoAuto" + " From NAP2_NatureOpP2" + " WHERE   EQU_NoAuto =" + lEQU_Noauto;
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                sSQL = "SELECT   FAP_Designation1, EQM_NoAuto, " + " FAP_NoLIgne, COP_NoAuto, CodeImmeuble, NAP2_Noauto " + " From FAP_FacArticleP2"
                    + " WHERE     (FAP_NoAuto = 0)";
                var tmpAdorsAdd = new ModAdo();
                rsAdd = tmpAdorsAdd.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                    foreach (DataRow rsRow in rs.Rows)
                    {
                        //rsAdd.AddNew();
                        var NewRow = rsAdd.NewRow();
                        NewRow["FAP_Designation1"] = rsRow["NAP2_Libelle"];
                        NewRow["NAP2_Noauto"] = rsRow["NAP2_Noauto"];
                        NewRow["FAP_Noligne"] = rsRow["MAP2_NoLigne"];
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["EQM_NoAuto"] = lEQM_NoAuto;
                        rsAdd.Rows.Add(NewRow);
                        tmpAdorsAdd.Update();

                        //rs.MoveNext();
                    }

                rs.Dispose();
                rsAdd.Dispose();

                rsAdd = null;
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModP2;fc_AddTaches");
            }
        }

        private static void fc_AddPlanning(int lEQU_Noauto, object lEQM_NoAuto, int lCop_NoAuto, string sCodeImmeuble,
         double dbDuree, double dbDureeReel, string sDateDebPlanif = "")
        {

            string sSQL = null;
            DataTable rs = default(DataTable);
            DataTable rsAdd = default(DataTable);

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (lEQU_Noauto == 0)
                {
                    return;
                }

                sSQL = "SELECT PEG_NoAuto, EQU_NoAuto, TPP_Code, PEG_JD, PEG_MD," + " PEG_AnD, PEG_JF, PEG_MF, PEG_AnF, SEM_Code, PEG_Duree,"
                    + " PEG_Visite, PEG_Tech, " + " PEG_Cout, PEG_ForceVisite, PEG_TotalHT," + " PEG_DureeReel, PEG_APlanifier, PEG_Calcul,"
                    + " PEG_AvisPassage, PEG_AvisNbJour, PEG_KFO, PEG_KMO," + " PEG_MtAchat , PEG_NbAn" + " From PEG_PeriodeGamme"
                    + " WHERE EQU_NoAuto =" + lEQU_Noauto;
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT  CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code," + " PEI_JD, PEI_MD, PEI_AnD, PEI_JF, PEI_MF, PEI_AnF,"
                    + " SEM_Code, PEI_Duree, PEI_Visite, " + " PEI_Tech, PEI_Cout, PEI_ForceVisite, COP_NoContrat,"
                    + " PEI_TotalHT, PEI_DureeReel, PEI_Intervenant, PEI_APlanifier," + " PEI_Calcul, PEI_IntAutre,"
                    + " COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour," + " PEI_KFO, PEI_KMO, PEI_Heure, PEI_ForceJournee," + " PEI_MtAchat,  "
                    + "  PEI_NbAn, PEI_DateDebut, PEI_DateFin," + " PEI_Samedi, PEI_Dimanche, PEI_Pentecote, PEI_Jourferiee, PEI_DureeVisu, PEI_DateDebut"
                    + " FROM         PEI_PeriodeGammeImm" + " WHERE PEI_Noauto = 0";
                var tmpAdorsAdd = new ModAdo();
                rsAdd = tmpAdorsAdd.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow rsRows in rs.Rows)
                    {
                        //PEI_NoAuto c'est la clé primaire je dois ajouter le code pour avoir le dernier id dans la table 
                        var NewRow = rsAdd.NewRow();
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["EQM_NoAuto"] = lEQM_NoAuto;
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        //rsAdd!PEI_Duree = rs!PEG_Duree
                        //rsAdd!PEI_DureeReel = rs!PEG_DureeReel
                        NewRow["PEI_Duree"] = dbDuree;
                        NewRow["PEI_DureeReel"] = dbDureeReel;
                        NewRow["PEI_DureeVisu"] = DateTime.Now.AddSeconds(dbDureeReel * 3600).ToString("HH:mm:ss");//Verifier
                        NewRow["PEI_AvisPassage"] = rsRows["PEG_AvisPassage"];
                        NewRow["PEI_AvisNbJour"] = rsRows["PEG_AvisNbJour"];
                        NewRow["TPP_Code"] = rsRows["TPP_Code"];
                        NewRow["PEI_JD"] = rsRows["PEG_JDv"];
                        NewRow["PEI_MD"] = rsRows["PEG_MD"];
                        NewRow["PEI_And"] = rsRows["PEG_AnD"];
                        NewRow["PEI_JF"] = rsRows["PEG_JF"];
                        NewRow["PEI_MF"] = rsRows["PEG_MF"];
                        NewRow["PEI_Anf"] = rsRows["PEG_AnF"];
                        NewRow["SEM_Code"] = rsRows["SEM_Code"];

                        //         rsAdd!PEI_Heure = rs!
                        //         rsAdd!PEI_ForceJournee = rs!
                        //         rsAdd!PEI_DateDebut = rs!
                        NewRow["PEI_Samedi"] = tGmao.lSamedi;
                        NewRow["PEI_Dimanche"] = tGmao.lDimanche;
                        NewRow["PEI_Pentecote"] = tGmao.lPentecote;
                        NewRow["PEI_Jourferiee"] = tGmao.lJoursFeriees;


                        NewRow["PEI_Visite"] = rsRows["PEG_Visite"];
                        NewRow["PEI_tech"] = rsRows["PEG_Tech"];
                        NewRow["PEI_ForceVisite"] = rsRows["PEG_ForceVisite"];

                        //rsAdd!COP_NoContrat = rs!
                        //rsAdd!PEI_Intervenant = rs!
                        //rsAdd!PEI_IntAutre = rs!

                        //=== PEI_APlanifier ==>n'est pas utilisée (servait à ne pas planifier mais a afficher dans le contrat.
                        //rsAdd!PEI_APlanifier = rs!PEG_APlanifier

                        NewRow["PEI_NbAn"] = rsRows["PEG_NbAn"];
                        NewRow["PEI_Cout"] = rsRows["PEG_Cout"];
                        NewRow["PEI_Calcul"] = rsRows["PEG_Calcul"];
                        NewRow["PEI_KFO"] = rsRows["PEG_KFO"];
                        NewRow["PEI_KMO"] = rsRows["PEG_KMO"];
                        NewRow["PEI_TotalHT"] = rsRows["PEG_TotalHT"];
                        NewRow["PEI_MtAchat"] = rsRows["PEG_MtAchat"];

                        if (General.IsDate(sDateDebPlanif))
                        {
                            NewRow["PEI_DateDebut"] = Convert.ToDateTime(sDateDebPlanif);
                        }
                        else
                        {
                            NewRow["PEI_DateDebut"] = System.DBNull.Value;
                        }
                        rsAdd.Rows.Add(NewRow);
                        tmpAdorsAdd.Update();
                        //rs.MoveNext();
                    }
                }
                rs.Dispose();
                rsAdd.Dispose();
                rsAdd = null;
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "MODP2;fc_AddPlanning;");
            }
        }


        public static void fc_CreeGammeFille(string sWhere, int PB1, object lblInsertion, string txtCodeImmeuble, int lCop_NoAuto)
        {
            int i = 0;
            int j = 0;
            double dbBar = 0;
            DataTable rs = default(DataTable);

            //lblInsertion.Visible = True
            try
            {
                PB1 = 0;
                lblInsertion = "Insertion des gammes filles en cours";
                General.sSQL = "SELECT  EQM_Code, EQM_Libelle, ICC_Noauto, EQM_Observation, CodeImmeuble" + " , EQM_NoLigne, COP_NoAuto, EQU_NoAuto"
                    + " From EQM_EquipementP2Imm" + " WHERE     (ICC_Noauto = 0)";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);

                //sSQl = "SELECT ICA_ImmCategorieInterv.CodeImmeuble, ICA_ImmCategorieInterv.CAI_Noauto," _
                //& " EQU_EquipementP2.EQU_Code, EQU_EquipementP2.EQU_Libelle, " _
                //& " EQU_EquipementP2.EQU_Observation" _
                //& " FROM ICA_ImmCategorieInterv INNER JOIN " _
                //& " CAI_CategoriInterv ON ICA_ImmCategorieInterv.CAI_Code =" _
                //& " CAI_CategoriInterv.CAI_Code INNER JOIN" _
                //& " EQU_EquipementP2 ON " _
                //& " CAI_CategoriInterv.CAI_Noauto = EQU_EquipementP2.CAI_Noauto LEFT OUTER JOIN" _
                //& " EQM_EquipementP2Imm ON ICA_ImmCategorieInterv.CAI_Noauto" _
                //& " = EQM_EquipementP2Imm.CAI_Noauto" _
                //& " WHERE  (ICA_ImmCategorieInterv.CodeImmeuble ='" & txtCodeImmeuble & "')" _
                //& " AND (EQM_EquipementP2Imm.EQM_NoAuto IS NULL)"


                //-- Recupére les gammes filles dans la table des paramétres

                General.sSQL = "SELECT ICC_IntervCategorieContrat.CodeImmeuble, ICC_IntervCategorieContrat.ICC_Noauto,"
                    + " EQU_EquipementP2.EQU_Code, EQU_EquipementP2.EQU_Libelle," + " EQU_EquipementP2.EQU_Observation ,EQU_EquipementP2.EQU_NoLigne"
                    + ", ICC_IntervCategorieContrat.CAI_Code,EQU_EquipementP2.EQU_NoAuto " + " FROM ICC_IntervCategorieContrat INNER JOIN"
                    + " CAI_CategoriInterv ON ICC_IntervCategorieContrat.CAI_Code =" + " CAI_CategoriInterv.CAI_Code INNER JOIN"
                    + " EQU_EquipementP2 ON CAI_CategoriInterv.CAI_Noauto = EQU_EquipementP2.CAI_Noauto"
                    + " where  ICC_IntervCategorieContrat.COP_NoAuto =" + General.nz(lCop_NoAuto, 0) + "" + sWhere;
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(General.sSQL);

                if (rs.Rows.Count == 0 || rs.Rows.Count == -1)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }
                dbBar = 100 / rs.Rows.Count;
                sWhere = "";
                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow rsRow in rs.Rows)
                    {
                        //rstmp.AddNew();
                        var NewRow = rstmp.NewRow();
                        NewRow["EQM_CODE"] = rsRow["EQU_Code"];
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //sWhere = "and ( EQM_Code ='" & gFr_DoublerQuote(rs!EQU_Code) & "'"
                            sWhere = "and ( EQU_EquipementP2.EQU_NoAuto =" + rsRow["EQU_NoAuto"] + "";
                        }
                        else
                        {
                            //sWhere = sWhere & " or EQM_Code ='" & gFr_DoublerQuote(rs!EQU_Code) & "'"
                            sWhere = sWhere + " or EQU_EquipementP2.EQU_NoAuto ='" + rsRow["EQU_NoAuto"] + "'";
                        }
                        NewRow["EQM_Libelle"] = rsRow["EQU_Libelle"];
                        NewRow["ICC_Noauto"] = rsRow["ICC_Noauto"];
                        NewRow["EQM_Observation"] = rsRow["EQU_Observation"];
                        NewRow["CodeImmeuble"] = rsRow["CodeImmeuble"];
                        NewRow["EQM_Noligne"] = General.nz(rsRow["EQU_NoLigne"], 0);
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        NewRow["EQU_NoAuto"] = rsRow["EQU_NoAuto"];
                        rstmp.Rows.Add(NewRow);
                        tmpAdorstmp.Update();

                        if (PB1 + dbBar >= 100)
                        {
                            PB1 = 100;
                        }
                        else
                        {
                            PB1 = PB1 + Convert.ToInt16(dbBar);
                        }
                        // rs.MoveNext();
                    }
                    sWhere = sWhere + ")";
                }

                lblInsertion = "Insertion du planning";

                General.sSQL = "SELECT  CodeImmeuble, EQM_NoAuto, TPP_Code, PEI_JD," + " PEI_MD, PEI_JF, PEI_MF, SEM_Code, PEI_Duree,"
                        + " PEI_Visite, PEI_Tech, PEI_Cout, PEI_ForceVisite, PEI_TotalHT, " + " PEI_DureeReel, PEI_APlanifier, PEI_Calcul, COP_NoAuto, "
                        + " PEI_AvisPassage, PEI_AvisNbJour, PEI_KFO, PEI_KMO" + " FROM         PEI_PeriodeGammeImm" + "  where  PEI_NoAuto = 0";
                rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);

                //sSQl = "SELECT  EQM_EquipementP2Imm.CodeImmeuble, EQM_EquipementP2Imm.EQM_NoAuto," _
                //& " PEG_PeriodeGamme.TPP_Code, PEG_PeriodeGamme.PEG_JD, " _
                //& " PEG_PeriodeGamme.PEG_MD, PEG_PeriodeGamme.PEG_JF, " _
                //& " PEG_PeriodeGamme.PEG_MF, PEG_PeriodeGamme.SEM_Code," _
                //& " PEG_PeriodeGamme.PEG_Duree , PEG_PeriodeGamme.PEG_Visite," _
                //& " PEG_PeriodeGamme.PEG_Tech, PEG_PeriodeGamme.PEG_Cout " _
                //& " , PEG_ForceVisite , PEG_TotalHT" _
                //& " FROM EQM_EquipementP2Imm INNER JOIN" _
                //& " PEG_PeriodeGamme INNER JOIN" _
                //& " EQU_EquipementP2 ON PEG_PeriodeGamme.EQU_NoAuto = EQU_EquipementP2.EQU_NoAuto ON" _
                //& " EQM_EquipementP2Imm.EQM_Code = EQU_EquipementP2.EQU_Code" _
                //& " WHERE EQM_EquipementP2Imm.COP_NoAuto = '" & nz(lCop_NoAuto, 0) & "'" _
                //& sWhere

                //--Modif rachid 09 nov 2003.
                //--Rechercher les périodicités des gammes filles dans les tables paramétres
                //--par rapport aux gammes éxistantes dans le contrat.

                General.sSQL = "SELECT  EQM_EquipementP2Imm.CodeImmeuble, EQM_EquipementP2Imm.EQM_NoAuto," + " PEG_PeriodeGamme.TPP_Code, PEG_PeriodeGamme.PEG_JD, "
                        + " PEG_PeriodeGamme.PEG_MD, PEG_PeriodeGamme.PEG_JF, " + " PEG_PeriodeGamme.PEG_MF, PEG_PeriodeGamme.SEM_Code,"
                        + " PEG_PeriodeGamme.PEG_Duree , PEG_PeriodeGamme.PEG_Visite," + " PEG_PeriodeGamme.PEG_Tech, PEG_PeriodeGamme.PEG_Cout "
                        + " , PEG_ForceVisite , PEG_TotalHT," + "  PEG_DureeReel, PEG_APlanifier," + " PEG_Calcul,  PEG_AvisPassage, PEG_AvisNbJour,"
                        + " PEG_KFO, " + " PEG_KMO " + " FROM         EQM_EquipementP2Imm INNER JOIN" + " PEG_PeriodeGamme INNER JOIN"
                        + " EQU_EquipementP2 ON PEG_PeriodeGamme.EQU_NoAuto = EQU_EquipementP2.EQU_NoAuto ON"
                        + " EQM_EquipementP2Imm.EQU_NoAuto = EQU_EquipementP2.EQU_NoAuto INNER JOIN"
                        + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto "
                        + " WHERE ICC_IntervCategorieContrat.COP_NoAuto = '" + General.nz(lCop_NoAuto, 0) + "'" + sWhere;


                rs = tmpAdors.fc_OpenRecordSet(General.sSQL);
                if (rs.Rows.Count == 0 || rs.Rows.Count == -1)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    // Exit Sub
                }
                else
                {
                    dbBar = 100 / rs.Rows.Count;
                    sWhere = "";
                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow rsRow in rs.Rows)
                        {
                            //rstmp.AddNew();
                            var NewRow = rstmp.NewRow();
                            NewRow["CodeImmeuble"] = txtCodeImmeuble;
                            NewRow["EQM_NoAuto"] = rsRow["EQM_NoAuto"];
                            NewRow["TPP_Code"] = rsRow["TPP_Code"];
                            NewRow["PEI_JD"] = rsRow["PEG_JD"];
                            NewRow["PEI_MD"] = rsRow["PEG_MD"];
                            NewRow["PEI_JF"] = rsRow["PEG_JF"];
                            NewRow["PEI_MF"] = rsRow["PEG_MF"];
                            NewRow["SEM_Code"] = rsRow["SEM_Code"];
                            NewRow["PEI_Duree"] = rsRow["PEG_Duree"];
                            NewRow["PEI_Visite"] = rsRow["PEG_Visite"];
                            NewRow["PEI_tech"] = rsRow["PEG_Tech"];
                            NewRow["PEI_Cout"] = rsRow["PEG_Cout"];
                            NewRow["PEI_ForceVisite"] = rsRow["PEG_ForceVisite"];
                            NewRow["PEI_TotalHT"] = rsRow["PEG_TotalHT"];
                            NewRow["PEI_DureeReel"] = rsRow["PEG_DureeReel"];
                            NewRow["PEI_APlanifier"] = rsRow["PEG_APlanifier"];
                            NewRow["PEI_Calcul"] = rsRow["PEG_Calcul"];
                            NewRow["COP_NoAuto"] = lCop_NoAuto;
                            NewRow["PEI_AvisPassage"] = rsRow["PEG_AvisPassage"];
                            NewRow["PEI_AvisNbJour"] = rsRow["PEG_AvisNbJour"];
                            NewRow["PEI_KFO"] = rsRow["PEG_KFO"];
                            NewRow["PEI_KMO"] = rsRow["PEG_KMO"];
                            rstmp.Rows.Add(NewRow);
                            tmpAdorstmp.Update();

                            if ((PB1 + dbBar) >= 100)
                            {
                                PB1 = 100;
                            }
                            else
                            {
                                PB1 = PB1 + Convert.ToInt16(dbBar);
                            }
                            //rs.MoveNext();
                        }
                    }
                }
                lblInsertion = "Insertion des Taches";
                General.sSQL = "SELECT FAP_CodeArticle, FAP_Designation1, FAP_NoAuto," + " EQM_NoAuto, CodeImmeuble,FAP_NoLIgne,COP_NoAuto "
                        + " From FAP_FacArticleP2" + " WHERE     (FAP_NoAuto = 0)";
                rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);

                //sSQl = "SELECT EQM_EquipementP2Imm.CodeImmeuble, EQM_EquipementP2Imm.EQM_NoAuto," _
                //& " FacArticle.CodeArticle, FacArticle.Designation1, " _
                //& " FacArticle.FAC_NoAuto, FacArticle.Fac_Noligne " _
                //& " FROM EQM_EquipementP2Imm INNER JOIN" _
                //& " EQU_EquipementP2 ON EQM_EquipementP2Imm.EQM_Code" _
                //& " = EQU_EquipementP2.EQU_Code INNER JOIN" _
                //& " FacArticle ON EQU_EquipementP2.EQU_NoAuto = FacArticle.EQU_NoAuto" _
                //& " WHERE     (EQM_EquipementP2Imm.CodeImmeuble = '" & gFr_DoublerQuote(txtCodeImmeuble) & "')" _
                //& sWhere

                //--Modif rachid 09 nov 2003.
                //--Rechercher les taches des gammes filles dans les tables paramétres
                //--par rapport aux gammes éxistantes dans le contrat.

                General.sSQL = "SELECT EQM_EquipementP2Imm.CodeImmeuble, EQM_EquipementP2Imm.EQM_NoAuto," + " FacArticle.CodeArticle, FacArticle.Designation1, "
                        + " FacArticle.FAC_NoAuto, FacArticle.Fac_Noligne " + " FROM         EQM_EquipementP2Imm INNER JOIN"
                        + " EQU_EquipementP2 ON EQM_EquipementP2Imm.EQU_NoAuto = EQU_EquipementP2.EQU_NoAuto INNER JOIN"
                        + " FacArticle ON EQU_EquipementP2.EQU_NoAuto = FacArticle.EQU_NoAuto INNER JOIN"
                        + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto"
                        + " WHERE     (ICC_IntervCategorieContrat.COP_NoAuto = '" + General.nz(lCop_NoAuto, 0) + "')" + sWhere;

                rs = tmpAdors.fc_OpenRecordSet(General.sSQL);
                if (rs.Rows.Count == 0 || rs.Rows.Count == -1)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }
                dbBar = 100 / rs.Rows.Count;
                sWhere = "";
                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow rsRow in rs.Rows)
                    {
                        //rstmp.AddNew();
                        var NewRow = rstmp.NewRow();
                        NewRow["FAP_CodeArticle"] = rsRow["CodeArticle"];
                        NewRow["FAP_Designation1"] = rsRow["Designation1"];
                        //rsTmp!FAP_NoAuto = rs!FAC_NoAuto
                        NewRow["EQM_NoAuto"] = rsRow["EQM_NoAuto"];
                        NewRow["CodeImmeuble"] = txtCodeImmeuble;
                        NewRow["FAP_Noligne"] = rsRow["FAC_NoLigne"];
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        rstmp.Rows.Add(NewRow);
                        tmpAdorstmp.Update();

                        if ((PB1 + dbBar) >= 100)
                        {
                            PB1 = 100;
                        }
                        else
                        {
                            PB1 = PB1 + Convert.ToInt16(dbBar);
                        }
                        //rs.MoveNext();
                    }
                }
                rs.Dispose();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2;fc_creeGammeFille");
            }
            //Resume Next
        }
        public static void fc_JFeriee(ref System.DateTime dDate, ref string[] tsJferiee)
        {
            int i = 0;
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo ModAdors = new ModAdo();

            sSQL = "SELECT     JFE_Jour, JFE_Mois" + " FROM JFE_Feriee";

            rs = ModAdors.fc_OpenRecordSet(sSQL);

            foreach (DataRow r in rs.Rows)
            {
                Array.Resize(ref tsJferiee, i + 1);
                tsJferiee[i] = r["JFE_Jour"] + "/" + r["JFE_Mois"] + "/" + dDate.Year;
                i = i + 1;
                // rs.MoveNext();
            }

            rs.Dispose();

            rs = null;
        }
    }
}
