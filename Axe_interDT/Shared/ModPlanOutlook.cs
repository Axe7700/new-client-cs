﻿using Axe_interDT.Views.Planning;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using XtremeCalendarControl;

namespace Axe_interDT.Shared
{
    class ModPlanOutlook
    {
        public struct IdEvent
        {
            public int lIdEvent;
            public int lNoInter;
            public string sTypeTable;
        }
        public struct IntervenantPlan
        {
            public int lNoauto;
            public string sPrenom;
            public string sNom;
        }
        public static bool bPlanUpdate;
        public static IdEvent[] tpEvent;
        public const double cPicForm_Width = 15135;
        public const double cPicForm_Height = 36680.02;

        public const double cFraConteneur_Width = 15135;
        public const double cFraConteneur_Height = 8295;



        public const double cCal_Width = 14895;
        public const double cCal_Height = 8175;
        public const string ctbInter = "Intervention";
        public const string ctbPLP = "PLP_PlanningPevtabsonnel";

        public static bool bStopTimerPlan;

        public const string cSansIntervenant = "Sans intervenant";

        //vert
        public const int cColorEtat03 = 0xc0ffc0;
        //rouge
        public const int cColorEtat04 = 0xaab3ff;
        public const short cDureeTimer = 2000;
        public const string cCustomPlanInt = "NoIntervention";
        public const string cCustomPlanInterv = "Intervenant";



        public const string cChampDuree = "Duree";
        public const string cChampDureeReal = "DureeRealise";
        public const string cChampDureePrevue = "DureePrevue";
        public const string cChampsStatut = "Statut";

        public const string cChampHeureDeb = "HeureDebut";
        public const string cChampHeureDebReal = "HeureDebutReal";
        public const string cChampHeureDebPrevue = "HeurePrevue";

        public const string cChampDate = "ChampsDate";
        public const string cChampDateReal = "DateRealise";
        public const string cChampDatePrevue = "DatePrevue";
        public const string cChampDateSaisie = "DateSaisie";


        public static int[] iTabEvent;

        public static string sWherePLanOut;


        public const string cCJEchelleTemp = "EchelleTemps";
        public const string cCJJourHeureMin = "JourHeureMin";
        public const string cCJJourHeureMax = "JourHeureMax";
        public const string cCalProvider = "Provider=Custom;DSN=LOCAL_AXECIEL";

        private const string cMinDuree = "1";
        private static readonly DateTime cHeureDebMin = new DateTime(0001, 01, 01, 08, 00, 00); //TODO . timespan or date cannot be declared const so i change const by readonly

        public static System.DateTime dtFivtabtDayVisible { get; set; }
        public static System.DateTime dtLastDayVisible;
        public static string sPlanningStatut;
        public static string sPlanningInterv;
        public static string sPlanningGroupe;
        public static int lSaveOldViewType;

        public const int cNumFicheStandard = 0;
        public const int cNoIntervention = 1;
        public const int cCodeImmeuble2 = 2;
        public const int cDateSaisie = 3;
        public const int cArticle = 4;
        public const int cDesignation = 5;
        public const int cCommentaire = 6;
        public const int cIntervenant = 7;
        public const int cDatePrevue = 8;
        public const int cHeurePrevue = 9;
        public const int cDureePrevue = 10;
        public const int cDateRealise = 11;
        public const int cHeureDebut2 = 12;
        public const int cHeureFin = 13;
        public const int cDuree = 14;
        public const int cCodeEtat = 15;
        public const int cNom = 16;
        public const int cPrenom = 17;
        public const int cNoMaj = 18;
        public const int cCouleurStatut = 19;
        public const int cCouleurDeFond = 20;
        public const int cVille = 21;
        public const int cNomInterv = 22;
        public const int cNomPrenom = 23;
        public const int cNoautoInterv = 24;

        public static DateTime dtFirstDayVisible;

        private UserDocPlanningCodeJock userDocPlanningCodeJock;



        public static void fc_GetSettingCodejock(UserDocPlanningCodeJock UserDocPlanningCodeJock)
        {
            object xtpCalendarWorkWeekView = null;
            object xtpCalendarThemeOffice2013 = null;
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            string stemp = null;
            int lVewType = 0;
            int lWidthCol;
            string sVewType;

            try
            {
                UserDocPlanningCodeJock.txtIntervenant.Text = General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.txtIntervenant.Name, "");

                UserDocPlanningCodeJock.CmbGroupe.Text = General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.CmbGroupe.Name, "");

                UserDocPlanningCodeJock.txtCodeEtatsInclus.Text = General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.txtCodeEtatsInclus.Name, "");

                //TODO 
                //UserDocPlanningCodeJock.PicForm.Width =
                //    Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name,
                //        UserDocPlanningCodeJock.PicForm.Name + "_Width", cPicForm_Width.ToString()));

                //UserDocPlanningCodeJock.PicForm.Height =
                //    Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name,
                //        UserDocPlanningCodeJock.PicForm.Name + "_Height", cPicForm_Height.ToString()));


                UserDocPlanningCodeJock.FraConteneur.Width = Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.FraConteneur.Name + "_Width", cFraConteneur_Width.ToString()));

                UserDocPlanningCodeJock.FraConteneur.Height = Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.FraConteneur.Name + "_Height", cFraConteneur_Height.ToString()));

                UserDocPlanningCodeJock.Cal.Width = Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.Cal.Name + "_Width", cCal_Width.ToString()));

                UserDocPlanningCodeJock.Cal.Height = Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.Cal.Name + "_Height", cCal_Height.ToString()));

                UserDocPlanningCodeJock.Cal.SetDataProvider(cCalProvider);
                //"Provider=Custom;DSN=LOCAL_GESTEN"

                if (!UserDocPlanningCodeJock.Cal.DataProvider.Open())
                {
                    UserDocPlanningCodeJock.Cal.DataProvider.Create();
                }

                UserDocPlanningCodeJock.Cal.DataProvider.Open();

                //=== applique un theme au calendrier.

                UserDocPlanningCodeJock.Cal.VisualTheme = CalendarTheme.xtpCalendarThemeOffice2013;

                lWidthCol = Convert.ToInt32(General.getFrmReg(UserDocPlanningCodeJock.Name, "Col_Width", 0 + ""));

                UserDocPlanningCodeJock.Cal.DayView.MinColumnWidth = 0;

                if (lWidthCol != 0)
                {
                    UserDocPlanningCodeJock.Cal.DayView.MinColumnWidth = lWidthCol;
                }

                UserDocPlanningCodeJock.Cal.RedrawControl();

                //CalendarControl.VisualTheme = xtpCalendarThemeOffice2003

                //==== calendrier en mode de lecture.
                //CalendarControl.ReadOnlyMode = True
                //CalendarControl.RedrawControl

                //=== change les libéllées de la barre de controle.
                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(0, "Jour");

                //=== change les libéllées de la barre de controle.
                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(1, "Semaine");

                //=== change les libéllées de la barre de controle.
                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(2, "Mois");

                //CalendarControl.CaptionButtonTitle 3, "Affichage Planification"

                //=== n'affiche pas la case semaine de travail (chaine vide dans sTitle)
                //CalendarControl.CaptionButtonTitle(5, "");
                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(4, "Semaine de travail");

                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(5, "Semaine");
                UserDocPlanningCodeJock.Cal.CaptionButtonTitle(5, "Grilles");

                UserDocPlanningCodeJock.Cal.ShowTooltipForAllDayEvents = true;

                UserDocPlanningCodeJock.Cal.ShowMultiColumnsButton = true;
                //CalendarControl.ShowTimelineButton = True

                //=== provoque l'evenement WievChanged.
                //CalendarControl.ViewType = xtpCalendarWorkWeekView

                //CalendarControl.ActiveView = xtpCalendarWorkWeekView
                //CalendarControl.ShowCaptionBavtabcrollDateButtons = True
                //CalendarControl.ShowTooltipForAllDayEvents = False
                //CalendarControl.ShowWhatsThis

                //=== déasctive les rappels.
                UserDocPlanningCodeJock.Cal.EnableReminders(false);

                //CalendarControl.ReadOnlyMode = True

                //stemp = General.getFrmReg( UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.DTDateRealiseDe.Name, "")

                //stemp = General.getFrmReg(UserDocPlanningCodeJock.Name, "DateDebutVue", "");

                //if (string.IsNullOrEmpty(stemp))
                //{
                //    //=== par defaut on active la semaine en couvtab
                //    dtDateDeb = fDate.fc_ReturnDayPrevious(DateAndTime.Today, modP2.cSLundi);
                //    dtDateFin = fDate.fc_ReturnDayNext(DateAndTime.Today, modP2.cSVendredi);
                //    UserDocPlanningCodeJock.DTDateRealiseDe.Value = dtDateDeb;
                //    UserDocPlanningCodeJock.DTDateRealiseAu.Value = dtDateFin;


                //}
                //else if (General.IsDate(stemp))
                //{
                //    dtDateDeb = Convert.ToDateTime(stemp);
                //    UserDocPlanningCodeJock.DTDateRealiseDe.Value = dtDateDeb;

                //    //stemp = General.getFrmReg( UserDocPlanningCodeJock.Name, UserDocPlanningCodeJock.DTDateRealiseAu.Name, "")
                //    stemp = Interaction.General.getFrmReg(General. UserDocPlanningCodeJock.Name, "DateFinVue", "");
                //    if (string.IsNullOrEmpty(stemp))
                //    {
                //        dtDateFin = fDate.fc_ReturnDayNext(dtDateDeb, modP2.cSVendredi);
                //        UserDocPlanningCodeJock.DTDateRealiseAu.Value = dtDateFin;
                //    }
                //    else if (General.IsDate(stemp))
                //    {
                //        dtDateFin = Convert.ToDateTime(stemp);
                //        UserDocPlanningCodeJock.DTDateRealiseAu._Value = dtDateFin;
                //    }
                //    else
                //    {
                //        dtDateFin = fDate.fc_ReturnDayNext(dtDateDeb, modP2.cSVendredi);
                //        UserDocPlanningCodeJock.DTDateRealiseAu.Value = dtDateFin;
                //    }


                //}
                //else
                //{
                //    dtDateDeb = fDate.fc_ReturnDayPrevious(DateAndTime.Today, modP2.cSLundi);
                //    dtDateFin = fDate.fc_ReturnDayNext(DateAndTime.Today, modP2.cSDimanche);
                //    UserDocPlanningCodeJock.DTDateRealiseDe.Value = dtDateDeb;
                //    UserDocPlanningCodeJock.DTDateRealiseAu.Value = dtDateFin;
                //}

                sVewType = General.getFrmReg("UserDocPlanningCodeJock", "ViewType_Cal", "");

                if (string.IsNullOrEmpty(sVewType))
                {
                    lVewType = (int)CalendarViewType.xtpCalendarMonthView;//constante =3
                }
                else
                {
                    lVewType = Convert.ToInt32(sVewType);
                    dtDateDeb = Convert.ToDateTime(General.getFrmReg(UserDocPlanningCodeJock.Name, "DateDebutPicker", DateTime.Now.ToString()));
                }

                UserDocPlanningCodeJock.Cal.DayView.TimeScale = Convert.ToInt32(General.getFrmReg("App", cCJEchelleTemp, 30 + ""));
                UserDocPlanningCodeJock.Cal.DayView.TimeScaleMinTime = Convert.ToDateTime(General.getFrmReg("App", cCJJourHeureMin, "8:00:00"));//#8:00:00 AM#
                UserDocPlanningCodeJock.Cal.DayView.TimeScaleMaxTime = Convert.ToDateTime(General.getFrmReg("App", cCJJourHeureMax, "19:00:00"));//#7:00:00 PM#

                //'=== desactive le menu ajout du planning 'Click to add appointment'
                UserDocPlanningCodeJock.Cal.Options.EnableAddNewTooltip = false;

                //'=== desactive l'ajout de rendez vous pris en saisisant une lettre sur le clavier.
                UserDocPlanningCodeJock.Cal.Options.EnableInPlaceCreateEvent = false;

                //'CalendarControl.Populate
                UserDocPlanningCodeJock.Cal.DayView.ScrollToWorkDayBegin();





                //'+++++++++++++++++++++++++DATEPICKER---------------------------------------



                UserDocPlanningCodeJock.DateCal.ResyncCalendar();

                UserDocPlanningCodeJock.DateCal.AttachToCalendar((XtremeCalendarControl.CalendarControl)UserDocPlanningCodeJock.Cal.GetOcx());

                //'=== met en surbtrillance la date du jour.
                UserDocPlanningCodeJock.DateCal.HighlightToday = true;

                //'=== change le libélle du bouton today.
                UserDocPlanningCodeJock.DateCal.TextTodayButton = "Aujourd'hui";

                //'DatePickerControl.TextNoneButton = ""
                //'DatePickerControl.BoldDaysWithEvents = True

                //'=== masque le bouton none.
                UserDocPlanningCodeJock.DateCal.ShowNoneButton = false;

                UserDocPlanningCodeJock.DateCal.ShowTodayButton = false;

                //'=== affiche tous les jouvtab possibles dans la datapicker.
                UserDocPlanningCodeJock.DateCal.ShowNonMonthDays = true;
                UserDocPlanningCodeJock.DateCal.BoldDaysWithEvents = true;

                //'=== info bulle.
                UserDocPlanningCodeJock.Cal.CustomFormat4Tooltip = "<TIME>" + "\r\n" + "<SUBJ> " + "<BODY>";

                if (!string.IsNullOrEmpty(sVewType))
                {
                    dtDateDeb = Convert.ToDateTime(General.getFrmReg(UserDocPlanningCodeJock.Name, "DateDebutPickerSelecttion", DateTime.Now.ToString()));
                    dtDateFin = Convert.ToDateTime(General.getFrmReg(UserDocPlanningCodeJock.Name, "DateFinPickerSelection", DateTime.Now.ToString()));

                    if (sVewType == CalendarViewType.xtpCalendarWorkWeekView.ToString() ||
                        sVewType == CalendarViewType.xtpCalendarWeekView.ToString())
                    {
                        dtDateDeb = fDate.fc_ReturnDayPrevious(DateTime.Now, modP2.cSLundi);
                        dtDateFin = fDate.fc_ReturnDayNext(DateTime.Now, modP2.cSVendredi);

                    }

                    UserDocPlanningCodeJock.Cal.DayView.ShowDays(dtDateDeb, dtDateFin);

                    // '=== Attention cette affectation declenche plusieurs evenement (qui permet de rafraichir la grille notamment).
                    if (Convert.ToInt32(UserDocPlanningCodeJock.Cal.ViewType) == lVewType)
                    {
                        if (sVewType == CalendarViewType.xtpCalendarWorkWeekView.ToString() ||
                            sVewType == CalendarViewType.xtpCalendarWeekView.ToString())
                        {
                            UserDocPlanningCodeJock.fc_refresh(false, true, dtDateDeb, dtDateFin);
                        }
                        else
                        {
                            UserDocPlanningCodeJock.fc_refresh();
                        }
                    }
                    else
                    {
                        UserDocPlanningCodeJock.Cal.ViewType = (CalendarViewType)lVewType;
                    }


                    // 'CalendarControl.ViewType = xtpCalendarDayView


                    //    ' CalendarControl.DayView.ShowDays dtDateDeb, dtDateFin
                    //'    DatePickerControl.SelectRange dtDateDeb, dtDateFin
                    //'     'DatePickerControl.MonthDelta = Month(dtDateDeb)
                    //   ' CalendarControl.RedrawControl

                    UserDocPlanningCodeJock.DateCal.ResyncCalendar();

                    //'    DatePickerControl.SelectRange dtDateDeb, dtDateFin 
                }

                //'CalendarControl.DayView.TimeScale = GetSetting(cFrNomApp, "App", cCJEchelleTemp, 30)
                //'CalendarControl.DayView.TimeScaleMinTime = GetSetting(cFrNomApp, "App", cCJJourHeureMin, #8:00:00 AM#)
                //'CalendarControl.DayView.TimeScaleMaxTime = GetSetting(cFrNomApp, "App", cCJJourHeureMax, #7:00:00 PM#)
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_GetSettingCodejock");

            }
        }
        public static void fc_SaveSettingCodejock(UserDocPlanningCodeJock UserDocPlanningCodeJock)
        {

            try
            {
                General.saveInReg("App", cCJEchelleTemp, UserDocPlanningCodeJock.Cal.DayView.TimeScale.ToString());

                General.saveInReg("App", cCJJourHeureMin, UserDocPlanningCodeJock.Cal.DayView.TimeScaleMinTime.ToString());

                General.saveInReg("App", cCJJourHeureMax, UserDocPlanningCodeJock.Cal.DayView.TimeScaleMaxTime.ToString());
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_GetSettingCodejock");
            }
        }
        public static void fc_SetWherePlanOut(string sVal)
        {

            try
            {
                sWherePLanOut = sVal;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_SetWherePlanOut");
            }

        }

        public static string fc_GetWherePlanOut()
        {
            string functionReturnValue = null;
            try
            {

                functionReturnValue = sWherePLanOut;
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_GetWherePlanOut");
                return functionReturnValue;
            }
        }

        public static int fc_ReturnNo(ref int lIdEvent, ref string stable)
        {
            int functionReturnValue = 0;
            int i = 0;

            try
            {
                for (i = 0; i <= tpEvent.Length - 1; i++)
                {
                    if (lIdEvent == tpEvent[i].lIdEvent)
                    {
                        functionReturnValue = tpEvent[i].lNoInter;
                        stable = tpEvent[i].sTypeTable;
                        return functionReturnValue;
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_ReturnNo");
                return functionReturnValue;
            }

        }

        public static int fc_ReturnIdEvent(int lNoIntervention)
        {
            int functionReturnValue = 0;
            int i = 0;


            //=== retourne l id event.

            try
            {
                //'=== test si le tableau est vide.
                if (tpEvent == null || tpEvent.Length == 0)
                    return 0;

                for (i = 0; i <= tpEvent.Length - 1; i++)
                {

                    if (tpEvent[i].lNoInter == lNoIntervention)
                    {
                        functionReturnValue = tpEvent[i].lIdEvent;
                        return functionReturnValue;
                    }

                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_ReturnNo");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sUser"></param>
        /// <returns></returns>
        public static object fc_CtrlEventMaj(string sUser)
        {
            object functionReturnValue = null;
            string sSQL = null;
            string sWhere = null;
            string stemp;
            int i = 0;

            //=== par defaut le tableau contient la valeur 0 pour indiquer que le tableu est vide
            //=== dans le cas contraire on affecte les numéros d 'interventions.}
            try
            {
                i = 0;
                iTabEvent = null;
                Array.Resize(ref iTabEvent, i + 1);

                iTabEvent[i] = 0;

                sSQL = "SELECT     PLANM_PLanMemoire.PLANM_Utilisateur, PLANM_PLanMemoire.PLANM_NoMaj, PLANM_PLanMemoire.NoIntervention "
                    + " FROM         PLANM_PLanMemoire INNER JOIN " + " Intervention ON PLANM_PLanMemoire.NoIntervention = Intervention.NoIntervention AND "
                    + "  PLANM_PLanMemoire.PLANM_NoMaj <> Intervention.NoMaj "
                    + " WHERE     (PLANM_PLanMemoire.PLANM_Utilisateur = N'" + StdSQLchaine.gFr_DoublerQuote(sUser) + "')";


                DataTable rs = new DataTable();
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    Array.Resize(ref iTabEvent, i + 1);
                    iTabEvent[i] = Convert.ToInt32(General.nz(rs.Rows[0]["NoIntervention"], 0));

                    i = i + 1;
                }

                rs.Dispose();

                sWhere = fc_GetWherePlanOut();
                //===============> Tested
                if (string.IsNullOrEmpty(sWhere))
                {
                    return functionReturnValue;
                }

                sSQL = "SELECT     NoIntervention, DateRealise" + " From Intervention ";
                sSQL = sSQL + sWhere;
                sSQL = sSQL + " AND NoIntervention NOT IN (SELECT     NoIntervention From PLANM_PLanMemoire " + " WHERE  PLANM_Utilisateur = '" + StdSQLchaine.gFr_DoublerQuote(sUser) + "')";
                //& " AND PLANM_Date >= '" & dtDate & "') AND PLANM_Date <= '" & dtDate & "'" _
                //& " AND DateRealise <= '" & dtDate & "')"
                //vtab = new DataTable();
                //vtab = ModAdo.fc_OpenRecordSet(sSQL);

                //=== controle s'il existe une nouvelle intervention cre� pour les crit�res ci dessous.

                SqlCommand cmd = new SqlCommand();
                SqlParameter param = new SqlParameter();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "GET_INTER_PLANNING";
                cmd.Connection = General.adocnn;
                cmd.Parameters.Add("@User", SqlDbType.VarChar).Value = sUser;
                //'stemp = Replace(sWhere, "'", "''")
                stemp = sWhere;
                cmd.Parameters.Add("@Where", SqlDbType.VarChar).Value = stemp;
                //'Cmd.Parameters.Item("@Where").value = ""
                //'Cmd.Execute

                if (General.adocnn.State != ConnectionState.Open)
                    General.adocnn.Open();

                //cmd.ExecuteNonQuery();
                rs = new DataTable();
                rs.Load(cmd.ExecuteReader());
                cmd.Dispose();//added by mohammed
                //if (General.adocnn.State != ConnectionState.Closed)
                //    General.adocnn.Close();

                //'''Set Cmd = New ADODB.Command
                //'''Set param = New ADODB.Parameter
                //'''
                //'''Cmd.CommandType = adCmdStoredProc
                //'''Cmd.CommandText = "GetTotHtFactFourn"
                //'''Set Cmd.ActiveConnection = adocnn
                //'''
                //'''Cmd.Parameters.Item("@lNumficheStandard").value = 111  '804667
                //'''
                //'''Cmd.Execute
                //'''
                //''''Set rs = New ADODB.Recordset
                //'''Set rs = Cmd.Execute

                if (rs != null)
                {
                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow dr in rs.Rows)
                        {
                            Array.Resize(ref iTabEvent, i + 1);
                            iTabEvent[i] = Convert.ToInt32(General.nz(dr["NoIntervention"], 0));
                            i = i + 1;
                        }
                    }
                    rs.Dispose();
                }

                rs = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modPlan;fc_trlEventMaj");
                return functionReturnValue;
            }
        }





        public static void FillStandardDurations_0m_2w(ref UltraCombo cmbDuration, bool bSnoozeBox)
        {
            DataTable cmbDurationSource = new DataTable();
            if (!bSnoozeBox)
            {
                cmbDurationSource.Rows.Add("0 minutes");
                cmbDurationSource.Rows.Add("1 minute");
            }

            cmbDurationSource.Rows.Add("5 minutes");
            cmbDurationSource.Rows.Add("10 minutes");
            cmbDurationSource.Rows.Add("15 minutes");
            cmbDurationSource.Rows.Add("30 minutes");

            cmbDurationSource.Rows.Add("1 heure");
            cmbDurationSource.Rows.Add("2 heures");
            cmbDurationSource.Rows.Add("4 heures");
            cmbDurationSource.Rows.Add("8 heures");

            cmbDurationSource.Rows.Add("0.5 jour");
            cmbDurationSource.Rows.Add("1 jour");
            cmbDurationSource.Rows.Add("2 Jours");
            cmbDurationSource.Rows.Add("3 Jours");
            cmbDurationSource.Rows.Add("4 Jours");

            cmbDurationSource.Rows.Add("1 semaine");
            cmbDurationSource.Rows.Add("2 semaines");
            cmbDuration.DataSource = cmbDurationSource;
        }
        public static string CalcStandardDurations_0m_2wString(int lDuration)
        {
            string functionReturnValue = null;
            switch (lDuration)
            {
                case 0:
                    functionReturnValue = "0 minutes";
                    break;
                case 1:
                    functionReturnValue = "1 minute";
                    break;
                case 5:
                    functionReturnValue = "5 minutes";
                    break;
                case 10:
                    functionReturnValue = "10 minutes";
                    break;
                case 15:
                    functionReturnValue = "15 minutes";
                    break;
                case 30:
                    functionReturnValue = "30 minutes";
                    break;

                case 60:
                    functionReturnValue = "1 hour";
                    break;
                case (60 * 2):
                    functionReturnValue = "2 hours";
                    break;
                case (60 * 4):
                    functionReturnValue = "4 hours";
                    break;
                case (60 * 8):
                    functionReturnValue = "8 hours";
                    break;

                case (60 * 12):
                    functionReturnValue = "0.5 day";
                    break;
                case (60 * 24):
                    functionReturnValue = "1 day";
                    break;
                case (60 * 24 * 2):
                    functionReturnValue = "2 days";
                    break;
                case (60 * 24 * 3):
                    functionReturnValue = "3 days";
                    break;
                case (60 * 24 * 4):
                    functionReturnValue = "4 days";
                    break;

                case (60 * 24 * 7):
                    functionReturnValue = "1 week";
                    break;
                case (60 * 24 * 7 * 2):
                    functionReturnValue = "2 weeks";
                    break;
            }
            return functionReturnValue;
        }


        public static int fc_InsertEvent(UserDocPlanningCodeJock userDocPlanningCodeJock, int LID, System.DateTime dtDateHourEventFivtabt, System.DateTime dtDateHourEventEnd,
            string sSujet, string sBody, string sStockeChampDate, string sStockeChampHeureDeb, string sStockeChampDuree, int lxtpCalendarImportanceNormal = 0,
            bool bRappel = false, int lIdShedule = 0, int lprocess_RecurrenceState = 0, int lprocess_RecurrencePatternID = 0, int lNoIntervention = 0, object sIntervenant = null,
            int lColor = 0, string sStatut = "", bool bHeurePrevue = false, string immeuble = "")
        {
            int functionReturnValue = 0;

            object xtpCalendarBusyStatusBusy = null;

            //=== défini l'importance de l'événement (basse = 0, 1 = normal, 2 haute)

            CalendarEvent pevent = default(CalendarEvent);


            try
            {
                //'=== crée un évenment et lui attribue un id.
                //'Set pEvent = Cal.DataProvider.CreateEventEx(LID)


                pevent = userDocPlanningCodeJock.Cal.DataProvider.CreateEvent();
                //'pEvent.Id = lNoIntervention
                pevent.Subject = sSujet;
                pevent.Body = sBody;
                //pEvent.Importance = lxtpCalendarImportanceNormal
                pevent.StartTime = dtDateHourEventFivtabt;
                pevent.EndTime = dtDateHourEventEnd;
                pevent.Reminder = bRappel;
                pevent.ScheduleID = lIdShedule;

                // pEvent.CustomProperties("process_RecurrenceState") = lprocess_RecurrenceState
                // pEvent.CustomProperties("process_RecurrencePatternID") = lprocess_RecurrencePatternID

                pevent.CustomProperties[cCustomPlanInterv] = sIntervenant;

                pevent.CustomProperties[cCustomPlanInt] = lNoIntervention;

                pevent.CustomProperties[cChampDate] = sStockeChampDate;

                pevent.CustomProperties[cChampHeureDeb] = sStockeChampHeureDeb;

                pevent.CustomProperties[cChampDuree] = sStockeChampDuree;

                pevent.CustomProperties[cChampsStatut] = sStatut;

                pevent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusBusy;

                //=== couleur.
                if (lColor != 0)
                {
                    // fc_MajList "", lColor
                    //if (immeuble.ToLower() == "delostal")
                    //{
                    //    XtremeCalendarControl.CalendarEventCategory CalEvCat = new XtremeCalendarControl.CalendarEventCategory();
                    //    //CalEvCat.Background.ColorLight = (uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(25, 198, 15));

                    //    //CalEvCat.Background.ColorDark = (uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(100, 250, 40));
                    //    CalEvCat.Background.SetSolidColor((uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Aquamarine));
                    //    //CalEvCat.Background.GradientFactor = 1;
                    //    CalEvCat.Id = pevent.Id;
                    //    CalEvCat.BorderColor = (uint)System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Yellow);
                    //    pevent.Categories.Add(CalEvCat.Id);
                    //    pevent.DataProvider.EventCategories.Add(CalEvCat);
                    //}
                    //else

                    pevent.Label = lColor;
                }

                if (bHeurePrevue == true)
                {
                    pevent.BusyStatus = CalendarEventBusyStatus.xtpCalendarBusyStatusOutOfOffice;
                }

                pevent.AllDayEvent = false;

                pevent.EventVisible = true;

                //pEvent.Update pEvent

                //
                //pEvent.MultipleSchedules.
                // pEvent.Update pEvent
                // pEvent.DataProvider.Open

                //pEvent.DataProvider.Schedules.Item(0).Id = 1
                //pEvent.DataProvider.Save
                //Cal.DataProvider .Save
                //pEvent.Update

                // pevent.Update pevent
                userDocPlanningCodeJock.Cal.DataProvider.AddEvent(pevent);

                //   Cal.DataProvider.Save

                functionReturnValue = pevent.Id;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook;fc_InsertEvent");
                return functionReturnValue;
            }
        }


        public static void fc_InsertEventADO(int lIdShedule, bool bload, UserDocPlanningCodeJock UserDocPlanningCodeJock,
                                            System.DateTime dtDateDuCombo, System.DateTime dtDateFinCombo, DataTable vtab, int w, ref ModAdo rsPModAdo)
        {

            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            System.DateTime dtHeureDeb = default(System.DateTime);
            System.DateTime dtHeureFin = default(System.DateTime);
            System.DateTime dtDate = default(System.DateTime);



            bool bFind = false;
            bool bFindShedule = false;

            int iTab = 0;
            double lMin;
            int lEventID = 0;
            int i = 0;
            double lColor = 0;

            double dbDurée = 0;

            string sIntervenant = null;
            string sSujet = null;
            string sBody = null;
            string sUser = null;

            DataTable rsP = default(DataTable);

            string sStockeChampDate = null;
            string sStockeChampHeureDeb = null;
            string sStockeChampDuree = null;
            string sStatut = null;

            bool bHeurePrevue = false;
            double duree;
            try
            {
                if (!string.IsNullOrEmpty(vtab.Rows[w][cDuree].ToString()))
                {
                    duree = Convert.ToDateTime(vtab.Rows[w][cDuree]).ToOADate();
                    duree = Convert.ToDouble(General.nz(duree, 0));
                }
                else
                {
                    duree = Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0));
                }
                if (General.IsDate(vtab.Rows[w][cDateRealise]))
                {

                    dtDateDeb = Convert.ToDateTime(vtab.Rows[w][cDateRealise]);

                    if (!General.IsDate(vtab.Rows[w][cHeureDebut2]))
                    {
                        if (General.IsDate(vtab.Rows[w][cHeurePrevue]))

                            dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeurePrevue]);
                        else

                            dtHeureDeb = cHeureDebMin;
                    }
                    ///============> Tested
                    else
                    {
                        dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeureDebut2]);
                    }

                    //=== défini l'heure de début.
                    //DateValue(dtDateDeb) & " " & TimeValue(dtHeureDeb)
                    dtDateDeb = Convert.ToDateTime(dtDateDeb.ToShortDateString() + " " + dtHeureDeb.TimeOfDay.ToString());//todo verifeir cette valeur

                    //=== calul de l'heure de fin.                  
                    if (!General.IsDate(vtab.Rows[w][cHeureFin]) && duree == 0)
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                        {
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        }
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);

                    }
                    ///============> Tested
                    else if (General.IsDate(vtab.Rows[w][cHeureFin]))
                    {
                        dtHeureFin = Convert.ToDateTime(vtab.Rows[w][cHeureFin]);
                    }
                    else if (duree > 0)
                    {
                        dbDurée = duree;
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);
                    }
                    else
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                    }
                    sStockeChampDate = cChampDateReal;
                    sStockeChampHeureDeb = cChampHeureDebReal;
                    sStockeChampDuree = cChampDureeReal;
                }
                else if (General.IsDate(vtab.Rows[w][cDatePrevue]))
                {

                    dtDateDeb = Convert.ToDateTime(vtab.Rows[w][cDatePrevue]);

                    if (!General.IsDate(vtab.Rows[w][cHeureDebut2]))
                    {
                        if (General.IsDate(vtab.Rows[w][cHeurePrevue]))
                            dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeurePrevue]);
                        else
                        {
                            dtHeureDeb = cHeureDebMin;//'#8:00:00 AM#
                        }
                    }
                    else
                    {
                        dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeureDebut2]);
                    }

                    //=== défini l'heure de début.

                    dtDateDeb = Convert.ToDateTime(dtDateDeb.ToShortDateString() + " " + dtHeureDeb.TimeOfDay.ToString());

                    //=== calul de l'heure de fin.

                    if (!General.IsDate(vtab.Rows[w][cHeureFin]) && duree == 0)
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);

                    }
                    else if (General.IsDate(vtab.Rows[w][cHeureFin]))
                    {
                        dtHeureFin = Convert.ToDateTime(vtab.Rows[w][cHeureFin]);//TODO . to verify this line with vb6 TimeValue(vtab(cHeureFin, w))
                    }
                    else if (duree > 0)
                    {
                        dbDurée = Convert.ToDouble(duree);
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);
                    }
                    else
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                    }
                    sStockeChampDate = cChampDatePrevue;
                    sStockeChampHeureDeb = cChampHeureDebPrevue;
                    sStockeChampDuree = cChampDureePrevue;

                    bHeurePrevue = true;
                }
                else if (General.IsDate(vtab.Rows[w][cDateSaisie]))
                {

                    dtDateDeb = Convert.ToDateTime(vtab.Rows[w][cDateSaisie]);

                    if (!General.IsDate(vtab.Rows[w][cHeureDebut2]))
                    {
                        if (General.IsDate(vtab.Rows[w][cHeurePrevue]))
                            dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeurePrevue]);
                        //TODO .to verify this line with vb6 TimeValue(vtab(cHeurePrevue, w))
                        else
                        {
                            dtHeureDeb = cHeureDebMin;
                        }
                    }
                    else
                    {
                        dtHeureDeb = Convert.ToDateTime(vtab.Rows[w][cHeureDebut2]);
                        //TODO .to verify this line with vb6 TimeValue(vtab(cHeureDebut2, w))
                    }

                    //=== défini l'heure de début.

                    dtDateDeb = Convert.ToDateTime(dtDateDeb.ToShortDateString() + " " + dtHeureDeb.TimeOfDay.ToString());

                    //=== calul de l'heure de fin.
                    if (!General.IsDate(vtab.Rows[w][cHeureFin]) && duree == 0)
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);

                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);

                    }
                    else if (General.IsDate(vtab.Rows[w][cHeureFin]))
                    {
                        dtHeureFin = Convert.ToDateTime(vtab.Rows[w][cHeureFin]);//TODO . to verify this line with vb6 TimeValue(vtab(cHeureFin, w))

                    }
                    else if (duree > 0)
                    {
                        dbDurée = Convert.ToDouble(duree);
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);
                    }
                    else
                    {
                        if (Convert.ToDouble(General.nz(vtab.Rows[w][cDureePrevue], 0)) > 0)
                            dbDurée = Convert.ToDouble(vtab.Rows[w][cDureePrevue]);
                        else
                        {
                            dbDurée = Convert.ToDouble(cMinDuree);
                        }
                        lMin = fDate.fc_GetMinFromHourCentesimales(dbDurée);
                        dtHeureFin = dtDateDeb.AddMinutes(lMin);
                        //dtHeureFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Hour, dbDurée, dtDateDeb);
                    }

                    sStockeChampDate = cChampDateSaisie;
                    sStockeChampHeureDeb = cChampHeureDebPrevue;
                    sStockeChampDuree = cChampDureePrevue;
                }
                else
                {
                    return;
                }

                //pEvent.StartTime = dtDateHeureRv

                //=== definit l'heure de fin.

                dtDateFin = Convert.ToDateTime(dtDateDeb.ToShortDateString() + " " + dtHeureFin.TimeOfDay.ToString());

                sIntervenant = vtab.Rows[w][cIntervenant] + "";

                //if(bHeurePrevue==true && General.IsDate(vtab.Rows[w][cHeurePrevue]))
                if (General.IsDate(vtab.Rows[w][cHeurePrevue]) && !General.IsDate(vtab.Rows[w][cHeureDebut2]))
                {
                    bHeurePrevue = true;
                    sSujet = Convert.ToDateTime(vtab.Rows[w][cHeurePrevue]).Hour.ToString("00") + "h" + Convert.ToDateTime(vtab.Rows[w][cHeurePrevue]).Minute.ToString("00") + " - "
                        + sIntervenant + "-" + vtab.Rows[w][cCodeImmeuble2] + " - " + vtab.Rows[w][cVille] + "\n\n";
                }
                else
                {
                    sSujet = sIntervenant + "-" + vtab.Rows[w][cCodeImmeuble2] + " - " + vtab.Rows[w][cVille] + "\n\n";
                }

                sBody = vtab.Rows[w][cDesignation] + " - " + vtab.Rows[w][cCommentaire];
                //lColor = CLng(nz(rs!CouleurStatut, 0))

                //'=== modif du 18 05 2019, la couleur de l'immeuble est prioritaire.
                if (Convert.ToDouble(General.nz(vtab.Rows[w][cCouleurDeFond], 0)) != 0)
                    lColor = Convert.ToDouble(General.nz(vtab.Rows[w][cCouleurDeFond], 0));
                else
                {
                    lColor = Convert.ToDouble(General.nz(vtab.Rows[w][cCouleurStatut], 0));
                }
                sStatut = vtab.Rows[w][cCodeEtat] + "";


                //if (bload == false)
                //{
                //=== controle si toutes ressources existent bien, dans le cas contraire on l'crée.
                //=== ou si l'intervenant affecté à l'intervention est modifié, on met à jour l'lIdShedule.
                //=== supprime les ressources si existantes
                //i = userDocPlanningCodeJock.Cal.MultipleResources.Count;//TODO . i changed this line because calendar is a prametere so we don't need the name of the user control to access to it
                i = UserDocPlanningCodeJock.Cal.DataProvider.Schedules.Count;
                lIdShedule = 0;
                ///===================> Tested
                if (i != 0)
                {
                    lIdShedule = 0;
                    while (i > 0)
                    {
                        // Cal.MultipleResources.Remove i - 1
                        //If UCase(Cal.MultipleResources.Item(i - 1).Name) = UCase(sIntervenant) Then
                        if (General.UCase(UserDocPlanningCodeJock.Cal.DataProvider.Schedules[i - 1].Name) == General.UCase(sIntervenant))
                        {
                            lIdShedule = UserDocPlanningCodeJock.Cal.DataProvider.Schedules[i - 1].Id;
                            break;
                        }
                        i = i - 1;
                    }
                }


                //If i<> 0 Then
                //       lIdShedule = 0
                //        Do While i > 0
                //           ' Cal.MultipleResources.Remove i - 1
                //            'If UCase(Cal.MultipleResources.Item(i - 1).Name) = UCase(sIntervenant) Then
                //            If UCase(Cal.DataProvider.Schedules(i -1).Name) = UCase(sIntervenant) Then
                //                   lIdShedule = Cal.DataProvider.Schedules(i - 1).Id



                //                    Exit Do
                //            End If
                //            i = i - 1
                //        Loop
                //End If

                lEventID = fc_InsertEvent(UserDocPlanningCodeJock, lEventID, dtDateDeb, dtDateFin, sSujet, sBody, sStockeChampDate,
                    sStockeChampHeureDeb, sStockeChampDuree, 0, false, lIdShedule, 0, 0,
                    Convert.ToInt32(General.nz(vtab.Rows[w][cNoIntervention], 0)), sIntervenant, Convert.ToInt32(lColor), sStatut, bHeurePrevue, immeuble: General.nz(vtab.Rows[w][cCodeImmeuble2], "").ToString());

                //If bLoad = False Then
                //=== boucle pour recherhcer l'intervention et remettre à jour l'ID.
                if (tpEvent != null)
                    for (iTab = 0; iTab <= tpEvent.Length - 1; iTab++)
                    {
                        if (tpEvent[iTab].lNoInter == Convert.ToInt32(General.nz(vtab.Rows[w][cNoIntervention], 0)))
                        {
                            bFind = true;
                            tpEvent[iTab].lIdEvent = lEventID;
                            break;
                        }
                    }

                if (bFind == false)
                {
                    if (tpEvent != null && tpEvent[0].lNoInter == 0)
                    {
                        iTab = 0;
                    }
                    else
                    {
                        iTab = tpEvent != null ? tpEvent.Length : 0;
                        Array.Resize(ref tpEvent, iTab + 1);//TODO this line must be verified in test Mode.
                    }

                    tpEvent[iTab].lIdEvent = lEventID;
                    tpEvent[iTab].lNoInter = Convert.ToInt32(vtab.Rows[w][cNoIntervention]);
                    tpEvent[iTab].sTypeTable = ctbInter;
                    iTab = iTab + 1;

                    sUser = General.fncUserName();

                    General.sSQL = "SELECT     PLANM_ID, Codeimmeuble, NoIntervention, PLANM_Date, PLANM_Utilisateur, PLANM_CreePar, PLANM_CreeLe, PLANM_PLanOuvert, PLANM_NoMaj, "
                        + " PLANM_DateDuSelect , PLANM_DateAuSelect " + " From PLANM_PLanMemoire ";

                    //& " WHERE     (PLANM_ID = 0)"

                    var where = "";
                    if (bload == true)
                    {
                        General.sSQL = General.sSQL + " WHERE     (PLANM_ID = 0)";

                        if (rsPModAdo == null)
                        {
                            rsPModAdo = new ModAdo();
                            rsPModAdo.fc_OpenRecordSet(General.sSQL, ChangeCursor: false);
                        }
                    }
                    else
                    {

                        where = " WHERE NoIntervention =" + General.nz(vtab.Rows[w]["NoIntervention"], 0)
                            + " and PLANM_Utilisateur ='" + StdSQLchaine.gFr_DoublerQuote(sUser) + "'";

                        General.sSQL = General.sSQL + where;
                    }

                    //Set vtabP = fc_OpenRecordSet(sSQL)

                    DataRow dr = null;
                    ModAdo ModAdo = null;

                    if (bload == false)
                    {

                        //===> Mondir le 19.03.2021, to fix speed
                        General.Execute("DELETE FROM PLANM_PLanMemoire " + where);
                        //===> Fin Modif Mondir

                        rsP = new DataTable();
                        ModAdo = new ModAdo();
                        rsP = ModAdo.fc_OpenRecordSet(General.sSQL, ChangeCursor: false);

                        //===> Mondir le 19.03.2021, replaced this by a query, better for speed https://groupe-dt.mantishub.io/view.php?id=2173
                        //if (rsP.Rows.Count > 0)
                        //{
                        //    //for (int j = rsP.Rows.Count - 1; j >= 0; j--)
                        //    //{
                        //    //    rsP.Rows[j].Delete();
                        //    //}
                        //    var xxxx = ModAdo.Update();
                        //}
                        //===> Fin modif Mondir

                        dr = rsP.NewRow();
                    }
                    else
                    {
                        dr = rsPModAdo.rsAdo.NewRow();
                    }

                    dr["CodeImmeuble"] = General.nz(vtab.Rows[w][cCodeImmeuble2], "");

                    dr["NoIntervention"] = General.nz(vtab.Rows[w][cNoIntervention], 0);
                    //=== à modifier.
                    //vtabP!PLANM_Date = nz(vtab!DateRealise, Null)

                    dr["PLANM_Date"] = General.nz(dtDateDeb, System.DBNull.Value);
                    dr["PLANM_Utilisateur"] = sUser;
                    dr["PLANM_CreePar"] = sUser;
                    dr["PLANM_CreeLe"] = DateTime.Now;
                    dr["PLANM_PLanOuvert"] = 1;

                    dr["PLANM_NoMaj"] = General.nz(vtab.Rows[w][cNoMaj], 0);
                    dr["PLANM_DateDuSelect"] = dtDateDuCombo;
                    dr["PLANM_DateAuSelect"] = dtDateFinCombo;

                    ///This code added by mohammed to catch an exception "Error While Updating Database".
                    //var sql = "select count(*) from PLANM_PLanMemoire WHERE NoIntervention='" + General.nz(vtab.Rows[w][cNoIntervention], 0) +
                    //    "' and PLANM_Date='" + General.nz(dtDateDeb, System.DBNull.Value) + "' and PLANM_Utilisateur='" + sUser + "'";
                    //var ado = new ModAdo();
                    //if (ado.fc_ADOlibelle(sql, ChangeCursor: false) == "0")
                    //{
                    //    if (bload == false)
                    //    {
                    //        rsP.Rows.Add(dr.ItemArray);
                    //        var xxx = ModAdo.Update();
                    //    }
                    //    else
                    //    {
                    //        rsPModAdo.rsAdo.Rows.Add(dr.ItemArray);
                    //    }
                    //}
                    //ado.Close();
                    //ModAdo.Close();
                    //rsP = null;

                    if (bload == false)
                    {
                        var sql = "select count(*) from PLANM_PLanMemoire WHERE NoIntervention='" + General.nz(vtab.Rows[w][cNoIntervention], 0) +
                            "' and PLANM_Date='" + General.nz(dtDateDeb, System.DBNull.Value) + "' and PLANM_Utilisateur='" + sUser + "'";
                        var ado = new ModAdo();
                        if (ado.fc_ADOlibelle(sql, ChangeCursor: false) == "0")
                        {
                            rsP.Rows.Add(dr.ItemArray);
                            var xxx = ModAdo.Update();
                        }
                    }
                    else
                    {
                        rsPModAdo.rsAdo.Rows.Add(dr.ItemArray);
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPlanOutlook; fc_InsertEventADO");

            }
        }
    }
}
