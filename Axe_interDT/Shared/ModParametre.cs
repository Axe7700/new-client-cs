﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    static class ModParametre
    {
        public static string NomServeurGecet { get; set; }
        public static string NomServeurRD;

        public static string NomBaseGecet { get; set; }
        public static string cODBCGecet { get; set; }
        public static DataTable rsGecet;
        public static SqlConnection adoGecet { get; set; }
        public static string sGetParam1;
        public static string sGetParam2;
        public static string sModeOuverture;
        public static string sNewVar;
        public static string sFicheAppelante;

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNameDoc"></param>
        /// <param name="sForm"></param>
        /// <param name="sNewVar"></param>
        /// <param name="sParam1"></param>
        /// <param name="sParam2"></param>
        /// <param name="sParam3"></param>
        /// <param name="sParam4"></param>
        /// <param name="sParam5"></param>
        /// <param name="sParam6"></param>
        /// <param name="sParam7"></param>
        /// <param name="sParam8"></param>
        /// <param name="sParam9"></param>
        /// <param name="sParam10"></param>
        /// <param name="sParam11"></param>
        /// <param name="sParam12"></param>
        /// <param name="sParam13"></param>
        /// <param name="sParam14"></param>
        /// <param name="sParam15"></param>
        /// <param name="sParam16"></param>
        /// <param name="sParam17"></param>
        /// <param name="sParam18"></param>
        /// <param name="sParam19"></param>
        /// <param name="sParam20"></param>
        /// <param name="sParam21"></param>
        /// <param name="sParam22"></param>
        /// <param name="sParam23"></param>
        /// <param name="sParam24"></param>
        /// <param name="sParam25"></param>
        public static void fc_SaveParamPosition(object sNameDoc, string sForm, string sNewVar = "", string sParam1 = "", string sParam2 = "", string sParam3 = "", string sParam4 = "", string sParam5 = "", string sParam6 = "", string sParam7 = "",
             string sParam8 = "", string sParam9 = "", string sParam10 = "", string sParam11 = "", string sParam12 = "", string sParam13 = "", string sParam14 = "", string sParam15 = "", string sParam16 = "", string sParam17 = "",
             string sParam18 = "", string sParam19 = "", string sParam20 = "", string sParam21 = "", string sParam22 = "", string sParam23 = "", string sParam24 = "", string sParam25 = "", string sParam26 = "", string sParam27 = "",
             string sParam28 = "", string sParam29 = "", string sParam30 = "", string sParam31 = "", string sParam32 = "", string sParam33 = "", string sParam34 = "")
        {
            if (sParam1 == null)
                sParam1 = "";
            if (sParam2 == null)
                sParam2 = "";
            if (sParam3 == null)
                sParam3 = "";
            if (sParam4 == null)
                sParam4 = "";
            if (sParam5 == null)
                sParam5 = "";
            if (sParam6 == null)
                sParam6 = "";
            if (sParam7 == null)
                sParam7 = "";
            if (sParam8 == null)
                sParam8 = "";
            if (sParam9 == null)
                sParam9 = "";
            if (sParam10 == null)
                sParam10 = "";
            if (sParam11 == null)
                sParam11 = "";
            if (sParam12 == null)
                sParam12 = "";
            if (sParam13 == null)
                sParam13 = "";
            if (sParam14 == null)
                sParam14 = "";
            if (sParam15 == null)
                sParam15 = "";
            if (sParam16 == null)
                sParam16 = "";
            if (sParam17 == null)
                sParam17 = "";
            if (sParam18 == null)
                sParam18 = "";
            if (sParam19 == null)
                sParam19 = "";
            if (sParam20 == null)
                sParam20 = "";
            if (sParam21 == null)
                sParam21 = "";
            if (sParam22 == null)
                sParam22 = "";
            if (sParam23 == null)
                sParam23 = "";
            if (sParam24 == null)
                sParam24 = "";
            if (sParam25 == null)
                sParam25 = "";
            ///====> Modif Mondir le 02.07.2020, demande par Mantis : https://groupe-dt.mantishub.io/view.php?id=1549
            if (sParam34 == null)
                sParam34 = "";
            ///====> Fin Modif Mondir
            switch (sForm)
            {
                case Variable.cUserDocClient:
                    General.saveInReg(Variable.cUserDocClient, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocClient, "NewVar", sNewVar);
                    break;
                case Variable.cUserDocImmeuble:
                    General.saveInReg(Variable.cUserDocImmeuble, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserDocImmeuble, "Historique", sParam1);

                    General.saveInReg(Variable.cUserDocImmeuble, "De", sParam2);
                    General.saveInReg(Variable.cUserDocImmeuble, "Au", sParam3);
                    General.saveInReg(Variable.cUserDocImmeuble, "Status", sParam4);
                    General.saveInReg(Variable.cUserDocImmeuble, "Intervenant", sParam5);
                    General.saveInReg(Variable.cUserDocImmeuble, "Article", sParam6);
                    General.saveInReg(Variable.cUserDocImmeuble, "Commentaire", sParam7);
                    General.saveInReg(Variable.cUserDocImmeuble, "SaisieDe", sParam8);
                    General.saveInReg(Variable.cUserDocImmeuble, "SaisieFin", sParam9);
                    break;
                case Variable.cUserDocStandard:
                    // numero de fiche standard
                    General.saveInReg(Variable.cUserDocStandard, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocStandard, "NewVar", sNewVar);
                    break;
                case Variable.cUserIntervention:
                    General.saveInReg(Variable.cUserIntervention, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserIntervention, "NewVar", sNewVar);
                    if (sNameDoc.ToString().ToUpper() == Variable.cUserDocStandard.ToUpper() || sNameDoc.ToString().ToUpper() == Variable.cUserDocImmeuble.ToUpper())
                    {
                        // numéro

                        // sPosParam1 contient le numero d'intervention sauf dans le cas de
                        // ou c'est la fiche standard qui est la fiche appelante alors celui ci contient
                        // le numero de fiche standard.
                        General.saveInReg(Variable.cUserIntervention, "CodeImmeuble", sParam1);
                        General.saveInReg(Variable.cUserIntervention, "DateSaisie", sParam2);
                        General.saveInReg(Variable.cUserIntervention, "Copro", sParam3);
                        General.saveInReg(Variable.cUserIntervention, "Courier", sParam4);
                        General.saveInReg(Variable.cUserIntervention, "Operation", sParam5);
                    }
                    break;
                case Variable.cUserBCmdHead:
                    General.saveInReg(Variable.cUserBCmdHead, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserBCmdHead, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserBCmdHead, "ModeOuverture", sParam1);
                    break;
                case Variable.cUderBCmdBody:
                    General.saveInReg(Variable.cUderBCmdBody, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUderBCmdBody, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUderBCmdBody, "ModeOuverture", sParam1);
                    break;
                case Variable.cUserBCLivraison:
                    General.saveInReg(Variable.cUserBCLivraison, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserBCLivraison, "NewVar", sNewVar);
                    break;
                case Variable.Cuserdocfourn:
                    General.saveInReg(Variable.Cuserdocfourn, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.Cuserdocfourn, "NewVar", sNewVar);
                    break;
                case Variable.cUserFactFourn:
                    General.saveInReg(Variable.cUserFactFourn, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserFactFourn, "NewVar", sNewVar);
                    break;
                case Variable.cUserDispatch:
                    General.saveInReg(Variable.cUserDispatch, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDispatch, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserDispatch, "Historique", sParam1);
                    General.saveInReg(Variable.cUserDispatch, "De", sParam2);
                    General.saveInReg(Variable.cUserDispatch, "Au", sParam3);
                    General.saveInReg(Variable.cUserDispatch, "Status", sParam4);
                    General.saveInReg(Variable.cUserDispatch, "Intervenant", sParam5);
                    General.saveInReg(Variable.cUserDispatch, "Article", sParam6);
                    General.saveInReg(Variable.cUserDispatch, "Commentaire", sParam7);
                    General.saveInReg(Variable.cUserDispatch, "Immeuble", sParam8);
                    General.saveInReg(Variable.cUserDispatch, "Activite", sParam9);
                    General.saveInReg(Variable.cUserDispatch, "Secteur", sParam10);
                    General.saveInReg(Variable.cUserDispatch, "saisieDe", sParam11);
                    General.saveInReg(Variable.cUserDispatch, "saisieAu", sParam12);
                    General.saveInReg(Variable.cUserDispatch, "PrevueDe", sParam13);
                    General.saveInReg(Variable.cUserDispatch, "PrevueAu", sParam14);
                    General.saveInReg(Variable.cUserDispatch, "Gerant", sParam15);
                    General.saveInReg(Variable.cUserDispatch, "EtatDevis", sParam16);
                    General.saveInReg(Variable.cUserDispatch, "Achat", sParam17);
                    General.saveInReg(Variable.cUserDispatch, "Gestionnaire", sParam18);
                    General.saveInReg(Variable.cUserDispatch, "chkAffaireAvecFact", sParam19);
                    General.saveInReg(Variable.cUserDispatch, "chkAffaireSansFact", sParam20);
                    General.saveInReg(Variable.cUserDispatch, "txtNoEnregistrement", sParam21);
                    General.saveInReg(Variable.cUserDispatch, "chkVisiteEntretien", sParam22);
                    General.saveInReg(Variable.cUserDispatch, "cmbDepan", sParam23);
                    General.saveInReg(Variable.cUserDispatch, "chkDevisEtablir", sParam24);
                    General.saveInReg(Variable.cUserDispatch, "chkDapeau", sParam25);
                    General.saveInReg(Variable.cUserDispatch, "cmdDispatch", sParam26);
                    General.saveInReg(Variable.cUserDispatch, "txtcommentaire", sParam27);
                    General.saveInReg(Variable.cUserDispatch, "boolBordereauJournee", sParam28);
                    General.saveInReg(Variable.cUserDispatch, "chkDevisPDA", sParam29);

                    General.saveInReg(Variable.cUserDispatch, "txtGroupe", sParam30);
                    General.saveInReg(Variable.cUserDispatch, "chkEncaissement",General.nz(sParam31,0).ToString());
                    General.saveInReg(Variable.cUserDispatch, "chkEncRex", General.nz(sParam32, 0).ToString());
                    General.saveInReg(Variable.cUserDispatch, "chkEncCompta", General.nz(sParam33, 0).ToString());
                    ///====> Modif Mondir le 02.07.2020, demande par Mantis : https://groupe-dt.mantishub.io/view.php?id=1549
                    General.saveInReg(Variable.cUserDispatch, "chkDemandeNonTraite", General.nz(sParam34, 0).ToString());
                    //====> Fin Modif

                    break;

                case Variable.cUserDocDevis:
                    General.saveInReg(Variable.cUserDocDevis, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocDevis, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserDocDevis, "Immeuble", sParam1);
                    General.saveInReg(Variable.cUserDocDevis, "CodeDevis", sParam2);                   
                    break;

                case Variable.cUserDispatchDevis:
                    General.saveInReg(Variable.cUserDispatchDevis, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDispatchDevis, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserDispatchDevis, "Historique", sParam1);
                    General.saveInReg(Variable.cUserDispatchDevis, "De", sParam2);
                    General.saveInReg(Variable.cUserDispatchDevis, "Au", sParam3);
                    General.saveInReg(Variable.cUserDispatchDevis, "Status", sParam4);
                    General.saveInReg(Variable.cUserDispatchDevis, "Intervenant", sParam5);
                    General.saveInReg(Variable.cUserDispatchDevis, "Article", sParam6);
                    General.saveInReg(Variable.cUserDispatchDevis, "Commentaire", sParam7);
                    General.saveInReg(Variable.cUserDispatchDevis, "Immeuble", sParam8);
                    General.saveInReg(Variable.cUserDispatchDevis, "Activite", sParam9);
                    General.saveInReg(Variable.cUserDispatchDevis, "responsableExploit", sParam10);
                    General.saveInReg(Variable.cUserDispatchDevis, "saisieDe", sParam11);
                    General.saveInReg(Variable.cUserDispatchDevis, "saisieAu", sParam12);
                    General.saveInReg(Variable.cUserDispatchDevis, "PrevueDe", sParam13);
                    General.saveInReg(Variable.cUserDispatchDevis, "PrevueAu", sParam14);
                    General.saveInReg(Variable.cUserDispatchDevis, "NumeroDevis", sParam15);
                    General.saveInReg(Variable.cUserDispatchDevis, "CHKRenovationChauff", sParam16);
                    General.saveInReg(Variable.cUserDispatchDevis, "txtNoEnregistrement", sParam17);
                    General.saveInReg(Variable.cUserDispatchDevis, "chkSansContrat", sParam18);
                    General.saveInReg(Variable.cUserDispatchDevis, "Receptionne", sParam19);
                    General.saveInReg(Variable.cUserDispatchDevis, "ClotureDevis", sParam20);
                    General.saveInReg(Variable.cUserDispatchDevis, "statut2", sParam21);
                    General.saveInReg(Variable.cUserDispatch, "txtGroupe", sParam22);
                    break;


                case Variable.cUserDocFicheContrat:
                    General.saveInReg(Variable.cUserDocFicheContrat, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocFicheContrat, "NewVar", sNewVar);
                    if (string.IsNullOrEmpty(sParam1))
                    {
                        General.saveInReg(Variable.cUserDocFicheContrat, "Avenant", "0");
                    }
                    else
                    {
                        General.saveInReg(Variable.cUserDocFicheContrat, "Avenant", sParam1);
                    }
                    break;
                case Variable.cUserDocP2:
                    General.saveInReg(Variable.cUserDocP2, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocP2, "NewVar", sNewVar);
                    if (sNameDoc.ToString().ToUpper() == Variable.cUserDocStandard.ToUpper())
                    {
                        // numéro
                        // sPosParam1 contient le numero d'intervention sauf dans le cas de
                        // ou c'est la fiche standard qui est la fiche appelante alors celui ci contient
                        // le numero de fiche standard.
                        General.saveInReg(Variable.cUserDocP2, "CodeImmeuble", sParam1);
                        General.saveInReg(Variable.cUserDocP2, "DateSaisie", sParam2);
                        General.saveInReg(Variable.cUserDocP2, "Copro", sParam3);
                        General.saveInReg(Variable.cUserDocP2, "Courier", sParam4);
                        //SaveSetting cFrNomApp, cUserIntervention, "Operation", sParam5
                        //SaveSetting cFrNomApp, cUserIntervention, "NoDevis", sParam6
                    }
                    break;

                case Variable.cUserDocHistoInterv:
                    General.saveInReg(Variable.cUserDocHistoInterv, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocHistoInterv, "NewVar", sNewVar);
                    break;

                case Variable.cUserDocHistoDevis:
                    General.saveInReg(Variable.cUserDocHistoDevis, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocHistoDevis, "NewVar", sNewVar);
                    break;

                case Variable.cUserDocFactFourn:
                    General.saveInReg(Variable.cUserDocFactFourn, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocFactFourn, "NewVar", sNewVar);
                    break;

                case Variable.cUserReceptMarchandise:
                    General.saveInReg(Variable.cUserReceptMarchandise, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserReceptMarchandise, "NewVar", sNewVar);
                    break;
                case Variable.cUserInterP2:
                    General.saveInReg(Variable.cUserInterP2, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserInterP2, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserInterP2, "CodeImmeuble", sParam1);
                    General.saveInReg(Variable.cUserInterP2, "COP_NoContrat", sParam2);
                    break;

                case Variable.cUserHistoReadSoft:

                    General.saveInReg(Variable.cUserHistoReadSoft, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserHistoReadSoft, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtNoBonDe", sParam1);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtNoBonAu", sParam2);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtCodeImmeuble", sParam3);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtCode1", sParam4);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtcodeFourn", sParam5);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtNoFactureDe", sParam6);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtNoFactureAu", sParam7);
                    // SaveSetting cFrNomApp, cUserHistoReadSoft, "txtRSTE_CreeParDe", sParam8
                    // SaveSetting cFrNomApp, cUserHistoReadSoft, "txtRSTE_CreeParAu", sParam9
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureDe", sParam8);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureAu", sParam9);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateComptabiliseDe", sParam10);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateComptabiliseAu", sParam11);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureDe", sParam12);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureAu", sParam13);
                    General.saveInReg(Variable.cUserHistoReadSoft, "cmbAcheteur", sParam14);
                    General.saveInReg(Variable.cUserHistoReadSoft, "cmbSTF_StatutFactFourn", sParam15);
                    General.saveInReg(Variable.cUserHistoReadSoft, "sOrderBy", sParam16);

                    General.saveInReg(Variable.cUserHistoReadSoft, "optSansBon", sParam17);
                    General.saveInReg(Variable.cUserHistoReadSoft, "optAvecBon", sParam18);
                    General.saveInReg(Variable.cUserHistoReadSoft, "optAllFact", sParam19);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtRSTE_HT", sParam20);
                    General.saveInReg(Variable.cUserHistoReadSoft, "txtNoDevis", sParam21);
                    break;

                case Variable.cUserDocDescriptifChaufferie:
                    General.saveInReg(Variable.cUserDocDescriptifChaufferie, "Origine", sNameDoc.ToString());
                    General.saveInReg(Variable.cUserDocDescriptifChaufferie, "NewVar", sNewVar);
                    General.saveInReg(Variable.cUserDocDescriptifChaufferie, "CodeImmeuble", sParam1);
                
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sFormulaire"></param>
        /// <returns></returns>
        public static bool fc_RecupParam(string sFormulaire)
        {
            bool functionReturnValue = false;

            switch (sFormulaire)
            {
                case Variable.cUserDocClient:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocClient, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserDocClient, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserDocImmeuble:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocImmeuble, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserDocImmeuble, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserDocStandard:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocStandard, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserDocStandard, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserIntervention:
                    sFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "Origine", "");
                    // sNewVar contient le numero d'intervention sauf dans le cas
                    // ou c'est la fiche standard qui est la fiche appelante alors celui ci contient
                    // le numero de fiche standard.
                    sNewVar = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserBCmdHead:
                    sFicheAppelante = General.getFrmReg(Variable.cUserBCmdHead, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                    sModeOuverture = General.getFrmReg(Variable.cUserBCmdHead, "ModeOuverture", "");
                    break;
                case Variable.cUderBCmdBody:
                    sFicheAppelante = General.getFrmReg(Variable.cUderBCmdBody, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUderBCmdBody, "NewVar", "");
                    break;
                case Variable.cUserBCLivraison:
                    sFicheAppelante = General.getFrmReg(Variable.cUserBCLivraison, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserBCLivraison, "NewVar", "");
                    break;
                case Variable.Cuserdocfourn:
                    sFicheAppelante = General.getFrmReg(Variable.Cuserdocfourn, "Origine");
                    sNewVar = General.getFrmReg(Variable.Cuserdocfourn, "NewVar", "");
                    break;
                case Variable.cUserFactFourn:
                    sFicheAppelante = General.getFrmReg(Variable.cUserFactFourn, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserFactFourn, "NewVar", "");
                    break;

                case Variable.cUserDocDevis:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

                case Variable.cUserDocDevisV2:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocDevisV2, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

                case Variable.cUserDocHistoDevis:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocHistoDevis, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserDocHistoDevis, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

                case Variable.cUserDocHistoInterv:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocHistoInterv, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserDocHistoInterv, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

                case Variable.cUserDocFicheContrat:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocFicheContrat, "Origine");
                    sNewVar = General.getFrmReg(Variable.cUserDocFicheContrat, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserAnalytique:
                    sFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "Origine", "");
                    // sNewVar contient le numero d'intervention sauf dans le cas
                    // ou c'est la fiche standard qui est la fiche appelante alors celui ci contient
                    // le numero de fiche standard.
                    sNewVar = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserDocP2:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocP2, "Origine", "");
                    // sNewVar contient le numero d'intervention sauf dans le cas
                    // ou c'est la fiche standard qui est la fiche appelante alors celui ci contient
                    // le numero de fiche standard.
                    sNewVar = General.getFrmReg(Variable.cUserDocP2, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

                case Variable.cUserDocRelTech:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocRelTech, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserDocRelTech, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserDocFactFourn:
                    sFicheAppelante = General.getFrmReg(Variable.cUserDocFactFourn, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserDocFactFourn, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserReceptMarchandise:
                    sFicheAppelante = General.getFrmReg(Variable.cUserReceptMarchandise, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserReceptMarchandise, "NewVar", "");
                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;
                case Variable.cUserPreCommande:

                    sFicheAppelante = General.getFrmReg(Variable.cUserPreCommande, "Origine", "");
                    sNewVar = General.getFrmReg(Variable.cUserPreCommande, "NewVar", "");
                    sGetParam1 = General.getFrmReg(Variable.cUserPreCommande, "NumFicheStandard", "");
                    sGetParam2 = General.getFrmReg(Variable.cUserPreCommande, "NoBonDeCommande", "");

                    if (!string.IsNullOrEmpty(sNewVar))
                    {
                        functionReturnValue = true;
                    }
                    break;

            }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_OpenConnGecet()
        {
            // ouvre une connection avec sage. sOdbcT contient le nom de l'odbc Sage
            if (string.IsNullOrEmpty(cODBCGecet))
            {
                cODBCGecet = $"SERVER={NomServeurGecet};DATABASE={NomBaseGecet};INTEGRATED SECURITY=TRUE;";
            }
            if (adoGecet == null)
            {
                adoGecet = new SqlConnection(cODBCGecet);
                adoGecet.Open();
            }
            else if (adoGecet.State == ConnectionState.Closed)
            {
                adoGecet = new SqlConnection(cODBCGecet);
                adoGecet.Open();
            }
            //    adoSage.Close
            //    Set adoSage = Nothing
        }
        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_CloseConnGecet()
        {
            if ((adoGecet != null))
            {
                if (adoGecet.State == ConnectionState.Open)
                {
                    adoGecet.Close();
                    adoGecet?.Dispose();
                }
            }
        }
    }
}
