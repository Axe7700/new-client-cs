﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Axe_interDT.Shared
{
    public static class StdSQLchaine
    {
        private const string cFrModName = "StdSQLchaine";
        private const string cEnter = " ^ ";
        public static string gFr_DoublerQuote(string vChaine, bool ftrim = true) => vChaine != null ? vChaine.Replace("'", "''") : "";

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWord"></param>
        /// <returns></returns>
        public static string fc_CtrlCharDevis(string sWord)
        {
            string functionReturnValue = null;
            string sRef = null;
            string[] sTab = null;
            string sCAR = null;
            bool BIn = false;
            string sNewWord = null;
            int i = 0;
            int x = 0;

            //=== controle la présence de caratére indésirables.

            try
            {
                sWord = sWord.Trim();

                sRef = "A@B@C@D@E@F@G@H@I@J@K@L@M@N@O@P@Q@R@S@T@U@V@W@X@Y@Z@a@b@c@d@e@f@g@h@i@j@k@l@m@n@o@p@q@r@s@t@u@v@w@x@y@z@1@2@3@4@5@6@7@8@9@0@ @.@/@\\@-";


                sTab = sRef.Split('@');

                for (i = 0; i <= sWord.Length -1; i++)
                {
                    sCAR = General.Mid(sWord, i + 1, 1);
                    BIn = false;

                    for (x = 0; x <= sTab.Length - 1; x++)
                    {
                        if (sTab[x] == sCAR)
                        {
                            BIn = true;
                            break;
                        }
                    }

                    if (BIn == true)
                    {
                        sNewWord = sNewWord + sCAR;
                    }
                    else if (BIn == false)
                    {
                        //   Stop
                    }
                }


                functionReturnValue = sNewWord;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "StdSQLchaine;fc_CtrlCharDevis");
                return functionReturnValue;
            }
        }

        public static object gFr_NombreAng(object vChaine)
        {
            object functionReturnValue = null;
            // Remplace les Virgules par des Points (Ne contrôle pas s'il y a des caractères non numériques)
            // Voir si équivalent Str
            return vChaine.ToString().Replace(",", ".");
            //int iPos = 0;
            //object sChaine = null;
            //try
            //{
            //    if (vChaine != null)
            //    {
            //        sChaine = vChaine;
            //        iPos = 0;
            //        do
            //        {
            //            iPos = sChaine.ToString().IndexOf(",", iPos + 1);
            //            if (iPos > 0)
            //            {
            //                General.Mid(sChaine.ToString(), iPos, 1) == ".";
            //                iPos = iPos + 1;
            //            }
            //        } while (!(iPos == 0));
            //        functionReturnValue = sChaine;
            //    }
            //    else
            //    {
            //        functionReturnValue = vChaine;
            //    }
            //    return functionReturnValue;
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, cFrModName + ",gFr_NombreAng");
            //}
        }

    }
}