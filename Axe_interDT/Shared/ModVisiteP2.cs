﻿using System;
using System.Data;

namespace Axe_interDT.Shared
{
    class ModVisiteP2
    {
        public static bool bInsertModeleP2;

        public struct TypeModInsterP2
        {
            public int lMVP_ID;
            public int lCop_NoAuto;
            public string sCodeImmeuble;
            public int lPDAB_Noauto;
        }

        public static TypeModInsterP2[] tpModInterP2;

        public struct value
        {
            public int lOldValue;
            public int lNewValue;
        }

        /// <summary>
        /// Mondir le 25.03.2021 pour ajouter la version V24.03.2021
        /// Mondir le 08.04.2021 this fonction not yet tested coz it is not used yet
        /// </summary>
        /// <param name="sNameModel"></param>
        /// <param name="lCOP_NoAuto"></param>
        /// <param name="sCodeimmeuble"></param>
        /// <param name="lPDAB_Noauto"></param>
        public static void fc_CreateModeleV2(string sNameModel, int lCOP_NoAuto, string sCodeimmeuble, int lPDAB_Noauto)
        {
            string sSQL = "";
            DataTable rs = null;
            ModAdo modAdors = new ModAdo();
            DataTable rsMod = null;
            ModAdo modAdorsMod = new ModAdo();
            string sUser = "";
            DateTime dtDate;
            int lMP2_ID = 0;
            int lGAN_ID = 0;
            int lICN_Noauto = 0;
            int lEQN_NoAuto = 0;
            int lEQM_Noauto = 0;

            try
            {
                sUser = General.fncUserName();
                dtDate = DateTime.Now;

                sSQL = "SELECT         MP2_ID, MP2_Nom, CreePar, CreeLe" +
                        " FROM            MP2_ModelePrestP2" +
                        " WHERE     (MP2_ID = 0)";

                rs = modAdors.fc_OpenRecordSet(sSQL);
                var newRow = rs.NewRow();

                rs = modAdors.fc_OpenRecordSet(sSQL, null, "MP2_ID", null, false, newRow);


                newRow["MVP_Nom"] = sNameModel;
                newRow["MVP_CreePar"] = sUser;
                newRow["MVP_CreeLe"] = dtDate;

                rs.Rows.Add(newRow);

                modAdors.Update();

                lMP2_ID = newRow["MP2_ID"].ToInt();

                modAdors.Close();

                sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier,"
                        + " COP_Noauto, GAI_NoLigne, CodeImmeuble, GAI_Compteur, GAI_CompteurObli"
                        + " From GAI_GammeImm "
                        + " WHERE ";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT        GAN_ID, GAN_Code, GAN_LIbelle, GAN_Select, GAN_NePasPlanifier, GAN_NoLigne,"
                        + " GAN_Compteur, GAN_CompteurObli, MP2_ID"
                        + " FROM            GAN_GammeMod"
                        + " WHERE GAN_ID = 0";

                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL);
                var rdModNewRow = rsMod.NewRow();

                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL, null, "GAN_ID", null, false, rdModNewRow);

                rdModNewRow["GAN_Code"] = rs.Rows[0]["GAM_Code"];
                rdModNewRow["GAN_LIbelle"] = rs.Rows[0]["GAI_LIbelle"];
                rdModNewRow["GAN_Select"] = 0;
                rdModNewRow["GAN_NoLigne"] = rs.Rows[0]["GAI_NoLigne"];
                rdModNewRow["GAN_Compteur"] = rs.Rows[0]["GAI_Compteur"];
                rdModNewRow["GAN_CompteurObli"] = rs.Rows[0]["GAI_CompteurObli"];
                rdModNewRow["MP2_ID"] = lMP2_ID;

                rsMod.Rows.Add(rdModNewRow);

                modAdorsMod.Update();

                lGAN_ID = rdModNewRow["GAN_ID"].ToInt();

                modAdorsMod.Close();
                modAdors.Close();

                //==== NIVEAU 2.
                sSQL = "SELECT     CodeImmeuble, CAI_Code, CAI_Libelle, ICC_Noauto, ICC_NoLigne, COP_Noauto, ICC_Cout, "
                + " COS_NoAuto, ICC_Affiche, PDAB_Noauto, ICC_Select, GAi_ID "
                + " From ICC_IntervCategorieContrat "
                + " WHERE ";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT        CAI_Code, CAI_Libelle, ICN_Noauto, ICN_NoLigne, ICN_Cout, COS_NoAuto, ICN_Affiche, ICN_Select, GAN_ID"
                        + " FROM            ICN_IntervCategorieContratMod"
                        + " WHERE EQN_NoAuto = 0";
                modAdorsMod = new ModAdo();
                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL);

                var rsModNewRow = rsMod.NewRow();
                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL, null, "ICN_Noauto", null, false, rsModNewRow);

                rsModNewRow["CAI_Code"] = rs.Rows[0]["CAI_Code"];
                rsModNewRow["CAI_Libelle"] = rs.Rows[0]["CAI_Libelle"];
                rsModNewRow["ICN_NoLigne"] = rs.Rows[0]["ICC_NoLigne"];
                rsModNewRow["ICN_Cout"] = rs.Rows[0]["ICC_Cout"];
                rsModNewRow["ICN_Affiche"] = rs.Rows[0]["ICC_Affiche"];
                rsModNewRow["ICN_Select"] = rs.Rows[0]["ICC_Select"];
                rsModNewRow["GAN_ID"] = lGAN_ID;

                rsMod.Rows.Add(rsModNewRow);
                modAdorsMod.Update();
                lICN_Noauto = rs.Rows[0]["ICN_Noauto"].ToInt();

                //=== NIVEAU 3
                sSQL = "SELECT     EQM_Code, EQM_Libelle, ICC_Noauto, EQM_Observation, EQM_Metier, EQM_Niveau, "
                        + " EQM_Caractere, EQM_NoAuto, CodeImmeuble, COP_NoContrat, "
                        + " EQM_NoLigne, EQM_Affiche, COP_NoAuto, EQU_NoAuto, EQU_TypeIntervenant, Duree,"
                        + " DureePlanning, CompteurObli, EQU_Select, "
                        + " EQU_P1Compteur "
                        + " FROM         EQM_EquipementP2Imm";

                sSQL = "SELECT   EQM_Code, EQM_Libelle, ICN_Noauto, EQN_Observation, EQN_Metier, EQN_Niveau,"
                        + " EQN_Caractere, EQN_NoAuto, EQN_NoLigne, EQN_Affiche, EQU_NoAuto, EQN_TypeIntervenant, Duree, DureePlanning, "
                        + " CompteurObli , EQN_Select, EQU_P1Compteur, CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID,"
                        + " ANO_Libelle, OPE_ID, OPE_Libelle"
                        + " FROM            EQN_EquipementP2ImmMod"
                        + " WHERE EQN_NoAuto = 0";


                modAdorsMod = new ModAdo();
                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL);

                rsModNewRow = rsMod.NewRow();
                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL, null, "ICN_Noauto", null, false, rsModNewRow);

                rsModNewRow["EQM_CODE"] = rs.Rows[0]["EQM_CODE"];
                rsModNewRow["EQM_Libelle"] = rs.Rows[0]["EQM_Libelle"];
                rsModNewRow["ICN_Noauto"] = rs.Rows[0]["ICN_Noauto"];
                rsModNewRow["EQN_Observation"] = rs.Rows[0]["EQN_Observation"];
                rsModNewRow["EQN_Metier"] = rs.Rows[0]["EQN_Metier"];
                rsModNewRow["EQN_Niveau"] = rs.Rows[0]["EQN_Niveau"];
                rsModNewRow["EQN_Caractere"] = rs.Rows[0]["EQN_Caractere"];
                rsModNewRow["EQN_NoLigne"] = rs.Rows[0]["EQN_NoLigne"];
                rsModNewRow["EQN_Affiche"] = rs.Rows[0]["EQN_Affiche"];
                rsModNewRow["EQU_NoAuto"] = rs.Rows[0]["EQU_NoAuto"];
                rsModNewRow["EQN_TypeIntervenant"] = rs.Rows[0]["EQN_TypeIntervenant"];
                rsModNewRow["Duree"] = rs.Rows[0]["Duree"];
                rsModNewRow["DureePlanning"] = rs.Rows[0]["DureePlanning"];
                rsModNewRow["CompteurObli"] = rs.Rows[0]["CompteurObli"];
                rsModNewRow["EQN_Select"] = 0;
                rsModNewRow["EQU_P1Compteur"] = rs.Rows[0]["EQU_P1Compteur"];
                rsModNewRow["CTR_ID"] = rs.Rows[0]["CTR_ID"];
                rsModNewRow["CTR_Libelle"] = rs.Rows[0]["CTR_Libelle"];
                rsModNewRow["MOY_ID"] = rs.Rows[0]["MOY_ID"];
                rsModNewRow["MOY_Libelle"] = rs.Rows[0]["MOY_Libelle"];
                rsModNewRow["ANO_ID"] = rs.Rows[0]["ANO_ID"];
                rsModNewRow["ANO_Libelle"] = rs.Rows[0]["ANO_Libelle"];
                rsModNewRow["OPE_ID"] = rs.Rows[0]["OPE_ID"];
                rsModNewRow["OPE_Libelle"] = rs.Rows[0]["OPE_Libelle"];

                rsMod.Rows.Add(rsModNewRow);
                modAdorsMod.Update();

                lEQN_NoAuto = rsModNewRow["EQN_NoAuto"].ToInt();

                sSQL = "SELECT     CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code, PEI_JD, PEI_MD, PEI_AnD, "
                        + " PEI_JF, PEI_MF, PEI_AnF, SEM_Code, PEI_Duree, PEI_Visite, "
                        + " PEI_Tech, PEI_Cout, PEI_ForceVisite, COP_NoContrat, PEI_TotalHT, PEI_DureeReel, "
                        + " PEI_Intervenant, PEI_APlanifier, PEI_Calcul, PEI_IntAutre, "
                        + " COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour, PEI_KFO, PEI_KMO, PEI_Heure,"
                        + " PEI_ForceJournee, PEI_MtAchat, PEI_Cycle, PEI_DateDebut,"
                        + " PEI_DateFin , PEI_DureeVisu, PEI_Samedi, PEI_Dimanche, PEI_Pentecote, "
                        + " PEI_Jourferiee, PEI_NbAn, PEI_CompteurObli, TempDate "
                        + " FROM         PEI_PeriodeGammeImm"
                        + " WHERE ";

                sSQL = "SELECT  EQM_NoAuto, PEN_NoAuto, TPP_Code, PEN_JD, PEN_MD, PEN_AnD, PEN_JF, PEN_MF, PEN_AnF, SEM_Code,"
                    + " PEN_Duree, PEN_Visite, PEN_Tech, PEN_Cout, PEN_ForceVisite, PEN_TotalHT, PEN_DureeReel, "
                    + " PEN_Intervenant, PEN_APlanifier, PEN_Calcul, PEN_IntAutre, PEN_AvisPassage, PEN_AvisNbJour,"
                    + " PEN_KFO, PEN_KMO, PEN_Heure, PEN_ForceJournee, PEN_MtAchat, PEN_Cycle, PEN_DateDebut, PEN_DateFin,"
                    + " PEN_DureeVisu , PEN_Samedi, PEN_Dimanche, PEN_Pentecote, PEN_Jourferiee, PEN_NbAn, PEN_CompteurObli,"
                    + " TempDate, PEN_PassageObli, ChampsTempAeffacer"
                    + " FROM            PEN_PeriodeGammeImmMod"
                    + " WHERE PEN_NoAuto = 0";

                modAdorsMod = new ModAdo();
                rsMod = modAdorsMod.fc_OpenRecordSet(sSQL);

                rsModNewRow = rsMod.NewRow();
                rsModNewRow["EQM_NoAuto"] = lEQM_Noauto;
                rsModNewRow["TPP_Code"] = rs.Rows[0]["TPP_Code"];
                rsModNewRow["PEN_JD"] = rs.Rows[0]["PEI_JD"];
                rsModNewRow["PEN_MD"] = rs.Rows[0]["PEI_MD"];
                rsModNewRow["PEN_AnD"] = rs.Rows[0]["PEI_And"];
                rsModNewRow["PEN_JF"] = rs.Rows[0]["PEI_JF"];
                rsModNewRow["PEN_MF"] = rs.Rows[0]["PEI_MF"];
                rsModNewRow["PEN_AnF"] = rs.Rows[0]["PEI_Anf"];
                rsModNewRow["SEM_Code"] = rs.Rows[0]["SEM_Code"];
                rsModNewRow["PEN_Duree"] = rs.Rows[0]["PEI_Duree"];
                rsModNewRow["PEN_Visite"] = rs.Rows[0]["PEI_Visite"];
                rsModNewRow["PEN_Tech"] = rs.Rows[0]["PEI_Tech"];
                rsModNewRow["PEN_Cout"] = rs.Rows[0]["PEN_Cout"];
                rsModNewRow["PEN_ForceVisite"] = rs.Rows[0]["PEI_ForceVisite"];
                rsModNewRow["PEN_TotalHT"] = rs.Rows[0]["PEI_TotalHT"];
                rsModNewRow["PEN_DureeReel"] = rs.Rows[0]["PEI_DureeReel"];
                rsModNewRow["PEN_Intervenant"] = rs.Rows[0]["PEI_Intervenant"];
                rsModNewRow["PEN_APlanifier"] = rs.Rows[0]["PEI_APlanifier"];
                rsModNewRow["PEN_Calcul"] = rs.Rows[0]["PEI_Calcul"];
                rsModNewRow["PEN_IntAutre"] = rs.Rows[0]["PEI_IntAutre"];
                rsModNewRow["PEN_AvisPassage"] = rs.Rows[0]["PEI_AvisPassage"];
                rsModNewRow["PEN_AvisNbJour"] = rs.Rows[0]["PEI_AvisNbJour"];
                rsModNewRow["PEN_KFO"] = rs.Rows[0]["PEI_KFO"];
                rsModNewRow["PEN_KMO"] = rs.Rows[0]["PEI_KMO"];
                rsModNewRow["PEN_Heure"] = rs.Rows[0]["PEI_Heure"];
                rsModNewRow["PEN_ForceJournee"] = rs.Rows[0]["PEI_ForceJournee"];
                rsModNewRow["PEN_MtAchat"] = rs.Rows[0]["PEI_MtAchat"];
                rsModNewRow["PEN_Cycle"] = rs.Rows[0]["PEI_Cycle"];
                rsModNewRow["PEN_DateDebut"] = rs.Rows[0]["PEI_DateDebut"];
                rsModNewRow["PEN_DateFin"] = rs.Rows[0]["PEI_DateFin"];
                rsModNewRow["PEN_DureeVisu"] = rs.Rows[0]["PEI_DureeVisu"];
                rsModNewRow["PEN_Samedi"] = rs.Rows[0]["PEI_Samedi"];
                rsModNewRow["PEN_Dimanche"] = rs.Rows[0]["PEI_Dimanche"];
                rsModNewRow["PEN_Pentecote"] = rs.Rows[0]["PEI_Pentecote"];
                rsModNewRow["PEN_Jourferiee"] = rs.Rows[0]["PEI_Jourferiee"];
                rsModNewRow["PEN_NbAn"] = rs.Rows[0]["PEI_NbAn"];
                rsModNewRow["PEN_CompteurObli"] = rs.Rows[0]["PEI_CompteurObli"];
                rsModNewRow["TempDate"] = rs.Rows[0]["TempDate"];
                rsModNewRow["PEN_PassageObli"] = rs.Rows[0]["PEI_PassageObli"];
                rsModNewRow["ChampsTempAeffacer"] = rs.Rows[0]["ChampsTempAeffacer"];

                rsMod.Rows.Add(rsModNewRow);

                modAdorsMod.Update();


                //PEN_NoAuto
                //================ NE PAS OUBLIER FAP_ARTICLE ============================================================='
                //================ NE PAS OUBLIER FAP_ARTICLE ============================================================='
                //================ NE PAS OUBLIER FAP_ARTICLE ============================================================='
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        public static object fc_InsertModele(int lCOP_NoautoMod, int lPDAB_NoautoMod, int lPDAB_Noauto, string sCodeImmeuble,
            int lCop_NoAuto, string sCop_Nocontrat)
        {
            object functionReturnValue = null;

            string sWhere = null;
            string sSql = null;
            DataTable rsMod = default(DataTable);
            DataTable rs = default(DataTable);
            int lEQM_NoAuto = 0;
            value[] tpValue = null;
            value[] tpValueEQM = null;
            value[] tpValuePEI = null;
            int i = 0;
            int iEQM = 0;
            int iPEI = 0;
            // ADODB.Field f = default(ADODB.Field);
            //=== retourne true si l'insertion s'est bien déroulé.
            // ERROR: Not supported in C#: OnErrorStatement
            functionReturnValue = true;

            try
            {
                //================INSERTION DES GAMMES.
                sSql = "SELECT  GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier,"
                        + " COP_Noauto, GAI_NoLigne, CodeImmeuble, GAI_Compteur, GAI_CompteurObli"
                        + " From GAI_GammeImm ";
                var tmpAdorsMod = new ModAdo();
                rsMod = tmpAdorsMod.fc_OpenRecordSet(sSql + " WHERE  PDAB_Noauto =" + lPDAB_NoautoMod + " AND COP_Noauto =" + lCOP_NoautoMod);

                if (rsMod.Rows.Count == 0)
                {
                    functionReturnValue = false;
                    rsMod.Dispose();
                    rsMod = null;
                    return functionReturnValue;
                }
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSql + " WHERE   GAi_ID = 0 ");
                int lGAi_ID = 0;
                //SqlCommandBuilder cb = new SqlCommandBuilder(tmpAdors.SDArsAdo);
                //tmpAdors.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                //tmpAdors.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                //SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                //insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                //insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                //tmpAdors.SDArsAdo.InsertCommand = insertCmd;
                //tmpAdors.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                //{
                //    if (e.StatementType == StatementType.Insert)
                //    {
                //        if(lGAi_ID == 0)
                //        {
                //            lGAi_ID = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                //        }

                //    }
                //});

                if (rsMod.Rows.Count > 0)
                {
                    foreach (DataRow rsModRow in rsMod.Rows)
                    {
                        //rs.AddNew();
                        var NewRow = rs.NewRow();
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["GAM_Code"] = rsModRow["GAM_Code"];
                        NewRow["GAI_LIbelle"] = rsModRow["GAI_LIbelle"];

                        //==============IMPORTANT.
                        //=== recherche le numéro de ligne.
                        NewRow["GAI_NoLigne"] = 0;
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        NewRow["GAI_NePasPlanifier"] = rsModRow["GAI_NePasPlanifier"];
                        NewRow["PDAB_Noauto"] = lPDAB_Noauto;
                        NewRow["GAI_Select"] = 0;
                        NewRow["GAI_CompteurObli"] = rsModRow["GAI_CompteurObli"];
                        NewRow["GAI_Compteur"] = rsModRow["GAI_Compteur"];
                        rs.Rows.Add(NewRow.ItemArray);
                        tmpAdors.Update();

                        Array.Resize(ref tpValue, i + 1);
                        tpValue[i].lOldValue = Convert.ToInt16(rsModRow["GAi_ID"]);
                        // rs.MoveLast();

                        using (var tmp = new ModAdo())
                            lGAi_ID = Convert.ToInt32(tmp.fc_ADOlibelle("select MAX(GAi_ID) FROM GAI_GammeImm"));
                        tpValue[i].lNewValue = lGAi_ID;
                        // tpValue[i].lNewValue = Convert.ToInt16(NewRow["GAi_ID"]);
                        i = i + 1;
                        // rsMod.MoveNext();
                    }
                }
                // rsMod.Close();
                // rs.Close();




                //==================INSERTION DES 2 EME NIVEAU DES GAMMES.
                sSql = "SELECT     CodeImmeuble, CAI_Code, CAI_Libelle, ICC_Noauto, ICC_NoLigne, COP_Noauto, ICC_Cout, "
                        + " COS_NoAuto, ICC_Affiche, PDAB_Noauto, ICC_Select, GAi_ID " + " From ICC_IntervCategorieContrat ";

                sWhere = "";
                for (i = 0; i <= tpValue.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE GAi_ID =" + tpValue[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR GAi_ID =" + tpValue[i].lOldValue;
                    }
                }

                //Set rsMod = fc_OpenRecordSet(sSQL & " WHERE  PDAB_Noauto =" & lPDAB_NoautoMod & " AND COP_Noauto =" & lCOP_NoautoMod)
                rsMod = tmpAdorsMod.fc_OpenRecordSet(sSql + sWhere);

                if (rsMod.Rows.Count == 0)
                {
                    functionReturnValue = false;
                    rsMod.Dispose();
                    rsMod = null;
                    return functionReturnValue;
                }

                rs = tmpAdors.fc_OpenRecordSet(sSql + " WHERE   ICC_Noauto = 0 ");
                int lICC_Noauto = 0;

                if (rsMod.Rows.Count > 0)
                {

                    foreach (DataRow rsModRow in rsMod.Rows)
                    { // rs.AddNew();
                        var NewRow = rs.NewRow();

                        for (i = 0; i <= tpValue.Length - 1; i++)
                        {
                            if (Convert.ToInt16(rsModRow["GAi_ID"]) == tpValue[i].lOldValue)
                            {
                                NewRow["GAi_ID"] = tpValue[i].lNewValue;
                            }
                        }
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["CAI_Code"] = rsModRow["CAI_Code"];
                        NewRow["CAI_Libelle"] = rsModRow["CAI_Libelle"];
                        //==============IMPORTANT.
                        //=== recherche le numéro de ligne.
                        NewRow["ICC_NoLigne"] = 0;
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        NewRow["ICC_Cout"] = rsModRow["ICC_Cout"];
                        NewRow["COS_NoAuto"] = rsModRow["COS_NoAuto"];
                        NewRow["ICC_Affiche"] = rsModRow["ICC_Affiche"];
                        NewRow["PDAB_Noauto"] = lPDAB_Noauto;
                        NewRow["ICC_Select"] = 0;
                        rs.Rows.Add(NewRow.ItemArray);
                        tmpAdors.Update();
                        Array.Resize(ref tpValue, i + 1);

                        tpValue[i].lOldValue = Convert.ToInt32(rsModRow["ICC_Noauto"]);

                        using (var tmp = new ModAdo())
                            lICC_Noauto = Convert.ToInt32(tmp.fc_ADOlibelle("select MAX(ICC_Noauto) FROM ICC_IntervCategorieContrat"));
                        //rs.MoveLast();
                        tpValue[i].lNewValue = lICC_Noauto;
                        i = i + 1;

                        //rsMod.MoveNext();
                    }
                }
                rsMod.Dispose();
                rs.Dispose();

                //======================INSERTION DU 3 EME NIVEAU.
                //sSQL = "SELECT     EQM_Code, EQM_Libelle, ICC_Noauto, EQM_Observation, EQM_Metier, EQM_Niveau, " _
                //& " EQM_Caractere, EQM_NoAuto, CodeImmeuble, COP_NoContrat, " _
                //& " EQM_NoLigne, EQM_Affiche, COP_NoAuto, EQU_NoAuto, EQU_TypeIntervenant, Duree," _
                //& " DureePlanning, CompteurObli, EQU_Select, " _
                //& " EQU_P1Compteur " _
                //& " FROM         EQM_EquipementP2Imm"

                //la clé primaire de cette table est EQU_NoAuto

                sSql = "SELECT * FROM EQM_EquipementP2Imm";
                sWhere = "";
                for (i = 0; i <= tpValue.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE ICC_Noauto =" + tpValue[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR ICC_Noauto =" + tpValue[i].lOldValue;
                    }
                }

                rsMod = tmpAdors.fc_OpenRecordSet(sSql + sWhere);
                rs = tmpAdors.fc_OpenRecordSet(sSql + " WHERE EQM_NoAuto = 0");


                iEQM = 0;

                if (rsMod.Rows.Count > 0)
                {
                    foreach (DataRow rsModRow in rsMod.Rows)
                    {
                        // rs.AddNew();
                        var NewRow = rs.NewRow();
                        foreach (DataColumn f in rsMod.Columns)
                        {

                            if (f.ColumnName.ToString().ToUpper() == "ICC_Noauto".ToString().ToUpper())
                            {
                                for (i = 0; i <= tpValue.Length - 1; i++)
                                {
                                    if (Convert.ToInt16(rsModRow["ICC_Noauto"]) == tpValue[i].lOldValue)
                                    {
                                        NewRow["ICC_Noauto"] = tpValue[i].lNewValue;
                                    }
                                }
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "CodeImmeuble".ToString().ToUpper())
                            {
                                NewRow["CodeImmeuble"] = sCodeImmeuble;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "COP_NoContrat".ToString().ToUpper())
                            {
                                NewRow["COP_NoContrat"] = sCop_Nocontrat;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "EQM_NoLigne".ToString().ToUpper())
                            {
                                //=== recherhcer le numéro de ligne.
                                //rs!EQM_NoLigne
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "COP_NoAuto".ToString().ToUpper())
                            {
                                NewRow["COP_NoAuto"] = lCop_NoAuto;
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "EQU_Select".ToString().ToUpper())
                            {
                                NewRow["EQU_Select"] = 0;
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "EQM_NoAuto".ToString().ToUpper())
                            {
                                //=== ne fait rien
                            }
                            else
                            {
                                NewRow[f.ColumnName] = rsModRow[f.ColumnName];
                            }
                        }

                        rs.Rows.Add(NewRow.ItemArray);
                        tmpAdors.Update();

                        Array.Resize(ref tpValueEQM, iEQM + 1);
                        tpValueEQM[iEQM].lOldValue = Convert.ToInt16(rsModRow["EQM_NoAuto"]);
                        //rs.MoveLast(); 
                        using (var tmp = new ModAdo())
                            lEQM_NoAuto = Convert.ToInt32(tmp.fc_ADOlibelle("select MAX(EQM_NoAuto) FROM EQM_EquipementP2Imm"));
                        tpValueEQM[iEQM].lNewValue = lEQM_NoAuto;
                        iEQM = iEQM + 1;

                        // rsMod.MoveNext();
                    }
                }
                rsMod.Dispose();
                rs.Dispose();


                //======== INSERTION DE LA PLANIFICATION DES GAMMES FILLES
                //sSQL = "SELECT     CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code, PEI_JD, PEI_MD, PEI_AnD, " _
                //& " PEI_JF, PEI_MF, PEI_AnF, SEM_Code, PEI_Duree, PEI_Visite, " _
                //& " PEI_Tech, PEI_Cout, PEI_ForceVisite, COP_NoContrat, PEI_TotalHT, PEI_DureeReel, " _
                //& " PEI_Intervenant, PEI_APlanifier, PEI_Calcul, PEI_IntAutre, " _
                //& " COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour, PEI_KFO, PEI_KMO, PEI_Heure," _
                //& " PEI_ForceJournee, PEI_MtAchat, PEI_Cycle, PEI_DateDebut," _
                //& " PEI_DateFin , PEI_DureeVisu, PEI_Samedi, PEI_Dimanche, PEI_Pentecote, " _
                //& " PEI_Jourferiee, PEI_NbAn, PEI_CompteurObli, TempDate " _
                //& " FROM         PEI_PeriodeGammeImm"

                sSql = "SELECT * FROM PEI_PeriodeGammeImm";

                sWhere = "";
                for (i = 0; i <= tpValueEQM.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                }

                rsMod = tmpAdorsMod.fc_OpenRecordSet(sSql + sWhere);
                rs = tmpAdors.fc_OpenRecordSet(sSql + " WHERE PEI_NoAuto = 0");
                iPEI = 0;

                if (rsMod.Rows.Count > 0)
                {
                    foreach (DataRow rsModRow in rsMod.Rows)
                    {
                        // rs.AddNew();
                        var NewRow = rs.NewRow();
                        foreach (DataColumn f in rsMod.Columns)
                        {
                            if (f.ColumnName.ToString().ToUpper() == "EQM_NoAuto".ToString().ToUpper())
                            {
                                for (i = 0; i <= tpValueEQM.Length - 1; i++)
                                {
                                    if (Convert.ToInt16(rsModRow["EQM_NoAuto"]) == tpValueEQM[i].lOldValue)
                                    {
                                        NewRow["EQM_NoAuto"] = tpValueEQM[i].lNewValue;
                                    }
                                }
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "CodeImmeuble".ToString().ToUpper())
                            {
                                NewRow["CodeImmeuble"] = sCodeImmeuble;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "COP_NoContrat".ToString().ToUpper())
                            {
                                NewRow["COP_NoContrat"] = sCop_Nocontrat;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "EQM_NoLigne".ToString().ToUpper())
                            {
                                //=== recherhcer le numéro de ligne.
                                //rs!EQM_NoLigne
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "COP_NoAuto".ToString().ToUpper())
                            {
                                NewRow["COP_NoAuto"] = lCop_NoAuto;
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "EQU_Select".ToString().ToUpper())
                            {
                                NewRow["EQU_Select"] = 0;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "PEI_Noauto".ToString().ToUpper())
                            {

                            }
                            else
                            {
                                NewRow[f.ColumnName] = rsModRow[f.ColumnName];
                            }
                        }
                        rs.Rows.Add(NewRow.ItemArray);
                        tmpAdors.Update();

                        //ReDim Preserve tpValuePEI(iPEI)
                        //tpValuePEI(iPEI).lOldValue = rsMod!PEI_NoAuto

                        //rs.MoveLast
                        //tpValuePEI(iPEI).lNewValue = rs!PEI_NoAuto
                        //iPEI = iPEI + 1

                        //rsMod.MoveNext();
                    }
                }
                rsMod.Dispose();
                rs.Dispose();


                //==== INSERTION DES TACHES.
                sSql = "SELECT * FROM FAP_FacArticleP2 ";
                sWhere = "";
                for (i = 0; i <= tpValueEQM.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                }

                rsMod = tmpAdorsMod.fc_OpenRecordSet(sSql + sWhere);
                rs = tmpAdors.fc_OpenRecordSet(sSql + " WHERE FAP_NoAuto = 0");

                if (rsMod.Rows.Count > 0)
                {
                    foreach (DataRow rsModRow in rsMod.Rows)
                    {
                        // rs.AddNew();
                        var NewRow = rs.NewRow();
                        foreach (DataColumn f in rsMod.Columns)
                        {

                            if (f.ColumnName.ToString().ToUpper() == "EQM_NoAuto".ToString().ToUpper())
                            {
                                for (i = 0; i <= tpValueEQM.Length - 1; i++)
                                {
                                    if (Convert.ToInt16(rsModRow["EQM_NoAuto"]) == tpValueEQM[i].lOldValue)
                                    {
                                        NewRow["EQM_NoAuto"] = tpValueEQM[i].lNewValue;
                                    }
                                }
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "CodeImmeuble".ToString().ToUpper())
                            {
                                NewRow["CodeImmeuble"] = sCodeImmeuble;

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "FAP_NoLIgne".ToString().ToUpper())
                            {
                                //=== recherhcer le numéro de ligne.
                                //rs!EQM_NoLigne
                            }
                            else if (f.ColumnName.ToString().ToUpper() == "COP_NoAuto".ToString().ToUpper())
                            {
                                NewRow["COP_NoAuto"] = lCop_NoAuto;
                                //ElseIf UCase(f.Name) = UCase("EQU_Select") Then
                                //       rs!EQU_Select = 0

                            }
                            else if (f.ColumnName.ToString().ToUpper() == "FAP_NoAuto".ToString().ToUpper())
                            {

                            }
                            else
                            {
                                NewRow[f.ColumnName] = rsModRow[f.ColumnName];
                            }
                        }

                        tmpAdors.Update();
                        // rsMod.MoveNext();
                    }
                }

                rsMod.Dispose();
                rs.Dispose();
                rsMod = null;
                rs = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModVisiteP2;fc_InsertModele");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Mondir le 08.04.2021 pour ajouter la version V24.03.2021
        /// Mondir le 08.04.2021 this fonction not yet tested coz it is not used yet
        /// </summary>
        /// <param name="lMP2_ID"></param>
        /// <param name="lPDAB_Noauto"></param>
        /// <param name="sCodeimmeuble"></param>
        /// <param name="lCOP_NoAuto"></param>
        /// <param name="sCop_Nocontrat"></param>
        /// <returns></returns>
        public static bool fc_InsertModeleV2(int lMP2_ID, int lPDAB_Noauto, string sCodeimmeuble, int lCOP_NoAuto, string sCop_Nocontrat)
        {
            string sWhere = "";
            string sSQL = "";
            DataTable rsMod = null;
            ModAdo rsModModAdo = new ModAdo();
            DataTable rs = null;
            ModAdo rsModAdo = new ModAdo();

            var tpValue = new value[0];
            var tpValueEQM = new value[0];
            var tpValuePEI = new value[0];
            int i = 1;
            int iEQM;
            int iPEI;
            //DataColumn f;

            var fc_InsertModeleV2 = false;

            //=== retourne true si l'insertion s'est bien déroulé.

            try
            {
                //================INSERTION DES GAMMES.
                //sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier, COP_Noauto, GAI_NoLigne, CodeImmeuble, GAI_Compteur, GAI_CompteurObli" _
                //        & " From GAI_GammeImm " _

                //Set rsMod = fc_OpenRecordSet(sSQL & " WHERE  PDAB_Noauto =" & lPDAB_NoautoMod & " AND COP_Noauto =" & lCOP_NoautoMod)

                sSQL = "SELECT        GAN_ID, GAN_Code, GAN_LIbelle, GAN_Select, GAN_NePasPlanifier, GAN_NoLigne, GAN_Compteur,"
                        + " GAN_CompteurObli, MP2_ID"
                        + " From GAN_GammeMod "
                        + " WHERE        MP2_ID = " + lMP2_ID
                        + " order by GAN_NoLigne ";

                rsMod = rsModModAdo.fc_OpenRecordSet(sSQL);

                if (rsMod == null || rsMod.Rows.Count == 0)
                {
                    fc_InsertModeleV2 = false;
                    return fc_InsertModeleV2;
                }

                sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier, COP_Noauto, GAI_NoLigne, CodeImmeuble, GAI_Compteur, GAI_CompteurObli"
                        + " From GAI_GammeImm ";

                rs = rsModAdo.fc_OpenRecordSet(sSQL + " WHERE   GAi_ID = 0");

                foreach (DataRow rsModRow in rsMod.Rows)
                {
                    var rsNewRow = rs.NewRow();

                    rsNewRow["CodeImmeuble"] = sCodeimmeuble;
                    rsNewRow["GAM_Code"] = rsMod.Rows[0]["GAN_Code"];
                    rsNewRow["GAI_LIbelle"] = rsMod.Rows[0]["GAN_LIbelle"];

                    //==============IMPORTANT.
                    //=== recherche le numéro de ligne.
                    rsNewRow["GAI_NoLigne"] = 0;
                    rsNewRow["COP_NoAuto"] = lCOP_NoAuto;
                    rsNewRow["GAI_NePasPlanifier"] = rsMod.Rows[0]["GAN_NePasPlanifier"];
                    rsNewRow["PDAB_Noauto"] = lPDAB_Noauto;
                    rsNewRow["GAI_Select"] = 0;
                    rsNewRow["GAI_CompteurObli"] = rsMod.Rows[0]["GAN_CompteurObli"];
                    rsNewRow["GAI_Compteur"] = rsMod.Rows[0]["GAN_Compteur"];

                    rsMod.Rows.Add(rsNewRow);

                    rsModModAdo.Update("GAi_ID", rsNewRow);

                    Array.Resize(ref tpValue, i);
                    tpValue[i - 1].lOldValue = rsMod.Rows[0]["GAN_ID"].ToInt();

                    tpValue[i - 1].lOldValue = rsNewRow["GAi_ID"].ToInt();

                    i = i + 1;
                }

                rsModAdo.Close();
                rsModAdo.Close();

                //==================INSERTION DES 2 EME NIVEAU DES GAMMES.
                sSQL = "SELECT     CAI_Code, CAI_Libelle, ICN_Noauto, ICN_NoLigne, ICN_Cout, COS_NoAuto, ICN_Affiche, ICN_Select, GAN_ID"
                + " From ICN_IntervCategorieContratMod ";

                sWhere = "";
                for (i = 0; i < tpValue.Length; i++)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE GAN_ID =" + tpValue[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR GAN_ID =" + tpValue[i].lOldValue;
                    }
                }

                //Set rsMod = fc_OpenRecordSet(sSQL & " WHERE  PDAB_Noauto =" & lPDAB_NoautoMod & " AND COP_Noauto =" & lCOP_NoautoMod)
                rsMod = rsModModAdo.fc_OpenRecordSet(sSQL + sWhere);

                if (rsMod != null && rsMod.Rows.Count == 0)
                {
                    fc_InsertModeleV2 = false;
                    rsModModAdo.Close();
                    return fc_InsertModeleV2;
                }

                sSQL = "SELECT     CodeImmeuble, CAI_Code, CAI_Libelle, ICC_Noauto, ICC_NoLigne, COP_Noauto, ICC_Cout, "
                        + " COS_NoAuto, ICC_Affiche, PDAB_Noauto, ICC_Select, GAi_ID "
                        + " From ICC_IntervCategorieContrat ";
                rs = rsModAdo.fc_OpenRecordSet(sSQL + " WHERE   ICC_Noauto = 0 ");

                foreach (DataRow rsModRow in rsMod.Rows)
                {
                    var rsNewRow = rs.NewRow();

                    for (i = 0; i < tpValue.Length; i++)
                    {
                        if (rsModRow["GAi_ID"].ToInt() == tpValue[i].lOldValue)
                        {
                            rsNewRow["GAi_ID"] = tpValue[i].lNewValue;
                        }
                    }

                    rsNewRow["CodeImmeuble"] = sCodeimmeuble;
                    rsNewRow["CAI_Code"] = rsModRow["CAI_Code"];
                    rsNewRow["CAI_Libelle"] = rsModRow["CAI_Libelle"];
                    //==============IMPORTANT.
                    //=== recherche le numéro de ligne.
                    rsNewRow["ICC_NoLigne"] = 0;
                    rsNewRow["COP_NoAuto"] = lCOP_NoAuto;
                    rsNewRow["ICC_Cout"] = rsModRow["ICN_Cout"];
                    rsNewRow["COS_NoAuto"] = rsModRow["COS_NoAuto"];
                    rsNewRow["ICC_Affiche"] = rsModRow["ICN_Affiche"];
                    rsNewRow["PDAB_Noauto"] = lPDAB_Noauto;
                    rsNewRow["ICC_Select"] = 0;

                    rs.Rows.Add(rsNewRow);
                    rsModAdo.Update("ICN_Noauto", rsNewRow);
                    Array.Resize(ref tpValue, i);
                    tpValue[i].lOldValue = rsModRow["ICC_Noauto"].ToInt();

                    tpValue[i].lNewValue = rsNewRow["ICN_Noauto"].ToInt();
                    i = i + 1;
                }

                rsModModAdo.Close();
                rsModAdo.Close();

                //======================INSERTION DU 3 EME NIVEAU.

                sSQL = "SELECT * FROM EQN_EquipementP2ImmMod";

                sWhere = "";
                for (i = 0; i < tpValue.Length; i++)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE ICN_Noauto =" + tpValue[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR ICN_Noauto =" + tpValue[i].lOldValue;
                    }
                }

                rsMod = rsModModAdo.fc_OpenRecordSet(sSQL + sWhere);

                sSQL = "SELECT * FROM EQM_EquipementP2Imm";
                rs = rsModAdo.fc_OpenRecordSet(sSQL + " WHERE EQM_NoAuto = 0");

                iEQM = 0;

                foreach (DataRow rsModRow in rsMod.Rows)
                {
                    var rsNewRow = rs.NewRow();
                    rsNewRow["CodeImmeuble"] = sCodeimmeuble;
                    rsNewRow["COP_NoContrat"] = sCop_Nocontrat;
                    rsNewRow["COP_NoAuto"] = lCOP_NoAuto;
                    rsNewRow["EQU_Select"] = 0;

                    foreach (DataColumn f in rsMod.Columns)
                    {
                        if (f.ColumnName.ToUpper() == "ICN_Noauto".ToUpper())
                        {
                            for (i = 0; i < tpValue.Length; i++)
                            {
                                if (rsModRow["ICN_Noauto"].ToInt() == tpValue[i].lOldValue)
                                {
                                    rsNewRow["ICC_Noauto"] = tpValue[i].lNewValue;
                                }
                            }
                        }
                        else if (f.ColumnName.ToUpper() == "EQn_NoAuto".ToUpper())
                        {
                            //=== ne fait rien
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Observation".ToUpper())
                        {
                            rsNewRow["EQM_Observation"] = rsModRow["EQN_Observation"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Metier".ToUpper())
                        {
                            rsNewRow["EQM_Metier"] = rsModRow["EQN_Metier"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Niveau".ToUpper())
                        {
                            rsNewRow["EQM_Niveau"] = rsModRow["EQN_Niveau"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Caractere".ToUpper())
                        {
                            rsNewRow["EQM_Caractere"] = rsModRow["EQN_Caractere"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_NoLigne".ToUpper())
                        {
                            rsNewRow["EQM_Noligne"] = rsModRow["EQN_NoLigne"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Affiche".ToUpper())
                        {
                            rsNewRow["EQM_Affiche"] = rsModRow["EQN_Affiche"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_TypeIntervenant".ToUpper())
                        {
                            rsNewRow["EQM_TypeIntervenant"] = rsModRow["EQN_TypeIntervenant"];
                        }
                        else if (f.ColumnName.ToUpper() == "EQN_Select".ToUpper())
                        {
                            rsNewRow["EQN_Select"] = rsModRow["EQN_Select"];
                        }
                        else
                        {
                            rsNewRow[f.ColumnName] = rsModRow[f.ColumnName];
                        }
                    }

                    rs.Rows.Add(rsNewRow);
                    rsModAdo.Update("EQM_NoAuto", rsNewRow);

                    Array.Resize(ref tpValueEQM, iEQM);
                    tpValueEQM[iEQM].lOldValue = rsModRow["EQN_NoAuto"].ToInt();

                    tpValueEQM[iEQM].lNewValue = rsNewRow["EQM_NoAuto"].ToInt();
                    iEQM = iEQM + 1;

                }

                rsModModAdo.Close();
                rsModAdo.Close();

                //======== INSERTION DE LA PLANIFICATION DES GAMMES FILLES
                //sSQL = "SELECT     CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code, PEI_JD, PEI_MD, PEI_AnD, " _
                //& " PEI_JF, PEI_MF, PEI_AnF, SEM_Code, PEI_Duree, PEI_Visite, " _
                //& " PEI_Tech, PEI_Cout, PEI_ForceVisite, COP_NoContrat, PEI_TotalHT, PEI_DureeReel, " _
                //& " PEI_Intervenant, PEI_APlanifier, PEI_Calcul, PEI_IntAutre, " _
                //& " COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour, PEI_KFO, PEI_KMO, PEI_Heure," _
                //& " PEI_ForceJournee, PEI_MtAchat, PEI_Cycle, PEI_DateDebut," _
                //& " PEI_DateFin , PEI_DureeVisu, PEI_Samedi, PEI_Dimanche, PEI_Pentecote, " _
                //& " PEI_Jourferiee, PEI_NbAn, PEI_CompteurObli, TempDate " _
                //& " FROM         PEI_PeriodeGammeImm"

                //=== voir si il faut reprendre le sous traitant.

                sSQL = "SELECT * FROM PEN_PeriodeGammeImmMod";

                sWhere = "";
                for (i = 0; i < tpValueEQM.Length; i++)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE EQN_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR EQN_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                }

                rsMod = rsModModAdo.fc_OpenRecordSet(sSQL + sWhere);

                sSQL = "SELECT * FROM PEI_PeriodeGammeImm";
                rs = rsModAdo.fc_OpenRecordSet(sSQL + " WHERE PEI_NoAuto = 0");

                iPEI = 0;

                foreach (DataRow rsModRow in rsMod.Rows)
                {
                    var rsNewRow = rs.NewRow();
                    rsNewRow["CodeImmeuble"] = sCodeimmeuble;
                    rsNewRow["COP_NoContrat"] = sCop_Nocontrat;
                    rsNewRow["COP_NoAuto"] = lCOP_NoAuto;
                    //rs!EQM_NoAuto = rsMod!EQM_NoAuto
                    rsNewRow["TPP_Code"] = rsModRow["TPP_Code"];
                    rsNewRow["PEI_JD"] = rsModRow["PEN_JD"];
                    rsNewRow["PEI_MD"] = rsModRow["PEN_MD"];
                    rsNewRow["PEI_And"] = rsModRow["PEN_AnD"];
                    rsNewRow["PEI_JF"] = rsModRow["PEN_JF"];
                    rsNewRow["PEI_MF"] = rsModRow["PEN_MF"];
                    rsNewRow["PEI_Anf"] = rsModRow["PEN_AnF"];
                    rsNewRow["SEM_Code"] = rsModRow["SEM_Code"];
                    rsNewRow["PEI_Duree"] = rsModRow["PEN_Duree"];
                    rsNewRow["PEI_Visite"] = rsModRow["PEN_Visite"];
                    rsNewRow["PEI_Tech"] = rsModRow["PEN_Tech"];
                    rsNewRow["PEI_Cout"] = rsModRow["PEN_Cout"];
                    rsNewRow["PEI_ForceVisite"] = rsModRow["PEN_ForceVisite"];
                    rsNewRow["PEI_TotalHT"] = rsModRow["PEN_TotalHT"];
                    rsNewRow["PEI_DureeReel"] = rsModRow["PEN_DureeReel"];
                    rsNewRow["PEI_Intervenant"] = rsModRow["PEN_Intervenant"];
                    rsNewRow["PEI_APlanifier"] = rsModRow["PEN_APlanifier"];

                    rsNewRow["PEI_Calcul"] = rsModRow["PEN_Calcul"];
                    rsNewRow["PEI_IntAutre"] = rsModRow["PEN_IntAutre"];
                    rsNewRow["PEI_AvisPassage"] = rsModRow["PEN_AvisPassage"];
                    rsNewRow["PEI_AvisNbJour"] = rsModRow["PEN_AvisNbJour"];
                    rsNewRow["PEI_KFO"] = rsModRow["PEN_KFO"];
                    rsNewRow["PEI_KMO"] = rsModRow["PEN_KMO"];
                    rsNewRow["PEI_Heure"] = rsModRow["PEN_Heure"];
                    rsNewRow["PEI_ForceJournee"] = rsModRow["PEN_ForceJournee"];
                    rsNewRow["PEI_MtAchat"] = rsModRow["PEN_MtAchat"];
                    rsNewRow["PEI_Cycle"] = rsModRow["PEN_Cycle"];
                    rsNewRow["PEI_DateDebut"] = rsModRow["PEN_DateDebut"];
                    rsNewRow["PEI_DateFin"] = rsModRow["PEN_DateFin"];
                    rsNewRow["PEI_DureeVisu"] = rsModRow["PEN_DureeVisu"];
                    rsNewRow["PEI_Samedi"] = rsModRow["PEN_Samedi"];
                    rsNewRow["PEI_Dimanche"] = rsModRow["PEN_Dimanche"];
                    rsNewRow["PEI_Pentecote"] = rsModRow["PEN_Pentecote"];
                    rsNewRow["PEI_Jourferiee"] = rsModRow["PEN_Jourferiee"];
                    rsNewRow["PEI_NbAn"] = rsModRow["PEN_NbAn"];
                    rsNewRow["PEI_CompteurObli"] = rsModRow["PEN_CompteurObli"];
                    rsNewRow["TempDate"] = rsModRow["TempDate"];

                    //=== REVOIR CE CODE.
                    foreach (DataColumn f in rsMod.Columns)
                    {
                        if (f.ColumnName.ToUpper() == "EQN_NoAuto".ToUpper())
                        {
                            for (i = 0; i < tpValueEQM.Length; i++)
                            {
                                if (rsModRow["EQN_NoAuto"].ToInt() == tpValueEQM[i].lOldValue)
                                {
                                    rsNewRow["EQM_NoAuto"] = tpValueEQM[i].lNewValue;
                                }
                            }
                        }
                    }

                    rs.Rows.Add(rsNewRow);
                    rsModAdo.Update();

                    //ReDim Preserve tpValuePEI(iPEI)
                    //tpValuePEI(iPEI).lOldValue = rsMod!PEI_NoAuto

                    //rs.MoveLast
                    //tpValuePEI(iPEI).lNewValue = rs!PEI_NoAuto
                    //iPEI = iPEI + 1
                }

                rsModModAdo?.Close();
                rsModAdo?.Close();

                //====  A FINIR LE 04/03/2021.

                //==== INSERTION DES TACHES.
                sSQL = "SELECT * FROM FAP_FacArticleP2 ";

                sWhere = "";
                for (i = 0; i < tpValueEQM.Length; i++)
                {
                    if (sWhere == "")
                    {
                        sWhere = " WHERE EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                    else
                    {
                        sWhere = sWhere + " OR EQM_NoAuto =" + tpValueEQM[i].lOldValue;
                    }
                }

                rsMod = rsModModAdo.fc_OpenRecordSet(sSQL + sWhere);

                rs = rsModAdo.fc_OpenRecordSet(sSQL + " WHERE FAP_NoAuto = 0");

                foreach (DataRow rsModRow in rsMod.Rows)
                {
                    var rsNewRow = rs.NewRow();

                    foreach (DataColumn f in rsMod.Columns)
                    {
                        if (f.ColumnName.ToUpper() == "EQM_NoAuto".ToUpper())
                        {
                            for (i = 0; i < tpValueEQM.Length; i++)
                            {
                                if (rsModRow["EQM_NoAuto"].ToInt() == tpValueEQM[i].lOldValue)
                                {
                                    rsNewRow["EQM_NoAuto"] = tpValueEQM[i].lNewValue;
                                }
                            }
                        }
                        else if (f.ColumnName.ToUpper() == "CodeImmeuble".ToUpper())
                        {
                            rsNewRow["CodeImmeuble"] = sCodeimmeuble;
                        }
                        else if (f.ColumnName.ToUpper() == "FAP_NoLIgne".ToUpper())
                        {
                            //=== recherhcer le numéro de ligne.
                            //rs!EQM_NoLigne
                        }
                        else if (f.ColumnName.ToUpper() == "COP_NoAuto".ToUpper())
                        {
                            rsNewRow["CodeImmeuble"] = lCOP_NoAuto;
                        }
                        //ElseIf UCase(f.Name) = UCase("EQU_Select") Then
                        //rs!EQU_Select = 0
                        else if (f.ColumnName.ToUpper() == "FAP_NoAuto".ToUpper())
                        {

                        }
                        else
                        {
                            rsNewRow[f.ColumnName] = rsModRow[f.ColumnName];
                        }
                    }

                    rs.Rows.Add(rsNewRow);
                    rsModAdo.Update();
                }

                rsModModAdo?.Close();
                rsModAdo?.Close();

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

            return fc_InsertModeleV2;
        }
    }
}
