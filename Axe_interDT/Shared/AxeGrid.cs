﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Shared
{
    class AxeGrid //
    {
        public UltraGrid UltraGrid { get; set; }
        public string Query { get; set; }
        public int Page { get; set; } = 1;
        public int Next { get; set; } = 100;
        public bool AllowUpdate { get; set; }
        public string OrderBy { get; set; }
        public DataTable DataTable { get; set; }
        public SqlDataAdapter SqlDataAdapter { get; set; }
        public SqlCommandBuilder SqlCommandBuilder { get; set; }
        public SqlCommand selectCommand;
        public bool ClearOldResulat { get; set; } = true;

        public AxeGrid(bool ClearOldResulat)
        {
            Page = 1;
            this.ClearOldResulat = ClearOldResulat;
        }
        public void Start(UltraGrid UltraGrid, string Query, string OrderBy, int Pagex, int Next, bool AllowUpdate)
        {
            //this.Page = Page;
            this.Next = Next;
            this.AllowUpdate = AllowUpdate;
            this.UltraGrid = UltraGrid;
            this.Query = Query;
            this.OrderBy = OrderBy;
            if (ClearOldResulat)
            {
                Page = 1;
                DataTable = new DataTable();
                UltraGrid.DataSource = DataTable;
            }
            ClearOldResulat = false;
            if (AllowUpdate && Query.Contains("join"))
                throw new Exception("The query contains a join keyword");
            if (AllowUpdate)
            {
                UltraGrid.AfterRowUpdate -= UltraGridOnAfterRowUpdate;
                UltraGrid.AfterRowUpdate += UltraGridOnAfterRowUpdate;
            }
            UltraGrid.AfterRowRegionScroll -= UltraGridOnAfterRowRegionScroll;
            UltraGrid.AfterRowRegionScroll += UltraGridOnAfterRowRegionScroll;
            LoadData(true);
        }
        private void UltraGridOnAfterRowUpdate(object sender, RowEventArgs rowEventArgs)
        {
            SqlCommandBuilder = new SqlCommandBuilder(SqlDataAdapter);
            SqlDataAdapter.Update(DataTable);
        }
        private void UltraGridOnAfterRowRegionScroll(object sender, RowScrollRegionEventArgs e)
        {
            if (e.RowScrollRegion.ScrollPosition + 17 >= (Page - 1) * Next)
            {
                LoadData();
            }
        }

        private void LoadData(bool FirstTime = false)
        {
            var TmpQuery = "";

            if (OrderBy == "")
                throw new Exception("Please enter a orderby column");

            //TmpQuery = Query + " ORDER BY " + OrderBy;

            //TmpQuery = FirstTime ? $" {TmpQuery} OFFSET 0 ROWS FETCH NEXT {Next} ROWS ONLY;" : $" {TmpQuery} OFFSET {Page * Next} ROWS FETCH NEXT {Next} ROWS ONLY;";

            //SqlDataAdapter = new SqlDataAdapter(TmpQuery, GetConnection.conn);
            //SqlDataAdapter.Fill(DataTable);

            var FinalQuery = $@"SELECT  *
            FROM ( SELECT    ROW_NUMBER() OVER(ORDER BY {OrderBy} ) AS RowNum, {Query.Replace("select", "").Replace("SELECT", "")} ) AS RowConstrainedResult WHERE   RowNum >{(Page - 1) * Next} AND RowNum <={Page * Next} ORDER BY RowNum";
            //Page++;
            selectCommand = new SqlCommand(FinalQuery, General.adocnn);
            selectCommand.Parameters.AddWithValue("Query", Query);
            selectCommand.Parameters.AddWithValue("OrderBy", OrderBy);
            selectCommand.Parameters.AddWithValue("PageSize", Next);
            selectCommand.Parameters.AddWithValue("Page", Page);


            //selectCommand.CommandType = CommandType.StoredProcedure;
            SqlDataAdapter = new SqlDataAdapter(selectCommand);
            SqlDataAdapter.Fill(DataTable);

            UltraGrid.DataSource = DataTable;
            UltraGrid.DisplayLayout.Bands[0].Columns["RowNum"].Hidden = true;
            UltraGrid.Text = $"{UltraGrid.Rows.Count}  Enrg.";
            Page++;
            if (!FirstTime)
            {
                //UltraGrid.ActiveRowScrollRegion.ScrollPosition = 0;
                UltraGrid.ActiveRowScrollRegion.ScrollPosition = Next * (Page - 1);
            }
        }
    }
}
