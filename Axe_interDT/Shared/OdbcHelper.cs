﻿using Microsoft.Win32;
using System;
using System.Linq;
using System.Runtime.InteropServices;

namespace Axe_interDT.Shared
{
    class OdbcHelper
    {

        public static string OdbcinstIniRegPath { get; set; }
        public static string OdbcIniRegPath { get; set; }

        // Request modes
        public static class OdbcRequestModes
        {
            public static int OdbcAddDsn = 1;
            public static int OdbcConfigDsn = 2;
            public static int OdbcRemoveDsn = 3;
            public static int OdbcAddSysDsn = 4;
            public static int OdbcConfigSysDsn = 5;
            public static int OdbcRemoveSysDsn = 6;
        }

        private static class OdbcDrivers
        {
            public static string Access2000 = "Microsoft Access Driver (*.MDB)";
            public static string SqlServer = "SQL Server";
            public static string SqlServerNative = "SQL Server Native Client 11.0";
        }

        [DllImport("ODBCCP32.dll")]
        private static extern bool SQLConfigDataSource(IntPtr hwndParent, int fRequest, string lpszDriver, string lpszAttributes);

        const int VbApiNull = 0;


        // Add odbc system DSN
        public static bool CreateDataSource(string server, string dsn, string description, string database, bool forceCreate = true)
        {
            if (!forceCreate && IsDsnExist(dsn)) return false;

            string connectionString = $"Server={server}\0DSN={dsn}\0Description={description}\0Database={database}\0Trusted_Connection=Yes\0";
            var result = SQLConfigDataSource((IntPtr)VbApiNull, OdbcRequestModes.OdbcAddDsn, General.fncUserName().ToUpper() == "MONDIR" ? OdbcDrivers.SqlServerNative : OdbcDrivers.SqlServer, connectionString);
            Console.WriteLine(result ? $"DataSource {dsn} Created OK" : $"There was a problem creating {dsn} DataSource");

            return result;
        }


        private static void SetRegPathForDsn()
        {

            OdbcIniRegPath = "Software\\ODBC\\ODBC.INI\\";
            OdbcinstIniRegPath = "Software\\ODBC\\ODBCINST.INI\\";

            // par defaut les utilisateurs de Gesten utilise les odbc du systeme 32bit
            //if (!Environment.Is64BitOperatingSystem)
            //{
            //    OdbcIniRegPath = "Software\\ODBC\\ODBC.INI\\";
            //    OdbcinstIniRegPath = "Software\\ODBC\\ODBCINST.INI\\";
            //}
            //else
            //{
            //    OdbcIniRegPath = "Software\\Wow6432Node\\ODBC\\ODBC.INI\\";
            //    OdbcinstIniRegPath = "Software\\Wow6432Node\\ODBC\\ODBCINST.INI\\";
            //}
        }
        // List of ODBC Driver types


        private static bool IsDsnExist(string dsnName)
        {
            bool result = false;
            try
            {
                SetRegPathForDsn();
                var sourcesKey = Registry.CurrentUser.OpenSubKey(OdbcIniRegPath);

                if (sourcesKey == null)
                {
                    Program.SaveException(new Exception($"{OdbcIniRegPath}  Registry key for sources does not exist"));
                }

                if (sourcesKey != null)
                {
                    string[] subkeys = sourcesKey.GetSubKeyNames();

                    result = subkeys.Any(s => s.Contains(dsnName));
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
            return result;
        }
    }
}
