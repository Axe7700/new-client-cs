﻿using Axe_interDT.Views;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Les_Articles;
using Axe_interDT.Views.Parametrages;
using Axe_interDT.Views.PorteFeuilleContrat;
using Infragistics.Win.UltraWinGrid;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public class ModAutorisation
    {
        public static int fc_DroitDetail(string sNomFicheReel, int message = 1)
        {
            int functionReturnValue = 0;
            string sSQL = null;
            DataTable rs = default(DataTable);
            string sMdp = null;
            int lLecture = 0;

            //== la focntion fc_DroitDetail retourne 0 si l'uilisateur ne peut accéder à la
            //== fiche envoyée en paramétre, retourne 1 si l'uilisateur a les droits neccessaire pour y acceder,
            //== retourne 2 si l'uitilsateur possede le mot de passe administrateur.

            var messageBox = message == 1 ? "Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche." : "Vous n'avez pas les autorisations nécessaires.";

            sSQL = "SELECT     DROF_NomFicheReel, DROF_MDP, DROF_MdpOuUtil"
                + " From DROF_DroitFiche"
                + " WHERE     (DROF_NomFicheReel = '" + sNomFicheReel + "')";
            ModAdo modAdors = new ModAdo();
            rs = modAdors.fc_OpenRecordSet(sSQL);
            functionReturnValue = 0;
            if (rs.Rows.Count > 0)
            {

                //=== si DROF_MdpOuUtil est à 1 le droit d'acces a la fiche est uniquement liée à un  mot de passe.

                if (Convert.ToInt16(General.nz(rs.Rows[0]["DROF_MdpOuUtil"], 0)) == 1)
                {

                    sMdp = Interaction.InputBox("Veuillez saisir un mot de passe.", "Mot de passe");
                    if (sMdp != rs.Rows[0]["DROF_MDP"].ToString())
                    {
                        sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                        if (sMdp != rs.Rows[0]["DROF_MDP"].ToString())
                        {
                            sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                            if (sMdp != rs.Rows[0]["DROF_MDP"].ToString())
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(messageBox, "Navigation annulée", MessageBoxButtons.OK);
                            }
                            else
                            {
                                functionReturnValue = 1;
                            }

                        }
                        else
                        {
                            functionReturnValue = 1;
                        }


                    }
                    else
                    {
                        functionReturnValue = 1;
                    }
                }
                else
                {
                    //+ General.fncUserName() +
                    //=== si DROF_MdpOuUtil est à  0 le droit d'acces à la fiche est liée à des droits
                    //=== (lecture, ecriture etc...).
                    //== on controle le droit de lecture.
                    //" + General.fncUserName() + "administrateur
                    if (General._ExecutionMode == General.ExecutionMode.Dev)
                        sSQL = "SELECT     DROA_Administration.DROA_Lecture"
                        + " FROM         DROA_Administration RIGHT OUTER JOIN"
                        + " USR_Users ON DROA_Administration.DROG_Nom = USR_Users.DROG_Nom"
                        + " WHERE     (USR_Users.USR_Nt = 'Administrateur') "
                        //+ " WHERE     (USR_Users.USR_Nt = 'administrateur') "
                        + " AND (DROA_Administration.DROF_NomFicheReel = '" + sNomFicheReel + "')";
                    else
                        sSQL = "SELECT     DROA_Administration.DROA_Lecture"
                            + " FROM         DROA_Administration RIGHT OUTER JOIN"
                            + " USR_Users ON DROA_Administration.DROG_Nom = USR_Users.DROG_Nom"
                            + " WHERE     (USR_Users.USR_Nt = '" + General.fncUserName() + "') "
                            //+ " WHERE     (USR_Users.USR_Nt = 'administrateur') "
                            + " AND (DROA_Administration.DROF_NomFicheReel = '" + sNomFicheReel + "')";

                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK);
                        functionReturnValue = 0;
                    }
                    else
                    {

                        lLecture = Convert.ToInt16(General.nz(rs.Rows[0]["DROA_Lecture"], 0));
                        if (lLecture == 1)
                        {
                            functionReturnValue = 1;
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK);
                            functionReturnValue = 0;

                        }
                    }

                }
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK);
                functionReturnValue = 0;
            }

            modAdors.Dispose();

            //rs = null;
            return functionReturnValue;

            ///'        sMdp = InputBox("Veuillez saisir un mot de passe.", "Mot de passe")
            ///'        If sMdp <> "intranet" Then
            ///'            MsgBox "Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", vbCritical, "Navigation annulée"
            ///'        Else
            ///'            fc_DroitDetail = 2
            ///'        End If
            ///'
        }

        public static void EnabledUDF(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    EnabledUDF(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                var tmp = C as iTalk.iTalk_TextBox_Small2;
                tmp.ReadOnly = true;
                // obj.ForeColor = vbBlue
            }
            else if (C is UltraCombo)
            {
                C.Enabled = false;
            }
            else if (C is System.Windows.Forms.CheckBox)
            {
                C.Enabled = false;
            }
            else if (C is System.Windows.Forms.ComboBox)
            {
                C.Enabled = false;
            }

        }

        public static void EnabledUDFLmodifAjout(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    EnabledUDFLmodifAjout(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                var tmp = C as iTalk.iTalk_TextBox_Small2;
                tmp.ReadOnly = false;
                // obj.ForeColor = vbBlue
            }
            else if (C is UltraCombo)
            {
                C.Enabled = true;
            }
            else if (C is System.Windows.Forms.CheckBox)
            {
                C.Enabled = true;
            }
            else if (C is System.Windows.Forms.ComboBox)
            {
                C.Enabled = true;
            }

        }

        public static void fc_DroitParFiche(string sNomFicheReel)
        {

            DataTable rs = default(DataTable);
            string sSQL = null;
            int lLecture = 0;
            int lModif = 0;
            int lSupp = 0;
            int lAjout = 0;
            int i = 0;
            object obj = null;


            try
            {

                //== controle si la fiche envoyée en paramétre a des droits au
                //== niveau du mot de passe ou de type lecture ecriture.
                sSQL = "SELECT     DROF_NomFicheReel, DROF_MDP, DROF_MdpOuUtil" + " From DROF_DroitFiche" + " WHERE     (DROF_NomFicheReel = '" + sNomFicheReel + "')";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }

                if (Convert.ToInt16(General.nz(rs.Rows[0]["DROF_MdpOuUtil"], 0)) == 1)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }
                else
                {

                    //" + General.fncUserName() + "administrateur
                    //== si la fiche a des droits au niveau de type lecture, ecriture etc...
                    if (General._ExecutionMode == General.ExecutionMode.Dev)
                        sSQL = "SELECT     DROA_Administration.DROA_Lecture, DROA_Administration.DROA_Modif," + " DROA_Administration.DROA_Supp, DROA_Administration.DROA_Ajout"
                           + " FROM         DROA_Administration INNER JOIN" + " USR_Users ON DROA_Administration.DROG_Nom = USR_Users.DROG_Nom"
                           + " WHERE     (DROA_Administration.DROF_NomFicheReel = '" + sNomFicheReel + "') " + " AND (USR_Users.USR_Nt = 'Administrateur')";
                    else
                        sSQL = "SELECT     DROA_Administration.DROA_Lecture, DROA_Administration.DROA_Modif," + " DROA_Administration.DROA_Supp, DROA_Administration.DROA_Ajout"
                                + " FROM         DROA_Administration INNER JOIN" + " USR_Users ON DROA_Administration.DROG_Nom = USR_Users.DROG_Nom"
                                + " WHERE     (DROA_Administration.DROF_NomFicheReel = '" + sNomFicheReel + "') " + " AND (USR_Users.USR_Nt = '" + General.fncUserName() + "')";

                    rs = tmpAdors.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count == 0)
                    {
                        rs.Dispose();
                        rs = null;
                        return;
                    }

                    lLecture = Convert.ToInt16(General.nz(rs.Rows[0]["DROA_Lecture"], 0));
                    lModif = Convert.ToInt16(General.nz(rs.Rows[0]["DROA_Modif"], 0));
                    lSupp = Convert.ToInt16(General.nz(rs.Rows[0]["DROA_Supp"], 0));
                    lAjout = Convert.ToInt16(General.nz(rs.Rows[0]["DROA_Ajout"], 0));

                    rs.Dispose();
                    rs = null;

                    var topControl = View.Theme.Theme.ControlAffiche;

                    switch (sNomFicheReel)
                    {
                        case Variable.cUserDocParamtre:

                            var userDocParametre = topControl as UserDocParamtre;

                            var _with1 = userDocParametre;
                            if (lModif == 1)
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridIndice.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridFormule.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridParametres.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridBE.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;

                            }
                            else
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridIndice.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridFormule.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridParametres.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridBE.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;


                            }


                            if (lAjout == 1)
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridIndice.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridFormule.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridParametres.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssGridBE.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;


                            }
                            else
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridIndice.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridFormule.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridParametres.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssGridBE.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;


                            }


                            if (lSupp == 1)
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridIndice.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridFormule.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridParametres.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssGridBE.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;


                            }
                            else
                            {
                                _with1.ssGridQualite.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridSpecifQualif.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridPersonnel.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGrilleStatutInterv.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridStatutCommande.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridAnaActivite.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridCodeEtatDevis.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False; ;
                                _with1.GridTypeFacture.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridIndice.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridTypeRatio.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridFormule.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridParametres.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.GridAchatVente.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssGridBE.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                                _with1.ssgCNC_Conccurent.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

                            }

                            break;

                        case ("UserPorteFeuilleContrat"):

                            var UPFC = topControl as UserPorteFeuilleContrat;
                            var _with2 = UPFC;

                            if (lModif == 1)
                            {
                                //.ssGridQualite.AllowUpdate = True
                            }
                            else
                            {

                            }


                            if (lAjout == 1)
                            {
                                _with2.cmdAjouter.Enabled = true;

                            }
                            else
                            {
                                _with2.cmdAjouter.Enabled = false;
                            }


                            if (lSupp == 1)
                            {
                                _with2.cmdSup.Enabled = true;
                            }
                            else
                            {
                                _with2.cmdSup.Enabled = false;
                            }


                            break;



                        case ("UserDocFournisseur"):

                            var UDF = topControl as UserDocFournisseur;
                            var _with3 = UDF;

                            _with3.cmdAjouter.Enabled = false;
                            _with3.cmdAnnuler.Enabled = false;
                            _with3.CmdSauver.Enabled = false;
                            _with3.cmdSupprimer.Enabled = false;

                            for (i = 0; i < _with3.ssFNS.DisplayLayout.Bands[0].Columns.Count; i++)
                            {

                                _with3.ssFNS.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }


                            for (i = 0; i < _with3.GridCptCharges.DisplayLayout.Bands[0].Columns.Count; i++)
                            {
                                _with3.GridCptCharges.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                            }


                            _with3.ssFNS.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                            _with3.ssFNS.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                            _with3.GridCptCharges.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                            _with3.GridCptCharges.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;

                            _with3.cmbCptCharges.Enabled = false;

                            EnabledUDF(topControl);

                            if (lModif == 1)
                            {
                                _with3.CmdSauver.Enabled = true;

                                for (i = 0; i < _with3.ssFNS.DisplayLayout.Bands[0].Columns.Count; i++)
                                {
                                    _with3.ssFNS.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }

                                for (i = 0; i < _with3.GridCptCharges.DisplayLayout.Bands[0].Columns.Count; i++)
                                {
                                    _with3.GridCptCharges.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }

                                EnabledUDFLmodifAjout(topControl);

                                _with3.cmbCptCharges.Enabled = true;
                            }

                            else
                            {
                                _with3.CmdSauver.Enabled = false;
                            }


                            if (lAjout == 1)
                            {
                                _with3.cmdAjouter.Enabled = true;
                                //== l ajout implique automatiquement l'activation du bouton valider.
                                _with3.CmdSauver.Enabled = true;
                                for (i = 0; i < _with3.ssFNS.DisplayLayout.Bands[0].Columns.Count; i++)
                                {

                                    _with3.ssFNS.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }
                                for (i = 0; i < _with3.GridCptCharges.DisplayLayout.Bands[0].Columns.Count; i++)
                                {

                                    _with3.GridCptCharges.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }

                                _with3.ssFNS.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                _with3.GridCptCharges.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;

                                EnabledUDFLmodifAjout(topControl);

                                _with3.cmbCptCharges.Enabled = true;
                            }
                            else
                            {
                                _with3.cmdAjouter.Enabled = false;
                            }

                            if (lSupp == 1)
                            {
                                _with3.cmdSupprimer.Enabled = true;
                                for (i = 0; i < _with3.ssFNS.DisplayLayout.Bands[0].Columns.Count; i++)
                                {

                                    _with3.ssFNS.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }
                                for (i = 0; i < _with3.GridCptCharges.DisplayLayout.Bands[0].Columns.Count; i++)
                                {

                                    _with3.GridCptCharges.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                                }

                                _with3.ssFNS.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                _with3.GridCptCharges.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;


                                EnabledUDFLmodifAjout(topControl);

                                _with3.cmbCptCharges.Enabled = true;
                            }
                            else
                            {
                                _with3.cmdSupprimer.Enabled = false;
                            }
                            break;

                        case Variable.cUserDocSageScan:

                            var userDocSageScan = topControl as UserDocSageScan;

                            var _with4 = userDocSageScan;
                            _with4.cmdComptabilise.Enabled = false;

                            if (lModif == 1)
                            {
                                _with4.cmdComptabilise.Enabled = true;
                            }
                            else
                            {
                                _with4.cmdComptabilise.Enabled = false;
                            }

                            if (lAjout == 1)
                            {
                                _with4.cmdComptabilise.Enabled = true;
                            }
                            else
                            {
                                _with4.cmdComptabilise.Enabled = false;
                            }

                            if (lSupp == 1)
                            {

                            }
                            else
                            {

                            }


                            break;

                        case ("UserDocArticle"):

                            UserDocArticle UDA = topControl as UserDocArticle;
                            UDA.GridArticle.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                            UDA.GridArticle.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                            UDA.CmdSauver.Enabled = false;

                            if (lModif == 1)
                            {
                                UDA.GridArticle.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                                UDA.CmdSauver.Enabled = true;
                            }

                            if (lAjout == 1)
                            {
                                UDA.GridArticle.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                                UDA.CmdSauver.Enabled = true;
                            }

                            if (lSupp == 1)
                            {
                                UDA.GridArticle.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                                UDA.CmdSauver.Enabled = true;
                            }
                            else
                            {

                            }
                            break;
                    }


                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, "ModAutorisation;fc_DroitParFiche");

            }
        }



        public static void fc_Droit(string sAut_Objet, string sAut_Formulaire, ref bool bAdd, ref bool bUpdate, ref bool bDelete)
        {

            int i = 0;
            try
            {

                //== Controle d'abord si l'utilisateur est un admin.
                if (General.sDroit.ToUpper() == General.cAdmin.ToUpper())
                {
                    // cmdValider.Enabled = False
                    bAdd = true;
                    bUpdate = true;
                    bDelete = true;
                    return;
                }

                //== si pas admin on controle les autorisations.
                for (i = 0; i < ModMain.tpDroit.Length; i++)
                {
                    if (ModMain.tpDroit[i].sFormulaire != null && ModMain.tpDroit[i].sFormulaire.ToUpper() == sAut_Formulaire.ToUpper()
                        && ModMain.tpDroit[i].sObjet != null && ModMain.tpDroit[i].sObjet.ToUpper() == sAut_Objet.ToUpper())
                    {
                        if (ModMain.tpDroit[i].lAjout == 1)
                        {
                            bAdd = true;
                        }
                        if (ModMain.tpDroit[i].lEcriture == 1)
                        {
                            bUpdate = true;
                        }
                        if (ModMain.tpDroit[i].lDelete == 1)
                        {
                            bDelete = false;
                        }

                    }

                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModAutorisation;fc_Droit");

            }
        }
    }
}
