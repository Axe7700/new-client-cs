﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System.Globalization;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class sheridan
    {
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Combo"></param>
        /// <param name="Requete"></param>
        /// <param name="strChampSel"></param>
        /// <param name="BlDropDown"></param>
        /// <param name="AdoConnection"></param>
        /// <param name="strChampDisplay"></param>
        public static void InitialiseCombo(UltraCombo Combo, string Requete, string strChampSel, bool BlDropDown = false, SqlConnection AdoConnection = null, string strChampDisplay = "")
        {
            try
            {
                var DataTable = new DataTable();
                var Adapter = new SqlDataAdapter(Requete, General.adocnn);
                if (AdoConnection != null)
                {
                    Adapter = new SqlDataAdapter(Requete, AdoConnection);
                }
                Adapter.Fill(DataTable);

                Combo.DataSource = DataTable;

                //===Ne pas oublier d'Alimenter le datasource du combo dans Propriété du Combo
                Combo.ValueMember = strChampSel;
                if (string.IsNullOrEmpty(strChampDisplay))
                {
                    Combo.DisplayMember = strChampSel;
                }
                else
                {
                    Combo.DisplayMember = strChampDisplay;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "sheridan;InitialiseCombo");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strStyle"></param>
        /// <param name="sscmbTemp"></param>
        public static void style(string strStyle, UltraCombo sscmbTemp)
        {
            int i = 0;
            //== saisir ici les styles personnalisées
            try
            {
                strStyle = strStyle.ToUpper();
                switch (strStyle)
                {
                    case "ECRAN BLEU":

                        for (i = 0; i <= sscmbTemp.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                        {
                            sscmbTemp.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextHAlign = HAlign.Left;
                        }

                        sscmbTemp.DisplayLayout.Override.RowAlternateAppearance.BackColor = ColorTranslator.FromOle(0xffffff);
                        if (sscmbTemp.DisplayLayout.Bands.Count > 1)
                            sscmbTemp.DisplayLayout.Bands[1].Override.RowAlternateAppearance.BackColor = ColorTranslator.FromOle(0x8000008);
                        break;
                    case "":
                        break;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Sheridan" + ";Style");
            }
        }

        public static void fc_DeleteRegGrille(string sFormName, UltraGrid objcontrole, bool blnWidth = true, bool blnPosition = true)
        {
            int i = 0;
            string sName = null;

            try
            {
                for (i = 0; i <= objcontrole.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    sName = objcontrole.DisplayLayout.Bands[0].Columns[i].Key;
                    if (blnWidth == true && blnPosition == true)
                    {
                        General.DeleteSetting(sFormName + objcontrole.Name);
                    }
                    else if (blnWidth == true)
                    {
                        General.DeleteSetting(sFormName + objcontrole.Name + sName + "_width");
                    }
                    else if (blnPosition == true)
                    {
                        if (!string.IsNullOrEmpty(General.getFrmReg(sFormName + objcontrole.Name, sName + "_Position", "")))
                        {
                            General.DeleteSetting(sFormName + objcontrole.Name + sName + "_Position");
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);

            }
        }

        public static void fc_LoadDimensionGrille(string sFormName, UltraGrid objcontrole)
        {
            int i = 0;
            string sName = null;
            string sDimension = null;
            string sRowHeight = null;
            string SeparatDecimal = null;

            //TODO : Mondir - Must Check This Fonctions

            return;

            try
            {
                SeparatDecimal = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;
                sRowHeight = General.getFrmReg(sFormName + objcontrole.Name, "RowHeight", objcontrole.DisplayLayout.Override.DefaultRowHeight.ToString());
                if (sRowHeight.Contains(SeparatDecimal) == false)
                {
                    if (SeparatDecimal == ".")
                    {
                        sRowHeight = sRowHeight.Replace(",", ".");
                    }
                    else if (SeparatDecimal == ",")
                    {
                        sRowHeight = sRowHeight.Replace(".", ",");
                    }
                }
               
                objcontrole.DisplayLayout.Override.DefaultRowHeight = (int)double.Parse(sRowHeight);

                for (i = 0; i <= objcontrole.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    sName = objcontrole.DisplayLayout.Bands[0].Columns[i].Key;
                    sDimension = General.getFrmReg(sFormName + objcontrole.Name, sName + "_width", objcontrole.DisplayLayout.Bands[0].Columns[i].Width.ToString());
                    if (sDimension.Contains(SeparatDecimal) == false)
                    {
                        if (SeparatDecimal == ".")
                        {
                            sDimension = sDimension.Replace(",", ".");
                        }
                        else if (SeparatDecimal == ",")
                        {
                            sDimension = sDimension.Replace(".", ",");
                        }
                    }
                    objcontrole.DisplayLayout.Bands[0].Columns[i].Width = (int)double.Parse(sDimension);
                    //objcontrole.DisplayLayout.Bands[0].Columns[i].Header.VisiblePosition = Convert.ToInt32(General.getFrmReg(sFormName + objcontrole.Name, sName + "_Position", objcontrole.DisplayLayout.Bands[0].Columns[i].Header.VisiblePosition.ToString()));
                }
            }
            catch(Exception ex)
            {
                Program.SaveException(ex);

                General.DeleteSetting(sFormName + objcontrole.Name);
            }
        }
        public static void fc_formatGrille(UltraGrid ssGrid, bool blecriture = true, bool bMultiselectRow = false, bool blStyleset = false)
        {
            bool bMultiselect = false;

            var _with1 = ssGrid;
            // RowNavigation = ssRowNavigationFull
            //  _with1.DisplayLayout.Override.RowLayoutCellNavigationVertical = SSDataWidgets_B_OLEDB.Constants_RowNavigation.ssRowNavigationFull;
            _with1.DisplayLayout.Override.SelectTypeCol = SelectType.None;
            _with1.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            _with1.DisplayLayout.Override.AllowDelete = DefaultableBoolean.True;
            _with1.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
            _with1.DisplayLayout.Override.AllowColMoving = AllowColMoving.NotAllowed;
            //_with1.  .AllowColumnShrinking = False //verifier
            _with1.DisplayLayout.Override.AllowColSwapping = AllowColSwapping.NotAllowed;

            if (bMultiselect == false)
            {
                _with1.DisplayLayout.Override.SelectTypeRow = SelectType.Single;
            }
            else
            {
                _with1.DisplayLayout.Override.SelectTypeRow = SelectType.Extended;
            }
            // _with1.MultiLine = false;verifier
            _with1.DisplayLayout.Appearance.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffff);
            //blanc
            _with1.DisplayLayout.Appearance.BackColor2 = System.Drawing.ColorTranslator.FromOle(0xffffff);
            //jaune
            _with1.DisplayLayout.Appearance.ForeColor = System.Drawing.ColorTranslator.FromOle(0x0);
            // noir
            if (blStyleset == true)
            {
                //  .StyleSets.Add "yesStyle"
                //  .StyleSets("yesStyle").BackColor = &HFF8080
                //  .StyleSets.Add "NoStyle"
                //  .StyleSets("NoStyle").BackColor = &HFFFFFF
                //  .StyleSets.Add "Jaune"
                //.StyleSets("Jaune").BackColor = &HC0FFFF
                /*
                 * _with1.StyleSets.Add("ALERTE");
                 _with1.StyleSets("ALERTE").BackColor = 0x8080ff;
                 // rouge
                 _with1.StyleSets("ALERTE").ForeColor = 0x80000012;
                 // noir

                 _with1.StyleSets.Add("VERT");
                 _with1.StyleSets("VERT").BackColor = 12648384;
                 // vert

                 _with1.StyleSets.Add("GrisFonce");
                 _with1.StyleSets("GrisFonce").BackColor = 0xc0c0c0;
                 // vert*/

            }
        }
        public static void AlimenteCmbAdditemTableau(UltraCombo sscmbTemp, string[] tTAb, string strStyle = "Ecran bleu")
        {
            // pour les controles additem saisir d'abord en dur
            //  la propriétes cols avec le nombre de colonnes souhaités
            // ne gére qu' un tableau à une dimension

            try
            {
                style(strStyle, sscmbTemp);
                sscmbTemp.DataSource = tTAb;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Sheridan" + ";AlimenteCmbAdditem");
                return;
            }
        }
        public static void fc_ColorLigneEncours(UltraGrid ssdbGrid)
        {//TODO
            //color la ligne en cours à mettre dans  _RowColChange
            int i = 0;
            var _with3 = ssdbGrid;


            // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
            for (i = 0; i <= _with3.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                // _with3.Columns(i).CellStyleSet("yesStyle", _with3.Row
               // _with3.DisplayLayout.Bands[0].Columns[i].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Color;
            }
        }
        public static bool fc_FindLibelle(UltraCombo Combo, string sMotAChercher, string sNameCol, string sNameColLibelle = "", string sLibelle = "")
        {
            bool functionReturnValue = false;

            //-- cette fonction retourne true si la valeur à rechercher se trouve dans la liste
            //-- déroulante envoyé dans la var. combo et retourne également le libelle dans la
            //-- variable sLibelle uniquement si la var. sNameColLibelle est alimenté par le nom
            //-- de la colonne ou se trouve le libellé.
            int i = 0;


            for (i = 0; i <= Combo.Rows.Count - 1; i++)
            {

                if (sMotAChercher == Combo.ActiveRow.Cells[sNameCol].Value.ToString())
                {
                    functionReturnValue = true;
                    if (!string.IsNullOrEmpty(sNameColLibelle))
                    {
                        sLibelle = Combo.ActiveRow.Cells[sNameColLibelle].Value.ToString();
                    }
                    break; // TODO: might not be correct. Was : Exit For
                }

                // Combo.MoveNext();
            }
            return functionReturnValue;
        }
        public static void fc_SavDimensionGrille(string sFormName, UltraGrid objcontrole)
        {
            return;
            int i;
            string sName;
            if (General.getFrmReg(sFormName, "SaveGrid", "0") != "0")
            //if (GetSetting(cFrNomApp, sFormName, "SaveGrid", "0") != "0")
            {
                //General.saveInReg(sFormName + objcontrole.Name, "RowHeight", objcontrole.Height.ToString());//TODO check this line
                General.saveInReg(sFormName + objcontrole.Name, "RowHeight", objcontrole.DisplayLayout.Override.DefaultRowHeight.ToString());///This libe Added By Mohammed
                //SaveSetting(cFrNomApp, sFormName + objcontrole.Name, "RowHeight", objcontrole.RowHeight);
                for (i = 0; i <= objcontrole.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    sName = objcontrole.DisplayLayout.Bands[0].Columns[i].Key;

                    General.saveInReg(sFormName + objcontrole.Name, sName + "_width", objcontrole.DisplayLayout.Bands[0].Columns[i].Width.ToString());
                    General.saveInReg(sFormName + objcontrole.Name, sName + "_Position", objcontrole.DisplayLayout.Bands[0].Columns[i].Index.ToString());

                    //SaveSetting(cFrNomApp, sFormName + objcontrole.Name, sName + "_width", objcontrole.Columns(i).Width);
                    //SaveSetting(cFrNomApp, sFormName + objcontrole.Name, sName + "_Position", objcontrole.Columns(i).Position);
                }
            }
            else
                General.saveInReg(sFormName, "SaveGrid", "1");
            //SaveSetting(cFrNomApp, sFormName, "SaveGrid", "1");
        }
    }
}
