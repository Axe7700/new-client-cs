﻿using Axe_interDT.View.Theme;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Facture_Mannuel;
using Axe_interDT.Views.Intervention.Forms;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using Microsoft.Win32;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Management;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class General
    {
        public enum ExecutionMode
        {
            Dev,
            Test,
            Prod
        }

        //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        public enum Company
        {
            DT,
            LONG,
            VF
        }
        public static string sClimMat;
        public static string sEtatExportImmAstreinteV9;
        public static string PROGUserDocExportImmAstreinte;
        public static string sEvolution { get; set; }
        public static string sCptBloqueSurFacture;
        public static string MsgDestinataire;
        public static string sCodeSvrClim;
        public static string sCodeSvrExpliot;
        public static string sCodeSvrTrx;
        public static string sCritreDevisFac;
        public static string sMDParticle;
        public static string sChefSecteurLibre;
        public static ExecutionMode _ExecutionMode { get; set; }
        public static Company _Company { get; set; }

        public const string cArtiNonQualifie = "BD2_";
        public static int lCreateInterViaGestAppel;
        public const string cPart = "PART";
        public static string sNoInterHistorique;
        public const string cOkToNavigue = "OK to navigate";
        public static bool bActiveAppelAlgoria;
        //public static bool InTestMode { get; set; } = false;
        static string regpath { get; set; } = @"HKEY_CURRENT_USER\Software\";
        public static DateTime cDefDateRealise = new DateTime(2000, 01, 01);
        public static DateTime cDateCalMo = new DateTime(2014, 09, 01);

        public static bool bDebugIntranet;

        //===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        public static string sChargeGmao { get; set; }
        //===> Fin Modif Mondir

        public static bool bLoadAlertAppel { get; set; }
        public static bool bLoadGestionAppel { get; set; }

        //=== constantes pour l'analytique secteur.
        //Public Const cANSC_FactFourn = "Facture Fournisseur"
        public const string cTYFO_Secteur7 = "SECTEUR7";
        public const string cTYFO_SecteurClim = "SECTEUR5";

        public const string cTYFO_SecteurFioul = "SECTEUR6";

        //Public Const cANAB_PLOMBERIE = "COUVERTURE"
        //Public Const cANAB_Couverture = "PLOMBERIE"
        public const string cANAB_Administratif = "SECTEUR3";

        public const string cProgIntranetV3PDA = "IntranetV3PDA";
        public const string cProgIntranetV3report = "IntranetV3report";

        public const string cExportDev = "DTDevisSQL-V9.rpt";
        public const string cExportDevV2 = "DTDevisSQL-V9-2.rpt";
        public const string cExportDevV2clim = "DTDevisSQL-V9-2clim.rpt";

        public static string sInfoPresetationV2;
        public static string sFileRapportPhoto;
        public static string RAPPORTINTERVENTIONV2_1;

        public static string sLastConnection;
        public static Timer TimerStandard;
        public static string sUChaufferie;
        public static string sArticleAllumer;
        public static string sArticleArret;
        public static string sPrestationAE;
        public static string sPathEdition;
        public static string sMoisDebutSaison;
        public static string sEcartSaisieFOD;
        public static string sAffRelevesInDispatch;
        public static string FacturesP2Lisa;
        public static string sBaseMetier;

        public static string sActiveCtlSatutInter;
        public static string sBloqueAchatSurDevis;

        public static ExerciceCtr[] tpExerciceCtr;

        public static string sExerciceDeb;
        public static string sExerciceFin;
        public static int lDetailExercice;
        public static int lGraphExelComp;

        public struct ExerciceCtr
        {
            public string sLibelle;
            public System.DateTime dtDebut;
            public System.DateTime dtFin;
        }

        public const int cArt80000 = 80000;
        public static int lChoixModele;

        public const string cContactSite = "CONTACTSITE";
        public const string cContactClient = "CONTACTCLIENT";
        public const string cContactGroupe = "CONTACTGROUPE";
        public const string cIntervEntretien = "AE";

        public const string cNewDtDevisSQL = "NewDtDevisSQL.rpt";
        //==================================
        public static string sRDOmail;

        public static string sTomtomLat;
        public static string stomtomLong;
        public static string sTomtomAdresse;
        public static string sAnalytiqueBCD;
        public static string sDateAnalytiqueBDC;
        public static string sVersionDevisV2;

        public static string sSQL;
        public static string sSQlAuto;

        public static int lDEVC_Noauto;

        public static int lDevisPzCopieA;

        public const string cGestionnaire = "GES";
        public const short clTotalRecord = 5;
        // format du lien des lien
        // sert pour la mise en forme
        public const bool cUnderline = true;
        public const int cForeColorLien = 0xc000c0;
        public const int cForeColorStandard = 0x0;
        public const int cNoir = 0x0;
        public const int cBleu = 0xff0000;
        public const uint cBlanc = 0x80000005;
        public const string cFournFrmMajFourn = "Tous les Fournisseurs";
        public const string cAN = "AN";
        public const string cEtatZZ = "ZZ";

        //=== constantes pour les boutons ajouter, valider, supprimer.
        public const string cAjouter = "Ajouter";

        public static string sFournOuClient;
        public const string cFourn = "Fournisseur";
        public const string cClient = "Client";

        public const string cArticleDefaut = "80000";

        public const string cMSGEDITEUR = "Veuillez contacter l'éditeur du Progiciel.";

        //========
        public const string cMulti = " Appuyer sur entrée pour afficher la fiche de recherche multicritère pour affiner votre recherche ou bien effacer la valeur saisie.";
        public const string cMSERR = "Données érronées";
        public const string cMSGDOUBLONS = "Cet enregistrement éxiste déjà.";
        public const string cMSGPRESENT = "Cet enregistrement éxiste déjà, appuyer plusieurs fois sur le bouton ECHAP(ESC) pour annuler ou modifier la valeur.";
        public const string cMSGDate = "Date invalide, veuillez entrer une date sous la forme 00/00/00 ou supprimer votre saisie";
        public const string cMSGTime = "Heure invalide, veuillez entrer une heure sous la forme 00:00:00 ou supprimer votre saisie";
        public const string DOSSIERFAX = "C:\\FAXLIBEM";
        public const string FICHIERFAX = "FaxEmission.reg";

        public const string PROMPTSUPPRESSION = "Vous allez supprimer un enregistrement.";
        public const string TITRESUPPRESSION = "Suppression";
        public const string PROMPTCHAINEVIDE = "Attention les champs N°Contrat et Avenant sont obligatoires";
        public const string TITREDONNEEFAUX = "Données éronnéés";
        public const string PROMPTPRIMAIREFAUX = "Ces numero de contrat et d'avenant existe déja." + "\n" + " Veuillez les remplacer et valider.";


        //*************** Partie Relevé de compte ***************
        public static string strSyntheseReleveDeComerc;
        public static string strSyntheseReleveDeviseur;
        public static string strReleveDeSyndic;
        public static string strReleveImmeuble;

        public static string strReleveDeviseur;
        //*************** Fin Partie Relevé de compte ***********

        public const string PROMPTAJOUT = "Vous allez ajouter un enregistrement";
        public const string TITREAJOUT = "Ajout";
        public const string PROMPTDATE = "Cette date n'est pas correcte";
        public const string PROMPTSANSPARAM = "Vous devez saisir des critéres de recherche";
        public const string TITRESANSPARAM = "Message";
        public const string PROMPTOUTIMPR = "Voulez vous imprimer la totalité des feuilles";
        public const string TITRETOUTIMPR = "Avertissement";

        public static string sPhyPath;
        public static string sVirPath;

        //========================================================
        public const string cSocietebis = "ECCEP";
        // Public Const cSociete = "ECCEP"
        public const string cLibSociete = "Génie climatique";
        public static int lngfrmMsgbox;

        //========================================================

        public static string sFACTURATIONTRAVAUX;
        public static string sAnalyTravauxAuto;
        public static string sSepSMS;

        public static string sNewCritereP2V2;
        public static string sContactMailViaBase;
        public static string sInitAdressListOutlook345_V2;
        public static string sImmGeolocObligatoire;
        public static string sDesactiveInform;
        public static string sDroitListeRouge;
        public static string sRamoneurParImmeuble;

        public static string sLattitudeImmeuble;
        public static string sLongitudeimmeuble;
        public static string sAdresseGPS;


        public static string CHEMINBASE { get; set; }
        public const short cCopro = 10;
        public const short cGerant = 12;
        public const short cFournisseur = 13;
        public const short cGardien = 14;
        public const short cDivers = 15;
        public const short cSociete = 16;

        public static bool bPrixGecetAActualiser;

        public const short cTel = 0;
        public const string cTelLbl = "Téléphone";

        public const short cFax = 1;
        public const string cFaxlbl = "Fax";

        public const short cInternet = 2;
        public const string cInternetlbl = "Internet";

        public const short cRepondeur = 3;
        public const string cRepondeurlbl = "Répondeur";

        public const short cCourrier = 4;
        public const string cCourrierlbl = "Courrier";

        public const short cRadio = 5;
        public const string cRadiolbl = "Radio";

        public const short cDirect = 6;
        public const string cDirectlbl = "Direct";

        public const short CEmail = 7;
        public const string cEmaillbl = "Email";

        public const short CAstreinte = 8;
        public const string Astreintelbl = "Astreinte";

        public const string cInconnulbl = "Inconnu";

        public static string sNbrMinuteFact;
        public static string sCoefAchatsMin;


        public static bool blControle;
        //facture manuel
        public static bool boolautre;


        public static string gsUtilisateur { get; set; }

        public static string gsCheminPackage;

        public static string gsRpt;
        public static string sCheminDossier { get; set; }
        public const string cSQLorAccess = "1";

        //Global Const DefaultSource = "Source_OLEDBLocal"
        public const string DefaultSource = "Source_OLEDB";
        public const string cLastConnection = "LastConnection";

        public static int lngInitRegitre;
        public static string cFrNomApp { get; set; }
        public static string cRegInter { get; set; } = "Axe_interDT";
        public const string cRegInterGdp = "INTERGDP";
        public const string cNameGDP = "AXECIEL_GAZDEPARIS";
        public const string cNameDelostal = "AXECIEL_DELOSTAL";
        public const string cNameDelostalTest = "AXECIEL_DELOSTAL_PDA";
        public const string cNameAmmann = "AXECIEL_AMMANN";
        public const string cNameLong = "AXECIEL_LONG";
        //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        public const string cNameVF = "AXECIEL_VanFroid";
        //===> Fin Modif 

        public const string cRegInterLong = "INTERLONG";



        public const string cCritereFinance = " AND CG_Num NOT LIKE '41131%' ";
        public const string cCrietereFinanceBis = ".dbo.F_ECRITUREC.CG_NUM  NOT LIKE '41131%' ";

        public static string cFrODBCbaseReseau { get; set; } = "Data Source=DTMDB";
        public static SqlConnection adocnn { get; set; }

        public static string PROGUserFacIndice;
        public static string gsProg;


        public static string sGecetV3;

        public static string PROGUserDocReleve;

        public static string sActiveDroitPlan;
        public static string sActiveModeReglementSageFourn;

        public static string ETATBONCOMMANDE;


        public const string cCorrigeFact = "CorrigeFact";



        public static string ETATINTERVENTION;
        public static string RAPPORTINTERVENTIONV2;
        public static string sValidCorpsDevisV2;
        public static string sCodePDASociete;

        public static string ETATOLDFACTUREMANUEL { get; set; }
        public static string ETATNEWFACTUREMANUEL;
        public static string PROGUserDocSite;
        public static string ETATANALYTIQUE2;
        public static string ETATANALYTIQUE2FACTURE;
        public static string ETATCourrierClient;
        public static string ETATRevisionRobinet;
        public static string PROGUserDocAnalytique2;
        public static string ETATDtdevisSQLSynthese;
        public static string EtatDtDevisSQLSyntheseCopie;

        public static string PROGUserInterP2;
        public static string PROGUserDocParametreV2;
        public static string PROGUserDocGestionAppel;
        public static string sKeyGeocodage;

        public static string sToleranceEntretien;
        public static string sPartGMAO;
        public static string sActiveP1;
        public static string sServeurSQL { get; set; }
        public static string cODBCP1;
        public static string sNbHeuresTotales;

        public static int lEditionCtr;
        public static string EtatContratOrigine;
        public static string PROGUserDocDdeChiffrage;

        //== Nouvel version sans VBD.===============.
        public static string PROGFICHEPRINCIPAL;
        public static string PROGUserDocStandard;
        public static string PROGUSERPRECOMMANDE;
        public static string PROGUserIntervention;
        public static string PROGUserDispatch;
        public static string PROGUserDispatchDevis;
        public static string PROGUserDocImmeuble;
        public static string PROGUserDocGroupe;

        public static string PROGUserDocClient;
        public static string PROGUserDocFourn;
        public static string PROGUserDocHistoDevis;
        public static string PROGFACCONTRAT;
        public static string PROGUserDocRelTech;
        public static string PROGUserBCLivraison;
        public static string PROGUSERUserDocSageScan;
        public static string PROGUderBCmdBody;
        public static string PROGUserBCmdHead;
        public static string PROGUserAnalytiqueAffaire;
        public static string PROGUserDocAnalyTRavaux;
        public static string PROGUserAnalyseContrat;
        public static string PROGUserDocPcaPar;
        public static string CRPCAPAR;
        public static string PROGUserDocHistoContrat;

        public static string PROGUserDocDescriptifChaufferie;
        public static string PROGUserDocParamP3;
        public static string sRamoneurP2particulier;

        public static string sEtatAvisDePassage { get; set; }

        public static string sAfficheLibParticulier;
        public static string sReleveCoproPoprietaire;
        public static string sGMAOcriterePartImmeuble;


        public static string PROGUserDocAnalyse;
        public static string PROGUserDocCtrlHeurePers;

        public static string sUseCoefGecet;
        public static double dbGenCoefGecet;

        public static string sExportTxtToLogim;
        public static string ETATDtdevisSQLv2;
        public static string ETATFacturemanuelleSQLv2;

        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        public static string ETATFacturemanuelleSQLv3;
        public static string ETATFACTUREMANUELExportV3 { get; set; }
        //===> Fin Modif Mondir

        public static string ETATStatTechnicien;
        public static string sSendPrestV2limite;
        public static string ETATFACTUREMANUELExport;
        public static string ETATFACTUREMANUELExportV2;
        public static DateTime dtdateFactManuV2;

        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        public static DateTime dtdateFactManuV3 { get; set; }
        public static string ETATfacturationContratV2 { get; set; }

        //===> Fin Modif Mondir

        public static DateTime dtdateDevExportV2;

        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        public static string ETATfacturationContratExportV2 { get; set; }
        public static string sAdresseEnvoie { get; set; }
        //===> Fin Modif Mondir

        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
        public static string sCptCLientDouteux { get; set; }
        //===> Fin Modif Mondir

        public static string sMailServiceInfo;
        public static string sSendInterMail;

        public static string sGrilleSalaire;
        public static string sPathExeIntervParImm;
        public static string sCrtlDevis;

        public static string sDateDevisV2;
        public static string sDateFactureManuelleV2 { get; set; }
        public static string sCalculPorteFeuilleCtrV2;
        public static string sServiceAnalytique;
        public static string PROGUserDocHistoInterv;
        public static string PROGUserDocArticle;
        public static string PROGUserDocCharge;
        public static string PROGUserDocContratAnalytique;
        public static string PROGUserFactFourn;
        public static string PROGUserDocFacManuel;
        public static string PROGIMPRIMEFACTURE;
        public static string PROGPARAMETRE2;
        public static string PROGPARAMETRE;
        public static string PROGUserReceptionMarchandise;
        public static string PROGUserListeArticle;
        public static string PROGListeContrat;
        public static string PROGUserPortefeuilleContrat;
        public static string PROGUserHistoReadSoft;
        public static string PROGUserDocCorrespondance;
        public static string sCaterogieArticleInter;
        public static string ProgUserDocPlanningCodeJock;

        public static string sInterlocuteurHistoAppel;
        //==========================================.
        public static string ProgUserDocVisuFact;

        public static DateTime dtServiceAnalytique;

        public static string[] tSelect;
        public static string ETATTACHESP2;
        public static string ETATVISITESP2;
        public static string sMotifGSP2;
        public static string sTypeIntP2;
        public static string sArticleAchat;
        public static string sArticleDevis;
        public static string sHeureFinMatin;
        public static string sHeureDebutPM;
        public static string sUrlLien;
        public static string sDefautFournisseur;
        public static string sBloqueBonImprime;
        public static string sTechObligatoire;
        public static string sAutoriseSuppBDC;
        public static string sNoFactureManuelAuto;
        public static string ScanFourn;
        public static string sCodeReglement;
        public static string sMonoIntervParDevis;
        public static string sDocObligatoirePourDevis;
        public static string sPrecommande;
        public static string sTarifGecetVisible;
        public static string sTarifGecetValeurDefaut;


        //== variables concernant le fax.
        public static string FAXMAKER;
        public static string NAMEFAXMAKER;
        public static string FAXMAKER_KEYDEST;
        public static string FAXMAKER_KEY;

        //=== variable read soft.
        public static string sModuleReadSoft;
        public static string sInterditAchatAZero;
        public static string sActiveEmail;
        public static string sWhereSst;
        public static string sWhereFf;

        public static string sDevModeSynthese;
        public static string sModeRegFacManu;
        public static string sModeRegFacContrat;
        public static string sModeRegFacManuCopro;
        public static string sModeRegClient;
        public static string sGecetV2;
        public static string sVersionImmParComm;
        public static string sTomTomCpt;
        public static string stomtomPsd;
        public static string stomtomUser;
        public static string sTomtom;
        public static string sTomtomAdr;
        public static string stomtomPrefInter;
        public static string sGroupeTomtom;
        public static string sTomtomRayonAdr;


        public const int cGrisClair = 0xc0c0c0;
        public const int cGrisFonce = 0x808080;


        public static string sDateComExploitOblitoire;

        public static string CHEMINETATFACT;
        public static string CHEMINETATPREST;
        public static string CHEMINETATPRESTv2;
        public static string CHEMINETAT;
        public static string CHEMINETATRelTech;
        public static string LIBNET;

        public static string NoTvaIntraCom;
        public static string NomSociete;
        public static string LogoSociete;
        public static string sImgEntete;
        public static string NomSousSociete;
        public static string LogoSousSociete;
        public static string PROGUserListeContrat;

        public static string PROGUserDocFormule;
        public static string PROGDEVIS;
        public static string PROGUserDocTypeRatio;
        public static string CHEMINEUROONLY;
        public static string sFicheContratV2;

        public static string gDatabase;
        public static string CHEMINFAX;
        public static string CHEMINMAIL;
        public static string CHEMINCPTRENDU;
        public const string cFolderCptRendu = "comptes-rendus";
        public const string cFolderSSt = "Sous-Traitants";

        public static string CHEMINCOURRIER;
        public static string CHEMINKOBBY;
        public static string FOLDERFACTFURN;
        public static string NomImmeuble;
        public static string SQL;
        public static bool gbAjout;
        public static bool BlnMajFrmQui;
        public static string strCodeOriCommentDevis;
        public static string strDocument;
        public static short giMsg;
        public static object fso;

        public static short nbDossierMail;

        public static string MesUtilFichStd;
        public static string MesOrigineCom;
        public static string MesEmplacement;
        public static string MesQui;
        public static string MesSociete;
        public static string MesNom;
        public static string MesCodeImmeuble;
        public static string MesCodeProduit;
        public static string MsgCodProd;
        public static string MsgCommentaire;
        public static string[] MesDossiers;
        public static string UsrSage;
        public static string PswdSage;
        public static string DataSourceSage;
        public static string[] strTabParam { get; set; } = new string[4];
        public static string ConnectionExterne;
        public static string AffDevis;
        public static string UtiliseSage;
        public static string ParamVisu;
        public static string UtiliseOutlook;
        public static string sActiveVersionPz;


        public static DataTable rstmp;
        public static ModAdo modAdorstmp;
        private static SqlDataAdapter SDArstmp { get; set; }
        public static string VersionDate { get; internal set; }
        public static string Commit { get; set; }
        public static string Version { get; set; }

        public static string NumFicheStdMsg;
        public static string FormatDateSQL;
        public static string FormatDateSansHeureSQL { get; set; }


        //Les variables suivantes servent à récupérer les codes à sélectionner
        //pour les dépanneurs et les commerciaux
        public static string[] CodeQualifTechnicien;
        public static string[] CodeQualifCommercial;
        public static string[] CodeQualifDispatch;
        public static string[] CodeQualifDeviseur;
        public static string[] CodeQualifGestionnaire;
        public static string[] CodeQualifParticulier;
        public static string[] CodeQualifAdministratif;
        public static string[] CodeQualifRespTrav;
        public static string[] CodeQualifRespExpl;
        public static int nbCodeTechnicien;
        public static int nbCodeCommercial;
        public static int nbCodeDispatch;
        public static int nbCodeDeviseur;
        public static int nbCodeGestionnaire;
        public static int nbCodeParticulier;
        public static int nbCodeAdministratif;
        public static int nbCodeRespTrav;
        public static int nbCodeRespExpl;

        public static string RepExeInterlibDT;
        public static string ProgExeInterlibDT;

        //--- Couleur des labels liée aux évenements de survol de la souris ------
        public static string CouleurRepos;
        public static string CouleurSurvol;
        public static long lngCouleurRepos;
        public static long lngCouleurSurvol;
        //--- Fin couleurs ---------------------------------------------------------

        //----------Variables utilisées pour le placement dynamique du logo de la societé ----
        public static double imgTop;
        public static double imgLeft;
        //---------- Fin ---------------------------------------------------------------------

        //--- Devis ----------------------------------------------
        public static string strDTDebourse;
        public static string strDTDebourseAffaire;
        public static string strDTDevisRecap;
        public static string strDTDevisRecapV2;

        public static string strDTDevis;
        public static string[] strModele;
        public static string strEtat;
        public static string strEtatReportDevis;
        public static string gNumeroDevis;
        public static double MO;
        public static double FO;
        public static double sT;
        public static double PxMO;
        public static double TVADefaut;
        public static bool SelectModele { get; set; }
        public static bool BoolAnnuler;

        public static string sDroit;
        public const string cUtil = "Util";
        public const string cAdmin = "Admin";
        public static double dbMontanthtDevis;

        public struct ParamDevis
        {
            public string sName;
            public string sValue;
        }

        public static ParamDevis[] tabParametresDevis;

        public static string sCheminEtat;
        public static string sNodevis;

        public static string sEtaleJournee;
        public static double dbTolerance;
        public static string sDesActiveSage;
        public static string sCptGeneralClient;
        public static string sAfficheCodeLong;
        public static string sCptGeneralFourn;
        //----------------Fin Devis --------------------------------

        public static short GridTypeSelect;

        public struct tpCodeSelect
        {
            public string sCode;
            public short sSelect;
            public string sChamp;
        }

        public static tpCodeSelect[] tabCodeSelect;

        //--------------Dossiers Intervention, Devis et Appel ------
        public static string DossierIntervention;
        public static string DossierDevis;
        public static string DossierAppel;
        //Préférences pour la feuille "Dossier Intervention"
        //Stockée dans la fenêtre Dossier Intervention
        public static string Drive1EnCours;
        // Idem
        public static string Dir1EnCours;
        // Idem
        public static string File1EnCours;
        //Stockée par la procédure STOCKELIEN
        public static string AfficheRetour;
        //----------Fin Dossier Intervention-------------------------

        public static string CompteEcriture;

        //Variable relative au P2 : élaboration du contrat
        public static string ETATcontrat;
        public static string ETATANALYTIQUE;
        //-- API permattant de placer une fenetre au premier plan.
        private const int SWP_NOMOVE = 0x2;
        private const int SWP_NOSIZE = 0x1;
        private const int SWP_NOACTIVATE = 0x10;
        private const int SWP_SHOWWINDOW = 0x40;
        private const short HWND_TOPMOST = -1;
        private const short HWND_NOTOPMOST = -2;
        private const short Flags = SWP_NOACTIVATE | SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE;

        public static string PROGUserAnalytique;
        public static string PROGUserAdminstration;
        public static string PROGUserDocParametre2;


        public static string PROGUserDocP2;
        public static string PROGUserDispacthContrat;
        public static string PROGUserDocLocalisation;
        public static string PROGUserInter_RelevesCompteurs;
        public static string PROGUserDocSuiviRelevesV2;
        public static string PROGUserDocVehicule;
        public static int lnbHistoInterPDA;
        public static int lnbHistoDevisPDA;
        public static string slienHttpWave;

        public static string CHEMINGRILLE;
        public static string sPriseEnCompteLocalP2;
        public static string sMAJMotifV2;
        public static string sP2version2;
        public static string sEnvoiePdaCorrective;
        public static string sUseProcStockeeGMAO;
        public static string sAnalyAffTypeOp;
        public static string sPDAv2Ramonage;
        public static string sFIltreInterP1;
        public static string strCodeUO;
        public static string sNomBaseHarmonie;
        public static string EtatVisiteP2v2;

        public static string sServeurHarmonie;
        public static string sPDARepEmissionFichier;
        public static string sCheminSignature;

        public const short cVirgule = 44;
        public const short cPoint = 46;

        public static string strFournisseurInterne;

        //Utilisées pour les relevés techniques
        public static bool gbnew;
        public static object gVar;

        //Utilisée pour les relevés de compte
        public static string MoisFinExercice;

        public static string[] SelectBcmdDetail;
        public static string LienSite;

        public static string sModeFermeture;

        public static string EtatImmeublesClient;


        //Ne pas masquer les sommes mais masquer les quantités
        public const string crDescritpif = "1";
        //Ne pas Masquer les sommes ni les quantités
        public const string crQuantitatif = "2";
        //Masquer les quantités
        public const string crPosition = "3";
        //Masquer les sommes et les quantités
        public const string crAffaire = "4";

        public static string cFrMultiSoc { get; set; } = "MULTI-SOCIETE";
        public const string cFrNomServer = "NomServeur";
        public const string cFrNomBase = "NomBase";
        public const string cFrSociete = "Societe";
        public const string cFrBaseS1 = "BaseS1";
        public const string cFrServeurS1 = "ServeurS1";
        public const string cFrBaseS2 = "BaseS2";
        public const string cFrServeurS2 = "ServeurS2";
        public const string cFrBaseS3 = "BaseS3";
        public const string cFrServeurS3 = "ServeurS3";
        public const string cFrBaseS4 = "BaseS4";
        public const string cFrServeurS4 = "ServeurS4";
        public const string cFrBaseS5 = "BaseS5";
        public const string cFrServeurS5 = "ServeurS5";
        public const string cFrBaseS6 = "BaseS6";
        public const string cFrServeurS6 = "ServeurS6";

        //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        public const string cFrBaseS7 = "BaseS7";
        public const string cFrServeurS7 = "ServeurS7";
        //===> FIn Modif Mondir


        public const string nomBaseS1 = "DELOSTAL";
        public const string nomBaseS2 = "PEZANT";
        public const string nomBaseS3 = "AXECIEL_Eccep";
        public const string nomBaseS4 = "AXECIEL_AMMANN";
        public const string nomBaseS5 = "GAZDEPARIS";
        public const string nomBaseS6 = "AXECIEL_LONG";

        const uint CW_USEDEFAULT = 0x80000000;
        public const string S1 = "S1";
        public const string S2 = "S2";
        public const string S3 = "S3";
        public const string S4 = "S4";
        public const string S5 = "S5";
        public const string S6 = "S6";
        //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        public const string S7 = "S7";
        //===> Fin Modif Mondir

        public static string cRAPPORT { get; set; }
        public static string cFACTURENONMISEAJOUR;
        public static string cCheminFichierFactContrat;
        public static string cCheminFichierErrTrx;
        public static string cCheminFichierAnalyCtr;
        public static string cCheminFichierPorteFeuille;
        public static string cCheminFichierCtrErr;
        //Public Const DefaultServer = "DTNT500"
        public static string DefaultServer { get; set; } = "DT-SQL-01";



        public struct ArtGecetPre
        {
            public string sNoAuto;
            public double Qte;
            public int lidBDD2 { get; set; }
        }

        //Public dbMargeSupDevis      As Double

        public static string METAFIC;

        public static string sLien0;
        public static string sLien1;
        public static string sLien2;
        public static string sNomLien0;
        public static string sNomLien1;
        public static string sNomLien2;
        public static string sCodeArticleP2;
        public static string ProgUserDocReglement;


        public static string sRespExploitParImmeuble;
        public static string sChefDeSecteurParImmeuble;
        public static string sExportPdf;
        public static string sExportPdfContrat;

        public static string sComptaAnalySecteur;

        public static ArtGecetPre[] tArtGecetPre;

        public static string sCreatePrestV2;

        public static string sJournalVente;
        //====> Mondir le 30.06.2020, ajout des modifs de la veriosn VB6 de 29.06.2020
        public static string sClimInitiale { get; set; }
        //====> Fin modif Mondir
        //====>Ahmed le 08 09 2020 Ajout des modification Push V 08 09 2020
        public static string sActiveFactMultiAppel { get; set; }
        //===
        //===> Mondir le 15.11.2020 pour integrer les modfis de la version V12.11.2020
        public static string sSignatureMail { get; set; }
        public static string sTVA300 { get; set; }
        public static double dbSeuilTTCtva300 { get; set; }
        public static string sContratMultiTVA;
        public static string PROGUserSyntheseAppel { get; set; }
        public static string sSignatureServiceFact { get; set; }
        public static bool bSignatureServFact { get; set; }
        //===> Fin Modif Mondir

        public static string sExportCrImm;
        public static string sPathReportCRv9;
        public static string sActiveEnvoieSurPDA;
        internal static bool showInfo;

        /// <summary>
        /// Tested
        /// </summary>
        public static void open_conn(bool bCtrlConnection = false)
        {
            string soledb = null;
            string sOdbc = null;
            string sSqlSever = null;
            string sNoMAJ = null;
            try
            {
                if (rstmp == null)
                {
                    rstmp = new DataTable();
                }
                //- chaine de connection
                if (adocnn == null)
                {
                    adocnn = new SqlConnection();
                }

                bDebugIntranet = false;

                DateTime dt = new DateTime(2019, 05, 27);
                if (DateTime.Now == dt && UCase(fncUserName()) == UCase("rachid abbouchi"))
                {
                    bDebugIntranet = true;
                }

                if (adocnn.State != ConnectionState.Open)
                {
                    cFrNomApp = cRegInter;

                    var AssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
                    if (AssemblyName.ToUpper() == "IntranetV3Heure".ToUpper())
                    {

                        cFrNomApp = "dtBASETEST";
                        soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=AXECIEL_DELOSTALTEST;" + "Data Source=dt-sql-01";

                        adocnn = new SqlConnection(soledb);
                        adocnn.Open();
                    }
                    else if (AssemblyName.ToUpper() == cProgIntranetV3PDA.ToUpper())
                    {
                        //cFrNomApp = "dtBASETEST"
                        cFrNomApp = "InterlibPDA";
                        soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=AXECIEL_DELOSTAL_PDA;" + "Data Source=dt-sql-01";
                        //soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" _
                        //& "Persist Security Info=False;" _
                        //& "Initial Catalog=AXECIEL_DELOSTAL_PDA;" _
                        //& "Data Source=."
                        adocnn = new SqlConnection(soledb);
                        fc_StockODBC_BaseTestPDA();
                    }
                    else if (AssemblyName.ToUpper() == cProgIntranetV3report.ToUpper())
                    {
                        cFrNomApp = "InterlibReport";
                        soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=AXECIEL_DELOSTAL_REPORT;" + "Data Source=dt-sql-01";
                        //soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" _
                        //        &"Persist Security Info=False;" _
                        //        &"Initial Catalog=AXECIEL_DELOSTAL_PDA;" _
                        //        &"Data Source=."

                        adocnn = new SqlConnection(soledb);
                        adocnn.Open();
                        fc_StockODBC_BaseTestReport();
                    }
                    else if (bDebugIntranet == true)
                    {
                        cFrNomApp = "GROUPE_DT_DEBUG";

                        soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;"
                            + "Persist Security Info=False;"
                            + "Initial Catalog=AXECIEL_LONG_DEBUG;"
                            + "Data Source=dt-sql-01";
                        adocnn = new SqlConnection(soledb);
                        adocnn.Open();
                    }
                    else
                    {
                        if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "31/01/2015")
                        {
                            soledb = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=AXECIEL_DELOSTALTEST;Data Source=.";
                        }
                        else
                        {
                            soledb = getFrmReg("App", DefaultSource, "");
                        }
                        if (string.IsNullOrEmpty(soledb))
                        {
                            sSqlSever = fc_SQLServer();
                            //- base access
                            if (Convert.ToDouble(sSqlSever) == 0)
                            {
                                sOdbc = gFr_ODBCBddRe();
                                adocnn = new SqlConnection(sOdbc);
                                adocnn.Open();
                                saveInReg("App", DefaultSource, "Provider=Microsoft.Jet.OLEDB.4.0;" + "Persist Security Info=False;" + " Data Source=" +
                                    adocnn.DataSource + ".mdb;Mode=Read|Write");

                                //- sql server
                            }
                            else
                            {
                                //- modifier ici pour connection parametrable.
                                //sOLEDB = "Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;" _
                                //& "Initial Catalog=agcdelostal;" _
                                //& "Data Source=LIB-IIS5"
                                //    SaveSetting cFrNomApp, "App", "Source_OLEDB", sOLEDB
                                adocnn = new SqlConnection(soledb);
                                adocnn.Open();
                            }

                        }
                        else
                        {
                            //=== modif du 30 10 2017, memorise la derniere connection.
                            sLastConnection = getFrmReg(cRegInter + "\\" + "App", cLastConnection, "", false).ToString();
                            //if (General.fncUserName().ToLower().Contains("mondir") && DateTime.Today.ToString("dd/MM/yyyy") == "23/07/2020")
                            //    sLastConnection = @"SERVER=.\SQL_AXECIEL;DATABASE=AXECIEL_DELOSTAL_PDA;INTEGRATED SECURITY=TRUE;";
                            if (string.IsNullOrEmpty(sLastConnection))
                            {
                                adocnn = new SqlConnection(soledb);
                                adocnn.Open();
                                saveInReg(cRegInter + "\\" + "App", cLastConnection, soledb, false);
                            }
                            else
                            {
                                adocnn = new SqlConnection(sLastConnection);
                                adocnn.Open();
                            }
                        }
                    }
                }

                if (bCtrlConnection == true)
                {
                    return;
                }

                //===> Mondir le 27.07.2020 : Commented by Mondir - No need for this
                //var AssemblyName2 = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

                //if (adocnn.Database.ToUpper() == cNameGDP.ToUpper())
                //{
                //    cFrNomApp = cRegInterGdp;
                //}
                //else if (AssemblyName2.ToUpper() == "IntranetV3Heure".ToUpper())
                //{
                //    cFrNomApp = "dtBASETEST";
                //}
                //else if (adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_PDA".ToUpper())
                //{
                //    //===========> Code Supprimé par Mondir
                //    //cFrNomApp = "InterlibPDA";
                //}
                //else if (adocnn.Database.ToUpper() == "AXECIEL_LONG_PDA".ToUpper())
                //{
                //    //===========> Code Supprimé par Mondir le 27.07.2020
                //    //cFrNomApp = "InterlibPDAlong";
                //}
                //else if (adocnn.Database.ToUpper() == cNameLong.ToUpper())
                //{
                //    cFrNomApp = cRegInterLong;
                //}
                //else if (adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_REPORT".ToUpper())
                //{
                //    cFrNomApp = "InterlibReport";
                //}
                //else if (bCtrlConnection == true)
                //{
                //    cFrNomApp = "GROUPE_DT_DEBUG";
                //}
                //else
                //{
                //    cFrNomApp = cRegInter;
                //}
                //===> Fin Modif Mondir

                cFrNomApp = cRegInter;


                //-controle si InitRegitre est à 1 on initialise les cles de registre.
                if (_ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                    SQL = "SELECT InitRegitreV2.InitRegistre,InitRegitreV2.NoMAJ FROM InitRegitreV2";
                else
                    SQL = "SELECT InitRegitre.InitRegistre,InitRegitre.NoMAJ FROM InitRegitre";

                rstmp = new DataTable();
                SDArstmp = new SqlDataAdapter(SQL, adocnn);
                SDArstmp.Fill(rstmp);

                sNoMAJ = getFrmReg("App", "NoMaj", "0").ToString();

                if (rstmp.Rows.Count > 0)
                {
                    lngInitRegitre = 0;
                    if (getFrmReg("App", "Dev").ToString() != "1")
                    {
                        if (Convert.ToInt32(sNoMAJ) >= Convert.ToInt32(nz(rstmp.Rows[0]["NoMaj"], "0")))
                        {
                            lngInitRegitre = 0;
                        }
                        else
                        {
                            lngInitRegitre = Convert.ToInt32(rstmp.Rows[0]["InitRegistre"]);
                            saveInReg("App", "NoMAJ", Convert.ToString(rstmp.Rows[0]["NoMaj"] + ""));

                            Theme.CopyTheme();
                        }
                    }
                    else if (getFrmReg("App", "ModeDev", "").ToString() == "1")
                    {
                        lngInitRegitre = 0;
                    }
                    else
                    {
                        lngInitRegitre = 1;
                    }
                }
                else
                {
                    lngInitRegitre = 1;
                }

                rstmp.Dispose();
                rstmp = null;

                StockeLien();
                ///fc_UnloadAllForms
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Module1;open_conn");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        public static void StockeLien()
        {
            string sSQLado = null;

            //Consulte les clés "Affiche Devis" et "Utilise Sage" afin de pouvoir ou non
            //afficher les éléments du devis dans le UserDocument UserDocStandard et
            //récupère la clé si utilisation de sage
            //fncAfficheParam

            //        RepExeInterlibDT = gfr_liaison("RepExeInterlibDT")
            //        ProgExeInterlibDT = gfr_liaison("ProgExeInterlibDT")

            //=== initialise les variables devant contenir les chemins des fichiers, base etc...

            if (lngInitRegitre == 1)
            {
                if (rstmp == null)
                {
                    rstmp = new DataTable();
                }

                if (_ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                    SDArstmp = new SqlDataAdapter("SELECT code,Adresse FROM lienV2", adocnn);
                else
                    SDArstmp = new SqlDataAdapter("SELECT code,Adresse FROM lien", adocnn);
                SDArstmp.Fill(rstmp);


                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow Row in rstmp.Rows)
                    {
                        saveInReg("App", Row["Code"] + "", Row["Adresse"] + "");
                    }
                }

                rstmp.Dispose();
                rstmp = null;
            }

            switch (ModuleAPI.getVersion())
            {
                case ModuleAPI.cWinXP:
                    ModuleAPI.sWinRep = "C:\\Windows\\";
                    ModuleAPI.sSysRep = "C:\\Windows\\System32\\";
                    break;
                case ModuleAPI.cWin2000:
                    ModuleAPI.sWinRep = "C:\\WINNT\\";
                    ModuleAPI.sSysRep = "C:\\WINNT\\System32\\";
                    break;
                case ModuleAPI.cWinNT4:
                    ModuleAPI.sWinRep = "C:\\WINNT\\";
                    ModuleAPI.sSysRep = "C:\\WINNT\\System32\\";
                    break;
                case ModuleAPI.cWinNT351:
                    ModuleAPI.sWinRep = "C:\\WINNT\\";
                    ModuleAPI.sSysRep = "C:\\WINNT\\System32\\";
                    break;
                case ModuleAPI.cWin98:
                    ModuleAPI.sWinRep = "C:\\Windows\\";
                    ModuleAPI.sSysRep = "C:\\Windows\\System32\\";
                    break;
                case ModuleAPI.cWin95:
                    ModuleAPI.sWinRep = "C:\\Windows\\";
                    ModuleAPI.sSysRep = "C:\\Windows\\System32\\";
                    break;
                default:
                    ModuleAPI.sWinRep = "C:\\Windows\\";
                    ModuleAPI.sSysRep = "C:\\Windows\\System32\\";
                    break;
            }

            gsCheminPackage = gfr_liaison("package");
            //Debug.Print gsCheminPackage

            ///   gsCheminPackage = "\\Dtnt500\intranetECCEP\PACKAGES\UTIL\PackageInterlib\"
            if (getFrmReg("App", "Main cur", "0").ToString() == "0")
            {
                if (Dossier.fc_ControleDossier(ModuleAPI.sWinRep, false) == false)
                {
                    Dossier.fc_CreateDossier(ModuleAPI.sWinRep);
                }
                if (Dossier.fc_ControleDossier(ModuleAPI.sSysRep, false) == false)
                {
                    Dossier.fc_CreateDossier(ModuleAPI.sSysRep);
                }

                //=== mise en commentaire le 19 10 2017.
                //If fc_ControleFichier(sSysRep & "Main.cur", False) = False Then
                //    gfr_CopierFichiers gsCheminPackage & "Support\", sSysRep, "Main.cur"
                //End If
                //=== fin  mise en commentaire le 19 10 2017.

                saveInReg("App", "Main cur", "1");
            }

            AfficheRetour = gfr_liaison("AfficheRetour");
            AffDevis = gfr_liaison("Affiche Devis");
            UtiliseSage = gfr_liaison("Utilise Sage");
            UtiliseOutlook = gfr_liaison("Utilise Outlook");
            PROGUserListeContrat = gfr_liaison("UserListeContrat");
            PROGUserDocClient = gfr_liaison("UserDocClient");
            PROGUserIntervention = gfr_liaison("UserIntervention");
            PROGUserDocStandard = gfr_liaison("UserDocStandard");
            PROGUserDocImmeuble = gfr_liaison("UserDocImmeuble");
            PROGPARAMETRE = gfr_liaison("UserDocParamtre");
            PROGFICHEPRINCIPAL = gfr_liaison("UserDocFichePrincipal");
            PROGUserDocFacManuel = gfr_liaison("UserDocFacManuel");
            PROGUserDocFourn = gfr_liaison("UserDocFourn");
            PROGUserBCmdHead = gfr_liaison("UserBCmdHead");
            PROGUderBCmdBody = gfr_liaison("UderBCmdBody");
            PROGUserBCLivraison = gfr_liaison("UserBCLivraison");
            PROGUSERUserDocSageScan = gfr_liaison("UserDocSageScan");
            PROGUserListeArticle = gfr_liaison("UserListeArticle");
            PROGUserDispatch = gfr_liaison("UserDocDispatch");
            PROGUserDocReleve = gfr_liaison("UserDocReleve");
            PROGUserDispatchDevis = gfr_liaison("UserDocDispatchDevis");
            PROGUserDocSite = "UserDocNavigate.vbd";
            PROGUserDocHistoInterv = gfr_liaison("UserDocHistoInterv");
            PROGUserDocHistoDevis = gfr_liaison("UserDocHistoDevis");
            PROGUserAnalytiqueAffaire = gfr_liaison("UserAnalytiqueAffaire");
            PROGUserDocAnalytique2 = gfr_liaison("UserDocAnalytique2");

            PROGUserFactFourn = gfr_liaison("UserFactFourn", "UserDocFactFourn.vbd");
            PROGUserReceptionMarchandise = gfr_liaison("UserReceptionMarchandise", "UserReceptionMarchandise.vbd");
            PROGUserDocRelTech = gfr_liaison("UserDocRelTech");
            ProgUserDocVisuFact = gfr_liaison("UserDocVisuFact", "UserDocPrintFacture.vbd");
            PROGUSERPRECOMMANDE = gfr_liaison("UserPrecommande");
            PROGUserPortefeuilleContrat = gfr_liaison("UserPortefeuilleContrat");
            PROGUserDocContratAnalytique = gfr_liaison("UserDocContratAnalytique");
            PROGUserDocAnalyTRavaux = gfr_liaison("UserDocAnalyTRavaux");
            PROGUserDocCharge = gfr_liaison("UserDocCharge");
            PROGUserHistoReadSoft = gfr_liaison("UserHistoReadSoft");



            NomSociete = gfr_liaison("Societe");
            NomSousSociete = gfr_liaison("Sous Societe");
            UsrSage = gfr_liaison("UserSage");
            PswdSage = gfr_liaison("PasswordSage");
            DataSourceSage = gfr_liaison("DataSourceSage");
            if (string.IsNullOrEmpty(DataSourceSage) || string.IsNullOrWhiteSpace(DataSourceSage))
            {
                UtiliseSage = "0";
            }
            else
            {
                SAGE.cODBC = DataSourceSage;
            }

            imgTop = Convert.ToDouble(nz(gfr_liaison("imgTop", "0"), 0));
            imgLeft = Convert.ToDouble(nz(gfr_liaison("imgLeft", "360"), 0));
            //==modif rachid
            imgTop = 50;
            imgLeft = 465;

            sHeureFinMatin = gfr_liaison("HeureFinMatin", "12:00:00");
            sHeureDebutPM = gfr_liaison("HeureDebutPM", "13:30:00");
            sNbrMinuteFact = gfr_liaison("NbrMinuteFact", "15");

            sArticleAchat = gfr_liaison("ArticleVenteAchats");
            sArticleDevis = gfr_liaison("ArticleDevis", "TM");
            sCoefAchatsMin = gfr_liaison("CoefAchatsMin", "1.30");

            ModParametre.NomServeurGecet = gfr_liaison("ServeurGecet");
            ModParametre.NomServeurRD = gfr_liaison("ServeurReadSoft");

            ModParametre.NomBaseGecet = gfr_liaison("BaseGecet");
            NoTvaIntraCom = gfr_liaison("NoTVAIntraCom");

            sPhyPath = gfr_liaison("PhyPath", "E:\\DELOSTALetTHIBAULT\\");
            sVirPath = gfr_liaison("VirPath", "..\\");

            sGecetV3 = gfr_liaison("GecetV3");
            //' sGecetV3 = ""
            if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "11/06/2019" || sGecetV3 == "1")
            {
                ModParametre.NomBaseGecet = "AXECIEL_GECET";
            }

            if (!string.IsNullOrEmpty(ModParametre.NomServeurGecet) && !string.IsNullOrEmpty(ModParametre.NomBaseGecet))
            {
                //ModParametre.cODBCGecet = "Provider=SQLOLEDB.1;Integrated Security=SSPI;";
                //ModParametre.cODBCGecet = ModParametre.cODBCGecet + "Persist Security Info=False;";
                //ModParametre.cODBCGecet = ModParametre.cODBCGecet + "User ID=" + fncUserName() + ";";
                //ModParametre.cODBCGecet = ModParametre.cODBCGecet + "Initial Catalog=" + ModParametre.NomBaseGecet + ";";
                //ModParametre.cODBCGecet = ModParametre.cODBCGecet + "Data Source=" + ModParametre.NomServeurGecet;

                ModParametre.cODBCGecet = $"SERVER={ModParametre.NomServeurGecet};DATABASE={ModParametre.NomBaseGecet};INTEGRATED SECURITY=TRUE;";

                //if (!InTestMode)
                //    ModParametre.cODBCGecet = $"SERVER={ModParametre.NomServeurGecet};DATABASE={ModParametre.NomBaseGecet};INTEGRATED SECURITY=TRUE;";
                //else
                //    ModParametre.cODBCGecet = $"SERVER=DT-SQL-01;DATABASE={ModParametre.NomBaseGecet};INTEGRATED SECURITY=TRUE;";
            }
            else
            {
                ModParametre.cODBCGecet = adocnn.ConnectionString;
            }

            LIBNET = gfr_liaison("LIBNET");
            //Debug.Print LIBNET

            MoisFinExercice = gfr_liaison("MoisFinExercice");
            //Ahmed: Modification du Format MM au lieu de mm pour cSharp
            // FormatDateSQL = (gfr_liaison("FormatDateSQL");
            FormatDateSQL = "dd/MM/yyyy HH:mm:ss";
            //if(General.fncUserName().ToLower().Contains("mondir"))
            //    FormatDateSQL = "yyyy/MM/dd HH: mm: ss";
            //FormatDateSansHeureSQL = gfr_liaison("FormatDateSansHeureSQL");
            FormatDateSansHeureSQL = "dd/MM/yyyy";
            //if (General.fncUserName().ToLower().Contains("mondir"))
            //    FormatDateSansHeureSQL = "yyyy/MM/dd";
            //        ETATBONCOMMANDE = gfr_liaison("CRBonDeCommande")
            ETATBONCOMMANDE = gfr_liaison("ReportBcmd");
            ETATINTERVENTION = gfr_liaison("CRIntervention");

            PROGIMPRIMEFACTURE = gfr_liaison("UserDocImprimeFacture");
            //        PROGUserDocFormule = gfr_liaison("UserDocFormule")
            //        PROGUserDocTypeRatio = gfr_liaison("UserDocTypeRatio")
            CHEMINEUROONLY = gfr_liaison("Report") + gfr_liaison("CRFacturationEuroOnly");
            PROGFACCONTRAT = gfr_liaison("UserDocFicheContrat");
            strEtatReportDevis = gfr_liaison("CrReportDevis");
            PROGUserDocArticle = gfr_liaison("UserDocArticle");
            CHEMINBASE = adocnn.ConnectionString;
            //        PROGUserFacIndice = gfr_liaison("UserDocFacIndice")
            PROGListeContrat = gfr_liaison("UserListeContrat");
            ETATcontrat = gfr_liaison("CRContratP2");
            ETATANALYTIQUE = gsRpt + gfr_liaison("CRanalytique");
            PROGUserAnalytique = gfr_liaison("UserAnalytique");
            //        PROGUserAdminstration = gfr_liaison("UserAdministration")
            PROGPARAMETRE2 = gfr_liaison("UserDocParametre2");
            PROGUserDocP2 = gfr_liaison("UserP2");
            PROGUserDocCorrespondance = gfr_liaison("UserDocCorrespondance");
            dbMontanthtDevis = Convert.ToDouble(nz(gfr_liaison("MontantDevis"), 0));
            CHEMINGRILLE = gfr_liaison("GRILLE");

            CHEMINETATFACT = gfr_liaison("CRFactIntervention");
            CHEMINETATPREST = gfr_liaison("CRPrestationParDepann");
            CHEMINETAT = gfr_liaison("CRDispatchIntervention");
            CHEMINETATRelTech = gfr_liaison("CRCR");
            ETATTACHESP2 = gsRpt + gfr_liaison("CRListeInterP2");
            ETATVISITESP2 = gsRpt + gfr_liaison("CRListeVisiteP2");

            PROGDEVIS = gfr_liaison("UserDocDevis");
            //        If ConnectionExterne = "1" Then
            //            gsCheminPackage = gfr_liaison("PackageExterne")
            //        Else
            gsCheminPackage = gfr_liaison("package");
            //Debug.Print gsCheminPackage
            //        End If
            gsRpt = gfr_liaison("Report");
            //Debug.Print gsRpt
            // gDatabase = gfr_liaison("Base")
            sCheminDossier = gfr_liaison("dossier");
            //Debug.Print sCheminDossier

            DossierIntervention = gfr_liaison("DossierIntervention");
            //Debug.Print DossierIntervention

            DossierDevis = gfr_liaison("DossierDevis");
            //Debug.Print DossierDevis

            DossierAppel = gfr_liaison("DossierAppel");
            //Debug.Print DossierAppel

            strEtat = gfr_liaison("CREtat1");
            ETATOLDFACTUREMANUEL = gsRpt + gfr_liaison("CRFactureManuelle");
            ETATNEWFACTUREMANUEL = gsRpt + gfr_liaison("CRNewFactureManuelle");
            CHEMINFAX = gfr_liaison("fax");
            //Debug.Print CHEMINFAX

            CHEMINCOURRIER = gfr_liaison("SCANCOURRIER");
            //Debug.Print CHEMINCOURRIER

            //CHEMINKOBBY = gfr_liaison("CHEMINKOBBY")
            FOLDERFACTFURN = gfr_liaison("FOLDERFACTFURN");
            // Debug.Print FOLDERFACTFURN

            strDTDebourseAffaire = gfr_liaison("CRDTDebourseAffaire");

            strDTDebourse = gfr_liaison("CRDTDebourse");
            strDTDevisRecap = gfr_liaison("CRDTDevisRecap");
            strDTDevis = gfr_liaison("CRDTDevis");
            ETATANALYTIQUE2 = gsRpt + gfr_liaison("CRAnalytiqueAffTrx");
            ETATANALYTIQUE2FACTURE = gsRpt + gfr_liaison("CRAnalytiqueAffTrxFacture");
            //Public strModele()              As String
            ETATCourrierClient = gsRpt + gfr_liaison("CRCourrierClient");
            ETATRevisionRobinet = gsRpt + gfr_liaison("CRRevisionRobinet");

            CouleurRepos = gfr_liaison("CouleurRepos");
            CouleurSurvol = gfr_liaison("CouleurSurvol");

            LogoSociete = gfr_liaison("LogoSociete");
            //Debug.Print LogoSociete

            if (Dossier.fc_ControleFichier(LogoSociete) == false)
            {
                LogoSociete = "";
            }

            sImgEntete = gfr_liaison("ImgEntete", "");
            //Debug.Print sImgEntete

            if (Dossier.fc_ControleFichier(sImgEntete) == false)
            {
                sImgEntete = "";
            }

            LogoSousSociete = gfr_liaison("LogoSousSociete");
            if (Dossier.fc_ControleFichier(LogoSousSociete) == false)
            {
                LogoSousSociete = "";
            }

            gFr_ODBCBddRe();
            nbACode("DossierMail");

            //        nbCodeTech
            //        nbCodeDisp
            //        nbCodeCom
            //        nbCodeDev
            //        nbCodeRespTravaux
            //        nbCodeRespExploitation
            StockSpecifQualif();

            //****** Etats Relevés de compte ******************
            //****** Les relevés de compte se situent dans la fiche immeuble
            strSyntheseReleveDeComerc = gsRpt + gfr_liaison("CRSyntheseReleveDeComerc");
            strSyntheseReleveDeviseur = gsRpt + gfr_liaison("CRSyntheseReleveDeviseur");
            strReleveDeSyndic = gsRpt + gfr_liaison("CRReleveDeSyndic");
            strReleveImmeuble = gsRpt + gfr_liaison("CRReleveImmeuble");
            strReleveDeviseur = gsRpt + gfr_liaison("CRReleveDeviseur");
            //****** Fin Etats Relevé de compte ****************


            lngCouleurSurvol = ConvertHexDec(CouleurSurvol);
            lngCouleurRepos = ConvertHexDec(CouleurRepos);


            PROGPARAMETRE2 = gfr_liaison("UserDocParametre2");
            PROGUserInterP2 = gfr_liaison("UserInterP2");
            sMotifGSP2 = gfr_liaison("MotifGSP2");
            sTypeIntP2 = gfr_liaison("TypeIntP2");

            EtatImmeublesClient = gfr_liaison("CRImmeubleClient", "ImmeublesClient.rpt");

            FAXMAKER = gfr_liaison("FAXMAKER");
            NAMEFAXMAKER = gfr_liaison("NAMEFAXMAKER").ToUpper();
            FAXMAKER_KEYDEST = gfr_liaison("FAXMAKER_KEYDEST").ToUpper();
            FAXMAKER_KEY = gfr_liaison("FAXMAKER_KEY").ToUpper();

            //== lien vers sites web à partir du document activeX
            sUrlLien = gfr_liaison("UrlLien");

            //== Flag permettant d'identifier tel fonction est en cours de
            //== de developpement.( 1 = oui, autre = non)
            sPrecommande = gfr_liaison("PreCommande");

            //== fournisseur par défaut.
            sDefautFournisseur = gfr_liaison("sDefautFournisseur");

            //== flag definissant si l'activation du blocage des des bons de commande apres impression
            //== est opérationel.
            sBloqueBonImprime = gfr_liaison("BloqueBonImprime");

            sTechObligatoire = gfr_liaison("TechObligatoire");

            sAutoriseSuppBDC = gfr_liaison("AutoriseSuppBDC");

            //== Autorise la facturation travaux.
            sFACTURATIONTRAVAUX = gfr_liaison("FacturationTravaux");

            //== met en automatique l'analytique dans la facturation traavux
            sAnalyTravauxAuto = gfr_liaison("AnalyTravauxAuto");

            sSepSMS = nz(gfr_liaison("sSepSMS"), "/").ToString();
            sNoFactureManuelAuto = gfr_liaison("NoFactureManuelAuto");

            //== met à jour en auto les tarifs GECET.
            sTarifGecetVisible = gfr_liaison("TarifGecetVisible");

            sTarifGecetValeurDefaut = gfr_liaison("TarifGecetValeurDefaut");

            //== chemin des factures fournisseurs scannées.
            ScanFourn = gfr_liaison("ScanFourn");
            //Debug.Print ScanFourn

            //== valeur par defaut du code de reglement
            sCodeReglement = gfr_liaison("CodeReglement");

            //== valeur n'autorisant la création d'une seule intervetion
            //== par devis.
            sMonoIntervParDevis = nz(gfr_liaison("MonoIntervParDevis"), "0").ToString();

            //==si sDocObligatoirePourDevis est à 1, un document de type fax ou courrier
            //== est obligatoire lors de l'acceptation de devis.
            sDocObligatoirePourDevis = nz(gfr_liaison("DocObligatoirePourDevis"), "0").ToString();

            //=== controle si module readsoft installé.
            sModuleReadSoft = nz(gfr_liaison("ReadSoft"), "").ToString();

            //=== si sInterditAchatAZero à 1 , on interdit les bons de commande non estimés.
            sInterditAchatAZero = nz(gfr_liaison("InterditAchatAZero"), "").ToString();

            //=== active l'envoie des interventiosn par email
            sActiveEmail = nz(gfr_liaison("ActiveEmail"), "").ToString();

            //=== clause pour sélectionner les factures de sous-traitance.
            sWhereSst = nz(gfr_liaison("WhereSst"), "").ToString();


            //=== clause pour ne pas sélectionner les factures de sous-traitance.
            sWhereFf = nz(gfr_liaison("WhereFf"), "").ToString();


            //=== Flag permettant m'affichage du devis en mode synthése (plus validation)
            sDevModeSynthese = nz(gfr_liaison("DevModeSynthese"), "").ToString();

            //=== flag permattant d'appliquer le mode de réglement de l'immeuble
            //=== pour les factures manuelles (demande de pezant)
            sModeRegFacManu = nz(gfr_liaison("ModeRegFacManu"), "").ToString();

            sModeRegFacContrat = nz(gfr_liaison("ModeRegFacContrat"), "").ToString();

            //=== flag permattant d'appliquer le mode de réglement de l'immeuble
            //=== pour les factures manuelles pour les copro (demande de pezant).
            sModeRegFacManuCopro = nz(gfr_liaison("ModeRegFacManuCopro"), "").ToString();

            //=== si sModeRegFacManu (voir ci dessus) est à 1 et sModeRegFacManuCopro est à 1
            //=== le mode de reglement provient de la fiche client et non
            //=== de la fiche immeuble si sModeRegClient est à 1 (demande de pezant).
            sModeRegClient = nz(gfr_liaison("ModeRegClient"), "").ToString();

            //=== etat de synthese de devis uniquement pour pezant.
            ETATDtdevisSQLSynthese = nz(gfr_liaison("DtdevisSQLSynthese"), "").ToString();
            EtatDtDevisSQLSyntheseCopie = nz(gfr_liaison("DtdevisSqlSyntheseCopieA"), "").ToString();


            //=== flag permettant d'activer la version dédié à pezant.
            if (DateTime.Now.ToString("dd/MM/yyyy") == "14/03/2011")
            {
                sActiveVersionPz = "1";
            }
            else
            {
                //sActiveVersionPz = nz(gfr_liaison("ActiveVersionPz"), "")
                sActiveVersionPz = "";
                //=== variable sans utilité.
            }

            //=== Active la connection avec la nouvelle base de donnée Gecet.
            sGecetV2 = nz(gfr_liaison("GecetV2"), "").ToString();
            //sGecetV2 = ""

            //=== affiche les modifications par immeuble.
            sVersionImmParComm = nz(gfr_liaison("VersionImmParComm"), "").ToString();
            if (DateTime.Now.ToString("dd/MM/yyyy") == "28/06/2011")
            {
                sVersionImmParComm = "0";
            }

            //=== la variable sTomtom est à 1 si la société en cours
            //=== utilise la geolocalisation tom tom.
            sTomtom = nz(gfr_liaison("tomtom"), "").ToString();
            if (DateTime.Now.ToString("dd/MM/yyyy") == "10/10/2011")
            {
                sTomtom = "1";
            }

            //=== Infos concernant les tomtoms.
            sTomTomCpt = nz(gfr_liaison("TomTomCpt"), "").ToString();
            stomtomPsd = nz(gfr_liaison("tomtomPsd"), "").ToString();
            stomtomUser = nz(gfr_liaison("tomtomUser"), "").ToString();
            sTomtomAdr = nz(gfr_liaison("tomtomAdr"), "").ToString();
            stomtomPrefInter = nz(gfr_liaison("tomtomPrefInter"), "").ToString();
            sGroupeTomtom = nz(gfr_liaison("GroupeTomtom"), "").ToString();
            sTomtomRayonAdr = nz(gfr_liaison("TomtomRayonAdr"), "").ToString();

            //=== crée un dossier de rapport (erreur) pour chaque utilisateur.
            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> ajout du condition sur le nom d'utilisateur
            //if (fncUserName().ToUpper() != "rachid abbouchi" && DateTime.Today.ToString("dd/MM/yyyy") == "12/07/2020")
            //{
            fc_CreateDossierErr();
            //}

            //=== chemin des fichiers métafic.
            METAFIC = nz(gfr_liaison("METAFIC"), "").ToString();
            saveInReg(ModP1Gaz.cFrNomAppAXE_P1 + "\\App", "CheminMetafic", METAFIC, false);

            //=== Url(s).
            sNomLien0 = gfr_liaison("NomLien0");
            sNomLien1 = gfr_liaison("NomLien1");
            sNomLien2 = gfr_liaison("NomLien2");
            sLien0 = gfr_liaison("Lien0");
            sLien1 = gfr_liaison("Lien1");
            sLien2 = gfr_liaison("Lien2");

            //=== Lien vers fiche ProgUserDocReglement.
            ProgUserDocReglement = gfr_liaison("UserDocReglement");

            //=== ajout du code article pour les visites d'entretien.
            sCodeArticleP2 = gfr_liaison("CodeArticleP2");
            if (DateTime.Now.ToString("dd/MM/yyyy") == "27/09/2011" && fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
            {
                sCodeArticleP2 = "1";
            }

            //==== variable temporaire à affacer.
            sRespExploitParImmeuble = gfr_liaison("sRespExploitParImmeuble");

            sChefDeSecteurParImmeuble = gfr_liaison("sChefDeSecteurParImmeuble");

            //=== Variable permmettant l'export des etats en pdf.
            if (DateTime.Now.ToString("dd/MM/yyyy") == "13/02/2012" && fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
            {
                sExportPdf = "1";
            }
            else
            {
                sExportPdf = gfr_liaison("ExportPdf");
            }


            sExportPdfContrat = gfr_liaison("ExportPdfContrat");

            //=== modif du 27 fevrier, creation analytique secteur en compta (delostal).
            if (fncUserName().ToUpper() == "RACHID ABBOUCHI".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "18/02/2013")
            {
                sComptaAnalySecteur = "1";
            }
            else
            {
                sComptaAnalySecteur = gfr_liaison("ComptaAnalySecteur");
            }

            //=== RDO MAIL.
            if (DateTime.Now.ToString("dd/MM/yyyy") == "16/08/2012" && fncUserName().ToUpper() == "rachid abbouchi".ToUpper() || fncUserName().ToUpper() == "MONDIR")
            {
                sRDOmail = "1";
            }
            else
            {
                sRDOmail = gfr_liaison("RDOmail");
                //sRDOmail = 0
            }

            //=== variable affecté à l'analytique des bon de commande Ammann (plomberie, couverture)
            sAnalytiqueBCD = gfr_liaison("AnalytiqueBCD");
            //=== date de la mise en place de l'analytque bon de commande (ammann).
            sDateAnalytiqueBDC = gfr_liaison("DateAnalytiqueBDC");

            //=== nouvelle version de devis (grille liée directement à la base de donnée).
            if (DateTime.Now.ToString("dd/MM/yyyy") == "12/06/2013" && fncUserName().ToUpper() == "Rachid Abbouchi".ToUpper())
            {
                sVersionDevisV2 = "1";
            }
            else
            {
                sVersionDevisV2 = nz(gfr_liaison("VersionDevisV2"), "").ToString();

            }

            //=== date a partir de laquelle le code commercial est obligatoire ainsi que le
            //=== responsable d'exploitation.
            sDateComExploitOblitoire = gfr_liaison("DateComExploitOblitoire");

            //=== analyse contrat.
            PROGUserAnalyseContrat = gfr_liaison("UserAnalyseContrat");

            //=== Historique contrat.
            PROGUserDocHistoContrat = gfr_liaison("UserDocHistoContrat");

            //=== fiche PCA PAR.
            PROGUserDocPcaPar = gfr_liaison("UserDocPcaPar");


            //=== etat PCA PAR.
            CRPCAPAR = gfr_liaison("Report") + gfr_liaison("CRPCAPAR");

            //=== export de données en format txt vers logiamatique.
            sExportTxtToLogim = gfr_liaison("ExportTxtToLogim");
            if (DateTime.Now.ToString("dd/MM/yyyy") == "28/01/2014" && fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
            {
                sExportTxtToLogim = "1";

            }


            //=== applique un coefficient sur les artciles gecet(Ammann).
            sUseCoefGecet = gfr_liaison("UseCoefGecet");
            if (sUseCoefGecet == "1")
            {
                if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                    sSQLado = "SELECT     adresse From LienV2 WHERE Code = 'CoefGECET'";
                else
                    sSQLado = "SELECT     adresse From Lien WHERE Code = 'CoefGECET'";
                using (var tmpModAdo = new ModAdo())
                    sSQLado = tmpModAdo.fc_ADOlibelle(sSQLado);
                dbGenCoefGecet = Convert.ToDouble(nz(sSQLado, 1));
            }
            else
            {
                dbGenCoefGecet = 1;
            }

            //=== Etat devis V2, trois taux de TVA 11 Mars 2014.
            ETATDtdevisSQLv2 = gfr_liaison("NewDtdevisSQLv2");
            sDateDevisV2 = gfr_liaison("DateDevisV2");
            strDTDevisRecapV2 = gfr_liaison("DtdevisRecapSQLv2");

            //=== Etat facture manuelle V2, trois taux de TVA 11 Mars 2014.
            ETATFacturemanuelleSQLv2 = gsRpt + gfr_liaison("FacturemanuelleSQLv2");
            sDateFactureManuelleV2 = gfr_liaison("FactureManuelleV2");

            //=== Ajout de la section ananlytique service.
            sServiceAnalytique = gfr_liaison("ServiceAnalytique");
            if (sServiceAnalytique == "1")
            {

                dtServiceAnalytique = Convert.ToDateTime(gfr_liaison("DateServiceAnalytique"));
            }
            if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "30/09/2014")
            {
                sServiceAnalytique = "1";
                dtServiceAnalytique = Convert.ToDateTime("30/09/2014");
            }

            //=== nouveau mode de calcul du portefeuille contrat.
            sCalculPorteFeuilleCtrV2 = gfr_liaison("CalculPorteFeuilleCtrV2");

            //=== active la création des prestations v2.
            sCreatePrestV2 = gfr_liaison("CreatePrestV2");

            //=== état  des prestations v2.
            CHEMINETATPRESTv2 = gfr_liaison("CRPrestationParDepannV2");

            //=== active la catégorie des articles pour la fiche interventions.
            sCaterogieArticleInter = gfr_liaison("CaterogieArticleInter");

            PROGUserDocAnalyse = gfr_liaison("UserDocAnalyse");

            ETATStatTechnicien = gfr_liaison("StatTechnicien");

            //=== Variable utilisé temporairement, permettant d'imprimer
            //=== l'état V2 des prestations contractuelle à quelques intervenant-s.
            sSendPrestV2limite = gfr_liaison("SendPrestV2limite");

            //=== adresse mail utilisée pour l'envoie des interventions correctives pour le moment.
            sMailServiceInfo = gfr_liaison("MailServiceInfo");

            //=== Autorier l'envoie d'intervention corrective par mail.
            sSendInterMail = gfr_liaison("SendInterMail");

            ETATFACTUREMANUELExport = gsRpt + gfr_liaison("CRFactureManuelleExport");

            //=== lien vers le formulaire de controle des heures.
            PROGUserDocCtrlHeurePers = gfr_liaison("UserDocCtrlHeurePers");

            PROGUserDocGroupe = gfr_liaison("UserDocGroupe");

            //== charge les taux horaires.
            //------Modif 20/05/2019
            //ModCalcul.fc_LoadChargePers("");

            //sGrilleSalaire = gfr_liaison("GrilleSalaire")
            //If UCase(fncUserName) = UCase("rachid abbouchi") And Date = #7/21/2015# Then
            sGrilleSalaire = "1";
            //End If

            sPathExeIntervParImm = gfr_liaison("ListingIntervenant");

            sCrtlDevis = gfr_liaison("CrtlDevis");


            sFicheContratV2 = gfr_liaison("FicheContratV2");


            //If UCase(fncUserName) = UCase("rachid abbouchi") And Date = #6/20/2016# Or UCase(adocnn.DefaultDatabase) = UCase("AXECIEL_DELOSTAL_PDA") Then
            if (sFicheContratV2 == "1")
            {
                sFicheContratV2 = "1";
                sPriseEnCompteLocalP2 = "1";
                sMAJMotifV2 = "1";
                sP2version2 = "1";
                sEnvoiePdaCorrective = "1";
                sPDAv2Ramonage = "1";
                sUseProcStockeeGMAO = Convert.ToString(0);
                PROGUserDispacthContrat = gfr_liaison("UserDispacthContrat");
                PROGUserInterP2 = gfr_liaison("UserInterP2");
                //sAnalyAffTypeOp = "1"
                PROGUserDocParametreV2 = gfr_liaison("UserDocParametreV2");
                sToleranceEntretien = "0";
                sPartGMAO = "0";
                sNbHeuresTotales = "0";
                PROGUserDocLocalisation = gfr_liaison("UserDocLocalisation");
            }

            CHEMINMAIL = gfr_liaison("PathMail");

            sExportCrImm = gfr_liaison("ExportCrImm");
            sPathReportCRv9 = gfr_liaison("PathReportCRv9");
            ETATFACTUREMANUELExportV2 = gsRpt + gfr_liaison("CRFactureManuelleExportV2");

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> date changé de 01/01/2030 à 01/01/2100
            dtdateFactManuV2 = Convert.ToDateTime(nz(gfr_liaison("dateFactManuV2"), "01/01/2100"));

            dtdateDevExportV2 = Convert.ToDateTime(nz(gfr_liaison("dateDevExportV2"), "01/01/2100"));
            //===> Fin Modif Mondir

            sActiveP1 = gfr_liaison("ActiveP1");

            //===> Mondir le 23.07.2020, pourquoi pas ajouté le nom serveur ?
            sServeurSQL = gfr_liaison("serveurSQL");

            // If sActiveP1 = "1" Then
            if (adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL".ToUpper() || adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_CSHARP_TEST".ToUpper() ||
               adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_PDA".ToUpper())
            {

                cODBCP1 = gfr_liaison("DataSourceP1");
                sServeurSQL = gfr_liaison("serveurSQL");

                if (DateTime.Now.ToString("dd/MM/yyyy") == "20/02/2017" && fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
                {
                    sServeurSQL = ".";
                }
                //ModP1.schaineConnectP1 = "Provider=SQLOLEDB.1;Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=P1_DT;" + "Data Source=" + sServeurSQL;

                if (General._ExecutionMode == ExecutionMode.Test)
                    ModP1.schaineConnectP1 = $"SERVER=DT-SQL-01;DATABASE=P1_DT_CSHARP_TEST;INTEGRATED SECURITY=TRUE;";
                else
                    ModP1.schaineConnectP1 = $"SERVER={sServeurSQL};DATABASE=P1_DT;INTEGRATED SECURITY=TRUE;";

                if (General.fncUserName().ToLower().Contains("mondir"))
                    ModP1.schaineConnectP1 = $"SERVER={sServeurSQL};DATABASE=P1_DT;INTEGRATED SECURITY=TRUE;";


                sMoisDebutSaison = "08";

                ModP1.fc_OpenConnP1();
            }
            else
            {
                ModP1.fc_CloseConnP1();
            }

            //==== variable relative aux envoie sur PDA.
            sActiveEnvoieSurPDA = gfr_liaison("ActiveEnvoieSurPDA");

            //If UCase(fncUserName) = UCase("rachid abbouchi") And Date = #6/20/2016# Or UCase(adocnn.DefaultDatabase) = UCase("AXECIEL_DELOSTAL_PDA") Then
            //sActiveEnvoieSurPDA = "1"
            //End If

            PROGUserInter_RelevesCompteurs = gfr_liaison("UserInter_RelevesCompteurs");

            PROGUserDocSuiviRelevesV2 = gfr_liaison("UserDocSuiviRelevesV2");

            sPDARepEmissionFichier = gfr_liaison("PDARepEmissionFichier");

            //=== Fiche vehicule.
            PROGUserDocVehicule = gfr_liaison("UserDocVehicule");

            //=== nombre d'intervention historisé envoté vers le PDA.
            using (var rmpModAdo = new ModAdo())
                lnbHistoInterPDA = Convert.ToInt32(nz(rmpModAdo.fc_ADOlibelle("SELECT     NbHistoInter From PDA_Param WHERE Code = 'HISTO'"), 0));

            //=== nombre de devis historisé envoté vers le PDA.
            using (var rmpModAdo = new ModAdo())
                lnbHistoDevisPDA = Convert.ToInt32(nz(rmpModAdo.fc_ADOlibelle("SELECT     NbHistoDevis From PDA_Param WHERE Code = 'HISTO'"), 0));

            //=== lien http vers les fichiers wave (pda)
            slienHttpWave = gfr_liaison("lienHttpWave")?.Trim();

            //=== active ou désactive le controle des statuts sur les interventions.
            sActiveCtlSatutInter = gfr_liaison("activeCtlSatutInter").Trim();

            //=== modif du 13 02 2017, bloque la crationde bon de commande si le devis est cloturé.
            sBloqueAchatSurDevis = gfr_liaison("BloqueAchatSurDevis");

            //=== active dans le P2, le plannification sur plusieurs jours si l'inter depasse 8 h.
            sEtaleJournee = gfr_liaison("EtaleJournee");

            dbTolerance = Convert.ToDouble(nz(gfr_liaison("SoldeToleranceFactureFour"), 100));
            //dbTolerance = 20

            //=== flag bloquant l'accés à sage, modif du 28/05/2017.
            sDesActiveSage = gfr_liaison("DesActiveSage");

            //=== stoke le compte général client.
            sCptGeneralClient = gfr_liaison("CptGeneralClient");

            //=== stoke le compte général fournisseur.
            sCptGeneralFourn = gfr_liaison("CptGeneralFourn");

            //===affiche le champ code Long.
            sAfficheCodeLong = gfr_liaison("AfficheCodeLong");

            //=== lien vers la fiche planning.
            ProgUserDocPlanningCodeJock = gfr_liaison("UserDocPlanningCodeJock");

            //=== nom interlocuteur pourr fiche historique frmHistorique.
            sInterlocuteurHistoAppel = gfr_liaison("InterlocuteurHistoAppel");

            //=== rapport intervention v2
            RAPPORTINTERVENTIONV2 = gfr_liaison("RapportInterventionV2");

            //=== Validation corps 2.
            sValidCorpsDevisV2 = gfr_liaison("ValidCorpsDevisV2");

            //sValidCorpsDevisV2 = "1"

            //=== code société pour la gestion des multi sociétées sous PDA.
            sCodePDASociete = gfr_liaison("CodePDASociete");

            //=== nouveau critére pour les visites d'entretien.
            if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "15/03/2018" || adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_PDA".ToUpper())
            {
                sNewCritereP2V2 = "1";
            }

            sNewCritereP2V2 = gfr_liaison("NewCritereP2V2");

            sContactMailViaBase = gfr_liaison("ContactMailViaBase");

            //=== nouvelle procedure pour extraitre les contacts sous Outlook.
            sInitAdressListOutlook345_V2 = gfr_liaison("InitAdressListOutlook345_V2");

            //=== si sImmGeolocObligatoire = 1, saisis du géocodage obligatoire.
            sImmGeolocObligatoire = gfr_liaison("ImmGeolocObligatoire");
            // sImmGeolocObligatoire = "1"

            //=== 30 05 2018. désactive l'inform dans l'envoie de l'intervention vers le PDA.
            sDesactiveInform = gfr_liaison("DesactiveInform");

            //=== le 26 06 2018, attribue des droits pour la case à cocher liste rouge.
            sDroitListeRouge = gfr_liaison("DroitListeRouge");

            //=== Ajout d'un ramoneur par immeuble.
            sRamoneurParImmeuble = gfr_liaison("RamoneurParImmeuble");

            //=== fiche descriptif chaufferie.
            PROGUserDocDescriptifChaufferie = gfr_liaison("UserDocDescriptifChaufferie");

            //=== Fiche parmaetre materiel en lien avec le P3.
            PROGUserDocParamP3 = gfr_liaison("UserDocParamP3");

            //'=== recupere le ramoneur par defaut pour les visites d'entretiens
            //'=== pour les particuliers.
            sRamoneurP2particulier = gfr_liaison("RamoneurP2particulier");

            //===> Mondir le 05.03.2021, this line wasnt here, dont know why
            //===> to fix #2268
            //=== état avis de passage.
            sEtatAvisDePassage = gfr_liaison("EtatAvisDePassage");
            //===> Fin Modif Mondir

            //=== affiche libelle particulier.
            sAfficheLibParticulier = gfr_liaison("AfficheLibParticulier");

            //==,Disocies les relevés des particulier copro et de propriataire
            sReleveCoproPoprietaire = gfr_liaison("ReleveCoproPoprietaire");


            if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "13/09/2018")
            {
                sReleveCoproPoprietaire = "1";
            }
            sGMAOcriterePartImmeuble = gfr_liaison("GMAOcriterePartImmeuble");

            if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "27/09/2018")
            {
                sGMAOcriterePartImmeuble = "1";
            }

            PROGUserDocGestionAppel = gfr_liaison("UserDocGestionAppel");


            sKeyGeocodage = gfr_liaison("KeyGeocodage");


            ModAppel.spathAlgoria = gfr_liaison("PathAlgoria");

            RAPPORTINTERVENTIONV2_1 = gfr_liaison("RAPPORTINTERVENTIONV2_1");

            if (fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "05/04/2019")
            {
                ModAppel.spathAlgoria = "C:\\temp\\LogAlgoria";
                RAPPORTINTERVENTIONV2_1 = "1";
                RAPPORTINTERVENTIONV2 = "RapportInterventionV2 nouvelle version avec photo.rpt";

            }

            ModAppel.spathAlgoria = gfr_liaison("PathAlgoria");
            //  '=== chemin complet du dossier des logs pour Algoria.
            ModAppel.spathAlgoriaFull = ModAppel.spathAlgoria + "\\ " + DateTime.Now.Year + " - " + DateTime.Now.Month + " - " + DateTime.Now.Day;


            //  '=== nomme le ficher de log par utilisateur + no proccess.
            // 'sNameFileAlgoria = fncUserName & "_" & GetCurrentProcessId
            ModAppel.sNameFileAlgoria = fncUserName();
            // 'UserDocGestionAppel.fc_LoadGestionAppel

            if (string.IsNullOrEmpty(ModAppel.spathAlgoriaFull) && bActiveAppelAlgoria == false)
            {
                if (Convert.ToInt32(getFrmReg(Variable.cUserDocGestionAppel, "ActiveGestionAppel", "0")) == 1)
                {
                    if (fncUserName().ToUpper() != "rachid abbouchi".ToUpper())
                    {
                        //Todo verifier ces lignes
                        //UserDocGestionAppel u = new UserDocGestionAppel();
                        //u.fc_LoadGestionAppel();
                    }

                }
            }
            ModAppel.sImmeubleDefaut = gfr_liaison("ImmeubleDefaut");
            ModAppel.sCodeReglementDefContrat = gfr_liaison("CodeReglementDefContrat");

            if (UCase(fncUserName()) == UCase("rachid abbouchi") && DateTime.Now.ToString() == "10/07/2019")
            {
                sGecetV3 = "1";
                ModParametre.NomBaseGecet = "AXECIEL_GECET";
            }

            sActiveDroitPlan = gfr_liaison("ActiveDroitPlan");
            sActiveModeReglementSageFourn = gfr_liaison("ActiveModeReglementSageFourn");
            //'sActiveModeReglementSageFourn = "1"
            if (UCase(fncUserName()) == UCase("rachid abbouchi") && DateTime.Now.ToString() == "27/03/2020")
            {
                DefaultServer = ".";
            }
            else
            {
                DefaultServer = "DT-SQL-01";
            }
            // ' Public sCodeSvrClim             As String
            //'    Public sCodeSvrExpliot          As String
            //'    Public sCodeSvrTrx              As String
            sCodeSvrClim = gfr_liaison("CodeDevClim");
            sCodeSvrExpliot = gfr_liaison("CodeDevExploit");
            sCodeSvrTrx = gfr_liaison("CodeDevTravaux");


            sEvolution = gfr_liaison("FactureManuelV2");
            if (fncUserName() == "mohammed")
                sEvolution = "1";

            sCritreDevisFac = gfr_liaison("CritreDevisFac");
            sMDParticle = gfr_liaison("MDParticle");
            sChefSecteurLibre = gfr_liaison("sChefSecteurLibre");


            PROGUserDocExportImmAstreinte = gfr_liaison("UserDocExportImmAstreinte");
            sClimMat = gfr_liaison("MatriculeClim");

            if (UCase(fncUserName()) == UCase("rachid abbouchi") && DateTime.Now.ToString() == "14/05/2020")
            {
                sEvolution = "1";
                //'sCptBloqueSurFacture = "1"
                sCptBloqueSurFacture = "0";
                sCodeSvrClim = "TMCLIM";
                sCodeSvrExpliot = "TMEXP";
                sCodeSvrTrx = "TMTRX";
                sCritreDevisFac = "1";
                sMDParticle = "1";
                sChefSecteurLibre = "1";
                sClimMat = "98";
            }

            sEtatExportImmAstreinteV9 = gsRpt + gfr_liaison("ExportImmAstreinteV9");

            sContratMultiTVA = gfr_liaison("ContratMultiTVA");
            sJournalVente = gsRpt + gfr_liaison("JournalVente");
            sJournalVente = "VENT";
            //===> Ahmed: Modification 08 09 2020  V 07 09 2020
            sClimInitiale = gfr_liaison("ClimInitiale");
            sActiveFactMultiAppel = gfr_liaison("ActiveFactMultiAppel");
            //==> Fin modification

            //===> Mondir le 15.11.2020 pour ajouter les modifs de la version V12.11.2020
            if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Now.ToString("dd/MM/yyyy") == "11/09/2020")
            {
                sActiveFactMultiAppel = "1";
            }
            //sJournalVente = ""

            sSignatureMail = gfr_liaison("SignatureMail");


            sTVA300 = gfr_liaison("TVA300");


            dbSeuilTTCtva300 = Convert.ToDouble(gfr_liaison("SeuilTVA300", "0"));

            PROGUserSyntheseAppel = gfr_liaison("UserSyntheseAppel");


            sSignatureServiceFact = "<p class=\"x_MsoNormal\"><b><span style=\"font-size:10.0pt; font-family:&quot;Verdana&quot;,sans-serif; color:#395BA3\">Service Facturation</span></b></p> ";
            sSignatureServiceFact = sSignatureServiceFact + " <p class=\"x_MsoNormal\"><span style=\"font-size:10.0pt; font-family:&quot;Verdana&quot;,sans-serif; color:#395BA3\">T. 01 41 99 96 09</span> ";
            sSignatureServiceFact = sSignatureServiceFact + " </p><p class=\"x_MsoNormal\"><span style=\"font-size:10.0pt; font-family:&quot;Verdana&quot;,sans-serif; color:#395BA3\">F. 01 41 99 96 10</span></p> ";
            sSignatureServiceFact = sSignatureServiceFact + " <p class=\"x_MsoNormal\"><b><span style=\"font-family:&quot;Verdana&quot;,sans-serif; color:#395BA3\">&nbsp;</span></b></p>";
            //===> Fin Modif Mondir

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version  V07.01.2021
            ETATFacturemanuelleSQLv3 = gsRpt + gfr_liaison("FacturemanuelleSQLv3");
            ETATFACTUREMANUELExportV3 = gsRpt + gfr_liaison("CRFactureManuelleExportV3");
            dtdateFactManuV3 = nz(gfr_liaison("dateFactManuV3"), "01/01/2100").ToDate();

            ETATfacturationContratV2 = gsRpt + gfr_liaison("FacturationContratV2");
            ETATfacturationContratExportV2 = gsRpt + gfr_liaison("FacturationContratExportV2");

            sAdresseEnvoie = gfr_liaison("AdresseEnvoie");
            //===> Fin Modif Mondir

            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
            //=== compte client douteux
            sCptCLientDouteux = gfr_liaison("CptCLientDouteux");
            if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Today.ToString("dd/MM/yyyy") == "01/12/2021" || General.fncUserName().ToLower() == "mondir".ToLower())
            {
                sCptCLientDouteux = "41600000";
            }
            //===> Fin Modif Mondir

            //===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
            //===> Mondir le 18.02.2021 pour ajouter les modfis de la version V16.02.2021
            sChargeGmao = gfr_liaison("ChargeGMAO");
            if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() || General.fncUserName().ToLower() == "mondir".ToLower()
            || General.fncUserName().ToUpper() == "Axeciel-user2".ToUpper() || General.fncUserName().ToUpper() == "Axeciel-user".ToUpper())
            {
                sChargeGmao = "1";
            }
            //===> Fin Modif Mondir
        }
        private static void TimerStandard_Timer()
        {
            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("");
        }
        private static object fc_StockODBC_BaseTestPDA()
        {
            object functionReturnValue = null;
            string strRegistre = null;
            int sNoFile = 0;
            string strUserName = null;
            string sNomServeur = null;

            try
            {
                sNomServeur = DefaultServer;

                strUserName = fncUserName();

                strRegistre = "Windows Registry Editor Version 5.00" + "\n";
                strRegistre = strRegistre + "\n";

                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI]" + "\n";
                strRegistre = strRegistre + "\n";

                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\ODBC Data Sources]" + "\n";

                strRegistre = strRegistre + "\"DELOSTALODBCTEST\"=\"SQL Server\"" + "\n";
                strRegistre = strRegistre + "\"DTDELOSTALTEST\"=\"SQL Server\"" + "\n";
                //strRegistre = strRegistre & """DTDELOSTAL""=""SQL Server""" & vbCrLf



                //=== DELOSTAL.
                //    strRegistre = strRegistre & vbCrLf
                //    strRegistre = strRegistre & "[HKEY_CURRENT_USER\Software\ODBC\ODBC.INI\DTDELOSTAL]" & vbCrLf
                //    strRegistre = strRegistre & """Driver""=""C:\\WINDOWS\\System32\\Sqlsrv32.dll""" & vbCrLf
                //    strRegistre = strRegistre & """Server""=""" & sNomServeur & """" & vbCrLf
                //    strRegistre = strRegistre & """Database""=""AXECIEL_DELOSTAL""" & vbCrLf
                //    strRegistre = strRegistre & """LastUser""=""" & strUserName & """" & vbCrLf
                //    strRegistre = strRegistre & """Trusted_Connection""=""Yes""" & vbCrLf


                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\DELOSTALODBCTEST]" + "\n";
                strRegistre = strRegistre + "\"Driver\"=\"C:\\\\WINDOWS\\\\System32\\\\Sqlsrv32.dll\"" + "\n";
                strRegistre = strRegistre + "\"Server\"=\"" + sNomServeur + "\"" + "\n";
                strRegistre = strRegistre + "\"Database\"=\"CPTA_DELOSTALTEST\"" + "\n";
                strRegistre = strRegistre + "\"LastUser\"=\"" + strUserName + "\"" + "\n";
                strRegistre = strRegistre + "\"Trusted_Connection\"=\"Yes\"" + "\n";
                strRegistre = strRegistre + "\n";

                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\DTDELOSTALTEST]" + "\n";
                strRegistre = strRegistre + "\"Driver\"=\"C:\\\\WINDOWS\\\\System32\\\\Sqlsrv32.dll\"" + "\n";
                strRegistre = strRegistre + "\"Server\"=\"" + sNomServeur + "\"" + "\n";
                strRegistre = strRegistre + "\"Database\"=\"AXECIEL_DELOSTAL_PDA\"" + "\n";
                strRegistre = strRegistre + "\"LastUser\"=\"" + strUserName + "\"" + "\n";
                strRegistre = strRegistre + "\"Trusted_Connection\"=\"Yes\"" + "\n";
                strRegistre = strRegistre + "\n";


                if (Dossier.fc_ControleDossier("C:\\temp") == false)
                {
                    Dossier.fc_CreateDossier("C:\\temp");
                }
                Dossier.fc_WriteInFile("C:\\temp\\SysGicODBC.reg", strRegistre);
                var ProcessInfo = new ProcessStartInfo();
                ProcessInfo.FileName = "regedit -s C:\\temp\\SysGicODBC.reg";
                ProcessInfo.RedirectStandardOutput = true;
                ProcessInfo.RedirectStandardError = true;
                ProcessInfo.UseShellExecute = false;
                ProcessInfo.CreateNoWindow = true;
                var Process = new Process();
                Process.StartInfo = ProcessInfo;
                Process.EnableRaisingEvents = true;
                Process.Start();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Multi Société", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public static string fncUserName()
        {
            string functionReturnValue = null;
            string sBuffer = null;
            int lSize = 0;

            sBuffer = GetUserName();
            lSize = sBuffer.Length;


            if (lSize > 0)
            {
                functionReturnValue = Left(sBuffer, lSize).Trim();
                functionReturnValue = Left(functionReturnValue, functionReturnValue.Length);

            }
            else
            {
                functionReturnValue = null;
            }
            return functionReturnValue;
            //return "administrateur";
        }

        private static string GetUserName()
        {
            //return "S.Guerin";
            return Environment.UserName;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="str"></param>
        /// <param name="lenght"></param>
        /// <returns></returns>
        public static string Left(string str, int lenght)
        {
            return string.IsNullOrWhiteSpace(str) ? "" : str.Substring(0, lenght > str.Length ? str.Length : lenght);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="vPremier"></param>
        /// <param name="vSecond"></param>
        /// <returns></returns>
        public static object nz(object vPremier, object vSecond)
        {
            return vPremier == null || vPremier.ToString() == "" || vPremier.ToString() == "null" ? vSecond : vPremier;
        }
        private static string fc_SQLServer()
        {
            string functionReturnValue = null;
            // si cSQLorAccess = 1 la base est sqlserver
            functionReturnValue = getFrmReg("App", "SQLserver", "").ToString();
            if (string.IsNullOrEmpty(functionReturnValue))
            {
                saveInReg("App", "SQLserver", cSQLorAccess);
            }
            return functionReturnValue;

        }

        public static string gFr_ODBCBddRe()
        {
            string functionReturnValue = null;
            // Renvoie chaine pour connection ODBC (stockée dans registres)
            string sODBCreseau = null;
            try
            {
                sODBCreseau = getFrmReg("App", DefaultSource, "").ToString();
                if (string.IsNullOrEmpty(sODBCreseau))
                {
                    sODBCreseau = cFrODBCbaseReseau;
                    saveInReg("App", DefaultSource, sODBCreseau);
                }
                functionReturnValue = sODBCreseau;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sLiaison"></param>
        /// <param name="sValeurDefaut"></param>
        /// <returns></returns>
        public static string gfr_liaison(string sLiaison, string sValeurDefaut = "")
        {
            string sAdresse = null;
            //  On Error GoTo Erreur
            //    If lngInitRegitre = 1 Then
            //        SaveSetting cFrNomApp, "App", sLiaison, ""
            //   End If
            sAdresse = getFrmReg("App", sLiaison, "").ToString();
            if (string.IsNullOrEmpty(sAdresse))
            {
                sAdresse = rechLiaison(sLiaison, sValeurDefaut);
                saveInReg("App", sLiaison, sAdresse);
            }
            return sAdresse;

        }
        private static string rechLiaison(string sCritere, string sAdresseDefaut = "")
        {
            string functionReturnValue = null;

            if (lngInitRegitre == 0 && ModMain.bMain == false)
            {
                return functionReturnValue;
            }
            if (rstmp == null)
            {
                rstmp = new DataTable();
            }

            rstmp = new DataTable();
            if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                SDArstmp = new SqlDataAdapter("SELECT Adresse from lienV2 where code='" + sCritere + "'", adocnn);
            else
                SDArstmp = new SqlDataAdapter("SELECT Adresse from lien where code='" + sCritere + "'", adocnn);
            SDArstmp.Fill(rstmp);

            if (rstmp.Rows.Count > 0)
            {
                functionReturnValue = rstmp.Rows[0]["Adresse"] + "";
            }
            else
            {
                ////=============> Mondir le 29.05.2020 bug found in Log must add test if in Prod insert in LienV2 else insert in Lien
                if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                    Execute("INSERT INTO LienV2 (Code,Adresse) VALUES ('" + sCritere + "','" + sAdresseDefaut + "')");
                else
                    Execute("INSERT INTO Lien (Code,Adresse) VALUES ('" + sCritere + "','" + sAdresseDefaut + "')");
                functionReturnValue = sAdresseDefaut;
            }
            rstmp.Dispose();
            rstmp = null;

            rstmp = new DataTable();

            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Query"></param>
        /// <returns></returns>
        public static int Execute(string Query, SqlConnection Conn = null)
        {
            //===> Mondir 08.09.2020, Ajout du LOG
            Program.SaveException(null, $"General.Execute() - Enter In The Function");
            Program.SaveException(null, $"General.Execute() - Query = {Query}");
            //===> Fin Modif Mondir

            var Resulat = 0;

            try
            {
                if (Conn == null)
                    Conn = adocnn;

                var Cmd = new SqlCommand(Query, Conn);
                if (Conn.State != ConnectionState.Open)
                    Conn.Open();
                Cmd.CommandTimeout = 120;
                Resulat = Cmd.ExecuteNonQuery();

                //===> Mondir 08.09.2020, Ajout du LOG
                Program.SaveException(null, $"General.Execute() - Executed And Resulat = {Resulat}");
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Program.SaveException(ex, Query);
                Program.SaveException(null, Query);
                Resulat = -1;
            }
            return Resulat;
        }
        public static int nbACode(string sCritere)
        {
            int functionReturnValue = 0;

            int i = 0;

            if (rstmp == null)
            {
                rstmp = new DataTable();
            }
            rstmp = new DataTable();
            if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                SDArstmp = new SqlDataAdapter("SELECT Adresse from lienV2 where ACode='" + sCritere + "' ORDER BY ACode, Code", adocnn);
            else
                SDArstmp = new SqlDataAdapter("SELECT Adresse from lien where ACode='" + sCritere + "' ORDER BY ACode, Code", adocnn);
            SDArstmp.Fill(rstmp);
            i = 1;


            if (rstmp.Rows.Count > 0)
            {
                foreach (DataRow Row in rstmp.Rows)
                {
                    //nbDossierMail = i
                    Array.Resize(ref MesDossiers, i + 1);
                    MesDossiers[i] = Row["Adresse"].ToString();
                    i = i + 1;
                }
            }

            functionReturnValue = i;
            rstmp.Dispose();
            rstmp = null;
            return functionReturnValue;

        }
        public static object StockSpecifQualif()
        {
            if (rstmp == null)
            {
                rstmp = new DataTable();
            }
            rstmp = new DataTable();
            SDArstmp = new SqlDataAdapter("SELECT CodeQualif,QualifTech,QualifCom,QualifDisp,QualifAdmin, QualifGest,QualifPart,QualifDev,QualifRtrav,QualifRexp FROM SpecifQualif ORDER BY CodeQualif", adocnn);
            SDArstmp.Fill(rstmp);

            nbCodeTechnicien = 0;
            nbCodeCommercial = 0;
            nbCodeDispatch = 0;
            nbCodeDeviseur = 0;
            nbCodeGestionnaire = 0;
            nbCodeParticulier = 0;
            nbCodeAdministratif = 0;
            nbCodeRespExpl = 0;
            nbCodeRespTrav = 0;

            if (rstmp.Rows.Count > 0)
            {
                foreach (DataRow Row in rstmp.Rows)
                {
                    if (Convert.ToBoolean(Row["QualifTech"]))
                    {
                        nbCodeTechnicien = nbCodeTechnicien + 1;
                        Array.Resize(ref CodeQualifTechnicien, nbCodeTechnicien + 1);
                        CodeQualifTechnicien[nbCodeTechnicien] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifCom"]))
                    {
                        nbCodeCommercial = nbCodeCommercial + 1;
                        Array.Resize(ref CodeQualifCommercial, nbCodeCommercial + 1);
                        CodeQualifCommercial[nbCodeCommercial] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifDisp"]))
                    {
                        nbCodeDispatch = nbCodeDispatch + 1;
                        Array.Resize(ref CodeQualifDispatch, nbCodeDispatch + 1);
                        CodeQualifDispatch[nbCodeDispatch] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifDev"]))
                    {
                        nbCodeDeviseur = nbCodeDeviseur + 1;
                        Array.Resize(ref CodeQualifDeviseur, nbCodeDeviseur + 1);
                        CodeQualifDeviseur[nbCodeDeviseur] = Row["CodeQualif"].ToString();
                    }

                    if (Row["QualifGest"] != DBNull.Value && Convert.ToBoolean(Row["QualifGest"]))
                    {
                        nbCodeGestionnaire = nbCodeGestionnaire + 1;
                        Array.Resize(ref CodeQualifGestionnaire, nbCodeGestionnaire + 1);
                        CodeQualifGestionnaire[nbCodeGestionnaire] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifPart"]))
                    {
                        nbCodeParticulier = nbCodeParticulier + 1;
                        Array.Resize(ref CodeQualifParticulier, nbCodeParticulier + 1);
                        CodeQualifParticulier[nbCodeParticulier] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifAdmin"]))
                    {
                        nbCodeAdministratif = nbCodeAdministratif + 1;
                        Array.Resize(ref CodeQualifAdministratif, nbCodeAdministratif + 1);
                        CodeQualifAdministratif[nbCodeAdministratif] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifRTrav"]))
                    {
                        nbCodeRespTrav = nbCodeRespTrav + 1;
                        Array.Resize(ref CodeQualifRespTrav, nbCodeRespTrav + 1);
                        CodeQualifRespTrav[nbCodeRespTrav] = Row["CodeQualif"].ToString();
                    }

                    if (Convert.ToBoolean(Row["QualifRexp"]))
                    {
                        nbCodeRespExpl = nbCodeRespExpl + 1;
                        Array.Resize(ref CodeQualifRespExpl, nbCodeRespExpl + 1);
                        CodeQualifRespExpl[nbCodeRespExpl] = Row["CodeQualif"].ToString();
                    }
                }
            }
            else
            {
                Array.Resize(ref CodeQualifTechnicien, nbCodeTechnicien + 1);
                CodeQualifTechnicien[nbCodeTechnicien] = "*";
                Array.Resize(ref CodeQualifCommercial, nbCodeCommercial + 1);
                CodeQualifCommercial[nbCodeCommercial] = "*";
                Array.Resize(ref CodeQualifDispatch, nbCodeDispatch + 1);
                CodeQualifDispatch[nbCodeDispatch] = "*";
                Array.Resize(ref CodeQualifDeviseur, nbCodeDeviseur + 1);
                CodeQualifDeviseur[nbCodeDeviseur] = "*";
                Array.Resize(ref CodeQualifGestionnaire, nbCodeGestionnaire + 1);
                CodeQualifGestionnaire[nbCodeGestionnaire] = "*";
                Array.Resize(ref CodeQualifParticulier, nbCodeParticulier + 1);
                CodeQualifParticulier[nbCodeParticulier] = "*";
                Array.Resize(ref CodeQualifAdministratif, nbCodeAdministratif + 1);
                CodeQualifAdministratif[nbCodeAdministratif] = "*";
                Array.Resize(ref CodeQualifRespTrav, nbCodeRespTrav + 1);
                CodeQualifRespTrav[nbCodeRespTrav] = "*";
                Array.Resize(ref CodeQualifRespExpl, nbCodeRespExpl + 1);
                CodeQualifRespExpl[nbCodeRespExpl] = "*";
            }

            rstmp.Dispose();
            rstmp = null;
            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="ValeurHex"></param>
        /// <returns></returns>
        public static long ConvertHexDec(string ValeurHex)
        {
            return 0 /*int.Parse(ValeurHex, System.Globalization.NumberStyles.HexNumber)*/;
            //double functionReturnValue = 0;

            //short i = 0;
            //short j = 0;
            //string[] tabHex = null;
            //string[] tabValHex = null;
            //int nbCaract = 0;

            //Array.Resize(ref tabHex, 17);
            ////initialisation du tableau de valeur Hexadecimale
            ////nota : les index du tableau correspondent à leur valeur hexadécimale
            //for (i = 0; i <= 9; i++)
            //{
            //    tabHex[i] = Convert.ToString(i);
            //}

            //tabHex[10] = "A";
            //tabHex[11] = "B";
            //tabHex[12] = "C";
            //tabHex[13] = "D";
            //tabHex[14] = "E";
            //tabHex[15] = "F";

            //nbCaract = ValeurHex.Length;
            //Array.Resize(ref tabValHex, nbCaract + 1);



            //for (i = 0; i <= nbCaract - 1; i++)
            //{
            //    tabValHex[i] = Mid(ValeurHex, i + 1, 1);
            //}

            //for (i = 0; i <= nbCaract - 1; i++)
            //{
            //    for (j = 0; j <= 15; j++)
            //    {
            //        if (tabValHex[i] == tabHex[j])
            //        {
            //            tabValHex[i] = Convert.ToString(j);
            //            break;
            //        }
            //    }
            //}
            //functionReturnValue = 0;
            //for (i = 0; i <= nbCaract - 1; i++)
            //{
            //    functionReturnValue = functionReturnValue + Convert.ToDouble(tabValHex[i]) * Math.Pow(16, ((nbCaract - 1) - i));
            //}
            //return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="str"></param>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public static string Mid(string str, int start, int end)
        {
            if (str.Length < start)
                return str;
            if (str.Length == 0)
                return "";
            if (end > str.Length)
                end = str.Length;
            return str.Substring(start - 1, end);
        }

        public static string Mid(string str, int start)
        {
            if (str.Length < start)
                return str;
            if (str.Length == 0)
                return "";
            return str.Substring(start - 1);
        }

        public static void fc_CreateDossierErr()
        {
            string sRapport = null;

            //=== crée un dossier de rapport (erreur) pour chaque utilisateur.
            sRapport = gfr_liaison("Rapport");
            sRapport = sRapport + fncUserName();
            Dossier.fc_CreateDossier(sRapport);

            cRAPPORT = sRapport + "\\" + "RapportFactManuel.txt";
            cFACTURENONMISEAJOUR = sRapport + "\\" + "FactureEnAttente.txt";
            cCheminFichierFactContrat = sRapport + "\\" + "erreurcontrat.txt";
            cCheminFichierErrTrx = sRapport + "\\" + "ErreurTravaux.txt";
            cCheminFichierAnalyCtr = sRapport + "\\" + "ErreurAnalyContrat.txt";
            cCheminFichierPorteFeuille = sRapport + "\\" + "ErreurPorteFeuilleContrat.txt";
            cCheminFichierCtrErr = sRapport + "\\" + "ErreurSituationContrat.txt";

            //Open cRAPPORT For Output As 2
            //Open cFACTURENONMISEAJOUR For Output As 3
            //Open cCheminFichierFactContrat For Output As 4
            //Open cCheminFichierErrTrx For Output As 5
            //Open cCheminFichierAnalyCtr For Output As 6
            //Open cCheminFichierPorteFeuille For Output As 7



        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sDateFormate"></param>
        /// <returns></returns>
        public static bool IsDate(string sDateFormate)
        {
            DateTime tmp;
            return DateTime.TryParse(sDateFormate, out tmp);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sDateFormate"></param>
        /// <returns></returns>
        public static bool IsDate(object sDateFormate)
        {
            DateTime tmp;
            return DateTime.TryParse(sDateFormate.ToString(), out tmp);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="key"></param>
        /// <param name="defaultValue"></param>
        /// <param name="UsecFrNomApp"></param>
        /// <returns></returns>
        public static string getFrmReg(string folder, string key, string defaultValue = "null", bool UsecFrNomApp = true)
        {
            if (UsecFrNomApp)
            {

                if (Registry.GetValue(regpath + $"{cFrNomApp}\\" + folder, key, defaultValue) == null || Registry.GetValue(regpath + $"{cFrNomApp}\\" + folder, key, defaultValue) == "null")
                {
                    //===========> Code Commenté par mondir pcq il affiche ça sur les machines des utilisateurs lors la premiere execution de l'application
                    //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(regpath + cFrNomApp + folder + "\\" + key + " not found");
                    return defaultValue;
                }
                else
                {
                    return Registry.GetValue(regpath + $"{cFrNomApp}\\" + folder, key, defaultValue).ToString();
                }
            }
            else
            {
                if (Registry.GetValue(regpath + folder, key, defaultValue) == null || Registry.GetValue(regpath + folder, key, defaultValue) == "null")
                {
                    //===========> Code Commenté par mondir pcq il affiche ça sur les machines des utilisateurs lors la premiere execution de l'application
                    //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(regpath + folder + "\\" + key + " not found");
                    return "";
                }
                else
                {
                    return Registry.GetValue(regpath + folder, key, defaultValue).ToString();
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="folder"></param>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="UsecFrNomApp"></param>
        public static void saveInReg(string folder, string key, string value, bool UsecFrNomApp = true)
        {
            if (value == null)
                value = "";

            if (UsecFrNomApp)
                Registry.SetValue(regpath + $"{cFrNomApp}\\" + folder, key, value);
            else
                Registry.SetValue(regpath + folder, key, value);
        }
        public static void DeleteSetting(string folder, bool UsecFrNomApp = true)
        {
            RegistryKey key = null;
            if (UsecFrNomApp)
            {
                key = Registry.CurrentUser.OpenSubKey(regpath + $"{cFrNomApp}\\", true);
            }
            else
            {
                key = Registry.CurrentUser.OpenSubKey(regpath, true);
            }
            if (key != null)///this if added by Mohammed
                key.DeleteSubKey(folder);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMois"></param>
        /// <param name="sYear"></param>
        /// <returns></returns>
        public static int CalculMaxiJourMois(int sMois, int sYear = 0)
        {
            if (sYear == 0)

                sYear = DateTime.Now.Year;

            return DateTime.DaysInMonth(sYear, sMois);

            short functionReturnValue = 0;

            bool biSextile = false;

            const short AnneeRef = 2000;



            if (((Convert.ToInt32(sYear) - AnneeRef) % 4) == 0)
            {
                biSextile = true;
            }
            else
            {
                biSextile = false;
            }

            switch (sMois)
            {
                case 1:
                    //Janvier
                    functionReturnValue = 31;
                    //31 jours
                    break;
                case 2:
                    if (biSextile == true)//tested quand syear=2016
                    {
                        functionReturnValue = 29;
                    }
                    else//tested
                    {
                        functionReturnValue = 28;
                    }
                    break;
                case 3:
                    functionReturnValue = 31;
                    break;
                case 4:
                    functionReturnValue = 30;
                    break;
                case 5:
                    functionReturnValue = 31;
                    break;
                case 6:
                    functionReturnValue = 30;
                    break;
                case 7:
                    functionReturnValue = 31;
                    break;
                case 8:
                    functionReturnValue = 31;
                    break;
                case 9:
                    functionReturnValue = 30;
                    break;
                case 10:
                    functionReturnValue = 31;
                    break;
                case 11:
                    functionReturnValue = 30;
                    break;
                case 12://tested
                    functionReturnValue = 31;
                    break;
            }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool IsNumeric(string text)
        {
            int entier;
            float dcm;
            if (string.IsNullOrEmpty(text))
                return false;
            return int.TryParse(text, out entier) || float.TryParse(text, out dcm);
        }
        public static void fc_MajCompteTiersGdp(string sCodeImmeuble, string sCode1)
        {
            string sSQL = null;
            DataTable rsGdp = default(DataTable);
            ModAdo modAdorsGdp = null;
            DataTable rsDt = default(DataTable);
            SqlDataAdapter SDArsDt;
            string sServ = null;

            //=== 11 12 2009 cette fonction est temporaire, les comptes tiers sont crées dans delostal
            //=== et non dans gaz de paris, cette focntion a pour but de mettre à jour la base gaz de paris.

            try
            {
                sSQL = "SELECT ncompte, codeimmeuble, CompteDivers FROM immeuble";

                if (!string.IsNullOrEmpty(sCodeImmeuble))
                {
                    sSQL = sSQL + " WHERE codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else if (!string.IsNullOrEmpty(sCode1))
                {
                    sSQL = sSQL + " WHERE code1 ='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }

                modAdorsGdp = new ModAdo();
                rsGdp = modAdorsGdp.fc_OpenRecordSet(sSQL);
                rsDt = new DataTable();

                foreach (DataRow rsGdpRow in rsGdp.Rows)
                {
                    sSQL = "select ncompte, CompteDivers  from immeuble where codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsGdpRow["CodeImmeuble"].ToString()) + "'";

                    if (adocnn.DataSource.ToUpper() != "DT-SQL-01")
                    {
                        sServ = ".";
                    }
                    else
                    {
                        sServ = adocnn.DataSource;

                    }

                    rsDt = new DataTable();

                    if (General._ExecutionMode == ExecutionMode.Test)
                        SDArsDt = new SqlDataAdapter(sSQL, "Provider=SQLOLEDB.1;" + "Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=DELOSTAL_CSHARP_TEST;" + "Data Source=" + sServ);
                    else
                        SDArsDt = new SqlDataAdapter(sSQL, "Provider=SQLOLEDB.1;" + "Integrated Security=SSPI;" + "Persist Security Info=False;" + "Initial Catalog=DELOSTAL;" + "Data Source=" + sServ);


                    SDArsDt.Fill(rsDt);

                    if (rsDt.Rows.Count > 0)
                    {
                        rsGdpRow["Ncompte"] = rsDt.Rows[0]["Ncompte"];
                        rsGdpRow["CompteDivers"] = rsDt.Rows[0]["CompteDivers"];
                        modAdorsGdp.Update();
                    }
                }

                rsDt = null;

                modAdorsGdp.Dispose();
                rsGdp = null;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                StdSQLchaine.gFr_DoublerQuote("general;fc_MajCompteTiersGdp");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWord"></param>
        /// <returns></returns>
        public static string fc_FormatCode(string sWord)
        {
            //== Controle si le mot saisie n'a pas de caractere incorrecte, cette fonction
            //== sert en general pour les cles.
            //== retourne une chaine vide si le code est incorrecte.


            int i = 0;
            int j = 0;
            char sChar;
            string returnValue = "";

            sWord = sWord.Trim();
            i = sWord.Length;

            for (i = 0; i < sWord.Length; i++)
            {
                sChar = sWord[i];

                //== contrôle si ce caractére est un espace.
                if (sChar == ' ')
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'utilisation du caractere espace est interdit dans un code.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    returnValue = "";
                    return returnValue;
                }

                //== contrôle si ce caractére respecte la codification en vigueur.
                short tmpChar = fc_Keypress((short)sChar);
                if (tmpChar == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'utilisation du caractere " + sChar + " est interdit dans un code.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    returnValue = "";
                    return returnValue;
                }
                else
                {
                    returnValue += sChar.ToString().ToUpper();
                }


            }

            //for (j = 1; j <= i; j++)
            //{

            //    //== decoupe le mot en caractere.
            //    sChar = Mid(sWord, j, 1);
            //    stmpChar = sChar;

            //    //== contrôle si ce caractére est un espace.
            //    if (sChar == " ")
            //    {
            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'utilisation du caractere espace est interdit dans un code.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        functionReturnValue = "";
            //        return functionReturnValue;
            //    }



            //    //== contrôle si ce caractére respecte la codification en vigueur.
            //    char tmpChar = fc_Keypress((short)sChar[0]).ToString()[0];
            //    sChar = tmpChar.ToString();
            //    if (sChar == "0")
            //    {

            //    }
            //    else
            //    {
            //        stemp = stemp + stmpChar.ToUpper();
            //    }

            //}
            //functionReturnValue = stemp;
            return returnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="ichar"></param>
        /// <returns></returns>
        public static short fc_Keypress(short ichar)
        {
            short functionReturnValue = 0;
            string sChar = null;

            //sChar = ((char)ichar).ToString();

            //ichar = (short)sChar[0];

            sChar = Convert.ToChar(ichar).ToString().ToUpper();
            ichar = (short)sChar[0];
            // de A maujuscule à Z majuscule
            if (ichar >= 65 && ichar <= 90)
            {
                functionReturnValue = ichar;
                // backspace
            }
            else if (ichar == 8)
            {
                functionReturnValue = ichar;
                // "-"
            }
            else if (ichar == 45)
            {
                functionReturnValue = ichar;
            }
            else if (ichar >= 48 && ichar <= 57)
            {
                functionReturnValue = ichar;
            }
            else
            {
                functionReturnValue = 0;
            }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sFiche"></param>
        /// <param name="sOldStatut"></param>
        /// <param name="sStatut"></param>
        /// <param name="sDateRealise"></param>
        /// <param name="sHprevue"></param>
        /// <param name="lCptObli"></param>
        /// <param name="lUrgent"></param>
        /// <param name="sDatePrevue"></param>
        /// <param name="lVisiteEntre"></param>
        public static void fc_HistoStatut(int lNoIntervention, string sCodeImmeuble, string sFiche, string sOldStatut, string sStatut, string sDateRealise = "",
            string sHprevue = "", int lCptObli = 0, int lUrgent = 0, string sDatePrevue = "",
            int lVisiteEntre = 0)
        {
            string sSQL = null;
            DateTime dtNow;

            try
            {
                //=== Ne pas activer, gerer d'bord la mise à jour du statut depûis la fiche dispatch.
                //Exit Sub


                //sSQL = "INSERT INTO HSI_HistoStatutInter(HSI_Date, HSI_ModifiePar, Nointervention," _
                //& " Codeimmeuble, Fiche, HSI_ancienStatut, HSI_NouveauStatut)" _
                //& " VALUES     ('" & Now & "', '" & fncUserName & "'," & lNointervention & " ,'" & gFr_DoublerQuote(sCodeImmeuble) & "'," _
                //& " '" & sFiche & "', '" & sOldStatut & "', '" & sStatut & "')"

                if (IsDate(sHprevue))
                {
                    sHprevue = Convert.ToDateTime(sHprevue).TimeOfDay.ToString("c");
                }
                else
                {
                    sHprevue = "";
                }

                if (IsDate(sDateRealise))
                {
                    sDateRealise = Convert.ToDateTime(sDateRealise).ToString(General.FormatDateSansHeureSQL);
                }
                else
                {
                    sDateRealise = "";
                }

                if (IsDate(sDatePrevue))
                {
                    sDatePrevue = Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSansHeureSQL);
                }
                else
                {
                    sDatePrevue = "";
                }

                dtNow = DateTime.Now;

                sSQL = "INSERT INTO HSI_HistoStatutInter(HSI_Date, HSI_ModifiePar, Nointervention," + " Codeimmeuble, Fiche, HSI_ancienStatut, HSI_NouveauStatut, " +
                    "HSI_DateRealisee,HSI_HeurePrevue, HSI_CompteurObli , HSI_Urgent , HSI_DatePrevue, HSI_VisiteEntretien)" +
                    " VALUES     ('" + DateTime.Now + "', '" + fncUserName() + "'," + lNoIntervention + " ,'" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'," +
                    " '" + sFiche + "', '" + sOldStatut + "', '" + sStatut + "'," + (string.IsNullOrEmpty(sDateRealise) ? "Null" : "'" + sDateRealise + "'") +
                    "," + "" + (string.IsNullOrEmpty(sHprevue) ? "Null" : "'" + sHprevue + "'") + "," + lCptObli + "," + lUrgent + "," +
                    (string.IsNullOrEmpty(sDatePrevue) ? "Null" : "'" + sDatePrevue + "'") + "" + "," + lVisiteEntre + ")";


                int xx = Execute(sSQL);

                sSQL = "UPDATE    Intervention SET  ModifierLe ='" + DateTime.Now.ToString(General.FormatDateSQL) +
                    "', ModifierPar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' "
                    + " WHERE     NoIntervention = " + lNoIntervention;

                xx = Execute(sSQL);

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "general;fc_HistoStatut");
            }
        }

        /// <summary>
        /// ===> Mondir le 23.11.2020, Cette fonction est appelée dans la fiche appel et la fiche Devis
        /// Pour suvgarder le statut de la fiche Devis
        /// </summary>
        /// <param name="lNbLigne"></param>
        /// <param name="sErreur"></param>
        public static void fc_HistoStatutDevis(string NumeroDevis, string NewStatut, string Origin)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_HistoStatutDevis() - Enter In The Function - NumeroDevis = {NumeroDevis} - NewStatut = {NewStatut}");
            //===> Fin Modif Mondir

            try
            {
                using (var tmpModAdo = new ModAdo())
                {
                    var OldStatut = tmpModAdo.fc_ADOlibelle($"SELECT CodeEtat FROM DevisEnTete WHERE NumeroDevis = '{NumeroDevis}'");

                    if (OldStatut.ToUpper() != NewStatut.ToUpper())
                    {
                        //===> Mondir le 25.06.2021 https://groupe-dt.mantishub.io/view.php?id=2508 added Date column
                        var req = $"INSERT INTO DevisHistoStatut VALUES('{NumeroDevis}', '{OldStatut}', '{NewStatut}', '{fncUserName()}', '{Origin}', '{DateTime.Now.ToString(General.FormatDateSQL)}')";
                        Execute(req);
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="chaine"></param>
        /// <param name="limite"></param>
        /// <returns></returns>
        public static string Right(string chaine, int limite)
        {
            if (chaine == null || chaine.Length == 0)
                return "";

            return chaine.Substring(chaine.Length - limite);

        }
        /// <summary>
        /// TODO ==> Dans vb6 nbDemiHeure est converti en CLng et qd on a une valeur par exemple 15.4,cette converstion prend la valeur 15 et si on a 15.6 ,elle ajout 1 et donc on 16
        /// mais qd dans c# convert.ToInt prends just la valeur entiere (les resultats de caluls ne sont pas ideniques )
        /// </summary>
        /// <param name="sDuree"></param>
        /// <param name="sMat"></param>
        /// <param name="dtDate"></param>
        /// <param name="blnDeplacement"></param>
        /// <returns></returns>
        public static string fc_CalcDebourse(System.DateTime sDuree, string sMat, System.DateTime dtDate, bool blnDeplacement = true)
        {
            string functionReturnValue = null;

            double nbDemiHeure = 0;
            double dblTauxHoraire = 0;
            double dblDeplacement = 0;

            if (blnDeplacement == true)//tested
            {
                dblDeplacement = Convert.ToDouble(gfr_liaison("MontantDeplacement", "50"));
            }
            else
            {
                dblDeplacement = 0;
            }

            if (string.IsNullOrEmpty(sGrilleSalaire))
            {
                dblTauxHoraire = Convert.ToDouble(gfr_liaison("TauxHoraire", "26"));

            }
            else if (sGrilleSalaire == "1" && dtDate > cDateCalMo)
            {
                int tmp = 0;
                dblTauxHoraire = ModCalcul.fc_ChargePers(sMat, dtDate.Month, dtDate.Year, ref tmp);
                //=== modif du 27 03 2017, si taux horaire = 0 alors on prends la valeur par défaut.
                if (dblTauxHoraire == 0)
                {
                    dblTauxHoraire = Convert.ToDouble(gfr_liaison("TauxHoraire", "26"));
                }
            }
            else//tested
            {
                dblTauxHoraire = Convert.ToDouble(gfr_liaison("TauxHoraire", "26"));
            }

            //Calcul du temps passé pour en calculer le déboursé
            if ((((sDuree.Hour * 3600) + (sDuree.Minute * 60) + sDuree.Second) % (Convert.ToDouble(sNbrMinuteFact) * 60)) / 60 >= Convert.ToDouble(sNbrMinuteFact))
            {
                nbDemiHeure = ((sDuree.Hour * 3600) + (sDuree.Minute * 60) + sDuree.Second) / (Convert.ToDouble(sNbrMinuteFact) * 60) + 1;
            }
            else
            {
                nbDemiHeure = ((sDuree.Hour * 3600) + (sDuree.Minute * 60) + sDuree.Second) / (Convert.ToDouble(sNbrMinuteFact) * 60);
            }
            nbDemiHeure = Math.Round(nbDemiHeure);
            //Taux horaire moyen : 50 €
            //Déplacement moyen : 50 €

            if (nbDemiHeure < 1 / (Convert.ToDouble(sNbrMinuteFact) / 30))
            {
                nbDemiHeure = (1 / (Convert.ToDouble(sNbrMinuteFact) / 30));
            }


            var d = Convert.ToDouble(nbDemiHeure * (dblTauxHoraire / (60 / Convert.ToInt32(sNbrMinuteFact))) + dblDeplacement);

            int f = 60 / Convert.ToInt32(sNbrMinuteFact);
            double g = dblTauxHoraire / (60 / Convert.ToInt32(sNbrMinuteFact));


            functionReturnValue = Convert.ToString(Math.Round(Convert.ToDouble(nz(d, "0")), 2));
            return functionReturnValue;
        }

        public static string fc_calcDuree(System.DateTime sDEBUT, System.DateTime sFin, int lSansPauseDejeuner = 0)
        {
            string strtempo = null;
            string sDuree = null;
            double sDureeDiff = 0;
            double lngHeure = 0;
            double lngMinute = 0;
            double lngSeconde = 0;
            double dblHeureCout = 0;
            double dblDebourse = 0;
            System.DateTime dtHeureFinMatin = default(System.DateTime);
            System.DateTime dtHeureDebutPM = default(System.DateTime);
            DateTime dtDeb;
            DateTime dtFin;


            dtHeureFinMatin = Convert.ToDateTime(nz(sHeureFinMatin, "12:00:00"));
            dtHeureDebutPM = Convert.ToDateTime(nz(sHeureDebutPM, "13:30:00"));

            //var sDEBUT1 = sDEBUT.TimeOfDay;
            //var sFin1= sFin.TimeOfDay;


            if (sDEBUT > sFin)
            {
                dtDeb = DateTime.Now.Date;
                dtFin = DateTime.Now.AddDays(1).Date;
            }
            else
            {
                dtDeb = DateTime.Now.Date;
                dtFin = DateTime.Now.Date;
            }


            //Calcul de la duree de l'intervention en seconde (sDureeDiff)

            //=== Cas 1 : Coupure entre midi et 2 :
            if (sDEBUT < dtHeureFinMatin && sFin > dtHeureDebutPM && lSansPauseDejeuner == 0)
            {
                sDureeDiff = (dtHeureFinMatin - sDEBUT).TotalSeconds + (sFin - dtHeureDebutPM).TotalSeconds;

                //=== Cas 2 : Pas de coupure, W commencé entre midi et 2
            }
            else if (sDEBUT > dtHeureFinMatin && sDEBUT < dtHeureDebutPM && sFin > Convert.ToDateTime(sHeureDebutPM))
            {
                sDureeDiff = (sFin - sDEBUT).TotalSeconds;

                //=== Cas 3 : Pas de coupure, W fini entre midi et 2
            }
            else if (sDEBUT < dtHeureFinMatin && sFin < dtHeureDebutPM && sFin > Convert.ToDateTime(sHeureFinMatin))
            {
                sDureeDiff = (sFin - sDEBUT).TotalSeconds;

                //=== Cas 4 : Pas de coupure, W le matin ou l'après-midi
            }
            else
            {
                //sDureeDiff = DateDiff("s", dtDeb & " " & Format(sDEBUT, "hh:mm:ss"), dtFin & " " & CDate(sFin)) 
                //Todo modifier cette ligne
                dtDeb = dtDeb.AddHours(sDEBUT.Hour).AddMinutes(sDEBUT.Minute).AddSeconds(sDEBUT.Second);
                dtFin = dtFin.AddHours(sFin.Hour).AddMinutes(sFin.Minute).AddSeconds(sFin.Second);
                sDureeDiff = (dtFin - dtDeb).TotalSeconds;
            }


            if (sDureeDiff < 0)
            {
                sDureeDiff = sDureeDiff * (-1);
            }
            lngHeure = Convert.ToInt32(sDureeDiff / 3600);

            if (lngHeure * 3600 > sDureeDiff)
            {
                lngHeure = lngHeure - 1;
            }

            lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

            if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
            {
                lngMinute = lngMinute - 1;
            }


            lngSeconde = (sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

            if (Convert.ToString(lngHeure).Length == 1)
            {
                sDuree = "0" + lngHeure + ":";
            }
            else
            {
                sDuree = lngHeure + ":";
            }

            if (Convert.ToString(lngMinute).Length == 1)
            {
                sDuree = sDuree + "0" + lngMinute + ":";
            }
            else
            {
                sDuree = sDuree + lngMinute + ":";
            }

            if (Convert.ToString(lngSeconde).Length == 1)
            {
                sDuree = sDuree + "0" + lngSeconde;
            }
            else
            {
                sDuree = sDuree + lngSeconde;
            }


            return Convert.ToDateTime(sDuree).ToString("HH:mm:ss");

        }


        public static int fc_sendFaxmaker(ReportDocument CR, string sReportFileName, string sSelectionFormula, int IndexFormula, string lNumber, string sdest, string sSortFields = "", bool bNotReset = false)
        {
            int functionReturnValue = 0;
            //pour utiliser cette fonction prevoir une formule
            // à placer sur l'état nommée faxmaker et envoyer en parametre
            //le numero du destinataire precedé du libellé FAXMK :
            // exemple cr.formulas(0) = "faxmaker ='FAXMK : 0304556677"
            //(dans cette exemple la formule porte le nom de faxmaker, )
            //NB: ne pas oublier l'espace entre FAXMK ET :

            string sNamePrinter = null;

            string sDriver = null;
            string sport = null;
            string sName = null;
            string Default_Renamed = null;
            string Message = null;
            string title = null;
            string MyValue = null;
            string SelectionFormula = "";

            Message = "Voulez vous envoyer un fax chez " + sdest + " au numéro inscrit ci-dessous ?";

            title = "Envoyer un fax";

            Default_Renamed = lNumber;

            MyValue = Microsoft.VisualBasic.Interaction.InputBox(Message, title, Default_Renamed);
            if (string.IsNullOrEmpty(MyValue))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée", "Envoi de fax", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = 0;
                return functionReturnValue;
            }

            lNumber = MyValue;

            try
            {

                var printerQuery = new ManagementObjectSearcher("SELECT * from Win32_Printer");
                var printers = printerQuery.Get();
                Program.log.Info("Envoi de fax...");
                foreach (var X in printers)
                {
                    //Ahmed: Possible reason for not sending the Pdf was IndexOf of the printer name see https://groupe-dt.mantishub.io/view.php?id=1373
                    //Mondir : Nope thats not right :D, must add .ToLower()
                    if (X.GetPropertyValue("Name").ToString().ToLower().Contains(NAMEFAXMAKER.ToLower()))
                    {
                        sName = X.GetPropertyValue("Name").ToString();
                        sport = X.GetPropertyValue("PortName").ToString();
                        sDriver = X.GetPropertyValue("DriverName").ToString();
                        Program.log.Info($"Fax: {sName} Port: {sport} sDriver={sDriver}");

                        // var isNetworkPrinter = printer.GetPropertyValue("Network");
                        break;
                    }



                }
                if (bNotReset == false)
                {
                    CR = new ReportDocument();
                }
                if (!string.IsNullOrEmpty(sReportFileName))
                {
                    CR.Load(sReportFileName);
                }
                string faxmaker = "'faxmaker='" + "'" + FAXMAKER_KEY + lNumber + "'";
                string destinataire = "'destinataire='" + "'" + FAXMAKER_KEYDEST + sdest + "'";
                CR.DataDefinition.FormulaFields["FaxMaker"].Text = faxmaker;
                CR.DataDefinition.FormulaFields["destinataire"].Text = destinataire;
                //CR.PrinterDriver = sDriver;
                CR.PrintOptions.PrinterName = sName;
                //CR.print.PrinterPort = sport;

                //.Connect = GetSetting(cFrNomApp, "App", "source_odbc")
                //.WindowShowPrintSetupBtn = True

                if (!string.IsNullOrEmpty(sSelectionFormula))
                {
                    CR.RecordSelectionFormula = sSelectionFormula;
                }
                var cc = CR.DataDefinition.SortFields.Count;
                if (!string.IsNullOrEmpty(sSortFields))
                {
                    //TODO : Mondir - Look for this line
                    //CR.set_SortFields(0, sSortFields);
                    var FieldDef1 = CR.Database.Tables["BCD_Detail"].Fields["BCD_NoLigne"];
                    CR.DataDefinition.SortFields[0].Field = FieldDef1;
                    CR.DataDefinition.SortFields[0].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;
                }

                CR.PrintToPrinter(1, true, 0, 0);

                //var c = new CrystalReportFormView(CR, sSelectionFormula);

                //CrystalDecisions.ReportAppServer.Controllers.PrintReportOptions popt = new CrystalDecisions.ReportAppServer.Controllers.PrintReportOptions();
                //popt.PrinterName = sName;
                //popt.NumberOfCopies = 1;
                //popt.Collated = false;
                //popt.AddPrinterPageRange(1, 1);
                //(c.CrystalReportViewer.ReportSource as ReportDocument).ReportClientDocument.PrintOutputController.PrintReport(popt);
                //c.Show();
                functionReturnValue = 1;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fax envoyé. Consulter ultérieurement votre boite de messagerie," + "vous recevrez  un Email indiquant l'état d'envoi du fax.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "module1;fc_sendFaxmaker;");
                functionReturnValue = 0;
                return functionReturnValue;
            }
        }
        public static string fc_ShowDevis(string sValue, string sNomReport = "", string sType = crPosition)
        {
            string functionReturnValue = null;
            //=== modif temporaire, les variables global remplacent
            //=== les controles suite au bug de l'affichage de l'état.

            ////===============> Tested
            if (string.IsNullOrEmpty(sNomReport))
            {
                sCheminEtat = fc_VersionEtatDevis(sValue);
                //    If IsDate(sDateDevisV2) Then
                //            If CDate(sDateDevisV2) <= dtDateCreationDev Then
                //                    sCheminEtat = gsRpt & ETATDtdevisSQLv2
                //            Else
                //                    sCheminEtat = gsRpt & "NewDtDevisSQL.rpt"
                //            End If
                //    Else
                //            sCheminEtat = gsRpt & "NewDtDevisSQL.rpt"
                //    End If
                //frmVisuReport.txtReport = gsRpt & "NewDtDevisSQL.rpt"
            }
            else
            {
                sCheminEtat = sNomReport;
                //frmVisuReport.txtReport = sNomReport
            }

            ///=================> Tested First
            if (sExportPdf == "1")
            {
                functionReturnValue = devModeCrystal.fc_ExportCrystal("{DevisEntete.NumeroDevis}='" + sValue + "'", sCheminEtat, "", "CHAINE", "", "", System.Windows.Forms.CheckState.Checked);
            }
            else
            {

                sNodevis = sValue;
                //frmVisuReport.txtFieldSelection.Text = sValue
                ReportDocument Report = new ReportDocument();
                Report.Load(sCheminEtat);
                Report.SetParameterValue(1, sNodevis);
                Report.SetParameterValue("@NoDevis", sNodevis);
                if (lDevisPzCopieA == 1)
                {
                    Report.SetParameterValue(2, lDEVC_Noauto);
                    Report.SetParameterValue("@DEVC_Noauto", lDEVC_Noauto);
                    lDevisPzCopieA = 0;
                }
                //TODO : Mondir - Check This Commented Lines
                //For i = 0 To UBound(tabParametresDevis) - 1
                //Report.FormulaFields.GetItemByName(tabParametresDevis(i).sName).Text = "'" & tabParametresDevis(i).sValue & "'"
                //Next
                CrystalReportFormView frmVisuReport = new CrystalReportFormView(Report, "");
                frmVisuReport.Show();
                // frmVisuReport.Close();

            }
            return functionReturnValue;

        }


        public static string fc_VersionEtatDevis(string sNodevis)
        {
            string functionReturnValue = null;
            //=== retourne la version de l'état de devis à utiliser.
            string sSQL = null;
            string sCheminEtat = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            try
            {
                //=== modif du 12 Mars 2014, finalement pour gerer les 3 taux de TVA on
                //=== garde l'ancien état. (attention cette portion de code peut servir si
                //=== un format de devis doit être mise en place).
                functionReturnValue = gsRpt + cNewDtDevisSQL;
                return functionReturnValue;

                sCheminEtat = "";

                modAdors = new ModAdo();

                sSQL = "SELECT     CodeEtat, DateCreation" + " From DevisEnTete WHERE    NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "'";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (General.IsDate(sDateDevisV2))
                {
                    if (General.IsDate(rs.Rows[0]["DateCreation"].ToString()))
                    {
                        if (Convert.ToDateTime(sDateDevisV2) <= Convert.ToDateTime(rs.Rows[0]["DateCreation"]))
                        {
                            sCheminEtat = gsRpt + ETATDtdevisSQLv2;
                        }
                        else
                        {
                            sCheminEtat = gsRpt + cNewDtDevisSQL;
                        }
                    }
                    else
                    {
                        sCheminEtat = gsRpt + cNewDtDevisSQL;
                    }
                }
                else
                {
                    sCheminEtat = gsRpt + cNewDtDevisSQL;
                }

                modAdors?.Dispose();

                functionReturnValue = sCheminEtat;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "ModGeneral;fc_VersionEtatDevis");
                return functionReturnValue;
            }
        }

        public static string fncNumDevis(bool bVersion, string sNumeroDevis = "", string sCode = "")
        {
            string functionReturnValue = null;

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;

            string sAnnee = null;
            string sMois = null;
            string sOrdre = null;
            string sLettre = null;
            string sSecteur = null;
            int iStart = 0;

            //nouveau numero de devis
            if (bVersion == false)
            {

                sAnnee = DateTime.Now.ToString("yyyy");
                sMois = DateTime.Now.ToString("MM");

                //recherche du dernier numero de devis pour l annee et le mois
                modAdors = new ModAdo();
                SQL = "SELECT Ordre FROM DevisEntete WHERE (Annee ='" + sAnnee + "') AND (Mois ='" + sMois + "') ORDER BY Ordre DESC";
                rs = modAdors.fc_OpenRecordSet(SQL);
                //sSecteur = Left(fc_ADOlibelle("select SEC_code from immeuble where codeimmeuble= '" & Scode & "'"), 1)
                if (rs.Rows.Count > 0)
                {
                    sOrdre = rs.Rows[0]["ordre"] + "";
                    sOrdre = Convert.ToString(1 + Convert.ToInt32(sOrdre));
                    if (Convert.ToInt32(sOrdre) == Convert.ToDouble("1000"))
                    {
                        sOrdre = fc_NumeroSup1000(sAnnee, sMois);
                    }
                    else
                    {

                        if (sOrdre.Length == 1)
                        {
                            sOrdre = "00" + sOrdre;
                        }
                        if (sOrdre.Length == 2)
                        {
                            sOrdre = "0" + sOrdre;
                        }
                    }
                }
                ///=================> Tested
                else
                {
                    sOrdre = "001";
                }
                modAdors?.Dispose();
                //fncNumDevis = sAnnee & sMois & sSecteur & sOrdre
                functionReturnValue = sAnnee + sMois + sOrdre;
                //----nouveau numero de version
            }
            else
            {

                iStart = sNumeroDevis.Length - 1;
                //detection de version
                if (General.Mid(sNumeroDevis, iStart, 1) == "/")
                {
                    //----ajout d une nouvelle version
                    sLettre = Convert.ToString((char)((int)Convert.ToChar(General.Right(sNumeroDevis, 1)) + 1));
                    functionReturnValue = Left(sNumeroDevis, iStart) + sLettre;
                    return functionReturnValue;
                }
                ///===============> Tested
                else
                {
                    //ajout de la 1ere version
                    functionReturnValue = sNumeroDevis + "/A";
                }

            }
            return functionReturnValue;
        }

        private static string fc_NumeroSup1000(string sAnnee, string sMois)
        {
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sSQL = null;
            int ltemp = 0;
            int i = 0;

            sSQL = "SELECT     Ordre, Mois, Annee " + " From DevisEnTete" + " WHERE   Mois = '" + sMois + "' AND Annee = '" + sAnnee + "' ";

            modAdors = new ModAdo();
            rs = modAdors.fc_OpenRecordSet(sSQL);

            ltemp = 0;
            foreach (DataRow rsRow in rs.Rows)
            {
                i = Convert.ToInt32(nz(rsRow["ordre"], 0));

                if (ltemp < i)
                {
                    ltemp = i;
                }

            }

            rs = null;
            modAdors?.Dispose();
            ltemp = ltemp + 1;
            return Convert.ToString(ltemp);
        }

        public static string fc_ReturnWhere(string sNomChamp, string sTexte, string sLangue = "1", string sTypeDeRecherche = "ESPACE", string sOrAnd = "AND")
        {

            string stemp = null;
            string sT = null;
            string[] tTAb = null;
            string sWhere = null;
            string SansApos = null;
            int i = 0;
            int j = 0;

            string[] sTab = null;

            sT = sTexte.Trim();
            sT = sT.Replace("'", "''");
            stemp = "";

            if (sTypeDeRecherche.ToUpper() == "ESPACE".ToUpper())
            {

                if (sT.Length > 0)
                {

                    for (i = 1; i <= sT.Length; i++)
                    {
                        if (General.Mid(sT, i, 1) == " ")
                        {
                            if (j == 0)
                            {
                                //== enléve les espaces doublées.
                                stemp = stemp + General.Mid(sT, i, 1);
                                j = 1;
                            }
                        }
                        else
                        {
                            j = 0;
                            stemp = stemp + General.Mid(sT, i, 1);
                        }
                    }

                    tTAb = stemp.Split(' ');
                    sWhere = "";

                    for (i = 0; i <= tTAb.Length - 1; i++)
                    {
                        if (i == 0)
                        {
                            sWhere = sNomChamp + " like '%" + tTAb[i].Replace("'", "''") + "%'";

                        }
                        else
                        {

                            if (sOrAnd.ToString().ToUpper() == "OR")
                            {
                                sWhere = sWhere + " OR " + sNomChamp + " like '%" + tTAb[i].Replace("'", "''") + "%'";

                            }
                            else if (sOrAnd.ToString() == "AND")
                            {
                                sWhere = sWhere + " AND " + sNomChamp + " like '%" + tTAb[i].Replace("'", "''") + "%'";

                            }

                        }
                    }

                }

            }
            else
            {
                sTab = sTexte.Split(' ');
                sWhere = "";

                for (i = 0; i <= sTab.Length - 1; i++)
                {
                    if (sTab[i].ToUpper() == "ET")
                    {
                        if (sLangue == "1")
                        {
                            sWhere = sWhere + " AND ";
                        }
                        else
                        {
                            sWhere = sWhere + " ET ";
                        }

                    }
                    else if (sTab[i].ToUpper() == "OU")
                    {
                        if (sLangue == "1")
                        {
                            sWhere = sWhere + " OR ";
                        }
                        else
                        {
                            sWhere = sWhere + " OU ";
                        }
                    }
                    else if (!string.IsNullOrEmpty(sTab[i].Trim()))
                    {
                        sWhere = sWhere + sNomChamp + " LIKE '%" + sTab[i].Replace("'", "''") + "%' ";
                    }



                }
            }

            return sWhere;

        }

        public static string fc_FormatNumber(string sNumber)
        {
            sNumber = sNumber.Replace(" ", "");
            sNumber = sNumber.Replace(",", ".");
            sNumber = sNumber.Replace("\n\r", "");
            sNumber = sNumber.Replace("\n", "");
            sNumber = sNumber.Replace("\r", "");
            sNumber = sNumber.Replace(((char)160).ToString(), "");
            return sNumber;
        }
        /// <summary>
        /// Tested
        /// </summary>
        public static void fc_UnloadAllForms()
        {

            int i = 0;
            var openForms = Application.OpenForms.Cast<Form>().ToList();//cast the openForms property to list<Form> in order to catch an exception 'the collection was modified ...'.

            foreach (Form F in openForms)
                if (!(F is UserDocFichePrincipal))
                    F.Close();

        }

        public static bool fc_InitRegGrilles(string sNomFeuille)
        {
            bool functionReturnValue = false;
            DataTable rsInit = default(DataTable);
            ModAdo modAdorsInit = null;
            string sNoMAJ = "";

            try
            {
                modAdorsInit = new ModAdo();
                sNoMAJ = General.getFrmReg(sNomFeuille, "NoMaj");

                if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                    rsInit = modAdorsInit.fc_OpenRecordSet("SELECT InitRegistre,NoMAJ FROM InitRegitreV2");
                else
                    rsInit = modAdorsInit.fc_OpenRecordSet("SELECT InitRegistre,NoMAJ FROM InitRegitre");

                if (rsInit.Rows.Count > 0)
                {
                    if (Convert.ToInt32(nz(sNoMAJ, "0")) >= Convert.ToInt32(nz(rsInit.Rows[0]["NoMaj"], "0")))
                    {
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                    else
                    {
                        General.saveInReg(sNomFeuille, "NoMAJ", Convert.ToString(rsInit.Rows[0]["NoMaj"] + ""));
                    }
                }
                else
                {
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                modAdorsInit?.Dispose();

                modAdorsInit = new ModAdo();

                rsInit = modAdorsInit.fc_OpenRecordSet("SELECT * FROM InitRegFeuil WHERE NomFeuille='" + sNomFeuille + "'");
                if (rsInit.Rows.Count > 0)
                {
                    if (rsInit.Rows[0]["SupprBaseReg"] + "" == "1")
                    {
                        functionReturnValue = true;
                    }
                    else
                    {
                        functionReturnValue = false;
                    }
                }
                else
                {
                    Execute("INSERT INTO InitRegFeuil (NomFeuille,SupprBaseReg) VALUES ('" + sNomFeuille + "','0')");
                    functionReturnValue = false;
                }

                rsInit?.Dispose();
                modAdorsInit?.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " fc_InitRegGrilles " + sNomFeuille + " ");
                return functionReturnValue;
            }
        }


        public static void fc_InsertContact(string sTypeContact, int lID_contact, string sRef)
        {
            string sSQL = null;
            int lReturn = 0;


            try
            {
                switch (sTypeContact)
                {

                    case cContactSite:
                        sSQL = "INSERT INTO IMC_ImmCorrespondant" + " (CodeCorresp_IMC, CodeImmeuble_IMM)" + " VALUES     (" + lID_contact + ", '" + StdSQLchaine.gFr_DoublerQuote(sRef) + "')";
                        lReturn = General.Execute(sSQL);
                        if (lReturn == 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur inattendue est survenue, l'insertion du contact est annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;

                    case cContactClient:

                        sSQL = "INSERT INTO Gestionnaire" + " (Code1, CodeGestinnaire) " + " VALUES  ('" + StdSQLchaine.gFr_DoublerQuote(sRef) + "', '" + lID_contact + "')";
                        lReturn = General.Execute(sSQL);
                        if (lReturn == 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur inattendue est survenue, l'insertion du contact est annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;

                    case cContactGroupe://tested

                        sSQL = "INSERT INTO ContactGroupe" + " (ID_Contact, CRCL_Code)" + " VALUES  ('" + lID_contact + "', '" + sRef + "')";
                        lReturn = General.Execute(sSQL);
                        if (lReturn == -1)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur inattendue est survenue," + "\n" + " l'insertion du contact est annulée, " + "\n" + "Il existe Déjà.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                        break;
                }
                return;
            }

            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "general;fc_InsertContact");

            }

        }

        public static void RefreshListBox(ListBox LB)
        {
            LB.Items.Clear();
            if (LB.Tag == null || !Directory.Exists(LB.Tag?.ToString()))
                return;
            foreach (string File in Directory.GetFiles(LB.Tag.ToString()))
            {
                if (LB.Tag.ToString()[LB.Tag.ToString().Length - 1] == '\\')
                    LB.Items.Add(File.Replace(LB.Tag.ToString(), ""));
                else
                    LB.Items.Add(File.Replace(LB.Tag.ToString() + "\\", ""));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bool"></param>
        /// <returns></returns>
        public static short ConvertBool(string @bool)
        {
            short functionReturnValue = 0;

            if (@bool.ToUpper() == "FAUX" || @bool.ToUpper() == "FALSE")
            {
                functionReturnValue = 0;
            }
            else if (@bool.ToUpper() == "VRAI" || @bool.ToUpper() == "TRUE")
            {
                functionReturnValue = 1;
            }
            return functionReturnValue;

        }

        public static string fc_ReturnMonthParam(string sMonth, bool bSansAccent = false)
        {
            string functionReturnValue = null;

            //=== retourne le mois en francais à partir d'une date.
            try
            {
                switch (sMonth)
                {

                    case "01":
                    case "1":
                        functionReturnValue = "Janvier";
                        break;
                    case "02":
                    case "2":
                        if (bSansAccent == true)
                        {
                            functionReturnValue = "Fevrier";
                        }
                        else
                        {
                            functionReturnValue = "Février";
                        }
                        break;
                    case "03":
                    case "3":
                        functionReturnValue = "Mars";
                        break;
                    case "04":
                    case "4":
                        functionReturnValue = "Avril";
                        break;
                    case "05":
                    case "5":
                        functionReturnValue = "Mai";
                        break;
                    case "06":
                    case "6":
                        functionReturnValue = "Juin";
                        break;
                    case "07":
                    case "7":
                        functionReturnValue = "Juillet";
                        break;
                    case "08":
                    case "8":
                        functionReturnValue = "Aout";
                        break;
                    case "09":
                    case "9":
                        functionReturnValue = "Septembre";
                        break;
                    case "10":
                        functionReturnValue = "Octobre";
                        break;
                    case "11":
                        functionReturnValue = "Novembre";
                        break;
                    case "12":
                        if (bSansAccent == true)
                        {
                            functionReturnValue = "Decembre";
                        }
                        else
                        {
                            functionReturnValue = "Décembre";
                        }
                        break;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "general;fc_ReturnMonth");
                return functionReturnValue;
            }
        }
        public static int fc_PointVirgule(int KeyAscii)
        {
            int functionReturnValue = 0;
            functionReturnValue = KeyAscii;
            if (KeyAscii == cVirgule)
            {
                functionReturnValue = cPoint;
            }
            if (!General.IsNumeric(KeyAscii.ToString()) && KeyAscii != cPoint && KeyAscii != 8)
            {
                KeyAscii = 0;
                functionReturnValue = KeyAscii;

            }
            return functionReturnValue;

        }
        public static double FncArrondir(double n, int p = 2)
        {  //TODO SALMA : verifier  cette fonction permet d'arrondir une valeur  de type double
            double functionReturn = 0;
            //Ahmed:25-09-2020: Adding third parameter to fixe decimal ceiling issue in UserDocImrimeFacture
            //ex: in Vb Rounding is 619.89 but in c# 619.88
            functionReturn = Math.Round(n, p, MidpointRounding.AwayFromZero);
            return functionReturn;
            /*
            string stemp = null;
            string SA = null;
            string str_Renamed = null;
            string sSep = null;
            string sResult = null;
            string sZero = null;
            object vN = null;
            object sSplit = null;
            int k = 0;
            bool fDecPart = false;
            fDecPart = true;
           
          
            vN = n;
            sSep = Strings.Mid(Convert.ToString(1 / 10), 2, 1);
            sZero = "";
         
            n = Convert.ToDouble(n);
         
            str_Renamed = Convert.ToString(n);
            if (Strings.InStr(str_Renamed, sSep) > 0)
            {
               
                sSplit = Strings.Split(str_Renamed, sSep)[1];
                if (Strings.Len(sSplit) > p)
                {
                    
                    sSplit = Strings.Left(sSplit, p + 1);
                  
                    if (Convert.ToInt32(Strings.Right(sSplit, 1)) >= 5)
                    {
                        
                        SA = Convert.ToString(Convert.ToInt32(Strings.Mid(sSplit, p, 1)) + 1);
                        
                        stemp = Strings.Left(sSplit, p - 1);
                      
                        while ((Strings.Mid(sSplit, p, 1) == "9"))
                        {
                            if (p > 1)
                            {
                                if (p == 2)
                                {
                                    stemp = "";
                                }
                                else
                                {
                                   
                                    stemp = Strings.Left(sSplit, p - 2);
                                }
                               
                                SA = Convert.ToString(Convert.ToInt32(Strings.Mid(sSplit, p - 1, 1)) + 1);
                                sZero = sZero + "0";
                            }
                            else
                            {
                                fDecPart = false;
                                break; // TODO: might not be correct. Was : Exit Do
                            }
                            p = p - 1;
                        }
                        if (fDecPart)
                        {
                            sResult = Convert.ToString(Convert.ToDouble(Strings.Split(str_Renamed, sSep)[0] + sSep + stemp + SA + sZero));
                        }
                        else
                        {
                           
                            sSplit = Strings.Split(str_Renamed, sSep)[0];
                            p = Strings.Len(sSplit);
                           
                            SA = Convert.ToString(Convert.ToInt32(Strings.Mid(sSplit, p, 1)) + 1);
                           
                            stemp = Strings.Left(sSplit, p - 1);
                            sZero = "";
                          
                            while ((Strings.Mid(sSplit, p, 1) == "9") & p > 1)
                            {
                                if (p > 2)
                                {
                                   
                                    stemp = Strings.Left(sSplit, p - 2);
                                }
                                else
                                {
                                    stemp = "";
                                }
                              
                                SA = Convert.ToString(Convert.ToInt32(Strings.Mid(sSplit, p - 1, 1)) + 1);
                                sZero = sZero + "0";
                                p = p - 1;
                            }
                            sResult = Convert.ToString(Convert.ToDouble(stemp + SA + sZero));
                        }
                    }
                    else
                    {
                       
                        sResult = Convert.ToString(Convert.ToDouble(Strings.Split(str_Renamed, sSep)[0] + sSep + Strings.Left(sSplit, p)));
                    }
                }
                else
                {
                  
                    sResult = vN;
                }
            }
            else
            {
                
                sResult = vN;
            }
            return Convert.ToDouble(sResult);*/
        }

        public static object fc_bln(bool blnVraiOuFaux = true, int lSql = 0)
        {
            object functionReturnValue = null;
            if (lSql == 0)
            {
                if (blnVraiOuFaux == true)
                {
                    functionReturnValue = 1;
                }
                else if (blnVraiOuFaux == false)
                {
                    functionReturnValue = 0;
                }
            }
            else
            {
                functionReturnValue = blnVraiOuFaux;
            }
            return functionReturnValue;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dtDate"></param>
        /// <returns></returns>
        public static string fc_ReturnMonth(System.DateTime dtDate)
        {
            string functionReturnValue = null;
            string sMonth = null;

            //=== retourne le mois en francais à partir d'une date.

            try
            {
                sMonth = Convert.ToString(dtDate.Month);
                sMonth = "0" + sMonth;
                switch (sMonth)
                {

                    case "01":
                        functionReturnValue = "Janvier";
                        break;
                    case "02":
                        functionReturnValue = "Février";
                        break;
                    case "03":
                        functionReturnValue = "Mars";
                        break;
                    case "04":
                        functionReturnValue = "Avril";
                        break;
                    case "05":
                        functionReturnValue = "Mai";
                        break;
                    case "06":
                        functionReturnValue = "Juin";
                        break;
                    case "07":
                        functionReturnValue = "Juillet";
                        break;
                    case "08":
                        functionReturnValue = "Aout";
                        break;
                    case "09":
                        functionReturnValue = "Septembre";
                        break;
                    case "10":
                        functionReturnValue = "Octobre";
                        break;
                    case "11":
                        functionReturnValue = "Novembre";
                        break;
                    case "12":
                        functionReturnValue = "Décembre";
                        break;

                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "general;fc_ReturnMonth");
                return functionReturnValue;
            }
        }

        public static object fc_VerrifAchatInterv(string sNoIntervention, string sNoBCmd)
        {
            object functionReturnValue = null;
            DataTable rsVerrifAchat = default(DataTable);
            ModAdo modAdorsVerrifAchat = null;

            try
            {
                modAdorsVerrifAchat = new ModAdo();

                rsVerrifAchat = modAdorsVerrifAchat.fc_OpenRecordSet("SELECT TypeCodeEtat.NoAchat,Intervention.NumFicheStandard FROM TypeCodeEtat " +
                    " INNER JOIN Intervention ON TypeCodeEtat.CodeEtat=Intervention.CodeEtat" + " WHERE Intervention.NoIntervention=" + sNoIntervention);

                if (rsVerrifAchat.Rows.Count > 0)
                {

                    if ((nz(rsVerrifAchat.Rows[0]["NoAchat"], "0")).ToString() == "1")
                    {
                        if (ModCourrier.InitialiseEtOuvreOutlook(false) == true)
                        {

                            if (adocnn.Database.ToUpper() == Variable.cDelostal.ToUpper())
                            {

                                ModCourrier.CreateMail(gfr_liaison("AdresseResponsable", "lmg@delostaletthibault.fr"), "LF@delostaletthibault.Fr;", "Achat non autorisé (Commande N° " + sNoBCmd + " )", "Appel N° : " + rsVerrifAchat.Rows[0]["Numfichestandard"] + " Intervention N° : " + sNoIntervention);

                            }
                            else if (adocnn.Database.ToUpper() == Variable.cAmmann.ToUpper())
                            {
                                ModCourrier.CreateMail(gfr_liaison("AdresseResponsable", "lmg@delostaletthibault.fr"), "", "Achat non autorisé (Commande N° " + sNoBCmd + " )", "Appel N° : " + rsVerrifAchat.Rows[0]["Numfichestandard"] + " Intervention N° : " + sNoIntervention);
                            }


                        }
                    }

                }
                modAdorsVerrifAchat.Close();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " fc_VerrifAchatInterv (NoIntervention : " + sNoIntervention + " , NoBonDeCommande : " + sNoBCmd + ") ");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static int Len(string str)
        {
            return str.Length;
        }
        public static System.DateTime CalcEch(System.DateTime dDate, string sCodeReg)
        {
            DataTable rs = default(DataTable);
            System.DateTime DatEch = default(System.DateTime);
            int iNbJour = 0;
            int iJour = 0;

            rs = new DataTable();
            var tmpAdors = new ModAdo();
            rs = tmpAdors.fc_OpenRecordSet("select * from codereglement " + " where code='" + sCodeReg + "'");

            DatEch = dDate.AddDays(Convert.ToDouble(nz(rs.Rows[0]["nombrejour"], 0)));

            if (rs.Rows[0]["typeecheance"].ToString() == "L")//Tested
            {

                iJour = Convert.ToInt16(nz(rs.Rows[0]["echeance"], 0));
                if (DatEch.Day > iJour)//tESTED
                {
                    DatEch = DatEch.AddMonths(1).AddDays(1 - DatEch.Day);
                }

                iNbJour = DatEch.AddMonths(1).AddDays(-(DatEch.AddMonths(1)).Day).Day;

                if (iNbJour < iJour)
                {
                    iJour = iNbJour;
                }

                DatEch = DatEch.AddDays(iJour - DatEch.Day);

            }
            else if (rs.Rows[0]["typeecheance"].ToString() == "F")//Tested
            {
                DatEch = DatEch.AddMonths(1).AddDays(-(DatEch.AddMonths(1)).Day);
                DatEch = DatEch.AddDays(Convert.ToDouble(nz(rs.Rows[0]["echeance"], 0)));


            }

            return DatEch;

        }

        public static object fc_VerrifAchatDevis(string sNoIntervention, string sNoBCmd)
        {
            object functionReturnValue = null;
            double dblCoutAchat = 0;
            double dblCoutDevis = 0;
            string strNodevis = null;

            try
            {
                using (var tmpModAdo = new ModAdo())
                    strNodevis = tmpModAdo.fc_ADOlibelle("SELECT GestionStandard.NoDevis FROM GestionStandard " + " INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard " +
                        " WHERE Intervention.NoIntervention=" + sNoIntervention);
                if (!string.IsNullOrEmpty(strNodevis))
                {
                    using (var tmpModAdo = new ModAdo())
                    {//=============>Tested
                        dblCoutAchat = Convert.ToDouble(nz(tmpModAdo.fc_ADOlibelle("SELECT SUM(BCD_PrixHT * BCD_Quantite) as [TotalBCMD] " +
                        " FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande" + " WHERE NOT BCD_PrixHT IS NULL AND NOT BCD_PrixHT='' " +
                        " AND NOT BCD_Quantite IS NULL AND NOT BCD_Quantite='' " + " AND BonDeCommande.NoIntervention=" + nz(sNoIntervention, "0")), "0"));
                        //Foud,STud,Quantite

                        dblCoutDevis = Convert.ToDouble(nz(tmpModAdo.fc_ADOlibelle("SELECT SUM(Foud * Quantite) as TotalFO " + " FROM DevisDetail " +
                            " WHERE NOT Foud IS NULL AND NOT Quantite IS NULL AND NumeroDevis='" + strNodevis + "'"), "0"));

                        dblCoutDevis = dblCoutDevis + Convert.ToDouble(nz(tmpModAdo.fc_ADOlibelle("SELECT SUM(Stud * Quantite) as TotalFO " + " FROM DevisDetail " +
                            " WHERE NOT Stud IS NULL AND NOT Quantite IS NULL AND NumeroDevis='" + strNodevis + "'"), "0"));
                    }
                    //If dblCoutAchat > (dblCoutDevis + CDbl(gfr_liaison("MargeSupDevis", "30"))) Then
                    if (dblCoutAchat > (dblCoutDevis + Convert.ToDouble(gfr_liaison("MargeSupDevis", "30"))))
                    {
                        if (ModCourrier.InitialiseEtOuvreOutlook(false) == true)
                        {

                            if (adocnn.Database.ToUpper() == Variable.cDelostal.ToUpper())
                            {

                                ModCourrier.CreateMail(gfr_liaison("AdresseResponsable", "lmg@delostaletthibault.fr"), "LF@delostaletthibault.Fr;", "Montant des achats supèrieur au devis (Commande N° " + sNoBCmd + " )", "Devis N° : " + strNodevis + " Intervention N° : " + sNoIntervention + "\n" + "Montant total des fournitures du devis : " + dblCoutDevis + " €, montant total des achats sur ce devis : " + dblCoutAchat + " €");

                            }
                            else if (adocnn.Database.ToUpper() == Variable.cAmmann.ToUpper())
                            {

                                ModCourrier.CreateMail(gfr_liaison("AdresseResponsable", "lmg@delostaletthibault.fr"), "", "Montant des achats supèrieur au devis (Commande N° " + sNoBCmd + " )", "Devis N° : " + strNodevis + " Intervention N° : " + sNoIntervention + "\n" + "Montant total des fournitures du devis : " + dblCoutDevis + " €, montant total des achats sur ce devis : " + dblCoutAchat + " €");

                            }

                        }
                    }

                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " fc_VerrifAchatDevis (NoIntervention : " + sNoIntervention + " , NoBonDeCommande : " + sNoBCmd + ") ");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="tsNoBonDeCommande"></param>
        public static void fc_FindFactFournRD(string[] tsNoBonDeCommande)
        {
            //=== recherche les factures scannées par le module ReadSoft.
            string sSQL = null;
            string sWhere = null;
            int i = 0;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            int j = 0;

            try
            {
                i = tsNoBonDeCommande.Length;

                for (i = 0; i <= tsNoBonDeCommande.Length - 1; i++)
                {
                    if (!string.IsNullOrEmpty(tsNoBonDeCommande[i]))
                    {
                        if (string.IsNullOrEmpty(sWhere))//tested
                        {
                            sWhere = " WHERE NoBCmd =" + tsNoBonDeCommande[i];
                        }
                        else
                        {
                            sWhere = sWhere + " OR NoBCmd =" + tsNoBonDeCommande[i];
                        }
                    }
                }

                sSQL = "SELECT CheminFichier, NoBCmd From FactFournPied " + sWhere;

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                frmAfficheFichier frmAfficheFichier = new frmAfficheFichier();

                frmAfficheFichier.LstFichier.Items.Clear();
                j = 0;
                foreach (DataRow rsRow in rs.Rows)
                {
                    if (!string.IsNullOrEmpty(nz(rsRow["CheminFichier"], "").ToString()))
                    {
                        frmAfficheFichier.LstFichier.Items.Add(rsRow["CheminFichier"].ToString());
                        j = j + 1;
                    }
                }

                modAdors.Close();

                frmAfficheFichier.lblTexte.Text = "Le système à trouvé " + j + " facture(s) scannée(s)." + "\n" + "Veuillez double cliquer sur un des fichier pour la visualiser.";
                frmAfficheFichier.ShowDialog();
                frmAfficheFichier.Close();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le système n'a pas trouvé de factures " + " fournisseurs scannées pour ce(s) bon(s) de commande ");
            }



        }

        public static string Replace(object str, object s, object r)
        {
            return str.ToString().Replace(s.ToString(), r.ToString());
        }

        public static string Trim(object str)
        {
            return str.ToString().Trim();
        }
        public static string UCase(object str)
        {
            return str == null ? "" : str.ToString().ToUpper();
        }

        public static void fc_Apercu(string sFile, string sSelectionFormula, bool bApercu = true, bool bShowBtnPrint = true)
        {

            short mainjob = 0;
            short result = 0;
            short jobnum = 0;
            short textLength = 0;
            int textHandle = 0;
            string SelectionText = null;
            CrystalReportFormView Form = null;


            try
            {
                SelectionText = sSelectionFormula;

                if (bApercu == true)
                {

                    //== Met les options à jours.
                    ReportDocument Report = new ReportDocument();
                    Report.Load(sFile);
                    Form = new CrystalReportFormView(Report, SelectionText);
                    Form.CrystalReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                    Form.CrystalReportViewer.ShowPageNavigateButtons = true;
                    Form.CrystalReportViewer.EnableDrillDown = false;
                    Form.CrystalReportViewer.ShowCloseButton = false;
                    Form.CrystalReportViewer.ShowPrintButton = bShowBtnPrint;
                    Form.CrystalReportViewer.ShowExportButton = true;
                    Form.CrystalReportViewer.ShowZoomButton = true;
                    Form.CrystalReportViewer.ShowTextSearchButton = false;
                    Form.CrystalReportViewer.ShowRefreshButton = false;
                    Form.Show();

                }

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible de charger l'état," + " veuillez contacter votre administrateur systeme.", "Serious Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            /*   
             *   Code de Mondir =====> Consulter le code du VB6
             *   tu as initiliser la variable result par zero donc on va tjr avoir
             *   le message affiché
             *   if (result == 0)
               {
                   Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible de charger l'état," + " veuillez contacter votre administrateur systeme.", "Serious Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                   return;
               }

               SelectionText = sSelectionFormula;

               if (bApercu == true)
               {

                   //== Met les options à jours.
                   ReportDocument Report = new ReportDocument();
                   Report.Load(sFile);
                   Form = new CrystalReportFormView(Report, SelectionText);
                   Form.CrystalReportViewer.ToolPanelView = CrystalDecisions.Windows.Forms.ToolPanelViewType.None;
                   Form.CrystalReportViewer.ShowPageNavigateButtons = true;
                   Form.CrystalReportViewer.EnableDrillDown = false;
                   Form.CrystalReportViewer.ShowCloseButton = false;
                   Form.CrystalReportViewer.ShowPrintButton = bShowBtnPrint;
                   Form.CrystalReportViewer.ShowExportButton = true;
                   Form.CrystalReportViewer.ShowZoomButton = true;
                   Form.CrystalReportViewer.ShowTextSearchButton = false;
                   Form.CrystalReportViewer.ShowRefreshButton = false;
                   Form.Show();

               }
               else
               {


               }*/
        }


        public static void ReinitPage(UserControl oPage)
        {
            System.Windows.Forms.Control oEl = null;
            System.Windows.Forms.ComboBox oCb = null;

            DataTable oRs = default(DataTable);

            foreach (Control oEl_loopVariable in oPage.Controls)
            {
                oEl = oEl_loopVariable;

                if (oEl is UltraCombo || oEl is System.Windows.Forms.ComboBox ||
                    oEl is System.Windows.Forms.TextBox ||
                    oEl is iTalk.iTalk_TextBox_Small2 ||
                    oEl is iTalk.iTalk_RichTextBox ||
                    oEl is System.Windows.Forms.Label &&
                    General.Left(oEl.Name, 3) == "txt")
                {

                    oEl.Text = "";

                }
                else if (oEl is System.Windows.Forms.ComboBox)
                {

                    oEl.Text = "";

                }
                else if (oEl is UltraGrid)
                {
                    var grid = oEl as UltraGrid;
                    grid.DataSource = null;
                }

            }

        }
        public static string fc_racineGerant(string sRacineGerant)
        {
            try
            {
                if (string.IsNullOrEmpty(sRacineGerant))
                    return "";
                sRacineGerant = StdSQLchaine.gFr_DoublerQuote(sRacineGerant);
                sRacineGerant = Replace(sRacineGerant, "*", "%");
                if (Right(sRacineGerant, 1) != "%")
                    sRacineGerant = sRacineGerant + "%";
                return sRacineGerant;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "general;fc_racineGerant");
                return "";
            }
        }
        public static string fc_Fdate(object sDateE, int lregistre = 0, int lCountry = 1, int lDelemiteur = 1)
        {
            string sDel = "", sformat = "", sdate;
            sdate = sDateE.ToString();
            if (!IsDate(sdate))
                return "";

            DateTime.TryParse(sdate, out var ConvertedDate);
            if (lregistre == 1)
            {
                sDel = gfr_liaison("sDelimiteurDate");
                sformat = gfr_liaison("sformatdate").ToUpper();
            }
            else
            {
                if (lCountry == 1)
                    sformat = "FRANCAIS";
                else if (lCountry == 2)
                    sformat = "ANGLAIS";
                if (lDelemiteur == 1)
                    sDel = "'";
                else if (lDelemiteur == 2)
                    sDel = "#";
            }
            switch (sformat)
            {
                case "ANGLAIS":
                    sdate = String.Format("{0:MM/dd/yyyy}", ConvertedDate);  // "03/09/08"

                    break;
                case "FRANCAIS":
                    sdate = String.Format("{0:dd/MM/yyyy}", ConvertedDate);  // "03/09/08"

                    break;
            }
            sdate = sDel + sdate + sDel;
            // Modification Ahmed  Removing Time from Date
            //ToShortDateString
            return sdate;
        }
        public static object nz0(object vPremier, object vSecond)
        {
            return vPremier == null || vPremier.ToString() == "" || vPremier.ToString() == "0" ? vSecond : vPremier;
        }

        public static void fc_FindFactFourn(string[] tsNoBonDeCommande, object ohwnd)
        {
            string sCheminFichier = null;
            int NbOccurence = 0;
            string Repertoire = null;
            string Masque = null;
            Dossier.ListeFichier ResultatRecherche = default(Dossier.ListeFichier);
            int i = 0;
            string[] sListeFichier = null;
            int j = 0;
            int X = 0;
            //---on paramètre le répertoire à vérifier
            Repertoire = ScanFourn;
            //---on test de bien avoir un "\" a la fin du chemin
            if (General.Right(Repertoire, 1) != "\\")
                Repertoire = Repertoire + "\\";
            //---le masque sert à filtrer les fichiers à rechercher
            //---exemple "*.doc" , "*.exe" ...
            Masque = "*.*";

            //---on récupère le nombre de fichiers trouvés
            NbOccurence = Dossier.Rechercher(Repertoire, Masque, ref ResultatRecherche);
            j = 0;
            sListeFichier = null;
            for (i = 1; i <= NbOccurence; i++)//TESTED
            {
                for (X = 0; X < tsNoBonDeCommande.Length; X++)//TESTED
                {
                    if (ResultatRecherche.Fichiers[i].cFileName.Contains(tsNoBonDeCommande[X]))
                    {
                        Array.Resize(ref sListeFichier, j + 1);
                        sListeFichier[j] = Trim(ResultatRecherche.Fichiers[i].cFileName);
                        j = j + 1;
                    }
                }
            }
            if (j == 0)//TESTED
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le système n'a pas trouvé de factures " + " fournisseurs scannées pour ce(s) bon(s) de commande ");
                //n°" & sNoBonDeCommande
                return;
            }

            //---on parcours tous les résultats trouvés
            //---et on affiche le nom du fichier dans la listbox
            if (j == 1)
            {
                ModuleAPI.Ouvrir(Repertoire + sListeFichier[0]);
            }
            else
            {
                frmAfficheFichier f = new frmAfficheFichier();
                f.LstFichier.Items.Clear();
                for (i = 0; i <= j - 1; i++)
                {
                    f.LstFichier.Items.Add(sListeFichier[i]);
                }

                f.txtCheminFichier.Text = Repertoire;
                f.lblTexte.Text = "Le système à trouvé " + j + " factures scannées. Veuillez double cliquer sur un des fichier pour la visualiser.";
                f.ShowDialog();
                f.Close();

            }
        }
        public static System.DateTime GetDateDebutExercice()
        {
            System.DateTime functionReturnValue = default(System.DateTime);
            string DateMini = null;
            string sMoisFinExercice = null;

            try
            {
                sMoisFinExercice = nz(gfr_liaison("MoisFinExercice"), "12").ToString();

                if (sMoisFinExercice == "12" || !IsNumeric(sMoisFinExercice))
                {
                    DateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Today.Year);
                }
                else
                {
                    if (DateTime.Today.Month >= 1 && DateTime.Today.Month <= Convert.ToInt16(sMoisFinExercice))
                    {
                        DateMini = "01/" + Convert.ToInt32(Convert.ToInt32(sMoisFinExercice) + 1).ToString("00")
                        + "/" + (DateTime.Today.Year - 1).ToString();
                        //MoisFinExercice + 1
                    }
                    else//tested
                    {
                        DateMini = "01/" + Convert.ToInt32(Convert.ToInt32(sMoisFinExercice) + 1).ToString("00")
                        + "/" + (DateTime.Today.Year).ToString();
                        //MoisFinExercice - 1
                    }
                }

                functionReturnValue = Convert.ToDateTime(DateMini);
                return functionReturnValue;

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, "Module Général ; GetDateDebutExercice");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="tsIntervention"></param>
        public static void fc_FindDevisPDA(string[] tsIntervention)
        {
            //=== recherche les factures scannées par le module ReadSoft.
            string sSQL = null;
            string sWhere = null;
            string sCheminFichier = null;
            int i = 0;
            DataTable rs = default(DataTable);
            int j = 0;

            i = tsIntervention.Length;

            try
            {
                for (i = 0; i <= tsIntervention.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(sWhere))//tested
                    {
                        sWhere = " WHERE Nointervention =" + tsIntervention[i];
                    }
                    else
                    {
                        sWhere = sWhere + " OR Nointervention =" + tsIntervention[i];
                    }

                }

                sSQL = "SELECT chemin FROM InterventionDoc " + sWhere + " and typefichier ='devis travaux'";

                var tmpAdo = new ModAdo();
                rs = tmpAdo.fc_OpenRecordSet(sSQL);
                frmAfficheFichier frmAfficheFichier = new frmAfficheFichier();
                frmAfficheFichier.LstFichier.Items.Clear();

                j = 0;
                foreach (DataRow r in rs.Rows)//tested
                {

                    if (!string.IsNullOrEmpty(nz(r["Chemin"], "").ToString()))
                    {
                        sCheminFichier = DossierIntervention + r["Chemin"].ToString();
                        frmAfficheFichier.LstFichier.Items.Add(sCheminFichier);
                        j = j + 1;
                    }
                    // rs.MoveNext();
                }



                rs.Dispose();
                rs = null;

                frmAfficheFichier.lblTexte.Text = "Le système à trouvé " + j + " fichier(s)." + "\n" + "Veuillez double cliquer sur un des fichier pour la visualiser.";
                frmAfficheFichier.ShowDialog();
                frmAfficheFichier.Close();

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le système n'a pas trouvé de devis transféré par le PDA pour ces interventions.");
                return;
            }

        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        public static void ListDirectory(TreeView treeView, string path)
        {
            try
            {
                treeView.Nodes.Clear();
                treeView.SuspendLayout();

                //var stack = new Stack<TreeNode>();
                //var rootDirectory = new DirectoryInfo(path);
                //var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
                //stack.Push(node);

                //while (stack.Count > 0)
                //{
                //    var currentNode = stack.Pop();
                //    currentNode.EnsureVisible();
                //    var directoryInfo = (DirectoryInfo)currentNode.Tag;
                //    if(directoryInfo.Exists)
                //    foreach (var directory in directoryInfo.GetDirectories())
                //    {
                //        var childDirectoryNode = new TreeNode(directory.Name) { Tag = directory.FullName };
                //        childDirectoryNode.ImageIndex = 1;
                //        childDirectoryNode.SelectedImageIndex = 0;
                //        currentNode.Nodes.Add(childDirectoryNode);
                //        // stack.Push(childDirectoryNode);
                //    }

                //}

                //treeView.Nodes.Add(node);

                //treeView.ExpandAll();
                //treeView.ResumeLayout();
                //foreach (TreeNode TN in treeView.Nodes)
                //    ListSubFolders(TN);


                if (Directory.Exists(path))
                {
                    var rootDirectory = new DirectoryInfo(path);
                    if (rootDirectory.GetDirectories().Length > 0)
                        foreach (var Dir in rootDirectory.GetDirectories())
                        {
                            var node = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            node.ImageIndex = 1;
                            node.SelectedImageIndex = 0;
                            treeView.Nodes.Add(node);
                        }

                    treeView.ExpandAll();
                    treeView.ResumeLayout();
                    foreach (TreeNode TN in treeView.Nodes)
                        ListSubFolders(TN);
                }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
                //Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="chemin"></param>
        /// <param name="parent"></param>
        public static void chargerNodes(string chemin, UltraTreeNode parent)
        {
            try
            {
                if (Directory.Exists(chemin) && !chemin.Contains("$Recycle.Bin"))
                {
                    var FileInfo = new DirectoryInfo(chemin);
                    var directories = FileInfo.GetDirectories();
                    string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.jpg;.png;.jpeg;.pdf;.msg;.docx;.accdb;.bmp;.contact;.pptx;.pub;.xlsx;.ppt;.mp3;.mp4;.wav.;.amr";
                    var extensionsPattern = pattern.Split(';');

                    if (directories.Length > 0 && parent.Nodes.Count == 0)
                    {
                        foreach (var directory in directories)
                        {
                            try
                            {
                                if (!directory.Name.Contains("$Recycle.Bin"))
                                {
                                    int filesCount = directory.GetFiles().Where(d => extensionsPattern.Contains(d.Extension)).Count();

                                    int DirectoriesCount = directory.GetDirectories().Length;

                                    string text = directory.Name + " ( " + DirectoriesCount + " dossiers / " + filesCount + " fichiers )";

                                    UltraTreeNode node = new UltraTreeNode(directory.FullName, text);

                                    node.Tag = directory.FullName;

                                    if (directory.GetDirectories().Length > 0)
                                    {
                                        node.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                                    }
                                    else
                                        node.Override.NodeAppearance.Image = Properties.Resources.close_folder;

                                    parent.Nodes.Add(node);
                                }
                            }
                            catch (Exception ex)
                            {
                                Program.SaveException(ex);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="chemin"></param>
        /// <param name="listBox1"></param>
        public static void chargerFiles(string chemin, ListBox listBox1)
        {
            try
            {
                if (Directory.Exists(chemin))
                {
                    var FileInfo = new DirectoryInfo(chemin);
                    //string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.png;.jpg;.jpeg;.pdf;.msg;.docx;.amr;.mp3;.mp4";
                    //string pattern = ".exe;.dll;.sys;.ocx;.oca;.chi;.propdesc;.ins;.dat;.mshc;.cab;.targets;.xml;.prg;.dbf;.app;.rif;.fon;.bmp;.msk;.mem";
                    string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.jpg;.png;.jpeg;.pdf;.msg;.docx;.accdb;.bmp;.contact;.pptx;.pub;.xlsx;.ppt;.mp3;.mp4;.wav.;.amr";
                    var extensionsPatternLowerCase = pattern.Split(';');
                    var files = FileInfo.GetFiles().Where(d => extensionsPatternLowerCase.Contains(d.Extension.ToLower()));
                    //var files = FileInfo.GetFiles();
                    listBox1.Items.Clear();

                    if (files.Count() > 0)
                    {
                        foreach (var file in files)
                        {
                            listBox1.Items.Add(file.Name);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="TN"></param>
        public static void ListSubFolders(TreeNode TN)
        {
            DirectoryInfo DI = new DirectoryInfo(TN.Tag.ToString());
            if (!TN.Tag.ToString().Contains("$Recycle.Bin") && DI.Exists)
                try
                {
                    if (DI.GetDirectories().Length > 0)
                    {
                        foreach (var Dir in DI.GetDirectories())
                        {
                            var childDirectoryNode = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            childDirectoryNode.ImageIndex = 1;
                            childDirectoryNode.SelectedImageIndex = 0;
                            TN.Nodes.Add(childDirectoryNode);
                            ListSubFolders(childDirectoryNode);
                        }
                    }
                }
                catch { }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="directoryInfo"></param>
        public static void FilesInDirectory(ListBox listBox, DirectoryInfo directoryInfo)
        {
            try
            {
                listBox.Items.Clear();
                if (directoryInfo.Exists)
                    foreach (var file in directoryInfo.GetFiles())
                        listBox.Items.Add(file.Name);
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private static object fc_StockODBC_BaseTestReport()
        {
            string strRegistre;
            int sNoFile;
            string strUserName;
            string sNomServeur;
            object functionReturnValue = null;
            try
            {
                sNomServeur = DefaultServer;
                strUserName = fncUserName();
                strRegistre = "Windows Registry Editor Version 5.00" + "\n";
                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI]" + "\n";
                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\ODBC Data Sources]" + "\n";

                strRegistre = strRegistre + "\"DELOSTALODBCREPORT\"=\"SQL Server\"" + "\n";
                strRegistre = strRegistre + "\"DTDELOSTALREPORT\"=\"SQL Server\"" + "\n";
                //'strRegistre = strRegistre & """DTDELOSTAL""=""SQL Server""" & vbCrLf



                //'=== DELOSTAL.
                //'    strRegistre = strRegistre & vbCrLf
                //'    strRegistre = strRegistre & "[HKEY_CURRENT_USER\Software\ODBC\ODBC.INI\DTDELOSTAL]" & vbCrLf
                //'    strRegistre = strRegistre & """Driver""=""C:\\WINDOWS\\System32\\Sqlsrv32.dll""" & vbCrLf
                //'    strRegistre = strRegistre & """Server""=""" & sNomServeur & """" & vbCrLf
                //'    strRegistre = strRegistre & """Database""=""AXECIEL_DELOSTAL""" & vbCrLf
                //'    strRegistre = strRegistre & """LastUser""=""" & strUserName & """" & vbCrLf
                //'    strRegistre = strRegistre & """Trusted_Connection""=""Yes""" & vbCrLf

                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\DELOSTALODBCREPORT]" + "\n";
                strRegistre = strRegistre + "\"Driver\"=\"C:\\\\WINDOWS\\\\System32\\\\Sqlsrv32.dll\"" + "\n";
                strRegistre = strRegistre + "\"Server\"=\"" + sNomServeur + "\"" + "\n";
                strRegistre = strRegistre + "\"Database\"=\"CPTA_DELOSTAL_REPORT\"" + "\n";
                strRegistre = strRegistre + "\"LastUser\"=\"" + strUserName + "\"" + "\n";
                strRegistre = strRegistre + "\"Trusted_Connection\"=\"Yes\"" + "\n";
                strRegistre = strRegistre + "\n";

                strRegistre = strRegistre + "\n";
                strRegistre = strRegistre + "[HKEY_CURRENT_USER\\Software\\ODBC\\ODBC.INI\\DTDELOSTALREPORT]" + "\n";
                strRegistre = strRegistre + "\"Driver\"=\"C:\\\\WINDOWS\\\\System32\\\\Sqlsrv32.dll\"" + "\n";
                strRegistre = strRegistre + "\"Server\"=\"" + sNomServeur + "\"" + "\n";
                strRegistre = strRegistre + "\"Database\"=\"AXECIEL_DELOSTAL_REPORT\"" + "\n";
                strRegistre = strRegistre + "\"LastUser\"=\"" + strUserName + "\"" + "\n";
                strRegistre = strRegistre + "\"Trusted_Connection\"=\"Yes\"" + "\n";
                strRegistre = strRegistre + "\n";


                if (Dossier.fc_ControleDossier("C:\\temp") == false)
                {
                    Dossier.fc_CreateDossier("C:\\temp");
                }
                Dossier.fc_WriteInFile("C:\\temp\\SysGicODBC.reg", strRegistre);

                var ProcessInfo = new ProcessStartInfo();
                ProcessInfo.FileName = "regedit -s C:\\temp\\SysGicODBC.reg";
                ProcessInfo.RedirectStandardOutput = true;
                ProcessInfo.RedirectStandardError = true;
                ProcessInfo.UseShellExecute = false;
                ProcessInfo.CreateNoWindow = true;
                var Process = new Process();
                Process.StartInfo = ProcessInfo;
                Process.EnableRaisingEvents = true;
                Process.Start();
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Multi Société", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            return functionReturnValue;
        }
        public static string gfr_liaisonV2(string sLiaison, string sValeurDefaut = "", bool blnMasquer = false, string aCode = "")
        {
            string sAdresse;

            if (lngInitRegitre == 1)

                saveInReg("App", sLiaison, "");
            //SaveSetting(cFrNomApp, "App", sLiaison, "");
            sAdresse = getFrmReg("App", sLiaison, "");
            //sAdresse = GetSetting(cFrNomApp, "App", sLiaison, "");
            if (sAdresse == "")
            {

                sAdresse = rechLiaisonV2(sLiaison, sValeurDefaut, blnMasquer, aCode);
                saveInReg("App", sLiaison, sLiaison);
                //SaveSetting(cFrNomApp, "App", sLiaison, sLiaison);
            }
            return sAdresse;
        }
        private static string rechLiaisonV2(string sCritere, string sAdresseDefaut = "", bool blnMasquer = false, string aCode = "")
        {
            DataTable rsLiaison = new DataTable();
            var ModAdorsLiaison = new ModAdo();
            if (rsLiaison != null)
            {
                //if (rsLiaison.State == adStateOpen)
                rsLiaison.Clear();
            }
            else
                rsLiaison = new DataTable();
            if (General._ExecutionMode == ExecutionMode.Prod || _ExecutionMode == ExecutionMode.Test)
                rsLiaison = ModAdorsLiaison.fc_OpenRecordSet("SELECT Adresse FROM lienV2 WHERE code = '" + sCritere + "' AND CodeUO = '" + strCodeUO + "'");
            else
                rsLiaison = ModAdorsLiaison.fc_OpenRecordSet("SELECT Adresse FROM lien WHERE code = '" + sCritere + "' AND CodeUO = '" + strCodeUO + "'");
            if (rsLiaison.Rows.Count > 0)
            {
                if (rsLiaison.Rows[0]["Adresse"] + "" == "")
                {
                    Execute("UPDATE Lien SET Adresse='" + sAdresseDefaut + "',Masquer=" + (blnMasquer == true ? 1 : 0) + " WHERE Code = '" + sCritere + "' AND CodeUO='" + strCodeUO + "'");
                    return sAdresseDefaut;
                }
                else
                    return rsLiaison.Rows[0]["Adresse"] + "";
            }
            else
            {
                Execute("INSERT INTO Lien (aCode, CodeUO, Code, Adresse, Masquer) VALUES ('" + aCode + "','" + strCodeUO + "','" + sCritere + "','" + sAdresseDefaut + "'," + (blnMasquer == true ? 1 : 0) + ")");
                return sAdresseDefaut;
            }

            rsLiaison.Clear();

        }
        public static string[] Split(string str, string str_spliter)
        {
            return str.Replace(str_spliter, ";").Split(';');//first way
            //return str.Split(new string[] {str_spliter}, StringSplitOptions.None);//second way
        }
        public static void fc_CleanForm(Control c)
        {
            foreach (Control ctrl in c.Controls)
            {
                fc_CleanForm(ctrl);

            }
            if (c is CheckBox)
            {
                CheckBox CB = (CheckBox)c;
                if (CB.Tag != null && UCase(CB.Tag) != UCase("Lock"))
                    CB.Checked = false;
            }
            if (c is iTalk.iTalk_TextBox_Small2)
            {
                if (c.Tag != null && UCase(c.Tag) != UCase("Lock"))
                {
                    Debug.Print(c.Name);
                    c.Text = "";
                }
            }
            if (c is UltraCombo)
            {
                UltraCombo UC = (UltraCombo)c;
                if (UC.Tag != null && UCase(UC.Tag) != UCase("Lock"))
                {

                    UC.Text = "";
                    UC.Value = "";
                    UC.DataSource = null;
                    //Todo:
                    //if (UC.Rows.Count > 0)
                    //{
                    //    for (int i = 0; i <= UC.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    //    {                          
                    //        UC.Columns(i).Text = "";
                    //        UC.Columns(i).value = "";
                    //    }
                    //}
                }
            }
            if (c is ComboBox)
            {
                ComboBox C = (ComboBox)c;
                if (C.Tag != null && UCase(C.Tag) != UCase("Lock"))
                {
                    C.Text = "";
                    if (C.SelectedIndex != -1)
                        C.SelectedIndex = -1;
                }
            }
            if (c is Label)
            {
                if (c.Tag != null && UCase(c.Tag) != UCase("Lock") && UCase(Left(c.Name, 3)) == UCase("lbl"))

                    c.Text = "";
            }
            if (c is PictureBox)
            {
                PictureBox P = (PictureBox)c;
                if (P.Tag != null && UCase(P.Tag) != UCase("Lock"))

                    //P.Picture = LoadPicture();
                    P.Image = null;
            }
            //if (c is Image)
            //{
            //    Image I = (Image)c;
            //    if (UCase(c.Tag) != Strings.UCase("Lock"))
            //        c.Picture = LoadPicture();
            //}
            if (c is ListBox)
            {
                ListBox LB = (ListBox)c;
                if (LB.Tag != null && UCase(LB.Tag.ToString()) != UCase("Lock"))
                {
                    // for (int i = 0; i <= LB.Items.Count - 1; i++)
                    //  LB.RemoveItem(0);
                    LB.DataSource = null;
                }
            }
        }
        public static string ConvertColExcel(int ValeurCol)
        {
            string functionReturnValue = null;

            short i = 0;
            string[] tabCol = null;
            double nRest = 0;
            string sCol = null;

            ValeurCol = Convert.ToInt16(Replace(Convert.ToString(ValeurCol), "&", ""));
            ValeurCol = Convert.ToInt16(Replace(Convert.ToString(ValeurCol), "H", ""));

            if (ValeurCol == 0)
            {
                functionReturnValue = "";
                return functionReturnValue;
            }

            tabCol = new string[27];
            //initialisation du tableau de valeur de colonnes (colonnes Excel de A à Z)
            //nota : les index du tableau correspondent à leur valeur de la colonne
            for (i = 1; i <= 26; i++)
            {
                tabCol[i] = (char)(64 + i) + "";
            }

            sCol = "";
            if (ValeurCol <= 26)
            {
                sCol = tabCol[ValeurCol];
            }
            else
            {
                sCol = "";
                nRest = Convert.ToDouble(ValeurCol) / Convert.ToDouble(26);
                while (nRest > 0)
                {
                    sCol = sCol + tabCol[Convert.ToInt16(nRest)];//TODO
                    nRest = (nRest - Convert.ToInt32(nRest)) * 26;//TODO
                    if (nRest < 26)
                    {
                        sCol = sCol + tabCol[Convert.ToInt32(nRest)];
                        break; // TODO: might not be correct. Was : Exit Do
                    }
                }
            }

            functionReturnValue = sCol;
            return functionReturnValue;

        }

        public static void fc_chargeStyleColorInter(UltraGrid ssGrid)
        {
            DataTable rsColor = new DataTable();
            string strCouleur;
            long strCodeCouleur;
            long strCouleurCaract;

            try
            {
                var ModAdorsColor = new ModAdo();
                sSQL = "SELECT DISTINCT CodeCouleur,LibelleCouleur,CouleurCaracteres,Gras,Italique,TaillePolice FROM TypeCouleur";
                rsColor = ModAdorsColor.fc_OpenRecordSet(sSQL);
                if (rsColor.Rows.Count > 0)
                {

                    foreach (DataRow Dr in rsColor.Rows)
                    {
                        strCouleur = Dr["LibelleCouleur"] + "";
                        strCodeCouleur = ConvertHexDec(Dr["CodeCouleur"] + "");

                        strCouleurCaract = Convert.ToInt64(nz(ConvertHexDec(Dr["CouleurCaracteres"] + ""), 16777215));

                        //ssGrid.StyleSets(strCouleur).BackColor = strCodeCouleur;
                        ssGrid.DisplayLayout.Appearance.BackColor = Color.FromArgb(Convert.ToInt32(strCodeCouleur));
                        ssGrid.DisplayLayout.Appearance.ForeColor = Color.FromArgb(Convert.ToInt32(strCodeCouleur));

                    }
                }

                rsColor.Clear();

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "General.bas;fc_chargeStyleColorInter;");
            }
        }

        public static void fc_GetParamForm(Control ctrl, UserControl sForm)
        {
            //Control ctrl;
            int i;
            foreach (Control c in ctrl.Controls)
            {
                fc_GetParamForm(c, sForm);
            }

            if (ctrl is iTalk.iTalk_TextBox_Small2)
            {
                if (ctrl.Text == "")
                    ctrl.Text = getFrmReg(sForm.Name, ctrl.Name, "");
                //ctrl.Text = GetSetting(cFrNomApp, sForm.Name, ctrl.Name, "");
            }

            if (ctrl is CheckBox)
            {
                CheckBox CB = (CheckBox)ctrl;
                CB.Checked = nz(getFrmReg(sForm.Name, ctrl.Name, ""), false).ToString() == "True";
            }


            if (ctrl is RadioButton)
            {
                RadioButton RB = (RadioButton)ctrl;
                RB.Checked = nz(getFrmReg(sForm.Name, ctrl.Name, false.ToString()), false).ToString() == "True";
            }



            if (ctrl is UltraCombo)
            {
                UltraCombo Cb = (UltraCombo)ctrl;
                if (Cb.Rows.Count > 0)
                {
                    if (getFrmReg(sForm.Name, "Text_" + ctrl.Name, "") != "")
                    {
                        if (getFrmReg(sForm.Name, "Val_" + ctrl.Name, "") != "")
                            Cb.Value = getFrmReg(sForm.Name, "Val_" + ctrl.Name, "");
                        else
                            Cb.Value = "";
                    }
                    for (i = 0; i <= Cb.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                        Cb.DisplayLayout.Bands[0].Columns[i].Header.Caption = getFrmReg(sForm.Name, "Text_" + Cb.Name + "_Col_" + Cb.DisplayLayout.Bands[0].Columns[i].Key, "");
                }
                Cb.Text = getFrmReg(sForm.Name, "Text_" + Cb.Name, "");
            }

            if (ctrl is ComboBox)
            {
                ComboBox Cb = (ComboBox)ctrl;
                ctrl.Text = getFrmReg(sForm.Name, "Text_" + Cb.Name, "");
            }

        }

        public static void InitOdbc()
        {
            var DELOSTALODBC = "";
            var DELOSTALODBC_DB = "";

            var DTDELOSTAL = "";
            var DTDELOSTAL_DB = "";


            if (General._ExecutionMode == ExecutionMode.Test)
            {
                if (adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL_PDA")
                {
                    DELOSTALODBC = "DELOSTALODBCTEST";
                    DELOSTALODBC_DB = "CPTA_DELOSTALTEST";

                    DTDELOSTAL = "DTDELOSTALTEST";
                    DTDELOSTAL_DB = "AXECIEL_DELOSTAL_PDA";
                }
                else if (adocnn.Database.ToUpper() == "AXECIEL_LONG_PDA")
                {
                    DELOSTALODBC = "LONGODBCTEST";
                    DELOSTALODBC_DB = "CPTA_LONG_CHAUFFAGETEST";

                    DTDELOSTAL = "DTLONGTEST";
                    DTDELOSTAL_DB = "AXECIEL_LONG_PDA";
                }
                else if (adocnn.Database.ToUpper() == "AXECIEL_VanFroid_PDA".ToUpper())
                {
                    DELOSTALODBC = "VANFROIDODBCTEST";
                    DELOSTALODBC_DB = "CPTA_VANFROID";

                    DTDELOSTAL = "DTVANFROIDTEST";
                    DTDELOSTAL_DB = "AXECIEL_VanFroid_PDA";
                }
            }
            else
            {
                if (adocnn.Database.ToUpper() == "AXECIEL_DELOSTAL")
                {
                    DELOSTALODBC = "DELOSTALODBC";
                    DELOSTALODBC_DB = "CPTA_DELOSTAL";

                    DTDELOSTAL = "DTDELOSTAL";
                    DTDELOSTAL_DB = "AXECIEL_DELOSTAL";
                }
                else if (adocnn.Database.ToUpper() == "AXECIEL_LONG")
                {
                    DELOSTALODBC = "LONGODBC";
                    //===> Mondir le 28.12.2020, not the correct database
                    //DELOSTALODBC_DB = "AXECIEL_LONG";
                    DELOSTALODBC_DB = "CPTA_LONG_CHAUFFAGE";
                    //===> Fin Modif Mondir

                    DTDELOSTAL = "DTLONG";
                    DTDELOSTAL_DB = "AXECIEL_LONG";
                }
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                else if (adocnn.Database.ToUpper() == "AXECIEL_VanFroid".ToUpper())
                {
                    DELOSTALODBC = "VANFROIDODBC";
                    DELOSTALODBC_DB = "CPTA_VANFROID";

                    DTDELOSTAL = "DTVANFROID";
                    DTDELOSTAL_DB = "AXECIEL_VanFroid";
                }
                //===> Fin Modif Mondir
            }

            //===> Mondir le 23.07.2020, check if General.sServeurSQL is empty
            //if(string.IsNullOrEmpty(General.sServeurSQL))
            //{
            //    General.sServeurSQL = 
            //}

            OdbcHelper.CreateDataSource(General.sServeurSQL, DELOSTALODBC,
                $"Connected to {DELOSTALODBC_DB}", DELOSTALODBC_DB);

            OdbcHelper.CreateDataSource(General.sServeurSQL, DTDELOSTAL,
                $"Connected to {DTDELOSTAL_DB}", DTDELOSTAL_DB);
        }

        //public static void InitMultiSociete()
        //{
        //    if (General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "InitDone", "null", false) == "1")
        //        return;

        //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\MULTI-SOCIETE\Param", "InitDone", "1");

        //    var NomServeur = "";
        //    var NomBase = "";

        //    var Societe = General.S1;

        //    var NomS1 = "Delostal & Thibault";
        //    var BaseS1 = "";
        //    var ServeurS1 = "";

        //    var NomS4 = "AMMANN";
        //    var BaseS4 = "";
        //    var ServeurS4 = "";

        //    var NomS6 = "LONG";
        //    var BaseS6 = "";
        //    var ServeurS6 = "";

        //    if (_ExecutionMode == ExecutionMode.Test)
        //    {
        //        NomServeur = "DT-SQL-01";
        //        NomBase = "AXECIEL_DELOSTAL_CSHARP_TEST";

        //        BaseS1 = "AXECIEL_DELOSTAL_CSHARP_TEST";
        //        ServeurS1 = "DT-SQL-01";

        //        NomS4 = "AXECIEL_AMMANN_CSHARP_TEST";
        //        ServeurS4 = "DT-SQL-01";

        //        BaseS6 = "AXECIEL_LONG_CSHARP_TEST";
        //        ServeurS6 = "DT-SQL-01";
        //    }
        //    else if(_ExecutionMode == ExecutionMode.Prod)
        //    {
        //        NomServeur = "DT-SQL-01";
        //        NomBase = "AXECIEL_DELOSTAL";

        //        BaseS1 = "AXECIEL_DELOSTAL";
        //        ServeurS1 = "DT-SQL-01";

        //        NomS4 = "AXECIEL_AMMANN";
        //        ServeurS4 = "DT-SQL-01";

        //        BaseS6 = "AXECIEL_LONG_CSHARP_TEST";
        //        ServeurS6 = "DT-SQL-01";
        //    }
        //    else
        //    {
        //        if(General.fncUserName() == "mondi")
        //        {
        //            NomServeur = ".\\SS2017";
        //            NomBase = "AXECIEL_DELOSTAL";

        //            BaseS1 = "AXECIEL_DELOSTAL";
        //            ServeurS1 = ".\\SS2017";

        //            NomS4 = "AXECIEL_AMMANN";
        //            ServeurS4 = ".\\SS2017";

        //            BaseS6 = "AXECIEL_LONG";
        //            ServeurS6 = ".\\SS2017";
        //        }
        //    }
        //}

        public static void InitSource_OLEDB()
        {
            //===> Old code before 23.07.2020
            //if (General._ExecutionMode == ExecutionMode.Test)
            //{
            //    Registry.SetValue($@"HKEY_CURRENT_USER\Software\{cRegInter}\App", "Source_OLEDB",
            //        @"SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL_PDA;INTEGRATED SECURITY=TRUE;");
            //    Registry.SetValue($@"HKEY_CURRENT_USER\Software\{cRegInter}\App", "LastConnection",
            //        @"SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL_PDA;INTEGRATED SECURITY=TRUE;");
            //}
            //else if (General._ExecutionMode == ExecutionMode.Prod)
            //{
            //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "Source_OLEDB", @"SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "LastConnection", @"SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //}
            //else
            //{
            //    //if (General.fncUserName().ToLower() == "mondir.jaatar")
            //    //{
            //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "Source_OLEDB", @"SERVER=.;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //    //Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "LastConnection", @"SERVER=.;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //    //}
            //    if (General.fncUserName().ToLower() == "mondir.jaatar")
            //    {
            //        Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "Source_OLEDB", @"SERVER=.\SQL_AXECIEL;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //        Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "LastConnection", @"SERVER=.\SQL_AXECIEL;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;");
            //    }
            //}

            //===> Mondir le 23.07.2020, Check the last used company
            //===> S1 : Delostal
            //===> S6 : LONG
            //===> S7 : VanFroid
            var ConnectionString = "";

            if (Registry.GetValue($@"HKEY_CURRENT_USER\SOFTWARE\{General.cFrMultiSoc}\Param", "Societe", "")?.ToString() == "S1")
            {
                cRegInter = "Axe_interDT";
            }
            else if (Registry.GetValue($@"HKEY_CURRENT_USER\SOFTWARE\{General.cFrMultiSoc}\Param", "Societe", "")?.ToString() == "S6")
            {
                cRegInter = "Axe_interLONG";
            }
            //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
            else if (Registry.GetValue($@"HKEY_CURRENT_USER\SOFTWARE\{General.cFrMultiSoc}\Param", "Societe", "")?.ToString() == "S7")
            {
                cRegInter = "Axe_interVF";
            }
            //===> Fin Modif Mondir
            else
                cRegInter = "Axe_interDT";

            //===> Check the used mode
            if (General._ExecutionMode == ExecutionMode.Test)
            {
                cRegInter += "_Test";
                if (cRegInter.Contains("Axe_interDT"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL_PDA;INTEGRATED SECURITY=TRUE;";
                else if (cRegInter.Contains("Axe_interLONG"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_LONG_PDA;INTEGRATED SECURITY=TRUE;";
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                else if (cRegInter.Contains("Axe_interVF"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_LONG_PDA;INTEGRATED SECURITY=TRUE;";
                //===> Fin Modif Mondir
            }
            else if (General._ExecutionMode == ExecutionMode.Prod)
            {
                if (cRegInter.Contains("Axe_interDT"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;";
                else if (cRegInter.Contains("Axe_interLONG"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_LONG;INTEGRATED SECURITY=TRUE;";
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                else if (cRegInter.Contains("Axe_interVF"))
                    ConnectionString = "SERVER=DT-SQL-01;DATABASE=AXECIEL_VanFroid;INTEGRATED SECURITY=TRUE;";
                //===> Fin Modif Mondir
            }
            else if (General._ExecutionMode == ExecutionMode.Dev)
            {
                //if (General.fncUserName().ToLower() == "mondir.jaatar")
                //{
                switch (_Company)
                {
                    case Company.DT:
                        ConnectionString = @"SERVER=.;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;";
                        break;
                    case Company.LONG:
                        ConnectionString = @"SERVER=.;DATABASE=AXECIEL_LONG;INTEGRATED SECURITY=TRUE;";
                        break;
                    //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                    case Company.VF:
                        ConnectionString = @"SERVER=.;DATABASE=AXECIEL_VanFroid;INTEGRATED SECURITY=TRUE;";
                        break;
                        //===> Fin Modif Mondir
                }
                //}

                //if (General.fncUserName().ToLower() == "mondir.jaatar")
                //{
                //    ConnectionString = @"SERVER=.\SQL_AXECIEL;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;";
                //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "Source_OLEDB", ConnectionString);
                //    Registry.SetValue(@"HKEY_CURRENT_USER\Software\Axe_interDT\App", "LastConnection", ConnectionString);
                //}
            }

            //===> Mondir, to change it to local Mondir
            //if (General.fncUserName().ToLower().Contains("mondir"))
            //{
            //    //ConnectionString = "SERVER=.\\SQL_AXECIEL;DATABASE=AXECIEL_DELOSTAL;INTEGRATED SECURITY=TRUE;";
            //    ConnectionString = ConnectionString.Replace("DT-SQL-01", ".");
            //}

            //===> Set the value of Source_OLEDB and LastConnection
            Registry.SetValue($@"HKEY_CURRENT_USER\Software\{cRegInter}\App", "Source_OLEDB", ConnectionString);
            Registry.SetValue($@"HKEY_CURRENT_USER\Software\{cRegInter}\App", "LastConnection", ConnectionString);
        }

        public static bool CheckConnexion()
        {
            try
            {
                var conex = getFrmReg(cRegInter + "\\" + "App", cLastConnection, "", false);
                var connection = new SqlConnection(conex);
                connection.Open();
                return true;
            }
            catch
            {
                try
                {
                    ChangeCompany(_Company == Company.DT ? "LONG" : "DT");
                    General.InitSource_OLEDB();
                    var conex = getFrmReg(cRegInter + "\\" + "App", cLastConnection, "", false);
                    var connection = new SqlConnection(conex);
                    connection.Open();
                    return true;
                }
                catch
                {
                    return false;
                }
            }
        }

        public static void ChangeCompany(string company)
        {
            switch (company)
            {
                case "DT":
                    General._Company = General.Company.DT;
                    General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrSociete, "S1", false);
                    break;
                case "LONG":
                    General._Company = General.Company.LONG;
                    General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrSociete, "S6", false);
                    break;
                case "VF":
                    General._Company = Company.VF;
                    General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrSociete, "S7", false);
                    break;
                default:
                    General._Company = General.Company.DT;
                    break;
            }
        }

        public static bool fc_CtrResponsableTravaux(string sNodevis, int lAction)
        {
            //=== 23/07/2019 controle si il existe un responsable travaux sur le devis, retourne true si il existe.
            //=== lAction = 0 si envoie sur PDA, lAction = 1 si cr�ation de bon de commande.

            string sSQL = "";
            bool fc_CtrResponsableTravaux = false;
            Program.SaveException(null, "function:fc_CtrResponsableTravaux();params==>sNodevis=" + sNodevis + " | lAction=" + lAction);
            try
            {
                sSQL = "SELECT     ResponsableTravaux From DevisEnTete WHERE     (NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "')";
                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))
                    fc_CtrResponsableTravaux = true;
                else
                {
                    fc_CtrResponsableTravaux = false;

                    //'If lAction = 0 Then
                    CustomMessageBox.Show("Avant de créer un bon de commande, " +
                        "vous devez d'abord affecter un responsable travaux sur le devis N°" + sNodevis, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //'ElseIf lAction = 1 Then
                    //'    MsgBox "Avant de créer un bon de commande, vous devez d'abord affecter un responsable travaux sur le devis N°" & sNodevis, _
                    //            vbInformation, "Opération annulée"


                    //'End If
                }
            }
            catch (Exception ee)
            {
                Program.SaveException(ee);
            }
            return fc_CtrResponsableTravaux;
        }
        public static int TotalMonths(DateTime start, DateTime end)
        {
            return ((end.Year - start.Year) * 12) + end.Month - start.Month;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        //private void ListDirectory(TreeView treeView, string path)
        //{
        //    try
        //    {
        //        treeView.Nodes.Clear();
        //        treeView.SuspendLayout();


        //        if (Directory.Exists(path))
        //        {
        //            var rootDirectory = new DirectoryInfo(path);
        //            if (rootDirectory.GetDirectories().Length > 0)
        //                foreach (var Dir in rootDirectory.GetDirectories())
        //                {
        //                    var node = new TreeNode(Dir.Name) { Tag = Dir.FullName };
        //                    node.ImageIndex = 1;
        //                    node.SelectedImageIndex = 0;
        //                    treeView.Nodes.Add(node);
        //                }

        //            treeView.ExpandAll();
        //            treeView.ResumeLayout();
        //            foreach (TreeNode TN in treeView.Nodes)
        //                ListSubFolders(TN);
        //        }

        //    }
        //    catch (Exception e)
        //    {
        //        Program.SaveException(e);
        //        Cursor = Cursors.Default;
        //    }
        //}

        public static void setTransparentRowColorOfGrid(Control C)
        {
            foreach (Control control in C.Controls)
                setTransparentRowColorOfGrid(control);
            if (C is UltraGrid)
            {
                var grid = C as UltraGrid;
                grid.DisplayLayout.DefaultSelectedBackColor = Color.Empty;
                grid.DisplayLayout.DefaultSelectedForeColor = Color.Empty;
                grid.DisplayLayout.Override.ResetActiveRowAppearance();
                grid.DisplayLayout.Override.ResetActiveRowCellAppearance();
                grid.DisplayLayout.Override.ResetActiveCellAppearance();
            }

        }
        //[DllImport("user32.dll")]
        //[return: MarshalAs(UnmanagedType.Bool)]
        //public static extern bool SetWindowPos(IntPtr hWnd, IntPtr hWndInsertAfter, int X, int Y, int cx, int cy, uint uFlags);
        public static void SetTopMostWindow(Form Window, bool Topmost)
        {//TODO
            //if (Topmost)
            //    SetWindowPos(Window.Handle, (IntPtr)HWND_TOPMOST, 0, 0, 0, 0, (uint)Flags);
            //else
            //    SetWindowPos(Window.Handle, (IntPtr)HWND_NOTOPMOST, 0, 0, 0, 0, (uint)Flags);
        }

        public static string fc_filtreDate(string txtDate)
        {

            string functionReturnValue;
            if ((txtDate.Length == 8 || txtDate.Length == 9 || txtDate.Length == 7 || txtDate.Length == 6) && txtDate.Contains("/") && txtDate.LastIndexOf("/") != txtDate.IndexOf("/") && IsDate(txtDate))
            {
                var date = txtDate.Split('/');
                // available format 
                // d/m/aa
                // dd/m/aa
                // d/mm/aa
                // dd/mm/aa
                // d/mm/aaaa
                // dd/mm/aaaa
                // dd/m/aaaa
                if (date.Length == 3)//check if txtDate has format */****
                {
                    var day = date[0].Length == 1 ? "0" + date[0] : date[0];
                    var month = date[1].Length == 1 ? "0" + date[1] : date[1];
                    var year = date[2].Length == 2 ? "20" + date[2] : date[2];
                    return day + "/" + month + "/" + year;
                }
            }
            if (txtDate.Length == 8 && !txtDate.Contains(".") && IsNumeric(txtDate))
            {
                //ddmmaaaa format
                var jour = txtDate.Substring(0, 2);
                var mois = txtDate.Substring(2, 2);
                var annee = txtDate.Substring(4);
                functionReturnValue = jour + "/" + mois + "/" + annee;
                return functionReturnValue;

            }
            else if (txtDate.Length == 4 && !txtDate.Contains(".") && IsNumeric(txtDate))
            {
                //ddmm format
                var jour = txtDate.Substring(0, 2);
                var mois = txtDate.Substring(2);
                var annee = DateTime.Now.Year;
                functionReturnValue = jour + "/" + mois + "/" + annee;
                return functionReturnValue;
            }
            else if (txtDate.Length == 3 && !txtDate.Contains(".") && IsNumeric(txtDate))
            {
                //dmm format
                txtDate = "0" + txtDate;
                var jour = txtDate.Substring(0, 2);
                var mois = txtDate.Substring(2);
                var annee = DateTime.Now.Year;
                functionReturnValue = jour + "/" + mois + "/" + annee;
                return functionReturnValue;
            }

            else
            {

                if (txtDate.Length == 10 && General.IsDate(txtDate))
                {
                    return txtDate;
                }
                functionReturnValue = "";
                return functionReturnValue;
            }

        }
        public static bool fc_ValidateDate(UltraDateTimeEditor txtSaisie)
        {
            if (!string.IsNullOrEmpty(txtSaisie.Text))
            {

                string str = General.fc_filtreDate(txtSaisie.Text);
                if (!string.IsNullOrEmpty(str) && General.IsDate(str))
                {
                    txtSaisie.Text = Convert.ToDateTime(str).ToShortDateString();
                    if (Convert.ToDateTime(txtSaisie.Text) < Convert.ToDateTime("01/01/1900"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSaisie.Focus();
                        return false;
                    }
                    return true;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSaisie.Focus();
                    return false;
                }

            }
            return false;
        }

        public static bool fc_ValidateDate(iTalk.iTalk_TextBox_Small2 txtSaisie)
        {
            if (!string.IsNullOrEmpty(txtSaisie.Text))
            {

                string str = General.fc_filtreDate(txtSaisie.Text);
                if (!string.IsNullOrEmpty(str) && General.IsDate(str))
                {
                    txtSaisie.Text = Convert.ToDateTime(str).ToShortDateString();
                    if (Convert.ToDateTime(txtSaisie.Text) < Convert.ToDateTime("01/01/1900"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtSaisie.Focus();
                        return false;
                    }
                    return true;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtSaisie.Focus();
                    return false;
                }

            }
            return false;
        }

        /// <summary>
        /// ////// ==============> Mondir : To Fix This Bug, https://groupe-dt.mantishub.io/view.php?id=1645,
            /////                  need to change a value in the register to force crystal report to render the same font size
        /// </summary>
        public static void ChangeRegValuesFromCrystal()
        {
            //1- HKEY_CURRENT_USER\Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export
            //2- Add a folder PDF
            //3- Add ForceLargerFonts and value of 1
            //4- HKEY_LOCAL_MACHINE\SOFTWARE\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports
            //5- Add a folder Export
            //6- Add a folder PDF
            //7- Add ForceLargerFonts and value of 1
            //TODO Mondir - Look at https://groupe-dt.mantishub.io/view.php?id=1785
            //if (Registry.GetValue(@"HKEY_CURRENT_USER\Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF", "ForceLargerFonts", "") == null)
            Registry.SetValue(@"HKEY_CURRENT_USER\Software\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF", "ForceLargerFonts", "0");
            //if (Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF", "ForceLargerFonts", "") == null)
            //    Registry.SetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\SAP BusinessObjects\Crystal Reports for .NET Framework 4.0\Crystal Reports\Export\PDF", "ForceLargerFonts", "1");
        }
        public static string fc_GetHourIntSeconde(DateTime sDEBUT, DateTime sFin, int lSansPauseDejeuner = 0)
        {
            string sDuree;
            double sDureeDiff = 0;
            DateTime dtHeureFinMatin;
            DateTime dtHeureDebutPM;
            DateTime dtDeb;
            DateTime dtFin;

            try
            {
                dtHeureFinMatin = Convert.ToDateTime(nz(sHeureFinMatin, "12:00:00"));
                dtHeureDebutPM = Convert.ToDateTime(nz(sHeureDebutPM, "13:30:00"));

                //sDEBUT = Format(sDEBUT, "hh:mm:ss");
                //sFin = Format(sFin, "hh:mm:ss");

                if (sDEBUT > sFin)
                {
                    dtDeb = DateTime.Now.Date;
                    dtFin = DateTime.Now.AddDays(1).Date;
                }
                else
                {
                    dtDeb = DateTime.Now.Date;
                    dtFin = DateTime.Now.Date;
                }
                // 'Calcul de la duree de l'intervention en seconde(sDureeDiff)

                //=== Cas 1 : Coupure entre midi et 2 :
                if (sDEBUT < dtHeureFinMatin && sFin > dtHeureDebutPM && lSansPauseDejeuner == 0)
                {
                    sDureeDiff = (dtHeureFinMatin - sDEBUT).TotalSeconds + (sFin - dtHeureDebutPM).TotalSeconds;

                }
                //'=== Cas 2 : Pas de coupure, W commencé entre midi et 2
                else if (sDEBUT > dtHeureFinMatin && sDEBUT < dtHeureDebutPM && sFin > Convert.ToDateTime(sHeureDebutPM))
                {
                    sDureeDiff = (sFin - sDEBUT).TotalSeconds;
                }
                //'=== Cas 3 : Pas de coupure, W fini entre midi et 2
                else if (sDEBUT < dtHeureFinMatin && sFin < dtHeureDebutPM && sFin > Convert.ToDateTime(sHeureFinMatin))
                {
                    sDureeDiff = (sFin - sDEBUT).TotalSeconds;
                }
                //'=== Cas 4 : Pas de coupure, W le matin ou l'après-midi
                else
                {
                    //'sDureeDiff = DateDiff("s", Format(sDEBUT, "hh:mm:ss"), CDate(sFin))
                    dtDeb = dtDeb.AddHours(sDEBUT.Hour).AddMinutes(sDEBUT.Minute).AddSeconds(sDEBUT.Second);
                    dtFin = dtFin.AddHours(sFin.Hour).AddMinutes(sFin.Minute).AddSeconds(sFin.Second);
                    sDureeDiff = (dtFin - dtDeb).TotalSeconds;
                }
                return sDureeDiff.ToString();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                return "";
            }
        }

        /// <summary>
        ///
        /// Modifs de la version V29.06.2020 de VB6 intégré par Mondir le 30.06.2020
        /// </summary>
        /// <param name="bBtVisualiseInVisible"></param>
        /// <param name="bFromDevis"></param>
        /// <param name="sCodeEtat"></param>
        /// <param name="sNodevis"></param>
        /// <param name="sCodeimmeuble"></param>
        /// <param name="Form"></param>
        /// <param name="lNointervention"></param>
        /// <param name="lNumficheStandard"></param>
        public static void fc_CreateFact(bool bBtVisualiseInVisible, bool bFromDevis, string sCodeEtat, string sNodevis, string sCodeimmeuble,
                                        UserControl Form, int lNointervention, int lNumficheStandard)
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo modAdoRs = new ModAdo();
            bool bAccompte;
            try
            {
                Variable.sImmFact = "";
                Variable.sDevisFact = "";
                Variable.lFactAVoir = 0;
                Variable.lFactInterOuDevis = 0;
                Variable.lFactNumFicheStandard = 0;
                Variable.lDefOuAcompteOuGarantie = 0;
                Variable.dbAccompteInterv = 0;

                frmFacture frmf = new frmFacture();
                frmf.OptDevis.Checked = false;
                frmf.OptDevis.Checked = false;
                frmf.OptAcompte.Checked = false;
                frmf.optDefinitive.Checked = false;
                frmf.optGarantie.Checked = false;

                if (bBtVisualiseInVisible == true)
                {
                    frmf.cmdVisualiser.Enabled = true;
                }
                else
                {
                    frmf.cmdVisualiser.Enabled = false;
                }

                frmf.txtstatut.Text = sCodeEtat;
                frmf.txtNumFicheStandard.Text = lNumficheStandard.ToString();

                if (bFromDevis == true)
                {
                    frmf.OptDevis.Checked = true;
                }
                //'=== Contrôle si intervention suite devis.
                if (sNodevis != "")
                {
                    frmf.OptDevis.Enabled = true;
                    frmf.Optintervention.Enabled = false;
                    frmf.optGarantie.Visible = true;
                    frmf.OptDevis.Checked = true;
                    //====> Mondir le 30.06.2020, modifs de la version VB 29.06.2020
                    frmf.lblNodevis.Text = "Devis n° " + sNodevis;
                    //====> Fin Modif Mondir
                }
                else
                {
                    frmf.OptDevis.Enabled = false;
                    frmf.Optintervention.Enabled = true;
                    frmf.optGarantie.Visible = false;
                    frmf.Optintervention.Checked = true;
                }

                frmf.ShowDialog();
                frmf.Close();
                if (Variable.lFactAVoir == 0)
                {
                    return;
                }
                if (Variable.lFactAVoir == 2 || Variable.lFactAVoir == 3)
                {
                    //'=== si lFactAVoir = 2 ==> facture.
                    // '=== si lFactAVoir = 3 ==> avoir.
                    if (sNodevis != "")
                    {
                        sSQL = "SELECT     Intervention.CodeEtat, Intervention.Acompte, GestionStandard.NumFicheStandard "
                               + " FROM         GestionStandard INNER JOIN "
                               + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard "
                               + " WHERE     (GestionStandard.DevAccep = 1) AND "
                               + " (GestionStandard.NoDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "')"
                               + "  and (Intervention.nofacture = '' or  Intervention.nofacture is null) "
                               + "  order by  Intervention.NoIntervention DESC ";
                        rs = modAdoRs.fc_OpenRecordSet(sSQL);
                        if (rs.Rows.Count == 0)
                        {
                            CustomMessageBox.Show("Aucune intervention à facturer, veuillez dupliquer une intervention si vous désirez facturer cette affaire.", "Aucune intervention à facturer", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            rs.Dispose();
                            rs = null;
                            return;
                        }

                        Variable.lFactNumFicheStandard = Convert.ToInt32(nz(rs.Rows[0]["NumFicheStandard"], 0));


                        //'=== lDefOuAcompteOuGarantie est à 1 => Facture interv accompte
                        //'=== lDefOuAcompteOuGarantie est 2 => Facture interv definitive.
                        //'=== lDefOuAcompteOuGarantie est => Facture devis de garantie.
                        bAccompte = false;
                        if (Variable.lDefOuAcompteOuGarantie == 1)
                        {
                            foreach (DataRow r in rs.Rows)
                            {
                                if (Convert.ToInt32(General.nz(r["Acompte"], 0)) > 0)
                                {
                                    bAccompte = true;
                                    Variable.dbAccompteInterv = Convert.ToInt32(General.nz(r["Acompte"], 0));
                                    break;
                                }
                            }
                            rs.Dispose();
                            rs = null;
                            //'=== modif du 01/04/2020, la saisie de l'accompte sur une intervention n'est plus obligatoire.
                            //'If bAccompte = False Then
                            //'        MsgBox "Vous devez saisir un acompte sur une intervention pour créer une facture d'acompte.", vbInformation, "Acompte absent"
                            //'        Exit Sub
                            //'End If
                        }
                    }
                    else
                    {
                        sSQL = "SELECT        NumFicheStandard "
                             + " From Intervention   WHERE        NoIntervention = " + lNointervention;
                        rs = modAdoRs.fc_OpenRecordSet(sSQL);

                        //'=== modif du 11 05 2020, les interventions sans devis avec des fiches d'appels differentes peuvent être cumulées
                        //'=== sur une même facture.
                        //'=== modif temporaire on n'affiche les inters que d'une fiche d'appel
                        Variable.lFactNumFicheStandard = Convert.ToInt32(nz(rs.Rows[0]["NumFicheStandard"], 0));

                        rs.Dispose();
                        rs = null;

                        //'=== lDefOuAcompteOuGarantie est à 1 => Facture interv accompte
                        //'=== lDefOuAcompteOuGarantie est 2 => Facture interv definitive.
                        //'=== lDefOuAcompteOuGarantie est => Facture devis de garantie.
                        if (Variable.lDefOuAcompteOuGarantie == 1)
                        {
                            sSQL = "SELECT        NumFicheStandard, NoFacture, Acompte from intervention"
                                + " WHERE        (NoFacture = '' OR NoFacture IS NULL) AND (NumFicheStandard = " + Variable.lFactNumFicheStandard + ")";

                            rs = modAdoRs.fc_OpenRecordSet(sSQL);

                            foreach (DataRow r in rs.Rows)
                            {
                                if (Convert.ToInt32(General.nz(r["Acompte"], 0)) > 0)
                                {
                                    bAccompte = true;
                                    Variable.dbAccompteInterv = Convert.ToInt32(General.nz(r["Acompte"], 0));
                                    break;
                                }
                            }
                            rs.Dispose();
                            rs = null;
                            //'=== modif du 01/04/2020, la saisie de l'accompte sur une intervention n'est plus obligatoire.
                            //'If bAccompte = False Then
                            //'        MsgBox "Vous devez saisir un acompte sur une intervention pour créer une facture d'acompte.", vbInformation, "Acompte absent"
                            //'        Exit Sub
                            //'End If

                        }
                    }

                    ///=======> Mondir : 15.06.2020 - The person who integrated Rachid's Modifs was pooping everywhere and not tested the modifcations
                    Variable.sImmFact = sCodeimmeuble;
                    Variable.sDevisFact = sNodevis;

                    View.Theme.Theme.Navigate(typeof(UserDocFacManuel));
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "General;fc_CreateFact");
            }
        }

        public static int fc_GetNumStandardFromInter(int lNointervention)
        {
            int returnFunction = 0;
            try
            {
                sSQL = "SELECT NumFicheStandard From Intervention WHERE NoIntervention = " + lNointervention;
                using (var tmpAdo = new ModAdo())
                {
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                    if (sSQL != "")
                    {
                        returnFunction = Convert.ToInt32(General.nz(sSQL, 0));
                    }
                    else
                    {
                        returnFunction = 0;
                    }
                    return returnFunction;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_GetNumStandardFromInter");
                return returnFunction;
            }
        }
        public static int fc_GetNumStandardFromDevis(string sNodevis)
        {
            int returnFunction = 0;
            try
            {
                sSQL = "SELECT  NumFicheStandard From Intervention WHERE   NoDevis ='" + sNodevis + "'";
                using (var tmpAdo = new ModAdo())
                {
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                    if (sSQL != "")
                    {
                        returnFunction = Convert.ToInt32(General.nz(sSQL, 0));
                    }
                    else
                    {
                        returnFunction = 0;
                    }
                    return returnFunction;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_GetNumStandardFromInter");
                return returnFunction;
            }
        }

        /// <summary>
        /// Modif de la version 02.07.2020 ajoutées par Mondir le 02.07.2020
        /// Modif de la version 02.07.2020 testées par Mondir le 02.07.2020
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public static string fc_CtrlPariculier(string sWhere)
        {
            string functionReturnValue = "";
            string sSQL = "";
            try
            {
                sSQL = "SELECT       GestionStandard.CodeParticulier"
                      + " FROM            GestionStandard INNER JOIN "
                      + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard";

                sSQL = sSQL + " " + sWhere;

                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                functionReturnValue = sSQL;

            }
            catch (Exception e)
            {
                Program.SaveException(e, "general;fc_CtrlPariculier");
            }

            return functionReturnValue;
        }

        /// <summary>
        /// Mondir le 07.05.2021, https://groupe-dt.mantishub.io/view.php?id=1381#c5964
        /// </summary>
        /// <param name="CodeImmeuble"></param>
        /// <param name="situation"></param>
        public static void UpdateSituation(string CodeImmeuble, string situation)
        {
            //===> Update Situation de la table Immeuble
            //var req = $"UPDATE Immeuble SET SitBoiteAClef = '{situation}' WHERE CodeImmeuble = '{CodeImmeuble}'";
            //Execute(req);

            //===> Update Situation de la table Intervention ===> Error
            //req = $"UPDATE Intervention SET SitBoiteAClef = '{situation}' WHERE CodeImmeuble = '{CodeImmeuble}'";
            //Execute(req);
        }
    }
}
