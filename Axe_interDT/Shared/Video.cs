﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    static class Video
    {
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <returns></returns>
        public static object fc_CreeDossierImm(string sCode)
        {
            object functionReturnValue = null;

            try
            {
                if (!string.IsNullOrEmpty(sCode))
                {
                    sCode = sCode.Trim();

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode) == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\photos") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Fournisseurs") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Fournisseurs\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Fournisseurs\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Devis") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Devis\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Devis\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Video") == -1)
                        return functionReturnValue;
                    //=== modif du 25 09 2014, creation du dossier amiante.
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\amiante") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\amiante\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\amiante\\out") == -1)
                        return functionReturnValue;
                    //=== modif du 12 11 2014, creation du dossier travaux.
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Travaux") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Travaux\\in") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Travaux\\out") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderCptRendu) == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt) == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\SIDEC") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\DIPAN") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\DICOP") == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\EPS") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\SOCOMARI") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\DFM") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\ECF") == -1)
                        return functionReturnValue;
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\AUTRES") == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\" + General.cFolderSSt + "\\LONG") == -1)
                        return functionReturnValue;

                    //Modif le 27.07.2020 par Salma
                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Relances") == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Relances\\Finances") == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Relances\\Devis") == -1)
                        return functionReturnValue;

                    if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\Relances\\Contrats") == -1)
                        return functionReturnValue;
                    //Fin Modif 27.07.2020

                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Video;fc_CreeDossier");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sNameFile"></param>
        /// <param name="bctrlFileIn"></param>
        /// <param name="bNameFile"></param>
        /// <returns></returns>
        public static int fc_RecherheVideo(string sCodeImmeuble, ref string sNameFile, bool bctrlFileIn = false, bool bNameFile = false)
        {
            int functionReturnValue = 0;

            int lNbVideo = 0;
            string sPath = null;
            try
            {
                sPath = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Video\\";
                //sPath = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Video\\*.*";

                //=== crée le dossier immeuble.
                fc_CreeDossierImm(sCodeImmeuble);

                ///==================> Tested
                if (bctrlFileIn == true)
                {
                    //=== bctrlFileIn est à true si on desire juste constater la presence d'au moins 1 fichier.
                    lNbVideo = Dossier.fc_CtrlFileIn(sPath);

                }
                else if (bNameFile == true)
                {
                    //==== bNameFile est a true si on desire retourner les noms des fichier,
                    //==== ne pas oublier d'envoyer en parametre une variable string vide dans sNameFile.
                    sNameFile = Dossier.fc_ListDirectoryName(sPath);

                }
                else
                {
                    //=== recherche le nombre de video present dans le dossier Video.
                    lNbVideo = Dossier.fc_ListDirectory(sPath);
                }

                functionReturnValue = lNbVideo;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "modVideo;fc_RecherheVideo");
                return functionReturnValue;
            }
        }


    }
}
