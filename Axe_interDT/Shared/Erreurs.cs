﻿using Axe_interDT.View.Theme;
using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class Erreurs
    {
        public static string cFrAppNom = "Message d'erreur";
        //' Retours de fonctions
        public static int cFrRecommencer = -5;    //' Recommencer (conflict réseau)
        public static int cFrAbandonner = -6;     //' Abandonner (conflict réseau)

        public static string cFrModName = "Mod gest Gestion ERREURS";
        public static int acDataErrContinue = 0;
        private static int acSysCmdRuntime = 6;
        private static int acComboBox = 111;
        private static int acListBox = 110;

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="e"></param>
        /// <param name="vtexte"></param>
        /// <param name="bAffErr"></param>
        public static void gFr_debug(Exception e, object vtexte, bool bAffErr = false)
        {
            //'Afficher le texte dans la fenêtre de debug et stocke l'info dans un fichier texte
            //' Afficher le message d'erreur standard si précisé
            var sFr_CheminCourant = "";
            object sTexte = "";
            var lErr = 0;
            var sErr = "";
            var strString = "";
            //' Mémoriser erreur en cours pour ne pas la perdre
            var ex = e as Win32Exception;
            if (ex != null)
                lErr = ex.ErrorCode;
            sErr = e.Message;
            try
            {
                if (!bAffErr) //'990713 afficher le message d'erreur à l'utilisateur
                {
                    if (sErr == "")
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(vtexte.ToString(), cFrAppNom, MessageBoxButtons.OK, MessageBoxIcon.Error); // '990922
                    else
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sErr, cFrAppNom, MessageBoxButtons.OK, MessageBoxIcon.Error); //' Icône avec croix sur fond rouge
                }

                //'==== Mise en forme du message
                sTexte = General.nz(vtexte, "");
                if (lErr != 0)
                    sTexte = sTexte + ":" + lErr + "," + sErr;
                else
                    sTexte = "" + sTexte;

                // 'Get the computer name
                strString = Environment.MachineName;

                //' Crée le nom du fichier.
                sFr_CheminCourant = Theme.LogFolder + "\\";
                var dt = DateTime.Now.ToString("yyyy-MM-dd");
                //Fr_AjouterAuFichier(sFr_CheminCourant + AppDomain.CurrentDomain.FriendlyName + $"-{dt}" + ".txt",
                //    DateTime.Now.ToString("dd/MM/yyyy hh:mm") + " => Nom App.: " +
                //    AppDomain.CurrentDomain.FriendlyName +
                //    " => ComputerName: " + strString + " => Nom du log: " + General.fncUserName() + "->Erreur :" +
                //    sTexte + "\n");
                Program.SaveException(e, vtexte.ToString());
                Theme.MainForm.Cursor = Cursors.Default;
            }
            catch
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, cFrAppNom, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sFichier"></param>
        /// <param name="sTexte"></param>
        public static void Fr_AjouterAuFichier(string sFichier, string sTexte)
        {
            try
            {
                // ' Écrit le texte dans le fichier.
                File.AppendAllText(sFichier, sTexte + "\n");
            }
            catch (Exception e)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Fr_AjouterAuFichier " + sFichier, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
    }
}
