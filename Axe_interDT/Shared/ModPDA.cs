﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class ModPDA
    {
        public static DataTable adocnnHarm;
        public static ModAdo modAdoadocnnHarm;

        const char cSep = '|';

        //Public Const cDateHeureNull = "00000000 00:00:00"

        public const string cRetChario = "\\r\\n";

        public struct RamonagePDA
        {
            public string sNoIntervention;
            public string sID_Horaire;
            public string sTypeOperation;
            public string sCodeImmeuble;
            public string sAdresse;
            public string sCP;
            public string sVille;
            public int lIdLocalisation;
            public string sNomLocalisation;
            public string sAngleRue;
            public string sResidence;
            public string lIDFicheRamonage;
            public string sDatePrevue;
            public string sDateRendezvous;
            public string sMatricule;
            public int lRAMI_PriseRV;
            public string sRAMI_INFO;
            public string lID_PDA;
            public int[] lIdetail;
            public string[] sMAE_Code;
            public string[] sLibelle;
            public string lNoLot;
            public string lNoOrdre;
            public int lPresenceGardien;
            public string sNomIntervenant;
            public string sID_CLIENT;
            public string sTPP_Code;
        }

        static int wNb;
        static string strEnergie;
        static short ChkLiv;
        static int Nbj;

        //Type InterPDA
        //    sTypeIntervention        As String
        //    sNoIntervention          As String
        //    sTypeOperation           As String
        //    sCodeimmeuble            As String
        //    sAdresse                 As String
        //    sCP                      As String
        //    sVille                   As String
        //    'sID_Horaire              As String
        //    sAngleRue                As String
        //    lIdLocalisation          As Long
        //    sNomLocalisation         As String
        //    sDatePrevue              As String
        //    sDateCreation            As String
        //    sCodeEtat                As String
        //    sNomIntervenant          As String
        //    sCodeArticle             As String
        //    sLibArticle              As String
        //    sTPP_Code                As String
        //    lsaisieCompteur          As Long
        //    sInfoComplementaire      As String
        //    sCommentaireCorrective   As String
        //    lUrgence                 As Long
        //    sTelephone               As String
        //    sInfoPrestation         As String
        //    sInfoStat               As String
        //    sMatricule              As String
        //    lIdetail()               As Long
        //    sMotif()                 As String
        //    sLibelle()               As String
        //
        //    sComptNoCompteur()                 As String
        //    sComptID()                         As String
        //    sComptLibelleCompteur()            As String
        //    sComptIdLocalisation()             As String
        //    sLocComptNomLocalisation()            As String
        //    sComptModeCalcul()                 As String
        //    sComptDateIndexPrecedent()         As String
        //    sComptIndexPrecedent()             As String
        //    sComptAllumeEteint()               As String
        //
        //End Type

        public struct InterPDA
        {
            public string sTypeIntervention;
            public string sNoIntervention;
            public string sTypeOperation;
            public string sCodeImmeuble;
            public string sAdresse;
            public string sCP;
            public string sVille;
            //sID_Horaire              As String
            public string sAngleRue;
            public string sDatePrevue;
            public string sDateCreation;
            public string sCodeEtat;
            public string sNomIntervenant;
            public string sCodeArticle { get; set; }
            public string sLibArticle;
            public string sTPP_Code;
            public int lsaisieCompteur;
            public string sInfoComplementaire;
            public string sCommentaireCorrective;
            public int lUrgence;
            public string sTelephone;
            public string sInfoPrestation;
            public string sInfoStat;
            public string sMatricule;
            //lIdetail()               As Long
            //sMotif()                 As String
            //sLibelle()               As String
            //=== prestations.
            public int[] lIdLocalisation;
            public string[] sNomLocalisation;
            public int[] lIdEquipement;
            public string[] sNomEquipement;
            //sIdComposant()            As String
            public string[] lIdComposant;
            public string[] sNomComposant;
            //===detail des prestations.
            public int[] lDetIdControle;
            public int[] lDetIdLocalisation;
            //sDetIdComposant()            As String
            public string[] lDetIdComposant;
            public int[] lDetIdEquipement;
            public string[] sDetNomControle;
            public string[] sDetMoyen;
            public string[] sDetAnomalie;
            public string[] sDetOperation;
            public string[] sFrequence;
            public int[] lReportee;
            //=== localisation des compteurs
            public int[] lLocComptIdLocalisation;
            public string[] sLocComptNomLocalisation;
            public int[] lLocComptCHauffage;
            public int[] lLocComptAllArret;
            public string[] sLocComptNomRondier;
            //=== compteurs.
            public string[] sComptSaison;
            public int[] lComptNumAppareil;
            public string[] sComptNomCOmpteur;
            public string[] sComptTypeCompteur;
            public string[] sComptDateIndexPrecedent;
            public string[] sComptIndexPrecedent;
            public string[] sComptNiveauMini;
            public string[] sComptNiveauMaxi;
            public string[] sComptDiametreCuve;
            public string[] sComptUnitePige;
            public int[] lComptIdLocalisation;
            public string[] sComptQteLivraison;
            //=== particulier.
            public bool boolPartExist;
            public string[] sCodeImmeublePart;
            public string[] sCodeParticulierPart;
            public string[] sNomPart;
            public string[] sAdressePart;
            public string[] sCpPart;
            public string[] sVillePart;
            public string[] sTelFixePart;
            public string[] sPortablePart;
            public string[] sMailPart;
        }


        public struct compteur
        {
            public string sNoCompteur;
            public string sIDCompteur;
            public string sLibelleCompteur;
            public string sIDLocalisation;
            public string sNomLocalisation;
            public string sTypeCalcul;
            public string sDateInex;
            public string sAllumageArret;
            public string sIndexPrecedent;
        }

        public const string cCOLLECTIVE = "COLLECTIVE";
        public const string cPRIVATIVE = "PRIVATIVE";

        const string cCalCONSO = "CONSOMMATION";
        const string cCalCUVE = "CUVE";
        const string cCalAPPOINT = "APPOINT";

        const string cCAllumage = "ALL";
        const string cCArret = "ARR";

        const string cEnteteCompteur = "COMPTEUR";

        public const string TypeInterPdaRamonage = "R";
        //Public Const TypeInterPdaPreventif = "P"
        public const string TypeInterPdaPreventif = "G";
        public const string TypeInterPdaCorrective = "C";


        public const string cID_Ramonage = "RAMONAGE";
        public const string cID_Corrective = "CORRECTIVE";
        public const string cID_Preventive = "PREVENTIVE";
        //PUBLIC CONST cID_




        //==rouge.
        public const int cColorRVaPrendrex = 0xc0c0ff;
        //===  vert.
        public const int cColorRVPrisx = 0xc0ffc0;
        //=== blanc.
        public const int cColorSansRVx = 0xffffff;

        public const string cNomRvAPrendre = "RVaPrendre";
        public const string cNomRvPris = "RVaPris";
        public const string cNomSansRv = "SansRV";
        public const string cSelection = "Selection";


        const string cEnerGaz = "GAZ";
        const string cEnerFIOUL = "FIOUL";
        const string cEnerCPCU = "CPCU";
        const string cEnerrESEAU = "RESEAU DE CHALEUR";
        const string cEnerbOIS = "BOIS";
        const string cEnerELECT = "ELECTRICITE";

        const string cPrestChau = "CHAUFFAGE";
        const string cPrestECS = "ECS";
        const string cPrestTE = "TRAITEMENT D'EAU";
        const string cPrestDISCO = "DISCONNECTEUR";
        const string cPrestsurp = "SURPRESSEUR";
        const string cPrestRELEVAGE = "RELEVAGE PARKING";
        const string cPrestVMC = "VMC";
        const string cPrestVMCDSC = "VMC /DSC";
        const string cPrestTELE = "TELESURVEILLANCE";
        const string cPrestMURALE = "MURALE";
        const string cPrestCLIM = "CLIM";
        const string cPrestCHAUCLIM = "CHAUFFAGE + CLIM";

        const string cTypeLigneINFO = "ET_INFO_IMMEUBLE";
        const string cEnteteRamonage = "INTERVENTION";
        const string cDetailRamonage = "RAMONAGE";
        const string cDetailInter = "LOCALISATION_GAMMES";
        const string cDetailControle = "CONTROLES";
        const string cFIN_Ramonage = "FF_RAMONAGE";
        const string cLocCompteur = "LOCALISATION_COMPTEUR";
        const string cDetCompteur = "COMPTEUR";

        const string cKeyParticulier = "REF_TIERS";

        public const string cStatut1 = "TD";
        public const string cStatut1Lib = "A TRANSMETTRE";
        const string cOui = "OUI";
        const string cNon = "NON";

        const string cTypeOpCreation = "C";
        const string cTypeOpSuppression = "S";
        const string cDebutNomFichier = "bo2har";

        const string cET_HIST_RAMONAGE = "HIST_INTERVENTION";
        const string cFF_HIST_RAMONAGE = "FF_HIST_INTERVENTION";

        const string cET_HIST_DEVIS = "HIST_DEVIS";
        const string cFF_HIST_DEVIS = "FF_HIST_DEVIS";

        //=== statut pda
        public const string cNonEnvoye = "Non Transmis";
        public const string cEnvoye = "Transmis";
        public const string cAnnulee = "Annulation confirmée";
        public const string cDemandeAnnulation = "Annulation demandée";
        public const string cAnnulationRefuse = "Annulation refusée";
        public const string cREcu = "Reçu";
        public const string cDebut = "commencée";
        public const string cFin = "Terminée";
        public const string cErreurTransmis = "Erreur transmission";
        public const string cSuspendu = "Suspendu";
        public const string cPasFait = "Pas fait";
        //Type RamonageDetail
        //     sNointeventionPDA      As String
        //     lIdDetail              As Long
        //     sMae_Code              As String
        //     sLibelle               As String
        //End Type


        const string TPPHebdomadaires = "Hebdo";
        const string TPPmensuelles = "Mensu";
        const string TPPbimensuelle = "Bimen";
        const string TPPbimestrielle = "Bimes";
        const string TPPtrimestrielle = "Trime";
        const string TPPsemestrielle = "Semes";
        const string TPPannuelles = "Annue";

        public static void fc_SendInterventionPDA(ModAdo modAdorsPDA, bool bModeSuppression, string sTypeIntervention, System.DateTime dtSendNow, bool bInterDuplice = false, string sMatImposer = "", string sFicheOrigine = "")
        {


            InterPDA[] tpInter = null;
            //Dim tpDetailInter()                     As RamonageDetail
            DataTable rsPDA = modAdorsPDA.rsAdo;
            int i = 0;
            int X = 0;
            int w = 0;
            int lCount = 0;
            int lID_PDA = 0;
            int lPresenceGardien = 0;
            int ltemp = 0;
            int LiDHoraire = 0;
            int lUrgence = 0;
            int lNodecade = 0;


            string sNomfichier = null;
            string sSQL = null;
            string sSQLID = null;
            string sMessage = null;
            string sdate = null;
            string sTypeCal = null;
            string sAllArret = null;
            string sSender = null;

            DataTable rsDet = default(DataTable);
            ModAdo modAdorsDet = null;
            DataTable rsImm = default(DataTable);
            ModAdo modAdorsImm = null;
            DataTable rsCompt = default(DataTable);
            ModAdo modAdorsCompt = null;
            DataTable rsReleve = default(DataTable);
            ModAdo modAdorsReleve = null;

            compteur[] tpCompteur = null;

            bool bSaisieCompteur = false;
            bool bDetail = false;
            string sInfoStat = null;
            string sInfoDevis = null;
            string sINfoPrest = null;
            string sALAR_Code = null;
            string sDPeriodeDu = null;
            string sDPeriodeAu = null;
            string sDateRel = null;

            System.DateTime dtSend = default(System.DateTime);
            System.DateTime dtPeriodeDU = default(System.DateTime);
            System.DateTime dtPeriodeAU = default(System.DateTime);
            System.DateTime dtDebut = default(System.DateTime);
            System.DateTime dtFin = default(System.DateTime);

            try
            {
                //=== si aucune intervention, on sort de la fonction.
                if (rsPDA.Rows.Count == 0)
                {
                    return;
                }

                dtSend = DateTime.Now;
                sSender = General.Left(General.fncUserName(), 50);

                lPresenceGardien = 0;
                bDetail = false;
                bSaisieCompteur = false;
                sAllArret = "";




                //=== intervention de type corrective.
                if (sTypeIntervention.ToUpper() == TypeInterPdaCorrective.ToUpper())
                {


                    foreach (DataRow rsPDARow in rsPDA.Rows)
                    {
                        i = 0;
                        tpInter = new InterPDA[i + 1];
                        //=== stocke le type d'intervention.
                        tpInter[i].sTypeIntervention = TypeInterPdaCorrective;

                        //=== stocke le numéro d'intervention.
                        tpInter[i].sNoIntervention = rsPDARow["NoIntervention"] + "";

                        //LiDHoraire = rsPDA("ID HORAIRE"]

                        //==== rechrche de l'id technique (id d'inetervention pda)
                        //If bModeSuppression = False Then
                        //       lID_PDA = fc_ReturnIDpda(TypeInterPdaCorrective, rsPDA!NoIntervention, rsPDA!Nom & "", rsPDA!Matricule & "", _
                        //cID_Corrective, rsPDA!CodeImmeuble, rsPDA("ID HORAIRE"])


                        //ElseIf bModeSuppression = True Then
                        //        lID_PDA = rsPDA!PDAID_ID
                        //End If
                        //tpInter(i).sID_Technique = lID_PDA



                        //=== signale au PDA qu'il s'agit d'une crétion ou d'une suppression d'intervention.
                        if (bModeSuppression == false)
                        {
                            tpInter[i].sTypeOperation = cTypeOpCreation;
                        }
                        else if (bModeSuppression == true)
                        {
                            tpInter[i].sTypeOperation = cTypeOpSuppression;
                        }



                        //=== stocke le code immeuble.
                        tpInter[i].sCodeImmeuble = rsPDARow["CodeImmeuble"].ToString();


                        //==== info immeuble.
                        sSQL = "SELECT     CodeImmeuble, Adresse, Adresse2_Imm, Ville, CodePostal, Gardien, AngleRue  " + "  " + " From Immeuble" +
                            "  WHERE   CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsPDARow["CodeImmeuble"].ToString()) + "'";
                        modAdorsImm = new ModAdo();
                        rsImm = modAdorsImm.fc_OpenRecordSet(sSQL);

                        if (rsImm.Rows.Count > 0)
                        {
                            tpInter[i].sAdresse = rsImm.Rows[0]["Adresse"] + " " + rsImm.Rows[0]["Adresse2_Imm"] + "";
                            tpInter[i].sAdresse = General.Left(tpInter[i].sAdresse, 100);

                            tpInter[i].sCP = General.Left(rsImm.Rows[0]["CodePostal"].ToString(), 10);
                            tpInter[i].sCP = General.Left(tpInter[i].sCP, 10);

                            tpInter[i].sVille = General.Left(rsImm.Rows[0]["Ville"] + "", 50);

                            tpInter[i].sAngleRue = General.Left(rsImm.Rows[0]["AngleRue"] + "", 255);

                        }
                        modAdorsImm.Close();


                        //=== id de la localsation, a voir avec HADI
                        //tpInter(i).lIdLocalisation = 0

                        //=== nom de la localisation pour les correctives on affiche le motif.
                        //tpInter(i).sNomLocalisation = nz(rsPDA!Designation, "")

                        //=== date de création.
                        sdate = "";
                        if (General.IsDate(rsPDARow["DateSaisie"].ToString()))
                        {
                            sdate = Convert.ToString(Convert.ToDateTime(rsPDARow["DateSaisie"]));

                            ///=============> Collented by Mondir, No Need to Add TimeOfDay Coz The Convert.ToDateTime Already added the TimeValue
                            //sdate = sdate + " " + Convert.ToString(Convert.ToDateTime(rsPDARow["DateSaisie"]).TimeOfDay);

                            sdate = fc_FormatDatePDA(sdate);
                        }
                        tpInter[i].sDateCreation = sdate;



                        //=== formater la date prevue.
                        sdate = "";

                        //=== modif du 10/06/2016, la date de réalisation est prioritaire devant la date
                        //=== prevue.
                        if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateRealise"], "").ToString()))
                        {
                            if (General.IsDate(rsPDARow["DateRealise"]))
                            {
                                sdate = Convert.ToDateTime(rsPDARow["DateRealise"]).ToString(General.FormatDateSansHeureSQL);
                            }

                            if (General.IsDate(rsPDARow["HeurePrevue"]))
                            {
                                sdate = sdate + " " + Convert.ToString(Convert.ToDateTime(rsPDARow["HeurePrevue"]).TimeOfDay);
                            }
                            sdate = fc_FormatDatePDA(sdate);

                        }
                        else if (!string.IsNullOrEmpty(General.nz(rsPDARow["DatePrevue"], "").ToString()))
                        {
                            if (General.IsDate(rsPDARow["DatePrevue"]))
                            {
                                sdate = Convert.ToDateTime(rsPDARow["DatePrevue"]).ToString(General.FormatDateSansHeureSQL);
                            }

                            if (General.IsDate(rsPDARow["HeurePrevue"]))
                            {
                                sdate = sdate + " " + Convert.ToString(Convert.ToDateTime(rsPDARow["HeurePrevue"]).TimeOfDay);
                            }
                            sdate = fc_FormatDatePDA(sdate);
                        }
                        else
                        {
                            //=== si inter corrective et pas de date prévue de saisie on prends la date de création.
                            if (sTypeIntervention.ToUpper() == TypeInterPdaCorrective.ToUpper())
                            {
                                sdate = tpInter[i].sDateCreation;
                            }
                            else
                            {
                                //sdate = fc_FormatDatePDA(CStr(Now))
                                sdate = "";
                            }
                        }
                        tpInter[i].sDatePrevue = sdate;

                        //=== stocke le code état.
                        tpInter[i].sCodeEtat = General.nz(rsPDARow["CodeEtat"], "00").ToString();

                        //=== stocke le code article.
                        tpInter[i].sCodeArticle = General.nz(rsPDARow["article"], "RAS").ToString();

                        //=== stocke le libelle de l'article.
                        tpInter[i].sLibArticle = General.nz(rsPDARow["designation"], "RAS").ToString();

                        //=== stocke l'urgence.
                        lUrgence = Convert.ToInt32(General.nz(rsPDARow["Urgence"], 0));
                        tpInter[i].lUrgence = lUrgence;

                        //=== stocke si allumage oun arret demandée.
                        sAllArret = rsPDARow["ALAR_Code"] + "";

                        //=== sotcke le numero du PDA (numero de l'appareil).
                        //tpInter(i).lID_PDA = nz(rsPDA!NoPDA, 0)


                        //=== stocke le nom de l'intervenant.
                        //tpInter(i).sNomIntervenant = rsPDA!Nom & ""
                        if (!string.IsNullOrEmpty(sMatImposer))
                        {
                            tpInter[i].sMatricule = sMatImposer;
                        }
                        else
                        {
                            tpInter[i].sMatricule = rsPDARow["Intervenant"] + "";
                        }

                        //=== stocke la périodicité
                        tpInter[i].sTPP_Code = "";





                        //=== indique si il faut une saisie de compteur.
                        tpInter[i].lsaisieCompteur = Convert.ToInt32(General.nz(rsPDARow["CompteurObli"], 0));
                        if (tpInter[i].lsaisieCompteur == 1)
                        {
                            //=== temporaire on bloque la saisie des compteurs, le temps de develooper les compteurs.
                            bSaisieCompteur = true;
                            //MsgBox "Le module de saisie des compteurs est en cours de développement, seul la demande d'intervention sera envoyée.", vbInformation, ""
                            //tpInter(i).lsaisieCompteur = 0
                            //bSaisieCompteur = False
                            if (bModeSuppression == true)
                            {
                                bSaisieCompteur = false;

                            }

                        }
                        else
                        {
                            bSaisieCompteur = false;
                        }

                        //=== info complementaire (champs libre envoyé et retourné tel quel avec le PDA).

                        if (!string.IsNullOrEmpty(General.nz(rsPDARow["Situation"], "").ToString()))
                        {

                            tpInter[i].sInfoComplementaire = "Etage " + rsPDARow["Situation"].ToString() + "";
                        }
                        else
                        {
                            tpInter[i].sInfoComplementaire = "";
                        }

                        //==== Description des travaux à réaliser (Correctives uniquement).
                        tpInter[i].sCommentaireCorrective = General.Left(rsPDARow["Commentaire"].ToString(), 500) + "";

                        //=== gerer les numéro de tel (enlever le +, 10caractéres)
                        //tpInter(i).sTelephone = nz(rsPDA!TelephoneContact, 0)
                        tpInter[i].sTelephone = rsPDARow["TelephoneContact"] + "";

                        //=== modif du 22 06 2016, fonction dédié au préventives
                        //fc_infostat CLng(tpInter(i).sNoIntervention), tpInter(i).sCodeImmeuble, tpInter(i).sMatricule, sInfoStat, sINfoPrest, sInfoDevis

                        //=== gérer les infos prestations.
                        tpInter[i].sInfoPrestation = sINfoPrest;

                        //=== gérer les infosStat.
                        tpInter[i].sInfoStat = sInfoStat;

                        //=== a controler pour les statuts.
                        if (bModeSuppression == false)
                        {
                            //sSQLID = "UPDATE INTERVENTION SET CODEETAT ='TI' WHERE NoIntervention =" & RSPDA!Nointervention
                            // adocnn.Execute sSQLID
                            //                        sSQLID = "UPDATE IND_InterDuree SET IND_Statut ='" & cEnvoye & "' WHERE IND_NOAUTO = " & rsPDARow["ID HORAIRE"]
                            //                        adocnn.Execute sSQLID
                            // RSPDA!IND_Statut = cEnvoye

                        }
                        else if (bModeSuppression == true)
                        {
                            //                        sSQLID = "UPDATE INTERVENTION SET CODEETAT ='TD' WHERE NoIntervention =" & RSPDA!Nointervention
                            //                        adocnn.Execute sSQLID
                            //
                            //                        sSQLID = "UPDATE IND_InterDuree SET IND_Statut ='" & cDemandeAnnulation & "' WHERE IND_NOAUTO = " & rsPDARow["ID HORAIRE"]
                            //                        adocnn.Execute sSQLID

                        }
                        //RSPDA.Update
                        if (bSaisieCompteur == true)
                        {

                            //If IsDate(tpInter(i).sDatePrevue) Then
                            //        sDateRel = tpInter(i).sDatePrevue
                            //ElseIf IsDate(tpInter(i).sDateCreation) Then
                            //        sDateRel = tpInter(i).sDateCreation
                            //Else
                            //        sDateRel = Date
                            //End If

                            if (General.IsDate(rsPDARow["DateRealise"]))
                            {
                                sDateRel = rsPDARow["DateRealise"].ToString();
                            }
                            else if (General.IsDate(rsPDARow["DatePrevue"]))
                            {
                                sDateRel = rsPDARow["DatePrevue"].ToString();
                            }
                            else if (General.IsDate(rsPDARow["DateSaisie"]))
                            {
                                sDateRel = rsPDARow["DateSaisie"].ToString();
                            }
                            else
                            {
                                sDateRel = Convert.ToString(DateTime.Today);
                            }



                            dtPeriodeDU = Convert.ToDateTime("01/" + Convert.ToDateTime(sDateRel).Month + "/" + Convert.ToDateTime(sDateRel).Year);


                            if (Convert.ToDateTime(sDateRel).Day <= 9)
                            {
                                dtPeriodeAU = fDate.fc_DateFinMois(dtPeriodeDU.AddMonths(-1), false);
                            }
                            else
                            {
                                dtPeriodeAU = fDate.fc_DateFinMois(dtPeriodeDU, false);
                            }

                            //=== recherche de la décade.
                            using (var tmpModAdo = new ModAdo())
                                lNodecade = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT No_Decade FROM P1_Decade WHERE Date_Decade <= '" + dtPeriodeAU.ToString(General.FormatDateSQL) +
                                    "'" + " And Saison = " + dtPeriodeAU.Year +
                                    " Order by Date_Decade Desc", false, ModP1.adoP1), 1));

                            //rsPDARow["NoDecade"] = lNodecade;
                            //rsPDARow["dPeriodeDu"] = dtPeriodeDU;
                            //rsPDARow["dPeriodeAu"] = dtPeriodeAU;
                            //rsPDARow["ALAR_Code_Inter"] = sAllArret;

                            var NoIntervention = rsPDARow["NoIntervention"];

                            General.Execute($"UPDATE Intervention SET NoDecade = '{lNodecade}', dPeriodeDu = '{dtPeriodeDU}', dPeriodeAu = '{dtPeriodeAU}', ALAR_Code='{StdSQLchaine.gFr_DoublerQuote(sAllArret)}' WHERE NoIntervention = '{NoIntervention}'");

                            //modAdorsPDA.Update();


                            if (fc_InsRelCompteur(i, sAllArret, tpInter[i].sNoIntervention, tpInter[i].sCodeImmeuble, Convert.ToString(dtPeriodeDU), Convert.ToString(dtPeriodeAU), Convert.ToInt32(General.nz(rsPDARow["NoDecade"], 0)), sDateRel, tpInter) == false)
                            {
                                bSaisieCompteur = false;

                            }
                            //                                '=== envoie les localisations des compteurs.
                            //                                sSQL = "SELECT * FROM GET_PDA_REL_LOC WHERE codeimmeuble ='" & gFr_DoublerQuote(tpInter(i).sCodeImmeuble) & "'"
                            //
                            //                                Set rsCompt = fc_OpenRecordSet(sSQL)
                            //
                            //                                w = 0
                            //                                If rsCompt.EOF And rsCompt.bof Then
                            //                                        bSaisieCompteur = False
                            //                                        rsCompt.Close
                            //                                        Set rsCompt = Nothing
                            //                                End If
                            //                                If bSaisieCompteur = True Then
                            //                                        Do While rsCompt.EOF = False
                            //
                            //                                                ReDim Preserve tpInter(i).lLocComptIdLocalisation(w)
                            //                                                tpInter(i).lLocComptIdLocalisation(w) = nz(rsCompt!PDAB_Noauto, 0)
                            //
                            //                                                ReDim Preserve tpInter(i).sLocComptNomLocalisation(w)
                            //                                                tpInter(i).sLocComptNomLocalisation(w) = rsCompt!PDAB_Libelle & ""
                            //
                            //                                                '=== indique si l'etat de la chauffarie est allumé ou éteinte.
                            //                                                ReDim Preserve tpInter(i).lLocComptCHauffage(w)
                            //                                                tpInter(i).lLocComptCHauffage(w) = nz(rsCompt!Chauffage, 0)
                            //
                            //                                                '=== dermande d'allumage ou d'arret du chauffage.
                            //                                                ReDim Preserve tpInter(i).lLocComptAllArret(w)
                            //                                                If UCase(sAllArret) = UCase("ALL") Then
                            //                                                    tpInter(i).lLocComptAllArret(w) = 1
                            //                                                ElseIf UCase(sAllArret) = UCase("ARR") Then
                            //                                                    tpInter(i).lLocComptAllArret(w) = 0
                            //                                                Else
                            //                                                    tpInter(i).lLocComptAllArret(w) = -1
                            //                                                End If
                            //
                            //                                                ReDim Preserve tpInter(i).sLocComptNomRondier(w)
                            //                                                tpInter(i).sLocComptNomRondier(w) = rsCompt!Libelle & ""
                            //
                            //                                                w = w + 1
                            //                                                rsCompt.MoveNext
                            //                                        Loop
                            //                                        rsCompt.Close
                            //                                        Set rsCompt = Nothing
                            //
                            //                                End If
                            //
                            //                                '=== recherche des comteurs et derniers index à relever.
                            //                                If bSaisieCompteur = True Then
                            //                                        sSQL = "SELECT * FROM GET_PDA_REL_CPT WHERE codeimmeuble ='" & gFr_DoublerQuote(tpInter(i).sCodeImmeuble) & "'"
                            //                                        Set rsCompt = fc_OpenRecordSet(sSQL)
                            //
                            //                                        If rsCompt.EOF And rsCompt.bof Then
                            //                                                rsCompt.Close
                            //                                                Set rsCompt = Nothing
                            //                                                bSaisieCompteur = False
                            //                                        End If
                            //
                            //                                        w = 0

                            //                                        Do While rsCompt.EOF = False
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptSaison(w)
                            //                                                    tpInter(i).sComptSaison(w) = nz(rsCompt!Saison, 0)
                            //
                            //                                                    ReDim Preserve tpInter(i).lComptNumAppareil(w)
                            //                                                    tpInter(i).lComptNumAppareil(w) = nz(rsCompt!NumAppareil, 0)
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptNomCOmpteur(w)
                            //                                                    tpInter(i).sComptNomCOmpteur(w) = rsCompt!Libelle & ""
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptTypeCompteur(w)
                            //                                                    tpInter(i).sComptTypeCompteur(w) = rsCompt!Energie & ""
                            //
                            //                                                    sdate = ""
                            //                                                    If IsDate(rsCompt!DatePrecedent) Then
                            //                                                            sdate = fc_FormatDatePDA(CStr(nz(rsCompt!DatePrecedent, "")))
                            //                                                    End If
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptDateIndexPrecedent(w)
                            //                                                    tpInter(i).sComptDateIndexPrecedent(w) = sdate
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptIndexPrecedent(w)
                            //                                                    tpInter(i).sComptIndexPrecedent(w) = nz(rsCompt!sComptIndexPrecedent, 0)
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptNiveauMini(w)
                            //                                                    tpInter(i).sComptNiveauMini(w) = nz(rsCompt!Niveau_Mini, 0)
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptNiveauMaxi(w)
                            //                                                    tpInter(i).sComptNiveauMaxi(w) = nz(rsCompt!Niveau_Maxi, 0)
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptDiametreCuve(w)
                            //                                                    tpInter(i).sComptDiametreCuve(w) = 0
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptUnitePige(w)
                            //                                                    tpInter(i).sComptUnitePige(w) = rsCompt!UnitePige & ""
                            //
                            //                                                    ReDim Preserve tpInter(i).lComptIdLocalisation(w)
                            //                                                    tpInter(i).lComptIdLocalisation(w) = rsCompt!ID_Localisation
                            //
                            //                                                    ReDim Preserve tpInter(i).sComptQteLivraison(w)
                            //                                                    tpInter(i).sComptQteLivraison(w) = rsCompt!CodeImmeuble
                            //
                            //
                            //                                                    rsCompt.MoveNext
                            //                                        Loop
                            //                                        rsCompt.Close
                            //                                        Set rsCompt = Nothing
                            //                                End If

                        }


                        bDetail = false;

                        //=== modif du 13 03 2018, ajout des adresses de fcaturation (particulier).
                        fc_particulier(tpInter, X);

                        if (fc_WriteFile(tpInter, bSaisieCompteur, bDetail) == true)
                        {
                            string NoIntervention = rsPDARow["NoIntervention"].ToString();

                            int xx = 0;

                            //sSql = "Update IND_InterDuree Set PDAID_ID =" & lID_PDA & "  Where IND_NoAuto =" & LiDHoraire & "  And Nointervention =  " & tpInter(i).sNoIntervention
                            //adocnn.Execute sSql

                            if (bModeSuppression == false)
                            {
                                rsPDARow["CodeEtatPDA"] = cEnvoye;

                                xx = General.Execute($"UPDATE Intervention SET CodeEtatPDA = '{cEnvoye}' WHERE NoIntervention = {NoIntervention}");
                                // RSPDA!CodeEtat = "TI"
                                //sSQLID = "UPDATE IND_InterDuree SET IND_Statut ='" & cEnvoye & "' WHERE IND_NOAUTO = " & LiDHoraire
                                //adocnn.Execute sSQLID

                            }
                            else if (bModeSuppression == true)
                            {
                                //sSQLID = "UPDATE INTERVENTION SET CODEETAT ='TD' WHERE NoIntervention =" & rsPDA!NoIntervention
                                //adocnn.Execute sSQLID

                                //sSQLID = "UPDATE IND_InterDuree SET IND_Statut ='" & cDemandeAnnulation & "' WHERE IND_NOAUTO = " & LiDHoraire
                                //adocnn.Execute sSQLID
                                rsPDARow["CodeEtatPDA"] = cDemandeAnnulation;

                                xx = General.Execute($"UPDATE Intervention SET CodeEtatPDA = '{cDemandeAnnulation}' WHERE NoIntervention = {NoIntervention}");
                                // RSPDA!CodeEtat = "TD"
                            }

                            xx = General.Execute($"UPDATE Intervention SET EnvoyeLe = '{dtSend}', EnvoyePar = '{sSender}' WHERE NoIntervention = {NoIntervention}");

                            //rsPDARow["EnvoyeLe"] = dtSend;
                            //rsPDARow["EnvoyePar"] = sSender;
                            //int xx = modAdorsPDA.Update();
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur s'est produite lors de la transmission, veuillez essayer à nouveau de renvoyer cette intervention.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            //sSQLID = "UPDATE IND_InterDuree SET IND_Statut ='" & cNonEnvoye & "' WHERE IND_NOAUTO = " & LiDHoraire
                            //adocnn.Execute sSQLID

                            //rsPDA.CancelUpdate();
                        }

                        //RSPDA.Update

                    }

                    modAdorsPDA.Close();

                }
                else if (sTypeIntervention.ToUpper() == TypeInterPdaPreventif.ToUpper())//TESTED
                {
                    i = 0;
                    X = 0;

                    DataRow[] rsPDAFilter = null;

                    //=== modif du 05/01/2016, le critére selection n'est pas nécessaire depuis la fiche intervention.
                    if (sFicheOrigine.ToUpper() != Variable.cUserIntervention.ToUpper())
                    {

                        rsPDAFilter = rsPDA.Select(" Selection = 1");
                    }

                    if (rsPDA.Rows.Count == 0 || (rsPDAFilter != null && rsPDAFilter.Length == 0))
                    {
                        return;
                    }

                    var tmpRowArray = rsPDAFilter != null ? rsPDAFilter : rsPDA.Rows.Cast<DataRow>().ToArray();

                    foreach (DataRow rsPDARow in tmpRowArray)
                    {
                        if (General.nz(rsPDARow["Selection"], 0).ToString() == "1" || sFicheOrigine.ToUpper() == Variable.cUserIntervention.ToUpper())
                        {
                            i = 0;
                            X = 0;

                            //ReDim Preserve tpInter(i)
                            tpInter = new InterPDA[i + 1];
                            //=== stocke le type d'intervention.
                            tpInter[i].sTypeIntervention = TypeInterPdaPreventif;

                            //=== indique si il s'agit d'une inetrvention priv. ou collective(uniquement pour les interventions correctives).
                            //tpInter(i).sPrivOuCollective = ""

                            //=== stocke le numéro d'intervention.
                            tpInter[i].sNoIntervention = rsPDARow["NoIntervention"].ToString();

                            //==== rechrche de l'id technique (id d'inetervention pda)
                            if (bModeSuppression == false)
                            {
                                //lID_PDA = fc_ReturnIDpda(TypeInterPdaPreventif, rsPDA!Nointervention, rsPDA!Nom & "", rsPDA!Intervenant, _
                                //cID_Preventive, rsPDA!CodeImmeuble, 0)

                            }
                            else if (bModeSuppression == true)
                            {
                                // lID_PDA = rsPDA!PDAID_ID
                            }
                            //tpInter(i).sID_Technique = lID_PDA





                            //=== signale au PDA qu'il s'agit d'une crétion ou d'une suppression d'intervention.
                            if (bModeSuppression == false)
                            {
                                tpInter[i].sTypeOperation = cTypeOpCreation;
                            }
                            else if (bModeSuppression == true)
                            {
                                tpInter[i].sTypeOperation = cTypeOpSuppression;
                            }

                            //=== numéro de lot.
                            //tpInter(i).lNoLot = 0

                            //=== numéro d'ordre.
                            //tpInter(i).lNoOrdre = 0

                            //=== stocke le code immeuble.
                            tpInter[i].sCodeImmeuble = rsPDARow["CodeImmeuble"].ToString();

                            //==== info immeuble.
                            sSQL = "SELECT     CodeImmeuble, Adresse, Adresse2_Imm, Ville, CodePostal, Gardien, AngleRue  " + "  " + " From Immeuble" + "  WHERE   CodeImmeuble = '" +
                                StdSQLchaine.gFr_DoublerQuote(rsPDARow["CodeImmeuble"].ToString()) + "'";

                            modAdorsImm = new ModAdo();
                            rsImm = modAdorsImm.fc_OpenRecordSet(sSQL);

                            if (rsImm.Rows.Count > 0)
                            {
                                tpInter[i].sAdresse = rsImm.Rows[0]["Adresse"] + " " + rsImm.Rows[0]["Adresse2_Imm"] + "";
                                tpInter[i].sAdresse = General.Left(tpInter[i].sAdresse, 100);
                                tpInter[i].sCP = General.Left(rsImm.Rows[0]["CodePostal"].ToString(), 10);
                                tpInter[i].sCP = General.Left(tpInter[i].sCP, 10);
                                tpInter[i].sVille = General.Left(rsImm.Rows[0]["Ville"] + "", 50);
                                tpInter[i].sAngleRue = General.Left(rsImm.Rows[0]["AngleRue"] + "", 255);
                                //tpInter(i).sResidence = Left(rsImm!Imm_Residence & "", 100)
                                //====  1 si gardien present, 0 si absent.
                                if (!string.IsNullOrEmpty(General.nz(rsImm.Rows[0]["Gardien"], "").ToString()) || !string.IsNullOrEmpty(General.nz(rsImm.Rows[0]["Gardien"].ToString(), "").ToString()))
                                {
                                    lPresenceGardien = 1;
                                }
                                else
                                {
                                    lPresenceGardien = 0;
                                }

                            }
                            modAdorsImm.Close();



                            //=== formater la date de création.
                            sdate = "";
                            //==== modif du 17 05 2016, on utilise la date visite.
                            //sdate = fc_FormatDatePDA(CStr(nz(rsPDA!DatePrevue, "")))
                            if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateVisite"], "").ToString()))
                            {

                                if (General.IsDate(rsPDARow["DateVisite"]))
                                {
                                    sdate = Convert.ToDateTime(rsPDARow["DateVisite"]).ToString("dd/MM/yyyy HH:mm:ss");

                                    //==============> Code Modifié par Mondir
                                    //sdate = sdate + " " + Convert.ToString(Convert.ToDateTime(rsPDARow["DateVisite"]).TimeOfDay);
                                }

                                sdate = fc_FormatDatePDA(sdate);

                            }
                            else if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateRealise"], "").ToString()))
                            {
                                if (General.IsDate(rsPDARow["DateRealise"]))
                                {
                                    sdate = Convert.ToDateTime(rsPDARow["DateRealise"]).ToString("dd/MM/yyyy");
                                }

                                if (General.IsDate(rsPDARow["HeurePrevue"]))
                                {
                                    sdate = sdate + " " + Convert.ToDateTime(rsPDARow["HeurePrevue"]).ToString("HH:mm:ss");
                                }
                                sdate = fc_FormatDatePDA(sdate);
                            }
                            else if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateSaisie"], "").ToString()))
                            {
                                if (General.IsDate(rsPDARow["DateRealise"]))
                                {
                                    sdate = Convert.ToDateTime(rsPDARow["DateRealise"]).ToString("dd/MM/yyyy");
                                }

                                if (General.IsDate(rsPDARow["HeurePrevue"]))
                                {
                                    sdate = sdate + " " + Convert.ToDateTime(rsPDARow["HeurePrevue"]).ToString("HH:mm:ss");
                                }
                                sdate = fc_FormatDatePDA(sdate);

                            }
                            //sdate = fc_FormatDatePDA(CStr(nz(rsPDA!DateVisite, "")))

                            tpInter[i].sDateCreation = sdate;
                            //tpInter(i).sDatePrevue = sdate


                            //=== date prévue
                            sdate = "";
                            if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateRealise"], "").ToString()))
                            {
                                if (General.IsDate(rsPDARow["DateRealise"]))
                                {
                                    sdate = Convert.ToDateTime(rsPDARow["DateRealise"]).ToString("dd/MM/yyyy");
                                }

                                if (General.IsDate(rsPDARow["HeurePrevue"]))
                                {
                                    sdate = sdate + " " + Convert.ToDateTime(rsPDARow["HeurePrevue"]).ToString("HH:mm:ss");
                                }
                                sdate = fc_FormatDatePDA(sdate);

                                //ElseIf nz(rsPDA!DatePrevue, "") <> "" Then
                                //    If IsDate(rsPDA!DatePrevue) Then
                                //        sdate = CStr(DateValue(rsPDA!DatePrevue))
                                //    End If
                                //
                                //    If IsDate(rsPDA!HeurePrevue) Then
                                //        sdate = sdate & " " & CStr(TimeValue(rsPDA!HeurePrevue))
                                //    End If
                                //    sdate = fc_FormatDatePDA(sdate)
                            }

                            tpInter[i].sDatePrevue = sdate;

                            //=== stocke le matricule.
                            if (!string.IsNullOrEmpty(sMatImposer))
                            {
                                tpInter[i].sMatricule = sMatImposer;
                            }
                            else
                            {
                                tpInter[i].sMatricule = rsPDARow["Intervenant"] + "";
                            }

                            //=== stocke le code état.
                            tpInter[i].sCodeEtat = rsPDARow["CodeEtat"] + "";


                            //=== stocke la périodicité
                            // tpInter(i).sTPP_Code = fc_Periodicite(nz(RSPDA!TPP_Code, ""))
                            tpInter[i].sTPP_Code = "";

                            //=== par defaut les visites d'entretien ne sont pas urgentes.
                            tpInter[i].lUrgence = 0;


                            //=== voir si il est neccesaire de saisir un numéro de tel pour les visites d'entretien.
                            tpInter[i].sTelephone = "";

                            //=== charge l'article.
                            tpInter[i].sCodeArticle = rsPDARow["article"] + "";


                            //=== carge la designation de l'article.
                            tpInter[i].sLibArticle = rsPDARow["designation"] + "";


                            //=== modif du 10 06 2016, la date de création est remplacé par la date de visite.
                            //=== date de création.
                            //sdate = ""
                            //If IsDate(rsPDA!DateSaisie) Then
                            //     sdate = CStr(DateValue(rsPDA!DateSaisie))
                            //     sdate = sdate & " " & CStr(TimeValue(rsPDA!DateSaisie))
                            //
                            //     sdate = fc_FormatDatePDA(sdate)
                            //Else
                            //    '=== on attribue une valeur par défaut si la date de cration est null, modif du 17/05/2016.
                            //    sdate = fc_FormatDatePDA(Date)
                            //End If
                            // tpInter(i).sDateCreation = sdate


                            //=== indique si il faut une saisie de compteur.
                            tpInter[i].lsaisieCompteur = Convert.ToInt32(General.nz(rsPDARow["CompteurObli"], 0));
                            if (tpInter[i].lsaisieCompteur == 1)
                            {
                                //bSaisieCompteur = True
                                //  MsgBox "Le module de saisie des compteurs est en cours de développement, seul la demande d'intervention sera envoyée.", vbInformation, ""
                                // tpInter(i).lsaisieCompteur = 0
                                bSaisieCompteur = true;

                                if (bModeSuppression == true)
                                {
                                    bSaisieCompteur = false;

                                }
                            }
                            else
                            {
                                bSaisieCompteur = false;
                            }

                            //                    tpInter(i).lsaisieCompteur = 0
                            //                    bSaisieCompteur = False

                            //=== envoie des infos complemantaires (ne sert pas pour le moment)
                            tpInter[i].sInfoComplementaire = "";

                            //=== intervention suspendu
                            //tpInter(i).sSuspendu = ""

                            //==== Description des travaux à réaliser (Correctives uniquement).
                            //tpInter(i).sCommentaireCorrective = ""

                            tpInter[i].sCommentaireCorrective = General.Left(rsPDARow["Commentaire"].ToString(), 500) + "";
                            tpInter[i].sCommentaireCorrective = tpInter[i].sCommentaireCorrective.Replace("--", cRetChario);

                            //sSQL = "SELECT     NoInterventionVisite, NoIntervention, CAI_Libelle, EQM_Code, CompteurObli " _
                            //& " From OperationP2" _
                            //& " WHERE     NoInterventionVisite = " & rsPDA!Nointervention

                            //=== stocke le détail de l'intervention non réalisé.
                            sSQL = "SELECT DISTINCT PDAB_Noauto, PDAB_Libelle, GAi_ID, GAI_LIbelle, CAI_code, CAI_Libelle, ICC_Noauto " + " From OperationP2 " + " WHERE     NumFicheStandard = " +
                                General.nz(rsPDARow["Numfichestandard"], 0) + " AND (INT_Realisee IS NULL OR INT_Realisee = 0)" + " ORDER BY PDAB_Noauto, GAi_ID, CAI_code";

                            modAdorsDet = new ModAdo();
                            rsDet = modAdorsDet.fc_OpenRecordSet(sSQL);
                            X = 0;
                            bDetail = false;

                            foreach (DataRow rsDetRow in rsDet.Rows)
                            {
                                bDetail = true;

                                Array.Resize(ref tpInter[i].lIdLocalisation, X + 1);
                                tpInter[i].lIdLocalisation[X] = Convert.ToInt32(rsDetRow["PDAB_Noauto"]);

                                Array.Resize(ref tpInter[i].sNomLocalisation, X + 1);
                                tpInter[i].sNomLocalisation[X] = rsDetRow["PDAB_Libelle"] + "";

                                Array.Resize(ref tpInter[i].lIdEquipement, X + 1);
                                tpInter[i].lIdEquipement[X] = Convert.ToInt32(rsDetRow["GAi_ID"]);

                                Array.Resize(ref tpInter[i].sNomEquipement, X + 1);
                                tpInter[i].sNomEquipement[X] = rsDetRow["GAI_LIbelle"] + "";

                                //ReDim Preserve tpInter(i).sIdComposant(X)
                                //tpInter(i).sIdComposant(X) = rsDet!CAI_Code & ""
                                Array.Resize(ref tpInter[i].lIdComposant, X + 1);
                                tpInter[i].lIdComposant[X] = rsDetRow["ICC_Noauto"] + "";



                                Array.Resize(ref tpInter[i].sNomComposant, X + 1);
                                tpInter[i].sNomComposant[X] = rsDetRow["CAI_Libelle"] + "";

                                X = X + 1;

                            }


                            sSQL = "SELECT     PDAB_Noauto, GAi_ID, GAI_LIbelle, CAI_code," + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle, NoIntervention AS ID, " + " INT_Realisee, TPP_Code, ICC_Noauto " +
                                " From OperationP2 " + " WHERE  NumFicheStandard = " + rsPDARow["Numfichestandard"] + " AND (INT_Realisee IS NULL OR INT_Realisee = 0)";

                            modAdorsDet = new ModAdo();
                            rsDet = modAdorsDet.fc_OpenRecordSet(sSQL);
                            X = 0;
                            bDetail = false;

                            foreach (DataRow rsDetRow in rsDet.Rows)
                            {
                                bDetail = true;

                                Array.Resize(ref tpInter[i].lDetIdControle, X + 1);
                                tpInter[i].lDetIdControle[X] = Convert.ToInt32(rsDetRow["Id"]);

                                Array.Resize(ref tpInter[i].lDetIdLocalisation, X + 1);
                                tpInter[i].lDetIdLocalisation[X] = Convert.ToInt32(rsDetRow["PDAB_Noauto"]);

                                //ReDim Preserve tpInter(i).sDetIdComposant(X)
                                //tpInter(i).sDetIdComposant(X) = rsDet!CAI_Code
                                Array.Resize(ref tpInter[i].lDetIdComposant, X + 1);
                                tpInter[i].lDetIdComposant[X] = rsDetRow["ICC_Noauto"].ToString();


                                Array.Resize(ref tpInter[i].lDetIdEquipement, X + 1);
                                tpInter[i].lDetIdEquipement[X] = Convert.ToInt32(rsDetRow["GAi_ID"] + "");

                                Array.Resize(ref tpInter[i].sDetNomControle, X + 1);
                                tpInter[i].sDetNomControle[X] = rsDetRow["CTR_Libelle"] + "";

                                Array.Resize(ref tpInter[i].sDetMoyen, X + 1);
                                tpInter[i].sDetMoyen[X] = rsDetRow["MOY_Libelle"] + "";

                                Array.Resize(ref tpInter[i].sDetAnomalie, X + 1);
                                tpInter[i].sDetAnomalie[X] = rsDetRow["ANO_Libelle"] + "";

                                Array.Resize(ref tpInter[i].sDetOperation, X + 1);
                                tpInter[i].sDetOperation[X] = rsDetRow["OPE_Libelle"] + "";

                                Array.Resize(ref tpInter[i].sFrequence, X + 1);
                                tpInter[i].sFrequence[X] = fc_ReturnPeriodicite(rsDetRow["TPP_Code"] + "");


                                Array.Resize(ref tpInter[i].lReportee, X + 1);
                                if (bInterDuplice == true)
                                {
                                    //=== si la fiche d'intervention est dupliqué, on considére
                                    //=== les gammes ont été reporté (non realisées lors de la derniere visite).
                                    tpInter[i].lReportee[X] = 1;
                                }
                                else
                                {
                                    tpInter[i].lReportee[X] = 0;
                                }


                                X = X + 1;

                            }


                            if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateVisite"], "").ToString()))
                            {

                                if (General.IsDate(rsPDARow["DateVisite"]))
                                {
                                    dtDebut = Convert.ToDateTime("01/" + Convert.ToDateTime(rsPDARow["DateVisite"]).Month + "/" + Convert.ToDateTime(rsPDARow["DateVisite"]).Year);
                                    dtFin = fDate.fc_FinDeMois(Convert.ToString(Convert.ToDateTime(rsPDARow["DateVisite"]).Month), Convert.ToString(Convert.ToDateTime(rsPDARow["DateVisite"]).Year));
                                }

                            }
                            else if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateRealise"], "").ToString()))
                            {
                                if (General.IsDate(rsPDARow["DateRealise"]))
                                {
                                    dtDebut = Convert.ToDateTime("01/" + Convert.ToDateTime(rsPDARow["DateRealise"]).Month + "/" + Convert.ToDateTime(rsPDARow["DateRealise"]).Year);
                                    dtFin = fDate.fc_FinDeMois(Convert.ToString(Convert.ToDateTime(rsPDARow["DateRealise"]).Month), Convert.ToString(Convert.ToDateTime(rsPDARow["DateRealise"]).Year));
                                }

                            }
                            else if (!string.IsNullOrEmpty(General.nz(rsPDARow["DateSaisie"], "").ToString()))
                            {
                                dtDebut = Convert.ToDateTime("01/" + Convert.ToDateTime(rsPDARow["DateSaisie"]).Month + "/" + Convert.ToDateTime(rsPDARow["DateSaisie"]).Year);
                                dtFin = fDate.fc_FinDeMois(Convert.ToString(Convert.ToDateTime(rsPDARow["DateSaisie"]).Month), Convert.ToString(Convert.ToDateTime(rsPDARow["DateSaisie"]).Year));
                            }

                            fc_infostat(Convert.ToInt32(tpInter[i].sNoIntervention), tpInter[i].sMatricule, tpInter[i].sCodeImmeuble, ref sInfoStat, ref sINfoPrest, ref sInfoDevis, dtDebut, dtFin);

                            //=== gérer les infos prestations.
                            tpInter[i].sInfoPrestation = sINfoPrest;

                            //=== gérer les infosStat.
                            tpInter[i].sInfoStat = sInfoStat + cRetChario + sInfoDevis;

                            //                    If bModeSuppression = False Then
                            //                        RSPDA!CodeEtatPDA = cEnvoye
                            //                        RSPDA!CodeEtat = "TI"
                            //                    ElseIf bModeSuppression = True Then
                            //                        RSPDA!CodeEtatPDA = cDemandeAnnulation
                            //                        RSPDA!CodeEtat = "TD"
                            //                    End If
                            //
                            //                    RSPDA!EnvoyeLe = dtSend
                            //                    RSPDA!EnvoyePar = sSender
                            //                    RSPDA.Update

                            if (bSaisieCompteur == true)
                            {

                                if (General.IsDate(rsPDARow["DateRealise"]))
                                {
                                    sDateRel = rsPDARow["DateRealise"].ToString();
                                }
                                else if (General.IsDate(rsPDARow["DatePrevue"]))
                                {
                                    sDateRel = rsPDARow["DatePrevue"].ToString();
                                }
                                else if (General.IsDate(rsPDARow["DateSaisie"]))
                                {
                                    sDateRel = rsPDARow["DateSaisie"].ToString();
                                }
                                else
                                {
                                    sDateRel = Convert.ToString(DateTime.Today);
                                }



                                dtPeriodeDU = Convert.ToDateTime("01/" + Convert.ToDateTime(sDateRel).Month + "/" + Convert.ToDateTime(sDateRel).Year);


                                if (Convert.ToDateTime(sDateRel).Day <= 9)
                                {
                                    dtPeriodeAU = fDate.fc_DateFinMois(dtPeriodeDU.AddMonths(-1), false);
                                }
                                else
                                {
                                    dtPeriodeAU = fDate.fc_DateFinMois(dtPeriodeDU, false);
                                }

                                //=== recherche de la décade.
                                using (var tmpModAdo = new ModAdo())
                                    lNodecade = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT No_Decade FROM P1_Decade WHERE Date_Decade <= '" +
                                        dtPeriodeAU.ToString(General.FormatDateSQL) + "'" + " And Saison = " + dtPeriodeAU.Year + " Order by Date_Decade Desc", false, ModP1.adoP1), 1));

                                //rsPDARow["NoDecade"] = lNodecade;
                                //rsPDARow["dPeriodeDu"] = dtPeriodeDU;
                                //rsPDARow["dPeriodeAu"] = dtPeriodeAU;
                                //rsPDARow["ALAR_Code_Inter"] = sAllArret;

                                var NoIntervention = rsPDARow["NoIntervention"];
                                int xx = General.Execute($"UPDATE Intervention SET NoDecade = '{lNodecade}', dPeriodeDu = '{dtPeriodeDU}', dPeriodeAu = '{dtPeriodeAU}'," +
                                    $" ALAR_Code= '{sAllArret}' WHERE NoIntervention = '{NoIntervention}'");

                                //modAdorsPDA.Update();


                                if (fc_InsRelCompteur(i, sAllArret, tpInter[i].sNoIntervention, tpInter[i].sCodeImmeuble, Convert.ToString(dtPeriodeDU),
                                    Convert.ToString(dtPeriodeAU), Convert.ToInt32(General.nz(rsPDARow["NoDecade"], 0)), sDateRel, tpInter) == false)
                                {
                                    bSaisieCompteur = false;

                                }
                            }


                            i = i + 1;
                            modAdorsDet.Close();
                        }

                        if (fc_WriteFile(tpInter, bSaisieCompteur, bDetail) == true)//tESTED
                        {
                            int xx = 0;
                            var NoIntervention = rsPDARow["NoIntervention"];

                            if (bModeSuppression == false)
                            {
                                rsPDARow["CodeEtatPDA"] = cEnvoye;
                                rsPDARow["CodeEtat"] = "0A";

                                xx = General.Execute($"UPDATE Intervention SET CodeEtatPDA = '{cEnvoye}', CodeEtat = '0A' WHERE NoIntervention = '{NoIntervention}'");
                            }
                            else if (bModeSuppression == true)
                            {
                                rsPDARow["CodeEtatPDA"] = cDemandeAnnulation;
                                rsPDARow["CodeEtat"] = "00";

                                xx = General.Execute($"UPDATE Intervention SET CodeEtatPDA = '{cDemandeAnnulation}', CodeEtat = '00' WHERE NoIntervention = '{NoIntervention}'");
                            }

                            rsPDARow["EnvoyeLe"] = dtSend;
                            rsPDARow["EnvoyePar"] = sSender;

                            xx = General.Execute($"UPDATE Intervention SET EnvoyeLe = '{dtSend}', EnvoyePar = '{sSender}' WHERE NoIntervention = '{NoIntervention}'");

                            if (bInterDuplice == false)
                            {
                                rsPDARow["ModifierLe"] = DateTime.Now;
                                rsPDARow["ModifierPar"] = sSender;

                                General.Execute($"UPDATE Intervention SET ModifierLe = '{DateTime.Now}', ModifierPar = '{sSender}' WHERE NoIntervention = '{NoIntervention}'");
                            }

                            //modAdorsPDA.Update();
                        }

                    }

                    modAdorsPDA.Close();
                    modAdorsDet?.Close();
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modPDA;fc_SendRamonage");
            }
        }

        private static string fc_FormatDatePDA(string sdate)
        {
            string functionReturnValue = null;
            System.DateTime dtDate = default(System.DateTime);


            if (!General.IsDate(sdate))
            {
                functionReturnValue = "";
            }
            else
            {

                dtDate = Convert.ToDateTime(sdate);

                functionReturnValue = dtDate.Year + "" + dtDate.Month.ToString("00") + "" + dtDate.Day.ToString("00") + " " + dtDate.Hour.ToString("00") + ":" +
                    dtDate.Minute.ToString("00") + ":" + dtDate.Second.ToString("00");



            }
            return functionReturnValue;



        }

        private static bool fc_InsRelCompteur(int i, string sAllArret, string lNoIntervention, string sCodeImmeuble, string sDPeriodeDu, string sDPeriodeAu, int lNodecade, string sDateRel, InterPDA[] tpInter)
        {
            bool functionReturnValue = false;

            bool bSaisieCompteur = false;
            DataTable rsCompt = default(DataTable);
            ModAdo modAdorsCompt = null;
            int w = 0;
            string sdate = "";
            string sPDAB_Libelle = "";
            string sTypeCalcul = "";

            try
            {
                functionReturnValue = true;

                //=== envoie les localisations des compteurs.
                General.sSQL = "SELECT * FROM GET_PDA_REL_LOC WHERE codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sCodeImmeuble) + "' ORDER BY PDAB_Libelle ";

                modAdorsCompt = new ModAdo();
                rsCompt = modAdorsCompt.fc_OpenRecordSet(General.sSQL);

                w = 0;
                if (rsCompt.Rows.Count == 0)
                {
                    bSaisieCompteur = false;
                    modAdorsCompt.Close();
                    functionReturnValue = false;
                }
                else
                {
                    bSaisieCompteur = true;
                }
                if (bSaisieCompteur == true)
                {
                    foreach (DataRow rsComptRow in rsCompt.Rows)
                    {
                        if (sPDAB_Libelle.ToUpper() != rsComptRow["PDAB_Libelle"].ToString().ToUpper())
                        {

                            sPDAB_Libelle = rsComptRow["PDAB_Libelle"] + "";

                            Array.Resize(ref tpInter[i].lLocComptIdLocalisation, w + 1);
                            tpInter[i].lLocComptIdLocalisation[w] = Convert.ToInt32(General.nz(rsComptRow["PDAB_Noauto"], 0));

                            Array.Resize(ref tpInter[i].sLocComptNomLocalisation, w + 1);
                            tpInter[i].sLocComptNomLocalisation[w] = rsComptRow["PDAB_Libelle"] + "";

                            //=== indique si l'etat de la chauffarie est allumé ou éteinte.
                            Array.Resize(ref tpInter[i].lLocComptCHauffage, w + 1);
                            tpInter[i].lLocComptCHauffage[w] = Convert.ToInt32(General.nz(rsComptRow["Chauffage"], 0));

                            //=== dermande d'allumage ou d'arret du chauffage.
                            Array.Resize(ref tpInter[i].lLocComptAllArret, w + 1);
                            if (sAllArret.ToUpper() == "ALL".ToUpper())
                            {
                                tpInter[i].lLocComptAllArret[w] = 1;
                            }
                            else if (sAllArret.ToUpper() == "ARR".ToUpper())
                            {
                                tpInter[i].lLocComptAllArret[w] = 0;
                            }
                            else
                            {
                                tpInter[i].lLocComptAllArret[w] = -1;
                            }

                            Array.Resize(ref tpInter[i].sLocComptNomRondier, w + 1);
                            tpInter[i].sLocComptNomRondier[w] = rsComptRow["Libelle"] + "";

                            w = w + 1;
                        }
                        Ins_DernierRel(Convert.ToInt32(General.nz(rsComptRow["NumAppareil"], 0)), rsComptRow["LibAppareil"] + "", Convert.ToString(lNoIntervention), sCodeImmeuble, sDPeriodeDu, sDPeriodeAu, lNodecade, sDateRel, General.nz(rsComptRow["OrigineP1"], 0).ToString(), rsComptRow["DiametreCuve"] + "");


                    }
                    modAdorsCompt.Close();

                }

                //=== recherche des comteurs et derniers index à relever.
                if (bSaisieCompteur == true)
                {
                    //sSql = "SELECT * FROM GET_PDA_REL_CPT WHERE codeimmeuble ='" & gFr_DoublerQuote(tpInter(i).sCodeImmeuble) & "'"
                    General.sSQL = "SELECT * FROM GET_PDA_REL_CPT WHERE  NoIntervention =" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sNoIntervention) + "";
                    modAdorsCompt = new ModAdo();
                    rsCompt = modAdorsCompt.fc_OpenRecordSet(General.sSQL);

                    if (rsCompt.Rows.Count == 0)
                    {
                        modAdorsCompt.Close();
                        bSaisieCompteur = false;
                        functionReturnValue = false;
                    }

                    w = 0;

                    foreach (DataRow rsComptRow in rsCompt.Rows)
                    {

                        Array.Resize(ref tpInter[i].sComptSaison, w + 1);
                        tpInter[i].sComptSaison[w] = Convert.ToString(General.nz(rsComptRow["Saison"], 0));

                        Array.Resize(ref tpInter[i].lComptNumAppareil, w + 1);
                        tpInter[i].lComptNumAppareil[w] = Convert.ToInt32(General.nz(rsComptRow["NumAppareil"], 0));

                        Array.Resize(ref tpInter[i].sComptNomCOmpteur, w + 1);
                        tpInter[i].sComptNomCOmpteur[w] = rsComptRow["Libelle"] + "";

                        //== type de calcul
                        Array.Resize(ref tpInter[i].sComptTypeCompteur, w + 1);
                        // sTypeCalcul = rsCompt!Type_Compteur

                        using (var tmpModAdo = new ModAdo())
                            sTypeCalcul = tmpModAdo.fc_ADOlibelle("SELECT Type From P1_TypeCpt WHERE  CODE = " + General.nz(rsComptRow["Type_Compteur"], 0));

                        var sTypeCalculToUpper = sTypeCalcul.ToUpper();

                        switch (sTypeCalculToUpper)
                        {
                            case "GAZ":
                            case "ECS":
                                sTypeCalcul = "CONSO";
                                break;

                            case "FOD":
                            case "BOIS":
                            case "FUEL":
                                sTypeCalcul = cCalCUVE;
                                break;

                            case "APPOINT":
                                sTypeCalcul = cCalAPPOINT;
                                break;

                            default:
                                //#LD (01/07/2016) : Par defaut : le type de compteur doit être "CONSO" (cas le plus répandu)
                                //             sTypeCalcul = UCase(nz(sTypeCalcul, cCalCONSO))
                                sTypeCalcul = "CONSO";
                                break;
                        }
                        tpInter[i].sComptTypeCompteur[w] = sTypeCalcul;

                        sdate = "";
                        if (General.IsDate(rsComptRow["DatePrecedent"].ToString()))
                        {
                            sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsComptRow["DatePrecedent"], "")));
                        }
                        else
                        {
                            sdate = fc_FormatDatePDA(Convert.ToString(DateTime.Today));
                        }

                        Array.Resize(ref tpInter[i].sComptDateIndexPrecedent, w + 1);
                        tpInter[i].sComptDateIndexPrecedent[w] = sdate;

                        Array.Resize(ref tpInter[i].sComptIndexPrecedent, w + 1);
                        tpInter[i].sComptIndexPrecedent[w] = General.nz(rsComptRow["IndexPrecedent"], 0).ToString();

                        Array.Resize(ref tpInter[i].sComptNiveauMini, w + 1);
                        tpInter[i].sComptNiveauMini[w] = General.nz(rsComptRow["Niveau_Mini"], 0).ToString();

                        Array.Resize(ref tpInter[i].sComptNiveauMaxi, w + 1);
                        tpInter[i].sComptNiveauMaxi[w] = General.nz(rsComptRow["Niveau_Maxi"], 0).ToString();

                        Array.Resize(ref tpInter[i].sComptDiametreCuve, w + 1);
                        tpInter[i].sComptDiametreCuve[w] = Convert.ToString(0);

                        Array.Resize(ref tpInter[i].sComptUnitePige, w + 1);
                        tpInter[i].sComptUnitePige[w] = rsComptRow["UnitePige"] + "";

                        Array.Resize(ref tpInter[i].lComptIdLocalisation, w + 1);
                        tpInter[i].lComptIdLocalisation[w] = Convert.ToInt32(rsComptRow["ID_Localisation"]);

                        Array.Resize(ref tpInter[i].sComptQteLivraison, w + 1);
                        tpInter[i].sComptQteLivraison[w] = "";
                        //rsCompt!CodeImmeuble

                        w = w + 1;
                    }
                    modAdorsCompt.Close();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modPDA;fc_InsRelCompteur");
                return functionReturnValue;
            }

        }

        public static object Ins_DernierRel(int NumApp, string Lib_Renamed, string sNoIntervention, string sCodeImmeuble, string sDPeriodeDu, string sDPeriodeAu, int lNodecade,
             string sDateRel, string sOrigineP1 = "0", string sDiametreCuve = "", bool bFind = false)
        {
            object functionReturnValue = null;

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            double Liv = 0;
            string NoDecade = null;
            string sNoFichierGraphe = null;
            string sEtatReleve = null;
            string sDatePrevue = null;
            System.DateTime dPeriodeDu = default(System.DateTime);
            System.DateTime dPeriodeAu = default(System.DateTime);
            string sPeriode = null;
            string[] tabPeriode = null;
            string sType = null;
            string SQL = null;

            try
            {
                Liv = 0;

                modAdors = new ModAdo();

                //=== Date du nouveau relevé.
                if (bFind == true || string.IsNullOrEmpty(sDateRel))
                {
                    using (var tmpModAdo = new ModAdo())
                        sDatePrevue = tmpModAdo.fc_ADOlibelle("SELECT DatePrevue FROM Intervention WHERE NoIntervention = " + sNoIntervention);
                    //& " AND CodeUO = '" & strCodeUO & "'")

                    if (!General.IsDate(sDatePrevue))
                    {
                        sDatePrevue = Convert.ToString(DateTime.Today);
                    }
                }
                else if (!General.IsDate(sDateRel))
                {
                    sDateRel = Convert.ToString(DateTime.Today);
                }
                else
                {
                    sDatePrevue = sDateRel;
                }

                using (var tmpModAdo = new ModAdo())
                    sNoFichierGraphe = tmpModAdo.fc_ADOlibelle("SELECT NoFichierGraphe FROM P12000 WHERE CodeAffaire = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'");
                // AND CodeNumOrdre = '" & sCodeNumOrdre & "'", , adoP1)

                using (var tmpModAdo = new ModAdo())
                    sType = tmpModAdo.fc_ADOlibelle("SELECT P1_TypeCpt.Type  FROM P1_TypeCpt INNER JOIN Imm_Appareils ON P1_TypeCpt.CODE = Imm_Appareils.Type_Compteur" + " WHERE Imm_Appareils.NumAppareil = " +
                        General.nz(NumApp, "0"));

                sEtatReleve = "AT";
                //gfr_liaison("EtatReleveBase", "AT", False, "Compteurs")

                dPeriodeDu = Convert.ToDateTime("01/01/1900");
                dPeriodeAu = Convert.ToDateTime("01/01/1900");


                if (bFind == true)
                {
                    sPeriode = "SELECT dPeriodeDu, dPeriodeAu, COALESCE(NoDecade, 0) AS Decade FROM Intervention WHERE NoIntervention = " + sNoIntervention + "";

                    rs = modAdors.fc_OpenRecordSet(sPeriode);

                    if (rs.Rows.Count > 0)
                    {
                        if (General.IsDate(rs.Rows[0]["dPeriodeDu"]))
                        {
                            dPeriodeDu = Convert.ToDateTime(sDPeriodeDu);
                        }

                        if (General.IsDate(rs.Rows[0]["dPeriodeAu"]))
                        {
                            dPeriodeAu = Convert.ToDateTime(sDPeriodeAu);
                        }

                        lNodecade = Convert.ToInt32(General.nz(rs.Rows[0]["NoDecade"], 0));
                    }
                    else
                    {

                    }

                    //Si pas de décade sur l'intervention : calcul du numéro de décade à partir de la période fin de l'intervention
                    if (lNodecade == 0)
                    {
                        using (var tmpModAdo = new ModAdo())
                            lNodecade = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT No_Decade FROM P1_Decade WHERE Date_Decade <= '" +
                                Convert.ToDateTime(dPeriodeAu).ToString(General.FormatDateSQL) + "'" + " And Saison = " + dPeriodeAu.Year + " Order by Date_Decade Desc", false, ModP1.adoP1), 1));
                    }
                }
                else
                {

                    if (General.IsDate(sDPeriodeDu))
                    {
                        dPeriodeDu = Convert.ToDateTime(sDPeriodeDu);
                    }

                    if (General.IsDate(sDPeriodeAu))
                    {
                        dPeriodeAu = Convert.ToDateTime(sDPeriodeAu);
                    }


                    //Si pas de décade sur l'intervention : calcul du numéro de décade à partir de la période fin de l'intervention
                    if (lNodecade == 0)
                    {
                        using (var tmpModAdo = new ModAdo())
                            lNodecade = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT No_Decade FROM P1_Decade WHERE Date_Decade <= '" + dPeriodeAu.ToString(General.FormatDateSQL) +
                                "'" + " And Saison = " + dPeriodeAu.Year + " Order by Date_Decade Desc", false, ModP1.adoP1), 1));
                    }
                }

                //=== modif du 30 10 2017, controle si le relevé existe.
                if (fc_CrlReleve(sCodeImmeuble, NumApp, Convert.ToInt32(sNoIntervention)) == true)
                {
                    return functionReturnValue;
                }
                //=== fin modif du 30 10 2017.

                if (sOrigineP1 == "1")
                {

                    SQL = "SELECT TOP 1 * " + " FROM P1_Releve" + " WHERE  " + " NumAppareil = " + NumApp + " " + " ORDER BY P1_Releve.PeriodeAu DESC, Saison DESC";

                    ConnectExtern connectExternrs = new ConnectExtern();
                    rs = connectExternrs.gFr_ADO_Openrecordset(SQL, ModP1.adoP1);
                    if (rs.Rows.Count > 0)
                    {

                        if (General.IsDate(rs.Rows[0]["PeriodeAu"] + ""))
                        {
                            Nbj = (int)(Convert.ToDateTime(sDatePrevue) - Convert.ToDateTime(rs.Rows[0]["PeriodeAu"])).TotalDays;
                        }
                        else
                        {
                            Nbj = 0;
                        }

                        SQL = "INSERT INTO INT_Releve ( " + " Etat, NoIntervention, NoFichierGraphe, NumAppareil, Libel_App, " +
                            "CodeUO, CodeAffaire, CodeNumOrdre, CodeChaufferie," + " Periodedu, PeriodeAu, Njours, Saison, indexdebut " +
                            " , NoDecade, chauffage, Livraison, ChkLiv, dPeriodeDu, dPeriodeAu, DiametreCuve)";
                        SQL = SQL + " VALUES ('" + sEtatReleve + "'," + sNoIntervention + "," + General.nz(sNoFichierGraphe, "-1") + "," + NumApp + "," + " '" + StdSQLchaine.gFr_DoublerQuote(Lib_Renamed) +
                            "', '01'," + " '" + sCodeImmeuble + "','00', '00' ";

                        SQL = SQL + ",'" + Convert.ToDateTime(rs.Rows[0]["PeriodeAu"]).ToString(General.FormatDateSQL) + "'," + "'" + Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," + " " +
                            Nbj + ", '" + Convert.ToDateTime(sDatePrevue).ToString("yyyy") + "' ";
                        if (sType.ToUpper() == "APPOINT")
                        {
                            SQL = SQL + " ," + General.nz(rs.Rows[0]["IndexFin"], 1) + "";
                        }
                        else
                        {
                            SQL = SQL + " ," + General.nz(rs.Rows[0]["IndexFin"], 0) + "";
                        }

                        object tmpovj = null;
                        if (ChkLiv == 0)
                            tmpovj = "Null";
                        else
                            tmpovj = Liv;

                        SQL = SQL + ", " + lNodecade + "," + rs.Rows[0]["Chauffage"] + ", " + General.nz(tmpovj, "Null") + ", " + " " + General.nz(ChkLiv, (int)System.Windows.Forms.CheckState.Unchecked) + "";
                        //Par defaut : mettre la période de fin du dernier relevé de compteur
                        SQL = SQL + ", '" + General.nz(rs.Rows[0]["dPeriodeAu"] + "", dPeriodeDu) + "'";
                        SQL = SQL + ",'" + dPeriodeAu + "'";
                        SQL = SQL + ", " + General.nz(sDiametreCuve, "0") + ")";

                        General.Execute(SQL);
                    }
                    else
                    {
                        Nbj = 0;
                        SQL = "INSERT INTO INT_Releve ( " + " Etat, NoIntervention, NoFichierGraphe, NumAppareil, Libel_App, " + "CodeUO, CodeAffaire, CodeNumOrdre, CodeChaufferie, " +
                            " Periodedu, PeriodeAu, Njours, Saison, IndexDebut" + " ,NoDecade, chauffage, Livraison, ChkLiv, dPeriodeDu, dPeriodeAu, DiametreCuve)";
                        SQL = SQL + " VALUES ('" + sEtatReleve + "'," + sNoIntervention + "," + General.nz(sNoFichierGraphe, "-1") + "," + NumApp + "," + " '" + StdSQLchaine.gFr_DoublerQuote(Lib_Renamed) +
                            "', '01'," + " '" + sCodeImmeuble + "','00', '00' " + ",'" + Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," + "'" +
                            Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," + " " + Nbj + ", '" + Convert.ToDateTime(sDatePrevue).ToString("yyyy") + "' ";
                        if (sType.ToUpper() == "APPOINT")
                        {
                            SQL = SQL + ", 1";
                        }
                        else
                        {
                            SQL = SQL + ", 0";

                        }

                        object tmpovj = null;
                        if (ChkLiv == 0)
                            tmpovj = "Null";
                        else
                            tmpovj = Liv;

                        SQL = SQL + " , " + lNodecade + "," + "0" + " , " + General.nz(tmpovj, "Null") + ", " + " " + General.nz(ChkLiv, (int)System.Windows.Forms.CheckState.Unchecked) + ", '" +
                            dPeriodeDu + "','" + dPeriodeAu + "', " + General.nz(sDiametreCuve, "0") + ")";

                        General.Execute(SQL);
                    }
                    connectExternrs.Close();
                }
                else
                {
                    SQL = "SELECT TOP 1 * ";
                    SQL = SQL + " FROM INT_Releve";
                    SQL = SQL + " WHERE  ";
                    SQL = SQL + " NumAppareil = " + NumApp + " ";
                    SQL = SQL + " AND (Etat = 'RT' ";
                    SQL = SQL + " OR Etat = 'RP1' ";
                    SQL = SQL + " OR Etat = 'RV' ";
                    //'=== modif du 02 07 2019.
                    SQL = SQL + " OR Etat = 'AV') ";

                    SQL = SQL + " ORDER BY INT_Releve.PeriodeAu DESC ";
                    SQL = SQL + ", INT_Releve.Saison DESC ";

                    ConnectExtern connectExternrs = new ConnectExtern();
                    rs = connectExternrs.gFr_ADO_Openrecordset(SQL);

                    if (rs.Rows.Count > 0)
                    {

                        if (General.IsDate(rs.Rows[0]["PeriodeAu"] + ""))
                        {
                            Nbj = (int)(DateTime.Today - Convert.ToDateTime(rs.Rows[0]["PeriodeAu"])).TotalDays;
                        }
                        else
                        {
                            Nbj = 0;
                        }

                        SQL = "INSERT INTO INT_Releve ( " + " Etat, NoIntervention, NoFichierGraphe, NumAppareil, Libel_App, "
                            + "CodeUO, CodeAffaire, CodeNumOrdre, CodeChaufferie," +
                            " Periodedu, PeriodeAu, Njours, Saison, indexdebut "
                            + " , NoDecade, chauffage, Livraison, ChkLiv, dPeriodeDu, dPeriodeAu, DiametreCuve)"
                            + " VALUES ('" + sEtatReleve + "'," + sNoIntervention +
                            "," + General.nz(sNoFichierGraphe, "-1") + "," + NumApp + "," + " '" + StdSQLchaine.gFr_DoublerQuote(Lib_Renamed) +
                            "', '01'," + " '" + sCodeImmeuble + "','00', '00' ";

                        SQL = SQL + ",'" + Convert.ToDateTime(General.nz(rs.Rows[0]["PeriodeAu"], "01/01/1900 00:00:00")).ToString(General.FormatDateSQL) + "',"
                            + "'" + Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," +
                            " " + Nbj + ", '" + DateTime.Today.ToString("yyyy") + "' ";
                        if (sType.ToUpper() == "APPOINT")
                        {
                            SQL = SQL + " ," + General.nz(rs.Rows[0]["IndexFin"], 1) + "";
                        }
                        else
                        {
                            SQL = SQL + " ," + General.nz(rs.Rows[0]["IndexFin"], 0) + "";
                        }

                        object tmpovj = null;
                        if (ChkLiv == 0)
                            tmpovj = "Null";
                        else
                            tmpovj = Liv;

                        SQL = SQL + " , " + lNodecade + "," + rs.Rows[0]["Chauffage"] + ", " + General.nz(tmpovj, "Null") + ", " + " " +
                            General.nz(ChkLiv, (int)System.Windows.Forms.CheckState.Unchecked) + ", '" + General.nz(rs.Rows[0]["dPeriodeAu"] + "", dPeriodeDu) +
                            "','" + dPeriodeAu + "', " + General.nz(sDiametreCuve, "0") + ")";

                        General.Execute(SQL);
                    }
                    else
                    {
                        Nbj = 0;
                        SQL = "INSERT INTO INT_Releve ( " + " Etat, NoIntervention, NoFichierGraphe, NumAppareil, Libel_App, " + "CodeUO, CodeAffaire, CodeNumOrdre, CodeChaufferie, " +
                            " Periodedu, PeriodeAu, Njours, Saison, IndexDebut" + " , NoDecade, chauffage, Livraison, ChkLiv, dPeriodeDu, dPeriodeAu, DiametreCuve)" + " VALUES ('" + sEtatReleve + "'," +
                            sNoIntervention + "," + General.nz(sNoFichierGraphe, "-1") + "," + NumApp + "," + " '" + StdSQLchaine.gFr_DoublerQuote(Lib_Renamed) +
                            "', '01'," + " '" + sCodeImmeuble + "','00', '00' " + ",'" + Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," + "'" +
                            Convert.ToDateTime(sDatePrevue).ToString(General.FormatDateSQL) + "'," + " " + Nbj + ", '" + Convert.ToDateTime(sDatePrevue).ToString("yyyy") + "' ";
                        if (sType.ToUpper() == "APPOINT")
                        {
                            SQL = SQL + ", 1 ";
                        }
                        else
                        {
                            SQL = SQL + ", 0 ";
                        }

                        object tmpovj = null;
                        if (ChkLiv == 0)
                            tmpovj = "Null";
                        else
                            tmpovj = Liv;

                        SQL = SQL + " , " + lNodecade + "," + "0" + " , " + General.nz(tmpovj, "Null") + ", " + " " + General.nz(ChkLiv, (int)System.Windows.Forms.CheckState.Unchecked) + ", '" + dPeriodeDu +
                            "','" + dPeriodeAu + "', " + General.nz(sDiametreCuve, "0") + ")";

                        General.Execute(SQL);
                    }
                    connectExternrs.Close();
                }
                rs = null;
                wNb = wNb + 1;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA.bas;Ins_DernierRel;");
                return functionReturnValue;
            }
        }


        private static bool fc_CrlReleve(string sCodeImmeuble, int lNumAppareil, int lNoIntervention)
        {
            bool functionReturnValue = false;
            string sSQL = null;

            try
            {
                functionReturnValue = false;

                sSQL = "SELECT     NumAppareil, CodeAffaire, NoIntervention" + " From INT_Releve " + " WHERE     CodeAffaire = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) +
                    "'" + " AND NumAppareil = " + lNumAppareil + " AND NoIntervention = " + lNoIntervention;

                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))
                {
                    functionReturnValue = true;
                }
                else
                {
                    functionReturnValue = false;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA;");
                return functionReturnValue;
            }
        }

        private static void fc_particulier(InterPDA[] tpInter, int X)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            int j = 0;

            try
            {
                sSQL = "SELECT     CodeImmeuble, CodeParticulier, Nom, Adresse, CodePostal, Ville, Tel, TelPortable_IMP, eMail" + " From ImmeublePart " + " WHERE     CodeImmeuble = '" +
                    StdSQLchaine.gFr_DoublerQuote(tpInter[X].sCodeImmeuble) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                j = 0;
                tpInter[X].boolPartExist = false;


                foreach (DataRow rsRow in rs.Rows)
                {

                    if (!string.IsNullOrEmpty(General.nz(rsRow["CodeParticulier"], "").ToString()))
                    {
                        tpInter[X].boolPartExist = true;

                        Array.Resize(ref tpInter[X].sCodeImmeublePart, j + 1);
                        tpInter[X].sCodeImmeublePart[j] = General.nz(rsRow["CodeImmeuble"], "").ToString();

                        Array.Resize(ref tpInter[X].sCodeParticulierPart, j + 1);
                        tpInter[X].sCodeParticulierPart[j] = General.nz(rsRow["CodeParticulier"], "").ToString();

                        Array.Resize(ref tpInter[X].sNomPart, j + 1);
                        tpInter[X].sNomPart[j] = General.nz(rsRow["Nom"], "").ToString();

                        Array.Resize(ref tpInter[X].sAdressePart, j + 1);
                        tpInter[X].sAdressePart[j] = General.nz(rsRow["Adresse"], "").ToString();

                        Array.Resize(ref tpInter[X].sCpPart, j + 1);
                        tpInter[X].sCpPart[j] = General.nz(rsRow["CodePostal"], "").ToString();

                        Array.Resize(ref tpInter[X].sVillePart, j + 1);
                        tpInter[X].sVillePart[j] = General.nz(rsRow["Ville"], "").ToString();

                        Array.Resize(ref tpInter[X].sTelFixePart, j + 1);
                        tpInter[X].sTelFixePart[j] = General.nz(rsRow["Tel"], "").ToString();

                        Array.Resize(ref tpInter[X].sPortablePart, j + 1);
                        tpInter[X].sPortablePart[j] = General.nz(rsRow["TelPortable_IMP"], "").ToString();

                        Array.Resize(ref tpInter[X].sMailPart, j + 1);
                        tpInter[X].sMailPart[j] = General.nz(rsRow["Email"], "").ToString();

                        j = j + 1;
                    }

                }


                modAdors.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modPDA;fc_particulier");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="tpInter"></param>
        /// <param name="bSaisieCompteur"></param>
        /// <param name="bDetail"></param>
        /// <returns></returns>
        private static bool fc_WriteFile(InterPDA[] tpInter, bool bSaisieCompteur, bool bDetail)
        {
            bool functionReturnValue = false;
            string sMessage = null;

            int i = 0;
            int X = 0;
            int lCount = 0;

            //Dim bDetail             As Boolean

            string sNomfichier = null;
            string sSQL = null;

            try
            {
                //=== création du message à envoyer sur le PDA.
                sMessage = "";
                for (i = 0; i <= tpInter.Length - 1; i++)
                {
                    //=== Type de ligne du fichier d'export
                    //===Type de ligne du fichier d'export
                    sMessage = cEnteteRamonage + cSep;
                    //=== Type d'intervention, 'R' = Ramonage, 'P' = Preventive, 'C' = Corrective
                    sMessage = sMessage + fc_Replace(tpInter[i].sTypeIntervention) + cSep;
                    //==== N°intervention métier
                    sMessage = sMessage + fc_Replace(tpInter[i].sNoIntervention) + cSep;
                    //=== matricule.
                    sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sMatricule), 50) + cSep;
                    //=== Type d'opération, C = Création, S = Suppression.
                    sMessage = sMessage + tpInter[i].sTypeOperation + cSep;
                    //=== info immeuble
                    sMessage = sMessage + fc_Replace(tpInter[i].sCodeImmeuble) + cSep;
                    sMessage = sMessage + General.nz(fc_Replace(tpInter[i].sAdresse.ToString().Trim()), "Adresse inconnue") + cSep;
                    sMessage = sMessage + General.nz(fc_Replace(tpInter[i].sCP.ToString().Trim()), "11111") + cSep;
                    sMessage = sMessage + General.nz(fc_Replace(tpInter[i].sVille.Trim()), "Ville inconnue") + cSep;
                    sMessage = sMessage + fc_Replace(tpInter[i].sAngleRue) + cSep;
                    //=== date prevue.
                    sMessage = sMessage + General.nz(tpInter[i].sDatePrevue, "") + cSep;
                    //=== date creation.
                    sMessage = sMessage + tpInter[i].sDateCreation + cSep;
                    //=== code état
                    sMessage = sMessage + General.Left(General.nz(tpInter[i].sCodeEtat, "0A").ToString(), 5) + cSep;
                    //=== code article.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(tpInter[i].sCodeArticle), "I").ToString(), 50) + cSep;
                    //===  libelle de l'article
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(tpInter[i].sLibArticle), "article inconnue").ToString(), 100) + cSep;
                    //=== intervention uurgente.
                    sMessage = sMessage + tpInter[i].lUrgence + cSep;
                    //=== info complementaire.
                    sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sInfoComplementaire), 50) + cSep;
                    //=== info commentaire corrective.
                    sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sCommentaireCorrective), 4000) + cSep;
                    //=== telephone.
                    sMessage = sMessage + fc_Replace(tpInter[i].sTelephone) + cSep;
                    //=== info stat.
                    sMessage = sMessage + fc_Replace(tpInter[i].sInfoStat) + cSep;
                    //=== info prestation.
                    sMessage = sMessage + fc_Replace(tpInter[i].sInfoPrestation) + cSep;
                    //=== sasie compteur.
                    sMessage = sMessage + General.nz(tpInter[i].lsaisieCompteur, 0) + cSep;

                    //=== modif du 12 03 2018, ajout du taux de tva et de la societe
                    if (!string.IsNullOrEmpty(General.sCodePDASociete))
                    {
                        //=== société.
                        sMessage = sMessage + General.Left(General.sCodePDASociete, 20) + cSep;
                        //=== taus de TVA.
                        sMessage = sMessage + cSep;
                    }

                    //sMessage = sMessage & tpInter(i).lIdLocalisation & cSep
                    //sMessage = sMessage & fc_Replace(tpInter(i).sNomLocalisation) & cSep
                    //=== nom de l'intervenant.
                    //sMessage = sMessage & fc_Replace(tpInter(i).sNomIntervenant) & cSep
                    //=== périodicité
                    //sMessage = sMessage & fc_Replace(tpInter(i).sTPP_Code) & cSep

                    X = 0;
                    lCount = 0;
                    if (bDetail == true)
                    {
                        for (X = 0; X <= tpInter[i].lIdLocalisation.Length - 1; X++)
                        {
                            lCount = lCount + 1;
                            sMessage = sMessage + "\n";

                            sMessage = sMessage + cDetailInter + cSep;
                            sMessage = sMessage + tpInter[i].lIdLocalisation[X] + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sNomLocalisation[X]), 100) + cSep;
                            sMessage = sMessage + tpInter[i].lIdEquipement[X] + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sNomEquipement[X]), 200) + cSep;
                            //sMessage = sMessage & tpInter(i).sIdComposant(X) & cSep
                            sMessage = sMessage + tpInter[i].lIdComposant[X] + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sNomComposant[X]), 200) + cSep;

                        }

                        for (X = 0; X <= tpInter[i].lDetIdControle.Length - 1; X++)
                        {
                            lCount = lCount + 1;
                            sMessage = sMessage + "\n";

                            sMessage = sMessage + cDetailControle + cSep;
                            sMessage = sMessage + tpInter[i].lDetIdControle[X] + cSep;
                            sMessage = sMessage + tpInter[i].lDetIdLocalisation[X] + cSep;
                            //sMessage = sMessage & tpInter(i).sDetIdComposant(X) & cSep
                            sMessage = sMessage + tpInter[i].lDetIdComposant[X] + cSep;

                            sMessage = sMessage + tpInter[i].lDetIdEquipement[X] + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sDetNomControle[X]), 200) + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sDetMoyen[X]), 200) + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sDetAnomalie[X]), 200) + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sDetOperation[X]), 200) + cSep;

                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sFrequence[X]), 3) + cSep;
                            sMessage = sMessage + tpInter[i].lReportee[X] + cSep;


                        }

                    }

                    if (bSaisieCompteur == true)
                    {
                        X = 0;

                        //=== Localisations des compteurs.
                        for (X = 0; X <= tpInter[i].lLocComptIdLocalisation.Length - 1; X++)
                        {
                            lCount = lCount + 1;
                            sMessage = sMessage + "\n";
                            sMessage = sMessage + cLocCompteur + cSep;
                            sMessage = sMessage + tpInter[i].lLocComptIdLocalisation[X] + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sLocComptNomLocalisation[X]), 100) + cSep;
                            sMessage = sMessage + tpInter[i].lLocComptCHauffage[X] + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].lLocComptAllArret[X], -1) + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sLocComptNomRondier[X]), 100) + cSep;
                        }
                        //=== compteurs.
                        for (X = 0; X <= tpInter[i].lComptNumAppareil.Length - 1; X++)
                        {
                            lCount = lCount + 1;
                            sMessage = sMessage + "\n";
                            sMessage = sMessage + cDetCompteur + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sComptSaison[X]), 4) + cSep;
                            sMessage = sMessage + tpInter[i].lComptNumAppareil[X] + cSep;

                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sComptNomCOmpteur[X]), 200) + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sComptTypeCompteur[X]), 10) + cSep;
                            sMessage = sMessage + tpInter[i].sComptDateIndexPrecedent[X] + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].sComptIndexPrecedent[X], 0) + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].sComptNiveauMini[X], "") + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].sComptNiveauMaxi[X], "") + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].sComptDiametreCuve[X], "") + cSep;
                            sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sComptUnitePige[X]), 2) + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].lComptIdLocalisation[X], 0) + cSep;
                            sMessage = sMessage + General.nz(tpInter[i].sComptQteLivraison[X], "") + cSep;

                        }

                    }


                    if (!string.IsNullOrEmpty(General.sCodePDASociete))
                    {
                        //=== code particulier
                        if (tpInter[i].boolPartExist == true)
                        {
                            X = 0;

                            for (X = 0; X <= tpInter[i].sCodeParticulierPart.Length - 1; X++)
                            {
                                lCount = lCount + 1;
                                sMessage = sMessage + "\n";
                                sMessage = sMessage + cKeyParticulier + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sCodeImmeublePart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sCodeParticulierPart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sNomPart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sAdressePart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sCpPart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sVillePart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sTelFixePart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sPortablePart[X]), 50) + cSep;
                                sMessage = sMessage + General.Left(fc_Replace(tpInter[i].sMailPart[X]), 100) + cSep;
                                //sMessage = sMessage & Left(fc_Replace(tpInter(i).(X)),) & cSep

                            }
                        }
                    }
                    //=== fin  de fichier
                    sMessage = sMessage + "\n";
                    sMessage = sMessage + cFIN_Ramonage + cSep;
                    sMessage = sMessage + (lCount + 2);

                    sMessage = sMessage;

                    //=== création du nom de fichier.
                    sNomfichier = fc_nomFichier(tpInter[i].sNoIntervention);

                    //=== création du fichier.
                    if (fc_CreateFichier(sNomfichier, sMessage) == true)
                    {
                        functionReturnValue = true;

                        //=== modif du 31 10 2016, ne pas envoyer si mode suppresdsion.
                        if (tpInter[i].sTypeOperation.ToUpper() != "S".ToUpper())
                        {
                            //=== envoie l'historique des 5 dernieres interventions pour un immeuble donnée
                            fc_HistoIntervention(tpInter[i].sCodeImmeuble, tpInter[i].sNoIntervention, i);
                            fc_HistoDevis(tpInter[i].sCodeImmeuble, tpInter[i].sNoIntervention, i);
                        }
                    }
                    else
                    {
                        functionReturnValue = false;
                    }



                }
                i = 0;

                sSQL = "INSERT INTO PDA_LOG(" + " TypeInter," + " [dateHeure], " + " NoIntervention,"
                    + " Matricule," + " TypeOperation, " + " Codeimmeuble, " + " DatePrevue,"
                    + " dateCreation, " + " CodeEtat, " + " CodeArticle," + " LibArticle,"
                    + " Urgence," + " InfoComplementaire, " + " CommentaireCorrective,"
                    + " TelePhone," + " InfoStat," + " InfoPrestation," + " SaisieCompteur,"
                    + " TypeEnvoie," + " EnvoyePar";

                sSQL = sSQL + ")";

                sSQL = sSQL + "VALUES (" + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sTypeIntervention) + "'," + "'" + DateTime.Now + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sNoIntervention) +
                    "'," + "'" + StdSQLchaine.gFr_DoublerQuote(General.Left(tpInter[i].sMatricule, 50)) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sTypeOperation) + "'," + "'" +
                    StdSQLchaine.gFr_DoublerQuote(tpInter[i].sCodeImmeuble) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sDatePrevue) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sDateCreation) +
                    "'," + "'" + StdSQLchaine.gFr_DoublerQuote(General.Left(tpInter[i].sCodeEtat, 5)) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(General.Left(tpInter[i].sCodeArticle, 50)) +
                    "'," + "'" + StdSQLchaine.gFr_DoublerQuote(General.Left(tpInter[i].sLibArticle, 100)) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].lUrgence.ToString()) + "'," +
                    "'" + StdSQLchaine.gFr_DoublerQuote(General.Left(tpInter[i].sInfoComplementaire, 50)) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sCommentaireCorrective) + "'," +
                    "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sTelephone.Trim().Replace(" ", "")) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sInfoStat) + "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].sInfoPrestation) +
                    "'," + "'" + StdSQLchaine.gFr_DoublerQuote(tpInter[i].lsaisieCompteur.ToString()) + "'," + "'Emission'," + "'" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";

                sSQL = sSQL + ")";


                int xx = General.Execute(sSQL);


                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPda;fc_WriteFile");
                return functionReturnValue;
            }

        }

        private static string fc_Replace(string sWord)
        {
            string functionReturnValue = null;


            //=== enleve les caracteres indésirable.

            try
            {
                if (sWord == null)
                    sWord = "";
                sWord = sWord.Replace("\r\n", "");
                sWord = sWord.Replace("\r", "");
                sWord = sWord.Replace("\n", "");
                sWord = sWord.Replace("|", "");


                functionReturnValue = sWord;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA;fc_Replace");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        private static string fc_nomFichier(string sID)
        {
            string functionReturnValue = null;

            try
            {
                functionReturnValue = General.Left(General.fncUserName().ToUpper(), 3);

                functionReturnValue = functionReturnValue + "_" + DateTime.Today.Year + DateTime.Today.Month.ToString("00") + DateTime.Today.Day.ToString("00");
                functionReturnValue = functionReturnValue + "_" + DateTime.Now.TimeOfDay.Hours.ToString("00") + DateTime.Now.Minute.ToString("00") +
                    DateTime.Now.Second.ToString("00");

                if (!string.IsNullOrEmpty(sID))
                {
                    functionReturnValue = functionReturnValue + "_" + sID;

                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modpda;fc_nomFichier");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNomfichier"></param>
        /// <param name="sCorps"></param>
        /// <returns></returns>
        private static bool fc_CreateFichier(string sNomfichier, string sCorps)
        {
            bool functionReturnValue = false;
            //=== création du fichier d'export.
            //===  retourne true si le fichier existe bien.
            string sNameFileTemp = null;
            string sNameFile = null;

            try
            {

                if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Today.ToString("dd/MM/yyyy") == "07/02/2019")
                {
                    General.sPDARepEmissionFichier = "C:\\temp\\pda\\GES2RAY\\";
                }
                else if (General.fncUserName().ToLower().Contains("mondir"))
                {
                    General.sPDARepEmissionFichier = "C:\\temp\\pda\\GES2RAY\\";
                    //General.sPDARepEmissionFichier = @"\\Dt-mob-01\data$\IMPORT\";
                }

                //=== création d'un fichier temporaire.
                sNameFileTemp = General.sPDARepEmissionFichier + sNomfichier + ".txt.tmp";

                File.AppendAllText(sNameFileTemp, sCorps, Encoding.Unicode);

                //=== controle que le fichier existe si oui on le renomme.
                if (Dossier.fc_ControleFichier(sNameFileTemp) == true)
                {
                    functionReturnValue = true;
                    sNameFile = General.sPDARepEmissionFichier + sNomfichier + ".txt";
                    Dossier.fc_DeplaceFichier(sNameFileTemp, sNameFile);
                    //===  modif du 16 03 2014 controle a nouveau si ce fichier existe.
                    if (Dossier.fc_ControleFichier(sNameFile) == true)
                    {
                        functionReturnValue = true;
                    }
                    else
                    {
                        functionReturnValue = false;
                    }
                }
                else
                {
                    functionReturnValue = false;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modpda;fc_CreateFichier");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sNoInterventionRam"></param>
        /// <param name="lindex"></param>
        private static void fc_HistoIntervention(string sCodeImmeuble, string sNoInterventionRam, int lindex)
        {

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sMessage = null;
            string sdate = null;
            string stemp = null;
            string sNomfichier = null;
            string sSQL = null;
            int i = 0;
            string sDateRealise = null;

            //=== envoie l 'historique des 5 dernieres interventions pour un immeuble donnée.

            try
            {
                //sSql = "SELECT  top 5   Intervention.NoIntervention, Intervention.INT_RapportIntervention, Intervention.DateRealise, Personnel.Nom, Personnel.Prenom, " _
                //& " INT_InterTechnicien.INT_NoAuto, Intervention.DateSaisie " _
                //& " FROM         Intervention INNER JOIN" _
                //& " INT_InterTechnicien ON Intervention.NoIntervention = INT_InterTechnicien.NoIntervention LEFT OUTER JOIN  " _
                //& "   Personnel ON INT_InterTechnicien.INT_Matricule = Personnel.Matricule " _
                //& " WHERE    Intervention.CodeImmeuble = '" & gFr_DoublerQuote(sCodeImmeuble) & "'" _
                //& " ORDER BY Intervention.DateRealise DESC"
                sSQL = "SELECT     TOP " + General.lnbHistoInterPDA + " Intervention.NoIntervention, Intervention.CodeImmeuble, Intervention.DateRealise, " +
                    " Intervention.Article,DateSaisie, Intervention.Designation, " + " Intervention.CodeEtat , TypeCodeEtat.LibelleCodeEtat, Intervention.Commentaire, Wave " + " FROM  " +
                    "       Intervention LEFT OUTER JOIN " + " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat " + " WHERE    Intervention.CodeImmeuble = '" +
                    StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'" + " and Intervention.DateRealise is not null AND (Wave IS NOT NULL)";

                sSQL = sSQL + " ORDER BY Intervention.DateRealise DESC";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);


                if (rs.Rows.Count == 0)
                {
                    modAdors.Close();
                    return;
                }

                i = 1;

                sMessage = "";

                foreach (DataRow rsRow in rs.Rows)
                {

                    if (i > 1)
                    {
                        sMessage = sMessage + "\n";

                    }

                    //=== entete histo intervention.
                    sMessage = sMessage + cET_HIST_RAMONAGE + cSep;

                    //=== No intervention.
                    sMessage = sMessage + rsRow["NoIntervention"] + cSep;
                    //=== code immeuble.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["CodeImmeuble"].ToString()) + "", "inconnu").ToString(), 50) + cSep;

                    //=== date de l'intervention.
                    sdate = "";
                    if (rsRow["DateRealise"] != DBNull.Value)
                    {
                        sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsRow["DateRealise"], "")));
                    }
                    else
                    {
                        sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsRow["DateSaisie"], "")));
                    }
                    sMessage = sMessage + sdate + cSep;

                    //=== code article.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["Article"].ToString()), "I").ToString(), 50) + cSep;

                    //=== libelle article.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["Designation"].ToString()) + "", "SANS").ToString(), 100) + cSep;

                    //=== lien http vers le fichier wave.
                    //sMessage = sMessage & Left(nz(fc_Replace(rs!Designation), "SANS"), 100) & cSep
                    if (!string.IsNullOrEmpty(General.nz(rsRow["Wave"], "").ToString()) && !string.IsNullOrEmpty(General.slienHttpWave))
                    {
                        stemp = Dossier.ExtractFileName(rsRow["Wave"].ToString());

                        sMessage = sMessage + General.slienHttpWave + stemp + cSep;
                    }
                    else
                    {
                        sMessage = sMessage + "" + cSep;
                    }
                    //=== code état.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["CodeEtat"] + ""), "XX").ToString(), 5).ToString() + cSep;

                    //=== libelle code état.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["LibelleCodeEtat"] + ""), "code état Inconnu").ToString(), 50) + cSep;

                    //=== commentaire.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["Commentaire"] + ""), "").ToString(), 50) + cSep;

                    //=== modif du 12 03 2018, ajout du taux de tva et de la societe
                    if (!string.IsNullOrEmpty(General.sCodePDASociete))
                    {
                        //=== société.
                        sMessage = sMessage + General.Left(General.sCodePDASociete, 20) + cSep;
                    }

                    i = i + 1;


                }

                modAdors.Close();

                sMessage = sMessage + "\n";
                sMessage = sMessage + cFF_HIST_RAMONAGE + cSep;
                sMessage = sMessage + i;

                //=== retourne le nom du fichier.
                stemp = sNoInterventionRam + Convert.ToString(lindex) + "histo";
                sNomfichier = fc_nomFichier(stemp);

                //=== création du fichier.
                fc_CreateFichier(sNomfichier, sMessage);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA;fc_HistoRamonage");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sNoInterventionRam"></param>
        /// <param name="lindex"></param>
        private static void fc_HistoDevis(string sCodeImmeuble, string sNoInterventionRam, int lindex)
        {

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sMessage = null;
            string sdate = null;
            string stemp = null;
            string sNomfichier = null;
            string sSQL = null;
            int i = 0;
            string sDateRealise = null;

            //=== envoie l 'historique des 5 dernieres interventions pour un immeuble donnée.


            try
            {


                sSQL = "SELECT     TOP " + General.lnbHistoDevisPDA + " DevisEnTete.CodeImmeuble, DevisEnTete.NumeroDevis, DevisEnTete.TitreDevis, DevisEnTete.CodeEtat, DevisEnTete.DateCreation, " +
                    " DevisEnTete.DateAcceptation , DevisCodeEtat.Libelle" + " FROM         DevisEnTete LEFT OUTER JOIN " + " DevisCodeEtat ON DevisEnTete.CodeEtat = DevisCodeEtat.Code " +
                    " WHERE     DevisEnTete.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' " + " ORDER BY DevisEnTete.DateAcceptation DESC";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    modAdors.Close();
                    return;
                }

                i = 1;

                sMessage = "";

                foreach (DataRow rsRow in rs.Rows)
                {

                    if (i > 1)
                    {
                        sMessage = sMessage + "\n";

                    }

                    //=== entete histo intervention.
                    sMessage = sMessage + cET_HIST_DEVIS + cSep;

                    //=== code immeuble.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["CodeImmeuble"] + ""), "inconnu").ToString(), 50) + cSep;

                    //=== numéro de devis.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["numerodevis"] + ""), "0000000").ToString(), 50) + cSep;

                    //=== titre devis.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["TitreDevis"] + ""), "sans titre").ToString(), 400) + cSep;

                    //=== code état.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["CodeEtat"] + ""), "XX").ToString(), 50) + cSep;

                    //=== libelle code état.
                    sMessage = sMessage + General.Left(General.nz(fc_Replace(rsRow["Libelle"] + ""), "code état Inconnu").ToString(), 50) + cSep;

                    //=== date de création.
                    sdate = "";
                    if (rsRow["DateCreation"] != DBNull.Value)
                    {
                        sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsRow["DateCreation"], "")));
                    }
                    else
                    {
                        sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsRow["DateAcceptation"], "")));
                    }
                    sMessage = sMessage + sdate + cSep;

                    //=== date acceptation.
                    sdate = "";
                    if (rsRow["DateCreation"] != DBNull.Value)
                    {
                        sdate = fc_FormatDatePDA(Convert.ToString(General.nz(rsRow["DateAcceptation"], "")));
                    }
                    else
                    {
                        sdate = "";
                    }
                    sMessage = sMessage + sdate + cSep;

                    //=== commentaire a alimenter plus tard.
                    sMessage = sMessage + "" + cSep;

                    //=== modif du 12 03 2018, ajout du taux de tva et de la societe
                    if (!string.IsNullOrEmpty(General.sCodePDASociete))
                    {
                        //=== société.
                        sMessage = sMessage + General.Left(General.sCodePDASociete, 20) + cSep;
                    }

                    i = i + 1;


                }

                modAdors.Close();

                sMessage = sMessage + "\n";
                sMessage = sMessage + cFF_HIST_DEVIS + cSep;
                sMessage = sMessage + i;

                //=== retourne le nom du fichier.
                stemp = sNoInterventionRam + Convert.ToString(lindex) + "histoDevis";
                sNomfichier = fc_nomFichier(stemp);

                //=== création du fichier.
                fc_CreateFichier(sNomfichier, sMessage);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA;fc_HistoRamonage");
            }

        }

        private static string fc_ReturnPeriodicite(string sLib)
        {
            string functionReturnValue = null;

            try
            {
                if (sLib.ToUpper() == "Annuel".ToUpper())
                {
                    functionReturnValue = "A";
                }
                else if (sLib.ToUpper() == "BiMensuel".ToUpper())
                {
                    functionReturnValue = "M";
                }
                else if (sLib.ToUpper() == "BiMestriel".ToUpper())
                {
                    functionReturnValue = "M";
                }
                else if (sLib.ToUpper() == "Hebdomadaire".ToUpper())
                {
                    functionReturnValue = "H";
                }
                else if (sLib.ToUpper() == "Mensuel".ToUpper())
                {
                    functionReturnValue = "M";
                }
                else if (sLib.ToUpper() == "Quotidien".ToUpper())
                {
                    functionReturnValue = "Q";
                }
                else if (sLib.ToUpper() == "Semestriel".ToUpper())
                {
                    functionReturnValue = "S";
                }
                else if (sLib.ToUpper() == "Trimestriel".ToUpper())
                {
                    functionReturnValue = "T";
                }
                else if (sLib.ToUpper() == "Cycle année".ToUpper())
                {
                    functionReturnValue = "A";
                }
                else if (sLib.ToUpper() == "Cycle Journée".ToUpper())
                {
                    functionReturnValue = "Q";
                }
                else if (sLib.ToUpper() == "Cycle mois".ToUpper())
                {
                    functionReturnValue = "M";
                }
                else
                {
                    functionReturnValue = "M";
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modpda;fc_ReturnPeriodicite");
                return functionReturnValue;
            }

        }

        public static void fc_infostat(int lNoIntervention, string sMat, string sCodeImmeuble, ref string sInfoStat, ref string sInfoPresta, ref string sInfoDevis, System.DateTime? dtDebut = null,
            System.DateTime? dtFin = null, string bEtat = "false", System.DateTime? dtNow = null)
        {
            //=== rstourne un contenu dans les variables sInfoStat, sInfoDevis et sInfoStat.

            string sSQL = null;
            string sMonth = null;
            DataTable rsImm = default(DataTable);
            ModAdo modAdorsImm = new ModAdo();
            DataTable rsAddEntete = default(DataTable);
            ModAdo modAdorsAddEntete = new ModAdo();
            DataTable rsAddt = default(DataTable);
            ModAdo modAdorsAddt = new ModAdo();
            DataTable rsint = default(DataTable);
            ModAdo modAdorsint = new ModAdo();

            string sSQLt = null;
            string sSQLspecif = null;
            DataTable rsT = default(DataTable);
            ModAdo modAdorsT = null;
            DataTable rsSpec = default(DataTable);
            ModAdo modAdorsSpec = null;


            System.DateTime dtDate = default(System.DateTime);
            System.DateTime dtDateDerniere = default(System.DateTime);
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);


            //Dim dtDateDebPlaningBisSimul As Date
            //Dim dtDateFinPlaningBisSimul As Date
            string sMessageErr = null;

            int lNbDevis = 0;
            int lnbInterMoisDernier = 0;
            int lNbPanneECS = 0;
            int lNbPanneCH = 0;
            int lnbPanneAutre = 0;
            int lnbPanneFacture = 0;
            int lNbAstreinte = 0;
            int lNbPanneEcsCh = 0;
            int xi = 0;
            string sDateDerniereVisite = null;
            string sUser = null;

            int ID = 0;

            bool bEntete = false;

            try
            {
                sUser = General.fncUserName();

                sMonth = Convert.ToString(DateTime.Today.Month);

                dtDateDeb = Convert.ToDateTime("01/" + sMonth + "/" + DateTime.Today.Year);

                dtDateFin = dtDateDeb.AddDays(-1);
                dtDateDeb = dtDateDeb.AddMonths(-1);

                if (Convert.ToBoolean(bEtat) == true)
                {
                    sSQL = "SELECT     Nointervention, Codeimmeuble, CodeArticle, Designation, Commentaire, ID, Intervenant , AFaire, TypeIntervenant"
                        + " ,Devis, NumeroDevis, TitreDevis, DateDevis, CodeEtatDevis" + " From InterP2Complement " + " WHERE     NoIntervention = " + lNoIntervention;
                    modAdorsAddt = new ModAdo();
                    rsAddt = modAdorsAddt.fc_OpenRecordSet(sSQL);

                    //=== requete pour l ajout dans la table en entete.
                    sSQL = "SELECT ID, NoIntervention, CodeImmeuble, Gardien, TelGardien,  HorairesLoge, CodeAcces1, "
                        + " CodeAcces2, SpecifAcces, CodeQualif_QUA, NomCorrespondant, " + " DateDerniereVisite , NbInterMoisDernier, NbPanneEcs, NbPanneCh, NbPanneAutre,"
                        + " NbInterFacturee, NbInterAstreinte, NbPanneEcsCh, CreeLe, CreePar " + " From InterP2EnTete " + " WHERE    NoIntervention = " + lNoIntervention;

                    rsAddEntete = modAdorsAddEntete.fc_OpenRecordSet(sSQL);
                    if (rsAddEntete.Rows.Count == 0)
                    {
                        bEntete = false;
                    }
                    else
                    {
                        bEntete = true;
                    }
                }



                //=== extrait les visites (table intervention) pour un mois donnée afin de controler si celle ci a déjà éte crée.
                sSQL = "SELECT   Intervention.NoIntervention, Intervention.DateRealise, FacArticle.TYI_Code, Intervention.CodeEtat, GestionStandard.Astreinte,"
                    + " Intervention.codeimmeuble, Intervention.VisiteEntretien, Intervention.Article " + " FROM         Intervention INNER JOIN "
                    + " FacArticle ON Intervention.Article = FacArticle.CodeArticle INNER JOIN"
                    + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard"
                    + " WHERE  Intervention.Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'"
                    + " and Intervention.DateRealise >='" + dtDateDeb + "'" + " and Intervention.DateRealise <='" + dtDateFin + "'";

                modAdorsint = new ModAdo();
                rsint = modAdorsint.fc_OpenRecordSet(sSQL);


                //Set rsAddEntete = fc_OpenRecordSet(SQL)

                //=== ajout de l'entete des visites P2.
                //rsImm.Filter = adFilterNone
                //rsImm.Filter = "CodeImmeuble ='" & gFr_DoublerQuote(rs!CodeImmeuble) & "'"

                //rsint.Filter = adFilterNone
                //rsint.Filter = "CodeImmeuble ='" & gFr_DoublerQuote(rs!CodeImmeuble) & "'"

                //rsint.Sort = "DateSaisie DESC"


                lnbInterMoisDernier = 0;
                lNbPanneCH = 0;
                lNbPanneECS = 0;
                lnbPanneAutre = 0;
                lnbPanneFacture = 0;
                lNbAstreinte = 0;
                lNbPanneEcsCh = 0;
                dtDateDerniere = Convert.ToDateTime("01/01/1950");

                foreach (DataRow rsintRow in rsint.Rows)
                {
                    //==== nbre total d'intervention effactuées le mois dernier.
                    lnbInterMoisDernier = lnbInterMoisDernier + 1;

                    if (rsintRow["CodeEtat"].ToString().ToUpper() == "ZZ")
                    {


                        //If UCase(rsint!TYI_Code) = UCase("CH") Then
                        if (General.nz(rsintRow["article"], "").ToString().ToUpper() == "I11".ToUpper())
                        {
                            //==== nbre total d'intervention chauffage effectuées le mois dernier.
                            lNbPanneCH = lNbPanneCH + 1;

                            //ElseIf UCase(rsint!TYI_Code) = UCase("ECS") Then
                        }
                        else if (rsintRow["article"].ToString().ToUpper() == "I25".ToUpper())
                        {
                            //==== nbre total d'intervention ECS effectuées le mois dernier.
                            lNbPanneECS = lNbPanneECS + 1;

                        }
                        else if (rsintRow["article"].ToString().ToUpper() == "I10".ToUpper())
                        {
                            //==== nbre total d'intervention ECS+CH effectuées le mois dernier.
                            lNbPanneEcsCh = lNbPanneEcsCh + 1;

                            //ElseIf UCase(rsint!TYI_Code) = UCase("AUTRE") Then
                        }
                        else
                        {
                            //==== nbre total d'intervention AUTRE effectuées le mois dernier.
                            lnbPanneAutre = lnbPanneAutre + 1;
                        }

                        if (General.IsDate(rsintRow["DateRealise"]) && General.nz(rsintRow["VisiteEntretien"], 0).ToString() == "1")
                        {
                            //=== stocke la date de la derniére visite.
                            if (Convert.ToDateTime(rsintRow["DateRealise"]) > dtDateDerniere)
                            {
                                dtDateDerniere = Convert.ToDateTime(rsintRow["DateRealise"]);
                            }
                        }
                    }

                    if (rsintRow["CodeEtat"].ToString().ToUpper() == "T")
                    {
                        //==== nbre total d'intervention facturées effectuées le mois dernier.
                        lnbPanneFacture = lnbPanneFacture + 1;

                        //If IsDate(rsint!DateRealise) Then
                        //        '=== stocke la date de la derniére visite.
                        //        If CDate(rsint!DateRealise) > dtDateDerniere Then
                        //                dtDateDerniere = rsint!DateRealise
                        //        End If
                        //End If
                    }

                    if (rsintRow["Astreinte"].ToString() == "1")
                    {
                        //==== nbre total d'intervention d'astreinte effectuées le mois dernier.
                        lNbAstreinte = lNbAstreinte + 1;

                        //If IsDate(rsint!DateRealise) Then
                        //        '=== stocke la date de la derniére visite.
                        //        If CDate(rsint!DateRealise) > dtDateDerniere Then
                        //                dtDateDerniere = rsint!DateRealise
                        //        End If
                        //End If
                    }

                }

                if (dtDateDerniere == Convert.ToDateTime("01/01/1950"))
                {
                    sSQLt = "SELECT     TOP (1) DateRealise From Intervention " + " WHERE   CodeEtat = 'ZZ' AND CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) +
                        "' " + " and VisiteEntretien = 1 " + " ORDER BY DateRealise DESC";
                    using (var tmpModAdo = new ModAdo())
                        sSQLt = tmpModAdo.fc_ADOlibelle(sSQLt);

                    if (General.IsDate(sSQLt))
                    {
                        dtDateDerniere = Convert.ToDateTime(sSQLt);
                        sDateDerniereVisite = Convert.ToString(dtDateDerniere);
                    }
                    else
                    {
                        sDateDerniereVisite = "";
                    }
                }
                else
                {
                    sDateDerniereVisite = Convert.ToString(dtDateDerniere);
                }



                if (Convert.ToBoolean(bEtat) == true)
                {
                    if (bEntete == false)
                    {
                        rsAddEntete.Rows.Add(rsAddEntete.NewRow());

                    }
                    SqlCommandBuilder cb = new SqlCommandBuilder(modAdorsAddEntete.SDArsAdo);
                    modAdorsAddEntete.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                    modAdorsAddEntete.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                    SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                    insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                    insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                    modAdorsAddEntete.SDArsAdo.InsertCommand = insertCmd;

                    modAdorsAddEntete.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                    {

                        if (e.StatementType == StatementType.Insert)
                        {

                            if (ID == 0)
                            {

                                ID = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                                rsAddEntete.Rows[0]["ID"] = ID;
                            }
                        }
                    });
                    rsAddEntete.Rows[0]["NoIntervention"] = lNoIntervention;
                    rsAddEntete.Rows[0]["CodeImmeuble"] = sCodeImmeuble;
                    rsAddEntete.Rows[0]["NbInterMoisDernier"] = lnbInterMoisDernier;
                    rsAddEntete.Rows[0]["NbPanneEcs"] = lNbPanneECS;
                    rsAddEntete.Rows[0]["NbPanneCh"] = lNbPanneCH;
                    rsAddEntete.Rows[0]["NbPanneEcsCh"] = lNbPanneEcsCh;
                    rsAddEntete.Rows[0]["NbPanneAutre"] = lnbPanneAutre;
                    rsAddEntete.Rows[0]["NbInterFacturee"] = lnbPanneFacture;
                    rsAddEntete.Rows[0]["NbInterAstreinte"] = lNbAstreinte;
                    rsAddEntete.Rows[0]["CreeLe"] = dtNow;
                    rsAddEntete.Rows[0]["CreePar"] = sUser;
                    if (General.IsDate(sDateDerniereVisite))
                    {
                        rsAddEntete.Rows[0]["DateDerniereVisite"] = dtDateDerniere;
                    }
                    modAdorsAddEntete.Update();
                }

                sInfoStat = "Date derniére visite d'entretien : " + General.nz(sSQLt, "NC") + " " + cRetChario;
                sInfoStat = sInfoStat + "Intervention du mois dernier : " + lnbInterMoisDernier + " " + cRetChario;
                sInfoStat = sInfoStat + "Pannes Ecs : " + lNbPanneECS + " " + cRetChario;
                sInfoStat = sInfoStat + "Pannes CH : " + lNbPanneCH + " " + cRetChario;
                sInfoStat = sInfoStat + " Pannes CH + ECS : " + lNbPanneEcsCh + " " + cRetChario;
                sInfoStat = sInfoStat + "Pannes Astreinte  " + lNbAstreinte + " " + cRetChario + cRetChario;

                modAdorsint.Close();

                //.Update
                //End With


                if (General.sInfoPresetationV2 == "1" || Convert.ToBoolean(bEtat) == true)
                {


                    sMessageErr = ModP2v2.fc_SimulationCreeCalendrierV2(dtDebut.Value, 0, dtFin.Value, true, "", sCodeImmeuble, "", "", "", false, 0, "1");

                    sSQL = "SELECT     Immeuble.CodeImmeuble, Immeuble.Gardien, Immeuble.TelGardien, Immeuble.HorairesLoge," + " Immeuble.CodeAcces1, Immeuble.CodeAcces2, " +
                        " Immeuble.SpecifAcces , IMC_ImmCorrespondant.Nom_IMC, IMC_ImmCorrespondant.CodeQualif_QUA " + " FROM         Immeuble left outer JOIN " +
                        " IMC_ImmCorrespondant ON Immeuble.CodeImmeuble = IMC_ImmCorrespondant.CodeImmeuble_IMM " + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                    modAdorsImm = new ModAdo();
                    rsImm = modAdorsImm.fc_OpenRecordSet(sSQL);

                    if (rsImm.Rows.Count > 0)
                    {

                        rsAddEntete.Rows[0]["Gardien"] = rsImm.Rows[0]["Gardien"];
                        rsAddEntete.Rows[0]["TelGardien"] = rsImm.Rows[0]["TelGardien"];
                        rsAddEntete.Rows[0]["HorairesLoge"] = rsImm.Rows[0]["HorairesLoge"];
                        rsAddEntete.Rows[0]["codeAcces1"] = rsImm.Rows[0]["codeAcces1"];
                        rsAddEntete.Rows[0]["CodeAcces2"] = rsImm.Rows[0]["CodeAcces2"];
                        rsAddEntete.Rows[0]["SpecifAcces"] = rsImm.Rows[0]["SpecifAcces"];
                        rsAddEntete.Rows[0]["CodeQualif_QUA"] = rsImm.Rows[0]["CodeQualif_QUA"];
                        rsAddEntete.Rows[0]["NomCorrespondant"] = rsImm.Rows[0]["nom_imc"];
                        modAdorsAddEntete.Update();
                    }
                    modAdorsImm.Close();
                    xi = 0;

                    if (ModP2v2.TpPrest != null)
                    {
                        for (xi = 0; xi <= ModP2v2.TpPrest.Length - 1; xi++)
                        {
                            if (!string.IsNullOrEmpty(ModP2v2.TpPrest[xi].sGAI_LIbelle))
                            {
                                var NewRow = rsAddt.NewRow();
                                NewRow["NoIntervention"] = lNoIntervention;
                                NewRow["CodeImmeuble"] = sCodeImmeuble;
                                //rsAddt!CodeArticle = rsT!CodeArticle
                                NewRow["designation"] = ModP2v2.TpPrest[xi].sGAI_LIbelle;
                                //rsAddt!Commentaire = rsT!Commentaire
                                NewRow["Intervenant"] = ModP2v2.TpPrest[xi].sMatricule;
                                NewRow["Devis"] = 0;
                                NewRow["AFaire"] = General.nz(ModP2v2.TpPrest[xi].lAFaireCemois, 0);
                                rsAddt.Rows.Add(NewRow);
                                modAdorsAddt.Update();
                            }
                        }
                    }

                }
                else
                {
                    //=== controle que l'intervenant est l'intervenant par defaut de l'immeuble, si vrai
                    //===  on recherche les autres prestations à effectuer apr les autres intervenants(par ex un sous traitant)
                    //===  pour chaque immeuble.
                    sSQLt = "SELECT CodeDepanneur From Immeuble " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'" +
                        " AND CodeDepanneur = '" + StdSQLchaine.gFr_DoublerQuote(sMat) + "'";
                    using (var tmpModAdo = new ModAdo())
                        sSQLt = tmpModAdo.fc_ADOlibelle(sSQLt);


                    //===  ajout de toutes les prestations asscoiées à l'immmeuble quelque soit l'intervenant.
                    sSQL = "SELECT   PreparationIntervention.Codeimmeuble, PreparationIntervention.Commentaire, PreparationIntervention." + General.fc_ReturnMonthParam(sMonth, true) +
                        "," + " PreparationIntervention.CodeArticle, FacArticle.Designation1, PreparationIntervention.Intervenant " + " , Personnel.CodeQualif" +
                        " FROM PreparationIntervention LEFT OUTER JOIN " + " Personnel ON PreparationIntervention.Intervenant = Personnel.Matricule LEFT OUTER JOIN " +
                        " FacArticle ON PreparationIntervention.CodeArticle = FacArticle.CodeArticle " + " WHERE Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                    modAdorsT = new ModAdo();
                    rsT = modAdorsT.fc_OpenRecordSet(sSQL);


                    if (rsT.Rows.Count == 0)
                    {
                        sInfoPresta = "";
                    }
                    else
                    {
                        sInfoPresta = "VISITES ET AVENANTS" + " " + cRetChario + cRetChario;
                    }



                    foreach (DataRow rsTRow in rsT.Rows)
                    {
                        sInfoPresta = sInfoPresta + "Opération: " + rsTRow["Designation1"] + " " + rsTRow["Commentaire"] + cRetChario;
                        using (var tmpModAdo = new ModAdo())
                            sInfoPresta = sInfoPresta + "Intervenant: " + tmpModAdo.fc_ADOlibelle("SELECT nom, prenom from personnel WHERE matricule ='" + rsTRow["Intervenant"] + "'", true) + cRetChario;

                        if (rsTRow[General.fc_ReturnMonthParam(sMonth, true)].ToString().ToUpper() == "True".ToUpper() ||
                             rsTRow[General.fc_ReturnMonthParam(sMonth, true)].ToString().ToUpper() == "1".ToUpper())
                        {
                            sInfoPresta = sInfoPresta + "Prévue pour le mois de: " + General.fc_ReturnMonthParam(sMonth) + cRetChario;
                        }

                        sInfoPresta = sInfoPresta + " " + cRetChario;

                    }
                    modAdorsT.Close();
                }

                //==== ajoute les 4 derniers devis en priorité les A, puis les 04, puis les 00.
                lNbDevis = 0;

                //=== devis en A.
                sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat" + " From DevisEnTete " +
                    " WHERE     (CodeEtat = 'A') AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "') " +
                    " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 " + " FROM          Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 " +
                    " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'))" + " ORDER BY DateCreation DESC ";

                modAdorsT = new ModAdo();
                rsT = modAdorsT.fc_OpenRecordSet(sSQL);

                sInfoDevis = "DEVIS" + cRetChario + cRetChario;

                foreach (DataRow rsTRow in rsT.Rows)
                {

                    sInfoDevis = sInfoDevis + "Titre: " + rsTRow["TitreDevis"] + cRetChario;
                    sInfoDevis = sInfoDevis + "Numéro: " + rsTRow["numerodevis"] + cRetChario;
                    sInfoDevis = sInfoDevis + "Date: " + rsTRow["DateAcceptation"] + cRetChario;
                    sInfoDevis = sInfoDevis + "Statut: Accepté" + cRetChario;

                    sInfoDevis = sInfoDevis + cRetChario;

                    if (Convert.ToBoolean(bEtat) == true)
                    {
                        var NewRow = rsAddt.NewRow();

                        NewRow["NoIntervention"] = lNoIntervention;
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["Devis"] = 1;
                        NewRow["numerodevis"] = rsTRow["numerodevis"];
                        NewRow["TitreDevis"] = rsTRow["TitreDevis"];
                        NewRow["DateDevis"] = rsTRow["DateAcceptation"];
                        NewRow["CodeEtatDevis"] = "A";
                        rsAddt.Rows.Add(NewRow);
                        modAdorsT.Update();
                    }
                    //sInfoDevis = sInfoDevis & rsT!TitreDevis & " - " & rsT!numerodevis & " - " & rsT!DateAcceptation & " - statut: " & rsT!CodeEtat & cRetChario
                    lNbDevis = lNbDevis + 1;

                }

                modAdorsT.Close();
                //=== devis en 04.
                if (lNbDevis < 4)
                {
                    sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat" + " From DevisEnTete " +
                        " WHERE     (CodeEtat = '04') AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "') " + " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 " +
                        " FROM          Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 " + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'))" +
                        " ORDER BY DateImpression DESC ";

                    modAdorsT = new ModAdo();
                    rsT = modAdorsT.fc_OpenRecordSet(sSQL);

                    foreach (DataRow rsTRow in rsT.Rows)
                    {

                        sInfoDevis = sInfoDevis + "Titre: " + rsTRow["TitreDevis"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Numéro: " + rsTRow["numerodevis"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Date: " + rsTRow["DateAcceptation"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Statut: Envoyé au client" + cRetChario;

                        sInfoDevis = sInfoDevis + cRetChario;

                        if (Convert.ToBoolean(bEtat) == true)
                        {
                            var NewRow = rsAddt.NewRow();

                            NewRow["NoIntervention"] = lNoIntervention;
                            NewRow["CodeImmeuble"] = sCodeImmeuble;
                            NewRow["Devis"] = 1;
                            NewRow["numerodevis"] = rsTRow["numerodevis"];
                            NewRow["TitreDevis"] = rsTRow["TitreDevis"];
                            NewRow["DateDevis"] = rsTRow["DateImpression"];
                            NewRow["CodeEtatDevis"] = "04";
                            rsAddt.Rows.Add(NewRow);
                            modAdorsT.Update();
                        }
                        lNbDevis = lNbDevis + 1;

                    }
                    modAdorsT.Close();
                }

                //=== devis en 00.
                if (lNbDevis < 4)
                {
                    sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat" + " From DevisEnTete " +
                        " WHERE     (CodeEtat = '00') AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "') " + " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 " +
                        " FROM          Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 " + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'))" +
                        " ORDER BY DateCreation DESC ";

                    modAdorsT = new ModAdo();
                    rsT = modAdorsT.fc_OpenRecordSet(sSQL);

                    foreach (DataRow rsTRow in rsT.Rows)
                    {

                        sInfoDevis = sInfoDevis + "Titre: " + rsTRow["TitreDevis"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Numéro: " + rsTRow["numerodevis"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Date: " + rsTRow["DateAcceptation"] + cRetChario;
                        sInfoDevis = sInfoDevis + "Statut: En attente " + cRetChario;

                        sInfoDevis = sInfoDevis + cRetChario;

                        if (Convert.ToBoolean(bEtat) == true)
                        {
                            var NewRow = rsAddt.NewRow();
                            NewRow["NoIntervention"] = lNoIntervention;
                            NewRow["CodeImmeuble"] = sCodeImmeuble;
                            NewRow["Devis"] = 1;
                            NewRow["numerodevis"] = rsTRow["numerodevis"];
                            NewRow["TitreDevis"] = rsTRow["TitreDevis"];
                            NewRow["DateDevis"] = rsTRow["DateCreation"];
                            NewRow["CodeEtatDevis"] = "00";
                            rsAddt.Rows.Add(NewRow);
                        }
                        lNbDevis = lNbDevis + 1;

                    }
                    modAdorsT.Close();
                }


                modAdorsT?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modPDA;fc_infostat");
            }

        }
        public static string fc_NumeroBadge(string sCodeImmeuble)
        {

            //=== affecte un numéro de bagde.

            DataTable rs = default(DataTable);
            string sSQL = null;
            string stemp = null;
            int lTot = 0;

            sSQL = "SELECT     NOauto FROM IMMEUBLE " + " WHERE Codeimmeuble ='"
                 + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
            var tmpAdors = new ModAdo();

            rs = tmpAdors.fc_OpenRecordSet(sSQL);

            if (rs.Rows.Count > 0)
            {

                stemp = Convert.ToString(rs.Rows[0]["Noauto"]);


            }
            rs.Dispose();

            //==== affecte un indice correspondant aux differentes localisations, ex; chauferie =1, sous station = 2....

            sSQL = "SELECT     COUNT(Codeimmeuble) AS tot"
                + " From PDAB_BadgePDA"
                + " GROUP BY Codeimmeuble, PDAB_IDBARRE "
                + " HAVING Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble)
                + "' and (PDAB_IDBARRE <> '' or PDAB_IDBARRE is not null)";

            rs = tmpAdors.fc_OpenRecordSet(sSQL);

            if (rs.Rows.Count == 0)
            {
                lTot = 1;
            }
            else
            {
                lTot = 0;
                foreach (DataRow rsR in rs.Rows)
                {
                    lTot = lTot + Convert.ToInt16(General.nz(rsR["tot"], 0));

                }
                lTot = lTot + 1;
            }

            rs.Dispose();
            rs = null;
            return stemp + "-" + lTot;

        }
        public static void fc_AddPDABimm(string sCodeImmeuble, bool bAjout, string sLibelle)
        {
            //==== fonction destiné à gérer les badges, crée au moins une localisation par défaut
            //=== si celle ci n'existe pas. si bAjout est à true on
            DataTable rs = default(DataTable);
            string sSQL = null;
            string stemp = null;

            try
            {
                sSQL = "SELECT PDAB_Noauto, PDAB_Libelle, PDAB_NonActif, "
                    + " PDAB_LieuBadge, Codeimmeuble, PDAB_ImageCB, Images, PDAB_IDBarre"
                    + " From PDAB_BadgePDA"
                    + " WHERE     Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                var NewRow = rs.NewRow();
                if (rs.Rows.Count == 0 || bAjout == true)
                {


                    NewRow["PDAB_NonActif"] = 0;
                    NewRow["CodeImmeuble"] = sCodeImmeuble;
                    if (bAjout == true && !string.IsNullOrEmpty(sLibelle))
                    {
                        NewRow["PDAB_Libelle"] = sLibelle;
                    }
                    else
                    {
                        NewRow["PDAB_Libelle"] = "Chaufferie";
                    }

                    //stemp = fc_NumeroBadge(sCodeimmeuble)
                    //rs!PDAB_IDBarre = stemp
                    rs.Rows.Add(NewRow);
                    int xx = tmpAdors.Update();
                    // fc_TreeBadge sCodeImmeuble

                }

                rs.Dispose();
                rs = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPda;fc_AddPDABimm");
            }
        }
        public static void App_Info(string sCodeImmeuble, string sCodeNumOrdre, string sCodeChaufferie, string sNoIntervention)
        {
            bool blnAmont;
            string sOrigineP1;
            string sDiametreCuve;
            DataTable rsAdo = new DataTable();

            try
            {
                General.SQL = "SELECT NumAppareil, Libelle, Energie,"
                   + " Amont, Aval, Type_Compteur, OrigineP1, DiametreCuve"
                   + " FROM Imm_Appareils ";
                General.SQL = General.SQL + " WHERE CodeImmeuble = '" + sCodeImmeuble + "'";
                General.SQL = General.SQL + " AND CodeNumOrdre = '" + sCodeNumOrdre + "'";
                General.SQL = General.SQL + " AND CodeChaufferie = '" + sCodeChaufferie + "'";
                General.SQL = General.SQL + " AND NumAppareil NOT IN (SELECT NumAppareil FROM INT_Releve WHERE NoIntervention = " + sNoIntervention + ")";
                General.SQL = General.SQL + " ORDER BY NumAppareil";
                ModAdo ADOlibelle = new ModAdo();

                wNb = 0;
                if (rsAdo.Rows.Count > 0)
                {
                    foreach (DataRow rs in rsAdo.Rows)
                    {
                        strEnergie = "";

                        sDiametreCuve = rs["DiametreCuve"] + "";

                        if (Convert.ToInt16(rs["Amont"]) == 1)
                        {
                            strEnergie = rs["Type_Compteur"] + "";
                            blnAmont = true;
                        }
                        else if (Convert.ToInt16(rs["Aval"]) == 1)
                        {
                            strEnergie = rs["Type_Compteur"] + "";
                            blnAmont = false;
                        }
                        else
                        {
                            strEnergie = rs["Type_Compteur"] + "";
                            blnAmont = false;
                        }
                        ChkLiv = Convert.ToInt16(General.nz(ADOlibelle.fc_ADOlibelle("SELECT Livraison FROM P1_TypeCpt WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(strEnergie) + "'"), 0));

                        sOrigineP1 = General.nz(rs["OrigineP1"] + "", "0").ToString();

                        // Procédure permettant de charger le dernier relevé connu pour chaque compteur
                        Ins_DernierRel(Convert.ToInt16(rs["NumAppareil"]), rs["Libelle"] + "", sNoIntervention, sCodeImmeuble, "", "", 0, "", sOrigineP1, sDiametreCuve, true);
                    }
                }
                else
                    strEnergie = "";
                rsAdo.Clear();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPDA.bas;App_Info;");

                //Screen.MousePointer = vbDefault;
                return;
            }
        }
        public static double fc_GetVolumePige(double dblSaisiePige, string sApp)
        {
            double dblCapaciteCuve;
            double dblDiametreCuve;
            double dblVolumePerCent;
            double dblHauteurPerCent;
            double dblVolumeCalcule;
            string sReq;
            double VolumePige;


            try
            {

                if (dblSaisiePige < 10)
                    dblSaisiePige = dblSaisiePige * 100;
                var ADOlibelle = new ModAdo();
                dblDiametreCuve = System.Convert.ToDouble(General.nz(ADOlibelle.fc_ADOlibelle("SELECT DiametreCuve FROM Imm_Appareils WHERE NumAppareil = " + General.nz(sApp, "0")), 0));
                if (dblDiametreCuve == 0)//tested
                {
                    return -1;
                }

                dblCapaciteCuve = System.Convert.ToDouble(General.nz(ADOlibelle.fc_ADOlibelle("SELECT Niveau_Maxi FROM P1_Appareil WHERE NumAppareil = " + General.nz(sApp, "0"), false, ModP1.adoP1), 0));
                if (dblCapaciteCuve == 0)
                {
                    return -1;
                }

                dblHauteurPerCent = dblSaisiePige * 100 / dblDiametreCuve;

                dblVolumePerCent = Convert.ToDouble(ADOlibelle.fc_ADOlibelle("SELECT TOP 1 VolPerCent FROM Type_CalculPige WHERE HPerCent >= " + dblHauteurPerCent + " ORDER BY VolPerCent ASC"));

                dblVolumeCalcule = dblCapaciteCuve * dblVolumePerCent / 100;

                return dblVolumeCalcule;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "ModPDA.bas;fc_GetVolumePige;");
                return -1;
            }

        }
        public static void fc_MajIndexFin()
        {

            string sReq;
            DataTable rsReleve;
            double SaisiePige;
            double ConsoPige;
            double ConsoJauge;
            double IndexFin;
            ModAdo M = new ModAdo();
            sReq = "SELECT * FROM INT_RELEVE ";
            sReq = sReq + " WHERE (Etat = 'RP1' OR Etat = 'RV') ";
            sReq = sReq + " AND COALESCE(IndexFin, 0) = 0 ";
            sReq = sReq + " AND ((SaisiePige IS NOT NULL AND SaisiePige <> 0) ";
            sReq = sReq + " OR (SaisieJauge IS NOT NULL AND SaisieJauge <> 0))";
            try
            {
                rsReleve = M.fc_OpenRecordSet(sReq);

                {

                    if (rsReleve.Rows.Count > 0)
                    {
                        foreach (DataRow RR in rsReleve.Rows)
                        {
                            SaisiePige = Convert.ToDouble(General.nz(RR["SaisiePige"], 0));
                            ConsoJauge = Convert.ToDouble(General.nz(RR["SaisieJauge"], 0));

                            if (SaisiePige != 0)
                            {
                                ConsoPige = fc_GetVolumePige(SaisiePige, System.Convert.ToString(RR["NumAppareil"] + ""));
                                if (ConsoPige != -1)
                                    IndexFin = ConsoPige;
                                else
                                    IndexFin = ConsoJauge;
                            }
                            else
                                IndexFin = ConsoJauge;

                            RR["IndexFin"] = IndexFin;
                            M.Update();

                            //RR.MoveNext();
                        }
                    }
                    ModAdo.fc_CloseRecordset(rsReleve);
                }

                sReq = "SELECT * FROM P1_RELEVE ";
                sReq = sReq + " WHERE (COALESCE(NoIntervention, 0) <> 0) ";
                sReq = sReq + " AND COALESCE(IndexFin, 0) = 0 ";
                sReq = sReq + " AND ((SaisiePige IS NOT NULL AND SaisiePige <> 0) ";
                sReq = sReq + " OR (SaisieJauge IS NOT NULL AND SaisieJauge <> 0))";

                rsReleve = M.fc_OpenRecordSet(sReq, null, "", ModP1.adoP1);
                {
                    var withBlock = rsReleve;
                    if (rsReleve.Rows.Count > 0)
                    {
                        foreach (DataRow RR in rsReleve.Rows)
                        {
                            SaisiePige = Convert.ToDouble(General.nz(RR["SaisiePige"], 0));
                            ConsoJauge = Convert.ToDouble(General.nz(RR["SaisieJauge"], 0));

                            if (SaisiePige != 0)
                            {
                                ConsoPige = fc_GetVolumePige(SaisiePige, System.Convert.ToString(RR["NumAppareil"] + ""));
                                if (ConsoPige != -1)
                                    IndexFin = ConsoPige;
                                else
                                    IndexFin = ConsoJauge;
                            }
                            else
                                IndexFin = ConsoJauge;

                            RR["IndexFin"] = IndexFin;
                            M.Update();
                        }
                    }
                    ModAdo.fc_CloseRecordset(rsReleve);
                }
                rsReleve = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Module : ModPDA;fc_MajIndexFin;");
                return;
            }
        }
    }
}
