﻿using Redemption;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;
using Exception = System.Exception;

namespace Axe_interDT.Shared
{
    static class ModCourrier
    {
        public static string ObjetMail;
        public static string MessageMail;
        public static string AdresseMail_Renamed { get; set; }
        public static string FichierAttache { get; set; }
        public static string list;
        public static string ImportanceMail;
        public static short nbrChampsTabCol;

        public static bool EnvoieMail { get; set; }

        public const string cTypeMsgAppel = "MsgAppel";
        public const string cTypeMsgInterv = "MsgInterv";
        public const string cTypeMsgBcmd = "MsgBcmd";
        public const string cTypeMsgDevis = "MsgDevis";

        public const string cTypeCorrespEm = "strEm";
        public const string cTypeCorrespDest = "strDest";
        public const string cTypeCorrespCopie = "strCopie";
        public const string cTypeCorrespCopieCachee = "strCopieCachee";

        public static string[] tabFichierAttache;
        public static string[] tabCopie;
        public static string[] tabCopieCachee;

        public static Microsoft.Office.Interop.Outlook.Application golApp;
        public static Microsoft.Office.Interop.Outlook.NameSpace golNameSpace;

        public static object RedNameSpace;

        public static Microsoft.Office.Interop.Outlook.Explorer MonExplorer;

        public const string cDomaineMail = "@delostaletthibault.fr";
        const string cExpediteur = "rst@delostaletthibault.Fr";

        //Public sTabAdresseMail() As String
        //Public sTabNomAdresse() As String

        public struct AdresseMail
        {
            public string sNom;
            public string sMail;
        }

        public static AdresseMail[] sTabAdresseMail;

        public const string cVide = "VIDE";

        public static bool InitialiseEtOuvreOutlook(bool blnAffMsg = true)
        {
            bool functionReturnValue = false;

            Microsoft.Office.Interop.Outlook.Inspector MYInspector = default(Microsoft.Office.Interop.Outlook.Inspector);
            Microsoft.Office.Interop.Outlook.NoteItem myItem = default(Microsoft.Office.Interop.Outlook.NoteItem);
            try
            {
                //Initialisation et ouverture d'outlook
                golApp = new Microsoft.Office.Interop.Outlook.Application();
                golNameSpace = golApp.GetNamespace("MAPI");
                while (golApp.ActiveInspector() != null)
                {
                    //            If MsgBox("Souhaitez-vous sauvegarder l'élément : " & golApp.ActiveInspector.CurrentItem.subject, vbYesNo, "Elément Outlook ouvert") = vbOK Then
                    golApp.ActiveInspector().Close(Microsoft.Office.Interop.Outlook.OlInspectorClose.olSave);
                    //            Else
                    //                golApp.ActiveInspector.Close olDiscard
                    //            End If

                }
                MonExplorer = null;
                if (MonExplorer == null)
                {
                    MonExplorer = golNameSpace.GetDefaultFolder(Microsoft.Office.Interop.Outlook.OlDefaultFolders.olFolderTasks).GetExplorer();
                }
                functionReturnValue = true;

                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                if (blnAffMsg == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'accéder à Outlook : " + e.Message, "Envoie d'un mail", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                EnvoieMail = false;
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        public static void InitAdressListOutlook345_V2()
        {
            Redemption.RDOSession Session = null;
            Microsoft.Office.Interop.Outlook.Application application = default(Microsoft.Office.Interop.Outlook.Application);
            Redemption.RDOAddressList Contacts = null;
            Redemption.RDOAddressEntries contactRDO = null;
            int i = 0;
            int X = 0;
            Redemption.MAPITable Table = null;
            //DataTable rs = default(DataTable);
            ModAdo modAdoDataTable;

            try
            {
                sTabAdresseMail = new AdresseMail[1];

                //MsgBox "V2 outlook"

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                Session = new Redemption.RDOSession();

                if (Session.LoggedOn == false)
                {
                    Session.Logon();
                }
                else
                {
                    //MsgBox "ok pour log"
                }

                Table = new Redemption.MAPITable();

                Contacts = Session.AddressBook.AddressLists().Item(Session.AddressBook.DefaultAddressList.Name);

                //Set contactRDO = Session.AddressBook.GAL.AddressEntries
                contactRDO = Contacts.AddressEntries;

                //rs = new DataTable();

                ADODB.Recordset rs = contactRDO.MAPITable.ExecSQL("SELECT \"DAV:displayname\" ,\"http://schemas.microsoft.com/mapi/proptag/0x39FE001F\" from Folder ");

                if (!(rs.EOF && rs.BOF))
                {

                    X = 0;
                    while (rs.EOF == false)
                    {
                        if (!string.IsNullOrEmpty(General.nz(rs.Fields[0].Value, "").ToString()) &&
                            !string.IsNullOrEmpty(General.nz(rs.Fields[1].Value, "").ToString()))
                        {

                            Array.Resize(ref sTabAdresseMail, X + 1);
                            sTabAdresseMail[X].sNom = rs.Fields[0].Value.ToString() + "";

                            Array.Resize(ref sTabAdresseMail, X + 1);
                            sTabAdresseMail[X].sMail = rs.Fields[1].Value.ToString() + "";
                            X = X + 1;
                        }
                        rs.MoveNext();
                    }
                    //foreach (DataRow rsRow in rs.Rows)
                    //{
                    //    //Debug.Print rs.rows[0][0).value & "   -  " & rs.rows[0][1).value
                    //    if (!string.IsNullOrEmpty(General.nz(rsRow[0], "").ToString()) &&
                    //        !string.IsNullOrEmpty(General.nz(rsRow[1], "").ToString()))
                    //    {

                    //        Array.Resize(ref sTabAdresseMail, X + 1);
                    //        sTabAdresseMail[X].sNom = rsRow[0].ToString();

                    //        Array.Resize(ref sTabAdresseMail, X + 1);
                    //        sTabAdresseMail[X].sMail = rsRow[1].ToString();
                    //        X = X + 1;
                    //    }
                    //}
                }
                //Set Contacts = Nothing
                contactRDO = null;

                Session = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        public static void InitAdressListOutlook345()
        {
            Redemption.RDOSession Session = null;
            Microsoft.Office.Interop.Outlook.Application application = null;
            Redemption.RDOAddressList Contacts = null;
            Redemption.RDOAddressEntries contactRDO = null;
            int i = 0;
            int X = 0;

            //Set Session = CreateObject("Redemption.RDOSession")
            //Set application = CreateObject("Outlook.Application")
            //Session.MAPIOBJECT = application.Session.MAPIOBJECT
            //Set Contacts = Session.AddressBook.AddressLists.Item("Liste d'adresses globale")
            //Set Contacts = Session.AddressBook.AddressLists.Item("" & Text1 & "")
            //MsgBox Contacts.AddressEntries.Count


            try
            {
                if (General.sContactMailViaBase == "1")
                {
                    InitContactViaBase();
                    return;
                }

                sTabAdresseMail = new AdresseMail[1];

                //If UCase(sTabAdresseMail(0)) = UCase(cVide) Then

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                Session = new Redemption.RDOSession();
                application = new Microsoft.Office.Interop.Outlook.Application();
                Session.MAPIOBJECT = application.Session.MAPIOBJECT;

                Redemption.MAPITable Table = null;
                //DataTable rs = default(DataTable);

                Table = new Redemption.MAPITable();
                Contacts = Session.AddressBook.AddressLists().Item(Session.AddressBook.DefaultAddressList.Name);
                //Set contactRDO = Session.AddressBook.GAL.AddressEntries
                contactRDO = Contacts.AddressEntries;

                //rs = new DataTable();


                ADODB.Recordset rs = contactRDO.MAPITable.ExecSQL("SELECT \"DAV:displayname\" ,\"http://schemas.microsoft.com/mapi/proptag/0x39FE001F\" from Folder ");

                if (!(rs.EOF && rs.BOF))
                {
                    X = 0;
                    while (rs.EOF == false)
                    {
                        //Debug.Print rs.rows[0][0).value & "   -  " & rs.rows[0][1).value
                        if (!string.IsNullOrEmpty(General.nz(rs.Fields[0].Value, "").ToString()) &&
                            !string.IsNullOrEmpty(General.nz(rs.Fields[1].Value, "").ToString()))
                        {

                            Array.Resize(ref sTabAdresseMail, X + 1);
                            sTabAdresseMail[X].sNom = rs.Fields[0].Value.ToString();

                            Array.Resize(ref sTabAdresseMail, X + 1);
                            sTabAdresseMail[X].sMail = rs.Fields[1].Value.ToString();
                            X = X + 1;
                        }
                        rs.MoveNext();
                    }
                }

                Contacts = null;
                contactRDO = null;
                application = null;
                Session = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                return;

                //        Set rs = contactRDO.MAPITable.ExecSQL("SELECT ""urn:schemas:httpmail:subject"", ""DAV:getlastmodified"" from Folder " & _
                //'                                                   "where ""urn:schemas:httpmail:subject"" IN ('bonjour') " & _
                //'                                                   "order by ""DAV:getlastmodified"" desc")
                //
                //    Do While rs.EOF = False
                //
                //        rs.MoveNext
                //    Loop
                //     Set rs = New ADODB.Recordset
                //    'Table.Item = application.ActiveExplorer.CurrentFolder.Items
                //    Set rs = contactRDO.MAPITable.ExecSQL("SELECT  Subject from Folder " & _
                //'                                                      "where ( Body like '*un traitement plus rapide*') " & _
                //'                                                      "")
                //     Set rs = New ADODB.Recordset
                //    Set rs = contactRDO.MAPITable.ExecSQL("SELECT ""urn:schemas:httpmail:subject"", ""DAV:getlastmodified"" from Folder ")
                //         If Not (rs.EOF And rs.bof) Then
                //                MsgBox "ok"
                //        End If
                //
                //    While Not rs.EOF = False
                //        Debug.Print (rs.rows[0]["Subject").value) ' '& " - " & rs.rows[0]["EntryID").value)
                //        rs.MoveNext
                //    Wend
                //
                //
                //
                //    'Set Contacts = Session.AddressBook.AddressLists.Item("Contacts")
                //    Set Contacts = Session.AddressBook.AddressLists.Item(Session.AddressBook.DefaultAddressList.Name)
                //    'Set contactRDO = Session.AddressBook.GAL.AddressEntries
                //    Set contactRDO = Contacts.AddressEntries
                //
                //      On Error GoTo 0
                //
                //      On Error GoTo erreurPDA
                //
                //      For i = 1 To Contacts.AddressEntries.Count
                //             Dim ok
                //             ok = contactRDO.MAPITable.GetRows(214)
                //
                //            If i = 1 Then
                //
                //                contactRDO.GetFirst
                //                ReDim Preserve sTabAdresseMail(X)
                //                 sTabAdresseMail(X).sNom = contactRDO.Item(i).Name
                //
                //
                //                ReDim Preserve sTabAdresseMail(X)
                //                sTabAdresseMail(X).sMail = contactRDO.Item(i).Address   ' sTabAdresseMail(x).sNom
                //
                //            Else
                //                contactRDO.GetNext
                //
                //                ReDim Preserve sTabAdresseMail(X)
                //                sTabAdresseMail(X).sNom = contactRDO.Item(i).Name
                //
                //                ReDim Preserve sTabAdresseMail(X)
                //                sTabAdresseMail(X).sMail = contactRDO.Item(i).Address
                //            End If
                //            X = X + 1
                //    Next i
                //
                //    Set Contacts = Nothing
                //    Set contactRDO = Nothing
                //    Set application = Nothing
                //    Set Session = Nothing
                //
                //    Screen.MousePointer = 1

                //End If
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        public static void InitContactViaBase()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = new ModAdo();
            int X = 0;

            try
            {
                sSQL = "SELECT     Nom, Prenom, Matricule, Email From Personnel " + " ORDER BY Nom";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                sTabAdresseMail = new AdresseMail[1];

                foreach (DataRow rsRow in rs.Rows)
                {

                    if (!string.IsNullOrEmpty(General.nz(rsRow["Email"], "").ToString()))
                    {

                        Array.Resize(ref sTabAdresseMail, X + 1);
                        sTabAdresseMail[X].sNom = rsRow["Nom"] + " " + rsRow["Prenom"];

                        Array.Resize(ref sTabAdresseMail, X + 1);
                        sTabAdresseMail[X].sMail = General.nz(rsRow["Email"], "").ToString();
                        X = X + 1;
                    }

                }

                rs = null;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Erreurs.gFr_debug(e, "ModuleMail;InitContactViaBase"); ;
            }

        }
        public static bool CreateMail(object vDestinat, object vCopie, object sSujet, object sMessage, object vAttach = null, string Separateur = "\t", string strChemin = "")
        {
            bool functionReturnValue = false;
            Microsoft.Office.Interop.Outlook.Application application = null;
            Microsoft.Office.Interop.Outlook.NameSpace namespace_Renamed = null;
            Redemption.SafeMailItem SafeItem = null;
            bool bResolveSuccess = false;
            Redemption.MAPIUtils utils = null;
            object oItem = null;
            Redemption.SafeRecipient Copie = null;
            int i = 0;
            int j = 0;
            int Debut = 0;
            int FIN = 0;
            string[] tTabDest = null;

            try
            {
                application = new Microsoft.Office.Interop.Outlook.Application();

                namespace_Renamed = application.GetNamespace("MAPI");

                namespace_Renamed.Logon();
                SafeItem = new Redemption.SafeMailItem();
                oItem = application.CreateItem(0);
                //Create a new message
                SafeItem.Item = oItem;

                SafeItem.Recipients.Add(Convert.ToString(vDestinat));
                // .Recipients.ResolveAll
                if (General.Right(vCopie.ToString(), 1) != ";" && !string.IsNullOrEmpty(vCopie.ToString()))
                {
                    vCopie = vCopie + ";";
                }

                if (!string.IsNullOrEmpty(vCopie.ToString()))
                {
                    tTabDest = vCopie.ToString().Split(';');
                    for (j = 0; j <= tTabDest.Length - 1; j++)
                    {
                        //If InStr(1, tTabDest(j), "@") > 0 Then


                        Copie = SafeItem.Recipients.Add(tTabDest[j]);
                        Copie.Type = (int)Microsoft.Office.Interop.Outlook.OlMailRecipientType.olCC;
                        //Else
                        //   . .tTabDest (j)
                        //End If

                    }
                    //Set Copie = .Recipients.Add(vCopie)

                }


                if ((vAttach != null))
                {
                    if (!string.IsNullOrEmpty(vAttach.ToString()) && vAttach.ToString().ToUpper() != " Pas de document attaché".ToUpper())
                    {
                        if (vAttach.ToString().Contains(Separateur) == false)
                        {

                            SafeItem.Attachments.Add(vAttach, null, sMessage.ToString().Length + 2);
                        }
                        else
                        {
                            Debut = 0;
                            FIN = 1;
                            FIN = vAttach.ToString().IndexOf(Separateur, FIN);
                            SafeItem.Attachments.Add(General.Left(vAttach.ToString(), FIN - 1), null, sMessage.ToString().Length + 1);

                            i = 1;
                            do
                            {
                                i = i + 1;
                                FIN = FIN + 1;
                                Debut = FIN;
                                FIN = vAttach.ToString().IndexOf(Separateur, FIN);
                                if (FIN == 0)
                                {
                                    SafeItem.Attachments.Add(General.Mid(vAttach.ToString(), Debut, vAttach.ToString().Length - Debut + 1), null, sMessage.ToString().Length + i + 1);
                                    break;
                                }
                                SafeItem.Attachments.Add(General.Mid(vAttach.ToString(), Debut, FIN - Debut), null, sMessage.ToString().Length + i + 1);
                            } while (true);
                        }
                    }
                }
                bResolveSuccess = SafeItem.Recipients.ResolveAll();
                SafeItem.Item.Subject = sSujet;

                //===> Mondir le 30.11.2020 ajout des modifs de la version V12.11.2020, module mail à été supprimé
                if (!string.IsNullOrEmpty(General.sSignatureMail))
                {
                    sMessage = sMessage + Environment.NewLine + Environment.NewLine + General.sSignatureMail;
                }
                //===> Fin Modif Mondir

                SafeItem.Body = "\n" + sMessage;

                switch (ImportanceMail)
                {
                    case "Haute":
                        SafeItem.Item.Importance = Microsoft.Office.Interop.Outlook.OlImportance.olImportanceHigh;
                        break;
                    case "Normale":
                        SafeItem.Item.Importance = Microsoft.Office.Interop.Outlook.OlImportance.olImportanceNormal;
                        break;
                    case "Basse":
                        SafeItem.Item.Importance = Microsoft.Office.Interop.Outlook.OlImportance.olImportanceLow;
                        break;
                }

                if (bResolveSuccess)
                {
                    SafeItem.Send();
                }
                else
                {
                    SafeItem.Item.Display();
                }
                utils = new Redemption.MAPIUtils();
                utils.DeliverNow();

                if (!string.IsNullOrEmpty(strChemin))
                {
                    if (Dossier.fc_ControleDossier(strChemin) == false)
                    {
                        Dossier.fc_CreateDossier(strChemin);
                    }
                    SafeItem.SaveAs(strChemin + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour + "-" + DateTime.Now.Minute + "-" +
                                    DateTime.Now.Second + ".msg", 3);
                }


                //==== modif du 26 sept 2013.
                //NameSpace.Logoff
                oItem = null;
                SafeItem = null;
                namespace_Renamed = null;
                application = null;
                utils = null;

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                EnvoieMail = false;
                Erreurs.gFr_debug(e, " ModuleMail CreateMail() ", true);
                functionReturnValue = false;
                return functionReturnValue;
            }
        }
        public static void InitAdressListOutlookRDO()
        {

            int i = 0;
            int X = 0;
            Microsoft.Office.Interop.Outlook.Application application = default(Microsoft.Office.Interop.Outlook.Application);
            Microsoft.Office.Interop.Outlook.NameSpace namespace_Renamed = default(Microsoft.Office.Interop.Outlook.NameSpace);
            Redemption.RDOSession Session = null;
            string sT = null;
            Redemption.RDOAddressEntries contactRDO = null;
            Redemption.RDOAddressEntry MailSmtp = null;
            Redemption.RDOAddressList ListGlobalRDO = null;
            string sNom = null;
            object objcontact = null;

            try
            {
                if (sTabAdresseMail != null && sTabAdresseMail.Length > 0 && sTabAdresseMail[0].sNom.ToUpper() == cVide.ToUpper())
                {
                    if (General.sInitAdressListOutlook345_V2 == "1")
                    {
                        InitAdressListOutlook345_V2();
                    }
                    else
                    {
                        InitAdressListOutlook345();
                    }
                    return;

                    //Cursor.Current = Cursors.WaitCursor;

                    //Session = new RDOSessionClass();
                    //application = new ApplicationClass();
                    ////=== il existe plueirurs maniere d'ouvrir un session.
                    ////====      1) Session.MAPIOBJECT = application.Session.MAPIOBJECT
                    ////====      2) Session.LogonExchangeMailbox fncUserName, "mail.delostaletthibault.fr" (attention
                    ////====         sur le serveur exchange, decochez "Hide from Exchange address lists" sur la liste
                    ////====         des users.
                    //namespace_Renamed = application.GetNamespace("MAPI");
                    //Session.MAPIOBJECT = namespace_Renamed.Session.MAPIOBJECT;
                    ////    If Not Session.LoggedOn Then
                    ////            Session.Logon
                    ////
                    ////    End If


                    //contactRDO = Session.AddressBook.GAL.AddressEntries;

                    //contactRDO = Session.AddressBook.GAL.AddressEntries;
                    //for (i = 1; i <= contactRDO.Count; i++)
                    //{
                    //    // For i = 1 To Session.AddressBook.GAL.AddressEntries.Count

                    //    // i = 1
                    //    // Do
                    //    if (contactRDO.Count > 0)
                    //    {

                    //        if (i == 1)
                    //        {
                    //            MailSmtp = contactRDO.GetFirst();
                    //        }
                    //        else
                    //        {
                    //            MailSmtp = contactRDO.GetNext();
                    //        }

                    //        Array.Resize(ref sTabAdresseMail, X + 1);
                    //        if ((MailSmtp != null))
                    //        {
                    //            sTabAdresseMail[X].sNom = MailSmtp.Name;
                    //            sTabAdresseMail[X].sMail = MailSmtp.SMTPAddress;
                    //        }
                    //        X = X + 1;
                    //    }
                    //    //                i = i + 1
                    //    //                If i = 1000 Then
                    //    //                    Exit Do
                    //    //                End If
                    //    //Loop
                    //}

                    //// Session.Logoff

                    //MailSmtp = null;
                    //contactRDO = null;
                    ////   Set application = Nothing
                    //Session = null;

                    //Cursor.Current = Cursors.Arrow;
                    //
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'initialiser Outlook, l'envoie par mail sera impossible" + "\n" + e.Message, "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
        public static void InitAdressListOutlook()
        {

            int i = 0;
            Redemption.AddressLists rals = null;
            //As New Redemption.AddressLists
            Redemption.AddressList ral = null;
            int X = 0;
            object application = null;
            object namespace_Renamed = null;
            string stemp = null;

            try
            {
                if (sTabAdresseMail[0].sNom.ToUpper() == cVide.ToUpper())
                {
                    if (General.sInitAdressListOutlook345_V2 == "1")
                    {
                        InitAdressListOutlook345_V2();
                    }
                    else
                    {
                        InitAdressListOutlook345();
                    }
                    return;

                    //Cursor.Current = Cursors.WaitCursor;

                    //application = Interaction.CreateObject("Outlook.Application");
                    //namespace_Renamed = application.GetNamespace("MAPI");
                    //namespace_Renamed.Logon();

                    //rals = Interaction.CreateObject("Redemption.AddressLists");
                    //ral = rals.Item("Liste d'adresses globale");

                    ////    For i = 1 To rals.Count
                    ////          s = s & rals(i).Name & vbCrLf
                    ////    Next

                    //for (i = 1; i <= ral.AddressEntries.Count; i++)
                    //{

                    //    Array.Resize(ref sTabAdresseMail, X + 1);
                    //    sTabAdresseMail[X].sNom = ral.AddressEntries[i].Name;
                    //    sTabAdresseMail[X].sMail = ral.AddressEntries[i].SMTPAddress;

                    //    X = X + 1;
                    //}

                    //ral = null;
                    //rals = null;
                    //namespace_Renamed.Logoff();

                    //namespace_Renamed = null;
                    //application = null;

                    //Cursor.Current = Cursors.Arrow;

                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                if ((rals != null))
                {
                    rals = null;
                }
                if ((ral != null))
                {
                    ral = null;
                }

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'initialiser Outlook, l'envoie par mail sera impossible" + "\n" + e.Message, "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public static bool fc_CtrlAdresseMail(string sMail)
        {
            bool functionReturnValue = false;
            //=== retourne true si l'adresse mail a un format correct.
            try
            {
                functionReturnValue = false;

                if (!sMail.ToUpper().Contains(cDomaineMail.ToUpper()))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse mail doit contenir le domaine " + cDomaineMail + ", veuillez ressaisir cette adresse.", "Adresse mail incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                if (sMail.ToUpper().Contains(" "))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse mail ne doit pas contenir d'espace, veuillez ressaisir cette adresse.", "Adresse mail incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModuleMail;fc_CtrlAdresseMail");
                return functionReturnValue;
            }
        }

        public static object fc_SendLMailCDO(string sFROM, string sTo, string sSubject, string sBody)
        {
            object functionReturnValue = null;
            int i = 0;

            try
            {
                string sMessage = null;
                CDO.Message obj = null;



                i = i + 0;
                //sMessage = "test"

                obj = new CDO.Message();
                reStart:


                var _with4 = obj;

                _with4.From = sFROM;
                // "Administrateur" & cDomaine
                //.From = "Administrateur@delostaletthibault.fr"
                _with4.To = sTo;

                _with4.To = sTo;
                //"sif" & cDomaine
                //.To = "sif@delostaletthibault.fr"

                _with4.Subject = sSubject;
                _with4.HTMLBody = sBody;

                // .AddAttachment ("LIEN ABSOLU VERS PIECE JOINTE")
                _with4.Configuration.Fields["http://schemas.microsoft.com/cdo/configuration/sendusing"].Value = 2;
                _with4.Configuration.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserver"].Value = "DT-XCH-01";
                _with4.Configuration.Fields["http://schemas.microsoft.com/cdo/configuration/smtpserverport"].Value = 25;
                _with4.Configuration.Fields.Update();
                // On Error Resume Next
                _with4.Send();
                return functionReturnValue;
            }
            catch (Exception ex)
            {

                if (i >= 3)
                {
                    Erreurs.gFr_debug(ex, "ModuleMail;fc_SendLMailCDO");
                }
                else
                {
                    i = i + 1;
                    fc_SendLMailCDO(sFROM, sTo, sSubject, sBody);
                }
                return functionReturnValue;
            }
        }

        public static bool CreateMailRDO(string vDestinat, string vCopie, string sSujet, string sMessage, object vAttach = null, char Separateur = '\t', string strChemin = "")
        {
            bool CreateMailRDO = false;
            try
            {
                int i = 0;
                int j = 0;
                int Debut = 0;
                int FIN = 0;
                int X = 0;
                int Y = 0;


                bool bResolveSuccess;
                bool bNametoFind;
                string[] tTabDest = new string[] { };

                RDOSession Session;
                Microsoft.Office.Interop.Outlook.Application application;
                RDOFolder Inbox;
                RDOMail Mail;
                RDORecipient Copie = null;
                //Redemption.MAPIUtils utils;


                Session = new RDOSession();
                application = new Microsoft.Office.Interop.Outlook.Application();

                Session.MAPIOBJECT = application.Session.MAPIOBJECT;
                Inbox = Session.GetDefaultFolder(rdoDefaultFolders.olFolderInbox); //' olFolderInbox = 6

                Mail = Inbox.Items.Add();

                //===> Mondir le 23.04.2021, line bellow is commented https://groupe-dt.mantishub.io/view.php?id=2416
                //Mail.Recipients.Add(vDestinat.ToString());
                //===> Fin Modif Mondir

                if (vCopie != "")
                {
                    vCopie = vCopie.Trim();
                    //                 '=== remplace les espaces par le separateur point virgule.
                    // vCopie = vCopie.Replace(" ", ";");
                    //                '=== remplace les espaces par le separateur point virgule.
                    vCopie = vCopie.Replace(";;", ";");
                    //                 '=== controle que le dernier caractere est un point virgule.
                    if (General.Right(vCopie, 1) != ";")
                        vCopie = vCopie + ";";

                    tTabDest = vCopie.Split(';');
                }

                bNametoFind = false;

                if (sTabAdresseMail != null)
                {
                    for (X = 0; X <= sTabAdresseMail.Length - 1; X++)
                    {
                        if (sTabAdresseMail[X].sNom.ToUpper() == vDestinat.ToUpper())
                        {
                            Mail.Recipients.Add(sTabAdresseMail[X].sMail);
                            bNametoFind = true;
                            if (General.Right(vCopie, 1) == "")
                                break;
                        }

                        if (General.Right(vCopie, 1) != "")
                        {
                            for (Y = 0; Y <= tTabDest.Length - 1; Y++)
                                //===> Mondir le 05.10.2020, ajout de || sTabAdresseMail[X].sMail.ToUpper() == tTabDest[Y].ToUpper(), car le copie passé est le mail et pas le nom
                                if (sTabAdresseMail[X].sNom.ToUpper() == tTabDest[Y].ToUpper() || sTabAdresseMail[X].sMail.ToUpper() == tTabDest[Y].ToUpper())
                                {
                                    Copie = Mail.Recipients.Add(sTabAdresseMail[X].sMail);
                                    Copie.Type = 2;
                                    break;
                                }

                        }

                    }
                }
                //si le nom n'est pas dans la liste outlook(ex:adresse extérieure avec nom
                //de domaine differente

                if (bNametoFind == false)
                    Mail.Recipients.Add(vDestinat);

                if (vAttach != null)
                {
                    if (vAttach.ToString() != "" && vAttach.ToString().ToUpper() != "Pas de document attaché".ToUpper())
                    {
                        if (!vAttach.ToString().Contains(Separateur))
                        {
                            //                        '==== controle si le fichie existe.
                            if (!Dossier.fc_ControleFichier(vAttach.ToString()))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier " + vAttach + " n'existe pas, il ne sera pas envoyé.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                            }

                            else
                            {

                                Mail.Attachments.Add(vAttach, null, sMessage.Length + 2);
                            }
                        }
                        else
                        {
                            //==================> Code Modifié par Mondir

                            var files = vAttach.ToString().Split(Separateur);
                            foreach (var file in files)
                            {
                                //                                            '==== controle si le fichie existe.
                                if (!Dossier.fc_ControleFichier(file))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier " + file + " n'existe pas, il ne sera pas envoyé.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                                }
                                else
                                {
                                    Mail.Attachments.Add(file);
                                }
                            }

                            //Debut = 0;

                            //FIN = 1;
                            //FIN = vAttach.ToString().IndexOf(Separateur);

                            ////                                            '==== controle si le fichie existe.
                            //if (!Dossier.fc_ControleFichier(General.Left(vAttach.ToString(), FIN - 1)))
                            //{
                            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier " + (General.Left(vAttach.ToString(), FIN - 1)) + " n'existe pas, il ne sera pas envoyé.", "", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Information);
                            //}
                            //else
                            //{
                            //    Mail.Attachments.Add(General.Left(vAttach.ToString(), FIN - 1), null, sMessage.Length + 1);
                            //}

                            //i = 1;
                            //while (true)
                            //{
                            //    i = i + 1;
                            //    FIN = FIN + 1;
                            //    Debut = FIN;
                            //    FIN = vAttach.ToString().IndexOf(Separateur);
                            //    if (FIN == 0)
                            //    {
                            //        //                                        '==== controle si le fichie existe.
                            //        if (!Dossier.fc_ControleFichier(General.Mid(vAttach.ToString(), Debut, vAttach.ToString().Length - Debut + 1)))
                            //        {
                            //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier " + General.Mid(vAttach.ToString(), Debut, vAttach.ToString().Length - Debut + 1) + " n'existe pas, il ne sera pas envoyé.",
                            //                "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //        }
                            //        else
                            //        {
                            //            Mail.Attachments.Add(General.Mid(vAttach.ToString(), Debut, vAttach.ToString().Length - Debut + 1), null, sMessage.Length + i + 1);
                            //        }
                            //        break;
                            //    }
                            //    //'==== controle si le fichie existe.
                            //    if (!Dossier.fc_ControleFichier(General.Mid(vAttach.ToString(), Debut, FIN - Debut)))
                            //    {
                            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier " + General.Mid(vAttach.ToString(), Debut, FIN - Debut) + " n'existe pas, il ne sera pas envoyé.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            //    }
                            //    else
                            //    {
                            //        Mail.Attachments.Add(General.Mid(vAttach.ToString(), Debut, FIN - Debut), null, sMessage.Length + i + 1);
                            //    }
                            //}



                        }
                    }
                }


                bResolveSuccess = Mail.Recipients.ResolveAll();
                Mail.Subject = sSujet;
                //Mail.Body = "\r\n" + sMessage;

                //===> Mondir le 30.11.2020, this line was commented !
                sMessage = sMessage.Replace("\n", " <br> ") + " <br>" + " <br>";

                //===> Mondir le 30.11.2020, ajout des modfis de la version V12.11.2020 car le fichier ModuleMail a été supprimé
                if (General.bSignatureServFact)
                {
                    sMessage = sMessage + General.sSignatureServiceFact + " <br>";
                    General.bSignatureServFact = false;
                }

                if (!string.IsNullOrEmpty(General.sSignatureMail))
                {
                    sMessage = sMessage + General.sSignatureMail;
                }
                //===> Fin Modif Mondir

                Mail.HTMLBody = sMessage;

                //Mail.HTMLBody = sMessage;
                switch (ImportanceMail)
                {
                    case "Haute":
                        Mail.Importance = 2;//' olImportanceHigh
                        break;
                    case "Normale":
                        Mail.Importance = 1;//' olImportanceNormal
                        break;
                    case "Basse":
                        Mail.Importance = 0;//' olImportanceLow
                        break;
                    default:
                        break;
                }

                RDOSignature oSgn;
                RDOSignatures oSgns;
                RDOAccount account;

                account = Session.Accounts.GetOrder(rdoAccountCategory.acMail).Item(1);
                //'Set oSgns = Session.Accounts.GetOrder(2).Item(1)                
                if (account != null)
                {
                    oSgn = account.NewMessageSignature;
                    if (oSgn != null)
                    {
                        //' oSgn.Import "C:\temp\signature\test.html", olDoc
                        oSgn.ApplyTo(Mail, false); /* 'apply at the bottom*/

                        //'.HTMLBody = .HTMLBody & oSgn.HTMLBody
                    }
                }

                if (bResolveSuccess)
                    Mail.Send();
                else
                    Mail.Display();

                if (strChemin != "")
                {
                    if (!Dossier.fc_ControleDossier(strChemin))
                    {
                        Dossier.fc_ControleDossier(strChemin);
                    }
                    else
                    {
                        Mail.SaveAs(strChemin + DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day + "-" + DateTime.Now.Hour +
                            "-" + DateTime.Now.Minute + "-" + DateTime.Now.Second + ".msg", 3);
                    }
                }

                //       ' Set utils = CreateObject("Redemption.MAPIUtils")
                //       ' utils.DeliverNow

                // utils = null;

                if (Copie != null)
                    Copie = null;
                Mail = null;
                Inbox = null;
                Session = null;
                application = null;

                CreateMailRDO = true;

                return CreateMailRDO;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                CreateMailRDO = false;
                return CreateMailRDO;
            }
        }


    }
}
