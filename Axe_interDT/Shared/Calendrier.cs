﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    class Calendrier
    {
        // Text formats
        public const short TXT_LEFT = 0;
        public const short TXT_CENTER = 1;
        public const short TXT_RIGHT = 2;
        public const short TXT_LEFTML = 3;
        public const short TXT_CENTERML = 4;
        public const short TXT_RIGHTML = 5;

        // 3D rectangle modes
        public const short FLAT = 0;
        public const short RAISED = 1;
        public const short SUNKEN = 2;

        // Planning Header types
        public const short T_HOUR = 0;
        public const short T_DAY = 1;
        public const short T_WEEK = 2;
        public const short T_MONTH = 3;
        public const short T_YEAR = 4;

        // Planning Header format
        // Vendredi
        public const short F_FULLDAY = 0;
        // Ven
        public const short F_ABREVDAY = 1;
        // V
        public const short F_INITDAY = 2;
        // jj/mm/aaaa
        public const short F_DATEDAY = 3;
        // jj
        public const short F_NUMDAY = 4;

        // jj/mm/aaaa (jj number of first week day)
        public const short F_DATEWEEK = 0;
        // ww (week number)
        public const short F_NUMWEEK = 1;

        // jj/mm/aaaa (jj number of first month day)
        public const short F_DATEMONTH = 0;
        // Decembre
        public const short F_FULLMONTH = 1;
        // Jan-Fev-Mar-Avr-Mai-Juin-Juil-Aou-Sep-Oct-Nov-Dec
        public const short F_ABREVMONTH = 2;
        // 1 to 12
        public const short F_NUMMONTH = 3;

        // Event Type patterns
        public const short P_NUMBER_PATTERN = 43;

        public const short P_NO_PATTERN = 0;

        public const short P_DIAGONAL_1 = 1;
        public const short P_DIAGONAL_2 = 2;
        public const short P_DIAGONAL_3 = 3;
        public const short P_DIAGONAL_4 = 4;
        public const short P_DIAGONAL_5 = 5;
        public const short P_DIAGONAL_6 = 6;

        public const short P_DIAGONAL_DASH_1 = 7;
        public const short P_DIAGONAL_DASH_2 = 8;
        public const short P_DIAGONAL_DASH_3 = 9;
        public const short P_DIAGONAL_DASH_4 = 10;
        public const short P_DIAGONAL_DASH_5 = 11;
        public const short P_DIAGONAL_DASH_6 = 12;

        public const short P_DIAGONAL_DASH_POINT_1 = 13;
        public const short P_DIAGONAL_DASH_POINT_2 = 14;
        public const short P_DIAGONAL_DASH_POINT_3 = 15;
        public const short P_DIAGONAL_DASH_POINT_4 = 16;
        public const short P_DIAGONAL_DASH_POINT_5 = 17;
        public const short P_DIAGONAL_DASH_POINT_6 = 180;

        public const short P_CROSS_1 = 19;
        public const short P_CROSS_2 = 20;
        public const short P_CROSS_3 = 21;
        public const short P_CROSS_4 = 22;
        public const short P_CROSS_5 = 23;
        public const short P_CROSS_6 = 24;

        public const short P_CIRCLE_1 = 25;
        public const short P_CIRCLE_2 = 26;
        public const short P_CIRCLE_3 = 27;

        public const short P_TRIANGLE_1 = 28;
        public const short P_TRIANGLE_2 = 29;

        public const short P_BUTTON_1 = 30;

        public const short P_SCALE_1 = 31;
        public const short P_SCALE_2 = 32;

        public const short P_COLISEE_1 = 33;

        public const short P_MESH_1 = 34;
        public const short P_MESH_2 = 35;

        public const short P_WAFFLE_1 = 36;
        public const short P_WAFFLE_2 = 37;

        public const short P_FLOOR_1 = 38;
        public const short P_FLOOR_2 = 39;

        public const short P_MILL_1 = 40;

        public const short P_OTHER_1 = 41;
        public const short P_OTHER_2 = 42;
        public const short P_OTHER_3 = 43;

        public const short one = 1;
        public const short two = 2;
    }
}
