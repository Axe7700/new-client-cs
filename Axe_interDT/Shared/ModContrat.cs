﻿using System;
using System.Data;

namespace Axe_interDT.Shared
{
    class ModContrat
    {

        public static string fc_RechercheContrat(string sCodeimmeuble, ref bool bWithContrat)
        {
            try
            {
                string sSQL = "";
                string sLibContrat = "";
                DataTable rstmp = new DataTable();
                ModAdo rstmpAdo = new ModAdo();

                sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " +
                       " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                sSQL = sSQL + " WHERE Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeuble) + "' AND Avenant=0";
                sSQL = sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";

                rstmp = rstmpAdo.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count > 0)//tested
                {
                    sLibContrat = rstmp.Rows[0]["Designation1"] + "";
                }
                rstmp.Dispose();
                //'===Si il existe un contrat, controle si non résilié.
                if (!string.IsNullOrEmpty(sLibContrat))
                {
                    sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 " +
                        " FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                        " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" + DateTime.Now + "' OR Contrat.DateFin IS NULL))) " +
                        " AND Contrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeuble) + "' order by avenant";
                    rstmp = rstmpAdo.fc_OpenRecordSet(sSQL);
                    if (rstmp.Rows.Count > 0)
                    {
                        sLibContrat = rstmp.Rows[0]["Designation1"] + "";

                        bWithContrat = true;
                    }
                    else
                    {
                        bWithContrat = false;
                    }
                    rstmp.Dispose();
                }
                return sLibContrat;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_RechercheContrat");
                return "";
            }
        }

        /// <summary>
        /// Mondir le 02.04.2021 https://groupe-dt.mantishub.io/view.php?id=2306
        /// i copied this fonction from fiche Intervention
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <returns></returns>
        public static DataTable fc_GetActiveContract(string sCodeImmeuble)
        {
            try
            {
                //===> Mondir le 11.10.2021, added FacArticle.COT_Code
                General.sSQL = "SELECT FacArticle.COT_Code, Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                       " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" + DateTime.Today.ToString(General.FormatDateSQL) + "')))" +
                       " AND Contrat.CodeImmeuble='" + sCodeImmeuble + "' order by avenant";

                var modAdorstmp = new ModAdo();
                var rstmp = modAdorstmp.fc_OpenRecordSet(General.sSQL);
                return rstmp;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
            return null;
        }

        /// <summary>
        /// Mondir le 31.05.2021, https://groupe-dt.mantishub.io/view.php?id=2445
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <returns></returns>
        public static int AllContractsCount(string sCodeImmeuble)
        {
            try
            {
                var query = $"SELECT COUNT(*) FROM Contrat WHERE CodeImmeuble = '{sCodeImmeuble}'";
                using (var tmpModAdo = new ModAdo())
                {
                    return tmpModAdo.fc_ADOlibelle(query).ToInt();
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return -1;
        }
    }
}
