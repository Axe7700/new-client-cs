﻿using Axe_interDT.Views.Theme.CustomMessageBox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class modAvoir
    {
        public static string fc_AJoutAvoir(String sNofacture)
        {
            //===> Mondir 08.09.2020, Ajout du LOG
            Program.SaveException(null, $"modAvoir.fc_AJoutAvoir() - Enter In The Function");
            Program.SaveException(null, $"modAvoir.fc_AJoutAvoir() - sNofacture = {sNofacture}");
            //===> Fin Modif Mondir

            string sSQL;
            ModAdo modAdorsAdd = new ModAdo();
            DataTable rsAdd;
            ModAdo modAdorsFAC = new ModAdo();
            DataTable rsFAC;
            int lCleNew = 0;
            int lCleOld = 0;
            DateTime dtDate;
            bool bFirst = false;
            string returnFuction = "";
            int lNoInter = 0;
            try
            {
                sSQL = "SELECT  * " + " From FactureManuelleEnTete " + " WHERE  NoFacture = '" + sNofacture + "'";
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);
                if (rsFAC.Rows.Count == 0)
                {
                    rsFAC = null;
                    rsAdd = null;
                    modAdorsFAC.Dispose();
                    modAdorsAdd.Dispose();
                    return returnFuction;
                }
                //Modif 14-07-2020 ajouté par salma
                sSQL = "SELECT NoIntervention from intervention where NoFacture ='" + StdSQLchaine.gFr_DoublerQuote(sNofacture) + "'";
                using (var tmpADO = new ModAdo())
                    lNoInter = Convert.ToInt32(General.nz(tmpADO.fc_ADOlibelle(sSQL), 0));

                lNoInter = fc_RechercheNoInterPourFacture(lNoInter);

                if (lNoInter == 0)
                    return "";
                //fin modif 14-07-2020

                // '==== avoir de la facture en cours.
                sSQL = "SELECT     CleFactureManuelle, Affaire, CodeUO, CommentaireFacture1, CommentaireFacture2, Client, ClientAdresse1, ClientAdresse2, ClientAdresse3, ClientCP, "
                      + " ClientVille, DateCouvertureDebut, DateCouvertureFin, NomIntervention, AdrIntervention1, AdrIntervention2, AdrIntervention3, CpIntervention,"
                      + " VilleIntervention, PaysIntervention, Site, Utilisateur, Devise, TauxDevise, Objet, TypeFacture, PayeInterne, TypeVolume, DateTravaux, VolumeM3, "
                      + " VolumeMW, Volume, DateEcheance, NoFacture, NumFacture, Facture, comptabilisee, CodeFicheFacture, Attache, Vref1, Nref1, Nref2, Initiales, "
                      + " CodeAffaire, Objet1, Objet2, ModeReglement, LibelleReglement, NCompte, Imprimer, NomPersonne, CodeTVA, typeReglement, ValiderPar, Verifiee, "
                      + " NoIntervention, VIR_Code, FTM_Compta, INT_AnaCode, ExportPDF, AxeCielTest, Definitive, ServiceAnalytique, DateMajCodeimmeuble, RefClient, "
                      + " DateComptabilisee, DateFacture "
                      + " From FactureManuelleEnTete "
                      + " WHERE CleFactureManuelle = 0 ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                //'===== ENB TETE DE FACTURE.
                sSQL = "SELECT Max(FactureManuelleEntete.CleFactureManuelle) AS MaxDeCleFactureManuelle FROM FactureManuelleEntete";
                var tmp = new ModAdo();
                sSQL = tmp.fc_ADOlibelle(sSQL);
                if (string.IsNullOrEmpty(sSQL))
                {
                    lCleNew = 1;
                }
                else
                {
                    lCleNew = Convert.ToInt32(sSQL) + 1;
                }

                lCleOld = rsFAC.Rows[0]["CleFactureManuelle"] != DBNull.Value ? Convert.ToInt32(rsFAC.Rows[0]["CleFactureManuelle"]) : 0;

                var newRow = rsAdd.NewRow();
                newRow["CleFactureManuelle"] = lCleNew;
                newRow["NoFacture"] = "XX" + lCleNew;
                //' voir comment est gérer le NumFacture
                newRow["Nompersonne"] = General.fncUserName();

                //'=== calcul de la date d'echéance.ModeReglement
                dtDate = DateTime.Now;
                newRow["DateFacture"] = dtDate;
                newRow["ModeReglement"] = rsFAC.Rows[0]["ModeReglement"];
                newRow["DateEcheance"] = General.nz(General.CalcEch(dtDate, rsFAC.Rows[0]["ModeReglement"] + ""), DBNull.Value);
                newRow["TypeFacture"] = rsFAC.Rows[0]["TypeFacture"];

                newRow["Affaire"] = rsFAC.Rows[0]["Affaire"];
                newRow["CodeUO"] = rsFAC.Rows[0]["CodeUO"];
                newRow["CommentaireFacture1"] = rsFAC.Rows[0]["CommentaireFacture1"];
                newRow["CommentaireFacture2"] = rsFAC.Rows[0]["CommentaireFacture2"];
                newRow["Client"] = rsFAC.Rows[0]["Client"];
                newRow["ClientAdresse1"] = rsFAC.Rows[0]["ClientAdresse1"];
                newRow["ClientAdresse2"] = rsFAC.Rows[0]["ClientAdresse2"];
                newRow["ClientAdresse3"] = rsFAC.Rows[0]["ClientAdresse3"];
                newRow["ClientCP"] = rsFAC.Rows[0]["ClientCP"];
                newRow["ClientVille"] = rsFAC.Rows[0]["ClientVille"];
                newRow["DateCouvertureDebut"] = rsFAC.Rows[0]["DateCouvertureDebut"];
                newRow["DateCouvertureFin"] = rsFAC.Rows[0]["DateCouvertureFin"];
                newRow["NomIntervention"] = rsFAC.Rows[0]["NomIntervention"];
                newRow["AdrIntervention1"] = rsFAC.Rows[0]["AdrIntervention1"];
                newRow["AdrIntervention2"] = rsFAC.Rows[0]["AdrIntervention2"];
                newRow["AdrIntervention3"] = rsFAC.Rows[0]["AdrIntervention3"];
                newRow["CpIntervention"] = rsFAC.Rows[0]["CpIntervention"];
                newRow["VilleIntervention"] = rsFAC.Rows[0]["VilleIntervention"];
                newRow["PaysIntervention"] = rsFAC.Rows[0]["PaysIntervention"];
                newRow["Site"] = rsFAC.Rows[0]["Site"];
                newRow["Utilisateur"] = General.fncUserName();
                newRow["Devise"] = rsFAC.Rows[0]["Devise"];
                newRow["TauxDevise"] = rsFAC.Rows[0]["TauxDevise"];
                newRow["Objet"] = rsFAC.Rows[0]["Objet"];

                newRow["PayeInterne"] = rsFAC.Rows[0]["PayeInterne"];
                newRow["TypeVolume"] = rsFAC.Rows[0]["TypeVolume"];
                newRow["DateTravaux"] = rsFAC.Rows[0]["DateTravaux"];
                newRow["VolumeM3"] = rsFAC.Rows[0]["VolumeM3"];
                newRow["VolumeMW"] = rsFAC.Rows[0]["VolumeMW"];

                newRow["Volume"] = rsFAC.Rows[0]["Volume"];

                newRow["CodeFicheFacture"] = rsFAC.Rows[0]["CodeFicheFacture"];
                newRow["CodeFicheFacture"] = rsFAC.Rows[0]["CodeFicheFacture"];
                newRow["Attache"] = rsFAC.Rows[0]["Attache"];
                newRow["Vref1"] = rsFAC.Rows[0]["Vref1"];
                newRow["Nref1"] = rsFAC.Rows[0]["Nref1"];
                newRow["Nref2"] = rsFAC.Rows[0]["Nref2"];
                //'=== initiale c est quoi ?
                //'newRow["Initiales"]= rsFAC.Rows[0]["
                newRow["CodeAffaire"] = rsFAC.Rows[0]["CodeAffaire"];
                newRow["Objet1"] = rsFAC.Rows[0]["Objet1"];
                newRow["Objet2"] = rsFAC.Rows[0]["Objet2"];

                newRow["LibelleReglement"] = rsFAC.Rows[0]["LibelleReglement"];
                newRow["Ncompte"] = rsFAC.Rows[0]["Ncompte"];
                //'=== voir c est quoi nompersonne.

                newRow["CodeTVA"] = rsFAC.Rows[0]["CodeTVA"];
                newRow["typeReglement"] = rsFAC.Rows[0]["typeReglement"];
                //newRow["NoIntervention"] = rsFAC.Rows[0]["NoIntervention"];
                //'=== modif du 14/07/2020.
                //Modif 14-07-2020 ajouté par salma
                newRow["NoIntervention"] = lNoInter;
                // fin modif 14-07-2020

                newRow["VIR_Code"] = rsFAC.Rows[0]["VIR_Code"];
                newRow["FTM_Compta"] = rsFAC.Rows[0]["FTM_Compta"];
                newRow["INT_AnaCode"] = rsFAC.Rows[0]["INT_AnaCode"];
                newRow["Definitive"] = rsFAC.Rows[0]["Definitive"];
                newRow["ServiceAnalytique"] = rsFAC.Rows[0]["ServiceAnalytique"];
                newRow["DateMajCodeimmeuble"] = rsFAC.Rows[0]["DateMajCodeimmeuble"];
                newRow["RefClient"] = rsFAC.Rows[0]["RefClient"];

                /// added by mohammed 22/06/2020 .====> this code doesn't exist in vb6 added  by mohammed to solve a diffrence result between vb6 and c# .in vb6 when we don't give a value to these columns(comptabilisee,ExportPDF) they take 0 but in c# they take NULL.
                if (newRow["comptabilisee"] == DBNull.Value)
                    newRow["comptabilisee"] = "0";
                if (newRow["ExportPDF"] == DBNull.Value)
                    newRow["ExportPDF"] = 0;
                rsAdd.Rows.Add(newRow.ItemArray);
                modAdorsAdd.Update();

                //returnFuction = "XX" + lCleNew;
                //Modif 17-07-2020 ajouté par salma
                returnFuction = "XX" + lCleNew + ";" + lNoInter;
                //Fin modif 17-07-2020

                //Modif 14-07-2020 ajouté par salma
                sSQL = "UPDATE  INTERVENTION set nofacture='" + "XX" + lCleNew + "' WHERE nointervention =" + lNoInter;
                General.Execute(sSQL);
                // fin modif 14-07-2020

                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();


                //'=== corps.
                sSQL = "SELECT     CleFactureManuelle, NumeroLigne, CodeCategorie, TarifArticle, Poste, Famille, SSFamille, Article, "
                        + " Designation, Quantite, Unite, PrixUnitaire, "
                        + " MontantLigne , TVA, Compte, Compte7, AnaActivite, TypePrestation, SousPrestation "
                        + " From FactureManuelleDetail "
                        + " WHERE  CleFactureManuelle = 0 ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT  *"
                        + " From FactureManuelleDetail "
                        + " WHERE  CleFactureManuelle = " + lCleOld;
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                //'bFirst = True
                var nRow = rsAdd.NewRow();
                nRow["CleFactureManuelle"] = lCleNew;
                nRow["Designation"] = "Avoir de la facture " + sNofacture;
                nRow["NumeroLigne"] = 1;
                rsAdd.Rows.Add(nRow.ItemArray);
                modAdorsAdd.Update();

                if (rsFAC.Rows.Count > 0)
                {
                    for (int i = 0; i < rsFAC.Rows.Count; i++)
                    {
                        var newR = rsAdd.NewRow();
                        newR["CleFactureManuelle"] = lCleNew;
                        newR["NumeroLigne"] = Convert.ToInt32(General.nz(rsFAC.Rows[i]["NumeroLigne"], 0).ToString()) + 1;
                        newR["CodeCategorie"] = rsFAC.Rows[i]["CodeCategorie"];
                        newR["TarifArticle"] = rsFAC.Rows[i]["TarifArticle"];
                        newR["Poste"] = rsFAC.Rows[i]["Poste"];
                        newR["FAMILLE"] = rsFAC.Rows[i]["FAMILLE"];
                        newR["SSFamille"] = rsFAC.Rows[i]["SSFamille"];
                        newR["article"] = rsFAC.Rows[i]["article"];
                        newR["Designation"] = rsFAC.Rows[i]["Designation"];
                        newR["Quantite"] = rsFAC.Rows[i]["Quantite"];
                        newR["Unite"] = rsFAC.Rows[i]["Unite"];
                        if (General.IsNumeric(rsFAC.Rows[i]["Prixunitaire"].ToString()))
                        {
                            newR["Prixunitaire"] = -Convert.ToDouble(rsFAC.Rows[i]["Prixunitaire"]) * (1);
                        }
                        if (General.IsNumeric(rsFAC.Rows[i]["MontantLigne"].ToString()))
                        {
                            newR["MontantLigne"] = -Convert.ToDouble(rsFAC.Rows[i]["MontantLigne"]) * (1);
                        }
                        newR["TVA"] = rsFAC.Rows[i]["TVA"];
                        newR["compte"] = rsFAC.Rows[i]["compte"];
                        newR["compte7"] = rsFAC.Rows[i]["compte7"];
                        newR["AnaActivite"] = rsFAC.Rows[i]["AnaActivite"];
                        newR["TypePrestation"] = rsFAC.Rows[i]["TypePrestation"];
                        newR["SousPrestation"] = rsFAC.Rows[i]["SousPrestation"];

                        rsAdd.Rows.Add(newR.ItemArray);
                        modAdorsAdd.Update();
                    }

                }

                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();

                sSQL = "SELECT     CleFactureManuelle, Poste, Designation, montantHT, TVA, montantTVA, montantTTC, Compte7, AnaActivite"
                       + " FROM         FactureManuellePiedBis "
                       + "  WHERE     CleFactureManuelle = 0  ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT  *"
                        + " From FactureManuellePiedBis "
                        + " WHERE  CleFactureManuelle = " + lCleOld;
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                if (rsFAC.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsFAC.Rows)
                    {
                        var Row = rsAdd.NewRow();
                        Row["CleFactureManuelle"] = lCleNew;
                        Row["Poste"] = dr["Poste"];
                        Row["Designation"] = dr["Designation"];
                        if (General.IsNumeric(dr["MontantHT"].ToString()))
                        {
                            Row["MontantHT"] = -Convert.ToDouble(dr["MontantHT"]) * (1);
                        }
                        Row["TVA"] = dr["TVA"];
                        if (General.IsNumeric(dr["MontantTVA"].ToString()))
                        {
                            Row["MontantTVA"] = -Convert.ToDouble(dr["MontantTVA"]) * (1);
                        }
                        if (General.IsNumeric(dr["MontantTTc"].ToString()))
                        {
                            Row["MontantTTc"] = -Convert.ToDouble(dr["MontantTTc"]) * (1);
                        }
                        Row["compte7"] = dr["compte7"];
                        Row["AnaActivite"] = dr["AnaActivite"];

                        rsAdd.Rows.Add(Row.ItemArray);
                        modAdorsAdd.Update();
                    }
                }
                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();

                sSQL = "SELECT     CleFactureManuelle, Prestation, SousPrestation, Compte, Pourcentage, MontantHT, CodeTva, TVA, MontantTVA, MontantTTC, UO_Anal, Affaire_Anal, "
                   + " Ordre_Anal, STMontantHT1, TVA1, STMontantTVA1, STMontantTTC1, STMontantHT2, TVA2, STMontantTVA2, STMontantTTC2, STMontantHT3, TVA3, "
                   + " STMontantTVA3, STMontantTTC3, STMontantHT4, TVA4, STMontantTVA4, STMontantTTC4, STMontantHT5, TVA5, STMontantTVA5, STMontantTTC5, "
                   + " AppelFond "
                   + " From FactureManuellePied "
                   + " WHERE    CleFactureManuelle = 0";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT  *"
                        + " From FactureManuellePied "
                        + " WHERE  CleFactureManuelle = " + lCleOld;
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                if (rsFAC.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsFAC.Rows)
                    {
                        var nr = rsAdd.NewRow();
                        nr["CleFactureManuelle"] = lCleNew;
                        nr["Prestation"] = dr["Prestation"];
                        nr["SousPrestation"] = dr["SousPrestation"];
                        nr["compte"] = dr["compte"];
                        nr["Pourcentage"] = dr["Pourcentage"];
                        if (General.IsNumeric(dr["MontantHT"].ToString()))
                        {
                            nr["MontantHT"] = dr["MontantHT"];
                        }
                        nr["CodeTVA"] = dr["CodeTVA"];
                        nr["TVA"] = dr["TVA"];
                        if (General.IsNumeric(dr["MontantTVA"].ToString()))
                        {
                            nr["MontantTVA"] = dr["MontantTVA"];
                        }
                        if (General.IsNumeric(dr["MontantTTc"].ToString()))
                        {
                            nr["MontantTTc"] = dr["MontantTTc"];
                        }
                        nr["UO_Anal"] = dr["UO_Anal"];
                        nr["Affaire_Anal"] = dr["Affaire_Anal"];
                        nr["Ordre_Anal"] = dr["Ordre_Anal"];
                        if (General.IsNumeric(dr["STMontantHT1"].ToString()))
                        {
                            nr["STMontantHT1"] = dr["STMontantHT1"];
                        }
                        nr["TVA1"] = dr["TVA1"];
                        if (General.IsNumeric(dr["STMontantTVA1"].ToString()))
                        {
                            nr["STMontantTVA1"] = dr["STMontantTVA1"];
                        }
                        if (General.IsNumeric(dr["STMontantTTC1"].ToString()))
                        {
                            nr["STMontantTTC1"] = dr["STMontantTTC1"];
                        }
                        if (General.IsNumeric(dr["STMontantHT2"].ToString()))
                        {
                            nr["STMontantHT2"] = dr["STMontantHT2"];
                        }
                        nr["TVA2"] = dr["TVA2"];
                        if (General.IsNumeric(dr["STMontantTVA2"].ToString()))
                        {
                            nr["STMontantTVA2"] = dr["STMontantTVA2"];
                        }
                        if (General.IsNumeric(dr["STMontantTTC2"].ToString()))
                        {
                            nr["STMontantTTC2"] = dr["STMontantTTC2"];
                        }
                        if (General.IsNumeric(dr["STMontantHT3"].ToString()))
                        {
                            nr["STMontantHT3"] = dr["STMontantHT3"];
                        }
                        nr["TVA3"] = dr["TVA3"];
                        if (General.IsNumeric(dr["STMontantTVA3"].ToString()))
                        {
                            nr["STMontantTVA3"] = dr["STMontantTVA3"];
                        }
                        if (General.IsNumeric(dr["STMontantTTC3"].ToString()))
                        {
                            nr["STMontantTTC3"] = dr["STMontantTTC3"];
                        }
                        if (General.IsNumeric(dr["STMontantHT4"].ToString()))
                        {
                            nr["STMontantHT4"] = dr["STMontantHT4"];
                        }
                        nr["TVA4"] = dr["TVA4"];
                        if (General.IsNumeric(dr["STMontantTVA4"].ToString()))
                        {
                            nr["STMontantTVA4"] = dr["STMontantTVA4"];
                        }
                        if (General.IsNumeric(dr["STMontantTTC4"].ToString()))
                        {
                            nr["STMontantTTC4"] = dr["STMontantTTC4"];
                        }
                        if (General.IsNumeric(dr["STMontantHT5"].ToString()))
                        {
                            nr["STMontantHT5"] = dr["STMontantHT5"];
                        }
                        nr["TVA5"] = dr["TVA5"];
                        if (General.IsNumeric(dr["STMontantTVA5"].ToString()))
                        {
                            nr["STMontantTVA5"] = dr["STMontantTVA5"];
                        }
                        if (General.IsNumeric(dr["STMontantTTC5"].ToString()))
                        {
                            nr["STMontantTTC5"] = dr["STMontantTTC5"];
                        }

                        nr["AppelFond"] = dr["AppelFond"];

                        rsAdd.Rows.Add(nr.ItemArray);
                        modAdorsAdd.Update();
                    }

                }

                //                '
                //'lCleOld = rsFac!CleFactureManuelle
                //'
                //'lCleNew = rsAdd!CleFactureManuelle
                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();
                return returnFuction;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModAvoir;fc_AJoutAvoir");
                return returnFuction;
            }
        }


        public static string fc_AJoutAvoirContrat(String sNofacture)
        {
            //            '=== Question quel type de facture TM,CM
            //'=== Compte de classe 7
            //'=== Reprendre juste le total HT ou chaque prestation ?
            //'=== Avoir partiel.
            //'=== Quel service analytique.
            //'=== date d'echéance.
            //'=== Voir comment est géré le num facture.
            //'=== pour les montants partiel(sur HT ou sur TTC)
            //'=== quelle est l'analytique activité.
            string sSQL;
            string sMode;
            ModAdo modAdorsAdd = new ModAdo();
            DataTable rsAdd;
            ModAdo modAdorsFAC = new ModAdo();
            DataTable rsFAC;
            int lCleNew = 0;
            int lCleOld = 0;
            DateTime dtDate;
            bool bFirst = false;

            double dbHT = 0;
            double dbTTC = 0;
            double dbTVA = 0;
            double dbMtTVA = 0;
            string returnFuction = "";

            try
            {
                sSQL = "SELECT     FactEnTete.NoFacture, FactEnTete.CodeImmeuble, FactEnTete.CommentaireFacture1, FactEnTete.CommentaireFacture2, "
                    + " FactEnTete.FactNomImmeuble, FactEnTete.FactAdresse, FactEnTete.FactAdresse2, FactEnTete.FactCP, FactEnTete.FactVille, "
                    + " FactEnTete.IntervAdresse, "
                    + " FactEnTete.IntervCP, FactEnTete.IntervVille, FactEnTete.NCompte, FactEnTete.CodeUO, Table1.CodeReglement, Immeuble.CodeReglementIMM,"
                    + " Immeuble.RefClient, Immeuble.VIRCode, Immeuble.CodeTVA "
                    + " FROM         FactEnTete INNER JOIN "
                    + " Immeuble ON FactEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                    + " Table1 ON Immeuble.Code1 = Table1.Code1 "
                    + " WHERE     FactEnTete.NoFacture = '" + StdSQLchaine.gFr_DoublerQuote(sNofacture) + "'";

                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);
                if (rsFAC.Rows.Count == 0)
                {
                    rsFAC = null;
                    rsAdd = null;
                    modAdorsFAC.Dispose();
                    modAdorsAdd.Dispose();
                    return returnFuction;
                }

                // '==== avoir de la facture en cours.
                sSQL = "SELECT     CleFactureManuelle, Affaire, CodeUO, CommentaireFacture1, CommentaireFacture2, Client, ClientAdresse1, ClientAdresse2, ClientAdresse3, ClientCP, "
                      + " ClientVille, DateCouvertureDebut, DateCouvertureFin, NomIntervention, AdrIntervention1, AdrIntervention2, AdrIntervention3, CpIntervention,"
                      + " VilleIntervention, PaysIntervention, Site, Utilisateur, Devise, TauxDevise, Objet, TypeFacture, PayeInterne, TypeVolume, DateTravaux, VolumeM3, "
                      + " VolumeMW, Volume, DateEcheance, NoFacture, NumFacture, Facture, comptabilisee, CodeFicheFacture, Attache, Vref1, Nref1, Nref2, Initiales, "
                      + " CodeAffaire, Objet1, Objet2, ModeReglement, LibelleReglement, NCompte, Imprimer, NomPersonne, CodeTVA, typeReglement, ValiderPar, Verifiee, "
                      + " NoIntervention, VIRCode, FTMCompta, INTAnaCode, ExportPDF, AxeCielTest, Definitive, ServiceAnalytique, DateMajCodeimmeuble, RefClient, "
                      + " DateComptabilisee, DateFacture "
                      + " From FactureManuelleEnTete "
                      + " WHERE CleFactureManuelle = 0 ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                //'===== ENB TETE DE FACTURE.
                sSQL = "SELECT Max(FactureManuelleEntete.CleFactureManuelle) AS MaxDeCleFactureManuelle FROM FactureManuelleEntete";
                var tmp = new ModAdo();
                sSQL = tmp.fc_ADOlibelle(sSQL);
                if (string.IsNullOrEmpty(sSQL))
                {
                    lCleNew = 1;
                }
                else
                {
                    lCleNew = Convert.ToInt32(sSQL) + 1;
                }

                lCleOld = rsFAC.Rows[0]["CleFactureManuelle"] != DBNull.Value ? Convert.ToInt32(rsFAC.Rows[0]["CleFactureManuelle"]) : 0;

                var newRow = rsAdd.NewRow();
                newRow["CleFactureManuelle"] = lCleNew;
                newRow["NoFacture"] = "XX" + lCleNew;
                //' voir comment est gérer le NumFacture
                newRow["Nompersonne"] = General.fncUserName();

                //'=== calcul de la date d'echéance.ModeReglement
                dtDate = DateTime.Now;
                newRow["DateFacture"] = dtDate;
                newRow["Affaire"] = rsFAC.Rows[0]["CodeUO"];
                newRow["CommentaireFacture1"] = rsFAC.Rows[0]["CommentaireFacture1"];
                newRow["CommentaireFacture2"] = rsFAC.Rows[0]["CommentaireFacture2"];
                //'=== doute sur ce champs, a voir à la fin.
                newRow["Client"] = rsFAC.Rows[0]["Client"];
                newRow["ClientAdresse1"] = rsFAC.Rows[0]["ClientAdresse1"];
                newRow["ClientAdresse2"] = rsFAC.Rows[0]["ClientAdresse2"];
                newRow["ClientCP"] = rsFAC.Rows[0]["ClientCP"];
                newRow["ClientVille"] = rsFAC.Rows[0]["ClientVille"];
                newRow["NomIntervention"] = rsFAC.Rows[0]["NomIntervention"];
                newRow["AdrIntervention1"] = rsFAC.Rows[0]["AdrIntervention1"];
                //=== a voir plus tard. pouquoi ca n'est pas alimenté.
                //newRow["AdrIntervention2"] = rsFAC.Rows[0]["AdrIntervention2"];
                //newRow["AdrIntervention3"] = rsFAC.Rows[0]["AdrIntervention3"];
                newRow["CpIntervention"] = rsFAC.Rows[0]["CpIntervention"];
                newRow["VilleIntervention"] = rsFAC.Rows[0]["VilleIntervention"];
                newRow["Utilisateur"] = General.fncUserName();
                newRow["TypeFacture"] = "CM";

                //'=== code TAV a voir plus tard.
                newRow["CodeTVA"] = rsFAC.Rows[0]["CodeTVA"];
                //'===  a voir plus tard
                newRow["ServiceAnalytique"] = rsFAC.Rows[0]["ServiceAnalytique"];
                newRow["Nompersonne"] = rsFAC.Rows[0]["Nompersonne"];
                newRow["Ncompte"] = rsFAC.Rows[0]["Ncompte"];
                sMode = rsFAC.Rows[0]["CodeReglement"].ToString();
                newRow["VIR_Code"] = rsFAC.Rows[0]["VIR_Code"];

                newRow["ModeReglement"] = sMode;
                newRow["DateEcheance "] = newRow["DateEcheance"] = General.nz(General.CalcEch(dtDate, sMode), DBNull.Value);
                newRow["LibelleReglement"] = tmp.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement  where Code='" + sMode + "'");
                newRow["TypeReglement"] = tmp.fc_ADOlibelle("SELECT CodeReglement.ModeReglt  FROM CodeReglement  where Code='" + sMode + "'");
                newRow["FTM_Compta"] = 0;
                newRow["Definitive"] = 0;
                newRow["RefClient"] = rsFAC.Rows[0]["RefClient"];
                // 'rsAdd!DateCouvertureDebut = rsFAC!DateCouvertureDebut
                //'rsAdd!DateCouvertureFin = rsFAC!DateCouvertureFin
                //'rsAdd!PaysIntervention = rsFAC!PaysIntervention
                //'rsAdd!Site = rsFAC!Site
                //'rsAdd!Devise = rsFAC!Devise
                //'rsAdd!TauxDevise = rsFAC!TauxDevise
                //'rsAdd!Objet = rsFAC!Objet
                //'rsAdd!PayeInterne = rsFAC!PayeInterne
                //'rsAdd!TypeVolume = rsFAC!TypeVolume
                //'rsAdd!DateTravaux = rsFAC!DateTravaux
                //'rsAdd!VolumeM3 = rsFAC!VolumeM3
                //'rsAdd!VolumeMW = rsFAC!VolumeMW
                //'rsAdd!Volume = rsFAC!Volume
                //'rsAdd!CodeFicheFacture = rsFAC!CodeFicheFacture
                //'rsAdd!Attache = rsFAC!Attache
                //'rsAdd!Vref1 = rsFAC!Vref1
                //'rsAdd!Nref1 = rsFAC!Nref1
                //'rsAdd!Nref2 = rsFAC!Nref2
                //'=== initiale c est quoi ?
                //'rsAdd!Initiales = rsFac!
                //'rsAdd!CodeAffaire = rsFAC!CodeAffaire
                //'rsAdd!Objet1 = rsFAC!Objet1
                //'rsAdd!Objet2 = rsFAC!Objet2
                //'rsAdd!INT_AnaCode = rsFAC!INT_AnaCode
                //'rsAdd!DateMajCodeimmeuble = rsFAC!DateMajCodeimmeuble
                //'=== initiale c est quoi ?

                rsAdd.Rows.Add(newRow.ItemArray);
                modAdorsAdd.Update();
                returnFuction = "XX" + lCleNew;

                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();


                //'=== corps.
                sSQL = "SELECT     CleFactureManuelle, NumeroLigne, CodeCategorie, TarifArticle, Poste, Famille, SSFamille, Article, "
                        + " Designation, Quantite, Unite, PrixUnitaire, "
                        + " MontantLigne , TVA, Compte, Compte7, AnaActivite, TypePrestation, SousPrestation "
                        + " From FactureManuelleDetail "
                        + " WHERE  CleFactureManuelle = 0 ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT     NoFacture, TotalHT, TauxTVA, MontantTVA, TTC"
                        + " From FacPied  "
                        + "  WHERE     NoFacture ='" + sNofacture + "'";
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                if (rsFAC.Rows.Count > 0)
                {
                    dbHT = Convert.ToDouble(General.nz(rsFAC.Rows[0]["TotalHT"], 0).ToString());
                    dbTTC = Convert.ToDouble(General.nz(rsFAC.Rows[0]["TTC"], 0).ToString());
                    dbTVA = Convert.ToDouble(General.nz(rsFAC.Rows[0]["TauxTVA"], 0).ToString());
                    dbMtTVA = Convert.ToDouble(General.nz(rsFAC.Rows[0]["MontantTVA"], 0).ToString());
                }
                rsFAC = null;
                modAdorsFAC.Dispose();

                sSQL = "SELECT     NoFacture, compte7, compte, TVA , FCE_Activite"
                        + " From FacCorpsEtat   "
                        + "  WHERE     NoFacture ='" + sNofacture + "'";
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                string sCpt7 = null;
                string sCptTva = null;
                string sAnaly = null;
                if (rsFAC.Rows.Count > 0)
                {
                    sCpt7 = rsFAC.Rows[0]["compte7"].ToString();
                    sCptTva = rsFAC.Rows[0]["compte"].ToString();
                    sAnaly = General.nz(rsFAC.Rows[0]["FCE_Activite"], "P2C").ToString();

                }

                var nRow = rsAdd.NewRow();
                nRow["CleFactureManuelle"] = lCleNew;
                nRow["Designation"] = "Avoir de la facture " + sNofacture;
                nRow["NumeroLigne"] = 1;
                nRow["Quantite"] = 1;
                nRow["Prixunitaire"] = dbHT * -1;
                nRow["MontantLigne"] = dbHT * -1;
                nRow["NumeroLigne"] = dbTVA;
                nRow["compte"] = sCptTva;
                nRow["compte7"] = sCpt7;
                nRow["AnaActivite"] = sAnaly;

                rsAdd.Rows.Add(nRow.ItemArray);
                modAdorsAdd.Update();


                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();

                sSQL = "SELECT     CleFactureManuelle, Poste, Designation, montantHT, TVA, montantTVA, montantTTC, Compte7, AnaActivite"
                       + " FROM         FactureManuellePiedBis "
                       + "  WHERE     CleFactureManuelle = 0  ";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                sSQL = "SELECT  *"
                        + " From FactureManuellePiedBis "
                        + " WHERE  CleFactureManuelle = " + lCleOld;
                rsFAC = new DataTable();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                if (rsFAC.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsFAC.Rows)
                    {
                        var Row = rsAdd.NewRow();
                        Row["CleFactureManuelle"] = lCleNew;
                        //Row["Poste"] = dr["Poste"];
                        //Row["Designation"] = dr["Designation"];
                        //if (General.IsNumeric(dr["MontantHT"].ToString()))
                        //{
                        Row["MontantHT"] = -dbHT * (1);
                        //}
                        Row["TVA"] = dbTVA;
                        //if (General.IsNumeric(dr["MontantTVA"].ToString()))
                        //{
                        Row["MontantTVA"] = -dbMtTVA * (1);
                        //}
                        //if (General.IsNumeric(dr["MontantTTc"].ToString()))
                        //{
                        Row["MontantTTc"] = -dbTTC * (1);
                        //}
                        Row["compte7"] = sCpt7;
                        Row["AnaActivite"] = sAnaly;

                        rsAdd.Rows.Add(Row.ItemArray);
                        modAdorsAdd.Update();
                    }
                }
                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();

                //'==== PIED.
                sSQL = "SELECT     CleFactureManuelle, Prestation, SousPrestation, Compte, Pourcentage, MontantHT, CodeTva, TVA, MontantTVA, MontantTTC, UOAnal, AffaireAnal, "
                   + " OrdreAnal, STMontantHT1, TVA1, STMontantTVA1, STMontantTTC1, STMontantHT2, TVA2, STMontantTVA2, STMontantTTC2, STMontantHT3, TVA3, "
                   + " STMontantTVA3, STMontantTTC3, STMontantHT4, TVA4, STMontantTVA4, STMontantTTC4, STMontantHT5, TVA5, STMontantTVA5, STMontantTTC5, "
                   + " AppelFond "
                   + " From FactureManuellePied "
                   + " WHERE    CleFactureManuelle = 0";
                rsAdd = new DataTable();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);

                //sSQL = "SELECT  *"
                //        + " From FactureManuellePied "
                //        + " WHERE  CleFactureManuelle = " + lCleOld;
                //rsFAC = new DataTable();
                //rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);

                if (rsFAC.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsFAC.Rows)
                    {
                        var nr = rsAdd.NewRow();
                        nr["CleFactureManuelle"] = lCleNew;
                        //nr["Prestation"] = dr["Prestation"];
                        //nr["SousPrestation"] = dr["SousPrestation"];
                        //nr["compte"] = dr["compte"];
                        //nr["Pourcentage"] = dr["Pourcentage"];
                        //if (General.IsNumeric(dr["MontantHT"].ToString()))
                        //{
                        nr["MontantHT"] = dbHT;
                        //}
                        //nr["CodeTVA "] = dr["CodeTVA "];
                        //nr["TVA "] = dr["TVA "];
                        //if (General.IsNumeric(dr["MontantTVA"].ToString()))
                        //{
                        nr["MontantTVA"] = dbMtTVA;
                        //}
                        //if (General.IsNumeric(dr["MontantTTc"].ToString()))
                        //{
                        nr["MontantTTc"] = dbTTC;
                        //}
                        //nr["TVA1"] = dbTVA;
                        //nr["Affaire_Anal"] = dr["Affaire_Anal"];
                        //nr["Ordre_Anal"] = dr["Ordre_Anal"];
                        //if (General.IsNumeric(dr["STMontantHT1"].ToString()))
                        //{
                        nr["STMontantHT1"] = dbHT;
                        //}
                        nr["TVA1"] = dbTVA;
                        //if (General.IsNumeric(dr["STMontantTVA1"].ToString()))
                        //{
                        nr["STMontantTVA1"] = dbMtTVA;
                        //}
                        //if (General.IsNumeric(dr["STMontantTTC1"].ToString()))
                        //{
                        nr["STMontantTTC1"] = dbTTC;
                        //}
                        //if (General.IsNumeric(dr["STMontantHT2"].ToString()))
                        //{
                        //    nr["STMontantHT2"] = dr["STMontantHT2"];
                        //}
                        //nr["TVA2"] = dr["TVA2"];
                        //if (General.IsNumeric(dr["STMontantTVA2"].ToString()))
                        //{
                        //    nr["STMontantTVA2"] = dr["STMontantTVA2"];
                        //}
                        //if (General.IsNumeric(dr["STMontantTTC2"].ToString()))
                        //{
                        //    nr["STMontantTTC2"] = dr["STMontantTTC2"];
                        //}
                        //if (General.IsNumeric(dr["STMontantHT3"].ToString()))
                        //{
                        //    nr["STMontantHT3"] = dr["STMontantHT3"];
                        //}
                        //nr["TVA3"] = dr["TVA3"];
                        //if (General.IsNumeric(dr["STMontantTVA3"].ToString()))
                        //{
                        //    nr["STMontantTVA3"] = dr["STMontantTVA3"];
                        //}
                        //if (General.IsNumeric(dr["STMontantTTC3"].ToString()))
                        //{
                        //    nr["STMontantTTC3"] = dr["STMontantTTC3"];
                        //}
                        //if (General.IsNumeric(dr["STMontantHT4"].ToString()))
                        //{
                        //    nr["STMontantHT4"] = dr["STMontantHT4"];
                        //}
                        //nr["TVA4"] = dr["TVA4"];
                        //if (General.IsNumeric(dr["STMontantTVA4"].ToString()))
                        //{
                        //    nr["STMontantTVA4"] = dr["STMontantTVA4"];
                        //}
                        //if (General.IsNumeric(dr["STMontantHT5"].ToString()))
                        //{
                        //    nr["STMontantHT5"] = dr["STMontantHT5"];
                        //}
                        //nr["TVA5"] = dr["TVA5"];
                        //if (General.IsNumeric(dr["STMontantTVA5"].ToString()))
                        //{
                        //    nr["STMontantTVA5"] = dr["STMontantTVA5"];
                        //}
                        //if (General.IsNumeric(dr["STMontantTTC5"].ToString()))
                        //{
                        //    nr["STMontantTTC5"] = dr["STMontantTTC5"];
                        //}

                        //nr["AppelFond"] = dr["AppelFond"];

                        rsAdd.Rows.Add(nr.ItemArray);
                        modAdorsAdd.Update();
                    }

                }

                //                '
                //'lCleOld = rsFac!CleFactureManuelle
                //'
                //'lCleNew = rsAdd!CleFactureManuelle
                rsFAC = null;
                rsAdd = null;
                modAdorsFAC.Dispose();
                modAdorsAdd.Dispose();
                return returnFuction;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModAvoir;fc_AJoutAvoir");
                return returnFuction;
            }
        }
        public static int fc_RechercheNoInterPourFacture(int lNointervention)
        {
            //===> Mondir 08.09.2020, Ajout du LOG
            Program.SaveException(null, $"modAvoir.fc_RechercheNoInterPourFacture() - Enter In The Function");
            Program.SaveException(null, $"modAvoir.fc_RechercheNoInterPourFacture() - lNointervention = {lNointervention}");
            //===> Fin Modif Mondir

            string sSQL = null;
            string sNoFactureTemp = null;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            bool bFind = false;
            int lCount = 0;
            int i = 0;
            int returnFunction = 0;

            try
            {
                sSQL = "SELECT        NumFicheStandard, NoIntervention, NoFacture"
                        + " From Intervention "
                        + " WHERE        NumFicheStandard IN "
                        + " (SELECT        NumFicheStandard "
                        + " FROM            Intervention AS Intervention_1 "
                        + " WHERE        NoIntervention = " + lNointervention + ") "
                        + " ORDER BY NoFacture";
                rs = rsAdo.fc_OpenRecordSet(sSQL);
                lCount = rs.Rows.Count;
                bFind = false;

                foreach (DataRow r in rs.Rows)
                {
                    if (string.IsNullOrEmpty(General.nz(r["NoFacture"], "").ToString()))
                    {
                        returnFunction = Convert.ToInt32(General.nz(r["nointervention"], 0));
                        bFind = true;
                        rsAdo.Update();
                        break;

                    }
                    else if (lCount == 1 && !string.IsNullOrEmpty(General.nz(r["NoFacture"], "").ToString()))
                    {
                        CustomMessageBox.Show("Vous ne pouvez pas créer d'avoir pour cette facture, aucune intervention n'est disponible," + "\t" + "veuillez d'abord dupliquer l'intervention.", "Avoir annulé", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                        returnFunction = 0;
                        rs.Dispose();
                        rs = null;
                        return returnFunction;
                    }
                    else
                    {
                        //'=== Recherche si une facture est attaché à plusieurs intervention, si c'est la cas on retourne le numéro
                        //'=== de d'intervention pour pouvoir l'attacher à la nouvelle facture.
                        if (sNoFactureTemp != General.nz(r["NoFacture"], "").ToString())
                        {
                            i = 0;
                            sNoFactureTemp = General.nz(r["NoFacture"], "") + "";
                        }
                        else if (sNoFactureTemp == General.nz(r["NoFacture"], "").ToString())
                        {
                            bFind = true;
                            returnFunction = Convert.ToInt32(General.nz(r["nointervention"], 0));
                            rs.Dispose();
                            rs = null;
                            return returnFunction;
                        }
                    }
                }
                rs.Dispose();
                rs = null;
                if (bFind == false)
                {
                    CustomMessageBox.Show("Vous ne pouvez pas créer d'avoir pour cette facture, aucune intervention n'est disponible," + "\t" + "veuillez d'abord dupliquer l'intervention.", "Avoir annulé", MessageBoxButtons.OKCancel, MessageBoxIcon.Information);
                    returnFunction = 0;
                }
                return returnFunction;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModAvoir;fc_RechercheNoInterPourFacture");
                return returnFunction;
            }
        }

    }
}
