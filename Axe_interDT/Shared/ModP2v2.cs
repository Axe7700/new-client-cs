﻿using Axe_interDT.View.Theme;
using iTalk;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    public static class ModP2v2
    {
        //Dim tlJourFeriee

        static DataTable rs;
        static ModAdo modAdors = null;
        public const string cTriAnnuel = "TRI-ANNUEL";
        public const string cTriAnnuel1 = "TRIANNUEL";

        //===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        public const string cIDHebdo = "H";
        public const string cIDMensuel = "M";
        public const string cIDBiMensuel = "BM";
        public const string cIDBiMestriel = "BT";
        public const string cIDTriMestriel = "T";
        public const string cIDSemestriel = "S";
        public const string cIDQuotidien = "Q";
        public const string cIDAnnuel = "A";
        public const string cIDCycleAnnuel = "CA";
        public const string cIDCycleJournee = "CJ";
        public const string cIDCycleMensuel = "CM";

        public struct GoupInterv
        {
            public string sInterv;
            public string sNomInterv;
            public string sCodeChefSecteur;
            public string sNomCodeChefSecteur;
            public double dbDurJanv;
            public double dbDurFev;
            public double dbDurMars;
            public double dbDurAvril;
            public double dbDurMai;
            public double dbDurJuin;
            public double dbDurJuillet;
            public double dbDurAout;
            public double dbDurSept;
            public double dbDurOct;
            public double dbDurNov;
            public double dbDurDec;
        }

        public struct GoupImmeuble
        {
            public string sCodeImmeuble;
            public double dbDurJanv;
            public double dbDurFev;
            public double dbDurMars;
            public double dbDurAvril;
            public double dbDurMai;
            public double dbDurJuin;
            public double dbDurJuillet;
            public double dbDurAout;
            public double dbDurSept;
            public double dbDurOct;
            public double dbDurNov;
            public double dbDurDec;
        }

        public struct GoupChefSecteur
        {
            public string sCodeChefSecteur;
            public string sNomChefSecteur;
            public double dbDurJanv;
            public double dbDurFev;
            public double dbDurMars;
            public double dbDurAvril;
            public double dbDurMai;
            public double dbDurJuin;
            public double dbDurJuillet;
            public double dbDurAout;
            public double dbDurSept;
            public double dbDurOct;
            public double dbDurNov;
            public double dbDurDec;
        }

        public struct ParamGMAOx
        {
            public int LidPEI_NoAuto;
            //===> Mondir le 17.02.2021 pour ajouter les modfis de la version V16.02.2021
            public string sLocalisation { get; set; }
            //===> Fin Modif Mondir
            public double dbDuree;
            public DateTime dtDebut;
            public DateTime dtFin;
            public string sInterv;
            public int lIntervAutre;
            public string sPrestation;
            public string sPeriodicite;
            public int lMois;
            public int ljour;
            public DateTime dtDate;
            public int lEQM_NoAuto;
            public string sCodeImmeuble;
            public string sCodeChefSecteur;
            //sJanvier As String
            //sFevrier As String
            //sMars As String
            //sAvril As String
            //sMai As String
            //sJuin As String
            //sJuillet As String
            //sSeptembre As String
            //sOctobre As String
            //sNovembre As String
            //sDecembre As String
        }
        //===> Fin Modif Mondir

        //===> Mondir le 17.02.2021 pour ajouter les modfis de la version V16.02.2021
        public struct ParamGMAOw
        {
            public int LidPEI_NoAuto;
            public string sPrestation;
            public string sComplement;
            public string sLocalisation;
            public string sInterv;
            public int lIntervAutre;
            public string sDoubler;
            public int lnbVisite;
            public string sRepartition;
            public string sCompteur;
            public string sJanvier;
            public string sFevrier;
            public string sMars;
            public string sAvril;
            public string sMai;
            public string sJuin;
            public string sJuillet;
            public string sAout;
            public string sSeptembre;
            public string sOctobre;
            public string sNovembre;
            public string sDecembre;
            public string sNoContrat;
            public string sAvt;
            public string sResilie;
            public string sJour;
            public string sSemaine;
            public string sHeure;
            public string sHorsWe;
            public double dbDuree;
            public DateTime dtDebut;
            public DateTime dtFin;
            public string sPeriodicite;
            public int lMois;
            public int ljour;
            public DateTime dtDate;
            public int lEQM_Noauto;
            public string sCodeimmeuble;
        }
        //===> Fin Modif Mondir

        public static double dtTotDureeP2;

        const string cDesignationP2 = "Visite d'entretien";

        //Variable contenant la durée maximale d'une journée de travail à planifier
        public static string sNbHeuresJ_P2;


        public const string cCodeVisite = "P01";

        public struct Prestation
        {
            public string sCai_Code;
            public string sGAI_LIbelle;
            public string sCodeImmeuble;
            public string sPrest;
            public string sIntervenant;
            public string sMois;
            public string sMatricule;
            public string lAFaireCemois;
        }

        public static Prestation[] TpPrest;

        public static string fc_SimulationCreeCalendrierV2(System.DateTime dtDebPlan, int lCop_NoAuto, System.DateTime dtFinPlan, bool bSimulation, string sIntervenant, string sCodeImmeuble,
             string sCai_Code = "", string sEQM_Code = "", string sSecteur = "", bool bAfficheNePasPlan = false,
     int lCompteurP1 = 0, string sInfoPrestationPDA = "")
        {
            string functionReturnValue = null;

            //=== retourne true si tout s'est bien passée.

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            DataTable rsExist = default(DataTable);
            ModAdo modAdorsExist = new ModAdo();
            DataTable rsT = default(DataTable);
            ModAdo modAdorsT = null;
            DataTable rsAddt = default(DataTable);
            ModAdo modAdorsAddt = null;

            System.DateTime dtDebOp = default(System.DateTime);
            System.DateTime dtFinOp = default(System.DateTime);
            System.DateTime dtDebTemp = default(System.DateTime);
            System.DateTime dtDebTemp2 = default(System.DateTime);
            System.DateTime dtXXX = default(System.DateTime);
            //Dim dtFinTemp       As Date
            System.DateTime dRef = default(System.DateTime);
            System.DateTime dDateAdd = default(System.DateTime);
            //Dim dDateCompte     As Date
            System.DateTime dDateInter = default(System.DateTime);
            System.DateTime DateControle = default(System.DateTime);
            System.DateTime dDebCal = default(System.DateTime);
            System.DateTime dFinCal = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            System.DateTime dtLot = default(System.DateTime);
            System.DateTime dbDebutPlanCtr = default(System.DateTime);
            System.DateTime[] tdPeriode = null;
            System.DateTime[] tdPeriodeAp = null;

            int lJd = 0;
            int lMd = 0;
            int lJf = 0;
            int lMf = 0;
            int lJRef = 0;
            int lMRef = 0;
            int ljour = 0;
            int lJourRef = 0;
            int i = 0;
            int lCIC_NoAuto = 0;
            int lPEI_APlanifier = 0;
            int j = 0;
            int x = 0;
            int o = 0;
            int lPEI_Cycle = 0;
            int lSamedi = 0;
            int lDimanche = 0;
            int lPaques = 0;
            int lPentecote = 0;
            int lJourFeriee = 0;
            int lNoIntervention = 0;


            string sCOP_DateFin = null;
            string sCOP_DateDebut = null;
            //Dim sDateDeb       As String
            //Dim sDateFin       As String
            string sMatricule = null;
            string sRamonneur = null;
            string sSousTraitant = null;
            string sTableOP = null;
            string sMessageErr = null;
            string sCodeChantier = null;
            string staches = null;
            string sWhere = null;
            string sUserName = null;
            string sPlanning = null;

            double dCout = 0;
            double lDuree = 0;

            bool bFinCtr = false;
            bool bDatePlan = false;
            bool bExistRecord = false;
            bool BIn = false;
            bool bDateOk = false;

            object vDuree = null;
            object vHeureFin = null;
            string sTableInt = null;

            bool bSansDateFin = false;

            //short iNoFic = 0;
            string sNomFic = null;
            string sWhere2 = null;
            string sWhere3 = null;
            string sWhereExits = null;
            int iDate = 0;
            bool bInfoPrest = false;


            int lpP2 = 0;
            int lpP2Ind = 0;

            //===> Mondir le 16.04.2021, desable update log https://groupe-dt.mantishub.io/view.php?id=2403
            ModAdo.LogActive = false;
            //===> Fin Modif Mondir

            try
            {

                TpPrest = null;

                if (!string.IsNullOrEmpty(sInfoPrestationPDA))
                {
                    bInfoPrest = true;
                }
                else
                {
                    bInfoPrest = false;
                }

                dtTotDureeP2 = 0;

                sUserName = General.fncUserName();

                //sNomFic = fc_GetFile("C:\", "Fusionne_P2")
                //iNoFic = FreeFile();
                //Open sNomFic For Output As #iNoFic

                //Print #iNoFic, "Critères sélectionnés :"
                //Print #iNoFic, vbTab & "Dates du " & dtDebPlan & " au " & dtFinPlan


                if (lCop_NoAuto != 0)
                {
                    //Print #iNoFic, vbTab & "Fiche GMAO : " & lCop_Noauto
                }
                else
                {
                    //Print #iNoFic, vbTab & "Fiche GMAO : Toutes confondues"
                }

                if (!string.IsNullOrEmpty(sCodeImmeuble))
                {
                    //Print #iNoFic, vbTab & "Site : " & sCodeImmeuble & "/" & sCodeNumOrdre
                }
                else
                {
                    //Print #iNoFic, vbTab & "Site : Tous sites confondus"
                }

                if (!string.IsNullOrEmpty(sIntervenant))
                {
                    //Print #iNoFic, vbTab & "Intervenant : " & sIntervenant
                }
                else
                {
                    //Print #iNoFic, vbTab & "Intervenant : Tous intervenants confondus"
                }

                //Print #iNoFic, ""


                functionReturnValue = "";

                if (dtDebPlan > dtFinPlan)
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de saisie" + "\n" + "La date de début doit être antérieure à la date de fin." + "", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                //=== modif du 17 10 2016, on simule les opérations à réaliser pour afficher
                //=== ces infos sur le PDA.
                if (bInfoPrest == true)
                {
                    sTableOP = "OperationP2info";
                    sPlanning = "PEGD_PeriodeDateGammeInfo";
                }
                else if (bSimulation == true)//tested
                {
                    //Print #iNoFic, "Début Simulation : " & Now
                    sTableInt = "InterventionP2Simul";
                    sTableOP = "OperationP2Simul";
                    staches = "TIP2S_TachesInterP2Simul";
                    sPlanning = "PEGD_PeriodeDateGammeSimul";
                }
                else
                {
                    //Print #iNoFic, "Début Planification : " & Now
                    sTableInt = "Intervention";
                    sTableOP = "OperationP2";
                    staches = "TIP2_TachesInterP2";
                    sPlanning = "PEGD_PeriodeDateGamme";
                }

                if (Convert.ToInt32(General.nz(General.sUseProcStockeeGMAO, 0)) == 0)
                {
                    //Print #iNoFic, "Fonctionnement normal (sans procédure stockée)"

                    //=== modif du 17 10 2016, on simule les opérations à réaliser pour afficher
                    //=== ces infos sur le PDA.
                    if (bInfoPrest == true)
                    {

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                        General.sSQL = "DELETE  FROM " + sTableOP + " WHERE Utilisateur = '" + sUserName + "'";
                        General.Execute(General.sSQL);

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                    }
                    else if (bSimulation == true)//tested
                    {


                        j = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous lancer une simulation ?" + "", "Simulation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                        if (j == (int)DialogResult.Yes)//tested
                        {
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                            General.sSQL = "DELETE FROM " + sTableInt + " WHERE Utilisateur = '" + sUserName + "'";
                            General.Execute(General.sSQL);

                            General.sSQL = "DELETE  FROM " + sTableOP + " WHERE Utilisateur = '" + sUserName + "'";
                            General.Execute(General.sSQL);

                            //sSQL = "DELETE FROM " & staches & " WHERE Utilisateur = '" & sUserName & "'"
                            //adocnn.Execute sSQL

                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                        }
                        else
                        {
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    else
                    {
                        j = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous lancer une planification de visites d'entretiens ?" + "", "Simulation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (j == (int)DialogResult.No)
                        {
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                        else
                        {
                            //TIP2_TachesInterP2
                            //Nettoyage de la base : suppression des opérations non planifiées (non attachées à une intervention)
                            //adocnn.Execute "DELETE FROM TIP2_TachesInterP2 WHERE NoInterventionOP IN (SELECT NoIntervention FROM OperationP2 WHERE NoInterventionVisite IS NULL)"
                            //adocnn.Execute "DELETE FROM OperationP2 WHERE NoInterventionVisite IS NULL"

                        }

                    }
                }
                else
                {
                    //Print #iNoFic, "Fonctionnement avec procédure stockée."
                }
                //End If


                //=== stocke les jours fériées.+=============>Tested
                modP2.fc_ChargeJourFeriee();

                if (Convert.ToInt32(General.nz(General.sUseProcStockeeGMAO, 0)) == 0)
                {
                    if (!string.IsNullOrEmpty(sIntervenant))//tested
                    {
                        General.sSQL = "SELECT DISTINCT ";
                    }
                    else//tested
                    {
                        General.sSQL = "SELECT ";
                    }

                    General.sSQL = General.sSQL + "  Cop_ContratP2.CodeImmeuble, " + " " + " COP_ContratP2.COP_NoContrat, " + " COP_ContratP2.COP_DateSignature,  " + " " + " COP_ContratP2.COP_SStraitant, " +
                        " COP_ContratP2.COP_NoAuto, " + " PEI_PeriodeGammeImm.EQM_NoAuto," + " PEI_PeriodeGammeImm.PEI_NoAuto, " + " PEI_PeriodeGammeImm.TPP_Code, " + " PEI_PeriodeGammeImm.SEM_Code, " +
                        " PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel ," + " PEI_PeriodeGammeImm.PEI_Visite, PEI_PeriodeGammeImm.PEI_Intervenant, " + " PEI_PeriodeGammeImm.PEI_Tech, PEI_PeriodeGammeImm.PEI_Cout," +
                        " PEI_PeriodeGammeImm.PEI_ForceVisite," + " PEI_IntAutre, PEI_APlanifier, " + " PEI_AvisPassage, PEI_AvisNbJour, PEI_Heure , PEI_ForceJournee,PEI_IntAutre, " +
                        " PEI_PeriodeGammeImm.PEI_Cycle,PEI_PeriodeGammeImm.PEI_Samedi, PEI_PeriodeGammeImm.PEI_Dimanche , " + " PEI_PeriodeGammeImm.PEI_Pentecote , PEI_PeriodeGammeImm.PEI_Jourferiee , PEI_PeriodeGammeImm.PEI_PassageObli, " +
                        " EQM_EquipementP2Imm.EQM_Code," + " EQM_EquipementP2Imm.EQM_Libelle, EQM_EquipementP2Imm.EQM_Observation, EQM_EquipementP2Imm.CompteurObli, " + " EQM_EquipementP2Imm.EQU_P1Compteur," +
                        " EQM_EquipementP2Imm.CTR_ID, EQM_EquipementP2Imm.CTR_Libelle, EQM_EquipementP2Imm.MOY_ID, EQM_EquipementP2Imm.MOY_Libelle, " + " EQM_EquipementP2Imm.ANO_ID, EQM_EquipementP2Imm.ANO_Libelle, EQM_EquipementP2Imm.OPE_ID, " +
                        " EQM_EquipementP2Imm.OPE_Libelle,";

                    //sSQL = sSQL & "  COP_ContratP2.COP_DateFin,"
                    General.sSQL = General.sSQL + "  Contrat.DateFin, ";

                    General.sSQL = General.sSQL + " ICC_IntervCategorieContrat.CAI_Code , ICC_IntervCategorieContrat.CAI_Libelle" + " , " + sPlanning + ".PEDG_DateDebut, " + "  " + sPlanning + ".PEDG_DateFin , " + sPlanning + ".PEDG_User ";

                    if (!string.IsNullOrEmpty(sIntervenant))
                    {
                        // sSQL = sSQL & ",IntervenantImm_INM.intervenant_INM  "
                    }

                    if (General.sPriseEnCompteLocalP2 == "1")
                    {
                        General.sSQL = General.sSQL + ", PDAB_BadgePDA.PDAB_Noauto, PDAB_BadgePDA.PDAB_Libelle, PDAB_BadgePDA.PDAB_IDBarre, "
                            + " GAI_GammeImm.GAi_ID, GAI_GammeImm.GAM_Code , GAI_GammeImm.GAI_LIbelle," +
                            " ICC_IntervCategorieContrat.ICC_Noauto, " + " GAI_GammeImm.GAI_CompteurObli, GAI_GammeImm.GAI_Compteur ";


                        //=== modif du 07 02 2016, ajout de la table Gamme.
                        //sSQL = sSQL & " FROM         GAI_GammeImm INNER JOIN" _
                        //& " " & sPlanning & " INNER JOIN " _
                        //& " PEI_PeriodeGammeImm INNER JOIN " _
                        //& " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN " _
                        //& " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON " _
                        //& " " & sPlanning & ".PEI_NoAuto = PEI_PeriodeGammeImm.PEI_NoAuto ON " _
                        //& " GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID RIGHT OUTER JOIN " _
                        //& " PDAB_BadgePDA INNER JOIN " _
                        //& " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble ON " _
                        //& " GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto AND GAI_GammeImm.COP_Noauto = COP_ContratP2.COP_NoAuto LEFT OUTER JOIN" _
                        //& " Immeuble ON COP_ContratP2.codeimmeuble = Immeuble.CodeImmeuble"

                        if (General.sGMAOcriterePartImmeuble == "1")
                        {
                            General.sSQL = General.sSQL + "  FROM         Gestionnaire RIGHT OUTER JOIN"
                                     + " Immeuble ON Gestionnaire.Code1 = Immeuble.Code1 AND Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire RIGHT OUTER JOIN "
                                     + " Contrat INNER JOIN "
                                     + " PDAB_BadgePDA INNER JOIN "
                                     + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble ON Contrat.NumContrat = COP_ContratP2.NumContratRef AND "
                                     + " Contrat.Avenant = COP_ContratP2.AvenantRef LEFT OUTER JOIN "
                                     + " GAI_GammeImm INNER JOIN "
                                     + " " + sPlanning + " INNER JOIN "
                                     + " PEI_PeriodeGammeImm INNER JOIN "
                                     + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                                     + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                                     + " " + sPlanning + ".PEI_NoAuto = PEI_PeriodeGammeImm.PEI_NoAuto ON GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID ON "
                                     + " PDAB_BadgePDA.PDAB_Noauto = GAI_GammeImm.PDAB_Noauto AND COP_ContratP2.COP_NoAuto = GAI_GammeImm.COP_Noauto ON "
                                    + "   Immeuble.CodeImmeuble = COP_ContratP2.codeimmeuble ";
                        }
                        else
                        {
                            //=== modif du 27 01 2016, ajout de la table contrat, le champs datefin de la table contrat remplace
                            //=== la date de fin de la table COP_CONTRATP2.
                            General.sSQL = General.sSQL + " FROM         Contrat INNER JOIN" + " PDAB_BadgePDA INNER JOIN "
                                + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble ON " +
                                "Contrat.NumContrat = COP_ContratP2.NumContratRef AND " + " Contrat.Avenant = COP_ContratP2.AvenantRef LEFT OUTER JOIN "
                                + " GAI_GammeImm INNER JOIN " + " " + sPlanning +
                                " INNER JOIN " + " PEI_PeriodeGammeImm INNER JOIN "
                                + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN " +
                                " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                                + " " + sPlanning + ".PEI_NoAuto = PEI_PeriodeGammeImm.PEI_NoAuto ON " +
                                " GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID ON PDAB_BadgePDA.PDAB_Noauto = GAI_GammeImm.PDAB_Noauto AND "
                                + " COP_ContratP2.COP_NoAuto = GAI_GammeImm.COP_Noauto LEFT OUTER JOIN " +
                                " Immeuble ON COP_ContratP2.codeimmeuble = Immeuble.CodeImmeuble ";
                        }



                        if (!string.IsNullOrEmpty(sIntervenant))
                        {
                            //sSQL = sSQL & " INNER JOIN IntervenantImm_INM ON Immeuble.CodeImmeuble = IntervenantImm_INM.Codeimmeuble_IMM"
                        }
                    }
                    else
                    {

                        General.sSQL = General.sSQL + " FROM PEI_PeriodeGammeImm " + " INNER JOIN EQM_EquipementP2Imm " + " ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto " +
                            " INNER JOIN ICC_IntervCategorieContrat " + " ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto " + " INNER JOIN" + " COP_ContratP2 " + " ON ICC_IntervCategorieContrat.COP_Noauto " +
                            "= COP_ContratP2.COP_NoAuto ";
                        General.sSQL = General.sSQL + " LEFT OUTER JOIN Immeuble ON ";
                        General.sSQL = General.sSQL + " COP_ContratP2.CodeImmeuble = Immeuble.CodeImmeuble ";
                    }


                    //=== ne pas récupérer les contrats terminés.
                    //sWhere = "  COP_ContratP2.COP_DateFin >='" & dtDebPlan & "'  "
                    //sWhere3 = "  COP_ContratP2.COP_DateFin >='" & dtDebPlan & "'  "

                    if (General.sNewCritereP2V2 == "1")
                    {
                        //sWhere = "  (COP_ContratP2.COP_DateSignature <='" & dtDebPlan & "' OR COP_DateSignature between '" & dtDebPlan & "' and '" & dtFinPlan & "')"
                        //sWhere3 = " (COP_ContratP2.COP_DateSignature <='" & dtDebPlan & "' OR COP_DateSignature between '" & dtDebPlan & "' and '" & dtFinPlan & "')"
                        sWhere = "  COP_ContratP2.COP_DateSignature <='" + dtFinPlan + "'";
                        sWhere3 = " COP_ContratP2.COP_DateSignature <='" + dtFinPlan + "'";
                    }
                    else//tested
                    {
                        sWhere = "  Contrat.DateFin >='" + dtDebPlan + "'  ";
                        sWhere3 = " Contrat.DateFin >='" + dtDebPlan + "'  ";
                    }

                    sWhere2 = "";

                    if (!string.IsNullOrEmpty(sIntervenant))
                    {
                        //sWhere2 = sWhere2 & " AND ((Immeuble.CodeDepanneur = '" & gFr_DoublerQuote(sIntervenant) & "') " _
                        //& " or (PEI_IntAutre  = 1 and PEI_Intervenant = '" & gFr_DoublerQuote(sIntervenant) & "' )) "
                        sWhere2 = sWhere2 + " AND ((Immeuble.CodeDepanneur = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' AND (PEI_IntAutre  = 0 or PEI_IntAutre is null )) " +
                            " or (PEI_IntAutre  = 1 and PEI_Intervenant = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' )) ";

                        if (General.sGMAOcriterePartImmeuble == "1")
                        {

                            sWhere2 = sWhere2 + " AND ((Immeuble.CodeDepanneur = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' AND (PEI_IntAutre  = 0 or PEI_IntAutre is null ) AND (CodeQualif_Qua <>'" + General.cPart + "' OR CodeQualif_Qua is null)) "
                                + " or (PEI_IntAutre  = 1 and PEI_Intervenant = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' ) "
                                + " OR (Immeuble.CodeRamoneur ='" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' and CodeQualif_Qua = '" + General.cPart + "') ) ";
                        }
                        else
                        {
                            sWhere2 = sWhere2 + " AND ((Immeuble.CodeDepanneur = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' AND (PEI_IntAutre  = 0 or PEI_IntAutre is null )) "
                               + " or (PEI_IntAutre  = 1 and PEI_Intervenant = '" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "' )) ";
                        }
                    }

                    if (lCop_NoAuto != 0)//tested
                    {
                        sWhere2 = sWhere2 + " AND COP_ContratP2.COP_NoAuto = " + lCop_NoAuto + "  ";
                    }

                    if (!string.IsNullOrEmpty(sCai_Code))
                    {
                        sWhere2 = sWhere2 + " AND ICC_IntervCategorieContrat.CAI_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCai_Code) + "'";
                    }

                    if (!string.IsNullOrEmpty(sEQM_Code))
                    {
                        sWhere2 = sWhere2 + " AND EQM_EquipementP2Imm.EQM_Code ='" + StdSQLchaine.gFr_DoublerQuote(sEQM_Code) + "'";
                    }

                    if (!string.IsNullOrEmpty(sCodeImmeuble))//tested
                    {
                        sWhere2 = sWhere2 + " AND COP_ContratP2.CodeImmeuble ='" + sCodeImmeuble + "'";
                    }

                    //If lCompteurP1 = 1 Then
                    //        sWhere2 = sWhere2 & " AND ( EQM_EquipementP2Imm.EQU_P1Compteur = 1)"
                    //Else
                    //        sWhere2 = sWhere2 & " AND  (EQM_EquipementP2Imm.EQU_P1Compteur is null OR EQM_EquipementP2Imm.EQU_P1Compteur = 0)"
                    //End If"

                    if (lCompteurP1 == 1)
                    {
                        //=== uniquement avec cpt P1
                        sWhere2 = sWhere2 + " AND ( EQM_EquipementP2Imm.EQU_P1Compteur = 1)";
                    }
                    else if (lCompteurP1 == 2)
                    {
                        //=== exclure avec cpt P1
                        sWhere2 = sWhere2 + " AND  (EQM_EquipementP2Imm.EQU_P1Compteur is null OR EQM_EquipementP2Imm.EQU_P1Compteur = 0)";
                    }
                    else if (lCompteurP1 == 3)
                    {
                        //=== aucun filtre, prends toutes les prestations avec ou sans compteur P1

                    }

                    sWhere = " WHERE (" + sWhere + sWhere2 + ")";

                    //=== modif du 26 10 2017 ajout ud champs resilie.
                    //sWhere3 = " WHERE (" & sWhere3 & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & ")"
                    sWhere3 = " WHERE (" + sWhere3 + sWhere2 + "  AND " + sPlanning + ".PEDG_User = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'" + " AND Contrat.Resiliee = 0)";

                    //==== recupére les contrats A.
                    //sWhere = sWhere & " OR (COP_Statut ='" & contratA & "' and COP_ContratP2.COP_DateFin IS NULL" & sWhere2 & ")"
                    //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratA & "' and COP_ContratP2.COP_DateFin IS NULL" & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & ")"
                    if (General.sNewCritereP2V2 == "1")
                    {
                        sWhere = sWhere + "  AND Contrat.Resiliee = 0";

                        //sWhere = sWhere & " OR (COP_Statut ='" & contratA & "' and Contrat.Resiliee = 0 " & sWhere2 & ")"
                        //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratA & "' " & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" _
                        //& " AND Contrat.Resiliee = 0)"
                        //sWhere = sWhere & " OR (COP_Statut ='" & contratAR & "' and Contrat.Resiliee = 0 " & sWhere2 & ")"

                        //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratAR & "' and and Contrat.Resiliee = 0 " & sWhere2 & "  AND " _
                        //& sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & " AND Contrat.Resiliee = 0 )"
                    }
                    else//tested
                    {

                        sWhere = sWhere + " OR (COP_Statut ='" + modP2.contratA + "' and Contrat.DateFin IS NULL" + sWhere2 + ")";

                        //=== modif du 26 10 2017 ajout du champ résilé.
                        //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratA & "' and Contrat.DateFin IS NULL" & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & ")"
                        sWhere3 = sWhere3 + " OR (COP_Statut ='" + modP2.contratA + "' and Contrat.DateFin IS NULL" + sWhere2 + "  AND " + sPlanning + ".PEDG_User = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) +
                            "'" + " AND Contrat.Resiliee = 0)";

                        //=== modif du 05 02 2019, on ne gére pas les statuts pour le moment


                        //==== recupére les contrats AR.
                        //sWhere = sWhere & " OR (COP_Statut ='" & contratAR & "' and COP_ContratP2.COP_DateFin IS NULL" & sWhere2 & ")"
                        //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratAR & "' and COP_ContratP2.COP_DateFin IS NULL" & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & ")"
                        sWhere = sWhere + " OR (COP_Statut ='" + modP2.contratAR + "' and Contrat.DateFin IS NULL" + sWhere2 + ")";

                        //=== modif du 26 10 2017 ajout du champ résilé.
                        //sWhere3 = sWhere3 & " OR (COP_Statut ='" & contratAR & "' and Contrat.DateFin IS NULL" & sWhere2 & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'" & ")"
                        sWhere3 = sWhere3 + " OR (COP_Statut ='" + modP2.contratAR + "' and Contrat.DateFin IS NULL" + sWhere2 + "  AND " + sPlanning + ".PEDG_User = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) +
                            "'" + " AND Contrat.Resiliee = 0 )";
                    }

                    //===> Mondir le 18.02.2021,https://groupe-dt.mantishub.io/view.php?id=2276#c5482
                    sWhere3 += " AND Cop_ContratP2.COP_Statut = 'A' ";
                    //===> Mondir le 18.02.2021, commented coz accepted
                    //if (General._ExecutionMode == General.ExecutionMode.Test)
                    //{
                    //    sWhere3 += " AND Cop_ContratP2.COP_Statut = 'A' ";
                    //}
                    //===> Fin Modif Mondir

                    //=== modif du 01 07 2013, ajout d'une table temporaire, celle ci servira à calculer les dates de debut et fin d'opération
                    //=== en effet celle ci sont desormais saisie sans l'année ex: jour debut =01 mois debut =10 donne 01/10/2013
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    fc_AttribueDateDate(sWhere, ref sMessageErr, sIntervenant, sPlanning);//tested

                    //sWhere = sWhere & "  AND " & sPlanning & ".PEDG_User = '" & gFr_DoublerQuote(fncUserName) & "'"
                    General.sSQL = General.sSQL + sWhere3 + " order by COP_ContratP2.COP_Noauto";

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                }
                else
                {

                    General.sSQL = "EXEC Write_SimulationCreeCalendrier ";
                    General.sSQL = General.sSQL + " @dtDebPlan = '" + dtDebPlan + "',";
                    General.sSQL = General.sSQL + " @lCop_NoAuto = " + lCop_NoAuto + ",";
                    General.sSQL = General.sSQL + " @dtFinPlan ='" + dtFinPlan + "',";
                    General.sSQL = General.sSQL + " @bSimulation = " + (bSimulation == true ? 1 : 0) + ",";
                    General.sSQL = General.sSQL + " @sIntervenant = '" + sIntervenant + "',";
                    General.sSQL = General.sSQL + " @sCodeImmeuble = '" + sCodeImmeuble + "',";
                    //sSQL = sSQL & " @sCodeNumOrdre = '" & sCodeNumOrdre & "',"
                    //sSQL = sSQL & " @sCodeChaufferie = '" & sCodeChaufferie & "',"
                    //sSQL = sSQL & " @sCodeImmeubleAu = '" & sCodeImmeubleAu & "',"
                    //sSQL = sSQL & " @sCodeNumOrdreAu = '" & sCodeNumOrdreAu & "',"
                    //sSQL = sSQL & " @sCodeChaufferieAu = '" & sCodeChaufferieAu & "',"
                    //sSQL = sSQL & " @sCodeUO = '" & sCodeUO & "',"
                    General.sSQL = General.sSQL + " @Utilisateur ='" + sUserName + "'";

                    SqlCommand SCM = new SqlCommand(General.sSQL, General.adocnn);
                    if (General.adocnn.State != ConnectionState.Open)
                        General.adocnn.Open();

                    DataTable DT = new DataTable();
                    DT.Load(SCM.ExecuteReader());
                    General.rstmp = DT;
                    //Déconnecte le recordset de la base de données
                    //    Set rstmp.ActiveConnection = Nothing
                }
                //Print #iNoFic, "Extraction des visites à planifier : " & Now



                //=== Aucune visite à plannifier.
                if (General.rstmp.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, Il n'y aucune visite à planifier.", "Visites GMAO", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    if ((General.rstmp != null))
                    {
                        General.rstmp = null;
                    }
                    return functionReturnValue;


                }

                //=== stocke la date de début de planification.
                dbDebutPlanCtr = Convert.ToDateTime(General.rstmp.Rows[0]["COP_DateSignature"]);

                //=== prépare le recordset qui enregistrera les gammes planiifiées.
                General.sSQL = "SELECT * FROM " + sTableOP + " WHERE NoIntervention = 0";
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(General.sSQL);

                if (bSimulation == false)
                {
                    //=== recupere les taches pour cette période de plannification pour contrôler si
                    //=== elles ont déjà été planifiées.
                    General.sSQL = "SELECT * FROM OperationP2 ";

                    //=== ne pas récupérer les contrats terminés.
                    General.sSQL = General.sSQL + " WHERE (OperationP2.DatePrevue  >='" + dtDebPlan + "'" + " AND OperationP2.DatePrevue <='" + dtFinPlan + "') ";

                    if (lCop_NoAuto != 0)
                    {
                        General.sSQL = General.sSQL + " AND OperationP2.COP_NoAuto = " + lCop_NoAuto + "  ";
                    }

                    if (!string.IsNullOrEmpty(sCodeImmeuble))
                    {
                        General.sSQL = General.sSQL + " AND OperationP2.CodeImmeuble ='" + sCodeImmeuble + "'";
                    }

                    rsExist = modAdorsExist.fc_OpenRecordSet(General.sSQL);
                    if (rsExist.Rows.Count == 0)
                    {
                        bExistRecord = false;
                    }
                    else
                    {
                        bExistRecord = true;
                    }
                    //=======> Mohammed 09.07.2020 ==> modifs of V09.07.2020
                    rsExist?.Dispose();
                    rsExist = null;
                }

                //=== enregistrement de la date de debut de la période à planifier.
                dRef = dtDebPlan;
                lJRef = dRef.Day;
                lMRef = dRef.Month;
                lJourRef = (int)dRef.DayOfWeek;
                dtLot = DateTime.Now;
                sMessageErr = "";

                //Print #iNoFic, "Parcours des visites à planifier : " & Now

                //=== Boucle sur toutes les prestations.
                lpP2 = 0;
                foreach (DataRow rstmpRow in General.rstmp.Rows)
                {

                    //If UCase(rstmp!CodeImmeuble) = UCase("11COUBERTIN") Then
                    //    Stop
                    //End If


                    Array.Resize(ref TpPrest, lpP2 + 1);
                    BIn = false;
                    for (lpP2Ind = 0; lpP2Ind < TpPrest.Length; lpP2Ind++)
                    {
                        if (TpPrest[lpP2Ind].sCodeImmeuble != null && TpPrest[lpP2Ind].sCai_Code != null && TpPrest[lpP2Ind].sGAI_LIbelle != null)
                        {
                            if (TpPrest[lpP2Ind].sCodeImmeuble.ToUpper() == rstmpRow["CodeImmeuble"].ToString().ToUpper() && TpPrest[lpP2Ind].sCai_Code.ToUpper() ==
                               rstmpRow["CAI_Code"].ToString().ToUpper() && TpPrest[lpP2Ind].sGAI_LIbelle.ToUpper() == rstmpRow["GAI_LIbelle"].ToString().ToUpper())
                            {

                                BIn = true;
                                break;

                            }
                        }

                    }
                    if (BIn == false)
                    {
                        TpPrest[lpP2].sCai_Code = rstmpRow["CAI_Code"] + "";
                        TpPrest[lpP2].sGAI_LIbelle = rstmpRow["GAI_LIbelle"] + "";
                        TpPrest[lpP2].sCodeImmeuble = rstmpRow["CodeImmeuble"] + "";
                        TpPrest[lpP2].sPrest = "Opération: " + rstmpRow["GAI_LIbelle"] + " - " + rstmpRow["CAI_Libelle"] + ModPDA.cRetChario;
                        if (rstmpRow["PEI_IntAutre"].ToString() == "1")
                        {
                            //====> Mondir le 02.07.2020, Old before V02.07.2020
                            //TpPrest[lpP2].sMatricule = rstmpRow["PEI_Intervenant"].ToString();
                            TpPrest[lpP2].sMatricule = General.nz(rstmpRow["PEI_Intervenant"]?.ToString(), modP2.FC_intervenantImm(rstmpRow["CodeImmeuble"]?.ToString()))?.ToString();
                            //====> Fin Modif Mondir
                            using (var tmpModAdo = new ModAdo())
                                TpPrest[lpP2].sIntervenant = tmpModAdo.fc_ADOlibelle("SELECT nom, prenom from personnel WHERE matricule ='" + TpPrest[lpP2].sMatricule + "'", true) + ModPDA.cRetChario;
                        }
                        else
                        {
                            TpPrest[lpP2].sMatricule = modP2.FC_intervenantImm(rstmpRow["CodeImmeuble"].ToString());
                            using (var tmpModAdo = new ModAdo())
                                TpPrest[lpP2].sIntervenant = tmpModAdo.fc_ADOlibelle("SELECT nom, prenom from personnel WHERE matricule ='" + TpPrest[lpP2].sMatricule + "'", true) + ModPDA.cRetChario;
                        }
                        sInfoPrestationPDA = TpPrest[lpP2].sGAI_LIbelle + "@@!" + rstmpRow["CAI_Libelle"] + "@@!" + rstmpRow["CodeImmeuble"] + "";

                        lpP2 = lpP2 + 1;

                    }

                    if (!string.IsNullOrEmpty(General.nz(rstmpRow["COP_SStraitant"], "").ToString()))
                    {
                        using (var tmpModAdo = new ModAdo())
                            sSousTraitant = tmpModAdo.fc_ADOlibelle("SELECT Matricule FROM Personnel WHERE Matricule = '" + General.nz(rstmpRow["COP_SStraitant"], "") + "'");
                    }
                    else
                    {
                        sSousTraitant = "";
                    }

                    //sRamonneur = nz(rstmp!COP_RAM, "")

                    //=== stocke le nom du chantier.
                    sCodeChantier = rstmpRow["CodeImmeuble"].ToString();

                    //=== modif du 17 Octobre, on alimente la variable infoPrestation.
                    //sInfoPrestationPDA =

                    //=== Controle si l'opération doit être planifiée ou pas.
                    lPEI_APlanifier = Convert.ToInt32(General.nz(rstmpRow["PEI_APlanifier"], 0));
                    //=== modif 01 Mars 2015, on affiche uniquement les prestations à ne pas planifier.
                    if (bAfficheNePasPlan == true && lPEI_APlanifier == 0)
                    {
                        goto Boucle;
                    }

                    if (lPEI_APlanifier == 1 && bAfficheNePasPlan == false)
                    {
                        goto Boucle;
                    }

                    //-- j ai osé mettre un goto, j ai mauvaise conscience... :-(, je m'en va manger des moules pour oublier ca ;-)...
                    if (string.IsNullOrEmpty(General.nz(rstmpRow["TPP_Code"], "").ToString()) || General.nz(rstmpRow["TPP_Code"], "").ToString().ToUpper() == "SELON BESOIN")
                    {
                        //==== gerer l'erreur sur la périodicité.
                        goto Boucle;
                        // erreur 001
                    }


                    //=== genere une erreur si il n'exsite pas de date de debut de planification.
                    if (string.IsNullOrEmpty(General.nz(rstmpRow["PEDG_DateDebut"], "").ToString()))
                    {

                        //smessageErr = smessageErr & "Erreur dans la fiche contrat n°" & rstmp!COP_NoContrat _
                        //& " (immeuble " & sCodeChantier & ") ==>" _
                        //& vbCrLf & "l'opération ''" & _
                        //rstmp!CAI_Libelle & " --> " & rstmp!EQM_Code & "'' n'a pas de date de debut,la planification est annulée." & vbCrLf & vbCrLf

                        goto Boucle;
                    }

                    //=== genere une erreur si le cycle n'est pas renseigné en présence de cycle Annuel, Mensuel, Hebdomadaire.
                    if (General.nz(rstmpRow["TPP_Code"], "").ToString().ToUpper() == modP2.cCycleAnnuel.ToUpper() || General.nz(rstmpRow["TPP_Code"], "").ToString().ToUpper() == modP2.cCycleJournee.ToUpper() ||
                        General.nz(rstmpRow["TPP_Code"], "").ToString().ToUpper() == modP2.cCycleMensuel.ToUpper())
                    {
                        if (General.nz(rstmpRow["PEI_Cycle"] + "", "0").ToString() == "0")
                        {

                            sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rstmpRow["COP_NoContrat"] + " (immeuble " + sCodeChantier + ") ==>" + "\n" +
                                " : ''" + rstmpRow["CAI_Libelle"] + " --> " + rstmpRow["EQM_CODE"] + "'' : le cycle n'a pas été renseigné, la planification est annulée." + "\n\n";

                            goto Boucle;
                        }
                    }
                    //cCycleAnnuel

                    //=== genere une erreur si il n'exsite pas de date de fin de planification.
                    if (string.IsNullOrEmpty(General.nz(rstmpRow["PEDG_DateFin"], "").ToString()))
                    {
                        //                sMessageErr = sMessageErr & "Erreur dans la fiche GMAO n°" & rstmp!COP_Noauto _
                        //'                        & " (chantier n°" & sCodeChantier & ") ==>" _
                        //'                        & vbCrLf & "l'opération ''" & _
                        //'                        rstmp!EQM_LIBELLE & "'' n'a pas de date de fin,la planification est annulée." & vbCrLf & vbCrLf
                        //
                        //            GoTo Boucle
                    }

                    lPEI_Cycle = Convert.ToInt32(General.nz(rstmpRow["PEI_Cycle"], 0));
                    lSamedi = Convert.ToInt32(General.nz(rstmpRow["PEI_Samedi"], 0));
                    lDimanche = Convert.ToInt32(General.nz(rstmpRow["PEI_Dimanche"], 0));
                    //lPaques = nz(rstmp!PEI_Paques, 0)
                    lPaques = 1;
                    lPentecote = Convert.ToInt32(General.nz(rstmpRow["PEI_Pentecote"], 0));
                    lJourFeriee = Convert.ToInt32(General.nz(rstmpRow["PEI_Jourferiee"], 0));
                    dtDebOp = Convert.ToDateTime(rstmpRow["PEDG_DateDebut"]);

                    bSansDateFin = false;

                    if (rstmpRow["PEDG_DateFin"] == DBNull.Value)
                    {
                        dtFinOp = dtFinPlan;
                        bSansDateFin = true;
                    }
                    else
                    {
                        dtFinOp = Convert.ToDateTime(rstmpRow["PEDG_DateFin"]);
                    }
                    //dtDebTemp = dtDebOp

                    //=== Stocke la date de fin du contrat.
                    //sCOP_DateFin = nz(rstmp!COP_DateFin, "")
                    sCOP_DateFin = General.nz(rstmpRow["DateFin"], "").ToString();
                    sCOP_DateDebut = General.nz(rstmpRow["COP_DateSignature"], "").ToString();

                    tdPeriodeAp = null;
                    tdPeriodeAp = new DateTime[1] { new DateTime(1753, 1, 1) };
                    o = 0;
                    bFinCtr = false;
                    dtDebTemp = dtDebOp;
                    bDatePlan = false;
                    bDateOk = false;

                    //=== stocke les dates à planifier.
                    while (dtDebTemp <= dtFinPlan)
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        //=== calcul toutes les dates à planifier à partir de la date de début de planification
                        //=== propre à la prestation jusqu'à la date de fin saisie par l'utilisateur
                        //=== ensuite le prog. filtre les dates comprises entre les dates de debut et de fin
                        //=== saisie par par l'utilisateur(envoyé en paramétre).
                        while (dtDebTemp <= dtFinOp)
                        {


                            if ((dtDebTemp >= dtDebPlan && dtDebTemp <= dtFinPlan) || bDateOk == true)
                            {

                                //=== controle si cette date tombe sur un jour férié, un week end ou le jour de la pentecote.

                                dtDebTemp2 = fc_DateAddControleV2(dtDebTemp, lSamedi, lDimanche, lJourFeriee, lPaques, lPentecote, General.nz(rstmpRow["SEM_Code"], "").ToString(), Convert.ToInt32(General.nz(rstmpRow["PEI_ForceJournee"], 0)), rstmpRow["TPP_Code"] + "");

                                //=== si la de fin de contrat est dépassé on ne crée plus de prestation.
                                //=== modif du 20 03 2018, on ne tient plus compte de la date de fin mais
                                //=== du champs rézsilié.
                                //If sCOP_DateFin <> "" Then
                                //    If dtDebTemp2 > CDate(sCOP_DateFin) Then
                                //        bFinCtr = True
                                //        Exit Do
                                //    End If
                                //End If

                                //If sCOP_DateDebut <> "" Then
                                //    If dtDebTemp2 < CDate(sCOP_DateDebut) Then
                                //        bFinCtr = True
                                //        Exit Do
                                //    End If
                                //End If

                                //=== ne pas oublier force visite.

                                iDate = -1;

                                if (tdPeriodeAp[0] == Convert.ToDateTime("01/01/1753"))
                                {
                                    iDate = 0;
                                }
                                else
                                {
                                    for (o = 0; o <= tdPeriodeAp.Length - 1; o++)
                                    {
                                        if (tdPeriodeAp[o] == dtDebTemp2)
                                        {
                                            iDate = o;
                                            break;
                                        }
                                    }
                                }

                                if (!string.IsNullOrEmpty(sCOP_DateDebut))
                                {
                                    if (dtDebTemp2 >= Convert.ToDateTime(sCOP_DateDebut))
                                    {
                                        if (iDate == -1)
                                        {
                                            Array.Resize(ref tdPeriodeAp, tdPeriodeAp.Length + 1);
                                            iDate = tdPeriodeAp.Length - 1;
                                        }


                                        tdPeriodeAp[iDate] = dtDebTemp2.Date;
                                    }
                                }


                                //==== 20 03 2018 attention a remettre en cas d'erreurs constatées.
                                //If iDate = -1 Then
                                //    ReDim Preserve tdPeriodeAp(UBound(tdPeriodeAp) + 1)
                                //    iDate = UBound(tdPeriodeAp)
                                //End If
                                //
                                //tdPeriodeAp(iDate) = dtDebTemp2
                                //==== 20 03 2018 fin attention a remettre en cas d'erreurs constatées.

                                //                o = o + 1
                                bDatePlan = true;
                            }

                            if (dtDebTemp.Month == 2 && dtDebTemp.Year == 2017)
                            {
                                //  Stop
                            }
                            //dtDebTemp = fc_DateAdd(rstmp!TPP_Code, dtDebTemp, lPEI_Cycle, nz(rstmp!SEM_code, ""), nz(rstmp!PEI_ForceJournee, 0))
                            dtDebTemp = fc_DateAdd(rstmpRow["TPP_Code"].ToString(), dtDebTemp, lPEI_Cycle, "", 0);

                            //'If dtDebTemp = #7/1/2018# Then
                            //        '    Stop
                            //'End If


                            dtXXX = fc_DateAddControleV2(dtDebTemp, lSamedi, lDimanche, lJourFeriee, lPaques, lPentecote, General.nz(rstmpRow["SEM_Code"], "").ToString(), Convert.ToInt32(General.nz(rstmpRow["PEI_ForceJournee"], 0)), rstmpRow["TPP_Code"] + "");
                            //=== modif du 25 04 2017.
                            bDateOk = false;
                            if (dtXXX >= dtDebPlan && dtXXX <= dtFinPlan)
                            {
                                bDateOk = true;
                            }

                        }

                        if (bFinCtr == true)
                        {
                            break;
                        }

                        if (bSansDateFin == false)
                        {
                            if (rstmpRow["TPP_Code"].ToString().ToUpper() + "" != modP2.cCycleAnnuel.ToUpper())
                            {
                                dtDebOp = dtDebOp.AddYears(1);
                                dtDebTemp = dtDebOp;
                                dtFinOp = dtFinOp.AddYears(1);
                            }
                            else
                            {
                                dtDebOp = dtDebOp.AddYears(lPEI_Cycle);
                                dtDebTemp = dtDebOp;
                                dtFinOp = dtFinOp.AddYears(lPEI_Cycle);
                            }
                        }

                    }

                    if (bDatePlan == false)
                    {
                        //If BIn = False Then
                        //    sInfoPrestationPDA = sInfoPrestationPDA & "@@!" & "Non" & "@@!"
                        //End If
                        goto Boucle;
                    }

                    for (i = 0; i <= tdPeriodeAp.Length - 1; i++)
                    {

                        //-- si Pei_Journee est à 1 alors on force la journée.
                        //        If rstmp!PEI_ForceVisite = 1 Then
                        //
                        //            DateControle = fc_DateAddControle(DateControle)
                        //        Else
                        //            DateControle = fc_DateAddControle(DateControle)
                        //        End If
                        //        If UCase(rstmp!TPP_Code) = cQuotidien Then
                        //             dDateCompte = DateControle
                        //        End If

                        if (bSimulation == false)
                        {
                            //=== controle si cette opération existe.
                            //If Not (rsExist.EOF And rsExist.BOF) Then
                            //=== modif du 06 01 2014, quand le filtre ne retourne aucun enregistrement, le eof est a true
                            //=== empechant de boucler sur certains enregistrement.
                            if (bExistRecord == true)
                            {

                                //rsExist.Filter = "EQM_CODE ='" & gFr_DoublerQuote(rstmp!EQM_Code & "") & "' and DatePrevue ='" & tdPeriodeAp(i) & "'" _
                                //& " AND COP_Noauto =" & nz(rstmp!COP_NoAuto, 0)

                                //=== modif du 20 04 2016, change le filtre EQM_Code par PEI_NoAuto.

                                //==========> Mohammed 09.06.2020 ==> Modifs of V09.07.2020 -- rachid's comment : modif du 08 07 2020, le filter consomme bcp trop de temps
                                // var rsExistFilter = rsExist.Select("PEI_NoAuto ='" + StdSQLchaine.gFr_DoublerQuote(rstmpRow["PEI_NoAuto"] + "") +
                                //     "' and DatePrevue ='" + tdPeriodeAp[i] + "'" + " AND COP_Noauto =" + General.nz(rstmpRow["COP_NoAuto"], 0));
                                sWhereExits = "SELECT NoIntervention FROM OperationP2 where PEI_NoAuto ='" + StdSQLchaine.gFr_DoublerQuote(rstmpRow["PEI_NoAuto"] + "") + "' and DatePrevue ='" + tdPeriodeAp[i] + "' AND COP_Noauto ='" + General.nz(rstmpRow["COP_NoAuto"], 0) + "'";
                                using (ModAdo ado = new ModAdo())
                                    sWhereExits = ado.fc_ADOlibelle(sWhereExits);
                                if (sWhereExits != "")
                                {
                                    //Print #iNoFic, "NOK : L' opération " & rstmp!EQM_Code & " existe déjà le " & tdPeriodeAp(i) & " pour le contrat " & nz(rstmp!COP_NoContrat, 0) & " : " & Now
                                    //===> Mondir le 15.10.2020, j'ai changer Boucle par Boucle2 pour corriger le ticket #2020, Boule sort de la boucle principale donc toutes les visites de la préstation vont
                                    //===> pas etre créés.
                                    goto Boucle2;
                                    //===> Fin Modif Mondir
                                }
                                else
                                {

                                    ///'''''''''''''''Stop
                                    //Print #iNoFic, "OK : Création de l'opération " & rstmp!EQM_Code & " le " & tdPeriodeAp(i) & " pour le contrat " & nz(rstmp!COP_NoContrat, 0) & " : " & Now
                                }
                            }
                        }
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                        var NewRow = rs.NewRow();

                        NewRow["SEM_Code"] = rstmpRow["SEM_Code"];

                        //=== modif du 12 10 2016, adapte la date n jour est forcé.
                        //                    Dim dtDateSem                   As Date
                        //                    'Dim dtDateSemCOmp               As Date
                        //                    Dim lNoJour     As Long
                        //                    Dim lNoJourComp     As Long
                        //
                        //
                        //                    If nz(rstmp!PEI_ForceJournee, 0) = 1 Then
                        //                            If nz(rs!SEM_Code, "") <> "" Then
                        //                                lNoJour = fc_ReturnDay(rs!SEM_Code)
                        //                                dtDateSem = tdPeriodeAp(i)
                        //
                        //                                If lNoJour <> 0 Then
                        //                                        Do
                        //                                            lNoJourComp = Weekday(dtDateSem, vbSunday)
                        //                                            If lNoJour = lNoJourComp Then
                        //                                                    tdPeriodeAp(i) = dtDateSem
                        //                                                    Exit Do
                        //                                            Else
                        //                                                    dtDateSem = DateAdd("d", 1, dtDateSem)
                        //                                            End If
                        //
                        //                                        Loop
                        //                                        'dtDateSemCOmp = Weekday(tdPeriodeAp(i), vbSunday)
                        //                                End If
                        //                            End If
                        //                    End If

                        var day = CultureInfo.InvariantCulture.Calendar.GetWeekOfYear(tdPeriodeAp[i], CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);

                        NewRow["NoSemaine"] = day;
                        NewRow["CodeImmeuble"] = rstmpRow["CodeImmeuble"];


                        //=== modif du 10/05/2017, si INT_ForceJournee ou PEI_ForceJournee, on affecte la valeur 1
                        //=== au champs PEI_ForceJournee.
                        if (General.nz(rstmpRow["PEI_PassageObli"], 0).ToString() == "1" || General.nz(rstmpRow["PEI_ForceJournee"], 0).ToString() == "1")
                        {
                            //rs!INT_ForceJournee = rstmp!PEI_ForceJournee
                            NewRow["INT_ForceJournee"] = 1;
                        }

                        //=== modif du 8 avril 2015, ajout du champs EQU_P1Compteur (compteur pris en compte dans le P1)
                        //=== qui servira à isoler cette intervention lors de la fusion.

                        //rs!CompteurObli = rstmp!CompteurObli
                        //If nz(rstmp!EQU_P1Compteur, 0) = 1 Then
                        //        '=== on force la saisie compteur dans le cas ou l'utilisateur a oublié de cocher cette case.
                        //        rs!CompteurObli = 1
                        //End If
                        //rs!EQU_P1Compteur = rstmp!EQU_P1Compteur


                        //=== modif du 8 juin 2016, pour la société delostal, la saisie des compteurs n'est pas obligatoire
                        //=== sauf si l'utilisateur le décide ou bien si l'immeuble a un contrat P1 (donc le champs CompteurObli ne fait plus
                        //=== foi, cest le champs EQU_P1Compteur qui rends la saisie obligatoire.
                        //rs!CompteurObli = rstmp!GAI_Compteur
                        if (General.nz(rstmpRow["GAI_CompteurObli"], 0).ToString() == "1")
                        {
                            //=== on force la saisie compteur dans le cas ou l'utilisateur a oublié de cocher cette case.
                            NewRow["CompteurObli"] = 1;
                        }
                        //rs!EQU_P1Compteur = rstmp!GAI_CompteurObli
                        NewRow["EQU_P1Compteur"] = rstmpRow["GAI_Compteur"];

                        // rs!Article =
                        NewRow["ICC_Noauto"] = rstmpRow["ICC_Noauto"];
                        NewRow["CAI_Code"] = rstmpRow["CAI_Code"];
                        NewRow["CAI_Libelle"] = rstmpRow["CAI_Libelle"];
                        NewRow["EQM_CODE"] = rstmpRow["EQM_CODE"];
                        NewRow["EQM_Libelle"] = rstmpRow["EQM_Libelle"];
                        NewRow["CTR_ID"] = rstmpRow["CTR_ID"];
                        NewRow["CTR_Libelle"] = rstmpRow["CTR_Libelle"];
                        NewRow["MOY_ID"] = rstmpRow["MOY_ID"];
                        NewRow["MOY_Libelle"] = rstmpRow["MOY_Libelle"];
                        NewRow["ANO_ID"] = rstmpRow["ANO_ID"];
                        NewRow["ANO_Libelle"] = rstmpRow["ANO_Libelle"];
                        NewRow["OPE_ID"] = rstmpRow["OPE_ID"];
                        NewRow["OPE_Libelle"] = rstmpRow["OPE_Libelle"];

                        if (General.sPriseEnCompteLocalP2 == "1")
                        {
                            NewRow["PDAB_Noauto"] = rstmpRow["PDAB_Noauto"];
                            NewRow["PDAB_Libelle"] = rstmpRow["PDAB_Libelle"];
                            NewRow["PDAB_IDBarre"] = rstmpRow["PDAB_IDBarre"];
                        }



                        //        If UCase(rstmp!PEI_tech & "") = "EXP" Then
                        //
                        //            If rstmp!PEI_IntAutre = 1 Then
                        //                rs!Intervenant = nz(rstmp!PEI_Intervenant, "")
                        //            Else
                        //                rs!Intervenant = FC_intervenantImm(rstmp!Codeimmeuble)
                        //            End If
                        //
                        //        ElseIf UCase(rstmp!PEI_tech & "" = "ST") Then
                        //
                        //            If rstmp!PEI_IntAutre = 1 Then
                        //                rs!Intervenant = nz(rstmp!PEI_Intervenant, "")
                        //            Else
                        //                rs!Intervenant = sSousTraitant
                        //            End If
                        //
                        //        ElseIf UCase(rstmp!PEI_tech & "") = "RAM" Then
                        //
                        //            If rstmp!PEI_IntAutre = 1 Then
                        //                rs!Intervenant = nz(rstmp!PEI_Intervenant, "")
                        //            Else
                        //                rs!Intervenant = sRamonneur
                        //            End If
                        //
                        //        Else

                        //=== si l'intervenant est envoyé en paramétre à cette fonction, la proriété source (requte sql) de l'objet recordset rstmp
                        //=== prend en compte l'intervenant par defaut de l'immeuble (voir ci dessous).
                        if (!string.IsNullOrEmpty(sIntervenant))
                        {
                            //=== modif du 25/11/2016, ajout de  If rstmp!PEI_IntAutre = 1 Then.
                            if (rstmpRow["PEI_IntAutre"].ToString() == "1")
                            {
                                NewRow["Intervenant"] = General.nz(rstmpRow["PEI_Intervenant"], "");
                            }
                            else
                            {
                                NewRow["Intervenant"] = sIntervenant;
                                //nz(rstmp!intervenant_INM, "")
                            }
                        }
                        else
                        {
                            if (rstmpRow["PEI_IntAutre"].ToString() == "1")
                            {
                                NewRow["Intervenant"] = General.nz(rstmpRow["PEI_Intervenant"], "");
                            }
                            else
                            {
                                NewRow["Intervenant"] = modP2.FC_intervenantImm(rstmpRow["CodeImmeuble"].ToString());
                            }
                        }



                        //        End If

                        //=== genere une erreur si l'intervenant n'existe pas.
                        if (string.IsNullOrEmpty(General.nz(NewRow["Intervenant"], "").ToString()))
                        {

                            sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rstmpRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" +
                                rstmpRow["CAI_Libelle"] + " --> " + rstmpRow["EQM_CODE"] + "'' n'est pas associée à un intervenant, la planification est annulée." + "\n\n";
                            //TODO : Mondir - Must Check This
                            //rs.CancelUpdate();
                            goto Boucle;
                        }

                        dtTemp = tdPeriodeAp[i];
                        NewRow["DatePrevue"] = dtTemp;
                        NewRow["INT_J"] = dtTemp.Day;

                        //=== modif du 17 Octobre, on alimente la variable infoPrestation.
                        for (lpP2Ind = 0; lpP2Ind <= TpPrest.Length - 1; lpP2Ind++)
                        {

                            if (TpPrest[lpP2Ind].sCodeImmeuble.ToUpper() == rstmpRow["CodeImmeuble"].ToString().ToUpper() && TpPrest[lpP2Ind].sCai_Code.ToUpper() == rstmpRow["CAI_Code"].ToString().ToUpper() &&
                                TpPrest[lpP2Ind].sGAI_LIbelle.ToUpper() == rstmpRow["GAI_LIbelle"].ToString().ToUpper())
                            {

                                if (string.IsNullOrEmpty(TpPrest[lpP2Ind].sMois))
                                {
                                    TpPrest[lpP2Ind].sMois = "Prévue pour le mois de: " + General.fc_ReturnMonthParam(Convert.ToString(Convert.ToDateTime(NewRow["DatePrevue"]).Month)) + ModPDA.cRetChario;
                                    TpPrest[lpP2Ind].lAFaireCemois = Convert.ToString(1);
                                }
                                break;

                            }
                        }

                        //=== calcul les dates des avis de passage.
                        if (General.nz(rstmpRow["PEI_AvisPassage"], 0).ToString() == "1")
                        {
                            NewRow["INT_DateAvisPass"] = dtTemp.AddDays(-Convert.ToInt32((General.nz(rstmpRow["PEI_AvisNbJour"], 0))));
                        }

                        NewRow["INT_M"] = dtTemp.Month;
                        NewRow["INT_Annee"] = dtTemp.Year;
                        //rs!CYC_NoAuto = lCIC_NoAuto
                        NewRow["TPP_Code"] = rstmpRow["TPP_Code"];

                        //=== génère une erreur si il n'exsite pas de durée.
                        if (string.IsNullOrEmpty(General.nz(rstmpRow["PEI_DureeReel"], "").ToString()))
                        {

                            sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rstmpRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" + rstmpRow["CAI_Libelle"] +
                                " --> " + rstmpRow["EQM_CODE"] + "'' n'a pas de durée, la planification est annulée." + "\n\n";
                            //rs.CancelUpdate
                            //GoTo Boucle
                        }


                        NewRow["Duree"] = General.nz(rstmpRow["PEI_DureeReel"], 1);

                        dtTotDureeP2 = Convert.ToDouble(General.nz(rstmpRow["PEI_DureeReel"], 1)) + dtTotDureeP2;

                        //-- si Pei_Journee est à 1 alors on force l'heure.
                        //If rstmp!PEI_ForceVisite = 1 Then
                        if (rstmpRow["PEI_ForceJournee"].ToString() == "1" || General.nz(rstmpRow["PEI_PassageObli"], 0).ToString() == "1")
                        {
                            NewRow["HeureDebut"] = (General.IsDate(rstmpRow["PEI_Heure"]) ? rstmpRow["PEI_Heure"] : modP2.cHeureDebut);
                        }
                        else
                        {
                            NewRow["HeureDebut"] = modP2.cHeureDebut;
                        }
                        lDuree = Convert.ToDouble(NewRow["Duree"]);



                        //=== calul de l'heure de debut
                        //=== ajout des heures.
                        //vDuree = Format(lDuree, "0.00")

                        //== extrait uniquements les heures.
                        //vDuree = Left(vDuree, InStr(1, vDuree, ".") - 1)

                        //=== ajoute les heures.
                        //vHeureFin = DateAdd("h", vDuree, rs!HeureDebut)

                        //=== ajout des minutes.
                        //vDuree = Format(lDuree, "0.00")

                        //=== extrait uniquement les minutes.
                        //vDuree = Right(vDuree, InStr(1, vDuree, "."))

                        //=== Ajoute les minutes.
                        //vHeureFin = DateAdd("n", vDuree, vHeureFin)

                        vHeureFin = Convert.ToDateTime(NewRow["HeureDebut"]).AddSeconds(lDuree * 3600).ToString("HH:mm:ss");

                        NewRow["HeureFin"] = vHeureFin;

                        NewRow["COP_NoAuto"] = General.nz(rstmpRow["COP_NoAuto"], 0);

                        //=== calcul des charges, à revoir.
                        //sSql = "SELECT PER_Charge From Personnel WHERE(Matricule = '" & UCase(sMatricule) & "')"
                        //dCout = nz(fc_ADOlibelle(sSql), 0) * nz(rstmp!PEI_Duree, 0)
                        dCout = 0;
                        NewRow["PEI_Cout"] = dCout;
                        NewRow["TPP_Code"] = rstmpRow["TPP_Code"];

                        NewRow["PEI_ForceVisite"] = rstmpRow["PEI_ForceVisite"];
                        NewRow["PEI_PassageObli"] = rstmpRow["PEI_PassageObli"];
                        NewRow["EQM_NoAuto"] = rstmpRow["EQM_NoAuto"];
                        NewRow["PEI_NoAuto"] = rstmpRow["PEI_NoAuto"];
                        NewRow["COP_NoContrat"] = rstmpRow["COP_NoContrat"];
                        //        rs!CYC_DateDeb = dtDebPlan
                        //        rs!CYC_DateFin = dtFinPlan

                        NewRow["Utilisateur"] = General.fncUserName();
                        NewRow["Lot"] = dtLot;
                        NewRow["Commentaire"] = rstmpRow["EQM_Observation"];

                        NewRow["GAi_ID"] = rstmpRow["GAi_ID"];
                        NewRow["GAM_Code"] = rstmpRow["GAM_Code"];
                        NewRow["GAI_LIbelle"] = rstmpRow["GAI_LIbelle"];
                        lNoIntervention = 0;
                        SqlCommandBuilder cb = new SqlCommandBuilder(modAdors.SDArsAdo);
                        modAdors.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                        modAdors.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                        SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                        insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                        insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                        modAdors.SDArsAdo.InsertCommand = insertCmd;

                        modAdors.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                        {

                            if (e.StatementType == StatementType.Insert)
                            {

                                if (lNoIntervention == 0)
                                {

                                    lNoIntervention =
                                        Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                                }
                            }
                        });

                        //using (var tmpModAdo = new ModAdo())
                        //    lNoIntervention = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoIntervention) FROM " + sTableOP));

                        NewRow["NoIntervention"] = lNoIntervention;
                        rs.Rows.Add(NewRow.ItemArray);
                        modAdors.Update();


                        //        'Print #iNoFic, "Opération générée ( " & lNointervention & " ) : " & Now

                        //=== AJOUT DES TACHES.
                        //        sSQL = "SELECT FAP_NoAuto, FAP_CodeArticle, FAP_Designation1," _
                        //'        & " EQM_NoAuto, FAP_NoLIgne, FAP_Affiche, COP_NoAuto, NAP2_Noauto" _
                        //'        & " From FAP_FacArticleP2" _
                        //'        & " WHERE EQM_NoAuto = " & nz(rstmp!EQM_NoAuto, 0)
                        //
                        //        Set rst = fc_OpenRecordSet(sSQL)
                        //
                        //        sSQL = "SELECT FAP_NoAuto, CodeUO, CodeImmeuble, CodeNumOrdre," _
                        //'            & " CodeChaufferie, TIP2_CodeArticle, TIP2_Designation1, EQM_NoAuto," _
                        //'            & " TIP2_NoLIgne, TIP2_Affiche , COP_NoAuto, NAP2_Noauto, NoInterventionOp," _
                        //'            & " Utilisateur, Lot " _
                        //'            & " From " & staches _
                        //'            & " "
                        //
                        //        If bSimulation = False Then
                        //            sSQL = sSQL & " WHERE TIP2_Noauto = 0"
                        //        Else
                        //            sSQL = sSQL & " WHERE TIP2S_Noauto = 0"
                        //        End If

                        //        'Print #iNoFic, "Affecte les tâches à l'opération générée ( " & lNointervention & " ) : " & Now


                        //=== modif du 17 10 2016, les taches ne sont plus gérées.
                        //sSQL = " INSERT INTO " & staches & " (FAP_NoAuto, CodeImmeuble,   TIP2_CodeArticle, TIP2_Designation1,"
                        //sSQL = sSQL & " EQM_NoAuto, TIP2_NoLIgne, TIP2_Affiche , COP_NoAuto, NAP2_Noauto, NoInterventionOp, " _
                        //& " Utilisateur, Lot  )"
                        //sSQL = sSQL & " SELECT FAP_NoAuto"
                        //sSQL = sSQL & ", '" & rstmp!CodeImmeuble & "'"
                        //sSQL = sSQL & ", FAP_CodeArticle, FAP_Designation1,"
                        //sSQL = sSQL & " EQM_NoAuto, FAP_NoLIgne, FAP_Affiche, COP_NoAuto, NAP2_Noauto"
                        //sSQL = sSQL & ", " & lNointervention & ""
                        //sSQL = sSQL & ", '" & sUserName & "'"
                        //sSQL = sSQL & ", '" & dtLot & "' "
                        //sSQL = sSQL & " FROM FAP_FacArticleP2 "
                        //sSQL = sSQL & " WHERE EQM_NoAuto = " & nz(rstmp!EQM_NoAuto, 0)

                        //adocnn.Execute sSQL



                        //        Set rsAddT = fc_OpenRecordSet(sSQL)
                        //
                        //        Do While rst.EOF = False
                        //            rsAddT.AddNew
                        //
                        //            rsAddT!FAP_NoAuto = rst!FAP_NoAuto
                        //            rsAddT!CodeUO = strCodeUO
                        //            rsAddT!CodeImmeuble = rstmp!CodeImmeuble & ""
                        //            rsAddT!CodeNumOrdre = rstmp!CodeNumOrdre & ""
                        //            rsAddT!CodeChaufferie = rstmp!CodeChaufferie & ""
                        //            rsAddT!TIP2_CodeArticle = rst!FAP_CodeArticle
                        //            rsAddT!TIP2_Designation1 = rst!FAP_Designation1
                        //            rsAddT!EQM_NoAuto = rst!EQM_NoAuto
                        //            rsAddT!TIP2_NoLIgne = rst!FAP_NoLIgne
                        //            rsAddT!TIP2_Affiche = rst!FAP_Affiche
                        //            rsAddT!COP_NoAuto = lCop_NoAuto
                        //            rsAddT!NAP2_Noauto = rst!NAP2_Noauto
                        //            rsAddT!NoInterventionOp = lNointervention
                        //            rsAddT!Utilisateur = sUserName
                        //            rsAddT!Lot = dtLot
                        //            rsAddT.UPDATE
                        //
                        //            rst.MoveNext
                        //        Loop
                        //
                        //
                        //        rst.Close
                        //        rsAddT.Close
                        //        Set rst = Nothing
                        //        Set rsAddT = Nothing
                        System.Windows.Forms.Application.DoEvents();
                        //===> Mondir le 15.10.2020, j'ai changer Boucle par Boucle2 pour corriger le ticket #2020, Boule sort de la boucle principale donc toutes les visites de la préstation vont
                        //===> pas etre créés.
                        Boucle2:
                        continue;
                        //===> Fin Modif Mondir
                    }
                    Boucle:

                    continue;
                    System.Windows.Forms.Application.DoEvents();
                }

                //Print #iNoFic, "Fin de l'extraction des visites (opérations) : " & Now

                if (bInfoPrest == true)
                {
                    //=== ne fait rien.
                }
                else
                {
                    short iNoFic = 0;
                    fc_fusionneP2v2(dtDebPlan, dtFinPlan, sIntervenant, sCodeImmeuble, lCop_NoAuto, bSimulation, dtLot, iNoFic);
                }

                if (!string.IsNullOrEmpty(sMessageErr))
                {
                    functionReturnValue = sMessageErr;
                }

                //Print #iNoFic, "Fin : " & Now

                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2;fc_SimulationCreeCalendrierV2");
                return functionReturnValue;
            }


            //===> Mondir le 16.04.2021, desable update log https://groupe-dt.mantishub.io/view.php?id=2403
            ModAdo.LogActive = true;
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sWhere"></param>
        /// <param name="sMessageErr"></param>
        /// <param name="sIntervenant"></param>
        /// <param name="sPlanning"></param>
        /// <returns></returns>
        private static object fc_AttribueDateDate(string sWhere, ref string sMessageErr, string sIntervenant, string sPlanning)
        {
            object functionReturnValue = null;
            string sSQL = null;
            string sCodeChantier = null;
            DataTable rsAdd = default(DataTable);
            ModAdo modAdorsAdd = null;
            System.DateTime dtCreat = default(System.DateTime);
            System.DateTime dtNow = default(System.DateTime);
            System.DateTime dtDateDepart = default(System.DateTime);
            System.DateTime dtDebOp = default(System.DateTime);
            System.DateTime dtFinOp = default(System.DateTime);
            System.DateTime dtFevrier = default(System.DateTime);

            int lJd = 0;
            int lMd = 0;
            int lJf = 0;
            int lMf = 0;

            try
            {
                if (!string.IsNullOrEmpty(sIntervenant))//tested
                {
                    sSQL = "SELECT DISTINCT ";
                }
                else//tested
                {
                    sSQL = "SELECT ";
                }




                //sSQL = sSQL & "  COP_ContratP2.codeimmeuble, COP_ContratP2.COP_NoContrat, COP_ContratP2.COP_DateSignature, COP_ContratP2.COP_DateFin, " _
                //& " PEI_PeriodeGammeImm.SEM_Code, PEI_PeriodeGammeImm.PEI_Visite, EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.EQM_Libelle, " _
                //& " ICC_IntervCategorieContrat.CAI_Libelle, PDAB_BadgePDA.PDAB_Noauto, PDAB_BadgePDA.PDAB_Libelle, PDAB_BadgePDA.PDAB_IDBarre," _
                //& " PEI_PeriodeGammeImm.PEI_JD, PEI_PeriodeGammeImm.PEI_MD, PEI_PeriodeGammeImm.PEI_JF, PEI_PeriodeGammeImm.PEI_MF, " _
                //& " PEI_PeriodeGammeImm.PEI_NoAuto, ICC_IntervCategorieContrat.CAI_Libelle AS Expr1, EQM_EquipementP2Imm.EQM_Code AS Expr2," _
                //& " GAI_GammeImm.GAM_Code , GAI_GammeImm.GAI_LIbelle " _
                //& " FROM         PDAB_BadgePDA INNER JOIN " _
                //& " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble INNER JOIN " _
                //& " PEI_PeriodeGammeImm INNER JOIN " _
                //& " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN " _
                //& " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON " _
                //& " PDAB_BadgePDA.PDAB_Noauto = ICC_IntervCategorieContrat.PDAB_Noauto INNER JOIN " _
                //& " GAI_GammeImm ON PDAB_BadgePDA.PDAB_Noauto = GAI_GammeImm.PDAB_Noauto AND " _
                //& " ICC_IntervCategorieContrat.GAi_ID = GAI_GammeImm.GAi_ID AND COP_ContratP2.COP_NoAuto = GAI_GammeImm.COP_Noauto LEFT OUTER JOIN " _
                //& " Immeuble ON COP_ContratP2.codeimmeuble = Immeuble.CodeImmeuble"


                //=== modif du 27 01 2016, ajout de la table contrat, le champs datefin de la table contrat remplace
                //=== la date de fin de la table COP_CONTRATP2.

                sSQL = sSQL + "  COP_ContratP2.codeimmeuble, COP_ContratP2.COP_NoContrat, COP_ContratP2.COP_DateSignature,  "
                    + " PEI_PeriodeGammeImm.SEM_Code, PEI_PeriodeGammeImm.PEI_Visite, " +
                    "EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.EQM_Libelle, "
                    + " ICC_IntervCategorieContrat.CAI_Libelle, PDAB_BadgePDA.PDAB_Noauto, PDAB_BadgePDA.PDAB_Libelle, PDAB_BadgePDA.PDAB_IDBarre," +
                    " PEI_PeriodeGammeImm.PEI_JD, PEI_PeriodeGammeImm.PEI_MD, PEI_PeriodeGammeImm.PEI_JF, PEI_PeriodeGammeImm.PEI_MF, "
                    + " PEI_PeriodeGammeImm.PEI_NoAuto, ICC_IntervCategorieContrat.CAI_Libelle AS Expr1," +
                    " EQM_EquipementP2Imm.EQM_Code AS Expr2,"
                    + " GAI_GammeImm.GAM_Code , GAI_GammeImm.GAI_LIbelle, Contrat.DateFin, Immeuble.CodeRamoneur ";

                if (General.sGMAOcriterePartImmeuble == "1")
                {
                    sSQL = sSQL + " FROM         Gestionnaire RIGHT OUTER JOIN"
                       + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire AND Gestionnaire.Code1 = Immeuble.Code1 RIGHT OUTER JOIN "
                       + " PDAB_BadgePDA INNER JOIN "
                       + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble INNER JOIN "
                       + " PEI_PeriodeGammeImm INNER JOIN "
                       + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                       + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                      + " PDAB_BadgePDA.PDAB_Noauto = ICC_IntervCategorieContrat.PDAB_Noauto INNER JOIN "
                      + " GAI_GammeImm ON PDAB_BadgePDA.PDAB_Noauto = GAI_GammeImm.PDAB_Noauto AND "
                      + " ICC_IntervCategorieContrat.GAi_ID = GAI_GammeImm.GAi_ID AND COP_ContratP2.COP_NoAuto = GAI_GammeImm.COP_Noauto INNER JOIN  "
                       + " Contrat ON COP_ContratP2.NumContratRef = Contrat.NumContrat AND COP_ContratP2.AvenantRef = Contrat.Avenant ON "
                      + " Immeuble.CodeImmeuble = COP_ContratP2.CodeImmeuble ";
                }
                else
                {
                    sSQL = sSQL + " FROM         PDAB_BadgePDA INNER JOIN"
                        + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble INNER JOIN "
                   + " PEI_PeriodeGammeImm INNER JOIN " +
                   " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                   + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                   +
                   " PDAB_BadgePDA.PDAB_Noauto = ICC_IntervCategorieContrat.PDAB_Noauto INNER JOIN "
                   + " GAI_GammeImm ON PDAB_BadgePDA.PDAB_Noauto = GAI_GammeImm.PDAB_Noauto AND "
                   + " ICC_IntervCategorieContrat.GAi_ID = " +
                   "GAI_GammeImm.GAi_ID AND COP_ContratP2.COP_NoAuto = GAI_GammeImm.COP_Noauto INNER JOIN "
                   + " Contrat ON COP_ContratP2.NumContratRef= Contrat.NumContrat AND COP_ContratP2.AvenantRef= Contrat.Avenant LEFT OUTER JOIN " +
                   " Immeuble ON COP_ContratP2.codeimmeuble = Immeuble.CodeImmeuble";

                }




                if (!string.IsNullOrEmpty(sIntervenant))
                {
                    //sSQL = sSQL & "  INNER JOIN Immeuble ON Immeuble.CodeImmeuble = COP_ContratP2.codeimmeuble INNER JOIN IntervenantImm_INM ON Immeuble.CodeImmeuble = IntervenantImm_INM.Codeimmeuble_IMM "
                }

                sSQL = sSQL + sWhere;


                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    return functionReturnValue;
                }

                sSQL = "DELETE FROM " + sPlanning + " WHERE PEDG_User = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' ";
                General.Execute(sSQL);

                sSQL = "SELECT     PEDG_Noauto, PEI_NoAuto, PEDG_DateFin, PEDG_DateDebut, PEDG_User, PEDG_DateCreat" + " From  " + sPlanning + " WHERE     (PEDG_Noauto = 0)";

                modAdorsAdd = new ModAdo();
                rsAdd = modAdorsAdd.fc_OpenRecordSet(sSQL);


                dtCreat = DateTime.Now;

                foreach (DataRow rsRow in rs.Rows)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    sCodeChantier = rsRow["CodeImmeuble"].ToString();

                    if (Convert.ToInt32(General.nz(rsRow["PEI_JD"], 0)) == 0)
                    {
                        sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rsRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" + rsRow["CAI_Libelle"] +
                            " --> " + rsRow["EQM_CODE"] + "'' n'a pas de jour de début, la planification est annulée." + "\n" + "\n";

                        continue;
                    }
                    else
                    {
                        lJd = Convert.ToInt32(rsRow["PEI_JD"]);
                    }

                    if (Convert.ToInt32(General.nz(rsRow["PEI_MD"], 0)) == 0)
                    {
                        sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rsRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" + rsRow["CAI_Libelle"] +
                            " --> " + rsRow["EQM_CODE"] + "'' n'a pas de mois de début, la planification est annulée." + "\n" + "\n";

                        continue;
                    }
                    else
                    {
                        lMd = Convert.ToInt32(rsRow["PEI_MD"]);
                    }

                    if (General.nz(rsRow["PEI_JF"], 0).ToString() == "0")
                    {
                        sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rsRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" +
                            rsRow["CAI_Libelle"] + " --> " + rsRow["EQM_CODE"] + "'' n'a pas de jours de fin, la planification est annulée." + "\n" + "\n";

                        continue;
                    }
                    else
                    {
                        lJf = Convert.ToInt32(rsRow["PEI_JF"]);
                    }

                    if (General.nz(rsRow["PEI_MF"], 0).ToString() == "0")
                    {
                        sMessageErr = sMessageErr + "Erreur dans la fiche contrat n°" + rsRow["COP_NoContrat"] + " (immeuble n°" + sCodeChantier + ") ==>" + "\n" + "l'opération ''" +
                            rsRow["CAI_Libelle"] + " --> " + rsRow["EQM_CODE"] + "'' n'a pas de mois de fin, la planification est annulée." + "\n" + "\n";

                        continue;
                    }
                    else
                    {
                        lMf = Convert.ToInt32(rsRow["PEI_MF"]);
                    }

                    if (rsRow["COP_DateSignature"] == DBNull.Value)
                    {
                        continue;
                    }

                    dtDateDepart = Convert.ToDateTime(rsRow["COP_DateSignature"]);

                    //=== si le mois début > mois fin
                    if (lMd > lMf)
                    {

                        if (lJf > dtDateDepart.Day && lMf > dtDateDepart.Month)
                        {
                            dtDebOp = Convert.ToDateTime(lJd + "/" + lMd + "/" + (dtDateDepart.Year));
                            dtFinOp = Convert.ToDateTime(lJf + "/" + lMf + "/" + (dtDateDepart.Year + 1));
                        }
                        else
                        {
                            dtDebOp = Convert.ToDateTime(lJd + "/" + lMd + "/" + (dtDateDepart.Year - 1));
                            dtFinOp = Convert.ToDateTime(lJf + "/" + lMf + "/" + (dtDateDepart.Year));
                        }

                    }
                    else//tested
                    {
                        dtDebOp = Convert.ToDateTime(lJd + "/" + lMd + "/" + dtDateDepart.Year);
                        //==== modif du 06 septempbre 2013, rechercher le jour de fin de mois uniquement pour le mois
                        //==== de fervrier. explication : dans la saisie des prestations dans le contrat, l'utilisateur peut saisir le jour
                        //==== 29 pour le mois de février, ce sui peut poser poser probléme si dans l'année pour le mois de février le dernier jour est le 28.
                        if (lMf == 2 && lJf >= 28)
                        {
                            dtFevrier = fDate.fc_FinDeMois(Convert.ToString(lMf), Convert.ToString(dtDateDepart.Year));
                            dtFinOp = Convert.ToDateTime(dtFevrier.Day + "/" + lMf + "/" + (dtDateDepart.Year));
                            // dtFinOp = lJf & "/" & lMf & "/" & (Year(dtDateDepart))
                        }
                        else
                        {
                            //===> Mondir le 03.07.2020 : Added this condition to check if the date is OK
                            //Sometimes the user put 30 in lMf, lMf varibale contains the month number, so it must not be bigger than 12
                            //In VB6 it pass, but not in C#, so i added this test, if KO we reverse the order
                            //===> Best solution is to find where PEI_MF AND PEI_JF are, and check if the date is OK before inserting
                            if (General.IsDate(lJf + "/" + lMf + "/" + dtDateDepart.Year))
                                dtFinOp = Convert.ToDateTime(lJf + "/" + lMf + "/" + dtDateDepart.Year);
                            else if (General.IsDate(lMf + "/" + lJf + "/" + dtDateDepart.Year))
                                dtFinOp = Convert.ToDateTime(lMf + "/" + lJf + "/" + dtDateDepart.Year);
                            //===> Fin Modif Mondir
                        }


                    }
                    int PEDG_Noauto = 0;

                    var NewRow = rsAdd.NewRow();

                    NewRow["PEI_NoAuto"] = rsRow["PEI_NoAuto"];
                    NewRow["PEDG_DateDebut"] = dtDebOp;
                    NewRow["PEDG_DateFin"] = dtFinOp;
                    NewRow["PEDG_User"] = General.fncUserName();
                    NewRow["PEDG_DateCreat"] = DateTime.Now;

                    //using (var tmpAdo = new ModAdo())
                    //PEDG_Noauto = Convert.ToInt32(tmpAdo.fc_ADOlibelle("SELECT   MAX(PEDG_Noauto) From " + sPlanning ));

                    SqlCommandBuilder cb = new SqlCommandBuilder(modAdorsAdd.SDArsAdo);
                    modAdorsAdd.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                    modAdorsAdd.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                    SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                    insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                    insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                    modAdorsAdd.SDArsAdo.InsertCommand = insertCmd;

                    modAdorsAdd.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                    {

                        if (e.StatementType == StatementType.Insert)
                        {

                            if (PEDG_Noauto == 0)
                            {

                                PEDG_Noauto =
                                    Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                            }
                        }
                    });

                    NewRow["PEDG_Noauto"] = PEDG_Noauto;
                    rsAdd.Rows.Add(NewRow.ItemArray);
                    modAdorsAdd.Update();

                }

                modAdorsAdd.Close();
                modAdors.Close();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2v2;fc_AttribueDateDate");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDate"></param>
        /// <param name="lSamedi"></param>
        /// <param name="lDimanche"></param>
        /// <param name="lJourFeriee"></param>
        /// <param name="lPaques"></param>
        /// <param name="lPentecote"></param>
        /// <param name="sSEM_Code"></param>
        /// <param name="lPEI"></param>
        /// <param name="sTPP_Code"></param>
        /// <returns></returns>
        public static System.DateTime fc_DateAddControleV2(System.DateTime dDate, int lSamedi = 0, int lDimanche = 0, int lJourFeriee = 0, int lPaques = 0, int lPentecote = 0, string sSEM_Code = "", int lPEI = 0, string sTPP_Code = "")
        {

            //=== Ajouter ici le code pour controler qu une date est valable,
            //=== ex jours fériées, samedi, dimanche.
            int i = 0;
            int lNoJour = 0;
            System.DateTime dtDateSem = default(System.DateTime);
            int lNoJourComp = 0;
            int lDateAdd = 0;
            //=== si lJourFeriee est à 1, on ne prends pas en compte les jours fériées.
            //If lJourFeriee = 1 Then
            //
            //    For i = 0 To UBound(tlJourFeriee, 2)
            //        If Day(dDate) = tlJourFeriee(0, i) And Month(dDate) = tlJourFeriee(1, i) Then
            //            dDate = DateAdd("d", 1, dDate)
            //        End If
            //    Next
            //End If
            //
            //
            //'=== si lPaques est à 1, on ne prends pas en compte paques.
            //If lPaques = 1 Then
            //    If fPaques(dDate) = True Then
            //        dDate = DateAdd("d", 1, dDate)
            //    End If
            //End If
            //
            //If lPentecote = 1 Then
            //    If fPentecote(dDate) = True Then
            //        dDate = DateAdd("d", 1, dDate)
            //    End If
            //End If

            //-- Ajoute un jour si Le jour Tombe le weekEnd
            lDateAdd = 0;
            //Do
            //    'on boucle pour mettre à jour la date selon le
            //    'jour demandé. ex tache hedomadaire à planifier tous les lundi
            //    'f Weekday(dDate) = lJour Then
            //    '    Exit Do
            //    'Else
            //    '    dDate = DateAdd("d", 1, dDate)
            //    'End If
            //
            //    '==== saute les samedi et dimanche.
            //    If Weekday(dDate) = cSamedi Then
            //            If lSamedi = 1 Then
            //                dDate = DateAdd("d", 1, dDate)
            //                lDateAdd = lDateAdd + 1
            //            'Else
            //            '    Exit Do
            //            End If
            //
            //    ElseIf Weekday(dDate) = cDimanche Then
            //            If lDimanche = 1 Then
            //                dDate = DateAdd("d", 1, dDate)
            //                lDateAdd = lDateAdd + 1
            //            'Else
            //            '    Exit Do
            //            End If
            //    ElseIf fc_CtrlJourFerie(dDate, lJourFeriee, lPaques, lPentecote) = True Then
            //            '=== ne fait rien, boucle une nouvelle fois, attention la vartiable
            //            '=== dDAte peut avoir été modifié dans la fonction fc_CtrlJourFerie.
            //            lDateAdd = lDateAdd + 1
            //    Else
            //            Exit Do
            //    End If
            //
            //Loop


            //=== modif du 23 01 2017, prise en compte du jour iùmposé.
            if (lPEI == 1)
            {
                if (!string.IsNullOrEmpty(sSEM_Code))
                {
                    lNoJour = modP2.fc_ReturnDay(sSEM_Code);

                    if (sTPP_Code.ToUpper() == modP2.cHebdo.ToUpper())
                    {
                        dDate = dDate.AddDays(-lNoJour);
                    }

                    dtDateSem = dDate;


                    if (lNoJour != 0)
                    {
                        do
                        {
                            lNoJourComp = (int)dtDateSem.DayOfWeek;
                            if (lNoJour == lNoJourComp)
                            {
                                dDate = dtDateSem;
                                break;
                            }
                            else
                            {
                                dtDateSem = dtDateSem.AddDays(1);
                            }

                        } while (true);
                        //dtDateSemCOmp = Weekday(tdPeriodeAp(i), vbSunday)
                    }

                    //==== modif du 25 04 2017, controle si ce jour est fériée.
                    do
                    {
                        //==== saute les samedi et dimanche.
                        if ((short)dDate.DayOfWeek == modP2.cSamedi)
                        {
                            if (lSamedi == 1)
                            {
                                dDate = dDate.AddDays(1);
                            }

                        }
                        else if ((short)dDate.DayOfWeek == modP2.cDimanche)
                        {
                            if (lDimanche == 1)
                            {
                                dDate = dDate.AddDays(1);

                            }
                        }
                        else if (fc_CtrlJourFerie(ref dDate, lJourFeriee, lPaques, lPentecote) == true)
                        {
                            //=== ne fait rien, boucle une nouvelle fois, attention la vartiable
                            //=== dDAte peut avoir été modifié dans la fonction fc_CtrlJourFerie.
                        }
                        else
                        {
                            break;
                        }

                    } while (true);
                    //==== fin modif du 25 04 2017.
                }
            }


            do
            {
                //on boucle pour mettre à jour la date selon le
                //jour demandé. ex tache hedomadaire à planifier tous les lundi
                //f Weekday(dDate) = lJour Then
                //    Exit Do
                //Else
                //    dDate = DateAdd("d", 1, dDate)
                //End If

                //==== saute les samedi et dimanche.
                if ((int)dDate.DayOfWeek == modP2.cSamedi)
                {
                    if (lSamedi == 1)
                    {
                        dDate = dDate.AddDays(1);
                        lDateAdd = lDateAdd + 1;
                        //=== 10 05 2018 a remettre en comenatire au cas ou.
                    }
                    else
                    {
                        break;
                        //=== fin 10 05 2018
                    }

                }
                else if ((int)dDate.DayOfWeek == modP2.cDimanche)
                {
                    if (lDimanche == 1)
                    {
                        dDate = dDate.AddDays(1);
                        lDateAdd = lDateAdd + 1;
                        //=== 10 05 2018 a remettre en comenatire au cas ou.
                    }
                    else
                    {
                        break;
                        //=== fin 10 05 2018
                    }
                }
                else if (fc_CtrlJourFerie(ref dDate, lJourFeriee, lPaques, lPentecote) == true)
                {
                    //=== ne fait rien, boucle une nouvelle fois, attention la vartiable
                    //=== dDAte peut avoir été modifié dans la fonction fc_CtrlJourFerie.
                    lDateAdd = lDateAdd + 1;
                }
                else
                {
                    break;
                }

            } while (true);

            return dDate;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sPeriode"></param>
        /// <param name="dDate"></param>
        /// <param name="lCycle"></param>
        /// <param name="sSEM_Code"></param>
        /// <param name="lPEI"></param>
        /// <returns></returns>
        public static System.DateTime fc_DateAdd(string sPeriode, System.DateTime dDate, int lCycle = 0, string sSEM_Code = "", int lPEI = 0)
        {

            int lMonth = 0;
            System.DateTime dtDateSem = default(System.DateTime);
            //Dim dtDateSemCOmp               As Date
            int lNoJour = 0;
            int lNoJourComp = 0;

            if (sPeriode.ToUpper() == modP2.cQuotidien.ToUpper())
            {
                dDate = dDate.AddDays(1);
            }
            else if (sPeriode.ToUpper() == modP2.cHebdo.ToUpper())
            {
                // ajoute une  semaine.
                dDate = dDate.AddDays(7);
            }
            else if (sPeriode.ToUpper() == modP2.cMensuel.ToUpper())
            {
                // ajoute un mois.
                dDate = dDate.AddMonths(1);
                //Do
                //on boucle pour mettre à jour la date sur le premier du mois
                // controle si c est un dimanche  ou un samedi.

                //   If Day(dDate) = 1 Then
                //       If Weekday(dDate) = cDimanche Then
                //           dDate = DateAdd("d", 1, dDate)
                //       End If
                //       Exit Do
                //   Else
                //      dDate = DateAdd("d", -1, dDate)
                //   End If
                //Loop
            }
            else if (sPeriode.ToUpper() == modP2.cBiMestriel.ToUpper())
            {
                dDate = dDate.AddMonths(2);
            }
            else if (sPeriode.ToUpper() == modP2.cTriMestriel.ToUpper())
            {
                dDate = dDate.AddMonths(3);
            }
            else if (sPeriode.ToUpper() == modP2.cSemestriel.ToUpper())
            {
                dDate = dDate.AddMonths(6);
            }
            else if (sPeriode.ToUpper() == modP2.cAnnuel.ToUpper())
            {
                dDate = dDate.AddYears(1);
            }
            else if (sPeriode.ToUpper() == cTriAnnuel.ToUpper() || sPeriode.ToUpper() == cTriAnnuel1.ToUpper())
            {
                dDate = dDate.AddYears(3);
            }
            else if (sPeriode.ToUpper() == modP2.cCycleAnnuel.ToUpper())
            {
                dDate = dDate.AddYears(lCycle);
            }
            else if (sPeriode.ToUpper() == modP2.cCycleJournee.ToUpper())
            {
                dDate = dDate.AddDays(lCycle);
            }
            else if (sPeriode.ToUpper() == modP2.cCycleMensuel.ToUpper())
            {
                dDate = dDate.AddMonths(lCycle);
            }
            else if (sPeriode.ToUpper() == modP2.cBiMensuel.ToUpper())
            {
                if (dDate.Day < 15)
                {
                    dDate = dDate.AddDays(15);

                }
                else if (dDate.Day >= 15)
                {
                    lMonth = dDate.Month;

                    dDate = dDate.AddDays(15);
                    if (lMonth == dDate.Month)
                    {
                        while (lMonth == dDate.Month)
                        {
                            //=== dernier jour du mois.
                            dDate = dDate.AddDays(1);
                        }
                    }
                    else if (lMonth != dDate.Month)
                    {
                        //=== si on ajoute le 15 jours le 15 feverier, on tombe par exemple sur le 3 mars
                        //=== or on veut que ce soit le premier jour ouvré.
                        dDate = Convert.ToDateTime("01/" + dDate.Month + "/" + dDate.Year);
                    }
                }
            }

            //=== modif du 12 10 2016, adapte la date n jour est forcé.
            //=== modif du 23 01 2017, cette parti n'est plus géré das cette fonction.
            if (lPEI == 1)
            {
                //        If sSEM_Code <> "" Then
                //            lNoJour = fc_ReturnDay(sSEM_Code)
                //            dtDateSem = dDate
                //
                //            If lNoJour <> 0 Then
                //                    Do
                //                        lNoJourComp = Weekday(dtDateSem, vbSunday)
                //                        If lNoJour = lNoJourComp Then
                //                                dDate = dtDateSem
                //                                Exit Do
                //                        Else
                //                                dtDateSem = DateAdd("d", 1, dtDateSem)C:\Users\AC1\delostal-thibault\Axe_interDT\Shared\Correcteur.cs
                //                        End If
                //
                //                    Loop
                //                    ''''dtDateSemCOmp = Weekday(tdPeriodeAp(i), vbSunday)
                //            End If
                //        End If
            }


            return dDate;

        }

        public static void fc_fusionneP2v2(System.DateTime dDebut, System.DateTime dFin, string sIntervenant, string sCodeImmeuble, int lCop_NoAuto, bool bSimulation, System.DateTime dtLot, short iNoFic = 0)
        {

            short i = 0;

            System.DateTime dDatePrevue = default(System.DateTime);
            System.DateTime dtHeureDebP = default(System.DateTime);
            System.DateTime dDatePrevueTemp = default(System.DateTime);

            int lNoSemaine = 0;
            int lINT_A = 0;
            int lINT_M = 0;
            int lINT_J = 0;


            int lnumfichestandard = 0;
            int lNoIntervention = 0;


            double dbDuree = 0;

            DataTable rsStandard = default(DataTable);
            ModAdo modAdorsStandard = null;
            string sDesignation = null;
            string sTablInt = null;
            string sTableOP = null;
            string sPDAB_Noauto = null;
            string sAnalytiqueActivite = null;
            string sSqlPrevue = null;
            string sWhereInt = null;
            string sCommentaireInter = null;

            int lEQU_P1Compteur = 0;

            double dbTotDuree = 0;

            bool bAddInter = false;

            double dbJour = 0;
            int lNbJour = 0;
            int xJ = 0;
            double dbDureeInt = 0;
            double dbDureeBase = 0;
            System.DateTime dtHeureFin = default(System.DateTime);
            System.DateTime dtHeureDebut = default(System.DateTime);
            bool bFirst = false;
            string reqModAdorsUpdate;
            try
            {

                // Print #iNoFic, "Début fusion : " & Now


                //==== affecte la table selon que l'on soit en mode simulation ou en réel.
                if (bSimulation == true)//tested
                {
                    sTablInt = "InterventionP2Simul";
                    sTableOP = "OperationP2Simul";
                }
                else//tested
                {
                    sTablInt = "Intervention";
                    sTableOP = "OperationP2";
                }

                General.sSQL = "SELECT NoIntervention,NoInterventionVisite, Duree,  " + " HeureDebut, HeureFin, CodeImmeuble,  "
                    + "  Cop_NoAuto,COP_NoContrat, Intervenant" + " , DatePrevue, Designation, Article, HeureDebut, HeureFin,NoSemaine,INT_Annee"
                    + " ,  TPP_Code, PDAB_Noauto, PDAB_Libelle, PDAB_IDBarre, CompteurObli, EQU_P1Compteur, NumFicheStandard, GAI_LIBELLE, INT_ForceJournee "
                    + " FROM " + sTableOP;



                //sWhereInt = " WHERE " & sTableOP & ".lot ='" & dtLot & "'" _
                //& " AND " & sTableOP & ".Utilisateur = '" & fncUserName & "' AND NoInterventionVisite IS NULL"
                sWhereInt = " WHERE " + sTableOP + ".Utilisateur = '" + General.fncUserName() + "' AND NoInterventionVisite IS NULL";

                General.sSQL = General.sSQL + sWhereInt;

                //===  Where DatePrevue>'" & dDebut & "'" and DatePrevue <='" & dFin & "' and NoInterventionVisite is null

                //--ajoute des clauses envoyées en paramétre.
                if (!string.IsNullOrEmpty(sIntervenant))
                {
                    General.sSQL = General.sSQL + " AND Intervenant = '" + sIntervenant + "'";
                }

                if (!string.IsNullOrEmpty(sCodeImmeuble))//tested
                {
                    General.sSQL = General.sSQL + " AND CodeImmeuble ='" + sCodeImmeuble + "'";
                }

                if (lCop_NoAuto != 0)//tested
                {
                    General.sSQL = General.sSQL + " AND Cop_NoAuto = " + lCop_NoAuto;
                }

                //sSQL = sSQL & " ORDER BY  CompteurObli DESC, CodeImmeuble,PDAB_Noauto, Intervenant ,INT_Annee, NoSemaine , DatePrevue "

                General.sSQL = General.sSQL + " ORDER BY CodeImmeuble, Intervenant ,INT_Annee, NoSemaine , DatePrevue, EQU_P1Compteur DESC, PDAB_Noauto ";
                //sSQL = sSQL & " , HeureDebut "

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);


                //--Crée un recordset pour la fusion des taches en visites
                General.sSQL = "SELECT * FROM " + sTablInt + "  WHERE NoIntervention = 0";

                //& " WHERE CodeImmeuble ='" & sCodeImmeuble & "' "
                //sSql = sSql & " AND CodeNumOrdre = '" & sCodeNumOrdre & "'"
                //sSql = sSql & " AND CodeChaufferie = '" & sCodeChaufferie & "'"

                ////////////***********************
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(General.sSQL);

                /////////////*********************
                ///
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    //=== recherche le code analytique contrat.
                    SAGE.fc_OpenConnSage();
                    using (var tmpModAdo = new ModAdo())
                        sAnalytiqueActivite = tmpModAdo.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteContrat='1'", false, SAGE.adoSage);
                    SAGE.fc_CloseConnSage();
                }

                if (General.rstmp.Rows.Count > 0)//tested
                {

                    if (bSimulation == false)//tested
                    {
                        //=== cree un recordset pour les créations des fiches standard.
                        General.sSQL = "SELECT NumFicheStandard, Utilisateur, DateAjout,"
                            + " CodeImmeuble, CodeOrigine1, CodeOrigine2, Source,Intervention, AnalytiqueActivite, GMAO_Visite, Lot" +
                            " FROM GestionStandard where NumFicheStandard = 0";

                        modAdorsStandard = new ModAdo();
                        rsStandard = modAdorsStandard.fc_OpenRecordSet(General.sSQL);
                    }

                    //=== Récup. du libelle de la designation.
                    using (var tmpModAdo = new ModAdo())
                        sDesignation = General.nz(tmpModAdo.fc_ADOlibelle("SELECT Designation1 FROM FacArticle WHERE CodeArticle = '" + cCodeVisite + "'"), cDesignationP2).ToString();

                    //=== Initialise les variables.
                    sCodeImmeuble = "";
                    sIntervenant = "";
                    lNoSemaine = 0;
                    lINT_A = 0;
                    lEQU_P1Compteur = 0;

                    foreach (DataRow rstmpRow in General.rstmp.Rows)
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        //sCommentaireInter

                        //If rs!Nosemaine = 52 Then Stop

                        //If sPDAB_Noauto <> nz(!PDAB_Noauto, "") _
                        //Or sCodeimmeuble <> nz(!CodeImmeuble, "") Or sIntervenant <> nz(!Intervenant, "") _
                        //Or lNoSemaine <> !NoSemaine Or lINT_A <> !INT_Annee _
                        //Or ((nz(!TPP_Code & "", "") = cCycleJournee) And dDatePrevue <> !DatePrevue) _
                        //Or ((UCase(nz(!TPP_Code & "", "")) = UCase(cQuotidien)) And dDatePrevue <> !DatePrevue) Or lEQU_P1Compteur <> nz(!EQU_P1Compteur, 0) Then 'Or nz(!EQU_P1Compteur, 0) = 1 Then


                        //=== modif du 07 02 2016, si il existe plusieurs localisations
                        //=== pour un même immeuble; ojn ne crée qu'une seule intervent
                        //If sCodeImmeuble <> nz(!CodeImmeuble, "") Or sIntervenant <> nz(!Intervenant, "") _
                        //Or lNoSemaine <> !NoSemaine Or lINT_A <> !INT_Annee _
                        //Or ((nz(!TPP_Code & "", "") = cCycleJournee) And dDatePrevue <> !DatePrevue) _
                        //Or ((UCase(nz(!TPP_Code & "", "")) = UCase(cQuotidien)) And dDatePrevue <> !DatePrevue) _
                        //Or lEQU_P1Compteur <> nz(!EQU_P1Compteur, 0) Or dDatePrevue <> !DatePrevue Then


                        //If sCodeImmeuble <> nz(!CodeImmeuble, "") Or sIntervenant <> nz(!Intervenant, "") _
                        //Or lNoSemaine <> !NoSemaine Or lINT_A <> !INT_Annee _
                        //Or ((nz(!TPP_Code & "", "") = cCycleJournee) And dDatePrevue <> !DatePrevue) _
                        //Or ((UCase(nz(!TPP_Code & "", "")) = UCase(cQuotidien)) And dDatePrevue <> !DatePrevue) _
                        //Or lEQU_P1Compteur <> nz(!EQU_P1Compteur, 0) Or dDatePrevue <> !DatePrevue Or dtHeureDebP <> !HeureDebut Then

                        //=== modif du 27 11 2017, on retire la condition  Or lEQU_P1Compteur <> nz(!EQU_P1Compteur, 0) pour
                        //=== fusionner les interventions meme celle comprenant une saisie de compteur.

                        //DataRow rowRs = null;

                        //===> Mondir le 15.01.2021, ajout du .TimeOfDay à dtHeureDebP et Convert.ToDateTime(rstmpRow["HeureDebut"]) pour corriger https://groupe-dt.mantishub.io/view.php?id=2257
                        //===> Car des fois dtHeureDebP == 1899-12-30 08:00:00.000 et Convert.ToDateTime(rstmpRow["HeureDebut"]) == 2021-02-01 08:00:00.000 donc ça ne va ma fusioner les deux inter
                        //===> due to database conception !
                        if (sCodeImmeuble != General.nz(rstmpRow["CodeImmeuble"], "").ToString() ||
                            sIntervenant != General.nz(rstmpRow["Intervenant"], "").ToString() ||
                            lNoSemaine != Convert.ToInt32(rstmpRow["NoSemaine"]) ||
                            lINT_A != Convert.ToInt32(rstmpRow["INT_Annee"]) ||
                            ((General.nz(rstmpRow["TPP_Code"] + "", "").ToString() == modP2.cCycleJournee) && dDatePrevue != Convert.ToDateTime(rstmpRow["DatePrevue"])) ||
                            ((General.nz(rstmpRow["TPP_Code"] + "", "").ToString().ToUpper() == modP2.cQuotidien.ToUpper()) && dDatePrevue != Convert.ToDateTime(rstmpRow["DatePrevue"])) ||
                            dDatePrevue != Convert.ToDateTime(rstmpRow["DatePrevue"]) ||
                            dtHeureDebP.TimeOfDay != Convert.ToDateTime(rstmpRow["HeureDebut"]).TimeOfDay)
                        {

                            lEQU_P1Compteur = Convert.ToInt32(General.nz(rstmpRow["EQU_P1Compteur"], 0));
                            sPDAB_Noauto = General.nz(rstmpRow["PDAB_Noauto"], "").ToString();
                            dbDuree = 0;
                            sIntervenant = General.nz(rstmpRow["Intervenant"], "").ToString();
                            dDatePrevue = Convert.ToDateTime(rstmpRow["DatePrevue"]);
                            sCodeImmeuble = General.nz(rstmpRow["CodeImmeuble"], "").ToString();
                            lNoSemaine = Convert.ToInt32(General.nz(rstmpRow["NoSemaine"], 0));
                            lINT_A = Convert.ToInt32(General.nz(rstmpRow["INT_Annee"], 0));
                            dbTotDuree = 0;
                            bAddInter = false;



                            if (bSimulation == false)//tested
                            {
                                //=== Création de la fiche standard.
                                var NewRow = rsStandard.NewRow();
                                lnumfichestandard = 0;
                                SqlCommandBuilder cb1 = new SqlCommandBuilder(modAdorsStandard.SDArsAdo);
                                modAdorsStandard.SDArsAdo.SelectCommand = cb1.DataAdapter.SelectCommand;
                                modAdorsStandard.SDArsAdo.UpdateCommand = cb1.GetUpdateCommand();
                                SqlCommand insertCmd1 = (SqlCommand)cb1.GetInsertCommand().Clone();
                                insertCmd1.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                                insertCmd1.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                                modAdorsStandard.SDArsAdo.InsertCommand = insertCmd1;

                                modAdorsStandard.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                                {

                                    if (e.StatementType == StatementType.Insert)
                                    {

                                        if (lnumfichestandard == 0)
                                        {
                                            lnumfichestandard =
                                               Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());

                                        }


                                    }
                                });

                                NewRow["NumFicheStandard"] = 0;
                                NewRow["Utilisateur"] = General.fncUserName();
                                //rsStandard!DateAjout = !DatePrevue 'Date1JourSemaine("Dimanche", "01/" & RecupValeurInDbcombo & "/" & txtannee)
                                NewRow["DateAjout"] = dtLot;
                                NewRow["CodeImmeuble"] = rstmpRow["CodeImmeuble"] + "";
                                NewRow["CodeOrigine1"] = "6";
                                NewRow["CodeOrigine2"] = "15";
                                NewRow["Source"] = "Prestation";
                                NewRow["Intervention"] = "1";
                                NewRow["AnalytiqueActivite"] = sAnalytiqueActivite;
                                //rsStandard!Intervention = "1"
                                NewRow["GMAO_Visite"] = 1;
                                NewRow["Lot"] = dtLot;
                                rsStandard.Rows.Add(NewRow.ItemArray);
                                modAdorsStandard.Update();
                                //cb1.Dispose();
                                //insertCmd1.Dispose();
                            }

                            //--creation de l'intervention
                            if (rs.Rows.Count > 0)//tested
                            {
                                modAdors.Update();
                            }

                            ajout:


                            SqlCommandBuilder cb2 = new SqlCommandBuilder(modAdors.SDArsAdo);
                            cb2.ConflictOption = ConflictOption.OverwriteChanges;
                            modAdors.SDArsAdo.SelectCommand = cb2.DataAdapter.SelectCommand;
                            modAdors.SDArsAdo.UpdateCommand = cb2.GetUpdateCommand();
                            SqlCommand insertCmd2 = (SqlCommand)cb2.GetInsertCommand().Clone();
                            insertCmd2.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                            insertCmd2.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                            modAdors.SDArsAdo.InsertCommand = insertCmd2;

                            var NewRow2 = rs.NewRow();
                            lNoIntervention = 0;

                            modAdors.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                            {

                                if (e.StatementType == StatementType.Insert)
                                {

                                    if (lNoIntervention == 0)
                                    {
                                        lNoIntervention = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                                        //NewRow2["NoIntervention"] = lNoIntervention;
                                        rs.Rows[rs.Rows.Count - 1]["NoIntervention"] = lNoIntervention;
                                    }



                                }

                            });
                            //                    rs.CancelUpdate


                            NewRow2["Numfichestandard"] = lnumfichestandard;
                            NewRow2["GMAO_Visite"] = 1;
                            NewRow2["VisiteEntretien"] = 1;
                            NewRow2["VisiteEntretienAuto"] = 1;
                            NewRow2["CreePar"] = General.fncUserName();
                            NewRow2["Utilisateur"] = General.fncUserName();
                            NewRow2["Lot"] = dtLot;
                            NewRow2["CreeLe"] = dtLot;
                            //rs!CreePar = fncUserName
                            NewRow2["CompteurObli"] = rstmpRow["CompteurObli"];
                            NewRow2["CompteurObliHisto"] = rstmpRow["CompteurObli"];
                            //=== modif du 26 07 2016 ajout du champ Rel_compteur.
                            NewRow2["Rel_compteur"] = rstmpRow["CompteurObli"];

                            NewRow2["CodeImmeuble"] = rstmpRow["CodeImmeuble"] + "";
                            NewRow2["CodeEtatPDA"] = ModPDA.cNonEnvoye;
                            //rs!secteur = fc_ADOlibelle("SELECT SEC_Code FROM IMMEUBLE WHERE codeimmeuble ='" & gFr_DoublerQuote(rstmp!CodeImmeuble) & "'")

                            //== à parametrer
                            NewRow2["TypeInterv"] = 0;

                            //rs!CYC_NoAuto = !CYC_NoAuto
                            NewRow2["COP_NoAuto"] = rstmpRow["COP_NoAuto"];
                            NewRow2["COP_NoContrat"] = rstmpRow["COP_NoContrat"];
                            NewRow2["Designation"] = sDesignation;
                            NewRow2["Article"] = cCodeVisite;

                            //rs!DatePrevue = !DatePrevue
                            //===modif du 04 05 2016
                            //rs!DateVisite = !DatePrevue
                            //=== modif du 23 01 2017.
                            if (bAddInter == true)
                            {
                                NewRow2["DateVisite"] = dDatePrevueTemp;
                                //bAddInter = False
                            }
                            else
                            {
                                NewRow2["DateVisite"] = dDatePrevue;
                            }

                            //rs!HeurePrevue = ""

                            NewRow2["Intervenant"] = rstmpRow["Intervenant"];
                            // rs!HeuredEbutP = #8:00:00 AM#
                            dtHeureDebP = Convert.ToDateTime(rstmpRow["HeureDebut"]);
                            NewRow2["heureDebutP"] = dtHeureDebP;

                            if (General.nz(rstmpRow["INT_ForceJournee"], 0).ToString() == "1" && bSimulation == false)
                            {
                                NewRow2["DateRealise"] = NewRow2["DateVisite"];
                                NewRow2["HeurePrevue"] = dtHeureDebP;
                            }

                            //== A modifier
                            NewRow2["PLE_Code"] = "INTP2";

                            //=== modif du 23 01 2016.
                            //rs!NoSemaine = !NoSemaine
                            NewRow2["NoSemaine"] = lNoSemaine;
                            NewRow2["INT_J"] = dDatePrevue.Day;
                            NewRow2["INT_M"] = dDatePrevue.Month;
                            NewRow2["INT_A"] = dDatePrevue.Year;
                            //--modif du 27 novembre 2003.
                            NewRow2["Designation"] = sDesignation;
                            NewRow2["Commentaire"] = "VISITE D'ENTRETIEN DU MOIS : " + dDatePrevue.Month;
                            NewRow2["dispatcheur"] = "8";
                            //rs!Datesaisie = !DatePrevue 'Date1JourSemaine("Dimanche", "01/" & RecupValeurInDbcombo & "/" & txtannee)
                            var zz = Convert.ToDateTime(Date1JourSemaine("Dimanche", Convert.ToDateTime("01/" + dDatePrevue.Month + "/" + dDatePrevue.Year)));
                            NewRow2["DateSaisie"] = Convert.ToDateTime(Date1JourSemaine("Dimanche", Convert.ToDateTime("01/" + dDatePrevue.Month + "/" + dDatePrevue.Year)));


                            NewRow2["CodeEtat"] = "00";
                            //=== modif du 07 02 2016, si il existe plusieurs localisations
                            //=== pour un même immeuble; ojn ne crée qu'une seule intervention.
                            if (General.sPriseEnCompteLocalP2 == "1")
                            {
                                //rs!PDAB_Noauto = rstmp!PDAB_Noauto
                                //rs!PDAB_Libelle = rstmp!PDAB_Libelle
                                //rs!PDAB_IDBarre = rstmp!PDAB_IDBarre
                            }

                            //===champs indiquant que cette visite est crée à destination du PDA.
                            NewRow2["VisitePDA"] = 1;

                            rs.Rows.Add(NewRow2.ItemArray);
                            modAdors.Update();

                            // rs.Rows[0]["NoInter"] = ModPDA.TypeInterPdaPreventif + lNoInterventionVisite; //Modifier par la requete ci-dessus
                            //using (ModAdo tmpAdo = new ModAdo())
                            //    lNoIntervention = Convert.ToInt32(tmpAdo.fc_ADOlibelle("select max(NoIntervention) from "+ sTablInt));
                            //reqModAdorsUpdate = $"Update " + sTablInt + " set NoInter ='" + ModPDA.TypeInterPdaPreventif + lNoIntervention + "' where NoIntervention=" + lNoIntervention;
                            //int xx=General.Execute(reqModAdorsUpdate);
                            rs.Rows[rs.Rows.Count - 1]["NoInter"] = ModPDA.TypeInterPdaPreventif + lNoIntervention;
                            modAdors.Update();
                        }

                        if (Convert.ToDateTime(rstmpRow["DatePrevue"]) == Convert.ToDateTime("02/07/2007"))
                        {
                            //Debug.Print("");
                        }

                        //==== modif du 23 01 2017, si la durée totale dépasse 8 h on crée une nouvelle intervention
                        //==== pour le lendemain.
                        if (bAddInter == false)
                        {
                            dbTotDuree = dbTotDuree + Convert.ToDouble(General.nz(rstmpRow["Duree"], 0));
                        }
                        else if (bAddInter == true)
                        {

                        }

                        //                            If bAddInter = True Then
                        //
                        //                                    bAddInter = False
                        //
                        //                                    If dbTotDuree > 8 Then
                        //                                            dbTotDuree = nz(rstmp!Duree, 0) - 8
                        //                                            dbTotDuree = Abs(dbTotDuree)
                        //                                            bAddInter = True
                        //
                        //
                        //                                            rs.Update
                        //
                        //                                            dDatePrevueTemp = DateAdd("d", 1, dDatePrevueTemp)
                        //                                            dDatePrevueTemp = fc_DateAddControleV2(dDatePrevueTemp, 1, 1, 1, 0, 1)
                        //
                        //                                            lNoSemaine = DatePart("ww", dDatePrevue, vbMonday, vbFirstFullWeek)
                        //
                        //                                            lINT_A = nz(!INT_Annee, 0)
                        //                                            GoTo ajout
                        //                                    Else
                        //                                            rs!DureeP = nz(rs!DureeP, 0) + dbTotDuree
                        //                                            rs!heureFinP = Format(DateAdd("s", rs!DureeP * 3600, rs!heureDebutP), "hh:mm:ss")
                        //                                            rs.Update
                        //                                            GoTo ajout
                        //                                    End If
                        //                            ElseIf dbTotDuree > 8 And nz(rstmp!Duree, 0) <= 8 Then
                        //                                         rs.Update
                        //
                        //                                        dbTotDuree = nz(rstmp!Duree, 0) - 8
                        //                                        dDatePrevueTemp = DateAdd("d", 1, dDatePrevue)
                        //                                        dDatePrevueTemp = fc_DateAddControleV2(dDatePrevueTemp, 1, 1, 1, 0, 1)
                        //
                        //                                        lNoSemaine = DatePart("ww", dDatePrevue, vbMonday, vbFirstFullWeek)
                        //
                        //                                        lINT_A = nz(!INT_Annee, 0)
                        //                                        bAddInter = True
                        //
                        //                                        GoTo ajout
                        //                            ElseIf nz(rstmp!Duree, 0) > 8 Then
                        //                                        dbTotDuree = nz(rstmp!Duree, 0) - 8
                        //                                        dbTotDuree = Abs(dbTotDuree)
                        //                                        rs!DureeP = nz(rs!DureeP, 0) + 8
                        //                                        rs!heureFinP = Format(DateAdd("s", rs!DureeP * 3600, rs!heureDebutP), "hh:mm:ss")
                        //                                        bAddInter = True
                        //                                        rs.Update
                        //
                        //                                        dDatePrevueTemp = DateAdd("d", 1, dDatePrevue)
                        //                                        dDatePrevueTemp = fc_DateAddControleV2(dDatePrevueTemp, 1, 1, 1, 0, 1)
                        //
                        //                                        lNoSemaine = DatePart("ww", dDatePrevue, vbMonday, vbFirstFullWeek)
                        //
                        //                                        lINT_A = nz(!INT_Annee, 0)
                        //                                        GoTo ajout
                        //                            Else



                        //Todo dans le cas de bSimulation!=true rstmpRow["Duree"] de type float et NewRow2["DureeP"] de type dateTime
                        if (bSimulation == true)
                        {
                            //DureeP
                            rs.Rows[rs.Rows.Count - 1]["DureeP"] = Convert.ToDouble(General.nz(rs.Rows[rs.Rows.Count - 1]["DureeP"], 0)) + Convert.ToDouble(General.nz(rstmpRow["Duree"], 0));

                            //heureFinP
                            rs.Rows[rs.Rows.Count - 1]["heureFinP"] = Convert.ToDateTime(rs.Rows[rs.Rows.Count - 1]["heureDebutP"]).AddSeconds(Convert.ToDouble(rs.Rows[rs.Rows.Count - 1]["DureeP"]) * 3600);

                        }
                        else
                        {

                            DateTime dt = DateTime.FromOADate(Convert.ToDouble(General.nz(rstmpRow["Duree"], 0)));
                            if (string.IsNullOrEmpty(rs.Rows[rs.Rows.Count - 1]["DureeP"].ToString()))
                            {
                                rs.Rows[rs.Rows.Count - 1]["DureeP"] = dt;

                            }
                            else
                            {
                                DateTime dp = Convert.ToDateTime(rs.Rows[rs.Rows.Count - 1]["DureeP"]);
                                rs.Rows[rs.Rows.Count - 1]["DureeP"] = dp.AddHours(dt.Hour).AddMinutes(dt.Minute).AddSeconds(dt.Second);

                            }


                            var ds = Convert.ToDateTime(rs.Rows[rs.Rows.Count - 1]["DureeP"]).ToOADate();
                            var s = ds * 3600;
                            rs.Rows[rs.Rows.Count - 1]["heureFinP"] = Convert.ToDateTime(rs.Rows[rs.Rows.Count - 1]["heureDebutP"]).AddSeconds(s);


                        }



                        //End If

                        ///' good rs!DureeP = nz(rs!DureeP, 0) + nz(rstmp!Duree, 0)



                        //rs!Duree = (nz(rs!DureeP, 0)) / 24
                        //rs!Duree = Format(DateAdd("s", rs!DureeP * 3600, #12:00:00 AM#), "hh:mm:ss")

                        //rs!HeurefinP = DateAdd("n", CDbl(nz(rs!DureeP, 0)) * 60, rs!HeuredEbutP)
                        ///'' good rs!heureFinP = Format(DateAdd("s", rs!DureeP * 3600, rs!heureDebutP), "hh:mm:ss")


                        if (!string.IsNullOrEmpty(General.nz(rstmpRow["GAI_LIbelle"], "").ToString()))
                        {
                            rs.Rows[rs.Rows.Count - 1]["Commentaire"] = General.Left(rs.Rows[rs.Rows.Count - 1]["Commentaire"] + "\n" + "--" + rstmpRow["GAI_LIbelle"], 500);
                        }


                        //modAdors.Update();
                        //reqModAdorsUpdate = $"Update " + sTablInt + " set  DureeP='" + rs.Rows[0]["DureeP"] + "'"
                        //    + ", heureFinP='" + rs.Rows[0]["heureFinP"] + "'"
                        //    + ", Commentaire='" + rs.Rows[0]["Commentaire"].ToString().Replace("'","''") + "'"
                        //     + "  where  NoIntervention=" + lNoIntervention;
                        //int yy= General.Execute(reqModAdorsUpdate);

                        //====met à jour la relation entre interventionP2 et operationP2.
                        if (bSimulation == true)
                        {
                            rstmpRow["NoInterventionVisite"] = lNoIntervention;
                            //rstmp!NumFicheStandard = lnumfichestandard
                        }
                        else
                        {
                            //rstmp!NoInterventionVisite = lNoInterventionVisite
                            rstmpRow["Numfichestandard"] = lnumfichestandard;
                            rstmpRow["NoInterventionVisite"] = 0;
                        }
                        General.modAdorstmp.Update();

                        System.Windows.Forms.Application.DoEvents();

                        //cumul les durées.
                        //dbDuree = dbDuree + nz(!Duree, 0)
                        //rs.MoveLast
                        //rs!DureeP = dbDuree
                        //rs.Update

                        // modAdors.Update();
                        // rowRs = NewRow2;
                    }



                    if (rs.Rows.Count > 0)//tested
                    {
                        modAdors.Update();
                    }


                    //                '==== CODE POUR DUPLIQUER INTER=================================
                    //                '=== ne pas oublmier la date.
                    //  rs.Filter = " DUREEP > 8
                    if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() && DateTime.Today.ToString() == "03/06/2017")
                    {
                        General.sEtaleJournee = "1";
                    }
                    if (General.sEtaleJournee == "1")
                    {
                        modAdors.Close();

                        General.sSQL = "SELECT * FROM " + sTablInt + " WHERE lot ='" + dtLot + "' and CreePar ='" + General.fncUserName() + "' and DureeP  > 8";

                        modAdors = new ModAdo();

                        rs = modAdors.fc_OpenRecordSet(General.sSQL);
                        //rs.MoveFirst
                        xJ = 0;


                        foreach (DataRow rsRow in rs.Rows)
                        {
                            dbJour = Convert.ToDouble(General.nz(rsRow["DureeP"], 0));
                            dbJour = dbJour / 8 + 0.5;

                            lNbJour = Convert.ToInt32(dbJour.ToString("00"));
                            //lNbJour = lNbJour - 1
                            dbDureeBase = Convert.ToDouble(General.nz(rsRow["DureeP"], 0));
                            dtHeureDebut = Convert.ToDateTime(rsRow["heureDebutP"]);
                            dDatePrevueTemp = Convert.ToDateTime(rsRow["DateVisite"]);
                            bFirst = true;

                            for (xJ = 0; xJ <= lNbJour - 1; xJ++)
                            {
                                if (bFirst == false)
                                {
                                    //' lignes suivantes à gérer.
                                    dDatePrevueTemp = dDatePrevueTemp.AddDays(1);
                                    dDatePrevueTemp = fc_DateAddControleV2(dDatePrevueTemp, 1, 1, 1, 0, 1);
                                    Calendar calendar = CultureInfo.InvariantCulture.Calendar;
                                    lNoSemaine = calendar.GetWeekOfYear(dDatePrevueTemp, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
                                }

                                lINT_J = dDatePrevueTemp.Day;
                                lINT_M = dDatePrevueTemp.Month;
                                lINT_A = dDatePrevueTemp.Year;

                                if (dbDureeBase > 8)
                                {
                                    dbDureeInt = 8;

                                    if (bFirst == true)
                                    {
                                        rsRow["DureeP"] = dbDureeInt;
                                        rsRow["heureFinP"] = dtHeureDebut.AddSeconds(dbDureeInt * 3600).ToString("HH:mm:ss");
                                        //                                            rs.Update

                                    }
                                }
                                else
                                {
                                    dbDureeInt = dbDureeBase;

                                    if (bFirst == true)
                                    {
                                        rsRow["DureeP"] = dbDureeInt;
                                        rsRow["heureFinP"] = dtHeureDebut.AddSeconds(dbDureeInt * 3600).ToString("HH:mm:ss");
                                        //                                                rs.Update

                                    }
                                }
                                if (bFirst == true)
                                {
                                    bFirst = false;
                                }
                                else
                                {
                                    dtHeureFin = Convert.ToDateTime(dtHeureDebut.AddSeconds(dbDureeInt * 3600).ToString("HH:mm:ss"));

                                    General.sSQL = "INSERT INTO " + sTablInt;
                                    General.sSQL = General.sSQL + "(NumFicheStandard, VisiteEntretien, VisiteEntretienAuto, CreePar ";
                                    General.sSQL = General.sSQL + ",GMAO_Visite";
                                    General.sSQL = General.sSQL + ", Utilisateur, Lot, CreeLe, CompteurObli";
                                    General.sSQL = General.sSQL + ", CompteurObliHisto, Rel_compteur, CodeImmeuble, CodeEtatPDA";
                                    General.sSQL = General.sSQL + ", TypeInterv, COP_NoAuto, COP_NoContrat, Designation";
                                    General.sSQL = General.sSQL + ", Article,  Intervenant, heureDebutP ";
                                    General.sSQL = General.sSQL + ", NoSemaine, INT_J, INT_M, INT_A";
                                    General.sSQL = General.sSQL + ", Commentaire, dispatcheur, DateSaisie";
                                    General.sSQL = General.sSQL + ", CodeEtat, VisitePDA, DureeP, heureFinP ";
                                    General.sSQL = General.sSQL + ", DateVisite, PLE_Code, NoInter";
                                    General.sSQL = General.sSQL + ")";


                                    General.sSQL = General.sSQL + " VALUES ";
                                    General.sSQL = General.sSQL + "(" + rsRow["Numfichestandard"] + ", " + rsRow["VisiteEntretien"] + "," + rsRow["VisiteEntretienAuto"] + ",'" + StdSQLchaine.gFr_DoublerQuote(rsRow["CreePar"].ToString()) + "'";
                                    General.sSQL = General.sSQL + "," + rsRow["GMAO_Visite"];
                                    General.sSQL = General.sSQL + ",'" + StdSQLchaine.gFr_DoublerQuote(rsRow["Utilisateur"].ToString()) + "','" + rsRow["Lot"] + "','" +
                                        rsRow["CreeLe"] + "'," + General.nz(rsRow["CompteurObli"], 0);
                                    General.sSQL = General.sSQL + "," + General.nz(rsRow["CompteurObliHisto"], 0) + "," + General.nz(rsRow["Rel_compteur"], 0) + ",'" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) +
                                        "','" + rsRow["CodeEtatPDA"] + "'";
                                    General.sSQL = General.sSQL + "," + rsRow["TypeInterv"] + "," + rsRow["COP_NoAuto"] + ",'" + rsRow["COP_NoContrat"] + "','" + StdSQLchaine.gFr_DoublerQuote(rsRow["Designation"].ToString()) + "'";
                                    General.sSQL = General.sSQL + ",'" + rsRow["Article"] + "','" + StdSQLchaine.gFr_DoublerQuote(rsRow["Intervenant"].ToString()) + "','" + Convert.ToDateTime(rsRow["heureDebutP"]).TimeOfDay + "'";
                                    General.sSQL = General.sSQL + "," + lNoSemaine + "," + lINT_J + "," + lINT_M + "," + lINT_A;
                                    General.sSQL = General.sSQL + ",'" + StdSQLchaine.gFr_DoublerQuote(rsRow["Commentaire"].ToString()) + "','" + rsRow["dispatcheur"] + "','" + rsRow["DateSaisie"] + "'";
                                    General.sSQL = General.sSQL + ",'" + rsRow["CodeEtat"] + "'," + rsRow["VisitePDA"] + "," + dbDureeInt + ",'" + dtHeureFin.TimeOfDay + "'";
                                    General.sSQL = General.sSQL + ",'" + dDatePrevueTemp + "','" + StdSQLchaine.gFr_DoublerQuote(rsRow["PLE_Code"].ToString()) + "','" + rsRow["NoInter"] + "'";
                                    General.sSQL = General.sSQL + ")";

                                    bFirst = false;

                                    General.Execute(General.sSQL);

                                }

                                dbDureeBase = dbDureeBase - 8;
                            }

                        }
                    }
                    //                '======================== FIN =======================================================

                    modAdors.Close();
                    General.modAdorstmp.Close();

                }


                // Print #iNoFic, "Réorganise : " & Now

                //=== limite les interventions à 8 heure maximun par jour,
                //=== modif du 26 aout, fonction non utilisé chez Gesten.

                //fc_OrganiseInter gsUtilisateur, dtLot, sTablInt, sTableOP, iNoFic

                General.rstmp = null;
                rs = null;

                //==== modif du 06 02 2014, beug constaté, parfois le champs compteur obligatoire ne se met
                //==== pas à jour  dans la table intervention, par sécurité on rebalaye
                //==== la table opération pour remettre à jour la base.
                //sSql = "SELECT  distinct   NoInterventionVisite, CompteurObli" _
                //& " FROM " & sTableOP _
                //& " WHERE lot ='" & dtLot & "'" _
                //& " AND Utilisateur = '" & fncUserName & "' AND CompteurObli = 1"
                // Set rs = fc_OpenRecordSet(sSql, 1)

                // Do While rs.EOF = False
                //         sSql = "UPDATE Intervention Set CompteurObli = 1 WHERE  NoIntervention = " & rs!NoInterventionVisite
                //         adocnn.Execute sSql
                //         rs.MoveNext
                // Loop

                // rs.Close
                rs = null;


                //=== modif du 15 06 2016, mise à jour du champs date realsié et heure prevue.
                if (sTableOP.ToUpper() != "OperationP2Simul".ToUpper())
                {
                    sSqlPrevue = "UPDATE    " + sTablInt + "  SET              DateRealise = " + sTablInt + ".DateVisite, HeurePrevue = " + sTablInt +
                        ".HeureDebutP " + " FROM         OperationP2 INNER JOIN " + sTablInt + "  ON " + sTableOP + ".NumFicheStandard = " + sTablInt +
                        ".NumFicheStandard " + sWhereInt + " and " + sTableOP + ".PEI_PassageObli = 1";

                    General.Execute(sSqlPrevue);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2.bas;fc_fusionneP2v2;");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDate"></param>
        /// <param name="lJourFeriee"></param>
        /// <param name="lPaques"></param>
        /// <param name="lPentecote"></param>
        /// <returns></returns>
        public static bool fc_CtrlJourFerie(ref System.DateTime dDate, int lJourFeriee = 0, int lPaques = 0, int lPentecote = 0)
        {
            bool functionReturnValue = false;
            int i = 0;

            functionReturnValue = false;

            if (lJourFeriee == 1)
            {

                for (i = 0; i <= modP2.tlJourFeriee.Length - 1; i++)
                {
                    if (dDate.Day == Convert.ToInt16(modP2.tlJourFeriee[i][0]) && dDate.Month == Convert.ToInt16(modP2.tlJourFeriee[i][1]))
                    {
                        dDate = dDate.AddDays(1);
                        functionReturnValue = true;
                        return functionReturnValue;
                    }
                }
            }


            //=== si lPaques est à 1, on ne prends pas en compte paques.
            if (lPaques == 1)
            {
                if (modP2.fPaques(dDate) == true)
                {
                    dDate = dDate.AddDays(1);
                    functionReturnValue = true;
                }
            }

            if (lPentecote == 1)
            {
                if (fPentecote(dDate) == true)
                {
                    dDate = dDate.AddDays(1);
                    functionReturnValue = true;
                }
            }
            return functionReturnValue;


        }


        public static bool fPentecote(DateTime dDate)
        {
            bool functionReturnValue = false;
            System.DateTime Paques = default(System.DateTime);
            System.DateTime LunPaq = default(System.DateTime);
            System.DateTime Ascension = default(System.DateTime);
            System.DateTime LunPent = default(System.DateTime);
            //Calcule le jour de Pâques en fonction
            //de l'année

            int a = 0;
            int b = 0;
            int C = 0;
            int d = 0;
            int e = 0;
            int f = 0;
            int g = 0;
            int h = 0;
            int i = 0;
            int j = 0;
            int k = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int p = 0;
            int An = 0;

            An = dDate.Year;
            a = An % 19;
            b = An / 100;
            C = An % 100;
            d = b / 4;
            e = b % 4;
            f = (b + 8) / 25;
            g = (b - f + 1) / 3;
            h = (19 * a + b - d - g + 15) % 30;
            i = C / 4;
            k = C % 4;
            l = (32 + 2 * e + 2 * i - h - k) % 7;
            m = (a + 11 * h + 22 * l) / 451;
            n = (h + l - 7 * m + 114) / 31;
            p = (h + l - 7 * m + 114) % 31;
            modP2.tabDPaque = new DateTime[3];



            //ReDim tabDPaque(1)


            Paques = new DateTime(An, n, p + 1);
            LunPaq = DateTime.FromOADate(Paques.ToOADate() + 1);
            //En déduit

            Ascension = DateTime.FromOADate(Paques.ToOADate() + 39);
            //jours fériés

            LunPent = DateTime.FromOADate(Paques.ToOADate() + 50);


            if (dDate == LunPent)
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }
            return functionReturnValue;
            //Paques = fPaques(An%)   'Cherche le jour de Pâques

        }

        public static string Date1JourSemaine(string strJour, System.DateTime dtDate)
        {
            string strJourBis = null;
            strJourBis = dtDate.ToString("dddd");
            while (strJour.ToUpper() != strJourBis.ToUpper())
            {
                dtDate = dtDate.AddDays(1);
                strJourBis = dtDate.ToString("dddd");
            }
            return Convert.ToString(dtDate);
        }

        public static string fc_IdAnomalie(string sLibelle)
        {
            string functionReturnValue = null;
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sSQL = "SELECT ANO_ID " + " From ANO_AnomalieGmao WHERE ANO_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(sLibelle) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                functionReturnValue = sSQL;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2V2;fc_IdAnomalie");
                return functionReturnValue;
            }
        }

        public static string fc_IdControle(string sLibelle)
        {
            string functionReturnValue = null;
            string sSQL = null;
            try
            {

                sSQL = "SELECT CTR_ID" + " From CTR_ControleGmao WHERE CTR_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(sLibelle) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                functionReturnValue = sSQL;
                return functionReturnValue;
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2V2;fc_IdControle");
                return functionReturnValue;
            }

        }

        public static string fc_IdMoyen(string sLibelle)
        {
            string functionReturnValue = null;
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sSQL = "SELECT MOY_ID" + " From MOY_MoyenGmao WHERE MOY_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(sLibelle) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                functionReturnValue = sSQL;
                return functionReturnValue;
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2V2;fc_IdMoyen");
                return functionReturnValue;
            }


        }

        public static string fc_IDOperation(string sLibelle)
        {
            string functionReturnValue = null;
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sSQL = "SELECT OPE_ID " + " From OPE_OperationGmao WHERE OPE_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(sLibelle) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                functionReturnValue = sSQL;
                return functionReturnValue;
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "modP2V2;fc_IDOperation");
                return functionReturnValue;
            }
        }

        public static void fc_MAJMotif(string sCodeImmeuble)
        {
            List<string> sTatut = null;
            int i = 0;
            bool bok = false;
            int lNewCAI_NoAuto = 0;
            int llCAI_NoAuto = 0;
            string sCAI_Libelle = null;
            DataTable rs = default(DataTable);
            DataTable rsDEl = default(DataTable);
            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                General.sSQL = "SELECT DISTINCT CAI_Code FROM ICC_IntervCategorieContrat"
                    + " INNER  JOIN  COP_CONTRATP2 on  ICC_IntervCategorieContrat.cop_NOAuto ="
                    + "  COP_CONTRATP2.COP_NoAuto " + " where (ICC_IntervCategorieContrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble)
                    + "'" + " and COP_Statut ='A') OR " + " (ICC_IntervCategorieContrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'"
                    + " and COP_Statut ='AR')";
                var tmpAdo = new ModAdo();
                General.rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                var _with4 = General.rstmp;

                //=== Stocke les codes des motifs trouvée dans les contrats liée à l'immeuble.
                if (_with4.Rows.Count > 0)
                {
                    //  _with4.MoveFirst();
                    foreach (DataRow rstmpRow in _with4.Rows)
                    {
                        if (rstmpRow["CAI_Code"] != null && !string.IsNullOrEmpty(rstmpRow["CAI_Code"].ToString()))
                        {
                            //Array.Resize(ref sTatut, i + 1);
                            sTatut[i] = General.nz(rstmpRow["CAI_Code"], "").ToString();
                            i = i + 1;
                        }
                        // _with4.MoveNext();
                    }
                }
                //  _with4.Close();

                //=== Ajoute les motifs généraux.
                General.sSQL = "SELECT CAI_CODE, CAI_Libelle FROM CAI_CategoriInterv WHERE TYM_CODE='GEN'";
                General.rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                var _with5 = General.rstmp;
                //=== Stocke les codes des motifs trouvée dans les contrats liée à l'immeuble.
                if (_with5.Rows.Count > 0)
                {
                    //  _with5.MoveFirst();
                    foreach (DataRow rstmpRow in _with5.Rows)
                    {
                        if (rstmpRow["CAI_Code"] != null && !string.IsNullOrEmpty(rstmpRow["CAI_Code"].ToString()))
                        {
                            //Array.Resize(ref sTatut, i + 1);
                            sTatut[i] = General.nz(rstmpRow["CAI_Code"], "").ToString();
                            i = i + 1;
                        }
                        // _with5.MoveNext();
                    }
                }
                // _with5.Close();


                //=== Recupére tous les code des motifs de l'immeuble.

                General.sSQL = "SELECT CAI_Code,CAI_NoAuto, CodeImmeuble,CAI_Libelle FROM ICA_ImmCategorieInterv " + " WHERE codeImmeuble='"
                    + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                General.rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                var _with6 = General.rstmp;

                //------AJOUT DES MOTIFS SI INEXISTANT DANS FICHE IMMEUBLE.
                for (i = 0; i <= sTatut.Count; i++)
                {
                    // _with6.MoveFirst();

                    bok = false;
                    if (_with6.Rows.Count > 0)
                    {
                        foreach (DataRow rstmpRow in _with4.Rows)
                        {
                            if (General.nz(rstmpRow["CAI_Code"], "").ToString().ToUpper() == sTatut[i].ToUpper())
                            {
                                bok = true;
                                break;
                            }
                            // _with6.MoveNext();
                        }
                    }

                    //--si code non présent on le crée.
                    if (bok == false)
                    {
                        sCAI_Libelle = tmpAdo.fc_ADOlibelle("SELECT CAI_Libelle FROM CAI_CategoriInterv WHERE CAI_CODE ='" + sTatut[i] + "'");

                        if (!string.IsNullOrEmpty(sCAI_Libelle))
                        {
                            var NewRow = General.rstmp.NewRow();
                            //_with6.AddNew();
                            NewRow["CAI_Code"] = sTatut[i];
                            NewRow["CodeImmeuble"] = sCodeImmeuble;
                            NewRow["CAI_Libelle"] = sCAI_Libelle;
                            General.rstmp.Rows.Add(NewRow);
                            tmpAdo.Update();

                            //=== Récupére le nouvel ID.
                            lNewCAI_NoAuto = Convert.ToInt16(NewRow["CAI_Noauto"]);

                            //=== Recupére l'ID du motif dans la table CAI_CategoriInterv.
                            llCAI_NoAuto = Convert.ToInt32(tmpAdo.fc_ADOlibelle("SELECT CAI_NoAuto FROM CAI_CategoriInterv WHERE CAI_CODE ='" + sTatut[i] + "'"));

                            //=== Supprime les éventuels doublons.
                            General.sSQL = "SELECT FacArticle.CodeArticle" + " FROM CAI_CategoriInterv INNER JOIN FacArticle "
                                + " ON CAI_CategoriInterv.CAI_Noauto = FacArticle.CAI_Noauto" + " WHERE (((CAI_CategoriInterv.CAI_Noauto)="
                                + llCAI_NoAuto + ")) ";
                            rs = tmpAdo.fc_OpenRecordSet(General.sSQL);
                            if (rs.Rows.Count > 0)
                                foreach (DataRow rsRow in rs.Rows)
                                {
                                    General.sSQL = "DELETE FROM IMA_ImmArticle where CODEIMMEUBLE ='" + sCodeImmeuble + "'"
                                                 + " and codearticle ='" + rsRow["CodeArticle"] + "'";
                                    General.Execute(General.sSQL);
                                    //rs.MoveNext();
                                }
                            //rs.Close();

                            rs = null;

                            //=== Ajoute les articles correspondant à la gamme.
                            ModModeleP2.fc_AddArticle(sCodeImmeuble, lNewCAI_NoAuto, llCAI_NoAuto);
                        }
                    }



                    //------SUPPRESSION DES MOTIFS SI INEXISTANT DANS CONTRATS.
                    //_with6.MoveFirst();

                    if (_with6.Rows.Count > 0)
                    {
                        foreach (DataRow rstmpRow in _with4.Rows)
                        {
                            bok = false;

                            for (i = 0; i <= sTatut.Count; i++)
                            {

                                if (General.nz(rstmpRow["CAI_Code"], "").ToString().ToUpper() == sTatut[i].ToString().ToUpper())
                                {
                                    bok = true;
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            if (bok == false)
                            {
                                //.Delete
                                rstmpRow.Delete();
                            }

                            // _with6.MoveNext();
                        }
                    }

                    return;
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sWhere"></param>
        /// <param name="lGroupe"></param>
        /// <param name="lab"></param>
        public static ParamGMAOx[] fc_LoadParamGMAO(string sWhere, int lGroupe, iTalk_TextBox_Small2 lab)
        {
            string sSQl = "";
            DataTable rs = null;
            ModAdo rsModAdo = new ModAdo();
            ParamGMAOx[] tGmao = null;
            string sPeriode = "";
            int i;
            string sPeriodeX = "";
            DateTime dtDeb;
            DateTime dtFin;
            string sDateTemp = "";
            int ltot = 0;
            int lcount;

            //=== lGroupe = 1 => goupé par intervenant.
            //=== lGroupe = 2 => goupé par immeuble.
            //=== lGroupe = 3 => goupé par chef de secteur.

            try
            {
                Theme.MainForm.Cursor = Cursors.WaitCursor;

                sSQl = "SELECT   PDAB_BadgePDA.PDAB_Libelle, GAI_GammeImm.GAI_LIbelle, GAI_GammeImm.GAI_Compteur,"
                    + " GAI_GammeImm.GAI_CompteurObli, ICC_IntervCategorieContrat.CAI_Libelle, "
                    + " ICC_IntervCategorieContrat.COP_Noauto, "
                    + " EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.CTR_Libelle, EQM_EquipementP2Imm.MOY_Libelle,"
                    + " EQM_EquipementP2Imm.ANO_Libelle, EQM_EquipementP2Imm.OPE_Libelle,"
                    + " EQM_EquipementP2Imm.CompteurObli, EQM_EquipementP2Imm.EQU_P1Compteur, PEI_PeriodeGammeImm.PEI_NoAuto, "
                    + " PEI_PeriodeGammeImm.TPP_Code, PEI_PeriodeGammeImm.PEI_DateDebut, PEI_PeriodeGammeImm.PEI_DateFin,"
                    + "PEI_PeriodeGammeImm.PEI_JD, PEI_PeriodeGammeImm.PEI_MD, PEI_PeriodeGammeImm.PEI_JF,"
                    + " PEI_PeriodeGammeImm.PEI_MF, PEI_PeriodeGammeImm.PEI_IntAutre, PEI_PeriodeGammeImm.PEI_Intervenant,"
                    + " PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel, PEI_PeriodeGammeImm.PEI_Visite,"
                    + " '' AS Total, EQM_EquipementP2Imm.EQM_NoAuto , Immeuble.CodeDepanneur,  PEI_PeriodeGammeImm.PEI_Cycle, Immeuble.CodeImmeuble "
                    + " , Immeuble.CodeChefSecteur, Immeuble.Code1 ";


                //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021, line below commented in this version
                //sSQl = sSQl + " FROM            GAI_GammeImm INNER JOIN"
                //            + " PDAB_BadgePDA ON GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto INNER JOIN "
                //            + " PEI_PeriodeGammeImm INNER JOIN "
                //            + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN"
                //            + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID INNER JOIN "
                //            + " Immeuble ON PDAB_BadgePDA.Codeimmeuble = Immeuble.CodeImmeuble ";

                //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 ===> Tested
                sSQl = sSQl + " FROM            GAI_GammeImm INNER JOIN"
                            + " PDAB_BadgePDA ON GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto INNER JOIN "
                            + " PEI_PeriodeGammeImm INNER JOIN "
                            + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                            + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID INNER JOIN "
                            + " Immeuble ON PDAB_BadgePDA.Codeimmeuble = Immeuble.CodeImmeuble INNER JOIN "
                            + " COP_ContratP2 ON GAI_GammeImm.COP_Noauto = COP_ContratP2.COP_NoAuto LEFT OUTER JOIN "
                            + " Contrat ON COP_ContratP2.NumContratRef = Contrat.NumContrat AND COP_ContratP2.AvenantRef = Contrat.Avenant";
                //===> Fin Modif Mondir

                //sWhere = " WHERE Immeuble.CodeImmeuble = '9HERBE'"

                //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
                sWhere = sWhere + " AND (Contrat.DateFin is null  or Contrat.DateFin < " + DateTime.Today.ToString("dd/MM/yyyy") + " )";
                //===> Fin Modif Mondir


                sSQl = sSQl + sWhere;

                sSQl = sSQl + " ORDER BY Immeuble.CodeImmeuble, EQM_EquipementP2Imm.EQM_NoAuto ";

                rs = rsModAdo.fc_OpenRecordSet(sSQl);

                Array.Resize(ref tGmao, 0);

                if (rs == null || rs.Rows.Count == 0)
                {
                    rsModAdo.Close();
                    return tGmao;
                }

                ltot = rs.Rows.Count;

                i = 0;
                lcount = 0;
                foreach (DataRow rsRow in rs.Rows)
                {
                    lcount = lcount + 1;
                    lab.Text = "phase 1/2 - " + lcount + "/" + ltot;
                    Application.DoEvents();
                    sPeriode = (rsRow["TPP_Code"] + "").ToUpper();

                    //=== date de début.
                    sDateTemp = rsRow["PEI_JD"] + "/" + rsRow["PEI_MD"] + "/" + DateTime.Now.Year;

                    //===> Tested First
                    if (sDateTemp.IsDate())
                    {
                        dtDeb = (rsRow["PEI_JD"] + "/" + rsRow["PEI_MD"] + "/" + DateTime.Now.Year).ToDate();
                    }
                    else if (General.Left(sDateTemp, 5) == "29/02" || General.Left(sDateTemp, 4) == "29/2")
                    {
                        dtDeb = ("28/2" + "/" + DateTime.Now.Year).ToDate();
                    }
                    else
                    {
                        continue;
                    }

                    //=== date de fin.
                    if (General.nz(rsRow["PEI_MF"], 0).ToDouble() < General.nz(rsRow["PEI_MD"], 0).ToDouble())
                    {
                        sDateTemp = rsRow["PEI_JF"] + "/" + rsRow["PEI_MF"] + "/" + (DateTime.Now.Year + 1);
                    }
                    else //===> Tested
                    {
                        sDateTemp = rsRow["PEI_JF"] + "/" + rsRow["PEI_MF"] + "/" + DateTime.Now.Year;
                    }

                    if (sDateTemp.IsDate())
                    {
                        dtFin = sDateTemp.ToDate();
                    }
                    else if (General.Left(sDateTemp, 5) == "29/02" || General.Left(sDateTemp, 4) == "29/2")
                    {
                        dtFin = ("28/2" + "/" + General.Right(sDateTemp, 4)).ToDate();
                    }
                    else
                    {
                        continue;
                    }

                    if (sPeriode == modP2.cHebdo.ToUpper())
                    {
                        sPeriodeX = cIDHebdo;
                    }
                    else if (sPeriode == modP2.cMensuel.ToUpper())
                    {
                        sPeriodeX = cIDMensuel;
                    }
                    else if (sPeriode == modP2.cBiMensuel.ToUpper())
                    {
                        sPeriodeX = cIDBiMensuel;
                    }
                    else if (sPeriode == modP2.cBiMestriel.ToUpper())
                    {
                        sPeriodeX = cIDBiMestriel;
                    }
                    else if (sPeriode == modP2.cTriMestriel.ToUpper())
                    {
                        sPeriodeX = cIDTriMestriel;
                    }
                    else if (sPeriode == modP2.cSemestriel.ToUpper())
                    {
                        sPeriodeX = cIDSemestriel;
                    }
                    else if (sPeriode == modP2.cQuotidien.ToUpper())
                    {
                        sPeriodeX = cIDQuotidien;
                    }
                    else if (sPeriode == modP2.cAnnuel.ToUpper())
                    {
                        sPeriodeX = cIDAnnuel;
                    }
                    else if (sPeriode == modP2.cCycleAnnuel.ToUpper())
                    {
                        sPeriodeX = cIDCycleAnnuel;
                    }
                    else if (sPeriode == modP2.cCycleJournee.ToUpper())
                    {
                        sPeriodeX = cIDCycleJournee;
                    }
                    else if (sPeriode == modP2.cCycleMensuel.ToUpper())
                    {
                        sPeriodeX = cIDCycleMensuel;
                    }
                    //Stop

                    if (sPeriode != "")
                    {
                        while (dtDeb <= dtFin)
                        {
                            Array.Resize(ref tGmao, i + 1);
                            tGmao[i].dbDuree = General.nz(rsRow["PEI_Duree"], 0).ToDouble();
                            tGmao[i].dtDebut = dtDeb;
                            tGmao[i].dtFin = dtFin;
                            tGmao[i].LidPEI_NoAuto = General.nz(rsRow["PEI_NoAuto"], 0).ToInt();
                            tGmao[i].lIntervAutre = General.nz(rsRow["PEI_IntAutre"], 0).ToInt();
                            tGmao[i].sPrestation = rsRow["GAI_LIbelle"] + ""; //& " " & rs!CAI_Libelle & ""
                            tGmao[i].sPeriodicite = sPeriodeX;
                            if (General.nz(rsRow["PEI_IntAutre"], 0).ToInt() == 0)
                            {
                                tGmao[i].sInterv = rsRow["CodeDepanneur"] + "";
                            }
                            else
                            {
                                tGmao[i].sInterv = rsRow["PEI_Intervenant"] + "";
                            }

                            tGmao[i].ljour = dtDeb.Day;
                            tGmao[i].lMois = dtDeb.Month;
                            tGmao[i].dtDate = dtDeb;
                            tGmao[i].lEQM_NoAuto = General.nz(rsRow["EQM_NoAuto"], 0).ToInt();
                            tGmao[i].sCodeImmeuble = rsRow["CodeImmeuble"] + "";
                            tGmao[i].sCodeChefSecteur = rsRow["CodeChefSecteur"] + "";

                            //===> Mondir le 17.02.2021 pour ajouter les modfis de la version V16.02.2021
                            tGmao[i].sLocalisation = rsRow["PDAB_Libelle"] + "";
                            //===> Fin Modif Mondir

                            if (sPeriode == modP2.cHebdo.ToUpper())
                            {
                                dtDeb = dtDeb.AddDays(7);
                            }
                            else if (sPeriode == modP2.cMensuel.ToUpper())
                            {
                                dtDeb = dtDeb.AddMonths(1);
                            }
                            else if (sPeriode == modP2.cBiMensuel.ToUpper())
                            {
                                dtDeb = dtDeb.AddDays(14);
                            }
                            else if (sPeriode == modP2.cBiMestriel.ToUpper())
                            {
                                dtDeb = dtDeb.AddMonths(2);
                            }
                            else if (sPeriode == modP2.cTriMestriel.ToUpper())
                            {
                                dtDeb = dtDeb.AddMonths(3);
                            }
                            else if (sPeriode == modP2.cSemestriel.ToUpper())
                            {
                                dtDeb = dtDeb.AddMonths(6);
                            }
                            else if (sPeriode == modP2.cAnnuel.ToUpper())
                            {
                                dtDeb = dtDeb.AddYears(1);
                            }
                            //=== a revoir pour le quotidien avec prise en compte du weekend.
                            else if (sPeriode == modP2.cQuotidien.ToUpper())
                            {
                                dtDeb = dtDeb.AddDays(1);
                            }
                            //=== voir comment gérer les cyles annuels.
                            else if (sPeriode == modP2.cCycleAnnuel.ToUpper())
                            {
                                dtDeb = dtDeb.AddYears(General.nz(rsRow["PEI_Cycle"], 1).ToInt());
                            }
                            else if (sPeriode == modP2.cCycleJournee.ToUpper())
                            {
                                sPeriodeX = cIDCycleJournee;
                                dtDeb = dtDeb.AddDays(General.nz(rsRow["PEI_Cycle"], 1).ToInt());
                            }
                            else if (sPeriode == modP2.cCycleMensuel.ToUpper())
                            {
                                dtDeb = dtDeb.AddMonths(General.nz(rsRow["PEI_Cycle"], 1).ToInt());
                            }
                            else
                            {
                                if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
                                {

                                }
                            }

                            i = i + 1;
                        }
                    }

                    SUITE:
                    continue;
                }

                if (lGroupe == 1)
                {
                    fc_GroupIntervenant(tGmao, lab);
                }
                else if (lGroupe == 2)
                {
                    fc_GroupImmeuble(tGmao, lab);
                }
                else if (lGroupe == 3)
                {
                    fc_GroupChefSecteur(tGmao, lab);
                }
                else if (lGroupe == 4)
                {
                    return tGmao;
                }

                Theme.MainForm.Cursor = Cursors.Default;

            }
            catch (Exception Ex)
            {
                Program.SaveException(Ex);
            }

            return tGmao;
        }

        /// <summary>
        /// ===> Mondir le 17.02.2021 pour ajouter les modfis de la version V16.02.2021
        /// </summary>
        /// <param name="tGmao"></param>
        /// <param name="lab"></param>
        /// <param name="bNotInsert"></param>
        /// <returns></returns>
        public static ParamGMAOw[] fc_GroupPrestation(ParamGMAOx[] tGmao, iTalk_TextBox_Small2 lab, bool bNotInsert = false)
        {
            int i;
            int X;
            int Y = 0;
            ParamGMAOw[] tpGoupPrest;
            bool BIn;
            string sUser = "";
            DateTime dtDate;
            int lCount = 0;
            int lTot;

            ParamGMAOw[] returnValue = null;

            try
            {
                //=== extrait les techniciens
                tpGoupPrest = new ParamGMAOw[1];

                lTot = tGmao.Length;

                for (i = 0; i < tGmao.Length; i++)
                {
                    lCount = lCount + 1;
                    lab.Text = "phase 2/2 - " + lCount + "/" + lTot;
                    Application.DoEvents();

                    BIn = false;
                    Theme.MainForm.Cursor = Cursors.WaitCursor;

                    for (X = 0; X < tpGoupPrest.Length; X++)
                    {
                        //If UCase(tpGoupPrest(X).LidPEI_NoAuto) = UCase(tGmao(i).LidPEI_NoAuto) Then
                        if (tpGoupPrest[X].lEQM_Noauto.ToString().ToUpper() ==
                            tGmao[i].lEQM_NoAuto.ToString().ToUpper())
                        {

                            BIn = true;
                            if (tGmao[i].lMois == 1)
                            {
                                tpGoupPrest[X].sJanvier = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurJanv = FncArrondir(tpGoupPrest(X).dbDurJanv + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 2)
                            {
                                tpGoupPrest[X].sFevrier = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurFev = FncArrondir(tpGoupPrest(X).dbDurFev + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 3)
                            {
                                tpGoupPrest[X].sMars = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurMars = FncArrondir(tpGoupPrest(X).dbDurMars + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 4)
                            {
                                tpGoupPrest[X].sAvril = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurAvril = FncArrondir(tpGoupPrest(X).dbDurAvril + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 5)
                            {
                                tpGoupPrest[X].sMai = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurMai = FncArrondir(tpGoupPrest(X).dbDurMai + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 6)
                            {
                                tpGoupPrest[X].sJuin = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurJuin = FncArrondir(tpGoupPrest(X).dbDurJuin + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 7)
                            {
                                tpGoupPrest[X].sJuillet = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurJuillet = FncArrondir(tpGoupPrest(X).dbDurJuillet + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 8)
                            {
                                tpGoupPrest[X].sAout = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurAout = FncArrondir(tpGoupPrest(X).dbDurAout + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 9)
                            {
                                tpGoupPrest[X].sSeptembre = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurSept = FncArrondir(tpGoupPrest(X).dbDurSept + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 10)
                            {
                                tpGoupPrest[X].sOctobre = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurOct = FncArrondir(tpGoupPrest(X).dbDurOct + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 11)
                            {
                                tpGoupPrest[X].sNovembre = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurNov = FncArrondir(tpGoupPrest(X).dbDurNov + tGmao(i).dbDuree, 2)
                            }
                            else if (tGmao[i].lMois == 12)
                            {
                                tpGoupPrest[X].sDecembre = tGmao[i].sPeriodicite;
                                //tpGoupPrest(X).dbDurDec = FncArrondir(tpGoupPrest(X).dbDurDec + tGmao(i).dbDuree, 2)
                            }
                            tpGoupPrest[Y - 1].lnbVisite = tpGoupPrest[Y - 1].lnbVisite + 1;
                            tpGoupPrest[Y - 1].dbDuree = tpGoupPrest[Y - 1].dbDuree + tGmao[i].dbDuree;
                        }
                    }

                    if (BIn == false)
                    {
                        Array.Resize(ref tpGoupPrest, Y + 1);

                        tpGoupPrest[Y].LidPEI_NoAuto = tGmao[i].LidPEI_NoAuto;
                        tpGoupPrest[Y].sPrestation = tGmao[i].sPrestation;

                        //=== > à faire voir avec xavier
                        tpGoupPrest[Y].sComplement = "";

                        tpGoupPrest[Y].sLocalisation = tGmao[i].sLocalisation;
                        tpGoupPrest[Y].sInterv = tGmao[i].sInterv;
                        tpGoupPrest[Y].lIntervAutre = tGmao[i].lIntervAutre;

                        //=== a faire
                        tpGoupPrest[Y].lnbVisite = tpGoupPrest[Y].lnbVisite + 1;

                        //== à faire
                        tpGoupPrest[Y].sRepartition = "";

                        //== faire
                        tpGoupPrest[Y].sCompteur = "";

                        //== à faire
                        tpGoupPrest[Y].sNoContrat = "";
                        //== à faire
                        tpGoupPrest[Y].sAvt = "";
                        //== à faire
                        tpGoupPrest[Y].sResilie = "";

                        //=== à faire
                        tpGoupPrest[Y].sJour = "";
                        //=== à faire
                        tpGoupPrest[Y].sSemaine = "";


                        //=== à faire
                        tpGoupPrest[Y].sHeure = "";

                        //=== à faire
                        tpGoupPrest[Y].sHorsWe = "";

                        //tpGoupPrest[Y].dbDuree = tGmao[i].dbDuree;
                        tpGoupPrest[Y].dbDuree = tpGoupPrest[Y].dbDuree + tGmao[i].dbDuree;
                        tpGoupPrest[Y].dtDebut = tGmao[i].dtDebut;
                        tpGoupPrest[Y].dtFin = tGmao[i].dtFin;
                        tpGoupPrest[Y].sPeriodicite = tGmao[i].sPeriodicite;
                        tpGoupPrest[Y].lMois = tGmao[i].lMois;
                        tpGoupPrest[Y].ljour = tGmao[i].ljour;
                        tpGoupPrest[Y].dtDate = tGmao[i].dtDate;
                        tpGoupPrest[Y].lEQM_Noauto = tGmao[i].lEQM_NoAuto;
                        tpGoupPrest[Y].sCodeimmeuble = tGmao[i].sCodeImmeuble;

                        Y = Y + 1;
                    }
                }

                if (bNotInsert == false)
                {
                    //'        sUser = fncUserName
                    //'        dtDate = Now
                    //    '
                    //'        sSQL = "Delete FROM Temp_GMAOChargeImmeuble where CreePar ='" & gFr_DoublerQuote(sUser) & "'"
                    //    '
                    //'        adocnn.Execute sSQL
                    //    '
                    //'        For Y = 0 To UBound(tpGoupPrest)
                    //    '
                    //'                sSQL = "INSERT INTO Temp_GMAOChargeImmeuble" _
                    //'                    & " ( Codeimmeuble, " _
                    //'                    & " JanvDuree, FevDuree, MarsDuree, AvrilDuree, MaiDuree, JuinDuree," _
                    //'                    & " JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree, DecDuree," _
                    //'                    & " CreePar, CreeLe)"
                    //    '
                    //'                sSQL = sSQL & "VALUES " _
                    //'                     & " ( '" & gFr_DoublerQuote(tpGoupPrest(Y).sCodeimmeuble) & "'," _
                    //'                     & " " & tpGoupPrest(Y).dbDurJanv & ", " & tpGoupPrest(Y).dbDurFev & ", " & tpGoupPrest(Y).dbDurMars & "," _
                    //'                     & tpGoupPrest(Y).dbDurAvril & ", " & tpGoupPrest(Y).dbDurMai & ", " & tpGoupPrest(Y).dbDurJuin & "," _
                    //'                     & tpGoupPrest(Y).dbDurJuillet & "," & tpGoupPrest(Y).dbDurAout & "," & tpGoupPrest(Y).dbDurSept & "," _
                    //'                     & tpGoupPrest(Y).dbDurOct & "," & tpGoupPrest(Y).dbDurNov & "," & tpGoupPrest(Y).dbDurDec & "," _
                    //'                     & "'" & gFr_DoublerQuote(sUser) & "', '" & dtDate & "')"
                    //'                adocnn.Execute sSQL
                    //'        Next
                }
                else
                {
                    returnValue = tpGoupPrest;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            Theme.MainForm.Cursor = Cursors.Arrow;
            return returnValue;
        }

        /// <summary>
        /// Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="tGmao"></param>
        /// <param name="lab"></param>
        public static void fc_GroupChefSecteur(ParamGMAOx[] tGmao, iTalk_TextBox_Small2 lab)
        {
            int i;
            int X;
            int Y = 0;
            GoupChefSecteur[] GoupChefSecteur;
            bool BIn;
            string sUser = "";
            DateTime dtDate;
            int lcount = 0;
            int ltot;

            try
            {
                //=== extrait les techniciens
                GoupChefSecteur = new GoupChefSecteur[1];
                ltot = tGmao.Length;

                for (i = 0; i < tGmao.Length; i++)
                {
                    lcount = lcount + 1;
                    lab.Text = "phase 2/2 - " + lcount + "/" + ltot;
                    Application.DoEvents();

                    BIn = false;
                    Theme.MainForm.Cursor = Cursors.WaitCursor;
                    for (X = 0; X < GoupChefSecteur.Length; X++)
                    {
                        if (GoupChefSecteur[X].sCodeChefSecteur?.ToUpper() == tGmao[i].sCodeChefSecteur.ToUpper())
                        {
                            BIn = true;
                            if (tGmao[i].lMois == 1)
                            {
                                GoupChefSecteur[X].dbDurJanv =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurJanv + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 2)
                            {
                                GoupChefSecteur[X].dbDurFev =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurFev + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 3)
                            {
                                GoupChefSecteur[X].dbDurMars =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurMars + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 4)
                            {
                                GoupChefSecteur[X].dbDurAvril =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurAvril + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 5)
                            {
                                GoupChefSecteur[X].dbDurMai =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurMai + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 6)
                            {
                                GoupChefSecteur[X].dbDurJuin =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurJuin + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 7)
                            {
                                GoupChefSecteur[X].dbDurJuillet =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurJuillet + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 8)
                            {
                                GoupChefSecteur[X].dbDurAout =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurAout + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 9)
                            {
                                GoupChefSecteur[X].dbDurSept =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurSept + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 10)
                            {
                                GoupChefSecteur[X].dbDurOct =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurOct + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 11)
                            {
                                GoupChefSecteur[X].dbDurNov =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurNov + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 12)
                            {
                                GoupChefSecteur[X].dbDurDec =
                                    General.FncArrondir(GoupChefSecteur[X].dbDurDec + tGmao[i].dbDuree, 2);
                            }
                        }
                    }

                    if (BIn == false)
                    {
                        Array.Resize(ref GoupChefSecteur, Y + 1);

                        GoupChefSecteur[Y].sCodeChefSecteur = tGmao[i].sCodeChefSecteur.ToUpper();

                        using (var tmpModAdo = new ModAdo())
                        {
                            GoupChefSecteur[Y].sNomChefSecteur = tmpModAdo.fc_ADOlibelle(
                                " SELECT TOP (1) Nom, Prenom " +
                                " From Personnel " +
                                " WHERE Matricule = '" + StdSQLchaine.gFr_DoublerQuote(tGmao[i].sCodeChefSecteur) + "'",
                                true);
                        }

                        if (tGmao[i].lMois == 1)
                        {
                            GoupChefSecteur[Y].dbDurJanv =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurJanv + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 2)
                        {
                            GoupChefSecteur[Y].dbDurFev =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurFev + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 3)
                        {
                            GoupChefSecteur[Y].dbDurMars =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurMars + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 4)
                        {
                            GoupChefSecteur[Y].dbDurAvril =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurAvril + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 5)
                        {
                            GoupChefSecteur[Y].dbDurMai =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurMai + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 6)
                        {
                            GoupChefSecteur[Y].dbDurJuin =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurJuin + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 7)
                        {
                            GoupChefSecteur[Y].dbDurJuillet =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurJuillet + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 8)
                        {
                            GoupChefSecteur[Y].dbDurAout =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurAout + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 9)
                        {
                            GoupChefSecteur[Y].dbDurSept =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurSept + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 10)
                        {
                            GoupChefSecteur[Y].dbDurOct =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurOct + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 11)
                        {
                            GoupChefSecteur[Y].dbDurNov =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurNov + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 12)
                        {
                            GoupChefSecteur[Y].dbDurDec =
                                General.FncArrondir(GoupChefSecteur[Y].dbDurDec + tGmao[i].dbDuree, 2);
                        }

                        Y = Y + 1;
                    }
                }

                sUser = General.fncUserName();
                dtDate = DateTime.Now;

                General.sSQL = "Delete FROM Temp_GMAOChargeChefSecteur where CreePar ='" + StdSQLchaine.gFr_DoublerQuote(sUser) + "'";

                General.Execute(General.sSQL);

                for (Y = 0; Y < GoupChefSecteur.Length; Y++)
                {
                    General.sSQL = "INSERT INTO Temp_GMAOChargeChefSecteur"
                                   + " ( CodeChefSecteur, NomChefSecteur ,"
                                   + " JanvDuree, FevDuree, MarsDuree, AvrilDuree, MaiDuree, JuinDuree,"
                                   + " JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree, DecDuree,"
                                   + " CreePar, CreeLe)";

                    General.sSQL = General.sSQL + "VALUES "
                        + " ( '" + StdSQLchaine.gFr_DoublerQuote(GoupChefSecteur[Y].sCodeChefSecteur) + "','" +
                        StdSQLchaine.gFr_DoublerQuote(GoupChefSecteur[Y].sNomChefSecteur) + "',"
                                                                              + " " + GoupChefSecteur[Y].dbDurJanv + ", " + GoupChefSecteur[Y].dbDurFev + ", " +
                                                                              GoupChefSecteur[Y].dbDurMars + ","
                                                                              + GoupChefSecteur[Y].dbDurAvril + ", " + GoupChefSecteur[Y].dbDurMai + ", " +
                                                                              GoupChefSecteur[Y].dbDurJuin + ","
                                                                              + GoupChefSecteur[Y].dbDurJuillet + "," + GoupChefSecteur[Y].dbDurAout + "," +
                                                                              GoupChefSecteur[Y].dbDurSept + ","
                                                                              + GoupChefSecteur[Y].dbDurOct + "," + GoupChefSecteur[Y].dbDurNov + "," +
                                                                              GoupChefSecteur[Y].dbDurDec + ","
                                                                              + "'" + StdSQLchaine.gFr_DoublerQuote(sUser) + "', '" + dtDate + "')";
                    General.Execute(General.sSQL);
                }

                Theme.MainForm.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="tGmao"></param>
        /// <param name="lab"></param>
        public static void fc_GroupIntervenant(ParamGMAOx[] tGmao, iTalk_TextBox_Small2 lab)
        {
            int i;
            int X;
            int Y = 0;
            GoupInterv[] TpGoupInterv;
            bool BIn;
            string sUser;
            DateTime dtDate;
            int lcount = 0;
            int ltot;

            try
            {
                Theme.MainForm.Cursor = Cursors.WaitCursor;
                //=== extrait les techniciens
                TpGoupInterv = new GoupInterv[1];

                ltot = tGmao.Length;
                for (i = 0; i < tGmao.Length; i++)
                {
                    lcount = lcount + 1;
                    lab.Text = "phase 2/2 - " + lcount + "/" + ltot;
                    Application.DoEvents();

                    BIn = false;
                    Theme.MainForm.Cursor = Cursors.WaitCursor;
                    for (X = 0; X < TpGoupInterv.Length; X++)
                    {
                        if (TpGoupInterv[X].sInterv?.ToUpper() == tGmao[i].sInterv.ToUpper())
                        {
                            BIn = true;
                            if (tGmao[i].lMois == 1)
                            {
                                TpGoupInterv[X].dbDurJanv =
                                    General.FncArrondir(TpGoupInterv[X].dbDurJanv + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 2)
                            {
                                TpGoupInterv[X].dbDurFev =
                                    General.FncArrondir(TpGoupInterv[X].dbDurFev + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 3)
                            {
                                TpGoupInterv[X].dbDurMars =
                                    General.FncArrondir(TpGoupInterv[X].dbDurMars + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 4)
                            {
                                TpGoupInterv[X].dbDurAvril =
                                    General.FncArrondir(TpGoupInterv[X].dbDurAvril + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 5)
                            {
                                TpGoupInterv[X].dbDurMai =
                                    General.FncArrondir(TpGoupInterv[X].dbDurMai + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 6)
                            {
                                TpGoupInterv[X].dbDurJuin =
                                    General.FncArrondir(TpGoupInterv[X].dbDurJuin + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 7)
                            {
                                TpGoupInterv[X].dbDurJuillet =
                                    General.FncArrondir(TpGoupInterv[X].dbDurJuillet + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 8)
                            {
                                TpGoupInterv[X].dbDurAout =
                                    General.FncArrondir(TpGoupInterv[X].dbDurAout + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 9)
                            {
                                TpGoupInterv[X].dbDurSept =
                                    General.FncArrondir(TpGoupInterv[X].dbDurSept + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 10)
                            {
                                TpGoupInterv[X].dbDurOct =
                                    General.FncArrondir(TpGoupInterv[X].dbDurOct + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 11)
                            {
                                TpGoupInterv[X].dbDurNov =
                                    General.FncArrondir(TpGoupInterv[X].dbDurNov + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 12)
                            {
                                TpGoupInterv[X].dbDurDec =
                                    General.FncArrondir(TpGoupInterv[X].dbDurDec + tGmao[i].dbDuree, 2);
                            }
                        }
                    }

                    if (BIn == false)
                    {
                        Array.Resize(ref TpGoupInterv, Y + 1);

                        TpGoupInterv[Y].sInterv = tGmao[i].sInterv.ToUpper();

                        using (var tmpModAdo = new ModAdo())
                        {
                            TpGoupInterv[Y].sNomInterv = tmpModAdo.fc_ADOlibelle(" SELECT TOP (1) Nom, Prenom "
                                                                                 + " From Personnel "
                                                                                 + " WHERE Matricule = '" +
                                                                                 StdSQLchaine.gFr_DoublerQuote(
                                                                                     tGmao[i].sInterv) +
                                                                                 "'", true);



                            TpGoupInterv[Y].sCodeChefSecteur = tGmao[i].sCodeChefSecteur.ToUpper();


                            TpGoupInterv[Y].sNomCodeChefSecteur = tmpModAdo.fc_ADOlibelle(" SELECT TOP (1) Nom, Prenom "
                                                                                          + " From Personnel "
                                                                                          + " WHERE Matricule = '" +
                                                                                          StdSQLchaine.gFr_DoublerQuote(
                                                                                              tGmao[i]
                                                                                                  .sCodeChefSecteur) +
                                                                                          "'", true);
                        }

                        if (tGmao[i].lMois == 1)
                        {
                            TpGoupInterv[Y].dbDurJanv =
                                General.FncArrondir(TpGoupInterv[Y].dbDurJanv + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 2)
                        {
                            TpGoupInterv[Y].dbDurFev =
                                General.FncArrondir(TpGoupInterv[Y].dbDurFev + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 3)
                        {
                            TpGoupInterv[Y].dbDurMars =
                                General.FncArrondir(TpGoupInterv[Y].dbDurMars + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 4)
                        {
                            TpGoupInterv[Y].dbDurAvril =
                                General.FncArrondir(TpGoupInterv[Y].dbDurAvril + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 5)
                        {
                            TpGoupInterv[Y].dbDurMai =
                                General.FncArrondir(TpGoupInterv[Y].dbDurMai + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 6)
                        {
                            TpGoupInterv[Y].dbDurJuin =
                                General.FncArrondir(TpGoupInterv[Y].dbDurJuin + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 7)
                        {
                            TpGoupInterv[Y].dbDurJuillet =
                                General.FncArrondir(TpGoupInterv[Y].dbDurJuillet + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 8)
                        {
                            TpGoupInterv[Y].dbDurAout =
                                General.FncArrondir(TpGoupInterv[Y].dbDurAout + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 9)
                        {
                            TpGoupInterv[Y].dbDurSept =
                                General.FncArrondir(TpGoupInterv[Y].dbDurSept + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 10)
                        {
                            TpGoupInterv[Y].dbDurOct =
                                General.FncArrondir(TpGoupInterv[Y].dbDurOct + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 11)
                        {
                            TpGoupInterv[Y].dbDurNov =
                                General.FncArrondir(TpGoupInterv[Y].dbDurNov + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 12)
                        {
                            TpGoupInterv[Y].dbDurDec =
                                General.FncArrondir(TpGoupInterv[Y].dbDurDec + tGmao[i].dbDuree, 2);
                        }

                        Y = Y + 1;
                    }
                }

                sUser = General.fncUserName();
                dtDate = DateTime.Now;

                General.sSQL = "Delete FROM Temp_GMAOChargeIntervenant where CreePar ='" +
                               StdSQLchaine.gFr_DoublerQuote(sUser) + "'";

                General.Execute(General.sSQL);

                for (Y = 0; Y < TpGoupInterv.Length; Y++)
                {
                    General.sSQL = "INSERT INTO Temp_GMAOChargeIntervenant"
                            + " ( Intervenant, NomIntervenant, ChefDeSecteur, NomChefDeSecteur,"
                            + " JanvDuree, FevDuree, MarsDuree, AvrilDuree, MaiDuree, JuinDuree,"
                            + " JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree, DecDuree,"
                            + " CreePar, CreeLe)";


                    General.sSQL = General.sSQL + "VALUES "
                                   + " ( '" + StdSQLchaine.gFr_DoublerQuote(TpGoupInterv[Y].sInterv) + "', '" +
                                   StdSQLchaine.gFr_DoublerQuote(TpGoupInterv[Y].sNomInterv) + "',"
                                   + " '" + StdSQLchaine.gFr_DoublerQuote(TpGoupInterv[Y].sCodeChefSecteur) + "', '" +
                                   StdSQLchaine.gFr_DoublerQuote(TpGoupInterv[Y].sNomCodeChefSecteur) + "',"
                                   + " " + TpGoupInterv[Y].dbDurJanv + ", " + TpGoupInterv[Y].dbDurFev + ", " +
                                   TpGoupInterv[Y].dbDurMars + ","
                                   + TpGoupInterv[Y].dbDurAvril + ", " + TpGoupInterv[Y].dbDurMai + ", " +
                                   TpGoupInterv[Y].dbDurJuin + ","
                                   + TpGoupInterv[Y].dbDurJuillet + "," + TpGoupInterv[Y].dbDurAout + "," +
                                   TpGoupInterv[Y].dbDurSept + ","
                                   + TpGoupInterv[Y].dbDurOct + "," + TpGoupInterv[Y].dbDurNov + "," +
                                   TpGoupInterv[Y].dbDurDec + ","
                                   + "'" + StdSQLchaine.gFr_DoublerQuote(sUser) + "', '" + dtDate + "')";
                    General.Execute(General.sSQL);
                }

                Theme.MainForm.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 change the return type from void to GoupImmeuble[] ===> Tested
        /// </summary>
        /// <param name="tGmao"></param>
        /// <param name="lab"></param>
        public static GoupImmeuble[] fc_GroupImmeuble(ParamGMAOx[] tGmao, iTalk_TextBox_Small2 lab, bool bNotInsert = false)
        {
            int i;
            int X;
            int Y = 0;
            GoupImmeuble[] TpGoupImmeuble = null;
            bool BIn;
            string sUser;
            DateTime dtDate;
            int lcount = 0;
            int ltot;

            try
            {
                //=== extrait les techniciens
                Array.Resize(ref TpGoupImmeuble, 0);

                ltot = tGmao?.Length ?? 0;

                for (i = 0; i < tGmao?.Length; i++)
                {

                    lcount = lcount + 1;
                    lab.Text = "phase 2/2 - " + lcount + "/" + ltot;
                    Application.DoEvents();

                    BIn = false;
                    Theme.MainForm.Cursor = Cursors.WaitCursor;
                    for (X = 0; X < TpGoupImmeuble.Length; X++)
                    {
                        if (TpGoupImmeuble[X].sCodeImmeuble?.ToUpper() == tGmao[i].sCodeImmeuble.ToUpper())
                        {
                            BIn = true;
                            if (tGmao[i].lMois == 1)
                            {
                                TpGoupImmeuble[X].dbDurJanv = General.FncArrondir(TpGoupImmeuble[X].dbDurJanv + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 2)
                            {
                                TpGoupImmeuble[X].dbDurFev = General.FncArrondir(TpGoupImmeuble[X].dbDurFev + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 3)
                            {
                                TpGoupImmeuble[X].dbDurMars = General.FncArrondir(TpGoupImmeuble[X].dbDurMars + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 4)
                            {
                                TpGoupImmeuble[X].dbDurAvril = General.FncArrondir(TpGoupImmeuble[X].dbDurAvril + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 5)
                            {
                                TpGoupImmeuble[X].dbDurMai = General.FncArrondir(TpGoupImmeuble[X].dbDurMai + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 6)
                            {
                                TpGoupImmeuble[X].dbDurJuin = General.FncArrondir(TpGoupImmeuble[X].dbDurJuin + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 7)
                            {
                                TpGoupImmeuble[X].dbDurJuillet = General.FncArrondir(TpGoupImmeuble[X].dbDurJuillet + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 8)
                            {
                                TpGoupImmeuble[X].dbDurAout = General.FncArrondir(TpGoupImmeuble[X].dbDurAout + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 9)
                            {
                                TpGoupImmeuble[X].dbDurSept = General.FncArrondir(TpGoupImmeuble[X].dbDurSept + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 10)
                            {
                                TpGoupImmeuble[X].dbDurOct = General.FncArrondir(TpGoupImmeuble[X].dbDurOct + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 11)
                            {
                                TpGoupImmeuble[X].dbDurNov = General.FncArrondir(TpGoupImmeuble[X].dbDurNov + tGmao[i].dbDuree, 2);
                            }
                            else if (tGmao[i].lMois == 12)
                            {
                                TpGoupImmeuble[X].dbDurDec = General.FncArrondir(TpGoupImmeuble[X].dbDurDec + tGmao[i].dbDuree, 2);
                            }
                        }
                    }

                    if (BIn == false)
                    {
                        Array.Resize(ref TpGoupImmeuble, Y + 1);

                        TpGoupImmeuble[Y].sCodeImmeuble = tGmao[i].sCodeImmeuble.ToUpper();

                        if (tGmao[i].lMois == 1)
                        {
                            TpGoupImmeuble[Y].dbDurJanv = General.FncArrondir(TpGoupImmeuble[Y].dbDurJanv + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 2)
                        {
                            TpGoupImmeuble[Y].dbDurFev = General.FncArrondir(TpGoupImmeuble[Y].dbDurFev + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 3)
                        {
                            TpGoupImmeuble[Y].dbDurMars = General.FncArrondir(TpGoupImmeuble[Y].dbDurMars + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 4)
                        {
                            TpGoupImmeuble[Y].dbDurAvril = General.FncArrondir(TpGoupImmeuble[Y].dbDurAvril + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 5)
                        {
                            TpGoupImmeuble[Y].dbDurMai = General.FncArrondir(TpGoupImmeuble[Y].dbDurMai + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 6)
                        {
                            TpGoupImmeuble[Y].dbDurJuin = General.FncArrondir(TpGoupImmeuble[Y].dbDurJuin + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 7)
                        {
                            TpGoupImmeuble[Y].dbDurJuillet = General.FncArrondir(TpGoupImmeuble[Y].dbDurJuillet + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 8)
                        {
                            TpGoupImmeuble[Y].dbDurAout = General.FncArrondir(TpGoupImmeuble[Y].dbDurAout + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 9)
                        {
                            TpGoupImmeuble[Y].dbDurSept = General.FncArrondir(TpGoupImmeuble[Y].dbDurSept + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 10)
                        {
                            TpGoupImmeuble[Y].dbDurOct = General.FncArrondir(TpGoupImmeuble[Y].dbDurOct + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 11)
                        {
                            TpGoupImmeuble[Y].dbDurNov = General.FncArrondir(TpGoupImmeuble[Y].dbDurNov + tGmao[i].dbDuree, 2);
                        }
                        else if (tGmao[i].lMois == 12)
                        {
                            TpGoupImmeuble[Y].dbDurDec = General.FncArrondir(TpGoupImmeuble[Y].dbDurDec + tGmao[i].dbDuree, 2);
                        }

                        Y = Y + 1;
                    }
                }

                //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 ===> Added if (bNotInsert == false) and else condtion
                if (bNotInsert == false)
                {
                    sUser = General.fncUserName();
                    dtDate = DateTime.Now;

                    General.sSQL = "Delete FROM Temp_GMAOChargeImmeuble where CreePar ='" +
                                   StdSQLchaine.gFr_DoublerQuote(sUser) + "'";

                    General.Execute(General.sSQL);

                    for (Y = 0; Y < TpGoupImmeuble.Length; Y++)
                    {
                        General.sSQL = "INSERT INTO Temp_GMAOChargeImmeuble"
                                       + " ( Codeimmeuble, "
                                       + " JanvDuree, FevDuree, MarsDuree, AvrilDuree, MaiDuree, JuinDuree,"
                                       + " JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree, DecDuree,"
                                       + " CreePar, CreeLe)";


                        General.sSQL = General.sSQL + "VALUES "
                                                    + " ( '" + StdSQLchaine.gFr_DoublerQuote(TpGoupImmeuble[Y]
                                                        .sCodeImmeuble) + "',"
                                                    + " " + TpGoupImmeuble[Y].dbDurJanv + ", " +
                                                    TpGoupImmeuble[Y].dbDurFev + ", " +
                                                    TpGoupImmeuble[Y].dbDurMars + ","
                                                    + TpGoupImmeuble[Y].dbDurAvril + ", " + TpGoupImmeuble[Y].dbDurMai +
                                                    ", " +
                                                    TpGoupImmeuble[Y].dbDurJuin + ","
                                                    + TpGoupImmeuble[Y].dbDurJuillet + "," +
                                                    TpGoupImmeuble[Y].dbDurAout + "," +
                                                    TpGoupImmeuble[Y].dbDurSept + ","
                                                    + TpGoupImmeuble[Y].dbDurOct + "," + TpGoupImmeuble[Y].dbDurNov +
                                                    "," +
                                                    TpGoupImmeuble[Y].dbDurDec + ","
                                                    + "'" + StdSQLchaine.gFr_DoublerQuote(sUser) + "', '" + dtDate +
                                                    "')";
                        General.Execute(General.sSQL);
                    }
                }
                else
                {
                    return TpGoupImmeuble;
                }

                Theme.MainForm.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return TpGoupImmeuble;
        }
    }
}
