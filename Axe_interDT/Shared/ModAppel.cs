﻿using System;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Shared
{
    class ModAppel
    {


        public const string cApiKey = "RzL836ON5ui2tDRvxt4g8NwsmsdJco/XNQsA/XEB4YvJ1g1sR7MfeY6G0NJ7VoxUYB5Tsbr9i1UeJUBPCoRBqA==";
        public const string cAdminTWS = "eExCV7BA";
        public const string cadminPSW = "notasecret";




        public const string Ctoken = "a:Guid";
        public const string cUser = "a:Username";
        public const string cDevice = "b:string";
        public const string cDomainGuid = "a:DomainGuid";
        public const string cResultMonitor = "TWS_WebStartMonitorResult";
        public const string cEvtMessageEvent = "eventType";
        public const string cMesAudit = "<CGenericEvent><EventType>AUDIT</EventType></CGenericEvent>82E51812-45C9-4733";//'& 0

        public static string spathAlgoria { get; set; }
        public static string spathAlgoriaFull { get; set; }
        public static string sImmeubleDefaut;
        public static string sCodeReglementDefContrat;
        public static string sNameFileAlgoria;
        public static string sTokenAlgoria;
        public static string sDeviceAlogoria { get; set; }
        public static string sUserAlogria;
        public static string[] sMessageAlgoria;
        public static string sDomainGuid;

        public const string cServAlgoria = "AZDT-CTI-01";
        public const string cPortAlgoria = "9000";

        public const string cAppelEntrant = "incoming";
        public const string cSimpleAppel = "simplecall";
        public const string cStatutDelivered = "delivered";

        public const string cidle = "idle";
        public const string cinitiated = "initiated";
        public const string coriginated = "originated";
        public const string cdelivered = "delivered";
        public const string cestablished = "established";
        public const string ccleared = "cleared";
        public const string cheld = "held";
        public const string cconferenced = "conferenced";
        public const string ctransfered = "transfered";
        public const string cdiverted = "diverted";
        public const string cqueued = "queued";
        public const string cretrieved = "retrieved";
        public const string cblocked = "blocked";
        public const string cpredelivered = "predelivered";
        public const string cdisconnected = "disconnected";
        public const string cnotified = "notified";
        public const string cfailed = "failed";
        public const string ccAll = "All";



        public static void fc_LogTWS()
        {
            string stemp;
            DateTime dt;
            string[] sTab = new string[] { };
            int X = 0;
            int i = 0;
            bool bIn;
            string path;
            try
            {
                if (Dossier.fc_ControleDossier(spathAlgoria) == false)
                {
                    Dossier.fc_ControleDossier(spathAlgoria);
                }
                //'MsgBox sPathAlgoria
                //'=== stocke les noms de dossier sur n jours.
                X = 0;
                for (i = 0; i < 4; i++)
                {
                    Array.Resize(ref sTab, X + 1);
                    dt = DateTime.Now.AddDays(-X);
                    string d = dt.Day.ToString();
                    string y = dt.Year.ToString();
                    string m = dt.Month.ToString();
                    stemp = dt.Year + "-" + dt.Month + "-" + d;
                    sTab[X] = stemp;
                    X = X + 1;
                }
                //'===  supprime les dossiers au delà de n jours.
                if (spathAlgoria != null)
                {
                    var fso = new DirectoryInfo(spathAlgoria);
                    var fd = fso.GetDirectories();

                    if (fd.Length > 0)
                    {
                        foreach (var direcetory in fd)
                        {
                            bIn = false;
                            for (X = 0; X <= sTab.Length - 1; X++)
                            {
                                if (General.UCase(direcetory.Name) == General.UCase(sTab[X]))
                                {
                                    bIn = true;
                                }
                            }
                            //'=== supprime les vieux dossiers.
                            if (bIn == false)
                            {
                                path = spathAlgoria + "\\" + direcetory.Name;
                                Directory.Delete(path, true);//delete all files 

                            }
                        }
                    }
                }

                //'=== créé le dossier log du jour.
                if (spathAlgoriaFull != null)
                {
                    Dossier.fc_ControleDossier(spathAlgoriaFull);
                }

                //'MsgBox sPathAlgoriaFull
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_LogTWS()");
            }
        }
        public static void fc_AddLog(string sTexte)
        {
            try
            {

                if (!Directory.Exists(spathAlgoriaFull))
                {
                    Directory.CreateDirectory(spathAlgoriaFull);
                }

                int iNumfichier = 0;

                //'=== Écrit le texte dans le fichier.
                File.AppendAllText(spathAlgoriaFull + "\\" + sNameFileAlgoria + ".txt", DateTime.Now.Year + " - " + DateTime.Now.Month + " - " + DateTime.Now.Day + "  " + DateTime.Now.Hour + ":" + DateTime.Now.Minute + ":" + DateTime.Now.Second + "====>" + sTexte);

            }
            catch (Exception ex)
            {
                //Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Fr_AjouterAuFichier " + spathAlgoriaFull, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


        }


    }
}
