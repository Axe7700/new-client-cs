﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Axe_interDT.Shared
{
    public static class modTt
    {
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sIDtomtom"></param>
        /// <returns></returns>
        public static string fc_IdTomtom(string sCodeImmeuble, string sIDtomtom)
        {
            string functionReturnValue = null;
            string sSql = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors;
            //=== met à jour l'id tom tom dans la table immeuble.

            try
            {
                if (string.IsNullOrEmpty(sIDtomtom))
                {
                    sSql = "select CodeImmeuble,LogTomTom, Noauto from immeuble " + " where codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSql);

                    if (rs.Rows.Count > 0)
                    {
                        if (string.IsNullOrEmpty(General.nz(rs.Rows[0]["LogTomTom"], "").ToString()))
                        {
                            rs.Rows[0]["LogTomTom"] = General.sTomtomAdr + rs.Rows[0]["Noauto"].ToString();
                            int xx = modAdors.Update();
                            functionReturnValue = General.sTomtomAdr + rs.Rows[0]["Noauto"];
                        }
                        else
                        {
                            functionReturnValue = rs.Rows[0]["LogTomTom"].ToString();
                        }
                        // Label25(10) = fc_IdTomtom
                    }

                    modAdors.Dispose();
                }
                else
                {
                    functionReturnValue = sIDtomtom;
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "modtt;fc_IdTomtom");
                return functionReturnValue;
            }
        }
    }
}
