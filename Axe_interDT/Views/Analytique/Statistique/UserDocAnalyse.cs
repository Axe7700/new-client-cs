﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.Analytique
{
    public partial class UserDocAnalyse : UserControl
    {
        public UserDocAnalyse()
        {
            InitializeComponent();
        }
        #region methodes
        /// <summary>
        /// TESTED
        /// </summary>
        private void FC_SaveParam()
        {
            General.saveInReg(this.Name, txtPeriodeDu.Name, txtPeriodeDu.Text);
            General.saveInReg(this.Name, txtPeriodeAu.Name, txtPeriodeAu.Text);
            General.saveInReg(this.Name, cmbIntervenant.Name, cmbIntervenant.Text);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_GetParam()
        {
            txtPeriodeDu.Text = General.getFrmReg(this.Name, "txtPeriodeDu", "");
            txtPeriodeAu.Text = General.getFrmReg(this.Name, "txtPeriodeAu", "");
            cmbIntervenant.Text = General.getFrmReg(this.Name, "cmbIntervenant", "");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlData()
        {
            bool functionReturnValue = false;
            string sSQL = null;
            //=== controle les données, retourne false en cas d'erreur.
            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                functionReturnValue = false;

                if (!General.IsDate(txtPeriodeDu.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date Période du est incorrecte.", "Données incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                if (!General.IsDate(txtPeriodeAu.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date Période au est incorrecte.", "Données incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'intervenant est obligatoire.", "Données incorrectes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlData");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="bExportExcel"></param>
        private void fc_LoadStat(bool bExportExcel)
        {
            string sWhere = null;
            string sSQL = null;
            string sSelectionFormula = null;
            string sReturn = null;
            DataTable rs = default(DataTable);
            DataTable rsAdd = default(DataTable);
            System.DateTime dtPeriodeDU = default(System.DateTime);
            System.DateTime dtPeriodeAU = default(System.DateTime);
            string sIntervenant = null;
            try
            {
                if (fc_CtrlData() == false)
                {
                    return;
                }

                FC_SaveParam();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                dtPeriodeDU = Convert.ToDateTime(txtPeriodeDu.Value);
                dtPeriodeAU = Convert.ToDateTime(txtPeriodeAu.Value);

                sWhere = "";
                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Intervenant" + (chkExclInterv.CheckState == CheckState.Unchecked ? "=" : "<>") + "'" + cmbIntervenant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.Intervenant" + (chkExclInterv.CheckState == CheckState.Unchecked ? "=" : "<>") + "'" + cmbIntervenant.Text + "'";
                    }
                }

                sIntervenant = cmbIntervenant.Text;
                sSQL = "SELECT     STAT_ID, STAT_Date, STAT_NbVisite00, STAT_NbVisite03, STAT_NbVisite04, STAT_NbVisiteAF, STAT_NbVisiteZZ, STAT_NbVisiteT, " + " STAT_PannesChEcs, STAT_PannesAstreinte, STAT_CreePar, STAT_Pannes00, STAT_Pannes03, STAT_Pannes04, STAT_PannesAF, STAT_PannesD," + " STAT_PannesZZ , STAT_PannesT, STAT_Devis04, STAT_DevisA, STAT_DevisT, STAT_Devis03, STAT_Devis00 " + " FROM         STAT_StatTechnicien " + " WHERE STAT_CreePar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var ModAdoToUpdate = new ModAdo();
                var ModAdo = new ModAdo();
                rsAdd = ModAdoToUpdate.fc_OpenRecordSet(sSQL);
                for (int i = 0; i < rsAdd.Rows.Count; i++)
                {
                    rsAdd.Rows[i].Delete();
                }
                var dr1 = rsAdd.NewRow();
                dr1["STAT_CreePar"] = General.fncUserName();
                dr1["STAT_Date"] = DateTime.Now;
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND VisiteEntretien = 1 AND CodeEtat = '00'" + " AND DateSaisie >='" + dtPeriodeDU + "' AND " + " DateSaisie <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);
                dr1["STAT_NbVisite00"] = General.nz(rs.Rows[0]["tot"], 0);

                //'=== nbre de visites d'entretien avec statut 03.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND  VisiteEntretien = 1 AND CodeEtat = '03'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);
                dr1["STAT_NbVisite03"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de visites d'entretien avec statut 04.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND  VisiteEntretien = 1 AND CodeEtat = '04'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_NbVisite04"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de visites d'entretien avec statut AF.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND  VisiteEntretien = 1 AND CodeEtat = 'AF'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_NbVisiteAF"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de visites d'entretien avec statut ZZ.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND  VisiteEntretien = 1 AND CodeEtat = 'ZZ'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_NbVisiteZZ"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de visites d'entretien avec facturé en T.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND  VisiteEntretien = 1 AND CodeEtat = 'T'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_NbVisiteT"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== Nbre de panne ECS et CH.
                sSQL = "SELECT     COUNT(Intervention.NoIntervention) AS tot" + " FROM         Intervention INNER JOIN " + " FacArticle ON Intervention.Article = FacArticle.CodeArticle " + sWhere + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'" + " AND (Article ='I25' OR Article ='I11' OR Article ='I10')";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesChEcs"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== Nbre de panne d'astreinte.
                sSQL = "SELECT     COUNT(Intervention.NoIntervention) AS tot" + " FROM         Intervention INNER JOIN" + " FacArticle ON Intervention.Article = FacArticle.CodeArticle INNER JOIN " + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard " + sWhere + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'" + " AND GestionStandard.Astreinte = 1";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesAstreinte"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes non effectué en 00.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = '00'" + " AND DateSaisie >='" + dtPeriodeDU + "' AND " + " DateSaisie <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_Pannes00"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut 03.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = '03'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_Pannes03"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut 04
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = '04'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_Pannes04"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut af.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = 'AF'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesAF"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut D.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = 'D'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesD"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut ZZ.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = 'ZZ'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesZZ"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbre de pannes avec statut T.
                sSQL = "SELECT     COUNT(NoIntervention) AS tot" + " From Intervention" + sWhere + " AND (VisiteEntretien = 0 or VisiteEntretien IS NULL)  " + " AND CodeEtat = 'T'" + " AND DateRealise >='" + dtPeriodeDU + "' AND " + " DateRealise <='" + dtPeriodeAU + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_PannesT"] = General.nz(rs.Rows[0]["tot"], 0);

                sSQL = "SELECT     COUNT(DevisEnTete.NumeroDevis) AS tot" + " FROM         DevisEnTete INNER JOIN " + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE     DevisEnTete.CodeEtat = '04' " + " AND DevisEnTete.DateImpression >='" + dtPeriodeDU + "'" + " AND DevisEnTete.DateImpression <= '" + dtPeriodeAU + "'" + " AND Immeuble.CodeDepanneur = '" + sIntervenant + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_Devis04"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== Nbres de devis en A (tous les devis).
                sSQL = "SELECT     COUNT(DevisEnTete.NumeroDevis) AS tot" + " FROM         DevisEnTete INNER JOIN " + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE     DevisEnTete.CodeEtat = '04' " + " AND Immeuble.CodeDepanneur = '" + sIntervenant + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_DevisA"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== Nbres de devis en T.
                sSQL = "SELECT     COUNT(DevisEnTete.NumeroDevis) AS tot" + " FROM         DevisEnTete INNER JOIN " + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE     DevisEnTete.CodeEtat = 'T' " + " AND DevisEnTete.DateImpression >='" + dtPeriodeDU + "'" + " AND DevisEnTete.DateImpression <= '" + dtPeriodeAU + "'" + " AND Immeuble.CodeDepanneur = '" + sIntervenant + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_DevisT"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== Nbres de devis en 03 (tous les devis).
                sSQL = "SELECT     COUNT(DevisEnTete.NumeroDevis) AS tot" + " FROM         DevisEnTete INNER JOIN " + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE     DevisEnTete.CodeEtat = '03' " + " AND Immeuble.CodeDepanneur = '" + sIntervenant + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);

                dr1["STAT_Devis03"] = General.nz(rs.Rows[0]["tot"], 0);

                //=== nbres de devis en 00.
                sSQL = "SELECT     COUNT(DevisEnTete.NumeroDevis) AS tot" + " FROM         DevisEnTete INNER JOIN " + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE     DevisEnTete.CodeEtat = '00' " + " AND DevisEnTete.DateImpression >='" + dtPeriodeDU + "'" + " AND DevisEnTete.DateImpression <= '" + dtPeriodeAU + "'" + " AND Immeuble.CodeDepanneur = '" + sIntervenant + "'";

                rs = ModAdo.fc_OpenRecordSet(sSQL);
                dr1["STAT_Devis00"] = General.nz(rs.Rows[0]["tot"], 0);
                rsAdd.Rows.Add(dr1);
                ModAdoToUpdate.Update();
                ModAdo.fc_CloseRecordset(rsAdd);
                rs = null;
                fc_exportExcel(bExportExcel);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadStat");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="bExportExcel"></param>
        private void fc_exportExcel(bool bExportExcel)
        {
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataColumn oField = default(DataColumn);

            DataTable rsExport = default(DataTable);

            string sSQL = null;
            string sAdd = null;

            int i = 0;
            try
            {
                sSQL = "SELECT STAT_NbVisite00 AS [Nombre de visite d'entretien en 00], STAT_NbVisite03 AS [Nombre de visite d'entretien en 03], " + " STAT_NbVisite04 AS [Nombre de visite d'entretien en 04], STAT_NbVisiteAF AS [Nombre de visite d'entretien en AF]," + " STAT_NbVisiteZZ AS [Nombre de visite d'entretien en ZZ], STAT_NbVisiteT AS [Nombre de visite d'entretien en T]," + " STAT_PannesChEcs AS [Pannes CH et ECS (articles I25-I11-I10)], STAT_PannesAstreinte AS [Pannes astreinte de nuit], STAT_Pannes00 AS [Interventions en 00]," + " STAT_Pannes03 AS [Interventions en 03], STAT_Pannes04 AS [Interventions en 04], STAT_PannesAF AS [Interventions en AF], STAT_PannesD AS [Interventions en D]," + " STAT_PannesZZ AS [Interventions en ZZ], STAT_PannesT AS [Interventions en T], STAT_Devis04 AS [Devis en 04 (Tous les devis)], STAT_DevisA AS [Devis en A]," + " STAT_DevisT AS [Devis en T], STAT_Devis03 AS [Tous les devis en 03 (tous les devis)], STAT_Devis00 AS [Devis en 00]" + " FROM         STAT_StatTechnicien " + " WHERE STAT_CreePar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
                var ModAdo = new ModAdo();
                rsExport = ModAdo.fc_OpenRecordSet(sSQL);

                ssGridStat.DataSource = null;
                DataTable dt = new DataTable();
                dt.Columns.Add("Titre");
                dt.Columns.Add("Valeur");
                foreach (DataRow dr in rsExport.Rows)
                {
                    foreach (DataColumn dc in rsExport.Columns)
                    {
                        dt.Rows.Add(dc.ColumnName, dr[dc.ColumnName]);
                    }
                }
                ssGridStat.DataSource = dt;
                if (bExportExcel == false)
                {
                    ModAdo.fc_CloseRecordset(rsExport);
                    rsExport = null;
                    return;
                }
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                //=== Titre du champs.
                oSheet.Cells[1, 1].value = "STATISTQUE DE L'INTERVENANT " + ModAdo.fc_ADOlibelle("SELECT Prenom, nom FROM personnel WHERE matricule ='" + cmbIntervenant.Text + "'", true) + " du " + txtPeriodeDu.Text + " au " + txtPeriodeAu.Text;
                oSheet.Cells[1, 1].Font.Bold = true;
                //=== affiche les noms des champs.
                i = 3;
                foreach (DataColumn dc in rsExport.Columns)
                {
                    oSheet.Cells[i, 1].value = dc.ColumnName;
                    i = i + 1;
                }
                Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                i = 3;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow dr in rsExport.Rows)
                {
                    foreach (DataColumn dc in rsExport.Columns)
                    {
                        oSheet.Cells[i, 2].value = dr[dc.ColumnName];
                        i = i + 1;
                    }
                }
                oResizeRange = oSheet.Range["A" + 3, "B" + i];
                oResizeRange.EntireColumn.AutoFit();

                oSheet.ListObjects.Add(Microsoft.Office.Interop.Excel.XlListObjectSourceType.xlSrcRange, oResizeRange, null, Microsoft.Office.Interop.Excel.XlYesNoGuess.xlNo).Name = "Tableau3";

                oSheet.ListObjects["Tableau3"].TableStyle = "TableStyleMedium12";

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                ModAdo.fc_CloseRecordset(rsExport);
                rsExport = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_exportExcel");
            }
        }

        #endregion

        #region events
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibIntervenant.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            General.sSQL = General.sSQL + " WHERE (NonActif is null or NonActif = 0) ";

            General.sSQL = General.sSQL + " order by matricule";

            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
        }

        

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyasci = (short)e.KeyChar;
            try
            {
                if (keyasci == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'"), "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string where = " (NonActif is null or NonActif = 0) ";
                        var Search = new SearchTemplate(null, null, req, where, "");
                        Search.SetValues(new Dictionary<string, string>() { { General.IsNumeric(cmbIntervenant.Text) ? "Matricule" : "Nom", cmbIntervenant.Text } });
                        Search.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbIntervenant.Text = Search.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibIntervenant.Text = Search.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            Search.Close();
                        };
                        Search.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                cmbIntervenant.Text = Search.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                lblLibIntervenant.Text = Search.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                Search.Close();
                            }

                        };
                        Search.ShowDialog();
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            txtPeriodeDu.Text = "";
            txtPeriodeAu.Text = "";
            cmbIntervenant.Text = "";
        }

       

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExecute_Click(object sender, EventArgs e)
        {
            try
            {
                fc_LoadStat(false);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdExecute_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportPlan_Click(object sender, EventArgs e)
        {
            fc_LoadStat(true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocAnalyse_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible) return;
            fc_GetParam();
            //View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocAnalyse");
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_ValueChanged(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibIntervenant.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
        }
        #endregion
    }
}
