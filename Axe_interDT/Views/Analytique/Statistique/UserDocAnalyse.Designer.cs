namespace Axe_interDT.Views.Analytique
{
    partial class UserDocAnalyse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGridStat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbIntervenant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtPeriodeDu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtPeriodeAu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label3 = new System.Windows.Forms.Label();
            this.chkExclInterv = new System.Windows.Forms.CheckBox();
            this.lblLibIntervenant = new System.Windows.Forms.Label();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdExportPlan = new System.Windows.Forms.Button();
            this.cmdExecute = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridStat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodeDu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodeAu)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ssGridStat, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ultraGroupBox1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 128F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ssGridStat
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridStat.DisplayLayout.Appearance = appearance1;
            this.ssGridStat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssGridStat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridStat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridStat.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridStat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridStat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridStat.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridStat.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridStat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridStat.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridStat.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridStat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridStat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridStat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridStat.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridStat.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridStat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridStat.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridStat.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridStat.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridStat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridStat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridStat.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridStat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridStat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridStat.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridStat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridStat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssGridStat.Location = new System.Drawing.Point(3, 131);
            this.ssGridStat.Name = "ssGridStat";
            this.ssGridStat.Size = new System.Drawing.Size(1844, 823);
            this.ssGridStat.TabIndex = 412;
            this.ssGridStat.Text = "ultraGrid1";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.tableLayoutPanel2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1844, 122);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1838, 116);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmbIntervenant, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtPeriodeDu, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtPeriodeAu, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.chkExclInterv, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblLibIntervenant, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdRechercheIntervenant, 2, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 50);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1832, 63);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(163, 31);
            this.label33.TabIndex = 384;
            this.label33.Text = "Période du";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(163, 32);
            this.label1.TabIndex = 384;
            this.label1.Text = "Intervenants";
            // 
            // cmbIntervenant
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenant.DisplayLayout.Appearance = appearance13;
            this.cmbIntervenant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbIntervenant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenant.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbIntervenant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenant.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbIntervenant.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbIntervenant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenant.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbIntervenant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenant.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbIntervenant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervenant.Location = new System.Drawing.Point(172, 34);
            this.cmbIntervenant.Name = "cmbIntervenant";
            this.cmbIntervenant.Size = new System.Drawing.Size(732, 27);
            this.cmbIntervenant.TabIndex = 506;
            this.cmbIntervenant.AfterCloseUp += new System.EventHandler(this.cmbIntervenant_AfterCloseUp);
            this.cmbIntervenant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervenant_BeforeDropDown);
            this.cmbIntervenant.ValueChanged += new System.EventHandler(this.cmbIntervenant_ValueChanged);
            this.cmbIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervenant_KeyPress);
            // 
            // txtPeriodeDu
            // 
            this.txtPeriodeDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPeriodeDu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriodeDu.Location = new System.Drawing.Point(172, 3);
            this.txtPeriodeDu.Name = "txtPeriodeDu";
            this.txtPeriodeDu.Size = new System.Drawing.Size(732, 26);
            this.txtPeriodeDu.TabIndex = 505;
            // 
            // txtPeriodeAu
            // 
            this.txtPeriodeAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPeriodeAu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPeriodeAu.Location = new System.Drawing.Point(1096, 3);
            this.txtPeriodeAu.Name = "txtPeriodeAu";
            this.txtPeriodeAu.Size = new System.Drawing.Size(733, 26);
            this.txtPeriodeAu.TabIndex = 505;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(941, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(149, 31);
            this.label3.TabIndex = 384;
            this.label3.Text = "Au";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // chkExclInterv
            // 
            this.chkExclInterv.AutoSize = true;
            this.chkExclInterv.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkExclInterv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclInterv.Location = new System.Drawing.Point(1096, 34);
            this.chkExclInterv.Name = "chkExclInterv";
            this.chkExclInterv.Size = new System.Drawing.Size(76, 26);
            this.chkExclInterv.TabIndex = 570;
            this.chkExclInterv.Text = "Exclure";
            this.chkExclInterv.UseVisualStyleBackColor = true;
            this.chkExclInterv.Visible = false;
            // 
            // lblLibIntervenant
            // 
            this.lblLibIntervenant.AutoSize = true;
            this.lblLibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibIntervenant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibIntervenant.Location = new System.Drawing.Point(941, 31);
            this.lblLibIntervenant.Name = "lblLibIntervenant";
            this.lblLibIntervenant.Size = new System.Drawing.Size(149, 32);
            this.lblLibIntervenant.TabIndex = 384;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRechercheIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(910, 34);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheIntervenant.TabIndex = 571;
            this.toolTip1.SetToolTip(this.cmdRechercheIntervenant, "Recherche de l\'intervenant");
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdExportPlan);
            this.flowLayoutPanel1.Controls.Add(this.cmdExecute);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1832, 41);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // cmdExportPlan
            // 
            this.cmdExportPlan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExportPlan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportPlan.FlatAppearance.BorderSize = 0;
            this.cmdExportPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportPlan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportPlan.ForeColor = System.Drawing.Color.White;
            this.cmdExportPlan.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExportPlan.Location = new System.Drawing.Point(1770, 2);
            this.cmdExportPlan.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportPlan.Name = "cmdExportPlan";
            this.cmdExportPlan.Size = new System.Drawing.Size(60, 35);
            this.cmdExportPlan.TabIndex = 394;
            this.toolTip1.SetToolTip(this.cmdExportPlan, "Exporter la sélection");
            this.cmdExportPlan.UseVisualStyleBackColor = false;
            this.cmdExportPlan.Click += new System.EventHandler(this.cmdExportPlan_Click);
            // 
            // cmdExecute
            // 
            this.cmdExecute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExecute.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExecute.FlatAppearance.BorderSize = 0;
            this.cmdExecute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExecute.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExecute.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdExecute.Location = new System.Drawing.Point(1706, 2);
            this.cmdExecute.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExecute.Name = "cmdExecute";
            this.cmdExecute.Size = new System.Drawing.Size(60, 35);
            this.cmdExecute.TabIndex = 395;
            this.cmdExecute.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExecute, "Rechercher");
            this.cmdExecute.UseVisualStyleBackColor = false;
            this.cmdExecute.Click += new System.EventHandler(this.cmdExecute_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Location = new System.Drawing.Point(1642, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(60, 35);
            this.cmdClean.TabIndex = 395;
            this.cmdClean.Tag = "UserDocAnalyse";
            this.cmdClean.Text = "Clean";
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // UserDocAnalyse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocAnalyse";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Statistique";
            this.VisibleChanged += new System.EventHandler(this.UserDocAnalyse_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridStat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodeDu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPeriodeAu)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenant;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtPeriodeDu;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtPeriodeAu;
        public System.Windows.Forms.CheckBox chkExclInterv;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdExportPlan;
        public System.Windows.Forms.Button cmdExecute;
        public System.Windows.Forms.Button cmdClean;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridStat;
        public System.Windows.Forms.Label lblLibIntervenant;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
