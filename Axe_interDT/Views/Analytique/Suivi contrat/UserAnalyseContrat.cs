﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using Axe_interDT.Shared;
using Axe_interDT.Views.Analytique.Forms;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using Microsoft.Office.Interop.Excel;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using DataTable = System.Data.DataTable;

namespace Axe_interDT.Views.Analytique
{
    public partial class UserAnalyseContrat : UserControl
    {
        public UserAnalyseContrat()
        {
            InitializeComponent();
        }

        #region variables
        const string cLimiteHtCtr = "2000";

        const string climitehtinf = "<" + cLimiteHtCtr + "€";
        const string cLimiteHtSup = ">" + cLimiteHtCtr + "€";

        const string cAccepte = "A";
        const string clibAccepte = "Contrats acceptés";
        const string cDetail = "Détail";
        const string cResilie = "R";
        const string clibResilie = "Contrats résiliés";

        string fmsg;

        bool blnErreur;

        const string cStyleResilie = "resilie";
        const int colorStyleResilie = 0xc0c0ff;

        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cLibArret = "Date d'arrêt";
        const short cCol = 0;

        const string cIMMEUBLE = "IMMEUBLE";
        const string cSYNDIC = "SYNDIC";
        const string cNo = "N°";
        const string cRC = "R.C.";
        const string cRE = "R.E.";
        const string cDATESIGNATURE = "DATE SIGNATURE";
        const string cDATERECEPTRESILIATION = "DATE RECEPTION RESILIATION";

        const string cDATEEFFECTIVE = "DATE EFFECTIVE";
        const string cEXERCICE = "EXERCICE";
        const string cINDICATEUR = "INDICATEUR ";
        const string cMONTANT = "MONTANT";
        const string cP1 = "P1";
        const string cP2 = "P2";
        const string cP3 = "P3";
        const string cP4 = "P4";
        const string cCommentaires = "COMMENTAIRES";
        const string cCAUSERENOVPERDUE = "CAUSE RENOV PERDUE";
        const string cRECONDUCTION = "RECONDUCTION";

        const string c2424 = "24/24";
        const string cRENOVATIONGAGNEE = "RENOVATION GAGNEE";

        const string cPosIMMEUBLE = "A";
        const string cPosSYNDIC = "B";
        const string cPosNo = "C";
        const string cPosRC = "D";
        const string cPosRE = "E";
        const string cPosDATESIGNATURE = "F";
        const string cPosDATEEFFECTIVE = "G";
        const string cPosEXERCICE = "H";
        const string cPosINDICATEUR = "I";
        const string cPosMONTANT = "J";
        const string cPosP1 = "K";
        const string cPosP2 = "L";
        const string cPosP3 = "M";
        const string cPosP4 = "N";
        const string cPosCommentaires = "O";
        const string cPos2424 = "P";
        const string cPosRENOVATIONGAGNEE = "Q";


        const string cPosResIMMEUBLE = "A";
        const string cPosResSYNDIC = "B";
        const string cPosResNo = "C";
        const string cPosResRC = "D";
        const string cPosResRE = "E";
        const string cPosResDATERECEPTIONRES = "F";
        const string cPosResDATEEFFECTIVE = "G";
        const string cPosResEXERCICE = "H";
        const string cPosResINDICATEUR = "I";
        const string cPosResMONTANT = "J";
        const string cPosResCommentaires = "K";
        const string cPosResCAUSERENOVPERDUE = "L";
        const string cPosResRECONDUCTION = "M";
        const string cResPosP1 = "N";
        const string cResPosP2 = "O";
        const string cResPosP3 = "P";
        const string cResPosP4 = "Q";


        const string cSheetContratAccepte = "Contrats acceptés";
        const string cSheetContratResilie = "Contrats résiliés";
        const string cShettACCPARRE = "ACC PAR EXPLOIT";
        const string cShettACCPARRC = "ACC PAR COMM";
        const string cShettRESILPARRE = "RESIL PAR EXPLOIT";
        const string cShettRESILPARRC = "RESIL PAR COMM";

        const string cSheetCompRE = "Comparatif RE";

        const short caddRow = 10;

        DataTable rsANCD;

        General.ExerciceCtr[] tpExerciceExe;

        System.Windows.Forms.PictureBox f;

        private ModAdo ModAdoGridUpdate;

        #endregion

        #region methodes

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private bool fc_controle()
        {
            bool functionReturnValue = false;
            DataTable rstmp = default(DataTable);
            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //=== Initialise le flag des erreurs.
                blnErreur = false;

                //=== test date
                if (!General.IsDate(txtDateArrete.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date Invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    functionReturnValue = true;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return functionReturnValue;
                }
                //=== controle si un calcul de P2 a déja été effectué pour cette date d'arret, si
                //=== oui demande confirmation pour écraser celle-ci.
                General.sSQL = "Select ANCE_DateArret from  ANCE_AnalyCtrEntete" + " where ANCE_DateArret ='" + txtDateArrete.Text + "'";
                var ModAdo = new ModAdo(); ;
                rstmp = ModAdo.fc_OpenRecordSet(General.sSQL);

                if (rstmp.Rows.Count > 0)
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention un arreté pour la date " + txtDateArrete.Text + " existe déjà. \r\nVoulez vous l'éffacer et en recréer un autre", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        FraArret.Visible = false;
                        for (int i = 0; i < rstmp.Rows.Count; i++)
                            rstmp.Rows[i].Delete();
                        //TODO . i added these lines to Delete all data when the answer is yes
                        string req = "select ANCE_Noauto from ANCE_AnalyCtrEntete where ANCE_DateArret='" + txtDateArrete.Text + "'";

                        int lANCE_Noauto = Convert.ToInt32(ModAdo.fc_ADOlibelle(req));

                        req = "DELETE FROM ANCDT_AnalyCtrData WHERE  ANCE_Noauto =" + lANCE_Noauto;
                        General.Execute(req);

                        req = "DELETE FROM ANCD_AnalyCtrDetail WHERE  ANCE_Noauto =" + lANCE_Noauto;
                        General.Execute(req);

                        req = "DELETE FROM ANCE_AnalyCtrEntete Where ANCE_Noauto =" + lANCE_Noauto;
                        General.Execute(req);
                        //                adocnn.Execute "DELETE FROM  AND_ANP2DETAIL" _
                        //'                    & " WHERE ANP_DateArret ='" & txtDateArrete & "'"
                    }
                    else
                    {
                        functionReturnValue = true;
                    }
                }

                //=== ouvre le fichier des erreurs
                try
                {
                    File.Delete(General.cCheminFichierCtrErr);
                }
                catch (Exception e) { Program.SaveException(e); }
                //=== ouvre le fichier des erreurs constatés.
                //FileSystem.FileOpen(1, General.cCheminFichierCtrErr, OpenMode.Output);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                fc_Err("", 0, "", 0, ex.Message);
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadTreev()
        {

            string sSQL = null;
            DataTable rs = default(DataTable);
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //==reinitialise le treview.
                if (TreeArret.Nodes.Count > 0)
                {
                    for (int i = 0; i < TreeArret.Nodes.Count; i++)
                    {
                        TreeArret.Nodes[i].Nodes.Clear();
                        TreeArret.Nodes[i].Remove();
                    }
                }
                TreeArret.Nodes.Add(cNiv1, cLibArret);
                TreeArret.Nodes.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                sSQL = "SELECT     ANCE_Noauto, ANCE_DateArret, ANCE_CreePar, ANCE_CreeLe, ANCE_Commentaire" + " From ANCE_AnalyCtrEntete " + " ORDER BY ANCE_DateArret DESC, ANCE_Noauto";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }
                foreach (DataRow dr in rs.Rows)
                {
                    if (!string.IsNullOrEmpty(General.nz(dr["ANCE_DateArret"], "") + ""))
                    {
                        //==Ajout des dates d'arrêt.
                        TreeArret.Nodes[0].Nodes.Add(cNiv2 + dr["ANCE_Noauto"], Convert.ToDateTime(dr["ANCE_DateArret"] + "").ToShortDateString());
                    }
                }
                TreeArret.Nodes[0].Nodes.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                //TreeArret.Nodes[1].Selected = true;
                TreeArret.Nodes[cNiv1].Expanded = true;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_loadTreePer");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="dtDateErret"></param>
        /// <param name="sCommentaire"></param>
        /// <returns></returns>
        public object fc_CalculCtr(System.DateTime dtDateErret, string sCommentaire)
        {
            object functionReturnValue = null;
            DataTable rsAdd = default(DataTable);
            DataTable rs = default(DataTable);

            string sSQL = null;
            string sLimiteHtInf = null;
            string sLimiteHtSup = null;
            string sCodeArticle = null;
            string sCodeImmeuble = null;
            string sNoContrat = null;
            string sCOT_Code = null;

            System.DateTime dteffet = default(System.DateTime);
            System.DateTime dtCreate = default(System.DateTime);
            System.DateTime dtDay = default(System.DateTime);

            int lANCE_Noauto = 0;
            int i = 0;
            int sAvenant = 0;

            double dbHT = 0;
            double dbHtP1 = 0;
            double dbHtP2 = 0;
            double dbHtP3 = 0;
            double dbHtP4 = 0;
            double dbIndex = 0;
            double dbTot = 0;

            bool bCtrResiliee = false;
            try
            {
                dtCreate = DateTime.Now;
                sLimiteHtInf = climitehtinf;
                sLimiteHtSup = cLimiteHtSup;


                //==== insertion de l'entete de l'analyse contrat.
                sSQL = "SELECT      ANCE_Noauto, ANCE_CreePar, ANCE_CreeLe, ANCE_DateArret, ANCE_Commentaire" + " From ANCE_AnalyCtrEntete " + " WHERE     (ANCE_Noauto = 0)";
                var ModAdoInsert = new ModAdo();
                rsAdd = ModAdoInsert.fc_OpenRecordSet(sSQL);
                var dr = rsAdd.NewRow();

                dr["ANCE_CreePar"] = General.fncUserName();
                dr["ANCE_CreeLe"] = dtCreate;
                dr["ANCE_DateArret"] = dtDateErret;
                dr["ANCE_Commentaire"] = General.nz(sCommentaire, System.DBNull.Value);
                SqlCommandBuilder cb = new SqlCommandBuilder(ModAdoInsert.SDArsAdo);
                ModAdoInsert.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                ModAdoInsert.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                ModAdoInsert.SDArsAdo.InsertCommand = insertCmd;

                ModAdoInsert.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {
                    if (e.StatementType == StatementType.Insert)
                    {
                        lANCE_Noauto = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                    }
                });
                rsAdd.Rows.Add(dr);
                ModAdoInsert.Update();

                Shared.ModAdo.fc_CloseRecordset(rsAdd);
                ModAdoInsert.SDArsAdo.Dispose();
                ModAdoInsert.SCBrsAdo.Dispose();
                ModAdoInsert.rsAdo.Dispose();
                ModAdoInsert.Dispose();
                rsAdd = null;
                ModAdoInsert.Close();
                ModAdoInsert.Dispose();
                PB1.Visible = true;
                PB1.Value = 30;
                PB1.PerformStep();
                //=== Insertion des données qui servront de base au calcul des contrats.
                sSQL = "INSERT INTO ANCDT_AnalyCtrData" + " (NumContrat, Avenant, CodeImmeuble, Code1, MatComm, InitailCom, NomCom, PrenomCom, " + " MatExploit, InitialExploit, NomExploit, PrenomExploit," + " DateDeSignature, DateEffet, DateFin, LibelleCont1, LibelleCont2, CodeArticle, " + " Designation1, BaseContractuelle, BaseActualisee, CON_Noauto, Resiliee, ANCE_Noauto, COT_Code, commentaire )";


                sSQL = sSQL + "SELECT    Contrat.NumContrat, Contrat.Avenant, Contrat.CodeImmeuble, Immeuble.Code1, Immeuble.CodeCommercial AS MatComm, " + " Personnel.Initiales AS InitailCom, Personnel.Nom AS NomCom, Personnel.Prenom AS PrenomCom, Immeuble.CodeRespExploit AS MatExploit, " + " Personnel_1.Initiales AS InitialExploit, Personnel_1.Nom AS NomExploit, Personnel_1.Prenom AS PrenomExploit, Contrat.DateDeSignature," + " Contrat.DateEffet, Contrat.DateFin, Contrat.LibelleCont1, Contrat.LibelleCont2, Contrat.CodeArticle, FacArticle.Designation1, " + " Contrat.BaseContractuelle, Contrat.BaseActualisee, Contrat.CON_Noauto, contrat.Resiliee," + lANCE_Noauto + " as ID , FacArticle.COT_Code " + ", ANCC_CommACCEPTE ";


                sSQL = sSQL + " FROM         CONT_ContratComment RIGHT OUTER JOIN" + " Contrat INNER JOIN" + " Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble ON CONT_ContratComment.Codeimmeuble = Contrat.CodeImmeuble AND" + " CONT_ContratComment.NumContrat = Contrat.NumContrat LEFT OUTER JOIN " + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle LEFT OUTER JOIN " + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule LEFT OUTER JOIN " + " Personnel AS Personnel_1 ON Immeuble.CodeRespExploit = Personnel_1.Matricule";

                sSQL = sSQL + " ORDER BY Contrat.CodeImmeuble,Contrat.NumContrat,Contrat.Avenant, contrat.Resiliee   ";
                General.Execute(sSQL);

                PB1.Value = 60;
                PB1.PerformStep();
                //=== Assoscie pour un champ commentaire chaque contrat.
                fc_AddContratComment();

                //=== selection des contrats à traiter.
                sSQL = "SELECT     ANCDT_AnalyCtrData, ANCE_Noauto, NumContrat, Avenant, CodeImmeuble, Code1, MatComm, InitailCom, NomCom, PrenomCom, MatExploit, " + " InitialExploit, NomExploit, PrenomExploit, DateDeSignature, DateEffet, DateFin, LibelleCont1, LibelleCont2, CodeArticle, Designation1, " + " BaseContractuelle , BaseActualisee, CON_Noauto, Resiliee, COT_Code, commentaire " + " From ANCDT_AnalyCtrData " + " WHERE ANCE_Noauto = " + lANCE_Noauto;
                sSQL = sSQL + " ORDER BY CodeImmeuble,NumContrat,Avenant";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count != 0)
                {
                    dbIndex = Convert.ToDouble(100) / Convert.ToDouble(rs.Rows.Count);
                }
                var ModAdoRsAddUpdate = new ModAdo();
                //=== recordset pour l'ajout des contrats.
                sSQL = "SELECT     ANCD_Noauto, ANCE_Noauto, Codeimmeuble, Code1, NumContrat, Avenant, ANCD_CommMat," + " ANCD_CommInitiale, ANCD_CommNomComplet, " + " ANCD_RespExploitMat, ANCD_RespExploitInitiale, ANCD_RespExploitNomComplet," + " ANCD_DateSignature, ANCD_DateEffet, ANCD_Exercice, " + " ANCD_Indicateur , ANCD_Ht, ANCD_HtP1, ANCD_HtP2, ANCD_HtP3, ANCD_HtP4," + " ANCD_Commentaire, ANCD_24Ssur24, ANCD_Renovation, ANCD_Statut, CON_Noauto,  ANCD_DateFin, ANCD_DateReceptFin  FROM         ANCD_AnalyCtrDetail" + " WHERE     (ANCD_Noauto = 0)";//TODO .column ANCD_Commentaire exist 2 times in QUERY .it's causing a problem while updating database using ModAdo .So I deleted one 

                var rsAddUpdate = ModAdoRsAddUpdate.fc_OpenRecordSet(sSQL);

                //========== CONTRATS ACCEPTES.
                sCodeImmeuble = "";
                sNoContrat = "";
                sAvenant = 0;

                PB1.Value = 100;
                PB1.Value = 1;
                PB1.PerformStep();
                DataRow rsDr = null;
                bool isAddNew = false;//TODO
                                      //int ANCE_Noauto = 0;
                int ANCD_Noauto = 0;
                cb.Dispose();
                insertCmd.Dispose();

                cb = new SqlCommandBuilder(ModAdoRsAddUpdate.SDArsAdo);
                ModAdoRsAddUpdate.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                ModAdoRsAddUpdate.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                ModAdoRsAddUpdate.SDArsAdo.InsertCommand = insertCmd;

                ModAdoRsAddUpdate.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {
                    if (e.StatementType == StatementType.Insert)
                    {
                        ANCD_Noauto = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                    }
                });



                foreach (DataRow dr1 in rs.Rows)
                {

                    //==== modif du 04 Nov 2013, controle si le contrat est resiliée
                    //=== on verifie la date de fin.

                    bCtrResiliee = false;

                    if (dr1["Resiliee"] != null && Convert.ToBoolean(dr1["Resiliee"]) == true)//TESTED
                    {
                        bCtrResiliee = true;
                    }
                    else
                    {
                        bCtrResiliee = false;
                    }

                    if (dr1["DateFin"] != DBNull.Value)//TESTED
                    {
                        //=== si la date de fin du contrat est supérieur à la date du jour
                        //=== alors on considére que ce contrat n'est pas résilié.
                        if (Convert.ToDateTime(dr1["DateFin"]) > DateTime.Today && bCtrResiliee == true)
                        {
                            bCtrResiliee = true;
                            fc_Err(dr1["NumContrat"] + "", Convert.ToInt32(dr1["avenant"]), dr1["CodeImmeuble"] + "", lANCE_Noauto, "Le contrat n°" + dr1["NumContrat"] + " avenant " + dr1["avenant"] + " est résilié mais sa date de fin se termine le " + dr1["DateFin"] + ", attention celui-ci a été comptabilisée comme résilié");
                        }
                        else if (Convert.ToDateTime(dr1["DateFin"]) < DateTime.Today)//TESTED
                        {
                            if (bCtrResiliee == false)//TESTED
                            {
                                fc_Err(dr1["NumContrat"] + "", Convert.ToInt32(dr1["avenant"]), dr1["CodeImmeuble"] + "", lANCE_Noauto, "Le contrat n°" + dr1["NumContrat"] + " avenant " + dr1["avenant"] + " n'est pas résilié mais sa date de fin est terminée depuis le " + dr1["DateFin"] + ", attention celui-ci a été comptabilisée comme non résilié");
                            }
                            else
                            {
                                bCtrResiliee = true;
                            }

                        }
                    }

                    if (bCtrResiliee == true)//TESTED
                    {
                        goto SUITE;
                    }

                    if (General.UCase(sCodeImmeuble) != General.UCase(dr1["CodeImmeuble"]))//TESTED
                    {
                        rsDr = rsAddUpdate.NewRow();
                        sCodeImmeuble = dr1["CodeImmeuble"] + "";
                        sNoContrat = dr1["NumContrat"] + "";

                        sAvenant = Convert.ToInt32(General.nz(rsDr["avenant"], 0));
                        dbHT = 0;
                        dbHtP1 = 0;
                        dbHtP2 = 0;
                        dbHtP3 = 0;
                        dbHtP4 = 0;
                        isAddNew = true;
                    }

                    if (General.UCase(sNoContrat) != General.UCase(dr1["NumContrat"]))//TESTED
                    {
                        rsDr = rsAddUpdate.NewRow();
                        //sCodeImmeuble = rsAdd!CodeImmeuble
                        sNoContrat = dr1["NumContrat"] + "";

                        sAvenant = Convert.ToInt32(General.nz(dr1["avenant"], 0));
                        dbHT = 0;
                        dbHtP1 = 0;
                        dbHtP2 = 0;
                        dbHtP3 = 0;
                        dbHtP4 = 0;
                        isAddNew = true;
                    }
                    string req = "update ANCD_AnalyCtrDetail set ";
                    req = req + " ANCE_Noauto='" + lANCE_Noauto + "',";
                    req = req + " CodeImmeuble='" + dr1["CodeImmeuble"] + "',";
                    req = req + " Code1='" + dr1["Code1"] + "',";
                    req = req + " NumContrat='" + dr1["NumContrat"] + "',";
                    req = req + " ANCD_CommMat='" + dr1["MatComm"] + "',";
                    req = req + " ANCD_CommInitiale='" + dr1["InitailCom"] + "',";
                    req = req + " ANCD_CommNomComplet='" + General.Left(dr1["NomCom"] + " " + dr1["PrenomCom"], 100) + "',";
                    req = req + " ANCD_RespExploitMat='" + dr1["MatExploit"] + "',";
                    req = req + " ANCD_RespExploitInitiale='" + dr1["InitialExploit"] + "',";
                    req = req + " ANCD_RespExploitNomComplet='" + dr1["NomExploit"] + " " + dr1["PrenomExploit"] + "',";
                    req = req + " ANCD_DateSignature='" + dr1["DateDeSignature"] + "',";
                    req = req + " ANCD_DateEffet='" + dr1["DateEffet"] + "',";
                    req = req + " ANCD_DateFin='" + dr1["DateFin"] + "',";
                    //rsAdd!CON_Noauto = rs!CON_Noauto
                    rsDr["ANCE_Noauto"] = lANCE_Noauto;
                    rsDr["CodeImmeuble"] = dr1["CodeImmeuble"];
                    rsDr["Code1"] = dr1["Code1"];
                    rsDr["NumContrat"] = dr1["NumContrat"];
                    //rsAdd!Avenant = rs!Avenant
                    rsDr["ANCD_CommMat"] = dr1["MatComm"];
                    rsDr["ANCD_CommInitiale"] = dr1["InitailCom"];
                    rsDr["ANCD_CommNomComplet"] = General.Left(dr1["NomCom"] + " " + dr1["PrenomCom"], 100);
                    rsDr["ANCD_RespExploitMat"] = dr1["MatExploit"];
                    rsDr["ANCD_RespExploitInitiale"] = dr1["InitialExploit"];
                    rsDr["ANCD_RespExploitNomComplet"] = dr1["NomExploit"] + " " + dr1["PrenomExploit"];
                    rsDr["ANCD_DateSignature"] = dr1["DateDeSignature"];
                    rsDr["ANCD_DateEffet"] = dr1["DateEffet"];

                    //=== recherche de l'exercice.
                    if (General.IsDate(dr1["DateEffet"]))//TESTED
                    {
                        dteffet = Convert.ToDateTime(dr1["DateEffet"]);
                        for (i = 0; i < General.tpExerciceCtr.Length; i++)
                        {
                            if (dteffet >= General.tpExerciceCtr[i].dtDebut && dteffet <= General.tpExerciceCtr[i].dtFin)
                            {
                                rsDr["ANCD_Exercice"] = General.tpExerciceCtr[i].sLibelle;
                                req = req + " ANCD_Exercice='" + General.tpExerciceCtr[i].sLibelle + "',";
                                break;
                            }
                        }
                    }

                    dbHT = 0;

                    if (Convert.ToInt32(General.nz(dr1["BaseActualisee"], 0)) > 0)//TESTEed
                    {
                        dbHT = Convert.ToDouble(dr1["BaseActualisee"]);
                    }
                    else//TESTED
                    {

                        dbHT = Convert.ToDouble(General.nz(dr1["BaseContractuelle"], 0));
                    }

                    //=== controle si il y a eu une rénovation.
                    sSQL = "SELECT     RenovationChauff, CodeImmeuble, CodeEtat, DateAcceptation" + " From DevisEnTete " + " Where (RenovationChauff = 1) and CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(dr1["CodeImmeuble"] + "") + "'" + " and(CodeEtat = 'T' or CodeEtat = 'TS' or CodeEtat = 'P3' or CodeEtat = 'CA')  ";

                    sSQL = ModAdo.fc_ADOlibelle(sSQL);
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        rsDr["ANCD_Renovation"] = 1;
                        req = req + "ANCD_Renovation='" + 1 + "',";
                    }

                    //==== code 38 avenant 24/24, code 34 teleAlarme, 80_AUGUST???
                    //==== P1 => 68 APPEL DE FONDS COMBUSTIBLE GAZ,122 ABONNEMENT, 166 APPEL DE FONDS COMBUSTIBLE CHAUFFAGE URBAIN.
                    //==== P3 => 11 GARANTIE TOTALE, 11A GARANTIE TOTALE
                    //==== P4 => 12 FINANCEMENT
                    sCodeArticle = dr1["CodeArticle"] + "";
                    sCodeArticle = General.UCase(sCodeArticle);
                    //==== modif du 4 novembre 2013, le type du contrat est determinée par l'artcile.
                    sCOT_Code = dr1["COT_Code"] + "";
                    sCOT_Code = General.UCase(sCOT_Code);

                    if (cP1.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP1 = dbHtP1 + dbHT;
                    else if (cP2.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP2 = dbHtP2 + dbHT;
                    else if (cP3.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP3 = dbHtP3 + dbHT;
                    else if (cP4.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP4 = dbHtP4 + dbHT;
                    else
                        dbHtP2 = dbHtP2 + dbHT;

                    if (General.UCase(sCodeArticle) == General.UCase("38") || General.UCase(sCodeArticle) == General.UCase("34"))
                    {
                        //=== contrat 24/24.
                        rsDr["ANCD_24Ssur24"] = 1;
                        req = req + " ANCD_24Ssur24='" + 1 + "',";
                    }
                    dbHT = dbHtP1 + dbHtP2 + dbHtP3 + dbHtP4;

                    if (dbHT > Convert.ToDouble(cLimiteHtCtr))//TESTED
                    {
                        rsDr["ANCD_Indicateur"] = sLimiteHtSup;
                        req = req + " ANCD_Indicateur='" + sLimiteHtSup + "',";
                    }
                    else
                    {
                        rsDr["ANCD_Indicateur"] = sLimiteHtInf;
                        req = req + " ANCD_Indicateur='" + sLimiteHtInf + "',";
                    }
                    rsDr["ANCD_Ht"] = dbHT;

                    req = req + " ANCD_Ht='" + dbHT + "',";
                    req = req + "ANCD_HtP1='" + dbHtP1 + "',";
                    req = req + "ANCD_HtP2='" + dbHtP2 + "',";
                    req = req + "ANCD_HtP3='" + dbHtP3 + "',";
                    req = req + "ANCD_HtP4='" + dbHtP4 + "',";
                    req = req + "ANCD_Statut='" + cResilie + "',";
                    req = req + "ANCD_Commentaire='" + dr1["Commentaire"].ToString().Replace("'", "''") + "' ";

                    rsDr["ANCD_HtP1"] = dbHtP1;
                    rsDr["ANCD_HtP2"] = dbHtP2;
                    rsDr["ANCD_HtP3"] = dbHtP3;
                    rsDr["ANCD_HtP4"] = dbHtP4;
                    rsDr["ANCD_Statut"] = cAccepte;
                    rsDr["ANCD_Commentaire"] = dr1["Commentaire"];
                    if (isAddNew)
                    {
                        rsAddUpdate.Rows.Add(rsDr.ItemArray);
                        ModAdoRsAddUpdate.Update();
                        isAddNew = false;
                    }
                    else
                    {
                        req = req + " WHERE ANCD_Noauto='" + ANCD_Noauto + "'";//TODO id add these lines of code because in vb6 when we don't have newrow they just update last inserted row .
                        General.Execute(req);
                    }
                SUITE:
                    dbTot = dbTot + dbIndex;
                    if (dbTot > 100)
                    {
                        PB1.Value = 100;
                    }
                    else
                    {
                        PB1.Value = Convert.ToInt32(dbTot);
                        PB1.PerformStep();
                    }
                }
                ModAdo.fc_CloseRecordset(rs);
                rs.Dispose();
                //dtDay = DateValue(Date)

                //========== CONTRATS RESILIES.
                sSQL = "SELECT     ANCDT_AnalyCtrData, ANCE_Noauto, NumContrat, Avenant, CodeImmeuble, Code1, MatComm, InitailCom, NomCom, PrenomCom, MatExploit, " + " InitialExploit, NomExploit, PrenomExploit, DateDeSignature, DateEffet, DateFin, LibelleCont1, LibelleCont2, CodeArticle, Designation1, " + " BaseContractuelle , BaseActualisee, CON_Noauto, Resiliee, COT_Code, commentaire " + " From ANCDT_AnalyCtrData " + " WHERE ANCE_Noauto = " + lANCE_Noauto + " AND DateFin is not null  ";

                sSQL = sSQL + " ORDER BY CodeImmeuble,NumContrat,Avenant ";
                rs = new DataTable();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                dbTot = 0;
                if (rs.Rows.Count != 0)
                {
                    dbIndex = Convert.ToDouble(100) / Convert.ToDouble(rs.Rows.Count);
                }
                sCodeImmeuble = "";
                sNoContrat = "";
                sAvenant = 0;
                rsDr = null;
                ANCD_Noauto = 0;
                isAddNew = false;

                foreach (DataRow dr1 in rs.Rows)
                {
                    if (General.UCase(sCodeImmeuble) != General.UCase(dr1["CodeImmeuble"]))
                    {
                        rsDr = rsAddUpdate.NewRow();
                        sCodeImmeuble = dr1["CodeImmeuble"] + "";
                        sNoContrat = dr1["NumContrat"] + "";

                        sAvenant = Convert.ToInt32(General.nz(rsDr["avenant"], 0));
                        dbHT = 0;
                        dbHtP1 = 0;
                        dbHtP2 = 0;
                        dbHtP3 = 0;
                        dbHtP4 = 0;
                        isAddNew = true;
                    }

                    if (General.UCase(sNoContrat) != General.UCase(dr1["NumContrat"]))
                    {
                        rsDr = rsAddUpdate.NewRow();
                        //sCodeImmeuble = rsAdd!CodeImmeuble
                        sNoContrat = dr1["NumContrat"] + "";

                        sAvenant = Convert.ToInt32(General.nz(dr1["avenant"], 0));
                        dbHT = 0;
                        dbHtP1 = 0;
                        dbHtP2 = 0;
                        dbHtP3 = 0;
                        dbHtP4 = 0;
                        isAddNew = true;
                    }
                    string req = "update ANCD_AnalyCtrDetail set ";
                    req = req + " ANCE_Noauto='" + lANCE_Noauto + "',";
                    req = req + " CodeImmeuble='" + dr1["CodeImmeuble"] + "',";
                    req = req + " Code1='" + dr1["Code1"] + "',";
                    req = req + " NumContrat='" + dr1["NumContrat"] + "',";
                    req = req + " ANCD_CommMat='" + dr1["MatComm"] + "',";
                    req = req + " ANCD_CommInitiale='" + dr1["InitailCom"] + "',";
                    req = req + " ANCD_CommNomComplet='" + General.Left(dr1["NomCom"] + " " + dr1["PrenomCom"], 100) + "',";
                    req = req + " ANCD_RespExploitMat='" + dr1["MatExploit"] + "',";
                    req = req + " ANCD_RespExploitInitiale='" + dr1["InitialExploit"] + "',";
                    req = req + " ANCD_RespExploitNomComplet='" + dr1["NomExploit"] + " " + dr1["PrenomExploit"] + "',";
                    req = req + " ANCD_DateSignature='" + dr1["DateDeSignature"] + "',";
                    req = req + " ANCD_DateEffet='" + dr1["DateEffet"] + "',";
                    req = req + " ANCD_DateFin='" + dr1["DateFin"] + "',";
                    //rsAdd!CON_Noauto = rs!CON_Noauto
                    rsDr["ANCE_Noauto"] = lANCE_Noauto;
                    rsDr["CodeImmeuble"] = dr1["CodeImmeuble"];
                    rsDr["Code1"] = dr1["Code1"];
                    rsDr["NumContrat"] = dr1["NumContrat"];
                    //rsAdd!Avenant = rs!Avenant
                    rsDr["ANCD_CommMat"] = dr1["MatComm"];
                    rsDr["ANCD_CommInitiale"] = dr1["InitailCom"];
                    rsDr["ANCD_CommNomComplet"] = General.Left(dr1["NomCom"] + " " + dr1["PrenomCom"], 100);
                    rsDr["ANCD_RespExploitMat"] = dr1["MatExploit"];
                    rsDr["ANCD_RespExploitInitiale"] = dr1["InitialExploit"];
                    rsDr["ANCD_RespExploitNomComplet"] = dr1["NomExploit"] + " " + dr1["PrenomExploit"];
                    rsDr["ANCD_DateSignature"] = dr1["DateDeSignature"];
                    rsDr["ANCD_DateEffet"] = dr1["DateEffet"];
                    rsDr["ANCD_DateFin"] = dr1["DateFin"];

                    //=== recherche de l'exercice.
                    if (General.IsDate(dr1["DateFin"]))
                    {
                        dteffet = Convert.ToDateTime(dr1["DateFin"]);
                        for (i = 0; i < General.tpExerciceCtr.Length; i++)
                        {
                            if (dteffet >= General.tpExerciceCtr[i].dtDebut && dteffet <= General.tpExerciceCtr[i].dtFin)
                            {
                                rsDr["ANCD_Exercice"] = General.tpExerciceCtr[i].sLibelle;
                                req = req + " ANCD_Exercice='" + General.tpExerciceCtr[i].sLibelle + "',";
                                break;
                            }
                        }
                    }

                    dbHT = 0;

                    if (Convert.ToInt32(General.nz(dr1["BaseActualisee"], 0)) > 0)
                    {
                        dbHT = Convert.ToDouble(dr1["BaseActualisee"]);
                    }
                    else
                    {
                        dbHT = Convert.ToDouble(General.nz(dr1["BaseContractuelle"], 0));
                    }
                    //==== code 38 avenant 24/24, code 34 teleAlarme, 80_AUGUST???
                    //==== P1 => 68 APPEL DE FONDS COMBUSTIBLE GAZ,122 ABONNEMENT, 166 APPEL DE FONDS COMBUSTIBLE CHAUFFAGE URBAIN.
                    //==== P3 => 11 GARANTIE TOTALE, 11A GARANTIE TOTALE
                    //==== P4 => 12 FINANCEMENT
                    sCodeArticle = dr1["CodeArticle"] + "";
                    sCodeArticle = General.UCase(sCodeArticle);
                    //==== modif du 4 novembre 2013, le type du contrat est determinée par l'artcile.
                    sCOT_Code = dr1["COT_Code"] + "";
                    sCOT_Code = General.UCase(sCOT_Code);

                    if (cP1.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP1 = dbHtP1 + dbHT;
                    else if (cP2.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP2 = dbHtP2 + dbHT;
                    else if (cP3.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP3 = dbHtP3 + dbHT;
                    else if (cP4.ToUpper() == sCOT_Code.ToUpper())
                        dbHtP4 = dbHtP4 + dbHT;
                    else
                        dbHtP2 = dbHtP2 + dbHT;
                    if (General.UCase(sCodeArticle) == General.UCase("38") || General.UCase(sCodeArticle) == General.UCase("34"))
                    {
                        //=== contrat 24/24.
                        rsDr["ANCD_24Ssur24"] = 1;
                        req = req + " ANCD_24Ssur24='" + 1 + "',";
                    }
                    dbHT = dbHtP1 + dbHtP2 + dbHtP3 + dbHtP4;

                    rsDr["ANCD_Ht"] = dbHT;
                    req = req + " ANCD_Ht='" + dbHT + "',";
                    if (dbHT > Convert.ToDouble(cLimiteHtCtr))
                    {
                        rsDr["ANCD_Indicateur"] = sLimiteHtSup;
                        req = req + " ANCD_Indicateur='" + sLimiteHtSup + "',";
                    }
                    else
                    {
                        rsDr["ANCD_Indicateur"] = sLimiteHtInf;
                        req = req + " ANCD_Indicateur='" + sLimiteHtInf + "',";
                    }
                    req = req + "ANCD_HtP1='" + dbHtP1 + "',";
                    req = req + "ANCD_HtP2='" + dbHtP2 + "',";
                    req = req + "ANCD_HtP3='" + dbHtP3 + "',";
                    req = req + "ANCD_HtP4='" + dbHtP4 + "',";
                    req = req + "ANCD_Statut='" + cResilie + "',";
                    req = req + "ANCD_Commentaire='" + dr1["Commentaire"].ToString().Replace("'", "''") + "' ";

                    rsDr["ANCD_HtP1"] = dbHtP1;
                    rsDr["ANCD_HtP2"] = dbHtP2;
                    rsDr["ANCD_HtP3"] = dbHtP3;
                    rsDr["ANCD_HtP4"] = dbHtP4;
                    rsDr["ANCD_Statut"] = cResilie;
                    rsDr["ANCD_Commentaire"] = dr1["Commentaire"];
                    //        rsadd!ANCD_Commentaire = rs!
                    //        rsadd!ANCD_24Ssur24 = rs!
                    //        rsadd!ANCD_Renovation = rs!
                    if (isAddNew)
                    {
                        rsAddUpdate.Rows.Add(rsDr.ItemArray);
                        ModAdoRsAddUpdate.Update();
                        isAddNew = false;
                    }
                    else
                    {
                        req = req + " WHERE ANCD_Noauto='" + ANCD_Noauto + "'";//TODO id add these lines of code because in vb6 when we don't have newrow they just update last inserted row .
                        General.Execute(req);
                    }
                    dbTot = dbTot + dbIndex;
                    if (dbTot > 100)
                    {
                        PB1.Value = 100;
                    }
                    else
                    {

                        PB1.Value = Convert.ToInt32(dbTot);
                        PB1.PerformStep();
                    }
                }
                rs = null;
                ModAdo.fc_CloseRecordset(rsAdd);
                rsAdd = null;

                PB1.Visible = false;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                fc_Err("", 0, "", 0, ex.Message);
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierPorteFeuille);
                }
            }
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

            PB1.Visible = false;
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_AddContratComment()
        {
            string sSQL = null;
            int X = 0;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "INSERT INTO CONT_ContratComment" + " (Codeimmeuble, NumContrat ) " + " SELECT DISTINCT    Contrat.CodeImmeuble, Contrat.NumContrat" + " FROM         Contrat LEFT OUTER JOIN " + " CONT_ContratComment  AS CONT_ContratComment_1 ON Contrat.NumContrat =   CONT_ContratComment_1.NumContrat " + "WHERE     (CONT_ContratComment_1.NumContrat IS NULL) AND (Contrat.CodeImmeuble IS NOT NULL AND Contrat.CodeImmeuble <> '')";

                General.Execute(sSQL);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_MajCommentaire");
            }
            //=== pour chaque contrat on associeun commentaire.
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNoContrat"></param>
        /// <param name="lAvenant"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="lANCE_Noauto"></param>
        /// <param name="sErreur"></param>
        private void fc_Err(string sNoContrat, int lAvenant, string sCodeImmeuble, int lANCE_Noauto, string sErreur = "")
        {
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                General.sSQL = "INSERT INTO ANCDTE_AnalyCtrDataErreur" + " (Codeimmeuble, NumContrat, Avenant, Erreur, ANCE_Noauto) "
                    + " VALUES     ('" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "', '" + StdSQLchaine.gFr_DoublerQuote(sNoContrat)
                    + "'," + lAvenant + "," + "'" + StdSQLchaine.gFr_DoublerQuote(sErreur) + "'," + lANCE_Noauto + ")";

                General.Execute(General.sSQL);

                blnErreur = true;
                if (!string.IsNullOrEmpty(sErreur))
                {
                    File.AppendAllText(General.cCheminFichierPorteFeuille, "\r\n" + sErreur);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_err");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lANCE_Noauto"></param>
        private void fc_LoadANCE(int lANCE_Noauto)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "SELECT     ANCE_Noauto, ANCE_CreePar, ANCE_CreeLe, ANCE_DateArret, ANCE_Commentaire" + " From ANCE_AnalyCtrEntete " + " WHERE   ANCE_Noauto =" + lANCE_Noauto;

                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    txtANCE_CreePar.Text = rs.Rows[0]["ANCE_CreePar"] + "";
                    txtANCE_CreeLe.Text = rs.Rows[0]["ANCE_CreeLe"] + "";
                    lblDataDate.Text = "BASE DE DONNEE DU " + rs.Rows[0]["ANCE_CreeLe"] + "";
                    txtANCE_DateArret.Text = rs.Rows[0]["ANCE_DateArret"] + "";
                    txtANCE_Commentaire.Text = rs.Rows[0]["ANCE_Commentaire"] + "";
                    txtANCE_Noauto.Text = rs.Rows[0]["ANCE_Noauto"] + "";
                }
                else
                {
                    fc_clear();
                }
                ModAdo.Close();
                rs = null;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadANCE");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lANCE_Noauto"></param>
        private void fc_LoadANCD(int lANCE_Noauto)
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "SELECT     ANCD_AnalyCtrDetail.ANCD_Noauto, ANCD_AnalyCtrDetail.ANCE_Noauto, ANCD_AnalyCtrDetail.Codeimmeuble, " + " CONT_ContratComment.ANCC_CommACCEPTE,CONT_ContratComment.ANCC_CommRESILIE,CONT_ContratComment.ANCC_DateReceptResil,CONT_ContratComment.ANCC_Reconduction," + " CONT_ContratComment.ANCC_RenovPerdue,  ANCD_AnalyCtrDetail.Code1, ANCD_AnalyCtrDetail.NumContrat, ANCD_AnalyCtrDetail.Avenant, " + " ANCD_AnalyCtrDetail.ANCD_CommMat, ANCD_AnalyCtrDetail.ANCD_CommInitiale, ANCD_AnalyCtrDetail.ANCD_CommNomComplet, " + " ANCD_AnalyCtrDetail.ANCD_RespExploitMat, ANCD_AnalyCtrDetail.ANCD_RespExploitInitiale, ANCD_AnalyCtrDetail.ANCD_RespExploitNomComplet," + " ANCD_AnalyCtrDetail.ANCD_DateSignature, ANCD_AnalyCtrDetail.ANCD_DateEffet,ANCD_DateFin, ANCD_AnalyCtrDetail.ANCD_Exercice, " + " ANCD_AnalyCtrDetail.ANCD_Indicateur, ANCD_AnalyCtrDetail.ANCD_Ht, ANCD_AnalyCtrDetail.ANCD_HtP1, ANCD_AnalyCtrDetail.ANCD_HtP2, " + " ANCD_AnalyCtrDetail.ANCD_HtP3, ANCD_AnalyCtrDetail.ANCD_HtP4, ANCD_AnalyCtrDetail.ANCD_Commentaire, " + " ANCD_AnalyCtrDetail.ANCD_24Ssur24, ANCD_AnalyCtrDetail.ANCD_Renovation, ANCD_AnalyCtrDetail.ANCD_Statut, " + " ANCD_AnalyCtrDetail.CON_Noauto " + " FROM         ANCD_AnalyCtrDetail LEFT OUTER JOIN" + " CONT_ContratComment ON ANCD_AnalyCtrDetail.Codeimmeuble = CONT_ContratComment.Codeimmeuble AND " + " ANCD_AnalyCtrDetail.NumContrat = CONT_ContratComment.NumContrat" + " WHERE  ANCD_AnalyCtrDetail.ANCE_Noauto = " + lANCE_Noauto + " ORDER BY ANCD_AnalyCtrDetail.Codeimmeuble";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAdoGridUpdate = new ModAdo();
                rsANCD = ModAdoGridUpdate.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridANCD.DataSource = rsANCD;
                ssGridANCD.UpdateData();
                txtTotal.Text = Convert.ToString(rsANCD.Rows.Count);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadANCD");
            }
        }

        /// <summary>
        /// TESTED   .TODO some methodes like copyFromRecordset() doesn't work
        /// </summary>
        /// <param name="lANCE_Noauto"></param>
        /// <param name="dtDebut"></param>
        /// <param name="dtFin"></param>
        /// <param name="lTotExercice"></param>
        /// <param name="lDetailExercice"></param>
        /// <param name="lGraphExelComp"></param>
        private void fc_Export(int lANCE_Noauto, object dtDebut, System.DateTime dtFin, int lTotExercice, int lDetailExercice, int lGraphExelComp)
        {
            double dbNbCont = 0;

            string sMatricule = null;
            string sSQL = null;
            string sFormule = null;
            string sNameSheet = null;
            string sNameSheet2 = null;
            string sNameSheet3 = null;
            string sNameSheet4 = null;
            string sNameSheetGraph = null;
            string sNamePivotField = null;
            string stemp = null;

            DataTable rs = default(DataTable);
            DataTable rsExp = default(DataTable);
            DataTable rsDet = default(DataTable);

            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);

            Microsoft.Office.Interop.Excel.Worksheet oSheet2 = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Worksheet oSheet3 = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Worksheet oSheet4 = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Worksheet oSheetGraph = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Worksheet oSheetDet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Worksheet oSheetDet2 = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Shape oShape = default(Microsoft.Office.Interop.Excel.Shape);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oRange = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.PivotItem oPivItem = default(Microsoft.Office.Interop.Excel.PivotItem);

            DataTable rsExport = default(DataTable);
            DataColumn oField = null;

            int i = 0;
            int j = 0;
            int X = 0;
            int lLigne = 0;
            int lexe = 0;
            int m = 0;
            int lnbSheet = 0;
            int ltotLigneAc = 0;
            int ltotLigneRe = 0;
            int xxxtemp = 0;

            double dbIndex = 0;
            double dbTot = 0;

            bool bExerciceIn = false;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
                //====  CONTRATS ACCEPTES.
                sSQL = "SELECT   ANCD_Noauto, ANCE_Noauto, ANCD_AnalyCtrDetail.Codeimmeuble,  Code1, ANCD_AnalyCtrDetail.NumContrat, Avenant, ANCD_CommMat," + " ANCD_CommInitiale, ANCD_CommNomComplet, " + " ANCD_RespExploitMat, ANCD_RespExploitInitiale, ANCD_RespExploitNomComplet," + " ANCD_DateSignature, ANCD_DateEffet, ANCD_Exercice, " + " ANCD_Indicateur , ANCD_Ht, ANCD_HtP1, ANCD_HtP2, ANCD_HtP3, ANCD_HtP4," + "  ANCD_24Ssur24, ANCD_Renovation, ANCD_Statut,ANCC_CommACCEPTE " + " FROM         ANCD_AnalyCtrDetail LEFT OUTER JOIN" + " CONT_ContratComment ON ANCD_AnalyCtrDetail.Codeimmeuble = CONT_ContratComment.Codeimmeuble AND " + " ANCD_AnalyCtrDetail.NumContrat = CONT_ContratComment.NumContrat" + " WHERE    ANCE_Noauto = " + lANCE_Noauto + " and ANCD_Statut ='" + cAccepte + "'" + " and ANCD_DateEffet >= '" + dtDebut + "'" + " and ANCD_DateEffet <='" + dtFin + "'" + " order by ANCD_DateEffet ";


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var ModAdo = new ModAdo();
                rsExp = ModAdo.fc_OpenRecordSet(sSQL);
                if (rsExp.Rows.Count == 0)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //FloodDisplay .RecordCount, "Export des contrats en cours, veuillez patienter ..."
                PB1.Visible = true;
                PB1.Value = 0;
                PB1.PerformStep();
                dbIndex = Convert.ToDouble(100) / Convert.ToDouble(rsExp.Rows.Count);

                //=== Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                // oXL.Visible = True
                oXL.Visible = false;

                //=== Get a new workbook.
                oWB = oXL.Workbooks.Add();

                //=== supprime les feuilles 2 et 3.

                //oWB.Worksheets[3].Delete();//TODO to look at this line

                //oWB.Worksheets[2].Delete();//TODO to look at this line

                //GoTo SUITE
                oSheet = oWB.ActiveSheet;
                lnbSheet = 1;

                sNameSheet = cSheetContratAccepte;
                //=== renomme la feuille et la colore en vert.
                oSheet.Name = sNameSheet;
                oSheet.Tab.Color = Color.FromArgb(80, 176, 0).ToArgb();
                //=== vert.
                //oSheet.Tab.Color = 255 '=== rouge

                //== affiche les noms des champs.
                i = 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cIMMEUBLE;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cSYNDIC;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cNo;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cRC;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cRE;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cDATESIGNATURE;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cDATEEFFECTIVE;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cEXERCICE;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cINDICATEUR;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cMONTANT;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cP1;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cP2;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cP3;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cP4;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cCommentaires;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = c2424;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cRENOVATIONGAGNEE;
                i = i + 1;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                lLigne = 2;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""

                //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                foreach (DataRow dr in rsExp.Rows)
                {
                    j = 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["CodeImmeuble"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["Code1"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["NumContrat"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_CommInitiale"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_RespExploitInitiale"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_DateSignature"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = Convert.ToDateTime(dr["ANCD_DateEffet"] + "").ToShortDateString();
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_Exercice"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_Indicateur"] + "";
                    if (General.UCase(dr["ANCD_Indicateur"]) == General.UCase("<" + cLimiteHtCtr))
                    {
                        oSheet.Cells[lLigne, j + cCol].Font.Color = -16776961;
                        //==rouge.
                    }
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_Ht"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_HtP1"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_HtP2"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_HtP3"] + "";
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCD_HtP4"] + "";
                    j = j + 1;

                    //=== recherche du commentaire.
                    //sSQL = "select"

                    oSheet.Cells[lLigne, j + cCol].value = dr["ANCC_CommACCEPTE"] + "";
                    j = j + 1;

                    if (Convert.ToInt32(General.nz(dr["ANCD_24Ssur24"], 0)) == 1)//TESTEd
                    {
                        oSheet.Cells[lLigne, j + cCol].value = "x";
                    }
                    j = j + 1;

                    if (Convert.ToInt32(General.nz(dr["ANCD_Renovation"], 0)) == 1)
                    {
                        oSheet.Cells[lLigne, j + cCol].value = "x";
                    }
                    j = j + 1;

                    lLigne = lLigne + 1;

                    dbTot = dbTot + dbIndex;

                    if (dbTot > 100)
                    {
                        PB1.Value = 100;
                    }
                    else
                    {
                        PB1.Value = Convert.ToInt32(dbTot);
                        PB1.PerformStep();
                    }
                    //fmsg = "Export contrat, veuillez patienter ..." & lLigne & " / " & IIf(.RecordCount = 0, 1, .RecordCount)
                    //FloodUpdateText lLigne
                }

                ltotLigneAc = lLigne - 1;

                PB1.Value = 5;

                PB1.PerformStep();
                //==== Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                //======= formate l'entete de colonne.
                oResizeRange.Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlSolid;
                oResizeRange.Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;
                oResizeRange.Interior.ThemeColor = Microsoft.Office.Interop.Excel.XlThemeColor.xlThemeColorAccent3;
                oResizeRange.Interior.TintAndShade = 0.399975585192419;
                oResizeRange.Interior.PatternTintAndShade = 0;
                //oResizeRange.Interior.ColorIndex = 15
                //===formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                //=== aligne automatiquement les colonnes selon la taille du texte.
                oResizeRange.EntireColumn.AutoFit();

                oResizeRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
                oResizeRange.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.WrapText = false;
                oResizeRange.Orientation = 0;
                oResizeRange.AddIndent = false;
                oResizeRange.IndentLevel = 0;
                oResizeRange.ShrinkToFit = false;
                oResizeRange.ReadingOrder = Convert.ToInt32(Microsoft.Office.Interop.Excel.Constants.xlContext);
                oResizeRange.MergeCells = false;

                oResizeRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.WrapText = false;
                oResizeRange.Orientation = 0;
                oResizeRange.AddIndent = false;
                oResizeRange.IndentLevel = 0;
                oResizeRange.ShrinkToFit = false;
                oResizeRange.ReadingOrder = Convert.ToInt32(Microsoft.Office.Interop.Excel.Constants.xlContext);
                oResizeRange.MergeCells = false;

                oResizeRange.Rows["1:1"].RowHeight = 45;

                //=== color la colonne indicateur si montant >2000 €.
                oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions.Add(Microsoft.Office.Interop.Excel.XlFormatConditionType.xlCellValue, Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlEqual, Formula1: "=\">2000€\"");

                oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions.Count].SetFirstPriority();

                oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].Font.Color = -16383844;

                oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].Font.TintAndShade = 0;

                oSheet.Range[cPosINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].StopIfTrue = false;

                //=== color la colonne montant si montant >2000 €.
                oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions.Add(Microsoft.Office.Interop.Excel.XlFormatConditionType.xlCellValue, Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlGreater, "=" + cLimiteHtCtr + "");

                oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions.Count].SetFirstPriority();

                oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].Font.Color = -16383844;

                oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].Font.TintAndShade = 0;

                oSheet.Range[cPosMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].StopIfTrue = false;

                //==== color une ligne sur deux.

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions.Add(Type: Microsoft.Office.Interop.Excel.XlFormatConditionType.xlExpression, Operator: Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlEqual, Formula1: "=MOD(LIGNE();2)");
                // oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions.Add(Type: Microsoft.Office.Interop.Excel.XlFormatConditionType.xlExpression, Operator: Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlEqual, Formula1: "=MOD(ROW(),2)");

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions.Count].SetFirstPriority();

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.ThemeColor = Microsoft.Office.Interop.Excel.XlThemeColor.xlThemeColorAccent3;

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.TintAndShade = 0.799981688894314;

                oSheet.Range[oSheet.Cells[2, 1], oSheet.Cells[lLigne - 1, j - 1]].FormatConditions[1].StopIfTrue = false;

                //==== insere des formule d pour sommmerles colonnes.
                sFormule = "=SUM(" + cPosMONTANT + 2 + ":" + cPosMONTANT + (lLigne - 1) + ")";
                oSheet.Range[cPosMONTANT + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cPosP1 + 2 + ":" + cPosP1 + (lLigne - 1) + ")";
                oSheet.Range[cPosP1 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cPosP2 + 2 + ":" + cPosP2 + (lLigne - 1) + ")";
                oSheet.Range[cPosP2 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cPosP3 + 2 + ":" + cPosP3 + (lLigne - 1) + ")";
                oSheet.Range[cPosP3 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cPosP4 + 2 + ":" + cPosP4 + (lLigne - 1) + ")";
                oSheet.Range[cPosP4 + lLigne].Formula = sFormule;
                // sFormule = "IIF(" & cPosMONTANT & 2 > cLimiteHtCtr

                //=== encadre les formules ci dessus.
                oSheet.Range[cPosMONTANT + lLigne, cPosP4 + lLigne].Borders.ColorIndex = 0;
                oSheet.Range[cPosMONTANT + lLigne, cPosP4 + lLigne].Borders.TintAndShade = 0;
                oSheet.Range[cPosMONTANT + lLigne, cPosP4 + lLigne].Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                //==== change la police en gras
                oSheet.Range[cPosIMMEUBLE + 2, cPosNo + (lLigne - 1)].Font.Bold = true;
                oSheet.Range[cPosDATEEFFECTIVE + 2, cPosMONTANT + (lLigne - 1)].Font.Bold = true;

                //=== chnane la taille de la police.
                oSheet.Range[cPosIMMEUBLE + 2, cPosRENOVATIONGAGNEE + lLigne].Font.Name = "Calibri";
                oSheet.Range[cPosIMMEUBLE + 2, cPosRENOVATIONGAGNEE + lLigne].Font.Size = 10;

                //==== fige les volets.
                oSheet.Range[cPosRC + 2].Activate();
                oXL.ActiveWindow.FreezePanes = true;

                PB1.Value = 10;
                PB1.PerformStep();
                //=== chyange la largeur des colonnes.

                oSheet.Columns[cPosMONTANT + ":" + cPosMONTANT].ColumnWidth = 14.71;

                oSheet.Columns[cPosP1 + ":" + cPosP1].ColumnWidth = 11.71;

                oSheet.Columns[cPosP2 + ":" + cPosP2].ColumnWidth = 11.71;

                oSheet.Columns[cPosP3 + ":" + cPosP3].ColumnWidth = 11.71;

                oSheet.Columns[cPosP4 + ":" + cPosP4].ColumnWidth = 11.71;

                oSheet.Columns[cPosCommentaires + ":" + cPosCommentaires].ColumnWidth = 56.43;

                oSheet.Columns[cPosNo + ":" + cPosNo].ColumnWidth = 8.71;

                oSheet.Columns[cPosRC + ":" + cPosRC].ColumnWidth = 8.71;

                oSheet.Columns[cPosRE + ":" + cPosRE].ColumnWidth = 8.71;

                //==== centre les cellules des donnees.
                oSheet.Range[cPosIMMEUBLE + 2, cPosINDICATEUR + (lLigne - 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet.Range[cPosIMMEUBLE + 2, cPosINDICATEUR + (lLigne - 1)].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Range[cPos2424 + 2, cPosRENOVATIONGAGNEE + (lLigne - 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet.Range[cPosCommentaires + 2, cPosRENOVATIONGAGNEE + (lLigne - 1)].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                //=== renvoyer a la ligne automatiquement.
                oSheet.Range[cPosIMMEUBLE + 1, cPosRENOVATIONGAGNEE + (lLigne - 1)].WrapText = true;

                //==== centre les cellules des en tetes.
                oSheet.Range[cPosIMMEUBLE + 1, cPosRENOVATIONGAGNEE + 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet.Range[cPosIMMEUBLE + 1, cPosRENOVATIONGAGNEE + 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                //=== active les filtres pour chaque colonnes.
                oSheet.Range[cPosIMMEUBLE + 1, cPosRENOVATIONGAGNEE + 1].AutoFilter(1, Type.Missing, XlAutoFilterOperator.xlAnd, Type.Missing, true);

                //=== dessine un tableau.
                oSheet.Range[oSheet.Cells[1, 1], oSheet.Cells[lLigne - 1, j - 1]].Borders.ColorIndex = 0;
                oSheet.Range[oSheet.Cells[1, 1], oSheet.Cells[lLigne - 1, j - 1]].Range[cPosMONTANT + lLigne, cPosP4 + lLigne].Borders.TintAndShade = 0;
                oSheet.Range[oSheet.Cells[1, 1], oSheet.Cells[lLigne - 1, j - 1]].Range[cPosMONTANT + lLigne, cPosP4 + lLigne].Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                PB1.Value = 15;
                PB1.PerformStep();
                //=== détail des contrats importées
                if (lDetailExercice == 1)
                {
                    sSQL = "SELECT     Code1, CodeImmeuble, NumContrat, Avenant, MatComm, InitailCom, NomCom, MatExploit," + " InitialExploit, NomExploit, DateDeSignature, DateEffet, " + " DateFin , LibelleCont1, LibelleCont2, CodeArticle, Designation1, BaseContractuelle, BaseActualisee, Resiliee " + " FROM         ANCDT_AnalyCtrData" + " WHERE    ANCE_Noauto = " + lANCE_Noauto + " and DateEffet >= '" + dtDebut + "'" + " and DateEffet <='" + dtFin + "'" + " order by DateEffet ";
                    rsDet = ModAdo.fc_OpenRecordSet(sSQL);
                    if (rsDet.Rows.Count > 0)
                    {
                        oSheetDet = oWB.Sheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);

                        lnbSheet = lnbSheet + 1;

                        oSheetDet.Name = General.Left(cDetail + " " + clibAccepte, 31);
                        int h = 0;
                        var data = new object[rsDet.Rows.Count, rsDet.Columns.Count + 1];
                        foreach (DataRow dr in rsDet.Rows)
                        {
                            for (int k = 0; k < rsDet.Columns.Count; k++)
                            {
                                if (dr[k].GetType() == typeof(DateTime))
                                    data[h, k] = Convert.ToDateTime(dr[k]).ToShortDateString();
                                else
                                    data[h, k] = dr[k].ToString();
                            }
                            h++;
                        }

                        //oSheetDet.Range["A3"].CopyFromRecordset(rsDet);//TODO. this methode throw an exception. i replaced this by these lines after
                        //var startCell = (Range)oSheetDet.Cells[3, 1];
                        //var endCell = (Range)oSheetDet.Cells[rsDet.Rows.Count + 2, rsDet.Columns.Count + 1];
                        //var rangeToWrite = oSheetDet.Range[startCell, endCell];
                        //rangeToWrite.Value2 = data;
                        oSheetDet.Range[(Range)oSheetDet.Cells[3, 1], (Range)oSheetDet.Cells[rsDet.Rows.Count + 2, rsDet.Columns.Count + 1]].Value2 = data;
                        //== titre de la feuille.
                        oSheetDet.Range["D1"].FormulaR1C1 = General.UCase(cDetail + " " + clibAccepte);
                        oSheetDet.Range["D1"].Font.Size = 14;
                        //=== fusionne.
                        oSheetDet.Range["D1:F1"].MergeCells = true;

                        //=== encadre.
                        //oSheetDet.Range["D1:F1").BorderAround  LineStyle:=xlContinuous, Weight:=xlMedium, Color:=vbRed     '-16776961

                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Color = 16776961;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Color = 16776961;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Color = 16776961;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Color = 16776961;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheetDet.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                    }

                    ModAdo.fc_CloseRecordset(rsDet);

                    rsDet = null;
                }

                PB1.Value = 25;
                PB1.PerformStep();
                for (m = 0; m <= 1; m++)
                {
                    for (lexe = 0; lexe < tpExerciceExe.Length; lexe++)
                    {
                        //======== INSERE UN TABHLEAU CROISEE DYNAMIQUE.
                        if (m == 0)
                        {
                            sNameSheet2 = cShettACCPARRE + " " + General.Left(General.Replace(tpExerciceExe[lexe].sLibelle, "/", "-"), 31);
                        }
                        else
                        {
                            sNameSheet2 = cShettACCPARRC + " " + General.Left(General.Replace(tpExerciceExe[lexe].sLibelle, "/", "-"), 31);
                        }

                        //Set oSheet2 = oWB.Sheets.Add(, After:=oWB.Sheets(sNameSheet))
                        oSheet2 = oWB.Sheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);

                        lnbSheet = lnbSheet + 1;

                        //=== excel version 2010
                        //=== excel version 2007
                        oWB.PivotCaches().Create(SourceType: Microsoft.Office.Interop.Excel.XlPivotTableSourceType.xlDatabase, SourceData: "" + cSheetContratAccepte + "!R1C1:R" + (lLigne - 1) + "C17").CreatePivotTable(TableDestination: oWB.Sheets[oSheet2.Name].Range["A4"], TableName: "Tableau croisé dynamique1");
                        //, DefaultVersion:=4
                        oSheet2.Name = sNameSheet2;

                        oSheet2.Cells[3, 1].Select();

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlPageField;

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).Position = 1;

                        if (m == 0)
                        {
                            //=== par responsable exploit puis commercial.

                            var _with15 = oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRE);

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;


                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Position = 1;


                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Position = 2;
                            sNamePivotField = cRE;

                        }
                        else
                        {
                            //=== par responsable commercial puis exploit.

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Position = 1;

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Position = 2;
                            sNamePivotField = cRC;

                        }
                        oSheet2.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cP1), "Somme de P1", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                        oSheet2.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cP2), "Somme de P2", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                        oSheet2.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cP3), "Somme de P3", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                        oSheet2.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cP4), "Somme de P4", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                        //=== afficher les etiquettes d'éléments sous forme de tableau.

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(sNamePivotField).LayoutForm = Microsoft.Office.Interop.Excel.XlLayoutFormType.xlTabular;

                        //=== renomme la colonne ne RE.
                        oSheet2.PivotTables("Tableau croisé dynamique1").CompactLayoutRowHeader = sNamePivotField;

                        //=== applique un style au tableau.
                        oSheet2.PivotTables("Tableau croisé dynamique1").TableStyle2 = "PivotStyleMedium14";

                        //=== encadre les sous totaux dans le tableau croisé dynamique.

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotSelect(sNamePivotField + "[All]", Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlLabelOnly & Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlFirstRow, true);

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotSelect(sNamePivotField + "[All;Total]", Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlDataAndLabel, true);

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;


                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].ColorIndex = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].ColorIndex = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].ColorIndex = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].ColorIndex = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        //=== augmente le taille des lignes.
                        oSheet2.Cells.RowHeight = 24.75;

                        //=== augmente le taille des colonnes.
                        oSheet2.Cells.ColumnWidth = 16;

                        //==== saisie du titre sur 2 lignes
                        oSheet2.Range["D1"].FormulaR1C1 = General.UCase(clibAccepte);
                        oSheet2.Range["D2"].FormulaR1C1 = "DU " + tpExerciceExe[lexe].dtDebut.ToShortDateString() + " AU " + tpExerciceExe[lexe].dtFin.ToShortDateString();
                        oSheet2.Range["D1:D2"].Font.Name = "Calibri";
                        oSheet2.Range["D1:D2"].Font.Size = 14;
                        oSheet2.Range["D1:D2"].Font.Color = -16776961;
                        //== rouge.
                        oSheet2.Range["D1:D2"].Font.Bold = true;

                        //=== alignement du titre
                        oSheet2.Range["D1:F2"].Font.Size = 14;
                        oSheet2.Range["D1:F2"].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet2.Range["D1:F2"].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet2.Range["D1:F1"].MergeCells = true;
                        oSheet2.Range["D2:F2"].MergeCells = true;
                        //oSheet2.Range["D1:F2").BorderAround xlContinuous, xlMedium, , -16776961
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Color = -16776961;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        // .ColorIndex = 0
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Color = -16776961;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Color = -16776961;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Color = -16776961;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheet2.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        //==== selectionne des valeurs dans la liste déroulante correspondant au filtre.

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).CurrentPage = "(All)";

                        oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).EnableMultiplePageItems = true;

                        //=== boucle sur tous les exercices present dans le filtre du fichier excel, controle
                        //=== si l'excercice dans le tableau tpExerciceExe(lexe).sLibelle existe bien,
                        //=== si il n existe pas on supprime le tablea croisé. Exemple si il n'y a aucun contrat
                        //=== pour un ercicie donnée, celui n'apparaitra pas dans le filtre Execl du tab. croisée.
                        bExerciceIn = false;

                        foreach (var ope in oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).PivotItems)
                        {
                            stemp = ope.Name;

                            if (General.UCase(stemp) == General.UCase(tpExerciceExe[lexe].sLibelle))
                            {
                                bExerciceIn = true;
                                break;
                            }
                        }

                        if (bExerciceIn == true)
                        {
                            //=== dans la liste deroulante des exercices present sur Excel , on decoche les exercices quio ne correspondent pas à l'exercice en cours.

                            foreach (var ope in oSheet2.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).PivotItems)
                            {
                                stemp = ope.Name;

                                if (General.UCase(stemp) != General.UCase(tpExerciceExe[lexe].sLibelle))
                                {
                                    //If oPivItem.Visible = True Then
                                    ope.Visible = false;
                                    //oPivItem.value
                                    //End If
                                }
                            }
                        }
                        else
                        {
                            //=== supprime le tableau croisée.

                            oSheet2.PivotTables("Tableau croisé dynamique1").ClearTable();

                            oSheet2.Cells[4, 5].value = "Pas de contrat pour l'excercice du " + tpExerciceExe[lexe].dtDebut.ToShortDateString() + " au " + tpExerciceExe[lexe].dtFin.ToShortDateString();
                        }

                        if (PB1.Value <= (100 - 5))
                        {
                            PB1.Value = PB1.Value + 5;
                            PB1.PerformStep();
                        }
                        else
                        {
                            PB1.Value = 100;
                            PB1.PerformStep();
                        }
                    }
                }

                PB1.Value = 100;
                PB1.PerformStep();

                //=====================================================================================================================
                //==========================  CONTRATS RESILIES. ======================================================================

                sSQL = "SELECT   ANCD_Noauto, ANCE_Noauto, ANCD_AnalyCtrDetail.Codeimmeuble , Code1, ANCD_AnalyCtrDetail.NumContrat, Avenant, ANCD_CommMat," + " ANCD_CommInitiale, ANCD_CommNomComplet, " + " ANCD_RespExploitMat, ANCD_RespExploitInitiale, ANCD_RespExploitNomComplet," + " ANCD_DateSignature, ANCD_DateEffet, ANCD_Exercice, " + " ANCD_Indicateur , ANCD_Ht, ANCD_HtP1, ANCD_HtP2, ANCD_HtP3, ANCD_HtP4," + "  ANCD_24Ssur24, ANCD_Renovation, ANCD_Statut,ANCD_DateFin, " + " ANCC_CommRESILIE, ANCC_Reconduction, ANCC_RenovPerdue, ANCC_DateReceptResil " + " FROM         ANCD_AnalyCtrDetail LEFT OUTER JOIN" + " CONT_ContratComment ON ANCD_AnalyCtrDetail.Codeimmeuble = CONT_ContratComment.Codeimmeuble AND " + " ANCD_AnalyCtrDetail.NumContrat = CONT_ContratComment.NumContrat " + " WHERE    ANCE_Noauto = " + lANCE_Noauto + " and ANCD_Statut ='" + cResilie + "'" + " and ANCD_DateFin >= '" + dtDebut + "'" + " and ANCD_DateFin <='" + dtFin + "'" + " order by  ANCD_DateFin ";

                rsExp = ModAdo.fc_OpenRecordSet(sSQL);

                if (rsExp.Rows.Count == 0)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    //FloodHide
                    return;
                }

                dbIndex = Convert.ToDouble(100) / Convert.ToDouble(rsExp.Rows.Count);
                dbTot = 0;
                //=== ajoute une feuilles.
                oSheet3 = oWB.Worksheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);
                lnbSheet = lnbSheet + 1;
                //GoTo SUITE
                //Set oSheet3 = oWB.ActiveSheet

                //=== renomme la feuille et la colore en vert.
                sNameSheet3 = cSheetContratResilie;
                oSheet3.Name = sNameSheet3;
                oSheet3.Tab.Color = 255;
                //=== rouge.

                //== affiche les noms des champs.
                i = 1;

                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cIMMEUBLE;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cSYNDIC;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cNo;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cRC;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cRE;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cDATERECEPTRESILIATION;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cDATEEFFECTIVE;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cEXERCICE;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cINDICATEUR;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cMONTANT;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cCommentaires;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cCAUSERENOVPERDUE;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cRECONDUCTION;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cP1;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cP2;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cP3;
                i = i + 1;


                oSheet3.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet3.Cells[1, i].value = cP4;
                i = i + 1;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                lLigne = 2;


                //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ////    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""


                //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // MsgBox "depart"
                //  oSheet3.Cells(lLigne, 1).value = sANT_MatCode
                foreach (DataRow dr in rsExp.Rows)
                {
                    j = 1;


                    oSheet3.Cells[lLigne, j + cCol].value = dr["CodeImmeuble"] + "";
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["Code1"] + "";
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["NumContrat"] + "";
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_CommInitiale"] + "";
                    j = j + 1;


                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_RespExploitInitiale"] + "";
                    j = j + 1;

                    //=== champs à alimenter DATE RECEPTION RESILIATION.

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCC_DateReceptResil"] + "";
                    j = j + 1;


                    oSheet3.Cells[lLigne, j + cCol].value = Convert.ToDateTime(dr["ANCD_DateFin"] + "").ToShortDateString();
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_Exercice"] + "";
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_Indicateur"] + "";
                    //              If UCase(rsExp!ANCD_Indicateur) = UCase("<" & cLimiteHtCtr) Then
                    //                        oSheet3.Cells(lLigne, j + cCol].Font.Color = -16776961 '==rouge.
                    //              End If
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_Ht"] + "";
                    j = j + 1;

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCC_CommRESILIE"] + "";
                    // oSheet.Cells(lLigne, j + cCol].value = rsExp!ANCD_Commentaire & ""
                    j = j + 1;

                    //=== champs a alimenter CAUSE RENOV PERDUE.

                    if (Convert.ToInt32(General.nz(dr["ANCC_RenovPerdue"], 0)) == 1)
                    {
                        // MsgBox "renov1"

                        oSheet3.Cells[lLigne, j + cCol].value = "x";
                        // MsgBox "renov2"
                    }
                    else
                    {

                        oSheet3.Cells[lLigne, j + cCol].value = "";
                    }
                    j = j + 1;

                    //=== champs a alimenter RECONDUCTION.

                    if (Convert.ToInt32(General.nz(dr["ANCC_Reconduction"], 0)) == 1)
                    {
                        //   MsgBox "erconduc1"

                        oSheet3.Cells[lLigne, j + cCol].value = "x";
                        // MsgBox "erconduc2"
                    }
                    else
                    {

                        oSheet3.Cells[lLigne, j + cCol].value = "";
                    }
                    j = j + 1;



                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_HtP1"] + "";
                    j = j + 1;


                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_HtP2"] + "";
                    j = j + 1;

                    //oSheet3.Cells(lLigne, j + cCol].value = 100

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_HtP3"] + "";
                    j = j + 1;

                    //oSheet3.Cells(lLigne, j + cCol].value = 200

                    oSheet3.Cells[lLigne, j + cCol].value = dr["ANCD_HtP4"] + "";
                    j = j + 1;

                    lLigne = lLigne + 1;

                    dbTot = dbTot + dbIndex;
                    if (dbTot > 100)
                    {
                        PB1.Value = 100;
                    }
                    else
                    {
                        PB1.Value = Convert.ToInt32(dbTot);
                        PB1.PerformStep();
                    }
                }

                // MsgBox "avant format"

                ltotLigneRe = lLigne - 1;

                PB1.Value = 5;
                PB1.PerformStep();
                //==== Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet3.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                //======= formate l'entete de colonne.
                oResizeRange.Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlSolid;
                oResizeRange.Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;
                oResizeRange.Interior.ThemeColor = Microsoft.Office.Interop.Excel.XlThemeColor.xlThemeColorAccent2;
                oResizeRange.Interior.TintAndShade = 0.399975585192419;
                oResizeRange.Interior.PatternTintAndShade = 0;
                //oResizeRange.Interior.ColorIndex = 15
                //===formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                //=== aligne automatiquement les colonnes selon la taille du texte.
                oResizeRange.EntireColumn.AutoFit();

                oResizeRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
                oResizeRange.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.WrapText = false;
                oResizeRange.Orientation = 0;
                oResizeRange.AddIndent = false;
                oResizeRange.IndentLevel = 0;
                oResizeRange.ShrinkToFit = false;
                oResizeRange.ReadingOrder = Convert.ToInt32(Microsoft.Office.Interop.Excel.Constants.xlContext);
                oResizeRange.MergeCells = false;

                oResizeRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oResizeRange.WrapText = false;
                oResizeRange.Orientation = 0;
                oResizeRange.AddIndent = false;
                oResizeRange.IndentLevel = 0;
                oResizeRange.ShrinkToFit = false;
                oResizeRange.ReadingOrder = Convert.ToInt32(Microsoft.Office.Interop.Excel.Constants.xlContext);
                oResizeRange.MergeCells = false;


                oResizeRange.Rows["1:1"].RowHeight = 45;

                //=== color la colonne indicateur si montant >2000 €.
                oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions.Add(Microsoft.Office.Interop.Excel.XlFormatConditionType.xlCellValue, Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlEqual, "=\">2000€\"");
                var t = oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)];

                oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[t.FormatConditions.Count].SetFirstPriority();

                oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].Font.Color = -16383844;

                oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].Font.TintAndShade = 0;



                oSheet3.Range[cPosResINDICATEUR + 2, cPosINDICATEUR + (lLigne - 1)].FormatConditions[1].StopIfTrue = false;


                //=== color la colonne montant si montant >2000 €.
                oSheet3.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions.Add(Microsoft.Office.Interop.Excel.XlFormatConditionType.xlCellValue, Microsoft.Office.Interop.Excel.XlFormatConditionOperator.xlGreater, "=" + cLimiteHtCtr + "");
                var tt = oSheet3.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)];

                oSheet3.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[tt.FormatConditions.Count].SetFirstPriority();


                oSheet3.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].Font.Color = -16383844;

                oSheet3.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].Font.TintAndShade = 0;



                oSheet.Range[cPosResMONTANT + 2, cPosMONTANT + (lLigne - 1)].FormatConditions[1].StopIfTrue = false;


                //==== color une ligne sur deux.
                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions.Add(Type: Microsoft.Office.Interop.Excel.XlFormatConditionType.xlExpression, Formula1: "=MOD(LIGNE();2)");

                // oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions.Add(Type: Microsoft.Office.Interop.Excel.XlFormatConditionType.xlExpression, Formula1: "=MOD(ROW(),2)");

                var t3 = oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]];
                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[t3.FormatConditions.Count].SetFirstPriority();

                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;

                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.PatternColorIndex = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;

                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.ThemeColor = Microsoft.Office.Interop.Excel.XlThemeColor.xlThemeColorAccent2;

                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[1].Interior.TintAndShade = 0.799981688894314;

                oSheet3.Range[oSheet3.Cells[2, 1], oSheet3.Cells[lLigne - 1, j - 1]].FormatConditions[1].StopIfTrue = false;


                PB1.Value = 10;
                PB1.PerformStep();
                //==== insere des formule d pour sommmerles colonnes.
                sFormule = "=SUM(" + cPosResMONTANT + 2 + ":" + cPosResMONTANT + (lLigne - 1) + ")";
                oSheet3.Range[cPosResMONTANT + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cResPosP1 + 2 + ":" + cResPosP1 + (lLigne - 1) + ")";
                oSheet3.Range[cResPosP1 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cResPosP2 + 2 + ":" + cResPosP2 + (lLigne - 1) + ")";
                oSheet3.Range[cResPosP2 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cResPosP3 + 2 + ":" + cResPosP3 + (lLigne - 1) + ")";
                oSheet3.Range[cResPosP3 + lLigne].Formula = sFormule;

                sFormule = "=SUM(" + cResPosP4 + 2 + ":" + cResPosP4 + (lLigne - 1) + ")";
                oSheet3.Range[cResPosP4 + lLigne].Formula = sFormule;
                // sFormule = "IIF(" & cPosMONTANT & 2 > cLimiteHtCtr

                //=== encadre les formules ci dessus.
                oSheet3.Range[cPosResMONTANT + lLigne, cResPosP4 + lLigne].Borders.ColorIndex = 0;
                oSheet3.Range[cPosResMONTANT + lLigne, cResPosP4 + lLigne].Borders.TintAndShade = 0;
                oSheet3.Range[cPosResMONTANT + lLigne, cResPosP4 + lLigne].Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;


                //==== change la police en gras
                oSheet3.Range[cPosResIMMEUBLE + 2, cPosResNo + (lLigne - 1)].Font.Bold = true;
                oSheet3.Range[cPosResDATEEFFECTIVE + 2, cPosResINDICATEUR + (lLigne - 1)].Font.Bold = true;

                //=== chnane la taille de la police.
                oSheet3.Range[cPosResIMMEUBLE + 2, cResPosP4 + lLigne].Font.Name = "Calibri";
                oSheet3.Range[cPosIMMEUBLE + 2, cResPosP4 + lLigne].Font.Size = 10;

                //==== fige les volets.
                oSheet3.Range[cPosRC + 2].Activate();
                oXL.ActiveWindow.FreezePanes = true;

                //=== chyange la largeur des colonnes.

                oSheet3.Columns[cPosResMONTANT + ":" + cPosResMONTANT].ColumnWidth = 14.71;

                oSheet3.Columns[cResPosP1 + ":" + cResPosP1].ColumnWidth = 11.71;

                oSheet3.Columns[cResPosP2 + ":" + cResPosP2].ColumnWidth = 11.71;

                oSheet3.Columns[cResPosP3 + ":" + cResPosP3].ColumnWidth = 11.71;

                oSheet3.Columns[cResPosP4 + ":" + cResPosP4].ColumnWidth = 11.71;

                oSheet3.Columns[cPosResCommentaires + ":" + cPosResCommentaires].ColumnWidth = 56.43;

                oSheet3.Columns[cPosResNo + ":" + cPosResNo].ColumnWidth = 8.71;

                oSheet3.Columns[cPosResRC + ":" + cPosResRC].ColumnWidth = 8.71;

                oSheet3.Columns[cPosResRE + ":" + cPosResRE].ColumnWidth = 8.71;

                //==== centre les cellules des donnees.
                oSheet3.Range[cPosResIMMEUBLE + 2, cPosResINDICATEUR + (lLigne - 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet3.Range[cPosResIMMEUBLE + 2, cPosResINDICATEUR + (lLigne - 1)].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet3.Range[cPosResCAUSERENOVPERDUE + 2, cPosResRECONDUCTION + (lLigne - 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet3.Range[cPosResCAUSERENOVPERDUE + 2, cPosResRECONDUCTION + (lLigne - 1)].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                //=== renvoyer a la ligne automatiquement.
                oSheet3.Range[cPosIMMEUBLE + 1, cResPosP4 + (lLigne - 1)].WrapText = true;


                //==== centre les cellules des en tetes.
                oSheet3.Range[cPosIMMEUBLE + 1, cResPosP4 + 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                oSheet3.Range[cPosIMMEUBLE + 1, cResPosP4 + 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                //=== active les filtres pour chaque colonnes.

                oSheet3.Range[cPosIMMEUBLE + 1, cResPosP4 + 1].AutoFilter(1, Type.Missing, XlAutoFilterOperator.xlAnd, Type.Missing, true);

                PB1.Value = 15;
                PB1.PerformStep();
                //=== détail des contrats importées
                if (lDetailExercice == 1)
                {

                    sSQL = "SELECT     Code1, CodeImmeuble, NumContrat, Avenant, MatComm, InitailCom, NomCom, MatExploit," + " InitialExploit, NomExploit, DateDeSignature, DateEffet, " + " DateFin , LibelleCont1, LibelleCont2, CodeArticle, Designation1, BaseContractuelle, BaseActualisee, Resiliee " + " FROM         ANCDT_AnalyCtrData" + " WHERE    ANCE_Noauto = " + lANCE_Noauto + " and DateFin >= '" + dtDebut + "'" + " and DateFin <='" + dtFin + "'" + " order by DateFin ";

                    rsDet = ModAdo.fc_OpenRecordSet(sSQL);

                    if (rsDet.Rows.Count > 0)
                    {
                        oSheetDet2 = oWB.Sheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);
                        lnbSheet = lnbSheet + 1;

                        oSheetDet2.Name = General.Left(cDetail + " " + clibResilie, 31);
                        int h = 0;
                        var data = new object[rsDet.Rows.Count, rsDet.Columns.Count + 1];
                        foreach (DataRow dr in rsDet.Rows)
                        {
                            for (int k = 0; k < rsDet.Columns.Count; k++)
                            {
                                if (dr[k].GetType() == typeof(DateTime))
                                    data[h, k] = Convert.ToDateTime(dr[k]).ToShortDateString();
                                else
                                    data[h, k] = dr[k].ToString();
                            }
                            h++;
                        }
                        //oSheetDet2.Range["A3"].CopyFromRecordset(rsDet);//TODO. this methode throw an exception
                        oSheetDet2.Range[(Range)oSheetDet2.Cells[3, 1], (Range)oSheetDet2.Cells[rsDet.Rows.Count + 2, rsDet.Columns.Count + 1]].Value2 = data;
                        //=== titre de la feuille.
                        oSheetDet2.Range["D1"].FormulaR1C1 = General.UCase(cDetail + " " + clibResilie);
                        oSheetDet2.Range["D1"].Font.Size = 14;
                        oSheetDet2.Range["D1:F1"].MergeCells = true;
                        // oSheetDet2.Range["D1:F1").BorderAround xlContinuous, xlMedium, , -16776961
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Color = -16776961;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        // .ColorIndex = 0
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Color = -16776961;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Color = -16776961;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Color = -16776961;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheetDet2.Range["D1:F1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                    }
                    ModAdo.fc_CloseRecordset(rsDet);
                    rsDet = null;
                    PB1.Value = 20;
                    PB1.PerformStep();
                }

                //  MsgBox "avant tableau"

                for (m = 0; m <= 1; m++)
                {
                    for (lexe = 0; lexe < tpExerciceExe.Length; lexe++)
                    {
                        //======== INSERE UN TABHLEAU CROISEE DYNAMIQUE.
                        if (m == 0)
                        {
                            sNameSheet2 = cShettRESILPARRE + " " + General.Left(General.Replace(tpExerciceExe[lexe].sLibelle, "/", "-"), 31);
                        }
                        else
                        {

                            sNameSheet2 = cShettRESILPARRC + " " + General.Left(General.Replace(tpExerciceExe[lexe].sLibelle, "/", "-"), 31);
                        }

                        oSheet4 = oWB.Sheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);
                        lnbSheet = lnbSheet + 1;

                        //  oWB.PivotCaches.Create(SourceType:=xlDatabase, SourceData:= _
                        //"" & cSheetContratResilie & "!R1C1:R" & lLigne - 1 & "C17", version:=xlPivotTableVersion14).CreatePivotTable _
                        //TableDestination:="" & oSheet4.Name & "!R4C1", TableName:="Tableau croisé dynamique1", _
                        //DefaultVersion:=4  '4 =xlPivotTableVersion1


                        oWB.PivotCaches().Create(SourceType: Microsoft.Office.Interop.Excel.XlPivotTableSourceType.xlDatabase, SourceData: "" + cSheetContratResilie + "!R1C1:R" + (lLigne - 1) + "C17").CreatePivotTable(TableDestination: oWB.Sheets[oSheet4.Name].Range["A4"], TableName: "Tableau croisé dynamique1");
                        //, DefaultVersion:=4

                        oSheet4.Name = sNameSheet2;


                        oSheet4.Cells[3, 1].Select();

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlPageField;

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).Position = 1;

                        if (m == 0)
                        {
                            //=== par responsable exploit puis commercial.

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Position = 1;

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Position = 2;
                            sNamePivotField = cRE;

                        }
                        else
                        {
                            //=== par responsable commercial puis exploit.


                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRC).Position = 1;


                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                            oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cRE).Position = 2;
                            sNamePivotField = cRC;
                        }



                        oSheet4.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cP1), "Somme de P1", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);


                        oSheet4.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cP2), "Somme de P2", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);


                        oSheet4.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cP3), "Somme de P3", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);


                        oSheet4.PivotTables("Tableau croisé dynamique1").AddDataField(oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cP4), "Somme de P4", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                        //=== afficher les etiquettes d'éléments sous forme de tableau.

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(sNamePivotField).LayoutForm = Microsoft.Office.Interop.Excel.XlLayoutFormType.xlTabular;

                        //=== renomme la colonne ne RE.

                        oSheet4.PivotTables("Tableau croisé dynamique1").CompactLayoutRowHeader = sNamePivotField;

                        //=== applique un style au tableau.

                        oSheet4.PivotTables("Tableau croisé dynamique1").TableStyle2 = "PivotStyleMedium14";



                        //=== encadre les sous totaux dans le tableau croisé dynamique.


                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotSelect(sNamePivotField + "[All]", Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlLabelOnly & Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlFirstRow, true);

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotSelect(sNamePivotField + "[All;Total]", Microsoft.Office.Interop.Excel.XlPTSelectionMode.xlDataAndLabel, true);

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        var _with46 = oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft];

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].ColorIndex = 0;
                        //.Color = 16776961

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].ColorIndex = 0;
                        //.Color = 16776961

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].ColorIndex = 0;
                        //.Color = 16776961

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].ColorIndex = 0;
                        //.Color = 16776961

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        oXL.Selection.Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        //=== augmente le taille des lignes.
                        oSheet4.Cells.RowHeight = 24.75;

                        //=== augmente le taille des colonnes.
                        oSheet4.Cells.ColumnWidth = 16;

                        //==== saisie du titre sur 2 lignes
                        oSheet4.Range["D1"].FormulaR1C1 = General.UCase(clibResilie);
                        oSheet4.Range["D2"].FormulaR1C1 = "DU " + tpExerciceExe[lexe].dtDebut.ToShortDateString() + " AU " + tpExerciceExe[lexe].dtFin.ToShortDateString();
                        oSheet4.Range["D1:D2"].Font.Name = "Calibri";
                        oSheet4.Range["D1:D2"].Font.Size = 14;
                        oSheet4.Range["D1:D2"].Font.Color = -16776961;
                        //== rouge.
                        oSheet4.Range["D1:D2"].Font.Bold = true;

                        //=== alignement du titre
                        oSheet4.Range["D1:F2"].Font.Size = 14;
                        oSheet4.Range["D1:F2"].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet4.Range["D1:F2"].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet4.Range["D1:F1"].MergeCells = true;
                        oSheet4.Range["D2:F2"].MergeCells = true;
                        //oSheet4.Range["D1:F2").BorderAround xlContinuous, xlMedium, , -16776961
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Color = 16776961;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Color = 16776961;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Color = 16776961;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        //.ColorIndex = 0
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Color = 16776961;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                        oSheet4.Range["D1:F2"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                        //==== selectionne des valeurs dans la liste déroulante correspondant au filtre.

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).CurrentPage = "(All)";

                        oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).EnableMultiplePageItems = true;

                        //=== boucle sur tous les exercices present dans le filtre du fichier excel, controle
                        //=== si l'excercice dans le tableau tpExerciceExe(lexe).sLibelle existe bien,
                        //=== si il n existe pas on supprime le tablea croisé. Exemple si il n'y a aucun contrat
                        //=== pour un ercicie donnée, celui n'apparaitra pas dans le filtre Execl du tab. croisée.
                        bExerciceIn = false;

                        foreach (var opt in oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).PivotItems)
                        {
                            stemp = opt.Name;

                            if (General.UCase(stemp) == General.UCase(tpExerciceExe[lexe].sLibelle))
                            {
                                bExerciceIn = true;
                                break;
                            }
                        }

                        if (bExerciceIn == true)
                        {
                            //=== dans la liste deroulante des exercices present sur Excel , on decoche les exercices quio ne correspondent pas à l'exercice en cours.

                            foreach (var opt in oSheet4.PivotTables("Tableau croisé dynamique1").PivotFields(cEXERCICE).PivotItems)
                            {
                                stemp = opt.Name;
                                // If UCase(stemp) <> UCase("(blank)") Then
                                if (General.UCase(stemp) != General.UCase(tpExerciceExe[lexe].sLibelle))
                                {
                                    // If oPivItem.Visible = True Then
                                    opt.Visible = true;
                                    // End If
                                }
                                //  End If
                            }
                        }
                        else
                        {
                            //=== supprime le tableau croisée.

                            oSheet4.PivotTables("Tableau croisé dynamique1").ClearTable();

                            oSheet4.Cells[4, 5].value = "Pas de contrat pour l'excercice du " + tpExerciceExe[lexe].dtDebut.ToShortDateString() + " au " + tpExerciceExe[lexe].dtFin.ToShortDateString();
                        }
                        if (PB1.Value <= (100 - 5))
                        {
                            PB1.Value = PB1.Value + 5;
                            PB1.PerformStep();
                        }
                        else
                        {
                            PB1.Value = 100;
                            PB1.PerformStep();
                        }
                    }
                }

                //  MsgBox "apres tableau"
                if (lGraphExelComp == 1)
                {
                    //========= INSERTION D UN TABLEAU CROISE + GRAPHIQUE (resiliée + accepté).
                    //=== 1) acceptés
                    sNameSheetGraph = cSheetCompRE;
                    oSheetGraph = oWB.Sheets.Add(Type.Missing, oWB.Sheets[lnbSheet], Type.Missing, Type.Missing);
                    lnbSheet = lnbSheet + 1;

                    oWB.PivotCaches().Create(Microsoft.Office.Interop.Excel.XlPivotTableSourceType.xlDatabase, "" + cSheetContratAccepte + "!R1C1:R" + ltotLigneAc + "C17", Microsoft.Office.Interop.Excel.XlPivotTableVersionList.xlPivotTableVersion14).CreatePivotTable(TableDestination: "" + oSheetGraph.Name + "!R4C1", TableName: "Tableau croisé dynamique7", DefaultVersion: 4);
                    //4 =xlPivotTableVersion1
                    //  oSheetGraph.Name = cSheetCompRE

                    oShape = oSheetGraph.Shapes.AddChart();
                    oShape.Chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;

                    oShape.Chart.SetSourceData(Source: oSheetGraph.Range["A$1:$C$18"]);

                    oShape.Left = 300;
                    oShape.Top = 48;


                    oSheetGraph.PivotTables("Tableau croisé dynamique7").PivotFields("R.E.").Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                    oSheetGraph.PivotTables("Tableau croisé dynamique7").PivotFields("R.E.").Position = 1;

                    oSheetGraph.PivotTables("Tableau croisé dynamique7").AddDataField(oSheetGraph.PivotTables("Tableau croisé dynamique7").PivotFields(cMONTANT), "Somme de Montant", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                    oSheetGraph.PivotTables("Tableau croisé dynamique7").PivotFields(cDATEEFFECTIVE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlPageField;

                    oSheetGraph.PivotTables("Tableau croisé dynamique7").PivotFields(cDATEEFFECTIVE).Position = 1;

                    oShape.Chart.ClearToMatchStyle();
                    oShape.Chart.ChartStyle = 5;
                    oShape.Chart.ClearToMatchStyle();

                    //==== 2) reslises
                    oWB.PivotCaches().Create(Microsoft.Office.Interop.Excel.XlPivotTableSourceType.xlDatabase, "" + cSheetContratResilie + "!R1C1:R" + ltotLigneRe + "C17", Microsoft.Office.Interop.Excel.XlPivotTableVersionList.xlPivotTableVersion14).CreatePivotTable(TableDestination: "" + oSheetGraph.Name + "!R24C1", TableName: "Tableau croisé dynamique1", DefaultVersion: 4);
                    //4 =xlPivotTableVersion1
                    oSheetGraph.Name = cSheetCompRE;

                    oShape = oSheetGraph.Shapes.AddChart();
                    oShape.Chart.ChartType = Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered;

                    oShape.Chart.SetSourceData(Source: oSheetGraph.Range["A$1:$C$18"]);

                    oShape.Left = 390;
                    oShape.Top = 360;


                    oSheetGraph.PivotTables("Tableau croisé dynamique1").PivotFields("R.E.").Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlRowField;

                    oSheetGraph.PivotTables("Tableau croisé dynamique1").PivotFields("R.E.").Position = 1;


                    oSheetGraph.PivotTables("Tableau croisé dynamique1").AddDataField(oSheetGraph.PivotTables("Tableau croisé dynamique1").PivotFields(cMONTANT), "Somme de Montant", Microsoft.Office.Interop.Excel.XlConsolidationFunction.xlSum);

                    oSheetGraph.PivotTables("Tableau croisé dynamique1").PivotFields(cDATEEFFECTIVE).Orientation = Microsoft.Office.Interop.Excel.XlPivotFieldOrientation.xlPageField;

                    oSheetGraph.PivotTables("Tableau croisé dynamique1").PivotFields(cDATEEFFECTIVE).Position = 1;

                    oShape.Chart.ClearToMatchStyle();
                    oShape.Chart.ChartStyle = 5;
                    oShape.Chart.ClearToMatchStyle();


                    //==== saisie du titre sur 2 lignes
                    //oSheetGraph.Range["D1"].FormulaR1C1 = UCase(clibResilie)
                    oSheetGraph.Range["F1"].FormulaR1C1 = "DU " + tpExerciceExe[0].dtDebut.ToShortDateString() + " AU " + tpExerciceExe[tpExerciceExe.Length - 1].dtFin.ToShortDateString();
                    oSheetGraph.Range["F1:F3"].Font.Name = "Calibri";
                    oSheetGraph.Range["F1:F3"].Font.Size = 14;
                    oSheetGraph.Range["F1:F3"].Font.Bold = true;
                    oSheetGraph.Range["A1:K1"].MergeCells = true;
                    // oSheetGraph.Range["A1:K1").BorderAround xlContinuous, xlMedium, , -16776961

                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalDown].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlDiagonalUp].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;

                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    //.ColorIndex = 0
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Color = 16776961;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].TintAndShade = 0;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    //.ColorIndex = 0
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Color = 16776961;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].TintAndShade = 0;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    //.ColorIndex = 0
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Color = 16776961;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].TintAndShade = 0;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                    //.ColorIndex = 0
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Color = 16776961;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].TintAndShade = 0;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideVertical].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                    oSheetGraph.Range["A1:K1"].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlInsideHorizontal].LineStyle = Microsoft.Office.Interop.Excel.Constants.xlNone;
                    oSheetGraph.Range["A1:K1"].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                    oSheetGraph.Range["A1:K1"].VerticalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                }

                oWB.Sheets[1].Select();

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;

                oResizeRange = null;

                oShape = null;

                oSheet = null;

                oSheet2 = null;

                oSheet3 = null;

                oSheet4 = null;

                oSheetDet = null;

                oSheetDet2 = null;

                oSheetGraph = null;

                oWB = null;

                oXL = null;

                PB1.Value = 100;
                PB1.PerformStep();
                PB1.Visible = false;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                //_with1.Close();

                ///=> With rsExp


                rs = null;

                PB1.Value = 100;
                PB1.Visible = false;
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                PB1.Value = 100;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Error: " + ex.HResult, MessageBoxButtons.OK, MessageBoxIcon.Information);
                //FloodHide
                PB1.Visible = false;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_clear()
        {

            txtANCE_CreePar.Text = "";
            txtANCE_CreeLe.Text = "";
            txtANCE_DateArret.Text = "";
            txtANCE_Commentaire.Text = "";
            txtANCE_Noauto.Text = "";
            lblDataDate.Text = "";
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sMoisFin"></param>
        private void fc_ChargeExercices(string sMoisFin)
        {

            string sqlExercices = null;
            DataTable rsExercices = default(DataTable);
            string strAddItem = null;
            string JourMaxiMois = null;
            string DateMini = null;
            string DateMaxi = null;
            int i = 0;
            bool blnMatch = false;


            int X = 0;

            General.tpExerciceCtr = null;

            blnMatch = false;

            SAGE.fc_OpenConnSage();

            rsExercices = new DataTable();

            sqlExercices = "SELECT DISTINCT YEAR(JM_Date) AS Annee";
            sqlExercices = sqlExercices + " FROM F_JMOUV";
            sqlExercices = sqlExercices + " ORDER BY YEAR(JM_Date) DESC";
            var ModAdo = new ModAdo();
            rsExercices = ModAdo.fc_OpenRecordSet(sqlExercices, null, "", SAGE.adoSage);

            /* ce code est ajouté dans le traitement de la boucle pour eviter un bug
            JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt16(sMoisFin)));

            if (sMoisFin == "12" || !General.IsNumeric(sMoisFin))
            {
                DateMini = "01/" + "01" + "/";
                DateMaxi = JourMaxiMois + "/12/";
            }
            else
            {
                if (sMoisFin == "12")
                {
                    DateMini = "01/" + "01" + "/";
                    DateMaxi = JourMaxiMois + "/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin)), "00") + "/";
                    //MoisFinExercice
                }
                else
                {
                    DateMini = "01/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin) + 1), "00") + "/";
                    //MoisFinExercice - 1
                    DateMaxi = JourMaxiMois + "/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin)), "00") + "/";
                    //MoisFinExercice
                }
            }*/

            X = 0;

            foreach (DataRow dr in rsExercices.Rows)
            {
                JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt32(sMoisFin),Convert.ToInt32(General.nz(dr["Annee"],0))));

                if (sMoisFin == "12" || !General.IsNumeric(sMoisFin))
                {
                    DateMini = "01/" + "01" + "/";
                    DateMaxi = JourMaxiMois + "/12/";
                }
                else
                {
                    if (sMoisFin == "12")
                    {
                        DateMini = "01/" + "01" + "/";
                        DateMaxi = JourMaxiMois + "/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin)), "00") + "/";
                        //MoisFinExercice
                    }
                    else
                    {
                        DateMini = "01/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin) + 1), "00") + "/";
                        //MoisFinExercice - 1
                        DateMaxi = JourMaxiMois + "/" + string.Format(Convert.ToString(Convert.ToInt16(sMoisFin)), "00") + "/";
                        //MoisFinExercice
                    }
                }
                if (Convert.ToInt32(dr["Annee"]) >= DateTime.Today.Year)
                {
                    if (sMoisFin != "12")
                    {

                        if (blnMatch == false && Convert.ToInt16(General.nz(sMoisFin, "1")) < DateTime.Now.Month)
                        {
                            Array.Resize(ref General.tpExerciceCtr, X + 1);
                            General.tpExerciceCtr[X].sLibelle = dr["Annee"] + "/" + (Convert.ToInt32(dr["Annee"]) + 1);
                            General.tpExerciceCtr[X].dtDebut = Convert.ToDateTime(DateMini + dr["Annee"]);
                            General.tpExerciceCtr[X].dtFin = Convert.ToDateTime(DateMaxi + (Convert.ToInt32(dr["Annee"]) + 1));
                            blnMatch = true;

                            X = X + 1;
                        }

                        Array.Resize(ref General.tpExerciceCtr, X + 1);
                        General.tpExerciceCtr[X].sLibelle = (Convert.ToInt32(dr["Annee"]) - 1) + "/" + (Convert.ToInt32(dr["Annee"]));
                        General.tpExerciceCtr[X].dtDebut = Convert.ToDateTime(DateMini + (Convert.ToInt32(dr["Annee"]) - 1));
                        General.tpExerciceCtr[X].dtFin = Convert.ToDateTime(DateMaxi + (dr["Annee"]));

                    }
                    else if (sMoisFin == "12")
                    {
                        Array.Resize(ref General.tpExerciceCtr, X + 1);
                        General.tpExerciceCtr[X].sLibelle = dr["Annee"] + "";
                        General.tpExerciceCtr[X].dtDebut = Convert.ToDateTime(DateMini + dr["Annee"]);
                        General.tpExerciceCtr[X].dtFin = Convert.ToDateTime(DateMaxi + dr["Annee"]);

                    }
                }
                else
                {
                    if (sMoisFin == "12")
                    {
                        Array.Resize(ref General.tpExerciceCtr, X + 1);
                        General.tpExerciceCtr[X].sLibelle = dr["Annee"] + "";
                        General.tpExerciceCtr[X].dtDebut = Convert.ToDateTime(DateMini + dr["Annee"]);
                        General.tpExerciceCtr[X].dtFin = Convert.ToDateTime(DateMaxi + dr["Annee"]);

                    }
                    else
                    {
                        Array.Resize(ref General.tpExerciceCtr, X + 1);
                        General.tpExerciceCtr[X].sLibelle = (Convert.ToInt32(dr["Annee"]) - 1) + "/" + dr["Annee"];
                        General.tpExerciceCtr[X].dtDebut = Convert.ToDateTime(DateMini + (Convert.ToInt32(dr["Annee"]) - 1));
                        General.tpExerciceCtr[X].dtFin = Convert.ToDateTime(DateMaxi + (dr["Annee"]));

                    }
                }
                X = X + 1;
            }
            SAGE.fc_CloseConnSage();
        }

        #endregion

        #region events
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            FraArret.Visible = true;
            ssGridANCD.Visible = false;
            txtDateArrete.Text = Convert.ToString(DateTime.Now.ToShortDateString());
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdCalcul_Click(object sender, EventArgs e)
        {
            try
            {
                blnErreur = false;

                if (fc_controle() == true)
                {
                    FraArret.Visible = false;
                    ssGridANCD.Visible = true;
                    return;
                }

                FraArret.Visible = false;
                ssGridANCD.Visible = true;
                //=== lance le calcul.
                fc_CalculCtr(Convert.ToDateTime(txtDateArrete.Text), txtcommentaire.Text);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierPorteFeuille);
                }
                //=== aliment le treeview.
                fc_LoadTreev();
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                fc_Err("", 0, "", 0, ex.Message);
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierPorteFeuille);
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
        }

        
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            try
            {
                General.sSQL = "UPDATE    ANCE_AnalyCtrEntete" + " Set ANCE_Commentaire ='" + StdSQLchaine.gFr_DoublerQuote(txtANCE_Commentaire.Text) + "'" + " WHERE ANCE_Noauto = " + General.nz(txtANCE_Noauto.Text, 0);

                General.Execute(General.sSQL);

                ssGridANCD.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdSauver_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSup_Click(object sender, EventArgs e)
        {
            UltraTreeNode oNode = null;
            int lANCE_Noauto = 0;
            string sSQL = null;
            bool bFind = false;
            try
            {
                foreach (UltraTreeNode nodeParent in TreeArret.Nodes)
                    foreach (UltraTreeNode node in nodeParent.Nodes)
                    {
                        oNode = node;
                        if (oNode.IsActive)
                        {
                            bFind = true;
                            break;
                        }
                    }
                if (bFind == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous supprimer  la date d'arrêté du " + oNode.Text + " ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                {
                    return;
                }
                lANCE_Noauto = Convert.ToInt32(General.Right(oNode.Key, General.Len(oNode.Key) - 1));

                sSQL = "DELETE FROM ANCDT_AnalyCtrData WHERE  ANCE_Noauto =" + lANCE_Noauto;
                General.Execute(sSQL);

                sSQL = "DELETE FROM ANCD_AnalyCtrDetail WHERE  ANCE_Noauto =" + lANCE_Noauto;
                General.Execute(sSQL);

                sSQL = "DELETE FROM ANCE_AnalyCtrEntete Where ANCE_Noauto =" + lANCE_Noauto;
                General.Execute(sSQL);
                //TreeArret_AfterSelect(oNode,new SelectEventArgs(new SelectedNodesCollection() {new object[1] {oNode}}));
                fc_LoadTreev();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeArret_AfterSelect(object sender, SelectEventArgs e)
        {
            if (e.NewSelections.Count > 0)
            {
                UltraTreeNode Node = e.NewSelections[0];


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv2))
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    fc_LoadANCE(Convert.ToInt32(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    fc_LoadANCD(Convert.ToInt32(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                }
                else
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    fc_LoadANCE(0);

                    fc_LoadANCD(0);

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    fc_clear();
                }
                Node.Selected = true;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            else
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                fc_LoadANCE(0);

                fc_LoadANCD(0);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                fc_clear();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdvisu_Click(object sender, EventArgs e)
        {
            UltraTreeNode oNode = null;
            bool bFind = false;
            bool bDepart = false;

            int X = 0;
            int lTotExercice = 0;
            int Y = 0;

            string dtDebut = null;
            string dtFin = null;
            try
            {
                foreach (UltraTreeNode nodeParent in TreeArret.Nodes)
                    foreach (UltraTreeNode node in nodeParent.Nodes)
                    {
                    oNode = node;
                    if (oNode.IsActive)
                    {
                        bFind = true;
                        break;
                    }
                }

                if (bFind == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous lancer un export sous Excel la date d'arrêté du " + oNode.Text + " ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }


                General.sExerciceDeb = "";
                General.sExerciceFin = "";
                General.lDetailExercice = 0;
                General.lGraphExelComp = 0;
                frmExerciceContrat frm = new frmExerciceContrat();
                frm.ShowDialog();
                if (string.IsNullOrEmpty(General.sExerciceDeb) || string.IsNullOrEmpty(General.sExerciceFin))
                {
                    return;
                }

                lTotExercice = 0;
                bDepart = false;
                tpExerciceExe = null;
                Y = 0;
                for (X = General.tpExerciceCtr.Length - 1; X >= 0; X--)
                {
                    bDepart = false;

                    if (General.UCase(General.sExerciceDeb) == General.UCase(General.tpExerciceCtr[X].sLibelle))
                    {
                        lTotExercice = lTotExercice + 1;
                        dtDebut = Convert.ToString(General.tpExerciceCtr[X].dtDebut);
                        Array.Resize(ref tpExerciceExe, Y + 1);
                        tpExerciceExe[Y].sLibelle = General.tpExerciceCtr[X].sLibelle;
                        tpExerciceExe[Y].dtDebut = General.tpExerciceCtr[X].dtDebut;
                        tpExerciceExe[Y].dtFin = General.tpExerciceCtr[X].dtFin;
                        Y = Y + 1;
                        bDepart = true;
                        if (General.UCase(General.sExerciceFin) == General.UCase(General.tpExerciceCtr[X].sLibelle))
                        {
                            dtFin = Convert.ToString(General.tpExerciceCtr[X].dtFin);
                            break;
                        }
                    }

                    if (General.UCase(General.sExerciceFin) == General.UCase(General.tpExerciceCtr[X].sLibelle))
                    {
                        lTotExercice = lTotExercice + 1;
                        dtFin = Convert.ToString(General.tpExerciceCtr[X].dtFin);
                        Array.Resize(ref tpExerciceExe, Y + 1);
                        tpExerciceExe[Y].sLibelle = General.tpExerciceCtr[X].sLibelle;
                        tpExerciceExe[Y].dtDebut = General.tpExerciceCtr[X].dtDebut;
                        tpExerciceExe[Y].dtFin = General.tpExerciceCtr[X].dtFin;
                        Y = Y + 1;
                        break;
                    }

                    if (Y > 0 & bDepart == false)
                    {
                        lTotExercice = lTotExercice + 1;
                        Array.Resize(ref tpExerciceExe, Y + 1);
                        tpExerciceExe[Y].sLibelle = General.tpExerciceCtr[X].sLibelle;
                        tpExerciceExe[Y].dtDebut = General.tpExerciceCtr[X].dtDebut;
                        tpExerciceExe[Y].dtFin = General.tpExerciceCtr[X].dtFin;
                        Y = Y + 1;
                    }

                }
                if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv2))
                {
                    fc_Export(Convert.ToInt32(General.Right(oNode.Key, General.Len(oNode.Key) - 1)), dtDebut, Convert.ToDateTime(dtFin), lTotExercice, General.lDetailExercice, General.lGraphExelComp);
                }

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridANCD_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            foreach (var column in ssGridANCD.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key.ToLower() == "ANCC_CommACCEPTE".ToLower() ||
                    column.Key.ToLower() == "ANCC_CommRESILIE".ToLower() ||
                    column.Key.ToLower() == "ANCC_DateReceptResil".ToLower() ||
                    column.Key.ToLower() == "ANCC_Reconduction".ToLower() ||
                    column.Key.ToLower() == "ANCC_RenovPerdue".ToLower())
                {
                    column.CellActivation = Activation.AllowEdit;
                }
                else
                {
                    column.CellActivation = Activation.NoEdit;
                    column.CellAppearance.ForeColor = Color.Blue;
                }
            }
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Noauto"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCE_Noauto"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCE_Noauto"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["Avenant"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Commentaire"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Statut"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["CON_Noauto"].Hidden = true;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_CommACCEPTE"].Header.Caption = "Commentaire sur contrat accepté";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_CommRESILIE"].Header.Caption = "Commentaire sur contrat résilié";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_DateReceptResil"].Header.Caption = "Date réception résiliation";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_Reconduction"].Header.Caption = "Reconduction";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_RenovPerdue"].Header.Caption = "Cause RENOV PERDUE";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_Reconduction"].Style = ColumnStyle.CheckBox;
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCC_RenovPerdue"].Style = ColumnStyle.CheckBox;
            ssGridANCD.DisplayLayout.Bands[0].Columns["Code1"].Header.Caption = "Code Gerant";
            ssGridANCD.DisplayLayout.Bands[0].Columns["NumContrat"].Header.Caption = "N° Contrat";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_CommMat"].Header.Caption = "Comm. Mat";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_CommInitiale"].Header.Caption = "Comm. Init";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_CommNomComplet"].Header.Caption = "Comm. Nom";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_RespExploitMat"].Header.Caption = "R.E. Mat";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_RespExploitInitiale"].Header.Caption = "R.E. Init";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_RespExploitNomComplet"].Header.Caption = "R.E. Nom";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_DateSignature"].Header.Caption = "Date Signature";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_DateEffet"].Header.Caption = "Date Effet";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_DateFin"].Header.Caption = "Date Fin";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Exercice"].Header.Caption = "Exercice";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Indicateur"].Header.Caption = "Indicateur";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Ht"].Header.Caption = "Ht";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_HtP1"].Header.Caption = "Ht P1";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_HtP2"].Header.Caption = "Ht P2";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_HtP3"].Header.Caption = "Ht P3";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_HtP4"].Header.Caption = "Ht P4";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_24Ssur24"].Header.Caption = "24/24";
            ssGridANCD.DisplayLayout.Bands[0].Columns["ANCD_Renovation"].Header.Caption = "Renovation GAGN2";
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            FraArret.Visible = false;
            ssGridANCD.Visible = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserAnalyseContrat_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            //=== alimente le treevieuw.
            fc_LoadTreev();
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserAnalyseContrat");
            fc_ChargeExercices(General.MoisFinExercice);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridANCD_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (ssGridANCD.ActiveCell.Column.Index == ssGridANCD.ActiveRow.Cells["ANCC_Reconduction"].Column.Index)
                {
                    if (ssGridANCD.ActiveRow.Cells["ANCC_Reconduction"].Text == "-1")
                    {
                        ssGridANCD.ActiveRow.Cells["ANCC_Reconduction"].Value = 1;
                    }
                }

                if (ssGridANCD.ActiveCell.Column.Index == ssGridANCD.ActiveRow.Cells["ANCC_RenovPerdue"].Column.Index)
                {
                    if (ssGridANCD.ActiveRow.Cells["ANCC_RenovPerdue"].Text == "-1")
                    {
                        ssGridANCD.ActiveRow.Cells["ANCC_RenovPerdue"].Value = 1;
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Erreurs.gFr_debug(ex, this.Name + ";ssGridANCD_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridANCD_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (General.nz(General.UCase(e.Row.Cells["ANCD_Statut"].Text), "0") + "" == General.UCase(cResilie))
            {
                e.Row.Appearance.BackColor = Color.FromArgb(255, 178, 178);

            }
            if (e.Row.Cells["ANCC_Reconduction"].Value != DBNull.Value)
                if (Convert.ToBoolean(e.Row.Cells["ANCC_Reconduction"].Value))
                    e.Row.Cells["ANCC_Reconduction"].Value = true;
                else
                    e.Row.Cells["ANCC_Reconduction"].Value = false;
            else
                e.Row.Cells["ANCC_Reconduction"].Value = false;
            if (e.Row.Cells["ANCC_RenovPerdue"].Value != DBNull.Value)
                if (Convert.ToBoolean(e.Row.Cells["ANCC_RenovPerdue"].Value))
                    e.Row.Cells["ANCC_RenovPerdue"].Value = true;
                else
                    e.Row.Cells["ANCC_RenovPerdue"].Value = false;
            else
                e.Row.Cells["ANCC_RenovPerdue"].Value = false;

            e.Row.Update();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridANCD_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridANCD_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["ANCC_CommACCEPTE"].DataChanged || e.Row.Cells["ANCC_CommRESILIE"].DataChanged || e.Row.Cells["ANCC_DateReceptResil"].DataChanged || e.Row.Cells["ANCC_Reconduction"].DataChanged || e.Row.Cells["ANCC_RenovPerdue"].DataChanged)
            {
                string Date = "NULL";
                if (!string.IsNullOrEmpty(e.Row.Cells["ANCC_DateReceptResil"].Value.ToString()))
                    Date = "'"+e.Row.Cells["ANCC_DateReceptResil"].Value.ToString()+"'";

                string req = "UPDATE CONT_ContratComment SET ANCC_CommACCEPTE='" + e.Row.Cells["ANCC_CommACCEPTE"].Value +
                             "',ANCC_CommRESILIE='" + e.Row.Cells["ANCC_CommRESILIE"].Value + "',ANCC_DateReceptResil=" + Date+ ",ANCC_Reconduction='" +
                             e.Row.Cells["ANCC_Reconduction"].Value + "',ANCC_RenovPerdue='" +
                             e.Row.Cells["ANCC_RenovPerdue"].Value + "' WHERE Codeimmeuble='" +
                             e.Row.Cells["Codeimmeuble"].Value + "' AND NumContrat='" + e.Row.Cells["NumContrat"].Value + "'";
                General.Execute(req);
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel3_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtANCE_DateArret_TextChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void txtANCE_CreePar_TextChanged(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel5_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void ultraGroupBox1_Click(object sender, EventArgs e)
        {

        }

        private void SSOleDBGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

        private void SSOleDBGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

        }

        private void FraArret_Click(object sender, EventArgs e)
        {

        }

        private void txtcommentaire_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtDateArrete_TextChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label33_Click(object sender, EventArgs e)
        {

        }

        private void PB1_Click(object sender, EventArgs e)
        {

        }

        private void lblDataDate_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel6_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void txtTotal_TextChanged(object sender, EventArgs e)
        {

        }

        private void ultraGroupBox2_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtANCE_CreeLe_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtANCE_Noauto_TextChanged(object sender, EventArgs e)
        {

        }

        private void ultraFormattedLinkLabel1_LinkClicked(object sender, Infragistics.Win.FormattedLinkLabel.LinkClickedEventArgs e)
        {

        }

        #endregion
    }
}
