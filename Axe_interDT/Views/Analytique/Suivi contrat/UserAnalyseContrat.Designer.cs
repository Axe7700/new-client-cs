namespace Axe_interDT.Views.Analytique
{
    partial class UserAnalyseContrat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.Button CmdSauver;
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtANCE_DateArret = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtANCE_CreePar = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtANCE_Commentaire = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ssGridANCD = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBGrid2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.FraArret = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtcommentaire = new iTalk.iTalk_TextBox_Small2();
            this.Command1 = new System.Windows.Forms.Button();
            this.CmdCalcul = new System.Windows.Forms.Button();
            this.txtDateArrete = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.ultraFormattedLinkLabel1 = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.PB1 = new System.Windows.Forms.ProgressBar();
            this.lblDataDate = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTotal = new iTalk.iTalk_TextBox_Small2();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.TreeArret = new Infragistics.Win.UltraWinTree.UltraTree();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdSup = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdvisu = new System.Windows.Forms.Button();
            this.txtANCE_CreeLe = new iTalk.iTalk_TextBox_Small2();
            this.txtANCE_Noauto = new iTalk.iTalk_TextBox_Small2();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            CmdSauver = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridANCD)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraArret)).BeginInit();
            this.FraArret.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeArret)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CmdSauver
            // 
            CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            CmdSauver.FlatAppearance.BorderSize = 0;
            CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            CmdSauver.Location = new System.Drawing.Point(1718, 2);
            CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            CmdSauver.Name = "CmdSauver";
            CmdSauver.Size = new System.Drawing.Size(60, 35);
            CmdSauver.TabIndex = 395;
            this.toolTip1.SetToolTip(CmdSauver, "Enregistrer");
            CmdSauver.UseVisualStyleBackColor = false;
            CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 46F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel1_Paint);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 77.77778F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox2, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 49);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 905);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel2_Paint);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.ultraGroupBox1, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.PB1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblDataDate, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 0, 5);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(412, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1429, 899);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel3_Paint);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 98F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.txtANCE_DateArret, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtANCE_CreePar, 3, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 42);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1423, 30);
            this.tableLayoutPanel4.TabIndex = 0;
            this.tableLayoutPanel4.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel4_Paint);
            // 
            // txtANCE_DateArret
            // 
            this.txtANCE_DateArret.AccAcceptNumbersOnly = false;
            this.txtANCE_DateArret.AccAllowComma = false;
            this.txtANCE_DateArret.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCE_DateArret.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCE_DateArret.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCE_DateArret.AccHidenValue = "";
            this.txtANCE_DateArret.AccNotAllowedChars = null;
            this.txtANCE_DateArret.AccReadOnly = false;
            this.txtANCE_DateArret.AccReadOnlyAllowDelete = false;
            this.txtANCE_DateArret.AccRequired = false;
            this.txtANCE_DateArret.BackColor = System.Drawing.Color.White;
            this.txtANCE_DateArret.CustomBackColor = System.Drawing.Color.White;
            this.txtANCE_DateArret.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCE_DateArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCE_DateArret.ForeColor = System.Drawing.Color.Black;
            this.txtANCE_DateArret.Location = new System.Drawing.Point(133, 2);
            this.txtANCE_DateArret.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCE_DateArret.MaxLength = 32767;
            this.txtANCE_DateArret.Multiline = false;
            this.txtANCE_DateArret.Name = "txtANCE_DateArret";
            this.txtANCE_DateArret.ReadOnly = true;
            this.txtANCE_DateArret.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCE_DateArret.Size = new System.Drawing.Size(593, 27);
            this.txtANCE_DateArret.TabIndex = 502;
            this.txtANCE_DateArret.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCE_DateArret.UseSystemPasswordChar = false;
            this.txtANCE_DateArret.TextChanged += new System.EventHandler(this.txtANCE_DateArret_TextChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 30);
            this.label2.TabIndex = 396;
            this.label2.Text = "Date arrêt";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(731, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(92, 30);
            this.label1.TabIndex = 396;
            this.label1.Text = "Par";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtANCE_CreePar
            // 
            this.txtANCE_CreePar.AccAcceptNumbersOnly = false;
            this.txtANCE_CreePar.AccAllowComma = false;
            this.txtANCE_CreePar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCE_CreePar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCE_CreePar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCE_CreePar.AccHidenValue = "";
            this.txtANCE_CreePar.AccNotAllowedChars = null;
            this.txtANCE_CreePar.AccReadOnly = false;
            this.txtANCE_CreePar.AccReadOnlyAllowDelete = false;
            this.txtANCE_CreePar.AccRequired = false;
            this.txtANCE_CreePar.BackColor = System.Drawing.Color.White;
            this.txtANCE_CreePar.CustomBackColor = System.Drawing.Color.White;
            this.txtANCE_CreePar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCE_CreePar.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCE_CreePar.ForeColor = System.Drawing.Color.Black;
            this.txtANCE_CreePar.Location = new System.Drawing.Point(828, 2);
            this.txtANCE_CreePar.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCE_CreePar.MaxLength = 32767;
            this.txtANCE_CreePar.Multiline = false;
            this.txtANCE_CreePar.Name = "txtANCE_CreePar";
            this.txtANCE_CreePar.ReadOnly = true;
            this.txtANCE_CreePar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCE_CreePar.Size = new System.Drawing.Size(593, 27);
            this.txtANCE_CreePar.TabIndex = 502;
            this.txtANCE_CreePar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCE_CreePar.UseSystemPasswordChar = false;
            this.txtANCE_CreePar.TextChanged += new System.EventHandler(this.txtANCE_CreePar_TextChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.txtANCE_Commentaire, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 78);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1423, 46);
            this.tableLayoutPanel5.TabIndex = 1;
            this.tableLayoutPanel5.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel5_Paint);
            // 
            // txtANCE_Commentaire
            // 
            this.txtANCE_Commentaire.AccAcceptNumbersOnly = false;
            this.txtANCE_Commentaire.AccAllowComma = false;
            this.txtANCE_Commentaire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCE_Commentaire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCE_Commentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCE_Commentaire.AccHidenValue = "";
            this.txtANCE_Commentaire.AccNotAllowedChars = null;
            this.txtANCE_Commentaire.AccReadOnly = false;
            this.txtANCE_Commentaire.AccReadOnlyAllowDelete = false;
            this.txtANCE_Commentaire.AccRequired = false;
            this.txtANCE_Commentaire.BackColor = System.Drawing.Color.White;
            this.txtANCE_Commentaire.CustomBackColor = System.Drawing.Color.White;
            this.txtANCE_Commentaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCE_Commentaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCE_Commentaire.ForeColor = System.Drawing.Color.Black;
            this.txtANCE_Commentaire.Location = new System.Drawing.Point(135, 2);
            this.txtANCE_Commentaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCE_Commentaire.MaxLength = 32767;
            this.txtANCE_Commentaire.Multiline = true;
            this.txtANCE_Commentaire.Name = "txtANCE_Commentaire";
            this.txtANCE_Commentaire.ReadOnly = false;
            this.txtANCE_Commentaire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCE_Commentaire.Size = new System.Drawing.Size(1286, 42);
            this.txtANCE_Commentaire.TabIndex = 502;
            this.txtANCE_Commentaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCE_Commentaire.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(127, 46);
            this.label3.TabIndex = 396;
            this.label3.Text = "Titre/ Observation";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox1.Controls.Add(this.ssGridANCD);
            this.ultraGroupBox1.Controls.Add(this.SSOleDBGrid1);
            this.ultraGroupBox1.Controls.Add(this.SSOleDBGrid2);
            this.ultraGroupBox1.Controls.Add(this.FraArret);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Location = new System.Drawing.Point(0, 156);
            this.ultraGroupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1429, 703);
            this.ultraGroupBox1.TabIndex = 2;
            this.ultraGroupBox1.Click += new System.EventHandler(this.ultraGroupBox1_Click);
            // 
            // ssGridANCD
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridANCD.DisplayLayout.Appearance = appearance1;
            this.ssGridANCD.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridANCD.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridANCD.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridANCD.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridANCD.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridANCD.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridANCD.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridANCD.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridANCD.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridANCD.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridANCD.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridANCD.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridANCD.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridANCD.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridANCD.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridANCD.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridANCD.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridANCD.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridANCD.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridANCD.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridANCD.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridANCD.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridANCD.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridANCD.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridANCD.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridANCD.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridANCD.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridANCD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridANCD.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssGridANCD.Location = new System.Drawing.Point(0, 0);
            this.ssGridANCD.Name = "ssGridANCD";
            this.ssGridANCD.Size = new System.Drawing.Size(1429, 703);
            this.ssGridANCD.TabIndex = 577;
            this.ssGridANCD.Text = "ultraGrid1";
            this.ssGridANCD.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridANCD_InitializeLayout);
            this.ssGridANCD.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridANCD_InitializeRow);
            this.ssGridANCD.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.ssGridANCD_BeforeRowUpdate);
            this.ssGridANCD.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssGridANCD_BeforeExitEditMode);
            this.ssGridANCD.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridANCD_BeforeRowsDeleted);
            // 
            // SSOleDBGrid1
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance13;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance23;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBGrid1.Location = new System.Drawing.Point(214, 322);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(186, 22);
            this.SSOleDBGrid1.TabIndex = 575;
            this.SSOleDBGrid1.Visible = false;
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            // 
            // SSOleDBGrid2
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid2.DisplayLayout.Appearance = appearance25;
            this.SSOleDBGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.SSOleDBGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.SSOleDBGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid2.DisplayLayout.Override.CellAppearance = appearance32;
            this.SSOleDBGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid2.DisplayLayout.Override.RowAppearance = appearance35;
            this.SSOleDBGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.SSOleDBGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBGrid2.Location = new System.Drawing.Point(214, 294);
            this.SSOleDBGrid2.Name = "SSOleDBGrid2";
            this.SSOleDBGrid2.Size = new System.Drawing.Size(186, 22);
            this.SSOleDBGrid2.TabIndex = 574;
            this.SSOleDBGrid2.Visible = false;
            this.SSOleDBGrid2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid2_InitializeLayout);
            // 
            // FraArret
            // 
            this.FraArret.AlphaBlendMode = Infragistics.Win.AlphaBlendMode.Standard;
            appearance37.BorderColor = System.Drawing.SystemColors.MenuHighlight;
            appearance37.BorderColor2 = System.Drawing.SystemColors.Highlight;
            appearance37.BorderColor3DBase = System.Drawing.SystemColors.Highlight;
            this.FraArret.Appearance = appearance37;
            appearance38.BorderColor = System.Drawing.SystemColors.MenuHighlight;
            appearance38.BorderColor2 = System.Drawing.SystemColors.Highlight;
            appearance38.BorderColor3DBase = System.Drawing.SystemColors.Highlight;
            this.FraArret.ContentAreaAppearance = appearance38;
            this.FraArret.Controls.Add(this.txtcommentaire);
            this.FraArret.Controls.Add(this.Command1);
            this.FraArret.Controls.Add(this.CmdCalcul);
            this.FraArret.Controls.Add(this.txtDateArrete);
            this.FraArret.Controls.Add(this.label4);
            this.FraArret.Controls.Add(this.label33);
            this.FraArret.Controls.Add(this.ultraFormattedLinkLabel1);
            this.FraArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FraArret.Location = new System.Drawing.Point(73, 45);
            this.FraArret.Name = "FraArret";
            this.FraArret.Size = new System.Drawing.Size(321, 194);
            this.FraArret.TabIndex = 576;
            this.FraArret.Visible = false;
            this.FraArret.Click += new System.EventHandler(this.FraArret_Click);
            // 
            // txtcommentaire
            // 
            this.txtcommentaire.AccAcceptNumbersOnly = false;
            this.txtcommentaire.AccAllowComma = false;
            this.txtcommentaire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcommentaire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcommentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcommentaire.AccHidenValue = "";
            this.txtcommentaire.AccNotAllowedChars = null;
            this.txtcommentaire.AccReadOnly = false;
            this.txtcommentaire.AccReadOnlyAllowDelete = false;
            this.txtcommentaire.AccRequired = false;
            this.txtcommentaire.BackColor = System.Drawing.Color.White;
            this.txtcommentaire.CustomBackColor = System.Drawing.Color.White;
            this.txtcommentaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtcommentaire.ForeColor = System.Drawing.Color.Black;
            this.txtcommentaire.Location = new System.Drawing.Point(9, 104);
            this.txtcommentaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtcommentaire.MaxLength = 32767;
            this.txtcommentaire.Multiline = true;
            this.txtcommentaire.Name = "txtcommentaire";
            this.txtcommentaire.ReadOnly = false;
            this.txtcommentaire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcommentaire.Size = new System.Drawing.Size(307, 47);
            this.txtcommentaire.TabIndex = 579;
            this.txtcommentaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcommentaire.UseSystemPasswordChar = false;
            this.txtcommentaire.TextChanged += new System.EventHandler(this.txtcommentaire_TextChanged);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Location = new System.Drawing.Point(141, 162);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(84, 28);
            this.Command1.TabIndex = 394;
            this.Command1.Text = "Annuler";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // CmdCalcul
            // 
            this.CmdCalcul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdCalcul.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdCalcul.FlatAppearance.BorderSize = 0;
            this.CmdCalcul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdCalcul.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CmdCalcul.ForeColor = System.Drawing.Color.White;
            this.CmdCalcul.Location = new System.Drawing.Point(229, 162);
            this.CmdCalcul.Margin = new System.Windows.Forms.Padding(2);
            this.CmdCalcul.Name = "CmdCalcul";
            this.CmdCalcul.Size = new System.Drawing.Size(84, 28);
            this.CmdCalcul.TabIndex = 394;
            this.CmdCalcul.Text = "Calcul";
            this.CmdCalcul.UseVisualStyleBackColor = false;
            this.CmdCalcul.Click += new System.EventHandler(this.CmdCalcul_Click);
            // 
            // txtDateArrete
            // 
            this.txtDateArrete.AccAcceptNumbersOnly = false;
            this.txtDateArrete.AccAllowComma = false;
            this.txtDateArrete.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateArrete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateArrete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateArrete.AccHidenValue = "";
            this.txtDateArrete.AccNotAllowedChars = null;
            this.txtDateArrete.AccReadOnly = false;
            this.txtDateArrete.AccReadOnlyAllowDelete = false;
            this.txtDateArrete.AccRequired = false;
            this.txtDateArrete.BackColor = System.Drawing.Color.White;
            this.txtDateArrete.CustomBackColor = System.Drawing.Color.White;
            this.txtDateArrete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtDateArrete.ForeColor = System.Drawing.Color.Black;
            this.txtDateArrete.Location = new System.Drawing.Point(9, 52);
            this.txtDateArrete.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateArrete.MaxLength = 32767;
            this.txtDateArrete.Multiline = false;
            this.txtDateArrete.Name = "txtDateArrete";
            this.txtDateArrete.ReadOnly = false;
            this.txtDateArrete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateArrete.Size = new System.Drawing.Size(307, 27);
            this.txtDateArrete.TabIndex = 579;
            this.txtDateArrete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateArrete.UseSystemPasswordChar = false;
            this.txtDateArrete.TextChanged += new System.EventHandler(this.txtDateArrete_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label4.Location = new System.Drawing.Point(6, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(129, 18);
            this.label4.TabIndex = 578;
            this.label4.Text = "Titre / Observation";
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label33.Location = new System.Drawing.Point(6, 34);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(210, 18);
            this.label33.TabIndex = 578;
            this.label33.Text = "Veuillez saisir une date d\'arrêté";
            this.label33.Click += new System.EventHandler(this.label33_Click);
            // 
            // ultraFormattedLinkLabel1
            // 
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.BorderColor = System.Drawing.SystemColors.MenuHighlight;
            appearance39.FontData.BoldAsString = "True";
            appearance39.FontData.Name = "Ubuntu";
            appearance39.FontData.SizeInPoints = 11F;
            appearance39.ForeColor = System.Drawing.Color.White;
            this.ultraFormattedLinkLabel1.Appearance = appearance39;
            this.ultraFormattedLinkLabel1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraFormattedLinkLabel1.Location = new System.Drawing.Point(3, 3);
            this.ultraFormattedLinkLabel1.Margin = new System.Windows.Forms.Padding(0);
            this.ultraFormattedLinkLabel1.Name = "ultraFormattedLinkLabel1";
            this.ultraFormattedLinkLabel1.Size = new System.Drawing.Size(315, 25);
            this.ultraFormattedLinkLabel1.TabIndex = 577;
            this.ultraFormattedLinkLabel1.TabStop = true;
            this.ultraFormattedLinkLabel1.Value = "Contrat";
            this.ultraFormattedLinkLabel1.LinkClicked += new Infragistics.Win.FormattedLinkLabel.LinkClickedEventHandler(this.ultraFormattedLinkLabel1_LinkClicked);
            // 
            // PB1
            // 
            this.PB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PB1.Location = new System.Drawing.Point(3, 3);
            this.PB1.Name = "PB1";
            this.PB1.Size = new System.Drawing.Size(1423, 33);
            this.PB1.TabIndex = 3;
            this.PB1.Visible = false;
            this.PB1.Click += new System.EventHandler(this.PB1_Click);
            // 
            // lblDataDate
            // 
            this.lblDataDate.AutoSize = true;
            this.lblDataDate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.lblDataDate.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDataDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDataDate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDataDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataDate.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblDataDate.Location = new System.Drawing.Point(0, 127);
            this.lblDataDate.Margin = new System.Windows.Forms.Padding(0);
            this.lblDataDate.Name = "lblDataDate";
            this.lblDataDate.Size = new System.Drawing.Size(1429, 29);
            this.lblDataDate.TabIndex = 503;
            this.lblDataDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDataDate.Click += new System.EventHandler(this.lblDataDate_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 5;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 97F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 91F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 958F));
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label7, 3, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtTotal, 4, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 862);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1423, 34);
            this.tableLayoutPanel6.TabIndex = 504;
            this.tableLayoutPanel6.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel6_Paint);
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(178)))), ((int)(((byte)(178)))));
            this.label6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(100, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(85, 19);
            this.label6.TabIndex = 397;
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 11);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 12);
            this.label5.TabIndex = 396;
            this.label5.Text = "CONTRAT RESILIE";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(324, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(108, 34);
            this.label7.TabIndex = 396;
            this.label7.Text = "Total contrat + avenant";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // txtTotal
            // 
            this.txtTotal.AccAcceptNumbersOnly = false;
            this.txtTotal.AccAllowComma = false;
            this.txtTotal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotal.AccHidenValue = "";
            this.txtTotal.AccNotAllowedChars = null;
            this.txtTotal.AccReadOnly = false;
            this.txtTotal.AccReadOnlyAllowDelete = false;
            this.txtTotal.AccRequired = false;
            this.txtTotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTotal.BackColor = System.Drawing.Color.White;
            this.txtTotal.CustomBackColor = System.Drawing.Color.White;
            this.txtTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotal.ForeColor = System.Drawing.Color.Black;
            this.txtTotal.Location = new System.Drawing.Point(467, 3);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotal.MaxLength = 32767;
            this.txtTotal.Multiline = false;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = true;
            this.txtTotal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotal.Size = new System.Drawing.Size(954, 27);
            this.txtTotal.TabIndex = 502;
            this.txtTotal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotal.UseSystemPasswordChar = false;
            this.txtTotal.TextChanged += new System.EventHandler(this.txtTotal_TextChanged);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox2.Controls.Add(this.tableLayoutPanel7);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Margin = new System.Windows.Forms.Padding(0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(409, 905);
            this.ultraGroupBox2.TabIndex = 503;
            this.ultraGroupBox2.Click += new System.EventHandler(this.ultraGroupBox2_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.TreeArret, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 7.266436F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 92.73357F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(409, 905);
            this.tableLayoutPanel7.TabIndex = 1;
            this.tableLayoutPanel7.Paint += new System.Windows.Forms.PaintEventHandler(this.tableLayoutPanel7_Paint);
            // 
            // TreeArret
            // 
            this.TreeArret.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeArret.Location = new System.Drawing.Point(3, 68);
            this.TreeArret.Name = "TreeArret";
            this.TreeArret.Size = new System.Drawing.Size(403, 834);
            this.TreeArret.TabIndex = 0;
            this.TreeArret.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeArret_AfterSelect);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel2.Controls.Add(this.cmdSup);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(171, 39);
            this.flowLayoutPanel2.TabIndex = 1;
            this.flowLayoutPanel2.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel2_Paint);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(109, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 394;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdSup
            // 
            this.cmdSup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSup.FlatAppearance.BorderSize = 0;
            this.cmdSup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSup.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSup.Location = new System.Drawing.Point(45, 2);
            this.cmdSup.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSup.Name = "cmdSup";
            this.cmdSup.Size = new System.Drawing.Size(60, 35);
            this.cmdSup.TabIndex = 396;
            this.cmdSup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSup, "Supprimer");
            this.cmdSup.UseVisualStyleBackColor = false;
            this.cmdSup.Click += new System.EventHandler(this.cmdSup_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdvisu);
            this.flowLayoutPanel1.Controls.Add(CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.txtANCE_CreeLe);
            this.flowLayoutPanel1.Controls.Add(this.txtANCE_Noauto);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1844, 40);
            this.flowLayoutPanel1.TabIndex = 1;
            this.flowLayoutPanel1.Paint += new System.Windows.Forms.PaintEventHandler(this.flowLayoutPanel1_Paint);
            // 
            // cmdvisu
            // 
            this.cmdvisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdvisu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdvisu.FlatAppearance.BorderSize = 0;
            this.cmdvisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdvisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdvisu.ForeColor = System.Drawing.Color.White;
            this.cmdvisu.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdvisu.Location = new System.Drawing.Point(1782, 2);
            this.cmdvisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdvisu.Name = "cmdvisu";
            this.cmdvisu.Size = new System.Drawing.Size(60, 35);
            this.cmdvisu.TabIndex = 393;
            this.toolTip1.SetToolTip(this.cmdvisu, "Aperçu avant Impression");
            this.cmdvisu.UseVisualStyleBackColor = false;
            this.cmdvisu.Click += new System.EventHandler(this.cmdvisu_Click);
            // 
            // txtANCE_CreeLe
            // 
            this.txtANCE_CreeLe.AccAcceptNumbersOnly = false;
            this.txtANCE_CreeLe.AccAllowComma = false;
            this.txtANCE_CreeLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCE_CreeLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCE_CreeLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCE_CreeLe.AccHidenValue = "";
            this.txtANCE_CreeLe.AccNotAllowedChars = null;
            this.txtANCE_CreeLe.AccReadOnly = false;
            this.txtANCE_CreeLe.AccReadOnlyAllowDelete = false;
            this.txtANCE_CreeLe.AccRequired = false;
            this.txtANCE_CreeLe.BackColor = System.Drawing.Color.White;
            this.txtANCE_CreeLe.CustomBackColor = System.Drawing.Color.White;
            this.txtANCE_CreeLe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCE_CreeLe.ForeColor = System.Drawing.Color.Black;
            this.txtANCE_CreeLe.Location = new System.Drawing.Point(1648, 2);
            this.txtANCE_CreeLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCE_CreeLe.MaxLength = 32767;
            this.txtANCE_CreeLe.Multiline = false;
            this.txtANCE_CreeLe.Name = "txtANCE_CreeLe";
            this.txtANCE_CreeLe.ReadOnly = false;
            this.txtANCE_CreeLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCE_CreeLe.Size = new System.Drawing.Size(66, 27);
            this.txtANCE_CreeLe.TabIndex = 502;
            this.txtANCE_CreeLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCE_CreeLe.UseSystemPasswordChar = false;
            this.txtANCE_CreeLe.Visible = false;
            this.txtANCE_CreeLe.TextChanged += new System.EventHandler(this.txtANCE_CreeLe_TextChanged);
            // 
            // txtANCE_Noauto
            // 
            this.txtANCE_Noauto.AccAcceptNumbersOnly = false;
            this.txtANCE_Noauto.AccAllowComma = false;
            this.txtANCE_Noauto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCE_Noauto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCE_Noauto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCE_Noauto.AccHidenValue = "";
            this.txtANCE_Noauto.AccNotAllowedChars = null;
            this.txtANCE_Noauto.AccReadOnly = false;
            this.txtANCE_Noauto.AccReadOnlyAllowDelete = false;
            this.txtANCE_Noauto.AccRequired = false;
            this.txtANCE_Noauto.BackColor = System.Drawing.Color.White;
            this.txtANCE_Noauto.CustomBackColor = System.Drawing.Color.White;
            this.txtANCE_Noauto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCE_Noauto.ForeColor = System.Drawing.Color.Black;
            this.txtANCE_Noauto.Location = new System.Drawing.Point(1578, 2);
            this.txtANCE_Noauto.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCE_Noauto.MaxLength = 32767;
            this.txtANCE_Noauto.Multiline = false;
            this.txtANCE_Noauto.Name = "txtANCE_Noauto";
            this.txtANCE_Noauto.ReadOnly = false;
            this.txtANCE_Noauto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCE_Noauto.Size = new System.Drawing.Size(66, 27);
            this.txtANCE_Noauto.TabIndex = 502;
            this.txtANCE_Noauto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCE_Noauto.UseSystemPasswordChar = false;
            this.txtANCE_Noauto.Visible = false;
            this.txtANCE_Noauto.TextChanged += new System.EventHandler(this.txtANCE_Noauto_TextChanged);
            // 
            // UserAnalyseContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserAnalyseContrat";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Suivi Contrat";
            this.VisibleChanged += new System.EventHandler(this.UserAnalyseContrat_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridANCD)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FraArret)).EndInit();
            this.FraArret.ResumeLayout(false);
            this.FraArret.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeArret)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.UltraWinTree.UltraTree TreeArret;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtANCE_DateArret;
        public iTalk.iTalk_TextBox_Small2 txtANCE_CreePar;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtANCE_Commentaire;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.ProgressBar PB1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdvisu;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdSup;
        public iTalk.iTalk_TextBox_Small2 txtANCE_CreeLe;
        public iTalk.iTalk_TextBox_Small2 txtANCE_Noauto;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBGrid1;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBGrid2;
        private Infragistics.Win.Misc.UltraGroupBox FraArret;
        public Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ultraFormattedLinkLabel1;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtcommentaire;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button CmdCalcul;
        public iTalk.iTalk_TextBox_Small2 txtDateArrete;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblDataDate;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridANCD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtTotal;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
