﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.Analytique.Forms
{
    public partial class frmExerciceContrat : Form
    {
        public frmExerciceContrat()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            General.sExerciceDeb = "";
            General.sExerciceFin = "";
            General.lDetailExercice = 0;
            General.lGraphExelComp = 0;
            this.Close();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            General.sExerciceDeb = cmbExercicesDeb.Text;
            General.sExerciceFin = cmbExercicesFin.Text;
            if (string.IsNullOrEmpty(General.sExerciceDeb))
            {
                General.sExerciceFin = "";
               Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un exercice de début dans la liste déroulante.", "Opération annulée", MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(General.sExerciceFin))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un exercice de fin dans la liste déroulante.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                General.sExerciceDeb = "";
                return;
            }
            if (fc_CtrlError(General.sExerciceDeb, General.sExerciceFin) == true)
            {
                General.sExerciceDeb = "";
                General.sExerciceFin = "";
                return;
            }
            General.lDetailExercice = Convert.ToInt32(General.nz( (chkExportDetail.CheckState),0));
            General.lGraphExelComp = Convert.ToInt32(General.nz( (chkGraphique.CheckState),0));

            this.Close();
        }
        private bool fc_CtrlError(string sDeb, string sFin)
        {
            bool functionReturnValue = false;
            string stemp = null;
            int lDeb = 0;
            int lFin = 0;

            //=== retourne true si erreur trouvé.
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                functionReturnValue = false;

                stemp = General.Left(sDeb, 4);

                if (General.IsNumeric(stemp))
                {
                    lDeb = Convert.ToInt32(stemp);
                }

                stemp = General.Left(sFin, 4);

                if (General.IsNumeric(stemp))
                {
                    lFin = Convert.ToInt32(stemp);
                }

                if (lDeb > lFin)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de cohérence, l' \"exercice du\" doit être antérieur à l'\"exercice au\".", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_Ctrl");
                return functionReturnValue;
            }
        }

        private void frmExerciceContrat_Load(object sender, EventArgs e)
        {
            int i = 0;
            string sAdditem = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                cmbExercicesDeb.Rows.ToList().ForEach(l => l.Delete());
                cmbExercicesFin.Rows.ToList().ForEach(l => l.Delete());
                DataTable dt=new DataTable();
                dt.Columns.Add("Exercice");
                dt.Columns.Add("Du");
                dt.Columns.Add("Au");
                for (i = 0; i < General.tpExerciceCtr.Length; i++)
                {
                    dt.Rows.Add(General.tpExerciceCtr[i].sLibelle, General.tpExerciceCtr[i].dtDebut, General.tpExerciceCtr[i].dtFin);
                }
                cmbExercicesDeb.DataSource = dt;
                cmbExercicesFin.DataSource = dt;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";Form_Load");
            }
        }
    }
}
