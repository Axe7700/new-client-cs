﻿namespace Axe_interDT.Views.Analytique.ExportImmeubleAstreinte
{
    partial class UserDocExportImmAstreinte
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocExportImmAstreinte));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdImmeubleHisto = new System.Windows.Forms.Button();
            this.txtCodeImmeubleHisto = new iTalk.iTalk_TextBox_Small2();
            this.Label240 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCode1Histo = new iTalk.iTalk_TextBox_Small2();
            this.cmdClient = new System.Windows.Forms.Button();
            this.cmdExport = new System.Windows.Forms.Button();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdImprime = new System.Windows.Forms.Button();
            this.GridAst = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblTot = new System.Windows.Forms.Label();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.optContrat = new System.Windows.Forms.RadioButton();
            this.optTous = new System.Windows.Forms.RadioButton();
            this.cmdRechercher = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridAst)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 133F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 400F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 87F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel1.Controls.Add(this.cmdImmeubleHisto, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCodeImmeubleHisto, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label240, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCode1Histo, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdClient, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdExport, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmdMail, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmdImprime, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.GridAst, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblTot, 3, 4);
            this.tableLayoutPanel1.Controls.Add(this.ultraGroupBox1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmdRechercher, 2, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 938);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdImmeubleHisto
            // 
            this.cmdImmeubleHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImmeubleHisto.FlatAppearance.BorderSize = 0;
            this.cmdImmeubleHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImmeubleHisto.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdImmeubleHisto.Image = ((System.Drawing.Image)(resources.GetObject("cmdImmeubleHisto.Image")));
            this.cmdImmeubleHisto.Location = new System.Drawing.Point(536, 3);
            this.cmdImmeubleHisto.Name = "cmdImmeubleHisto";
            this.cmdImmeubleHisto.Size = new System.Drawing.Size(24, 24);
            this.cmdImmeubleHisto.TabIndex = 574;
            this.cmdImmeubleHisto.UseVisualStyleBackColor = false;
            this.cmdImmeubleHisto.Click += new System.EventHandler(this.cmdImmeubleHisto_Click);
            // 
            // txtCodeImmeubleHisto
            // 
            this.txtCodeImmeubleHisto.AccAcceptNumbersOnly = false;
            this.txtCodeImmeubleHisto.AccAllowComma = false;
            this.txtCodeImmeubleHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeubleHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeubleHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeubleHisto.AccHidenValue = "";
            this.txtCodeImmeubleHisto.AccNotAllowedChars = null;
            this.txtCodeImmeubleHisto.AccReadOnly = false;
            this.txtCodeImmeubleHisto.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeubleHisto.AccRequired = false;
            this.txtCodeImmeubleHisto.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeubleHisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCodeImmeubleHisto.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeubleHisto.Location = new System.Drawing.Point(135, 2);
            this.txtCodeImmeubleHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeubleHisto.MaxLength = 32767;
            this.txtCodeImmeubleHisto.Multiline = false;
            this.txtCodeImmeubleHisto.Name = "txtCodeImmeubleHisto";
            this.txtCodeImmeubleHisto.ReadOnly = false;
            this.txtCodeImmeubleHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeubleHisto.Size = new System.Drawing.Size(396, 27);
            this.txtCodeImmeubleHisto.TabIndex = 542;
            this.txtCodeImmeubleHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeubleHisto.UseSystemPasswordChar = false;
            this.txtCodeImmeubleHisto.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCodeImmeubleHisto_KeyDown);
            // 
            // Label240
            // 
            this.Label240.AutoSize = true;
            this.Label240.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label240.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label240.Location = new System.Drawing.Point(0, 0);
            this.Label240.Margin = new System.Windows.Forms.Padding(0);
            this.Label240.Name = "Label240";
            this.Label240.Size = new System.Drawing.Size(133, 30);
            this.Label240.TabIndex = 385;
            this.Label240.Tag = "0";
            this.Label240.Text = "Immeuble";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 30);
            this.label1.Margin = new System.Windows.Forms.Padding(0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 18);
            this.label1.TabIndex = 386;
            this.label1.Tag = "0";
            this.label1.Text = "Client";
            // 
            // txtCode1Histo
            // 
            this.txtCode1Histo.AccAcceptNumbersOnly = false;
            this.txtCode1Histo.AccAllowComma = false;
            this.txtCode1Histo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1Histo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1Histo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1Histo.AccHidenValue = "";
            this.txtCode1Histo.AccNotAllowedChars = null;
            this.txtCode1Histo.AccReadOnly = false;
            this.txtCode1Histo.AccReadOnlyAllowDelete = false;
            this.txtCode1Histo.AccRequired = false;
            this.txtCode1Histo.BackColor = System.Drawing.Color.White;
            this.txtCode1Histo.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1Histo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1Histo.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCode1Histo.ForeColor = System.Drawing.Color.Black;
            this.txtCode1Histo.Location = new System.Drawing.Point(135, 32);
            this.txtCode1Histo.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1Histo.MaxLength = 32767;
            this.txtCode1Histo.Multiline = false;
            this.txtCode1Histo.Name = "txtCode1Histo";
            this.txtCode1Histo.ReadOnly = false;
            this.txtCode1Histo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1Histo.Size = new System.Drawing.Size(396, 27);
            this.txtCode1Histo.TabIndex = 543;
            this.txtCode1Histo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1Histo.UseSystemPasswordChar = false;
            this.txtCode1Histo.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCode1Histo_KeyDown);
            // 
            // cmdClient
            // 
            this.cmdClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClient.FlatAppearance.BorderSize = 0;
            this.cmdClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClient.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdClient.Image = ((System.Drawing.Image)(resources.GetObject("cmdClient.Image")));
            this.cmdClient.Location = new System.Drawing.Point(536, 33);
            this.cmdClient.Name = "cmdClient";
            this.cmdClient.Size = new System.Drawing.Size(24, 24);
            this.cmdClient.TabIndex = 575;
            this.cmdClient.UseVisualStyleBackColor = false;
            this.cmdClient.Click += new System.EventHandler(this.cmdClient_Click);
            // 
            // cmdExport
            // 
            this.cmdExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExport.ForeColor = System.Drawing.Color.White;
            this.cmdExport.Image = ((System.Drawing.Image)(resources.GetObject("cmdExport.Image")));
            this.cmdExport.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExport.Location = new System.Drawing.Point(1754, 102);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(84, 55);
            this.cmdExport.TabIndex = 582;
            this.cmdExport.Tag = "";
            this.cmdExport.Text = "Export Excel";
            this.cmdExport.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExport.UseVisualStyleBackColor = false;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            // 
            // cmdMail
            // 
            this.cmdMail.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMail.ForeColor = System.Drawing.Color.White;
            this.cmdMail.Image = ((System.Drawing.Image)(resources.GetObject("cmdMail.Image")));
            this.cmdMail.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdMail.Location = new System.Drawing.Point(1667, 102);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.cmdMail.Size = new System.Drawing.Size(83, 55);
            this.cmdMail.TabIndex = 585;
            this.cmdMail.Tag = "";
            this.cmdMail.Text = "Mail";
            this.cmdMail.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdMail.UseVisualStyleBackColor = false;
            this.cmdMail.Click += new System.EventHandler(this.cmdMail_Click);
            // 
            // cmdImprime
            // 
            this.cmdImprime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdImprime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.tableLayoutPanel1.SetColumnSpan(this.cmdImprime, 2);
            this.cmdImprime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprime.FlatAppearance.BorderSize = 0;
            this.cmdImprime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdImprime.ForeColor = System.Drawing.Color.White;
            this.cmdImprime.Location = new System.Drawing.Point(1678, 2);
            this.cmdImprime.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprime.Name = "cmdImprime";
            this.tableLayoutPanel1.SetRowSpan(this.cmdImprime, 2);
            this.cmdImprime.Size = new System.Drawing.Size(164, 56);
            this.cmdImprime.TabIndex = 584;
            this.cmdImprime.Tag = "";
            this.cmdImprime.Text = "Affiche pdf pour immeuble avec contrat 24/24h";
            this.cmdImprime.UseVisualStyleBackColor = false;
            this.cmdImprime.Click += new System.EventHandler(this.cmdImprime_Click);
            // 
            // GridAst
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.GridAst, 5);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridAst.DisplayLayout.Appearance = appearance1;
            this.GridAst.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridAst.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridAst.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridAst.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridAst.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridAst.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridAst.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridAst.DisplayLayout.MaxColScrollRegions = 1;
            this.GridAst.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridAst.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridAst.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridAst.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridAst.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridAst.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridAst.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridAst.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridAst.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridAst.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridAst.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridAst.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridAst.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridAst.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridAst.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridAst.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridAst.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridAst.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridAst.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridAst.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridAst.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridAst.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridAst.Location = new System.Drawing.Point(3, 242);
            this.GridAst.Name = "GridAst";
            this.GridAst.Size = new System.Drawing.Size(1838, 693);
            this.GridAst.TabIndex = 586;
            this.GridAst.Text = "ultraGrid1";
            this.GridAst.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridAst_InitializeLayout);
            // 
            // lblTot
            // 
            this.lblTot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblTot.AutoSize = true;
            this.lblTot.Location = new System.Drawing.Point(1749, 216);
            this.lblTot.Name = "lblTot";
            this.lblTot.Size = new System.Drawing.Size(0, 13);
            this.lblTot.TabIndex = 587;
            this.lblTot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.tableLayoutPanel1.SetColumnSpan(this.ultraGroupBox1, 2);
            this.ultraGroupBox1.Controls.Add(this.flowLayoutPanel1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 103);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(527, 110);
            this.ultraGroupBox1.TabIndex = 588;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.optContrat);
            this.flowLayoutPanel1.Controls.Add(this.optTous);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(527, 110);
            this.flowLayoutPanel1.TabIndex = 542;
            // 
            // optContrat
            // 
            this.optContrat.AutoSize = true;
            this.optContrat.Checked = true;
            this.optContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optContrat.Location = new System.Drawing.Point(0, 0);
            this.optContrat.Margin = new System.Windows.Forms.Padding(0);
            this.optContrat.Name = "optContrat";
            this.optContrat.Size = new System.Drawing.Size(183, 22);
            this.optContrat.TabIndex = 540;
            this.optContrat.TabStop = true;
            this.optContrat.Text = "Immeuble avec contrats";
            this.optContrat.UseVisualStyleBackColor = true;
            // 
            // optTous
            // 
            this.optTous.AutoSize = true;
            this.optTous.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optTous.Location = new System.Drawing.Point(0, 22);
            this.optTous.Margin = new System.Windows.Forms.Padding(0);
            this.optTous.Name = "optTous";
            this.optTous.Size = new System.Drawing.Size(309, 22);
            this.optTous.TabIndex = 541;
            this.optTous.Text = "Tous les immeubles avec ou sans contrats";
            this.optTous.UseVisualStyleBackColor = true;
            // 
            // cmdRechercher
            // 
            this.cmdRechercher.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRechercher.FlatAppearance.BorderSize = 0;
            this.cmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRechercher.ForeColor = System.Drawing.Color.White;
            this.cmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdRechercher.Location = new System.Drawing.Point(1579, 102);
            this.cmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRechercher.Name = "cmdRechercher";
            this.cmdRechercher.Size = new System.Drawing.Size(84, 55);
            this.cmdRechercher.TabIndex = 589;
            this.cmdRechercher.Tag = "";
            this.cmdRechercher.UseVisualStyleBackColor = false;
            this.cmdRechercher.Click += new System.EventHandler(this.cmdRechercher_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1850, 957);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            // 
            // UserDocExportImmAstreinte
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupBox1);
            this.Name = "UserDocExportImmAstreinte";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Export Immeuble Astreinte";
            this.Load += new System.EventHandler(this.UserDocExportImmAstreinte_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDocExportImmAstreinte_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridAst)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label Label240;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.RadioButton optContrat;
        public System.Windows.Forms.RadioButton optTous;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeubleHisto;
        public iTalk.iTalk_TextBox_Small2 txtCode1Histo;
        public System.Windows.Forms.Button cmdImmeubleHisto;
        public System.Windows.Forms.Button cmdClient;
        public System.Windows.Forms.Button cmdExport;
        public System.Windows.Forms.Button cmdImprime;
        public System.Windows.Forms.Button cmdMail;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridAst;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblTot;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdRechercher;
    }
}
