﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Analytique.ExportImmeubleAstreinte
{
    public partial class UserDocExportImmAstreinte : UserControl
    {
        private string cServClim = "DELOSTAL MULTITECHNIQUES";
        private string cServChauf = "DELOSTAL CHAUFFAGE ET ENERGIE";
        DataTable rs = new DataTable();
        public UserDocExportImmAstreinte()
        {
            InitializeComponent();
        }

        private void cmdImmeubleHisto_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "SELECT CodeImmeuble as \"Code immeuble\", "
                                 + " Adresse as \"adresse\", Ville as \"Ville\""
                                 + " , anglerue as \"angle de rue\""
                                 + " FROM Immeuble ";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleHisto.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeImmeubleHisto.Text = fg.ugResultat.ActiveRow.Cells["Code immeuble"].Text.ToString();


                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeImmeubleHisto.Text = fg.ugResultat.ActiveRow.Cells["Code immeuble"].Value.ToString();


                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImmeubleHisto_Click");
            }
        }

        private void cmdClient_Click(object sender, EventArgs e)
        {
            try
            {
                string requete =
                    "SELECT Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1 ";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche client" };
                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1Histo.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {


                    txtCode1Histo.Text = fg.ugResultat.ActiveRow.Cells["Nom Directeur"].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCode1Histo.Text = fg.ugResultat.ActiveRow.Cells["Nom Directeur"].Value.ToString();


                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImmeubleHisto_Click");
            }
        }

        private void cmdImprime_Click(object sender, EventArgs e)
        {
            //===> Mondir le 05.10.2020, supprimeé cette ligne car l'etat est affiché sans adresse, + les données de cette table ne soint pas utilisée
            //string sSelectionFormula = "{Temp_Astreinte.AstreinteNuit} = 1";
            string sSelectionFormula = "1 = 1";

            Cursor = Cursors.WaitCursor;

            if (string.IsNullOrEmpty(txtCodeImmeubleHisto.Text) && string.IsNullOrEmpty(txtCode1Histo.Text))
            {
                var dr = CustomMessageBox.Show("", $"Vous n'avez défini aucune condition, le traitement prendra beaucoup de temps{Environment.NewLine}Vous-vous continuer ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.No)
                    return;
            }

            if (txtCodeImmeubleHisto.Text != "")
                sSelectionFormula += " AND {Immeuble.CodeImmeuble} ='" +
                                     StdSQLchaine.gFr_DoublerQuote(txtCodeImmeubleHisto.Text) + "'";

            if (txtCode1Histo.Text != "")
                sSelectionFormula += " AND {Immeuble.code1} ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1Histo.Text) + "'";

            ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
            ModCrystalPDF.tabFormulas[0].sNom = "";
            var sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.sEtatExportImmAstreinteV9, "", "", "", "", CheckState.Checked);

            Cursor = Cursors.Default;

            ModuleAPI.Ouvrir(sReturn);

        }

        private void cmdMail_Click(object sender, EventArgs e)
        {
            fc_LoadExport(true);
        }



        private void txtCodeImmeubleHisto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmdImmeubleHisto_Click(null, null);
        }

        private void txtCode1Histo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                cmdClient_Click(null, null);
        }

        /// <summary>
        /// Mondir le 05.10.2020, to add search button
        /// </summary>
        /// <returns></returns>
        private bool fc_Load()
        {
            Cursor = System.Windows.Forms.Cursors.WaitCursor;

            string sWhere = "";
            var sSQL = "DELETE FROM Temp_Astreinte WHERE  CreePar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
            General.Execute(sSQL);

            sSQL = "INSERT INTO Temp_Astreinte"
                + " ( DateCreation, CreePar, MatExploit, NomExploit, PrenomExploit, CodeImmeuble,"
               + " Adresse, Adresse2_Imm, Autre,CodePostal,  Ville , Consigne)";



            sSQL = sSQL + " SELECT DISTINCT "
                + "'" + DateTime.Now + "' as DateCreation, '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' as CreePar"
                + " ,Immeuble.CodeRespExploit, Personnel.Nom, Personnel.Prenom "
                + " ,Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.Adresse2_Imm  "
                + " ,Immeuble.AngleRue, Immeuble.CodePostal, Immeuble.Ville "
                + " , Immeuble.Consigne ";

            if (optContrat.Checked)
            {

                sSQL = sSQL + " FROM            Immeuble INNER JOIN "
                            + " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble LEFT OUTER JOIN "
                            + " Personnel ON Immeuble.CodeRespExploit = Personnel.Matricule ";


                //===> Mondir le 19.11.2020, la facturation ne dois pas être un critère. ticket https://groupe-dt.mantishub.io/view.php?id=2080#c5173
                //===> Mondir le 29.11.2020 pour ajouté les Modifs de la version V29.11.2020, Rachid did the same thing in this version
                //sWhere = " WHERE        (Contrat.Resiliee = 0) AND (Contrat.nonFacturable = 0)";
                sWhere = " WHERE        (Contrat.Resiliee = 0) ";
                //===> Fin Modif Mondir
            }
            else
            {
                sSQL = sSQL + " FROM            Immeuble LEFT OUTER JOIN "
                       + " Personnel ON Immeuble.CodeRespExploit = Personnel.Matricule";
            }

            if (txtCodeImmeubleHisto.Text != "")
            {
                if (sWhere == "")
                {
                    sWhere = " WHERE Immeuble.CodeImmeuble ='" +
                             StdSQLchaine.gFr_DoublerQuote(txtCodeImmeubleHisto.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " AND Immeuble.CodeImmeuble ='" +
                             StdSQLchaine.gFr_DoublerQuote(txtCodeImmeubleHisto.Text) + "'";
                }
            }


            if (txtCode1Histo.Text != "")
            {
                if (sWhere == "")
                {
                    sWhere = " WHERE Immeuble.code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1Histo.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " AND Immeuble.code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1Histo.Text) + "'";
                }
            }

            General.Execute(sSQL + sWhere);


            //'=== identifie les contrats en cours (important quand la selection se fait sur tous les immeubles).
            sSQL = "UPDATE       Temp_Astreinte"
                   + " Set ContratEnCours = 1 "
                   + " FROM            Temp_Astreinte INNER JOIN "
                   + " Contrat ON Temp_Astreinte.CodeImmeuble = Contrat.CodeImmeuble "
                   //===> Mondir le 19.11.2020, la facturation ne dois pas être un critère. ticket https://groupe-dt.mantishub.io/view.php?id=2080#c5173
                   //===> Mondir le 29.11.2020 pour ajouter la version V29.11.2020, Rachid did the same thing in this version
                   //+ " WHERE        (Contrat.Resiliee = 0) AND (Contrat.nonFacturable = 0)";
                   + " WHERE        (Contrat.Resiliee = 0) ";

            General.Execute(sSQL);

            sSQL = "UPDATE       Temp_Astreinte"
                   + " SET         Service = '" + cServClim + "', LimiteInter = 'LUNDI AU VENDREDI DE 8H A 17H' WHERE CreePar = '" +
                   StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                   + " AND MatExploit = '" + General.sClimMat + "' AND Temp_Astreinte.ContratEnCours = 1";
            General.Execute(sSQL);

            sSQL = "UPDATE       Temp_Astreinte"
                   + " SET         Service = '" + cServChauf + "', LimiteInter = '7/7 de 8H à 19H' WHERE CreePar = '" +
                   StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                   + " AND MatExploit <> '" + General.sClimMat + "' AND Temp_Astreinte.ContratEnCours = 1";
            General.Execute(sSQL);

            sSQL = "UPDATE       Temp_Astreinte "
                   + "  set LimiteInter = '24h/24h MT', AstreinteNuit = 1"
                   + " FROM            Contrat INNER JOIN "
                   + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle INNER JOIN "
                   + " Temp_Astreinte ON Contrat.CodeImmeuble = Temp_Astreinte.CodeImmeuble "
                   + " WHERE        (FacArticle.[24sur24] = 1) AND Temp_Astreinte.MatExploit = '" + General.sClimMat + "'"
                   + " AND CreePar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' AND Temp_Astreinte.ContratEnCours = 1";
            General.Execute(sSQL);

            sSQL = "UPDATE       Temp_Astreinte "
                   + "  set LimiteInter = '24h/24h', AstreinteNuit = 1"
                   + " FROM            Contrat INNER JOIN "
                   + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle INNER JOIN "
                   + " Temp_Astreinte ON Contrat.CodeImmeuble = Temp_Astreinte.CodeImmeuble "
                   + " WHERE        (FacArticle.[24sur24] = 1) AND Temp_Astreinte.MatExploit <> '" + General.sClimMat + "'"
                   + " AND CreePar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' AND Temp_Astreinte.ContratEnCours = 1";
            General.Execute(sSQL);

            sSQL = "SELECT        NomExploit as Responsable,  Service, LimiteInter as [Limite intervention], CodeImmeuble,"
                   + " Adresse, Adresse2_Imm as [Adresse 2], Autre, CodePostal as [Code postal], Ville, Consigne"
                   + " FROM            Temp_Astreinte"
                   + " WHERE CreePar='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                   + " Order by CodeImmeuble";



            var modAdors = new ModAdo();

            rs = modAdors.fc_OpenRecordSet(sSQL);

            Cursor = Cursors.Default;

            GridAst.DataSource = rs;

            if (rs.Rows.Count == 0)
            {
                MessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            return true;

        }

        private void fc_LoadExport(bool bSendMail)
        {
            try
            {
                //====> Mondir le 02.10.2020, demadé par Rachid de changer la postion de cette ligne
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                Application.DoEvents();

                Microsoft.Office.Interop.Excel.Application oXL;
                Microsoft.Office.Interop.Excel.Workbook oWB;
                Microsoft.Office.Interop.Excel.Worksheet oSheet;
                Microsoft.Office.Interop.Excel.Range oRng;
                Microsoft.Office.Interop.Excel.Range oResizeRange;
                lblTot.Text = "";

                //===> Mondir le 05.06.2020, to add the search button
                if (!fc_Load())
                    return;

                lblTot.Text = rs.Rows.Count.ToString();
                // '==Start Excel and get Application object.


                //=== Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //=== Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                int i = 1;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataColumn col in rs.Columns)
                {
                    oSheet.Cells[1, i].Style.VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = col.ColumnName;
                    i++;
                }

                // rows
                for (i = 0; i < rs.Rows.Count; i++)
                {
                    // to do: format datetime values before printing
                    for (var j = 0; j < rs.Columns.Count; j++)
                    {
                        oSheet.Cells[i + 2, j + 1] = rs.Rows[i][j];
                    }
                }

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: rs.Columns.Count];

                oResizeRange.Interior.ColorIndex = 15;

                oResizeRange.EntireColumn.AutoFit();

                if (bSendMail)
                {

                    var sFullPath = Dossier.fc_ReturnPathTemp("Astreinte", true, ".xlsx");
                    oWB.SaveAs(sFullPath);


                    oSheet = null;
                    oWB.Close();
                    oWB = null;
                    oXL = null;

                    // fc_NettoieDossier sNomRep, "*.xls"
                    frmMail frmMail = new frmMail();
                    ModMain.bActivate = true;
                    frmMail.SSOleDBcmbA.Text = "superviseur@excelia.eu";
                    frmMail.txtCopie.Text = "j.gresle@groupe-dt.fr;D.DONGUI@groupe-dt.fr;S.Ramos@groupe-dt.fr;";
                    frmMail.txtObjet.Text = "Tableau de Suivi 24h/24h Delostal et Thibault";
                    frmMail.txtMessage.Text = "Bonjour," + Environment.NewLine + Environment.NewLine;
                    frmMail.txtMessage.Text = frmMail.txtMessage.Text + "Je vous remercie de bien vouloir prendre en compte"
                                                                    + " notre tableau de suivi des contrats 24/24 modifié ce jour."
                                                                    + Environment.NewLine + Environment.NewLine;
                    frmMail.txtMessage.Text = frmMail.txtMessage.Text + "Nous restons à votre disposition au besoin," + Environment.NewLine + Environment.NewLine;
                    frmMail.txtMessage.Text = frmMail.txtMessage.Text + "Cordialement." + Environment.NewLine + Environment.NewLine;
                    frmMail.txtMessage.Text = frmMail.txtMessage.Text + "L'équipe D&T" + Environment.NewLine;
                    frmMail.txtMessage.Text = frmMail.txtMessage.Text + "T. 01 41 99 95 95";

                    ModCourrier.FichierAttache = sFullPath;
                    frmMail.txtDocuments.Text = ModCourrier.FichierAttache;

                    frmMail.txtDossierSauvegarde.Text = "";

                    frmMail.ShowDialog();

                    frmMail.Close();
                }
                else
                {
                    oXL.Visible = true;
                }

                //GridAst.Refresh();
                //===> Mondir le 02.10.2020, moved to last line in fonction, if exception before this line, the cursor will stay 
                //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;


            }
            catch (Exception e)
            {
                Program.SaveException(e);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Error: " + e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        }

        private void cmdExport_Click(object sender, EventArgs e)
        {
            fc_LoadExport(false);
        }

        private void GridAst_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {


        }

        private void UserDocExportImmAstreinte_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {

            }
        }

        private void UserDocExportImmAstreinte_Load(object sender, EventArgs e)
        {


            var ssdbGridCorpsSource = new DataTable();
            ssdbGridCorpsSource.Columns.Add("Responsable");
            ssdbGridCorpsSource.Columns.Add("Service");
            ssdbGridCorpsSource.Columns.Add("Limite intervention");
            ssdbGridCorpsSource.Columns.Add("CodeImmeuble");
            ssdbGridCorpsSource.Columns.Add("Adresse");
            ssdbGridCorpsSource.Columns.Add("Adresse 2");
            ssdbGridCorpsSource.Columns.Add("Autre");
            ssdbGridCorpsSource.Columns.Add("CodePostal");
            ssdbGridCorpsSource.Columns.Add("Ville");
            ssdbGridCorpsSource.Columns.Add("Consigne");


            GridAst.DataSource = ssdbGridCorpsSource;
        }

        private void cmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Load();
        }
    }
}
