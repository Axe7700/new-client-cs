﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Analytique.User_DocAnalytique2.Forms;
using Axe_interDT.Views.Appel.Fiche_Multi_Fiche_Appel;
using Axe_interDT.Views.Commande.UserPreCommande;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Analytique
{
    public partial class UserDocAnalytique2 : UserControl
    {
        public UserDocAnalytique2()
        {
            InitializeComponent();
        }

        #region Variables
        string sSQL;
        DataTable rsDevis;
        DataTable rsBon;
        DataTable rsFF;
        DataTable rsSst;
        DataTable rsFClient;
        DataTable rsMo;

        const string cDEVIS = "devis";
        const string cBon = "BDC";
        const string cFF = "FactF";
        const string cFc = "FactClient";
        const string cMO = "MO";
        const string cSst = "SST";
        Variable.tailleCtl[] tTCtldevis;
        Variable.tailleCtl[] tTctlBon;
        Variable.tailleCtl[] tTctlFF;
        Variable.tailleCtl[] tTclFClient;
        Variable.tailleCtl[] tTclMO;
        Variable.tailleCtl[] tTctlSst;


        const short cZFraHeight = 7455;
        const short cZFraLeft = 120;
        const short cZFraTop = 720;
        const short cZFRAWidht = 12615;

        const short cGridHeight = 6735;
        const short cGridLeft = 120;
        const short cGridTop = 240;
        const short cGridWidht = 12135;

        const short cBtHeight = 315;
        const short cBtLeft = 12240;
        const short cBtTop = 240;
        const short cBtWidht = 255;

        const short clblTotHeiht = 280;
        const short clblLeft = 8160;
        const short clblTop = 6960;
        const short clblWidht = 1815;

        const short ctxtTotHeight = 285;
        const short ctxtRotLeft = 9960;
        const short ctxtTop = 6960;
        const short ctxtWidht = 2295;
        #endregion

        #region methodes

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lEAD_NoAUto"></param>
        private void fc_Coef(int lEAD_NoAUto)
        {

            lblMOReel.Text = txtHTMO.Text;
            //lblAchatsReels = txtHTBCD
            lblAchatsReels.Text = txtHTFF.Text;
            lblVentesReelles.Text = txtHTFactureClient.Text;
            lblChargesReelles.Text = Convert.ToString(Convert.ToDouble(Convert.ToDouble(General.nz(txtHTMO.Text, 0)) + Convert.ToDouble(General.nz(txtHTFF.Text, 0))) + Convert.ToDouble(General.nz(txtHTSst.Text, 0)));
            lblSstReel.Text = txtHTSst.Text;
            if (!(lblChargesReelles.Text == "0"))
            {
                lblCoefficientReel.Text = Convert.ToString(Convert.ToDouble(General.nz((lblVentesReelles.Text), 0)) / Convert.ToDouble(General.nz((lblChargesReelles.Text), 0)));
            }
            lblCoefficientReel.Text = string.Format(General.FncArrondir(Convert.ToDouble(General.nz(lblCoefficientReel.Text, 0)), 3) + "", "### ##0.000");

            sSQL = "UPDATE    ETAE_AffaireEntete SET" + " ETAE_MoReel =" + General.nz(txtHTMO.Text, 0) + " , ETAE_AchatReel = " + General.nz(txtHTFF.Text, 0) + ", EATE_VenteReel = " + General.nz(txtHTFactureClient.Text, 0) + ", ETAE_CoefReel = " + General.nz(lblCoefficientReel.Text, 0) + ",  ETAE_SstReel =" + General.nz(txtHTSst.Text, 0) + " WHERE     (ETAE_Noauto = " + lEAD_NoAUto + ") ";

            General.Execute(sSQL);

        }

        /// <summary>
        /// TESTED
        /// Mondir le 15.11.2020 pour ajouter les modifs de la version V12.11.2020
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        private void fc_grilleMo(int lnumfichestandard, string sCodeImmeuble, string sCode1, string sdate = "", string sWhereMulti = "")
        {
            string sqlWhere = null;
            double dbTotHT = 0;
            double dbTempTotMO = 0;
            DataTable rsAdd = default(DataTable);


            sSQL = "SELECT     GestionStandard.NumFicheStandard, Intervention.DateRealise," + " Intervention.NoIntervention, Intervention.HeureDebut," + " Intervention.HeureFin, Intervention.Duree, " + " Intervention.Debourse AS MO, Intervention.CodeEtat," + " Immeuble.CodeImmeuble, Intervention.Intervenant, SansPauseDejeuner " + " FROM         GestionStandard" + " INNER JOIN Intervention " + " ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard " + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble";

            //Mondir le 15.11.2020 pour ajouter les modifs de la version V12.11.2020, ajout de Intervention.CodeEtat =  'TS'
            sqlWhere = " WHERE   (Intervention.CodeEtat = 'AF' OR" + " Intervention.CodeEtat = 'E' OR" + " Intervention.CodeEtat = 'S' OR" +
                       " Intervention.CodeEtat = 'T' OR" + " Intervention.CodeEtat =  'D' OR" + " Intervention.CodeEtat =  'P3' or" +
                       " Intervention.CodeEtat =  'TS' )";

            //Mondir le 15.11.2020 pour ajouter les modifs de la version V12.11.2020 ==> ajout de cette condition
            if (!string.IsNullOrEmpty(sWhereMulti))
            {
                sqlWhere = sqlWhere + " " + sWhereMulti.Replace("WHERE", "AND");
            }
            else
            {
                if (lnumfichestandard != 0)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                    }

                }
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sdate) && General.IsDate(sdate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                    sqlWhere = " WHERE Intervention.DateRealise <='" + Convert.ToDateTime(sdate + " 23:59:59") + "'";
                else
                    sqlWhere = sqlWhere + " AND Intervention.DateRealise <='" + Convert.ToDateTime(sdate + " 23:59:59") + "'";
            }

            sSQL = sSQL + sqlWhere + " ORDER BY GestionStandard.NumFicheStandard DESC, Immeuble.CodeImmeuble,Intervention.NoIntervention ";
            var ModAdo = new ModAdo();
            rsMo = ModAdo.fc_OpenRecordSet(sSQL);
            DataTable dt = new DataTable();
            dt.Columns.Add("NumFicheStandard");
            dt.Columns.Add("DateRealise");
            dt.Columns.Add("NoIntervention");
            dt.Columns.Add("HeureDebut");
            dt.Columns.Add("HeureFin");
            dt.Columns.Add("Duree");
            dt.Columns.Add("MO");
            dt.Columns.Add("CodeEtat");
            dt.Columns.Add("CodeImmeuble");
            dt.Columns.Add("Intervenant");
            dt.Columns.Add("SansPauseDejeuner");
            foreach (DataRow dr in rsMo.Rows)
            {
                object duree = null;
                object heureDebut = null;
                object heureFin = null;
                if (dr["Duree"] != DBNull.Value)
                    duree = Convert.ToDateTime(dr["Duree"]).TimeOfDay;
                if (dr["HeureDebut"] != DBNull.Value)
                    heureDebut = Convert.ToDateTime(dr["HeureDebut"]).TimeOfDay;
                if (dr["HeureFin"] != DBNull.Value)
                    heureFin = Convert.ToDateTime(dr["HeureFin"]).TimeOfDay;
                dt.Rows.Add(dr["NumFicheStandard"], dr["DateRealise"], dr["NoIntervention"], heureDebut, heureFin, duree, dr["MO"], dr["CodeEtat"], dr["CodeImmeuble"], dr["Intervenant"], dr["SansPauseDejeuner"]);
            }
            ssMO.DataSource = dt;
            ssMO.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020, ajout du string sWhereMulti = ""
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        private void fc_GrilleFactSst(int lnumfichestandard, string sCodeImmeuble, string sCode1, string sdate = "", string sWhereMulti = "")
        {
            string sqlWhere = null;

            DataTable rsAdd = default(DataTable);

            sSQL = "SELECT FactFournEntete.EC_NoFacture, FactFournEntete.DateLivraison," + " fournisseurArticle.Raisonsocial, FactFournEntete.RefFacture, FactFournPied.NoBCmd, " + " FactFournPied.Montant , FactFournPied.CodeImmeuble, FactFournEntete.NoAutoFacture " + " , CheminFichier " + " FROM         FactFournEntete INNER JOIN" + " FactFournPied " + " ON FactFournEntete.NoAutoFacture = FactFournPied.NoAutoFacture " + " LEFT OUTER JOIN fournisseurArticle " + " ON FactFournEntete.CodeFournisseur = fournisseurArticle.Cleauto " + " INNER JOIN BonDeCommande " + " ON FactFournPied.NoBCmd = BonDeCommande.NoBonDeCommande" + " INNER JOIN Intervention " + " ON BonDeCommande.NoIntervention = Intervention.NoIntervention" + " INNER JOIN Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

            //Set rsFF = fc_OpenRecordSet(sSQL)

            //Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020, ajout du la condition if (!string.IsNullOrEmpty(General.sWhereSst))
            if (!string.IsNullOrEmpty(General.sWhereSst))
            {
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = General.sWhereSst + " " + sWhereMulti.Replace("WHERE", "AND");
                }
                else
                {
                    sqlWhere = General.sWhereSst;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = sWhereMulti;
                }
                else
                {
                    sqlWhere = "";
                }
            }

            //Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020, ajout du la condition if (string.IsNullOrEmpty(sWhereMulti))
            if (string.IsNullOrEmpty(sWhereMulti))
            {
                if (lnumfichestandard != 0)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                }
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sdate) && General.IsDate(sdate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sdate) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sdate) + "'";
                }
            }

            sSQL = sSQL + sqlWhere;
            var ModAdo = new ModAdo();
            rsSst = ModAdo.fc_OpenRecordSet(sSQL);
            ssFactSst.DataSource = rsSst;
            ssFactSst.UpdateData();

        }

        /// <summary>
        /// TESTED
        /// Mondir le 15.11.2020, ajout des modifs de la version V12.11.2020, ajout du string sWhereMulti = ""
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        private void fc_GrilleFactFourn(int lnumfichestandard, string sCodeImmeuble, string sCode1, string sdate = "", string sWhereMulti = "")
        {
            string sqlWhere = null;
            DataTable rsAdd = default(DataTable);

            sSQL = "SELECT FactFournEntete.EC_NoFacture, FactFournEntete.DateLivraison," + " fournisseurArticle.Raisonsocial, FactFournEntete.RefFacture, FactFournPied.NoBCmd, " +
                   " FactFournPied.Montant , FactFournPied.CodeImmeuble, FactFournEntete.NoAutoFacture " + " , CheminFichier " + " FROM         FactFournEntete INNER JOIN" +
                   " FactFournPied " + " ON FactFournEntete.NoAutoFacture = FactFournPied.NoAutoFacture " + " LEFT OUTER JOIN fournisseurArticle " + " ON FactFournEntete.CodeFournisseur = fournisseurArticle.Cleauto " +
                   " INNER JOIN BonDeCommande " + " ON FactFournPied.NoBCmd = BonDeCommande.NoBonDeCommande" + " INNER JOIN Intervention " + " ON BonDeCommande.NoIntervention = Intervention.NoIntervention" +
                   " INNER JOIN Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

            // Set rsFF = fc_OpenRecordSet(sSQL)

            //Mondir le 15.11.2020, ajout des modifs de la version V12.11.2020, ajout du condition if (!string.IsNullOrEmpty(sWhereMulti))
            if (!string.IsNullOrEmpty(General.sWhereFf))
            {
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = General.sWhereFf + " " + sWhereMulti.Replace("WHERE", "AND");
                }
                else
                {
                    sqlWhere = General.sWhereFf;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(sWhereMulti))
                {
                    sqlWhere = sWhereMulti;
                }
                else
                {
                    sqlWhere = "";
                }
            }

            //Mondir le 15.11.2020, ajout des modifs de la version V12.11.2020, ajout du if (string.IsNullOrEmpty(sWhereMulti))
            if (string.IsNullOrEmpty(sWhereMulti))
            {
                if (lnumfichestandard != 0)
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard=" + lnumfichestandard + "";
                    }
                }
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sdate) && General.IsDate(sdate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sdate + " 23:59:59") + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND FactFournEntete.DateLivraison <='" + Convert.ToDateTime(sdate + " 23:59:59") + "'";
                }
            }

            sSQL = sSQL + sqlWhere;
            var ModAdo = new ModAdo();
            rsFF = ModAdo.fc_OpenRecordSet(sSQL);
            ssFactFourn.DataSource = rsFF;
            ssFactFourn.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        private void fc_grilleDevis(int lnumfichestandard, string sCodeImmeuble, string sCode1)
        {

            string sSQL = null;
            string sqlWhere = null;

            sSQL = "SELECT     DevisEnTete.NumeroDevis, DevisEnTete.DateCreation, GestionStandard.NumFicheStandard,"
                + " SUM(DevisDetail.MOd) AS [MO Prevues], "
                + " SUM(DevisDetail.FOd) AS [Achats prévus], SUM(DevisDetail.STd) AS [Sous Traitance],"
                + " DevisEnTete.TotalHT, Immeuble.Code1," + " Immeuble.CodeImmeuble"
                + " FROM         GestionStandard INNER JOIN"
                + " Immeuble ON GestionStandard.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN"
                + " DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis LEFT OUTER JOIN"
                + " DevisDetail ON DevisEnTete.NumeroDevis = DevisDetail.NumeroDevis";

            sqlWhere = "";

            if (lnumfichestandard != 0)
            {
                sqlWhere = " WHERE GestionStandard.NumFicheStandard=" + lnumfichestandard + "";
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            sSQL = sSQL + sqlWhere;

            sSQL = sSQL + " GROUP BY GestionStandard.NumFicheStandard, DevisEnTete.TotalHT," + " DevisEnTete.NumeroDevis, DevisEnTete.DateCreation, " + " Immeuble.Code1, Immeuble.CodeImmeuble ";

            rsDevis = new DataTable();
            var ModAdo = new ModAdo();
            rsDevis = ModAdo.fc_OpenRecordSet(sSQL);

            ssDevis.DataSource = rsDevis;
            ssDevis.UpdateData();

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCode1"></param>
        private void fc_GrilleFactureClient(int lnumfichestandard, string sCodeImmeuble, string sCode1, string sdate = "")
        {

            string sqlWhere = null;

            DataTable rsAdd = default(DataTable);


            sSQL = "SELECT DISTINCT " + " FactureManuelleEnTete.NoFacture, FactureManuelleEnTete.DateFacture," + " FactureManuellePied.MontantHT AS Ventes, Immeuble.CodeImmeuble," + " Intervention.NumFicheStandard,  FactureManuelleEnTete.ServiceAnalytique " + " FROM         Intervention " + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble " + " LEFT OUTER JOIN FactureManuelleEnTete " + " ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture" + " LEFT OUTER JOIN FactureManuellePied " + " ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle";

            sqlWhere = " WHERE  (FactureManuelleEnTete.Facture = 1)";

            if (lnumfichestandard != 0)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                }
            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sdate) && General.IsDate(sdate))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE FactureManuelleEnTete.DateFacture <='" + Convert.ToDateTime(sdate) + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND FactureManuelleEnTete.DateFacture <='" + Convert.ToDateTime(sdate) + "'";
                }
            }

            sSQL = sSQL + sqlWhere + "ORDER BY FactureManuelleEnTete.NoFacture, Immeuble.CodeImmeuble";

            rsFClient = new DataTable();
            var ModAdo = new ModAdo();
            rsFClient = ModAdo.fc_OpenRecordSet(sSQL);
            ssFClient.DataSource = rsFClient;
            ssFClient.UpdateData();

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_grilleBonDeCommande()
        {
            string sSQL = null;
            int i = 0;
            ssBonDeCommande.Rows.ToList().ForEach(l => l.Delete(false));

            //try
            //{
            //    i = ModAnalytique.tBCD!=null? ModAnalytique.tBCD.Length:0;
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //}
            try
            {
                sSQL = "";
                DataTable dt = new DataTable();
                dt.Columns.Add("NoBonDeCommande");
                dt.Columns.Add("Date");
                dt.Columns.Add("Nom");
                dt.Columns.Add("N° Fiche d'appel /");
                dt.Columns.Add("NoIntervention");
                dt.Columns.Add("CodeImmeuble");
                dt.Columns.Add("Code Etat");
                dt.Columns.Add("Total Ht");
                dt.Columns.Add("TotFacture");
                dt.Columns.Add("Solde");
                dt.Columns.Add("Service Analytique");
                double curtemp = default(double);
                ssBonDeCommande.DataSource = dt;

                if (ModAnalytique.tBCD == null) return;

                i = ModAnalytique.tBCD.Length;
                for (i = 0; i < ModAnalytique.tBCD.Length; i++)
                {
                    curtemp = ModAnalytique.tBCD[i].dbSolde;
                    dt.Rows.Add(
                        ModAnalytique.tBCD[i].lNoBonDeCommande,
                        ModAnalytique.tBCD[i].sDateCommande,
                        ModAnalytique.tBCD[i].sNomFourn,
                        ModAnalytique.tBCD[i].lnumfichestandard,
                        ModAnalytique.tBCD[i].lNoIntervention,
                        ModAnalytique.tBCD[i].sCodeImmeuble,
                        ModAnalytique.tBCD[i].sCodeEtat,
                        ModAnalytique.tBCD[i].dbTotHT,
                        ModAnalytique.tBCD[i].dbTotFacture,
                        curtemp,
                        ModAnalytique.tBCD[i].sServiceAnaly);
                    ssBonDeCommande.DataSource = dt;
                }

                ModAnalytique.tBCD = null;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private int fc_AddEtat()
        {
            int functionReturnValue = 0;
            string sSQL = null;
            DataTable rsAdd = default(DataTable);

            sSQL = "SELECT     codeimmeuble, ETAE_Noauto, Utilisateur, Numfichestandard, Code1" + " From ETAE_AffaireEntete" + " WHERE     (ETAE_Noauto = 0)";
            var ModAdo = new ModAdo();
            rsAdd = ModAdo.fc_OpenRecordSet(sSQL);
            SqlCommandBuilder cb = new SqlCommandBuilder(ModAdo.SDArsAdo);
            ModAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            ModAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            ModAdo.SDArsAdo.InsertCommand = insertCmd;
            int ETAE_Noauto = 0;
            ModAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {
                if (e.StatementType == StatementType.Insert)
                {
                    ETAE_Noauto =
                         Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                }
            });
            var dr = rsAdd.NewRow();
            dr["CodeImmeuble"] = txtCodeImmeuble.Text;
            dr["Utilisateur"] = General.fncUserName();
            dr["Numfichestandard"] = txtNumFicheStandard.Text;
            dr["Code1"] = txtGerant.Text;
            rsAdd.Rows.Add(dr.ItemArray);
            ModAdo.Update();

            functionReturnValue = ETAE_Noauto;
            ModAdo.Close();
            rsAdd = null;
            return functionReturnValue;
        }
        private void fc_clear()
        {
            lblMOPrevues.Text = "0";
            lblAchatsPrevus.Text = "0";
            lblChargesPrevues.Text = "0";
            lblVentesPrevues.Text = "0";
            lblCoefPrevu.Text = "0";
            // lblMargePrevue.Caption = "0"

            lblMOReel.Text = "0";
            lblAchatsReels.Text = "0";
            lblChargesReelles.Text = "0";
            lblVentesReelles.Text = "0";
            lblCoefficientReel.Text = "0";
            // lblMargeReelle.Caption = "0"
        }
        private void fc_MO(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto)
        {
            string sqlWhere = null;
            double dbTotHT = 0;
            double dbTempTotMO = 0;
            DataTable rsAdd = default(DataTable);


            sSQL = "SELECT     GestionStandard.NumFicheStandard, Intervention.DateRealise," + " Intervention.NoIntervention, Intervention.HeureDebut," + " Intervention.HeureFin, Intervention.Duree, " + " Intervention.Debourse AS MO, Intervention.CodeEtat," + " Immeuble.CodeImmeuble, Intervention.Intervenant" + " FROM         GestionStandard" + " INNER JOIN Intervention " + " ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard " + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble";



            sqlWhere = " WHERE   (Intervention.CodeEtat = 'AF' OR" + " Intervention.CodeEtat = 'E' OR" + " Intervention.CodeEtat = 'S' OR" + " Intervention.CodeEtat = 'T' OR" + " Intervention.CodeEtat =  'D')";


            if (lnumfichestandard != 0)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                }

            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            sSQL = sSQL + sqlWhere + " ORDER BY GestionStandard.NumFicheStandard DESC, Immeuble.CodeImmeuble,Intervention.NoIntervention ";
            var ModAdo = new ModAdo();
            rsMo = ModAdo.fc_OpenRecordSet(sSQL);

            if (chkGrille.Checked)
            {
                //== charge les taux horaire.
                ModCalcul.fc_LoadChargePers("");
            }

            //== calcul du total HT.
            dbTotHT = 0;
            foreach (DataRow dr in rsMo.Rows)
            {

                if (dr["DateRealise"] != DBNull.Value && chkGrille.Checked)
                {
                    // ERROR: Not supported in C#: OnErrorStatement

                    dbTempTotMO = ModCalcul.fc_calculMo(2, dr["Duree"] + "", dr["HeureDebut"] + "", dr["HeureFin"] + "", "", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"] + ""), true);

                    dbTotHT = dbTotHT + dbTempTotMO;
                }
                else
                {

                    dbTotHT = dbTotHT + Convert.ToDouble(General.nz(dr["MO"], 0));
                }

            }

            txtHTMO.Text = Convert.ToString(General.FncArrondir(dbTotHT, 2));


            sSQL = "SELECT ETAE_Noauto, CodeImmeuble, Code1,  Utilisateur, " + " ETAM_NumFicheStandard, ETAM_DateRealise, ETAM_NoIntervention, ETAM_HeureDebut, " + " ETAM_HeureFin , ETAM_Duree, ETAM_Mo, ETAM_CodeEtat, ETAM_Intervenant" + " From ETAM_AffaireMo" + " WHERE   ETAM_Noauto = 0";
            var ModAdoForUpdate = new ModAdo();
            rsAdd = ModAdoForUpdate.fc_OpenRecordSet(sSQL);


            foreach (DataRow dr in rsMo.Rows)
            {
                var dr1 = rsMo.NewRow();
                dr1["ETAE_Noauto"] = lETAE_NoAuto;
                dr1["CodeImmeuble"] = dr["CodeImmeuble"];
                dr1["Code1"] = txtGerant.Text;
                dr1["Utilisateur"] = General.fncUserName();
                dr1["ETAM_NumFicheStandard"] = dr["Numfichestandard"];
                dr1["ETAM_dateRealise"] = dr["DateRealise"];
                dr1["ETAM_NoIntervention"] = dr["Nointervention"];
                dr1["ETAM_HeureDebut"] = dr["HeureDebut"];
                dr1["ETAM_HeureFin"] = dr["HeureFin"];
                dr1["ETAM_Duree"] = dr["HeureFin"];
                dr1["ETAM_Mo"] = dr["MO"];
                dr1["ETAM_CodeEtat"] = dr["CodeEtat"];
                dr1["ETAM_intervenant"] = dr["Intervenant"];
                rsAdd.Rows.Add(dr1.ItemArray);
                ModAdoForUpdate.Update();

            }

            ModAdo.fc_CloseRecordset(rsAdd);

            rsAdd = null;

            ssMO.DataSource = rsMo;
            ssMO.Update();

        }
        private void fc_FactureClient(int lnumfichestandard, string sCodeImmeuble, string sCode1, int lETAE_NoAuto)
        {

            string sqlWhere = null;
            double dbTotHT = 0;
            DataTable rsAdd = default(DataTable);


            sSQL = "SELECT DISTINCT " + " FactureManuelleEnTete.NoFacture, FactureManuelleEnTete.DateFacture," + " FactureManuellePied.MontantHT AS Ventes, Immeuble.CodeImmeuble," + " Intervention.NumFicheStandard" + " FROM         Intervention " + " INNER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble " + " LEFT OUTER JOIN FactureManuelleEnTete " + " ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture" + " LEFT OUTER JOIN FactureManuellePied " + " ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle";

            sqlWhere = " WHERE  (FactureManuelleEnTete.Facture = 1)";

            if (lnumfichestandard != 0)
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Intervention.NumFicheStandard=" + lnumfichestandard + "";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Intervention.NumFicheStandard = " + lnumfichestandard + "";
                }

            }

            if (!string.IsNullOrEmpty(sCodeImmeuble))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
            }

            if (!string.IsNullOrEmpty(sCode1))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {

                    sqlWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
                else
                {

                    sqlWhere = sqlWhere + " AND immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
                }
            }

            sSQL = sSQL + sqlWhere + "ORDER BY FactureManuelleEnTete.NoFacture, Immeuble.CodeImmeuble";

            rsFClient = new DataTable();
            var ModAdo = new ModAdo();
            rsFClient = ModAdo.fc_OpenRecordSet(sSQL);

            dbTotHT = 0;
            foreach (DataRow dr in rsFClient.Rows)
            {
                dbTotHT = dbTotHT + Convert.ToDouble(General.nz(dr["Ventes"], 0));
            }

            //if (!(rsFClient.EOF & rsFClient.bof))
            //{
            //    rsFClient.MoveFirst();
            //}

            txtHTFactureClient.Text = Convert.ToString(General.FncArrondir(dbTotHT, 2));

            //== insére dans table pour prepa&ration état.

            sSQL = "SELECT     ETAC_NoAuto, ETAE_Noauto, Utilisateur, CodeImmeuble," + " Code1, ETAC_Nofacture, ETAC_dateFacture, ETAC_Mtht, ETAC_Numfichestandard" + " From ETAC_AffaireClient" + " WHERE     (ETAC_NoAuto = 0)";
            var ModAdoForUpdate = new ModAdo();
            rsAdd = ModAdo.fc_OpenRecordSet(sSQL);

            foreach (DataRow dr in rsFClient.Rows)
            {
                var dr1 = rsAdd.NewRow();
                dr1["ETAE_Noauto"] = lETAE_NoAuto;
                dr1["Utilisateur"] = General.fncUserName();
                dr1["CodeImmeuble"] = dr["CodeImmeuble"];
                dr1["Code1"] = txtGerant.Text;
                dr1["ETAC_Nofacture"] = dr["NoFacture"];
                dr1["ETAC_Datefacture"] = dr["DateFacture"];
                dr1["ETAC_MtHt"] = dr["Ventes"];
                dr1["ETAC_Numfichestandard"] = dr["Numfichestandard"];
                rsAdd.Rows.Add(dr1);
                ModAdoForUpdate.Update();
            }
            ModAdo.fc_CloseRecordset(rsAdd);
            rsAdd = null;

            ssFClient.DataSource = rsFClient;
            ssFClient.UpdateData();
        }

        #endregion

        #region Events
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbExecuter_Click(object sender, EventArgs e)
        {
            int lETAE_NoAuto = 0;
            int[] tAffaire = new int[1];
            try
            {
                //===> Mondir le 15.11.2020, modif de la version V12.11.2020 VB6
                ModAnalytique.sWhereMulti = "";
                //===> Fin Modif Mondir

                if (!string.IsNullOrEmpty(txtNumFicheStandard.Text) || !string.IsNullOrEmpty(txtCodeImmeuble.Text)
                                                                    || !string.IsNullOrEmpty(txtGerant.Text) || !string.IsNullOrEmpty(txtarretAu.Text))
                {

                    if (!string.IsNullOrEmpty(txtarretAu.Text))
                    {
                        if (!General.IsDate(txtarretAu.Text))
                        {
                            CustomMessageBox.Show("Date incorrecte.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }

                    tAffaire[0] = Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0));


                    lETAE_NoAuto = ModAnalytique.fc_AnalyAffaire(tAffaire, txtCodeImmeuble.Text, txtGerant.Text, Convert.ToInt32(General.nz((int)chkGrille.CheckState, 0)), false, txtarretAu.Text, false);

                    txtETAE_NoAuto.Text = Convert.ToString(lETAE_NoAuto);
                    lblMOPrevues.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbMoprevue);
                    lblAchatsPrevus.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbAchatPrevue);
                    lblSousTraitance.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbSstPrevue);
                    lblChargesPrevues.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbMoprevue + ModAnalytique.tCalcaffaire[0].dbAchatPrevue + ModAnalytique.tCalcaffaire[0].dbSstPrevue);
                    lblVentesPrevues.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbVentesPrevues);
                    lblCoefPrevu.Text = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbCoefPrevue);

                    txtHTClient.Text = Convert.ToString(General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbHtClient, 2));
                    fc_grilleDevis(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)), txtCodeImmeuble.Text, txtGerant.Text);

                    txtHTBCD.Text = Convert.ToString(General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbHTBCD, 2));
                    fc_grilleBonDeCommande();

                    txtHTFF.Text = Convert.ToString(General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbAchatReel, 2));
                    fc_GrilleFactFourn(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)), txtCodeImmeuble.Text, txtGerant.Text, txtarretAu.Text, ModAnalytique.sWhereMulti);

                    //===> Mondir le 15.11.2020 pour integrer les modifs de la version le V12.11.2020, REMARQUE ===> cette modfis n'etais pas dans cette version !
                    //===> Ces modfis ne sont pas affichée dans le serveur Git
                    txtHTSst.Text = Math.Round(ModAnalytique.tCalcaffaire[0].dbSstReel, 2).ToString();
                    lblSScapacite.Text = Math.Round(ModAnalytique.tCalcaffaire[0].dbSstCapacite, 2).ToString();
                    lblSScompetence.Text = Math.Round(ModAnalytique.tCalcaffaire[0].dbSstCompetence, 2).ToString();

                    fc_GrilleFactSst(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)), txtCodeImmeuble.Text, txtGerant.Text,
                        txtarretAu.Text, ModAnalytique.sWhereMulti);

                    txtHTFactureClient.Text = Math.Round(ModAnalytique.tCalcaffaire[0].dbVentesreel, 2).ToString();
                    fc_GrilleFactureClient(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, "0")), txtCodeImmeuble.Text,
                        txtGerant.Text, txtarretAu.Text);

                    txtHTMO.Text = Math.Round(ModAnalytique.tCalcaffaire[0].dbMoReel, 2).ToString();
                    fc_grilleMo(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)), txtCodeImmeuble.Text, txtGerant.Text,
                        txtarretAu.Text, ModAnalytique.sWhereMulti);

                    if (!string.IsNullOrEmpty(ModAnalytique.sWhereMulti))
                    {
                        PicMultiAppel.Visible = true;
                    }
                    else
                    {
                        PicMultiAppel.Visible = false;
                    }


                    fc_Coef(lETAE_NoAuto);

                    General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", txtNumFicheStandard.Text);
                    General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", txtCodeImmeuble.Text);
                    General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", txtGerant.Text);
                    General.saveInReg(Variable.cUserDocAnalytique2, "GrilleSalaire", ((int)chkGrille.CheckState).ToString());

                    //===> Fin Modif Mondir
                }

                //===> Mondir le 15.11.2020 pour integrer les modifs de la version le V12.11.2020
                ModAnalytique.sWhereMulti = "";
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbExecuter_Click");
            }
        }

        private void cmdCloture_Click(object sender, EventArgs e)
        {
            try
            {
                string sNodevis = null;


                sNodevis = ssDevis.ActiveRow.Cells["NumeroDevis"].Text;

                if (string.IsNullOrEmpty(sNodevis))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis en cours.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                frmClotureDev frm = new frmClotureDev();
                frm.txtNoDevis.Text = sNodevis;
                frm.ShowDialog();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdCloture_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            try
            {
                // ERROR: Not supported in C#: OnErrorStatement



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                cmbExecuter_Click(cmbExecuter, new System.EventArgs());


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ReportDocument cr = new ReportDocument();
                cr.Load(General.ETATANALYTIQUE2);
                string formula = "";
                if (!string.IsNullOrEmpty(txtETAE_NoAuto.Text))
                {
                    formula = "{ETAE_AffaireEntete.ETAE_Noauto} = " + txtETAE_NoAuto.Text + " and {ETAE_AffaireEntete.Utilisateur} ='" + General.fncUserName() + "'";
                }
                CrystalReportFormView rpt = new CrystalReportFormView(cr, formula);
                rpt.Show();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdEditer_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdZoomDevis_Click(object sender, EventArgs e)
        {
            Button cmd = sender as Button;
            TableLayoutPanel tb = cmd.Parent.Parent as TableLayoutPanel;

            if (cmd.Text == "+")
            {
                cmd.Text = "-";
                foreach (RowStyle t in tb.RowStyles)
                {
                    if (t == tb.RowStyles[tb.GetRow(cmd.Parent)])//check if the parent row of the button clicked
                        tb.RowStyles[tb.GetRow(cmd.Parent)].Height = 100;
                    else
                    {
                        if (t.SizeType == SizeType.Percent)//to minimize just the rows that contains ultragrid
                            t.Height = 1;
                    }
                }
            }
            else
            {
                cmd.Text = "+";
                foreach (RowStyle t in tb.RowStyles)
                {
                    if (t.SizeType == SizeType.Percent)
                        t.Height = (float)16.67;
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBonDeCommande_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            DataTable rs = default(DataTable);
            int lnumfichestandard = 0;
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet("Select NumFicheStandard from Intervention" + " where NoIntervention='" + ssBonDeCommande.ActiveRow.Cells["NoIntervention"].Value + "'");
            if (rs.Rows.Count > 0)
            {
                lnumfichestandard = Convert.ToInt32(General.nz(rs.Rows[0]["Numfichestandard"], 0));
                General.saveInReg("UserDocStandard", "NewVar", rs.Rows[0]["Numfichestandard"] + "");
            }
            //==Fonction en cours de developpement.
            if (General.sPrecommande == "1")
            {
                General.saveInReg(Variable.cUserPreCommande, "Origine", this.Name);
                General.saveInReg(Variable.cUserPreCommande, "NewVar", ssBonDeCommande.ActiveRow.Cells["NoIntervention"].Value + "");
                General.saveInReg(Variable.cUserPreCommande, "NumFicheStandard", Convert.ToString(lnumfichestandard));
                General.saveInReg(Variable.cUserPreCommande, "NoBonDeCommande", ssBonDeCommande.ActiveRow.Cells["NoBondeCommande"].Value + "");

                View.Theme.Theme.Navigate(typeof(UserPreCommande));
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_InitializeRow(object sender, InitializeRowEventArgs e)
        {

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            try
            {
                ModParametre.fc_SaveParamPosition(Variable.cUserDocHistoDevis, Variable.cUserDocDevis, ssDevis.ActiveRow.Cells["NumeroDEvis"].Value + "", ssDevis.ActiveRow.Cells["CodeImmeuble"].Value + "", ssDevis.ActiveRow.Cells["NumeroDEvis"].Value + "");
                View.Theme.Theme.Navigate(typeof(UserDocDevis));
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_DblClick");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBonDeCommande_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            double dbSolde = 0;
            double dbTotFour = 0;
            double dbTotFacture = 0;
            double dbDiff = 0;
            try
            {
                dbSolde = Convert.ToDouble(General.nz(e.Row.Cells["Solde"].Value, 0));
                if (dbSolde == 0)
                {
                    e.Row.Cells["Solde"].Appearance.BackColor = Color.LightGreen;
                }
                else
                {
                    dbTotFacture = Convert.ToDouble(General.nz(e.Row.Cells["TotFacture"].Value, 0));
                    if (dbTotFacture == 0)
                    {
                        e.Row.Cells["Solde"].Appearance.BackColor = Color.White;
                    }
                    else
                    {
                        dbDiff = dbTotFacture * (General.dbTolerance / 100);
                        if (System.Math.Abs(dbSolde) <= dbDiff)
                        {
                            e.Row.Cells["Solde"].Appearance.BackColor = Color.LightGreen;
                        }
                        else
                        {
                            e.Row.Cells["Solde"].Appearance.BackColor = Color.LightPink;
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBonDeCommande_RowLoaded");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactFourn_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, ssFactFourn.ActiveRow != null ? ssFactFourn.ActiveRow.Cells["NoAutoFacture"].Value + "" : "");
            if (!string.IsNullOrEmpty(ssFactFourn.ActiveRow.Cells["CheminFichier"].Text))
            {
                ModuleAPI.Ouvrir(ssFactFourn.ActiveRow.Cells["CheminFichier"].Text);
            }
            else
            {
                View.Theme.Theme.Navigate(typeof(UserDocFactFourn));
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserFactFourn);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactSst_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, ssFactFourn.ActiveRow != null ? ssFactFourn.ActiveRow.Cells["NoAutoFacture"].Value + "" : "");
            //===> Mondir le 04.09.2011, cmmened line is an old line, pour corriger https://groupe-dt.mantishub.io/view.php?id=2393
            //if (ssFactFourn.ActiveRow != null && !string.IsNullOrEmpty(ssFactFourn.ActiveRow.Cells["CheminFichier"].Text))
            if (ssFactSst.ActiveRow != null && !string.IsNullOrEmpty(ssFactSst.ActiveRow.Cells["CheminFichier"].Text))
            {
                ModuleAPI.Ouvrir(ssFactSst.ActiveRow.Cells["CheminFichier"].Text);
            }
            else
            {
                View.Theme.Theme.Navigate(typeof(UserDocFactFourn));
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserFactFourn);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFClient_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            string sCrystal = null;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            sCrystal = "{FactureManuelleEntete.NoFacture}='" + ssFClient.ActiveRow.Cells["NoFacture"].Value + "'";
            ReportDocument rpt = new ReportDocument();
            rpt.Load(General.ETATNEWFACTUREMANUEL);
            //rpt.Destination = Crystal.DestinationConstants.crptToWindow;
            CrystalReportFormView cr = new CrystalReportFormView(rpt, sCrystal);
            cr.Show();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssMO_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, ssMO.ActiveRow.Cells["NoIntervention"].Value + "");
            View.Theme.Theme.Navigate(typeof(UserIntervention));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssMO_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(e.Row.Cells["heuredebut"].Text))
            {
                e.Row.Cells["heuredebut"].Value = Convert.ToDateTime(e.Row.Cells["heuredebut"].Value).TimeOfDay.ToString();
                //Format(, "00:00:00")
            }
            if (!string.IsNullOrEmpty(e.Row.Cells["heureFin"].Text))
            {
                var t = Convert.ToDateTime(e.Row.Cells["heureFin"].Text).TimeOfDay.ToString();
                e.Row.Cells["heureFin"].Value = t;
                //Format(, "00:00:00")
            }
            if (!string.IsNullOrEmpty(e.Row.Cells["duree"].Text))
            {
                e.Row.Cells["duree"].Value = Convert.ToDateTime(e.Row.Cells["duree"].Value).TimeOfDay.ToString();
                //Format(, "00:00:00")
            }
            if (e.Row.Cells["SansPauseDejeuner"].Value != DBNull.Value)
            {
                if (e.Row.Cells["SansPauseDejeuner"].Value.ToString() == "1")
                    e.Row.Cells["SansPauseDejeuner"].Value = true;
                else
                    e.Row.Cells["SansPauseDejeuner"].Value = false;
            }
            else
            {
                e.Row.Cells["SansPauseDejeuner"].Value = false;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocAnalytique2_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            General.open_conn();
            txtNumFicheStandard.Text = General.getFrmReg(Variable.cUserDocAnalytique2, "NewVar", "");
            txtCodeImmeuble.Text = General.getFrmReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
            txtGerant.Text = General.getFrmReg(Variable.cUserDocAnalytique2, "Gerant", "");
            if (!string.IsNullOrEmpty(txtNumFicheStandard.Text) || !string.IsNullOrEmpty(txtCodeImmeuble.Text) || !string.IsNullOrEmpty(txtGerant.Text))
            {
                cmbExecuter_Click(cmbExecuter, new System.EventArgs());
            }
            //ssBonDeCommande.StyleSets["NonSOlde"].BackColor = 0xc0c0ff;
            //ssBonDeCommande.StyleSets["NonSOlde"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //ssBonDeCommande.StyleSets["Solde"].BackColor = 0xc0ffc0;
            //ssBonDeCommande.StyleSets["Solde"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //ssBonDeCommande.StyleSets["Normal"].BackColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White);
            //ssBonDeCommande.StyleSets["Normal"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
            chkGrille.CheckState = (CheckState)Convert.ToInt16(General.getFrmReg(Variable.cUserDocAnalytique2, "GrilleSalaire", Convert.ToString(0)));
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocAnalytique2");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssMO_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssMO.DisplayLayout.Bands[0].Columns["DateRealise"].Style = ColumnStyle.Date;
            ssMO.DisplayLayout.Bands[0].Columns["SansPauseDejeuner"].Style = ColumnStyle.CheckBox;
            ssMO.DisplayLayout.Bands[0].Columns["SansPauseDejeuner"].Header.Caption = "Sans Pause Dejeuner";
            foreach (var cl in ssMO.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.ActivateOnly;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactSst_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssFactSst.DisplayLayout.Bands[0].Columns["EC_NoFacture"].Hidden = true;
            ssFactSst.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            ssFactSst.DisplayLayout.Bands[0].Columns["DateLivraison"].Header.Caption = "DateFacture";
            foreach (var cl in ssFactSst.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.NoEdit;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactFourn_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssFactFourn.DisplayLayout.Bands[0].Columns["EC_NoFacture"].Hidden = true;
            ssFactFourn.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            ssFactFourn.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            ssFactFourn.DisplayLayout.Bands[0].Columns["DateLivraison"].Header.Caption = "DateFacture";
            foreach (var cl in ssFactFourn.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.NoEdit;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssDevis.DisplayLayout.Bands[0].Columns["DateCreation"].Header.Caption = "DateCreation Devis";
            foreach (var cl in ssDevis.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.NoEdit;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBonDeCommande_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssBonDeCommande.DisplayLayout.Bands[0].Columns["TotFacture"].Header.Caption = "Tot Facturé";
            ssBonDeCommande.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            foreach (var cl in ssBonDeCommande.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.NoEdit;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFClient_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            foreach (var cl in ssFClient.DisplayLayout.Bands[0].Columns)
            {
                cl.CellActivation = Activation.ActivateOnly;
            }
        }

        #endregion

        #region unused functions
        /*=========== UNUSED FUNCTIONS function to zoom in & zoom out the ultragrid . i do my custom function*/
        //==================BEGIN 
        private void cmdZoom_ControlFetchBon(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try { X = tTctlBon.Length; } catch (Exception ex) { Program.SaveException(ex); }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchBon(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cBon)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTctlBon, i + 1);

                        tTctlBon[i].sName = cc.Name;

                        tTctlBon[i].dbHeight = cc.Height;
                        tTctlBon[i].dbLeft = cc.Left;
                        tTctlBon[i].dbTop = cc.Top;
                        tTctlBon[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cBon))
                {
                    for (i = 0; i < tTctlBon.Length; i++)
                    {
                        if (General.UCase(tTctlBon[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTctlBon[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTctlBon[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTctlBon[i].dbTop);
                            cc.Width = Convert.ToInt32(tTctlBon[i].dbWith);
                        }
                    }
                }
            }
        }
        private void cmdzoomBon_Click(object sender, EventArgs e)
        {
            object ctl = null;
            int i = 0;
            int X = 0;
            Button cmdzoomBon = sender as Button;
            if (cmdzoomBon.Text == "+")
            {
                cmdzoomBon.Text = "-";
                //== controle si le tableau est deja alimenté.
                // ERROR: Not supported in C#: OnErrorStatement

                try { X = tTctlBon.Length; } catch (Exception ex) { Program.SaveException(ex); }
                // ERROR: Not supported in C#: OnErrorStatement
                cmdZoom_ControlFetchBon(this, cmdzoomBon.Text);

                fc_Zoom(cBon);
            }
            else
            {
                cmdzoomBon.Text = "+";
                cmdZoom_ControlFetchBon(this, cmdzoomBon.Text);
            }
        }

        void fc_Zoom_fetchControl(Control cc, string sTag)
        {
            foreach (Control c in cc.Controls)
            {
                fc_Zoom_fetchControl(c, sTag);
            }
            if (cc.Tag.ToString().Contains(sTag))
            {
                if (cc is System.Windows.Forms.PictureBox)
                {

                    cc.Width = cZFRAWidht;

                    cc.Height = cZFraHeight;

                    cc.Top = cZFraTop;

                    cc.Left = cZFraLeft;

                }
                else if (cc is System.Windows.Forms.Button)
                {

                    cc.Width = cBtWidht;

                    cc.Height = cBtHeight;

                    cc.Top = cBtTop;

                    cc.Left = cBtLeft;

                }
                else if (cc is UltraGrid)
                {

                    cc.Width = cGridWidht;

                    cc.Height = cGridHeight;

                    cc.Top = cGridTop;

                    cc.Left = cGridLeft;

                }
                else if (cc is System.Windows.Forms.Label)
                {

                    cc.Width = clblWidht;

                    cc.Height = clblTotHeiht;

                    cc.Top = clblTop;

                    cc.Left = clblLeft;

                }
                else if (cc is iTalk.iTalk_TextBox_Small2)
                {

                    cc.Width = ctxtWidht;

                    cc.Height = ctxtTotHeight;

                    cc.Top = ctxtTop;

                    cc.Left = ctxtRotLeft;

                }
            }
        }
        private void fc_Zoom(string sTag)
        {
            fc_Zoom_fetchControl(this, sTag);
        }
        private void cmdZoomFF_Click(object sender, EventArgs e)
        {
            int i = 0;
            int X = 0;
            Button cmdzoomFF = sender as Button;
            if (cmdzoomFF.Text == "+")
            {
                cmdzoomFF.Text = "-";
                //== controle si le tableau est deja alimenté.
                // ERROR: Not supported in C#: OnErrorStatement

                try { X = tTctlBon.Length; } catch (Exception ex) { Program.SaveException(ex); }
                // ERROR: Not supported in C#: OnErrorStatement
                cmdZoom_ControlFetchFF(this, cmdzoomFF.Text);

                fc_Zoom(cFF);
            }
            else
            {
                cmdzoomFF.Text = "+";
                cmdZoom_ControlFetchFF(this, cmdzoomFF.Text);
            }
        }

        void cmdZoom_ControlFetchFF(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try { X = tTctlFF.Length; } catch { }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchFF(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cFF)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTctlFF, i + 1);

                        tTctlFF[i].sName = cc.Name;

                        tTctlFF[i].dbHeight = cc.Height;
                        tTctlFF[i].dbLeft = cc.Left;
                        tTctlFF[i].dbTop = cc.Top;
                        tTctlFF[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cFF))
                {
                    for (i = 0; i < tTctlFF.Length; i++)
                    {
                        if (General.UCase(tTctlFF[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTctlFF[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTctlFF[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTctlFF[i].dbTop);
                            cc.Width = Convert.ToInt32(tTctlFF[i].dbWith);
                        }
                    }
                }
            }
        }
        void cmdZoom_ControlFetchDevis(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try { X = tTCtldevis.Length; } catch (Exception e) { Program.SaveException(e); }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchDevis(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cDEVIS)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTCtldevis, i + 1);

                        tTCtldevis[i].sName = cc.Name;

                        tTCtldevis[i].dbHeight = cc.Height;
                        tTCtldevis[i].dbLeft = cc.Left;
                        tTCtldevis[i].dbTop = cc.Top;
                        tTCtldevis[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cDEVIS))
                {
                    for (i = 0; i < tTCtldevis.Length; i++)
                    {
                        if (General.UCase(tTCtldevis[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTCtldevis[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTCtldevis[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTCtldevis[i].dbTop);
                            cc.Width = Convert.ToInt32(tTCtldevis[i].dbWith);
                        }
                    }
                }
            }
        }
        void cmdZoom_ControlFetchSST(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try { X = tTctlSst.Length; } catch (Exception e) { Program.SaveException(e); }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchDevis(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cSst)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTctlSst, i + 1);

                        tTctlSst[i].sName = cc.Name;

                        tTctlSst[i].dbHeight = cc.Height;
                        tTctlSst[i].dbLeft = cc.Left;
                        tTctlSst[i].dbTop = cc.Top;
                        tTctlSst[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cSst))
                {
                    for (i = 0; i < tTctlSst.Length; i++)
                    {
                        if (General.UCase(tTctlSst[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTctlSst[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTctlSst[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTctlSst[i].dbTop);
                            cc.Width = Convert.ToInt32(tTctlSst[i].dbWith);
                        }
                    }
                }
            }
        }
        private void cmdZoomSST_Click(object sender, EventArgs e)
        {

        }

        void cmdZoom_ControlFetchFClient(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try { X = tTclFClient.Length; } catch (Exception e) { Program.SaveException(e); }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchFClient(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cFc)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTclFClient, i + 1);

                        tTclFClient[i].sName = cc.Name;

                        tTclFClient[i].dbHeight = cc.Height;
                        tTclFClient[i].dbLeft = cc.Left;
                        tTclFClient[i].dbTop = cc.Top;
                        tTclFClient[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cFc))
                {
                    for (i = 0; i < tTclFClient.Length; i++)
                    {
                        if (General.UCase(tTclFClient[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTclFClient[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTclFClient[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTclFClient[i].dbTop);
                            cc.Width = Convert.ToInt32(tTclFClient[i].dbWith);
                        }
                    }
                }
            }
        }
        private void cmdZoomFClient_Click(object sender, EventArgs e)
        {

        }

        void cmdZoom_ControlFetchMO(Control cc, string text)
        {
            int i = 0;
            int X = 0;
            try
            {
                X = tTclMO.Length;
            }
            catch (Exception e) { Program.SaveException(e); }
            foreach (Control c in cc.Controls)
            {
                cmdZoom_ControlFetchMO(c, text);
            }
            if (text == "+")
            {
                if (cc.Tag.ToString().Contains(General.UCase(cMO)))
                {
                    if (X == 0)
                    {
                        Array.Resize(ref tTclMO, i + 1);

                        tTclMO[i].sName = cc.Name;

                        tTclMO[i].dbHeight = cc.Height;
                        tTclMO[i].dbLeft = cc.Left;
                        tTclMO[i].dbTop = cc.Top;
                        tTclMO[i].dbWith = cc.Width;
                    }
                    //cc./*ZOrder*/(0);
                    i = i + 1;
                }
            }
            else
            {
                if (cc.Tag.ToString().Contains(cMO))
                {
                    for (i = 0; i < tTclMO.Length; i++)
                    {
                        if (General.UCase(tTclMO[i].sName) == General.UCase(cc.Name))
                        {
                            cc.Height = Convert.ToInt32(tTclMO[i].dbHeight);
                            cc.Left = Convert.ToInt32(tTclMO[i].dbLeft);
                            cc.Top = Convert.ToInt32(tTclMO[i].dbTop);
                            cc.Width = Convert.ToInt32(tTclMO[i].dbWith);
                        }
                    }
                }
            }
        }
        private void cmdZoomMO_Click(object sender, EventArgs e)
        {

        }

        private void cmdRechercheAppel_Click(object sender, EventArgs e)
        {

        }
        //===========END
        #endregion

        private void UserDocAnalytique2_Load(object sender, EventArgs e)
        {
            //===> Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020
            PicMultiAppel.Visible = false;
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblSynthese_Click(object sender, EventArgs e)
        {
            General.saveInReg(Variable.cUserSyntheseAppel, "NewVar", txtNumFicheStandard.Text);

            View.Theme.Theme.Navigate(typeof(UserSyntheseAppel));
        }
    }
}
