﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinEditors;
using Microsoft.VisualBasic;

namespace Axe_interDT.Views.Analytique.User_DocAnalytique2.Forms
{
    public partial class frmClotureDev : Form
    {
        public frmClotureDev()
        {
            InitializeComponent();
        }
         const string cStatutTermine="T";
         const string sStatutP="P3";

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="sCodeimmeuble"></param>
        /// <param name="sTatut"></param>
        private void fc_Majreception(string sNodevis,string sCodeimmeuble,string sTatut)
        {
            string sSQL = "";
            DataTable rs = default(DataTable);
            try
            {
                if (Check46.Checked==false && Check47.Checked)
                {
                    if (General.UCase(sTatut) != General.UCase(cStatutTermine) &&
                        General.UCase(sTatut) != General.UCase(sStatutP))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous ne pouvez pas réceptionner un devis tant qu'il n' pas le statut T (terminée) ou P3.",
                            "Opération anuulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Check47.Checked = false;
                        DTPicker10.Value = null;
                        return;
                    }
                    fc_AlerteMailDevis(sNodevis);
                    txtDevisEntete26.Text = General.fncUserName();
                    Check46.Checked = true;

                }
                else
                {
                    Check46.Checked = Check47.Checked;
                    if (!Check47.Checked)
                        DTPicker10.Value = null;
                }
                if (!Check49.Checked && Check48.Checked)
                {
                    if (General.UCase(sTatut) != General.UCase(cStatutTermine) &&
                        General.UCase(sTatut) != General.UCase(sStatutP))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous ne pouvez pas clôturer un devis tant qu'il n' pas le statut T (terminée) ou P3.",
                            "Opération anuulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Check48.Checked = false;
                        DTPicker11.Value = null;
                        return;
                    }
                    fc_AlerteMailClotureDevis(sNodevis, sCodeimmeuble);
                    Check49.Checked = true;
                }
                else
                {
                    Check49.Checked = Check48.Checked;
                    if (Check48.Checked == false)
                        DTPicker11.Value = null;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_MajReception");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        public void fc_AlerteMailDevis(string sNodevis)
        {
            string sSQL;
            DataTable rs = default(DataTable);
            string sCode;
            string sCodeImm;
            string sSujet;
            string sMessage;
            try
            {
                sSQL =
                    "SELECT     NumeroDevis, CodeDeviseur, codeimmeuble  from DevisEnTete  WHERE     NumeroDevis = '" +
                    StdSQLchaine.gFr_DoublerQuote(sNodevis) + "'";
                var ModAdo=new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    sCode = rs.Rows[0]["CodeDeviseur"].ToString();
                    sCodeImm = rs.Rows[0]["CodeImmeuble"].ToString();
                }
                else
                {
                    ModAdo.Close();
                    rs = null;
                    return;
                }
                ModAdo.fc_CloseRecordset(rs);

                sSQL = "SELECT     Personnel_1.Email, Personnel_1.Nom, Personnel_1.Prenom FROM         Personnel INNER JOIN  Personnel AS Personnel_1 ON Personnel.RespService = Personnel_1.Initiales  WHERE     Personnel.Matricule = '"+StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(General.nz(rs.Rows[0]["Email"], "").ToString()))
                    {
                        
                        sSujet = "Devis n°" + sNodevis + " réceptionné, immeuble" + sCodeImm;
                        sMessage = "Bonjour M." + rs.Rows[0]["Nom"]+ "," +"\r\n" + "\r\n";
                        sMessage = sMessage + "Le devis numéro " + sNodevis + ", immeuble " + sCodeImm +
                                   " a été réceptionné," + "\r\n";
                        sMessage = sMessage + "merci de procéder à la clôture du devis.";
                        sMessage = sMessage + "\r\n";
                        sMessage = sMessage + "\r\n";
                        sMessage = sMessage + "Cet e-mail vous a été envoyé par un automate, merci de ne pas répondre.";
                        ModCourrier.CreateMail(rs.Rows[0]["Email"].ToString(), "", sSujet, sMessage);

                    }
                }
                ModAdo.Close();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModDiv;fc_AlerteMailDevis");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="sCodeimmeuble"></param>
        public void fc_AlerteMailClotureDevis(string sNodevis, string sCodeimmeuble)
        {
            string sSujet;
            string sMessage;
            try
            {
                sSujet = "Devis n°" + sNodevis + " réceptionné, immeuble" + sCodeimmeuble;
                sMessage = "Bonjour ,"+"\r\n" + "\r\n";
                sMessage = sMessage + "-   Pour information le devis numéro " + sNodevis + ", immeuble " + sCodeimmeuble +
                           " a été clôturé." + "\r\n";

                sMessage = sMessage+ "\r\n";
                sMessage = sMessage + "Cet e-mail vous a été envoyé par un automate, merci de ne pas répondre.";

                ModCourrier.CreateMail("lm.gresle@groupe-dt.fr", "", sSujet, sMessage);
                ModCourrier.CreateMail("F.LEBORGNE@groupe-dt.fr", "", sSujet, sMessage);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModDiv;fc_AlerteMailDevis");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            if (fc_CtrlErrAvanc() == true)
                return;
            fc_Majreception(txtNoDevis.Text,txtCodeImmeuble.Text,txtCodeEtat.Text);

            Cursor.Current = Cursors.WaitCursor;
            fc_ValideAvance(txtNoDevis.Text);


        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_ValideAvance(string sNodevis)
        {
            string sSQL;
            DataTable rs = default(DataTable);
            try
            {
                if(string.IsNullOrEmpty(sNodevis))return;

                sSQL= "SELECT         NumeroDevis, Receptionne, ComSurRecept, ClotureDevis, ComSurCloture, DateReception, ReceptionnePar, " +
                      "ClotureLe  FROM DevisEnTete  WHERE NumeroDevis ='"+StdSQLchaine.gFr_DoublerQuote(sNodevis) +
                "'";//TODO add to the QUERY 'NumeroDevis' because this column is the primary key of the table.ModAdo.update() based on this column to write the update QUERY
                var ModAdo=new ModAdo();

                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    rs.Rows[0]["ComSurRecept"] = General.nz(txtDevisEntete23.Text, DBNull.Value);
                    rs.Rows[0]["ComSurCloture"] = General.nz(txtDevisEntete24.Text, DBNull.Value);
                    rs.Rows[0]["Receptionne"] = General.nz(Convert.ToInt32(Check47.CheckState), DBNull.Value);
                    rs.Rows[0]["ClotureDevis"] = General.nz(Convert.ToInt32(Check48.CheckState), DBNull.Value);
                    if (DTPicker10.Value!=null && General.IsDate(DTPicker10.Value))
                        rs.Rows[0]["DateReception"] = DTPicker10.Value;
                    else
                    {
                        rs.Rows[0]["DateReception"] = DBNull.Value;
                    }
                    if(DTPicker11.Value != null && General.IsDate(DTPicker11.Value))
                        rs.Rows[0]["ClotureLe"] = DTPicker11.Value;
                    else
                    {
                        rs.Rows[0]["ClotureLe"] = DBNull.Value;
                    }
                    rs.Rows[0]["ReceptionnePar"] = General.nz(txtDevisEntete26.Text, DBNull.Value);
                  
                    
                }
                ModAdo.Update();
                ModAdo.Close();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_ValideAvance");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public bool fc_CtrlErrAvanc()
        {
            //'=== retourne true en cas d'erreur constatée.
            if (Check47.Checked && !General.IsDate(DTPicker10.Value))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    "Vous avez cliqué sur la case à cocher 'réceptionné', vous devez saisir une date de clôture.",
                    "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }

            if (Check48.Checked && !General.IsDate(DTPicker11.Value))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    "Vous avez cliqué sur la case à cocher 'clôturé', vous devez saisir une date de clôture.",
                    "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return true;
            }
            return false;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_load(string sNodevis)
        {
            string sSQL;
            DataTable rs = default(DataTable);

            try
            {
                if (string.IsNullOrEmpty(sNodevis))
                    return;
                sSQL= "SELECT CodeEtat,CodeImmeuble,         Receptionne, ComSurRecept, " +
                      "ClotureDevis, ComSurCloture, DateReception, ReceptionnePar, ClotureLe  FROM DevisEnTete  " +
                      "WHERE NumeroDevis ='"+StdSQLchaine.gFr_DoublerQuote(sNodevis)+ "'"; 
                var ModAdo=new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    txtCodeEtat.Text = rs.Rows[0]["CodeEtat"].ToString();
                    txtCodeImmeuble.Text = rs.Rows[0]["CodeImmeuble"].ToString();
                    txtDevisEntete23.Text = rs.Rows[0]["ComSurRecept"].ToString();
                    txtDevisEntete24.Text = rs.Rows[0]["ComSurCloture"].ToString();
                    Check47.Checked = Convert.ToInt32(General.nz(rs.Rows[0]["Receptionne"], 0)) ==0 ? false : true;
                    Check48.Checked = Convert.ToInt32(General.nz(rs.Rows[0]["ClotureDevis"], 0)) ==0 ? false : true;
                    DTPicker10.Value = General.IsDate(rs.Rows[0]["DateReception"]) ? rs.Rows[0]["DateReception"] : null;
                    DTPicker11.Value = General.IsDate(rs.Rows[0]["ClotureLe"]) ? rs.Rows[0]["ClotureLe"] : null;
                    txtDevisEntete26.Text = rs.Rows[0]["ReceptionnePar"].ToString();
                }
                ModAdo.Update();
                ModAdo.Close();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_load");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmClotureDev_Load(object sender, EventArgs e)
        {
            fc_load(txtNoDevis.Text);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check48_CheckedChanged(object sender, EventArgs e)
        {
            var tag = Convert.ToInt32((sender as CheckBox).Tag);
            switch (tag)
            {
                case 7:
                    if (Check47.Checked == false)
                    {
                        DTPicker10.Value = null;
                        txtDevisEntete26.Text = "";
                    }
                    else
                    {
                        DTPicker10.Value = DateTime.Now;
                    }
                    break;
                case 8:
                    if (Check48.Checked == false)
                    {
                        DTPicker11.Value = null;
                    }
                    else
                    {
                        DTPicker11.Value = DateTime.Now;
                    }
                    break;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DTPicker10_BeforeDropDown(object sender, CancelEventArgs e)
        {
            var sen = (sender as UltraDateTimeEditor);
            var checkbox =(StateEditorButton)sen.ButtonsLeft[0];
            checkbox.Checked = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DTPicker11_AfterEditorButtonCheckStateChanged(object sender, EditorButtonEventArgs e)
        {
            var sen = sender as UltraDateTimeEditor;
            var check = (StateEditorButton) e.Button;
            if (check.Checked)
                sen.Value = DateTime.Now;
            else
            {
                sen.Value = null;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DTPicker11_ValueChanged(object sender, EventArgs e)
        {
            var sen = sender as UltraDateTimeEditor;
            var checkbox = (StateEditorButton)sen.ButtonsLeft[0];
            if (sen.Value == null)
                checkbox.Checked = false;
            else
            {
                checkbox.Checked = true;
            }
        }
    }
}
