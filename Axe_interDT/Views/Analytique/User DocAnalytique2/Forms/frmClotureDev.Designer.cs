﻿namespace Axe_interDT.Views.Analytique.User_DocAnalytique2.Forms
{
    partial class frmClotureDev
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Button CmdSauver;
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton1 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton2 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.Check48 = new System.Windows.Forms.CheckBox();
            this.txtDevisEntete24 = new iTalk.iTalk_RichTextBox();
            this.txtDevisEntete23 = new iTalk.iTalk_RichTextBox();
            this.DTPicker10 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DTPicker11 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDevisEntete26 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeEtat = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNoDevis = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.Check49 = new System.Windows.Forms.CheckBox();
            this.Check47 = new System.Windows.Forms.CheckBox();
            this.Check46 = new System.Windows.Forms.CheckBox();
            CmdSauver = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker11)).BeginInit();
            this.SuspendLayout();
            // 
            // CmdSauver
            // 
            CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            CmdSauver.FlatAppearance.BorderSize = 0;
            CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            CmdSauver.Location = new System.Drawing.Point(436, 1);
            CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            CmdSauver.Name = "CmdSauver";
            CmdSauver.Size = new System.Drawing.Size(60, 35);
            CmdSauver.TabIndex = 576;
            CmdSauver.UseVisualStyleBackColor = false;
            CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(CmdSauver);
            this.ultraGroupBox1.Controls.Add(this.Check48);
            this.ultraGroupBox1.Controls.Add(this.txtDevisEntete24);
            this.ultraGroupBox1.Controls.Add(this.txtDevisEntete23);
            this.ultraGroupBox1.Controls.Add(this.DTPicker10);
            this.ultraGroupBox1.Controls.Add(this.DTPicker11);
            this.ultraGroupBox1.Controls.Add(this.label4);
            this.ultraGroupBox1.Controls.Add(this.label5);
            this.ultraGroupBox1.Controls.Add(this.label3);
            this.ultraGroupBox1.Controls.Add(this.txtDevisEntete26);
            this.ultraGroupBox1.Controls.Add(this.txtCodeEtat);
            this.ultraGroupBox1.Controls.Add(this.label2);
            this.ultraGroupBox1.Controls.Add(this.txtCodeImmeuble);
            this.ultraGroupBox1.Controls.Add(this.label1);
            this.ultraGroupBox1.Controls.Add(this.txtNoDevis);
            this.ultraGroupBox1.Controls.Add(this.label33);
            this.ultraGroupBox1.Controls.Add(this.Check49);
            this.ultraGroupBox1.Controls.Add(this.Check47);
            this.ultraGroupBox1.Controls.Add(this.Check46);
            this.ultraGroupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 3);
            this.ultraGroupBox1.MinimumSize = new System.Drawing.Size(498, 545);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(514, 579);
            this.ultraGroupBox1.TabIndex = 0;
            // 
            // Check48
            // 
            this.Check48.AutoSize = true;
            this.Check48.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check48.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Check48.Location = new System.Drawing.Point(56, 387);
            this.Check48.Name = "Check48";
            this.Check48.Size = new System.Drawing.Size(81, 23);
            this.Check48.TabIndex = 575;
            this.Check48.Tag = "8";
            this.Check48.Text = "Clôturé";
            this.Check48.UseVisualStyleBackColor = true;
            this.Check48.CheckedChanged += new System.EventHandler(this.Check48_CheckedChanged);
            // 
            // txtDevisEntete24
            // 
            this.txtDevisEntete24.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevisEntete24.AutoWordSelection = false;
            this.txtDevisEntete24.BackColor = System.Drawing.Color.Transparent;
            this.txtDevisEntete24.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevisEntete24.ForeColor = System.Drawing.Color.Black;
            this.txtDevisEntete24.Location = new System.Drawing.Point(142, 440);
            this.txtDevisEntete24.Name = "txtDevisEntete24";
            this.txtDevisEntete24.ReadOnly = false;
            this.txtDevisEntete24.Size = new System.Drawing.Size(310, 131);
            this.txtDevisEntete24.TabIndex = 7;
            this.txtDevisEntete24.WordWrap = true;
            // 
            // txtDevisEntete23
            // 
            this.txtDevisEntete23.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevisEntete23.AutoWordSelection = false;
            this.txtDevisEntete23.BackColor = System.Drawing.Color.Transparent;
            this.txtDevisEntete23.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevisEntete23.ForeColor = System.Drawing.Color.Black;
            this.txtDevisEntete23.Location = new System.Drawing.Point(142, 271);
            this.txtDevisEntete23.Name = "txtDevisEntete23";
            this.txtDevisEntete23.ReadOnly = false;
            this.txtDevisEntete23.Size = new System.Drawing.Size(300, 110);
            this.txtDevisEntete23.TabIndex = 5;
            this.txtDevisEntete23.WordWrap = true;
            // 
            // DTPicker10
            // 
            this.DTPicker10.ButtonsLeft.Add(stateEditorButton1);
            this.DTPicker10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker10.Location = new System.Drawing.Point(143, 176);
            this.DTPicker10.Name = "DTPicker10";
            this.DTPicker10.Size = new System.Drawing.Size(299, 26);
            this.DTPicker10.TabIndex = 3;
            this.DTPicker10.ValueChanged += new System.EventHandler(this.DTPicker11_ValueChanged);
            this.DTPicker10.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker10_BeforeDropDown);
            this.DTPicker10.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.DTPicker11_AfterEditorButtonCheckStateChanged);
            // 
            // DTPicker11
            // 
            appearance1.BorderColor = System.Drawing.Color.White;
            appearance1.BorderColor2 = System.Drawing.Color.White;
            stateEditorButton2.Appearance = appearance1;
            appearance2.BorderColor = System.Drawing.Color.White;
            appearance2.BorderColor2 = System.Drawing.Color.White;
            stateEditorButton2.PressedAppearance = appearance2;
            this.DTPicker11.ButtonsLeft.Add(stateEditorButton2);
            this.DTPicker11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DTPicker11.Location = new System.Drawing.Point(142, 387);
            this.DTPicker11.Name = "DTPicker11";
            this.DTPicker11.Size = new System.Drawing.Size(299, 26);
            this.DTPicker11.TabIndex = 6;
            this.DTPicker11.ValueChanged += new System.EventHandler(this.DTPicker11_ValueChanged);
            this.DTPicker11.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker10_BeforeDropDown);
            this.DTPicker11.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.DTPicker11_AfterEditorButtonCheckStateChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(149, 421);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(200, 19);
            this.label4.TabIndex = 571;
            this.label4.Text = "Commentaire sur réception";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(149, 252);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(200, 19);
            this.label5.TabIndex = 571;
            this.label5.Text = "Commentaire sur réception";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(9, 206);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(123, 19);
            this.label3.TabIndex = 571;
            this.label3.Text = "Réceptionne par";
            // 
            // txtDevisEntete26
            // 
            this.txtDevisEntete26.AccAcceptNumbersOnly = false;
            this.txtDevisEntete26.AccAllowComma = false;
            this.txtDevisEntete26.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDevisEntete26.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDevisEntete26.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevisEntete26.AccHidenValue = "";
            this.txtDevisEntete26.AccNotAllowedChars = null;
            this.txtDevisEntete26.AccReadOnly = false;
            this.txtDevisEntete26.AccReadOnlyAllowDelete = false;
            this.txtDevisEntete26.AccRequired = false;
            this.txtDevisEntete26.BackColor = System.Drawing.Color.White;
            this.txtDevisEntete26.CustomBackColor = System.Drawing.Color.White;
            this.txtDevisEntete26.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDevisEntete26.ForeColor = System.Drawing.Color.Blue;
            this.txtDevisEntete26.Location = new System.Drawing.Point(143, 206);
            this.txtDevisEntete26.Margin = new System.Windows.Forms.Padding(2);
            this.txtDevisEntete26.MaxLength = 32767;
            this.txtDevisEntete26.Multiline = false;
            this.txtDevisEntete26.Name = "txtDevisEntete26";
            this.txtDevisEntete26.ReadOnly = true;
            this.txtDevisEntete26.Size = new System.Drawing.Size(299, 27);
            this.txtDevisEntete26.TabIndex = 4;
            this.txtDevisEntete26.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDevisEntete26.UseSystemPasswordChar = false;
            // 
            // txtCodeEtat
            // 
            this.txtCodeEtat.AccAcceptNumbersOnly = false;
            this.txtCodeEtat.AccAllowComma = false;
            this.txtCodeEtat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeEtat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeEtat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeEtat.AccHidenValue = "";
            this.txtCodeEtat.AccNotAllowedChars = null;
            this.txtCodeEtat.AccReadOnly = false;
            this.txtCodeEtat.AccReadOnlyAllowDelete = false;
            this.txtCodeEtat.AccRequired = false;
            this.txtCodeEtat.BackColor = System.Drawing.Color.White;
            this.txtCodeEtat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeEtat.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeEtat.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeEtat.Location = new System.Drawing.Point(142, 135);
            this.txtCodeEtat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeEtat.MaxLength = 32767;
            this.txtCodeEtat.Multiline = false;
            this.txtCodeEtat.Name = "txtCodeEtat";
            this.txtCodeEtat.ReadOnly = true;
            this.txtCodeEtat.Size = new System.Drawing.Size(299, 27);
            this.txtCodeEtat.TabIndex = 2;
            this.txtCodeEtat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeEtat.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(90, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(52, 19);
            this.label2.TabIndex = 571;
            this.label2.Text = "Statut";
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(142, 104);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = true;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(299, 27);
            this.txtCodeImmeuble.TabIndex = 1;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 100);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 571;
            this.label1.Text = "Code immeuble";
            // 
            // txtNoDevis
            // 
            this.txtNoDevis.AccAcceptNumbersOnly = false;
            this.txtNoDevis.AccAllowComma = false;
            this.txtNoDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoDevis.AccHidenValue = "";
            this.txtNoDevis.AccNotAllowedChars = null;
            this.txtNoDevis.AccReadOnly = false;
            this.txtNoDevis.AccReadOnlyAllowDelete = false;
            this.txtNoDevis.AccRequired = false;
            this.txtNoDevis.BackColor = System.Drawing.Color.White;
            this.txtNoDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNoDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoDevis.ForeColor = System.Drawing.Color.Blue;
            this.txtNoDevis.Location = new System.Drawing.Point(143, 74);
            this.txtNoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoDevis.MaxLength = 32767;
            this.txtNoDevis.Multiline = false;
            this.txtNoDevis.Name = "txtNoDevis";
            this.txtNoDevis.ReadOnly = true;
            this.txtNoDevis.Size = new System.Drawing.Size(299, 27);
            this.txtNoDevis.TabIndex = 0;
            this.txtNoDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoDevis.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(73, 74);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 19);
            this.label33.TabIndex = 571;
            this.label33.Text = "N°Devis";
            // 
            // Check49
            // 
            this.Check49.AutoSize = true;
            this.Check49.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check49.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.Check49.Location = new System.Drawing.Point(61, 35);
            this.Check49.Name = "Check49";
            this.Check49.Size = new System.Drawing.Size(81, 23);
            this.Check49.TabIndex = 570;
            this.Check49.Tag = "9";
            this.Check49.Text = "Clôturé";
            this.Check49.UseVisualStyleBackColor = true;
            this.Check49.Visible = false;
            // 
            // Check47
            // 
            this.Check47.AutoSize = true;
            this.Check47.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check47.Location = new System.Drawing.Point(22, 176);
            this.Check47.Name = "Check47";
            this.Check47.Size = new System.Drawing.Size(116, 23);
            this.Check47.TabIndex = 570;
            this.Check47.Tag = "7";
            this.Check47.Text = "Réceptionné";
            this.Check47.UseVisualStyleBackColor = true;
            this.Check47.CheckedChanged += new System.EventHandler(this.Check48_CheckedChanged);
            // 
            // Check46
            // 
            this.Check46.AutoSize = true;
            this.Check46.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check46.Location = new System.Drawing.Point(24, 9);
            this.Check46.Name = "Check46";
            this.Check46.Size = new System.Drawing.Size(116, 23);
            this.Check46.TabIndex = 570;
            this.Check46.Tag = "6";
            this.Check46.Text = "Réceptionné";
            this.Check46.UseVisualStyleBackColor = true;
            this.Check46.Visible = false;
            // 
            // frmClotureDev
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(520, 586);
            this.Controls.Add(this.ultraGroupBox1);
            this.MaximumSize = new System.Drawing.Size(536, 625);
            this.MinimumSize = new System.Drawing.Size(536, 625);
            this.Name = "frmClotureDev";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmClotureDev";
            this.Load += new System.EventHandler(this.frmClotureDev_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker11)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        public System.Windows.Forms.CheckBox Check49;
        public System.Windows.Forms.CheckBox Check46;
        public System.Windows.Forms.Label label33;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTPicker11;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtDevisEntete26;
        public iTalk.iTalk_TextBox_Small2 txtCodeEtat;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtNoDevis;
        public System.Windows.Forms.CheckBox Check47;
        public System.Windows.Forms.CheckBox Check48;
        public iTalk.iTalk_RichTextBox txtDevisEntete24;
        public iTalk.iTalk_RichTextBox txtDevisEntete23;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTPicker10;
        public System.Windows.Forms.Label label4;
    }
}