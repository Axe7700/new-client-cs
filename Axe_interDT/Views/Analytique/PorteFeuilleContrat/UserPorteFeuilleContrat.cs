﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using static Axe_interDT.Shared.Variable;

namespace Axe_interDT.Views.PorteFeuilleContrat
{
    public partial class UserPorteFeuilleContrat : UserControl
    {
        public UserPorteFeuilleContrat()
        {
            InitializeComponent();
        }

        string sSQL;

        PictureBox f;

        string fmsg;

        DataTable rs;
        DataTable rsPorteFeuille;
        DataTable rsGrpPrtFeuille;

        bool blnErreur;
        //Const cCheminFichierPorteFeuille = "C:\ErreurPorteFeuilleContrat.txt"
        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cLibArret = "Date d'arrêt";
        const short cCol = 0;

        const string cDateArret = "DateArret";
        const string cNumContrat = "N°Contrat";
        const string CAvenant = "Avenant";
        const string cCodeImmeuble = "Code Immeuble";
        const string cCode1 = "Gerant";
        const string cCodeArticle = "Code Article";
        const string cDesignation = "Libelle";
        const string cCA_Num = "Activite";
        const string cMatricule = "Matricule";
        const string cInitial = "Initial";
        const string cPrenom = "Prenom";
        const string cNom = "Nom";

        const string cGrpArret = "POC_DateArret";
        const string cGrpMat = "Matricule";
        const string cGrpInit = "Initiale";
        const string cGrpPrenom = "Prenom";
        const string cGrpNom = "Nom";
        const string cGrpNbCont = "Nb de contrat";
        const string cGrpTotalHT = "TotalHT";

        const string cGrpNbP1 = "Nb P1";
        const string cGrpNbP2 = "Nb P2";
        const string cGrpNbP2avt = "Nb P2 avt";
        const string cGrpNbP3 = "Nb P3";
        const string cGrpNbP4 = "Nb P4";

        const string cGrpTotalHTp2C_p2avt = "Total HT P2C + P2 avenant";
        const string cGrpTotalHTp1 = "Total HT P1";
        const string cGrpTotalHTp2 = "Total HT P2 C";
        const string cGrpTotalHTp2Avt = "Total HT P2 avenant";
        const string cGrpTotalHTp3 = "Total HT P3";
        const string cGrpTotalHTp4 = "Total HT P4";


        const string cGrpNbAvt = "Nb Avenant";
        const string cCrpTotalAvt = "Total Avenant";

        const string cGrpAvenant = "Avenant";

        //===> Mondir le 16.10.2020 : https://groupe-dt.mantishub.io/view.php?id=1819
        const string cGrpNbImmeuble = "Nb d'immeuble";
        const string cGrpNbFacturable = "Nb Facturable";
        const string cGrpNbNonFacturable = "Nb Non Facturable";
        //===> Fin Modif Mondir


        const string cP1 = "P1";
        const string cP2 = "P2";
        const string cP3 = "P3";
        const string cP4 = "P4";
        const string cP2avt = "P2AVENANT";
        const string cP2C = "P2C";
        ModAdo ModAdors = new ModAdo();
        ModAdo ModAdorstmp = new ModAdo();
        ModAdo ModAdorsPOC = new ModAdo();
        ModAdo ModAdorsComm = new ModAdo();
        ModAdo ModAdorsPorteFeuille = new ModAdo();
        ModAdo ModAdorsGrpPrtFeuille = new ModAdo();
        ModAdo ModAdorsCount = new ModAdo();
        ModAdo ModAdorsComplet = new ModAdo();
        //GrpPrt[] tGrpPrt;
        //GrpPrtV2[] tGrpPrtV2;

        GrpPrt[] tGrpPrt;
        GrpPrtV2[] tGrpPrtV2;
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            FraArret.Visible = true;
            txtDateArrete.Text = DateTime.Now.ToString("dd/MM/yyyy");
            //txtDateArrete = ""

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDetailExcel_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            UltraTreeNode oNode = null;
            bool bFind = false;
            tbFlood.Value = 0;
            tbFlood.PerformStep();
            try
            {
                foreach (UltraTreeNode oNodee in TreeArret.Nodes[0].Nodes)
                {
                    if (oNodee.Selected == true)
                    {
                        oNode = oNodee;
                        bFind = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                if (bFind == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous expoter sous Excel le portefeuille contrat pour la date d'arrêté du " + oNode.Text + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv2))
                {
                    //fc_CommercialExport Right(oNode.Key, Len(oNode.Key) - 1), cMatricule

                    if (optCommercial.Checked == true)
                    {
                        //fc_CommercialExport Right(oNode.Key, Len(oNode.Key) - 1), cMatricule
                        fc_commercial(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)));

                    }
                    else if (optChefDeSecteur.Checked == true)
                    {
                        fc_ChefDeSecteur(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)));

                    }
                    else if (optRespExploit.Checked == true)
                    {
                        fc_RespExploit(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)));

                    }
                    else if (OptIntervenant.Checked == true)
                    {
                        FC_intervenant(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)));
                    }

                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_DetailExportV2(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)), cMatricule);
                    }
                    else
                    {
                        fc_CommercialExport(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)), cMatricule);
                    }

                }
                return;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        /// <param name="sNameMat"></param>
        private void fc_DetailExportV2(System.DateTime dtarret, string sNameMat)
        {


            double dbNbCont = 0;
            string sMatricule = null;
            DataTable rs = default(DataTable);
            Microsoft.Office.Interop.Excel.Application oXL /*= default(Microsoft.Office.Interop.Excel.Application)*/;
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);


            int i = 0;
            int j = 0;
            int lLigne = 0;

            try
            {

                Cursor.Current = Cursors.WaitCursor;

                if (rsPorteFeuille.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                FloodDisplay1(rsPorteFeuille.Rows.Count, "Export Détaillé du portefeuille contrat, veuillez patienter .../");

                // Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                i = 1;
                //== affiche les noms des champs.
                foreach (DataColumn oField in rsPorteFeuille.Columns)
                {
                    if ((General.UCase(oField.ColumnName) != General.UCase("Ht P1"))
                        && (General.UCase(oField.ColumnName) != General.UCase("Ht P2"))
                        && (General.UCase(oField.ColumnName) != General.UCase("Ht P3"))
                        && (General.UCase(oField.ColumnName) != General.UCase("Ht P4"))
                        && (General.UCase(oField.ColumnName) != General.UCase("activite")))
                    {

                        //If UCase(oField.Name) <> UCase("POC_Ca_Num") Then
                        oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                        oSheet.Cells[1, i].value = oField.ColumnName;
                        i = i + 1;
                    }
                }

                Cursor.Current = Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 2;


                Cursor.Current = Cursors.WaitCursor;
                sMatricule = General.UCase(rsPorteFeuille.Rows[0][sNameMat].ToString()) + "";


                FloodDisplay2(rsPorteFeuille.Rows.Count);

                int v = 0;
                foreach (DataRow dr in rsPorteFeuille.Rows)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    j = 1;
                    //  oSheet.Cells[lLigne, 1].value = sANT_MatCode
                    if (General.UCase(dr[sNameMat].ToString()) + "" != General.UCase(sMatricule))
                    {
                        //lLigne = lLigne + 1;
                        //oSheet.Cells[lLigne, j + cCol].value = "TOTAL";
                        //' oSheet.Cells[lLigne, j + cCol + 1].value = oField.value
                        sMatricule = General.UCase(dr[sNameMat].ToString()) + "";
                    }

                    foreach (DataColumn oField in rsPorteFeuille.Columns)
                    {
                        if ((General.UCase(oField.ColumnName) != General.UCase("Ht P1"))
                            && (General.UCase(oField.ColumnName) != General.UCase("Ht P2"))
                            && (General.UCase(oField.ColumnName) != General.UCase("Ht P3"))
                            && (General.UCase(oField.ColumnName) != General.UCase("Ht P4"))
                            && (General.UCase(oField.ColumnName) != General.UCase("activite")))
                        {

                            oSheet.Cells[lLigne, j + cCol].value = dr[oField.ColumnName];
                            j = j + 1;
                        }
                    }

                    //remplacer par progress :FloodUpdateText(lLigne);
                    lLigne = lLigne + 1;
                    v++;
                    fmsg = "Export Détaillé du portefeuille contrat, veuillez patienter ..." + v + " / " + (rsPorteFeuille.Rows.Count == 0 ? 1 : rsPorteFeuille.Rows.Count);
                    FloodUpdateText(v, fmsg);

                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                FloodHide();

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;

                Cursor.Current = Cursors.Arrow;
                rsPorteFeuille.Clear();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "erreur");
                FloodHide();
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdPorteFeuille_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            try
            {
                if (fc_controle() == true)
                {
                    FraArret.Visible = false;
                    return;
                }
                FraArret.Visible = false;

                //=== modif du 01/12/2014, nouveau mode de calcul de contrat.
                if (General.sCalculPorteFeuilleCtrV2 == "1")
                {
                    fc_CalculPorteFeuilleV2(Convert.ToDateTime(txtDateArrete.Text));
                }
                else
                {
                    fc_CalculPorteFeuille(Convert.ToDateTime(txtDateArrete.Text));
                }
                //  cmdImprimer_Click

                //FileSystem.FileClose(1);
                Cursor.Current = Cursors.Arrow;
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierPorteFeuille);
                }

                fc_LoadTreev();

                return;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                fc_Err("", 0, "", DateTime.Today);
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierPorteFeuille);
                }
                //FileSystem.FileClose(1);
                Cursor.Current = Cursors.Arrow;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_controle()
        {
            bool functionReturnValue = false;
            DataTable rstmp = default(DataTable);

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                //--Initialise le flag des erreurs.
                blnErreur = false;
                //--test date
                if (!General.IsDate(txtDateArrete.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date Invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    Cursor.Current = Cursors.Arrow;
                    return functionReturnValue;
                }



                //=== modif du 21 11 2014, interdire de créer un arrété
                //=== antérieur à la date du jour.
                if (Convert.ToDateTime(txtDateArrete.Text) < DateTime.Today)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date d'arrêté doit être égal ou supérieur à la date du jour.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    Cursor.Current = Cursors.Arrow;
                    return functionReturnValue;
                }

                //--controle si un calcul de P2 a déja été effectué pour cette date d'arret, si
                //-- oui demande confirmation pour écraser celle-ci.
                sSQL = "Select POC_DateArret,POC_NoAuto from  POC_PorteFeuilleCtr" + " where POC_DateArret ='" + txtDateArrete.Text + "'";
                ModAdorsPOC = new ModAdo();
                rstmp = ModAdorsPOC.fc_OpenRecordSet(sSQL);

                if (rstmp.Rows.Count > 0)
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention un arreté pour la date " + txtDateArrete.Text + " existe déjà. "
                        + "\t" + " Voulez vous l'éffacer et en recréer un autre", "", MessageBoxButtons.YesNo) == DialogResult.Yes)
                    {
                        FraArret.Visible = false;

                        foreach (DataRow r in rstmp.Rows)
                        {
                            r.Delete();
                        }
                        ModAdorsPOC.Update();

                        //                adocnn.Execute "DELETE FROM  AND_ANP2DETAIL" _
                        //'                    & " WHERE ANP_DateArret ='" & txtDateArrete & "'"

                    }
                    else
                    {
                        functionReturnValue = true;
                    }
                }

                //-- ouvre le fichier des erreurs


                //FileSystem.Kill(General.cCheminFichierPorteFeuille);
                if (File.Exists(General.cCheminFichierPorteFeuille))
                    File.Delete(General.cCheminFichierPorteFeuille);

                //ouvre le fichier des erreurs constatés.
                //FileSystem.FileOpen(1, General.cCheminFichierPorteFeuille, OpenMode.Output);
                File.AppendAllText(General.cCheminFichierPorteFeuille, "", Encoding.Unicode);

                Cursor.Current = Cursors.Arrow;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_controle");
                return functionReturnValue;
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_CalculPorteFeuilleV2(System.DateTime dtarret)
        {
            DataTable rsPOC = default(DataTable);
            bool bCtrResiliee = false;
            DataTable rsComm = default(DataTable);
            string sSQlcom = null;
            string sCommImm = null;
            string sCommClt = null;
            bool bComClt = false;

            //===> Mondir le 12.01.2020, commented to fix #2153
            //sSQL = "SELECT Contrat.NumContrat, Contrat.Avenant, Contrat.LibelleCont1,"
            //    + " Contrat.LibelleCont2, Contrat.BaseContractuelle, Contrat.BaseActualisee,"
            //    + " Contrat.CodeArticle, Contrat.nonFacturable, Contrat.FactureManuelle,FacArticle.Designation1, FacArticle.ca_Num, "
            //    + " Personnel.Initiales as [ComInitial], Personnel.Matricule as [ComMat],"
            //    + " Personnel.Nom as [ComNom], Personnel.Prenom as [ComPrenom],";
            //sSQL = sSQL + " Intervenant.Initiales as [IntervInitial], "
            //    + " Intervenant.Matricule as [IntervMat], Intervenant.Nom as [IntervNom],"
            //    + " Intervenant.Prenom as [IntervPrenom], "
            //    + "  Contrat.Resiliee,";
            //sSQL = sSQL + " ChefDeSecteur.Initiales as [ChefInitial], "
            //    + " ChefDeSecteur.Matricule as [ChefMat],"
            //    + " ChefDeSecteur.Nom as [ChefNom], ChefDeSecteur.Prenom as [ChefPrenom], ";
            //sSQL = sSQL + " RespExploit.Initiales as [RespInitial], "
            //    + " RespExploit.Matricule as [RespMat], RespExploit.Nom as [RespNom],"
            //    + " RespExploit.Prenom as [RespPrenom], Table1.commercial "
            //    + " ,Table1.Code1, Immeuble.CodeImmeuble ,  Contrat.DateFin, facarticle.COT2_Code ";
            //===> Fin Modif Mondir

            // sSQL = sSQL & " FROM ((((((Contrat " _
            //& " INNER JOIN  Immeuble " _
            //& " ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble)" _
            //& " INNER JOIN Table1 " _
            //& " ON Immeuble.Code1 = Table1.Code1) " _
            //& " LEFT JOIN Personnel " _
            //& " ON Table1.commercial = Personnel.Matricule) " _
            //& " LEFT JOIN Personnel AS Intervenant " _
            //& " ON Immeuble.CodeDepanneur = Intervenant.Matricule) " _
            //& " LEFT JOIN FacArticle " _
            //& " ON Contrat.CodeArticle = FacArticle.CodeArticle)" _
            //& " LEFT JOIN Personnel AS ChefDeSecteur " _
            //& " ON Intervenant.CSecteur = ChefDeSecteur.Initiales)" _
            //& " LEFT JOIN Personnel AS RespExploit " _
            //& " ON Intervenant.CRespExploit = RespExploit.Initiales" _
            //& " WHERE (((Contrat.Resiliee)=" & fc_bln(False) & "))"

            //===modif du 25 10 2013, le chef de secteur est liée au responsable d'exploitatio et non
            //=== à l'intervenanat.
            //===> Mondir le 12.01.2020, commented to fix #2153
            //sSQL = sSQL + "FROM         Personnel AS ChefDeSecteur RIGHT OUTER JOIN"
            //    + " Personnel AS RespExploit ON ChefDeSecteur.Initiales = RespExploit.CSecteur RIGHT OUTER JOIN"
            //    + " Contrat INNER JOIN" + " Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
            //    + " Table1 ON Immeuble.Code1 = Table1.Code1 ON RespExploit.Matricule = Immeuble.CodeRespExploit LEFT OUTER JOIN "
            //    + " Personnel ON Table1.commercial = Personnel.Matricule LEFT OUTER JOIN "
            //    + " Personnel AS Intervenant ON Immeuble.CodeDepanneur = Intervenant.Matricule LEFT OUTER JOIN "
            //    + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle ";
            //sSQL = sSQL + " Where ((Contrat.Resiliee = 0) ";
            //sSQL = sSQL + " OR (Contrat.DateFin >'" + DateTime.Today + "'))  ";
            //===> Fin Modif Mondir
            //=== critére a mettre en cas de demande de calcul à une date antérieur.
            //sSQL = sSQL & " OR (Contrat.DateFin >'" & txttemp & "'))  and DateEffet <='" & txttemp & "'"


            //sSQL = sSQL & " ORDER BY RespInitial"

            //===> Mondir le 12.01.2020, requette modifiée pour corriger #2153
            sSQL = @"SELECT Contrat.NumContrat, Contrat.Avenant, Contrat.LibelleCont1, Contrat.LibelleCont2, Contrat.BaseContractuelle, Contrat.BaseActualisee,
            Contrat.CodeArticle, Contrat.nonFacturable, Contrat.FactureManuelle,FacArticle.Designation1, FacArticle.ca_Num,  Personnel.Initiales as [ComInitial],
            Personnel.Matricule as [ComMat], Personnel.Nom as [ComNom], Personnel.Prenom as [ComPrenom], Intervenant.Initiales as [IntervInitial],
            Intervenant.Matricule as [IntervMat], Intervenant.Nom as [IntervNom], Intervenant.Prenom as [IntervPrenom],   Contrat.Resiliee, ChefDeSecteur.Initiales as [ChefInitial],
            ChefDeSecteur.Matricule as [ChefMat], ChefDeSecteur.Nom as [ChefNom], ChefDeSecteur.Prenom as [ChefPrenom],  RespExploit.Initiales as [RespInitial],  RespExploit.Matricule as [RespMat],
            RespExploit.Nom as [RespNom], RespExploit.Prenom as [RespPrenom], Table1.commercial  ,Table1.Code1, Immeuble.CodeImmeuble ,  Contrat.DateFin, facarticle.COT2_Code
                FROM         Personnel AS ChefDeSecteur
                JOIN Immeuble ON Immeuble.CodeChefSecteur = ChefDeSecteur.Matricule
            RIGHT OUTER JOIN Contrat ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble
            LEFT OUTER JOIN FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle
            JOIN Table1 ON Table1.Code1 = Immeuble.Code1
            LEFT OUTER JOIN Personnel ON Personnel.Matricule = Table1.commercial
            LEFT OUTER JOIN Personnel AS Intervenant ON Immeuble.CodeDepanneur = Intervenant.Matricule
            RIGHT OUTER JOIN Personnel AS RespExploit ON RespExploit.Initiales = ChefDeSecteur.CRespExploit";
            sSQL += $" Where ((Contrat.Resiliee = 0) OR (Contrat.DateFin >'" + DateTime.Today + "')) ";
            //===> Fin Modif Mondir

            //TODO : Mondir - Must Uncomment
            if (!General.fncUserName().Contains("mondir"))
                sSQL = sSQL + " ORDER BY RespExploit.Initiales";
            else
                sSQL = sSQL += " ORDER BY Contrat.NumContrat, Contrat.Avenant";

            rs = ModAdors.fc_OpenRecordSet(sSQL);

            sSQL = "SELECT POC_NoAuto, POC_DateArret, NumContrat, Avenant,"
                + " CodeImmeuble, Code1, POC_Lib1, POC_Lib2, POC_BaseContrat,"
                + " CodeArticle, POC_Designation, "
                + " POC_Ca_Num, POC_ComInitial, POC_ComMat, POC_ComPrenom,"
                + " POC_ComNom, POC_IntervInitial, POC_IntervMat, POC_IntervPrenom,"
                + " POC_IntervNom, POC_CdsInitial, POC_CdsMat, POC_CdsPrenom,"
                + " POC_CdsNom, POC_RexpInitial, POC_RexpMat, POC_RexpPrenom,"
                + " POC_RexpNom," + " POC_HtP1, POC_HtP2, POC_HtP3, POC_HtP4, POC_HtP2avt, POC_HtP2_P2avt, POC_COT2_Code "
                + " FROM         POC_PorteFeuilleCtr where POC_NoAuto = 0";
            ModAdorsPOC = new ModAdo();
            rsPOC = ModAdorsPOC.fc_OpenRecordSet(sSQL);


            foreach (DataRow drr in rs.Rows)
            {
                bCtrResiliee = false;

                if (drr["Resiliee"] != null && Convert.ToBoolean(drr["Resiliee"]) == true)
                {
                    bCtrResiliee = true;
                }
                else
                {
                    bCtrResiliee = false;
                }


                if (!string.IsNullOrEmpty(drr["DateFin"].ToString()))
                {
                    //=== si la date de fin du contrat est supérieur à la date du jour
                    //=== alors on considére que ce contrat n'est pas résilié.
                    if (Convert.ToDateTime(drr["DateFin"]) > DateTime.Today && bCtrResiliee == true)
                    {
                        bCtrResiliee = true;
                        fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "Le contrat n°" + drr["NumContrat"] + " avenant " + drr["avenant"] + " est résilié mais sa date de fin se termine le " + drr["DateFin"] + ", attention celui-ci a été comptabilisée comme résilié");
                    }
                    else if (Convert.ToDateTime(drr["DateFin"]) < DateTime.Today)
                    {
                        if (bCtrResiliee == false)
                        {
                            fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "Le contrat n°" + drr["NumContrat"] + " avenant " + drr["avenant"] + " n'est pas résilié mais sa date de fin est terminée depuis le " + drr["DateFin"] + ", attention celui-ci a été comptabilisée comme non résilié");
                        }
                        else
                        {
                            bCtrResiliee = true;
                        }

                    }
                }

                //=== modif du 16 decembre, on ne prends plus en compte les contrats non facturable
                //=== sauf si le champs FactureManuelle est à true (ce champs indique que le contrat
                //=== est facturé manuellement).

                ///added by mohammed 24.06.2020 
                // '=== nouvelle modif du 24 06 2020, demande de Jules on prend en compte les
                //'=== non fcaturable.
                //=== modif du 27/07/2020, erreur sur la condition.
                if (drr["FactureManuelle"] != DBNull.Value && Convert.ToBoolean(drr["FactureManuelle"]))
                {
                    bCtrResiliee = false;
                }

                if (bCtrResiliee == true)
                {
                    continue;
                }

                //_with2.AddNew();
                DataRow Nr = rsPOC.NewRow();
                Nr["POC_DateArret"] = dtarret.ToShortDateString();
                Nr["NumContrat"] = drr["NumContrat"];
                Nr["avenant"] = drr["avenant"];
                Nr["CodeImmeuble"] = drr["CodeImmeuble"];
                Nr["Code1"] = drr["Code1"];
                Nr["POC_Lib1"] = drr["LibelleCont1"];
                Nr["POC_Lib2"] = drr["LibelleCont2"];
                Nr["POC_COT2_Code"] = drr["COT2_Code"];

                if (!string.IsNullOrEmpty(General.nz(drr["BaseActualisee"], "").ToString()))
                {
                    Nr["POC_BaseContrat"] = drr["BaseActualisee"];
                    //=== modif du 25 octobre 2013, repartir les montants
                    //=== contractuelle selon le type de contrat P1, P1, P3, P4.
                    if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP1))
                    {
                        Nr["POC_HtP1"] = drr["BaseActualisee"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP2C))
                    {
                        Nr["POC_HtP2"] = drr["BaseActualisee"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP2avt))
                    {
                        Nr["POC_HtP2avt"] = drr["BaseActualisee"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP3))
                    {
                        Nr["POC_HtP3"] = drr["BaseActualisee"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP4))
                    {
                        Nr["POC_HtP4"] = drr["BaseActualisee"];

                    }
                    else
                    {
                        fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "L'article  " + drr["CodeArticle"] + " " + drr["Designation1"] + " n'est pas ataché à un type de contrat(P1,P2...).");
                    }

                }
                else if (!string.IsNullOrEmpty(General.nz(drr["BaseContractuelle"], "").ToString()))
                {
                    Nr["POC_BaseContrat"] = drr["BaseContractuelle"];
                    //=== modif du 25 octobre 2013, repartir les montants
                    //=== contractuelle selon le type de contrat P1, P1, P3, P4.
                    if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP1))
                    {
                        Nr["POC_HtP1"] = drr["BaseContractuelle"];
                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP2C))
                    {
                        Nr["POC_HtP2"] = drr["BaseContractuelle"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP2avt))
                    {
                        Nr["POC_HtP2avt"] = drr["BaseActualisee"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP3))
                    {
                        Nr["POC_HtP3"] = drr["BaseContractuelle"];

                    }
                    else if (General.UCase(drr["COT2_Code"].ToString()) == General.UCase(cP4))
                    {
                        Nr["POC_HtP4"] = drr["BaseContractuelle"];
                    }
                    else
                    {
                        fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "L'article  " + drr["CodeArticle"] + " " + drr["Designation1"] + " n'est pas ataché à un type de contrat(P1,P2...).");
                    }

                }
                else
                {
                    fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "Le contrat " + drr["NumContrat"] + " avenant" + " " + drr["avenant"] + " n'a pas de montant de base contractuelle.");
                }
                Nr["CodeArticle"] = drr["CodeArticle"];
                Nr["POC_Designation"] = drr["Designation1"];

                //If nz(rs!ca_num, "") = "" Then
                //    fc_Err rs!NumContrat, rs!avenant, rs!CodeImmeuble, dtarret, _
                //"Le code Article " & rs!CodeArticle & " dans la base d'article n'a pas de code analytique activité."
                //Else
                Nr["POC_Ca_Num"] = "XXXXX";
                //rs!ca_num
                //End If


                if (string.IsNullOrEmpty(General.nz(drr["COT2_Code"], "").ToString()))
                {
                    fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "Le code Article " + drr["CodeArticle"] + " dans la base d'article n'a pas de code type de contrat 2.");
                    //Else
                    //!POC_Ca_Num = rs!COT2_Code
                }

                if (string.IsNullOrEmpty(General.nz(drr["Commercial"], "").ToString()))
                {
                    fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "Le gérant " + drr["Code1"] + " n'est pas associé à un commercial.");
                }

                //=== commercial.
                //=== modif du 19 02 2014, controle si le commercial affecté au client est le même affecté à l'immeuble
                //===  si vrai on donne la pririté au commercial de l'immeuble.
                sCommImm = "";
                sCommClt = "";
                bComClt = true;
                sSQlcom = "SELECT     Immeuble.CodeImmeuble, Immeuble.CodeCommercial, Personnel.Matricule, Personnel.Initiales, Personnel.Nom, Personnel.Prenom"
                    + " FROM         Immeuble LEFT OUTER JOIN "
                    + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule"
                    + " WHERE     Immeuble.CodeImmeuble = '"
                    + StdSQLchaine.gFr_DoublerQuote(drr["CodeImmeuble"].ToString())
                    + "'";
                rsComm = ModAdorsComm.fc_OpenRecordSet(sSQlcom);

                if (rsComm.Rows.Count > 0)
                {
                    sCommImm = rsComm.Rows[0]["Matricule"] + "";
                    sCommImm = General.UCase(sCommImm);
                    ///============> Mondir : 04.06.2020 - Bug - Replace rs.Rows[0]["ComMat"] by drr["ComMat"]
                    sCommClt = drr["ComMat"] + "";
                    sCommClt = General.UCase(sCommClt);
                    if (!string.IsNullOrEmpty(sCommImm))
                    {
                        if (General.UCase(sCommImm) != General.UCase(sCommClt))
                        {
                            bComClt = false;
                        }
                    }
                }
                if (bComClt == true)
                {
                    Nr["POC_ComInitial"] = drr["ComInitial"];
                    Nr["POC_ComMat"] = drr["ComMat"];
                    Nr["POC_ComPrenom"] = drr["ComPrenom"];
                    Nr["POC_ComNom"] = drr["ComNom"];
                }
                else
                {
                    if (rsComm.Rows.Count > 0)
                    {
                        Nr["POC_ComInitial"] = rsComm.Rows[0]["Initiales"];
                        Nr["POC_ComMat"] = rsComm.Rows[0]["Matricule"];
                        Nr["POC_ComPrenom"] = rsComm.Rows[0]["Prenom"];
                        Nr["POC_ComNom"] = rsComm.Rows[0]["Nom"];
                    }
                }
                rsComm.Clear();

                if (string.IsNullOrEmpty(General.nz(drr["IntervMat"], "").ToString()))
                {
                    fc_Err(drr["NumContrat"].ToString(), Convert.ToInt32(drr["avenant"]), drr["CodeImmeuble"].ToString(), dtarret, "L'immeuble " + drr["CodeImmeuble"] + " n'est pas associé à un intervenant.");
                }

                //=== intervenant.
                Nr["POC_IntervInitial"] = drr["IntervInitial"];
                Nr["POC_IntervMat"] = drr["IntervMat"];
                Nr["POC_IntervPrenom"] = drr["IntervPrenom"];
                Nr["POC_IntervNom"] = drr["IntervNom"];

                //=== chef de secteur
                Nr["POC_CdsInitial"] = drr["ChefInitial"];
                Nr["POC_CdsMat"] = drr["ChefMat"];
                Nr["POC_CdsPrenom"] = drr["ChefPrenom"];
                Nr["POC_CdsNom"] = drr["ChefNom"];

                //=== responsable d'exploitation.
                Nr["POC_RexpInitial"] = drr["ChefInitial"];
                Nr["POC_RexpMat"] = drr["RespMat"];
                Nr["POC_RexpPrenom"] = drr["RespPrenom"];
                Nr["POC_RexpNom"] = drr["RespNom"];

                rsPOC.Rows.Add(Nr);
                ModAdorsPOC.Update();
            }
            ModAdorsPOC.Update();

            int i = 0;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_CalculPorteFeuille(System.DateTime dtarret)
        {
            DataTable rsPOC = default(DataTable);
            bool bCtrResiliee = false;
            DataTable rsComm = default(DataTable);
            string sSQlcom = null;
            string sCommImm = null;
            string sCommClt = null;
            bool bComClt = false;

            sSQL = "SELECT Contrat.NumContrat, Contrat.Avenant, Contrat.LibelleCont1,"
                + " Contrat.LibelleCont2, Contrat.BaseContractuelle, Contrat.BaseActualisee,"
                + " Contrat.CodeArticle, FacArticle.Designation1, FacArticle.ca_Num, "
                + " Personnel.Initiales as [ComInitial], Personnel.Matricule as [ComMat],"
                + " Personnel.Nom as [ComNom], Personnel.Prenom as [ComPrenom],";
            sSQL = sSQL + " Intervenant.Initiales as [IntervInitial], "
                + " Intervenant.Matricule as [IntervMat], Intervenant.Nom as [IntervNom],"
                + " Intervenant.Prenom as [IntervPrenom], "
                + "  Contrat.Resiliee,";
            sSQL = sSQL + " ChefDeSecteur.Initiales as [ChefInitial], "
                + " ChefDeSecteur.Matricule as [ChefMat],"
                + " ChefDeSecteur.Nom as [ChefNom], ChefDeSecteur.Prenom as [ChefPrenom], ";
            sSQL = sSQL + " RespExploit.Initiales as [RespInitial], "
                + " RespExploit.Matricule as [RespMat], RespExploit.Nom as [RespNom],"
                + " RespExploit.Prenom as [RespPrenom], Table1.commercial "
                + " ,Table1.Code1, Immeuble.CodeImmeuble , FacArticle.COT_Code , Contrat.DateFin ";

            // sSQL = sSQL & " FROM ((((((Contrat " _
            //& " INNER JOIN  Immeuble " _
            //& " ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble)" _
            //& " INNER JOIN Table1 " _
            //& " ON Immeuble.Code1 = Table1.Code1) " _
            //& " LEFT JOIN Personnel " _
            //& " ON Table1.commercial = Personnel.Matricule) " _
            //& " LEFT JOIN Personnel AS Intervenant " _
            //& " ON Immeuble.CodeDepanneur = Intervenant.Matricule) " _
            //& " LEFT JOIN FacArticle " _
            //& " ON Contrat.CodeArticle = FacArticle.CodeArticle)" _
            //& " LEFT JOIN Personnel AS ChefDeSecteur " _
            //& " ON Intervenant.CSecteur = ChefDeSecteur.Initiales)" _
            //& " LEFT JOIN Personnel AS RespExploit " _
            //& " ON Intervenant.CRespExploit = RespExploit.Initiales" _
            //& " WHERE (((Contrat.Resiliee)=" & fc_bln(False) & "))"

            //===modif du 25 10 2013, le chef de secteur est liée au responsable d'exploitatio et non
            //=== à l'intervenanat.
            sSQL = sSQL + "FROM         Personnel AS ChefDeSecteur RIGHT OUTER JOIN"
                + " Personnel AS RespExploit ON ChefDeSecteur.Initiales = RespExploit.CSecteur RIGHT OUTER JOIN"
                + " Contrat INNER JOIN"
                + " Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                + " Table1 ON Immeuble.Code1 = Table1.Code1 ON RespExploit.Matricule = Immeuble.CodeRespExploit LEFT OUTER JOIN "
                + " Personnel ON Table1.commercial = Personnel.Matricule LEFT OUTER JOIN "
                + " Personnel AS Intervenant ON Immeuble.CodeDepanneur = Intervenant.Matricule LEFT OUTER JOIN "
                + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle "
                + " Where (Contrat.Resiliee = 0) "
                + " OR (Contrat.DateFin >'" + DateTime.Today + "')";


            sSQL = sSQL + " ORDER BY RespInitial";

            rs = ModAdors.fc_OpenRecordSet(sSQL);

            sSQL = "SELECT POC_NoAuto, POC_DateArret, NumContrat, Avenant,"
                + " CodeImmeuble, Code1, POC_Lib1, POC_Lib2, POC_BaseContrat,"
                + " CodeArticle, POC_Designation, " + " POC_Ca_Num, POC_ComInitial, POC_ComMat, POC_ComPrenom,"
                + " POC_ComNom, POC_IntervInitial, POC_IntervMat, POC_IntervPrenom,"
                + " POC_IntervNom, POC_CdsInitial, POC_CdsMat, POC_CdsPrenom,"
                + " POC_CdsNom, POC_RexpInitial, POC_RexpMat, POC_RexpPrenom,"
                + " POC_RexpNom," + " POC_HtP1, POC_HtP2, POC_HtP3, POC_HtP4 "
                + " FROM         POC_PorteFeuilleCtr where POC_NoAuto = 0";

            rsPOC = ModAdorsPOC.fc_OpenRecordSet(sSQL);

            var _with3 = rsPOC;


            foreach (DataRow dr in rs.Rows)
            {

                bCtrResiliee = false;

                if (Convert.ToBoolean(dr["Resiliee"]) == true)
                {
                    bCtrResiliee = true;
                }
                else
                {
                    bCtrResiliee = false;
                }


                if (!string.IsNullOrEmpty(dr["DateFin"].ToString()))
                {
                    //=== si la date de fin du contrat est supérieur à la date du jour
                    //=== alors on considére que ce contrat n'est pas résilié.
                    if (Convert.ToDateTime(dr["DateFin"]) > DateTime.Today && bCtrResiliee == true)
                    {
                        bCtrResiliee = true;
                        fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le contrat n°" + dr["NumContrat"] + " avenant " + dr["avenant"] + " est résilié mais sa date de fin se termine le " + dr["DateFin"] + ", attention celui-ci a été comptabilisée comme résilié");
                    }
                    else if (Convert.ToDateTime(dr["DateFin"]) < DateTime.Today)
                    {
                        if (bCtrResiliee == false)
                        {
                            fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le contrat n°" + dr["NumContrat"] + " avenant " + dr["avenant"] + " n'est pas résilié mais sa date de fin est terminée depuis le " + dr["DateFin"] + ", attention celui-ci a été comptabilisée comme non résilié");
                        }
                        else
                        {
                            bCtrResiliee = true;
                        }

                    }
                }


                if (bCtrResiliee == true)
                {
                    continue;
                }

                DataRow RP = rsPOC.NewRow();
                RP["POC_DateArret"] = dtarret.ToShortDateString();
                RP["NumContrat"] = dr["NumContrat"];
                RP["avenant"] = dr["avenant"];
                RP["CodeImmeuble"] = dr["CodeImmeuble"];
                RP["Code1"] = dr["Code1"];
                RP["POC_Lib1"] = dr["LibelleCont1"];
                RP["POC_Lib2"] = dr["LibelleCont2"];

                if (!string.IsNullOrEmpty(General.nz(dr["BaseActualisee"], "").ToString()))
                {
                    RP["POC_BaseContrat"] = dr["BaseActualisee"];
                    //=== modif du 25 octobre 2013, repartir les montants
                    //=== contractuelle selon le type de contrat P1, P1, P3, P4.
                    if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP1))
                    {
                        RP["POC_HtP1"] = dr["BaseActualisee"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP2))
                    {
                        RP["POC_HtP2"] = dr["BaseActualisee"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP3))
                    {
                        RP["POC_HtP3"] = dr["BaseActualisee"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP4))
                    {
                        RP["POC_HtP4"] = dr["BaseActualisee"];
                    }
                    else
                    {
                        fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "L'article  " + dr["CodeArticle"] + " " + dr["Designation1"] + " n'est pas ataché à un type de contrat(P1,P2...).");
                    }

                }
                else if (!string.IsNullOrEmpty(General.nz(dr["BaseContractuelle"], "").ToString()))
                {
                    RP["POC_BaseContrat"] = dr["BaseContractuelle"];
                    //=== modif du 25 octobre 2013, repartir les montants
                    //=== contractuelle selon le type de contrat P1, P1, P3, P4.
                    if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP1))
                    {
                        RP["POC_HtP1"] = dr["BaseContractuelle"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP2))
                    {
                        RP["POC_HtP2"] = dr["BaseContractuelle"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP3))
                    {
                        RP["POC_HtP3"] = dr["BaseContractuelle"];

                    }
                    else if (General.UCase(dr["COT_Code"].ToString()) == General.UCase(cP4))
                    {
                        RP["POC_HtP4"] = dr["BaseContractuelle"];
                    }
                    else
                    {
                        fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "L'article  " + dr["CodeArticle"] + " " + dr["Designation1"] + " n'est pas ataché à un type de contrat(P1,P2...).");
                    }

                }
                else
                {
                    fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le contrat " + dr["NumContrat"] + " avenant" + " " + dr["avenant"] + " n'a pas de montant de base contractuelle.");
                }
                RP["CodeArticle"] = dr["CodeArticle"];
                RP["POC_Designation"] = dr["Designation1"];

                //if (string.IsNullOrEmpty(General.nz(dr["ca_num"], "").ToString()))
                //{
                //    fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le code Article " + dr["CodeArticle"] + " dans la base d'article n'a pas de code analytique activité.");
                //}
                //else
                //{
                //    RP["POC_Ca_Num"] = dr["ca_num"];
                //RP["POC_Ca_Num"] = "XXXXX";
                //}
                if (string.IsNullOrEmpty(General.nz(dr["ca_num"], "").ToString()))
                {
                    fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le code Article " + dr["CodeArticle"] + " dans la base d'article n'a pas de code type de contrat 2.");
                }
                else
                {
                    RP["POC_Ca_Num"] = dr["ca_num"];
                }
                if (string.IsNullOrEmpty(General.nz(dr["Commercial"], "").ToString()))
                {
                    fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "Le gérant " + dr["Code1"] + " n'est pas associé à un commercial.");
                }

                //=== commercial.
                //=== modif du 19 02 2014, controle si le commercial affecté au client est le même affecté à l'immeuble
                //===  si vrai on donne la pririté au commercial de l'immeuble.
                sCommImm = "";
                sCommClt = "";
                bComClt = true;
                sSQlcom = "SELECT     Immeuble.CodeImmeuble, Immeuble.CodeCommercial, Personnel.Matricule, Personnel.Initiales, Personnel.Nom, Personnel.Prenom"
                    + " FROM         Immeuble LEFT OUTER JOIN " + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule"
                    + " WHERE     Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(dr["CodeImmeuble"].ToString()) + "'";
                rsComm = ModAdorsComm.fc_OpenRecordSet(sSQlcom);

                if (rsComm.Rows.Count > 0)
                {
                    sCommImm = rsComm.Rows[0]["Matricule"] + "";
                    sCommImm = General.UCase(sCommImm);
                    sCommClt = dr["ComMat"] + "";
                    sCommClt = General.UCase(sCommClt);
                    if (!string.IsNullOrEmpty(sCommImm))
                    {
                        if (General.UCase(sCommImm) != General.UCase(sCommClt))
                        {
                            bComClt = false;
                        }
                    }
                }

                if (bComClt == true)
                {
                    RP["POC_ComInitial"] = dr["ComInitial"];
                    RP["POC_ComMat"] = dr["ComMat"];
                    RP["POC_ComPrenom"] = dr["ComPrenom"];
                    RP["POC_ComNom"] = dr["ComNom"];
                }
                else
                {
                    if (rsComm.Rows.Count > 0)
                    {
                        RP["POC_ComInitial"] = rsComm.Rows[0]["Initiales"];
                        RP["POC_ComMat"] = rsComm.Rows[0]["Matricule"];
                        RP["POC_ComPrenom"] = rsComm.Rows[0]["Prenom"];
                        RP["POC_ComNom"] = rsComm.Rows[0]["Nom"];
                    }

                }
                rsComm.Clear();

                if (string.IsNullOrEmpty(General.nz(dr["IntervMat"], "").ToString()))
                {
                    fc_Err(dr["NumContrat"].ToString(), Convert.ToInt32(dr["avenant"]), dr["CodeImmeuble"].ToString(), dtarret, "L'immeuble " + dr["CodeImmeuble"] + " n'est pas associé à un intervenant.");
                }

                //=== intervenant.
                RP["POC_IntervInitial"] = dr["IntervInitial"];
                RP["POC_IntervMat"] = dr["IntervMat"];
                RP["POC_IntervPrenom"] = dr["IntervPrenom"];
                RP["POC_IntervNom"] = dr["IntervNom"];

                //=== chef de secteur
                RP["POC_CdsInitial"] = dr["ChefInitial"];
                RP["POC_CdsMat"] = dr["ChefMat"];
                RP["POC_CdsPrenom"] = dr["ChefPrenom"];
                RP["POC_CdsNom"] = dr["ChefNom"];

                //=== responsable d'exploitation.
                RP["POC_RexpInitial"] = dr["ChefInitial"];
                RP["POC_RexpMat"] = dr["RespMat"];
                RP["POC_RexpPrenom"] = dr["RespPrenom"];
                RP["POC_RexpNom"] = dr["RespNom"];

                rsPOC.Rows.Add(RP.ItemArray);


            }
            ModAdorsPOC.Update();
            int i = 0;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNoContrat"></param>
        /// <param name="lAvenant"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="dtarret"></param>
        /// <param name="sErreur"></param>
        private void fc_Err(string sNoContrat, int lAvenant, string sCodeImmeuble, System.DateTime dtarret, string sErreur = "")
        {

            try
            {
                sSQL = "INSERT INTO POCE_PorteFeuilleErreur" + " (POC_DateArret, codeimmeuble, NumContrat, Avenant, Erreur)"
                    + " VALUES     ('" + dtarret + "' , '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "',"
                    + "'" + StdSQLchaine.gFr_DoublerQuote(sNoContrat) + "', " + lAvenant + ", '" + StdSQLchaine.gFr_DoublerQuote(sErreur) + "')";

                General.Execute(sSQL);

                blnErreur = true;
                if (!string.IsNullOrEmpty(sErreur))
                {
                    //FileSystem.PrintLine(1, sErreur);
                    File.AppendAllText(General.cCheminFichierPorteFeuille, sErreur + "\r\n", Encoding.Unicode);
                }

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, Convert.ToBoolean("fc_err"));
            }



        }

        private void cmdSup_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fonction non installée");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdVisu_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            UltraTreeNode oNode = null;
            bool bFind = false;

            try
            {
                foreach (UltraTreeNode oNode_loopVariable in TreeArret.Nodes[0].Nodes)
                {
                    oNode = oNode_loopVariable;
                    if (oNode.Selected == true)
                    {
                        bFind = true;
                        break;
                    }
                }

                if (bFind == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous exporter sous Excel le portefeuille contrat pour la date d'arrêté du " + oNode.Text + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv2))
                {
                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_GrpExportV2(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)), cMatricule);
                    }
                    else
                    {
                        fc_CommercialGrpExport(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)), cMatricule);
                    }
                }
                return;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        /// <param name="sNameMat"></param>
        private void fc_GrpExportV2(System.DateTime dtarret, string sNameMat)
        {
            double dbNbCont = 0;
            string sMatricule = null;
            DataTable rs = default(DataTable);
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);

            int i = 0;
            int j = 0;
            int X = 0;
            int lLigne = 0;


            Cursor.Current = Cursors.WaitCursor;

            if (rsGrpPrtFeuille.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            // Start Excel and get Application object.
            //oXL = Interaction.CreateObject("Excel.Application");

            oXL = new Microsoft.Office.Interop.Excel.Application();
            oXL.Visible = false;

            // Get a new workbook.
            oWB = oXL.Workbooks.Add();
            oSheet = oWB.ActiveSheet;

            //== affiche les noms des champs.

            i = 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpArret;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpMat;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNom;

            //===> Mondir le 16.10.2020, https://groupe-dt.mantishub.io/view.php?id=1819
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbImmeuble;
            //===> Fin Modif Mondir

            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbP1;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbP2;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbP2avt;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbP3;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbP4;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp2C_p2avt;
            i = i + 1;
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp1;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp2;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp2Avt;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp3;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpTotalHTp4;
            i = i + 1;

            //===> Mondir le 16.10.2020, https://groupe-dt.mantishub.io/view.php?id=1819
            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbFacturable;
            i = i + 1;

            oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
            oSheet.Cells[1, i].value = cGrpNbNonFacturable;
            i = i + 1;
            //===> Fin Modif Mondir

            Cursor.Current = Cursors.WaitCursor;

            // Starting at E1, fill headers for the number of columns selected.
            oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

            oResizeRange.Interior.ColorIndex = 15;
            //formate la ligne en gras.
            oResizeRange.Font.Bold = true;

            oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
            //oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

            lLigne = 2;


            Cursor.Current = Cursors.WaitCursor;
            //    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""

            Cursor.Current = Cursors.WaitCursor;

            //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
            for (X = 0; X < tGrpPrtV2.Length; X++)
            {
                j = 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dtDateArret;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].sMat;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].sNom;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].nbrImmeuble;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbNbP1;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbNbP2;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbNbP2avt;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbNbP3;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbNbP4;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP2_P2avt;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP1;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP2;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP2avt;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP3;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].dbTotHTP4;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].nbrFacturable;
                j = j + 1;

                oSheet.Cells[lLigne, j + cCol].value = tGrpPrtV2[X].nbrNonFacturable;
                j = j + 1;


                lLigne = lLigne + 1;

            }

            ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
            //formate la ligne en gras.
            oResizeRange.Font.Bold = true;

            oXL.Visible = true;
            oXL.UserControl = true;
            oRng = null;
            oSheet = null;
            oWB = null;
            oXL = null;
            Cursor.Current = Cursors.Arrow;



            rs = null;


            return;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        /// <param name="sNameMat"></param>
        private void fc_CommercialGrpExport(System.DateTime dtarret, string sNameMat)
        {
            double dbNbCont = 0;
            string sMatricule = null;
            DataTable rs = default(DataTable);
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);

            int i = 0;
            int j = 0;
            int X = 0;
            int lLigne = 0;


            try
            {
                Cursor.Current = Cursors.WaitCursor;




                if (rsGrpPrtFeuille.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // Start Excel and get Application object.
                // oXL = Interaction.CreateObject("Excel.Application");
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;



                //== affiche les noms des champs.

                i = 1;
                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpArret;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpMat;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpNom;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpNbCont;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpTotalHT;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpNbAvt;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cCrpTotalAvt;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpTotalHTp1;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpTotalHTp2;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpTotalHTp3;
                i = i + 1;

                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = cGrpTotalHTp4;
                i = i + 1;

                Cursor.Current = Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;


                Cursor.Current = Cursors.WaitCursor;
                //    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""

                Cursor.Current = Cursors.WaitCursor;

                //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                for (X = 0; X < tGrpPrt.Length; X++)
                {
                    j = 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dtDateArret;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].sMat;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].sNom;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbNbAvt0;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotAvt0;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbNbAvtAutre;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotAvAutre;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotHTP1;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotHTP2;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotHTP3;
                    j = j + 1;

                    oSheet.Cells[lLigne, j + cCol].value = tGrpPrt[X].dbTotHTP4;
                    j = j + 1;

                    lLigne = lLigne + 1;

                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                Cursor.Current = Cursors.Arrow;

                rs = null;


                return;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_CommercialGrpExport");
            }

        }
        private void fc_CommercialGrpExport2903(System.DateTime dtarret, string sNameMat)
        {
            double dbNbCont = 0;
            string sMatricule = null;
            DataTable rs = default(DataTable);
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);


            int i = 0;
            int j = 0;
            int lLigne = 0;


            try
            {
                Cursor.Current = Cursors.WaitCursor;


                var _with6 = rsGrpPrtFeuille;
                if (rsGrpPrtFeuille.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                // Start Excel and get Application object.
                //oXL = Interaction.CreateObject("Excel.Application");
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;



                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn DC in rsGrpPrtFeuille.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = DC.ColumnName;
                    i = i + 1;
                }

                Cursor.Current = Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;

                Cursor.Current = Cursors.WaitCursor;
                //    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""

                foreach (DataRow dr in rsGrpPrtFeuille.Rows)
                {
                    Cursor.Current = Cursors.WaitCursor;
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    //            If UCase(rsPorteFeuille.Fields(sNameMat).value) & "" <> UCase(sMatricule) Then
                    //                 lLigne = lLigne + 1
                    //                 oSheet.Cells(lLigne, j + cCol).value = "TOTAL"
                    //                '' oSheet.Cells(lLigne, j + cCol + 1).value = oField.value
                    //                 sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""
                    //            End If

                    foreach (DataColumn C in rsGrpPrtFeuille.Columns)
                    {
                        oSheet.Cells[lLigne, j + cCol].value = dr[C.ColumnName];
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;

                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                Cursor.Current = Cursors.Arrow;

                rsGrpPrtFeuille.Clear();

                rs = null;


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_CommercialGrpExport2903");
            }


        }

        private void Command1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            FraArret.Visible = false;
        }

        private void Command2_Click()
        {

        }


        private void optChefDeSecteur_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {

            if (optChefDeSecteur.Checked)
            {
                fc_Opt();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Opt()
        {

            bool bFind = false;

            foreach (UltraTreeNode oNode in TreeArret.Nodes[0].Nodes)
            {

                if (oNode.Selected == true)
                {
                    bFind = true;
                    break;
                }
            }

            if (bFind == false)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //TreeArret.ActiveNode = oNode;
            TreeArret_AfterSelect(TreeArret, null);

        }

        private void optCommercial_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optCommercial.Checked)
            {
                fc_Opt();
            }
        }

        private void OptIntervenant_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (OptIntervenant.Checked)
            {
                fc_Opt();
            }
        }

        private void optRespExploit_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optRespExploit.Checked)
            {
                fc_Opt();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeArret_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode Node = TreeArret.ActiveNode;
            // Node = e.NewSelections[0];

            //===> Mondir le 01.10.2020 : sometime the node is empty
            if (Node == null)
            {
                Node = TreeArret.Nodes[0].Nodes[0];
                //return;
            }
            //===> Fin Modif Mondir

            Cursor.Current = Cursors.WaitCursor;

            if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv2))
            {
                if (optCommercial.Checked == true)//Tested
                {

                    lblPar.Text = "RESULTAT PAR COMMERCIAL";

                    Cursor.Current = Cursors.WaitCursor;
                    fc_commercial(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    Cursor.Current = Cursors.WaitCursor;

                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_GrpV2(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)), 0);
                    }
                    else
                    {
                        fc_GrpCommercial(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                    }
                }
                else if (optChefDeSecteur.Checked == true)//Tested
                {

                    lblPar.Text = "RESULTAT PAR CHEF DE SECTEUR";

                    Cursor.Current = Cursors.WaitCursor;
                    fc_ChefDeSecteur(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    Cursor.Current = Cursors.WaitCursor;

                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_GrpV2(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)), 1);
                    }
                    else
                    {
                        fc_GrpChefDeSecteur(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                    }
                }
                else if (optRespExploit.Checked == true)//Tested
                {
                    lblPar.Text = "RESULTAT PAR RESPONSABLE D'EXPLOITATION";

                    Cursor.Current = Cursors.WaitCursor;
                    fc_RespExploit(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    Cursor.Current = Cursors.WaitCursor;

                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_GrpV2(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)), 2);
                    }
                    else
                    {
                        fc_GrpRespExploit(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                    }
                }
                else if (OptIntervenant.Checked == true)
                {
                    lblPar.Text = "RESULTAT PAR INTERVENANT";

                    Cursor.Current = Cursors.WaitCursor;
                    FC_intervenant(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));

                    Cursor.Current = Cursors.WaitCursor;

                    if (General.sCalculPorteFeuilleCtrV2 == "1")
                    {
                        fc_GrpV2(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)), 3);
                    }
                    else
                    {
                        fc_GrpIntervenant(Convert.ToDateTime(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                    }
                }
            }

            Cursor.Current = Cursors.Arrow;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void FC_intervenant(DateTime dtarret)
        {
            //sSQL = "SELECT     POC_DateArret as [" + cDateArret + "]," + " NumContrat as [" + cNumContrat + "],"
            //    + " Avenant as [" + CAvenant + "]," + " CodeImmeuble as [" + cCodeImmeuble + "],"
            //    + " Code1 as [" + cCode1 + "]," + " CodeArticle as [" + cCodeArticle + "],"
            //    + " POC_Designation as [" + cDesignation + "]," + " POC_Ca_Num as [" + cCA_Num + "],"
            //    + " POC_IntervMat as [" + cMatricule + "]," + " POC_IntervPrenom as [" + cPrenom + "],"
            //    + " POC_IntervNom as [" + cNom + "]," + " POC_BaseContrat AS " + cGrpTotalHT + ""
            //    + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] "
            //    + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'"
            //    + " order by POC_IntervMat";

            //rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            //ssPortefeuille.Refresh();


            sSQL = "SELECT   POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "],"
                + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "],"
                + " Code1 as [" + cCode1 + "],"
                + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "],"
                + " POC_IntervMat as [" + cMatricule + "],"
                + " POC_IntervPrenom as [" + cPrenom + "],"
                + " POC_IntervNom as [" + cNom + "],"
                + " POC_BaseContrat AS " + cGrpTotalHT + ""
                 + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] ";

            sSQL = sSQL + " From POC_PorteFeuilleCtr"
                  + " WHERE  POC_DateArret ='" + dtarret + "'";

            sSQL = sSQL + " order by POC_IntervMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.DataSource = rsPorteFeuille;
            ssPortefeuille.Refresh();

        }
        private void fc_Intervenant2903(ref System.DateTime dtarret)
        {


            sSQL = "SELECT     POC_DateArret as [" + cDateArret + "]," + " NumContrat as [" + cNumContrat + "],"
                + " Avenant as [" + CAvenant + "]," + " CodeImmeuble as [" + cCodeImmeuble + "],"
                + " Code1 as [" + cCode1 + "]," + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "]," + " POC_Ca_Num as [" + cCA_Num + "],"
                + " POC_IntervMat as [" + cMatricule + "]," + " POC_IntervInitial as [" + cInitial + "],"
                + " POC_IntervPrenom as [" + cPrenom + "]," + " POC_IntervNom as [" + cNom + "]"
                + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'"
                + " order by POC_IntervMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.Refresh();



        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_GrpIntervenant(System.DateTime dtarret)
        {

            //sSQL = "SELECT POC_DateArret AS " & cGrpArret & "," _
            //'        & " POC_IntervMat AS " & cGrpMat & "," _
            //'        & " POC_IntervPrenom AS " & cGrpPrenom & "," _
            //'        & " POC_IntervNom AS " & cGrpNom & ", " _
            //'        & " SUM(POC_BaseContrat) AS " & cGrpTotalHT & "," _
            //'        & " COUNT(POC_NoAuto) AS [" & cGrpNbCont & "]" _
            //'        & " From POC_PorteFeuilleCtr" _
            //'        & " GROUP BY POC_DateArret ," _
            //'        & " POC_IntervMat ," _
            //'        & " POC_IntervPrenom," _
            //'        & " POC_IntervNom" _
            //'        & " HAVING  POC_DateArret ='" & dtArret & "'" _
            //'        & " ORDER BY POC_IntervMat"
            //
            //    Set rsGrpPrtFeuille = New ADODB.Recordset
            //    rsGrpPrtFeuille.Open sSQL, adocnn, adOpenKeyset, adLockReadOnly
            //    ssGrpPort.ReBind
            //=============================================
            string sMat = null;

            int i = 0;
            double dbTOTGen = 0;
            string sAddItem = null;

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + "," + " POC_IntervMat AS "
                + cGrpMat + "," + " POC_IntervNom AS " + cGrpNom + ", " + " SUM(POC_BaseContrat) AS "
                + cGrpTotalHT + "," + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "],"
                + " Avenant as [" + cGrpAvenant + "]," + " SUM(POC_HtP1) AS [" + cGrpTotalHTp1 + "],"
                + " SUM(POC_HtP2) AS [" + cGrpTotalHTp2 + "]," + " SUM(POC_HtP3) AS [" + cGrpTotalHTp3 + "],"
                + " SUM(POC_HtP4) AS [" + cGrpTotalHTp4 + "] " + " From POC_PorteFeuilleCtr"
                + " GROUP BY POC_DateArret ," + " POC_IntervMat ," + " POC_IntervNom," + " Avenant"
                + " HAVING  POC_DateArret ='" + dtarret + "'" + " ORDER BY POC_IntervMat,Avenant ";


            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);
            ssGrpPort.DataSource = null;

            tGrpPrt = null;

            if (rsGrpPrtFeuille.Rows.Count > 0)
            {
                sMat = rsGrpPrtFeuille.Rows[0][cGrpMat] + "";
                i = 0;
                Array.Resize(ref tGrpPrt, i + 1);


                foreach (DataRow DrPf in rsGrpPrtFeuille.Rows)
                {

                    if (sMat != DrPf[cGrpMat] + "")
                    {
                        sMat = DrPf[cGrpMat] + "";
                        i = i + 1;
                        Array.Resize(ref tGrpPrt, i + 1);
                    }
                    tGrpPrt[i].dtDateArret = dtarret;
                    tGrpPrt[i].sMat = sMat;
                    tGrpPrt[i].sNom = DrPf[cGrpNom] + "";
                    if (Convert.ToString(General.nz(DrPf[CAvenant], "0")) == "0")
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvt0 = General.FncArrondir(Convert.ToDouble(tGrpPrt[i].dbTotAvt0 + Convert.ToDouble(DrPf[cGrpTotalHT])), 2);
                        tGrpPrt[i].dbNbAvt0 = tGrpPrt[i].dbNbAvt0 + Convert.ToDouble(DrPf[cGrpNbCont]);
                    }
                    else
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvAutre = General.FncArrondir(tGrpPrt[i].dbTotAvAutre + Convert.ToDouble(General.nz(DrPf[cGrpTotalHT], 0)), 2);
                        tGrpPrt[i].dbNbAvtAutre = tGrpPrt[i].dbNbAvtAutre + Convert.ToDouble(General.nz(DrPf[cGrpNbCont], 0));
                    }
                    tGrpPrt[i].dbTotHT = General.FncArrondir(tGrpPrt[i].dbTotHT + Convert.ToDouble(General.nz(DrPf[cGrpTotalHT], 0)), 2);
                    tGrpPrt[i].dbTotNb = tGrpPrt[i].dbTotNb + Convert.ToDouble(General.nz((DrPf[cGrpNbCont]), 0));

                    tGrpPrt[i].dbTotHTP1 = General.FncArrondir(tGrpPrt[i].dbTotHTP1 + Convert.ToDouble(General.nz(DrPf[cGrpTotalHTp1], 0)), 2);
                    tGrpPrt[i].dbTotHTP2 = General.FncArrondir(tGrpPrt[i].dbTotHTP2 + Convert.ToDouble(General.nz(DrPf[cGrpTotalHTp2], 0)), 2);
                    tGrpPrt[i].dbTotHTP3 = General.FncArrondir(tGrpPrt[i].dbTotHTP3 + Convert.ToDouble(General.nz(DrPf[cGrpTotalHTp3], 0)), 2);
                    tGrpPrt[i].dbTotHTP4 = General.FncArrondir(tGrpPrt[i].dbTotHTP4 + Convert.ToDouble(General.nz(DrPf[cGrpTotalHTp4], 0)), 2);


                }
                DataTable dtp = new DataTable();
                dtp.Columns.Add("DateArret");
                dtp.Columns.Add("Matricule");
                dtp.Columns.Add("Nom");
                dtp.Columns.Add("Nbre de contrat");
                dtp.Columns.Add("Total HT");
                dtp.Columns.Add("Nb Avenant");
                dtp.Columns.Add("Total Avenant");
                dtp.Columns.Add("HTP1");
                dtp.Columns.Add("HTP2");
                dtp.Columns.Add("HTP3");
                dtp.Columns.Add("HTP4");
                for (i = 0; i < tGrpPrt.Length; i++)
                {
                    DataRow dr = dtp.NewRow();

                    dr["DateArret"] = tGrpPrt[i].dtDateArret;
                    dr["Matricule"] = tGrpPrt[i].sMat;
                    dr["Nom"] = tGrpPrt[i].sNom;
                    dr["Nbre de contrat"] = tGrpPrt[i].dbNbAvt0;
                    dr["Total HT"] = tGrpPrt[i].dbTotAvt0;
                    dr["Nb Avenant"] = tGrpPrt[i].dbNbAvtAutre;
                    dr["Total Avenant"] = tGrpPrt[i].dbTotAvAutre;
                    dr["HTP1"] = tGrpPrt[i].dbTotHTP1;
                    dr["HTP2"] = tGrpPrt[i].dbTotHTP2;
                    dr["HTP3"] = tGrpPrt[i].dbTotHTP3;
                    dr["HTP4"] = tGrpPrt[i].dbTotHTP4;

                    dtp.Rows.Add(dr);

                }
                ssGrpPort.DataSource = dtp;

                //sAddItem = "@" + tGrpPrt[i].dtDateArret + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].sMat + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].sNom + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvt0 + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvt0 + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvtAutre + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvAutre + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP1 + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP2 + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP3 + "@$";
                //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP4 + "@$";
                //ssGrpPort.AddItem(sAddItem);


            }

        }
        private void fc_GrpIntervenant2903(ref System.DateTime dtarret)
        {

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + ","
                + " POC_IntervInitial AS " + cGrpInit + ","
                + " POC_IntervMat AS " + cGrpMat + "," + " POC_IntervPrenom AS "
                + cGrpPrenom + "," + " POC_IntervNom AS " + cGrpNom + ", "
                + " SUM(POC_BaseContrat) AS " + cGrpTotalHT + ","
                + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "]"
                + " From POC_PorteFeuilleCtr" + " GROUP BY POC_DateArret ,"
                + " POC_IntervInitial," + " POC_IntervMat ," + " POC_IntervPrenom,"
                + " POC_IntervNom" + " HAVING  POC_DateArret ='" + dtarret + "'"
                + " ORDER BY POC_IntervMat";

            rsGrpPrtFeuille = new DataTable();
            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_GrpRespExploit(System.DateTime dtarret)
        {

            //sSQL = "SELECT POC_DateArret AS " & cGrpArret & "," _
            //'        & " POC_RexpMat AS " & cGrpMat & "," _
            //'        & " POC_RexpPrenom AS " & cGrpPrenom & "," _
            //'        & " POC_RexpNom AS " & cGrpNom & ", " _
            //'        & " SUM(POC_BaseContrat) AS " & cGrpTotalHT & "," _
            //'        & " COUNT(POC_NoAuto) AS [" & cGrpNbCont & "]" _
            //'        & " From POC_PorteFeuilleCtr" _
            //'        & " GROUP BY POC_DateArret ," _
            //'        & " POC_RexpMat ," _
            //'        & " POC_RexpPrenom," _
            //'        & " POC_RexpNom" _
            //'        & " HAVING  POC_DateArret ='" & dtArret & "'" _
            //'        & " ORDER BY POC_RexpMat"
            //
            //    Set rsGrpPrtFeuille = New ADODB.Recordset
            //    rsGrpPrtFeuille.Open sSQL, adocnn, adOpenKeyset, adLockReadOnly
            //    ssGrpPort.ReBind
            //===============================================
            string sMat = null;

            int i = 0;
            double dbTOTGen = 0;
            string sAddItem = null;

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + ","
                + " POC_RexpMat AS " + cGrpMat + "," + " POC_RexpNom AS "
                + cGrpNom + ", " + " SUM(POC_BaseContrat) AS " + cGrpTotalHT + ","
                + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "],"
                + " Avenant as [" + cGrpAvenant + "],"
                + " SUM(POC_HtP1) AS [" + cGrpTotalHTp1 + "],"
                + " SUM(POC_HtP2) AS [" + cGrpTotalHTp2 + "],"
                + " SUM(POC_HtP3) AS [" + cGrpTotalHTp3 + "],"
                + " SUM(POC_HtP4) AS [" + cGrpTotalHTp4 + "] "
                + " From POC_PorteFeuilleCtr" + " GROUP BY POC_DateArret ,"
                + " POC_RexpMat ," + " POC_RexpNom," + " Avenant"
                + " HAVING  POC_DateArret ='" + dtarret + "'"
                + " ORDER BY POC_RexpMat,Avenant ";


            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);

            ssGrpPort.DataSource = null;
            tGrpPrt = null;

            var _with8 = rsGrpPrtFeuille;

            if (rsGrpPrtFeuille.Rows.Count > 0)
            {
                sMat = rsGrpPrtFeuille.Rows[0][cGrpMat] + "";
                i = 0;
                Array.Resize(ref tGrpPrt, i + 1);

                foreach (DataRow DrGpf in rsGrpPrtFeuille.Rows)
                {

                    if (sMat != DrGpf[cGrpMat] + "")
                    {
                        sMat = DrGpf[cGrpMat] + "";
                        i = i + 1;
                        Array.Resize(ref tGrpPrt, i + 1);
                    }
                    tGrpPrt[i].dtDateArret = dtarret;
                    tGrpPrt[i].sMat = sMat;
                    tGrpPrt[i].sNom = DrGpf[cGrpNom] + "";
                    if (Convert.ToString(General.nz(DrGpf[CAvenant], "0")) == "0")
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvt0 = General.FncArrondir(tGrpPrt[i].dbTotAvt0 + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHT], 0)), 2);
                        tGrpPrt[i].dbNbAvt0 = tGrpPrt[i].dbNbAvt0 + Convert.ToDouble(General.nz(DrGpf[cGrpNbCont], 0));
                    }
                    else
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvAutre = General.FncArrondir(tGrpPrt[i].dbTotAvAutre + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHT], 0)), 2);
                        tGrpPrt[i].dbNbAvtAutre = tGrpPrt[i].dbNbAvtAutre + Convert.ToDouble(General.nz(DrGpf[cGrpNbCont], 0));
                    }
                    tGrpPrt[i].dbTotHT = General.FncArrondir(tGrpPrt[i].dbTotHT + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHT], 0)), 2);
                    tGrpPrt[i].dbTotNb = tGrpPrt[i].dbTotNb + Convert.ToDouble(General.nz(DrGpf[cGrpNbCont], 0));

                    tGrpPrt[i].dbTotHTP1 = General.FncArrondir(tGrpPrt[i].dbTotHTP1 + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHTp1], 0)), 2);
                    tGrpPrt[i].dbTotHTP2 = General.FncArrondir(tGrpPrt[i].dbTotHTP2 + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHTp2], 0)), 2);
                    tGrpPrt[i].dbTotHTP3 = General.FncArrondir(tGrpPrt[i].dbTotHTP3 + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHTp3], 0)), 2);
                    tGrpPrt[i].dbTotHTP4 = General.FncArrondir(tGrpPrt[i].dbTotHTP4 + Convert.ToDouble(General.nz(DrGpf[cGrpTotalHTp4], 0)), 2);

                }
                DataTable dtttt = new DataTable();

                dtttt.Columns.Add("DateArret");
                dtttt.Columns.Add("Matricule");
                dtttt.Columns.Add("Nom");
                dtttt.Columns.Add("Nbre de contrat");
                dtttt.Columns.Add("Total HT");
                dtttt.Columns.Add("Nb Avenant");
                dtttt.Columns.Add("Total Avenant");
                dtttt.Columns.Add("HTP1");
                dtttt.Columns.Add("HTP2");
                dtttt.Columns.Add("HTP3");
                dtttt.Columns.Add("HTP4");
                for (i = 0; i < tGrpPrt.Length; i++)
                {
                    //sAddItem = "@" + tGrpPrt[i].dtDateArret + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].sMat + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].sNom + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvt0 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvt0 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvtAutre + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvAutre + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP1 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP2 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP3 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP4 + "@$";

                    //ssGrpPort.AddItem(sAddItem);

                    DataRow dr = dtttt.NewRow();

                    dr["DateArret"] = tGrpPrt[i].dtDateArret;
                    dr["Matricule"] = tGrpPrt[i].sMat;
                    dr["Nom"] = tGrpPrt[i].sNom;
                    dr["Nbre de contrat"] = tGrpPrt[i].dbNbAvt0;
                    dr["Total HT"] = tGrpPrt[i].dbTotAvt0;
                    dr["Nb Avenant"] = tGrpPrt[i].dbNbAvtAutre;
                    dr["Total Avenant"] = tGrpPrt[i].dbTotAvAutre;
                    dr["HTP1"] = tGrpPrt[i].dbTotHTP1;
                    dr["HTP2"] = tGrpPrt[i].dbTotHTP2;
                    dr["HTP3"] = tGrpPrt[i].dbTotHTP3;
                    dr["HTP4"] = tGrpPrt[i].dbTotHTP4;

                    dtttt.Rows.Add(dr);

                }
                ssGrpPort.DataSource = dtttt;
            }

        }
        private void fc_GrpRespExploit2903(ref System.DateTime dtarret)
        {

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + ","
                + " POC_RexpInitial AS " + cGrpInit + ","
                + " POC_RexpMat AS " + cGrpMat + ","
                + " POC_RexpPrenom AS " + cGrpPrenom + ","
                + " POC_RexpNom AS " + cGrpNom + ", "
                + " SUM(POC_BaseContrat) AS " + cGrpTotalHT + ","
                + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "]"
                + " From POC_PorteFeuilleCtr" + " GROUP BY POC_DateArret ,"
                + " POC_RexpInitial," + " POC_RexpMat ," + " POC_RexpPrenom,"
                + " POC_RexpNom" + " HAVING  POC_DateArret ='" + dtarret + "'"
                + " ORDER BY POC_RexpMat";

            rsGrpPrtFeuille = new DataTable();
            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_RespExploit(System.DateTime dtarret)
        {

            sSQL = "SELECT   POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "],"
                + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "],"
                + " Code1 as [" + cCode1 + "]," + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_RexpMat as [" + cMatricule + "],"
                + " POC_RexpPrenom as [" + cPrenom + "]," + " POC_RexpNom as [" + cNom + "],"
                + " POC_BaseContrat AS " + cGrpTotalHT + ""
                + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] "
                + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'"
                + " order by POC_RexpMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.DataSource = rsPorteFeuille;
            ssPortefeuille.Refresh();


        }
        private void fc_RespExploit2903(ref System.DateTime dtarret)
        {


            sSQL = "SELECT     POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "]," + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "]," + " Code1 as [" + cCode1 + "],"
                + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_RexpMat as [" + cMatricule + "],"
                + " POC_RexpInitial as [" + cInitial + "],"
                + " POC_RexpPrenom as [" + cPrenom + "]," + " POC_RexpNom as [" + cNom + "]"
                + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] "
                + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'"
                + " order by POC_RexpMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);

            ssPortefeuille.Refresh();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_ChefDeSecteur(System.DateTime dtarret)
        {


            sSQL = "SELECT   POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "]," + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "]," + " Code1 as [" + cCode1 + "],"
                + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_CdsMat as [" + cMatricule + "],"
                + " POC_CdsPrenom as [" + cPrenom + "]," + " POC_CdsNom as [" + cNom + "],"
                + " POC_BaseContrat AS " + cGrpTotalHT + ""
                + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] "
                + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'" + " order by POC_CdsMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.DataSource = rsPorteFeuille;
            ssPortefeuille.Refresh();


        }
        private void fc_ChefDeSecteur2903(ref System.DateTime dtarret)
        {


            sSQL = "SELECT     POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "],"
                + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "],"
                + " Code1 as [" + cCode1 + "]," + " CodeArticle as [" + cCodeArticle + "],"
                + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_CdsMat as [" + cMatricule + "],"
                + " POC_CdsInitial as [" + cInitial + "],"
                + " POC_CdsPrenom as [" + cPrenom + "]," + " POC_CdsNom as [" + cNom + "]"
                + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] "
                + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'"
                + " order by POC_CdsMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.Refresh();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_GrpChefDeSecteur(DateTime dtarret)
        {

            //sSQL = "SELECT POC_DateArret AS " & cGrpArret & "," _
            //'        & " POC_CdsMat AS " & cGrpMat & "," _
            //'        & " POC_CdsPrenom AS " & cGrpPrenom & "," _
            //'        & " POC_CdsNom AS " & cGrpNom & ", " _
            //'        & " SUM(POC_BaseContrat) AS " & cGrpTotalHT & "," _
            //'        & " COUNT(POC_NoAuto) AS [" & cGrpNbCont & "]" _
            //'        & " From POC_PorteFeuilleCtr" _
            //'        & " GROUP BY POC_DateArret ," _
            //'        & " POC_CdsMat ," _
            //'        & " POC_CdsPrenom," _
            //'        & " POC_CdsNom" _
            //'        & " HAVING  POC_DateArret ='" & dtArret & "'" _
            //'        & " ORDER BY POC_CdsMat"
            //
            //    Set rsGrpPrtFeuille = New ADODB.Recordset
            //    rsGrpPrtFeuille.Open sSQL, adocnn, adOpenKeyset, adLockReadOnly
            //    ssGrpPort.ReBind
            //=================================================================
            string sMat = null;

            int i = 0;
            double dbTOTGen = 0;
            string sAddItem = null;

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + "," + " POC_CdsMat AS "
                + cGrpMat + "," + " POC_CdsNom AS " + cGrpNom + ", "
                + " SUM(POC_BaseContrat) AS " + cGrpTotalHT + ","
                + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "],"
                + " Avenant as [" + cGrpAvenant + "]," + " SUM(POC_HtP1) AS [" + cGrpTotalHTp1 + "],"
                + " SUM(POC_HtP2) AS [" + cGrpTotalHTp2 + "]," + " SUM(POC_HtP3) AS [" + cGrpTotalHTp3
                + "]," + " SUM(POC_HtP4) AS [" + cGrpTotalHTp4 + "] "
                + " From POC_PorteFeuilleCtr" + " GROUP BY POC_DateArret ," + " POC_CdsMat ,"
                + " POC_CdsNom," + " Avenant" + " HAVING  POC_DateArret ='" + dtarret + "'"
                + " ORDER BY POC_CdsMat,Avenant ";


            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);

            ssGrpPort.DataSource = null;
            tGrpPrt = null;

            var _with9 = rsGrpPrtFeuille;

            if (rsGrpPrtFeuille.Rows.Count > 0)
            {
                sMat = rsGrpPrtFeuille.Rows[0][cGrpMat] + "";
                i = 0;
                Array.Resize(ref tGrpPrt, i + 1);


                foreach (DataRow drg in rsGrpPrtFeuille.Rows)
                {

                    if (sMat != drg[cGrpMat] + "")
                    {
                        sMat = drg[cGrpMat] + "";
                        i = i + 1;
                        Array.Resize(ref tGrpPrt, i + 1);
                    }
                    tGrpPrt[i].dtDateArret = dtarret;
                    tGrpPrt[i].sMat = sMat;
                    tGrpPrt[i].sNom = drg[cGrpNom] + "";
                    if (Convert.ToString(General.nz(drg[CAvenant], "0")) == "0")
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvt0 = General.FncArrondir(tGrpPrt[i].dbTotAvt0 + Convert.ToDouble(General.nz(drg[cGrpTotalHT], 0)), 2);
                        tGrpPrt[i].dbNbAvt0 = tGrpPrt[i].dbNbAvt0 + Convert.ToDouble(General.nz(drg[cGrpNbCont], 0));
                    }
                    else
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvAutre = General.FncArrondir(tGrpPrt[i].dbTotAvAutre + Convert.ToDouble(General.nz(drg[cGrpTotalHT], 0)), 2);
                        tGrpPrt[i].dbNbAvtAutre = tGrpPrt[i].dbNbAvtAutre + Convert.ToDouble(General.nz(drg[cGrpNbCont], 0));
                    }
                    tGrpPrt[i].dbTotHT = General.FncArrondir(tGrpPrt[i].dbTotHT + Convert.ToDouble(General.nz(drg[cGrpTotalHT], 0)), 2);
                    tGrpPrt[i].dbTotNb = tGrpPrt[i].dbTotNb + Convert.ToDouble(General.nz(drg[cGrpNbCont], 0));

                    tGrpPrt[i].dbTotHTP1 = General.FncArrondir(tGrpPrt[i].dbTotHTP1 + Convert.ToDouble(General.nz(drg[cGrpTotalHTp1], 0)), 2);
                    tGrpPrt[i].dbTotHTP2 = General.FncArrondir(tGrpPrt[i].dbTotHTP2 + Convert.ToDouble(General.nz(drg[cGrpTotalHTp2], 0)), 2);
                    tGrpPrt[i].dbTotHTP3 = General.FncArrondir(tGrpPrt[i].dbTotHTP3 + Convert.ToDouble(General.nz(drg[cGrpTotalHTp3], 0)), 2);
                    tGrpPrt[i].dbTotHTP4 = General.FncArrondir(tGrpPrt[i].dbTotHTP4 + Convert.ToDouble(General.nz(drg[cGrpTotalHTp4], 0)), 2);

                }


                DataTable dttt = new DataTable();

                dttt.Columns.Add("DateArret");
                dttt.Columns.Add("Matricule");
                dttt.Columns.Add("Nom");
                dttt.Columns.Add("Nbre de contrat");
                dttt.Columns.Add("Total HT");
                dttt.Columns.Add("Nb Avenant");
                dttt.Columns.Add("Total Avenant");
                dttt.Columns.Add("HTP1");
                dttt.Columns.Add("HTP2");
                dttt.Columns.Add("HTP3");
                dttt.Columns.Add("HTP4");

                for (i = 0; i < tGrpPrt.Length; i++)
                {
                    //sAddItem = "@" + tGrpPrt[i].dtDateArret + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].sMat + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].sNom + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvt0 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvt0 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbNbAvtAutre + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotAvAutre + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP1 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP2 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP3 + "@$";
                    //sAddItem = sAddItem + "@" + tGrpPrt[i].dbTotHTP4 + "@$";

                    //ssGrpPort.AddItem(sAddItem);

                    DataRow dr = dttt.NewRow();

                    dr["DateArret"] = tGrpPrt[i].dtDateArret;
                    dr["Matricule"] = tGrpPrt[i].sMat;
                    dr["Nom"] = tGrpPrt[i].sNom;
                    dr["Nbre de contrat"] = tGrpPrt[i].dbNbAvt0;
                    dr["Total HT"] = tGrpPrt[i].dbTotAvt0;
                    dr["Nb Avenant"] = tGrpPrt[i].dbNbAvtAutre;
                    dr["Total Avenant"] = tGrpPrt[i].dbTotAvAutre;
                    dr["HTP1"] = tGrpPrt[i].dbTotHTP1;
                    dr["HTP2"] = tGrpPrt[i].dbTotHTP2;
                    dr["HTP3"] = tGrpPrt[i].dbTotHTP3;
                    dr["HTP4"] = tGrpPrt[i].dbTotHTP4;

                    dttt.Rows.Add(dr);

                }
                ssGrpPort.DataSource = dttt;
            }




        }
        private void fc_GrpChefDeSecteur2903(ref System.DateTime dtarret)
        {

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + ","
                + " POC_CdsInitial AS " + cGrpInit + ","
                + " POC_CdsMat AS " + cGrpMat + "," + " POC_CdsPrenom AS "
                + cGrpPrenom + "," + " POC_CdsNom AS " + cGrpNom + ", "
                + " SUM(POC_BaseContrat) AS " + cGrpTotalHT + ","
                + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "]" + " From POC_PorteFeuilleCtr"
                + " GROUP BY POC_DateArret ," + "POC_CdsInitial," + " POC_CdsMat ,"
                + " POC_CdsPrenom," + " POC_CdsNom" + " HAVING  POC_DateArret ='"
                + dtarret + "'" + " ORDER BY POC_CdsMat";

            rsGrpPrtFeuille = new DataTable();
            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        /// <param name="lTypeInterv"></param>
        private void fc_GrpV2(System.DateTime dtarret, int lTypeInterv)
        {
            string sMat = null;

            int i = 0;
            double dbTOTGen = 0;
            string sAddItem = null;
            string sOrder = null;
            DataTable rsCount = default(DataTable);
            int xi = 0;
            if (lTypeInterv == 0)
            {
                //=== par commercial.
                sSQL = "SELECT  POC_DateArret as " + cGrpArret + ","
                    + " POC_ComMat AS " + cGrpMat + "," + " POC_ComNom AS "
                    + cGrpNom + ", " + " COUNT(*) AS [" + cGrpNbCont + "], "
                    + " POC_COT2_Code, " + " SUM(POC_BaseContrat) AS  " + cGrpTotalHT
                    + "" + " From POC_PorteFeuilleCtr "
                    + " GROUP BY POC_DateArret, POC_ComMat, POC_ComNom, POC_COT2_Code "
                    + " HAVING  POC_DateArret = '" + dtarret + "'"
                    + " ORDER BY Matricule,POC_COT2_Code ";

            }
            else if (lTypeInterv == 1)
            {
                //=== par chef de secteur.
                sSQL = "SELECT  POC_DateArret as " + cGrpArret + "," + " POC_CdsMat AS "
                    + cGrpMat + "," + " POC_CdsNom AS " + cGrpNom + ", "
                    + " COUNT(*) AS [" + cGrpNbCont + "], " + " POC_COT2_Code, "
                    + " SUM(POC_BaseContrat) AS  " + cGrpTotalHT + ""
                    + " From POC_PorteFeuilleCtr "
                    + " GROUP BY POC_DateArret, POC_CdsMat, POC_CdsNom, POC_COT2_Code "
                    + " HAVING  POC_DateArret = '" + dtarret + "'"
                    + " ORDER BY Matricule,POC_COT2_Code ";

            }
            else if (lTypeInterv == 2)
            {
                //=== responsable d exploit.
                sSQL = "SELECT  POC_DateArret as " + cGrpArret + ","
                    + " POC_RexpMat AS " + cGrpMat + "," + " POC_RexpNom AS "
                    + cGrpNom + ", " + " COUNT(*) AS [" + cGrpNbCont + "], "
                    + " POC_COT2_Code, " + " SUM(POC_BaseContrat) AS  " + cGrpTotalHT
                    + "" + " From POC_PorteFeuilleCtr " + " GROUP BY POC_DateArret, POC_RexpMat, POC_RexpNom, POC_COT2_Code "
                    + " HAVING  POC_DateArret = '" + dtarret + "'"
                    + " ORDER BY Matricule,POC_COT2_Code ";

            }
            else if (lTypeInterv == 3)
            {
                //=== par intervenant.
                sSQL = "SELECT  POC_DateArret as " + cGrpArret + "," + " POC_IntervMat AS "
                    + cGrpMat + "," + " POC_IntervNom AS " + cGrpNom + ", "
                    + " COUNT(*) AS [" + cGrpNbCont + "], " + " POC_COT2_Code, "
                    + " SUM(POC_BaseContrat) AS  " + cGrpTotalHT + ""
                    + " From POC_PorteFeuilleCtr "
                    + " GROUP BY POC_DateArret, POC_IntervMat, POC_IntervNom, POC_COT2_Code "
                    + " HAVING  POC_DateArret = '" + dtarret + "'"
                    + " ORDER BY Matricule,POC_COT2_Code ";

            }

            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);

            // ssGrpPortV2.RemoveAll();
            ssGrpPortV2.DataSource = null;
            tGrpPrtV2 = null;

            var _with10 = rsGrpPrtFeuille;
            var tmpModAdo = new ModAdo();

            if (rsGrpPrtFeuille.Rows.Count > 0)
            {
                sMat = rsGrpPrtFeuille.Rows[0][cGrpMat] + "";
                i = 0;
                Array.Resize(ref tGrpPrtV2, i + 1);


                foreach (DataRow dr in rsGrpPrtFeuille.Rows)
                {

                    if (sMat != dr[cGrpMat] + "")
                    {
                        sMat = dr[cGrpMat] + "";
                        i = i + 1;
                        Array.Resize(ref tGrpPrtV2, i + 1);
                    }
                    tGrpPrtV2[i].dtDateArret = dtarret;
                    tGrpPrtV2[i].sMat = sMat;
                    tGrpPrtV2[i].sNom = dr[cGrpNom] + "";

                    //===> Mondir le 16.10.2020, requested https://groupe-dt.mantishub.io/view.php?id=1819
                    if (tGrpPrtV2[i].nbrImmeuble == 0)
                    {
                        var query = $@"SELECT COUNT(DISTINCT POC_PorteFeuilleCtr.CodeImmeuble) AS 'NbrImmeuble', SUM(CASE WHEN nonFacturable = 1 THEN 1 ELSE 0 END) AS 'NonFacturable',
                                        SUM(CASE WHEN nonFacturable = 0 THEN 1 ELSE 0 END) AS 'Facturable' FROM POC_PorteFeuilleCtr
                                        JOIN Contrat ON Contrat.NumContrat = POC_PorteFeuilleCtr.NumContrat AND Contrat.Avenant = POC_PorteFeuilleCtr.Avenant 
                                        WHERE POC_DateArret = '{dtarret}' ";
                        //=== par commercial.
                        if (lTypeInterv == 0)
                        {
                            query += $@"AND POC_ComMat = '{sMat}'";
                        }
                        //=== par chef de secteur.
                        else if (lTypeInterv == 1)
                        {
                            query += $"AND POC_CdsMat = '{sMat}'";
                        }
                        //=== responsable d exploit.
                        else if (lTypeInterv == 2)
                        {
                            query += $"AND POC_RexpMat = '{sMat}'";
                        }
                        //=== par intervenant.
                        else if (lTypeInterv == 3)
                        {
                            query += $"AND POC_IntervMat = '{sMat}'";
                        }
                        var row = tmpModAdo.fc_OpenRecordSet(query).Rows[0];
                        tGrpPrtV2[i].nbrImmeuble = Convert.ToInt32(General.nz(row["NbrImmeuble"], 0));
                        tGrpPrtV2[i].nbrNonFacturable = Convert.ToInt32(General.nz(row["NonFacturable"], 0));
                        tGrpPrtV2[i].nbrFacturable = Convert.ToInt32(General.nz(row["Facturable"], 0));
                    }
                    //===> Fin Modif Mondir

                    //switch (General.UCase(dr["POC_COT2_Code"].ToString()))
                    //{
                    //    case General.UCase(cP1):
                    //        tGrpPrtV2[i].dbNbP1 =Convert.ToDouble(General.nz(  dr[cGrpNbCont],0));
                    //        tGrpPrtV2[i].dbTotHTP1 = Convert.ToDouble(General.nz(dr[cGrpTotalHT],   0));
                    //        break;
                    //    case General.UCase(cP2C):
                    //        tGrpPrtV2[i].dbNbP2 = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                    //        tGrpPrtV2[i].dbTotHTP2 = Convert.ToDouble(General.nz(dr[cGrpTotalHT],   0));
                    //        tGrpPrtV2[i].dbTotHTP2_P2avt = tGrpPrtV2[i].dbTotHTP2_P2avt + tGrpPrtV2[i].dbTotHTP2;
                    //        break;
                    //    case General.UCase(cP2avt):
                    //        tGrpPrtV2[i].dbNbP2avt = Convert.ToDouble(General.nz(dr[cGrpNbCont],   0));
                    //        tGrpPrtV2[i].dbTotHTP2avt = Convert.ToDouble(General.nz(dr[cGrpTotalHT],   0));
                    //        tGrpPrtV2[i].dbTotHTP2_P2avt = tGrpPrtV2[i].dbTotHTP2_P2avt + tGrpPrtV2[i].dbTotHTP2avt;
                    //        break;
                    //    case General.UCase(cP3):
                    //        tGrpPrtV2[i].dbNbP3 = Convert.ToDouble(General.nz(dr[cGrpNbCont],   0));
                    //        tGrpPrtV2[i].dbTotHTP3 = Convert.ToDouble(General.nz(dr[cGrpTotalHT],   0));
                    //        break;
                    //    case General.UCase(cP4):
                    //        tGrpPrtV2[i].dbNbP4 = Convert.ToDouble(General.nz(dr[cGrpNbCont],   0));
                    //        tGrpPrtV2[i].dbTotHTP4 = Convert.ToDouble(General.nz(dr[cGrpTotalHT],  0));
                    //        break;

                    //}


                    if (General.UCase(dr["POC_COT2_Code"].ToString()) == General.UCase(cP1))
                    {
                        tGrpPrtV2[i].dbNbP1 = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                        tGrpPrtV2[i].dbTotHTP1 = Convert.ToDouble(General.nz(dr[cGrpTotalHT], 0));
                    }
                    else if (General.UCase(dr["POC_COT2_Code"].ToString()) == General.UCase(cP2C))
                    {
                        tGrpPrtV2[i].dbNbP2 = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                        tGrpPrtV2[i].dbTotHTP2 = Convert.ToDouble(General.nz(dr[cGrpTotalHT], 0));
                        tGrpPrtV2[i].dbTotHTP2_P2avt = tGrpPrtV2[i].dbTotHTP2_P2avt + tGrpPrtV2[i].dbTotHTP2;
                    }
                    else if (General.UCase(dr["POC_COT2_Code"].ToString()) == General.UCase(cP2avt))
                    {
                        tGrpPrtV2[i].dbNbP2avt = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                        tGrpPrtV2[i].dbTotHTP2avt = Convert.ToDouble(General.nz(dr[cGrpTotalHT], 0));
                        tGrpPrtV2[i].dbTotHTP2_P2avt = tGrpPrtV2[i].dbTotHTP2_P2avt + tGrpPrtV2[i].dbTotHTP2avt;
                    }
                    else if (General.UCase(dr["POC_COT2_Code"].ToString()) == General.UCase(cP3))
                    {

                        tGrpPrtV2[i].dbNbP3 = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                        tGrpPrtV2[i].dbTotHTP3 = Convert.ToDouble(General.nz(dr[cGrpTotalHT], 0));
                    }
                    else if (General.UCase(dr["POC_COT2_Code"].ToString()) == General.UCase(cP4))
                    {

                        tGrpPrtV2[i].dbNbP4 = Convert.ToDouble(General.nz(dr[cGrpNbCont], 0));
                        tGrpPrtV2[i].dbTotHTP4 = Convert.ToDouble(General.nz(dr[cGrpTotalHT], 0));
                    }


                }

                Cursor.Current = Cursors.WaitCursor;
                //=== Modif du 15/12/2014, Pour les contrats P1,P3,P4
                //=== si il existe plusieurs fiches contrat pour un même
                //=== immeuble , on comptabilise qu un seul contrat en quantité,
                //===  en revanche on cumul le montant des bases contrat.
                if (lTypeInterv == 0)
                {
                    //=== par commercial.
                    sSQL = "SELECT DISTINCT POC_ComMat as " + cGrpMat + ",";
                    sOrder = "  ORDER BY POC_ComMat, POC_COT2_Code, CodeImmeuble";

                }
                else if (lTypeInterv == 1)
                {
                    //=== par chef de secteur.
                    sSQL = "SELECT DISTINCT POC_CdsMat as " + cGrpMat + ",";
                    sOrder = "  ORDER BY POC_CdsMat, POC_COT2_Code, CodeImmeuble";

                }
                else if (lTypeInterv == 2)
                {
                    //=== responsable d exploit.
                    sSQL = "SELECT DISTINCT POC_RexpMat as " + cGrpMat + ",";
                    sOrder = "  ORDER BY POC_RexpMat, POC_COT2_Code, CodeImmeuble";

                }
                else if (lTypeInterv == 3)
                {
                    //=== par intervenant.
                    sSQL = "SELECT DISTINCT POC_IntervMat as " + cGrpMat + ",";
                    sOrder = "  ORDER BY POC_IntervMat, POC_COT2_Code, CodeImmeuble";

                }

                sSQL = sSQL + " POC_COT2_Code, CodeImmeuble" + " From POC_PorteFeuilleCtr "
                    + " WHERE     (POC_DateArret = '" + dtarret + "') AND "
                    + " (POC_COT2_Code = '" + cP1 + "' OR POC_COT2_Code = '"
                    + cP3 + "' OR POC_COT2_Code = '" + cP4 + "')" + "" + sOrder;

                rsCount = ModAdorsCount.fc_OpenRecordSet(sSQL);
                for (i = 0; i <= tGrpPrtV2.Length - 1; i++)
                {

                    //=== calcul le nbre de contrat P1
                    //rsCount.Filter = ADODB.FilterGroupEnum.adFilterNone;
                    sSQL = cGrpMat + "='" + StdSQLchaine.gFr_DoublerQuote(tGrpPrtV2[i].sMat) + "'";
                    //rsCount.Select = sSQL + " and POC_COT2_Code ='" + cP1 + "'";
                    var drs = rsCount.Select(sSQL + " and POC_COT2_Code ='" + cP1 + "'");
                    //xi = 0;

                    //foreach(DataRow drC in rsCount.Rows)
                    //{
                    //    xi = xi + 1;

                    //}
                    tGrpPrtV2[i].dbNbP1 = drs.Length;


                    //=== calcul le nbre de contrat P3
                    //rsCount.Filter = ADODB.FilterGroupEnum.adFilterNone;
                    sSQL = cGrpMat + "='" + StdSQLchaine.gFr_DoublerQuote(tGrpPrtV2[i].sMat) + "'";
                    //rsCount.Select = sSQL + " and POC_COT2_Code ='" + cP3 + "'";
                    drs = rsCount.Select(sSQL + " and POC_COT2_Code ='" + cP3 + "'");
                    //xi = 0;
                    //while (rsCount.EOF == false)
                    //{
                    //    xi = xi + 1;

                    //}
                    tGrpPrtV2[i].dbNbP3 = drs.Length;

                    //=== calcul le nbre de contrat P4
                    // rsCount.Filter = ADODB.FilterGroupEnum.adFilterNone;
                    sSQL = cGrpMat + "='" + StdSQLchaine.gFr_DoublerQuote(tGrpPrtV2[i].sMat) + "'";
                    //rsCount.Select = sSQL + " and POC_COT2_Code ='" + cP4 + "'";
                    drs = rsCount.Select(sSQL + " and POC_COT2_Code ='" + cP4 + "'");
                    //xi = 0;
                    //while (rsCount.EOF == false)
                    //{
                    //    xi = xi + 1;
                    //    rsCount.MoveNext();
                    //}
                    tGrpPrtV2[i].dbNbP4 = drs.Length;
                    Cursor.Current = Cursors.WaitCursor;
                }
                DataTable dtt = new DataTable();
                dtt.Columns.Add("DateArret");
                dtt.Columns.Add("Matricule");
                dtt.Columns.Add("Nom");
                dtt.Columns.Add("NbrImmeuble");
                dtt.Columns.Add("NbP1");
                dtt.Columns.Add("NbP2C");
                dtt.Columns.Add("NbP2avt");
                dtt.Columns.Add("NbP3");
                dtt.Columns.Add("NbP4");
                dtt.Columns.Add("HTP2_P2avt");
                dtt.Columns.Add("HTP1");
                dtt.Columns.Add("HTP2C");
                dtt.Columns.Add("HTP2avt");
                dtt.Columns.Add("HTP3");
                dtt.Columns.Add("HTP4");

                for (i = 0; i <= tGrpPrtV2.Length - 1; i++)
                {

                    DataRow dr = dtt.NewRow();

                    dr["DateArret"] = tGrpPrtV2[i].dtDateArret;
                    dr["Matricule"] = tGrpPrtV2[i].sMat;
                    dr["Nom"] = tGrpPrtV2[i].sNom;
                    dr["NbP1"] = tGrpPrtV2[i].dbNbP1;
                    dr["NbP2C"] = tGrpPrtV2[i].dbNbP2;
                    dr["NbP2avt"] = tGrpPrtV2[i].dbNbP2avt;
                    dr["NbP3"] = tGrpPrtV2[i].dbNbP3;
                    dr["NbP4"] = tGrpPrtV2[i].dbNbP4;
                    dr["HTP2_P2avt"] = tGrpPrtV2[i].dbTotHTP2_P2avt;
                    dr["HTP1"] = tGrpPrtV2[i].dbTotHTP1;
                    dr["HTP2C"] = tGrpPrtV2[i].dbTotHTP2;
                    dr["HTP2avt"] = tGrpPrtV2[i].dbTotHTP2avt;
                    dr["HTP3"] = tGrpPrtV2[i].dbTotHTP3;
                    dr["HTP4"] = tGrpPrtV2[i].dbTotHTP4;
                    dr["NbrImmeuble"] = tGrpPrtV2[i].nbrImmeuble;

                    dtt.Rows.Add(dr);
                }
                ssGrpPortV2.DataSource = dtt;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_GrpCommercial(System.DateTime dtarret)
        {
            string sMat = null;

            int i = 0;
            double dbTOTGen = 0;
            string sAddItem = null;

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + "," + " POC_ComMat AS "
                + cGrpMat + "," + " POC_ComNom AS " + cGrpNom + ", " + " SUM(POC_BaseContrat) AS "
                + cGrpTotalHT + "," + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "],"
                + " Avenant as [" + cGrpAvenant + "]," + " SUM(POC_HtP1) AS [" + cGrpTotalHTp1 + "],"
                + " SUM(POC_HtP2) AS [" + cGrpTotalHTp2 + "]," + " SUM(POC_HtP3) AS [" + cGrpTotalHTp3 + "],"
                + " SUM(POC_HtP4) AS [" + cGrpTotalHTp4 + "] "
                + " From POC_PorteFeuilleCtr" + " GROUP BY POC_DateArret ," + " POC_ComMat ,"
                + " POC_ComNom," + " Avenant" + " HAVING  POC_DateArret ='" + dtarret + "'"
                + " ORDER BY POC_ComMat,Avenant ";


            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);

            ssGrpPort.DataSource = null;
            tGrpPrt = null;

            var _with11 = rsGrpPrtFeuille;

            if (rsGrpPrtFeuille.Rows.Count > 0)
            {
                sMat = rsGrpPrtFeuille.Rows[0][cGrpMat] + "";
                i = 0;
                Array.Resize(ref tGrpPrt, i + 1);


                foreach (DataRow drgpf in rsGrpPrtFeuille.Rows)
                {

                    if (sMat != drgpf[cGrpMat] + "")
                    {
                        sMat = drgpf[cGrpMat] + "";
                        i = i + 1;
                        Array.Resize(ref tGrpPrt, i + 1);
                    }
                    tGrpPrt[i].dtDateArret = dtarret;
                    tGrpPrt[i].sMat = sMat;
                    tGrpPrt[i].sNom = drgpf[cGrpNom] + "";
                    if (Convert.ToString(General.nz((drgpf[CAvenant]), "0")) == "0")
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvt0 = General.FncArrondir(tGrpPrt[i].dbTotAvt0 + Convert.ToDouble(drgpf[cGrpTotalHT]), 2);
                        tGrpPrt[i].dbNbAvt0 = tGrpPrt[i].dbNbAvt0 + Convert.ToDouble(drgpf[cGrpNbCont]);
                    }
                    else
                    {
                        //== cumul tous les contrats avec avenant 0.
                        tGrpPrt[i].dbTotAvAutre = General.FncArrondir(tGrpPrt[i].dbTotAvAutre + Convert.ToDouble(drgpf[cGrpTotalHT]), 2);
                        tGrpPrt[i].dbNbAvtAutre = tGrpPrt[i].dbNbAvtAutre + Convert.ToDouble(drgpf[cGrpNbCont]);
                    }
                    tGrpPrt[i].dbTotHT = General.FncArrondir(tGrpPrt[i].dbTotHT + Convert.ToDouble(drgpf[cGrpTotalHT]), 2);
                    tGrpPrt[i].dbTotNb = tGrpPrt[i].dbTotNb + Convert.ToDouble(drgpf[cGrpNbCont]);

                    tGrpPrt[i].dbTotHTP1 = General.FncArrondir(tGrpPrt[i].dbTotHTP1 + Convert.ToDouble(General.nz(drgpf[cGrpTotalHTp1], 0)), 2);
                    tGrpPrt[i].dbTotHTP2 = General.FncArrondir(tGrpPrt[i].dbTotHTP2 + Convert.ToDouble(General.nz(drgpf[cGrpTotalHTp2], 0)), 2);
                    tGrpPrt[i].dbTotHTP3 = General.FncArrondir(tGrpPrt[i].dbTotHTP3 + Convert.ToDouble(General.nz(drgpf[cGrpTotalHTp3], 0)), 2);
                    tGrpPrt[i].dbTotHTP4 = General.FncArrondir(tGrpPrt[i].dbTotHTP4 + Convert.ToDouble(General.nz(drgpf[cGrpTotalHTp4], 0)), 2);


                }
                DataTable dt = new DataTable();
                dt.Columns.Add("DateArret");
                dt.Columns.Add("Matricule");
                dt.Columns.Add("Nom");
                dt.Columns.Add("Nbre de contrat");
                dt.Columns.Add("Total HT");
                dt.Columns.Add("Nb Avenant");
                dt.Columns.Add("Total Avenant");
                dt.Columns.Add("HTP1");
                dt.Columns.Add("HTP2");
                dt.Columns.Add("HTP3");
                dt.Columns.Add("HTP4");
                for (i = 0; i < tGrpPrt.Length; i++)
                {

                    DataRow dr = dt.NewRow();

                    dr["DateArret"] = tGrpPrt[i].dtDateArret;
                    dr["Matricule"] = tGrpPrt[i].sMat;
                    dr["Nom"] = tGrpPrt[i].sNom;
                    dr["Nbre de contrat"] = tGrpPrt[i].dbNbAvt0;
                    dr["Total HT"] = tGrpPrt[i].dbTotAvt0;
                    dr["Nb Avenant"] = tGrpPrt[i].dbNbAvtAutre;
                    dr["Total Avenant"] = tGrpPrt[i].dbTotAvAutre;
                    dr["HTP1"] = tGrpPrt[i].dbTotHTP1;
                    dr["HTP2"] = tGrpPrt[i].dbTotHTP2;
                    dr["HTP3"] = tGrpPrt[i].dbTotHTP3;
                    dr["HTP4"] = tGrpPrt[i].dbTotHTP4;

                    dt.Rows.Add(dr);
                }
                ssGrpPort.DataSource = dt;
            }



        }
        private void fc_GrpCommercial2903(ref System.DateTime dtarret)
        {

            sSQL = "SELECT POC_DateArret AS " + cGrpArret + "," + " POC_ComInitial AS "
                + cGrpInit + "," + " POC_ComMat AS " + cGrpMat + "," + " POC_ComPrenom AS "
                + cGrpPrenom + "," + " POC_ComNom AS " + cGrpNom + ", " + " SUM(POC_BaseContrat) AS "
                + cGrpTotalHT + "," + " COUNT(POC_NoAuto) AS [" + cGrpNbCont + "]" + " From POC_PorteFeuilleCtr"
                + " GROUP BY POC_DateArret ," + "POC_ComInitial," + " POC_ComMat ," + " POC_ComPrenom,"
                + "POC_ComNom" + " HAVING  POC_DateArret ='" + dtarret + "'" + " ORDER BY POC_ComMat";


            rsGrpPrtFeuille = new DataTable();
            rsGrpPrtFeuille = ModAdorsGrpPrtFeuille.fc_OpenRecordSet(sSQL);
            ssGrpPort.DataSource = rsGrpPrtFeuille;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_commercial(System.DateTime dtarret)
        {
            sSQL = "SELECT  POC_NoAuto, POC_DateArret as [" + cDateArret + "]," + " NumContrat as ["
                + cNumContrat + "]," + " Avenant as [" + CAvenant + "]," + " CodeImmeuble as ["
                + cCodeImmeuble + "]," + " Code1 as [" + cCode1 + "]," + " CodeArticle as ["
                + cCodeArticle + "]," + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_ComMat as [" + cMatricule + "],"
                + " POC_ComNom as [" + cNom + "], " + " POC_BaseContrat AS " + cGrpTotalHT + ""
                + " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4], POC_COT2_Code  as [type de contrat]" + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='" + dtarret + "'" + " order by  POC_ComMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);
            ssPortefeuille.DataSource = rsPorteFeuille;
            ssPortefeuille.Refresh();
        }
        private void fc_commercial2903(ref System.DateTime dtarret)
        {


            sSQL = "SELECT     POC_DateArret as [" + cDateArret + "],"
                + " NumContrat as [" + cNumContrat + "]," + " Avenant as [" + CAvenant + "],"
                + " CodeImmeuble as [" + cCodeImmeuble + "]," + " Code1 as [" + cCode1 + "],"
                + " CodeArticle as [" + cCodeArticle + "]," + " POC_Designation as [" + cDesignation + "],"
                + " POC_Ca_Num as [" + cCA_Num + "]," + " POC_ComMat as [" + cMatricule + "],"
                + " POC_ComInitial as [" + cInitial + "]," + " POC_ComPrenom as [" + cPrenom + "],"
                + " POC_ComNom as [" + cNom + "]" + " From POC_PorteFeuilleCtr" + " WHERE  POC_DateArret ='"
                + dtarret + "'" + " order by POC_ComMat";

            rsPorteFeuille = ModAdorsPorteFeuille.fc_OpenRecordSet(sSQL);

            ssPortefeuille.Refresh();


        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadTreev()
        {

            string sSQL = null;
            DataTable rs = default(DataTable);

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //==reinitialise le treview.
                TreeArret.Nodes.Clear();
                TreeArret.Nodes.Add(cNiv1, cLibArret);

                sSQL = "SELECT     DISTINCT POC_DateArret " + " From POC_PorteFeuilleCtr "
                    + " ORDER BY POC_DateArret DESC";

                rs = ModAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    Cursor.Current = Cursors.Arrow;
                    return;
                }


                for (int i = 0; i < rs.Rows.Count; i++)
                {
                    var datearretDB = rs.Rows[i]["POC_DateArret"];

                    if (!string.IsNullOrEmpty(General.nz(datearretDB, "").ToString()) && General.IsDate(datearretDB))
                    {
                        var datearret = Convert.ToDateTime(datearretDB).ToString("dd/MM/yyyy");

                        //==Ajout des dates d'arrêt.
                        if (TreeArret.GetNodeByKey(cNiv2 + datearret) == null)
                            TreeArret.Nodes[0].Nodes.Add(cNiv2 + datearret, datearret);
                    }
                }

                TreeArret.Nodes[0].Selected = true;
                TreeArret.ActiveNode = TreeArret.Nodes[0];
                TreeArret.Nodes[cNiv1].Expanded = true;

                TreeArret.Nodes[0].Nodes.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                TreeArret.Nodes[0].Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                Cursor.Current = Cursors.Arrow;

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_loadTreePer");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        /// <param name="sNameMat"></param>
        private void fc_CommercialExport(System.DateTime dtarret, string sNameMat)
        {
            double dbNbCont = 0;
            string sMatricule = null;
            DataTable rs = default(DataTable);
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);
            DataTable oField = default(DataTable);

            int i = 0;
            int j = 0;
            int lLigne = 0;
            int v = 0;
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                var _with12 = rsPorteFeuille;

                if (rsPorteFeuille.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                FloodDisplay1(rsPorteFeuille.Rows.Count, "Export Détaillé du portefeuille contrat, veuillez patienter .../");

                // Start Excel and get Application object.
                //oXL = Interaction.CreateObject("Excel.Application");
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oFieldd in rsPorteFeuille.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oFieldd.ColumnName;
                    i = i + 1;
                }

                Cursor.Current = Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sMatricule = General.UCase(rsPorteFeuille.Rows[0][sNameMat].ToString()) + "";

                FloodDisplay2(rsPorteFeuille.Rows.Count);

                foreach (DataRow dr in rsPorteFeuille.Rows)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    if (General.UCase(dr[sNameMat].ToString()) + "" != General.UCase(sMatricule))
                    {
                        lLigne = lLigne + 1;
                        oSheet.Cells[lLigne, j + cCol] = "TOTAL";
                        //' oSheet.Cells(lLigne, j + cCol + 1).value = oField.value
                        sMatricule = General.UCase(dr[sNameMat] + "");
                    }

                    foreach (DataColumn oFielddd in rsPorteFeuille.Columns)
                    {
                        oSheet.Cells[lLigne, j + cCol] = dr[oFielddd];
                        j = j + 1;
                    }

                    lLigne = lLigne + 1;
                    v++;
                    fmsg = "Export Détaillé du portefeuille contrat, veuillez patienter ..." + v + " / " + (rsPorteFeuille.Rows.Count == 0 ? 1 : rsPorteFeuille.Rows.Count);

                    FloodUpdateText(v, fmsg);

                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                FloodHide();

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                rsPorteFeuille.Clear();
                rs = null;

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_CommercialExport");
                FloodHide();
            }
        }

        //Todo[remplacer par progressBar]
        private void FloodUpdateText(int upperLimit, string floodMessage)
        {

            panel1.Visible = true;
            tbFlood.Visible = true;
            tbFlood.Value = upperLimit;
            tbFlood.PerformStep();
            label33.Text = floodMessage;
            label33.BackColor = Color.Transparent;
            //    int r = 0;
            //    if (progress <= Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsY(f.ClientRectangle.Height))
            //    if (progress <= f.ClientRectangle.Height)
            //        {
            //        if (progress > Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsY(f.ClientRectangle.Height))
            //            progress = Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsY(f.ClientRectangle.Height);
            //        f.Cls();
            //        f.CurrentX = 2;
            //        f.CurrentY = (Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsY(f.ClientRectangle.Height) - f.TextHeight(fmsg)) / 2;
            //        f.Print(fmsg);
            //        f.Line(0, 0) - (progress, VB6.PixelsToTwipsY(f.ClientRectangle.Height)), System.Drawing.ColorTranslator.ToOle(f.ForeColor), BF
            //        Application.DoEvents();
            //    }

        }

        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPorteFeuilleContrat_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            fc_LoadTreev();
            optCommercial.Checked = true;
            //== applique les droits par fiche.
            ModAutorisation.fc_DroitParFiche(this.Name);
            //    lblNavigation(0).Caption = sNomLien0
            //    lblNavigation(1).Caption = sNomLien1
            //    lblNavigation(2).Caption = sNomLien2

            if (General.sCalculPorteFeuilleCtrV2 == "1")//Tested
            {
                ssGrpPort.Visible = false;
                ssGrpPortV2.Visible = true;
            }
            else
            {
                ssGrpPort.Visible = true;
                ssGrpPortV2.Visible = false;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportComplet_Click(object sender, EventArgs e)
        {
            UltraTreeNode oNode = new UltraTreeNode(); /*= TreeArret.ActiveNode*/
            //MSComctlLib.Node oNode;
            bool bFind = false;
            tbFlood.Value = 0;
            tbFlood.PerformStep();
            try
            {
                foreach (UltraTreeNode oNodee in TreeArret.Nodes[0].Nodes)
                {
                    if (oNodee.Selected == true)
                    {
                        oNode = oNodee;
                        bFind = true;
                        break;
                    }
                }

                if (bFind == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez sélectionner une date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous expoter sous Excel le portefeuille contrat pour la date d'arrêté du " + oNode.Text + "?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    return;

                if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv2))
                    fc_DetailExportCompletV2(Convert.ToDateTime(General.Right(oNode.Key, General.Len(oNode.Key) - 1)));

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex + "Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtarret"></param>
        private void fc_DetailExportCompletV2(DateTime dtarret)
        {
            double dbNbCont;
            string sMatricule;
            DataTable rs;
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);

            DataTable rsExport;
            DataColumn oField;

            long i;
            long j;
            int lLigne;
            DataTable rsComplet;
            string sSQL;
            try
            {
                //  Screen.MousePointer = 11;
                /*

               '    sSQL = "SELECT  POC_DateArret as [" & cDateArret & "]," _
                       & " NumContrat as [" & cNumContrat & "]," _
                       & " Avenant as [" & CAvenant & "]," _
                       & " CodeImmeuble as [" & cCodeImmeuble & "]," _
                       & " Code1 as [" & cCode1 & "]," _
                       & " CodeArticle as [" & cCodeArticle & "]," _
                       & " POC_Designation as [" & cDesignation & "]," _
                       & " POC_IntervMat as [" & cMatricule & "]," _
                       & " POC_IntervPrenom as [" & cPrenom & "]," _
                       & " POC_IntervNom as [" & cNom & "]," _
                       & " POC_BaseContrat AS " & cGrpTotalHT & "" _
                        & " ,POC_HtP1 AS [Ht P1], POC_HtP2 AS [Ht P2], POC_HtP3 AS [Ht P3], POC_HtP4 AS [Ht P4] " _


                 sSQL = sSQL & " ,  POC_HtP2avt AS [Ht P2 avt], POC_HtP2_P2avt AS [Ht P2C + P2 avt]," _
                   & " POC_RexpMat AS [responsable exploit], POC_RexpPrenom AS [Prenom Exploit], POC_RexpNom AS [Nom Exploit], " _
                   & " POC_ComMat AS Commercial, POC_ComPrenom AS [Prenom com], POC_ComNom AS [Nom Com], " _
                   & " POC_CdsMat As [Chef de secteur], POC_CdsPrenom As [Prenom secteur], POC_CdsNom As [Nom secteur], " _
                   & " POC_COT2_Code As [Type prestation]"

                */
                sSQL = "SELECT  POC_PorteFeuilleCtr.POC_DateArret AS DateArret, POC_PorteFeuilleCtr.NumContrat AS [N°Contrat], POC_PorteFeuilleCtr.Avenant, "
                       + " POC_PorteFeuilleCtr.CodeImmeuble AS [Code Immeuble], Immeuble.Adresse, Immeuble.Adresse2_Imm, Immeuble.AngleRue, Immeuble.CodePostal, "
                       + " Immeuble.Ville, POC_PorteFeuilleCtr.Code1 AS Gerant, POC_PorteFeuilleCtr.CodeArticle AS [Code Article], "
                       + " POC_PorteFeuilleCtr.POC_Designation AS Libelle, POC_PorteFeuilleCtr.POC_IntervMat AS Matricule, "
                       + " POC_PorteFeuilleCtr.POC_IntervPrenom AS Prenom, POC_PorteFeuilleCtr.POC_IntervNom AS Nom, POC_PorteFeuilleCtr.POC_BaseContrat AS TotalHT, "
                       + " POC_PorteFeuilleCtr.POC_HtP1 AS [Ht P1], POC_PorteFeuilleCtr.POC_HtP2 AS [Ht P2], POC_PorteFeuilleCtr.POC_HtP3 AS [Ht P3], "
                       + " POC_PorteFeuilleCtr.POC_HtP4 AS [Ht P4], POC_PorteFeuilleCtr.POC_HtP2avt AS [Ht P2 avt], "
                       + " POC_PorteFeuilleCtr.POC_HtP2_P2avt AS [Ht P2C + P2 avt], POC_PorteFeuilleCtr.POC_RexpMat AS [responsable exploit], "
                       + " POC_PorteFeuilleCtr.POC_RexpPrenom AS [Prenom Exploit], POC_PorteFeuilleCtr.POC_RexpNom AS [Nom Exploit], "
                       + " POC_PorteFeuilleCtr.POC_ComMat AS Commercial, POC_PorteFeuilleCtr.POC_ComPrenom AS [Prenom com], "
                       + " POC_PorteFeuilleCtr.POC_ComNom AS [Nom Com], POC_PorteFeuilleCtr.POC_CdsMat AS [Chef de secteur], "
                       + " POC_PorteFeuilleCtr.POC_CdsPrenom AS [Prenom secteur], POC_PorteFeuilleCtr.POC_CdsNom AS [Nom secteur], "
                       + " POC_PorteFeuilleCtr.POC_COT2_Code AS [Type prestation] ";


                sSQL = sSQL + "  FROM         POC_PorteFeuilleCtr INNER JOIN "
                            + " Immeuble ON POC_PorteFeuilleCtr.CodeImmeuble = Immeuble.CodeImmeuble ";


                sSQL = sSQL + " "
                      + " WHERE  POC_DateArret ='" + dtarret + "'";

                rsComplet = ModAdorsComplet.fc_OpenRecordSet(sSQL);


                //var withBlock = rsComplet;
                if (rsComplet.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour cette date d'arrêté.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //Todo chaime a laissé ce todo sans commentaire ..
                //elle a remplacé FloodDisplay par un Ultraprocessbar

                FloodDisplay1(rsComplet.Rows.Count, "Export Détaillé du portefeuille contrat, veuillez patienter .../");
                int v = 0;

                // Start Excel and get Application object.
                //  Set oXL = CreateObject("Excel.Application")
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                //Set oSheet = oWB.ActiveSheet

                i = 1;
                // == affiche les noms des champs.
                foreach (DataColumn oFieldd in rsComplet.Columns)
                {
                    oField = oFieldd;
                    // If (UCase(oField.Name) <> UCase("Ht P1")) And (UCase(oField.Name) <> UCase("Ht P2")) _
                    // And (UCase(oField.Name) <> UCase("Ht P3")) And (UCase(oField.Name) <> UCase("Ht P4")) _
                    // And (UCase(oField.Name) <> UCase("activite")) Then
                    if ((General.UCase(oField.ColumnName) != General.UCase("activite")))
                    {
                        // If UCase(oField.Name) <> UCase("POC_Ca_Num") Then

                        oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                        oSheet.Cells[1, i].value = oField.ColumnName;
                        i = i + 1;
                    }
                }

                //Screen.MousePointer = 11;

                //' Starting at E1, fill headers for the number of columns selected.
                // Set oResizeRange = oSheet.Range("A1", "B1").Resize(ColumnSize:=i - 1)

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                // formate la ligne en gras.
                oResizeRange.Font.Bold = true;


                //Set oResizeRange = oSheet.Range("A1", "B1").Resize(ColumnSize:=i - 1)
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 2;

                //Screen.MousePointer = 11;

                FloodDisplay2(rsComplet.Rows.Count);

                foreach (DataRow dr in rsComplet.Rows)
                {
                    //Screen.MousePointer = 11;
                    j = 1;
                    // oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    // If UCase(rsPorteFeuille.Fields(sNameMat).value) & "" <> UCase(sMatricule) Then

                    // lLigne = lLigne + 1
                    // oSheet.Cells(lLigne, j + cCol).value = "TOTAL"

                    // ' oSheet.Cells(lLigne, j + cCol + 1).value = oField.value
                    // sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""
                    // End If

                    foreach (DataColumn oFieldd in rsComplet.Columns)
                    {
                        // If (UCase(oField.Name) <> UCase("Ht P1")) And (UCase(oField.Name) <> UCase("Ht P2")) _
                        // And (UCase(oField.Name) <> UCase("Ht P3")) And (UCase(oField.Name) <> UCase("Ht P4")) _
                        // And (UCase(oField.Name) <> UCase("activite")) Then
                        if ((General.UCase(oFieldd.ColumnName) != General.UCase(cCA_Num)))
                        {
                            oSheet.Cells[lLigne, j + cCol].value = dr[oFieldd.ColumnName];

                        }
                        j = j + 1;
                    }


                    lLigne = lLigne + 1;
                    v++;
                    fmsg = "Export Détaillé du portefeuille contrat, veuillez patienter ..." + v + " / " + rsComplet.Rows.Count;
                    FloodUpdateText(v, fmsg);

                }

                /// oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                // formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                FloodHide();

                oXL.Visible = true;
                oXL.UserControl = true;

                //Set oRng = Nothing

                //Screen.MousePointer = 1;
                Cursor.Current = Cursors.WaitCursor;
                rsComplet.Clear();

            }
            catch (Exception err)
            {
                Program.SaveException(err);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: " + err);
                FloodHide();

            }
        }

        private void ssGrpPortV2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DateArret"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime;
            e.Layout.Bands[0].Columns["DateArret"].Format = "dd/MM/yyyy";

            e.Layout.Bands[0].Columns["DateArret"].Header.Caption = "Date Arret";

            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[0].CellActivation = Activation.NoEdit;
            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[1].CellActivation = Activation.NoEdit;
            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[2].CellActivation = Activation.NoEdit;
            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[3].CellActivation = Activation.NoEdit;
            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[8].Header.Caption = "HTP2C+P2avt";
            this.ssGrpPortV2.DisplayLayout.Bands[0].Columns[8].CellAppearance.BackColor = Color.LightGray;


        }

        private void ssGrpPort_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DateArret"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.DateTime;
            e.Layout.Bands[0].Columns["DateArret"].Format = "dd/MM/yyyy";

            e.Layout.Bands[0].Columns["DateArret"].Header.Caption = "Date Arret";


        }

        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="upperLimit"></param>
        /// <param name="floodMessage"></param>
        private void FloodDisplay1(int upperLimit, string floodMessage)
        {
            //Remplacer le tpFlood par le progressBar
            //pour la premiere fois on affiche le pannel et le label 
            label33.Text = floodMessage + upperLimit;
            label33.Visible = true;
            panel1.Visible = true;
            tbFlood.Maximum = 0;
            //var f = tbFlood;
            //f.BackColor = ColorTranslator.FromOle(0xffffff);
            //f.ForeColor = ColorTranslator.FromOle(0x0);

            ////f.DrawMode = 10;
            ////f.FillStyle = 0;
            ////f.ScaleHeight = upperLimit;
            ////f.Cls();

            //f.Visible = true;

            //fmsg = floodMessage;

            //Me.cmdArret.Visible = True

            //Application.DoEvents();
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="upperLimit"></param>
        private void FloodDisplay2(int upperLimit)
        {
            //afficher le progressBar 
            tbFlood.Maximum = upperLimit;
            this.tbFlood.Visible = true;
            label33.Visible = true;
        }
        /// <summary>
        /// TEsted
        /// </summary>
        private void FloodHide()
        {
            //rendre le pannel qui contient le progressBar(tbFlood) invisbile
            panel1.Visible = false;
            label33.Text = "";

            //if ((f != null))
            //{
            //    f.Visible = false;
            //    //Todo
            //    //f.Cls();
            //}
            //this.tbFloodd.Visible = false;
            //// Me.cmdArret.Visible = False
            //f.Image = null;

            //Application.DoEvents();
        }

        /// <summary>
        /// Mondir le 15.06.2021, demande de Marion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonSaveAnalytique_Click(object sender, EventArgs e)
        {
            if (ssGrpPortV2.Rows.Count == 0 || TreeArret.ActiveNode == null)
            {
                CustomMessageBox.Show("La grille est vide, Veuillez choisir une date d'arrêté", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            var type = -1;
            if (optCommercial.Checked)
            {
                type = 1;
            }
            else if (optChefDeSecteur.Checked)
            {
                type = 2;
            }
            else if (optRespExploit.Checked)
            {
                type = 3;
            }
            else if (OptIntervenant.Checked)
            {
                type = 4;
            }

            var dateArrete = TreeArret.ActiveNode.Text;

            General.Execute($"DELETE FROM POC_PorteFeuilleCtrSaveAnalytique WHERE DateArrete = '{dateArrete}' AND Type = {type}");

            var tmpModAdo = new ModAdo();
            var dt = tmpModAdo.fc_OpenRecordSet($"SELECT * FROM POC_PorteFeuilleCtrSaveAnalytique WHERE DateArrete = '{dateArrete}'");

            var dataSource = ssGrpPortV2.DataSource as DataTable;

            foreach (DataRow row in dataSource.Rows)
            {
                var newRow = dt.NewRow();

                newRow["Type"] = type;
                newRow["DateArrete"] = dateArrete;
                newRow["Matricule"] = row["Matricule"];
                newRow["Nom"] = row["Nom"];
                newRow["NbrImmeuble"] = row["NbrImmeuble"];
                newRow["NbP1"] = row["NbP1"];
                newRow["NbP2C"] = row["NbP2C"];
                newRow["NbP2avt"] = row["NbP2avt"];
                newRow["NbP3"] = row["NbP3"];
                newRow["NbP4"] = row["NbP4"];
                newRow["HTP2_P2avt"] = row["HTP2_P2avt"];
                newRow["HTP1"] = row["HTP1"];
                newRow["HTP2C"] = row["HTP2C"];
                newRow["HTP2avt"] = row["HTP2avt"];
                newRow["HTP3"] = row["HTP3"];
                newRow["HTP4"] = row["HTP4"];

                dt.Rows.Add(newRow);
            }

            tmpModAdo.Update();

            CustomMessageBox.Show("Données enregistrées", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
    }
}

