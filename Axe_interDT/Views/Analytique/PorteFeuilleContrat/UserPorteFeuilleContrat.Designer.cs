namespace Axe_interDT.Views.PorteFeuilleContrat
{
    partial class UserPorteFeuilleContrat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserPorteFeuilleContrat));
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.OptIntervenant = new System.Windows.Forms.RadioButton();
            this.optRespExploit = new System.Windows.Forms.RadioButton();
            this.optChefDeSecteur = new System.Windows.Forms.RadioButton();
            this.optCommercial = new System.Windows.Forms.RadioButton();
            this.TreeArret = new Infragistics.Win.UltraWinTree.UltraTree();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblPar = new System.Windows.Forms.Label();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.FraArret = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdPorteFeuille = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.txttemp = new iTalk.iTalk_TextBox_Small2();
            this.txtDateArrete = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.ssGrpPort = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssGrpPortV2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssPortefeuille = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdSup = new System.Windows.Forms.Button();
            this.cmdvisu = new System.Windows.Forms.Button();
            this.cmdDetailExcel = new System.Windows.Forms.Button();
            this.cmdExportComplet = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label33 = new System.Windows.Forms.Label();
            this.tbFlood = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.buttonSaveAnalytique = new System.Windows.Forms.Button();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeArret)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.FraArret.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGrpPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGrpPortV2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssPortefeuille)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel1);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame1.Location = new System.Drawing.Point(3, 48);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1844, 906);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.46114F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.53886F));
            this.tableLayoutPanel1.Controls.Add(this.Frame2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.TreeArret, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1838, 883);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.Frame2.Controls.Add(this.OptIntervenant);
            this.Frame2.Controls.Add(this.optRespExploit);
            this.Frame2.Controls.Add(this.optChefDeSecteur);
            this.Frame2.Controls.Add(this.optCommercial);
            this.Frame2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame2.Location = new System.Drawing.Point(504, 0);
            this.Frame2.Margin = new System.Windows.Forms.Padding(0);
            this.Frame2.Name = "Frame2";
            this.Frame2.Padding = new System.Windows.Forms.Padding(0);
            this.Frame2.Size = new System.Drawing.Size(1334, 48);
            this.Frame2.TabIndex = 50;
            this.Frame2.TabStop = false;
            // 
            // OptIntervenant
            // 
            this.OptIntervenant.AutoSize = true;
            this.OptIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptIntervenant.Location = new System.Drawing.Point(459, 9);
            this.OptIntervenant.Name = "OptIntervenant";
            this.OptIntervenant.Size = new System.Drawing.Size(108, 23);
            this.OptIntervenant.TabIndex = 3;
            this.OptIntervenant.Text = "Intervenant";
            this.OptIntervenant.UseVisualStyleBackColor = true;
            this.OptIntervenant.CheckedChanged += new System.EventHandler(this.OptIntervenant_CheckedChanged);
            // 
            // optRespExploit
            // 
            this.optRespExploit.AutoSize = true;
            this.optRespExploit.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optRespExploit.Location = new System.Drawing.Point(248, 9);
            this.optRespExploit.Name = "optRespExploit";
            this.optRespExploit.Size = new System.Drawing.Size(214, 23);
            this.optRespExploit.TabIndex = 2;
            this.optRespExploit.Text = "Responsable d\'exploitation";
            this.optRespExploit.UseVisualStyleBackColor = true;
            this.optRespExploit.CheckedChanged += new System.EventHandler(this.optRespExploit_CheckedChanged);
            // 
            // optChefDeSecteur
            // 
            this.optChefDeSecteur.AutoSize = true;
            this.optChefDeSecteur.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optChefDeSecteur.Location = new System.Drawing.Point(112, 9);
            this.optChefDeSecteur.Name = "optChefDeSecteur";
            this.optChefDeSecteur.Size = new System.Drawing.Size(139, 23);
            this.optChefDeSecteur.TabIndex = 1;
            this.optChefDeSecteur.Text = "Chef de secteur";
            this.optChefDeSecteur.UseVisualStyleBackColor = true;
            this.optChefDeSecteur.CheckedChanged += new System.EventHandler(this.optChefDeSecteur_CheckedChanged);
            // 
            // optCommercial
            // 
            this.optCommercial.AutoSize = true;
            this.optCommercial.Checked = true;
            this.optCommercial.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optCommercial.Location = new System.Drawing.Point(0, 9);
            this.optCommercial.Name = "optCommercial";
            this.optCommercial.Size = new System.Drawing.Size(110, 23);
            this.optCommercial.TabIndex = 0;
            this.optCommercial.TabStop = true;
            this.optCommercial.Text = "Commercial";
            this.optCommercial.UseVisualStyleBackColor = true;
            this.optCommercial.CheckedChanged += new System.EventHandler(this.optCommercial_CheckedChanged);
            // 
            // TreeArret
            // 
            this.TreeArret.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeArret.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeArret.Location = new System.Drawing.Point(3, 51);
            this.TreeArret.Name = "TreeArret";
            this.TreeArret.Size = new System.Drawing.Size(498, 829);
            this.TreeArret.TabIndex = 0;
            this.TreeArret.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeArret_AfterSelect);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.lblPar, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(507, 51);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 5.335968F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 94.66403F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1328, 829);
            this.tableLayoutPanel2.TabIndex = 590;
            // 
            // lblPar
            // 
            this.lblPar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(204)))));
            this.lblPar.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblPar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblPar.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPar.ForeColor = System.Drawing.Color.Black;
            this.lblPar.Location = new System.Drawing.Point(3, 0);
            this.lblPar.Name = "lblPar";
            this.lblPar.Size = new System.Drawing.Size(1322, 44);
            this.lblPar.TabIndex = 395;
            this.lblPar.Text = " ";
            this.lblPar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox1.Controls.Add(this.FraArret);
            this.ultraGroupBox1.Controls.Add(this.ssGrpPort);
            this.ultraGroupBox1.Controls.Add(this.ssGrpPortV2);
            this.ultraGroupBox1.Controls.Add(this.ssPortefeuille);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 47);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1322, 779);
            this.ultraGroupBox1.TabIndex = 596;
            // 
            // FraArret
            // 
            this.FraArret.BackColor = System.Drawing.Color.Transparent;
            this.FraArret.Controls.Add(this.label1);
            this.FraArret.Controls.Add(this.cmdPorteFeuille);
            this.FraArret.Controls.Add(this.Command1);
            this.FraArret.Controls.Add(this.txttemp);
            this.FraArret.Controls.Add(this.txtDateArrete);
            this.FraArret.Controls.Add(this.label3);
            this.FraArret.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FraArret.Location = new System.Drawing.Point(81, 195);
            this.FraArret.Name = "FraArret";
            this.FraArret.Size = new System.Drawing.Size(347, 142);
            this.FraArret.TabIndex = 40;
            this.FraArret.TabStop = false;
            this.FraArret.Visible = false;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(1, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(346, 32);
            this.label1.TabIndex = 395;
            this.label1.Text = "    Portefeuille Contrat";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdPorteFeuille
            // 
            this.cmdPorteFeuille.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdPorteFeuille.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdPorteFeuille.FlatAppearance.BorderSize = 0;
            this.cmdPorteFeuille.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPorteFeuille.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdPorteFeuille.ForeColor = System.Drawing.Color.White;
            this.cmdPorteFeuille.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdPorteFeuille.Location = new System.Drawing.Point(249, 102);
            this.cmdPorteFeuille.Margin = new System.Windows.Forms.Padding(2);
            this.cmdPorteFeuille.Name = "cmdPorteFeuille";
            this.cmdPorteFeuille.Size = new System.Drawing.Size(93, 35);
            this.cmdPorteFeuille.TabIndex = 5;
            this.cmdPorteFeuille.Text = "Calcul";
            this.cmdPorteFeuille.UseVisualStyleBackColor = false;
            this.cmdPorteFeuille.Click += new System.EventHandler(this.cmdPorteFeuille_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(152, 102);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(93, 35);
            this.Command1.TabIndex = 4;
            this.Command1.Text = "Annuler";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // txttemp
            // 
            this.txttemp.AccAcceptNumbersOnly = false;
            this.txttemp.AccAllowComma = false;
            this.txttemp.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txttemp.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txttemp.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txttemp.AccHidenValue = "";
            this.txttemp.AccNotAllowedChars = null;
            this.txttemp.AccReadOnly = false;
            this.txttemp.AccReadOnlyAllowDelete = false;
            this.txttemp.AccRequired = false;
            this.txttemp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txttemp.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txttemp.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txttemp.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txttemp.Location = new System.Drawing.Point(197, 70);
            this.txttemp.Margin = new System.Windows.Forms.Padding(2);
            this.txttemp.MaxLength = 32767;
            this.txttemp.Multiline = false;
            this.txttemp.Name = "txttemp";
            this.txttemp.ReadOnly = false;
            this.txttemp.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txttemp.Size = new System.Drawing.Size(145, 27);
            this.txttemp.TabIndex = 3;
            this.txttemp.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txttemp.UseSystemPasswordChar = false;
            this.txttemp.Visible = false;
            // 
            // txtDateArrete
            // 
            this.txtDateArrete.AccAcceptNumbersOnly = false;
            this.txtDateArrete.AccAllowComma = false;
            this.txtDateArrete.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateArrete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateArrete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateArrete.AccHidenValue = "";
            this.txtDateArrete.AccNotAllowedChars = null;
            this.txtDateArrete.AccReadOnly = false;
            this.txtDateArrete.AccReadOnlyAllowDelete = false;
            this.txtDateArrete.AccRequired = false;
            this.txtDateArrete.BackColor = System.Drawing.Color.White;
            this.txtDateArrete.CustomBackColor = System.Drawing.Color.White;
            this.txtDateArrete.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateArrete.ForeColor = System.Drawing.Color.Black;
            this.txtDateArrete.Location = new System.Drawing.Point(9, 73);
            this.txtDateArrete.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateArrete.MaxLength = 32767;
            this.txtDateArrete.Multiline = false;
            this.txtDateArrete.Name = "txtDateArrete";
            this.txtDateArrete.ReadOnly = false;
            this.txtDateArrete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateArrete.Size = new System.Drawing.Size(155, 27);
            this.txtDateArrete.TabIndex = 2;
            this.txtDateArrete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateArrete.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 36);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(224, 19);
            this.label3.TabIndex = 1;
            this.label3.Text = "Veuillez saisir une date d\'arrêté";
            // 
            // ssGrpPort
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGrpPort.DisplayLayout.Appearance = appearance1;
            this.ssGrpPort.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGrpPort.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGrpPort.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGrpPort.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGrpPort.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGrpPort.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGrpPort.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGrpPort.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGrpPort.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGrpPort.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGrpPort.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGrpPort.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGrpPort.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGrpPort.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGrpPort.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGrpPort.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGrpPort.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGrpPort.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGrpPort.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGrpPort.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGrpPort.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGrpPort.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGrpPort.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGrpPort.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGrpPort.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGrpPort.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGrpPort.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGrpPort.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGrpPort.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGrpPort.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGrpPort.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssGrpPort.Location = new System.Drawing.Point(0, 0);
            this.ssGrpPort.Name = "ssGrpPort";
            this.ssGrpPort.Size = new System.Drawing.Size(1322, 779);
            this.ssGrpPort.TabIndex = 0;
            this.ssGrpPort.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGrpPort_InitializeLayout);
            // 
            // ssGrpPortV2
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGrpPortV2.DisplayLayout.Appearance = appearance13;
            this.ssGrpPortV2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGrpPortV2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGrpPortV2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGrpPortV2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGrpPortV2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGrpPortV2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGrpPortV2.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGrpPortV2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGrpPortV2.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGrpPortV2.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGrpPortV2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGrpPortV2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGrpPortV2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGrpPortV2.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGrpPortV2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGrpPortV2.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGrpPortV2.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGrpPortV2.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGrpPortV2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGrpPortV2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGrpPortV2.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGrpPortV2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGrpPortV2.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGrpPortV2.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGrpPortV2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGrpPortV2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGrpPortV2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGrpPortV2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssGrpPortV2.Location = new System.Drawing.Point(0, 0);
            this.ssGrpPortV2.Name = "ssGrpPortV2";
            this.ssGrpPortV2.Size = new System.Drawing.Size(1322, 779);
            this.ssGrpPortV2.TabIndex = 83;
            this.ssGrpPortV2.Text = "ultraGrid1";
            this.ssGrpPortV2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGrpPortV2_InitializeLayout);
            // 
            // ssPortefeuille
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssPortefeuille.DisplayLayout.Appearance = appearance25;
            this.ssPortefeuille.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssPortefeuille.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ssPortefeuille.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssPortefeuille.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ssPortefeuille.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssPortefeuille.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ssPortefeuille.DisplayLayout.MaxColScrollRegions = 1;
            this.ssPortefeuille.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssPortefeuille.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssPortefeuille.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ssPortefeuille.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssPortefeuille.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ssPortefeuille.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssPortefeuille.DisplayLayout.Override.CellAppearance = appearance32;
            this.ssPortefeuille.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssPortefeuille.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ssPortefeuille.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ssPortefeuille.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ssPortefeuille.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssPortefeuille.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ssPortefeuille.DisplayLayout.Override.RowAppearance = appearance35;
            this.ssPortefeuille.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssPortefeuille.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssPortefeuille.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ssPortefeuille.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssPortefeuille.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssPortefeuille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssPortefeuille.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssPortefeuille.Location = new System.Drawing.Point(0, 0);
            this.ssPortefeuille.Name = "ssPortefeuille";
            this.ssPortefeuille.Size = new System.Drawing.Size(1322, 779);
            this.ssPortefeuille.TabIndex = 56;
            this.ssPortefeuille.Text = "ultraGrid1";
            this.ssPortefeuille.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.cmdSup);
            this.flowLayoutPanel1.Controls.Add(this.cmdvisu);
            this.flowLayoutPanel1.Controls.Add(this.cmdDetailExcel);
            this.flowLayoutPanel1.Controls.Add(this.cmdExportComplet);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.buttonSaveAnalytique);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1306, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(541, 39);
            this.flowLayoutPanel1.TabIndex = 587;
            // 
            // cmdSup
            // 
            this.cmdSup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSup.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSup.FlatAppearance.BorderSize = 0;
            this.cmdSup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSup.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSup.Image = ((System.Drawing.Image)(resources.GetObject("cmdSup.Image")));
            this.cmdSup.Location = new System.Drawing.Point(2, 2);
            this.cmdSup.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSup.Name = "cmdSup";
            this.cmdSup.Size = new System.Drawing.Size(60, 35);
            this.cmdSup.TabIndex = 0;
            this.cmdSup.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSup, "Supprimer");
            this.cmdSup.UseVisualStyleBackColor = false;
            this.cmdSup.Click += new System.EventHandler(this.cmdSup_Click);
            // 
            // cmdvisu
            // 
            this.cmdvisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdvisu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdvisu.FlatAppearance.BorderSize = 0;
            this.cmdvisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdvisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdvisu.ForeColor = System.Drawing.Color.White;
            this.cmdvisu.Image = ((System.Drawing.Image)(resources.GetObject("cmdvisu.Image")));
            this.cmdvisu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdvisu.Location = new System.Drawing.Point(66, 2);
            this.cmdvisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdvisu.Name = "cmdvisu";
            this.cmdvisu.Size = new System.Drawing.Size(85, 35);
            this.cmdvisu.TabIndex = 1;
            this.cmdvisu.Text = "       Excel";
            this.toolTip1.SetToolTip(this.cmdvisu, "Aperçu avant Impression");
            this.cmdvisu.UseVisualStyleBackColor = false;
            this.cmdvisu.Click += new System.EventHandler(this.cmdVisu_Click);
            // 
            // cmdDetailExcel
            // 
            this.cmdDetailExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdDetailExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDetailExcel.FlatAppearance.BorderSize = 0;
            this.cmdDetailExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDetailExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDetailExcel.ForeColor = System.Drawing.Color.White;
            this.cmdDetailExcel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDetailExcel.Location = new System.Drawing.Point(155, 2);
            this.cmdDetailExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDetailExcel.Name = "cmdDetailExcel";
            this.cmdDetailExcel.Size = new System.Drawing.Size(93, 35);
            this.cmdDetailExcel.TabIndex = 2;
            this.cmdDetailExcel.Text = "Détail Excel";
            this.toolTip1.SetToolTip(this.cmdDetailExcel, "Aperçu avant Impression");
            this.cmdDetailExcel.UseVisualStyleBackColor = false;
            this.cmdDetailExcel.Click += new System.EventHandler(this.cmdDetailExcel_Click);
            // 
            // cmdExportComplet
            // 
            this.cmdExportComplet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportComplet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportComplet.FlatAppearance.BorderSize = 0;
            this.cmdExportComplet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportComplet.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportComplet.ForeColor = System.Drawing.Color.White;
            this.cmdExportComplet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExportComplet.Location = new System.Drawing.Point(252, 2);
            this.cmdExportComplet.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportComplet.Name = "cmdExportComplet";
            this.cmdExportComplet.Size = new System.Drawing.Size(157, 35);
            this.cmdExportComplet.TabIndex = 3;
            this.cmdExportComplet.Text = "Export détail complet";
            this.toolTip1.SetToolTip(this.cmdExportComplet, "Aperçu avant Impression");
            this.cmdExportComplet.UseVisualStyleBackColor = false;
            this.cmdExportComplet.Click += new System.EventHandler(this.cmdExportComplet_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = ((System.Drawing.Image)(resources.GetObject("cmdAjouter.Image")));
            this.cmdAjouter.Location = new System.Drawing.Point(413, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 4;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label33);
            this.panel1.Controls.Add(this.tbFlood);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1226, 45);
            this.panel1.TabIndex = 588;
            this.panel1.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(2, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(0, 19);
            this.label33.TabIndex = 0;
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // tbFlood
            // 
            appearance38.BackColor = System.Drawing.Color.LawnGreen;
            appearance38.BackColor2 = System.Drawing.Color.White;
            this.tbFlood.Appearance = appearance38;
            this.tbFlood.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tbFlood.Location = new System.Drawing.Point(0, 0);
            this.tbFlood.Margin = new System.Windows.Forms.Padding(0);
            this.tbFlood.Maximum = 0;
            this.tbFlood.Name = "tbFlood";
            this.tbFlood.Size = new System.Drawing.Size(1226, 45);
            this.tbFlood.Step = 1;
            this.tbFlood.TabIndex = 1;
            this.tbFlood.Text = "[Percent]";
            this.tbFlood.TextVisible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 624F));
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1850, 45);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Frame1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // buttonSaveAnalytique
            // 
            this.buttonSaveAnalytique.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveAnalytique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.buttonSaveAnalytique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buttonSaveAnalytique.FlatAppearance.BorderSize = 0;
            this.buttonSaveAnalytique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSaveAnalytique.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonSaveAnalytique.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.buttonSaveAnalytique.Location = new System.Drawing.Point(477, 2);
            this.buttonSaveAnalytique.Margin = new System.Windows.Forms.Padding(2);
            this.buttonSaveAnalytique.Name = "buttonSaveAnalytique";
            this.buttonSaveAnalytique.Size = new System.Drawing.Size(60, 35);
            this.buttonSaveAnalytique.TabIndex = 5;
            this.buttonSaveAnalytique.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.buttonSaveAnalytique, "Ajouter");
            this.buttonSaveAnalytique.UseVisualStyleBackColor = false;
            this.buttonSaveAnalytique.Click += new System.EventHandler(this.buttonSaveAnalytique_Click);
            // 
            // UserPorteFeuilleContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "UserPorteFeuilleContrat";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche Portefeuille Contrat";
            this.VisibleChanged += new System.EventHandler(this.UserPorteFeuilleContrat_VisibleChanged);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeArret)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.FraArret.ResumeLayout(false);
            this.FraArret.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGrpPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGrpPortV2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssPortefeuille)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.GroupBox Frame2;
        private Infragistics.Win.UltraWinTree.UltraTree TreeArret;
        public System.Windows.Forms.RadioButton OptIntervenant;
        public System.Windows.Forms.RadioButton optRespExploit;
        public System.Windows.Forms.RadioButton optChefDeSecteur;
        public System.Windows.Forms.RadioButton optCommercial;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        public System.Windows.Forms.GroupBox FraArret;
        public System.Windows.Forms.Button cmdPorteFeuille;
        public System.Windows.Forms.Button Command1;
        public iTalk.iTalk_TextBox_Small2 txttemp;
        public iTalk.iTalk_TextBox_Small2 txtDateArrete;
        public System.Windows.Forms.Label label3;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGrpPort;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGrpPortV2;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssPortefeuille;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdSup;
        public System.Windows.Forms.Button cmdvisu;
        public System.Windows.Forms.Button cmdExportComplet;
        public System.Windows.Forms.Button cmdAjouter;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label label33;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar tbFlood;
        public System.Windows.Forms.Button cmdDetailExcel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblPar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button buttonSaveAnalytique;
    }
}
