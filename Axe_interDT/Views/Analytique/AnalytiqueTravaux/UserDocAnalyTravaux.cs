﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Gerant.Forms;
using System.Net.NetworkInformation;
using System.IO;
using static Axe_interDT.Shared.Variable;
using System.Data.SqlClient;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.Analytique.AnalytiqueTravaux
{
    public partial class UserDocAnalyTravaux : UserControl
    {
        AnalyTRavaux[] tAnalyTRavaux;
        AnalyTRavauxImm[] tAnalyTRavauxImm;

        string sUser;
        string sAddMAc;

        //Const cCheminFichierErrTrx = "C:\ErreurTravaux.txt"

        bool blnErreur;

        string sError;

        const short cCol = 0;
        private frmSelect frm;

        public UserDocAnalyTravaux()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Travaux()
        {
            try
            {
                string sSQL = null;


                if (fc_CtrlErreurSaisie() == true)
                    return;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                fc_MtTRavaux(Convert.ToDateTime(txtinterDe.Text), Convert.ToDateTime(txtIntereAu.Text));

                fc_resultat();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                //FileSystem.FileClose(1);
                if (blnErreur == true)
                {

                    ModuleAPI.Ouvrir(General.cCheminFichierErrTrx);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Contrat");

                fc_Err(ex.Message);
                if (blnErreur == true)
                {

                    ModuleAPI.Ouvrir(General.cCheminFichierErrTrx);
                }
                //FileSystem.FileClose(1);
            }


            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_resultat()
        {
            string sSQL = null;

            string sWhere = null;
            string sWhere1 = null;
            string sWhere2 = null;
            string sWhere3 = null;

            string sOrderBy = null;
            string sNumFicheStandard = null;
            string sNoIntervention = null;
            string sCodeImmeuble = null;
            string sDureeFormat = null;
            string sNoBonDeCommande = null;
            string[] tsNoInterFacture = null;
            string sAddItem = null;

            DataTable rs = default(DataTable);
            ModAdo modAdoRs = new ModAdo();
            double dbCoef = 0;
            double dbTempTotMO = 0;

            double dbVenteReel = 0;
            double dbTotAchatReel = 0;
            double dbTotMoReel = 0;

            double dbTotVentePrevu = 0;
            double dbtotAchatPrevu = 0;
            double dbTotMoPrevu = 0;

            bool bFirst = false;
            bool bFact = false;
            bool bAffiche = false;

            int i = 0;
            int lTypeCalcul = 0;
            int j = 0;
            int X = 0;

            if (OptDefaut.Checked == true)
            {
                lTypeCalcul = 1;

            }
            else if (OptGrille.Checked == true)
            {
                lTypeCalcul = 2;

            }

            //sSQL = "SELECT     Intervention.NoIntervention, Intervention.Debourse AS MO," _
            //& " Intervention.CodeEtat, BonDeCommande.ProduitTotalHT AS Achats, " _
            //& " FactureManuellePied.MontantHT AS Ventes, FactureManuelleEnTete.TypeFacture," _
            //& " FactureManuelleEnTete.NoFacture, BCD_Detail.BCD_PrixHT," _
            //& " ANT_AnalyTrx.CodeImmeuble, ANT_AnalyTrx.Code1, " _
            //& " ANT_AnalyTrx.ANT_MatCommercial, ANT_AnalyTrx.ANT_NomComercial," _
            //& " ANT_AnalyTrx.ANT_MatInterv, ANT_AnalyTrx.ANT_NomInterv," _
            //& " ANT_AnalyTrx.ANT_MatChefDeSecteur, ANT_AnalyTrx.ANT_NomChefDeSecteur," _
            //& " ANT_AnalyTrx.ANT_MatRespExploit, ANT_AnalyTrx.ANT_NomRespExploit," _
            //& " ANT_AnalyTrx.NumFicheStandard, ANT_AnalyTrx.ANT_DateAjoutStandard," _
            //& " ANT_AnalyTrx.NoDevis, ANT_AnalyTrx.ANT_TotalHTDev," _
            //& " ANT_AnalyTrx.ANT_MoDev, ANT_AnalyTrx.ANT_FodDev, " _
            //& " ANT_AnalyTrx.ANT_StdDev,BonDeCommande.NoBonDeCommande" _
            //& " FROM         BCD_Detail RIGHT OUTER JOIN" _
            //& " BonDeCommande ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande RIGHT OUTER JOIN" _
            //& " Intervention INNER JOIN" _
            //& " ANT_AnalyTrx ON Intervention.NumFicheStandard = ANT_AnalyTrx.NumFicheStandard ON" _
            //& " BonDeCommande.NoIntervention = Intervention.NoIntervention LEFT OUTER JOIN" _
            //& " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture LEFT OUTER JOIN" _
            //& " FactureManuellePied ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle"


            sSQL = "SELECT  ANT_AnalyTrx.ANT_DateAjoutStandard, ANT_AnalyTrx.NumFicheStandard,"
                + " Intervention.NoIntervention, Intervention.CodeEtat, ANT_AnalyTrx.NoDevis, "
                + " ANT_AnalyTrx.ANT_TotalHTDev, ANT_AnalyTrx.ANT_MoDev, ANT_AnalyTrx.ANT_FodDev,"
                + " ANT_AnalyTrx.ANT_StdDev, BonDeCommande.NoBonDeCommande, Intervention.Debourse ,"
                + " BCD_Detail.BCD_PrixHT, ANT_AnalyTrx.CodeImmeuble, ANT_AnalyTrx.Code1,"
                + " ANT_AnalyTrx.ANT_MatCommercial, ANT_AnalyTrx.ANT_NomComercial,"
                + " ANT_AnalyTrx.ANT_MatInterv, ANT_AnalyTrx.ANT_NomInterv, ANT_AnalyTrx.ANT_MatChefDeSecteur,"
                + " ANT_AnalyTrx.ANT_NomChefDeSecteur,ANT_AnalyTrx.ANT_MatRespExploit,"
                + " ANT_AnalyTrx.ANT_NomRespExploit, Intervention.HeureDebut,"
                + " Intervention.HeureFin, Intervention.Duree,Intervention.DateRealise, intervention.intervenant,"
                + " Intervention.NoFacture  " + " FROM         BCD_Detail RIGHT OUTER JOIN"
                + " BonDeCommande ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande RIGHT OUTER JOIN"
                + " Intervention INNER JOIN"
                + " ANT_AnalyTrx ON Intervention.NumFicheStandard = ANT_AnalyTrx.NumFicheStandard ON"
                + " BonDeCommande.NoIntervention = Intervention.NoIntervention";

            sWhere = " ANT_User ='" + General.fncUserName() + "'";

            if (!string.IsNullOrEmpty(txtWhereCodeEtat.Text))
            {
                sWhere = sWhere + " AND " + txtWhereCodeEtat.Text;
            }

            //    sWhere = " WHERE     (Intervention.CodeEtat = 'AF' OR" _
            //'        & "  Intervention.CodeEtat = 'E' OR" _
            //'        & "  Intervention.CodeEtat = 'S' OR" _
            //'        & "  Intervention.CodeEtat = 'T' OR" _
            //'        & "  Intervention.CodeEtat = 'D')"

            if (!string.IsNullOrEmpty(cmbCommercial.Text))
            {
                sWhere = sWhere + " AND ANT_AnalyTrx.ANT_MatCommercial ='" + StdSQLchaine.gFr_DoublerQuote(cmbCommercial.SelectedText) + "'";
            }

            if (!string.IsNullOrEmpty(cmbIntervenant.Text))
            {
                sWhere = sWhere + " AND ANT_AnalyTrx.ANT_MatInterv ='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbCDS.Text))
            {

                sWhere = sWhere + " AND ANT_AnalyTrx.ANT_MatChefDeSecteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbCDS.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbRespExploit.Text))
            {

                sWhere = sWhere + " AND ANT_AnalyTrx.ANT_MatRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(cmbRespExploit.Text) + "'";

            }

            //    If chkReceptionne.value = 1 Then
            //            sWhere = sWhere & " and DevisEntete.Receptionne = 1"
            //    End If
            //
            //    If chkClotureDevis.value = 1 Then
            //            sWhere = sWhere & " and DevisEntete.ClotureDevis = 1"
            //    End If

            if (!string.IsNullOrEmpty(sWhere))
                sWhere = " WHERE " + sWhere;

            sOrderBy = " ORDER BY ANT_AnalyTrx.CodeImmeuble, ANT_AnalyTrx.NumFicheStandard , Intervention.NoIntervention, BonDeCommande.NoBonDeCommande";

            sSQL = sSQL + sWhere + sOrderBy;

            rs = modAdoRs.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement trouvée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ssTRavauxAnaly.DataSource = null;
                ssTRavauxAnaly.Visible = true;
                return;
            }

            dbCoef = Convert.ToDouble(General.nz(txtCoef.Text, 0));

            tAnalyTRavaux = null;
            tsNoInterFacture = null;
            tAnalyTRavauxImm = null;
            ///Erase tAnalyTRavaux
            i = 0;
            sCodeImmeuble = "";
            sNoIntervention = "";
            sNumFicheStandard = "";
            bFirst = true;

            //== charge les taux horaires.
            ModCalcul.fc_LoadChargePers("");

            foreach (DataRow r in rs.Rows)
            {
                //'If nz(rs!NumFicheStandard, "") = 233046 Then Stop
                //If sCodeImmeuble <> nz(rs!CodeImmeuble, "") Then

                if (sNumFicheStandard != General.nz(r["Numfichestandard"], "").ToString())
                {

                    sNumFicheStandard = General.nz(r["Numfichestandard"], "").ToString();
                    if (bFirst == true)//TESTED
                    {
                        i = 0;
                        Array.Resize(ref tsNoInterFacture, j + 1);
                        bFirst = false;
                    }
                    else
                    {
                        i = i + 1;
                    }

                    Array.Resize(ref tAnalyTRavaux, i + 1);

                    tsNoInterFacture = null;
                    j = 0;
                    Array.Resize(ref tsNoInterFacture, j + 1);

                    tAnalyTRavaux[i].sImmeuble = r["CodeImmeuble"] + "";
                    tAnalyTRavaux[i].sGerant = r["Code1"] + "";

                    tAnalyTRavaux[i].sNumFicheStandard = General.nz(r["Numfichestandard"], "").ToString();
                    tAnalyTRavaux[i].sMatCommercial = r["ANT_MatCommercial"] + "";
                    tAnalyTRavaux[i].sNomCommercial = r["ANT_NomComercial"] + "";
                    tAnalyTRavaux[i].sMatInterv = r["ANT_MatInterv"] + "";
                    tAnalyTRavaux[i].sNomInterv = r["ANT_NomInterv"] + "";
                    tAnalyTRavaux[i].sMatCDS = r["ANT_MatChefDeSecteur"] + "";
                    tAnalyTRavaux[i].sNomCDS = r["ANT_NomChefDeSecteur"] + "";
                    tAnalyTRavaux[i].sMatRespExploit = r["ANT_MatRespExploit"] + "";
                    tAnalyTRavaux[i].sNomRespExploit = r["ANT_NomRespExploit"] + "";
                    tAnalyTRavaux[i].sNodevis = r["NoDevis"] + "";
                    tAnalyTRavaux[i].dbVentePrevu = General.FncArrondir(Convert.ToDouble(General.nz(r["ANT_TotalHTDev"], 0)), 2);
                    tAnalyTRavaux[i].dbMoPrevu = General.FncArrondir(Convert.ToDouble(General.nz(r["ANT_MoDev"], 0)), 2);

                    tAnalyTRavaux[i].dbAchatPrevu = General.FncArrondir(Convert.ToDouble(General.nz(r["ANT_StdDev"], 0)) + Convert.ToDouble(General.nz(r["ANT_FodDev"], 0)), 2);
                    //=============================
                    //            Type AnalyTRavaux
                    //            dbCoef                  As Double
                    //            dbGain                  As Double

                    //=============================

                }


                if (!string.IsNullOrEmpty(General.nz(r["Nointervention"], "").ToString()))
                {
                    if (sNoIntervention != r["Nointervention"].ToString())//TESTED
                    {

                        sNoIntervention = r["Nointervention"].ToString();

                        //== calcul de la vente reél.

                        if (!string.IsNullOrEmpty(General.nz(r["NoFacture"], "").ToString()))//TESTED
                        {
                            //== sachant qu'une facture peut regrouper plusieurs interventions,
                            //== on controle pour chaque intervention si la facture a déja ete pris en
                            //== compte dans le calcul.
                            bFact = true;
                            for (X = 0; X <= tsNoInterFacture.Length - 1; X++)
                            {
                                if (tsNoInterFacture[X] != null)
                                {
                                    if (General.UCase(tsNoInterFacture[X]) == General.UCase(r["NoFacture"]))
                                    {
                                        bFact = false;
                                        break; // TODO: might not be correct. Was : Exit For
                                    }
                                }
                            }

                            if (bFact == true)//TESTED
                            {
                                Array.Resize(ref tsNoInterFacture, j + 1);
                                tsNoInterFacture[j] = General.UCase(r["NoFacture"]);
                                sSQL = "SELECT     FactureManuellePied.MontantHT"
                                    + " FROM         FactureManuelleEnTete INNER JOIN"
                                    + " FactureManuellePied ON FactureManuelleEnTete.CleFactureManuelle "
                                    + " = FactureManuellePied.CleFactureManuelle"
                                    + " WHERE  (FactureManuelleEnTete.NoFacture = '" + tsNoInterFacture[j] + "')";
                                using (var tmp = new ModAdo())
                                {
                                    dbVenteReel = Convert.ToDouble(General.nz(tmp.fc_ADOlibelle(sSQL), 0));
                                }

                                dbVenteReel = General.FncArrondir(dbVenteReel, 2);
                                tAnalyTRavaux[i].dbVente = dbVenteReel;
                                j = j + 1;

                            }
                        }


                        //== calcul de la main d'oeuvre.
                        dbTempTotMO = 0;
                        if (chkNbHeure.CheckState == CheckState.Checked)
                        {

                            sDureeFormat = txtNbHeure.Text;

                            if (!string.IsNullOrEmpty(r["DateRealise"].ToString()))
                            {
                                dbTempTotMO = ModCalcul.fc_calculMo(lTypeCalcul, r["Duree"] + "", r["HeureDebut"] + "", r["HeureFin"] + "", sDureeFormat, r["Intervenant"] + "", Convert.ToDateTime(r["DateRealise"] + ""), (chkDeplacement.Checked ? true : false));
                            }
                            tAnalyTRavaux[i].dbMo = General.FncArrondir(dbTempTotMO, 2);
                            tAnalyTRavaux[i].dbMo = General.FncArrondir(tAnalyTRavaux[i].dbMo + dbTempTotMO, 2);

                        }
                        else//TESTED
                        {

                            if (!string.IsNullOrEmpty(r["DateRealise"].ToString()))
                            {
                                dbTempTotMO = ModCalcul.fc_calculMo(lTypeCalcul, r["Duree"].ToString(), r["HeureDebut"] + "", r["HeureFin"] + "", "", r["Intervenant"] + "", Convert.ToDateTime(r["DateRealise"] + ""), (chkDeplacement.Checked ? true : false));
                                tAnalyTRavaux[i].dbMo = General.FncArrondir(tAnalyTRavaux[i].dbMo + dbTempTotMO, 2);
                            }
                        }
                    }
                }

                //== calcul des achats réels.

                if (!string.IsNullOrEmpty(General.nz(r["NoBonDeCommande"], "").ToString()))
                {
                    if (sNoBonDeCommande != r["NoBonDeCommande"].ToString())
                    {
                        sNoBonDeCommande = r["NoBonDeCommande"].ToString();
                        using (var tmp = new ModAdo())
                        {
                            tAnalyTRavaux[i].dbAchatReel = tAnalyTRavaux[i].dbAchatReel + Convert.ToDouble(General.nz(tmp.fc_ADOlibelle("SELECT sum( Montant) as MT From FactFournPied" + " WHERE NoBCmd =" + r["NoBonDeCommande"]), 0));
                        }
                    }
                }

                //rs.MoveNext();
            }


            rs.Dispose();

            rs = null;
            sCodeImmeuble = "";

            //== Groupement par immeuble.
            if (optImmeuble.Checked == true)
            {
                sCodeImmeuble = "";
                j = 0;
                bFirst = true;

                for (i = 0; i <= tAnalyTRavaux.Length - 1; i++)
                {
                    if (sCodeImmeuble != tAnalyTRavaux[i].sImmeuble)
                    {

                        if (bFirst == true)
                        {
                            j = 0;
                            bFirst = false;
                        }
                        else
                        {
                            j = j + 1;
                        }

                        Array.Resize(ref tAnalyTRavauxImm, j + 1);
                        sCodeImmeuble = tAnalyTRavaux[i].sImmeuble;

                        //var _with1 = tAnalyTRavauxImm[j];
                        tAnalyTRavauxImm[j].sImmeuble = tAnalyTRavaux[i].sImmeuble;
                        tAnalyTRavauxImm[j].sGerant = tAnalyTRavaux[i].sGerant;
                        tAnalyTRavauxImm[j].sMatCommercial = tAnalyTRavaux[i].sMatCommercial;
                        tAnalyTRavauxImm[j].sNomCommercial = tAnalyTRavaux[i].sNomCommercial;
                        tAnalyTRavauxImm[j].sMatRespExploit = tAnalyTRavaux[i].sMatRespExploit;
                        tAnalyTRavauxImm[j].sNomRespExploit = tAnalyTRavaux[i].sNomRespExploit;
                        tAnalyTRavauxImm[j].sMatInterv = tAnalyTRavaux[i].sMatInterv;
                        tAnalyTRavauxImm[j].sNomInterv = tAnalyTRavaux[i].sNomInterv;
                        tAnalyTRavauxImm[j].sMatCDS = tAnalyTRavaux[i].sMatCDS;
                        tAnalyTRavauxImm[j].sNomCDS = tAnalyTRavaux[i].sNomCDS;


                    }

                    //var _with2 = tAnalyTRavauxImm[j];
                    tAnalyTRavauxImm[j].dbMoPrevu = General.FncArrondir(tAnalyTRavauxImm[j].dbMoPrevu + tAnalyTRavaux[i].dbMoPrevu, 2);
                    tAnalyTRavauxImm[j].dbAchatPrevu = General.FncArrondir(tAnalyTRavauxImm[j].dbAchatPrevu + tAnalyTRavaux[i].dbAchatPrevu, 2);
                    tAnalyTRavauxImm[j].dbVentePrevu = General.FncArrondir(tAnalyTRavauxImm[j].dbVentePrevu + tAnalyTRavaux[i].dbVentePrevu, 2);
                    tAnalyTRavauxImm[j].dbVente = General.FncArrondir(tAnalyTRavauxImm[j].dbVente + tAnalyTRavaux[i].dbVente, 2);
                    tAnalyTRavauxImm[j].dbMo = General.FncArrondir(tAnalyTRavauxImm[j].dbMo + tAnalyTRavaux[i].dbMo, 2);
                    tAnalyTRavauxImm[j].dbAchatReel = General.FncArrondir((tAnalyTRavauxImm[j].dbAchatReel + tAnalyTRavaux[i].dbAchatReel), 2);

                }
            }
            dbTotVentePrevu = 0;
            dbtotAchatPrevu = 0;
            dbTotMoPrevu = 0;

            dbVenteReel = 0;
            dbTotAchatReel = 0;
            dbTotMoReel = 0;

            //== calcul du coef et des gains.
            if (optImmeuble.Checked == true)
            {
                for (i = 0; i <= tAnalyTRavauxImm.Length - 1; i++)
                {
                    var _with3 = tAnalyTRavauxImm[i];

                    dbTotVentePrevu = dbTotVentePrevu + tAnalyTRavauxImm[i].dbVentePrevu;
                    dbtotAchatPrevu = dbtotAchatPrevu + tAnalyTRavauxImm[i].dbAchatPrevu;
                    dbTotMoPrevu = dbTotMoPrevu + tAnalyTRavauxImm[i].dbMoPrevu;

                    dbVenteReel = dbVenteReel + tAnalyTRavauxImm[i].dbVente;
                    dbTotAchatReel = dbTotAchatReel + tAnalyTRavauxImm[i].dbAchatReel;
                    dbTotMoReel = dbTotMoReel + tAnalyTRavauxImm[i].dbMo;

                    //== coef et vente prevue.
                    if (tAnalyTRavauxImm[i].dbVentePrevu == 0 && tAnalyTRavauxImm[i].dbMoPrevu == 0 && tAnalyTRavauxImm[i].dbAchatPrevu == 0)
                    {
                        tAnalyTRavauxImm[i].dbCoefPrevu = 0;
                    }
                    else
                    {
                        tAnalyTRavauxImm[i].dbCoefPrevu = tAnalyTRavauxImm[i].dbVentePrevu / ((tAnalyTRavauxImm[i].dbMoPrevu + tAnalyTRavauxImm[i].dbAchatPrevu) == 0 ? tAnalyTRavauxImm[i].dbVentePrevu : (tAnalyTRavauxImm[i].dbMoPrevu + tAnalyTRavauxImm[i].dbAchatPrevu));
                        tAnalyTRavauxImm[i].dbCoefPrevu = General.FncArrondir(tAnalyTRavauxImm[i].dbCoefPrevu, 3);
                    }
                    tAnalyTRavauxImm[i].dbGainPrevu = tAnalyTRavauxImm[i].dbVentePrevu - (tAnalyTRavauxImm[i].dbMoPrevu + tAnalyTRavauxImm[i].dbAchatPrevu);

                    //== coef et vente réalisée.
                    if (tAnalyTRavauxImm[i].dbVente == 0 && tAnalyTRavauxImm[i].dbMo == 0 && tAnalyTRavauxImm[i].dbAchatReel == 0)
                    {
                        tAnalyTRavauxImm[i].dbCoef = 0;
                    }
                    else
                    {
                        tAnalyTRavauxImm[i].dbCoef = tAnalyTRavauxImm[i].dbVente / ((tAnalyTRavauxImm[i].dbMo + tAnalyTRavauxImm[i].dbAchatReel) == 0 ? tAnalyTRavauxImm[i].dbVente : (tAnalyTRavauxImm[i].dbMo + tAnalyTRavauxImm[i].dbAchatReel));
                        tAnalyTRavauxImm[i].dbCoef = General.FncArrondir(tAnalyTRavauxImm[i].dbCoef, 3);
                    }
                    tAnalyTRavauxImm[i].dbGain = tAnalyTRavauxImm[i].dbVente - (tAnalyTRavauxImm[i].dbMo + tAnalyTRavauxImm[i].dbAchatReel);

                }
            }
            else if (optAffaire.Checked == true)
            {
                for (i = 0; i <= tAnalyTRavaux.Length - 1; i++)
                {
                    var _with4 = tAnalyTRavaux[i];

                    dbTotVentePrevu = dbTotVentePrevu + tAnalyTRavaux[i].dbVentePrevu;
                    dbtotAchatPrevu = dbtotAchatPrevu + tAnalyTRavaux[i].dbAchatPrevu;
                    dbTotMoPrevu = dbTotMoPrevu + tAnalyTRavaux[i].dbMoPrevu;

                    dbVenteReel = dbVenteReel + tAnalyTRavaux[i].dbVente;
                    dbTotAchatReel = dbTotAchatReel + tAnalyTRavaux[i].dbAchatReel;
                    dbTotMoReel = dbTotMoReel + tAnalyTRavaux[i].dbMo;

                    //== coef et vente prevue.
                    if (tAnalyTRavaux[i].dbVentePrevu == 0 && tAnalyTRavaux[i].dbMoPrevu == 0 && tAnalyTRavaux[i].dbAchatPrevu == 0)
                    {
                        tAnalyTRavaux[i].dbCoefPrevu = 0;
                    }
                    else
                    {
                        tAnalyTRavaux[i].dbCoefPrevu = tAnalyTRavaux[i].dbVentePrevu / ((tAnalyTRavaux[i].dbMoPrevu + tAnalyTRavaux[i].dbAchatPrevu) == 0 ? tAnalyTRavaux[i].dbVentePrevu : (tAnalyTRavaux[i].dbMoPrevu + tAnalyTRavaux[i].dbAchatPrevu));
                        tAnalyTRavaux[i].dbCoefPrevu = General.FncArrondir(tAnalyTRavaux[i].dbCoefPrevu, 3);
                    }
                    tAnalyTRavaux[i].dbGainPrevu = tAnalyTRavaux[i].dbVentePrevu - (tAnalyTRavaux[i].dbMoPrevu + tAnalyTRavaux[i].dbAchatPrevu);

                    //== coef et vente réalisée.
                    if (tAnalyTRavaux[i].dbVente == 0 && tAnalyTRavaux[i].dbMo == 0 && tAnalyTRavaux[i].dbAchatReel == 0)
                    {
                        tAnalyTRavaux[i].dbCoef = 0;
                    }
                    else
                    {
                        tAnalyTRavaux[i].dbCoef = tAnalyTRavaux[i].dbVente / ((tAnalyTRavaux[i].dbMo + tAnalyTRavaux[i].dbAchatReel) == 0 ? tAnalyTRavaux[i].dbVente : (tAnalyTRavaux[i].dbMo + tAnalyTRavaux[i].dbAchatReel));
                        tAnalyTRavaux[i].dbCoef = General.FncArrondir(tAnalyTRavaux[i].dbCoef, 3);
                    }
                    tAnalyTRavaux[i].dbGain = tAnalyTRavaux[i].dbVente - (tAnalyTRavaux[i].dbMo + tAnalyTRavaux[i].dbAchatReel);

                }
            }
            //== alimente le tableau.

            ssTRavauxAnaly.Visible = false;

            DataTable dt = new DataTable();

            dt.Columns.Add("CodeImmeuble");
            dt.Columns.Add("Gerant");
            dt.Columns.Add("Commercial");
            dt.Columns.Add("Resp.Exploit");
            dt.Columns.Add("Secteur");
            dt.Columns.Add("Vente Prévue");
            dt.Columns.Add("Vente Réelle");
            dt.Columns.Add("MO Prévue");
            dt.Columns.Add("MO Réelle");
            dt.Columns.Add("Achats Prévue");
            dt.Columns.Add("Achats Réel");
            dt.Columns.Add("Coef Prévue");
            dt.Columns.Add("Coef Réel");
            dt.Columns.Add("Gain Prévue");
            dt.Columns.Add("Gain Réel");
            dt.Columns.Add("N° Fiche Standard");
            dt.Columns.Add("N° Devis");
            dt.Columns.Add("Intervenant");
            if (optImmeuble.Checked == true)
            {
                for (i = 0; i <= tAnalyTRavauxImm.Length - 1; i++)
                {
                    var _with5 = tAnalyTRavauxImm[i];
                    bAffiche = false;
                    if (!string.IsNullOrEmpty(txtCoef.Text))
                    {
                        switch (Combo4.Text)
                        {
                            case "=":
                                if (General.FncArrondir(_with5.dbCoef, 3) == dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">":
                                if (General.FncArrondir(_with5.dbCoef, 3) > dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">=":
                                if (General.FncArrondir(_with5.dbCoef, 3) >= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<":
                                if (General.FncArrondir(_with5.dbCoef, 3) < dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<=":
                                if (General.FncArrondir(_with5.dbCoef, 3) <= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                        }
                    }
                    else
                    {
                        bAffiche = true;
                    }
                    //ssTRavauxAnaly.DisplayLayout.Bands[0].Columns[0].Hidden = true;
                    //ssTRavauxAnaly.DisplayLayout.Bands[0].Columns[1].Hidden = true;

                    if (bAffiche == true)
                    {
                        var dr = dt.NewRow();

                        dr["CodeImmeuble"] = _with5.sImmeuble;
                        dr["Gerant"] = _with5.sGerant;
                        dr["Commercial"] = _with5.sNomCommercial;
                        dr["Resp.Exploit"] = _with5.sNomRespExploit;
                        dr["Secteur"] = _with5.sNomCDS;

                        //== Vente.
                        dr["Vente Prévue"] = General.FncArrondir(_with5.dbVentePrevu, 2);
                        dr["Vente Réelle"] = General.FncArrondir(_with5.dbVente, 2);

                        //== Mo.
                        dr["MO Prévue"] = General.FncArrondir(_with5.dbMoPrevu, 2);
                        dr["MO Réelle"] = General.FncArrondir(_with5.dbMo, 2);

                        //== achat previsionnel.
                        dr["Achats Prévue"] = General.FncArrondir(_with5.dbAchatPrevu, 2);

                        //==Achat Réel.               
                        dr["Achats Réel"] = General.FncArrondir(_with5.dbAchatReel, 2);

                        dr["Coef Prévue"] = General.FncArrondir(_with5.dbCoefPrevu, 3);
                        dr["Coef Réel"] = General.FncArrondir(_with5.dbCoef, 3);

                        dr["Gain Prévue"] = General.FncArrondir(_with5.dbGainPrevu, 3);
                        dr["Gain Réel"] = General.FncArrondir(_with5.dbGain, 3);


                        dt.Rows.Add(dr.ItemArray);
                        ssTRavauxAnaly.DataSource = dt;
                        ssTRavauxAnaly.UpdateData();

                        /* sAddItem = "@@$";
                         sAddItem = sAddItem + "@@$";
                         sAddItem = sAddItem + "@" + _with5.sImmeuble + "@$";
                         sAddItem = sAddItem + "@" + _with5.sGerant + "@$";
                         sAddItem = sAddItem + "@" + _with5.sNomCommercial + "@$";
                         sAddItem = sAddItem + "@" + _with5.sNomRespExploit + "@$";
                         sAddItem = sAddItem + "@" + _with5.sNomCDS + "@$";
                         sAddItem = sAddItem + "@" + _with5.sNomInterv + "@$";

                         //== Vente .
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbVentePrevu,  2) + "@$";
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbVente,  2) + "@$";

                         //== Mo.
                         sAddItem = sAddItem + "@" + General.FncArrondir(_with5.dbMoPrevu,  2) + "@$";
                         sAddItem = sAddItem + "@" + General.FncArrondir(_with5.dbMo, 2) + "@$";

                         //== achat previsionnel.
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbAchatPrevu,  2) + "@$";
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbAchatReel,  2) + "@$";

                         //==Achat Réel.
                         //                    sAddItem = sAddItem & "@" & FncArrondir(.dbAchatPrevu, 2) & "@$"
                         //                    sAddItem = sAddItem & "@" & FncArrondir(.dbAchatReel, 2) & "@$"

                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbCoefPrevu,  3) + "@$";
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbCoef,  3) + "@$";

                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbGainPrevu,  3) + "@$";
                         sAddItem = sAddItem + "@" + General.FncArrondir( _with5.dbGain,  3) + "@$";
                         ssTRavauxAnaly.AddItem(sAddItem);*/
                    }

                }

            }
            else if (optAffaire.Checked == true)
            {

                for (i = 0; i <= tAnalyTRavaux.Length - 1; i++)
                {
                    var _with6 = tAnalyTRavaux[i];
                    bAffiche = false;
                    if (!string.IsNullOrEmpty(txtCoef.Text))
                    {
                        switch (Combo4.Text)
                        {
                            case "=":
                                if (General.FncArrondir(_with6.dbCoef, 3) == dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">":
                                if (General.FncArrondir(_with6.dbCoef, 3) > dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">=":
                                if (General.FncArrondir(_with6.dbCoef, 3) >= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<":
                                if (General.FncArrondir(_with6.dbCoef, 3) < dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<=":
                                if (General.FncArrondir(_with6.dbCoef, 3) <= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                        }
                    }
                    else
                    {
                        bAffiche = true;
                    }

                    if (bAffiche == true)
                    {


                        var dr = dt.NewRow();
                        dr["N° Fiche Standard"] = _with6.sNumFicheStandard;
                        dr["N° Devis"] = _with6.sNodevis;
                        dr["CodeImmeuble"] = _with6.sImmeuble;
                        dr["Gerant"] = _with6.sGerant;
                        dr["Commercial"] = _with6.sNomCommercial;
                        dr["Resp.Exploit"] = _with6.sNomRespExploit;
                        dr["Secteur"] = _with6.sNomCDS;
                        dr["Intervenant"] = _with6.sNomInterv;
                        //== Vente.
                        dr["Vente Prévue"] = General.FncArrondir(_with6.dbVentePrevu, 2);
                        dr["Vente Réelle"] = General.FncArrondir(_with6.dbVente, 2);

                        //== Mo.
                        dr["MO Prévue"] = General.FncArrondir(_with6.dbMoPrevu, 2);
                        dr["MO Réelle"] = General.FncArrondir(_with6.dbMo, 2);

                        //== achat previsionnel.
                        dr["Achats Prévue"] = General.FncArrondir(_with6.dbAchatPrevu, 2);

                        //==Achat Réel.               
                        dr["Achats Réel"] = General.FncArrondir(_with6.dbAchatReel, 2);

                        dr["Coef Prévue"] = General.FncArrondir(_with6.dbCoefPrevu, 3);
                        dr["Coef Réel"] = General.FncArrondir(_with6.dbCoef, 3);

                        dr["Gain Prévue"] = General.FncArrondir(_with6.dbGainPrevu, 3);
                        dr["Gain Réel"] = General.FncArrondir(_with6.dbGain, 3);


                        dt.Rows.Add(dr.ItemArray);
                        ssTRavauxAnaly.DataSource = dt;
                        ssTRavauxAnaly.UpdateData();
                        /*sAddItem = "@" + _with6.sNumFicheStandard + "@$";
                        sAddItem = sAddItem + "@" + _with6.sNodevis + "@$";
                        sAddItem = sAddItem + "@" + _with6.sImmeuble + "@$";
                        sAddItem = sAddItem + "@" + _with6.sGerant + "@$";
                        sAddItem = sAddItem + "@" + _with6.sNomCommercial + "@$";
                        sAddItem = sAddItem + "@" + _with6.sNomRespExploit + "@$";
                        sAddItem = sAddItem + "@" + _with6.sNomCDS + "@$";
                        sAddItem = sAddItem + "@" + _with6.sNomInterv + "@$";

                        //== Vente .
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbVentePrevu,  2) + "@$";
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbVente,  2) + "@$";

                        //== Mo.
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbMoPrevu,  2) + "@$";
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbMo,  2) + "@$";

                        //== achat previsionnel.
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbAchatPrevu,  2) + "@$";
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbAchatReel,  2) + "@$";

                        //==Achat Réel.
                        //                    sAddItem = sAddItem & "@" & FncArrondir(.dbAchatPrevu, 2) & "@$"
                        //                    sAddItem = sAddItem & "@" & FncArrondir(.dbAchatReel, 2) & "@$"

                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbCoefPrevu,  3) + "@$";
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbCoef,  3) + "@$";

                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbGainPrevu,  3) + "@$";
                        sAddItem = sAddItem + "@" + General.FncArrondir( _with6.dbGain,  3) + "@$";
                        ssTRavauxAnaly.AddItem(sAddItem);*/
                    }

                }
            }

            ssTRavauxAnaly.Visible = true;

            lblVentesReelles.Text = Convert.ToString(General.FncArrondir(dbVenteReel, 2));
            lblMOReel.Text = Convert.ToString(General.FncArrondir(dbTotMoReel, 2));
            lblAchatsReels.Text = Convert.ToString(General.FncArrondir(dbTotAchatReel, 2));
            lblChargesReelles.Text = Convert.ToString(General.FncArrondir(dbTotMoReel + dbTotAchatReel, 2));
            if (lblChargesReelles.Text == "0")
            {
                if (dbVenteReel != 0)
                {
                    lblCoefficientReel.Text = Convert.ToString(General.FncArrondir(dbVenteReel / dbVenteReel, 3));
                }
            }
            else
            {
                lblCoefficientReel.Text = Convert.ToString(General.FncArrondir(dbVenteReel / Convert.ToDouble(lblChargesReelles.Text), 3));
            }

            lblVentesPrevues.Text = Convert.ToString(General.FncArrondir(dbTotVentePrevu, 2));
            lblMOPrevues.Text = Convert.ToString(General.FncArrondir(dbTotMoPrevu, 2));
            lblAchatsPrevus.Text = Convert.ToString(General.FncArrondir(dbtotAchatPrevu, 2));
            lblChargesPrevues.Text = Convert.ToString(General.FncArrondir(dbTotMoPrevu + dbtotAchatPrevu, 2));
            if (lblChargesPrevues.Text == "0")
            {
                if (dbTotVentePrevu != 0)
                {
                    lblCoefPrevu.Text = Convert.ToString(General.FncArrondir(dbTotVentePrevu / dbTotVentePrevu, 3));
                }
            }
            else
            {
                lblCoefPrevu.Text = Convert.ToString(General.FncArrondir(dbTotVentePrevu / Convert.ToDouble(lblChargesPrevues.Text), 3));
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sErreur"></param>
        private void fc_Err(string sErreur = "")
        {
            blnErreur = true;

            if (!string.IsNullOrEmpty(sErreur) && !sError.Contains(sErreur))
            {
                File.AppendAllText(General.cCheminFichierErrTrx, sErreur + "\r\n");
            }

            sError = sError + sErreur;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dtDeb"></param>
        /// <param name="dtFin"></param>
        private void fc_MtTRavaux(System.DateTime dtDeb, System.DateTime dtFin)
        {
            string sSQL = null;
            string sWhere = null;

            DataTable rsAddTrx = default(DataTable);
            DataTable rsTRx = default(DataTable);
            ModAdo rsAddTrxAdo = new ModAdo();
            ModAdo rsTRxAdo = new ModAdo();
            int lnumfichestandard = 0;

            bool bAddNew = false;
            double dbMo = 0;
            double dbFod = 0;
            double dbStd = 0;

            ssTRavauxAnaly.DataSource = null;
            int ANT_NoAuto = 0;
            //== afface la selection precedente.
            sSQL = "DELETE FROM ANT_AnalyTrx WHERE ANT_User='" + sUser + "'";
            var xx = General.Execute(sSQL);


            //== Insere dans une table temporaire tous les fiches d'appels
            //== avec ou sans devis.

            sSQL = "SELECT ANT_NoAuto, ANT_Mac, ANT_User, CodeImmeuble, Code1,"
                + " ANT_MatCommercial, ANT_NomComercial, ANT_MatInterv, ANT_NomInterv, "
                + " ANT_MatChefDeSecteur, ANT_NomChefDeSecteur, ANT_MatRespExploit,"
                + " ANT_NomRespExploit, NumFicheStandard, ANT_DateAjoutStandard, NoDevis,"
                + " ANT_TotalHTDev , ANT_MoDev, ANT_FodDev, ANT_StdDev"
                + " FROM         ANT_AnalyTrx" + " WHERE ANT_NoAuto = 0";

            rsAddTrx = rsAddTrxAdo.fc_OpenRecordSet(sSQL);


            sSQL = "SELECT   immeuble.codeImmeuble,  Immeuble.Code1, Commercial.Matricule AS MatCommercial,"
                + " Commercial.Nom AS NomCommercial, Intervenant.Matricule AS MatInterv, "
                + " Intervenant.Nom AS NomInterv, ChefDeSecteur.Matricule AS MatCDS,"
                + " ChefDeSecteur.Nom AS NomCDS, RespExploit.Matricule AS MatRespExploit,"
                + " RespExploit.Nom AS NomRespExploit, GestionStandard.NumFicheStandard,"
                + " GestionStandard.DateAjout, DevisEnTete.NumeroDevis,"
                + " DevisEnTete.TotalHT , DevisDetail.MOd, DevisDetail.FOd, DevisDetail.STd"
                + " FROM         Immeuble INNER JOIN"
                + " Table1 ON Immeuble.Code1 = Table1.Code1 INNER JOIN"
                + " GestionStandard ON Immeuble.CodeImmeuble = GestionStandard.CodeImmeuble LEFT OUTER JOIN"
                + " DevisEnTete LEFT OUTER JOIN"
                + " DevisDetail ON DevisEnTete.NumeroDevis = DevisDetail.NumeroDevis ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis LEFT OUTER JOIN"
                + " Personnel Commercial ON Table1.commercial = Commercial.Matricule LEFT OUTER JOIN"
                + " Personnel Intervenant ON Immeuble.CodeDepanneur = Intervenant.Matricule LEFT OUTER JOIN"
                + " Personnel ChefDeSecteur ON Intervenant.CSecteur = ChefDeSecteur.Initiales LEFT OUTER JOIN"
                + " Personnel RespExploit ON Intervenant.CRespExploit = RespExploit.Initiales";


            sWhere = "";

            sWhere = " WHERE GestionStandard.DateAjout >='" + dtDeb.ToShortDateString() + " 00:00:00" + "'";
            sWhere = sWhere + " AND GestionStandard.DateAjout<='" + dtFin.ToShortDateString() + " 23:59:59" + "'";

            if (!string.IsNullOrEmpty(cmbCodeGerant.Text))
            {

                sWhere = sWhere + "  And  Table1.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
            }

            if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
            {

                sWhere = sWhere + "  And Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
            }

            if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
            {
                sWhere = sWhere + "  And GestionStandard.NumFicheStandard =" + txtNumFicheStandard.Text + "";
            }

            sWhere = sWhere + " order by   GestionStandard.NumFicheStandard,DevisEnTete.NumeroDevis ";

            sSQL = sSQL + sWhere;

            rsTRx = rsTRxAdo.fc_OpenRecordSet(sSQL);

            lnumfichestandard = 0;
            SqlCommandBuilder cb = new SqlCommandBuilder(rsAddTrxAdo.SDArsAdo);
            rsAddTrxAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            rsAddTrxAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            rsAddTrxAdo.SDArsAdo.InsertCommand = insertCmd;

            rsAddTrxAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {
                if (e.StatementType == StatementType.Insert)
                {
                    if (ANT_NoAuto == 0)
                    {
                        ANT_NoAuto = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                    }
                }
            });
            foreach (DataRow r in rsTRx.Rows)
            {
                ///If IsNumeric(rsTRx!numerodevis) Then Stop

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //If rstrx!CodeImmeuble = "17PECLET" Then Stop
                if (lnumfichestandard != Convert.ToInt32(r["Numfichestandard"]))
                {
                    ANT_NoAuto = 0;

                    lnumfichestandard = Convert.ToInt32(General.nz(r["Numfichestandard"], 0));
                    var newRow = rsAddTrx.NewRow();

                    newRow["Numfichestandard"] = lnumfichestandard;
                    newRow["ANT_DateAjoutStandard"] = General.nz(r["DateAjout"], System.DBNull.Value);
                    newRow["CodeImmeuble"] = r["CodeImmeuble"];
                    newRow["Code1"] = r["Code1"];
                    //=== adresse MAC
                    newRow["ANT_Mac"] = sAddMAc;
                    newRow["ANT_User"] = sUser;

                    newRow["ANT_TotalHTDev"] = General.nz(r["TotalHT"], 0);

                    newRow["NoDevis"] = General.nz(r["numerodevis"], System.DBNull.Value);

                    //==commercial.

                    if (string.IsNullOrEmpty(General.nz(r["MatCommercial"], "").ToString()))
                    {
                        fc_Err("Le gérant " + r["Code1"] + " n'est pas rattaché à un commercial.");
                    }
                    newRow["ANT_MatCommercial"] = r["MatCommercial"] + "";
                    newRow["ANT_NomComercial"] = r["NomCommercial"] + "";

                    //==intervenant.

                    if (string.IsNullOrEmpty(General.nz(r["MatInterv"], "").ToString()))
                    {
                        fc_Err("L'immeuble " + r["CodeImmeuble"] + " n'est pas rattaché à un intervenant.");
                    }
                    newRow["ANT_MatInterv"] = r["MatInterv"];
                    newRow["ANT_NomInterv"] = r["NomInterv"];

                    //== chef de secteur.

                    if (string.IsNullOrEmpty(General.nz(r["MatCDS"], "").ToString()) && !string.IsNullOrEmpty(General.nz(r["MatInterv"], "").ToString()))
                    {
                        fc_Err("L'intervenant " + r["MatInterv"] + " n'est pas rattaché à un chef de secteur.");
                    }
                    newRow["ANT_MatChefDeSecteur"] = r["MatCDS"] + "";
                    newRow["ANT_NomChefDeSecteur"] = r["NomCDS"] + "";

                    //==Responsable Exploit.

                    if (string.IsNullOrEmpty(General.nz(r["MatRespExploit"], "").ToString()) && !string.IsNullOrEmpty(General.nz(r["MatInterv"], "").ToString()))
                    {
                        fc_Err("L'intervenant " + r["MatInterv"] + " n'est pas rattaché à un responsable d'exploitation.");
                    }
                    newRow["ANT_MatRespExploit"] = r["MatRespExploit"] + "";
                    newRow["ANT_NomRespExploit"] = r["NomRespExploit"] + "";

                    dbMo = 0;
                    dbFod = 0;
                    dbStd = 0;
                    newRow["ANT_MoDev"] = dbMo;
                    newRow["ANT_FodDev"] = dbFod;
                    newRow["ANT_StdDev"] = dbStd;
                    rsAddTrx.Rows.Add(newRow.ItemArray);
                    var xxx = rsAddTrxAdo.Update();
                }


                dbMo = dbMo + Convert.ToDouble(General.nz(r["Mod"], 0));
                dbFod = dbFod + Convert.ToDouble(General.nz(r["Fod"], 0));
                dbStd = dbStd + Convert.ToDouble(General.nz(r["Std"], 0));

                //'jai ajouter cette requete afin de modifier les valeurs 'ANT_MoDev' 'ANT_StdDev' 'ANT_FodDev' de la table ANT_AnalyTrx qui a été inserer
                string req = $"update ANT_AnalyTrx set ANT_MoDev={dbMo}, ANT_FodDev={dbFod}, ANT_StdDev={dbStd} where  ANT_NoAuto={ ANT_NoAuto }";
                var xxxx = General.Execute(req);
                xxxx = rsAddTrxAdo.Update();
                //rsAddTrx.MoveLast();
                //rsTRx.MoveNext();
            }


            rsTRx.Dispose();
            rsAddTrx.Dispose();

            rsTRx = null;

            rsAddTrx = null;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlErreurSaisie()
        {
            bool functionReturnValue = false;
            //== retourne vrai si une erreur est decelée.
            if (string.IsNullOrEmpty(txtinterDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date réalisée de debut. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (string.IsNullOrEmpty(txtIntereAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date réalisée de fin. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCoef.Text))
            {
                if (!General.IsNumeric(txtCoef.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique pour le coef. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;

                }
                else if (string.IsNullOrEmpty(Combo4.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Sélectionnez dans la liste déroulante un opérateur (=,>=,<=,>,<) pour le coefficient. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;

                }
            }

            if (chkNbHeure.Checked && !General.IsDate(txtNbHeure.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez coché la case Nb Heurepar defaut, vous devez saisir un nombre d 'heure par defaut.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }


            //-- ouvre le fichier des erreurs          
            File.Delete(General.cCheminFichierErrTrx);//verifier


            //ouvre le fichier des erreurs constatés.
            File.AppendAllText(General.cCheminFichierErrTrx, "");

            lblVentesReelles.Text = "";
            lblMOReel.Text = "";
            lblAchatsReels.Text = "";
            lblChargesReelles.Text = "";
            lblCoefficientReel.Text = "";

            lblVentesPrevues.Text = "";
            lblMOPrevues.Text = "";
            lblAchatsPrevus.Text = "";
            lblChargesPrevues.Text = "";
            lblCoefPrevu.Text = "";

            ssTRavauxAnaly.Visible = false;

            sError = "";
            return functionReturnValue;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeGerant.Rows.Count < 1)
            {
                var _with7 = this;
                General.sSQL = "SELECT Table1.Code1 AS [Code Client], Table1.Adresse1,"
                    + " Table1.CodePostal, Table1.Ville" + " From Table1" + " ORDER BY Code1";
                sheridan.InitialiseCombo(_with7.cmbCodeGerant, General.sSQL, "Code Client");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataTable rsGerant = default(DataTable);
            ModAdo rsGerantAdo = new ModAdo();
            bool blnGoRecherche = false;
            short KeyAscii = (short)e.KeyChar;
            try
            {

                blnGoRecherche = false;
                if (KeyAscii == 13)
                {
                    rsGerant = new DataTable();
                    rsGerant = rsGerantAdo.fc_OpenRecordSet("SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 LIKE '%" + cmbCodeGerant.Text + "%'");
                    if (rsGerant.Rows.Count > 0)
                    {
                        if (rsGerant.Rows.Count > 1)
                        {
                            blnGoRecherche = true;
                        }
                    }
                    else
                    {
                        blnGoRecherche = true;
                    }

                    if (blnGoRecherche == true)
                    {

                        string requete = "SELECT Code1 as \"Code Gérant\","
                            + " Adresse1 as \"Adresse\"," + " Ville as \"Ville\" , CodePostal AS \"Code Postal\" FROM Table1 ";
                        string where = "";

                        SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  d'un gérant" };

                        fg.SetValues(new Dictionary<string, string> { { "Code1", cmbCodeGerant.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();

                    }
                    else
                    {
                        ///   cmbIntervention_Click
                    }
                    rsGerant.Dispose();

                    rsGerant = null;
                }

                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbCodeGerant_KeyPress");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeimmeuble.Rows.Count < 1)
            {
                var _with9 = this;
                General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse,"
                    + " Immeuble.CodePostal, Immeuble.Ville"
                    + " From Immeuble" + " order by CodeImmeuble";
                sheridan.InitialiseCombo(_with9.cmbCodeimmeuble, General.sSQL, "codeImmeuble");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmp = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + cmbCodeimmeuble.Text + "'"), "").ToString()))
                    {


                        string requete = "SELECT CodeImmeuble as \"Code Immeuble\","
                            + " Adresse as  \"Adresse \"," + " Ville as  \"Ville \" , anglerue as  \"Angle de rue \", Code1 as  \"Gerant \" FROM Immeuble ";

                        SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche  d'une immeuble" };

                        fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", cmbCodeimmeuble.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                        ///'cmbIntervention_Click
                    }
                    return;
                }
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lblLibCommercial.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCommercial.Text + "'");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmp = new ModAdo();

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCommercial.Text + "'"), "").ToString()))
                    {


                        string requete = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\","
                                + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\""
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

                        SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche  d'un Commercial" };

                        if (General.IsNumeric(cmbCommercial.Text))
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbCommercial.Text } });

                        }
                        else
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Nom", cmbCommercial.Text } });

                        }
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibCommercial.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibCommercial.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                    }
                    return;
                }
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_KeyPress");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lblLibIntervenant.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Text + "'");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmp = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Text + "'"), "").ToString()))
                    {

                        string requete = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\","
                                + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\""
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche  d'un intervenant" };

                        if (General.IsNumeric(cmbCommercial.Text))
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbIntervenant.Text } });

                        }
                        else
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Nom", cmbIntervenant.Text } });

                        }
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                        //        cmbIntervention_Click
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string req = null;
            int i = 0;
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";

            if (General.CodeQualifCommercial.Length > 0)
            {
                req = req + " WHERE ";
                for (i = 1; i <= General.CodeQualifCommercial.Length - 1; i++)
                {
                    if (i < (General.CodeQualifCommercial.Length - 1))
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                    }
                    else
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                    }
                }
            }

            General.sSQL = General.sSQL + req + " ORDER BY Personnel.Nom";

            sheridan.InitialiseCombo(cmbCommercial, General.sSQL, "Matricule");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCDS_Click(object sender, EventArgs e)
        {
            cmbCDS_KeyPress(cmbCDS, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        private void cmdClean_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            txtinterDe.Text = "";
            txtIntereAu.Text = "";
            cmbCodeimmeuble.Text = "";
            cmbCodeGerant.Text = "";
            txtNumFicheStandard.Text = "";
            Combo4.Text = "";
            txtCoef.Text = "";
            cmbCommercial.Text = "";
            cmbIntervenant.Text = "";
            cmbCDS.Text = "";
            cmbRespExploit.Text = "";
            txtNbHeure.Text = "  :  :  ";
            txtCodeEtat1.Text = "";
            txtCodeEtat2.Text = "";
            txtCodeEtat3.Text = "";
            txtCodeEtat4.Text = "";
            txtCodeEtat5.Text = "";
            txtCodeEtat6.Text = "";
            txtCodeEtat7.Text = "";
            txtCodeEtat8.Text = "";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGerant_Click(object sender, EventArgs e)
        {
            cmbCodeGerant_KeyPress(cmbCodeGerant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lblLibRespExploit.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbRespExploit.Text + "'");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbRespExploit, General.sSQL, "Matricule");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmp = new ModAdo();

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbRespExploit.Text + "'"), "").ToString()))
                    {
                        string requete = "SELECT Personnel.Matricule as \"Matricule\","
                                 + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                                 + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\","
                                 + " Personnel.NumRadio as \"NumRadio\""
                                 + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

                        SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche  d'un responsable" };

                        if (General.IsNumeric(cmbIntervenant.Text))
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbRespExploit.Text } });

                        }
                        else
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Nom", cmbRespExploit.Text } });

                        }
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbRespExploit.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibRespExploit.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbRespExploit.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibRespExploit.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();



                    }
                    //        cmbIntervention_Click
                }
                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }


        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="C"></param>
        /// <param name="j"></param>
        private void LoopControls1(Control C, int j)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    LoopControls1(CC, j);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                if (C.Tag != null && General.UCase(C.Tag) == General.UCase("txtCodeEtat" + j))
                {
                    C.Text = "";
                    return; // TODO: might not be correct. Was : Exit For
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="C"></param>
        /// <param name="j"></param>
        /// <param name="text"></param>
        private void LoopControls2(Control C, int j, string text)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    LoopControls2(CC, j, text);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                if (C.Tag != null && General.UCase(C.Tag) == General.UCase("txtCodeEtat" + j))
                {
                    C.Text = text;
                    return; // TODO: might not be correct. Was : Exit For
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMultiSelect_Click(object sender, EventArgs e)
        {
            int i = 0;
            object obj = null;
            int j = 0;
            frm = new frmSelect();
            frm.txtSelect.Text = "SELECT '' as \"Sélection\",CodeEtat AS \"CodeEtat\"," + " LibelleCodeEtat as \"Libelle\" FROM TypeCodeEtat";

            frm.ShowDialog();

            if (frm.chkAnnuler.CheckState == 0)
            {
                //frm.GridSelect.MoveFirst();

                for (j = 1; j <= 8; j++)
                {
                    LoopControls1(this, j);
                    //foreach (object obj_loopVariable in this.Controls)
                    //{
                    //    obj = obj_loopVariable;                      
                    //    if (General.UCase(obj.Tag) == General.UCase("txtCodeEtat" + j))
                    //    {                      
                    //        obj.Text = "";
                    //        break; // TODO: might not be correct. Was : Exit For
                    //    }
                    //}
                }
                j = 0;
                txtWhereCodeEtat.Text = "";

                for (i = 0; i <= frm.GridSelect.Rows.Count - 1; i++)
                {
                    if (Convert.ToBoolean(frm.GridSelect.Rows[i].Cells[0].Value))
                    {
                        if (!string.IsNullOrEmpty(txtWhereCodeEtat.Text))
                        {
                            txtWhereCodeEtat.Text = txtWhereCodeEtat.Text + " OR ";
                        }
                        else
                        {
                            txtWhereCodeEtat.Text = "( ";
                        }
                        txtWhereCodeEtat.Text = txtWhereCodeEtat.Text + " Intervention.CodeEtat='" + frm.GridSelect.Rows[i].Cells[1].Value.ToString() + "'";
                        j = j + 1;
                        LoopControls2(this, j, frm.GridSelect.Rows[i].Cells[1].Value.ToString());
                        //foreach (object obj_loopVariable in this.Controls)
                        //{
                        //    obj = obj_loopVariable;                           
                        //    if (General.UCase(obj.Tag) == General.UCase("txtCodeEtat" + j))
                        //    {                                
                        //        obj.Text = frm.GridSelect.DisplayLayout.Bands[0].Columns[1].Key;
                        //        break; // TODO: might not be correct. Was : Exit For
                        //    }
                        //}

                    }
                    //_with14.GridSelect.MoveNext();
                }
                if (!string.IsNullOrEmpty(txtWhereCodeEtat.Text))
                {
                    txtWhereCodeEtat.Text = txtWhereCodeEtat.Text + " )";
                }

            }
            else
            {
                //txtWhereCodeEtat.Text = ""
            }
            frm.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheAppel_Click(object sender, EventArgs e)
        {
            try
            {

                string requete = "SELECT GestionStandard.NumFicheStandard as \"No Fiche standard\","
                    + " GestionStandard.Utilisateur as \"Utilisateur\","
                    + " GestionStandard.DateAjout as \"date\", GestionStandard.CodeParticulier as \"Client\","
                    + " GestionStandard.CodeImmeuble as \"Référence immeuble\" From GestionStandard";


                SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche d'une fiche d'appel" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtNumFicheStandard.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtNumFicheStandard.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Rechercher");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            cmbCommercial_KeyPress(cmbCommercial, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRespExploit_Click(object sender, EventArgs e)
        {
            cmbRespExploit_KeyPress(cmbRespExploit, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTravaux_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            fc_Travaux();
            fc_SaveSetting();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdvisu_Click(object sender, EventArgs e)
        {

            try
            {
                Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
                Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
                Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
                Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
                Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);

                DataTable rsExport = default(DataTable);
                //ADODB.Field oField = default(ADODB.Field);

                int i = 0;
                int j = 0;
                string sMatricule = null;
                int lLigne = 0;
                int X = 0;
                int x1 = 0;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //==Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                //==Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                rsExport = ssTRavauxAnaly.DataSource as DataTable;
                //== affiche les noms des champs.
                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //==Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //==formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;

                //ssTRavauxAnaly.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {

                        oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                    //_with14.MoveNext();
                }



                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;

                oSheet = null;

                oWB = null;

                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;



                return;
            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Error: " + ex.HResult, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lblLibChefDeSecteur.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCDS.Text + "'");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbCDS, General.sSQL, "Matricule");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmp = new ModAdo();

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCDS.Text + "'"), "").ToString()))
                    {
                        string requete = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\","
                           + " Personnel.prenom as \"Prenom\","
                           + " Qualification.Qualification as \"Qualification\","
                           + " Personnel.Memoguard as \"MemoGuard\","
                           + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";


                        SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "Recherche  d'un chef de secteur" };

                        if (General.IsNumeric(cmbIntervenant.Text))
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbCDS.Text } });

                        }
                        else
                        {
                            fg.SetValues(new Dictionary<string, string> { { "Nom", cmbCDS.Text } });

                        }
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {

                            cmbCDS.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibChefDeSecteur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCDS.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibChefDeSecteur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            }
                        };

                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                    }
                    //        cmbIntervention_Click
                }
                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_SaveSetting()
        {

            General.saveInReg(Variable.cUserDocAnalyTRavaux, "interDe", txtinterDe.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "interAu", txtIntereAu.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeImmeuble", cmbCodeimmeuble.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeGerant", cmbCodeGerant.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "NumFicheStandard", txtNumFicheStandard.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "OperateurCoefReel", Combo4.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CoefReel", txtCoef.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "Commercial", cmbCommercial.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "Intervenant", cmbIntervenant.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "ChefDeSecteur", cmbCDS.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "RespExploit", cmbRespExploit.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "NbHeure", txtNbHeure.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat1", txtCodeEtat1.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat2", txtCodeEtat2.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat3", txtCodeEtat3.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat4", txtCodeEtat4.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat5", txtCodeEtat5.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat6", txtCodeEtat6.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat7", txtCodeEtat7.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "CodeEtat8", txtCodeEtat8.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "ChkNbHeure", Convert.ToInt16(chkNbHeure.CheckState).ToString());
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "chkDeplacement", Convert.ToInt16(chkDeplacement.CheckState).ToString());
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "OptDefaut", Convert.ToString(OptDefaut.Checked));
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "OPtGrille", Convert.ToString(OptGrille.Checked));
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "WhereCodeEtat", txtWhereCodeEtat.Text);
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "optImmeuble", Convert.ToString(optImmeuble.Checked));
            General.saveInReg(Variable.cUserDocAnalyTRavaux, "optAffaire", Convert.ToString(optAffaire.Checked));


        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssTRavauxAnaly_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            fc_SaveSetting();

            if (optAffaire.Checked == true)
            {

                General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", ssTRavauxAnaly.ActiveRow.Cells["N° Fiche Standard"].Text);
                General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
                General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");
                View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));

            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocAnalyTravaux_VisibleChanged(object sender, EventArgs e)
        {

            if (!Visible)
                return;
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocAnalyTravaux");
            // TODO
            sAddMAc = (from nic in NetworkInterface.GetAllNetworkInterfaces()
                       where nic.OperationalStatus == OperationalStatus.Up
                       select nic.GetPhysicalAddress().ToString()
                    ).FirstOrDefault();

            sUser = General.fncUserName();
            txtinterDe.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "interDe", "");
            txtIntereAu.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "interAu", "");
            cmbCodeimmeuble.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeImmeuble", "");
            cmbCodeGerant.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeGerant", "");
            txtNumFicheStandard.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "NumFicheStandard", "");
            Combo4.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "OperateurCoefReel", "");
            txtCoef.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CoefReel", "");
            cmbCommercial.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "Commercial", "");
            cmbIntervenant.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "Intervenant", "");
            cmbCDS.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "ChefDeSecteur", "");
            cmbRespExploit.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "RespExploit", "");
            txtNbHeure.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "NbHeure", "  :  :  ");
            txtCodeEtat1.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat1", "");
            txtCodeEtat2.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat2", "");
            txtCodeEtat3.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat3", "");
            txtCodeEtat4.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat4", "");
            txtCodeEtat5.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat5", "");
            txtCodeEtat6.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat6", "");
            txtCodeEtat7.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat7", "");
            txtCodeEtat8.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "CodeEtat8", "");
            chkNbHeure.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "ChkNbHeure", "0") == "1";
            chkDeplacement.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "chkDeplacement", "0") == "1";
            OptDefaut.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "OptDefaut", Convert.ToString(OptDefaut.Checked)) == "True";
            OptGrille.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "OPtGrille", Convert.ToString(OptGrille.Checked)) == "True";
            optImmeuble.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "optImmeuble", Convert.ToString(optImmeuble.Checked)) == "True";
            optAffaire.Checked = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "optAffaire", Convert.ToString(optAffaire.Checked)) == "True";

            txtWhereCodeEtat.Text = General.getFrmReg(Variable.cUserDocAnalyTRavaux, "WhereCodeEtat", "");

            chkNbHeure_CheckedChanged(chkNbHeure, new System.EventArgs());
            fc_LoadCombo();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadCombo()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("Opérateur");
            dt.Rows.Add("=");
            dt.Rows.Add(">");
            dt.Rows.Add("<");
            dt.Rows.Add(">=");
            dt.Rows.Add("<=");
            Combo4.DataSource = dt;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkNbHeure_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNbHeure.CheckState == CheckState.Checked)
            {
                txtNbHeure.Enabled = true;
                txtNbHeure.BackColor = System.Drawing.ColorTranslator.FromOle(0x8000005);
                //chkDeplacement.Enabled = True
            }
            else
            {
                txtNbHeure.Enabled = false;
                txtNbHeure.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                // chkDeplacement.Enabled = False
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssTRavauxAnaly_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssTRavauxAnaly.DisplayLayout.Bands[0].Columns["N° Fiche Standard"].Hidden = true;
            ssTRavauxAnaly.DisplayLayout.Bands[0].Columns["N° Devis"].Hidden = true;
            ssTRavauxAnaly.DisplayLayout.Bands[0].Columns["Intervenant"].Hidden = true;
            //dt.Columns.Add("N° Fiche Standard");
            //dt.Columns.Add("N° Devis");
            //dt.Columns.Add("Intervenant");
            foreach (var column in ssTRavauxAnaly.DisplayLayout.Bands[0].Columns)
            {
                column.CellActivation = Activation.ActivateOnly;
            }
        }

        private void UserDocAnalyTravaux_Load(object sender, EventArgs e)
        {

        }
    }
}
