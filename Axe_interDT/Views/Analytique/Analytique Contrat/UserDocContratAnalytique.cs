﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using DataTable = System.Data.DataTable;

namespace Axe_interDT.Views.Analytique
{
    public partial class UserDocContratAnalytique : UserControl
    {
        public UserDocContratAnalytique()
        {
            InitializeComponent();
        }

        #region Variables
        Variable.AnalyCtr[] tAnaLyctr;
        Variable.AnalyCtrImm[] tAnalYCtrImm;

        string sAddMAc;
        string sUser;

        bool blnErreur;
        //Const cCheminFichierAnalyCtr = "C:\ErreurAnalyContrat.txt"
        const string cExCodeImmeuble = "Code Immeuble";
        const string cExCode1 = "Gerant";
        const string cExCommercial = "Commercial";
        const string cExRespExploit = "Responsable d'exploitation";
        const string cExSEcteur = "Chef De Secteur";
        const string cExIntervenant = "Intervenant";
        const string cExVente = "Vente";
        const string cExMo = "MO";
        const string cExAchat = "Achat";
        const string cExAchatReel = "Achat Réel";
        const string cExCoef = "Coefficient";
        const string cExGain = "Gain";
        const short cCol = 0;

        string sError;
        #endregion

        //System.Drawing.Font defaultFont;

        //void loopControl(Control C)
        //{
        //    foreach (Control CC in C.Controls)
        //        loopControl(CC);
        //    C.Font = defaultFont;
        //}

        #region methodes
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadCombo()
        {
            object sAddItem = null;
            //Combo4.Rows.ToList().ForEach(r => r.Delete());
            Combo4.DataSource = null;
            DataTable dt = new DataTable();
            dt.Columns.Add("operation");
            dt.Rows.Add("=");
            dt.Rows.Add(">");
            dt.Rows.Add("<");
            dt.Rows.Add(">=");
            dt.Rows.Add("<=");
            Combo4.DataSource = dt;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Contrat()
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                if (fc_CtrlErreurSaisie() == true)
                    return;

                fc_MtCtr(Convert.ToDateTime(txtinterDe.Text), Convert.ToDateTime(txtIntereAu.Text));

                fc_resultat();

                if (blnErreur == true)
                {

                    ModuleAPI.Ouvrir(General.cCheminFichierAnalyCtr);
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Contrat");

                fc_Err(ex.Message);
                if (blnErreur == true)
                {
                    ModuleAPI.Ouvrir(General.cCheminFichierAnalyCtr);
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlErreurSaisie()
        {
            bool functionReturnValue = false;
            //== retourne vrai si une erreur est decelée.
            if (string.IsNullOrEmpty(txtinterDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date réalisée de debut. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (string.IsNullOrEmpty(txtIntereAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date réalisée de fin. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCoef.Text))
            {
                if (!General.IsNumeric(txtCoef.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique pour le coef. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;

                }
                else if (string.IsNullOrEmpty(Combo4.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Sélectionnez dans la liste déroulante un opérateur (=,>=,<=,>,<) pour le coefficient. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;

                }
            }

            if (chkNbHeure.CheckState == CheckState.Checked && !General.IsDate(txtNbHeure.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez coché la case Nb Heurepar defaut, vous devez saisir un nombre d 'heure par defaut.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                return functionReturnValue;
            }


            //-- ouvre le fichier des erreurs
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                File.Delete(General.cCheminFichierAnalyCtr);
            }
            catch (Exception ex) { Program.SaveException(ex); }
            try
            {
                //ouvre le fichier des erreurs constatés.

                // Open cCheminFichierAnalyCtr For Output As #1  //TODO
            }
            catch { }
            // ERROR: Not supported in C#: OnErrorStatement




            lblVentesReelles.Text = "";
            lblMOReel.Text = "";
            lblAchatsReels.Text = "";
            lblChargesReelles.Text = "";
            lblCoefficientReel.Text = "";

            lblVentesPrevues.Text = "";
            lblMOPrevues.Text = "";
            lblAchatsPrevus.Text = "";
            lblChargesPrevues.Text = "";
            lblCoefPrevu.Text = "";

            ssContratAnaly.Visible = false;

            sError = "";
            return functionReturnValue;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="dtDebReal"></param>
        /// <param name="dtFinReal"></param>
        private void fc_MtCtr(System.DateTime dtDebReal, System.DateTime dtFinReal)//TODO this function must be verified
        {

            DataTable rsCtr = default(DataTable);
            DataTable rsAddCtr = default(DataTable);

            string sSQL = null;
            string sCodeImmeuble = null;
            string sWhere = null;

            double dbTot = 0;
            double dbNbjCouvert = 0;
            double dbBase = 0;

            System.DateTime dtFin = default(System.DateTime);

            bool bAddNew = false;

            //== afface la selection precedente.
            sSQL = "DELETE FROM ANC_AnalyCtr WHERE ANC_User='" + sUser + "'";
            General.Execute(sSQL);

            //== Insere dans une table temporaire tous les montants des
            //== contrats groupé par immeuble

            sSQL = "SELECT     ANC_NoAuto, ANC_Mac, ANC_User, CodeImmeuble, Code1, ANC_MtACtualise" +
                   ",  ANC_MatCommercial, ANC_NomComercial, ANC_MatInterv, ANC_NomInterv" +
                   " , ANC_MatChefDeSecteur, ANC_NomChefDeSecteur, ANC_MatRespExploit, ANC_NomRespExploit" +
                   " From ANC_AnalyCtr" + " WHERE     (ANC_NoAuto = 0) ";
            var ModAdoRsAddCr = new ModAdo();
            rsAddCtr = ModAdoRsAddCr.fc_OpenRecordSet(sSQL);


            sSQL = "SELECT Contrat.NumContrat, Contrat.Avenant, Contrat.CodeImmeuble," +
                   " Immeuble.Code1, Contrat.BaseContractuelle, Contrat.BaseActualisee, " +
                   " Contrat.Resiliee,Contrat.DateFin, " +
                   " Commercial.Matricule AS MatCommercial, Commercial.Nom AS NomCommercial," +
                   " Intervenant.Matricule AS MatInterv, Intervenant.Nom AS NomInterv, " +
                   " ChefDeSecteur.Matricule AS MatCDS, ChefDeSecteur.Nom AS NomCDS," +
                   " RespExploit.Matricule AS MatRespExploit, RespExploit.Nom AS NomRespExploit" +
                   " FROM Personnel ChefDeSecteur RIGHT OUTER JOIN" + " Personnel Intervenant RIGHT OUTER JOIN" +
                   " Contrat INNER JOIN" + " Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN" +
                   " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN" +
                   " Personnel Commercial ON Table1.commercial = Commercial.Matricule " +
                   " ON Intervenant.Matricule = Immeuble.CodeDepanneur ON" +
                   " ChefDeSecteur.Initiales = Intervenant.CSecteur LEFT OUTER JOIN" +
                   " Personnel RespExploit ON Intervenant.CRespExploit = RespExploit.Initiales";
            sWhere = "";

            if (!string.IsNullOrEmpty(cmbCodeGerant.Text))//TESTED
            {
                if (string.IsNullOrEmpty(sWhere))//TESTED
                {
                    sWhere = " Where Table1.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + "  And  Table1.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                }

            }

            if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))//TESTED
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " Where Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) +
                             "'";
                }
                else
                {
                    sWhere = sWhere + "  And Immeuble.CodeImmeuble ='" +
                             StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                }

            }

            sWhere = sWhere + " order by  Immeuble.Codeimmeuble";
            // where table1.code1 ='" & gFr_DoublerQuote(cmbCodeGerant) & "' order by immeuble.codeimmeuble"
            var ModAdo = new ModAdo();
            rsCtr = ModAdo.fc_OpenRecordSet(sSQL + sWhere);

            sCodeImmeuble = "";
            DataRow dr1 = null;
            string sCode = "";
            int ANC_NoAuto = 0;
            SqlCommandBuilder cb = new SqlCommandBuilder(ModAdoRsAddCr.SDArsAdo);
            ModAdoRsAddCr.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            ModAdoRsAddCr.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            ModAdoRsAddCr.SDArsAdo.InsertCommand = insertCmd;

            ModAdoRsAddCr.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {
                if (e.StatementType == StatementType.Insert)
                {
                    ANC_NoAuto =
                        Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                }
            });

            foreach (DataRow dr in rsCtr.Rows)
            {
                dbBase = 0;
                //bAddNew = false;
                if (sCodeImmeuble != dr["CodeImmeuble"] + "")//TESTED
                {
                    sCodeImmeuble = dr["CodeImmeuble"] + "";
                    dr1 = rsAddCtr.NewRow();
                    dr1["CodeImmeuble"] = dr["CodeImmeuble"];
                    sCode = dr["Code1"] + "";
                    dr1["Code1"] = dr["Code1"];
                    dr1["ANC_Mac"] = sAddMAc;
                    dr1["ANC_User"] = sUser;
                    dbTot = 0;

                    bAddNew = true;
                }

                //== controle si le contrat pointé en résiliée a une date de fin de contrat.
                if (Convert.ToBoolean(General.nz(dr["Resiliee"], false)) == true && DBNull.Value == dr["DateFin"])//TESTED
                {

                    fc_Err("Le contrat n°" + dr["NumContrat"] + " avenant " + dr["avenant"] + " est pointé en résilié mais ne comporte" + " pas de date de fin, ce contrat ne sera pas pris en compte dans le calcul.");
                    //rsAddCtr.Rows.Add(dr1.ItemArray);
                    //ModAdoRsAddCr.Update();//TODO .
                    if (bAddNew == true)
                    {
                        rsAddCtr.Rows.Add(dr1.ItemArray);
                        ModAdoRsAddCr.Update();
                        bAddNew = false;
                    }
                }
                else if (dr["DateFin"] == DBNull.Value || Convert.ToDateTime(dr["DateFin"]) > dtDebReal)//TESTED
                {
                    //if (bAddNew == true)
                    //{
                    //    rsAddCtr.Rows.Add(dr1.ItemArray);
                    //    ModAdoRsAddCr.Update();
                    //}
                    //== calcul du nbre de jours couvert pour le calcul du montant du contrat.
                    if (dr["DateFin"] != DBNull.Value)//TESTED
                    {
                        if (Convert.ToDateTime(dr["DateFin"]) >= dtDebReal && Convert.ToDateTime(dr["DateFin"]) <= dtFinReal)//TESTED
                        {
                            dbNbjCouvert = Math.Abs((Convert.ToDateTime(dr["DateFin"]) - dtDebReal).TotalDays);
                        }
                        else//TESTED
                        {
                            dbNbjCouvert = Math.Abs((dtDebReal - dtFinReal).TotalDays);
                        }
                    }
                    else//TESTED
                    {
                        dbNbjCouvert = Math.Abs((dtDebReal - dtFinReal).TotalDays);
                    }

                    if (!string.IsNullOrEmpty(General.nz(dr["BaseActualisee"], "") + ""))//TESTED
                    {
                        dbBase = Convert.ToDouble(General.nz(dr["BaseActualisee"], 0));
                    }
                    else if (!string.IsNullOrEmpty(General.nz(dr["BaseContractuelle"], "") + ""))//TESTED
                    {
                        dbBase = Convert.ToDouble(General.nz(dr["BaseContractuelle"], 0));
                    }
                    else
                    {
                        fc_Err("Le contrat n°" + dr["NumContrat"] + " avenant " + dr["avenant"] + " n'a pas de base contractuelle.");
                    }

                    //== sachant qu un montant d'un contrat se fait sur un an
                    //== on fait un prorata de la base contractuelle sur période calculée.
                    dbBase = dbBase / 364;
                    //' dbBase = dbBase / dbNbjCouvert
                    dbBase = dbBase * dbNbjCouvert;
                    dbBase = General.FncArrondir(dbBase, 2);
                    dbTot = dbTot + dbBase;
                    string req = "UPDATE ANC_AnalyCtr SET ";
                    req = req + " ANC_MtACtualise ='" + dbTot + "', ";
                    dr1["ANC_MtACtualise"] = dbTot;
                    //==commercial.
                    if (string.IsNullOrEmpty(General.nz(dr["MatCommercial"], "") + ""))//TESTED
                    {
                        fc_Err("Le gérant " + dr["Code1"] + " n'est pas rattaché à un commercial.");
                    }
                    req = req + "ANC_MatCommercial ='" + dr["MatCommercial"] + "',";
                    req = req + "ANC_NomComercial ='" + dr["NomCommercial"] + "',";
                    dr1["ANC_MatCommercial"] = dr["MatCommercial"];
                    dr1["ANC_NomComercial"] = dr["NomCommercial"];
                    //==intervenant.

                    if (string.IsNullOrEmpty(General.nz(dr["MatInterv"], "") + ""))//TESTED
                    {
                        fc_Err("L'immeuble " + dr["CodeImmeuble"] + " n'est pas rattaché à un intervenant.");
                    }
                    req = req + "ANC_MatInterv = '" + dr["MatInterv"] + "',";
                    req = req + "ANC_NomInterv = '" + dr["NomInterv"] + "',";
                    dr1["ANC_MatInterv"] = dr["MatInterv"];
                    dr1["ANC_NomInterv"] = dr["NomInterv"];
                    //== chef de secteur.

                    if (string.IsNullOrEmpty(General.nz(dr["MatCDS"], "") + "") && !string.IsNullOrEmpty(General.nz(dr["MatInterv"], "") + ""))//TESTED
                    {
                        fc_Err("L'intervenant " + dr["MatInterv"] + " n'est pas rattaché à un chef de secteur.");
                    }
                    req = req + "ANC_MatChefDeSecteur='" + dr["MatCDS"] + "',";
                    req = req + "ANC_NomChefDeSecteur='" + dr["NomCDS"] + "',";
                    dr1["ANC_MatChefDeSecteur"] = dr["MatCDS"];
                    dr1["ANC_NomChefDeSecteur"] = dr["NomCDS"];
                    //==Responsable Exploit.

                    if (string.IsNullOrEmpty(General.nz(dr["MatRespExploit"], "") + "") && !string.IsNullOrEmpty(General.nz(dr["MatInterv"], "") + ""))//TESTED
                    {
                        fc_Err("L'intervenant " + dr["MatInterv"] + " n'est pas rattaché à un responsable d'exploitation.");
                    }
                    req = req + "ANC_MatRespExploit='" + dr["MatRespExploit"] + "',";
                    req = req + "ANC_NomRespExploit='" + dr["NomRespExploit"] + "'";
                    dr1["ANC_MatRespExploit"] = dr["MatRespExploit"];
                    dr1["ANC_NomRespExploit"] = dr["NomRespExploit"];

                    if (bAddNew)
                    {
                        rsAddCtr.Rows.Add(dr1.ItemArray);
                        ModAdoRsAddCr.Update(); //TODO .  
                        bAddNew = false;
                    }
                    else
                    {
                        req = req + " WHERE ANC_NoAuto='" + ANC_NoAuto + "'";
                        General.Execute(req);//TODO. i changed ModAdo.Update() by this line because in vb6 when we don't have rsAddCtr.NewRow() they just update the last inserted line.so i can't do it with ModAdo.update() i changed it to QUERY.
                    }
                    bAddNew = false;
                }

            }
            ModAdo.fc_CloseRecordset(rsAddCtr);
            ModAdo.fc_CloseRecordset(rsCtr);
            rsAddCtr = null;
            rsCtr = null;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sErreur"></param>
        private void fc_Err(string sErreur = "")
        {
            blnErreur = true;

            if (!string.IsNullOrEmpty(sErreur) && !sError.Contains(sErreur))
            {
                File.AppendAllText(General.cCheminFichierAnalyCtr, "\r\n" + sErreur);
                //Print #1, sErreur //TODO i don't understund this line
            }
            sError = sError + sErreur;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_resultat()
        {

            DataTable rs = default(DataTable);

            bool bFirst = false;
            bool bAffiche = false;

            string sNumFicheStandard = null;
            string sNoIntervention = null;
            string sCodeImmeuble = null;
            string sSQL = null;
            string sAddItem = null;
            string sNoBonDeCommande = null;
            string sWhere1 = null;
            string sWhere2 = null;
            string sWhere3 = null;
            string sDureeFormat = null;

            int i = 0;
            int X = 0;
            int lTypeCalcul = 0;

            double dbTotMo = 0;
            double dbTotVente = 0;
            double dbTotAchatReel = 0;
            double totAchat = 0;
            double dbCoef = 0;
            double dbTempTotMO = 0;

            if (OptDefaut.Checked == true)
            {
                lTypeCalcul = 1;

            }
            else if (OptGrille.Checked == true)
            {
                lTypeCalcul = 2;

            }

            ssContratAnaly.Visible = false;

            sSQL = "SELECT ANC_AnalyCtr.ANC_MtACtualise, GestionStandard.NumFicheStandard," +
                   " Intervention.NoIntervention, Intervention.Debourse, BCD_Detail.BCD_PrixHT, " +
                   " Intervention.DateRealise," + " ANC_AnalyCtr.ANC_MatCommercial, ANC_AnalyCtr.ANC_NomComercial," +
                   " ANC_AnalyCtr.ANC_MatInterv,ANC_AnalyCtr.ANC_NomInterv, " +
                   " ANC_AnalyCtr.ANC_MatChefDeSecteur, ANC_AnalyCtr.ANC_NomChefDeSecteur, " +
                   " ANC_AnalyCtr.ANC_MatRespExploit,ANC_AnalyCtr.ANC_NomRespExploit" +
                   " , ANC_AnalyCtr.CodeImmeuble, ANC_AnalyCtr.Code1, BonDeCommande.NoBonDeCommande " +
                   " , Intervention.HeureDebut, Intervention.HeureFin, Intervention.Duree" +
                   " , Intervention.Intervenant" + " FROM         ANC_AnalyCtr LEFT OUTER JOIN" +
                   " GestionStandard LEFT OUTER JOIN" +
                   " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard" +
                   " LEFT OUTER JOIN BonDeCommande " + " ON Intervention.NoIntervention = BonDeCommande.NoIntervention" +
                   " LEFT OUTER JOIN BCD_Detail " + " ON BonDeCommande.NoBonDeCommande = BCD_Detail.BCD_Cle ON" +
                   " ANC_AnalyCtr.CodeImmeuble = GestionStandard.CodeImmeuble" + "";

            sWhere1 = " WHERE (intervention.DateRealise >='" + txtinterDe.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " AND intervention.DateRealise <='" + txtIntereAu.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " and (GestionStandard.NoDevis IS NULL OR GestionStandard.NoDevis = ''" +
                      " and  Intervention.CodeEtat = 'ZZ')";


            sWhere2 = sWhere2 + " or (intervention.DateRealise >='" + txtinterDe.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " AND intervention.DateRealise <='" + txtIntereAu.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " AND Intervention.Article = 'P01'";

            sWhere3 = sWhere3 + " or (intervention.DateRealise >='" + txtinterDe.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " AND intervention.DateRealise <='" + txtIntereAu.DateTime.ToString("dd/MM/yyyy") + "'" +
                      " AND Intervention.Article = 'PO1'";




            if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) +
                          "'";

                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) +
                          "'";

                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) +
                          "'";

            }

            if (!string.IsNullOrEmpty(cmbCodeGerant.Text))//TESTED
            {

                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbCommercial.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.ANC_MatCommercial ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'";
                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.ANC_MatCommercial ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'";
                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.ANC_MatCommercial ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbIntervenant.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.ANC_MatInterv ='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.ANC_MatInterv ='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.ANC_MatInterv ='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbCDS.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.ANC_MatChefDeSecteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbCDS.Text) + "'";
                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.ANC_MatChefDeSecteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbCDS.Text) + "'";
                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.ANC_MatChefDeSecteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbCDS.Text) + "'";

            }

            if (!string.IsNullOrEmpty(cmbRespExploit.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND ANC_AnalyCtr.ANC_MatRespExploit ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbRespExploit.Text) + "'";
                sWhere2 = sWhere2 + " AND ANC_AnalyCtr.ANC_MatRespExploit ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbRespExploit.Text) + "'";
                sWhere3 = sWhere3 + " AND ANC_AnalyCtr.ANC_MatRespExploit ='" +
                          StdSQLchaine.gFr_DoublerQuote(cmbRespExploit.Text) + "'";


            }

            if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))//TESTED
            {
                sWhere1 = sWhere1 + " AND GestionStandard.NumFicheStandard ='" + txtNumFicheStandard.Text + "'";
                sWhere2 = sWhere2 + " AND GestionStandard.NumFicheStandard ='" + txtNumFicheStandard.Text + "'";
                sWhere3 = sWhere3 + " AND GestionStandard.NumFicheStandard ='" + txtNumFicheStandard.Text + "'";

            }

            sWhere1 = sWhere1 + ")";
            sWhere2 = sWhere2 + ")";
            sWhere3 = sWhere3 + ")";

            sSQL = sSQL + sWhere1 + sWhere2 + sWhere3 +
                   " ORDER BY ANC_AnalyCtr.CodeImmeuble, GestionStandard.NumFicheStandard," +
                   " Intervention.NoIntervention, BonDeCommande.NoBonDeCommande";

            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement trouvée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ssContratAnaly.DataSource = null;
                ssContratAnaly.Visible = true;
                return;
            }

            dbCoef = Convert.ToDouble(General.nz(txtCoef.Text, 0));

            tAnaLyctr = null;
            tAnalYCtrImm = null;
            i = 0;
            sCodeImmeuble = "";
            sNoIntervention = "";
            sNumFicheStandard = "";
            bFirst = true;
            //== charge les taux horaires.
            ModCalcul.fc_LoadChargePers("");
            foreach (DataRow dr in rs.Rows)
            {
                if (sNumFicheStandard != General.nz(dr["Numfichestandard"], "") + "")//TESTED
                {
                    sNumFicheStandard = General.nz(dr["Numfichestandard"], "") + "";
                    if (bFirst == true)//tested
                    {
                        i = 0;
                        bFirst = false;
                    }
                    else//tested
                    {
                        i = i + 1;
                    }
                    Array.Resize(ref tAnaLyctr, i + 1);

                    tAnaLyctr[i].sImmeuble = dr["CodeImmeuble"] + "";
                    tAnaLyctr[i].sGerant = dr["Code1"] + "";
                    tAnaLyctr[i].sNumFicheStandard = General.nz(dr["Numfichestandard"], "") + "";
                    tAnaLyctr[i].dbVente = Convert.ToDouble(General.FncArrondir(Convert.ToDouble(General.nz(dr["ANC_MtACtualise"], 0)), 2));
                    tAnaLyctr[i].sMatCommercial = dr["ANC_MatCommercial"] + "";
                    tAnaLyctr[i].sNomCommercial = dr["ANC_NomComercial"] + "";
                    tAnaLyctr[i].sMatInterv = dr["ANC_MatInterv"] + "";
                    tAnaLyctr[i].sNomInterv = dr["ANC_NomInterv"] + "";
                    tAnaLyctr[i].sMatCDS = dr["ANC_MatChefDeSecteur"] + "";
                    tAnaLyctr[i].sNomCDS = dr["ANC_NomChefDeSecteur"] + "";
                    tAnaLyctr[i].sMatRespExploit = dr["ANC_MatRespExploit"] + "";
                    tAnaLyctr[i].sNomRespExploit = dr["ANC_NomRespExploit"] + "";

                }
                if (!string.IsNullOrEmpty(General.nz(dr["Nointervention"], "") + ""))
                {
                    if (sNoIntervention != dr["Nointervention"] + "")
                    {
                        sNoIntervention = dr["Nointervention"] + "";

                        //== modif du 12 avril applique un nbr d heure par defaut si
                        //== l'intervention a été réalisée mais ne contient aucun deboursé calculé sur l 'inter

                        dbTempTotMO = 0;
                        if (chkNbHeure.CheckState == CheckState.Checked)//TESTED
                        {
                            sDureeFormat = txtNbHeure.Text;
                            dbTempTotMO = ModCalcul.fc_calculMo(lTypeCalcul, dr["Duree"] + "", dr["HeureDebut"] + "", dr["HeureFin"] + "", sDureeFormat, dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"] + ""), (chkDeplacement.CheckState == CheckState.Checked ? true : false));
                            tAnaLyctr[i].dbMo = General.FncArrondir(dbTempTotMO, 2);
                            tAnaLyctr[i].dbMo = General.FncArrondir(tAnaLyctr[i].dbMo + dbTempTotMO, 2);
                        }
                        else//tested
                        {
                            dbTempTotMO = ModCalcul.fc_calculMo(lTypeCalcul, dr["Duree"] + "", dr["HeureDebut"] + "", dr["HeureFin"] + "", "", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"] + ""), (chkDeplacement.CheckState == CheckState.Checked ? true : false));
                            tAnaLyctr[i].dbMo = General.FncArrondir(tAnaLyctr[i].dbMo + dbTempTotMO, 2);
                        }
                        tAnaLyctr[i].dbAchat = General.FncArrondir(tAnaLyctr[i].dbAchat + Convert.ToDouble(General.nz(dr["BCD_PrixHT"], 0)), 2);
                    }
                    else//tested
                    {
                        tAnaLyctr[i].dbAchat = General.FncArrondir(tAnaLyctr[i].dbAchat + Convert.ToDouble(General.nz(dr["BCD_PrixHT"], 0)), 2);
                    }
                }
                if (!string.IsNullOrEmpty(General.nz(dr["NoBonDeCommande"], "") + ""))//TESTED
                {
                    if (sNoBonDeCommande != dr["NoBonDeCommande"] + "")
                    {
                        sNoBonDeCommande = dr["NoBonDeCommande"] + "";

                        tAnaLyctr[i].dbAchatReel = Convert.ToDouble(General.nz(ModAdo.fc_ADOlibelle("SELECT Montant From FactFournPied" + " WHERE NoBCmd =" + dr["NoBonDeCommande"]), 0));
                    }
                }
            }
            ModAdo.fc_CloseRecordset(rs);
            rs = null;
            //== injecte un tableau ces meme données mais groupées par immeuble.
            sCodeImmeuble = "";
            X = 0;
            bFirst = true;
            tAnalYCtrImm = null;
            for (i = 0; i < tAnaLyctr.Length; i++)
            {
                if (sCodeImmeuble != tAnaLyctr[i].sImmeuble)//tested
                {
                    sCodeImmeuble = tAnaLyctr[i].sImmeuble;
                    if (bFirst == true)//tested
                    {
                        X = 0;
                        bFirst = false;
                    }
                    else
                    {
                        X = X + 1;
                    }

                    Array.Resize(ref tAnalYCtrImm, X + 1);

                    tAnalYCtrImm[X].sImmeuble = tAnaLyctr[i].sImmeuble;
                    tAnalYCtrImm[X].sGerant = tAnaLyctr[i].sGerant;

                    tAnalYCtrImm[X].sMatCommercial = tAnaLyctr[i].sMatCommercial;
                    tAnalYCtrImm[X].sNomCommercial = tAnaLyctr[i].sNomCommercial;

                    tAnalYCtrImm[X].sMatRespExploit = tAnaLyctr[i].sMatRespExploit;
                    tAnalYCtrImm[X].sNomRespExploit = tAnaLyctr[i].sNomRespExploit;

                    tAnalYCtrImm[X].sMatCDS = tAnaLyctr[i].sMatCDS;
                    tAnalYCtrImm[X].sNomCDS = tAnaLyctr[i].sNomCDS;

                    tAnalYCtrImm[X].sMatInterv = tAnaLyctr[i].sMatInterv;
                    tAnalYCtrImm[X].sNomInterv = tAnaLyctr[i].sNomInterv;
                    tAnalYCtrImm[X].dbVente = tAnaLyctr[i].dbVente;
                }
                //== stocke les montants.
                tAnalYCtrImm[X].dbAchat = tAnalYCtrImm[X].dbAchat + tAnaLyctr[i].dbAchat;
                tAnalYCtrImm[X].dbAchatReel = tAnalYCtrImm[X].dbAchatReel + tAnaLyctr[i].dbAchatReel;
                tAnalYCtrImm[X].dbMo = tAnalYCtrImm[X].dbMo + tAnaLyctr[i].dbMo;

                if (tAnalYCtrImm[X].dbVente == 0 && tAnalYCtrImm[X].dbMo == 0 && tAnalYCtrImm[X].dbAchatReel == 0)//tested
                {
                    tAnalYCtrImm[X].dbCoef = 0;
                }
                else//tested
                {
                    tAnalYCtrImm[X].dbCoef = tAnalYCtrImm[X].dbVente / ((tAnalYCtrImm[X].dbMo + tAnalYCtrImm[X].dbAchatReel) == 0 ? tAnalYCtrImm[X].dbVente : (tAnalYCtrImm[X].dbMo + tAnalYCtrImm[X].dbAchatReel));
                }
                tAnalYCtrImm[X].dbGain = tAnalYCtrImm[X].dbVente - (tAnalYCtrImm[X].dbMo + tAnalYCtrImm[X].dbAchatReel);
            }
            //== alimente le tableau.
            dbTotMo = 0;
            dbTotVente = 0;
            dbTotAchatReel = 0;
            //ssContratAnaly.DataSource = null;

            ssContratAnaly.Visible = false;
            DataTable dt = new DataTable();
            dt.Columns.Add("first");
            dt.Columns.Add("CodeImmeuble");
            dt.Columns.Add("Gerant");
            dt.Columns.Add("Commercial");
            dt.Columns.Add("RespExploit");
            dt.Columns.Add("Secteur");
            dt.Columns.Add("Intervenant");
            dt.Columns.Add("Vente");
            dt.Columns.Add("MO");
            dt.Columns.Add("Achat");
            dt.Columns.Add("AchatReel");
            dt.Columns.Add("Coef");
            dt.Columns.Add("Gain");
            for (i = 0; i < tAnalYCtrImm.Length; i++)
            {
                var _with4 = tAnalYCtrImm[i];
                bAffiche = false;
                if (!string.IsNullOrEmpty(txtCoef.Text))
                {
                    switch (Combo4.Text)
                    {
                        case "=":
                            if (General.FncArrondir(_with4.dbCoef, 3) == dbCoef)
                            {
                                bAffiche = true;
                            }
                            break;

                        case ">":
                            if (General.FncArrondir(_with4.dbCoef, 3) > dbCoef)
                            {
                                bAffiche = true;
                            }
                            break;

                        case ">=":
                            if (General.FncArrondir(_with4.dbCoef, 3) >= dbCoef)
                            {
                                bAffiche = true;
                            }
                            break;

                        case "<":
                            if (General.FncArrondir(_with4.dbCoef, 3) < dbCoef)
                            {
                                bAffiche = true;
                            }
                            break;

                        case "<=":
                            if (General.FncArrondir(_with4.dbCoef, 3) <= dbCoef)
                            {
                                bAffiche = true;
                            }
                            break;

                    }
                }
                else//TESTED
                {
                    bAffiche = true;
                }
                if (bAffiche == true)//TESTED
                {
                    var dr = dt.NewRow();
                    dr["first"] = "";
                    dr["CodeImmeuble"] = _with4.sImmeuble;
                    dr["Gerant"] = _with4.sGerant;
                    dr["Commercial"] = _with4.sNomCommercial;
                    dr["RespExploit"] = _with4.sNomRespExploit;
                    dr["Secteur"] = _with4.sNomCDS;
                    dr["Intervenant"] = _with4.sNomInterv;
                    dr["Vente"] = General.FncArrondir(_with4.dbVente, 2); //== Vente.
                    dbTotVente = dbTotVente + General.FncArrondir(_with4.dbVente, 2);
                    //== Mo.
                    dr["MO"] = General.FncArrondir(_with4.dbMo, 2);
                    dbTotMo = dbTotMo + General.FncArrondir(_with4.dbMo, 2);
                    //== achat previsionnel.
                    dr["Achat"] = General.FncArrondir(_with4.dbAchat, 2);
                    totAchat = totAchat + General.FncArrondir(_with4.dbAchat, 2);
                    //==Achat Réel.
                    dr["AchatReel"] = General.FncArrondir(_with4.dbAchatReel, 2);
                    dbTotAchatReel = dbTotAchatReel + General.FncArrondir(_with4.dbAchatReel, 2);
                    dr["Coef"] = General.FncArrondir(_with4.dbCoef, 3);
                    dr["Gain"] = General.FncArrondir(_with4.dbGain, 3);
                    dt.Rows.Add(dr.ItemArray);

                }
            }
            ssContratAnaly.DataSource = dt;
            ssContratAnaly.UpdateData();
            ssContratAnaly.Visible = true;
            lblVentesReelles.Text = Convert.ToString(General.FncArrondir(dbTotVente, 2));
            lblMOReel.Text = Convert.ToString(General.FncArrondir(dbTotMo, 2));
            lblAchatsReels.Text = Convert.ToString(General.FncArrondir(dbTotAchatReel, 2));
            lblChargesReelles.Text = Convert.ToString(General.FncArrondir(dbTotMo + dbTotAchatReel, 2));
            if (lblChargesReelles.Text == "0")
            {
                if (dbTotVente != 0)
                {
                    lblCoefficientReel.Text = Convert.ToString(General.FncArrondir(dbTotVente / dbTotVente, 3));
                }
            }
            else//TESTED
            {
                lblCoefficientReel.Text = Convert.ToString(General.FncArrondir(dbTotVente / Convert.ToDouble(lblChargesReelles.Text), 3));
            }
            lblVentesPrevues.Text = Convert.ToString(General.FncArrondir(dbTotVente, 2));
            lblMOPrevues.Text = Convert.ToString(General.FncArrondir(dbTotMo, 2));
            lblAchatsPrevus.Text = Convert.ToString(General.FncArrondir(totAchat, 2));
            lblChargesPrevues.Text = Convert.ToString(General.FncArrondir(dbTotMo + totAchat, 2));
            if (lblChargesPrevues.Text == "0")
            {
                if (dbTotVente != 0)
                {
                    lblCoefPrevu.Text = Convert.ToString(General.FncArrondir(dbTotVente / dbTotVente, 3));
                }
            }
            else//TESTED
            {
                lblCoefPrevu.Text = Convert.ToString(General.FncArrondir(dbTotVente / Convert.ToDouble(lblChargesPrevues.Text), 3));
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_ExportExel()
        {
            double dbNbCont = 0;
            double dbCoef = 0;
            string sMatricule = null;

            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);
            ADODB.Field oField = default(ADODB.Field);

            int i = 0;
            int j = 0;
            int X = 0;
            int lLigne = 0;
            bool bAffiche = false;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;



                // Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                //== affiche les noms des champs.

                i = 1;
                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExCodeImmeuble;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExCode1;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExCommercial;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExRespExploit;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExSEcteur;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExIntervenant;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExVente;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExMo;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExAchat;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExAchatReel;
                i = i + 1;



                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExCoef;
                i = i + 1;


                oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;

                oSheet.Cells[1, i].value = cExGain;
                i = i + 1;



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //    sMatricule = UCase(rsPorteFeuille.Fields(sNameMat).value) & ""


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (General.IsNumeric(txtCoef.Text))
                {
                    dbCoef = Convert.ToDouble(txtCoef.Text);
                }
                //  oSheet.Cells(lLigne, 1).value = sANT_MatCode

                for (X = 0; X < tAnalYCtrImm.Length; X++)
                {
                    bAffiche = false;
                    if (!string.IsNullOrEmpty(txtCoef.Text))
                    {
                        switch (Combo4.Text)
                        {
                            case "=":
                                if (General.FncArrondir(tAnalYCtrImm[X].dbCoef, 3) == dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">":
                                if (General.FncArrondir(tAnalYCtrImm[X].dbCoef, 3) > dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case ">=":
                                if (General.FncArrondir(tAnalYCtrImm[X].dbCoef, 3) >= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<":
                                if (General.FncArrondir(tAnalYCtrImm[X].dbCoef, 3) < dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                            case "<=":
                                if (General.FncArrondir(tAnalYCtrImm[X].dbCoef, 3) <= dbCoef)
                                {
                                    bAffiche = true;
                                }
                                break;

                        }
                    }
                    else
                    {
                        bAffiche = true;
                    }
                    if (bAffiche == true)
                    {
                        j = 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sImmeuble;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sGerant;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sNomCommercial;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sMatRespExploit;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sNomCDS;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].sNomInterv;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbVente;
                        j = j + 1;
                        //==

                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbMo;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbAchat;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbAchatReel;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbCoef;
                        j = j + 1;


                        oSheet.Cells[lLigne, j + cCol].value = tAnalYCtrImm[X].dbGain;
                        j = j + 1;

                        lLigne = lLigne + 1;
                    }
                }
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Error: " + ex.HResult, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        #region events
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkNbHeure_CheckedChanged(object sender, EventArgs e)
        {

            if (chkNbHeure.Checked)
            {
                txtNbHeure.Enabled = true;
                txtNbHeure.BackColor = System.Drawing.Color.White;
                chkDeplacement.Enabled = true;
            }
            else
            {
                txtNbHeure.Enabled = false;
                txtNbHeure.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                chkDeplacement.Enabled = false;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibChefDeSecteur.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCDS.Value + "'");

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbCDS, General.sSQL, "Matricule");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyascii = (short)e.KeyChar;
            try
            {
                if (keyascii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCDS.Value + "'"), "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        var _with5 = new SearchTemplate(null, null, req, "", "");
                        _with5.SetValues(new Dictionary<string, string>() { { General.IsNumeric(cmbCDS.Text) ? "Matricule" : "Nom", cmbCDS.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCDS.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibChefDeSecteur.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                cmbCDS.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                lblLibChefDeSecteur.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                _with5.Close();
                            }
                        };
                        _with5.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeGerant.Rows.Count < 1)
            {
                var _with6 = this;
                General.sSQL = "SELECT Table1.Code1 AS [Code Client], Table1.Adresse1," + " Table1.CodePostal, Table1.Ville" + " From Table1" + " ORDER BY Code1";
                sheridan.InitialiseCombo(_with6.cmbCodeGerant, General.sSQL, "Code Client");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataTable rsGerant = default(DataTable);
            bool blnGoRecherche = false;
            var keyascii = (short)e.KeyChar;
            ModAdo ModAdo = null;
            try
            {
                if (keyascii == 13)
                {
                    rsGerant = new DataTable();
                    ModAdo = new ModAdo();
                    rsGerant = ModAdo.fc_OpenRecordSet("SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 LIKE '%" + cmbCodeGerant.Value + "%'");
                    if (rsGerant.Rows.Count > 0)
                    {
                        if (rsGerant.Rows.Count > 1)
                            blnGoRecherche = true;
                    }
                    else
                    {
                        blnGoRecherche = true;
                    }
                    if (blnGoRecherche)
                    {
                        string req = "SELECT Code1 as \"Code Gérant\"," + " Adresse1 as \"Adresse\"," + " Ville as \"Ville\" , CodePostal AS \"Code Postal\" FROM Table1 ";
                        var _with5 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'un gérant" };
                        _with5.SetValues(new Dictionary<string, string>() { { "Code1", cmbCodeGerant.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCodeGerant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Code Gérant") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)e.KeyChar == 13)
                            {
                                cmbCodeGerant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Code Gérant") + "";
                                _with5.Close();
                            }

                        };
                        _with5.ShowDialog();
                    }
                }
                ModAdo.fc_CloseRecordset(rsGerant);
                rsGerant = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCodeGerant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeimmeuble.Rows.Count < 1)
            {
                var _with8 = this;
                General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse," + " Immeuble.CodePostal, Immeuble.Ville" + " From Immeuble" + " order by CodeImmeuble";
                sheridan.InitialiseCombo(_with8.cmbCodeimmeuble, General.sSQL, "codeImmeuble");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyascii = (short)e.KeyChar;
            try
            {
                if (keyascii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + cmbCodeimmeuble.Text + "'"), "") + ""))
                    {
                        string req = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";
                        var _with5 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'un immeuble" };
                        _with5.SetValues(new Dictionary<string, string>() { { "CodeImmeuble", cmbCodeimmeuble.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCodeimmeuble.Text = _with5.ugResultat.ActiveRow.GetCellValue("Code Immeuble") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)e.KeyChar == 13)
                            {
                                cmbCodeimmeuble.Text = _with5.ugResultat.ActiveRow.GetCellValue("Code Immeuble") + "";
                                _with5.Close();
                            }
                        };
                        _with5.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibCommercial.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCommercial.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string req = null;
            int i = 0;
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            if (General.CodeQualifCommercial.Length > 0)
            {
                req = req + " WHERE ";
                for (i = 0; i < General.CodeQualifCommercial.Length; i++)
                {
                    if (i < General.CodeQualifCommercial.Length - 1)
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                    }
                    else
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                    }
                }
            }

            General.sSQL = General.sSQL + req + " ORDER BY Personnel.Nom";

            sheridan.InitialiseCombo(cmbCommercial, General.sSQL, "Matricule");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyascii = (short)e.KeyChar;
            try
            {
                if (keyascii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule = '" + cmbCommercial.Value + "'"), "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        var _with5 = new SearchTemplate(null, null, req, "", "");
                        _with5.SetValues(new Dictionary<string, string>() { { General.IsNumeric(cmbCommercial.Text) ? "Matricule" : "Nom", cmbCommercial.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCommercial.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibCommercial.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)e.KeyChar == 13)
                            {
                                cmbCommercial.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                lblLibCommercial.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                _with5.Close();
                            }
                        };
                        _with5.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibIntervenant.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyascii = (short)e.KeyChar;
            try
            {
                if (keyascii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'"), "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        var _with5 = new SearchTemplate(null, null, req, "", "");
                        _with5.SetValues(new Dictionary<string, string>() { { General.IsNumeric(cmbIntervenant.Text) ? "Matricule" : "Nom", cmbIntervenant.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbIntervenant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibIntervenant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                cmbIntervenant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                lblLibIntervenant.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                _with5.Close();
                            }

                        };
                        _with5.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibRespExploit.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbRespExploit.Text + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " order by matricule";

            sheridan.InitialiseCombo(cmbRespExploit, General.sSQL, "Matricule");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_KeyPress(object sender, KeyPressEventArgs e)
        {
            var keyascii = (short)e.KeyChar;
            try
            {
                if (keyascii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbRespExploit.Value + "'"), "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        var _with5 = new SearchTemplate(null, null, req, "", "");
                        _with5.SetValues(new Dictionary<string, string>() { { General.IsNumeric(cmbRespExploit.Text) ? "Matricule" : "Nom", cmbRespExploit.Text } });
                        _with5.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbRespExploit.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibRespExploit.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            _with5.Close();
                        };
                        _with5.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                cmbRespExploit.Text = _with5.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                lblLibRespExploit.Text = _with5.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                _with5.Close();
                            }
                        };
                        _with5.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCDS_Click(object sender, EventArgs e)
        {
            cmbCDS_KeyPress(cmbCDS, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCtr_Click(object sender, EventArgs e)
        {
            fc_Contrat();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGerant_Click(object sender, EventArgs e)
        {
            cmbCodeGerant_KeyPress(cmbCodeGerant, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheAppel_Click(object sender, EventArgs e)
        {
            try
            {
                var req = "SELECT GestionStandard.NumFicheStandard as \"No Fiche standard\"," + " GestionStandard.Utilisateur as \"Utilisateur\"," + " GestionStandard.DateAjout as \"date\", GestionStandard.CodeParticulier as \"Client\"," + " GestionStandard.CodeImmeuble as \"Référence immeuble\" From GestionStandard";
                var search = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'une fiche d'appel" };
                search.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    txtNumFicheStandard.Text = search.ugResultat.ActiveRow.GetCellValue("No Fiche standard") + "";
                    search.Close();
                };
                search.ugResultat.KeyPress += (se, ev) =>
                {
                    if ((short)ev.KeyChar == 13)
                    {
                        // charge les enregistrements de l'immeuble
                        txtNumFicheStandard.Text = search.ugResultat.ActiveRow.GetCellValue("No Fiche standard") + "";
                        search.Close();
                    }
                };
                search.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Rechercher");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            cmbCommercial_KeyPress(cmbCommercial, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRespExploit_Click(object sender, EventArgs e)
        {
            cmbRespExploit_KeyPress(cmbRespExploit, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdvisu_Click(object sender, EventArgs e)
        {
            bool bFind = false;
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous exporter sous Excel ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                fc_ExportExel();

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez une mauvaise version d'Excel.", "Opération annulée", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
        }

        private void cmdvisu_VisibleChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssContratAnaly_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //foreach (var column in ssContratAnaly.DisplayLayout.Bands[0].Columns)
            //{

            //}
            ssContratAnaly.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().ToList().ForEach(c => c.CellActivation = Activation.ActivateOnly);
            ssContratAnaly.DisplayLayout.Bands[0].Columns["first"].Hidden = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_ValueChanged(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibIntervenant.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCDS_ValueChanged(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibChefDeSecteur.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCDS.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRespExploit_ValueChanged(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibRespExploit.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbRespExploit.Text + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_ValueChanged(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibCommercial.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCommercial.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocContratAnalytique_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible) return;
            sAddMAc = ModuleAPI.GetMACAddress();
            sUser = General.fncUserName();
            fc_LoadCombo();
            chkNbHeure_CheckedChanged(chkNbHeure, new System.EventArgs());
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocContratAnalytique");
        }

        private void UserDocContratAnalytique_Load(object sender, EventArgs e)
        {
            //defaultFont=General.GetFont();
            //loopControl(this);
        }
        #endregion
    }
}
