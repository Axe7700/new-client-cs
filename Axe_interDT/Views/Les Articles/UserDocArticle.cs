﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Les_Articles.Forms;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Les_Articles
{
    public partial class UserDocArticle : UserControl
    {
        short intTemp;
        short ok2;
        object vartemp;
        bool ok;
        bool blnKeyPress;
        DataTable adors;
        SqlDataAdapter SDAadors;
        DataTable rsArticle;
        SqlDataAdapter SDArsArticle;
        SqlCommandBuilder SCBrsArticle;
        ModAdo tmpAdorsArticle;

        bool bLoad = true;
        public UserDocArticle()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAnalytiqueAcitvite_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbRecherche_Click(object sender, EventArgs e)
        {
            string sWhere = null;
            sWhere = "";
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT CodeArticle , CodeCategorieArticle, COT_Code,COT2_Code, ChaudiereCondensation,TYI_Code,ALAR_Code ,Correspondance, Masquer, Designation1"
                    + ", totalHT, FAMD_Code, FAMSD_Code, Mobilite " + ",Urgent, SaisieCompteur, ";
                General.sSQL = General.sSQL + "Designation2 , Designation3, Designation4, Designation5, Designation6, Designation7,";
                General.sSQL = General.sSQL + "Designation8 , Designation9, Designation10, CG_Num, CG_Intitule, CG_Num2, CG_Intitule2,";
                General.sSQL = General.sSQL + "ca_Num, CA_INTITULE,AutoLiquidation, PrixAchat, AncienPrixAchat, DateChangement , Coefficient, PrixRevient, Debit, Encaissement, Taux";
                General.sSQL = General.sSQL + " FROM FacArticle";

                var _with1 = this;

                if (!string.IsNullOrEmpty(_with1.cmbCodeArticle.Text))
                {
                    //Vérification de la présence d'une "*" pour la recherche approximative
                    if (_with1.cmbCodeArticle.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //Remplacement de "*" par "%" : critère SQL
                            sWhere = " WHERE CodeArticle like '" + _with1.cmbCodeArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeArticle like '" + _with1.cmbCodeArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CodeArticle ='" + _with1.cmbCodeArticle.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeArticle ='" + _with1.cmbCodeArticle.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.cmbFamille.Text))
                {
                    //Vérification de la présence d'une "*" pour la recherche approximative
                    if (_with1.cmbFamille.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //Remplacement de "*" par "%" : critère SQL
                            sWhere = " WHERE FAMD_Code like '" + _with1.cmbFamille.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND FAMD_Code like '" + _with1.cmbFamille.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE FAMD_Code ='" + _with1.cmbFamille.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND FAMD_Code ='" + _with1.cmbFamille.Text + "'";
                        }
                    }
                }


                if (!string.IsNullOrEmpty(_with1.cmbSousFamille.Text))
                {
                    //Vérification de la présence d'une "*" pour la recherche approximative
                    if (_with1.cmbSousFamille.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //Remplacement de "*" par "%" : critère SQL
                            sWhere = " WHERE FAMSD_Code like '" + _with1.cmbSousFamille.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND FAMSD_Code like '" + _with1.cmbSousFamille.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE FAMSD_Code ='" + _with1.cmbSousFamille.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND FAMSD_Code ='" + _with1.cmbSousFamille.Text + "'";
                        }
                    }
                }


                if (!string.IsNullOrEmpty(_with1.SSCategorieArticle.Text))
                {
                    if (_with1.SSCategorieArticle.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CodeCategorieArticle like '" + _with1.SSCategorieArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeCategorieArticle ='" + _with1.SSCategorieArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CodeCategorieArticle ='" + _with1.SSCategorieArticle.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeCategorieArticle ='" + _with1.SSCategorieArticle.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.ssTypeArticle.Text))
                {
                    if (_with1.ssTypeArticle.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CodeTypeArticle like '" + _with1.ssTypeArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeTypeArticle ='" + _with1.ssTypeArticle.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CodeTypeArticle ='" + _with1.ssTypeArticle.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CodeTypeArticle ='" + _with1.ssTypeArticle.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.cmbTxPlein.Text))
                {
                    if (_with1.cmbTxPlein.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CG_Num2 like '" + _with1.cmbTxPlein.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CG_Num2 like '" + _with1.cmbTxPlein.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CG_Num2 ='" + _with1.cmbTxPlein.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CG_Num2 ='" + _with1.cmbTxPlein.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.cmbTxReduit.Text))
                {
                    if (_with1.cmbTxReduit.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CG_Num like '" + _with1.cmbTxReduit.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CG_Num like '" + _with1.cmbTxReduit.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE CG_Num ='" + _with1.cmbTxReduit.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND CG_Num ='" + _with1.cmbTxReduit.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.cmbAnalytiqueAcitvite.Text))
                {
                    if (_with1.cmbAnalytiqueAcitvite.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE ca_Num like '" + _with1.cmbAnalytiqueAcitvite.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND ca_Num like '" + _with1.cmbAnalytiqueAcitvite.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE ca_Num ='" + _with1.cmbAnalytiqueAcitvite.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND ca_Num ='" + _with1.cmbAnalytiqueAcitvite.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.Text16.Text))
                {
                    if (_with1.Text16.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE PrixAchat like '" + _with1.Text16.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND PrixAchat like '" + _with1.Text16.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE PrixAchat ='" + _with1.Text16.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND PrixAchat ='" + _with1.Text16.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.Text18.Text))
                {
                    if (_with1.Text18.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE DateChangement like '" + _with1.Text18.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND DateChangement like '" + _with1.Text18.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE DateChangement ='" + _with1.Text18.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND DateChangement ='" + _with1.Text18.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.Text19.Text))
                {
                    if (_with1.Text19.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Coefficient like '" + _with1.Text19.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Coefficient like '" + _with1.Text19.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Coefficient ='" + _with1.Text19.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Coefficient ='" + _with1.Text19.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with1.Text20.Text))
                {
                    if (_with1.Text20.Text.Contains("*"))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE PrixRevient like '" + _with1.Text20.Text.Replace("*", "%").ToString() + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND PrixRevient like '" + _with1.Text20.Text.Replace("*", "%").ToString() + "'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE PrixRevient ='" + _with1.Text20.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND PrixRevient ='" + _with1.Text20.Text + "'";
                        }
                    }
                }

                //===========> Mondir 27.05.2020, rsArticle.Dispose(); stop the app when the grid is in edit mode
                //if ((rsArticle != null))
                //{

                //    rsArticle.Dispose();

                //}
                //else
                //{
                //    rsArticle = new DataTable();
                //}

                rsArticle = new DataTable();

                tmpAdorsArticle = new ModAdo();
                rsArticle = tmpAdorsArticle.fc_OpenRecordSet(General.sSQL + sWhere);
                GridArticle.DataSource = rsArticle;
                bLoad = true;
                GridArticle.UpdateData();
                bLoad = false;
                var _with2 = this;
                General.saveInReg(Variable.cUserDocArticle, "CodeArticle", _with2.cmbCodeArticle.Text);
                General.saveInReg(Variable.cUserDocArticle, "CodeCategorieArticle", _with2.SSCategorieArticle.Text);
                General.saveInReg(Variable.cUserDocArticle, "CodeTypeArticle", _with2.ssTypeArticle.Text);
                General.saveInReg(Variable.cUserDocArticle, "TauxPlein", _with2.cmbTxPlein.Text);
                General.saveInReg(Variable.cUserDocArticle, "TauxReduit", _with2.cmbTxReduit.Text);
                General.saveInReg(Variable.cUserDocArticle, "AnalytiqueActivite", _with2.cmbAnalytiqueAcitvite.Text);
                General.saveInReg(Variable.cUserDocArticle, "PrixAchat", _with2.Text16.Text);
                General.saveInReg(Variable.cUserDocArticle, "DateChangement", _with2.Text18.Text);
                General.saveInReg(Variable.cUserDocArticle, "Coefficient", _with2.Text19.Text);
                General.saveInReg(Variable.cUserDocArticle, "PrixRevient", _with2.Text20.Text);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " cmbRecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTxPlein_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTxReduit_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFamille_Click(object sender, EventArgs e)
        {
            frmFamilleDevis frm = new frmFamilleDevis();
            frm.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            try
            {
                GridArticle.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdSauver_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridArticle_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            blnKeyPress = false;
            if (bLoad) return;
            int xx = tmpAdorsArticle.Update();


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridArticle_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {

                string NameCol = GridArticle.ActiveCell.Column.Key.ToString().ToUpper();

                if (NameCol == "ChaudiereCondensation".ToString().ToUpper())
                {
                    if (GridArticle.ActiveRow.Cells[NameCol].Text == "-1")
                    {
                        GridArticle.ActiveRow.Cells[NameCol].Value = 1;
                    }
                }

                if (NameCol == "Urgent".ToString().ToUpper())
                {
                    if (GridArticle.ActiveRow.Cells[NameCol].Text == "-1")
                    {
                        GridArticle.ActiveRow.Cells[NameCol].Value = 1;
                    }
                }

                if (NameCol == "SaisieCompteur".ToString().ToUpper())
                {
                    if (GridArticle.ActiveRow.Cells[NameCol].Text == "-1")
                    {
                        GridArticle.ActiveRow.Cells[NameCol].Value = 1;
                    }
                }

                if (NameCol == "Correspondance".ToString().ToUpper())
                {
                    if (GridArticle.ActiveRow.Cells[NameCol].Text == "-1")
                    {
                        GridArticle.ActiveRow.Cells[NameCol].Value = 1;
                    }
                }

                if (NameCol == "Mobilite".ToString().ToUpper())
                {
                    if (GridArticle.ActiveRow.Cells[NameCol].Text == "-1")
                    {
                        GridArticle.ActiveRow.Cells[NameCol].Value = 1;
                    }
                }

                if (NameCol == "FAMD_Code".ToString().ToUpper())
                {
                    fc_SousFamille(GridArticle.ActiveRow.Cells["FAMD_Code"].Text);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridArticle_BeforeColUpdate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridArticle_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            try
            {
                if (bLoad) return;// added to stop this event from firing when the search button clicked
                //=== si l'article est de type contrat,
                //=== on force l'utilisateur à saisir quel type de contrat il s'agit.
                if (e.Row.Cells["CodeCategorieArticle"].Value.ToString().ToUpper() == "P".ToString().ToUpper())
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["COT_Code"].Value.ToString().ToUpper()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour les articles contrat vous devez préciser le type de contrat (P1, P2, P3 ou P4).", "Saisie incomplète", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Row.Cells["COT_Code"].Activate();
                        e.Cancel = true;
                        return;
                    }
                    if (string.IsNullOrEmpty(e.Row.Cells["COT2_Code"].Value.ToString().ToUpper()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour les articles contrat vous devez préciser le type de contrat 2.", "Saisie incomplète", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Row.Cells["COT2_Code"].Activate();
                        e.Cancel = true;
                        return;
                    }

                }

                //=== si l'article est de type intervention,
                //=== on force l'utilisateur à saisir quel type de panne il s'agit.
                if (e.Row.Cells["CodeCategorieArticle"].Value.ToString().ToUpper() == "I".ToString().ToUpper())
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["TYI_Code"].Text.ToString().ToUpper()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour les articles intervention vous devez préciser le type de de panne.", "Saisie incomplète", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Row.Cells["TYI_Code"].Activate();
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        e.Row.Cells["TYI_Code"].Value = e.Row.Cells["TYI_Code"].Text.ToString().Trim();
                    }

                }
                if (string.IsNullOrEmpty(e.Row.Cells["CodeArticle"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code Article est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //e.Row.CancelUpdate();
                    e.Row.Cells["CodeArticle"].Activate();
                    e.Cancel = true;
                    return;
                }
                if (e.Row.Cells["CodeArticle"].DataChanged)
                {
                    if (e.Row.Cells["CodeArticle"].OriginalValue.ToString() != e.Row.Cells["CodeArticle"].Text)
                    {
                        using (var tmpModAdo = new ModAdo())
                        {
                            var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FacArticle WHERE CodeArticle= '{e.Row.Cells["CodeArticle"].Text}' "));
                            if (inDb > 0)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                e.Row.CancelUpdate();
                                e.Cancel = true;
                            }
                        }
                    }
                }



            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridArticle_BeforeRowUpdate");
            }
        }

        private void GridArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void ssTypeArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text10_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text16_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text18_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text18_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text18.Text) && !(General.IsDate(Text18.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text18.Focus();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text19_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text19_Leave(object sender, EventArgs e)
        {
            if (!(General.IsNumeric(Text19.Text)) && !string.IsNullOrEmpty(Text19.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur du coefficient n'est pas une valeur numerique." + "\n" + "Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text19.Focus();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text20_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            }
        }

        private void UserDocArticle_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocArticle");
            /*if (ModMain.bActivate == true)
            {*/
            //    For Each objcontrole In Me.Controls
            //       If TypeOf objcontrole Is SSOleDBGrid Then
            //          fc_LoadDimensionGrille Me.Name, objcontrole
            //       End If
            //    Next


            General.open_conn();
            /*

            if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
            {
                imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                if (Information.IsNumeric(General.imgTop))
                {
                    imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                }
                if (Information.IsNumeric(General.imgLeft))
                {
                    imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                }
            }

            if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
            {
                lblNomSociete.Text = General.NomSousSociete;
                System.Windows.Forms.Application.DoEvents();
            }
            */

            //    sSql = ""
            //    sSql = "SELECT CodeArticle , CodeCategorieArticle, Designation1,"
            //    sSql = sSql & "Designation2 , Designation3, Designation4, Designation5, Designation6, Designation7,"
            //    sSql = sSql & "Designation8 , Designation9, Designation10, CG_Num, CG_Intitule, CG_Num2, CG_Intitule2,"
            //    sSql = sSql & "ca_Num, CA_INTITULE, PrixAchat, AncienPrixAchat, DateChangement , Coefficient, PrixRevient, Debit, Encaissement, Taux"
            //    sSql = sSql & " FROM FacArticle"
            //
            //    Set rsArticle = New ADODB.Recordset
            //    rsArticle.Open sSql, adocnn, adOpenKeyset, adLockOptimistic
            //
            //    Me.GridArticle.ReBind

            fc_ChargeCombo();
            var _with9 = this;
            _with9.cmbCodeArticle.Text = General.getFrmReg(Variable.cUserDocArticle, "CodeArticle", "");
            _with9.SSCategorieArticle.Text = General.getFrmReg(Variable.cUserDocArticle, "CodeCategorieArticle", "");
            _with9.ssTypeArticle.Text = General.getFrmReg(Variable.cUserDocArticle, "CodeTypeArticle", "");
            _with9.cmbTxPlein.Text = General.getFrmReg(Variable.cUserDocArticle, "TauxPlein", "");
            _with9.cmbTxReduit.Text = General.getFrmReg(Variable.cUserDocArticle, "TauxReduit", "");
            _with9.cmbAnalytiqueAcitvite.Text = General.getFrmReg(Variable.cUserDocArticle, "AnalytiqueActivite", "");
            _with9.Text16.Text = General.getFrmReg(Variable.cUserDocArticle, "PrixAchat", "");
            _with9.Text18.Text = General.getFrmReg(Variable.cUserDocArticle, "DateChangement", "");
            _with9.Text19.Text = General.getFrmReg(Variable.cUserDocArticle, "Coefficient", "");
            _with9.Text20.Text = General.getFrmReg(Variable.cUserDocArticle, "PrixRevient", "");
            cmbRecherche_Click(cmbRecherche, new System.EventArgs());
            /* lblNavigation[0].Text = General.sNomLien0;
             lblNavigation[1].Text = General.sNomLien1;
             lblNavigation[2].Text = General.sNomLien2;*/

            ModAutorisation.fc_DroitParFiche(this.Name);

            // }
            // ModMain.bActivate = false;

            //Set adocnn = New ADODB.Connection
            //    adocnn.Open CHEMINBASE
            //
            //With Adodc1
            //    .ConnectionString = CHEMINBASE
            //    .RecordSource = "SELECT FacArticle.* From FacArticle ORDER BY FacArticle.CodeArticle"
            //    .Refresh
            //End With
            //With Adodc2
            //    .ConnectionString = CHEMINBASE
            //    .RecordSource = "Select * from FacArticleCategorie"
            //    .Refresh
            //End With

        }
        /// <summary>
        /// Testé
        /// </summary>
        public void fc_ChargeCombo()
        {
            var _with10 = this;
            //===@@@ modif du 29 05 2017, desactive Sage.
            ///========================> Testé
            if (General.sDesActiveSage == "1")
            {

            }
            ///=======================> Testé
            else
            {
                SAGE.fc_OpenConnSage();
                General.sSQL = "";
                General.sSQL = "SELECT F_COMPTEA.CA_NUM as [Compte] , F_COMPTEA.CA_INTITULE as [Intitule] FROM F_COMPTEA WHERE N_ANALYTIQUE=1";
                sheridan.InitialiseCombo(_with10.DropAnalytique, General.sSQL, "Compte", true, SAGE.adoSage);
                //  GridArticle.DisplayLayout.Bands[0].Columns["CA_NUM"].ValueList = DropAnalytique;
                General.sSQL = "";
                General.sSQL = "SELECT F_COMPTEA.CA_NUM as [Compte] , F_COMPTEA.CA_INTITULE as [Intitule] FROM F_COMPTEA WHERE N_ANALYTIQUE=1";
                sheridan.InitialiseCombo(_with10.cmbAnalytiqueAcitvite, General.sSQL, "Compte", false, SAGE.adoSage);
            }

            General.sSQL = "";
            General.sSQL = "Select CodeCategorieArticle as [Code],LibelleCatArticle as [Libelle] from FacArticleCategorie";
            sheridan.InitialiseCombo(_with10.DropCategorieArticle, General.sSQL, "Code", true);
            //  GridArticle.DisplayLayout.Bands[0].Columns["CodeCategorieArticle"].ValueList = DropCategorieArticle;
            General.sSQL = "";
            General.sSQL = "Select CodeCategorieArticle as [Code],LibelleCatArticle as [Libelle] from FacArticleCategorie";
            sheridan.InitialiseCombo(_with10.SSCategorieArticle, General.sSQL, "Code", false);
            General.sSQL = "";
            General.sSQL = "SELECT DISTINCT CodeTypeArticle as [TypeArticle] FROM FacArticle";
            sheridan.InitialiseCombo(_with10.ssTypeArticle, General.sSQL, "TypeArticle", false);
            General.sSQL = "";
            General.sSQL = "Select CodeArticle as [Code],Designation1 as [Libelle] FROM FacArticle";
            sheridan.InitialiseCombo(_with10.cmbCodeArticle, General.sSQL, "Code", false);

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                General.sSQL = "";
                //sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 ) ORDER BY F_COMPTEG.CG_NUM "
                General.sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 ) ORDER BY F_COMPTEG.CG_NUM ";
                sheridan.InitialiseCombo(_with10.DropTauxPlein, General.sSQL, "Code comptable", true, SAGE.adoSage);
                // GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM2"].ValueList = DropTauxPlein;
            }

            //=== allumage, arret.
            General.sSQL = "SELECT     ALAR_Code as code , ALAR_Libelle as Libelle From ALAR_AllumageArret ORDER BY ALAR_Code";
            sheridan.InitialiseCombo(DropAllArret, General.sSQL, "Code", true);
            // GridArticle.DisplayLayout.Bands[0].Columns["ALAR_Code"].ValueList = DropAllArret;

            //===Famille
            General.sSQL = "SELECT     FAMD_Code as code, FAMD_Designation as Désigantion From FAMD_FamilleDevis ORDER BY FAMD_Code";
            sheridan.InitialiseCombo(DropFamille, General.sSQL, "Code", true);
            //    GridArticle.DisplayLayout.Bands[0].Columns["FAMD_Code"].ValueList = DropFamille;
            General.sSQL = "";
            General.sSQL = "SELECT     FAMD_Code as code, FAMD_Designation as Désigantion From FAMD_FamilleDevis ORDER BY FAMD_Code";
            sheridan.InitialiseCombo(_with10.cmbFamille, General.sSQL, "Code", false);
            // sous famille
            fc_SousFamille("");
            General.sSQL = "";
            General.sSQL = "SELECT     FAMSD_Code as code, FAMSD_Designation as Désigantion From FAMSD_SousFamilleDevis order by FAMSD_Code";
            //WHERE     (FAMD_Code = N'2')"
            sheridan.InitialiseCombo(_with10.cmbSousFamille, General.sSQL, "Code", false);
            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                General.sSQL = "";
                //sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 ) ORDER BY F_COMPTEG.CG_NUM "
                General.sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >=3  ) ORDER BY F_COMPTEG.CG_NUM ";
                sheridan.InitialiseCombo(_with10.cmbTxPlein, General.sSQL, "Code comptable", false, SAGE.adoSage);
                General.sSQL = "";
                //sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 ) ORDER BY F_COMPTEG.CG_NUM "
                General.sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 ) ORDER BY F_COMPTEG.CG_NUM ";
                sheridan.InitialiseCombo(_with10.DropTauxReduit, General.sSQL, "Code comptable", true, SAGE.adoSage);
                // GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM"].ValueList = DropTauxReduit;
                General.sSQL = "";
                //sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 ) ORDER BY F_COMPTEG.CG_NUM "
                General.sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 ) ORDER BY F_COMPTEG.CG_NUM ";
                sheridan.InitialiseCombo(_with10.cmbTxReduit, General.sSQL, "Code comptable", false, SAGE.adoSage);
                SAGE.fc_CloseConnSage();
            }

            //====modif du 25 10 2013, ajout de la liste déroulante type de contrat.
            General.sSQL = "SELECT     COT_Code  as [Code]  FROM         COT_ContratType ORDER BY CODE";
            sheridan.InitialiseCombo(ssDropCOT_ContratType, General.sSQL, "Code", true);
            //   GridArticle.DisplayLayout.Bands[0].Columns["COT_Code"].ValueList = ssDropCOT_ContratType;
            //====modif du 26 11 2014, ajout de la liste déroulante type de contrat 2.
            General.sSQL = "SELECT     COT2_Code  as [Code]  FROM         COT2_ContratType ORDER BY CODE";
            sheridan.InitialiseCombo(ssDropCOT2_ContratType, General.sSQL, "Code", true);
            //   GridArticle.DisplayLayout.Bands[0].Columns["COT2_Code"].ValueList = ssDropCOT2_ContratType;
            //=== modif du 16/12/2014,Ajout du type de  panne.
            General.sSQL = "SELECT     TYI_Code as [Code] From TYI_TypeInter ORDER BY TYI_Code";
            sheridan.InitialiseCombo(DropTypePanne, General.sSQL, "Code", true);
            //   GridArticle.DisplayLayout.Bands[0].Columns["TYI_Code"].ValueList = DropTypePanne;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_SousFamille(string sCode)
        {
            try
            {
                //===Sous Famille
                General.sSQL = "SELECT   FAMSD_Code as code, FAMSD_Designation as Désigantion From FAMSD_SousFamilleDevis ";
                if (!string.IsNullOrEmpty(sCode))
                {
                    General.sSQL = General.sSQL + " WHERE FAMD_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "' ";
                }
                General.sSQL = General.sSQL + " order by FAMSD_Code";
                //WHERE     (FAMD_Code = N'2')"
                sheridan.InitialiseCombo(DropSousFm, General.sSQL, "Code", true);
                //GridArticle.DisplayLayout.Bands[0].Columns["FAMSD_Code"].ValueList = DropSousFm;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_SousFamille");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridArticle_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridArticle.DisplayLayout.Bands[0].Columns["CodeArticle"].Header.Caption = "Code Article";
            GridArticle.DisplayLayout.Bands[0].Columns["CA_NUM"].Header.Caption = "Catégorie Article";
            GridArticle.DisplayLayout.Bands[0].Columns["CodeCategorieArticle"].ValueList = SSCategorieArticle;
            GridArticle.DisplayLayout.Bands[0].Columns["CA_NUM"].ValueList = DropCategorieArticle;
            GridArticle.DisplayLayout.Bands[0].Columns["COT_Code"].Header.Caption = "Type contrat";
            GridArticle.DisplayLayout.Bands[0].Columns["COT_Code"].ValueList = ssDropCOT_ContratType;
            GridArticle.DisplayLayout.Bands[0].Columns["COT2_Code"].Header.Caption = "Type contrat 2";
            GridArticle.DisplayLayout.Bands[0].Columns["COT2_Code"].ValueList = ssDropCOT2_ContratType;
            GridArticle.DisplayLayout.Bands[0].Columns["ALAR_Code"].Header.Caption = "Allumage/Arret";
            GridArticle.DisplayLayout.Bands[0].Columns["ALAR_Code"].ValueList = DropAllArret;
            GridArticle.DisplayLayout.Bands[0].Columns["TYI_Code"].Header.Caption = "Type de panne";
            GridArticle.DisplayLayout.Bands[0].Columns["TYI_Code"].ValueList = DropTypePanne;
            GridArticle.DisplayLayout.Bands[0].Columns["FAMD_Code"].Header.Caption = "Famille";
            GridArticle.DisplayLayout.Bands[0].Columns["FAMD_Code"].ValueList = DropFamille;
            GridArticle.DisplayLayout.Bands[0].Columns["FAMSD_Code"].Header.Caption = "Sous Famille";
            GridArticle.DisplayLayout.Bands[0].Columns["FAMSD_Code"].ValueList = DropSousFm;
            GridArticle.DisplayLayout.Bands[0].Columns["CA_NUM"].Header.Caption = "Analytique Activité";
            GridArticle.DisplayLayout.Bands[0].Columns["CA_NUM"].ValueList = DropAnalytique;
            GridArticle.DisplayLayout.Bands[0].Columns["CA_INTITULE"].Header.Caption = "Libelle Analytique ";
            GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM"].Header.Caption = "Taux Reduit ";
            GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM"].ValueList = DropTauxReduit;
            GridArticle.DisplayLayout.Bands[0].Columns["CG_INTITULE"].Header.Caption = "Libelle Taux reduit ";
            GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM2"].Header.Caption = "Taux Plein";
            GridArticle.DisplayLayout.Bands[0].Columns["CG_NUM2"].ValueList = DropTauxPlein;
            GridArticle.DisplayLayout.Bands[0].Columns["CG_INTITULE2"].Header.Caption = "Libelle Taux Plein ";
            GridArticle.DisplayLayout.Bands[0].Columns["PrixRevient"].Header.Caption = "Prix de revient ";
            GridArticle.DisplayLayout.Bands[0].Columns["DateChangement"].Header.Caption = "Date de changement ";
            GridArticle.DisplayLayout.Bands[0].Columns["AncienPrixAchat"].Header.Caption = "Ancien Prix d'achat";
            GridArticle.DisplayLayout.Bands[0].Columns["PrixAchat"].Header.Caption = "Prix d'achat ";
            GridArticle.DisplayLayout.Bands[0].Columns["Mobilite"].Header.Caption = "Mobilité ";
            GridArticle.DisplayLayout.Bands[0].Columns["Mobilite"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridArticle.DisplayLayout.Bands[0].Columns["Mobilite"].DefaultCellValue = false;
            GridArticle.DisplayLayout.Bands[0].Columns["Urgent"].Header.Caption = " Taux Urgent ";
            GridArticle.DisplayLayout.Bands[0].Columns["Urgent"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridArticle.DisplayLayout.Bands[0].Columns["SaisieCompteur"].Header.Caption = "Saisie de compteur ";
            GridArticle.DisplayLayout.Bands[0].Columns["SaisieCompteur"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridArticle.DisplayLayout.Bands[0].Columns["Designation1"].Header.Caption = "Designation";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation2"].Header.Caption = "Designation1";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation3"].Header.Caption = "Designation2";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation4"].Header.Caption = "Designation3";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation5"].Header.Caption = "Designation4";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation6"].Header.Caption = "Designation5";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation7"].Header.Caption = "Designation6";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation8"].Header.Caption = "Designation7";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation9"].Header.Caption = "Designation8";
            GridArticle.DisplayLayout.Bands[0].Columns["Designation10"].Header.Caption = "Designation9";
            GridArticle.DisplayLayout.Bands[0].Columns["ChaudiereCondensation"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridArticle.DisplayLayout.Bands[0].Columns["Correspondance"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

        }


        private void GridArticle_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key.ToUpper() == "CG_NUM".ToUpper())
            {
                var row = DropTauxReduit.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                if (row != null)
                {
                    GridArticle.ActiveRow.Cells["CG_INTITULE"].Value = row.Cells[1].Value;
                }
            }
            if (e.Cell.Column.Key.ToUpper() == "CG_NUM2".ToUpper())
            {
                var row = DropTauxPlein.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);

                if (row != null)
                {
                    GridArticle.ActiveRow.Cells["CG_INTITULE2"].Value = row.Cells[1].Value;
                }

            }

        }

        private void GridArticle_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            try
            {

                if (e.ReInitialize)
                    return;
                //GridArticle.EventManager.AllEventsEnabled = false;
                if (e.Row.Cells["Mobilite"].Value == DBNull.Value || e.Row.Cells["Mobilite"].Value.ToString() == "0")
                {
                    e.Row.Cells["Mobilite"].Value = false;
                }
                if (e.Row.Cells["Urgent"].Value == DBNull.Value || e.Row.Cells["Urgent"].Value.ToString() == "0")
                {
                    e.Row.Cells["Urgent"].Value = false;
                }
                if (e.Row.Cells["SaisieCompteur"].Value == DBNull.Value || e.Row.Cells["SaisieCompteur"].Value.ToString() == "0")
                {
                    e.Row.Cells["SaisieCompteur"].Value = false;
                }
                if (e.Row.Cells["ChaudiereCondensation"].Value == DBNull.Value || e.Row.Cells["ChaudiereCondensation"].Value.ToString() == "0")
                {
                    e.Row.Cells["ChaudiereCondensation"].Value = false;
                }
                if (e.Row.Cells["Masquer"].Value == DBNull.Value || e.Row.Cells["Masquer"].Value.ToString() == "0")
                {
                    e.Row.Cells["Masquer"].Value = false;
                }
                if (e.Row.Cells["Correspondance"].Value == DBNull.Value || e.Row.Cells["Correspondance"].Value.ToString() == "0")
                {
                    e.Row.Cells["Correspondance"].Value = false;
                }
                if (e.Row.Cells["Debit"].Value == DBNull.Value || e.Row.Cells["Debit"].Value.ToString() == "0")
                {
                    e.Row.Cells["Debit"].Value = false;
                }
                if (e.Row.Cells["Encaissement"].Value == DBNull.Value || e.Row.Cells["Encaissement"].Value.ToString() == "0")
                {
                    e.Row.Cells["Encaissement"].Value = false;
                }
                //GridArticle.UpdateData();
                //GridArticle.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridArticle_InitializeRow");
            }
        }

        private void GridArticle_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsArticle.Update();
            fc_ChargeCombo();
        }
    }
}

