﻿namespace Axe_interDT.Views.Les_Articles.Forms
{
    partial class frmFamilleDevis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTree.Override _override1 = new Infragistics.Win.UltraWinTree.Override();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Frame42 = new System.Windows.Forms.GroupBox();
            this.GridFam = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Frame44 = new System.Windows.Forms.GroupBox();
            this.TreeView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.label2 = new System.Windows.Forms.Label();
            this.Frame45 = new System.Windows.Forms.GroupBox();
            this.GridSousFam = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdSousFamille = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            this.Frame42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFam)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.Frame44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).BeginInit();
            this.Frame45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSousFam)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.Frame42);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(670, 570);
            // 
            // Frame42
            // 
            this.Frame42.BackColor = System.Drawing.Color.Transparent;
            this.Frame42.Controls.Add(this.GridFam);
            this.Frame42.Controls.Add(this.CmdSauver);
            this.Frame42.Controls.Add(this.label33);
            this.Frame42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame42.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame42.Location = new System.Drawing.Point(0, 0);
            this.Frame42.Name = "Frame42";
            this.Frame42.Size = new System.Drawing.Size(670, 570);
            this.Frame42.TabIndex = 411;
            this.Frame42.TabStop = false;
            this.Frame42.Tag = "2";
            // 
            // GridFam
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridFam.DisplayLayout.Appearance = appearance1;
            this.GridFam.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridFam.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridFam.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFam.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFam.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridFam.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFam.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridFam.DisplayLayout.MaxColScrollRegions = 1;
            this.GridFam.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridFam.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridFam.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridFam.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridFam.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridFam.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridFam.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridFam.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridFam.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridFam.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridFam.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridFam.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFam.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridFam.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridFam.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridFam.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridFam.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridFam.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridFam.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridFam.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridFam.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridFam.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridFam.Font = new System.Drawing.Font("Ubuntu", 8.25F);
            this.GridFam.Location = new System.Drawing.Point(4, 69);
            this.GridFam.Name = "GridFam";
            this.GridFam.Size = new System.Drawing.Size(660, 469);
            this.GridFam.TabIndex = 412;
            this.GridFam.Text = "ultraGrid1";
            this.GridFam.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridFam_InitializeLayout);
            this.GridFam.AfterRowsDeleted += new System.EventHandler(this.GridFam_AfterRowsDeleted);
            this.GridFam.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridFam_AfterRowUpdate);
            this.GridFam.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridFam_BeforeRowUpdate);
            this.GridFam.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridFam_BeforeExitEditMode);
            this.GridFam.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridFam_BeforeRowsDeleted);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(600, 18);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 385;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.label33.Location = new System.Drawing.Point(246, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(77, 24);
            this.label33.TabIndex = 384;
            this.label33.Text = "Famille";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.Frame44);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(670, 570);
            // 
            // Frame44
            // 
            this.Frame44.BackColor = System.Drawing.Color.Transparent;
            this.Frame44.Controls.Add(this.TreeView1);
            this.Frame44.Controls.Add(this.label2);
            this.Frame44.Controls.Add(this.Frame45);
            this.Frame44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame44.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame44.Location = new System.Drawing.Point(0, 0);
            this.Frame44.Name = "Frame44";
            this.Frame44.Size = new System.Drawing.Size(670, 570);
            this.Frame44.TabIndex = 411;
            this.Frame44.TabStop = false;
            this.Frame44.Tag = "4";
            // 
            // TreeView1
            // 
            appearance13.BorderColor = System.Drawing.Color.Black;
            this.TreeView1.Appearance = appearance13;
            this.TreeView1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.TreeView1.DisplayStyle = Infragistics.Win.UltraWinTree.UltraTreeDisplayStyle.Standard;
            this.TreeView1.Location = new System.Drawing.Point(11, 58);
            this.TreeView1.Name = "TreeView1";
            appearance14.Image = global::Axe_interDT.Properties.Resources.folder_24x24;
            _override1.NodeAppearance = appearance14;
            this.TreeView1.Override = _override1;
            this.TreeView1.Size = new System.Drawing.Size(211, 509);
            this.TreeView1.TabIndex = 413;
            this.TreeView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeView1_AfterSelect);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.label2.Location = new System.Drawing.Point(38, 18);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 24);
            this.label2.TabIndex = 412;
            this.label2.Text = " Famille";
            // 
            // Frame45
            // 
            this.Frame45.BackColor = System.Drawing.Color.Transparent;
            this.Frame45.Controls.Add(this.GridSousFam);
            this.Frame45.Controls.Add(this.cmdSousFamille);
            this.Frame45.Controls.Add(this.label1);
            this.Frame45.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame45.Location = new System.Drawing.Point(228, 49);
            this.Frame45.Name = "Frame45";
            this.Frame45.Size = new System.Drawing.Size(439, 518);
            this.Frame45.TabIndex = 411;
            this.Frame45.TabStop = false;
            this.Frame45.Tag = "5";
            // 
            // GridSousFam
            // 
            appearance15.BackColor = System.Drawing.SystemColors.Window;
            appearance15.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridSousFam.DisplayLayout.Appearance = appearance15;
            this.GridSousFam.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridSousFam.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridSousFam.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance16.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance16.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance16.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSousFam.DisplayLayout.GroupByBox.Appearance = appearance16;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSousFam.DisplayLayout.GroupByBox.BandLabelAppearance = appearance17;
            this.GridSousFam.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance18.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance18.BackColor2 = System.Drawing.SystemColors.Control;
            appearance18.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance18.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSousFam.DisplayLayout.GroupByBox.PromptAppearance = appearance18;
            this.GridSousFam.DisplayLayout.MaxColScrollRegions = 1;
            this.GridSousFam.DisplayLayout.MaxRowScrollRegions = 1;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            appearance19.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridSousFam.DisplayLayout.Override.ActiveCellAppearance = appearance19;
            appearance20.BackColor = System.Drawing.SystemColors.Highlight;
            appearance20.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridSousFam.DisplayLayout.Override.ActiveRowAppearance = appearance20;
            this.GridSousFam.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridSousFam.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridSousFam.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridSousFam.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridSousFam.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance21.BackColor = System.Drawing.SystemColors.Window;
            this.GridSousFam.DisplayLayout.Override.CardAreaAppearance = appearance21;
            appearance22.BorderColor = System.Drawing.Color.Silver;
            appearance22.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridSousFam.DisplayLayout.Override.CellAppearance = appearance22;
            this.GridSousFam.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridSousFam.DisplayLayout.Override.CellPadding = 0;
            appearance23.BackColor = System.Drawing.SystemColors.Control;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSousFam.DisplayLayout.Override.GroupByRowAppearance = appearance23;
            appearance24.TextHAlignAsString = "Left";
            this.GridSousFam.DisplayLayout.Override.HeaderAppearance = appearance24;
            this.GridSousFam.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridSousFam.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.Color.Silver;
            this.GridSousFam.DisplayLayout.Override.RowAppearance = appearance25;
            this.GridSousFam.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridSousFam.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance26.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridSousFam.DisplayLayout.Override.TemplateAddRowAppearance = appearance26;
            this.GridSousFam.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridSousFam.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridSousFam.Font = new System.Drawing.Font("Ubuntu", 8.25F);
            this.GridSousFam.Location = new System.Drawing.Point(6, 61);
            this.GridSousFam.Name = "GridSousFam";
            this.GridSousFam.Size = new System.Drawing.Size(428, 451);
            this.GridSousFam.TabIndex = 412;
            this.GridSousFam.Text = "ultraGrid1";
            this.GridSousFam.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridSousFam_InitializeLayout);
            this.GridSousFam.AfterRowsDeleted += new System.EventHandler(this.GridSousFam_AfterRowsDeleted);
            this.GridSousFam.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridSousFam_AfterRowUpdate);
            this.GridSousFam.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridSousFam_BeforeRowUpdate);
            this.GridSousFam.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridSousFam_BeforeExitEditMode);
            this.GridSousFam.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridSousFam_BeforeRowsDeleted);
            // 
            // cmdSousFamille
            // 
            this.cmdSousFamille.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSousFamille.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSousFamille.FlatAppearance.BorderSize = 0;
            this.cmdSousFamille.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSousFamille.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSousFamille.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdSousFamille.Location = new System.Drawing.Point(375, 20);
            this.cmdSousFamille.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSousFamille.Name = "cmdSousFamille";
            this.cmdSousFamille.Size = new System.Drawing.Size(60, 35);
            this.cmdSousFamille.TabIndex = 386;
            this.cmdSousFamille.Tag = "";
            this.toolTip1.SetToolTip(this.cmdSousFamille, "Enregistrer");
            this.cmdSousFamille.UseVisualStyleBackColor = false;
            this.cmdSousFamille.Click += new System.EventHandler(this.cmdSousFamille_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.label1.Location = new System.Drawing.Point(148, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(126, 24);
            this.label1.TabIndex = 385;
            this.label1.Text = "Sous Famille";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.SSTab1.Location = new System.Drawing.Point(0, 0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(672, 594);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab13.AllowMoving = Infragistics.Win.DefaultableBoolean.True;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "Famille";
            ultraTab1.TabPage = this.ultraTabPageControl2;
            ultraTab1.Text = "Sous Famille";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13,
            ultraTab1});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(670, 570);
            // 
            // frmFamilleDevis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(672, 594);
            this.Controls.Add(this.SSTab1);
            this.MaximumSize = new System.Drawing.Size(688, 633);
            this.MinimumSize = new System.Drawing.Size(688, 633);
            this.Name = "frmFamilleDevis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmFamilleDevis";
            this.Load += new System.EventHandler(this.frmFamilleDevis_Load);
            this.VisibleChanged += new System.EventHandler(this.frmFamilleDevis_VisibleChanged);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.Frame42.ResumeLayout(false);
            this.Frame42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFam)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.Frame44.ResumeLayout(false);
            this.Frame44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).EndInit();
            this.Frame45.ResumeLayout(false);
            this.Frame45.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridSousFam)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.GroupBox Frame42;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button CmdSauver;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridFam;
        private System.Windows.Forms.GroupBox Frame44;
        private System.Windows.Forms.GroupBox Frame45;
        public System.Windows.Forms.Button cmdSousFamille;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridSousFam;
        private Infragistics.Win.UltraWinTree.UltraTree TreeView1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}