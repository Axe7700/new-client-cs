﻿using Axe_interDT.Shared;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Les_Articles.Forms
{
    public partial class frmFamilleDevis : Form
    {
        public frmFamilleDevis()
        {
            InitializeComponent();
        }

        DataTable rsFam;
        SqlDataAdapter SDArsFam;
        DataTable rsSousF;
        SqlDataAdapter SDArsSousF;
        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cNiv3 = "C";
        ModAdo tmpAdorsFam;
        ModAdo tmpAdorsSousF;
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadFamille()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT     FAMD_Noauto, FAMD_Code, FAMD_Designation" + " From FAMD_FamilleDevis " + " ORDER BY FAMD_Code";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                rsFam = new DataTable();
                tmpAdorsFam = new ModAdo();
                // SDArsFam = new SqlDataAdapter(sSQL, General.adocnn);
                // SDArsFam.Fill(rsFam);
                rsFam = tmpAdorsFam.fc_OpenRecordSet(sSQL, GridFam, "FAMD_Noauto");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridFam.DataSource = rsFam;
                GridFam.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }


        }/// <summary>
         /// Tested
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            GridFam.UpdateData();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSousFamille_Click(object sender, EventArgs e)
        {
            GridSousFam.UpdateData();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFamilleDevis_VisibleChanged(object sender, EventArgs e)
        {
            fc_LoadFamille();
            fc_loadTreeView();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_loadTreeView()
        {

            int i = 0;
            UltraTreeNode oNode = null;
            bool bselect = false;
            string sFAMD_Code = null;
            //Dim llFAC_NoAuto        As Long
            //Dim sCOdeArticle        As String
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            General.sSQL = "SELECT     FAMD_Noauto, FAMD_Code, FAMD_Designation" + " From FAMD_FamilleDevis " + " ORDER BY FAMD_Code";
            try
            {
                var tmpAdorstmp = new ModAdo();
                General.rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);
                var _with1 = General.rstmp;
                //==reinitialise le treview.
                TreeView1.Nodes.Clear();

                if (General.rstmp.Rows.Count > 0)
                {
                    sFAMD_Code = "";
                    //sCodeArticle = ""
                    //llFAC_NoAuto = 0

                    foreach (DataRow rstmpRow in General.rstmp.Rows)
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        //==Ajout du 1er niveau de hiérarchie.
                        if (sFAMD_Code.ToString().ToUpper() != rstmpRow["FAMD_Code"].ToString().ToUpper() + "")
                        {
                         
                           
                            sFAMD_Code = rstmpRow["FAMD_Code"] + "";
                            UltraTreeNode UTN_N1 = new UltraTreeNode(cNiv1 + rstmpRow["FAMD_Code"] + "", rstmpRow["FAMD_Code"] + "" + "-" + rstmpRow["FAMD_Designation"] + "");
                            UTN_N1.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                            UTN_N1.Override.SelectedNodeAppearance.Image = Properties.Resources.folder_opened16;                          
                            TreeView1.Nodes.Add(UTN_N1);
                          
                        }

                        //            '==Ajout du 2eme niveau de hiérarchie.
                        //            If Not IsNull(!CodeArticle) Then
                        //
                        //                If llFAC_NoAuto <> !FAC_NoAuto Then
                        //                    TreeView1.Nodes.Add cNiv1 & !CAI_Noauto, tvwChild, cNiv2 & !FAC_NoAuto, nz(!Designation1, ""), 2, 4
                        //                    llFAC_NoAuto = !FAC_NoAuto
                        //
                        //                End If
                        //
                        //                '==Ajout du 3eme niveau de hiérarchie.
                        //                'If Not IsNull(!FAP_NoAuto) Then
                        //               '     TreeView1.Nodes.Add cNiv2 & !EQM_NoAuto, tvwChild, cNiv3 & !FAP_NoAuto, nz(!FAP_Designation1, ""), 3, 5
                        //                'End If
                        //
                        //            End If


                        //_with1.MoveNext();
                    }
                }
                // _with1.Close();


                General.rstmp = null;

                bselect = false;

                foreach (UltraTreeNode oNode_loopVariable in TreeView1.Nodes)
                {
                    oNode = oNode_loopVariable;
                    if (oNode.Selected == true)
                    {
                        bselect = true;
                    }
                }

                if (bselect == false)
                {
                    Frame45.Visible = false;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Loadtreeview;");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {

            if (GridFam.ActiveCell.Column.Index == GridFam.ActiveRow.Cells["FAMD_Code"].Column.Index)
            {
                GridFam.ActiveRow.Cells["FAMD_Code"].Value = GridFam.ActiveRow.Cells["FAMD_Code"].Text.ToString().Trim();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            string sID = null;
            UltraTreeNode  oNode = null;
            try
            {

                foreach (UltraTreeNode oNode_loopVariable in TreeView1.Nodes)
                {
                    oNode = oNode_loopVariable;
                    if (oNode.Selected == true)
                    {
                        sID = General.Mid(oNode.Key, 2, oNode.Key.Length - 1);
                    }
                }

                if (GridSousFam.ActiveCell.Column.Index == GridSousFam.ActiveRow.Cells["FAMSD_Code"].Column.Index)
                {
                    GridSousFam.ActiveRow.Cells["FAMSD_Code"].Value = GridSousFam.ActiveRow.Cells["FAMSD_Code"].Text.ToString().Trim();
                }

                if (string.IsNullOrEmpty(GridSousFam.ActiveRow.Cells["FAMD_Code"].Text) && !string.IsNullOrEmpty(sID))
                {
                    GridSousFam.ActiveRow.Cells["FAMD_Code"].Value = sID;
                }


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";");
            }

        }
       
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_LoadSousFam(string sCode)
        {
            string sSQL = null;
            try
            {
                sSQL = "SELECT     FAMSD_Noauto, FAMD_Code,FAMSD_Code, FAMSD_Designation"
                        + " From FAMSD_SousFamilleDevis " + " WHERE     (FAMD_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "') "
                        + "  ORDER BY FAMSD_Code";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                rsSousF = new DataTable();
                tmpAdorsSousF = new ModAdo();
                rsSousF = tmpAdorsSousF.fc_OpenRecordSet(sSQL, GridSousFam, "FAMSD_Noauto");
                //SDArsSousF = new SqlDataAdapter(sSQL, General.adocnn);
                //SDArsSousF.Fill(rsSousF);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridSousFam.DataSource = rsSousF;
                GridSousFam.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }


        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridFam.DisplayLayout.Bands[0].Columns["FAMD_Noauto"].Hidden = true;
            GridFam.DisplayLayout.Bands[0].Columns["FAMD_Code"].Header.Caption = "Code ";
            GridFam.DisplayLayout.Bands[0].Columns["FAMD_Designation"].Header.Caption = "Désignation";
        }
        /// <summary>
        /// Testeed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridSousFam.DisplayLayout.Bands[0].Columns["FAMSD_Noauto"].Hidden = true;
            GridSousFam.DisplayLayout.Bands[0].Columns["FAMD_Code"].Hidden = true;
            GridSousFam.DisplayLayout.Bands[0].Columns["FAMSD_Code"].Header.Caption = "Code";
            GridSousFam.DisplayLayout.Bands[0].Columns["FAMSD_Designation"].Header.Caption = "Désignation";
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            int xx = tmpAdorsFam.Update();
              fc_LoadFamille();
            fc_loadTreeView();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_AfterRowsDeleted(object sender, EventArgs e)

        {
            int xx = tmpAdorsFam.Update();
            fc_loadTreeView();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFam_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["FAMD_Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code  est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }

            if (e.Row.Cells["FAMD_Code"].DataChanged)
            {
                string ss = e.Row.Cells["FAMD_Code"].OriginalValue.ToString();
                if (e.Row.Cells["FAMD_Code"].OriginalValue.ToString() != e.Row.Cells["FAMD_Code"].Text)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FAMD_FamilleDevis  WHERE FAMD_Code= '{e.Row.Cells["FAMD_Code"].Text}'  "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            string ss = e.Row.Cells["FAMSD_Code"].OriginalValue.ToString();
            int xx = tmpAdorsSousF.Update();
            string ID = e.Row.Cells["FAMD_Code"].Value.ToString();
            fc_LoadSousFam(ID);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["FAMSD_Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code  est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //e.Row.CancelUpdate();
                e.Cancel = true;
            }
            string ss = e.Row.Cells["FAMSD_Code"].OriginalValue.ToString();
            if (e.Row.Cells["FAMSD_Code"].DataChanged)
            {
                if (e.Row.Cells["FAMSD_Code"].OriginalValue.ToString() != e.Row.Cells["FAMSD_Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FAMSD_SousFamilleDevis  WHERE FAMSD_Code= '{e.Row.Cells["FAMSD_Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFam_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsSousF.Update();

        }

        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {

           UltraTreeNode Node = TreeView1.ActiveNode;
            string sID = null;
            Node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            sID = General.Mid(Node.Key, 2, Node.Key.Length - 1);

            if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv1.ToString().ToUpper())
            {
                Frame45.Visible = true;
                fc_LoadSousFam(sID);
            }
            else
            {
                Frame45.Visible = false;
            }
        }

        private void frmFamilleDevis_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
        }
    }
}
