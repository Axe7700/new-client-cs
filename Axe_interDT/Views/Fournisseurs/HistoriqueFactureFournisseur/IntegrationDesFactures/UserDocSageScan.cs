﻿//using Microsoft.VisualBasic;

using Axe_interDT.Shared;
using Axe_interDT.Shared.FilterOfGrids;
using Axe_interDT.Views.Fournisseur.IntegrationDesFactures.Forms;
using Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views
{
    public partial class UserDocSageScan : UserControl
    {
        public UserDocSageScan()
        {
            InitializeComponent();
        }

        DataTable rsRSTE;
        ModAdo ModAdoRSTE = new ModAdo();
        ModAdo ModAdoFacE = new ModAdo();
        ModAdo ModAdoPied = new ModAdo();
        ModAdo ModAdoDetail = new ModAdo();
        ModAdo ModAdoDetailTVA = new ModAdo();
        ModAdo AdoLib;
        const string cReadSoft = "ReadSoft";
        const string cDesignation = "Facturation automatique (ReadSoft)";
        const string cAnalyServiceADM = "ADM";

        int lNoLigne;
        int iOldCol;
        bool bTriAsc;
        string sOrderBy;
        private const short cParite = 0;

        string sSQL1 = null;
        string sSQL2 = null;
        bool existBonC = false;

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void cmdComptabilise_Click(object sender, System.EventArgs eventArgs)
        {
            //ssRSTE.CtlUpdate();

            if (fc_CtrlFactFourn() == false)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Intégration annulée, veuillez consulter le listing des anomalies et corriger ces erreurs avant d'intégrer à nouveau. ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            //cmdVisuFactures_Click
            //fc_ctrlInvoice

            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous intégrer ces factures.", "Intégration", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                return;
            }

            fc_InsertFactFourn();

            fc_CtrlFactFourn(true);

            fc_CtrlFactFourn();

        }
        //Tested
        private void cmdControle_Click(object sender, System.EventArgs eventArgs)
        {
            ModAdoRSTE.Update();
            fc_CtrlFactFourn();
        }
        //Tested
        private void CmdSauver_Click(object sender, System.EventArgs eventArgs)
        {
            ModAdoRSTE.Update();
            ssRSTE.UpdateData();
        }

        private void fc_InsertFactFourn()
        {
            string sSQL = null;
            DataTable rsFacE = new DataTable();
            DataTable rsPied = new DataTable();
            DataTable rsDetail = new DataTable();
            DataTable rsDetailTva = new DataTable();
            int lNoAutoFacture = 0;
            DateTime dtDate = default(DateTime);
            int i = 0;

            //===  en tete.
            sSQL = "SELECT NoAutoFacture, CodeFournisseur, RefFacture, Montant,"
                + " ModeRgt, CG_NUM, TaxeCG_NUM, CT_NUM, DateLivraison, Commentaire, DateSaisie, "
                + " FactureCreat , DateModif, FactureModif, Verrou, EC_NoFacture, DateEcheance,"
                + " AjusterMtTVA" + " , ReadSoft, " + " TaxeCG_NUMautoLiquide "
                + " From FactFournEntete" + " WHERE     (NoAutoFacture = 0)";

            rsFacE = ModAdoFacE.fc_OpenRecordSet(sSQL);

            //=== PIED.
            sSQL = "SELECT     NoAutoFacture, NoBCmd, CodeImmeuble, Montant, AnalytiqueImmeuble, CheminFichier"
                + " From FactFournPied" + " WHERE  NoAutoFacture = 0";

            rsPied = ModAdoPied.fc_OpenRecordSet(sSQL);


            //=== CORPS
            sSQL = "SELECT     NoAutoFacture, NoLigne, NoBcmd, RefLigneCmd, DesignationLigneCmd,"
                + " QteCommande, QteLivre, MontantLigne, CG_NUM, TaxeCG_NUM, "
                + " TaxeTA_Taux , EC_NoCptCharge, EC_NoCptTva, AnalytiqueAffaire, "
                + " AnalytiqueImm, RegistreTaxe, AnalytiqueActivite, TaxeCG_NUMautoLiquide "
                + " From FactFournDetail" + " WHERE NoAutoFacture = 0 AND NoLigne = 0";

            rsDetail = ModAdoDetail.fc_OpenRecordSet(sSQL);


            //=== DETAIL TVA.
            sSQL = "SELECT     NoAutoFacture, CptTVA, HT, TVA, TTC, CptTVAAutoliquide, TVAautoLiquide, AutoLiquidation "
                + " From FactFournHtTvaCpta" + " WHERE     (NoAutoFacture = 0) AND (CptTVA = N'0')";

            rsDetailTva = ModAdoDetailTVA.fc_OpenRecordSet(sSQL);

            foreach (DataRow Dr in rsRSTE.Rows)
            {

                //=== contrôle si cette facture est séléctionné pour l'intégration
                if (General.nz(Dr["RSTE_Selectionne"], 0).ToString() == "0")
                {
                    continue;
                }

                //=== controle si la facture a déja été intégré.
                if (General.nz(Dr["FactNoautofacture"], "0").ToString() != "0")
                {
                    continue;
                }
                //=== controle si la facture est liée a un bon de commande.
                //=== RSTE_SansNoCommande = 1 ( sans bon de commande).
                if (General.nz(Dr["RSTE_SansNoCommande"], 0).ToString() == "1")
                {
                    continue;
                }

                //=========== EN TETE DE FACTURE =================

                DataRow NRFE = rsFacE.NewRow();
                int NoAutoFacture = 0;
                SqlCommandBuilder cb = new SqlCommandBuilder(ModAdoFacE.SDArsAdo);
                ModAdoFacE.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                ModAdoFacE.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                ModAdoFacE.SDArsAdo.InsertCommand = insertCmd;

                ModAdoFacE.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {

                    if (e.StatementType == StatementType.Insert)
                    {
                        if (NoAutoFacture == 0)
                        {
                            NoAutoFacture = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                        }


                    }
                });

                NRFE["CodeFournisseur"] = Dr["RSTE_CodeFournCleAuto"];
                NRFE["RefFacture"] = Dr["RSTE_NoFacture"];
                //== champs suivant n est pas uililisé????
                //rsFacE!Montant = RSTE!
                NRFE["ModeRgt"] = Dr["RSTE_ModeRgt"];
                NRFE["CG_NUM"] = Dr["RSTE_CptChargeDef"];
                NRFE["TaxeCG_Num"] = Dr["RSTE_CptTvaDef"];
                NRFE["CT_NUM"] = Dr["RSTE_CT_Num"];
                //== champs à renseigner.
                //rsFacE!DateLivraison = RSTE!
                //== champs suivant n est pas uililisé????
                //rsFacE!Commentaire = RSTE!
                NRFE["DateSaisie"] = Dr["RSTE_dateCreat"];
                NRFE["FactureCreat"] = cReadSoft;
                NRFE["DateModif"] = DateTime.Now;
                NRFE["FactureModif"] = Dr["RSTE_CreePar"];
                NRFE["Verrou"] = DBNull.Value;
                NRFE["DateEcheance"] = Dr["RSTE_DateEcheance"];
                NRFE["AjusterMtTVA"] = 0;
                NRFE["DateLivraison"] = Dr["RSTE_DateFacture"];
                NRFE["ReadSoft"] = 1;
                //=== modif du 21 01 2014, autiliquidation de la TVA.
                NRFE["TaxeCG_NUMautoLiquide"] = Dr["RSTE_CptAutoLiquidation"];
                rsFacE.Rows.Add(NRFE.ItemArray);
                ModAdoFacE.Update();

                //===============================================
                //=========== PIED ==============================
                DataRow NRP = rsPied.NewRow();

                NRP["NoAutoFacture"] = NoAutoFacture;
                NRP["NoBcmd"] = Dr["NoBonDeCommande"];
                NRP["CodeImmeuble"] = Dr["CodeImmeuble"];
                NRP["CheminFichier"] = Dr["RSTE_CheminFichierClasse"];
                NRP["Montant"] = Dr["RSTE_HT"];
                //== champs suivant n est pas uililisé????
                //rsPied!AnalytiqueImmeuble = rsRSTE!
                rsPied.Rows.Add(NRP.ItemArray);
                ModAdoPied.Update();

                //===> Mondir le 08.03.2021 pour corriger https://groupe-dt.mantishub.io/view.php?id=2340
                //===> Mondirt le 11.03.2021, commanted and moved dow
                //if (Dr["NoBonDeCommande"] != null)
                //{
                //     var req = $"UPDATE BonDeCommande SET CodeE = 'CF' WHERE NoBonDeCommande = '{Dr["NoBonDeCommande"]}'";
                //    General.Execute(req);
                //}
                //===> Fin Modif Mondir


                //==============================================
                //========= CORPS DETAIL =======================
                sSQL = "SELECT     NoAutoFacture, NoLigne, NoBcmd, RefLigneCmd, DesignationLigneCmd,"
                    + " QteCommande, QteLivre, MontantLigne, CG_NUM, TaxeCG_NUM, "
                    + " TaxeTA_Taux , EC_NoCptCharge, EC_NoCptTva, AnalytiqueAffaire, "
                    + " AnalytiqueImm, RegistreTaxe, AnalytiqueActivite" + " From FactFournDetail";

                DataRow DRD = rsDetail.NewRow();

                DRD["NoAutoFacture"] = NoAutoFacture;
                DRD["NoLigne"] = 1;
                DRD["NoBcmd"] = Dr["NoBonDeCommande"];
                //rsDetail!RefLigneCmd = rsRSTE!
                DRD["DesignationLigneCmd"] = cDesignation;
                DRD["QteCommande"] = 1;
                DRD["QteLivre"] = 1;
                DRD["MontantLigne"] = Dr["RSTE_HT"];
                DRD["CG_NUM"] = Dr["RSTE_CptChargeDef"];
                DRD["TaxeCG_Num"] = Dr["RSTE_CptTvaDef"];
                DRD["TaxeTA_Taux"] = Dr["RSTE_TauxTVA"];
                DRD["TaxeCG_NUMautoLiquide"] = Dr["RSTE_CptAutoLiquidation"];

                rsDetail.Rows.Add(DRD.ItemArray);
                ModAdoDetail.Update();
                //==============================================
                //========= CORPS DETAIL TVA ===================

                sSQL = "SELECT     NoAutoFacture, CptTVA, HT, TVA, TTC, AutoLiquidation " + " From FactFournHtTvaCpta";
                DataRow DRDT = rsDetailTva.NewRow();

                DRDT["NoAutoFacture"] = NoAutoFacture;
                DRDT["CptTVA"] = Dr["RSTE_CptTvaDef"];
                DRDT["TVA"] = Dr["RSTE_MtTVA"];
                DRDT["CptTVAAutoliquide"] = Dr["RSTE_CptAutoLiquidation"];
                // rsDetailTva!TVAautoLiquide = IIf(Not IsNull(rsRSTE!RSTE_MtTVA), -rsRSTE!RSTE_MtTVA, 0)
                double TVAautoLiquide = Convert.ToDouble(Dr["RSTE_MtTVA"]) * (-1);
                DRDT["TVAautoLiquide"] = (!string.IsNullOrEmpty(Dr["RSTE_MtTVA"].ToString()) ? TVAautoLiquide : 0);

                rsDetailTva.Rows.Add(DRDT.ItemArray);
                ModAdoDetailTVA.Update();

                //    '=== modif du 21 01 2014, gestion de l'autoliquidation.
                //    If nz(rsRSTE!RSTE_CptAutoLiquidation, "") <> "" Then
                //        rsDetailTva.AddNew
                //        rsDetailTva!NoAutoFacture = lNoAutoFacture
                //        rsDetailTva!CptTVA = rsRSTE!RSTE_CptAutoLiquidation
                //        rsDetailTva!TVA = IIf(Not IsNull(rsRSTE!RSTE_MtTVA), -rsRSTE!RSTE_MtTVA, 0)
                //        rsDetailTva.Update
                //    End If

                //=== met le champs FactNoautofacture dans la table RSTE_ReadSoftEnTete
                //=== avec le numéro unique de la table FactFournEntete.
                Dr["FactNoautofacture"] = NoAutoFacture;
                //General.Execute("update RSTE_ReadSoftEnTete set FactNoautofacture='" + lNoAutoFacture +
                //                "' where RSTE_NoAuto='" + Dr["RSTE_NoAuto"] + "'");

                //===> Mondir le 11.03.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2340#c5592
                using (var tmpModAdo = new ModAdo())
                {
                    var req = $"SELECT     SUM(RSTE_HT) AS Total From RSTE_ReadSoftEnTete WHERE     NoBonDeCommande = '{Dr["NoBonDeCommande"]}' AND STF_Code != 'NV'";
                    var totalFacture = tmpModAdo.fc_ADOlibelle(req).ToDouble();

                    req = $"SELECT  SUM(BCD_Quantite * BCD_PrixHT)  From BCD_Detail WHERE   BCD_Cle = '{Dr["NoBonDeCommande"]}'";
                    var totalBcmd = tmpModAdo.fc_ADOlibelle(req).ToDouble();

                    if (totalFacture > totalBcmd * 0.98)
                    {
                        req = $"UPDATE BonDeCommande SET CodeE = 'CF' WHERE NoBonDeCommande = '{Dr["NoBonDeCommande"]}'";
                        General.Execute(req);
                    }
                }

                ModAdoRSTE.Update();

            }

            ModAdoFacE.Close();
            ModAdoPied.Close();
            ModAdoDetail.Close();
            ModAdoDetailTVA.Close();

            rsPied = null;
            rsFacE = null;
            rsDetail = null;
            rsDetail = null;


            //===================================
            //===================================
            sSQL = "SELECT  RSTE_NoAuto ,RSTE_NoLigne ,  RSTE_dateCreat, RSTE_CreePar, RSTE_NoFacture,"
                + " NoBonDeCommande, CodeImmeuble, RSTE_HT, RSTE_TauxTVA, RSTE_MtTVA, "
                + " RSTE_TTC, RSTE_DateFacture, RSTE_DateEcheance, RSTE_CodeFournCleAuto,"
                + " RSTE_CodeFourn, RSTE_RaisonSocial,RSTE_ModeRgt, RSTE_CT_Num,"
                + " RSTE_CptChargeDef , RSTE_CptTvaDef , RSTE_Comptabilise, "
                + " RSTE_DateComptabilise,RSTE_Anomalie, FactNoautofacture  "
                + " FROM         RSTE_ReadSoftEnTete" + " WHERE     (RSTE_Comptabilise IS NULL) OR"
                + " (RSTE_Comptabilise = 0)";

            //===================================
            //===================================

            dtDate = DateTime.Today;



            foreach (DataRow DRR in rsRSTE.Rows)
            {

                if (General.nz(DRR["RSTE_Selectionne"], 0).ToString() == "1")
                {

                    if (General.nz(DRR["FactNoautofacture"], 0).ToString() != "0")
                    {
                        //=== RSTE_SansNoCommande = 1 ( avec bon de commande).
                        //i = fc_IntegrerFacture(Convert.ToInt32(General.nz(DRR["FactNoautofacture"], 0).ToString()), Convert.ToDateTime(DRR["RSTE_DateFacture"].ToString()), DRR["RSTE_CT_Num"].ToString(), DRR["RSTE_NoFacture"].ToString(), DRR["RSTE_RaisonSocial"].ToString(), 0, Convert.ToDateTime(DRR["RSTE_DateEcheance"].ToString()), dtDate);
                        //'=== modif du 05 06 2019, ajuste la TVA.

                        i = fc_IntegrerFacture(Convert.ToInt32(General.nz(DRR["FactNoautofacture"], 0)), Convert.ToDateTime(DRR["RSTE_DateFacture"])

                                            , DRR["RSTE_CT_Num"].ToString(), DRR["RSTE_NoFacture"].ToString(),

                                                DRR["RSTE_RaisonSocial"].ToString(),

                                             1, Convert.ToDateTime(DRR["RSTE_DateEcheance"]), dtDate);

                    }
                    else if (General.nz(DRR["RSTE_SansNoCommande"], 0).ToString() == "1")
                    {
                        //=== controle si la facture est liée a un bon de commande.
                        //=== RSTE_SansNoCommande = 1 ( sans bon de commande).

                        //i = fc_IntegreFactSansCom(General.nz(DRR["RSTE_NoAuto"], 0), Convert.ToDateTime(DRR["RSTE_DateFacture"].ToString()), DRR["RSTE_CT_Num"].ToString(), DRR["RSTE_NoFacture"].ToString(), DRR["RSTE_RaisonSocial"].ToString(), 0, Convert.ToDateTime(DRR["RSTE_DateEcheance"].ToString()), dtDate);

                        i = fc_IntegreFactSansCom(General.nz(DRR["RSTE_NoAuto"], 0), Convert.ToDateTime(DRR["RSTE_DateFacture"])
                            , DRR["RSTE_CT_Num"].ToString(), DRR["RSTE_NoFacture"].ToString(),
                              DRR["RSTE_RaisonSocial"].ToString(),
                             1, Convert.ToDateTime(DRR["RSTE_DateEcheance"]), dtDate);

                    }
                }

            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCT_Num"></param>
        /// <returns></returns>
        private long fc_GetCodeReglementSage(string sCT_Num)
        {
            //=== retourne le mode de réglement correspondant au code SAGE.
            string sSQL = "";
            long fc_GetCodeReglementSage = 0;

            try
            {
                sSQL = "SELECT      ModeReglement.CodeSAGE"
                + " FROM         fournisseurArticle INNER JOIN "
                + " CodeReglement ON fournisseurArticle.ModeRgt = CodeReglement.Libelle INNER JOIN "
                + " ModeReglement ON CodeReglement.ModeReglt = ModeReglement.Code "
                + " WHERE     (fournisseurArticle.NCompte = '" + StdSQLchaine.gFr_DoublerQuote(sCT_Num) + "')";

                using (var tmpModAdo = new ModAdo())
                    sSQL = General.nz(tmpModAdo.fc_ADOlibelle(sSQL), 0).ToString();

                fc_GetCodeReglementSage = long.Parse(sSQL);
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                fc_GetCodeReglementSage = 0;
            }

            return fc_GetCodeReglementSage;
        }

        private int fc_IntegrerFacture(int lNoAutoFacture, DateTime dtDateFacture, string sCptFourn, string sNofacture, string sRaisonSocial, int lAjusteTva, DateTime dtdateEcheance, DateTime dtJour)
        {
            int functionReturnValue = 0;

            const string c1 = "1";
            //Plan analytique Activité (plan principal)
            const string c2 = "2";
            //Plan analytique Affaire
            const string c3 = "3";
            //Plan analytique Immeuble
            const string c4 = "4";
            const string c6 = "6";

            string strCT_Num = null;
            //Nom du compte Fournisseur
            string strCG_Num = null;
            //Numéro du compte fournisseur
            string strCA_Num = null;
            //Nom du compte analytique (pour chaque immeuble
            //et pour chaque affaire)
            string strCloture = null;
            //Etat du journal d'achat
            string strJo_Num = null;
            //Nom du journal des Achats
            string strJM_Date = null;
            //Date du journal des achats
            string[] tabCptCharges = null;
            //Tableau comptenant les différents
            //comptes de charges
            string[] tabCptTVA = null;
            //Tableau comptenant les différents
            //comptes de TVA
            string[] TabCptFournisseur = null;
            //Tableau comptenant les différents
            //comptes de Charges
            string[] tabMontant = null;
            //Tableau des montants
            string[] tabCptAnalytiqueImm = null;
            //Tableau des comptes analytiques Immeuble
            string[] tabCptAnalytiqueAffaire = null;
            //Tableau des comptes analytiques Affaire
            string[] tabCptAnalytiqueActivite = null;
            //Tableau des comptes analytiques Activité
            string[] tabCA_Num = null;
            //Tableau des comptes analytiques
            string[] tabEC_No = null;
            //Tableau de lien avec les champs
            //EC_No
            string[] tabCptAnalytiqueAffImm = null;
            // Tableau des codes immeubles liées à l'activité affaire.

            string[] tabTauxBase = null;
            //Base du taux : 19.6% par exemple
            string sAnalytiqueActivite = null;


            string strReqIns = null;
            string strRequete = null;
            int i = 0;
            int nbRecAff = 0;
            int MaxRT_No = 0;
            //Pour l'intégration dans le registre de taxe
            bool Ecrit = false;
            bool blnTrans = false;
            int intDeroule = 0;

            string NoFactureSage = null;
            //Sert uniquement à afficher le numéro
            //d'enregistrement sage à la fin de l'intégration
            string sSQLn = null;

            DataTable rsCpta = default(DataTable);
            ModAdo rsCptaAdo = new ModAdo();

            double dbTTC = 0;
            double dbHT = 0;
            double dbTVA = 0;
            int xxx = 0;

            double dbHTsecteur4 = 0;
            double dbCumulHTsecteur4 = 0;
            double dbHtAutoLiquidation = 0;
            bool bAutoLiquidation = false;

            bool bSecteur4 = false;

            string sReqCancel = null;

            DateTime dtDateNewFact = default(DateTime);

            //Schéma d'intégration :
            //   1 - Table F_EcritureC
            //       1-1 - Une ligne par compte de charge avec montant HT
            //       1-2 - Une ligne par compte de TVA (pour ensuite les intégrer dans)
            //                   le registre de taxe)
            //       1-3 - Une ligne pour le compte Fournisseur avec montant total TTC

            //         Champs de la table F_EcritureC :
            //        JO_Num  EC_No   EC_NoLink   JM_Date EC_Jour EC_Date EC_Piece
            //        EC_RefPiece EC_TresoPiece   CG_Num  CG_NumCont  CT_Num  EC_Intitule
            //        N_Reglement EC_Echeance EC_Parite   EC_Quantite N_Devise
            //        EC_Sens EC_Montant  EC_Lettre   EC_Lettrage EC_Point    EC_Pointage
            //        EC_Impression   EC_Cloture  EC_CType    EC_Rappel   CT_NumCont
            //        EC_LettreQ EC_LettrageQ    EC_ANType   EC_RType    EC_Devise
            //        EC_Remise EC_ExportRappro EC_ExportExpert TA_Code EC_ASPLink  cbProt


            //   2 - Table F_EcritureA
            //       2-1 - Une ligne par compte analytique immeuble (montant HT des
            //                   charges de l'immeuble)
            //       2-2 - Une ligne par compte analytique Affaire (montant HT des affaires
            //                   liées aux différents immeubles)
            //       2-3 - Une ligne par compte analytique Activité (montant HT des activités
            //                   liées aux différents appels)


            //        Champs de la table F_EcritureA
            //        EC_No   N_Analytique    EA_Ligne    CA_Num  cbCA_Num
            //        EA_Montant EA_Quantite



            //   3 - Table F_RegTaxe
            //       3-1 - Une ligne par Compte de taxe (montant de la TVA)

            //        Champs de la table F_RegTaxe
            //        EC_No   RT_No   RT_Type RT_DateReg  RT_DatePiece
            //        CT_Num  cbCT_Num    TA_Provenance01 TA_Provenance02 TA_Provenance03
            //        TA_Provenance04 TA_Provenance05 CG_Num01    CG_Num02    CG_Num03
            //        CG_Num04    CG_Num05    TA_TTaux01  TA_TTaux02  TA_TTaux03 TA_TTaux04
            //        TA_TTaux05  TA_Taux01   TA_Taux02   TA_Taux03   TA_Taux04  TA_Taux05
            //        RT_Base01   RT_Base02   RT_Base03   RT_Base04   RT_Base05  RT_Montant01
            //        RT_Montant02    RT_Montant03    RT_Montant04    RT_Montant05
            //        TA_Code01   TA_Code02   TA_Code03   TA_Code04   TA_Code05   JM_Date

            //===> Mondir le 15.03.2021,  signalé par a.guisse
            var transactionName = "Trans1" + General.Replace(General.fncUserName(), " ", "").Replace(".", "");
            //===> Fin Modif Mondir

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante..");
                    return functionReturnValue;
                }


                intDeroule = 0;
                //Démarrage et ouverture de la connexion Sage

                Ecrit = false;
                blnTrans = false;
                dbHT = 0;
                dbTTC = 0;
                dbTVA = 0;
                dbHtAutoLiquidation = 0;

                SAGE.fc_OpenConnSage();
                AdoLib = new ModAdo();
                intDeroule = 1;  //Liaison au journal de charge : lecture et éventuellement écriture

                // Récupération du compte de vente (propre à chaque société)
                strJo_Num = General.gfr_liaison("JournalCharges");

                //Définition de la date du journal par rapport à celle de la facture
                //strJM_Date = "01/" + string.Format(dtDateFacture.Month.ToString(), "00") + "/" + dtDateFacture.Year;

                //=== modif du 12 06 2019, on calcule la date JM_Date et ec_jour par rapport au cloture.
                dtDateNewFact = GeneralXav.fc_DatePieceCLoture(dtDateFacture, strJo_Num);
                strJM_Date = "01/" + dtDateNewFact.Month.ToString("00") + "/" + dtDateNewFact.Year;

                //Numéro de compte asocié au plan du compte fournisseur
                strCG_Num = AdoLib.fc_ADOlibelle("SELECT CG_NumPrinc FROM F_CompteT WHERE CT_Num='" + sCptFourn + "'", false, SAGE.adoSage);

                if (string.IsNullOrEmpty(AdoLib.fc_ADOlibelle("SELECT JO_Num FROM F_JMOUV WHERE JO_NUM='" + strJo_Num + "'" + " AND JM_DATE='" + strJM_Date + "'", false, SAGE.adoSage)))
                {
                    string cmd = "INSERT INTO F_JMOUV (JO_Num,JM_Date,JM_Cloture,JM_Impression)" + " VALUES ('" + strJo_Num + "','" + strJM_Date + "',0,0)";
                    int xx = General.Execute(cmd, SAGE.adoSage);

                    ///MsgBox "Ouverture du journal d'achat pour la période du " & strJM_Date, vbInformation, "Ouverture du journal d'achat"
                    strCloture = "0";
                }
                else
                {
                    //Récupération de l'état de clôture du journal
                    strCloture = AdoLib.fc_ADOlibelle("SELECT JM_Cloture FROM F_JMOUV " + " WHERE JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'");
                }

                if (strCloture == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Le journal d'achat " + strJo_Num + " du " + strJM_Date + " est clôturé" + "\r\n" +
                        "Cette facture ne peut être intégrée pour la date inscrite", "Intégration annulée",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return functionReturnValue;
                    ///txtDateFacture.SetFocus
                }
                else if (string.IsNullOrEmpty(strCloture))
                {
                    string cmd = "UPDATE F_JMOUV SET JM_Cloture = 0 WHERE JO_Num = '" + strJo_Num + "' AND JM_Date = '" + strJM_Date + "'";
                    int yy = General.Execute(cmd, SAGE.adoSage);
                    //Ecriture du JM_Cloture à 0 car celui-ci a la valeur <Null>

                }

                //Début de la transaction Sage (sur adoSage) afin de pouvoir revenir en arrière
                //en cas de problèmes

                intDeroule = 2;

                SqlCommand cm = new SqlCommand("BEGIN TRANSACTION " + transactionName, SAGE.adoSage);
                int trans = cm.ExecuteNonQuery();
                blnTrans = true;
                //   1 - Table F_EcritureC
                //       1-1 - Une ligne par compte de charge avec montant HT
                //        Calcul du nombre de compte de charge et stockage de ceux-ci dans un tableau
                //        Récupération des montants HT pour chaque compte de charge
                rsCpta = new DataTable();

                strRequete = "SELECT CG_NUM,SUM(MontantLigne) AS Total FROM FactFournDetail ";
                strRequete = strRequete + " WHERE NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (EC_NoCptCharge IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY CG_NUM ";
                strRequete = strRequete + " HAVING (NOT (SUM(MontantLigne) IS NULL)) AND (SUM(MontantLigne) <> 0)";
                strRequete = strRequete + " ORDER BY CG_NUM ";

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    Array.Resize(ref tabCptCharges, 1);
                    Array.Resize(ref tabMontant, 1);


                    foreach (DataRow RCpta in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }
                        tabCptCharges[i] = RCpta["CG_NUM"] + "";
                        tabMontant[i] = RCpta["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbHT = dbHT + Convert.ToDouble(tabMontant[i]);
                        dbHtAutoLiquidation = dbHT;

                        i = i + 1;
                    }

                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel, EC_DateRelance, CT_Num, [Date facture], CT_NumCont ";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);

                    for (i = 0; i <= tabCptCharges.Length - 1; i++)
                    {
                        DataRow DRCpt = rsCpta.NewRow();

                        DRCpt["JO_Num"] = strJo_Num;
                        DRCpt["EC_NoLink"] = 0;
                        DRCpt["JM_Date"] = strJM_Date;
                        //DRCpt["EC_Jour"] = dtDateFacture.Day;
                        DRCpt["EC_Jour"] = dtDateNewFact.Day;

                        //DRCpt["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        DRCpt["EC_Date"] = DateTime.Now;

                        DRCpt["EC_Piece"] = General.Left(sNofacture, 13);
                        DRCpt["EC_RefPiece"] = General.Left(sNofacture, 17);
                        DRCpt["CG_NUM"] = tabCptCharges[i];
                        DRCpt["EC_Intitule"] = General.Left(sRaisonSocial, 35);

                        if (General.sActiveModeReglementSageFourn == "1")
                            DRCpt["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            DRCpt["N_Reglement"] = 0;

                        DRCpt["EC_Parite"] = cParite;  //6.55957
                        DRCpt["EC_Quantite"] = 0;
                        DRCpt["N_Devise"] = 0; //2 '1:Franc 2:Euro 0:Aucun
                        DRCpt["EC_Sens"] = 0;
                        DRCpt["EC_No"] = 0;
                        DRCpt["EC_Montant"] = tabMontant[i];
                        DRCpt["EC_Lettre"] = 0;
                        DRCpt["EC_Point"] = 0;
                        DRCpt["EC_Impression"] = 0;
                        DRCpt["EC_Cloture"] = 0;
                        DRCpt["EC_CType"] = 0;
                        DRCpt["EC_Rappel"] = 0;
                        //=== modif du 18 /02/2011(ajout d'un champs)
                        DRCpt["EC_DateRelance"] = "01/01/1900";
                        //=== modif du 28 05 2019, mise a jour du champs CT_NUM
                        //DRCpt["CT_NUM "] = General.Left(sCptFourn, 17);
                        DRCpt["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        DRCpt["CT_NumCont"] = General.Left(sCptFourn, 17);
                        rsCpta.Rows.Add(DRCpt.ItemArray);
                        rsCptaAdo.Update();

                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        string EC_No = cmd.ExecuteScalar().ToString();
                        DRCpt["EC_No"] = EC_No;

                        int xx = General.Execute("UPDATE FactFournDetail SET EC_NoCptCharge=" + EC_No + "" +
                            " WHERE NoAutoFacture=" + lNoAutoFacture + " AND CG_NUM='" + tabCptCharges[i] + "'");

                    }
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                //       1-2 - Une ligne par compte de TVA (pour ensuite les intégrer dans)
                //                   le registre de taxe)
                //        Calcul du nombre de compte de TVA et stockage de ceux-ci dans un tableau
                //        Récupération des montants des différentes taxe pour chaque compte de TVA
                if (lAjusteTva == 0)
                {

                    strRequete = "SELECT CG_Num,TaxeCG_NUM,(SUM(MontantLigne)*(TaxeTA_Taux/100)) AS Total FROM FactFournDetail ";
                    strRequete = strRequete + " WHERE NoAutoFacture=" + lNoAutoFacture + " AND ";
                    strRequete = strRequete + " (EC_NoCptTva IS NULL) AND QteLivre>0";
                    strRequete = strRequete + " GROUP BY CG_Num,TaxeCG_NUM,TaxeTA_Taux ";
                    strRequete = strRequete + " HAVING (NOT (SUM(MontantLigne)*(TaxeTA_Taux/100) IS NULL)) AND (SUM(MontantLigne)*(TaxeTA_Taux/100) <> 0)";
                    strRequete = strRequete + " ORDER BY TaxeCG_NUM,CG_Num";
                }
                else
                {
                    strRequete = "SELECT NoAutoFacture, CptTVA  AS TaxeCG_Num, SUM(TVA) AS TOTAL"
                                 + " From FactFournHtTvaCpta"
                                 + " GROUP BY NoAutoFacture, CptTVA"
                                 + " HAVING      (NoAutoFacture =" + lNoAutoFacture + ")";

                }

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabCptCharges = new string[1];
                    tabMontant = new string[1];


                    foreach (DataRow DRC in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }

                        tabCptTVA[i] = DRC["TaxeCG_Num"] + "";
                        tabMontant[i] = DRC["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbTVA = Convert.ToDouble(tabMontant[i]) + dbTVA;
                        if (lAjusteTva == 0)
                        {
                            tabCptCharges[i] = DRC["CG_NUM"] + "";
                        }
                        else if (lAjusteTva == 1)
                        {
                            tabCptCharges[i] = "";
                        }
                        i = i + 1;

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel , EC_DateRelance, CT_Num, [Date facture], CT_NumCont ";



                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        DataRow RC = rsCpta.NewRow();

                        RC["JO_Num"] = strJo_Num;
                        RC["EC_NoLink"] = 0;
                        RC["JM_Date"] = strJM_Date;
                        //RC["EC_Jour"] = dtDateFacture.Day;
                        RC["EC_Jour"] = dtDateNewFact.Day;

                        //RC["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        RC["EC_Date"] = DateTime.Now;

                        RC["EC_Piece"] = General.Left(sNofacture, 13);
                        RC["EC_RefPiece"] = General.Left(sNofacture, 17);
                        RC["CG_NUM"] = tabCptTVA[i];
                        //!CG_NumCont = tabCptCharges(i)
                        RC["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                        if (General.sActiveModeReglementSageFourn == "1")
                            RC["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            RC["N_Reglement"] = 0;
                        RC["EC_Parite"] = cParite;  // 6.55957
                        RC["EC_Quantite"] = 0;
                        RC["N_Devise"] = 0;//2
                        RC["EC_Sens"] = 0;
                        RC["EC_Montant"] = tabMontant[i];
                        RC["EC_Lettre"] = 0;
                        RC["EC_Point"] = 0;
                        RC["EC_Impression"] = 0;
                        RC["EC_Cloture"] = 0;
                        RC["EC_CType"] = 0;
                        RC["EC_Rappel"] = 0;
                        RC["EC_No"] = 0;
                        //=== modif du 18 /02/2011(ajout d'un champs)
                        RC["EC_DateRelance"] = "01/01/1900";
                        //=== modif du 28 05 2019, mise a jour du champs CT_NUM
                        //RC["CT_NUM"] = General.Left(sCptFourn, 17);
                        RC["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        RC["CT_NumCont"] = General.Left(sCptFourn, 17);
                        rsCpta.Rows.Add(RC.ItemArray);
                        rsCptaAdo.Update();

                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        string EC_No = cmd.ExecuteScalar().ToString();
                        RC["EC_No"] = EC_No;

                        int xx = General.Execute("UPDATE FactFournDetail SET EC_NoCptTva=" + EC_No +
                                        " WHERE NoAutoFacture=" + lNoAutoFacture + " AND TaxeCG_NUM='"
                                        + tabCptTVA[i] + "' AND " + " CG_Num='" + tabCptCharges[i] + "'");
                    }


                    //=== modif du 21 janvier 2014 gestion de l'autoliquidation.
                    bAutoLiquidation = false;
                    General.sSQlAuto = "SELECT     TaxeCG_NUMautoLiquide From FactFournEntete"
                                       + " WHERE     NoAutoFacture = " + lNoAutoFacture;

                    General.sSQlAuto = AdoLib.fc_ADOlibelle(General.sSQlAuto);
                    if (!string.IsNullOrEmpty(General.sSQlAuto))
                    {
                        bAutoLiquidation = true;
                        for (i = 0; i <= tabCptTVA.Length - 1; i++)
                        {
                            DataRow RP = rsCpta.NewRow();

                            RP["JO_Num"] = strJo_Num;
                            RP["EC_NoLink"] = 0;
                            RP["JM_Date"] = strJM_Date;
                            //RP["EC_Jour"] = dtDateFacture.Day;
                            RP["EC_Jour"] = dtDateNewFact.Day;

                            // RP["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                            //=== modif du 28 05 2019, on affecte la date du jour.
                            RP["EC_Date"] = DateTime.Now;

                            RP["EC_Piece"] = General.Left(sNofacture, 13);
                            RP["EC_RefPiece"] = General.Left(sNofacture, 17);
                            //=== a faire rechercher le bon compte de tva.
                            RP["CG_NUM"] = General.sSQlAuto;
                            //!CG_NumCont = tabCptCharges(i)
                            RP["EC_Intitule"] = General.Left(sRaisonSocial, 35);

                            if (General.sActiveModeReglementSageFourn == "1")
                                RP["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                            else
                                RP["N_Reglement"] = 0;
                            RP["EC_Parite"] = cParite;  // 6.55957
                            RP["EC_Quantite"] = 0;
                            RP["N_Devise"] = 0; //2
                            //===  attention on change le sens dans le cas d'une autoliquidation.
                            RP["EC_Sens"] = 1;
                            RP["EC_Montant"] = tabMontant[i];
                            RP["EC_Lettre"] = 0;
                            RP["EC_Point"] = 0;
                            RP["EC_Impression"] = 0;
                            RP["EC_Cloture"] = 0;
                            RP["EC_CType"] = 0;
                            RP["EC_Rappel"] = 0;
                            RP["EC_No"] = 0;
                            //=== modif du 18 /02/2011(ajout d'un champs)
                            RP["EC_DateRelance"] = "01/01/1900";
                            //=== modif du 28 05 2019, mise a jour du champs CT_NUM
                            //RP["CT_NUM"] = General.Left(sCptFourn, 17);
                            RP["Date facture"] = dtDateFacture;
                            //=== modif du 12 06 2019, ajout du champ CT_NumCont
                            RP["CT_NumCont"] = General.Left(sCptFourn, 17);
                            rsCpta.Rows.Add(RP.ItemArray);
                            rsCptaAdo.Update();

                            SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                            string EC_No = cmd.ExecuteScalar().ToString();
                            RP["EC_No"] = EC_No;

                            int xx = General.Execute("UPDATE FactFournDetail SET EC_NoCptTvAautoLiquidation=" + EC_No +
                                            " WHERE NoAutoFacture=" + lNoAutoFacture + " AND TaxeCG_NUM='" +
                                            tabCptTVA[i] + "' AND " + " CG_Num='" + tabCptCharges[i] + "'");

                        }

                    }

                }
                //=== debut de l'operateur de condition: If Not rsCpta.EOF And Not rsCpta.bof Then



                //       1-3 - Une ligne pour le compte Fournisseur avec montant total TTC
                //        Calcul du nombre de compte de TVA et stockage de ceux-ci dans un tableau
                //        Récupération des montants TTC pour chaque compte de Charge
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                strRequete = "SELECT FactFournDetail.CG_Num,(SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) AS Total FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=FactFournEntete.NoAutoFacture ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournEntete.EC_NoFacture IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY FactFournDetail.CG_Num,FactFournDetail.TaxeTA_Taux ";
                strRequete = strRequete + " HAVING (NOT (SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) IS NULL) AND ((SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) <> 0)";
                strRequete = strRequete + " ORDER BY FactFournDetail.CG_Num";

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (lAjusteTva == 1)
                {
                    dbTTC = dbHT + dbTVA;

                }

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    TabCptFournisseur = new string[1];
                    tabMontant = new string[1];


                    TabCptFournisseur[0] = rsCpta.Rows[0]["CG_NUM"] + "";

                    tabMontant[0] = Convert.ToString(0);

                    foreach (DataRow dr in rsCpta.Rows)
                    {
                        tabMontant[0] = Convert.ToString(Convert.ToDouble(tabMontant[0]) + General.FncArrondir(Convert.ToDouble(General.nz(dr["Total"], "0")), 2));

                    }
                    tabMontant[0] = Convert.ToString(FncArrondirFact(tabMontant[0]));

                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont, CT_Num,  EC_Intitule,EC_Echeance, ";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";

                    //strReqIns = strReqIns & "  EC_ANType, EC_Devise, EC_ASPLink, EC_Norme," _
                    //& " TA_Provenance, EC_DateRelance, EC_StatusRegle, EC_MontantRegle, EC_NoCloture, "

                    //== modif du 8 juin 2011, le champs ASP_Link n'existe plus.
                    strReqIns = strReqIns + "  EC_ANType, EC_Devise,  EC_Norme," +
                                " TA_Provenance, EC_DateRelance, EC_StatusRegle, EC_MontantRegle, EC_NoCloture, ";


                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel, [Date facture], CT_NumCont";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);

                    for (i = 0; i <= TabCptFournisseur.Length - 1; i++)
                    {
                        DataRow dr = rsCpta.NewRow();

                        dr["JO_Num"] = strJo_Num;
                        dr["EC_NoLink"] = 0;
                        dr["JM_Date"] = strJM_Date;
                        //dr["EC_Jour"] = dtDateFacture.Day;
                        dr["EC_Jour"] = dtDateNewFact.Day;

                        //dr["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        dr["EC_Date"] = DateTime.Now;

                        dr["EC_Piece"] = General.Left(sNofacture, 13);
                        dr["EC_RefPiece"] = General.Left(sNofacture, 17);
                        dr["CG_NUM"] = strCG_Num;
                        // !CG_NumCont = TabCptFournisseur(i)
                        dr["CT_NUM"] = General.Left(sCptFourn, 17);
                        dr["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                        dr["EC_Echeance"] = string.Format(dtdateEcheance.ToString(), General.FormatDateSansHeureSQL);
                        if (General.sActiveModeReglementSageFourn == "1")
                            dr["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            dr["N_Reglement"] = 0;
                        dr["EC_Parite"] = cParite; //6.55957
                        dr["EC_Quantite"] = 0;
                        dr["N_Devise"] = 0;//2
                        dr["EC_Sens"] = 1;
                        if (bAutoLiquidation == true)
                        {
                            dr["EC_Montant"] = dbHtAutoLiquidation;
                        }
                        else if (lAjusteTva == 0)
                        {
                            dr["EC_Montant"] = tabMontant[i];
                        }
                        else
                        {
                            dr["EC_Montant"] = dbTTC;
                        }
                        dr["EC_Lettre"] = 0;
                        dr["EC_Point"] = 0;
                        dr["EC_Impression"] = 0;
                        dr["EC_Cloture"] = 0;
                        dr["EC_CType"] = 0;
                        dr["EC_Rappel"] = 0;
                        dr["EC_No"] = 0;
                        //=== modif du 18 02 2010.(ajout de noouveaux champs)
                        dr["EC_ANType"] = 0;
                        dr["EC_NoCloture"] = 0;
                        dr["EC_Devise"] = 0;
                        //== modif du 8 juin 2011, le champs ASP_Link n'existe plus.
                        //!EC_ASPLink = 0
                        dr["EC_Norme"] = 0;
                        dr["TA_Provenance"] = 0;
                        dr["EC_DateRelance"] = "01/01/1900";
                        dr["EC_StatusRegle"] = 0;
                        dr["EC_MontantRegle"] = 0;
                        dr["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        dr["CT_NumCont"] = General.Left(sCptFourn, 17);
                        rsCpta.Rows.Add(dr.ItemArray);
                        rsCptaAdo.Update();

                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        NoFactureSage = cmd.ExecuteScalar().ToString();
                        ///  txtNoComptable.Text = NoFactureSage
                        functionReturnValue = Convert.ToInt32(NoFactureSage);

                        int xx = General.Execute("UPDATE FactFournEntete SET EC_NoFacture=" + NoFactureSage +
                                        " WHERE NoAutoFacture=" + lNoAutoFacture);

                        int yy = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = 1, " +
                                        " RSTE_DateComptabilise ='" + dtJour + "'" + " WHERE FactNoautofacture =" + lNoAutoFacture);

                    }
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                //   2 - Table F_EcritureA
                //       2-1 - Une ligne par compte analytique immeuble (montant HT des
                //                   charges de l'immeuble)

                strRequete = "SELECT Immeuble.CodeImmeuble, ";
                strRequete = strRequete + " LEFT(Immeuble.NCompte,13) AS CptAnalytique, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande ON BonDeCommande.NoBonDeCommande=";
                strRequete = strRequete + " FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN Immeuble ON BonDeCommande.CodeImmeuble=";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueImm IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY Immeuble.NCompte,FactFournDetail.EC_NoCptCharge,";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                strRequete = strRequete + " ORDER BY Immeuble.CodeImmeuble ";

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);


                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptAnalytiqueImm = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];
                    foreach (DataRow r in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptAnalytiqueImm, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabCptAnalytiqueImm[i] = r["CodeImmeuble"] + "";
                        tabMontant[i] = r["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = r["EC_No"] + "";
                        tabCA_Num[i] = r["CptAnalytique"] + "";
                        i = i + 1;

                    }


                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";

                    for (i = 0; i <= tabCptAnalytiqueImm.Length - 1; i++)
                    {

                        strRequete = tabEC_No[i] + ","; //EC_No
                        strRequete = strRequete + c3 + ","; //N_Analytique
                        strRequete = strRequete + i + 1 + ","; //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";//CA_Num
                        strRequete = strRequete + "'" + tabMontant[i] + "',"; //EA_Montant
                        strRequete = strRequete + 0;//EA_Quantite
                        strRequete = strRequete + ")";

                        SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        int xx = cmd.ExecuteNonQuery();

                        int yy = General.Execute("UPDATE FactFournDetail SET AnalytiqueImm='1' WHERE NoAutoFacture=" +
                                        lNoAutoFacture + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                    }

                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                //       2-2 - Une ligne par compte analytique Affaire (montant HT des affaires
                //                   liées aux différents immeubles)

                strRequete = "SELECT Immeuble.CodeImmeuble, ";
                strRequete = strRequete + " LEFT(Immeuble.NCompte,13) AS CptAnalytique, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant, ";
                strRequete = strRequete + " BonDeCommande.NumFicheStandard ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande ON BonDeCommande.NoBonDeCommande=";
                strRequete = strRequete + " FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN Immeuble ON BonDeCommande.CodeImmeuble=";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueAffaire IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY Immeuble.NCompte,FactFournDetail.EC_NoCptCharge,";
                strRequete = strRequete + " Immeuble.CodeImmeuble, BonDeCommande.NumFicheStandard ";
                strRequete = strRequete +
                             " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<> 0 ";
                strRequete = strRequete + " ORDER BY Immeuble.CodeImmeuble ";
                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptAnalytiqueAffaire = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];
                    tabCptAnalytiqueAffImm = new string[1];


                    foreach (DataRow r in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptAnalytiqueAffaire, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                            Array.Resize(ref tabCptAnalytiqueAffImm, i + 1);
                        }
                        tabCptAnalytiqueAffaire[i] = r["Numfichestandard"] + "";
                        tabMontant[i] = r["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = r["EC_No"] + "";
                        tabCA_Num[i] = "AF" + r["Numfichestandard"] + "";
                        tabCptAnalytiqueAffImm[i] = r["CodeImmeuble"] + "";
                        i = i + 1;

                    }

                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";

                    for (i = 0; i <= tabCptAnalytiqueAffaire.Length - 1; i++)
                    {
                        //== modif du 18 decembre 2007, controle si le compte analytique de l'affaire existe,
                        //== si non création de celui-ci.
                        SAGE.VerifCreeAnalytique(tabCptAnalytiqueAffaire[i], tabCptAnalytiqueAffaire[i], tabCptAnalytiqueAffImm[i], true);
                        //TODO .this function close the adosage connection so the transaction stopped. and it throw a exception when command.ExecuteNonQuery() is called

                        strRequete = tabEC_No[i] + ","; //EC_No
                        strRequete = strRequete + c2 + ","; //N_Analytique
                        strRequete = strRequete + i + 1 + ",";//EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";//CA_Num
                        strRequete = strRequete + "'" + tabMontant[i] + "',"; //EA_Montant
                        strRequete = strRequete + 0; //EA_Quantite
                        strRequete = strRequete + ")";


                        SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        int xx = cmd.ExecuteNonQuery();

                        int yy = General.Execute("UPDATE FactFournDetail SET AnalytiqueAffaire='1' WHERE NoAutoFacture="
                                        + lNoAutoFacture + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");


                    }

                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                //       2-3 - Une ligne par compte analytique Activité (montant HT des activités
                //                   liées aux différents appels
                //                   (récupéré par les bons de commande))

                strRequete = "SELECT GestionStandard.AnalytiqueActivite, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande";
                strRequete = strRequete + "  ON BonDeCommande.NoBonDeCommande = FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN GestionStandard ";
                strRequete = strRequete + " ON BonDeCommande.NumFicheStandard = GestionStandard.NumFicheStandard ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueActivite IS NULL) AND QteLivre>0 ";
                //== modif rachid l analytique activité depend seulement si il existe une intervention.
                //strRequete = strRequete & " AND (NOT GestionStandard.AnalytiqueActivite IS NULL)"
                strRequete = strRequete + " GROUP BY GestionStandard.AnalytiqueActivite,FactFournDetail.EC_NoCptCharge";
                strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                strRequete = strRequete + " ORDER BY GestionStandard.AnalytiqueActivite ";
                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];

                    foreach (DataRow dr in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";

                        //== modif rachid du 1 aout 2005,l analytique activite se base
                        //== sur l'existence d'un devis ou pas.

                        //'tabCA_Num(i) = rsCpta!AnalytiqueActivite & ""
                        sSQLn = "SELECT DevisEnTete.NumeroDevis" + " FROM BonDeCommande INNER JOIN"
                                + " FactFournDetail " + " ON BonDeCommande.NoBonDeCommande = FactFournDetail.NoBcmd "
                                + " INNER JOIN" + " GestionStandard"
                                + " ON BonDeCommande.NumFicheStandard = GestionStandard.NumFicheStandard"
                                + " INNER JOIN DevisEnTete " +
                                " ON GestionStandard.NumFicheStandard = DevisEnTete.NumFicheStandard"
                                + " WHERE FactFournDetail.NoAutoFacture =" + lNoAutoFacture;

                        sSQLn = AdoLib.fc_ADOlibelle(sSQLn, true);

                        if (string.IsNullOrEmpty(sSQLn))
                        {
                            tabCA_Num[i] = AdoLib.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA " + " WHERE ActiviteTravaux='1'", false, SAGE.adoSage);
                        }
                        else
                        {
                            tabCA_Num[i] = AdoLib.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA " + " WHERE ActiviteDevis='1'", false, SAGE.adoSage);
                        }

                        i = i + 1;

                    }

                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";

                    for (i = 0; i <= tabCA_Num.Length - 1; i++)
                    {

                        strRequete = tabEC_No[i] + ",";  //EC_No
                        strRequete = strRequete + c1 + ","; //N_Analytique
                        strRequete = strRequete + i + 1 + ","; //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',"; //CA_Num
                        strRequete = strRequete + tabMontant[i] + ","; //EA_Montant
                        strRequete = strRequete + 0; //EA_Quantite
                        strRequete = strRequete + ")";

                        SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        int xx = cmd.ExecuteNonQuery();
                        int yy = General.Execute("UPDATE FactFournDetail SET AnalytiqueActivite='1' WHERE NoAutoFacture="
                            + lNoAutoFacture + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                    }

                }

                rsCptaAdo.Dispose();
                rsCpta.Clear();

                if (General.sComptaAnalySecteur == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameDelostal))
                {

                    //=== modif du 27 fevrier 2012, ajout d'une nouvelle analytque secteur.

                    //       2-4 - Une ligne par compte analytique SECTEUR (montant HT PAR SECTEUR
                    //       determiné par le type de fournisseur
                    strRequete = "SELECT     FactFournDetail.EC_NoCptCharge AS EC_No," +
                                 " SUM(FactFournDetail.MontantLigne) AS Montant, fournisseurArticle.TYFO_Code, " +
                                 " BonDeCommande.Acheteur , BonDeCommande.Codeimmeuble " +
                                 " FROM         FactFournDetail INNER JOIN" +
                                 " FactFournEntete ON FactFournDetail.NoAutoFacture = FactFournEntete.NoAutoFacture " +
                                 " INNER JOIN fournisseurArticle " +
                                 " ON FactFournEntete.CodeFournisseur = fournisseurArticle.Cleauto " +
                                 " LEFT OUTER JOIN BonDeCommande " +
                                 " ON FactFournDetail.NoBcmd = BonDeCommande.NoBonDeCommande " +
                                 " LEFT OUTER JOIN TYFO_TypeFourn " +
                                 " ON fournisseurArticle.TYFO_Code = TYFO_TypeFourn.TYFO_Code";

                    strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND "
                                            + " (FactFournDetail.AnalytiqueSecteur IS NULL) AND QteLivre>0 ";

                    strRequete = strRequete + " GROUP BY FactFournDetail.EC_NoCptCharge, fournisseurArticle.TYFO_Code,"
                                            + " BonDeCommande.Acheteur , BonDeCommande.Codeimmeuble";

                    strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                    strRequete = strRequete + " ORDER BY fournisseurArticle.TYFO_Code ";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    bSecteur4 = false;

                    if (rsCpta.Rows.Count > 0)
                    {
                        Ecrit = true;
                        i = 0;
                        tabMontant = new string[1];
                        tabEC_No = new string[1];
                        tabCA_Num = new string[1];


                        foreach (DataRow r in rsCpta.Rows)
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabCA_Num, i + 1);
                            }
                            tabMontant[i] = r["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = r["EC_No"] + "";

                            if (string.IsNullOrEmpty(General.nz(r["TYFO_Code"], "").ToString()))
                            {
                                //=== le type de fournisseur (TYFO_Code) n'est pas alimenté, on affecte le HT dans le secteur 7 (divers).
                                tabCA_Num[i] = General.cTYFO_Secteur7;

                            }
                            else if (General.nz(r["TYFO_Code"], "").ToString() == "2" ||
                                     General.nz(r["TYFO_Code"], "").ToString() == "3" ||
                                     General.nz(r["TYFO_Code"], "").ToString() == "4" ||
                                     General.nz(r["TYFO_Code"], "").ToString() == "5")
                            {
                                //=== le type de fournisseur (TYFO_Code)= 2 (fournisseur de bien)
                                //=== le type de fournisseur (TYFO_Code)= 3 (SOUS TRAITANT CAPACITE)
                                //=== le type de fournisseur (TYFO_Code)= 4 (SOUS TRAITANT COMPETENCE)
                                //=== le type de fournisseur (TYFO_Code)= 5 (SOUS TRAITANT CONTRAT)

                                //=== on prend le nom de l'acheteur du bon de commande, si cet  acheteur
                                //=== fait partie du groupe clim, on affecte le HT au secteur clim.
                                //=== sinon on recherche le secteur du responsable d'exploit de l'immeuble et
                                //=== on lui affecte le HT.

                                //=== cherche dans la table ANCL_AnalyClim si l'acheteur fait partie du groupe clim.
                                sSQLn = "SELECT   ANCL_User   From ANCL_AnalyClim WHERE ANCL_User = '"
                                        + r["Acheteur"] + "'";
                                sSQLn = AdoLib.fc_ADOlibelle(sSQLn, true);

                                if (!string.IsNullOrEmpty(sSQLn))
                                {
                                    //===secteur clim.
                                    tabCA_Num[i] = General.cTYFO_SecteurClim;
                                }
                                else
                                {
                                    sSQLn = "SELECT  ANSE_AnalySecteur.ANSE_secteur"
                                            + " FROM  Immeuble INNER JOIN"
                                            + " ANSE_AnalySecteur ON Immeuble.CodeRespExploit = ANSE_AnalySecteur.Matricule"
                                            + " WHERE Immeuble.CodeImmeuble = '"
                                            + StdSQLchaine.gFr_DoublerQuote(r["CodeImmeuble"].ToString()) + "'";
                                    sSQLn = AdoLib.fc_ADOlibelle(sSQLn);
                                    if (!string.IsNullOrEmpty(sSQLn))
                                    {
                                        tabCA_Num[i] = sSQLn;
                                    }
                                    else
                                    {
                                        tabCA_Num[i] = General.cTYFO_Secteur7;
                                    }
                                }

                            }
                            else if (General.nz(r["TYFO_Code"], "").ToString() == "1")
                            {
                                //=== le type de fournisseur (TYFO_Code)= 1 (fioul), on affecte le HT AU secteur 6 (fioul)
                                tabCA_Num[i] = General.cTYFO_SecteurFioul;

                                // ElseIf nz(rsCpta!TYFO_Code, "5") = "5" Then
                                //=== le type de fournisseur (TYFO_Code)= 5 (fournisseur contrat), on divise le HT par
                                //=== les 4 secteurs (secteur 1, secteur 2, secteur 3, secteur4).

                                //bSecteur4 = True
                            }


                            i = i + 1;

                        }

                        strReqIns = "INSERT INTO F_ECRITUREA (";
                        strReqIns = strReqIns + "EC_No,";
                        strReqIns = strReqIns + "N_Analytique,";
                        strReqIns = strReqIns + "EA_Ligne,";
                        strReqIns = strReqIns + "CA_Num,";
                        strReqIns = strReqIns + "EA_Montant,";
                        strReqIns = strReqIns + "EA_Quantite";
                        strReqIns = strReqIns + ") VALUES (";

                        for (i = 0; i <= tabCA_Num.Length - 1; i++)
                        {
                            strRequete = tabEC_No[i] + ",";//EC_No
                            strRequete = strRequete + c4 + ","; //N_Analytique
                            strRequete = strRequete + i + 1 + ",";//EA_Ligne
                            strRequete = strRequete + "'" + tabCA_Num[i] + "',";//CA_Num
                            strRequete = strRequete + tabMontant[i] + ",";//EA_Montant
                            strRequete = strRequete + 0;//EA_Quantite
                            strRequete = strRequete + ")";



                            SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                            int xx = cmd.ExecuteNonQuery();
                            int yy = General.Execute("UPDATE FactFournDetail SET AnalytiqueSecteur='1', CaNumSecteur ='" +
                                            tabCA_Num[i] + "' WHERE NoAutoFacture=" + lNoAutoFacture +
                                            " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                        }

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();
                }

                if (General.sComptaAnalySecteur == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameAmmann))
                {

                    //=== modif du 18  fevrier 2013, ajout d'une nouvelle analytque secteur.

                    //       2-4 - Une ligne par compte analytique SECTEUR (montant HT PAR SECTEUR
                    //       determiné par le code analytique du bon de commande.

                    strRequete =
                        "SELECT     FactFournDetail.EC_NoCptCharge AS EC_No, SUM(FactFournDetail.MontantLigne) AS Montant, BonDeCommande.Codeimmeuble, " +
                        "        BonDeCommande.ANAB_Code , ANAB_AnalytiqueBCD.CaNumSecteur " +
                        " FROM         ANAB_AnalytiqueBCD INNER JOIN " +
                        " BonDeCommande ON ANAB_AnalytiqueBCD.ANAB_Code = BonDeCommande.ANAB_Code RIGHT OUTER JOIN " +
                        " FactFournDetail INNER JOIN " +
                        " FactFournEntete ON FactFournDetail.NoAutoFacture = FactFournEntete.NoAutoFacture ON " +
                        " BonDeCommande.NoBonDeCommande = FactFournDetail.NoBcmd ";

                    strRequete = strRequete + " WHERE     (FactFournDetail.NoAutoFacture = " + lNoAutoFacture + ") "
                                            + " AND (FactFournDetail.QteLivre > 0) AND (FactFournDetail.AnalytiqueSecteur IS NULL) ";

                    strRequete = strRequete + " GROUP BY FactFournDetail.EC_NoCptCharge, BonDeCommande.Codeimmeuble, BonDeCommande.ANAB_Code, ANAB_AnalytiqueBCD.CaNumSecteur ";

                    strRequete = strRequete + " HAVING      (NOT (SUM(FactFournDetail.MontantLigne) IS NULL)) AND (SUM(FactFournDetail.MontantLigne) <> 0) ";

                    strRequete = strRequete + " ORDER BY BonDeCommande.ANAB_Code ";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    bSecteur4 = false;

                    if (rsCpta.Rows.Count > 0)
                    {
                        Ecrit = true;
                        i = 0;
                        tabMontant = new string[1];
                        tabEC_No = new string[1];
                        tabCA_Num = new string[1];



                        foreach (DataRow d in rsCpta.Rows)
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabCA_Num, i + 1);
                            }
                            tabMontant[i] = d["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = d["EC_No"] + "";

                            if (string.IsNullOrEmpty(General.nz(d["ANAB_Code"], "").ToString()))
                            {
                                //=== si pas de code analytique, on affacte au secteur 7.
                                tabCA_Num[i] = General.cANAB_Administratif;

                            }
                            else
                            {
                                //==== secteur lié au bon de commande et la table  ANAB_AnalytiqueBCD.
                                tabCA_Num[i] = d["CaNumSecteur"].ToString();
                            }

                            i = i + 1;

                        }

                        strReqIns = "INSERT INTO F_ECRITUREA (";
                        strReqIns = strReqIns + "EC_No,";
                        strReqIns = strReqIns + "N_Analytique,";
                        strReqIns = strReqIns + "EA_Ligne,";
                        strReqIns = strReqIns + "CA_Num,";
                        strReqIns = strReqIns + "EA_Montant,";
                        strReqIns = strReqIns + "EA_Quantite";
                        strReqIns = strReqIns + ") VALUES (";

                        for (i = 0; i <= tabCA_Num.Length - 1; i++)
                        {

                            strRequete = tabEC_No[i] + ","; //EC_No
                            strRequete = strRequete + c4 + ",";//N_Analytique
                            strRequete = strRequete + i + 1 + ","; //EA_Ligne
                            strRequete = strRequete + "'" + tabCA_Num[i] + "',"; //CA_Num
                            strRequete = strRequete + tabMontant[i] + ","; //EA_Montant
                            strRequete = strRequete + 0; //EA_Quantite
                            strRequete = strRequete + ")";


                            SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                            int xx = cmd.ExecuteNonQuery();
                            int yy = General.Execute("UPDATE FactFournDetail SET AnalytiqueSecteur='1', CaNumSecteur ='"
                                            + tabCA_Num[i] + "' WHERE NoAutoFacture="
                                            + lNoAutoFacture + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                        }

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();
                }

                if (General.sServiceAnalytique == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameDelostal) &&
                    dtDateFacture >= General.dtServiceAnalytique)
                {

                    //=== modif du 29 septembre 2014, ajout d'une nouvelle analytque service.

                    //       2-4 - Une ligne par compte analytique SERVICE (montant HT PAR service
                    //       determiné par le service saisie sur le bon de commande.

                    strRequete = "SELECT     FactFournDetail.EC_NoCptCharge AS EC_No, " +
                                 " SUM(FactFournDetail.MontantLigne) AS Montant,  BonDeCommande.ServiceAnalytique, BonDeCommande.DateCommande " +
                                 " FROM         FactFournDetail INNER JOIN " +
                                 " BonDeCommande ON BonDeCommande.NoBonDeCommande = FactFournDetail.NoBcmd";

                    strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND "
                                            + " (FactFournDetail.AnalytiqueService IS NULL) AND QteLivre>0 ";

                    strRequete = strRequete + " GROUP BY FactFournDetail.EC_NoCptCharge, BonDeCommande.ServiceAnalytique, FactFournDetail.AnalytiqueService,  BonDeCommande.DateCommande  ";

                    strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                    strRequete = strRequete + " ORDER BY FactFournDetail.AnalytiqueService ";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    bSecteur4 = false;

                    //=== modif du 24 10 2014, controle la date du bon de commande, ne prends en compte uniquement les bon fait apres la date
                    if (rsCpta.Rows.Count > 0)
                    {
                        if (General.IsDate(rsCpta.Rows[0]["DateCommande"]))
                        {
                            if (Convert.ToDateTime(rsCpta.Rows[0]["DateCommande"]) >= General.dtServiceAnalytique)
                            {

                                Ecrit = true;
                                i = 0;
                                tabMontant = new string[1];
                                tabEC_No = new string[1];
                                tabCA_Num = new string[1];

                                foreach (DataRow dr in rsCpta.Rows)
                                {
                                    if (i > 0)
                                    {
                                        Array.Resize(ref tabMontant, i + 1);
                                        Array.Resize(ref tabEC_No, i + 1);
                                        Array.Resize(ref tabCA_Num, i + 1);
                                    }
                                    tabMontant[i] = dr["Montant"] + "";
                                    tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                                    tabEC_No[i] = dr["EC_No"] + "";

                                    tabCA_Num[i] = General.UCase(dr["ServiceAnalytique"].ToString()) + "";

                                    i = i + 1;

                                }

                                strReqIns = "INSERT INTO F_ECRITUREA (";
                                strReqIns = strReqIns + "EC_No,";
                                strReqIns = strReqIns + "N_Analytique,";
                                strReqIns = strReqIns + "EA_Ligne,";
                                strReqIns = strReqIns + "CA_Num,";
                                strReqIns = strReqIns + "EA_Montant,";
                                strReqIns = strReqIns + "EA_Quantite";
                                strReqIns = strReqIns + ") VALUES (";

                                for (i = 0; i <= tabCA_Num.Length - 1; i++)
                                {

                                    strRequete = tabEC_No[i] + ","; //EC_No
                                    strRequete = strRequete + c6 + ","; //N_Analytique
                                    strRequete = strRequete + i + 1 + ","; //EA_Ligne
                                    strRequete = strRequete + "'" + tabCA_Num[i] + "',"; //CA_Num
                                    strRequete = strRequete + tabMontant[i] + ","; //EA_Montant
                                    strRequete = strRequete + 0;//EA_Quantite
                                    strRequete = strRequete + ")";

                                    SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                                    int xx = cmd.ExecuteNonQuery();

                                    int yy = General.Execute("UPDATE FactFournDetail set AnalytiqueService ='"
                                                    + tabCA_Num[i] + "' WHERE NoAutoFacture=" + lNoAutoFacture
                                                    + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                                }

                            }

                        }
                    }
                    rsCpta.Dispose();
                    rsCpta.Clear();
                }


                //   3 - Table F_RegTaxe
                //       3-1 - Une ligne par Compte de taxe (montant de la TVA)

                strRequete = "SELECT FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux, ";
                strRequete = strRequete + " (SUM(FactFournDetail.MontantLigne)*(TaxeTA_Taux/100)) AS Montant, ";
                strRequete = strRequete + " FactFournEntete.EC_NoFacture AS EC_No";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=";
                strRequete = strRequete + " FactFournEntete.NoAutoFacture ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + lNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.RegistreTaxe IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY FactFournEntete.EC_NoFacture, ";
                strRequete = strRequete + " FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux ";
                strRequete = strRequete +
                             " HAVING (NOT (SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100)) IS NULL) AND (SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))<>0 ";
                strRequete = strRequete + " ORDER BY FactFournEntete.EC_NoFacture, ";
                strRequete = strRequete + " FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux ";

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabTauxBase = new string[1];

                    foreach (DataRow DRCpta in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabTauxBase, i + 1);
                        }
                        tabCptTVA[i] = DRCpta["TaxeCG_Num"] + "";
                        tabMontant[i] = DRCpta["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = DRCpta["EC_No"] + "";
                        tabTauxBase[i] = DRCpta["TaxeTA_Taux"] + "";
                        i = i + 1;

                        //==== modif du 27 janvier 2014, gestion de l'autoliquidation
                        //==== attention di plussieurs revoir le programme pour test.
                        //=== modif du 21 janvier 2014 gestion de l'autoliquidation.
                        General.sSQlAuto = "SELECT TaxeCG_NUMautoLiquide FROM FactFournDetail"
                                           + " WHERE     NoAutoFacture = " + lNoAutoFacture;

                        General.sSQlAuto = AdoLib.fc_ADOlibelle(General.sSQlAuto);
                        if (!string.IsNullOrEmpty(General.sSQlAuto))
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabCptTVA, i + 1);
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabTauxBase, i + 1);
                            }
                            //tabCptTVA(i) = rsCpta!TaxeCG_Num & ""
                            tabCptTVA[i] = General.sSQlAuto;
                            tabMontant[i] = DRCpta["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = DRCpta["EC_No"] + "";
                            tabTauxBase[i] = DRCpta["TaxeTA_Taux"] + "";
                            i = i + 1;
                        }

                    }


                    strReqIns = "INSERT INTO F_REGTAXE (";
                    strReqIns = strReqIns + "EC_No,RT_No,RT_Type, RT_DateReg,  RT_DatePiece,";
                    strReqIns = strReqIns + "CT_Num,";
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        strReqIns = strReqIns + "TA_Provenance0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "CG_Num0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_TTaux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Taux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "RT_Base0" + (i + 1) + ",";
                        strReqIns = strReqIns + "RT_Montant0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Code0" + (i + 1) + ",";
                        if (i == 4)
                            break; // TODO: might not be correct. Was : Exit For
                        //Pas plus de 5 boucles :
                        //limité par SAGE
                    }

                    strReqIns = strReqIns + "JM_Date ";
                    strReqIns = strReqIns + ") VALUES ( ";
                    i = 0;
                    strRequete = tabEC_No[i] + ",";
                    //EC_No
                    MaxRT_No = Convert.ToInt32(General.nz(AdoLib.fc_ADOlibelle("SELECT MAX(RT_No) AS Maximum FROM F_REGTAXE", false, SAGE.adoSage), "0"));
                    MaxRT_No = MaxRT_No + 1;
                    strRequete = strRequete + MaxRT_No + ","; //RT_No
                    strRequete = strRequete + "0,"; //RT_Type
                    strRequete = strRequete + "'" + DateTime.Today + "',"; //RT_DateReg
                    strRequete = strRequete + "'" + dtDateFacture + "',"; //RT_DatePiece
                    strRequete = strRequete + "'" + General.Left(sCptFourn, 17) + "',"; //CT_Num

                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        strRequete = strRequete + "0,"; //TA_Provenance0i
                        strRequete = strRequete + "'" + General.Left(tabCptTVA[i], 13) + "',";//CG_Num0i
                        strRequete = strRequete + "0,"; //TA_TTaux0i
                        strRequete = strRequete + General.nz(tabTauxBase[i], "0") + ","; //TA_Taux0i
                        strRequete = strRequete + "0,"; //RT_Base0i
                        strRequete = strRequete + General.nz(tabMontant[i], "0") + ",";  //RT_Montant0i
                        strRequete = strRequete + "'1',"; //TA_Code0i
                        if (i == 4)
                            break;
                        //Pas plus de 5 boucles :
                        //limité par SAGE
                    }

                    strRequete = strRequete + "'" + strJM_Date + "' ";
                    strRequete = strRequete + ")";


                    SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                    int xx = cmd.ExecuteNonQuery();

                    int yy = General.Execute("UPDATE FactFournDetail SET RegistreTaxe='1' WHERE "
                                    + "NoAutoFacture=" + lNoAutoFacture + "");
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();



                if (Ecrit == true)
                {
                    SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactionName, SAGE.adoSage);
                    int yy = cmd.ExecuteNonQuery();

                    int xx = General.Execute("UPDATE FactFournEntete SET Verrou='1' WHERE NoAutoFacture = " + lNoAutoFacture);
                    // lblNoComptable.Visible = True
                    // txtNoComptable.Visible = True
                    // fc_Verrouillage
                    // MsgBox "Facture intégrée." & vbCrLf & "N° comptable : " & NoFactureSage, vbInformation, "Intégration de la facture en comptabilité"
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "La facture n° " + sNofacture +
                        " n'est pas valide (quantités facturées à 0, pas de comptes de charges ...)." + "\r\n" +
                        "Intégration annulée.", "Intégration de la facture en compta", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    /// chkIntegration.value = 0
                    SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactionName, SAGE.adoSage);
                    int yy = cmd.ExecuteNonQuery();

                    int xx = General.Execute(
                        "UPDATE    FactFournDetail SET EC_NoCptCharge = NULL, EC_NoCptTva = NULL, AnalytiqueAffaire = NULL, " +
                        " AnalytiqueImm = NULL, RegistreTaxe = NULL, AnalytiqueActivite = NULL, AnalytiqueSecteur = NULL " +
                        " WHERE (NoAutoFacture=" + lNoAutoFacture + ")");

                    int z = General.Execute("UPDATE FactFournEntete SET EC_NoFacture=NULL WHERE NoAutoFacture=" + lNoAutoFacture + "");

                    int ww = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = NULL, " + " RSTE_DateComptabilise = NULL" + " WHERE FactNoautofacture =" + lNoAutoFacture);

                    //    adocnn.Execute "ROLLBACK TRANSACTION Trans2" & fncusername

                }

                SAGE.fc_CloseConnSage();
                if ((rsCpta != null))
                {
                    rsCpta.Clear();
                    rsCpta = null;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                if (intDeroule == 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le journal d'achat est ouvert sur un autre poste." + "\r\n" + "Son accés étant impossible, l'intégration de la facture est impossible.", "Intégration annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Erreurs.gFr_debug(ex, Name + " fc_IntegrerFacture");
                }
                else
                {
                    int xx;
                    if (blnTrans == true)
                    {

                        SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactionName, SAGE.adoSage);
                        int yy = cmd.ExecuteNonQuery();
                        //   Resume
                    }
                    xx = General.Execute("UPDATE    FactFournDetail SET EC_NoCptCharge = NULL, EC_NoCptTva = NULL, AnalytiqueAffaire = NULL, AnalytiqueImm = NULL, RegistreTaxe = NULL, AnalytiqueActivite = NULL WHERE (NoAutoFacture=" + lNoAutoFacture + ")");
                    xx = General.Execute("UPDATE FactFournEntete SET EC_NoFacture=NULL WHERE NoAutoFacture=" + lNoAutoFacture + "");
                    xx = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = NULL, " + " RSTE_DateComptabilise = NULL" + " WHERE FactNoautofacture =" + lNoAutoFacture);
                    Erreurs.gFr_debug(ex, Name + "  Erreur IntegrerFacture ");
                }
                return functionReturnValue;
            }
        }

        private int fc_IntegreFactSansCom(object lRSTE_NoAuto, DateTime dtDateFacture, string sCptFourn, string sNofacture, string sRaisonSocial, int lAjusteTva, DateTime dtdateEcheance, DateTime dtJour)
        {
            int functionReturnValue = 0;

            const string c1 = "1";
            //Plan analytique Activité (plan principal)
            const string c2 = "2";
            //Plan analytique Affaire
            const string c3 = "3";
            //Plan analytique Immeuble
            const string c4 = "4";
            const string c6 = "6";
            string strCT_Num = null;
            //Nom du compte Fournisseur
            string strCG_Num = null;
            //Numéro du compte fournisseur
            string strCA_Num = null;
            //Nom du compte analytique (pour chaque immeuble
            //et pour chaque affaire)
            string strCloture = null;
            //Etat du journal d'achat
            string strJo_Num = null;
            //Nom du journal des Achats
            string strJM_Date = null;
            //Date du journal des achats
            string[] tabCptCharges = null;
            //Tableau comptenant les différents
            //comptes de charges
            string[] tabCptTVA = null;
            //Tableau comptenant les différents
            //comptes de TVA
            string[] TabCptFournisseur = null;
            //Tableau comptenant les différents
            //comptes de Charges
            string[] tabMontant = null;
            //Tableau des montants
            string[] tabCptAnalytiqueImm = null;
            //Tableau des comptes analytiques Immeuble
            string[] tabCptAnalytiqueAffaire = null;
            //Tableau des comptes analytiques Affaire
            string[] tabCptAnalytiqueActivite = null;
            //Tableau des comptes analytiques Activité
            string[] tabCA_Num = null;
            //Tableau des comptes analytiques
            string[] tabEC_No = null;
            //Tableau de lien avec les champs
            //EC_No
            string[] tabCptAnalytiqueAffImm = null;
            // Tableau des codes immeubles liées à l'activité affaire.

            string[] tabTauxBase = null;
            //Base du taux : 19.6% par exemple
            string sAnalytiqueActivite = null;


            string strReqIns = null;
            string strRequete = null;
            int i = 0;
            int nbRecAff = 0;
            int MaxRT_No = 0;
            //Pour l'intégration dans le registre de taxe
            bool Ecrit = false;
            bool blnTrans = false;
            int intDeroule = 0;

            string NoFactureSage = null;
            //Sert uniquement à afficher le numéro
            //d'enregistrement sage à la fin de l'intégration
            string sSQLn = null;

            DataTable rsCpta = default(DataTable);
            ModAdo rsCptaAdo = new ModAdo();

            double dbTTC = 0;
            double dbHT = 0;
            double dbTVA = 0;
            double dbDifSecteur = 0;


            bool bSecteur4 = false;
            double dbHTsecteur4 = 0;
            double dbCumulHTsecteur4 = 0;
            int xxx = 0;


            double dbHtAutoLiquidation = 0;
            bool bAutoLiquidation = false;

            DateTime dtDateNewFact = default(DateTime);
            ModAdo adoLib = new ModAdo();

            var transactioName = "Trans1" + General.Replace(General.fncUserName(), " ", "").Replace(".", "");

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante..");
                    return functionReturnValue;
                }


                intDeroule = 0;
                //Démarrage et ouverture de la connexion Sage

                Ecrit = false;
                blnTrans = false;
                dbHT = 0;
                dbTTC = 0;
                dbTVA = 0;
                dbHtAutoLiquidation = 0;




                SAGE.fc_OpenConnSage();

                intDeroule = 1;
                //Liaison au journal de charge : lecture et éventuellement écriture

                // Récupération du compte de vente (propre à chaque société)
                strJo_Num = General.gfr_liaison("JournalCharges");

                //Définition de la date du journal par rapport à celle de la facture
                //strJM_Date = "01/" + string.Format(dtDateFacture.Month.ToString(), "00") + "/" + dtDateFacture.Year;
                //=== modif du 12 06 2019, on calcule la date JM_Date et ec_jour par rapport au cloture.
                dtDateNewFact = GeneralXav.fc_DatePieceCLoture(dtDateFacture, strJo_Num);
                strJM_Date = "01/" + dtDateNewFact.Month.ToString("00") + "/" + dtDateNewFact.Year;


                //Numéro de compte asocié au plan du compte fournisseur
                strCG_Num = adoLib.fc_ADOlibelle("SELECT CG_NumPrinc FROM F_CompteT WHERE CT_Num='" + sCptFourn + "'", false, SAGE.adoSage);

                if (string.IsNullOrEmpty(adoLib.fc_ADOlibelle("SELECT JO_Num FROM F_JMOUV WHERE JO_NUM='" + strJo_Num + "'" + " AND JM_DATE='" + strJM_Date + "'", false, SAGE.adoSage)))
                {
                    //TODO: Prosudur stocke introuvable [transaction]
                    int xx = General.Execute("INSERT INTO F_JMOUV (JO_Num,JM_Date,JM_Cloture,JM_Impression)" + " VALUES ('" + strJo_Num + "','" + strJM_Date + "',0,0)", SAGE.adoSage);
                    strCloture = "0";
                }
                else
                {
                    //Récupération de l'état de clôture du journal
                    strCloture = adoLib.fc_ADOlibelle("SELECT JM_Cloture FROM F_JMOUV " + " WHERE JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'");
                }
                if (strCloture == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Le journal d'achat " + strJo_Num + " du " + strJM_Date + " est clôturé" + "\r\n" +
                        "Cette facture ne peut être intégrée pour la date inscrite", "Intégration annulée",
                        MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return functionReturnValue;
                    ///txtDateFacture.SetFocus
                }
                else if (string.IsNullOrEmpty(strCloture))
                {
                    int cmd = General.Execute("UPDATE F_JMOUV SET JM_Cloture=0 WHERE  JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'", SAGE.adoSage);

                    //Ecriture du JM_Cloture à 0 car celui-ci a la valeur <Null>
                    //General.Execute("UPDATE F_JMOUV SET JM_Cloture=0 WHERE  JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'");
                }

                //Début de la transaction Sage (sur adoSage) afin de pouvoir revenir en arrière
                //en cas de problèmes

                intDeroule = 2;

                SqlCommand cm = new SqlCommand("BEGIN TRANSACTION " + transactioName, SAGE.adoSage);
                int cmdTrans = cm.ExecuteNonQuery();


                //General.Execute("BEGIN TRANSACTION Trans1" + General.Replace(General.fncUserName(), " ", ""));
                blnTrans = true;
                //   1 - Table F_EcritureC
                //       1-1 - Une ligne par compte de charge avec montant HT
                //        Calcul du nombre de compte de charge et stockage de ceux-ci dans un tableau
                //        Récupération des montants HT pour chaque compte de charge
                rsCpta = new DataTable();

                strRequete = "SELECT    RSTE_CptChargeDef as CG_NUM, " + " RSTE_HT as Total "
                             + " From RSTE_ReadSoftEnTete " + " WHERE RSTE_NoAuto = " + lRSTE_NoAuto;

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    Array.Resize(ref tabCptCharges, 1);
                    Array.Resize(ref tabMontant, 1);


                    foreach (DataRow d in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }
                        tabCptCharges[i] = d["CG_NUM"] + "";
                        tabMontant[i] = d["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbHT = dbHT + Convert.ToDouble(tabMontant[i]);
                        dbHtAutoLiquidation = dbHT;
                        i = i + 1;

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel, EC_DateRelance , [Date facture], CT_NumCont";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);

                    for (i = 0; i <= tabCptCharges.Length - 1; i++)
                    {
                        DataRow dr = rsCpta.NewRow();
                        var _with5 = rsCpta;
                        dr["JO_Num"] = strJo_Num;
                        dr["EC_NoLink"] = 0;
                        dr["JM_Date"] = strJM_Date;
                        //dr["EC_Jour"] = dtDateFacture.Day;
                        dr["EC_Jour"] = dtDateNewFact.Day;

                        //dr["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        dr["EC_Date"] = DateTime.Now;
                        dr["EC_Piece"] = General.Left(sNofacture, 13);
                        dr["EC_RefPiece"] = General.Left(sNofacture, 17);
                        dr["CG_NUM"] = tabCptCharges[i];
                        dr["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                        if (General.sActiveModeReglementSageFourn == "1")
                            dr["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            dr["N_Reglement"] = 0;
                        dr["EC_Parite"] = cParite;
                        // 6.55957
                        dr["EC_Quantite"] = 0;
                        dr["N_Devise"] = 0;
                        //2 '1:Franc 2:Euro 0:Aucun
                        dr["EC_Sens"] = 0;
                        dr["EC_No"] = 0;
                        dr["EC_Montant"] = tabMontant[i];
                        dr["EC_Lettre"] = 0;
                        dr["EC_Point"] = 0;
                        dr["EC_Impression"] = 0;
                        dr["EC_Cloture"] = 0;
                        dr["EC_CType"] = 0;
                        dr["EC_Rappel"] = 0;
                        //=== modif du 18 /02/2011(ajout d'un champs)
                        dr["EC_DateRelance"] = "01/01/1900";
                        dr["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        dr["CT_NumCont"] = General.Left(sCptFourn, 17);

                        rsCpta.Rows.Add(dr.ItemArray);
                        rsCptaAdo.Update();
                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        string ss = cmd.ExecuteScalar().ToString();
                        dr["EC_No"] = ss;

                        int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set RSTE_EC_NoCptCharge =" + dr["EC_No"] +
                                        " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);
                    }
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                //       1-2 - Une ligne par compte de TVA (pour ensuite les intégrer dans)
                //                   le registre de taxe)
                //        Calcul du nombre de compte de TVA et stockage de ceux-ci dans un tableau
                //        Récupération des montants des différentes taxe pour chaque compte de TVA
                if (lAjusteTva == 0)
                {
                    strRequete = "SELECT RSTE_CptChargeDef as CG_Num, RSTE_CptTvaDef as TaxeCG_NUM "
                                 + " , RSTE_HT, RSTE_TauxTVA," + " RSTE_HT * (RSTE_TauxTVA / 100) AS Total"
                                 + " From RSTE_ReadSoftEnTete" + " WHERE RSTE_NoAuto =" + lRSTE_NoAuto;
                }
                else
                {
                    strRequete = "SELECT RSTE_CptChargeDef as CG_Num, RSTE_CptTvaDef as TaxeCG_NUM "

                    + " , RSTE_HT, "

                    + " SUM(RSTE_MtTVA) AS  Total "

                    + " From RSTE_ReadSoftEnTete"

                    + " WHERE RSTE_NoAuto =" + lRSTE_NoAuto + " GROUP BY RSTE_CptChargeDef, RSTE_CptTvaDef, RSTE_HT, RSTE_MtTVA";

                }

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabCptCharges = new string[1];
                    tabMontant = new string[1];

                    foreach (DataRow r in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }
                        tabCptTVA[i] = r["TaxeCG_Num"] + "";
                        tabMontant[i] = r["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbTVA = Convert.ToDouble(tabMontant[i]) + dbTVA;
                        if (lAjusteTva == 0)
                        {
                            tabCptCharges[i] = r["CG_NUM"] + "";
                        }
                        else if (lAjusteTva == 1)
                        {
                            tabCptCharges[i] = "";
                        }
                        i = i + 1;

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel, EC_DateRelance,[Date facture], CT_NumCont ";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        DataRow r = rsCpta.NewRow();

                        r["JO_Num"] = strJo_Num;
                        r["EC_NoLink"] = 0;
                        r["JM_Date"] = strJM_Date;
                        //r["EC_Jour"] = dtDateFacture.Day;
                        r["EC_Jour"] = dtDateNewFact.Day;

                        //r["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        r["EC_Date"] = DateTime.Now;
                        r["EC_Piece"] = General.Left(sNofacture, 13);
                        r["EC_RefPiece"] = General.Left(sNofacture, 17);
                        r["CG_NUM"] = tabCptTVA[i];
                        //!CG_NumCont = tabCptCharges(i)
                        r["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                        if (General.sActiveModeReglementSageFourn == "1")
                            r["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            r["N_Reglement"] = 0;
                        r["EC_Parite"] = cParite;
                        //6.55957
                        r["EC_Quantite"] = 0;
                        r["N_Devise"] = 0;
                        //2
                        r["EC_Sens"] = 0;
                        r["EC_Montant"] = tabMontant[i];
                        r["EC_Lettre"] = 0;
                        r["EC_Point"] = 0;
                        r["EC_Impression"] = 0;
                        r["EC_Cloture"] = 0;
                        r["EC_CType"] = 0;
                        r["EC_Rappel"] = 0;
                        r["EC_No"] = 0;
                        //=== modif du 18 /02/2011(ajout d'un champs)
                        r["EC_DateRelance"] = "01/01/1900";
                        r["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        r["CT_NumCont"] = General.Left(sCptFourn, 17);
                        rsCpta.Rows.Add(r.ItemArray);
                        rsCptaAdo.Update();
                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        string ss = cmd.ExecuteScalar().ToString();
                        r["EC_No"] = ss;
                        int yy = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set RSTE_EC_NoCptTva = " + r["EC_No"] +
                                        " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);
                    }

                    //=== modif du 21 janvier 2014 gestion de l'autoliquidation.
                    General.sSQlAuto = "SELECT     RSTE_CptAutoLiquidation" + " From RSTE_ReadSoftEnTete " +
                                       " WHERE     RSTE_NoAuto =" + lRSTE_NoAuto;
                    bAutoLiquidation = false;

                    General.sSQlAuto = adoLib.fc_ADOlibelle(General.sSQlAuto);
                    if (!string.IsNullOrEmpty(General.sSQlAuto))
                    {
                        bAutoLiquidation = true;
                        for (i = 0; i <= tabCptTVA.Length - 1; i++)
                        {
                            DataRow dd = rsCpta.NewRow();
                            var _with7 = rsCpta;
                            dd["JO_Num"] = strJo_Num;
                            dd["EC_NoLink"] = 0;
                            dd["JM_Date"] = strJM_Date;
                            //dd["EC_Jour"] = dtDateFacture.Day;
                            dd["EC_Jour"] = dtDateNewFact.Day;

                            //!EC_Date = Format(dtDateFacture, FormatDateSansHeureSQL)
                            //=== modif du 28 05 2019, on affecte la date du jour.
                            dd["EC_Date"] = DateTime.Now;
                            dd["EC_Date"] = string.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                            dd["EC_Piece"] = General.Left(sNofacture, 13);
                            dd["EC_RefPiece"] = General.Left(sNofacture, 17);
                            //!CG_NUM = tabCptTVA(i)
                            dd["CG_NUM"] = General.sSQlAuto;
                            //!CG_NumCont = tabCptCharges(i)
                            dd["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                            if (General.sActiveModeReglementSageFourn == "1")
                                dd["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                            else
                                dd["N_Reglement"] = 0;
                            dd["EC_Parite"] = cParite;
                            //6.55957
                            dd["EC_Quantite"] = 0;
                            dd["N_Devise"] = 0;
                            //2
                            //==== attention bien changer le sens en cas d'autoliquidation de la tva.
                            dd["EC_Sens"] = 1;
                            dd["EC_Montant"] = tabMontant[i];
                            dd["EC_Lettre"] = 0;
                            dd["EC_Point"] = 0;
                            dd["EC_Impression"] = 0;
                            dd["EC_Cloture"] = 0;
                            dd["EC_CType"] = 0;
                            dd["EC_Rappel"] = 0;
                            dd["EC_No"] = 0;
                            //=== modif du 18 /02/2011(ajout d'un champs)
                            dd["EC_DateRelance"] = "01/01/1900";
                            dd["Date facture"] = dtDateFacture;
                            //=== modif du 12 06 2019, ajout du champ CT_NumCont
                            dd["CT_NumCont"] = General.Left(sCptFourn, 17);
                            rsCpta.Rows.Add(dd.ItemArray);
                            ModAdoDetail.Update();
                            SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                            string ss = cmd.ExecuteScalar().ToString();
                            dd["EC_No"] = ss;
                            int yy = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set RSTE_EC_NoCptTvaAutoL = " +
                                             dd["EC_No"] + " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);
                        }
                    }

                }

                //       1-3 - Une ligne pour le compte Fournisseur avec montant total TTC
                //        Calcul du nombre de compte de TVA et stockage de ceux-ci dans un tableau
                //        Récupération des montants TTC pour chaque compte de Charge
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                strRequete = " SELECT RSTE_CptChargeDef as CG_Num," +
                             " (RSTE_HT) + (RSTE_HT * (RSTE_TauxTVA / 100)) AS total" + " From RSTE_ReadSoftEnTete" +
                             " WHERE RSTE_NoAuto = " + lRSTE_NoAuto;

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);


                if (lAjusteTva == 1)
                {
                    dbTTC = dbHT + dbTVA;

                }

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    TabCptFournisseur = new string[1];
                    tabMontant = new string[1];


                    TabCptFournisseur[0] = rsCpta.Rows[0]["CG_NUM"] + "";

                    tabMontant[0] = Convert.ToString(0);

                    foreach (DataRow drc in rsCpta.Rows)
                    {
                        tabMontant[0] = Convert.ToString(Convert.ToDouble(tabMontant[0]) + General.FncArrondir(Convert.ToDouble(General.nz(drc["Total"], "0")), 2));

                    }
                    tabMontant[0] = Convert.ToString(FncArrondirFact(tabMontant[0]));
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont, CT_Num,  EC_Intitule,EC_Echeance, ";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel,";

                    //== modif du 8 juin 2011, le champs ASP_Link n'existe plus.
                    strReqIns = strReqIns + "  EC_ANType, EC_Devise,  EC_Norme," +
                                " TA_Provenance, EC_DateRelance, EC_StatusRegle, EC_MontantRegle, EC_NoCloture, [Date facture], CT_NumCont";


                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + sNofacture + "'";

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);
                    for (i = 0; i <= TabCptFournisseur.Length - 1; i++)
                    {
                        DataRow R = rsCpta.NewRow();
                        var _with8 = rsCpta;
                        R["JO_Num"] = strJo_Num;
                        R["EC_NoLink"] = 0;
                        R["JM_Date"] = strJM_Date;
                        //R["EC_Jour"] = dtDateFacture.Day;
                        R["EC_Jour"] = dtDateNewFact.Day;

                        //R["EC_Date"] = String.Format(dtDateFacture.ToString(), General.FormatDateSansHeureSQL);
                        //=== modif du 28 05 2019, on affecte la date du jour.
                        R["EC_Date"] = DateTime.Now;
                        R["EC_Piece"] = General.Left(sNofacture, 13);
                        R["EC_RefPiece"] = General.Left(sNofacture, 17);
                        R["CG_NUM"] = strCG_Num;
                        // !CG_NumCont = TabCptFournisseur(i)
                        R["CT_NUM"] = General.Left(sCptFourn, 17);
                        R["EC_Intitule"] = General.Left(sRaisonSocial, 35);
                        R["EC_Echeance"] = String.Format(dtdateEcheance.ToString(), General.FormatDateSansHeureSQL);
                        if (General.sActiveModeReglementSageFourn == "1")
                            R["N_Reglement"] = fc_GetCodeReglementSage(sCptFourn);
                        else
                            R["N_Reglement"] = 0;
                        R["EC_Parite"] = cParite;
                        // 6.55957
                        R["EC_Quantite"] = 0;
                        R["N_Devise"] = 0;
                        //2
                        R["EC_Sens"] = 1;
                        if (bAutoLiquidation == true)
                        {
                            R["EC_Montant"] = dbHtAutoLiquidation;
                        }
                        else if (lAjusteTva == 0)
                        {
                            R["EC_Montant"] = tabMontant[i];
                        }
                        else
                        {
                            R["EC_Montant"] = dbTTC;
                        }
                        R["EC_Lettre"] = 0;
                        R["EC_Point"] = 0;
                        R["EC_Impression"] = 0;
                        R["EC_Cloture"] = 0;
                        R["EC_CType"] = 0;
                        R["EC_Rappel"] = 0;
                        R["EC_No"] = 0;

                        //=== modif du 18 02 2010.(ajout de noouveaux champs)
                        R["EC_ANType"] = 0;
                        R["EC_NoCloture"] = 0;
                        R["EC_Devise"] = 0;
                        //== modif du 8 juin 2011, le champs ASP_Link n'existe plus.
                        //!EC_ASPLink = 0
                        R["EC_Norme"] = 0;
                        R["TA_Provenance"] = 0;
                        R["EC_DateRelance"] = "01/01/1900";
                        R["EC_StatusRegle"] = 0;
                        R["EC_MontantRegle"] = 0;
                        R["Date facture"] = dtDateFacture;
                        //=== modif du 12 06 2019, ajout du champ CT_NumCont
                        R["CT_NumCont"] = General.Left(sCptFourn, 17);
                        rsCpta.Rows.Add(R.ItemArray);
                        rsCptaAdo.Update();
                        SqlCommand cmd = new SqlCommand("select max(EC_No) from F_ECRITUREC", SAGE.adoSage);
                        string ss = cmd.ExecuteScalar().ToString();
                        R["EC_No"] = ss;
                        NoFactureSage = R["EC_No"] + "";
                        ///  txtNoComptable.Text = NoFactureSage
                        functionReturnValue = Convert.ToInt32(NoFactureSage);

                        int yy = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = 1, " + " RSTE_EC_NoTTC = " +
                                         R["EC_No"] + "," + " RSTE_DateComptabilise ='" + dtJour + "'" +
                                         " WHERE RSTE_NoAuto =" + lRSTE_NoAuto);
                    }
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();


                //   2 - Table F_EcritureA
                //       2-1 - Une ligne par compte analytique Activité (montant HT des activités
                //                   liées aux différents appels
                //                   (récupéré par les bons de commande))

                strRequete = "SELECT     RSTE_Analytique as AnalytiqueActivite, "
                             + " RSTE_EC_NoCptCharge AS EC_No , RSTE_HT AS Montant "
                             + " From RSTE_ReadSoftEnTete" + " WHERE  RSTE_NoAuto = " + lRSTE_NoAuto;


                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];


                    foreach (DataRow dr in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";
                        tabCA_Num[i] = dr["AnalytiqueActivite"] + "";

                        i = i + 1;

                    }

                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";

                    for (i = 0; i <= tabCA_Num.Length - 1; i++)
                    {

                        strRequete = tabEC_No[i] + ",";
                        //EC_No
                        strRequete = strRequete + c1 + ",";
                        //N_Analytique
                        strRequete = strRequete + (i + 1) + ",";
                        //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                        //CA_Num
                        strRequete = strRequete + tabMontant[i] + ",";
                        //EA_Montant
                        strRequete = strRequete + 0;
                        //EA_Quantite
                        strRequete = strRequete + ")";
                        SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        int yy = cmd.ExecuteNonQuery();//TODO . this query sometimes throw an exception foreign key constraint .verify code VB6

                        int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set AnalytiqueActivite = 1 "
                                        + " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);
                    }

                }

                rsCptaAdo.Dispose();
                rsCpta.Clear();

                if (General.sComptaAnalySecteur == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameDelostal))
                {

                    strRequete = "SELECT     RSTE_ReadSoftEnTete.RSTE_EC_NoCptCharge AS EC_No," +
                                 " RSTE_ReadSoftEnTete.RSTE_HT AS Montant, fournisseurArticle.TYFO_Code" +
                                 " FROM         RSTE_ReadSoftEnTete INNER JOIN" +
                                 " fournisseurArticle ON RSTE_ReadSoftEnTete.RSTE_CodeFournCleAuto = fournisseurArticle.Cleauto" +
                                 " WHERE  RSTE_NoAuto = " + lRSTE_NoAuto;

                    bSecteur4 = false;

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    if (rsCpta.Rows.Count > 0)
                    {
                        Ecrit = true;
                        i = 0;
                        tabMontant = new string[1];
                        tabEC_No = new string[1];
                        tabCA_Num = new string[1];



                        foreach (DataRow r in rsCpta.Rows)
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabCA_Num, i + 1);
                            }
                            tabMontant[i] = r["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = r["EC_No"] + "";

                            if (string.IsNullOrEmpty(General.nz(r["TYFO_Code"], "").ToString()))
                            {
                                //=== le type de fournisseur (TYFO_Code) n'est pas alimenté, on affecte le HT dans le secteur 7 (divers).
                                tabCA_Num[i] = General.cTYFO_Secteur7;

                            }
                            else if (General.nz(r["TYFO_Code"], "").ToString() == "2" ||
                                     General.nz(r["TYFO_Code"], "").ToString() == "3" ||
                                     General.nz(r["TYFO_Code"], "").ToString() == "4")
                            {
                                //=== le type de fournisseur (TYFO_Code)= 2 (fournisseur de bien)
                                //=== le type de fournisseur (TYFO_Code)= 3 (SOUS TRAITANT CAPACITE)
                                //=== le type de fournisseur (TYFO_Code)= 4 (SOUS TRAITANT COMPETENCE)
                                //=== le type de fournisseur (TYFO_Code)= 5 (SOUS TRAITANT CONTRAT)

                                //=== affectation du au ht dans le secteur 7.
                                tabCA_Num[i] = General.cTYFO_Secteur7;


                            }
                            else if (General.nz(r["TYFO_Code"], "").ToString() == "1")
                            {
                                //=== le type de fournisseur (TYFO_Code)= 1 (fioul), on affecte le HT AU secteur 6 (fioul)
                                tabCA_Num[i] = General.cTYFO_SecteurFioul;

                            }
                            else if (General.nz(r["TYFO_Code"], "5").ToString() == "5")
                            {
                                //=== le type de fournisseur (TYFO_Code)= 5 (fournisseur contrat), on divise le HT par
                                //=== les 4 secteurs (secteur 1, secteur 2, secteur 3, secteur4).

                                bSecteur4 = true;
                            }


                            i = i + 1;

                        }

                        strReqIns = "INSERT INTO F_ECRITUREA (";
                        strReqIns = strReqIns + "EC_No,";
                        strReqIns = strReqIns + "N_Analytique,";
                        strReqIns = strReqIns + "EA_Ligne,";
                        strReqIns = strReqIns + "CA_Num,";
                        strReqIns = strReqIns + "EA_Montant,";
                        strReqIns = strReqIns + "EA_Quantite";
                        strReqIns = strReqIns + ") VALUES (";

                        for (i = 0; i <= tabCA_Num.Length - 1; i++)
                        {
                            if (bSecteur4 == true)
                            {

                                dbHTsecteur4 = General.FncArrondir(Convert.ToDouble(tabMontant[i]) / 4);
                                dbCumulHTsecteur4 = 0;

                                for (xxx = 1; xxx <= 4; xxx++)
                                {
                                    if (xxx == 4)
                                    {
                                        //=== controle si il y a une difference entre le total ht et
                                        //=== le cumul du total ht/4
                                        dbCumulHTsecteur4 = dbCumulHTsecteur4 + dbHTsecteur4;
                                        if (Convert.ToDecimal(tabMontant[i]) != Convert.ToDecimal(dbCumulHTsecteur4))
                                        {
                                            dbDifSecteur = 0;
                                            dbDifSecteur = Convert.ToDouble(tabMontant[i]) - dbCumulHTsecteur4;
                                            //dbCumulHTsecteur4 = CCur(tabCA_Num(i) - dbCumulHTsecteur4)
                                            dbHTsecteur4 = dbHTsecteur4 + dbDifSecteur;

                                        }
                                    }

                                    strRequete = tabEC_No[i] + ",";
                                    //EC_No
                                    strRequete = strRequete + c4 + ",";
                                    //N_Analytique
                                    strRequete = strRequete + (i + 1) + (xxx - 1) + ",";
                                    //EA_Ligne
                                    strRequete = strRequete + "'SECTEUR" + xxx + "',";
                                    //CA_Num
                                    strRequete = strRequete + dbHTsecteur4 + ",";
                                    //EA_Montant
                                    strRequete = strRequete + 0;
                                    //EA_Quantite
                                    strRequete = strRequete + ")";

                                    SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                                    int yy = cmd.ExecuteNonQuery();

                                    dbCumulHTsecteur4 = dbCumulHTsecteur4 + dbHTsecteur4;
                                }

                                //adoSage.Execute strReqIns & strRequete

                                int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set AnalytiqueSecteur = 1 ," +
                                                 "  SecteurAnaly = '4SECTEUR'" + " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);


                            }
                            else
                            {
                                strRequete = tabEC_No[i] + ",";
                                //EC_No
                                strRequete = strRequete + c4 + ",";
                                //N_Analytique
                                strRequete = strRequete + (i + 1) + ",";
                                //EA_Ligne
                                strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                                //CA_Num
                                strRequete = strRequete + tabMontant[i] + ",";
                                //EA_Montant
                                strRequete = strRequete + 0;
                                //EA_Quantite
                                strRequete = strRequete + ")";

                                SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                                int yy = cmd.ExecuteNonQuery();
                                int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set AnalytiqueSecteur = 1, " +
                                                "  SecteurAnaly = '" + tabCA_Num[i] + "'" + " WHERE RSTE_NoAuto = " +
                                                lRSTE_NoAuto);

                            }
                        }
                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();
                }
                if (General.sComptaAnalySecteur == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameAmmann))
                {

                    strRequete = "SELECT     RSTE_ReadSoftEnTete.RSTE_EC_NoCptCharge AS EC_No," +
                                 " RSTE_ReadSoftEnTete.RSTE_HT AS Montant " + " FROM         RSTE_ReadSoftEnTete " +
                                 " WHERE  RSTE_NoAuto = " + lRSTE_NoAuto;

                    bSecteur4 = false;

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    if (rsCpta.Rows.Count > 0)
                    {
                        Ecrit = true;
                        i = 0;
                        tabMontant = new string[1];
                        tabEC_No = new string[1];
                        tabCA_Num = new string[1];

                        foreach (DataRow dr in rsCpta.Rows)
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabCA_Num, i + 1);
                            }
                            tabMontant[i] = dr["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = dr["EC_No"] + "";


                            tabCA_Num[i] = General.cANAB_Administratif;

                            i = i + 1;

                        }

                        strReqIns = "INSERT INTO F_ECRITUREA (";
                        strReqIns = strReqIns + "EC_No,";
                        strReqIns = strReqIns + "N_Analytique,";
                        strReqIns = strReqIns + "EA_Ligne,";
                        strReqIns = strReqIns + "CA_Num,";
                        strReqIns = strReqIns + "EA_Montant,";
                        strReqIns = strReqIns + "EA_Quantite";
                        strReqIns = strReqIns + ") VALUES (";

                        for (i = 0; i <= tabCA_Num.Length - 1; i++)
                        {

                            strRequete = tabEC_No[i] + ",";
                            //EC_No
                            strRequete = strRequete + c4 + ",";
                            //N_Analytique
                            strRequete = strRequete + (i + 1) + ",";
                            //EA_Ligne
                            strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                            //CA_Num
                            strRequete = strRequete + tabMontant[i] + ",";
                            //EA_Montant
                            strRequete = strRequete + 0;
                            //EA_Quantite
                            strRequete = strRequete + ")";

                            SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                            int yy = cmd.ExecuteNonQuery();
                            int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set AnalytiqueSecteur = 1, " +
                                            "  SecteurAnaly = '" + tabCA_Num[i] + "'" + " WHERE RSTE_NoAuto = " +
                                            lRSTE_NoAuto);
                        }

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();

                }

                if (General.sServiceAnalytique == "1" &&
                    General.UCase(General.adocnn.Database.ToString()) == General.UCase(General.cNameDelostal) &&
                    dtDateFacture >= General.dtServiceAnalytique)
                {
                    //=== modif du 29 septembre 2014, ajout d'une nouvelle analytque service.

                    //       2-4 - Une ligne par compte analytique SERVICE (montant HT PAR service
                    //       determiné par le service saisie sur la fiche fournisseur, si celle ci n'est pas saisie on affecte
                    //       la valeur ADM(service administratif.
                    strRequete = "SELECT     RSTE_ReadSoftEnTete.RSTE_EC_NoCptCharge AS EC_No," +
                                 " RSTE_ReadSoftEnTete.RSTE_HT AS Montant, RSTE_ReadSoftEnTete.RSTE_ServiceAnalytique " +
                                 " FROM         RSTE_ReadSoftEnTete " + " WHERE  RSTE_NoAuto = " + lRSTE_NoAuto;

                    bSecteur4 = false;

                    rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                    if (rsCpta.Rows.Count > 0)
                    {
                        Ecrit = true;
                        i = 0;
                        tabMontant = new string[1];
                        tabEC_No = new string[1];
                        tabCA_Num = new string[1];



                        foreach (DataRow drr in rsCpta.Rows)
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabCA_Num, i + 1);
                            }
                            tabMontant[i] = drr["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = drr["EC_No"] + "";

                            tabCA_Num[i] = General.nz(drr["RSTE_ServiceAnalytique"], cAnalyServiceADM).ToString();

                            i = i + 1;

                        }

                        strReqIns = "INSERT INTO F_ECRITUREA (";
                        strReqIns = strReqIns + "EC_No,";
                        strReqIns = strReqIns + "N_Analytique,";
                        strReqIns = strReqIns + "EA_Ligne,";
                        strReqIns = strReqIns + "CA_Num,";
                        strReqIns = strReqIns + "EA_Montant,";
                        strReqIns = strReqIns + "EA_Quantite";
                        strReqIns = strReqIns + ") VALUES (";

                        for (i = 0; i <= tabCA_Num.Length - 1; i++)
                        {

                            strRequete = tabEC_No[i] + ",";
                            //EC_No
                            strRequete = strRequete + c6 + ",";
                            //N_Analytique
                            strRequete = strRequete + (i + 1) + ",";
                            //EA_Ligne
                            strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                            //CA_Num
                            strRequete = strRequete + tabMontant[i] + ",";
                            //EA_Montant
                            strRequete = strRequete + 0;
                            //EA_Quantite
                            strRequete = strRequete + ")";

                            SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                            int yy = cmd.ExecuteNonQuery();
                            General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set RSTE_MajServAnalytique = 1 " +
                                            " WHERE RSTE_NoAuto = " + lRSTE_NoAuto);
                        }

                    }
                    rsCptaAdo.Dispose();
                    rsCpta.Clear();
                }
                //   3 - Table F_RegTaxe
                strRequete = "SELECT     RSTE_CptTvaDef as TaxeCG_NUM, RSTE_TauxTVA as TaxeTA_Taux ," +
                             " RSTE_HT * (RSTE_TauxTVA / 100) AS Montant, RSTE_EC_NoTTC as EC_No " +
                             " From RSTE_ReadSoftEnTete" + " WHERE RSTE_NoAuto = " + lRSTE_NoAuto;

                rsCpta = rsCptaAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabTauxBase = new string[1];

                    foreach (DataRow rr in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabTauxBase, i + 1);
                        }
                        tabCptTVA[i] = rr["TaxeCG_Num"] + "";
                        tabMontant[i] = rr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = rr["EC_No"] + "";
                        tabTauxBase[i] = rr["TaxeTA_Taux"] + "";
                        i = i + 1;

                        //=== modif du 21 janvier 2014 gestion de l'autoliquidation.
                        General.sSQlAuto = "SELECT     RSTE_CptAutoLiquidation" + " From RSTE_ReadSoftEnTete " +
                                           " WHERE     RSTE_NoAuto =" + lRSTE_NoAuto;

                        General.sSQlAuto = adoLib.fc_ADOlibelle(General.sSQlAuto);
                        if (!string.IsNullOrEmpty(General.sSQlAuto))
                        {
                            if (i > 0)
                            {
                                Array.Resize(ref tabCptTVA, i + 1);
                                Array.Resize(ref tabMontant, i + 1);
                                Array.Resize(ref tabEC_No, i + 1);
                                Array.Resize(ref tabTauxBase, i + 1);
                            }
                            tabCptTVA[i] = rr["TaxeCG_Num"] + "";
                            tabMontant[i] = rr["Montant"] + "";
                            tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                            tabEC_No[i] = rr["EC_No"] + "";
                            tabTauxBase[i] = rr["TaxeTA_Taux"] + "";
                            i = i + 1;
                        }

                    }


                    strReqIns = "INSERT INTO F_REGTAXE (";
                    strReqIns = strReqIns + "EC_No,RT_No,RT_Type, RT_DateReg,  RT_DatePiece,";
                    strReqIns = strReqIns + "CT_Num,";
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        strReqIns = strReqIns + "TA_Provenance0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "CG_Num0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_TTaux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Taux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "RT_Base0" + (i + 1) + ",";
                        strReqIns = strReqIns + "RT_Montant0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Code0" + (i + 1) + ",";
                        if (i == 4)
                            break; // TODO: might not be correct. Was : Exit For
                                   //Pas plus de 5 boucles :
                                   //limité par SAGE
                    }

                    strReqIns = strReqIns + "JM_Date ";
                    strReqIns = strReqIns + ") VALUES ( ";
                    i = 0;
                    strRequete = tabEC_No[i] + ",";
                    //EC_No
                    MaxRT_No = Convert.ToInt32(General.nz(adoLib.fc_ADOlibelle("SELECT MAX(RT_No) AS Maximum FROM F_REGTAXE", false, SAGE.adoSage), "0"));
                    MaxRT_No = MaxRT_No + 1;
                    strRequete = strRequete + MaxRT_No + ",";
                    //RT_No
                    strRequete = strRequete + "0,";
                    //RT_Type
                    strRequete = strRequete + "'" + DateTime.Today + "',";
                    //RT_DateReg
                    strRequete = strRequete + "'" + dtDateFacture + "',";
                    //RT_DatePiece
                    strRequete = strRequete + "'" + General.Left(sCptFourn, 17) + "',";
                    //CT_Num

                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        strRequete = strRequete + "0,";
                        //TA_Provenance0i
                        strRequete = strRequete + "'" + General.Left(tabCptTVA[i], 13) + "',";
                        //CG_Num0i
                        strRequete = strRequete + "0,";
                        //TA_TTaux0i
                        strRequete = strRequete + General.nz(tabTauxBase[i], "0") + ",";
                        //TA_Taux0i
                        strRequete = strRequete + "0,";
                        //RT_Base0i
                        strRequete = strRequete + General.nz(tabMontant[i], "0") + ",";
                        //RT_Montant0i
                        strRequete = strRequete + "'1',";
                        //TA_Code0i
                        if (i == 4)
                            break; // TODO: might not be correct. Was : Exit For
                                   //Pas plus de 5 boucles :
                                   //limité par SAGE
                    }

                    strRequete = strRequete + "'" + strJM_Date + "' ";
                    strRequete = strRequete + ")";

                    SqlCommand cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                    int yy = cmd.ExecuteNonQuery();
                    int xx = General.Execute("UPDATE    RSTE_ReadSoftEnTete" + " Set RegistreTaxe = 1" + " WHERE RSTE_NoAuto = " +
                                    lRSTE_NoAuto);
                }
                rsCptaAdo.Dispose();
                rsCpta.Clear();

                if (Ecrit == true)
                {

                    SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactioName, SAGE.adoSage);
                    int cmdEndTrans = cmd.ExecuteNonQuery();
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "La facture n° " + sNofacture +
                        " n'est pas valide (quantités facturées à 0, pas de comptes de charges ...)." + "\r\n" +
                        "Intégration annulée.", "Intégration de la facture en compta", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    /// chkIntegration.value = 0
                    SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactioName, SAGE.adoSage);
                    int cmdEndTrans = cmd.ExecuteNonQuery();

                    int yy = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = NULL, " +
                                    " RSTE_EC_NoCptCharge = null, RSTE_EC_NoCptTva = null , " +
                                    " RSTE_EC_NoTTC = NULL, RSTE_Analytique = NULL , " +
                                    " AnalytiqueActivite = NULL , RegistreTaxe = NULL, " +
                                    " RSTE_DateComptabilise = NULL" + " WHERE RSTE_NoAuto =" + lRSTE_NoAuto);

                    //    adocnn.Execute "ROLLBACK TRANSACTION Trans2" & fncusername

                }
                SAGE.fc_CloseConnSage();
                if ((rsCpta != null))
                {
                    rsCpta.Clear();
                    rsCptaAdo.Dispose();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                if (intDeroule == 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le journal d'achat est ouvert sur un autre poste." + "\r\n" + "Son accés étant impossible, l'intégration de la facture est impossible.", "Intégration annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Erreurs.gFr_debug(ex, Name + "fc_IntegrerFacture");
                }
                else
                {
                    if (blnTrans == true)
                    {
                        SqlCommand cmd = new SqlCommand("COMMIT TRANSACTION " + transactioName, SAGE.adoSage);
                        int cmdEndTrans = cmd.ExecuteNonQuery();
                    }

                    int yy = General.Execute("UPDATE RSTE_ReadSoftEnTete SET RSTE_Comptabilise = NULL, " + " RSTE_EC_NoCptCharge = null, RSTE_EC_NoCptTva = null , "
                        + " RSTE_EC_NoTTC = NULL, RSTE_Analytique = NULL , " + " AnalytiqueActivite = NULL , RegistreTaxe = NULL, " + " RSTE_DateComptabilise = NULL"
                        + " WHERE RSTE_NoAuto =" + lRSTE_NoAuto);
                    Erreurs.gFr_debug(ex, Name + "Erreur IntegrerFacture");
                }
                return functionReturnValue;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bCtrlAll"></param>
        /// <param name="lTotErr"></param>
        /// <returns></returns>
        private int fc_CtrlAllOrOneInv(bool bCtrlAll, int lTotErr)
        {
            int functionReturnValue = 0;
            string sSQL = null;
            bool bDroitCorrige = false;

            //== controle la facture en cours ou toutes les factures
            //== Cette fonction utilise la objet recordset global rsRSTE( voir la fonction cmdVisuFactures_Click)
            //== la fonction retourne 1 si aucune erreur decelé, 2 dans le cas contraire.
            functionReturnValue = 1;

            //=== recherche si l'utilisateur a le droit de corriger
            //=== certaines erreurs.
            sSQL = "SELECT     AUT_Formulaire, AUT_Objet, AUT_Nom" + " From AUT_Autorisation"
                + " WHERE AUT_Formulaire ='" + Variable.cUserDocSageScan
                + "'" + " AND AUT_Objet ='" + General.cCorrigeFact + "'"
                + " AND AUT_Nom ='" + General.fncUserName() + "'";

            sSQL = AdoLib.fc_ADOlibelle(sSQL);
            bDroitCorrige = false;
            if (!string.IsNullOrEmpty(sSQL))
            {
                bDroitCorrige = true;
            }


            if (bCtrlAll == true)
            {

                txtAnomalies.Text = "";
                lTotErr = 0;

                foreach (DataRow dr in rsRSTE.Rows)
                {
                    dr["RSTE_Anomalie"] = 0;
                    if (fc_ctrlInvoice(bDroitCorrige, dr) == 2)
                    {
                        functionReturnValue = 2;
                        lTotErr = lTotErr + 1;
                    }
                }

            }
            else
            {
                fc_ctrlInvoice(bDroitCorrige);
            }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bDroitCorrige"></param>
        /// <param name="dr"></param>
        /// <returns></returns>
        private int fc_ctrlInvoice(bool bDroitCorrige, DataRow dr = null)
        {
            int functionReturnValue = 0;
            string sSQL = null;
            string sSQlFourn = "";
            string sResult = null;
            DataTable rs = default(DataTable);

            double dbHT = 0;
            double dbHtFourn = 0;
            double dbMtTva = 0;
            double dbTTC = 0;
            double dbSomme = 0;
            string sTexte = null;
            DataTable rsServ = default(DataTable);
            AdoLib = new ModAdo();
            if (bDroitCorrige == true)
            {
                sTexte = "OBSERVATION ";
            }
            else
            {
                sTexte = "";
            }

            //== la fonction retourne 1 si aucune erreur decelé, 2 dans le cas contraire.
            //== Cette fonction utilise la objet recordset global rsRSTE( voir la fonction cmdVisuFactures_Click)


            functionReturnValue = 1;

            //rsRSTE.MoveFirst

            //txtAnomalies = ""

            //Debug.Print rsRSTE!RSTE_NoLigne

            //Do While rsRSTE.EOF = False


            if (General.nz(dr["RSTE_Selectionne"], 0).ToString() == "0")
            {
                return functionReturnValue;
            }

            //============ Controle si le numero de bon de commande existe.
            //============ Si rsRSTE!RSTE_SansNoCommande est 0 donc l existe un bon de commande associé à cette facture.
            if (General.nz(dr["RSTE_SansNoCommande"], 0).ToString() == "0")
            {
                sSQL = "SELECT  NoBonDeCommande From BonDeCommande WHERE NoBonDeCommande =" + General.nz(dr["NoBonDeCommande"], 0);
                sResult = AdoLib.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sResult))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un numéro de bon de commande inexistant." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }

                //==== modif du  18 fevrier, controle si le bon de commande est attaché à une analytique (ammann).
                if (General.sComptaAnalySecteur == "1" && General.UCase(General.adocnn.Database) == General.UCase(General.cNameAmmann))
                {
                    sSQL = "SELECT  ANAB_Code From BonDeCommande WHERE NoBonDeCommande =" + General.nz(dr["NoBonDeCommande"], 0);
                    sResult = AdoLib.fc_ADOlibelle(sSQL);

                    if (string.IsNullOrEmpty(sResult))
                    {
                        txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "Le bon de commande" + General.nz(dr["NoBonDeCommande"], 0) + " n'est pas attaché à un code analytique." + "\r\n";
                        functionReturnValue = 2;
                        dr["RSTE_Anomalie"] = 1;
                        ModAdoRSTE.Update();
                    }

                }
            }

            //============ Controle si le code immeuble est correct.
            if (General.nz(dr["RSTE_SansNoCommande"], 0).ToString() == "0")
            {
                sSQL = "SELECT codeimmeuble FROM immeuble where Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(dr["CodeImmeuble"].ToString()) + "'";
                sResult = AdoLib.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(General.nz(dr["CodeImmeuble"], "").ToString()) || string.IsNullOrEmpty(sResult))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un code immeuble inexistant." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }
                else
                {
                    if (General.sComptaAnalySecteur == "1" && General.UCase(General.adocnn.Database) == General.UCase(General.cNameDelostal))
                    {
                        //=== modif du 04 01 2012, ne pas controler immeuble delostal.
                        if (General.UCase(dr["CodeImmeuble"].ToString()) != General.UCase("delostal"))
                        {
                            //=== modif du 05 03 2012.
                            //====== controle si il existe un responsable d'exploit et que celui ci fait
                            //====== parti du groupe analytique.
                            sSQL = "SELECT  ANSE_AnalySecteur.ANSE_secteur" + " FROM         Immeuble INNER JOIN" + " ANSE_AnalySecteur ON Immeuble.CodeRespExploit = ANSE_AnalySecteur.Matricule" + " WHERE Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(dr["CodeImmeuble"].ToString()) + "'";
                            sSQL = AdoLib.fc_ADOlibelle(sSQL);
                            if (string.IsNullOrEmpty(sSQL))
                            {
                                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + "(immeuble=> " + dr["CodeImmeuble"] + ") a un responsable d'exploitation inexistant ou erronée." + "\r\n";
                                functionReturnValue = 2;
                                dr["RSTE_Anomalie"] = 1;
                                ModAdoRSTE.Update();
                            }
                        }
                    }
                }
            }

            //=========== Controle le taux de TVA.
            //=========== Controle si le HT est different de 0.
            if (General.nz(dr["RSTE_HT"], 0).ToString() == "0")
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un montant HT incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();
            }

            //========== controle le TTC.
            if (General.nz(dr["RSTE_TTC"], 0).ToString() == "0")
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un montant TTC incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }
            //============ controle le montant de tva.
            //=== modif du 20 jion 2013, autorise les taux de tva à 0.
            if (General.nz(dr["RSTE_MtTVA"], 0).ToString() == "0" && dr["RSTE_TauxTVA"].ToString() != "0")
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un montant de TVA incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }

            //========== controle si le TTC = HT + Mt TVA
            dbTTC = Convert.ToDouble(General.nz(dr["RSTE_TTC"], 0));
            dbMtTva = Convert.ToDouble(General.nz(dr["RSTE_MtTVA"], 0));
            dbHT = Convert.ToDouble(General.nz(dr["RSTE_HT"], 0));
            //==== modif du 21 01 2014, gestion de l'autoliquidation.
            //=== controle si la facture a une tva autoliquidé, si le champs RSTE_CptAutoLiquidation est alimenté
            //=== alors le TTC doit etre égale au HT.

            if (!string.IsNullOrEmpty(General.nz(dr["RSTE_CptAutoLiquidation"], "").ToString()))
            {
                if (Convert.ToDecimal(dbTTC) != Convert.ToDecimal(dbHT))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a une tva auto liquidé, le HT doit être égale au TTC." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }

            }
            else
            {
                dbSomme = dbHT + dbMtTva;
                if (Convert.ToDecimal(dbTTC) != Convert.ToDecimal(dbSomme))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un TTC qui n'est pas égale à la somme du HT et du montant de TVA." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }
            }
            //========== controle de la date de facture.
            if (!General.IsDate(dr["RSTE_DateFacture"]))
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a une date de facture incorrecte." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();
            }
            else
            {
                //===> Mondir le 29.11.2020 pour ajouter les modfis de la version V29.11.2020
                if (dr["RSTE_DateFacture"].IsDate() && dr["RSTE_DateFacture"].ToDate() > DateTime.Now.Date)
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => "
                                   + "La facture numéro " + dr["RSTE_NoFacture"]
                                   + " a une date de facture postérieure à la date du jour.\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }
                //===> Fin Modif V29.11.2020
            }
            //========== controle la date d'écheance.
            if (!General.IsDate(dr["RSTE_DateEcheance"]))
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a une date d'échéance incorrecte." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }
            //========== controle si la date d'échéance est supérieur ou égale à la date d'échéance.
            if (General.IsDate(dr["RSTE_DateEcheance"]) && General.IsDate(dr["RSTE_DateFacture"]))
            {
                if (Convert.ToDateTime(dr["RSTE_DateEcheance"]) < Convert.ToDateTime(dr["RSTE_DateFacture"]))
                {

                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a une date d'échéance inférieure à la date de facture." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();

                }

            }
            //========= controle si le code et le compte fournisseur sont correct.
            //===> Mondir le 29.11.2020 pour ajouter les modfis de la version V29.11.2020
            //===> Added BCD_Obligatoire to the select Query
            sSQL = "SELECT     Nom, Cleauto, Raisonsocial, NCompte, Type, TYFO_Code, BCD_Obligatoire " + " From fournisseurArticle" + " WHERE   Cleauto =" + General.nz(dr["RSTE_CodeFournCleAuto"], 0);

            rs = AdoLib.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count == 0)
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un code fournisseur incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();
            }
            else
            {

                //==== teste si le compte fournisseur existe.
                if (string.IsNullOrEmpty(General.nz(dr["RSTE_CT_Num"], "").ToString()))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un compte fournisseur incorrect." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();
                }
                else
                {
                    //=== controle si ce compte existe en compta.
                    if (string.IsNullOrEmpty(fc_CtrCt_Num(dr["RSTE_CT_Num"].ToString())))
                    {
                        //== création du compte.
                        txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => Le compte fournisseur " + dr["RSTE_CT_Num"] + " n'existe pas en comptabilité, veuillez vous rendre sur la fiche fournisseur afin de le créer." + "\r\n";
                        functionReturnValue = 2;
                        dr["RSTE_Anomalie"] = 1;
                        ModAdoRSTE.Update();
                    }

                }
                //=== controle si ce fournisseur a un compte analytique secteur.
                //=== 05 03 2012.
                if (General.sComptaAnalySecteur == "1" && General.UCase(General.adocnn.Database) == General.UCase(General.cNameDelostal))
                {
                    if (string.IsNullOrEmpty(General.nz(rs.Rows[0]["TYFO_Code"], "").ToString()))
                    {
                        txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " est a attaché à un fournisseur (" + rs.Rows[0]["RaisonSocial"] + ") sans code analytique secteur. ." + "\r\n";
                        functionReturnValue = 2;
                        dr["RSTE_Anomalie"] = 1;
                        ModAdoRSTE.Update();

                    }
                }
                //=== controle si le type de fournisseur est bien renseigné.
                if (string.IsNullOrEmpty(rs.Rows[0]["Type"].ToString()))
                {
                    txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => Le fournisseur " + rs.Rows[0]["RaisonSocial"] + " à un type d'agence incorect (sous traitant, fournisseur) ." + "\r\n";
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                    ModAdoRSTE.Update();

                }
                dr["RSTE_FournSst"] = rs.Rows[0]["Type"];

                //===> Mondir le 29.11.2020 pour ajouter les modfis de la version V29.11.2020
                //'=== modif du 18 11 2020
                //'=== si BCD_Obligatoire = 1, bon de commande obligatoire
                //'=== si BCD_Obligatoire = 2, bon de commande obligatoire non obligatoire
                //'=== si BCD_Obligatoire = 3, avec ou sans bon de commande
                //'                    If nz(rsRSTE!BCD_Obligatoire, 0) = 1 And nz(rsRSTE!NoBonDeCommande, 0) = 0 Then
                //'                             txtAnomalies = txtAnomalies & "Ligne n° " & rsRSTE!RSTE_NoLigne _
                //'                                & " => Pour le fournisseur " & rs!RaisonSocial _
                //'                                & " le numéro de bon de commande est obligatoire." & vbCrLf
                //'                                fc_ctrlInvoice = 2
                //'                                rsRSTE!RSTE_Anomalie = 1
                //'                                rsRSTE.Update
                //'                    End If
                //    '
                //'                    ' === rends obligatoire la saisie
                //'                    If nz(rsRSTE!BCD_Obligatoire, 0) = 0 Then
                //'                            txtAnomalies = txtAnomalies & "Ligne n° " & rsRSTE!RSTE_NoLigne _
                //'                                & " => Pour le fournisseur " & rs!RaisonSocial _
                //'                                & " vous devez préciser sur la fiche fournisseur si le bon de commande est obligatoire." & vbCrLf
                //'                                fc_ctrlInvoice = 2
                //'                                rsRSTE!RSTE_Anomalie = 1
                //'                                rsRSTE.Update
                //'                    End If
                //===> Fin Modif Mondir
            }
            rs.Clear();
            rs = null;
            //============= controle si le compte de charge existe.
            if (string.IsNullOrEmpty(General.nz(dr["RSTE_CptChargeDef"], "").ToString()))
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un compte de charge incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }
            //============= controle si le compte de TVA existe.
            if (string.IsNullOrEmpty(General.nz(dr["RSTE_CptTvaDef"], "").ToString()))
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un compte de TVA incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }

            //=========== controle si le mode de reglement existe.
            if (string.IsNullOrEmpty(General.nz(dr["RSTE_ModeRgt"], "").ToString()))
            {
                txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un compte de TVA incorrect." + "\r\n";
                functionReturnValue = 2;
                dr["RSTE_Anomalie"] = 1;
                ModAdoRSTE.Update();

            }

            //============ controle si l'analitique activité existe.
            if (General.nz(dr["RSTE_SansNoCommande"], 0).ToString() == "1")
            {
                if (string.IsNullOrEmpty(General.nz(dr["RSTE_Analytique"], "").ToString()))
                {
                    sSQlFourn = "SELECT     Analytique From fournisseurArticle WHERE   Code = '" + ssRSTE.ActiveRow.Cells["RSTE_CodeFourn"].Text + "'";

                    using (var tmpModAdo = new ModAdo())
                        sSQlFourn = tmpModAdo.fc_ADOlibelle(sSQlFourn);

                    if (string.IsNullOrEmpty(sSQlFourn))
                    {
                        txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "La facture numéro " + dr["RSTE_NoFacture"] + " a un compte analytique inexistant." + "\r\n";
                        functionReturnValue = 2;
                        dr["RSTE_Anomalie"] = 1;
                        ModAdoRSTE.Update();
                    }
                    else
                    {
                        ssRSTE.ActiveRow.Cells["RSTE_CodeFourn"].Value = sSQlFourn;
                        ssRSTE.UpdateData();
                    }


                }
            }

            //=========== controle si le responsable d'exploitation existe et si il est bien renseigné.

            //=========== controle si le fournisseur une analytique secteur.

            //=========== contrôle si il existe des doublons au niveau du numéro de facture.

            sSQL = "SELECT     RSTE_NoAuto,RSTE_NoFacture, RSTE_HT, NoBonDeCommande " + " From RSTE_ReadSoftEnTete" + " WHERE RSTE_CodeFourn ='"
                + dr["RSTE_CodeFourn"] + "'" + " AND RSTE_NoAuto <> " + General.nz(dr["RSTE_NoAuto"], 0) + " AND  RSTE_NoFacture ='" + dr["RSTE_NoFacture"] + "'";

            sSQL = AdoLib.fc_ADOlibelle(sSQL);

            if (!string.IsNullOrEmpty(sSQL))
            {
                txtAnomalies.Text = txtAnomalies.Text + sTexte + "Ligne n° " + dr["RSTE_NoLigne"] + " => " + "Le numéro de facture " + dr["RSTE_NoFacture"] + " existe plusieurs fois pour le fournisseur " + dr["RSTE_CodeFourn"] + "." + "\r\n";
                if (bDroitCorrige == false)
                {
                    functionReturnValue = 2;
                    dr["RSTE_Anomalie"] = 1;
                }
                else
                {
                    dr["RSTE_Anomalie"] = 7;
                }
                ModAdoRSTE.Update();

            }

            //========== modif du 29 09 2014, ajout de l'axe analytique service, on recherche sur le bon de commande la service associé.
            if (General.sServiceAnalytique == "1")
            {
                if (General.nz(dr["RSTE_SansNoCommande"], 0).ToString() == "0")
                {
                    //=== facture fournisseur avec bon de commande

                    //===  on affecte cette analytique unbiquement au bon de commande apres la date défini dans dtServiceAnalytique.
                    //=== voir si on compare cette date de creation du bon de commande ou de la date de facture.
                    if (General.IsDate(dr["RSTE_DateFacture"].ToString()) && Convert.ToDateTime(dr["RSTE_DateFacture"]) >= General.dtServiceAnalytique)
                    {
                        //If nz(rsRSTE!RSTE_ServiceAnalytique, "") = "" Then

                        sSQL = "SELECT     NoBonDeCommande, ServiceAnalytique, DateCommande" + " From BonDeCommande WHERE NoBonDeCommande =" + General.nz(dr["NoBonDeCommande"], 0);

                        rsServ = AdoLib.fc_OpenRecordSet(sSQL);

                        //===> Mondir le 10.03.2021,  added this condition to check if there is a row, befor access to the DateCommande column #3534#
                        //===> There this same codition below i dont know why they didnt put it here, dont want to change the position of that conditon
                        if (rsServ.Rows.Count > 0)
                        {
                            if (General.IsDate(rsServ.Rows[0]["DateCommande"]))
                            {
                                //=== modif demandé par fred, seul les bon de commande crées à partir de la date
                                //=== contenu dans dtServiceAnalytique sont controlés
                                if (Convert.ToDateTime(rsServ.Rows[0]["DateCommande"]) >= General.dtServiceAnalytique)
                                {

                                    if (rsServ.Rows.Count > 0)
                                    {
                                        if (string.IsNullOrEmpty(General.nz(rsServ.Rows[0]["ServiceAnalytique"], "")
                                            .ToString()))
                                        {
                                            //=== on crée une anomalie si le champs service n'est pas renseigné.
                                            txtAnomalies.Text =
                                                txtAnomalies.Text + "Ligne n° " + dr["RSTE_NoLigne"] + " => " +
                                                "Le bon de commande " + General.nz(dr["NoBonDeCommande"], 0) +
                                                " n'est pas attaché à un code analytique service." + "\r\n";
                                            functionReturnValue = 2;
                                            dr["RSTE_Anomalie"] = 1;
                                            dr["RSTE_ServiceAnalytique"] = "";
                                            ModAdoRSTE.Update();
                                        }
                                        else
                                        {
                                            dr["RSTE_ServiceAnalytique"] =
                                                General.UCase(rsServ.Rows[0]["ServiceAnalytique"].ToString());
                                            ModAdoRSTE.Update();
                                        }
                                    }
                                }

                            }
                        }

                        AdoLib.Dispose();
                        rsServ.Clear();
                        rsServ = null;
                        //End If
                    }
                }
                else if (General.nz(dr["RSTE_SansNoCommande"], 0).ToString() == "1")
                {
                    //=== facture fourniseur sans bon de commande.
                    //=== on recherche le code analytique service dans la fiche fournisseur, si
                    //=== inexistant on affecte par défaut la valeur ADM (service administratif).
                    if (General.IsDate(dr["RSTE_DateFacture"].ToString()) && Convert.ToDateTime(dr["RSTE_DateFacture"]) >= General.dtServiceAnalytique)
                    {
                        if (string.IsNullOrEmpty(General.nz(dr["RSTE_ServiceAnalytique"], "").ToString()))
                        {
                            sSQL = "SELECT  ServiceAnalytique " + " From fournisseurArticle" + " WHERE   Cleauto =" + General.nz(dr["RSTE_CodeFournCleAuto"], 0);

                            rsServ = AdoLib.fc_OpenRecordSet(sSQL);

                            if (rsServ.Rows.Count > 0)
                            {
                                if (string.IsNullOrEmpty(General.nz(rsServ.Rows[0]["ServiceAnalytique"], "").ToString()))
                                {
                                    dr["RSTE_ServiceAnalytique"] = cAnalyServiceADM;

                                }
                                else
                                {
                                    dr["RSTE_ServiceAnalytique"] = rsServ.Rows[0]["ServiceAnalytique"];
                                }
                                ModAdoRSTE.Update();
                            }
                            else
                            {
                                dr["RSTE_ServiceAnalytique"] = cAnalyServiceADM;
                                ModAdoRSTE.Update();
                            }
                            AdoLib.Dispose();
                            rsServ.Clear();
                            rsServ = null;
                        }
                    }
                }

            }



            //========== contrôle si le montant de la facture éxiste déjà pour ce fournisseur.

            //                sSQL = "SELECT     RSTE_NoAuto,RSTE_NoFacture, RSTE_HT, NoBonDeCommande " _
            //'                    & " From RSTE_ReadSoftEnTete" _
            //'                    & " WHERE RSTE_CodeFourn ='" & rsRSTE!RSTE_CodeFourn & "'" _
            //'                    & " AND RSTE_NoAuto <> " & nz(rsRSTE!RSTE_NoAuto, 0) _
            //'                    & " AND RSTE_HT =" & nz(rsRSTE!RSTE_HT, 0) & ""
            //
            //
            //                sSQL = fc_ADOlibelle(sSQL)
            //
            //                If sSQL <> "" Then
            //                             txtAnomalies = txtAnomalies & sTexte & "Ligne n° " & rsRSTE!RSTE_NoLigne & " => " _
            //'                                & "La montant de la facture " & rsRSTE!RSTE_NoFacture _
            //'                                & " existe déjà pour ce fournisseur." & vbCrLf
            //
            //                                If bDroitCorrige = False Then
            //                                     fc_ctrlInvoice = 2
            //                                     rsRSTE!RSTE_Anomalie = 1
            //                                Else
            //                                     rsRSTE!RSTE_Anomalie = 7
            //                                End If
            //                                rsRSTE.Update
            //                End If


            //=== modif du 21 janvier 2013, supprsesion de ce message d'erreur.
            //=========== Contrôle si ce bon de commande a déjà été facturé.
            //            If nz(rsRSTE!RSTE_SansNoCommande, 0) <> 1 Then
            //                If nz(rsRSTE!NoBonDeCommande, "") <> "" Then
            //                        sSQL = "SELECT NoBonDeCommande FROM RSTE_ReadSoftEnTete " _
            //'                            & " WHERE NoBonDeCommande =" & rsRSTE!NoBonDeCommande _
            //'                            & " AND RSTE_NoAuto <> " & nz(rsRSTE!RSTE_NoAuto, 0)
            //                        sSQL = fc_ADOlibelle(sSQL)
            //                        If sSQL <> "" Then
            //                             txtAnomalies = txtAnomalies & sTexte & "Ligne n° " & rsRSTE!RSTE_NoLigne & " => " _
            //'                                & "Le numéro de bon de commande " & rsRSTE!NoBonDeCommande _
            //'                                & " a déjà été facturé." & vbCrLf
            //
            //                                If bDroitCorrige = False Then
            //                                    fc_ctrlInvoice = 2
            //                                    rsRSTE!RSTE_Anomalie = 1
            //                                Else
            //                                     rsRSTE!RSTE_Anomalie = 7
            //                                End If
            //                                rsRSTE.Update
            //                        End If
            //                End If
            //            End If

            //=========== Controle si le code fournisseur de la facture est le meme que celui du bon de commande.
            //     If nz(rsRSTE!RSTE_SansNoCommande, 0) = 0 Then
            //            sSQL = "SELECT     CodeFourn From BonDeCommande WHERE NoBonDeCommande = " & nz(rsRSTE!NoBonDeCommande, 0)
            //            sResult = fc_ADOlibelle(sSQL)
            //
            //            If UCase(sResult) <> UCase(nz(rsRSTE!RSTE_CodeFournCleAuto, "")) Then
            //
            //                        txtAnomalies = txtAnomalies & "Ligne n° " & rsRSTE!RSTE_NoLigne & " => " & "La facture numéro " & rsRSTE!RSTE_NoFacture & " a un n° de fiche fournisseur (" & rsRSTE!RSTE_CodeFournCleAuto _
            //'                        & ") diffèrent  de celui du bon de commande (" & sResult & ")." _
            //'                             & vbCrLf
            //
            //                        'fc_ctrlInvoice = 2
            //                        'rsRSTE!RSTE_Anomalie = 2
            //                        rsRSTE!STF_Code = cStatutV
            //                        rsRSTE.Update
            //            End If
            //    End If
            //

            //=== metttre cette fonction toujours à la fin des controles.
            fc_MajStatut(dr);
            AdoLib.Dispose();
            return functionReturnValue;



            //==== évite d'afficher un message d'erreur plus de 3 fois.
            //Erreurs.gFr_debug(this.Name + ";fc_ctrlInvoice");
            //return functionReturnValue;

        }

        private string fc_CtrCt_Num(string sCpteFournisseur)
        {
            string functionReturnValue = null;

            General.sSQL = "SELECT CT_Num From F_COMPTET " + " WHERE CT_Num='"
            + sCpteFournisseur + "'";

            SAGE.fc_OpenConnSage();
            using (var adoLib = new ModAdo())
                functionReturnValue = adoLib.fc_ADOlibelle(General.sSQL, false, SAGE.adoSage);

            SAGE.fc_CloseConnSage();
            return functionReturnValue;

        }

        private void fc_SaveEntete(ref DataTable rsRSTE)
        {
            //Dim sSQL        As String
            //Dim rsFacE      As ADODB.Recordset
            //'Dim RSTE        As ADODB.Recordset
            //
            //'sSQL = "SELECT RSTE_NoAuto, RSTE_dateCreat, RSTE_CreePar, RSTE_DateFacture, " _
            //'    & " RSTE_NoFacture, NoBonDeCommande, CodeImmeuble, RSTE_CodeFournCleAuto, " _
            //'    & " RSTE_CodeFourn, RSTE_RaisonSocial, RSTE_CT_Num, RSTE_CptChargeDef," _
            //'    & " RSTE_CptTvaDef, RSTE_TTC, RSTE_HT, RSTE_TauxTVA, RSTE_MtTVA," _
            //'    & " RSTE_DateEcheance , RSTE_Integre, RSTE_ModeRgt" _
            //'    & " From RSTE_ReadSoftEnTete" _
            //'    & " WHERE     (RSTE_Integre IS NULL) OR" _
            //'    & " (RSTE_Integre = 0)"
            //
            //'Set RSTE = fc_OpenRecordSet(sSQL)
            //
            //
            //sSQL = "SELECT NoAutoFacture, CodeFournisseur, RefFacture, Montant," _
            //'    & " ModeRgt, CG_NUM, TaxeCG_NUM, CT_NUM, DateLivraison, Commentaire, DateSaisie, " _
            //'    & " FactureCreat , DateModif, FactureModif, Verrou, EC_NoFacture, DateEcheance, AjusterMtTVA" _
            //'    & " , ReadSoft " _
            //'    & " From FactFournEntete" _
            //'    & " WHERE     (NoAutoFacture = 0)"
            //
            //Set rsFacE = fc_OpenRecordSet(sSQL)
            //
            //
            //Do While RSTE.EOF = False
            //
            //    rsFacE!CodeFournisseur = rsRSTE!RSTE_CodeFournCleAuto
            //    rsFacE!RefFacture = rsRSTE!RSTE_NoFacture
            //    '== champs suivant n est pas uililisé????
            //    'rsFacE!Montant = RSTE!
            //    rsFacE!ModeRgt = rsRSTE!RSTE_ModeRgt
            //    rsFacE!CG_NUM = rsRSTE!RSTE_CptChargeDef
            //    rsFacE!TaxeCG_NUM = rsRSTE!RSTE_CptTvaDef
            //    rsFacE!CT_NUM = rsRSTE!RSTE_CT_Num
            //    '== champs à renseigner.
            //    'rsFacE!DateLivraison = RSTE!
            //    '== champs suivant n est pas uililisé????
            //    'rsFacE!Commentaire = RSTE!
            //    rsFacE!DateSaisie = rsRSTE!RSTE_dateCreat
            //    rsFacE!FactureCreat = cReadSoft
            //    rsFacE!DateModif = Now
            //    rsFacE!FactureModif = rsRSTE!RSTE_CreePar
            //    rsFacE!Verrou = Null
            //    rsFacE!DateEcheance = rsRSTE!RSTE_DateEcheance
            //    rsFacE!AjusterMtTVA = 0
            //    rsFacE!ReadSoft = 1
            //
            //    RSTE.MoveNext
            //Loop
            //
            //RSTE.Close
            //rsFacE.Close
            //
            //Set RSTE = Nothing
            //Set rsFacE = Nothing

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bSendMail"></param>
        /// <returns></returns>
        private bool fc_CtrlFactFourn(bool bSendMail = false)
        {
            bool functionReturnValue = false;
            //==== controle les factures fournisseurs à intégrer, retourne false
            //==== en cas d'anomalies constatées et true dans le cas contraire.

            string sSQL = null;
            int lTotErr = 0;
            double dtMontant = 0;
            bool bAlerte = false;
            int lMaj;

            AdoLib = new ModAdo();

            try
            {
                //=== modif du 22 07 2019 applique une valeur par d�faut pour le champs analytique.

                sSQL = "UPDATE    RSTE_ReadSoftEnTete Set RSTE_Analytique = 'TSP', MajAuto = 1 "

                     + " WHERE     (RSTE_Comptabilise IS NULL OR RSTE_Comptabilise = 0) "

                    + " AND (RSTE_Analytique IS NULL OR RSTE_Analytique = '')";


                lMaj = General.Execute(sSQL);

                if (bSendMail == true)
                {
                    //=== contrôle si l'alerte sur le montant HT est activée.
                    if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                        sSQL = "SELECT adresse FROM lienV2 WHERE Code ='" + Variable.FFAlerteSurHt + "'";
                    else
                        sSQL = "SELECT adresse FROM lien WHERE Code ='" + Variable.FFAlerteSurHt + "'";
                    sSQL = AdoLib.fc_ADOlibelle(sSQL);
                    if (sSQL == "1")
                    {
                        bAlerte = true;
                        if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                            sSQL = "SELECT adresse FROM lienV2 WHERE Code = '" + Variable.FFAlerteMt + "'";
                        else
                            sSQL = "SELECT adresse FROM lien WHERE Code = '" + Variable.FFAlerteMt + "'";
                        //=== recherche le montant de seuil minimum
                        sSQL = General.nz(AdoLib.fc_ADOlibelle(sSQL), "0").ToString();
                        if (General.IsNumeric(sSQL))
                        {
                            dtMontant = Convert.ToDouble(sSQL);
                        }

                    }
                    else
                    {
                        bAlerte = false;
                    }
                }

                functionReturnValue = true;


                Cursor.Current = Cursors.WaitCursor;

                sSQL = "SELECT  RSTE_NoAuto ,RSTE_Selectionne,  RSTE_NoLigne ,  RSTE_dateCreat, RSTE_CreePar, RSTE_NoFacture,"
                    + " NoBonDeCommande, '' as HtBcd,CodeImmeuble, '' as HtCumul, RSTE_HT, RSTE_TauxTVA, RSTE_MtTVA, "
                    + " RSTE_TTC, RSTE_DateFacture, RSTE_DateEcheance, RSTE_CodeFournCleAuto,"
                    + " RSTE_CodeFourn, RSTE_RaisonSocial,RSTE_ModeRgt, RSTE_CT_Num,"
                    + " RSTE_CptChargeDef , RSTE_CptTvaDef , "
                    + " RSTE_AutoLiquidationRD, RSTE_CptAutoLiquidation, RSTE_TvaAutoLiquidation, "
                    + " RSTE_Comptabilise, " + " RSTE_DateComptabilise,RSTE_Anomalie, "
                    + " FactNoautofacture , RSTE_CheminFichierClasse, RSTE_SansNoCommande "
                    + ", RSTE_Analytique, STF_Code , RSTE_FournSst, RSTE_ServiceAnalytique "
                    + " FROM RSTE_ReadSoftEnTete";

                if (bSendMail == false)
                {
                    sSQL = sSQL + " WHERE     (RSTE_Comptabilise IS NULL) OR" + " (RSTE_Comptabilise = 0)";
                }
                else
                {
                    sSQL = sSQL + " WHERE   STF_Code  ='" + Variable.cStatutNV + "'";

                }

                if (txtRSTE_DateFactureDe.Text != "")
                {
                    sSQL = sSQL + " AND RSTE_DateFacture >='" + (txtRSTE_DateFactureDe.Text + " 00:00:00").Trim() + "'";
                }

                if (txtRSTE_DateFactureAu.Text != "")
                {
                    sSQL = sSQL + " AND RSTE_DateFacture <='" + (txtRSTE_DateFactureAu.Text + " 23:59:59").Trim() + "'";
                }

                sSQL = sSQL + sOrderBy;

                rsRSTE = ModAdoRSTE.fc_OpenRecordSet(sSQL);

                lTotErr = 0;
                if (rsRSTE.Rows.Count > 0)
                {

                    lNoLigne = 1;

                    Cursor.Current = Cursors.WaitCursor;

                    foreach (DataRow R in rsRSTE.Rows)
                    {
                        Cursor.Current = Cursors.WaitCursor;

                        //=== Met a jour le numéro de ligne.
                        R["RSTE_NoLigne"] = lNoLigne;
                        lNoLigne = lNoLigne + 1;

                        //=== Met a 0 le champs anomalie.
                        R["RSTE_Anomalie"] = 0;
                        R["STF_Code"] = Variable.cStatutNV;

                        //=== envoie un mail si le montant de la facture est superieur
                        //=== à un plafond parametré.
                        if (bSendMail == true)
                        {
                            if (General.nz(R["RSTE_Selectionne"], 0).ToString() == "0")
                            {
                                //=== facture non sélectionné, pas d'action.
                            }
                            else if ((Convert.ToDouble(R["RSTE_HT"].ToString()) >= dtMontant && General.nz(R["NoBonDeCommande"], 0).ToString() != "0")
                                || (General.nz(R["RSTE_FournSst"], 0).ToString() == "0" && General.nz(R["NoBonDeCommande"], 0).ToString() != "0"))
                            {

                                R["STF_Code"] = Variable.cStatutAV;
                                if (bAlerte == true)
                                {
                                    fc_sendMail(Convert.ToInt32(R["NoBonDeCommande"].ToString()), General.nz(R["RSTE_CheminFichierClasse"], "").ToString(), Convert.ToInt32(General.nz(R["RSTE_FournSst"], 0).ToString()), dtMontant);
                                }
                                //fc_MajStatut rsRSTE

                            }
                            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                            else if (General.adocnn.Database.ToUpper() == General.cNameLong.ToUpper() && General.Left(R["RSTE_CptChargeDef"]?.ToString(), 3) == "604")
                            {
                                R["STF_Code"] = Variable.cStatutAV;
                                //rsRSTE!STF_Code = cStatutNV
                            }
                            //===> Fin Modif Mondir
                            else
                            {
                                R["STF_Code"] = Variable.cStatutV;
                                //fc_MajStatut rsRSTE
                            }
                        }
                        if (!string.IsNullOrEmpty(R["NoBonDeCommande"].ToString()))
                        {
                            //=== cumul fcature par bon de commande.
                            sSQL1 = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete"
                            + " WHERE     NoBonDeCommande = " + General.nz(R["NoBonDeCommande"], 0);

                            ////=== total ht du bon de commande.
                            sSQL2 = "SELECT   sum(  BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail "
                           + " WHERE   BCD_Cle = "
                           + General.nz(R["NoBonDeCommande"], 0);

                            //=== cumul fcature par bon de commande.
                            sSQL1 = AdoLib.fc_ADOlibelle(sSQL1);
                            R["HtCumul"] = General.nz(sSQL1, 0);

                            ////=== total ht du bon de commande.
                            sSQL2 = AdoLib.fc_ADOlibelle(sSQL2);
                            R["HtBcd"] = General.nz(sSQL2, 0);


                        }

                        //fc_MajStatut rsRSTE

                        ModAdoRSTE.Update();
                    }

                    if (fc_CtrlAllOrOneInv(true, lTotErr) == 2)
                    {
                        functionReturnValue = false;
                    }

                }
                else
                {
                    txtAnomalies.Text = "";
                }

                // ssRSTE.ReBind();

                ssRSTE.DataSource = rsRSTE;
                ssRSTE.UpdateData();

                lblFact.Text = Convert.ToString(rsRSTE.Rows.Count);
                lbltotFactErreur.Text = Convert.ToString(lTotErr);
                Cursor.Current = Cursors.Arrow;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

            return functionReturnValue;
            // erreur:

            //Erreurs.gFr_debug(this.Name + ";cmdVisuFactures_Click");
            //return functionReturnValue;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rsRSTERow"></param>
        /// <returns></returns>
        private bool fc_MajStatut(DataRow rsRSTERow)
        {
            bool functionReturnValue = false;

            double dbHT = 0;
            double dbHtFourn = 0;
            double dbHtBCD = 0;
            string sSQL = null;
            string sResult = null;

            AdoLib = new ModAdo();
            try
            {
                //========= COntrole si le HT du bon de commande est different de la facture HT. les compte de charge qui commence par 611
                //========= correspondent à de la sous traitance.
                functionReturnValue = false;

                //===> Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
                //===> Ajout d'un condition sur le nom de la base LONG
                if (General.nz(rsRSTERow["RSTE_Anomalie"], 0).ToString() != "1")
                {
                    if (General.adocnn.Database.ToUpper() == General.cNameLong.ToUpper())
                    {
                        if (General.nz(rsRSTERow["RSTE_SansNoCommande"], 0).ToString() == "1")
                        {
                            rsRSTERow["STF_Code"] = Variable.cStatutV;
                            ModAdoRSTE.Update();
                            functionReturnValue = true;

                        }
                        else if (General.Left(General.nz(rsRSTERow["RSTE_CptChargeDef"], "").ToString(), 3) != "604")
                        {
                            dbHT = Convert.ToDouble(General.nz(rsRSTERow["RSTE_HT"], 0));

                            sSQL = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete" +
                                   " WHERE     NoBonDeCommande = " + General.nz(rsRSTERow["NoBonDeCommande"], 0);

                            dbHtFourn = Convert.ToDouble(General.nz(AdoLib.fc_ADOlibelle(sSQL), 0));

                            //=== total ht du bon de commande.
                            sSQL = "SELECT   sum(  BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail " +
                                   " WHERE   BCD_Cle = " + General.nz(rsRSTERow["NoBonDeCommande"], 0);

                            dbHtBCD = Convert.ToDouble(General.nz(AdoLib.fc_ADOlibelle(sSQL), 0));


                            if (Convert.ToDecimal(dbHtBCD) != Convert.ToDecimal(dbHT))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutAV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                            else if (Convert.ToDecimal(dbHT) != Convert.ToDecimal(dbHtFourn))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutAV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                            else if (Convert.ToDecimal(dbHT) == Convert.ToDecimal(dbHtFourn))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                        }
                        else if (General.Left(General.nz(rsRSTERow["RSTE_CptChargeDef"], "").ToString(), 3) == "604")
                        {
                            //=== sous traitance.
                            rsRSTERow["STF_Code"] = Variable.cStatutAV;
                            ModAdoRSTE.Update();
                            functionReturnValue = true;
                        }
                    }
                    else
                    {
                        if (General.nz(rsRSTERow["RSTE_SansNoCommande"], 0).ToString() == "1")
                        {
                            rsRSTERow["STF_Code"] = Variable.cStatutV;
                            ModAdoRSTE.Update();
                            functionReturnValue = true;

                        }
                        else if (General.Left(General.nz(rsRSTERow["RSTE_CptChargeDef"], "").ToString(), 3) != "611")
                        {
                            dbHT = Convert.ToDouble(General.nz(rsRSTERow["RSTE_HT"], 0));

                            sSQL = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete" +
                                   " WHERE     NoBonDeCommande = " + General.nz(rsRSTERow["NoBonDeCommande"], 0);

                            dbHtFourn = Convert.ToDouble(General.nz(AdoLib.fc_ADOlibelle(sSQL), 0));

                            //=== total ht du bon de commande.
                            sSQL = "SELECT   sum(  BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail " +
                                   " WHERE   BCD_Cle = " + General.nz(rsRSTERow["NoBonDeCommande"], 0);

                            dbHtBCD = Convert.ToDouble(General.nz(AdoLib.fc_ADOlibelle(sSQL), 0));


                            if (Convert.ToDecimal(dbHtBCD) != Convert.ToDecimal(dbHT))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutAV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                            else if (Convert.ToDecimal(dbHT) != Convert.ToDecimal(dbHtFourn))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutAV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                            else if (Convert.ToDecimal(dbHT) == Convert.ToDecimal(dbHtFourn))
                            {
                                rsRSTERow["STF_Code"] = Variable.cStatutV;
                                ModAdoRSTE.Update();
                                functionReturnValue = true;
                            }
                        }
                        else if (General.Left(General.nz(rsRSTERow["RSTE_CptChargeDef"], "").ToString(), 3) == "611")
                        {
                            //=== sous traitance.
                            rsRSTERow["STF_Code"] = Variable.cStatutAV;
                            ModAdoRSTE.Update();
                            functionReturnValue = true;
                        }
                    }

                    //=========== Controle si le code fournisseur de la facture est le meme que celui du bon de commande.
                    if (General.nz(rsRSTERow["RSTE_SansNoCommande"], 0).ToString() == "0")
                    {
                        sSQL = "SELECT     CodeFourn From BonDeCommande WHERE NoBonDeCommande = " +
                               General.nz(rsRSTERow["NoBonDeCommande"], 0);
                        sResult = AdoLib.fc_ADOlibelle(sSQL);

                        if (General.UCase(sResult) !=
                            General.UCase(General.nz(rsRSTERow["RSTE_CodeFournCleAuto"], "").ToString()))
                        {

                            txtAnomalies.Text = txtAnomalies.Text + "Ligne n° " + rsRSTERow["RSTE_NoLigne"] +
                                                " => " + "La facture numéro " + rsRSTERow["RSTE_NoFacture"] +
                                                " a un n° de fiche fournisseur (" +
                                                rsRSTERow["RSTE_CodeFournCleAuto"] +
                                                ") diffèrent  de celui du bon de commande (" + sResult + ")." +
                                                "\r\n";

                            //fc_ctrlInvoice = 2
                            //rsRSTE!RSTE_Anomalie = 2
                            rsRSTERow["STF_Code"] = Variable.cStatutAV;
                            ModAdoRSTE.Update();
                        }
                    }


                }
                AdoLib.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_MajStatut");
                return functionReturnValue;
            }

        }

        private void fc_sendMail(int lNoBon, string sCheminFichier, int lfournSt, double dbmontant = 0)
        {

            string sMessage = null;
            string sDestMail = null;
            string sSujet = null;
            AdoLib = new ModAdo();
            //=== recherche le nom de l'acheteur.
            sDestMail = AdoLib.fc_ADOlibelle("SELECT     Acheteur" + " From BonDeCommande" + " WHERE    NoBonDeCommande = " + lNoBon);

            if (string.IsNullOrEmpty(sDestMail))
            {
                sDestMail = "FL@delostaletthibault.Fr";

            }
            else
            {
                sDestMail = sDestMail + "@delostaletthibault.fr";
            }

            sMessage = "Bonjour";

            //=== fournisseur.
            if (lfournSt == 1)
            {

                if (dbmontant > 0)
                {
                    sMessage = sMessage + "\r\n" + "\r\n"
                        + "vous trouverez ci-joint une facture fournisseur "
                        + " dont le montant est supérieur à " + dbmontant + " euros.";
                }
                else
                {

                    sMessage = sMessage + "\r\n" + "\r\n"
                        + "vous trouverez ci-joint une facture fournisseur "
                        + " dont le montant est supérieur à 1000 euros.";
                }

                sSujet = "Facture fournisseur";
                // === sous traitant.
            }
            else
            {

                sMessage = sMessage + "\r\n" + "\r\n" + "vous trouverez ci-joint une facture d'un sous-traitant ";
                sSujet = "facture sous-traitant";
            }

            sMessage = sMessage + "\r\n" + "Cette facture doit être validée par vos soins.";
            sMessage = sMessage + "\r\n" + "Pour valider cette facture, procéder de la manière suivante :";
            sMessage = sMessage + "\r\n" + "\r\n" + "     - rendez vous sur l'intranet," + " cliquez sur 'historiques des factures fournisseurs' " + " présent dans le menu 'FOUNISSEURS'";
            sMessage = sMessage + "\r\n" + "     - rechercher votre facture avec " + "les critères mis à votre disposition";
            sMessage = sMessage + "\r\n" + "     - sélectionner votre facture dans la grille" + " en cliquant sur la ligne correspondante à votre facture.";
            sMessage = sMessage + "\r\n" + "     - faites un clic droit sur la grille, " + " un menu apparaît, dans ce menu cliquez sur 'contrôler'" + " un formulaire apparaît.";
            sMessage = sMessage + "\r\n" + "     - dans  ce formulaire faites votre choix parmi les options proposées";

            sMessage = sMessage + "\r\n" + "     - cliquez sur 'OK'";

            sMessage = sMessage + "\r\n" + "\r\n" + "Cordialement";
            sMessage = sMessage + "\r\n" + "\r\n" + "SERVICE COMPTABILITE";

            ModCourrier.FichierAttache = sCheminFichier;

            Cursor.Current = Cursors.WaitCursor;

            //CreateMail "rachid_abbouchi@hotmail.com", "raschid_abb@yahoo.fr;", _
            //"Facture Fournisseur", sMessage, FichierAttache, vbTab

            //CreateMail "sif@delostaletthibault.Fr", "sif@delostaletthibault.Fr;", _
            //"Facture Fournisseur", sMessage, FichierAttache, vbTab

            //=== envoie de email.


            if (General.UCase(General.adocnn.Database) == General.UCase(Variable.cDelostal))
            {
                ModCourrier.CreateMail(sDestMail, "LF@delostaletthibault.Fr;", sSujet, sMessage, ModCourrier.FichierAttache, "\t");
            }
            else if (General.UCase(General.adocnn.Database) == General.UCase(Variable.cAmmann))
            {
                ModCourrier.CreateMail(sDestMail, "lmg@delostaletthibault.Fr;DIB@delostaletthibault.Fr;t.gresle@maison-ammann.fr;", sSujet, sMessage, ModCourrier.FichierAttache, "\t");
            }
            AdoLib.Dispose();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadCpteCharges()
        {
            ModAdo adotempAdo = new ModAdo();
            DataTable adotemp = default(DataTable);
            string sSQL = null;

            SAGE.fc_OpenConnSage();

            adotemp = new DataTable();

            //sSQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable]," _
            //& " F_COMPTEG.CG_INTITULE AS [Intitulé]," _
            //& " F_TAXE.CG_NUM, F_TAXE.TA_TAUX " _
            //& " FROM F_COMPTEG INNER JOIN " _
            //& " (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) " _
            //& " ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
            //& " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)=8 ) " _
            //& "  ORDER BY F_COMPTEG.CG_NUM"

            sSQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable],"
                + " F_COMPTEG.CG_INTITULE AS [Intitulé],"
                + " F_TAXE.CG_NUM, F_TAXE.TA_TAUX "
                + " FROM F_COMPTEG INNER JOIN "
                + " (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) "
                + " ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                + " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)>= 3 ) "
                + "  ORDER BY F_COMPTEG.CG_NUM";


            adotemp = adotempAdo.fc_OpenRecordSet(sSQL, null, "", SAGE.adoSage);

            DataTable DtCmbCptChargesBis = new DataTable();
            DtCmbCptChargesBis.Columns.Add("Compte de charge");
            DtCmbCptChargesBis.Columns.Add("Intitulé");
            DtCmbCptChargesBis.Columns.Add("Compte de Taxe");
            DtCmbCptChargesBis.Columns.Add("Taux de TVA");

            if (adotemp.Rows.Count > 0)
            {
                cmbCptChargesBis.DataSource = null;
                foreach (DataRow DR in adotemp.Rows)
                {
                    DataRow dr = DtCmbCptChargesBis.NewRow();
                    dr["Compte de charge"] = DR["Code comptable"];
                    dr["Intitulé"] = DR["Intitulé"];
                    dr["Compte de Taxe"] = DR["CG_NUM"];
                    dr["Taux de TVA"] = DR["TA_Taux"];
                    DtCmbCptChargesBis.Rows.Add(dr.ItemArray);

                }
            }
            cmbCptChargesBis.DataSource = DtCmbCptChargesBis;
            //cmbCptChargesBis.DataFieldList = "Column 0";
            //ssRSTE.Columns["RSTE_CptChargeDef"].DropDownHwnd = cmbCptChargesBis.hWnd;
            cmbCptChargesBis.ValueMember = "Compte de charge";

            adotemp = null;
            adotempAdo.Dispose();
            SAGE.fc_CloseConnSage();

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadCpteTvaAutoLiquidation()
        {
            ModAdo adotempAdo = new ModAdo();
            DataTable adotemp = default(DataTable);
            string sSQL = null;

            SAGE.fc_OpenConnSage();

            adotemp = new DataTable();

            sSQL = "SELECT DISTINCT CG_Num, TA_Intitule, TA_Taux" + " From F_TAXE " + " ORDER BY CG_Num";

            adotemp = adotempAdo.fc_OpenRecordSet(sSQL, null, "", SAGE.adoSage);

            DataTable DtDropTVAautol = new DataTable();
            DtDropTVAautol.Columns.Add("Compte de taxe");
            DtDropTVAautol.Columns.Add("Intitule");
            DtDropTVAautol.Columns.Add("Compte de TVA");

            if (adotemp.Rows.Count > 0)
            {
                DropTVAautol.DataSource = null;
                foreach (DataRow drr in adotemp.Rows)
                {
                    DataRow dr = DtDropTVAautol.NewRow();
                    dr["Compte de taxe"] = drr["CG_NUM"];
                    dr["Intitule"] = drr["TA_Intitule"];
                    dr["Compte de TVA"] = drr["TA_Taux"];
                    DtDropTVAautol.Rows.Add(drr.ItemArray);

                }

            }
            DropTVAautol.DataSource = DtDropTVAautol;
            // DropTVAautol.DataFieldList = "Column 0";
            //ssRSTE.Columns["RSTE_CptAutoLiquidation"].DropDownHwnd = DropTVAautol.hWnd;
            DropTVAautol.ValueMember = "Compte de taxe";

            adotemp = null;
            adotempAdo.Dispose();
            SAGE.fc_CloseConnSage();

        }


        private void cmdSelect_Click(object sender, System.EventArgs eventArgs)
        {

            foreach (DataRow row in rsRSTE.Rows)
            {
                row["RSTE_Selectionne"] = 1;
            }
            ModAdoRSTE.Update();
            ssRSTE.DataSource = rsRSTE;
            ssRSTE.UpdateData();
        }
        //Tested
        private void Command1_Click(object sender, System.EventArgs eventArgs)
        {
            frmParamFactFourn FPF = new frmParamFactFourn();
            FPF.ShowDialog();
        }


        private void DropImmeuble_CloseUp(object sender, System.EventArgs eventArgs)
        {

            //var _with12 = ssRSTE;
            //_with12.Columns("CodeImmeuble").value = DropImmeuble.Columns["CodeImmeuble"].Value;
            ssRSTE.DisplayLayout.Bands[0].Columns["CodeImmeuble"].ValueList = DropImmeuble.DisplayLayout.Bands[0].Columns["CodeImmeuble"].ValueList;

        }

        private void UserDocSageScan_VisibleChanged(object sender, EventArgs e)
        {


            if (!Visible)
                return;

            //    lblNavigation(0).Caption = sNomLien0
            //    lblNavigation(1).Caption = sNomLien1
            //    lblNavigation(2).Caption = sNomLien2


            //=== Ajout de la section ananlytique service.
            General.sServiceAnalytique = General.gfr_liaison("ServiceAnalytique");
            if (General.sServiceAnalytique == "1")
            {
                General.dtServiceAnalytique = Convert.ToDateTime(General.gfr_liaison("DateServiceAnalytique"));
            }


            fc_CtrlFactFourn();
            fc_DropImmeuble();
            fc_DropFourn();
            ModAutorisation.fc_DroitParFiche(Variable.cUserDocSageScan);


        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropAnaly()
        {
            string sSQL = null;

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                SAGE.fc_OpenConnSage();

                sSQL = "SELECT distinct F_COMPTEA.CA_NUM From F_COMPTEA WHERE F_COMPTEA.N_ANALYTIQUE=1";

                sheridan.InitialiseCombo(DropAnalytique, sSQL, "CA_NUM", true, SAGE.adoSage);

                sSQL = "SELECT distinct F_COMPTEA.CA_NUM From F_COMPTEA WHERE F_COMPTEA.N_ANALYTIQUE=6";

                sheridan.InitialiseCombo(DropAnalyService, sSQL, "CA_NUM", true, SAGE.adoSage);

                SAGE.fc_CloseConnSage();
                //InitialiseCombo Me.DropFourn, adoDropFourn, _
                //sSQL , "Code", True


            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropFourn()
        {
            string sSQL = null;

            sSQL = "SELECT     Code, Cleauto, NCompte, Raisonsocial, ModeRgt" + " FROM         fournisseurArticle"
                + " WHERE     (fournisseurArticle.NePasAfficher IS NULL) OR" + " (fournisseurArticle.NePasAfficher = 0)" + " ORDER BY fournisseurArticle.Code";


            sheridan.InitialiseCombo(DropFourn, sSQL, "Code", true);
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CodeFourn"].ValueList = DropFourn;
            //this.ssRSTE.Columns["RSTE_CodeFourn"].DropDownHwnd = this.DropFourn.hWnd;

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropImmeuble()
        {
            string sSQL = null;

            sSQL = "SELECT CodeImmeuble, adresse, ville" + " FROM IMMEUBLE order by CodeImmeuble ";


            sheridan.InitialiseCombo(DropImmeuble, sSQL, "CodeImmeuble", true);
            ssRSTE.DisplayLayout.Bands[0].Columns["CodeImmeuble"].ValueList = DropImmeuble;
            // this.ssRSTE.Columns["CodeImmeuble"].DropDownHwnd = this.DropImmeuble.hWnd;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="eventArgs"></param>
        private void UserDocSageScan_Load(object sender, System.EventArgs eventArgs)
        {

            fc_LoadCpteCharges();
            fc_LoadCpteTvaAutoLiquidation();
            fc_DropAnaly();
            //    ssRSTE.StyleSets["ERREUR"].BackColor = 0x8080ff;
            //    // rouge.
            //    ssRSTE.StyleSets["ERREUR"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
            //    ssRSTE.StyleSets["AVOIR"].BackColor = 8454016;
            //    // vert
            //    ssRSTE.StyleSets["AVOIR"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);
            //    ssRSTE.StyleSets["OBS"].BackColor = 0xffc0ff;
            //    // vert
            //    ssRSTE.StyleSets["OBS"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //    ssRSTE.StyleSets["AUTO"].BackColor = System.Drawing.ColorTranslator.ToOle(lblAutoLiquidation.BackColor);
            //    ssRSTE.StyleSets["AUTO"].ForeColor = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.Black);

            //    //ssDEP_DescriptifChaufferie.StyleSets("P3").BackColor = &HC0C0C0
            //    //ssDEP_DescriptifChaufferie.StyleSets("P3").ForeColor = vbBlue

        }


        private void ssRSTE_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (ssRSTE.ActiveRow != null && General.nz((ssRSTE.ActiveRow.Cells["RSTE_Comptabilise"].Value), "0").ToString() == "1")
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cette facture est déjà comptabilisée, vous ne pouvez pas la supprimer.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            //===> Mondir le 09.11.2020, erreur constaté, si Cancel, il faut annuler ==> Changé == DialogResult.No à != DialogResultat.Yes
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNoCancel) != DialogResult.Yes)
            {
                e.Cancel = true;
            }

        }

        private void ssRSTE_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            ///' fc_CtrlAllOrOneInv False
            ///                   
        }
        private void ssRSTE_DoubleClick(object sender, EventArgs e)
        {
            if (ssRSTE.ActiveRow != null && !string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Text))
            {

                ModuleAPI.Ouvrir(ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Text);

            }
        }

        private void ssRSTE_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {

            string sSQL = null;
            int i = 0;
            AdoLib = new ModAdo();
            try
            {
                //if (e.ReInitialize) return;
                //ssRSTE.UpdateData();
                var _with15 = ssRSTE;
                //== applique une coubleur si une anomalie est déceleé.

                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    if (General.UCase(e.Row.Cells["RSTE_Comptabilise"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_DateComptabilise"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_dateCreat"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_CreePar"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_CptTvaDef"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_CheminFichierClasse"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption)
                        || General.UCase(e.Row.Cells["RSTE_NoLigne"].Column.Header.Caption) == General.UCase(e.Row.Cells[i].Column.Header.Caption))
                    {

                        e.Row.Cells[i].Column.CellActivation = Activation.NoEdit;
                        e.Row.Cells[i].Appearance.BackColor = Color.Silver;
                        //== gris
                        e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                    }
                    if (e.Row.Cells["RSTE_Anomalie"].Text == "1")
                    {
                        //e.Row.Cells[i].CellStyleSet("ERREUR", _with15.Row);
                        e.Row.Cells[i].Appearance.BackColor = Color.LightCoral;
                    }
                    else if (e.Row.Cells["RSTE_TTC"].Text != "" && Convert.ToDouble(e.Row.Cells["RSTE_TTC"].Text) < 0)
                    {
                        // e.Row.Cells[i].CellStyleSet("AVOIR", _with15.Row);
                        e.Row.Cells[i].Appearance.BackColor = Color.PaleGreen;

                    }
                    else if (e.Row.Cells["RSTE_Anomalie"].Text == "7")
                    {
                        //_with15.Columns(i).CellStyleSet("OBS", _with15.Row);
                        e.Row.Cells[i].Appearance.BackColor = Color.Pink;
                    }

                    if (!string.IsNullOrEmpty(e.Row.Cells[25].Text))
                    {
                        e.Row.Cells["RSTE_NoLigne"].Appearance.BackColor = Color.Fuchsia;
                        e.Row.Cells["RSTE_CptChargeDef"].Appearance.BackColor = Color.Fuchsia;
                        // .Columns("RSTE_EC_NoCptTva").CellStyleSet "AUTO", .Row
                        e.Row.Cells[25].Appearance.BackColor = Color.Fuchsia;
                        e.Row.Cells["RSTE_TTC"].Appearance.BackColor = Color.Fuchsia;
                        e.Row.Cells["RSTE_HT"].Appearance.BackColor = Color.Fuchsia;

                        // .Columns("RSTE_NoLigne").CellStyleSet "AUTO", .Row
                        // .Columns("RSTE_NoLigne").CellStyleSet "AUTO", .Row

                    }
                }


                AdoLib.Dispose();

                return;
            }
            catch (Exception E)
            {
                Erreurs.gFr_debug(E, "ssRSTE_RowLoaded");
            }




        }

        public double FncArrondirFact(object n, int p = 2)
        {
            return Math.Round(Convert.ToDouble(n), p);

        }

        private void ssRSTE_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssRSTE.DisplayLayout.Bands[0].Columns[0].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[1].Header.Caption = "Selectionné";
            ssRSTE.DisplayLayout.Bands[0].Columns[1].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssRSTE.DisplayLayout.Bands[0].Columns[1].Editor.DataFilter = new BooleanColumnDataFilter();

            ssRSTE.DisplayLayout.Bands[0].Columns[2].Header.Caption = "Noligne";
            ssRSTE.DisplayLayout.Bands[0].Columns[3].Header.Caption = "Date de création";
            ssRSTE.DisplayLayout.Bands[0].Columns[4].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[5].Header.Caption = "N°Facture";
            ssRSTE.DisplayLayout.Bands[0].Columns[6].Header.Caption = "N°Bon De Commande";
            ssRSTE.DisplayLayout.Bands[0].Columns[7].Header.Caption = "HT Bon de commande";
            ssRSTE.DisplayLayout.Bands[0].Columns[8].Header.Caption = "Code immeuble";
            ssRSTE.DisplayLayout.Bands[0].Columns[9].Header.Caption = "Comul Factur";
            ssRSTE.DisplayLayout.Bands[0].Columns[10].Header.Caption = "HT";
            ssRSTE.DisplayLayout.Bands[0].Columns[11].Header.Caption = "Taux TVA";
            ssRSTE.DisplayLayout.Bands[0].Columns[12].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[13].Header.Caption = "TTC";
            ssRSTE.DisplayLayout.Bands[0].Columns[14].Header.Caption = "Date Facture";
            ssRSTE.DisplayLayout.Bands[0].Columns[15].Header.Caption = "Date Echeance";
            ssRSTE.DisplayLayout.Bands[0].Columns[16].Header.Caption = "N° Fiche Fourn";
            ssRSTE.DisplayLayout.Bands[0].Columns[17].Header.Caption = "Code Fourn";
            ssRSTE.DisplayLayout.Bands[0].Columns[18].Header.Caption = "Raison social";
            ssRSTE.DisplayLayout.Bands[0].Columns[19].Header.Caption = "Mode de Rgt";
            ssRSTE.DisplayLayout.Bands[0].Columns[20].Header.Caption = "N° Cpte Fournisseur";
            ssRSTE.DisplayLayout.Bands[0].Columns[21].Header.Caption = "N° Cpte charge";
            ssRSTE.DisplayLayout.Bands[0].Columns[22].Header.Caption = "Cpte de TVA";
            ssRSTE.DisplayLayout.Bands[0].Columns[23].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[24].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[25].Header.Caption = "Cpte TVA auto liquidation";
            ssRSTE.DisplayLayout.Bands[0].Columns[26].Header.Caption = "Comptabilisée";
            ssRSTE.DisplayLayout.Bands[0].Columns[27].Header.Caption = "Date Comptabilisé";
            ssRSTE.DisplayLayout.Bands[0].Columns[28].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[29].Header.Caption = "N° Fiche facture";
            ssRSTE.DisplayLayout.Bands[0].Columns[30].Header.Caption = "chemain fichier";
            ssRSTE.DisplayLayout.Bands[0].Columns[31].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[32].Header.Caption = "Analytique";
            ssRSTE.DisplayLayout.Bands[0].Columns[33].Header.Caption = "STE_Code";
            ssRSTE.DisplayLayout.Bands[0].Columns[34].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns[35].Header.Caption = "Service(Analitique)";

            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CptChargeDef"].ValueList = cmbCptChargesBis;
            ssRSTE.DisplayLayout.Bands[0].Columns[25].ValueList = DropTVAautol;

            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_Analytique"].ValueList = DropAnalytique;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_ServiceAnalytique"].ValueList = DropAnalyService;

            //===> Mondir le 29.11.2020 pour ajouter les modifs de la version V29.11.2020
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_SansNoCommande"].Header.Caption = "0 = bcd obligatoire              1 =Bcd sans bon de cmd";
            //===> Fin Modif Mondir

        }


        private void lblGoHistorique_Click(object sender, LinkLabelLinkClickedEventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserHistoReadSoft));
        }

        private void cmdDeSelect_Click(object sender, EventArgs e)
        {
            foreach (DataRow row in rsRSTE.Rows)
            {
                row["RSTE_Selectionne"] = 0;
            }
            ModAdoRSTE.Update();
            ssRSTE.Refresh();
        }

        private void ssRSTE_AfterRowUpdate(object sender, RowEventArgs e)
        {
            ModAdoRSTE.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_CellChange(object sender, CellEventArgs e)
        {
            try
            {

                if (e.Cell.Column.Key.ToUpper() == "CodeImmeuble".ToUpper())//tested
                {
                    var row = DropImmeuble.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                    if (row != null)
                    {
                        ssRSTE.ActiveRow.Cells["CodeImmeuble"].Value = DropImmeuble.ActiveRow != null ? DropImmeuble.ActiveRow.Cells["CodeImmeuble"].Value : "";

                    }
                }
                if (e.Cell.Column.Key.ToUpper() == "RSTE_CptChargeDef".ToUpper())//Tested
                {
                    var row = cmbCptChargesBis.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                    if (row != null)
                    {
                        ssRSTE.ActiveRow.Cells["RSTE_CptChargeDef"].Value = cmbCptChargesBis.ActiveRow != null ? cmbCptChargesBis.ActiveRow.Cells["Compte de Charge"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_CptTvaDef"].Value = cmbCptChargesBis.ActiveRow != null ? cmbCptChargesBis.ActiveRow.Cells["Compte de taxe"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_TauxTVA"].Value = cmbCptChargesBis.ActiveRow != null ? cmbCptChargesBis.ActiveRow.Cells["Taux de TVA"].Value : "";
                    }
                }

                if (e.Cell.Column.Key.ToUpper() == "RSTE_CodeFourn".ToUpper())//tested
                {
                    var row = DropFourn.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                    if (row != null)
                    {
                        ssRSTE.ActiveRow.Cells["RSTE_CodeFourn"].Value = DropFourn.ActiveRow != null ? DropFourn.ActiveRow.Cells["Code"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_CodeFournCleAuto"].Value = DropFourn.ActiveRow != null ? DropFourn.ActiveRow.Cells["cleauto"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_CT_Num"].Value = DropFourn.ActiveRow != null ? DropFourn.ActiveRow.Cells["NCompte"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_RaisonSocial"].Value = DropFourn.ActiveRow != null ? DropFourn.ActiveRow.Cells["Raisonsocial"].Value : "";
                        ssRSTE.ActiveRow.Cells["RSTE_ModeRgt"].Value = DropFourn.ActiveRow != null ? DropFourn.ActiveRow.Cells["ModeRgt"].Value : "";

                    }
                }
                ssRSTE.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssRSTE_CellChange");
            }

        }



        private void ssRSTE_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            string sSQL = null;
            AdoLib = new ModAdo();
            var _with14 = ssRSTE;
            if (ssRSTE.ActiveCell == null) return;
            //===== Colonne NoBonDeCommande.
            if (ssRSTE.ActiveCell.Column.Index == ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Column.Index
                && ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text != ssRSTE.ActiveRow.Cells["NoBonDeCommande"].OriginalValue.ToString())
            {
                //==== recherche du bon de commande.
                sSQL = "SELECT     Codeimmeuble" + " From BonDeCommande" + " WHERE    NoBonDeCommande = " + General.nz(ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text, 0);
                sSQL = AdoLib.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous avez changé le numéro de bon de commande,"
                        + "\r\n" + " veuillez contrôler les éléments suivants : "
                        + "\r\n" + "  - Code immeuble " + "\r\n"
                        + "                - Code fournisseur " + "\r\n"
                        + "                - Compte Fournisseur", "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    //=== cumul fcature par bon de commande.
                    sSQL1 = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete"
                    + " WHERE     NoBonDeCommande = " + General.nz(ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text, 0);
                    ////=== total ht du bon de commande.
                    sSQL2 = "SELECT   sum(  BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail "
                   + " WHERE   BCD_Cle = "
                   + General.nz(ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text, 0);

                    //if (!string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text))
                    //{ 
                    //=== cumul fcature par bon de commande.
                    sSQL1 = AdoLib.fc_ADOlibelle(sSQL1);
                    ssRSTE.ActiveRow.Cells["HtCumul"].Value = General.nz(sSQL1, 0);

                    ////=== total ht du bon de commande.
                    sSQL2 = AdoLib.fc_ADOlibelle(sSQL2);
                    ssRSTE.ActiveRow.Cells["HtBcd"].Value = General.nz(sSQL2, 0);
                }
                else if (string.IsNullOrEmpty(sSQL))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce bon de commande n'existe pas.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;

                }

            }

            AdoLib.Dispose();
        }

        private void ssRSTE_AfterExitEditMode(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Mondir le 09.11.2020, ajouté par Mondir, Signalé via mail par Frédéric LEBORGNE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_AfterRowsDeleted(object sender, EventArgs e)
        {
            var xx = ModAdoRSTE.Update();
        }
    }
}

