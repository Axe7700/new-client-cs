﻿namespace Axe_interDT.Views.Fournisseur.IntegrationDesFactures.Forms
{
    partial class frmParamFactFourn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtFFAlerteMt = new iTalk.iTalk_TextBox_Small2();
            this.chkAlerteSurHt = new System.Windows.Forms.CheckBox();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.txtFFAlerteMt);
            this.groupBox6.Controls.Add(this.chkAlerteSurHt);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(12, 41);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(582, 84);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Alerte sur montant HT";
            // 
            // txtFFAlerteMt
            // 
            this.txtFFAlerteMt.AccAcceptNumbersOnly = false;
            this.txtFFAlerteMt.AccAllowComma = false;
            this.txtFFAlerteMt.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFFAlerteMt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFFAlerteMt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFFAlerteMt.AccHidenValue = "";
            this.txtFFAlerteMt.AccNotAllowedChars = null;
            this.txtFFAlerteMt.AccReadOnly = false;
            this.txtFFAlerteMt.AccReadOnlyAllowDelete = false;
            this.txtFFAlerteMt.AccRequired = false;
            this.txtFFAlerteMt.BackColor = System.Drawing.Color.White;
            this.txtFFAlerteMt.CustomBackColor = System.Drawing.Color.White;
            this.txtFFAlerteMt.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFFAlerteMt.ForeColor = System.Drawing.Color.Black;
            this.txtFFAlerteMt.Location = new System.Drawing.Point(458, 38);
            this.txtFFAlerteMt.Margin = new System.Windows.Forms.Padding(2);
            this.txtFFAlerteMt.MaxLength = 32767;
            this.txtFFAlerteMt.Multiline = false;
            this.txtFFAlerteMt.Name = "txtFFAlerteMt";
            this.txtFFAlerteMt.ReadOnly = false;
            this.txtFFAlerteMt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFFAlerteMt.Size = new System.Drawing.Size(119, 27);
            this.txtFFAlerteMt.TabIndex = 571;
            this.txtFFAlerteMt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFFAlerteMt.UseSystemPasswordChar = false;
            // 
            // chkAlerteSurHt
            // 
            this.chkAlerteSurHt.AutoSize = true;
            this.chkAlerteSurHt.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAlerteSurHt.Location = new System.Drawing.Point(17, 37);
            this.chkAlerteSurHt.Name = "chkAlerteSurHt";
            this.chkAlerteSurHt.Size = new System.Drawing.Size(436, 23);
            this.chkAlerteSurHt.TabIndex = 570;
            this.chkAlerteSurHt.Text = "Envoyer une alerte (Email) si le montant  HT est supérieur à ";
            this.chkAlerteSurHt.UseVisualStyleBackColor = true;
            this.chkAlerteSurHt.CheckedChanged += new System.EventHandler(this.chkAlerteSurHt_CheckStateChanged);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(534, 1);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 412;
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // frmParamFactFourn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(606, 151);
            this.Controls.Add(this.CmdSauver);
            this.Controls.Add(this.groupBox6);
            this.MaximumSize = new System.Drawing.Size(622, 190);
            this.MinimumSize = new System.Drawing.Size(622, 190);
            this.Name = "frmParamFactFourn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmParamFactFourn";
            this.Load += new System.EventHandler(this.frmParamFactFourn_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.CheckBox chkAlerteSurHt;
        public iTalk.iTalk_TextBox_Small2 txtFFAlerteMt;
        public System.Windows.Forms.Button CmdSauver;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}