﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseur.IntegrationDesFactures.Forms
{
    public partial class frmParamFactFourn : Form
    {
        ModAdo ModAdoPF = new ModAdo();
        public frmParamFactFourn()
        {
            InitializeComponent();
        }
        private void Label1_Click()
        {

        }
        //Tested
        private void chkAlerteSurHt_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (chkAlerteSurHt.Checked == true)
            {
                txtFFAlerteMt.Enabled = true;
            }
            else
            {
                txtFFAlerteMt.Text = "0";
                txtFFAlerteMt.Enabled = false;

            }
        }
        //Tested
        private void CmdSauver_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            try
            {

                if (chkAlerteSurHt.Checked == true)
                {
                    if (!General.IsNumeric(txtFFAlerteMt.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le montant HT est incorrect.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                General.sSQL = "UPDATE lien  set adresse ='" + chkAlerteSurHt.CheckState + "'" + " WHERE Code ='" + Variable.FFAlerteSurHt + "'";
                General.Execute(General.sSQL);

                General.sSQL = "UPDATE lien  set adresse ='" + txtFFAlerteMt.Text + "'" + " WHERE Code ='" + Variable.FFAlerteMt + "'";
                General.Execute(General.sSQL);

                this.Close();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdSauver_Click");
            }

        }
        //Tested
        private void frmParamFactFourn_Load(System.Object eventSender, System.EventArgs eventArgs)
        {

            fc_Loadparam();

        }
        //Tested
        private void fc_Loadparam()
        {
            string sSQL = null;

            if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                sSQL = "SELECT adresse FROM lienV2 WHERE Code ='" + Variable.FFAlerteSurHt + "'";
            else
                sSQL = "SELECT adresse FROM lien WHERE Code ='" + Variable.FFAlerteSurHt + "'";

            sSQL = ModAdoPF.fc_ADOlibelle(sSQL);
            if (string.IsNullOrEmpty(sSQL))
            {

                sSQL = "INSERT INTO Lien" + "(Code, adresse)" + " VALUES     ('" + Variable.FFAlerteSurHt + "', '" + "0" + "')";
                General.Execute(sSQL);

                sSQL = "INSERT INTO Lien" + "(Code, adresse)" + " VALUES     ('" + Variable.FFAlerteMt + "', '" + "0" + "')";
                General.Execute(sSQL);

            }


            if (sSQL == "Checked")
            {
                chkAlerteSurHt.Checked = true;

                //== recherche le montant.
                txtFFAlerteMt.Enabled = true;
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    sSQL = "SELECT adresse FROM lienV2 WHERE Code = '" + Variable.FFAlerteMt + "'";
                else
                    sSQL = "SELECT adresse FROM lien WHERE Code = '" + Variable.FFAlerteMt + "'";
                txtFFAlerteMt.Text = General.nz(ModAdoPF.fc_ADOlibelle(sSQL), "0").ToString();

            }
            else
            {
                chkAlerteSurHt.Checked = false;
                txtFFAlerteMt.Enabled = false;
                txtFFAlerteMt.Text = Convert.ToString(0);
            }

        }
    }
}
