﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Commande.UserPreCommande;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur.Forms;
using Axe_interDT.Views.Intervention;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur
{
    public partial class UserHistoReadSoft : UserControl
    {
        string sRSTE_NoAuto = null;

        int iOldCol = 0;
        bool bTriAsc;
        string sSqlRSTE;
        string sOrderBy;
        const short cCol = 0;
        const string Cmdp = "admin";



        DataTable rsRSTE = new DataTable();
        ModAdo rsRSTEAdo = new ModAdo();


        public UserHistoReadSoft()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbAcheteur_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            //sSQL = "SELECT USR_Nt as [LogIn], USR_Name FROM USR_Users" _
            //& "  WHERE     (USR_NonActif = 0) OR" _
            //& " (USR_NonActif IS NULL)" _
            //& " ORDER BY USR_Nt"

            sSQL = "SELECT USR_Nt as [LogIn], USR_Name FROM USR_Users" + " ORDER BY USR_Nt";

            sheridan.InitialiseCombo(cmbAcheteur, sSQL, "LogIn");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSTF_StatutFactFourn_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT     STF_Code as [Code], STF_Libelle as [Libelle]" + " FROM STF_StatutFactFourn";

            sheridan.InitialiseCombo(cmbSTF_StatutFactFourn, sSQL, "Code");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            txtNoBonDe.Text = "";
            txtNoBonAu.Text = "";
            txtCodeImmeuble.Text = "";
            txtCode1.Text = "";
            txtCodeFourn.Text = "";
            txtRaisonSocial.Text = "";
            txtNoFactureDe.Text = "";
            txtNoFactureAu.Text = "";
            txtRSTE_DateComptabiliseDe.Text = "";
            txtRSTE_DateComptabiliseAu.Text = "";
            txtRSTE_DateFactureDe.Text = "";
            txtRSTE_DateFactureAu.Text = "";
            cmbAcheteur.Text = "";
            cmbSTF_StatutFactFourn.Text = "";
            txtRSTE_HT2.Text = "";
            txtNodevis.Text = "";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMail_Click(object sender, EventArgs e)
        {
            frmSendFactFourn frm = new frmSendFactFourn();
            frm.txtFile.Text = ssRSTE.ActiveRow != null ? ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Text : "";
            frm.ShowDialog();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheDevis_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "SELECT NumeroDevis AS \"N°Devis\", codeimmeuble as \"CodeImmeuble\" FROM DEVISENTETE  ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche N°devis" };

                fg.SetValues(new Dictionary<string, string> { { "NumeroDevis", txtNodevis.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtNodevis.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtNodevis.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdRechercheDevis_Click");
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);
            //ADODB.Field oField = default(ADODB.Field);

            int i = 0;
            int j = 0;
            string sMatricule = null;
            string sSQL = null;
            int lLigne = 0;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                rsExport = rsRSTE;

                var _with2 = rsExport;

                if (_with2.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }

                // '=== modif du 20 04 2015, ajout des colonnes cumul facture + ft bon de commande.
                // oSheet.Cells(1, i).VerticalAlignment = xlVAlignCenter
                // oSheet.Cells(1, i).value = "Cumul Facture"
                // i = i + 1
                //
                //oSheet.Cells(1, i).VerticalAlignment = xlVAlignCenter
                //oSheet.Cells(1, i).value = "Total ht bon de commande"
                //i = i + 1


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;

                // _with2.MoveFirst();
                var tmp = new ModAdo();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {

                        if (General.UCase("HtBcd") == General.UCase(rsExportCol.ColumnName))
                        {
                            //=== total ht du bon de commande.

                            //sSQL = "SELECT  sum(   BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail "
                            //    + " WHERE   BCD_Cle = " + General.nz(rsExportRows["NoBonDeCommande"], 0);

                            //if (Convert.ToInt32(General.nz(rsExportRows["NoBonDeCommande"], 0)) != 0)
                            //{
                            //    sSQL = tmp.fc_ADOlibelle(sSQL);

                            //    oSheet.Cells[lLigne, j + cCol].value = General.FncArrondir(Convert.ToDouble(General.nz(sSQL, 0)), 2);
                            //}
                            if (Convert.ToInt32(General.nz(rsExportRows["NoBonDeCommande"], 0)) != 0)
                            {
                                sSQL = "SELECT  BCD_Quantite,BCD_PrixHT " + " From BCD_Detail"
                                    + " WHERE   BCD_Cle = " + Convert.ToInt32(General.nz(rsExportRows["NoBonDeCommande"], 0));
                                var dt = new DataTable();
                                var modAdo = new ModAdo();
                                dt = modAdo.fc_OpenRecordSet(sSQL);
                                decimal sommeTabprd = 0;
                                decimal[] Tabprd = null;
                                for (int q = 0; q < dt.Rows.Count; q++)
                                {
                                    var BcdQt = dt.Rows[q]["BCD_Quantite"].ToString();
                                    BcdQt = BcdQt.Replace(',', '.').Trim();
                                    if (!string.IsNullOrEmpty(BcdQt) && General.IsNumeric(BcdQt)
                                        && !string.IsNullOrEmpty(dt.Rows[q]["BCD_PrixHT"].ToString())
                                        && General.IsNumeric(dt.Rows[q]["BCD_PrixHT"].ToString()))
                                    {
                                        Array.Resize(ref Tabprd, q + 1);
                                        Tabprd[q] = (Convert.ToDecimal(BcdQt)) * Convert.ToDecimal(dt.Rows[q]["BCD_PrixHT"]);
                                        sommeTabprd = sommeTabprd + Tabprd[q];
                                    }

                                }
                                oSheet.Cells[lLigne, j + cCol].value = General.FncArrondir(Convert.ToDouble(General.nz(sommeTabprd, 0)), 2);
                            }

                        }
                        else if (General.UCase("HtCumul") == General.UCase(rsExportCol.ColumnName))
                        {
                            //=== cumul facture par bon de commande.

                            sSQL = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete" + " WHERE     NoBonDeCommande = "
                                + General.nz(rsExportRows["NoBonDeCommande"], 0);


                            if (Convert.ToInt32(General.nz(rsExportRows["NoBonDeCommande"], 0)) != 0)
                            {
                                sSQL = tmp.fc_ADOlibelle(sSQL);

                                oSheet.Cells[lLigne, j + cCol].value = General.nz(sSQL, 0);

                            }
                        }
                        else
                        {

                            oSheet.Cells[lLigne, j + cCol].value = rsExportRows[rsExportCol.ColumnName];
                        }
                        j = j + 1;
                    }


                    lLigne = lLigne + 1;
                    // _with2.MoveNext();
                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;

                oSheet = null;

                oWB = null;

                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                // _with2.Dispose();

                // rsExport = null;
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Error: " + ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }



        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGerant_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "SELECT " + "Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1   ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  des gerants" };

                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCode1.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCode1.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGerant_Click");
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "SELECT " + "CodeImmeuble as \"Nom directeur\", " + " Adresse as \"adresse\", Ville as \"Ville\" " + " , anglerue as \"angle de rue\" " + " FROM Immeuble ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  des immeuble" };

                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechFourn_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";

                string requete = "SELECT  Code as \"code\",CleAuto as  \"N°Fiche\" ,  Raisonsocial as \"RaisonSociale\" ,"
                    + " Adresse as \"Adresse\", Ville as \"Ville\", NePasAfficher as \"NePasAfficher\" " + " FROM fournisseurArticle";
                string where = " (code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 ) ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  des fournisseurs" };

                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                    txtCodeFourn.Text = sCode;
                    txtRaisonSocial.Text = fg.ugResultat.ActiveRow.Cells["RaisonSociale"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                        txtCodeFourn.Text = sCode;
                        txtRaisonSocial.Text = fg.ugResultat.ActiveRow.Cells["RaisonSociale"].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechFourn_Click");
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisuFactures_Click(object sender, EventArgs e)
        {
            fc_savPar();

            fc_CtrlFactFourn();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlFactFourn()
        {
            bool functionReturnValue = false;
            //==== controle les factures fournisseurs à intégrer, retourne false
            //==== en cas d'anomalies constatées et true dans le cas contraire.

            string sSQL = null;
            string sFROM = null;
            string sWhere = null;
            string sOrder = null;





            try
            {

                if (!string.IsNullOrEmpty(txtRSTE_HT2.Text))
                {
                    if (!General.IsNumeric(txtRSTE_HT2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le montant n'est pas un numérique", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return functionReturnValue;
                    }

                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // sSQL = "SELECT  RSTE_NoAuto ,RSTE_NoLigne ,  RSTE_dateCreat, RSTE_CreePar, RSTE_NoFacture," _
                //& " NoBonDeCommande,  RSTE_ReadSoftEnTete.CodeImmeuble, RSTE_HT, RSTE_TauxTVA, RSTE_MtTVA, " _
                //& " RSTE_TTC, RSTE_DateFacture, RSTE_DateEcheance, RSTE_CodeFournCleAuto," _
                //& " RSTE_CodeFourn, RSTE_RaisonSocial,RSTE_ModeRgt, RSTE_CT_Num," _
                //& " RSTE_CptChargeDef , RSTE_CptTvaDef , RSTE_Comptabilise, " _
                //& " RSTE_DateComptabilise,RSTE_Anomalie, FactNoautofacture , Immeuble.Code1, RSTE_CheminFichierClasse" _
                //& " ,RSTE_SansNoCommande " _
                //& " FROM RSTE_ReadSoftEnTete LEFT OUTER JOIN" _
                //& " Immeuble ON RSTE_ReadSoftEnTete.CodeImmeuble = Immeuble.CodeImmeuble" _
                //& " " _
                //
                sSqlRSTE = "SELECT     RSTE_ReadSoftEnTete.RSTE_NoAuto, RSTE_ReadSoftEnTete.RSTE_NoLigne, "
                    + " RSTE_ReadSoftEnTete.RSTE_dateCreat,RSTE_ReadSoftEnTete.RSTE_CreePar,"
                    + " RSTE_ReadSoftEnTete.RSTE_NoFacture, RSTE_ReadSoftEnTete.NoBonDeCommande, '' as HtBcd,"
                    + " BonDeCommande.Acheteur ," + " RSTE_ReadSoftEnTete.CodeImmeuble,'' as HtCumul, RSTE_ReadSoftEnTete.RSTE_HT,"
                    + " RSTE_ReadSoftEnTete.RSTE_TauxTVA, RSTE_ReadSoftEnTete.RSTE_MtTVA,"
                    + " RSTE_ReadSoftEnTete.RSTE_TTC, RSTE_ReadSoftEnTete.RSTE_DateFacture,"
                    + " RSTE_ReadSoftEnTete.RSTE_DateEcheance,RSTE_ReadSoftEnTete.RSTE_CodeFournCleAuto,"
                    + " RSTE_ReadSoftEnTete.RSTE_CodeFourn, RSTE_ReadSoftEnTete.RSTE_RaisonSocial,"
                    + " RSTE_ReadSoftEnTete.RSTE_ModeRgt, RSTE_ReadSoftEnTete.RSTE_CT_Num,"
                    + " RSTE_ReadSoftEnTete.RSTE_CptChargeDef, RSTE_ReadSoftEnTete.RSTE_CptTvaDef,"
                    + " RSTE_ReadSoftEnTete.RSTE_Comptabilise, RSTE_ReadSoftEnTete.RSTE_DateComptabilise,"
                    + " RSTE_ReadSoftEnTete.RSTE_Anomalie, RSTE_ReadSoftEnTete.FactNoautofacture,"
                    + " Immeuble.Code1,RSTE_ReadSoftEnTete.RSTE_CheminFichierClasse ,"
                    + " RSTE_ReadSoftEnTete.RSTE_SansNoCommande, RSTE_ReadSoftEnTete.STF_Code, "
                    + " RSTE_ReadSoftEnTete.RSTE_Commentaire , RSTE_ReadSoftEnTete.RSTE_ControleLe, "
                    + "  Intervention.NoIntervention,RSTE_ReadSoftEnTete.RSTE_ControlePar,"
                    + " GestionStandard.NoDevis, GestionStandard.NumFicheStandard";

                sFROM = " FROM Intervention INNER JOIN" + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                    + " RIGHT OUTER JOIN BonDeCommande " + " ON Intervention.NoIntervention = BonDeCommande.NoIntervention "
                    + " RIGHT OUTER JOIN" + " RSTE_ReadSoftEnTete ON BonDeCommande.NoBonDeCommande = RSTE_ReadSoftEnTete.NoBonDeCommande"
                    + " LEFT OUTER JOIN Immeuble " + " ON RSTE_ReadSoftEnTete.CodeImmeuble = Immeuble.CodeImmeuble";

                sWhere = "";

                if (!string.IsNullOrEmpty(txtNoBonDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.NoBonDeCommande >= " + txtNoBonDe.Text.Trim();
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_ReadSoftEnTete.NoBonDeCommande >=" + txtNoBonDe.Text.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(cmbAcheteur.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE BonDeCommande.Acheteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbAcheteur.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND BonDeCommande.Acheteur ='" + StdSQLchaine.gFr_DoublerQuote(cmbAcheteur.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtRSTE_HT1.Text))
                {
                    if (!string.IsNullOrEmpty(txtRSTE_HT2.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {

                            sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.RSTE_HT between "
                                + StdSQLchaine.gFr_NombreAng(txtRSTE_HT1.Text) + " AND "
                                + StdSQLchaine.gFr_NombreAng(txtRSTE_HT2.Text);
                        }
                        else
                        {

                            sWhere = sWhere + " AND RSTE_ReadSoftEnTete.RSTE_HT between "
                                + StdSQLchaine.gFr_NombreAng(txtRSTE_HT1.Text) + " AND "
                                + StdSQLchaine.gFr_NombreAng(txtRSTE_HT2.Text);
                        }
                    }
                    else
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {

                            sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.RSTE_HT <= "
                                    + StdSQLchaine.gFr_NombreAng(txtRSTE_HT1.Text);
                        }
                        else
                        {

                            sWhere = sWhere + " AND RSTE_ReadSoftEnTete.RSTE_HT <="
                                    + StdSQLchaine.gFr_NombreAng(txtRSTE_HT1.Text);
                        }
                    }
                }
                if (!string.IsNullOrEmpty(txtRSTE_HT2.Text))
                {
                    if (string.IsNullOrEmpty(txtRSTE_HT1.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {

                            sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.RSTE_HT >= " + StdSQLchaine.gFr_NombreAng(txtRSTE_HT2.Text);
                        }
                        else
                        {

                            sWhere = sWhere + " AND RSTE_ReadSoftEnTete.RSTE_HT >=" + StdSQLchaine.gFr_NombreAng(txtRSTE_HT2.Text);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtNoBonAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.NoBonDeCommande <= " + txtNoBonAu.Text.Trim();
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_ReadSoftEnTete.NoBonDeCommande <=" + txtNoBonAu.Text.Trim();
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND RSTE_ReadSoftEnTete.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtNodevis.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE GestionStandard.NoDevis ='" + StdSQLchaine.gFr_DoublerQuote(txtNodevis.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND GestionStandard.NoDevis='" + StdSQLchaine.gFr_DoublerQuote(txtNodevis.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeFourn.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE RSTE_ReadSoftEnTete.RSTE_CodeFourn ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeFourn.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND RSTE_ReadSoftEnTete.RSTE_CodeFourn ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeFourn.Text) + "'";
                    }

                }


                if (!string.IsNullOrEmpty(txtCode1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoFactureDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE RSTE_NoFacture >='" + StdSQLchaine.gFr_DoublerQuote(txtNoFactureDe.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND RSTE_NoFacture >='" + StdSQLchaine.gFr_DoublerQuote(txtNoFactureDe.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoFactureAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE RSTE_NoFacture <='" + StdSQLchaine.gFr_DoublerQuote(txtNoFactureAu.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND RSTE_NoFacture <='" + StdSQLchaine.gFr_DoublerQuote(txtNoFactureAu.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtRSTE_DateComptabiliseDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_DateComptabilise >='" + (txtRSTE_DateComptabiliseDe.Text + " 00:00:00").Trim() + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_DateComptabilise >='" + (txtRSTE_DateComptabiliseDe.Text + " 00:00:00").Trim() + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtRSTE_DateComptabiliseAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_DateComptabilise <='" + (txtRSTE_DateComptabiliseAu.Text + " 23:59:59").Trim() + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_DateComptabilise <='" + (txtRSTE_DateComptabiliseAu.Text + " 23:59:59").Trim() + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtRSTE_DateFactureDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_DateFacture >='" + (txtRSTE_DateFactureDe.Text + " 00:00:00").Trim() + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_DateFacture >='" + (txtRSTE_DateFactureDe.Text + " 00:00:00").Trim() + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtRSTE_DateFactureAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_DateFacture <='" + (txtRSTE_DateFactureAu.Text + " 23:59:59").Trim() + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_DateFacture <='" + (txtRSTE_DateFactureAu.Text + " 23:59:59").Trim() + "'";
                    }
                }

                if (optAvecBon.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE (RSTE_SansNoCommande = 0 or RSTE_SansNoCommande is null) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (RSTE_SansNoCommande = 0 or RSTE_SansNoCommande is null) ";
                    }
                }
                else if (optSansBon.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE RSTE_SansNoCommande = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RSTE_SansNoCommande = 1 ";
                    }

                }

                if (!string.IsNullOrEmpty(cmbSTF_StatutFactFourn.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " WHERE STF_Code = '" + cmbSTF_StatutFactFourn.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND STF_Code = '" + cmbSTF_StatutFactFourn.Text + "'";
                    }
                }

                if (string.IsNullOrEmpty(sOrderBy))
                {
                    sOrder = " order by RSTE_dateCreat";
                }
                else
                {
                    sOrder = " " + sOrderBy;
                }

                sSqlRSTE = sSqlRSTE + sFROM + sWhere;

                rsRSTE = rsRSTEAdo.fc_OpenRecordSet(sSqlRSTE + sOrder);
                //ssRSTE.DataSource = rsRSTE;
                DataTable dtSource = new DataTable();
                dtSource.Columns.Add("RSTE_NoAuto", typeof(int));
                dtSource.Columns.Add("RSTE_NoLigne", typeof(int));
                dtSource.Columns.Add("RSTE_dateCreat", typeof(DateTime));
                dtSource.Columns.Add("RSTE_CreePar");
                dtSource.Columns.Add("RSTE_NoFacture");
                dtSource.Columns.Add("NoBonDeCommande", typeof(int));
                dtSource.Columns.Add("HtBcd", typeof(double));
                dtSource.Columns.Add("CodeImmeuble");
                dtSource.Columns.Add("Acheteur");
                dtSource.Columns.Add("HtCumul", typeof(double));
                dtSource.Columns.Add("RSTE_HT", typeof(double));
                dtSource.Columns.Add("RSTE_TauxTVA", typeof(double));
                dtSource.Columns.Add("RSTE_MtTVA", typeof(double));
                dtSource.Columns.Add("RSTE_TTC", typeof(double));
                dtSource.Columns.Add("RSTE_DateFacture", typeof(DateTime));
                dtSource.Columns.Add("RSTE_DateEcheance", typeof(DateTime));
                dtSource.Columns.Add("RSTE_CodeFournCleAuto", typeof(int));
                dtSource.Columns.Add("RSTE_CodeFourn");
                dtSource.Columns.Add("RSTE_RaisonSocial");
                dtSource.Columns.Add("RSTE_ModeRgt");
                dtSource.Columns.Add("RSTE_CT_Num");
                dtSource.Columns.Add("RSTE_CptChargeDef");
                dtSource.Columns.Add("RSTE_CptTvaDef");
                dtSource.Columns.Add("RSTE_Comptabilise", typeof(int));
                dtSource.Columns.Add("RSTE_DateComptabilise", typeof(DateTime));
                dtSource.Columns.Add("RSTE_Anomalie");
                dtSource.Columns.Add("FactNoautofacture");
                dtSource.Columns.Add("Code1");
                dtSource.Columns.Add("RSTE_CheminFichierClasse");
                dtSource.Columns.Add("RSTE_SansNoCommande", typeof(int));
                dtSource.Columns.Add("STF_Code");
                dtSource.Columns.Add("RSTE_Commentaire");
                dtSource.Columns.Add("RSTE_Controlele");
                dtSource.Columns.Add("NoIntervention", typeof(int));
                dtSource.Columns.Add("RSTE_ControlePar");
                dtSource.Columns.Add("NoDevis");
                dtSource.Columns.Add("NumFicheStandard", typeof(int));

                ssRSTE.DataSource = dtSource;

                //ssRSTE.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;

                //ssRSTE.EventManager.AllEventsEnabled = false;

                if (rsRSTE != null && rsRSTE.Rows.Count > 0)
                {
                    if (rsRSTE.Rows.Count > 1000)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez afficher " + rsRSTE.Rows.Count + " articles." + "\n" + "Ca risque de prendre du temps, souhaitez-vous continuer ?", "Nombre de données important",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            ssRSTE.DataSource = dtSource;
                            rsRSTE?.Dispose();
                            Cursor.Current = Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    foreach (DataRow dr in rsRSTE.Rows)
                    {
                        //Application.DoEvents();
                        dtSource.Rows.Add(dr["RSTE_NoAuto"],
                            dr["RSTE_NoLigne"].ToString() == "" ? DBNull.Value : dr["RSTE_NoLigne"],
                            dr["RSTE_dateCreat"].ToString() == "" ? DBNull.Value : dr["RSTE_dateCreat"],
                            dr["RSTE_CreePar"].ToString() == "" ? DBNull.Value : dr["RSTE_CreePar"],
                            dr["RSTE_NoFacture"].ToString() == "" ? DBNull.Value : dr["RSTE_NoFacture"],
                            dr["NoBonDeCommande"].ToString() == "" ? DBNull.Value : dr["NoBonDeCommande"],
                            dr["HtBcd"].ToString() == "" ? DBNull.Value : dr["HtBcd"],
                            dr["CodeImmeuble"].ToString() == "" ? DBNull.Value : dr["CodeImmeuble"],
                            dr["Acheteur"].ToString() == "" ? DBNull.Value : dr["Acheteur"],
                            dr["HtCumul"].ToString() == "" ? DBNull.Value : dr["HtCumul"],
                            dr["RSTE_HT"].ToString() == "" ? DBNull.Value : dr["RSTE_HT"],
                            dr["RSTE_TauxTVA"].ToString() == "" ? DBNull.Value : dr["RSTE_TauxTVA"],
                            dr["RSTE_MtTVA"].ToString() == "" ? DBNull.Value : dr["RSTE_MtTVA"],
                            dr["RSTE_TTC"].ToString() == "" ? DBNull.Value : dr["RSTE_TTC"],
                            dr["RSTE_DateFacture"].ToString() == "" ? DBNull.Value : dr["RSTE_DateFacture"],
                            dr["RSTE_DateEcheance"].ToString() == "" ? DBNull.Value : dr["RSTE_DateEcheance"],
                            dr["RSTE_CodeFournCleAuto"].ToString() == "" ? DBNull.Value : dr["RSTE_CodeFournCleAuto"],
                            dr["RSTE_CodeFourn"].ToString() == "" ? DBNull.Value : dr["RSTE_CodeFourn"],
                            dr["RSTE_RaisonSocial"].ToString() == "" ? DBNull.Value : dr["RSTE_RaisonSocial"],
                            dr["RSTE_ModeRgt"].ToString() == "" ? DBNull.Value : dr["RSTE_ModeRgt"],
                            dr["RSTE_CT_Num"].ToString() == "" ? DBNull.Value : dr["RSTE_CT_Num"],
                            dr["RSTE_CptChargeDef"].ToString() == "" ? DBNull.Value : dr["RSTE_CptChargeDef"],
                            dr["RSTE_CptTvaDef"].ToString() == "" ? DBNull.Value : dr["RSTE_CptTvaDef"],
                            dr["RSTE_Comptabilise"].ToString() == "" ? DBNull.Value : dr["RSTE_Comptabilise"],
                            dr["RSTE_DateComptabilise"].ToString() == "" ? DBNull.Value : dr["RSTE_DateComptabilise"],
                            dr["RSTE_Anomalie"].ToString() == "" ? DBNull.Value : dr["RSTE_Anomalie"],
                            dr["FactNoautofacture"].ToString() == "" ? DBNull.Value : dr["FactNoautofacture"],
                            dr["Code1"].ToString() == "" ? DBNull.Value : dr["Code1"],
                            dr["RSTE_CheminFichierClasse"].ToString() == "" ? DBNull.Value : dr["RSTE_CheminFichierClasse"],
                            dr["RSTE_SansNoCommande"].ToString() == "" ? DBNull.Value : dr["RSTE_SansNoCommande"],
                            dr["STF_Code"].ToString() == "" ? DBNull.Value : dr["STF_Code"],
                            dr["RSTE_Commentaire"].ToString() == "" ? DBNull.Value : dr["RSTE_Commentaire"],
                            dr["RSTE_Controlele"].ToString() == "" ? DBNull.Value : dr["RSTE_Controlele"],
                            dr["NoIntervention"].ToString() == "" ? DBNull.Value : dr["NoIntervention"],
                            dr["RSTE_ControlePar"].ToString() == "" ? DBNull.Value : dr["RSTE_ControlePar"],
                            dr["NoDevis"].ToString() == "" ? DBNull.Value : dr["NoDevis"],
                            dr["NumFicheStandard"].ToString() == "" ? DBNull.Value : dr["NumFicheStandard"]);

                        Application.DoEvents();

                    }
                }

                //ssRSTE.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.SortSingle;
                txtTot.Text = rsRSTE.Rows.Count.ToString();
                using (var tmp = new ModAdo())
                {
                    TotHt.Text = General.nz(tmp.fc_ADOlibelle("SELECT sum(RSTE_HT) as Max " + sFROM + sWhere), "0").ToString();

                }

                //ssRSTE.EventManager.AllEventsEnabled = true;

                //ssRSTE.Rows.Refresh(RefreshRow.FireInitializeRow);

                //ssRSTE.UpdateData();



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return functionReturnValue;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlFactFourn");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserHistoReadSoft_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                return;
            }

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserHistoReadSoft");

            txtNoBonDe.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtNoBonDe", "");
            txtNoBonAu.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtNoBonAu", "");
            txtCodeImmeuble.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtCodeImmeuble", "");
            txtCode1.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtCode1", "");
            txtCodeFourn.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtcodeFourn", "");
            txtNoFactureDe.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtNoFactureDe", "");
            txtNoFactureAu.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtNoFactureAu", "");
            //txtRSTE_CreeParDe = GetSetting(cFrNomApp, cUserHistoReadSoft, "txtRSTE_CreeParDe", "")
            // txtRSTE_CreeParAu = GetSetting(cFrNomApp, cUserHistoReadSoft, "txtRSTE_CreeParAu", "")
            txtRSTE_DateFactureDe.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureDe", "");
            txtRSTE_DateFactureAu.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureAu", "");
            txtRSTE_DateComptabiliseDe.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateComptabiliseDe", "");
            txtRSTE_DateComptabiliseAu.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateComptabiliseAu", "");
            txtRSTE_DateFactureDe.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureDe", "");
            txtRSTE_DateFactureAu.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_DateFactureAu", "");
            cmbAcheteur.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "cmbAcheteur", "");
            cmbSTF_StatutFactFourn.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "cmbSTF_StatutFactFourn", "");
            sOrderBy = General.getFrmReg(Variable.cUserHistoReadSoft, "sOrderBy", "");

            optSansBon.Checked = General.getFrmReg(Variable.cUserHistoReadSoft, "optSansBon", "1").ToString() == "True";
            optAvecBon.Checked = General.getFrmReg(Variable.cUserHistoReadSoft, "optAvecBon", "1").ToString() == "True";
            optAllFact.Checked = General.getFrmReg(Variable.cUserHistoReadSoft, "optAllFact", "1").ToString() == "True";

            txtRSTE_HT2.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtRSTE_HT", "");
            txtNodevis.Text = General.getFrmReg(Variable.cUserHistoReadSoft, "txtNoDevis", "");

            sRSTE_NoAuto = General.getFrmReg(Variable.cUserHistoReadSoft, "NewVar", "");

            //SizeChanged += UserHistoReadSoft_SizeChanged;

            //================> Code Modifié par Mondir
            fc_CtrlFactFourn();

            if (!string.IsNullOrEmpty(sRSTE_NoAuto) && General.IsNumeric(sRSTE_NoAuto))
            {
                if ((rsRSTE != null))
                {

                    {
                        //rsRSTE.Find("RSTE_NoAuto = " + sRSTE_NoAuto, , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                        //if (!rsRSTE.EOF)
                        //{

                        //    ssRSTE.Bookmark = rsRSTE.Bookmark;
                        //    ssRSTE.SelBookmarks.RemoveAll();
                        //    ssRSTE.SelBookmarks.Add((ssRSTE.GetBookmark(0)));
                        //}
                        var r = ssRSTE.Rows != null ? ssRSTE.Rows.Where(l => l.Cells["RSTE_NoAuto"].Value.ToString() == sRSTE_NoAuto).FirstOrDefault() : null;
                        if (r != null)
                        {
                            r.Activate();
                            r.Selected = true;
                        }
                    }
                }
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir le 12.03.2021 changer le mot de pass, demanadé oar Fréd
            var aut = ModAutorisation.fc_DroitDetail("UserHistoReadSoft-ssRSTE_BeforeRowsDeleted", 2);
            if (aut == 0)
            {
                e.Cancel = true;
                return;
            }

            try
            {
                string sSQL = "";

                if (Convert.ToInt32(General.nz((ssRSTE.ActiveRow.Cells["RSTE_SansNoCommande"].Value), 0)) == 0)
                {
                    sSQL = "DELETE FROM FactFournHtTvaCpta WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
                    General.Execute(sSQL);

                    sSQL = "DELETE FROM FactFournEntete WHERE NoAutoFacture =" + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
                    General.Execute(sSQL);

                    sSQL = "DELETE FROM FactFournPied WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
                    General.Execute(sSQL);

                    sSQL = "DELETE FROM FactFournDetail WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
                    General.Execute(sSQL);
                }

                sSQL = "DELETE FROM RSTE_ReadSoftEnTete WHERE RSTE_Noauto =" + ssRSTE.ActiveRow.Cells["RSTE_Noauto"].Value;
                General.Execute(sSQL);

                if (Dossier.fc_ControleFichier((ssRSTE.ActiveRow.Cells["RSTE_CheminFichierclasse"].Text)) == true)
                {
                    Dossier.fc_SupprimeFichier((ssRSTE.ActiveRow.Cells["RSTE_CheminFichierclasse"].Text));
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }

            //===> Fin Modif Mondir

            //====> Mondir le 12.03.2021, code bellow is commented
            //string sSQL = null;
            //bool bok = false;
            //try
            //{
            //    e.DisplayPromptMsg = false;

            //    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous supprimer la facture n°" + ssRSTE.ActiveRow.Cells["RSTE_NoFacture"].Text + " ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //    {

            //        sSQL = Interaction.InputBox("Veuillez saisir un mot de passe.", "Mot de passe");
            //        if (sSQL != Cmdp)
            //        {

            //            sSQL = Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");

            //            if (sSQL != Cmdp)
            //            {
            //                sSQL = Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");

            //                if (sSQL != Cmdp)
            //                {
            //                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppression annulée, vous n'avez pas les autorisations nécessaires pour Supprimer cette facture.", "Suppression annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //                    e.Cancel = true;
            //                    return;
            //                }
            //                else if (sSQL == Cmdp)
            //                {
            //                    bok = true;

            //                }
            //            }
            //            else if (sSQL == Cmdp)
            //            {
            //                bok = true;
            //            }

            //        }
            //        else if (sSQL == Cmdp)
            //        {
            //            bok = true;

            //        }


            //        if (bok == true)
            //        {

            //            if (Convert.ToInt32(General.nz((ssRSTE.ActiveRow.Cells["RSTE_SansNoCommande"].Value), 0)) == 0)
            //            {
            //                sSQL = "DELETE FROM FactFournHtTvaCpta WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
            //                General.Execute(sSQL);

            //                sSQL = "DELETE FROM FactFournEntete WHERE NoAutoFacture =" + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
            //                General.Execute(sSQL);

            //                sSQL = "DELETE FROM FactFournPied WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
            //                General.Execute(sSQL);

            //                sSQL = "DELETE FROM FactFournDetail WHERE NoAutoFacture = " + ssRSTE.ActiveRow.Cells["FactNoautofacture"].Value;
            //                General.Execute(sSQL);
            //            }

            //            sSQL = "DELETE FROM RSTE_ReadSoftEnTete WHERE RSTE_Noauto =" + ssRSTE.ActiveRow.Cells["RSTE_Noauto"].Value;
            //            General.Execute(sSQL);

            //            if (Dossier.fc_ControleFichier((ssRSTE.ActiveRow.Cells["RSTE_CheminFichierclasse"].Text)) == true)
            //            {
            //                Dossier.fc_SupprimeFichier((ssRSTE.ActiveRow.Cells["RSTE_CheminFichierclasse"].Text));
            //            }

            //            // fc_CtrlFactFourn();

            //        }
            //        else
            //        {
            //            e.Cancel = true;
            //        }
            //    }
            //    else
            //    {
            //        e.Cancel = true;
            //        return;
            //    }

            //    return;
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, this.Name + ";ssRSTE_BeforeDelete");
            //}
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_savPar()
        {
            try
            {
                var _with9 = ssRSTE;

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserHistoReadSoft, ssRSTE.ActiveRow != null ? ssRSTE.ActiveRow.Cells["RSTE_NoAuto"].Value.ToString() : "",
                txtNoBonDe.Text, txtNoBonAu.Text, txtCodeImmeuble.Text, txtCode1.Text, txtCodeFourn.Text, txtNoFactureDe.Text, txtNoFactureAu.Text,
                txtRSTE_DateFactureDe.Text, txtRSTE_DateFactureAu.Text, txtRSTE_DateComptabiliseDe.Text, txtRSTE_DateComptabiliseAu.Text,
                txtRSTE_DateFactureDe.Text, txtRSTE_DateFactureAu.Text, cmbAcheteur.Text, cmbSTF_StatutFactFourn.Text,
                sOrderBy, Convert.ToString(optSansBon.Checked),
                Convert.ToString(optAvecBon.Checked), Convert.ToString(optAllFact.Checked), txtRSTE_HT2.Text, txtNodevis.Text);
            }
            catch (Exception e) { Program.SaveException(e); }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Text))
            {
                ModuleAPI.Ouvrir(ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Text);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            int i = 0;
            string sSQL = null;
            var tmp = new ModAdo();
            var _with10 = ssRSTE;
            var cc = ssRSTE.Rows.Count;
            //== applique une coubleur si une anomalie est déceleé.
            if (e.Row.Cells["RSTE_Comptabilise"].Text == "1")
            {
                //                If .Columns("RSTE_TTC").value < 0 Then
                //                    For I = 0 To .Columns.Count - 1
                //
                //                        .Columns(I).CellStyleSet "AVOIR", .Row
                //                    Next I
                //                End If

            }

            if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutNV))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.DarkGray;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }

            }
            else if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutV))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.WhiteSmoke;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }

            }
            else if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutAV))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.PaleGreen;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }

            }
            else if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutAN))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.PaleVioletRed;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }

            }
            else if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutEN))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.HotPink;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }

            }
            else if (General.UCase(e.Row.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutAR))
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.Khaki;
                    e.Row.Cells[i].Appearance.ForeColor = Color.Black;
                }
            }

            //=== cumul fcature par bon de commande.

            sSQL = "SELECT     SUM(RSTE_HT) AS Expr1" + " From RSTE_ReadSoftEnTete" + " WHERE     NoBonDeCommande = "
                           + General.nz(e.Row.Cells["NoBonDeCommande"].Text, 0);

            if (!string.IsNullOrEmpty(e.Row.Cells["NoBonDeCommande"].Text))
            {
                sSQL = tmp.fc_ADOlibelle(sSQL);


                e.Row.Cells["HtCumul"].Value = General.nz(sSQL, 0);
            }
            //  BCD_Quantite est de type varchar donc une errur ce declenche lorsqu on fait la somme
            //sSQL = "SELECT  sum(   BCD_Quantite * BCD_PrixHT) AS Expr1" + " From BCD_Detail "
            //    + " WHERE   BCD_Cle = " + General.nz(e.Row.Cells["NoBonDeCommande"].Text, 0);

            //if (!string.IsNullOrEmpty(e.Row.Cells["NoBonDeCommande"].Text))
            //{
            //    sSQL = tmp.fc_ADOlibelle(sSQL);
            //    e.Row.Cells["HtBcd"].Value = General.FncArrondir(Convert.ToDouble(General.nz(sSQL, 0)), 2);
            //}
            //=== total ht du bon de commande.
            if (!string.IsNullOrEmpty(e.Row.Cells["NoBonDeCommande"].Text))
            {
                sSQL = "SELECT  BCD_Quantite,BCD_PrixHT " + " From BCD_Detail"
                    + " WHERE   BCD_Cle = " + General.nz(e.Row.Cells["NoBonDeCommande"].Text, 0);
                var dt = new DataTable();
                var modAdo = new ModAdo();
                dt = modAdo.fc_OpenRecordSet(sSQL);
                decimal sommeTabprd = 0;
                decimal[] Tabprd = null;
                for (int q = 0; q < dt.Rows.Count; q++)
                {
                    var BcdQt = dt.Rows[q]["BCD_Quantite"].ToString();
                    BcdQt = BcdQt.Replace(',', '.').Trim();
                    if (!string.IsNullOrEmpty(BcdQt) && General.IsNumeric(BcdQt)
                        && !string.IsNullOrEmpty(dt.Rows[q]["BCD_PrixHT"].ToString())
                        && General.IsNumeric(dt.Rows[q]["BCD_PrixHT"].ToString()))
                    {
                        Array.Resize(ref Tabprd, q + 1);
                        Tabprd[q] = (Convert.ToDecimal(BcdQt)) * Convert.ToDecimal(dt.Rows[q]["BCD_PrixHT"]);
                        sommeTabprd = sommeTabprd + Tabprd[q];
                    }

                }
                e.Row.Cells["HtBcd"].Value = General.FncArrondir(Convert.ToDouble(General.nz(sommeTabprd, 0)), 2);
            }
            e.Row.Update();
            // ssRSTE.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNoBonDe_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNoBonDe.Text) && !General.IsNumeric(txtNoBonDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Valeur invalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNoBonDe.Text = "";
                return;
            }

            if (string.IsNullOrEmpty(txtNoBonAu.Text))
                txtNoBonAu.Text = txtNoBonDe.Text;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNoFactureDe_Leave(object sender, EventArgs e)
        {

            if (string.IsNullOrEmpty(txtNoFactureAu.Text))
                txtNoFactureAu.Text = txtNoFactureDe.Text;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRSTE_DateFactureDe_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRSTE_DateFactureDe.Text) && !General.IsDate(txtRSTE_DateFactureDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtRSTE_DateFactureDe.Text = "";
                return;
            }
            if (string.IsNullOrEmpty(txtRSTE_DateFactureAu.Text))
                txtRSTE_DateFactureAu.Text = txtRSTE_DateFactureDe.Text;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRSTE_DateComptabiliseDe_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRSTE_DateComptabiliseDe.Text) && !General.IsDate(txtRSTE_DateComptabiliseDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date inivalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtRSTE_DateComptabiliseDe.Text = "";
                return;
            }
            if (string.IsNullOrEmpty(txtRSTE_DateComptabiliseAu.Text))
                txtRSTE_DateComptabiliseAu.Text = txtRSTE_DateComptabiliseDe.Text;
        }

        private void lblGoIntegration_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var lDroit = ModAutorisation.fc_DroitDetail("UserDocSageScan");
            if (lDroit == 0)
                return;

            View.Theme.Theme.Navigate(typeof(UserDocSageScan));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssRSTE.EventManager.AllEventsEnabled = false;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_NoAuto"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_NoLigne"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_dateCreat"].Header.Caption = "Scannée le";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CreePar"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_NoFacture"].Header.Caption = "N° Facture";
            ssRSTE.DisplayLayout.Bands[0].Columns["NoBonDeCommande"].Header.Caption = "N°BonDeCommande";
            ssRSTE.DisplayLayout.Bands[0].Columns["HtBcd"].Header.Caption = "Ht Bon De Commande";
            ssRSTE.DisplayLayout.Bands[0].Columns["HtCumul"].Header.Caption = "Cummul Facture HT";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_HT"].Header.Caption = "HT";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_HT"].Format = "#,##0.00";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_TauxTVA"].Header.Caption = "Taux TVA";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_MtTVA"].Header.Caption = "MT TVA";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_TTC"].Header.Caption = "TTC";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_DateFacture"].Header.Caption = "Date Facture";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_DateEcheance"].Header.Caption = "Date Echeance";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CodeFournCleAuto"].Header.Caption = "N° Fiche Fourn.";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CodeFourn"].Header.Caption = "Code Fournisseur";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_RaisonSocial"].Header.Caption = "Raison Social";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_ModeRgt"].Header.Caption = "Mode Rgt";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CT_Num"].Header.Caption = "N° Cpte Fournisseur";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CptChargeDef"].Header.Caption = "Cpte de charge";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CptTvaDef"].Header.Caption = "Cpte de TVA";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_Comptabilise"].Header.Caption = "Comptabilise";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_DateComptabilise"].Header.Caption = "Date Comptabilise";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_Anomalie"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["FactNoautofacture"].Header.Caption = "N° Fiche Facture";
            ssRSTE.DisplayLayout.Bands[0].Columns["Code1"].Header.Caption = "Gérant";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CheminFichierClasse"].Header.Caption = "Chemin Fichier";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_CheminFichierClasse"].Width = 300;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_SansNoCommande"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["STF_Code"].Header.Caption = "Statut Facturation";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_Commentaire"].Header.Caption = "Commentaire";
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_Controlele"].Header.Caption = "Controlé le";
            ssRSTE.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_ControlePar"].Header.Caption = "Controlé Par";
            ssRSTE.DisplayLayout.Bands[0].Columns["NumFicheStandard"].Header.Caption = "N° Affaire";

            //ssRSTE.DisplayLayout.Bands[0].Columns["RSTE_dateCreat"].DataType = typeof(DateTime);

            //ssRSTE.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            ssRSTE.EventManager.AllEventsEnabled = true;
        }

        private void ssRSTE_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAcheteur_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "SELECT USR_Nt as \"LogIn\", " + " USR_Name as \"Nom\"  FROM USR_Users ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche N°devis" };

                fg.SetValues(new Dictionary<string, string> { { "USR_Nt", txtNodevis.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    cmbAcheteur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        cmbAcheteur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAcheteur_Click");
                return;
            }
        }

        private void UserHistoReadSoft_SizeChanged(object sender, EventArgs e)
        {
            fc_CtrlFactFourn();

            if (!string.IsNullOrEmpty(sRSTE_NoAuto) && General.IsNumeric(sRSTE_NoAuto))
            {
                if ((rsRSTE != null))
                {

                    {
                        //rsRSTE.Find("RSTE_NoAuto = " + sRSTE_NoAuto, , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                        //if (!rsRSTE.EOF)
                        //{

                        //    ssRSTE.Bookmark = rsRSTE.Bookmark;
                        //    ssRSTE.SelBookmarks.RemoveAll();
                        //    ssRSTE.SelBookmarks.Add((ssRSTE.GetBookmark(0)));
                        //}
                        ssRSTE.Rows.Where(l => l.Cells["RSTE_NoAuto"].ToString() == sRSTE_NoAuto).ToList().ForEach(l => l.Selected = true);
                    }
                }
            }

            SizeChanged -= UserHistoReadSoft_SizeChanged;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuBonDeCommande_Click(object sender, EventArgs e)
        {
            if (General.sPrecommande == "1")//'==Fonction en cours de developpement.
            {
                if (ssRSTE.ActiveRow != null && !string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Text))
                {
                    fc_savPar();
                    General.saveInReg(Variable.cUserDocStandard, "NewVar", ssRSTE.ActiveRow.Cells["Numfichestandard"].Value.ToString());

                    General.saveInReg(Variable.cUserPreCommande, "Origine", Variable.cUserBCLivraison);
                    General.saveInReg(Variable.cUserPreCommande, "NewVar", ssRSTE.ActiveRow.Cells["NoIntervention"].Value.ToString());
                    General.saveInReg(Variable.cUserPreCommande, "NumFicheStandard", ssRSTE.ActiveRow.Cells["Numfichestandard"].Value.ToString());
                    General.saveInReg(Variable.cUserPreCommande, "NoBonDeCommande", ssRSTE.ActiveRow.Cells["NoBonDeCommande"].Value.ToString());
                    //'   bclick = True
                    View.Theme.Theme.Navigate(typeof(UserPreCommande));
                }
                else
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun bon de commande trouvé pour cette facture", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuDevis_Click(object sender, EventArgs e)
        {
            if (ssRSTE.ActiveRow != null && !string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["NoDevis"].Text))
            {
                //'===stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, ssRSTE.ActiveRow.Cells["NoDevis"].Value.ToString());
                fc_savPar();

                View.Theme.Theme.Navigate(typeof(UserDocDevis));
            }
            else
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis trouvé pour cette facture", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuIntervention_Click(object sender, EventArgs e)
        {
            if (ssRSTE.ActiveRow != null && !string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["NoIntervention"].Text))
            {
                //'===stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, ssRSTE.ActiveRow.Cells["NoIntervention"].Text);
                fc_savPar();

                View.Theme.Theme.Navigate(typeof(UserIntervention));
            }
            else
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention, Cette facture n'est pas liée à un bon de commande. ", "Aucune intervention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuVisu_Click(object sender, EventArgs e)
        {
            //===> Mondir le 20.11.2020, Cette cndition etais en bas, je l'ai remonté pour corrigé le ticket https://groupe-dt.mantishub.io/view.php?id=2084
            if (ssRSTE.ActiveRow == null)
                return;
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["RSTE_CheminFichierClasse"].Value.ToString()))
            {
                ssRSTE_DoubleClickRow(null, null);
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention en cours.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuAnalytique_Click(object sender, EventArgs e)
        {
            if (ssRSTE.ActiveRow != null && !string.IsNullOrEmpty(ssRSTE.ActiveRow.Cells["Nointervention"].Text))
            {
                //' stocke les paramétres pour la feuille de destination.
                General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", ssRSTE.ActiveRow.Cells["Numfichestandard"].Value.ToString());
                General.saveInReg(Variable.cUserDocAnalytique2, "codeimmeuble", "");
                General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");
                fc_savPar();
                View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));
            }
            else
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, aucune affaire en cours.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuValidation_Click(object sender, EventArgs e)
        {
            frmValideFacture frm = new frmValideFacture();
            if (ssRSTE.ActiveRow != null && General.UCase(ssRSTE.ActiveRow.Cells["STF_Code"].Text) == General.UCase(Variable.cStatutNV))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas contrôler cette facture,celle-ci doit d'abord être validée par le service de comptabilité.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Variable.bFactureValide = false;
            Variable.bFactureRefusee = false;
            Variable.bFactureEnAttente = false;
            Variable.bFactureRegule = false;

            Variable.sTempVariable = "";
            if (ssRSTE.ActiveRow != null && General.UCase(ssRSTE.ActiveRow.Cells["STF_Code"].Value.ToString()) == General.UCase(Variable.cStatutAN))
            {

                frm.OptAnnuler.Checked = true;
                frm.txtRSTE_Commentaire.Text = ssRSTE.ActiveRow.Cells["RSTE_Commentaire"].Value.ToString();
            }
            else if (ssRSTE.ActiveRow != null && General.UCase(ssRSTE.ActiveRow.Cells["STF_Code"].Value.ToString()) == General.UCase(Variable.cStatutV))
            {
                frm.optValider.Checked = true;

            }

            //frmValideFacture.Show vbModal
            frm.ShowDialog();

            if (Variable.bFactureRefusee == true)//Tested
            {
                ssRSTE.ActiveRow.Cells["STF_Code"].Value = Variable.cStatutAN;
                ssRSTE.ActiveRow.Cells["RSTE_Commentaire"].Value = Variable.sTempVariable;
                ssRSTE.ActiveRow.Cells["RSTE_ControleLe"].Value = DateTime.Now;
                ssRSTE.ActiveRow.Cells["RSTE_ControlePar"].Value = General.fncUserName();
                //''''''  ssRSTE.Columns("FactNoautofacture").value = lFactNoautofacture
                string reqUpdate = "update RSTE_ReadSoftEnTete set  STF_Code = '" + Variable.cStatutAN
                    + "' , RSTE_Commentaire ='" + StdSQLchaine.gFr_DoublerQuote(Variable.sTempVariable)
                    + "', RSTE_ControleLe ='" + DateTime.Now + "', RSTE_ControlePar ='"
                    + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'  where RSTE_NoAuto ="
                    + ssRSTE.ActiveRow.Cells["RSTE_NoAuto"].Value;
                int xx = General.Execute(reqUpdate);
                ssRSTE.UpdateData();
            }
            else if (Variable.bFactureValide == true)//tested
            {
                ssRSTE.ActiveRow.Cells["STF_Code"].Value = Variable.cStatutV;
                ssRSTE.ActiveRow.Cells["RSTE_Commentaire"].Value = Variable.sTempVariable;
                ssRSTE.ActiveRow.Cells["RSTE_ControleLe"].Value = DateTime.Now;
                ssRSTE.ActiveRow.Cells["RSTE_ControlePar"].Value = General.fncUserName();
                string reqUpdate = "update RSTE_ReadSoftEnTete set STF_Code ='" + Variable.cStatutV
                    + "',RSTE_Commentaire ='" + StdSQLchaine.gFr_DoublerQuote(Variable.sTempVariable)
                    + "',RSTE_ControleLe ='" + DateTime.Now + "',RSTE_ControlePar ='"
                    + StdSQLchaine.gFr_DoublerQuote(General.fncUserName())
                    + "'  where RSTE_NoAuto =" + ssRSTE.ActiveRow.Cells["RSTE_NoAuto"].Value;
                int xx = General.Execute(reqUpdate);
                ssRSTE.UpdateData();
            }
            else if (Variable.bFactureEnAttente == true)//tested
            {
                ssRSTE.ActiveRow.Cells["STF_Code"].Value = Variable.cStatutEN;
                ssRSTE.ActiveRow.Cells["RSTE_Commentaire"].Value = Variable.sTempVariable;
                ssRSTE.ActiveRow.Cells["RSTE_ControleLe"].Value = DateTime.Now;
                ssRSTE.ActiveRow.Cells["RSTE_ControlePar"].Value = General.fncUserName();
                string reqUpdate = "update RSTE_ReadSoftEnTete set STF_Code ='" + Variable.cStatutEN
                    + "',RSTE_Commentaire ='" + StdSQLchaine.gFr_DoublerQuote(Variable.sTempVariable)
                    + "',RSTE_ControleLe ='" + DateTime.Now + "',RSTE_ControlePar ='"
                    + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'  where RSTE_NoAuto ="
                    + ssRSTE.ActiveRow.Cells["RSTE_NoAuto"].Value;
                int xx = General.Execute(reqUpdate);
                ssRSTE.UpdateData();
            }
            else if (Variable.bFactureRegule == true)//tested
            {
                ssRSTE.ActiveRow.Cells["STF_Code"].Value = Variable.cStatutAR;
                ssRSTE.ActiveRow.Cells["RSTE_Commentaire"].Value = Variable.sTempVariable;
                ssRSTE.ActiveRow.Cells["RSTE_ControleLe"].Value = DateTime.Now;
                ssRSTE.ActiveRow.Cells["RSTE_ControlePar"].Value = General.fncUserName();
                string reqUpdate = "update RSTE_ReadSoftEnTete set STF_Code ='" + Variable.cStatutAR
                    + "',RSTE_Commentaire ='" + StdSQLchaine.gFr_DoublerQuote(Variable.sTempVariable)
                    + "',RSTE_ControleLe ='" + DateTime.Now + "',RSTE_ControlePar ='"
                    + StdSQLchaine.gFr_DoublerQuote(General.fncUserName())
                    + "'  where RSTE_NoAuto =" + ssRSTE.ActiveRow.Cells["RSTE_NoAuto"].Value;
                int xx = General.Execute(reqUpdate);
                ssRSTE.UpdateData();
            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssRSTE_AfterSortChange(object sender, BandEventArgs e)
        {
            //if (e.Band.SortedColumns[0].Index == iOldCol)
            //{
            //    bTriAsc = !bTriAsc;
            //}
            //else
            //{
            //    bTriAsc = true;
            //}
            //string s = bTriAsc ? " " : " Desc";
            //if (General.UCase(e.Band.SortedColumns[0].ToString()) == General.UCase("NoIntervention"))
            //{

            //    sOrderBy = " ORDER BY intervention.Nointervention " + s;
            //}
            //else if (General.UCase(e.Band.SortedColumns[0].ToString()) == General.UCase("Nobondecommande"))
            //{

            //    sOrderBy = " ORDER BY bondecommande.Nobondecommande " + s;
            //}
            //else if (General.UCase(e.Band.SortedColumns[0].ToString()) == General.UCase("CODEIMMEUBLE"))
            //{

            //    sOrderBy = " ORDER BY Immeuble.CodeImmeuble " + s;
            //}
            //else if (General.UCase(e.Band.SortedColumns[0].ToString()) == General.UCase("NODEVIS"))
            //{

            //    sOrderBy = " ORDER BY Gestionstandard.nodevis " +s;
            //}
            //else if (General.UCase(e.Band.SortedColumns[0].ToString()) == General.UCase("numfichestandard"))
            //{

            //    sOrderBy = " ORDER BY Gestionstandard.numfichestandard  " + s;
            //}
            //else
            //{

            //    sOrderBy = " ORDER BY " + e.Band.SortedColumns[0].ToString() + s;
            //}
            //// 'clause de classement
            //rsRSTE = rsRSTEAdo.fc_OpenRecordSet(sSqlRSTE + sOrderBy);

            //if (rsRSTE.Rows.Count == 0)
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune facture pour ces paramétres ?", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}
            ////ssRSTE.DataSource=rsRSTE ;

            //DataTable dtSource = new DataTable();
            //dtSource.Columns.Add("RSTE_NoAuto");
            //dtSource.Columns.Add("RSTE_NoLigne");
            //dtSource.Columns.Add("RSTE_dateCreat");
            //dtSource.Columns.Add("RSTE_CreePar");
            //dtSource.Columns.Add("RSTE_NoFacture");
            //dtSource.Columns.Add("NoBonDeCommande");
            //dtSource.Columns.Add("HtBcd");
            //dtSource.Columns.Add("CodeImmeuble");
            //dtSource.Columns.Add("Acheteur");
            //dtSource.Columns.Add("HtCumul");
            //dtSource.Columns.Add("RSTE_HT");
            //dtSource.Columns.Add("RSTE_TauxTVA");
            //dtSource.Columns.Add("RSTE_MtTVA");
            //dtSource.Columns.Add("RSTE_TTC");
            //dtSource.Columns.Add("RSTE_DateFacture");
            //dtSource.Columns.Add("RSTE_DateEcheance");
            //dtSource.Columns.Add("RSTE_CodeFournCleAuto");
            //dtSource.Columns.Add("RSTE_CodeFourn");
            //dtSource.Columns.Add("RSTE_RaisonSocial");
            //dtSource.Columns.Add("RSTE_ModeRgt");
            //dtSource.Columns.Add("RSTE_CT_Num");
            //dtSource.Columns.Add("RSTE_CptChargeDef");
            //dtSource.Columns.Add("RSTE_CptTvaDef");
            //dtSource.Columns.Add("RSTE_Comptabilise");
            //dtSource.Columns.Add("RSTE_DateComptabilise");
            //dtSource.Columns.Add("RSTE_Anomalie");
            //dtSource.Columns.Add("FactNoautofacture");
            //dtSource.Columns.Add("Code1");
            //dtSource.Columns.Add("RSTE_CheminFichierClasse");
            //dtSource.Columns.Add("RSTE_SansNoCommande");

            //dtSource.Columns.Add("STF_Code");
            //dtSource.Columns.Add("RSTE_Commentaire");
            //dtSource.Columns.Add("RSTE_Controlele");
            //dtSource.Columns.Add("NoIntervention");
            //dtSource.Columns.Add("RSTE_ControlePar");
            //dtSource.Columns.Add("NoDevis");
            //dtSource.Columns.Add("NumFicheStandard");




            //ssRSTE.DataSource = dtSource;

            ////ssRSTE.EventManager.AllEventsEnabled = false;

            //if (rsRSTE.Rows.Count > 0)
            //{
            //    foreach (DataRow dr in rsRSTE.Rows)
            //    {
            //        Application.DoEvents();
            //        dtSource.Rows.Add(dr["RSTE_NoAuto"],
            //            dr["RSTE_NoLigne"],
            //            dr["RSTE_dateCreat"],
            //            dr["RSTE_CreePar"],
            //            dr["RSTE_NoFacture"],
            //            dr["NoBonDeCommande"],
            //            dr["HtBcd"],
            //            dr["CodeImmeuble"],
            //            dr["Acheteur"],
            //            dr["HtCumul"],
            //            dr["RSTE_HT"],
            //            dr["RSTE_TauxTVA"],
            //            dr["RSTE_MtTVA"],
            //            dr["RSTE_TTC"],
            //            dr["RSTE_DateFacture"],
            //            dr["RSTE_DateEcheance"],
            //            dr["RSTE_CodeFournCleAuto"],
            //            dr["RSTE_CodeFourn"],
            //            dr["RSTE_RaisonSocial"],
            //            dr["RSTE_ModeRgt"],
            //            dr["RSTE_CT_Num"],
            //            dr["RSTE_CptChargeDef"],
            //            dr["RSTE_CptTvaDef"],
            //            dr["RSTE_Comptabilise"],
            //            dr["RSTE_DateComptabilise"],
            //            dr["RSTE_Anomalie"],
            //            dr["FactNoautofacture"],
            //            dr["Code1"],
            //            dr["RSTE_CheminFichierClasse"],
            //            dr["RSTE_SansNoCommande"],
            //            dr["STF_Code"],
            //            dr["RSTE_Commentaire"],
            //            dr["RSTE_Controlele"], dr["NoIntervention"], dr["RSTE_ControlePar"], dr["NoDevis"], dr["NumFicheStandard"]);
            //    }
            //}
            //        Application.DoEvents();
            //iOldCol = e.Band.SortedColumns[0].Index;

        }

        private void txtRSTE_DateComptabiliseAu_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRSTE_DateComptabiliseAu.Text) && !General.IsDate(txtRSTE_DateComptabiliseAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date inivalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtRSTE_DateComptabiliseAu.Text = "";
                return;
            }
        }

        private void txtNoBonAu_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtNoBonAu.Text) && !General.IsNumeric(txtNoBonAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Valeur inivalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtNoBonAu.Text = "";
                return;
            }
        }

        private void txtRSTE_DateFactureAu_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtRSTE_DateFactureAu.Text) && !General.IsDate(txtRSTE_DateFactureAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date inivalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtRSTE_DateFactureAu.Text = "";
                return;
            }
        }

        private void ssRSTE_AfterRowsDeleted(object sender, EventArgs e)
        {
            fc_CtrlFactFourn();
        }
    }
}

