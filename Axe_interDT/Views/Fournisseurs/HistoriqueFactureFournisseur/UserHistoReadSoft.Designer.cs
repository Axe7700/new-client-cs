namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur
{
    partial class UserHistoReadSoft
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ssRSTE = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.mnuGrid = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.mnuValidation = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuVisu = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuIntervention = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuBonDeCommande = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuDevis = new System.Windows.Forms.ToolStripMenuItem();
            this.mnuAnalytique = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lbl11 = new System.Windows.Forms.Label();
            this.lbl00 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNoBonDe = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeFourn = new iTalk.iTalk_TextBox_Small2();
            this.txtNoFactureDe = new iTalk.iTalk_TextBox_Small2();
            this.txtNoBonAu = new iTalk.iTalk_TextBox_Small2();
            this.txtRSTE_DateFactureDe = new iTalk.iTalk_TextBox_Small2();
            this.txtNodevis = new iTalk.iTalk_TextBox_Small2();
            this.txtRSTE_DateFactureAu = new iTalk.iTalk_TextBox_Small2();
            this.txtNoFactureAu = new iTalk.iTalk_TextBox_Small2();
            this.txtRSTE_DateComptabiliseDe = new iTalk.iTalk_TextBox_Small2();
            this.txtRSTE_DateComptabiliseAu = new iTalk.iTalk_TextBox_Small2();
            this.cmbAcheteur = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.cmdGerant = new System.Windows.Forms.Button();
            this.cmdRechFourn = new System.Windows.Forms.Button();
            this.cmdRechercheDevis = new System.Windows.Forms.Button();
            this.txtRaisonSocial = new iTalk.iTalk_TextBox_Small2();
            this.optAllFact = new System.Windows.Forms.RadioButton();
            this.optAvecBon = new System.Windows.Forms.RadioButton();
            this.optSansBon = new System.Windows.Forms.RadioButton();
            this.cmdAcheteur = new System.Windows.Forms.Button();
            this.cmbSTF_StatutFactFourn = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRSTE_HT2 = new iTalk.iTalk_TextBox_Small2();
            this.label20 = new System.Windows.Forms.Label();
            this.txtRSTE_HT1 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.TotHt = new iTalk.iTalk_TextBox_Small2();
            this.txtTot = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdVisuFactures = new System.Windows.Forms.Button();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.lblGoIntegration = new System.Windows.Forms.LinkLabel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssRSTE)).BeginInit();
            this.mnuGrid.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAcheteur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSTF_StatutFactFourn)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel4.SetColumnSpan(this.tableLayoutPanel1, 2);
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.ssRSTE, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 188F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 911);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ssRSTE
            // 
            this.ssRSTE.ContextMenuStrip = this.mnuGrid;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssRSTE.DisplayLayout.Appearance = appearance1;
            this.ssRSTE.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssRSTE.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssRSTE.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssRSTE.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssRSTE.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssRSTE.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssRSTE.DisplayLayout.LoadStyle = Infragistics.Win.UltraWinGrid.LoadStyle.LoadOnDemand;
            this.ssRSTE.DisplayLayout.MaxColScrollRegions = 1;
            this.ssRSTE.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssRSTE.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssRSTE.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssRSTE.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssRSTE.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssRSTE.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssRSTE.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssRSTE.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssRSTE.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssRSTE.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssRSTE.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssRSTE.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssRSTE.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssRSTE.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssRSTE.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssRSTE.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssRSTE.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssRSTE.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssRSTE.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssRSTE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssRSTE.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssRSTE.Location = new System.Drawing.Point(3, 254);
            this.ssRSTE.Name = "ssRSTE";
            this.ssRSTE.Size = new System.Drawing.Size(1838, 654);
            this.ssRSTE.TabIndex = 526;
            this.ssRSTE.Text = "ultraGrid1";
            this.ssRSTE.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssRSTE_InitializeLayout);
            this.ssRSTE.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssRSTE_InitializeRow);
            this.ssRSTE.AfterRowsDeleted += new System.EventHandler(this.ssRSTE_AfterRowsDeleted);
            this.ssRSTE.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssRSTE_BeforeRowsDeleted);
            this.ssRSTE.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssRSTE_Error);
            this.ssRSTE.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssRSTE_DoubleClickRow);
            // 
            // mnuGrid
            // 
            this.mnuGrid.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.mnuValidation,
            this.mnuVisu,
            this.mnuIntervention,
            this.mnuBonDeCommande,
            this.mnuDevis,
            this.mnuAnalytique});
            this.mnuGrid.Name = "mnuGrid";
            this.mnuGrid.Size = new System.Drawing.Size(197, 136);
            // 
            // mnuValidation
            // 
            this.mnuValidation.Name = "mnuValidation";
            this.mnuValidation.Size = new System.Drawing.Size(196, 22);
            this.mnuValidation.Text = "Controler";
            this.mnuValidation.Click += new System.EventHandler(this.mnuValidation_Click);
            // 
            // mnuVisu
            // 
            this.mnuVisu.Name = "mnuVisu";
            this.mnuVisu.Size = new System.Drawing.Size(196, 22);
            this.mnuVisu.Text = "Visualiser la facture";
            this.mnuVisu.Click += new System.EventHandler(this.mnuVisu_Click);
            // 
            // mnuIntervention
            // 
            this.mnuIntervention.Name = "mnuIntervention";
            this.mnuIntervention.Size = new System.Drawing.Size(196, 22);
            this.mnuIntervention.Text = "=> Intevention";
            this.mnuIntervention.Click += new System.EventHandler(this.mnuIntervention_Click);
            // 
            // mnuBonDeCommande
            // 
            this.mnuBonDeCommande.Name = "mnuBonDeCommande";
            this.mnuBonDeCommande.Size = new System.Drawing.Size(196, 22);
            this.mnuBonDeCommande.Text = "=> Bon de Commande";
            this.mnuBonDeCommande.Click += new System.EventHandler(this.mnuBonDeCommande_Click);
            // 
            // mnuDevis
            // 
            this.mnuDevis.Name = "mnuDevis";
            this.mnuDevis.Size = new System.Drawing.Size(196, 22);
            this.mnuDevis.Text = "=> Devis";
            this.mnuDevis.Click += new System.EventHandler(this.mnuDevis_Click);
            // 
            // mnuAnalytique
            // 
            this.mnuAnalytique.Name = "mnuAnalytique";
            this.mnuAnalytique.Size = new System.Drawing.Size(196, 22);
            this.mnuAnalytique.Text = "=> Analytique";
            this.mnuAnalytique.Click += new System.EventHandler(this.mnuAnalytique_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 8;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.lbl11, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.lbl00, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lbl10, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label6, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.label8, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.label7, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label9, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label11, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.label13, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtNoBonDe, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCodeImmeuble, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCode1, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtCodeFourn, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtNoFactureDe, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtNoBonAu, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_DateFactureDe, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNodevis, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_DateFactureAu, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNoFactureAu, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_DateComptabiliseDe, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_DateComptabiliseAu, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.cmbAcheteur, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheImmeuble, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdGerant, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.cmdRechFourn, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheDevis, 6, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtRaisonSocial, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.optAllFact, 7, 3);
            this.tableLayoutPanel2.Controls.Add(this.optAvecBon, 7, 4);
            this.tableLayoutPanel2.Controls.Add(this.optSansBon, 7, 5);
            this.tableLayoutPanel2.Controls.Add(this.cmdAcheteur, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmbSTF_StatutFactFourn, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.label12, 6, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_HT2, 7, 2);
            this.tableLayoutPanel2.Controls.Add(this.label20, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtRSTE_HT1, 5, 2);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 6;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1838, 182);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.Location = new System.Drawing.Point(3, 150);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(129, 19);
            this.lbl11.TabIndex = 398;
            this.lbl11.Text = "Comptabilisée du";
            // 
            // lbl00
            // 
            this.lbl00.AutoSize = true;
            this.lbl00.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl00.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl00.Location = new System.Drawing.Point(3, 0);
            this.lbl00.Name = "lbl00";
            this.lbl00.Size = new System.Drawing.Size(154, 30);
            this.lbl00.TabIndex = 384;
            this.lbl00.Text = "N°bon de commande";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 30);
            this.label1.TabIndex = 385;
            this.label1.Text = "Immeuble";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Gerant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 19);
            this.label3.TabIndex = 387;
            this.label3.Text = "Fournisseur";
            // 
            // lbl10
            // 
            this.lbl10.AutoSize = true;
            this.lbl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl10.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl10.Location = new System.Drawing.Point(3, 120);
            this.lbl10.Name = "lbl10";
            this.lbl10.Size = new System.Drawing.Size(154, 30);
            this.lbl10.TabIndex = 388;
            this.lbl10.Tag = "0";
            this.lbl10.Text = "N° facture";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(507, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 30);
            this.label5.TabIndex = 389;
            this.label5.Text = "Au ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(507, 150);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(30, 32);
            this.label6.TabIndex = 390;
            this.label6.Text = "Au ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(507, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(30, 30);
            this.label8.TabIndex = 392;
            this.label8.Text = "Au ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(887, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(140, 30);
            this.label7.TabIndex = 391;
            this.label7.Text = "Date de facture du";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(887, 30);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(140, 30);
            this.label9.TabIndex = 393;
            this.label9.Text = "Acheteur";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(887, 90);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 30);
            this.label11.TabIndex = 395;
            this.label11.Text = "Statut";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1377, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(111, 30);
            this.label13.TabIndex = 397;
            this.label13.Text = "Au";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(887, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(140, 30);
            this.label10.TabIndex = 394;
            this.label10.Text = "No Devis";
            // 
            // txtNoBonDe
            // 
            this.txtNoBonDe.AccAcceptNumbersOnly = false;
            this.txtNoBonDe.AccAllowComma = false;
            this.txtNoBonDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBonDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBonDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBonDe.AccHidenValue = "";
            this.txtNoBonDe.AccNotAllowedChars = null;
            this.txtNoBonDe.AccReadOnly = false;
            this.txtNoBonDe.AccReadOnlyAllowDelete = false;
            this.txtNoBonDe.AccRequired = false;
            this.txtNoBonDe.BackColor = System.Drawing.Color.White;
            this.txtNoBonDe.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBonDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBonDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBonDe.ForeColor = System.Drawing.Color.Black;
            this.txtNoBonDe.Location = new System.Drawing.Point(162, 2);
            this.txtNoBonDe.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBonDe.MaxLength = 32767;
            this.txtNoBonDe.Multiline = false;
            this.txtNoBonDe.Name = "txtNoBonDe";
            this.txtNoBonDe.ReadOnly = false;
            this.txtNoBonDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBonDe.Size = new System.Drawing.Size(340, 27);
            this.txtNoBonDe.TabIndex = 502;
            this.txtNoBonDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBonDe.UseSystemPasswordChar = false;
            this.txtNoBonDe.Leave += new System.EventHandler(this.txtNoBonDe_Leave);
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(162, 32);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(340, 27);
            this.txtCodeImmeuble.TabIndex = 504;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(162, 62);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 32767;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = false;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(340, 27);
            this.txtCode1.TabIndex = 505;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            // 
            // txtCodeFourn
            // 
            this.txtCodeFourn.AccAcceptNumbersOnly = false;
            this.txtCodeFourn.AccAllowComma = false;
            this.txtCodeFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFourn.AccHidenValue = "";
            this.txtCodeFourn.AccNotAllowedChars = null;
            this.txtCodeFourn.AccReadOnly = false;
            this.txtCodeFourn.AccReadOnlyAllowDelete = false;
            this.txtCodeFourn.AccRequired = false;
            this.txtCodeFourn.BackColor = System.Drawing.Color.White;
            this.txtCodeFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFourn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeFourn.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeFourn.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFourn.Location = new System.Drawing.Point(162, 92);
            this.txtCodeFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFourn.MaxLength = 32767;
            this.txtCodeFourn.Multiline = false;
            this.txtCodeFourn.Name = "txtCodeFourn";
            this.txtCodeFourn.ReadOnly = false;
            this.txtCodeFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFourn.Size = new System.Drawing.Size(340, 27);
            this.txtCodeFourn.TabIndex = 506;
            this.txtCodeFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFourn.UseSystemPasswordChar = false;
            // 
            // txtNoFactureDe
            // 
            this.txtNoFactureDe.AccAcceptNumbersOnly = false;
            this.txtNoFactureDe.AccAllowComma = false;
            this.txtNoFactureDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoFactureDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoFactureDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoFactureDe.AccHidenValue = "";
            this.txtNoFactureDe.AccNotAllowedChars = null;
            this.txtNoFactureDe.AccReadOnly = false;
            this.txtNoFactureDe.AccReadOnlyAllowDelete = false;
            this.txtNoFactureDe.AccRequired = false;
            this.txtNoFactureDe.BackColor = System.Drawing.Color.White;
            this.txtNoFactureDe.CustomBackColor = System.Drawing.Color.White;
            this.txtNoFactureDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoFactureDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoFactureDe.ForeColor = System.Drawing.Color.Black;
            this.txtNoFactureDe.Location = new System.Drawing.Point(162, 122);
            this.txtNoFactureDe.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoFactureDe.MaxLength = 32767;
            this.txtNoFactureDe.Multiline = false;
            this.txtNoFactureDe.Name = "txtNoFactureDe";
            this.txtNoFactureDe.ReadOnly = false;
            this.txtNoFactureDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoFactureDe.Size = new System.Drawing.Size(340, 27);
            this.txtNoFactureDe.TabIndex = 508;
            this.txtNoFactureDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoFactureDe.UseSystemPasswordChar = false;
            this.txtNoFactureDe.Leave += new System.EventHandler(this.txtNoFactureDe_Leave);
            // 
            // txtNoBonAu
            // 
            this.txtNoBonAu.AccAcceptNumbersOnly = false;
            this.txtNoBonAu.AccAllowComma = false;
            this.txtNoBonAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBonAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBonAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBonAu.AccHidenValue = "";
            this.txtNoBonAu.AccNotAllowedChars = null;
            this.txtNoBonAu.AccReadOnly = false;
            this.txtNoBonAu.AccReadOnlyAllowDelete = false;
            this.txtNoBonAu.AccRequired = false;
            this.txtNoBonAu.BackColor = System.Drawing.Color.White;
            this.txtNoBonAu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBonAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBonAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBonAu.ForeColor = System.Drawing.Color.Black;
            this.txtNoBonAu.Location = new System.Drawing.Point(542, 2);
            this.txtNoBonAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBonAu.MaxLength = 32767;
            this.txtNoBonAu.Multiline = false;
            this.txtNoBonAu.Name = "txtNoBonAu";
            this.txtNoBonAu.ReadOnly = false;
            this.txtNoBonAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBonAu.Size = new System.Drawing.Size(340, 27);
            this.txtNoBonAu.TabIndex = 503;
            this.txtNoBonAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBonAu.UseSystemPasswordChar = false;
            this.txtNoBonAu.Leave += new System.EventHandler(this.txtNoBonAu_Leave);
            // 
            // txtRSTE_DateFactureDe
            // 
            this.txtRSTE_DateFactureDe.AccAcceptNumbersOnly = false;
            this.txtRSTE_DateFactureDe.AccAllowComma = false;
            this.txtRSTE_DateFactureDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_DateFactureDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_DateFactureDe.AccHidenValue = "";
            this.txtRSTE_DateFactureDe.AccNotAllowedChars = null;
            this.txtRSTE_DateFactureDe.AccReadOnly = false;
            this.txtRSTE_DateFactureDe.AccReadOnlyAllowDelete = false;
            this.txtRSTE_DateFactureDe.AccRequired = false;
            this.txtRSTE_DateFactureDe.BackColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureDe.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_DateFactureDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_DateFactureDe.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_DateFactureDe.Location = new System.Drawing.Point(1032, 2);
            this.txtRSTE_DateFactureDe.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_DateFactureDe.MaxLength = 32767;
            this.txtRSTE_DateFactureDe.Multiline = false;
            this.txtRSTE_DateFactureDe.Name = "txtRSTE_DateFactureDe";
            this.txtRSTE_DateFactureDe.ReadOnly = false;
            this.txtRSTE_DateFactureDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_DateFactureDe.Size = new System.Drawing.Size(340, 27);
            this.txtRSTE_DateFactureDe.TabIndex = 512;
            this.txtRSTE_DateFactureDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_DateFactureDe.UseSystemPasswordChar = false;
            this.txtRSTE_DateFactureDe.Leave += new System.EventHandler(this.txtRSTE_DateFactureDe_Leave);
            // 
            // txtNodevis
            // 
            this.txtNodevis.AccAcceptNumbersOnly = false;
            this.txtNodevis.AccAllowComma = false;
            this.txtNodevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNodevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNodevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNodevis.AccHidenValue = "";
            this.txtNodevis.AccNotAllowedChars = null;
            this.txtNodevis.AccReadOnly = false;
            this.txtNodevis.AccReadOnlyAllowDelete = false;
            this.txtNodevis.AccRequired = false;
            this.txtNodevis.BackColor = System.Drawing.Color.White;
            this.txtNodevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNodevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNodevis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNodevis.ForeColor = System.Drawing.Color.Black;
            this.txtNodevis.Location = new System.Drawing.Point(1032, 122);
            this.txtNodevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNodevis.MaxLength = 32767;
            this.txtNodevis.Multiline = false;
            this.txtNodevis.Name = "txtNodevis";
            this.txtNodevis.ReadOnly = false;
            this.txtNodevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNodevis.Size = new System.Drawing.Size(340, 27);
            this.txtNodevis.TabIndex = 517;
            this.txtNodevis.TabStop = false;
            this.txtNodevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNodevis.UseSystemPasswordChar = false;
            // 
            // txtRSTE_DateFactureAu
            // 
            this.txtRSTE_DateFactureAu.AccAcceptNumbersOnly = false;
            this.txtRSTE_DateFactureAu.AccAllowComma = false;
            this.txtRSTE_DateFactureAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_DateFactureAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_DateFactureAu.AccHidenValue = "";
            this.txtRSTE_DateFactureAu.AccNotAllowedChars = null;
            this.txtRSTE_DateFactureAu.AccReadOnly = false;
            this.txtRSTE_DateFactureAu.AccReadOnlyAllowDelete = false;
            this.txtRSTE_DateFactureAu.AccRequired = false;
            this.txtRSTE_DateFactureAu.BackColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureAu.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_DateFactureAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_DateFactureAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_DateFactureAu.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_DateFactureAu.Location = new System.Drawing.Point(1493, 2);
            this.txtRSTE_DateFactureAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_DateFactureAu.MaxLength = 32767;
            this.txtRSTE_DateFactureAu.Multiline = false;
            this.txtRSTE_DateFactureAu.Name = "txtRSTE_DateFactureAu";
            this.txtRSTE_DateFactureAu.ReadOnly = false;
            this.txtRSTE_DateFactureAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_DateFactureAu.Size = new System.Drawing.Size(343, 27);
            this.txtRSTE_DateFactureAu.TabIndex = 513;
            this.txtRSTE_DateFactureAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_DateFactureAu.UseSystemPasswordChar = false;
            this.txtRSTE_DateFactureAu.Leave += new System.EventHandler(this.txtRSTE_DateFactureAu_Leave);
            // 
            // txtNoFactureAu
            // 
            this.txtNoFactureAu.AccAcceptNumbersOnly = false;
            this.txtNoFactureAu.AccAllowComma = false;
            this.txtNoFactureAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoFactureAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoFactureAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoFactureAu.AccHidenValue = "";
            this.txtNoFactureAu.AccNotAllowedChars = null;
            this.txtNoFactureAu.AccReadOnly = false;
            this.txtNoFactureAu.AccReadOnlyAllowDelete = false;
            this.txtNoFactureAu.AccRequired = false;
            this.txtNoFactureAu.BackColor = System.Drawing.Color.White;
            this.txtNoFactureAu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoFactureAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoFactureAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoFactureAu.ForeColor = System.Drawing.Color.Black;
            this.txtNoFactureAu.Location = new System.Drawing.Point(542, 122);
            this.txtNoFactureAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoFactureAu.MaxLength = 32767;
            this.txtNoFactureAu.Multiline = false;
            this.txtNoFactureAu.Name = "txtNoFactureAu";
            this.txtNoFactureAu.ReadOnly = false;
            this.txtNoFactureAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoFactureAu.Size = new System.Drawing.Size(340, 27);
            this.txtNoFactureAu.TabIndex = 509;
            this.txtNoFactureAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoFactureAu.UseSystemPasswordChar = false;
            // 
            // txtRSTE_DateComptabiliseDe
            // 
            this.txtRSTE_DateComptabiliseDe.AccAcceptNumbersOnly = false;
            this.txtRSTE_DateComptabiliseDe.AccAllowComma = false;
            this.txtRSTE_DateComptabiliseDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_DateComptabiliseDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_DateComptabiliseDe.AccHidenValue = "";
            this.txtRSTE_DateComptabiliseDe.AccNotAllowedChars = null;
            this.txtRSTE_DateComptabiliseDe.AccReadOnly = false;
            this.txtRSTE_DateComptabiliseDe.AccReadOnlyAllowDelete = false;
            this.txtRSTE_DateComptabiliseDe.AccRequired = false;
            this.txtRSTE_DateComptabiliseDe.BackColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseDe.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_DateComptabiliseDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_DateComptabiliseDe.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_DateComptabiliseDe.Location = new System.Drawing.Point(162, 152);
            this.txtRSTE_DateComptabiliseDe.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_DateComptabiliseDe.MaxLength = 32767;
            this.txtRSTE_DateComptabiliseDe.Multiline = false;
            this.txtRSTE_DateComptabiliseDe.Name = "txtRSTE_DateComptabiliseDe";
            this.txtRSTE_DateComptabiliseDe.ReadOnly = false;
            this.txtRSTE_DateComptabiliseDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_DateComptabiliseDe.Size = new System.Drawing.Size(340, 27);
            this.txtRSTE_DateComptabiliseDe.TabIndex = 510;
            this.txtRSTE_DateComptabiliseDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_DateComptabiliseDe.UseSystemPasswordChar = false;
            this.txtRSTE_DateComptabiliseDe.Leave += new System.EventHandler(this.txtRSTE_DateComptabiliseDe_Leave);
            // 
            // txtRSTE_DateComptabiliseAu
            // 
            this.txtRSTE_DateComptabiliseAu.AccAcceptNumbersOnly = false;
            this.txtRSTE_DateComptabiliseAu.AccAllowComma = false;
            this.txtRSTE_DateComptabiliseAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_DateComptabiliseAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_DateComptabiliseAu.AccHidenValue = "";
            this.txtRSTE_DateComptabiliseAu.AccNotAllowedChars = null;
            this.txtRSTE_DateComptabiliseAu.AccReadOnly = false;
            this.txtRSTE_DateComptabiliseAu.AccReadOnlyAllowDelete = false;
            this.txtRSTE_DateComptabiliseAu.AccRequired = false;
            this.txtRSTE_DateComptabiliseAu.BackColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseAu.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_DateComptabiliseAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_DateComptabiliseAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_DateComptabiliseAu.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_DateComptabiliseAu.Location = new System.Drawing.Point(542, 152);
            this.txtRSTE_DateComptabiliseAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_DateComptabiliseAu.MaxLength = 32767;
            this.txtRSTE_DateComptabiliseAu.Multiline = false;
            this.txtRSTE_DateComptabiliseAu.Name = "txtRSTE_DateComptabiliseAu";
            this.txtRSTE_DateComptabiliseAu.ReadOnly = false;
            this.txtRSTE_DateComptabiliseAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_DateComptabiliseAu.Size = new System.Drawing.Size(340, 27);
            this.txtRSTE_DateComptabiliseAu.TabIndex = 511;
            this.txtRSTE_DateComptabiliseAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_DateComptabiliseAu.UseSystemPasswordChar = false;
            this.txtRSTE_DateComptabiliseAu.Leave += new System.EventHandler(this.txtRSTE_DateComptabiliseAu_Leave);
            // 
            // cmbAcheteur
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbAcheteur.DisplayLayout.Appearance = appearance13;
            this.cmbAcheteur.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbAcheteur.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbAcheteur.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbAcheteur.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbAcheteur.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbAcheteur.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbAcheteur.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbAcheteur.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbAcheteur.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbAcheteur.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbAcheteur.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbAcheteur.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbAcheteur.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbAcheteur.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbAcheteur.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbAcheteur.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbAcheteur.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbAcheteur.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbAcheteur.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbAcheteur.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbAcheteur.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbAcheteur.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbAcheteur.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbAcheteur.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbAcheteur.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbAcheteur.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbAcheteur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbAcheteur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbAcheteur.Location = new System.Drawing.Point(1033, 33);
            this.cmbAcheteur.Name = "cmbAcheteur";
            this.cmbAcheteur.Size = new System.Drawing.Size(338, 27);
            this.cmbAcheteur.TabIndex = 514;
            this.cmbAcheteur.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbAcheteur_BeforeDropDown);
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(507, 33);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheImmeuble.TabIndex = 523;
            this.cmdRechercheImmeuble.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheImmeuble, "Recherche de l\'immeuble");
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // cmdGerant
            // 
            this.cmdGerant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdGerant.FlatAppearance.BorderSize = 0;
            this.cmdGerant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGerant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdGerant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdGerant.Location = new System.Drawing.Point(507, 63);
            this.cmdGerant.Name = "cmdGerant";
            this.cmdGerant.Size = new System.Drawing.Size(24, 19);
            this.cmdGerant.TabIndex = 524;
            this.cmdGerant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdGerant, "Recherche d\'un fournisseur");
            this.cmdGerant.UseVisualStyleBackColor = false;
            this.cmdGerant.Click += new System.EventHandler(this.cmdGerant_Click);
            // 
            // cmdRechFourn
            // 
            this.cmdRechFourn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechFourn.FlatAppearance.BorderSize = 0;
            this.cmdRechFourn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechFourn.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechFourn.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechFourn.Location = new System.Drawing.Point(507, 93);
            this.cmdRechFourn.Name = "cmdRechFourn";
            this.cmdRechFourn.Size = new System.Drawing.Size(24, 19);
            this.cmdRechFourn.TabIndex = 525;
            this.cmdRechFourn.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechFourn, "Recherche d\'un fournisseur");
            this.cmdRechFourn.UseVisualStyleBackColor = false;
            this.cmdRechFourn.Click += new System.EventHandler(this.cmdRechFourn_Click);
            // 
            // cmdRechercheDevis
            // 
            this.cmdRechercheDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheDevis.FlatAppearance.BorderSize = 0;
            this.cmdRechercheDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheDevis.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheDevis.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheDevis.Location = new System.Drawing.Point(1377, 123);
            this.cmdRechercheDevis.Name = "cmdRechercheDevis";
            this.cmdRechercheDevis.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheDevis.TabIndex = 5201;
            this.cmdRechercheDevis.Tag = "";
            this.cmdRechercheDevis.UseVisualStyleBackColor = false;
            this.cmdRechercheDevis.Click += new System.EventHandler(this.cmdRechercheDevis_Click);
            // 
            // txtRaisonSocial
            // 
            this.txtRaisonSocial.AccAcceptNumbersOnly = false;
            this.txtRaisonSocial.AccAllowComma = false;
            this.txtRaisonSocial.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRaisonSocial.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRaisonSocial.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRaisonSocial.AccHidenValue = "";
            this.txtRaisonSocial.AccNotAllowedChars = null;
            this.txtRaisonSocial.AccReadOnly = false;
            this.txtRaisonSocial.AccReadOnlyAllowDelete = false;
            this.txtRaisonSocial.AccRequired = false;
            this.txtRaisonSocial.BackColor = System.Drawing.Color.White;
            this.txtRaisonSocial.CustomBackColor = System.Drawing.Color.White;
            this.txtRaisonSocial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRaisonSocial.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRaisonSocial.ForeColor = System.Drawing.Color.Black;
            this.txtRaisonSocial.Location = new System.Drawing.Point(542, 92);
            this.txtRaisonSocial.Margin = new System.Windows.Forms.Padding(2);
            this.txtRaisonSocial.MaxLength = 32767;
            this.txtRaisonSocial.Multiline = false;
            this.txtRaisonSocial.Name = "txtRaisonSocial";
            this.txtRaisonSocial.ReadOnly = false;
            this.txtRaisonSocial.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.txtRaisonSocial.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRaisonSocial.Size = new System.Drawing.Size(340, 27);
            this.txtRaisonSocial.TabIndex = 507;
            this.txtRaisonSocial.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRaisonSocial.UseSystemPasswordChar = false;
            // 
            // optAllFact
            // 
            this.optAllFact.AutoSize = true;
            this.optAllFact.Checked = true;
            this.optAllFact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optAllFact.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optAllFact.Location = new System.Drawing.Point(1494, 93);
            this.optAllFact.Name = "optAllFact";
            this.optAllFact.Size = new System.Drawing.Size(341, 24);
            this.optAllFact.TabIndex = 538;
            this.optAllFact.TabStop = true;
            this.optAllFact.Text = "Toutes les factures";
            this.optAllFact.UseVisualStyleBackColor = true;
            // 
            // optAvecBon
            // 
            this.optAvecBon.AutoSize = true;
            this.optAvecBon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optAvecBon.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optAvecBon.Location = new System.Drawing.Point(1494, 123);
            this.optAvecBon.Name = "optAvecBon";
            this.optAvecBon.Size = new System.Drawing.Size(341, 24);
            this.optAvecBon.TabIndex = 539;
            this.optAvecBon.Text = "Factures avec bon de commande";
            this.optAvecBon.UseVisualStyleBackColor = true;
            // 
            // optSansBon
            // 
            this.optSansBon.AutoSize = true;
            this.optSansBon.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optSansBon.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optSansBon.Location = new System.Drawing.Point(1494, 153);
            this.optSansBon.Name = "optSansBon";
            this.optSansBon.Size = new System.Drawing.Size(341, 26);
            this.optSansBon.TabIndex = 540;
            this.optSansBon.Text = "Factures sans bon de commande";
            this.optSansBon.UseVisualStyleBackColor = true;
            // 
            // cmdAcheteur
            // 
            this.cmdAcheteur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAcheteur.FlatAppearance.BorderSize = 0;
            this.cmdAcheteur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAcheteur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdAcheteur.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdAcheteur.Location = new System.Drawing.Point(1377, 33);
            this.cmdAcheteur.Name = "cmdAcheteur";
            this.cmdAcheteur.Size = new System.Drawing.Size(24, 19);
            this.cmdAcheteur.TabIndex = 5261;
            this.cmdAcheteur.Tag = "";
            this.toolTip1.SetToolTip(this.cmdAcheteur, "Recherche d\'un gérant");
            this.cmdAcheteur.UseVisualStyleBackColor = false;
            this.cmdAcheteur.Click += new System.EventHandler(this.cmdAcheteur_Click);
            // 
            // cmbSTF_StatutFactFourn
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Appearance = appearance25;
            this.cmbSTF_StatutFactFourn.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbSTF_StatutFactFourn.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbSTF_StatutFactFourn.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbSTF_StatutFactFourn.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cmbSTF_StatutFactFourn.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbSTF_StatutFactFourn.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cmbSTF_StatutFactFourn.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbSTF_StatutFactFourn.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.CellAppearance = appearance32;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.RowAppearance = appearance35;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbSTF_StatutFactFourn.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cmbSTF_StatutFactFourn.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbSTF_StatutFactFourn.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbSTF_StatutFactFourn.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbSTF_StatutFactFourn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbSTF_StatutFactFourn.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbSTF_StatutFactFourn.Location = new System.Drawing.Point(1033, 93);
            this.cmbSTF_StatutFactFourn.Name = "cmbSTF_StatutFactFourn";
            this.cmbSTF_StatutFactFourn.Size = new System.Drawing.Size(338, 27);
            this.cmbSTF_StatutFactFourn.TabIndex = 515;
            this.cmbSTF_StatutFactFourn.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbSTF_StatutFactFourn_BeforeDropDown);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1377, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(111, 19);
            this.label12.TabIndex = 396;
            this.label12.Text = "Montant HT >=";
            // 
            // txtRSTE_HT2
            // 
            this.txtRSTE_HT2.AccAcceptNumbersOnly = false;
            this.txtRSTE_HT2.AccAllowComma = false;
            this.txtRSTE_HT2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_HT2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_HT2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_HT2.AccHidenValue = "";
            this.txtRSTE_HT2.AccNotAllowedChars = null;
            this.txtRSTE_HT2.AccReadOnly = false;
            this.txtRSTE_HT2.AccReadOnlyAllowDelete = false;
            this.txtRSTE_HT2.AccRequired = false;
            this.txtRSTE_HT2.BackColor = System.Drawing.Color.White;
            this.txtRSTE_HT2.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_HT2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_HT2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_HT2.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_HT2.Location = new System.Drawing.Point(1493, 62);
            this.txtRSTE_HT2.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_HT2.MaxLength = 32767;
            this.txtRSTE_HT2.Multiline = false;
            this.txtRSTE_HT2.Name = "txtRSTE_HT2";
            this.txtRSTE_HT2.ReadOnly = false;
            this.txtRSTE_HT2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_HT2.Size = new System.Drawing.Size(343, 27);
            this.txtRSTE_HT2.TabIndex = 516;
            this.txtRSTE_HT2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_HT2.UseSystemPasswordChar = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(887, 60);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(140, 30);
            this.label20.TabIndex = 5262;
            this.label20.Text = "Montant HT <=";
            // 
            // txtRSTE_HT1
            // 
            this.txtRSTE_HT1.AccAcceptNumbersOnly = false;
            this.txtRSTE_HT1.AccAllowComma = false;
            this.txtRSTE_HT1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRSTE_HT1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRSTE_HT1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_HT1.AccHidenValue = "";
            this.txtRSTE_HT1.AccNotAllowedChars = null;
            this.txtRSTE_HT1.AccReadOnly = false;
            this.txtRSTE_HT1.AccReadOnlyAllowDelete = false;
            this.txtRSTE_HT1.AccRequired = false;
            this.txtRSTE_HT1.BackColor = System.Drawing.Color.White;
            this.txtRSTE_HT1.CustomBackColor = System.Drawing.Color.White;
            this.txtRSTE_HT1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRSTE_HT1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRSTE_HT1.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_HT1.Location = new System.Drawing.Point(1032, 62);
            this.txtRSTE_HT1.Margin = new System.Windows.Forms.Padding(2);
            this.txtRSTE_HT1.MaxLength = 32767;
            this.txtRSTE_HT1.Multiline = false;
            this.txtRSTE_HT1.Name = "txtRSTE_HT1";
            this.txtRSTE_HT1.ReadOnly = false;
            this.txtRSTE_HT1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRSTE_HT1.Size = new System.Drawing.Size(340, 27);
            this.txtRSTE_HT1.TabIndex = 5263;
            this.txtRSTE_HT1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRSTE_HT1.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 8;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.TotHt, 7, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTot, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.label18, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.label16, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label33, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel4, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label14, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label15, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label17, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.label19, 6, 1);
            this.tableLayoutPanel3.Controls.Add(this.panel5, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 4, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 191);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1838, 57);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // TotHt
            // 
            this.TotHt.AccAcceptNumbersOnly = false;
            this.TotHt.AccAllowComma = false;
            this.TotHt.AccBackgroundColor = System.Drawing.Color.White;
            this.TotHt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TotHt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TotHt.AccHidenValue = "";
            this.TotHt.AccNotAllowedChars = null;
            this.TotHt.AccReadOnly = false;
            this.TotHt.AccReadOnlyAllowDelete = false;
            this.TotHt.AccRequired = false;
            this.TotHt.BackColor = System.Drawing.Color.White;
            this.TotHt.CustomBackColor = System.Drawing.Color.White;
            this.TotHt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TotHt.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.TotHt.ForeColor = System.Drawing.Color.Black;
            this.TotHt.Location = new System.Drawing.Point(1274, 30);
            this.TotHt.Margin = new System.Windows.Forms.Padding(2);
            this.TotHt.MaxLength = 32767;
            this.TotHt.Multiline = false;
            this.TotHt.Name = "TotHt";
            this.TotHt.ReadOnly = false;
            this.TotHt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TotHt.Size = new System.Drawing.Size(562, 27);
            this.TotHt.TabIndex = 503;
            this.TotHt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TotHt.UseSystemPasswordChar = false;
            // 
            // txtTot
            // 
            this.txtTot.AccAcceptNumbersOnly = false;
            this.txtTot.AccAllowComma = false;
            this.txtTot.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTot.AccHidenValue = "";
            this.txtTot.AccNotAllowedChars = null;
            this.txtTot.AccReadOnly = false;
            this.txtTot.AccReadOnlyAllowDelete = false;
            this.txtTot.AccRequired = false;
            this.txtTot.BackColor = System.Drawing.Color.White;
            this.txtTot.CustomBackColor = System.Drawing.Color.White;
            this.txtTot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTot.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTot.ForeColor = System.Drawing.Color.Black;
            this.txtTot.Location = new System.Drawing.Point(1274, 2);
            this.txtTot.Margin = new System.Windows.Forms.Padding(2);
            this.txtTot.MaxLength = 32767;
            this.txtTot.Multiline = false;
            this.txtTot.Name = "txtTot";
            this.txtTot.ReadOnly = false;
            this.txtTot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTot.Size = new System.Drawing.Size(562, 27);
            this.txtTot.TabIndex = 502;
            this.txtTot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTot.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Right;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(1222, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 28);
            this.label18.TabIndex = 390;
            this.label18.Text = "Total ";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(621, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 28);
            this.label16.TabIndex = 388;
            this.label16.Text = "ANOMALIE";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(369, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(177, 28);
            this.label4.TabIndex = 385;
            this.label4.Text = "ANOMALIE REGULARISEE";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(72, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(222, 28);
            this.label33.TabIndex = 384;
            this.label33.Text = "EN ATTENTE";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.PaleGreen;
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(303, 34);
            this.panel4.Margin = new System.Windows.Forms.Padding(6);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(57, 17);
            this.panel4.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.LemonChiffon;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(303, 6);
            this.panel3.Margin = new System.Windows.Forms.Padding(6);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(57, 16);
            this.panel3.TabIndex = 1;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.HotPink;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.ForeColor = System.Drawing.Color.HotPink;
            this.panel1.Location = new System.Drawing.Point(6, 6);
            this.panel1.Margin = new System.Windows.Forms.Padding(6);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(57, 16);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.DimGray;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(6, 34);
            this.panel2.Margin = new System.Windows.Forms.Padding(6);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(57, 17);
            this.panel2.TabIndex = 1;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(72, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(222, 29);
            this.label14.TabIndex = 386;
            this.label14.Text = "FACTURE NON COMPTABILISEE";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(369, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(177, 29);
            this.label15.TabIndex = 387;
            this.label15.Text = "A VALIDER";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(621, 28);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 29);
            this.label17.TabIndex = 389;
            this.label17.Text = "Validée";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Right;
            this.label19.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(1139, 28);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(130, 29);
            this.label19.TabIndex = 391;
            this.label19.Text = "Montant Total HT";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.PaleVioletRed;
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(555, 6);
            this.panel5.Margin = new System.Windows.Forms.Padding(6);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(57, 16);
            this.panel5.TabIndex = 1;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(555, 34);
            this.panel6.Margin = new System.Windows.Forms.Padding(6);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(57, 17);
            this.panel6.TabIndex = 392;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdVisuFactures);
            this.flowLayoutPanel1.Controls.Add(this.cmdExportExcel);
            this.flowLayoutPanel1.Controls.Add(this.cmdMail);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1569, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(281, 40);
            this.flowLayoutPanel1.TabIndex = 518;
            // 
            // cmdVisuFactures
            // 
            this.cmdVisuFactures.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdVisuFactures.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdVisuFactures.FlatAppearance.BorderSize = 0;
            this.cmdVisuFactures.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisuFactures.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdVisuFactures.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdVisuFactures.Location = new System.Drawing.Point(2, 2);
            this.cmdVisuFactures.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisuFactures.Name = "cmdVisuFactures";
            this.cmdVisuFactures.Size = new System.Drawing.Size(60, 35);
            this.cmdVisuFactures.TabIndex = 2;
            this.cmdVisuFactures.Tag = "";
            this.cmdVisuFactures.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdVisuFactures, "Rechercher");
            this.cmdVisuFactures.UseVisualStyleBackColor = false;
            this.cmdVisuFactures.Click += new System.EventHandler(this.cmdVisuFactures_Click);
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExportExcel.Location = new System.Drawing.Point(66, 2);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(60, 35);
            this.cmdExportExcel.TabIndex = 522;
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExportExcel, "Exporter La selection");
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // cmdMail
            // 
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMail.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Image = global::Axe_interDT.Properties.Resources.new_post_24;
            this.cmdMail.Location = new System.Drawing.Point(130, 2);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.cmdMail.Size = new System.Drawing.Size(60, 35);
            this.cmdMail.TabIndex = 521;
            this.cmdMail.UseVisualStyleBackColor = false;
            this.cmdMail.Click += new System.EventHandler(this.cmdMail_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClean.Location = new System.Drawing.Point(194, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(85, 35);
            this.cmdClean.TabIndex = 523;
            this.cmdClean.Text = "   Clean";
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // lblGoIntegration
            // 
            this.lblGoIntegration.AutoSize = true;
            this.lblGoIntegration.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblGoIntegration.Location = new System.Drawing.Point(3, 10);
            this.lblGoIntegration.Margin = new System.Windows.Forms.Padding(3, 10, 3, 0);
            this.lblGoIntegration.Name = "lblGoIntegration";
            this.lblGoIntegration.Size = new System.Drawing.Size(179, 19);
            this.lblGoIntegration.TabIndex = 2;
            this.lblGoIntegration.TabStop = true;
            this.lblGoIntegration.Text = "Intégration des factures ";
            this.lblGoIntegration.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoIntegration_LinkClicked);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.lblGoIntegration, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel4.TabIndex = 519;
            // 
            // UserHistoReadSoft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "UserHistoReadSoft";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Historiques Factures Fournisseurs";
            this.VisibleChanged += new System.EventHandler(this.UserHistoReadSoft_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssRSTE)).EndInit();
            this.mnuGrid.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbAcheteur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbSTF_StatutFactFourn)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdVisuFactures;
        public System.Windows.Forms.Button cmdMail;
        public System.Windows.Forms.Button cmdExportExcel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label lbl11;
        public System.Windows.Forms.Label lbl00;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lbl10;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtNoBonDe;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
        public iTalk.iTalk_TextBox_Small2 txtCodeFourn;
        public iTalk.iTalk_TextBox_Small2 txtNoFactureDe;
        public iTalk.iTalk_TextBox_Small2 txtNoBonAu;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_DateFactureDe;
        public iTalk.iTalk_TextBox_Small2 txtNodevis;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_HT2;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_DateFactureAu;
        public iTalk.iTalk_TextBox_Small2 txtNoFactureAu;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_DateComptabiliseDe;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_DateComptabiliseAu;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbAcheteur;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbSTF_StatutFactFourn;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public System.Windows.Forms.Button cmdGerant;
        public System.Windows.Forms.Button cmdRechFourn;
        public System.Windows.Forms.Button cmdRechercheDevis;
        public iTalk.iTalk_TextBox_Small2 txtRaisonSocial;
        private System.Windows.Forms.LinkLabel lblGoIntegration;
        public System.Windows.Forms.RadioButton optAllFact;
        public System.Windows.Forms.RadioButton optAvecBon;
        public System.Windows.Forms.RadioButton optSansBon;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label19;
        private System.Windows.Forms.Panel panel6;
        public iTalk.iTalk_TextBox_Small2 TotHt;
        public iTalk.iTalk_TextBox_Small2 txtTot;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssRSTE;
        public System.Windows.Forms.Button cmdAcheteur;
        private System.Windows.Forms.ContextMenuStrip mnuGrid;
        private System.Windows.Forms.ToolStripMenuItem mnuValidation;
        private System.Windows.Forms.ToolStripMenuItem mnuVisu;
        private System.Windows.Forms.ToolStripMenuItem mnuIntervention;
        private System.Windows.Forms.ToolStripMenuItem mnuBonDeCommande;
        private System.Windows.Forms.ToolStripMenuItem mnuDevis;
        private System.Windows.Forms.ToolStripMenuItem mnuAnalytique;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Label label20;
        public iTalk.iTalk_TextBox_Small2 txtRSTE_HT1;
    }
}
