﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur.Forms
{
    public partial class frmValideFacture : Form
    {
        public frmValideFacture()
        {
            InitializeComponent();
            OptAnnuler.Checked = false;
            optValider.Checked = false;
            Variable.sTempVariable = "";
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            if (optValider.Checked)
            {
                Variable.bFactureValide = true;
            }
            else if (OptAnnuler.Checked)
            {
                Variable.bFactureRefusee = true;
                if (string.IsNullOrEmpty(txtRSTE_Commentaire.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez saisir la raison du refus de cette facture.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            else if (OptEnAttente.Checked)
            {
                Variable.bFactureEnAttente = true;
            }
            else if (OptanomalieRegule.Checked)
            {
                Variable.bFactureRegule = true;
            }
            Variable.sTempVariable=txtRSTE_Commentaire.Text;

            this.Close();
        }

        private void Command2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmValideFacture_Load(object sender, EventArgs e)
        {

        }
    }
}
