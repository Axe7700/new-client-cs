﻿namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur.Forms
{
    partial class frmSendFactFourn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SSOleDBcmbDestinataire = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCopie = new iTalk.iTalk_TextBox_Small2();
            this.txtSujet = new iTalk.iTalk_RichTextBox();
            this.txtCommentaires = new iTalk.iTalk_RichTextBox();
            this.cmdCC = new System.Windows.Forms.Button();
            this.txtFile = new iTalk.iTalk_TextBox_Small2();
            this.btnEnvoyer = new System.Windows.Forms.Button();
            this.btnAnnuler = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbDestinataire)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(29, 93);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(44, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Sujet";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(29, 50);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 19);
            this.label1.TabIndex = 385;
            this.label1.Text = "Destinataire";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(29, 168);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 19);
            this.label2.TabIndex = 386;
            this.label2.Text = "Commentaires";
            // 
            // SSOleDBcmbDestinataire
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBcmbDestinataire.DisplayLayout.Appearance = appearance1;
            this.SSOleDBcmbDestinataire.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBcmbDestinataire.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSOleDBcmbDestinataire.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBcmbDestinataire.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSOleDBcmbDestinataire.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBcmbDestinataire.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBcmbDestinataire.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBcmbDestinataire.Location = new System.Drawing.Point(143, 50);
            this.SSOleDBcmbDestinataire.Name = "SSOleDBcmbDestinataire";
            this.SSOleDBcmbDestinataire.Size = new System.Drawing.Size(169, 22);
            this.SSOleDBcmbDestinataire.TabIndex = 0;
            // 
            // txtCopie
            // 
            this.txtCopie.AccAcceptNumbersOnly = false;
            this.txtCopie.AccAllowComma = false;
            this.txtCopie.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCopie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCopie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCopie.AccHidenValue = "";
            this.txtCopie.AccNotAllowedChars = null;
            this.txtCopie.AccReadOnly = false;
            this.txtCopie.AccReadOnlyAllowDelete = false;
            this.txtCopie.AccRequired = false;
            this.txtCopie.BackColor = System.Drawing.Color.White;
            this.txtCopie.CustomBackColor = System.Drawing.Color.White;
            this.txtCopie.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCopie.ForeColor = System.Drawing.Color.Black;
            this.txtCopie.Location = new System.Drawing.Point(356, 50);
            this.txtCopie.Margin = new System.Windows.Forms.Padding(2);
            this.txtCopie.MaxLength = 32767;
            this.txtCopie.Multiline = false;
            this.txtCopie.Name = "txtCopie";
            this.txtCopie.ReadOnly = false;
            this.txtCopie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCopie.Size = new System.Drawing.Size(182, 27);
            this.txtCopie.TabIndex = 1;
            this.txtCopie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCopie.UseSystemPasswordChar = false;
            // 
            // txtSujet
            // 
            this.txtSujet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSujet.AutoWordSelection = false;
            this.txtSujet.BackColor = System.Drawing.Color.Transparent;
            this.txtSujet.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtSujet.ForeColor = System.Drawing.Color.Black;
            this.txtSujet.Location = new System.Drawing.Point(143, 93);
            this.txtSujet.Name = "txtSujet";
            this.txtSujet.ReadOnly = false;
            this.txtSujet.Size = new System.Drawing.Size(395, 59);
            this.txtSujet.TabIndex = 2;
            this.txtSujet.WordWrap = true;
            // 
            // txtCommentaires
            // 
            this.txtCommentaires.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaires.AutoWordSelection = false;
            this.txtCommentaires.BackColor = System.Drawing.Color.Transparent;
            this.txtCommentaires.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCommentaires.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaires.Location = new System.Drawing.Point(143, 168);
            this.txtCommentaires.Name = "txtCommentaires";
            this.txtCommentaires.ReadOnly = false;
            this.txtCommentaires.Size = new System.Drawing.Size(395, 122);
            this.txtCommentaires.TabIndex = 3;
            this.txtCommentaires.WordWrap = true;
            // 
            // cmdCC
            // 
            this.cmdCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCC.FlatAppearance.BorderSize = 0;
            this.cmdCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCC.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCC.ForeColor = System.Drawing.Color.White;
            this.cmdCC.Location = new System.Drawing.Point(318, 50);
            this.cmdCC.Name = "cmdCC";
            this.cmdCC.Size = new System.Drawing.Size(33, 22);
            this.cmdCC.TabIndex = 571;
            this.cmdCC.Text = "Cc..";
            this.cmdCC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdCC.UseVisualStyleBackColor = false;
            this.cmdCC.Click += new System.EventHandler(this.cmdCC_Click);
            // 
            // txtFile
            // 
            this.txtFile.AccAcceptNumbersOnly = false;
            this.txtFile.AccAllowComma = false;
            this.txtFile.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFile.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFile.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFile.AccHidenValue = "";
            this.txtFile.AccNotAllowedChars = null;
            this.txtFile.AccReadOnly = false;
            this.txtFile.AccReadOnlyAllowDelete = false;
            this.txtFile.AccRequired = false;
            this.txtFile.BackColor = System.Drawing.Color.White;
            this.txtFile.CustomBackColor = System.Drawing.Color.White;
            this.txtFile.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtFile.ForeColor = System.Drawing.Color.Black;
            this.txtFile.Location = new System.Drawing.Point(11, 306);
            this.txtFile.Margin = new System.Windows.Forms.Padding(2);
            this.txtFile.MaxLength = 32767;
            this.txtFile.Multiline = false;
            this.txtFile.Name = "txtFile";
            this.txtFile.ReadOnly = false;
            this.txtFile.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFile.Size = new System.Drawing.Size(182, 27);
            this.txtFile.TabIndex = 572;
            this.txtFile.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFile.UseSystemPasswordChar = false;
            this.txtFile.Visible = false;
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.btnEnvoyer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnEnvoyer.FlatAppearance.BorderSize = 0;
            this.btnEnvoyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnvoyer.Image = global::Axe_interDT.Properties.Resources.new_post_24;
            this.btnEnvoyer.Location = new System.Drawing.Point(252, 306);
            this.btnEnvoyer.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.Size = new System.Drawing.Size(60, 35);
            this.btnEnvoyer.TabIndex = 573;
            this.btnEnvoyer.UseVisualStyleBackColor = false;
            this.btnEnvoyer.Click += new System.EventHandler(this.btnEnvoyer_Click);
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.btnAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnnuler.FlatAppearance.BorderSize = 0;
            this.btnAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnuler.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.btnAnnuler.Location = new System.Drawing.Point(333, 306);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(60, 35);
            this.btnAnnuler.TabIndex = 574;
            this.btnAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAnnuler.UseVisualStyleBackColor = false;
            // 
            // frmSendFactFourn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(589, 370);
            this.Controls.Add(this.btnAnnuler);
            this.Controls.Add(this.btnEnvoyer);
            this.Controls.Add(this.txtFile);
            this.Controls.Add(this.cmdCC);
            this.Controls.Add(this.txtCommentaires);
            this.Controls.Add(this.txtSujet);
            this.Controls.Add(this.txtCopie);
            this.Controls.Add(this.SSOleDBcmbDestinataire);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.MaximumSize = new System.Drawing.Size(605, 409);
            this.MinimumSize = new System.Drawing.Size(605, 409);
            this.Name = "frmSendFactFourn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmSendFactFourn";
            this.Load += new System.EventHandler(this.frmSendFactFourn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbDestinataire)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBcmbDestinataire;
        public iTalk.iTalk_TextBox_Small2 txtCopie;
        public iTalk.iTalk_RichTextBox txtSujet;
        public iTalk.iTalk_RichTextBox txtCommentaires;
        public System.Windows.Forms.Button cmdCC;
        public iTalk.iTalk_TextBox_Small2 txtFile;
        public System.Windows.Forms.Button btnEnvoyer;
        public System.Windows.Forms.Button btnAnnuler;
    }
}