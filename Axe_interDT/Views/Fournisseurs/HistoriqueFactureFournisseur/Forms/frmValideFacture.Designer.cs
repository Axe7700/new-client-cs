﻿namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur.Forms
{
    partial class frmValideFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.optValider = new System.Windows.Forms.RadioButton();
            this.OptAnnuler = new System.Windows.Forms.RadioButton();
            this.OptEnAttente = new System.Windows.Forms.RadioButton();
            this.OptanomalieRegule = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.Command1 = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.txtRSTE_Commentaire = new iTalk.iTalk_RichTextBox();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(78, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(62, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Total ht";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(223, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 19);
            this.label1.TabIndex = 385;
            this.label1.Text = "Cumul Facture";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(398, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 19);
            this.label2.TabIndex = 386;
            this.label2.Text = "Estimation";
            // 
            // optValider
            // 
            this.optValider.AutoSize = true;
            this.optValider.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optValider.Location = new System.Drawing.Point(12, 46);
            this.optValider.Name = "optValider";
            this.optValider.Size = new System.Drawing.Size(145, 23);
            this.optValider.TabIndex = 538;
            this.optValider.TabStop = true;
            this.optValider.Text = "Valider la facture";
            this.optValider.UseVisualStyleBackColor = true;
            // 
            // OptAnnuler
            // 
            this.OptAnnuler.AutoSize = true;
            this.OptAnnuler.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptAnnuler.Location = new System.Drawing.Point(163, 46);
            this.OptAnnuler.Name = "OptAnnuler";
            this.OptAnnuler.Size = new System.Drawing.Size(164, 23);
            this.OptAnnuler.TabIndex = 539;
            this.OptAnnuler.TabStop = true;
            this.OptAnnuler.Text = "anomalie constatée";
            this.OptAnnuler.UseVisualStyleBackColor = true;
            // 
            // OptEnAttente
            // 
            this.OptEnAttente.AutoSize = true;
            this.OptEnAttente.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEnAttente.Location = new System.Drawing.Point(343, 46);
            this.OptEnAttente.Name = "OptEnAttente";
            this.OptEnAttente.Size = new System.Drawing.Size(104, 23);
            this.OptEnAttente.TabIndex = 540;
            this.OptEnAttente.TabStop = true;
            this.OptEnAttente.Text = "En attente ";
            this.OptEnAttente.UseVisualStyleBackColor = true;
            // 
            // OptanomalieRegule
            // 
            this.OptanomalieRegule.AutoSize = true;
            this.OptanomalieRegule.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptanomalieRegule.Location = new System.Drawing.Point(476, 46);
            this.OptanomalieRegule.Name = "OptanomalieRegule";
            this.OptanomalieRegule.Size = new System.Drawing.Size(172, 23);
            this.OptanomalieRegule.TabIndex = 541;
            this.OptanomalieRegule.TabStop = true;
            this.OptanomalieRegule.Text = "anomalie régularisée";
            this.OptanomalieRegule.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(197, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 19);
            this.label3.TabIndex = 542;
            this.label3.Text = "Commentaire";
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(144, 191);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(85, 35);
            this.Command1.TabIndex = 571;
            this.Command1.Text = "    ok";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.Command2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command2.Location = new System.Drawing.Point(248, 191);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(85, 35);
            this.Command2.TabIndex = 570;
            this.Command2.Text = "Annuler";
            this.Command2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // txtRSTE_Commentaire
            // 
            this.txtRSTE_Commentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRSTE_Commentaire.AutoWordSelection = false;
            this.txtRSTE_Commentaire.BackColor = System.Drawing.Color.Transparent;
            this.txtRSTE_Commentaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRSTE_Commentaire.ForeColor = System.Drawing.Color.Black;
            this.txtRSTE_Commentaire.Location = new System.Drawing.Point(343, 81);
            this.txtRSTE_Commentaire.Name = "txtRSTE_Commentaire";
            this.txtRSTE_Commentaire.ReadOnly = false;
            this.txtRSTE_Commentaire.Size = new System.Drawing.Size(305, 100);
            this.txtRSTE_Commentaire.TabIndex = 569;
            this.txtRSTE_Commentaire.WordWrap = true;
            // 
            // frmValideFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(660, 237);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.Command2);
            this.Controls.Add(this.txtRSTE_Commentaire);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.OptanomalieRegule);
            this.Controls.Add(this.OptEnAttente);
            this.Controls.Add(this.OptAnnuler);
            this.Controls.Add(this.optValider);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.MaximumSize = new System.Drawing.Size(676, 276);
            this.MinimumSize = new System.Drawing.Size(676, 276);
            this.Name = "frmValideFacture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Contrôle";
            this.Load += new System.EventHandler(this.frmValideFacture_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.RadioButton optValider;
        public System.Windows.Forms.RadioButton OptAnnuler;
        public System.Windows.Forms.RadioButton OptEnAttente;
        public System.Windows.Forms.RadioButton OptanomalieRegule;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_RichTextBox txtRSTE_Commentaire;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button Command2;
    }
}