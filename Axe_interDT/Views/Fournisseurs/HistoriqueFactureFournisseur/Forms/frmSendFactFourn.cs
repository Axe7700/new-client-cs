﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Devis.Forms;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur.Forms
{
    public partial class frmSendFactFourn : Form
    {
        public frmSendFactFourn()
        {
            InitializeComponent();
        }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            ModCourrier.FichierAttache = txtFile.Text;

            ModCourrier.CreateMail(SSOleDBcmbDestinataire, ModCourrier.list, txtSujet.Text, txtCommentaires.Text, ModCourrier.FichierAttache);

            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCC_Click(object sender, EventArgs e)
        {
            frmCopie frmc = new frmCopie();
            frmc.ShowDialog();
            frmc.Close();
            txtCopie.Text = ModCourrier.list;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSendFactFourn_Load(object sender, EventArgs e)
        {
            int i = 0;

            for (i = 1; i <= General.MesDossiers.Length - 1; i++)
            {
                initialiseComboMailOutlook(ref General.MesDossiers[i]);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="Dossier"></param>
        public void initialiseComboMailOutlook(ref string Dossier)
        {

            int Index = 0;
            Redemption.AddressLists rals = new Redemption.AddressLists();
            Redemption.AddressList ral = null;
            string s = null;
            try
            {

                ral = rals.Item(Dossier);

                s = Convert.ToString(ral.AddressEntries.Count);

                var SSOleDBcmbASource = new DataTable();
                SSOleDBcmbASource.Columns.Add("Initiales");

                for (Index = 1; Index <= ral.AddressEntries.Count - 1; Index++)
                {
                    SSOleDBcmbASource.Rows.Add(ral.AddressEntries[Index].Name);

                }
                SSOleDBcmbDestinataire.DataSource = SSOleDBcmbASource;

                rals = null;

                ral = null;

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                rals = null;
                ral = null;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'initialiser Outlook, l'envoie par mail sera impossible", "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
