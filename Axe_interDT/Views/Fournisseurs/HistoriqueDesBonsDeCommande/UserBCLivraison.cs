﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Commande.UserPreCommande;
using Axe_interDT.Views.Intervention;
using Infragistics.Win.UltraWinGrid;
using Axe_interDT.Views.Fournisseurs.Fournisseur;

namespace Axe_interDT.Views.Fournisseur.HistoriqueDesBonsDeCommande
{
    public partial class UserBCLivraison : UserControl
    {
        public UserBCLivraison()
        {
            InitializeComponent();
        }
        bool bclick;
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            clearTextBox();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="C"></param>
        private void Foreachcontrols(Control C)
        {
            foreach (Control cc in C.Controls)
            {
                Foreachcontrols(cc);
            }
            if (C is iTalk.iTalk_TextBox_Small2 || C is UltraCombo)
                C.Text = "";
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void clearTextBox()
        {
            Foreachcontrols(this);
            Grid0.DataSource = null;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDelD_Click(object sender, EventArgs e)
        {
            sub_Valider();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void sub_Valider()
        {

            // ERROR: Not supported in C#: OnErrorStatement


            DataTable rs = default(DataTable);
            string sSQL = null;
            string sWhere = null;

            bclick = false;
            try
            {
                sSQL = "SELECT BonDeCommande.Signataire,BonDeCommande.Codeimmeuble,BonDeCommande.DateCommande," + "BonDeCommande.DateLivraison , BonDeCommande.CodeFourn, BonDeCommande.NoIntervention ," + "BonDeCommande.NoBonDeCommande , BonDeCommande.CodeE as CodeEtatComm , BCF_FactFourn.BCF_NoFact ," + " BCF_FactFourn.BCF_HT ,FournisseurArticle.raisonSocial,FournisseurArticle.Code,FNS_Agences.FNS_Agence,Intervention.CodeEtat as CodeEtatInt,TypeCodeEtat.LibelleCodeEtat " + "FROM BonDeCommande INNER JOIN FournisseurArticle ON Bondecommande.codefourn = FournisseurArticle.cleauto LEFT JOIN FNS_Agences ON Bondecommande.CodeAg = FNS_Agences.FNS_No LEFT JOIN BCF_FactFourn ON BonDeCommande.NoBonDeCommande = BCF_FactFourn.BCF_Cle INNER JOIN " + "Intervention ON BonDeCommande.NoIntervention = Intervention.NoIntervention Left JOIN " + " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat INNER JOIN TypeEtatCommande on BonDeCommande.CodeE = TypeEtatCommande.code";

                sWhere = " Where ";

                if (!string.IsNullOrEmpty(txt00.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoBonDeCommande >= " + Convert.ToDouble(txt00.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt01.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoBonDeCommande <= " + Convert.ToDouble(txt01.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt10.Text))
                {
                    sWhere = sWhere + " BCF_FactFourn.BCF_NoFact >= " + Convert.ToDouble(txt10.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt11.Text))
                {
                    sWhere = sWhere + " BCF_FactFourn.BCF_NoFact <= " + Convert.ToDouble(txt11.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt20.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoIntervention >= " + Convert.ToDouble(txt20.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt21.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoIntervention <= " + Convert.ToDouble(txt21.Text) + "  And   ";
                }

                if (!string.IsNullOrEmpty(txtDeNoAppel.Text))
                {
                    sWhere = sWhere + " Intervention.NumFicheStandard >= " + Convert.ToDouble(txtDeNoAppel.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txtAuNoAppel.Text))
                {
                    sWhere = sWhere + " Intervention.NumFicheStandard <= " + General.nz(txtAuNoAppel.Text, 0) + "  And   ";
                }

                if (!string.IsNullOrEmpty(txt30.Text))
                {
                    sWhere = sWhere + " BonDeCommande.CodeE='" + txt30.Text + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt31.Text))
                {
                    sWhere = sWhere + " Intervention.CodeEtat='" + txt31.Text + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt02.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateCommande >= '" + Convert.ToDateTime(txt02.Text).ToString("dd/MM/yyyy HH:mm:ss") + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt03.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateCommande <= '" + Convert.ToDateTime(txt03.Text + " 23:59:59").ToString("dd/MM/yyyy HH:mm:ss") + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt12.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateLivraison >= '" + Convert.ToDateTime(txt12.Text).ToString("dd/MM/yyyy HH:mm:ss") + "' And  ";
                }
                if (!string.IsNullOrEmpty(txt13.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateLivraison <= '" + Convert.ToDateTime(txt13.Text + " 23:59:59").ToString("dd/MM/yyyy HH:mm:ss") + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt22.Text))
                {
                    sWhere = sWhere + " FournisseurArticle.code='" + StdSQLchaine.gFr_DoublerQuote(txt22.Text) + "'And Bondecommande.CodeAg=" + General.nz((Text1.Text), "0") + "  And   ";
                }
                if (!string.IsNullOrEmpty(txt23.Text))
                {
                    sWhere = sWhere + " BonDeCommande.Signataire='" + StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "' And   ";
                }
                if (!string.IsNullOrEmpty(txt33.Text))
                {
                    sWhere = sWhere + "BonDeCommande.Codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(txt33.Text) + "' And   ";
                }
                if (!string.IsNullOrEmpty(txtTechnicien.Text))
                {
                    sWhere = sWhere + " technicien = '" + StdSQLchaine.gFr_DoublerQuote(txtTechnicien.Text) + "' And   ";
                }

                if (!string.IsNullOrEmpty(txt32.Text))
                {
                    sWhere = sWhere + "BCF_HT  >= " + Convert.ToDouble(txt33.Text) + "        ";
                }



                sWhere = General.Left(sWhere, General.Len(sWhere) - 7);

                sSQL = sSQL + sWhere;
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);

                Grid0.DataSource = null;
                // ERROR: Not supported in C#: OnErrorStatement
                DataTable dt = new DataTable();
                dt.Columns.Add("DateCommande");
                dt.Columns.Add("Date Liv");
                dt.Columns.Add("NoCmd");
                dt.Columns.Add("NoInt");
                dt.Columns.Add("raisonSocial");
                dt.Columns.Add("FNS_Agence");
                dt.Columns.Add("Signataire");
                dt.Columns.Add("CodeImmeuble");
                dt.Columns.Add("BCF_NoFact");
                dt.Columns.Add("CodeEtatInt");
                dt.Columns.Add("CodeEtatComm");
                dt.Columns.Add("BCF_HT");
                try
                {
                    string sDateLiv = null;
                    if (rs.Rows.Count > 0)
                    {
                        if (sDateLiv == "01/01/1900")
                            sDateLiv = "";
                        foreach (DataRow dr in rs.Rows)
                        {
                            //System.Windows.Forms.Application.DoEvents();
                            //On Error Resume Next
                            if (bclick == true)
                                break;

                            dt.Rows.Add(dr["DateCommande"], sDateLiv, dr["NoBondecommande"], dr["NoIntervention"], dr["raisonSocial"], dr["FNS_Agence"], dr["Signataire"], dr["CodeImmeuble"], dr["BCF_NoFact"], dr["CodeEtatInt"], dr["CodeEtatComm"], dr["BCF_HT"]);
                            //rs.MoveNext();
                        }
                        Grid0.DataSource = dt;

                    }
                    ModAdo.fc_CloseRecordset(rs);
                    return;
                }
                catch (Exception ex)
                {
                    Erreurs.gFr_debug(ex, this.Name + ";sub_Valider");
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";sub_Valider");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervention_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string sInt = null;
            sInt = General.getFrmReg("UserBCLivraison", "NoInt");
            if (!string.IsNullOrEmpty(sInt))
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, sInt);
                View.Theme.Theme.Navigate(typeof(UserIntervention));
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechAgence_Click(object sender, EventArgs e)
        {
            string sCode = null;
            double nCode = 0;
            try
            {
                string req = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\" ," + " FNS_Agences.FNS_No As \"NoIntAgence\", FNS_Agences.FNS_Ville as \"Agence Ville\"," + " FNS_Agences.FNS_Agence As \"Agence\" " + " FROM fournisseurArticle inner Join FNS_Agences On FournisseurArticle.Cleauto = FNS_Agences.FNS_cle";
                var search = new SearchTemplate(null, null, req, "", "") { Text = "Filtrer vos bon de commandes par fournisseur" };
                search.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    Text1.Text = search.ugResultat.ActiveRow.GetCellValue("NoIntAgence").ToString();
                    search.Close();
                };
                search.ugResultat.KeyPress += (se, ev) =>
                {
                    if ((short)ev.KeyChar == 13)
                    {
                        if (search.ugResultat.ActiveRow != null)
                        {
                            Text1.Text = search.ugResultat.ActiveRow.GetCellValue("NoIntAgence").ToString();
                            search.Close();
                        }
                    }
                };
                search.ShowDialog();
                fc_LibAgence();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheFourn_Click");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LibAgence()
        {
            string sSQL = null;
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "SELECT  Fns_Ville From FNS_Agences WHERE  Fns_No = " + General.nz(Text1.Text, 0);
                var ModAdo = new ModAdo();
                sSQL = ModAdo.fc_ADOlibelle(sSQL);

                lblAgence.Text = sSQL;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LibAgence");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheFourn_Click(object sender, EventArgs e)
        {
            string sCode = null;
            double nCode = 0;
            try
            {
                string req = "SELECT  Code as \"Code Fourn.\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\" , NePasAfficher as  \"Ne pas afficher\" FROM fournisseurArticle ";

                var search = new SearchTemplate(null, null, req, "", "") { Text = "Filtrer vos bon de commandes par fournisseur" };
                search.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txt22.Text = search.ugResultat.ActiveRow.GetCellValue("Code Fourn.").ToString();
                    search.Close();
                };
                search.ugResultat.KeyPress += (se, ev) =>
                {
                    if ((short)ev.KeyChar == 13)
                    {
                        if (search.ugResultat.ActiveRow != null)
                        {
                            txt22.Text = search.ugResultat.ActiveRow.GetCellValue("Code Fourn.").ToString();
                            search.Close();
                        }
                    }
                };
                search.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheFourn_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIm_Click(object sender, EventArgs e)
        {
            string sCode = null;
            double nCode = 0;
            try
            {
                string req = "SELECT CodeImmeuble as \"CodeImmeuble\", AngleRue as \"AngleRue\", Adresse as \"Adresse\", Ville as \"Ville\", CodePostal as \"CodePostal\" From Immeuble";
                var search = new SearchTemplate(null, null, req, "", "") { Text = "Filtrer vos bon de commandes par immeuble" };
                search.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txt33.Text = search.ugResultat.ActiveRow.GetCellValue("CodeImmeuble").ToString();
                    search.Close();
                };
                search.ugResultat.KeyPress += (se, ev) =>
                {
                    if ((short)ev.KeyChar == 13)
                    {
                        if (search.ugResultat.ActiveRow != null)
                        {
                            txt33.Text = search.ugResultat.ActiveRow.GetCellValue("CodeImmeuble").ToString();
                            search.Close();
                        }
                    }
                };
                search.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid0_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            DataTable rs = default(DataTable);
            int lnumfichestandard = 0;
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet("Select NumFicheStandard from Intervention" + " where NoIntervention=" + Grid0.ActiveRow.Cells[3].Value);
            if (rs.Rows.Count > 0)
            {
                lnumfichestandard = Convert.ToInt32(General.nz(rs.Rows[0]["Numfichestandard"], 0));
                General.saveInReg("UserDocStandard", "NewVar", rs.Rows[0]["NumFicheStandard"].ToString());
            }
            string[] tabSetting = new string[2];
            string[][] tabTemp = new string[][] { };
            //==Fonction en cours de developpement.
            if (General.sPrecommande == "1")//TESTED
            {
                General.saveInReg(Variable.cUserPreCommande, "Origine", this.Name);
                General.saveInReg(Variable.cUserPreCommande, "NewVar", Grid0.ActiveRow.Cells["NoInt"].Value.ToString());
                General.saveInReg(Variable.cUserPreCommande, "NumFicheStandard", Convert.ToString(lnumfichestandard));
                General.saveInReg(Variable.cUserPreCommande, "NoBonDeCommande", Grid0.ActiveRow.Cells["NoCmd"].Value.ToString());
                bclick = true;
                View.Theme.Theme.Navigate(typeof(UserPreCommande));

            }
            else
            {
                General.saveInReg("UserBCmdHead", "NoBcmd", Grid0.ActiveRow.Cells[2].Value.ToString());




                tabSetting[0] = "NoBcmd";

                tabSetting[1] = Grid0.ActiveRow.Cells[2].Value.ToString();

                //tabSetting.CopyTo(tabTemp, 0);
                Array.Resize(ref tabTemp, 1);
                tabTemp[0] = tabSetting;
                //for (int j = 0; j < tabTemp.Length; j++)
                //{
                //    tabTemp[j] = new string[j];
                //    for (int i = 0; i < tabSetting.Length; i++)
                //    {
                //        tabTemp[j][i] = tabSetting[i].ToString();
                //    }
                //    Array.Resize(ref tabTemp, j + 1);
                //}

                GeneralXav.stockRegParam("UserBCmdHead", tabTemp);


                tabSetting[0] = "NoInt";

                tabSetting[1] = Grid0.ActiveRow.Cells[3].Value.ToString();
                //tabSetting.CopyTo(tabTemp, 0);
                tabTemp[0] = tabSetting;

                GeneralXav.stockRegParam("UserBCmdHead", tabTemp);


                tabSetting[0] = "NoImmeuble";

                tabSetting[1] = Grid0.ActiveRow.Cells[7].Value.ToString();
                //tabSetting.CopyTo(tabTemp, 0);
                tabTemp[0] = tabSetting;
                GeneralXav.stockRegParam("UserBCmdHead", tabTemp);

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserBCmdHead, "");

                // fc_Navigue Me, gsCheminPackage & PROGUserBCmdHead
                bclick = true;
                //View.Theme.Theme.Navigate(typeof(UserCmdBody));//TODO 
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            
            ModParametre.fc_SaveParamPosition(this.Name, Variable.Cuserdocfourn, "");
            View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label13_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            // stocke les paramétres pour la feuille de destination.
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUderBCmdBody, "");
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);//TODO.
            //View.Theme.Theme.Navigate(typeof(UserBCmdBody));//TODO.
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label22_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.saveInReg("UserBCmdHead", "NoBcmd", "");
            string[] tabSetting = new string[2];
            string[][] tabTemp = new string[1][];
            //tabSetting(0) = "NoBcmd"
            //tabSetting(1) = Grid0.Columns(2).value
            //tabTemp(0) = tabSetting
            //Call stockRegParam("UserBCmdHead", tabTemp)
            tabSetting[0] = "NoInt";
            tabSetting[1] = General.getFrmReg("UserBCmdHead", "NoInt");
            //tabSetting.CopyTo(tabTemp, 0);
            tabTemp[0] = tabSetting;
            GeneralXav.stockRegParam("UserBCmdHead", tabTemp);
            tabSetting[0] = "NoImmeuble";
            var ModAdo = new ModAdo();
            tabSetting[1] = ModAdo.fc_ADOlibelle("SELECT Intervention.CodeImmeuble FROM Intervention" + " WHERE Intervention.NoIntervention='" + General.getFrmReg("UserBCmdHead", "NoInt") + "'");
            //tabSetting.CopyTo(tabTemp, 0);
            tabTemp[0] = tabSetting;
            //tabTemp[0] = Microsoft.VisualBasic.Compatibility.VB6.Support.CopyArray(tabSetting);
            GeneralXav.stockRegParam("UserBCmdHead", tabTemp);
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserBCmdHead, "");

            //==Fonction en cours de developpement.
            if (General.sPrecommande == "1")
            {
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUSERPRECOMMANDE);
                View.Theme.Theme.Navigate(typeof(UserPreCommande));
            }
            else
            {
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCmdHead); //TODO
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label7_Click(object sender, EventArgs e)
        {
            clear();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void clear()
        {
            Foreachcontrols(this);
            Grid0.DataSource = null;
            Label7.Enabled = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblClean_Click(object sender, EventArgs e)
        {
            txt00.Text = "";
            txt01.Text = "";
            txt02.Text = "";
            txt03.Text = "";
            txt10.Text = "";
            txt11.Text = "";
            txt12.Text = "";
            txt13.Text = "";
            txt20.Text = "";
            txt21.Text = "";
            txt22.Text = "";
            txt23.Text = "";
            txt30.Text = "";
            txt31.Text = "";
            txt32.Text = "";
            txt33.Text = "";
            txtDeNoAppel.Text = "";
            txtAuNoAppel.Text = "";
            txtTechnicien.Text = "";
            Text1.Text = "";
            lblAgence.Text = "";
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt00_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt00_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt01.Text))
                txt01.Text = txt00.Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt01_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt02_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;            
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt03_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt10_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt10_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt11.Text))
                txt11.Text = txt10.Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt20_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt21.Text))
                txt21.Text = txt20.Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt02_Leave(object sender, EventArgs e)
        {          
            if (string.IsNullOrEmpty(txt03.Text))
                txt03.Text = txt02.Text;
            
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt12_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txt13.Text))
                txt13.Text = txt12.Text;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt11_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt12_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
          
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt13_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
           
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt20_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt21_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt22_AfterCloseUp(object sender, EventArgs e)
        {
            Label7.Enabled = true;
            if (txt22.ActiveRow != null)
                Text1.Text = txt22.ActiveRow.Cells[3].Value.ToString();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt22_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            if (txt22.Rows.Count < 1)
            {
                sSQL = "SELECT FournisseurArticle.Code, FournisseurArticle.RaisonSocial," + " FNS_Agences.FNS_Agence ,FNS_Agences.FNS_No" + " FROM FNS_Agences RIGHT JOIN FournisseurArticle ON FNS_Agences.Fns_Cle = FournisseurArticle.cleauto";

                sheridan.InitialiseCombo(txt22, sSQL, "Code");

            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt23_AfterCloseUp(object sender, EventArgs e)
        {
            if (txt23.ActiveRow != null)
                Text2.Text = txt23.ActiveRow.Cells[0].Value.ToString();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt30_Click(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt23_Click(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt31_Click(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt32_TextChanged(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt33_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (txt33.Rows.Count < 1)
            {
                var _with5 = this;
                General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse," + " Immeuble.CodePostal, Immeuble.Ville" + " From Immeuble" + " order by CodeImmeuble";
                sheridan.InitialiseCombo(_with5.txt33, General.sSQL, "codeImmeuble");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt33_Click(object sender, EventArgs e)
        {
            Label7.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDeNoAppel_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAuNoAppel.Text))
                txtAuNoAppel.Text = txtDeNoAppel.Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserBCLivraison_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible) return;
            string stemp = "";
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            ModParametre.fc_RecupParam("UserBCLivraison");
            if (General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserPreCommande2))
            {
                txt20.Text = General.getFrmReg("UserBCmdHead", "NoInt","");
                txt21.Text = txt20.Text;
                txtDeNoAppel.Text = General.getFrmReg("UserBCmdHead", "NoFicheAppel","");
                txtAuNoAppel.Text = txtDeNoAppel.Text;
                sub_Valider();

            }
            else if (!(ModParametre.sFicheAppelante == "UserBCmdHead" || ModParametre.sFicheAppelante == "UderBCmdBody" || ModParametre.sFicheAppelante == "UserDocFourn" || ModParametre.sFicheAppelante == "UserFactFourn" || ModParametre.sFicheAppelante == "UserDocIntervention"))
            {
                General.saveInReg("UserBCmdHead", "NoBcmd","");
                General.saveInReg("UserBCmdHead", "NoFNS","");
            }
            else
            {
                if (General.getFrmReg("UserBCmdHead", "NoInt") != "-1")
                {
                    txt20.Text = General.getFrmReg("UserBCmdHead", "NoInt","");
                    txt21.Text = txt20.Text;
                    stemp = General.getFrmReg("UserBCmdHead", "NoImmeuble","");
                    txt33.Text = stemp.ToString();
                    sub_Valider();
                }
                else
                {
                    // If GetSetting(cFrNomApp, "UserDocFourn", "sFicheAppelante") = "" Then
                    //   SaveSetting cFrNomApp, "UserBCmdHead", "NoInt", "-1"
                    //   SaveSetting cFrNomApp, "UserBCmdHead", "NoImmeuble", ""
                    //Label12.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(1560);
                    //Label1.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2280);
                    Label13.Visible = false;
                    Label22.Visible = false;
                    cmdIntervention.Visible = false;
                    //End If
                }
            }
            if (Convert.ToInt32(General.nz(General.getFrmReg("UserBCmdHead", "NoBcmd"), -1)) != -1)
                Label13.Enabled = true;
            System.Windows.Forms.Application.DoEvents();
            LoadStateIntList();
            LoadStateCommList();
            // LoadListBuilding
            //LoadListFurn
            LoadSignataire();
            if (ModParametre.sFicheAppelante == "UserIntervention" || ModParametre.sFicheAppelante == "UserDocStandard")
            {
                txt20.Text = General.getFrmReg("UserBCLivraison", "NoInt","");
                txt21.Text = txt20.Text;
                sub_Valider();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void LoadStateIntList()
        {
            DataTable rs = default(DataTable);
            string stemp = null;
            stemp = "Select typecodeetat.CodeEtat,typecodeetat.LibelleCodeEtat from typecodeetat";
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(stemp);
            DataTable dt = new DataTable();
            dt.Columns.Add("CodeEtat");
            dt.Columns.Add("LibelleCodeEtat");
            if (rs.Rows.Count > 0)
            {
                foreach (DataRow dr in rs.Rows)
                {
                    dt.Rows.Add(dr["CodeEtat"], dr["LibelleCodeEtat"]);
                    txt31.DataSource = dt;
                }
            }
            ModAdo.fc_CloseRecordset(rs);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void LoadSignataire()
        {
            DataTable rs = default(DataTable);
            string stemp = null;
            stemp = "Select USR_Nt,USR_Name from USR_Users";
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(stemp);
            DataTable dt = new DataTable();
            dt.Columns.Add("Usr_Nt");
            dt.Columns.Add("Usr_Name");
            if (rs.Rows.Count > 0)
            {
                foreach (DataRow dr in rs.Rows)
                {
                    dt.Rows.Add(dr["Usr_Nt"], dr["Usr_Name"]);
                    txt23.DataSource = dt;
                }
            }
            ModAdo.fc_CloseRecordset(rs);
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void LoadStateCommList()
        {
            DataTable rs = default(DataTable);
            string stemp = null;
            stemp = "Select typeetatcommande.Code,typeetatcommande.Libelle from typeetatcommande";
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(stemp);
            DataTable dt = new DataTable();
            dt.Columns.Add("Code");
            dt.Columns.Add("Libelle");
            if (rs.Rows.Count > 0)
            {
                foreach (DataRow dr in rs.Rows)
                {
                    dt.Rows.Add(dr["Code"], dr["Libelle"]);
                    txt30.DataSource = dt;
                }
            }
            ModAdo.fc_CloseRecordset(rs);
        }

        private void Grid0_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DateCommande");
            dt.Columns.Add("Date Liv");
            dt.Columns.Add("NoCmd");
            dt.Columns.Add("NoInt");
            dt.Columns.Add("raisonSocial");
            dt.Columns.Add("FNS_Agence");
            dt.Columns.Add("Signataire");
            dt.Columns.Add("CodeImmeuble");
            dt.Columns.Add("BCF_NoFact");
            dt.Columns.Add("CodeEtatInt");
            dt.Columns.Add("CodeEtatComm");
            dt.Columns.Add("BCF_HT");
            Grid0.DisplayLayout.Bands[0].Columns["DateCommande"].Header.Caption = "Date Cmd";
            Grid0.DisplayLayout.Bands[0].Columns["Date Liv"].Header.Caption = "Date Liv.";
            Grid0.DisplayLayout.Bands[0].Columns["NoCmd"].Header.Caption = "NoCmd";
            Grid0.DisplayLayout.Bands[0].Columns["raisonSocial"].Header.Caption = "Fournisseur";
            Grid0.DisplayLayout.Bands[0].Columns["FNS_Agence"].Header.Caption = "Agence";
            Grid0.DisplayLayout.Bands[0].Columns["Signataire"].Header.Caption = "Signataire";
            Grid0.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "Immeuble";
            Grid0.DisplayLayout.Bands[0].Columns["BCF_NoFact"].Header.Caption = "No Facture";
            Grid0.DisplayLayout.Bands[0].Columns["CodeEtatComm"].Header.Caption = "Statut Int";
            Grid0.DisplayLayout.Bands[0].Columns["CodeEtatComm"].Header.Caption = "Statut Cmd";
            Grid0.DisplayLayout.Bands[0].Columns["BCF_HT"].Header.Caption = "Ht";
        }

  

        private void txt02_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt02.Text) && !General.IsDate(txt02.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une date valide", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt02.Text = "";
                txt03.Text = "";
                return;
            }
        }

        private void txt12_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt12.Text) && !General.IsDate(txt12.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une date valide", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt12.Text = "";
                txt13.Text = "";
                return;
            }
        }

        private void txt03_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt03.Text) && !General.IsDate(txt03.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une date valide", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt03.Text = "";
                return;
            }
        }

        private void txt13_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt13.Text) && !General.IsDate(txt13.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une date valide", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt13.Text = "";
                return;
            }
        }

        private void txt00_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt00.Text) && !General.IsNumeric(txt00.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt00.Text = "";
                txt01.Text = "";
                return;
            }
        }

        private void txt01_Validated(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txt01.Text) && !General.IsNumeric(txt01.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt01.Text = "";
                return;
            }
        }

        private void txtDeNoAppel_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDeNoAppel.Text) && !General.IsNumeric(txtDeNoAppel.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDeNoAppel.Text = "";
                txtAuNoAppel.Text = "";
                return;
            }
        }

        private void txtAuNoAppel_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtAuNoAppel.Text) && !General.IsNumeric(txtAuNoAppel.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtAuNoAppel.Text = "";
                return;
            }
        }

        private void txt10_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt10.Text) && !General.IsNumeric(txt10.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt10.Text = "";
                txt11.Text = "";
                return;
            }
        }

        private void txt11_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txt11.Text) && !General.IsNumeric(txt11.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("saisir une valeur numérique", "",
                MessageBoxButtons.OK, MessageBoxIcon.Information);
                txt11.Text = "";
                return;
            }

        }
    }
}
