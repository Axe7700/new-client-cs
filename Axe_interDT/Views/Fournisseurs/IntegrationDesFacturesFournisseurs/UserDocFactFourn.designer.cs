namespace Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs
{
    partial class UserDocFactFourn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.Cmd17 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtEcheance = new iTalk.iTalk_TextBox_Small2();
            this.txtMontantFacture = new iTalk.iTalk_TextBox_Small2();
            this.txtNoAutoFacture = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblDateCreation = new System.Windows.Forms.Label();
            this.lblDateModif = new System.Windows.Forms.Label();
            this.lblCreateur = new System.Windows.Forms.Label();
            this.lblNomModif = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtCodeFourn = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.txtVerrou = new iTalk.iTalk_TextBox_Small2();
            this.txtModeRgt = new iTalk.iTalk_TextBox_Small2();
            this.txtTauxTVA = new iTalk.iTalk_TextBox_Small2();
            this.txtNbreJour = new iTalk.iTalk_TextBox_Small2();
            this.txtTypeEcheance = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheRaisonSociale = new System.Windows.Forms.Button();
            this.txtRaisonFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.lblGoFournisseur = new System.Windows.Forms.LinkLabel();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lblNoComptable = new System.Windows.Forms.Label();
            this.txtCompteFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.txtDateFacture = new iTalk.iTalk_TextBox_Small2();
            this.TxtNoFacture = new iTalk.iTalk_TextBox_Small2();
            this.txtDateEcheance = new iTalk.iTalk_TextBox_Small2();
            this.cmbCptCharges = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblModeReglement = new iTalk.iTalk_TextBox_Small2();
            this.lblTauxTva = new iTalk.iTalk_TextBox_Small2();
            this.txtNoComptable = new iTalk.iTalk_TextBox_Small2();
            this.chkIntegration = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.ssFactFournHtTvaCpta = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtCommentaire = new iTalk.iTalk_RichTextBox();
            this.cmdfactScann = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.chkAjusteMtTVA = new System.Windows.Forms.CheckBox();
            this.txtTTC = new iTalk.iTalk_TextBox_Small2();
            this.label14 = new System.Windows.Forms.Label();
            this.cmbCptChargesBis = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.GridFactFournPied = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNoBcmd = new iTalk.iTalk_TextBox_Small2();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdAvoir = new System.Windows.Forms.Button();
            this.cmdEnleverUnBcmd = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheBCMD = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNoFacture = new System.Windows.Forms.Label();
            this.GridFactFournDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCptCharges)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssFactFournHtTvaCpta)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCptChargesBis)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournPied)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.GridFactFournDetail, 0, 6);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 81F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 127F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdImprimer);
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdSupprimer);
            this.flowLayoutPanel1.Controls.Add(this.Cmd17);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1844, 42);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdImprimer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImprimer.Location = new System.Drawing.Point(1782, 2);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdImprimer.TabIndex = 392;
            this.cmdImprimer.UseVisualStyleBackColor = false;
            this.cmdImprimer.Visible = false;
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(1718, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 375;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(1654, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 374;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(1590, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 366;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(1526, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 376;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(1462, 2);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 377;
            this.cmdSupprimer.Tag = "";
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSupprimer, "Supprimer");
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            // 
            // Cmd17
            // 
            this.Cmd17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Cmd17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd17.FlatAppearance.BorderSize = 0;
            this.Cmd17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd17.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Cmd17.ForeColor = System.Drawing.Color.White;
            this.Cmd17.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.Cmd17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd17.Location = new System.Drawing.Point(1373, 2);
            this.Cmd17.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd17.Name = "Cmd17";
            this.Cmd17.Size = new System.Drawing.Size(85, 35);
            this.Cmd17.TabIndex = 391;
            this.Cmd17.Text = "Stocker";
            this.Cmd17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cmd17.UseVisualStyleBackColor = false;
            this.Cmd17.Visible = false;
            this.Cmd17.Click += new System.EventHandler(this.Cmd17_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 169F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 53F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 225F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblDateCreation, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblDateModif, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblCreateur, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblNomModif, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 80);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 75);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.txtEcheance);
            this.flowLayoutPanel3.Controls.Add(this.txtMontantFacture);
            this.flowLayoutPanel3.Controls.Add(this.txtNoAutoFacture);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(579, 40);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1262, 32);
            this.flowLayoutPanel3.TabIndex = 399;
            this.flowLayoutPanel3.Visible = false;
            // 
            // txtEcheance
            // 
            this.txtEcheance.AccAcceptNumbersOnly = false;
            this.txtEcheance.AccAllowComma = false;
            this.txtEcheance.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEcheance.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEcheance.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEcheance.AccHidenValue = "";
            this.txtEcheance.AccNotAllowedChars = null;
            this.txtEcheance.AccReadOnly = false;
            this.txtEcheance.AccReadOnlyAllowDelete = false;
            this.txtEcheance.AccRequired = false;
            this.txtEcheance.BackColor = System.Drawing.Color.White;
            this.txtEcheance.CustomBackColor = System.Drawing.Color.White;
            this.txtEcheance.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEcheance.ForeColor = System.Drawing.Color.Black;
            this.txtEcheance.Location = new System.Drawing.Point(2, 2);
            this.txtEcheance.Margin = new System.Windows.Forms.Padding(2);
            this.txtEcheance.MaxLength = 32767;
            this.txtEcheance.Multiline = false;
            this.txtEcheance.Name = "txtEcheance";
            this.txtEcheance.ReadOnly = false;
            this.txtEcheance.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEcheance.Size = new System.Drawing.Size(24, 27);
            this.txtEcheance.TabIndex = 502;
            this.txtEcheance.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEcheance.UseSystemPasswordChar = false;
            this.txtEcheance.Visible = false;
            // 
            // txtMontantFacture
            // 
            this.txtMontantFacture.AccAcceptNumbersOnly = false;
            this.txtMontantFacture.AccAllowComma = false;
            this.txtMontantFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMontantFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMontantFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMontantFacture.AccHidenValue = "";
            this.txtMontantFacture.AccNotAllowedChars = null;
            this.txtMontantFacture.AccReadOnly = false;
            this.txtMontantFacture.AccReadOnlyAllowDelete = false;
            this.txtMontantFacture.AccRequired = false;
            this.txtMontantFacture.BackColor = System.Drawing.Color.White;
            this.txtMontantFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtMontantFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMontantFacture.ForeColor = System.Drawing.Color.Black;
            this.txtMontantFacture.Location = new System.Drawing.Point(30, 2);
            this.txtMontantFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantFacture.MaxLength = 32767;
            this.txtMontantFacture.Multiline = false;
            this.txtMontantFacture.Name = "txtMontantFacture";
            this.txtMontantFacture.ReadOnly = false;
            this.txtMontantFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMontantFacture.Size = new System.Drawing.Size(24, 27);
            this.txtMontantFacture.TabIndex = 502;
            this.txtMontantFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMontantFacture.UseSystemPasswordChar = false;
            this.txtMontantFacture.Visible = false;
            // 
            // txtNoAutoFacture
            // 
            this.txtNoAutoFacture.AccAcceptNumbersOnly = false;
            this.txtNoAutoFacture.AccAllowComma = false;
            this.txtNoAutoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoAutoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoAutoFacture.AccHidenValue = "";
            this.txtNoAutoFacture.AccNotAllowedChars = null;
            this.txtNoAutoFacture.AccReadOnly = false;
            this.txtNoAutoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoAutoFacture.AccRequired = false;
            this.txtNoAutoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoAutoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoAutoFacture.Location = new System.Drawing.Point(58, 2);
            this.txtNoAutoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoAutoFacture.MaxLength = 32767;
            this.txtNoAutoFacture.Multiline = false;
            this.txtNoAutoFacture.Name = "txtNoAutoFacture";
            this.txtNoAutoFacture.ReadOnly = false;
            this.txtNoAutoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoAutoFacture.Size = new System.Drawing.Size(24, 27);
            this.txtNoAutoFacture.TabIndex = 502;
            this.txtNoAutoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoAutoFacture.UseSystemPasswordChar = false;
            this.txtNoAutoFacture.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 19);
            this.label2.TabIndex = 397;
            this.label2.Text = "Créée le :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(3, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 19);
            this.label1.TabIndex = 397;
            this.label1.Text = "Modifiée le :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(301, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 19);
            this.label3.TabIndex = 397;
            this.label3.Text = "Par :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(301, 37);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(39, 19);
            this.label4.TabIndex = 397;
            this.label4.Text = "Par :";
            // 
            // lblDateCreation
            // 
            this.lblDateCreation.AutoSize = true;
            this.lblDateCreation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDateCreation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateCreation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDateCreation.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblDateCreation.Location = new System.Drawing.Point(132, 3);
            this.lblDateCreation.Margin = new System.Windows.Forms.Padding(3);
            this.lblDateCreation.Name = "lblDateCreation";
            this.lblDateCreation.Size = new System.Drawing.Size(163, 31);
            this.lblDateCreation.TabIndex = 397;
            this.lblDateCreation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblDateModif
            // 
            this.lblDateModif.AutoSize = true;
            this.lblDateModif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDateModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateModif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblDateModif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblDateModif.Location = new System.Drawing.Point(132, 40);
            this.lblDateModif.Margin = new System.Windows.Forms.Padding(3);
            this.lblDateModif.Name = "lblDateModif";
            this.lblDateModif.Size = new System.Drawing.Size(163, 32);
            this.lblDateModif.TabIndex = 397;
            this.lblDateModif.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblCreateur
            // 
            this.lblCreateur.AutoSize = true;
            this.lblCreateur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCreateur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreateur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblCreateur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblCreateur.Location = new System.Drawing.Point(354, 3);
            this.lblCreateur.Margin = new System.Windows.Forms.Padding(3);
            this.lblCreateur.Name = "lblCreateur";
            this.lblCreateur.Size = new System.Drawing.Size(219, 31);
            this.lblCreateur.TabIndex = 397;
            this.lblCreateur.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNomModif
            // 
            this.lblNomModif.AutoSize = true;
            this.lblNomModif.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNomModif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNomModif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNomModif.Location = new System.Drawing.Point(354, 40);
            this.lblNomModif.Margin = new System.Windows.Forms.Padding(3);
            this.lblNomModif.Name = "lblNomModif";
            this.lblNomModif.Size = new System.Drawing.Size(219, 32);
            this.lblNomModif.TabIndex = 397;
            this.lblNomModif.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.txtCodeFourn);
            this.flowLayoutPanel2.Controls.Add(this.txtCodeFournisseur);
            this.flowLayoutPanel2.Controls.Add(this.txtVerrou);
            this.flowLayoutPanel2.Controls.Add(this.txtModeRgt);
            this.flowLayoutPanel2.Controls.Add(this.txtTauxTVA);
            this.flowLayoutPanel2.Controls.Add(this.txtNbreJour);
            this.flowLayoutPanel2.Controls.Add(this.txtTypeEcheance);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(579, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1262, 31);
            this.flowLayoutPanel2.TabIndex = 398;
            this.flowLayoutPanel2.Visible = false;
            // 
            // txtCodeFourn
            // 
            this.txtCodeFourn.AccAcceptNumbersOnly = false;
            this.txtCodeFourn.AccAllowComma = false;
            this.txtCodeFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFourn.AccHidenValue = "";
            this.txtCodeFourn.AccNotAllowedChars = null;
            this.txtCodeFourn.AccReadOnly = false;
            this.txtCodeFourn.AccReadOnlyAllowDelete = false;
            this.txtCodeFourn.AccRequired = false;
            this.txtCodeFourn.BackColor = System.Drawing.Color.White;
            this.txtCodeFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFourn.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeFourn.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFourn.Location = new System.Drawing.Point(2, 2);
            this.txtCodeFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFourn.MaxLength = 32767;
            this.txtCodeFourn.Multiline = false;
            this.txtCodeFourn.Name = "txtCodeFourn";
            this.txtCodeFourn.ReadOnly = false;
            this.txtCodeFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFourn.Size = new System.Drawing.Size(24, 27);
            this.txtCodeFourn.TabIndex = 502;
            this.txtCodeFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFourn.UseSystemPasswordChar = false;
            this.txtCodeFourn.Visible = false;
            // 
            // txtCodeFournisseur
            // 
            this.txtCodeFournisseur.AccAcceptNumbersOnly = false;
            this.txtCodeFournisseur.AccAllowComma = false;
            this.txtCodeFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFournisseur.AccHidenValue = "";
            this.txtCodeFournisseur.AccNotAllowedChars = null;
            this.txtCodeFournisseur.AccReadOnly = false;
            this.txtCodeFournisseur.AccReadOnlyAllowDelete = false;
            this.txtCodeFournisseur.AccRequired = false;
            this.txtCodeFournisseur.BackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFournisseur.Location = new System.Drawing.Point(30, 2);
            this.txtCodeFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFournisseur.MaxLength = 32767;
            this.txtCodeFournisseur.Multiline = false;
            this.txtCodeFournisseur.Name = "txtCodeFournisseur";
            this.txtCodeFournisseur.ReadOnly = false;
            this.txtCodeFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFournisseur.Size = new System.Drawing.Size(24, 27);
            this.txtCodeFournisseur.TabIndex = 502;
            this.txtCodeFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFournisseur.UseSystemPasswordChar = false;
            this.txtCodeFournisseur.Visible = false;
            // 
            // txtVerrou
            // 
            this.txtVerrou.AccAcceptNumbersOnly = false;
            this.txtVerrou.AccAllowComma = false;
            this.txtVerrou.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVerrou.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVerrou.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVerrou.AccHidenValue = "";
            this.txtVerrou.AccNotAllowedChars = null;
            this.txtVerrou.AccReadOnly = false;
            this.txtVerrou.AccReadOnlyAllowDelete = false;
            this.txtVerrou.AccRequired = false;
            this.txtVerrou.BackColor = System.Drawing.Color.White;
            this.txtVerrou.CustomBackColor = System.Drawing.Color.White;
            this.txtVerrou.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtVerrou.ForeColor = System.Drawing.Color.Black;
            this.txtVerrou.Location = new System.Drawing.Point(58, 2);
            this.txtVerrou.Margin = new System.Windows.Forms.Padding(2);
            this.txtVerrou.MaxLength = 32767;
            this.txtVerrou.Multiline = false;
            this.txtVerrou.Name = "txtVerrou";
            this.txtVerrou.ReadOnly = false;
            this.txtVerrou.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVerrou.Size = new System.Drawing.Size(24, 27);
            this.txtVerrou.TabIndex = 502;
            this.txtVerrou.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVerrou.UseSystemPasswordChar = false;
            this.txtVerrou.Visible = false;
            // 
            // txtModeRgt
            // 
            this.txtModeRgt.AccAcceptNumbersOnly = false;
            this.txtModeRgt.AccAllowComma = false;
            this.txtModeRgt.AccBackgroundColor = System.Drawing.Color.White;
            this.txtModeRgt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtModeRgt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtModeRgt.AccHidenValue = "";
            this.txtModeRgt.AccNotAllowedChars = null;
            this.txtModeRgt.AccReadOnly = false;
            this.txtModeRgt.AccReadOnlyAllowDelete = false;
            this.txtModeRgt.AccRequired = false;
            this.txtModeRgt.BackColor = System.Drawing.Color.White;
            this.txtModeRgt.CustomBackColor = System.Drawing.Color.White;
            this.txtModeRgt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtModeRgt.ForeColor = System.Drawing.Color.Black;
            this.txtModeRgt.Location = new System.Drawing.Point(86, 2);
            this.txtModeRgt.Margin = new System.Windows.Forms.Padding(2);
            this.txtModeRgt.MaxLength = 32767;
            this.txtModeRgt.Multiline = false;
            this.txtModeRgt.Name = "txtModeRgt";
            this.txtModeRgt.ReadOnly = false;
            this.txtModeRgt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtModeRgt.Size = new System.Drawing.Size(24, 27);
            this.txtModeRgt.TabIndex = 502;
            this.txtModeRgt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtModeRgt.UseSystemPasswordChar = false;
            this.txtModeRgt.Visible = false;
            // 
            // txtTauxTVA
            // 
            this.txtTauxTVA.AccAcceptNumbersOnly = false;
            this.txtTauxTVA.AccAllowComma = false;
            this.txtTauxTVA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTauxTVA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTauxTVA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTauxTVA.AccHidenValue = "";
            this.txtTauxTVA.AccNotAllowedChars = null;
            this.txtTauxTVA.AccReadOnly = false;
            this.txtTauxTVA.AccReadOnlyAllowDelete = false;
            this.txtTauxTVA.AccRequired = false;
            this.txtTauxTVA.BackColor = System.Drawing.Color.White;
            this.txtTauxTVA.CustomBackColor = System.Drawing.Color.White;
            this.txtTauxTVA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTauxTVA.ForeColor = System.Drawing.Color.Black;
            this.txtTauxTVA.Location = new System.Drawing.Point(114, 2);
            this.txtTauxTVA.Margin = new System.Windows.Forms.Padding(2);
            this.txtTauxTVA.MaxLength = 32767;
            this.txtTauxTVA.Multiline = false;
            this.txtTauxTVA.Name = "txtTauxTVA";
            this.txtTauxTVA.ReadOnly = false;
            this.txtTauxTVA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTauxTVA.Size = new System.Drawing.Size(24, 27);
            this.txtTauxTVA.TabIndex = 502;
            this.txtTauxTVA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTauxTVA.UseSystemPasswordChar = false;
            this.txtTauxTVA.Visible = false;
            // 
            // txtNbreJour
            // 
            this.txtNbreJour.AccAcceptNumbersOnly = false;
            this.txtNbreJour.AccAllowComma = false;
            this.txtNbreJour.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNbreJour.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNbreJour.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNbreJour.AccHidenValue = "";
            this.txtNbreJour.AccNotAllowedChars = null;
            this.txtNbreJour.AccReadOnly = false;
            this.txtNbreJour.AccReadOnlyAllowDelete = false;
            this.txtNbreJour.AccRequired = false;
            this.txtNbreJour.BackColor = System.Drawing.Color.White;
            this.txtNbreJour.CustomBackColor = System.Drawing.Color.White;
            this.txtNbreJour.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNbreJour.ForeColor = System.Drawing.Color.Black;
            this.txtNbreJour.Location = new System.Drawing.Point(142, 2);
            this.txtNbreJour.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbreJour.MaxLength = 32767;
            this.txtNbreJour.Multiline = false;
            this.txtNbreJour.Name = "txtNbreJour";
            this.txtNbreJour.ReadOnly = false;
            this.txtNbreJour.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNbreJour.Size = new System.Drawing.Size(24, 27);
            this.txtNbreJour.TabIndex = 502;
            this.txtNbreJour.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNbreJour.UseSystemPasswordChar = false;
            this.txtNbreJour.Visible = false;
            // 
            // txtTypeEcheance
            // 
            this.txtTypeEcheance.AccAcceptNumbersOnly = false;
            this.txtTypeEcheance.AccAllowComma = false;
            this.txtTypeEcheance.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTypeEcheance.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTypeEcheance.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTypeEcheance.AccHidenValue = "";
            this.txtTypeEcheance.AccNotAllowedChars = null;
            this.txtTypeEcheance.AccReadOnly = false;
            this.txtTypeEcheance.AccReadOnlyAllowDelete = false;
            this.txtTypeEcheance.AccRequired = false;
            this.txtTypeEcheance.BackColor = System.Drawing.Color.White;
            this.txtTypeEcheance.CustomBackColor = System.Drawing.Color.White;
            this.txtTypeEcheance.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTypeEcheance.ForeColor = System.Drawing.Color.Black;
            this.txtTypeEcheance.Location = new System.Drawing.Point(170, 2);
            this.txtTypeEcheance.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeEcheance.MaxLength = 32767;
            this.txtTypeEcheance.Multiline = false;
            this.txtTypeEcheance.Name = "txtTypeEcheance";
            this.txtTypeEcheance.ReadOnly = false;
            this.txtTypeEcheance.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTypeEcheance.Size = new System.Drawing.Size(24, 27);
            this.txtTypeEcheance.TabIndex = 502;
            this.txtTypeEcheance.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTypeEcheance.UseSystemPasswordChar = false;
            this.txtTypeEcheance.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.24601F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 184F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.877F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.877F));
            this.tableLayoutPanel3.Controls.Add(this.cmdRechercheRaisonSociale, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtRaisonFournisseur, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblGoFournisseur, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label9, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.label10, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.label8, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label11, 5, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblNoComptable, 5, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtCompteFournisseur, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtDateFacture, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.TxtNoFacture, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtDateEcheance, 6, 2);
            this.tableLayoutPanel3.Controls.Add(this.cmbCptCharges, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblModeReglement, 4, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblTauxTva, 4, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtNoComptable, 6, 3);
            this.tableLayoutPanel3.Controls.Add(this.chkIntegration, 6, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 161);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1844, 121);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // cmdRechercheRaisonSociale
            // 
            this.cmdRechercheRaisonSociale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheRaisonSociale.FlatAppearance.BorderSize = 0;
            this.cmdRechercheRaisonSociale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheRaisonSociale.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdRechercheRaisonSociale.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheRaisonSociale.Location = new System.Drawing.Point(780, 3);
            this.cmdRechercheRaisonSociale.Name = "cmdRechercheRaisonSociale";
            this.cmdRechercheRaisonSociale.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheRaisonSociale.TabIndex = 505;
            this.cmdRechercheRaisonSociale.Tag = "Recherche du Fournisseur";
            this.cmdRechercheRaisonSociale.UseVisualStyleBackColor = false;
            this.cmdRechercheRaisonSociale.Click += new System.EventHandler(this.cmdRechercheRaisonSociale_Click);
            // 
            // txtRaisonFournisseur
            // 
            this.txtRaisonFournisseur.AccAcceptNumbersOnly = false;
            this.txtRaisonFournisseur.AccAllowComma = false;
            this.txtRaisonFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRaisonFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRaisonFournisseur.AccHidenValue = "";
            this.txtRaisonFournisseur.AccNotAllowedChars = null;
            this.txtRaisonFournisseur.AccReadOnly = false;
            this.txtRaisonFournisseur.AccReadOnlyAllowDelete = false;
            this.txtRaisonFournisseur.AccRequired = false;
            this.txtRaisonFournisseur.BackColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRaisonFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtRaisonFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtRaisonFournisseur.Location = new System.Drawing.Point(146, 2);
            this.txtRaisonFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtRaisonFournisseur.MaxLength = 32767;
            this.txtRaisonFournisseur.Multiline = false;
            this.txtRaisonFournisseur.Name = "txtRaisonFournisseur";
            this.txtRaisonFournisseur.ReadOnly = false;
            this.txtRaisonFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRaisonFournisseur.Size = new System.Drawing.Size(629, 27);
            this.txtRaisonFournisseur.TabIndex = 450;
            this.txtRaisonFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRaisonFournisseur.UseSystemPasswordChar = false;
            // 
            // lblGoFournisseur
            // 
            this.lblGoFournisseur.AutoSize = true;
            this.lblGoFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblGoFournisseur.Location = new System.Drawing.Point(3, 0);
            this.lblGoFournisseur.Name = "lblGoFournisseur";
            this.lblGoFournisseur.Size = new System.Drawing.Size(91, 19);
            this.lblGoFournisseur.TabIndex = 0;
            this.lblGoFournisseur.TabStop = true;
            this.lblGoFournisseur.Text = "Fournisseur";
            this.lblGoFournisseur.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoFournisseur_LinkClicked);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(3, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(65, 19);
            this.label5.TabIndex = 397;
            this.label5.Text = "Compte";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(3, 60);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(120, 19);
            this.label6.TabIndex = 397;
            this.label6.Text = "Date de Facture";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(3, 90);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 19);
            this.label7.TabIndex = 397;
            this.label7.Text = "Compte de charge";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(814, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(147, 19);
            this.label9.TabIndex = 397;
            this.label9.Text = "Mode de règlement";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label10.Location = new System.Drawing.Point(814, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(171, 19);
            this.label10.TabIndex = 397;
            this.label10.Text = "--> Compte TVA associé";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(814, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(173, 19);
            this.label8.TabIndex = 397;
            this.label8.Text = "Référence de la facture";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label11.Location = new System.Drawing.Point(1365, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(78, 19);
            this.label11.TabIndex = 397;
            this.label11.Text = "Echéance";
            // 
            // lblNoComptable
            // 
            this.lblNoComptable.AutoSize = true;
            this.lblNoComptable.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNoComptable.Location = new System.Drawing.Point(1365, 90);
            this.lblNoComptable.Name = "lblNoComptable";
            this.lblNoComptable.Size = new System.Drawing.Size(105, 19);
            this.lblNoComptable.TabIndex = 397;
            this.lblNoComptable.Text = "N° Comptable";
            // 
            // txtCompteFournisseur
            // 
            this.txtCompteFournisseur.AccAcceptNumbersOnly = false;
            this.txtCompteFournisseur.AccAllowComma = false;
            this.txtCompteFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCompteFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCompteFournisseur.AccHidenValue = "";
            this.txtCompteFournisseur.AccNotAllowedChars = null;
            this.txtCompteFournisseur.AccReadOnly = false;
            this.txtCompteFournisseur.AccReadOnlyAllowDelete = false;
            this.txtCompteFournisseur.AccRequired = false;
            this.txtCompteFournisseur.BackColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCompteFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCompteFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtCompteFournisseur.Location = new System.Drawing.Point(146, 32);
            this.txtCompteFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompteFournisseur.MaxLength = 32767;
            this.txtCompteFournisseur.Multiline = false;
            this.txtCompteFournisseur.Name = "txtCompteFournisseur";
            this.txtCompteFournisseur.ReadOnly = true;
            this.txtCompteFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCompteFournisseur.Size = new System.Drawing.Size(629, 27);
            this.txtCompteFournisseur.TabIndex = 451;
            this.txtCompteFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCompteFournisseur.UseSystemPasswordChar = false;
            // 
            // txtDateFacture
            // 
            this.txtDateFacture.AccAcceptNumbersOnly = false;
            this.txtDateFacture.AccAllowComma = false;
            this.txtDateFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateFacture.AccHidenValue = "";
            this.txtDateFacture.AccNotAllowedChars = null;
            this.txtDateFacture.AccReadOnly = false;
            this.txtDateFacture.AccReadOnlyAllowDelete = false;
            this.txtDateFacture.AccRequired = false;
            this.txtDateFacture.BackColor = System.Drawing.Color.White;
            this.txtDateFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtDateFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateFacture.ForeColor = System.Drawing.Color.Black;
            this.txtDateFacture.Location = new System.Drawing.Point(146, 62);
            this.txtDateFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateFacture.MaxLength = 32767;
            this.txtDateFacture.Multiline = false;
            this.txtDateFacture.Name = "txtDateFacture";
            this.txtDateFacture.ReadOnly = false;
            this.txtDateFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateFacture.Size = new System.Drawing.Size(629, 27);
            this.txtDateFacture.TabIndex = 452;
            this.txtDateFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateFacture.UseSystemPasswordChar = false;
            this.txtDateFacture.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateFacture_Validating);
            // 
            // TxtNoFacture
            // 
            this.TxtNoFacture.AccAcceptNumbersOnly = false;
            this.TxtNoFacture.AccAllowComma = false;
            this.TxtNoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.TxtNoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TxtNoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TxtNoFacture.AccHidenValue = "";
            this.TxtNoFacture.AccNotAllowedChars = null;
            this.TxtNoFacture.AccReadOnly = false;
            this.TxtNoFacture.AccReadOnlyAllowDelete = false;
            this.TxtNoFacture.AccRequired = false;
            this.TxtNoFacture.BackColor = System.Drawing.Color.White;
            this.TxtNoFacture.CustomBackColor = System.Drawing.Color.White;
            this.TxtNoFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TxtNoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TxtNoFacture.ForeColor = System.Drawing.Color.Black;
            this.TxtNoFacture.Location = new System.Drawing.Point(997, 32);
            this.TxtNoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.TxtNoFacture.MaxLength = 32767;
            this.TxtNoFacture.Multiline = false;
            this.TxtNoFacture.Name = "TxtNoFacture";
            this.TxtNoFacture.ReadOnly = false;
            this.TxtNoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TxtNoFacture.Size = new System.Drawing.Size(363, 27);
            this.TxtNoFacture.TabIndex = 454;
            this.TxtNoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TxtNoFacture.UseSystemPasswordChar = false;
            // 
            // txtDateEcheance
            // 
            this.txtDateEcheance.AccAcceptNumbersOnly = false;
            this.txtDateEcheance.AccAllowComma = false;
            this.txtDateEcheance.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateEcheance.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateEcheance.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateEcheance.AccHidenValue = "";
            this.txtDateEcheance.AccNotAllowedChars = null;
            this.txtDateEcheance.AccReadOnly = false;
            this.txtDateEcheance.AccReadOnlyAllowDelete = false;
            this.txtDateEcheance.AccRequired = false;
            this.txtDateEcheance.BackColor = System.Drawing.Color.White;
            this.txtDateEcheance.CustomBackColor = System.Drawing.Color.White;
            this.txtDateEcheance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateEcheance.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateEcheance.ForeColor = System.Drawing.Color.Black;
            this.txtDateEcheance.Location = new System.Drawing.Point(1477, 62);
            this.txtDateEcheance.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateEcheance.MaxLength = 32767;
            this.txtDateEcheance.Multiline = false;
            this.txtDateEcheance.Name = "txtDateEcheance";
            this.txtDateEcheance.ReadOnly = false;
            this.txtDateEcheance.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateEcheance.Size = new System.Drawing.Size(365, 27);
            this.txtDateEcheance.TabIndex = 457;
            this.txtDateEcheance.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateEcheance.UseSystemPasswordChar = false;
            // 
            // cmbCptCharges
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCptCharges.DisplayLayout.Appearance = appearance1;
            this.cmbCptCharges.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCptCharges.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCptCharges.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCptCharges.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbCptCharges.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCptCharges.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbCptCharges.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCptCharges.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCptCharges.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCptCharges.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbCptCharges.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCptCharges.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCptCharges.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCptCharges.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbCptCharges.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCptCharges.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCptCharges.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbCptCharges.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbCptCharges.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCptCharges.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbCptCharges.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbCptCharges.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCptCharges.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbCptCharges.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCptCharges.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCptCharges.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCptCharges.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCptCharges.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbCptCharges.Location = new System.Drawing.Point(147, 93);
            this.cmbCptCharges.Name = "cmbCptCharges";
            this.cmbCptCharges.Size = new System.Drawing.Size(627, 27);
            this.cmbCptCharges.TabIndex = 453;
            this.cmbCptCharges.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.cmbCptCharges_InitializeLayout);
            this.cmbCptCharges.AfterCloseUp += new System.EventHandler(this.cmbCptCharges_AfterCloseUp);
            // 
            // lblModeReglement
            // 
            this.lblModeReglement.AccAcceptNumbersOnly = false;
            this.lblModeReglement.AccAllowComma = false;
            this.lblModeReglement.AccBackgroundColor = System.Drawing.Color.White;
            this.lblModeReglement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblModeReglement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblModeReglement.AccHidenValue = "";
            this.lblModeReglement.AccNotAllowedChars = null;
            this.lblModeReglement.AccReadOnly = false;
            this.lblModeReglement.AccReadOnlyAllowDelete = false;
            this.lblModeReglement.AccRequired = false;
            this.lblModeReglement.BackColor = System.Drawing.Color.White;
            this.lblModeReglement.CustomBackColor = System.Drawing.Color.White;
            this.lblModeReglement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblModeReglement.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblModeReglement.ForeColor = System.Drawing.Color.Black;
            this.lblModeReglement.Location = new System.Drawing.Point(997, 62);
            this.lblModeReglement.Margin = new System.Windows.Forms.Padding(2);
            this.lblModeReglement.MaxLength = 32767;
            this.lblModeReglement.Multiline = false;
            this.lblModeReglement.Name = "lblModeReglement";
            this.lblModeReglement.ReadOnly = true;
            this.lblModeReglement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblModeReglement.Size = new System.Drawing.Size(363, 27);
            this.lblModeReglement.TabIndex = 455;
            this.lblModeReglement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblModeReglement.UseSystemPasswordChar = false;
            // 
            // lblTauxTva
            // 
            this.lblTauxTva.AccAcceptNumbersOnly = false;
            this.lblTauxTva.AccAllowComma = false;
            this.lblTauxTva.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTauxTva.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTauxTva.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTauxTva.AccHidenValue = "";
            this.lblTauxTva.AccNotAllowedChars = null;
            this.lblTauxTva.AccReadOnly = false;
            this.lblTauxTva.AccReadOnlyAllowDelete = false;
            this.lblTauxTva.AccRequired = false;
            this.lblTauxTva.BackColor = System.Drawing.Color.White;
            this.lblTauxTva.CustomBackColor = System.Drawing.Color.White;
            this.lblTauxTva.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTauxTva.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblTauxTva.ForeColor = System.Drawing.Color.Black;
            this.lblTauxTva.Location = new System.Drawing.Point(997, 92);
            this.lblTauxTva.Margin = new System.Windows.Forms.Padding(2);
            this.lblTauxTva.MaxLength = 32767;
            this.lblTauxTva.Multiline = false;
            this.lblTauxTva.Name = "lblTauxTva";
            this.lblTauxTva.ReadOnly = true;
            this.lblTauxTva.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTauxTva.Size = new System.Drawing.Size(363, 27);
            this.lblTauxTva.TabIndex = 456;
            this.lblTauxTva.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTauxTva.UseSystemPasswordChar = false;
            // 
            // txtNoComptable
            // 
            this.txtNoComptable.AccAcceptNumbersOnly = false;
            this.txtNoComptable.AccAllowComma = false;
            this.txtNoComptable.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoComptable.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoComptable.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoComptable.AccHidenValue = "";
            this.txtNoComptable.AccNotAllowedChars = null;
            this.txtNoComptable.AccReadOnly = false;
            this.txtNoComptable.AccReadOnlyAllowDelete = false;
            this.txtNoComptable.AccRequired = false;
            this.txtNoComptable.BackColor = System.Drawing.Color.White;
            this.txtNoComptable.CustomBackColor = System.Drawing.Color.White;
            this.txtNoComptable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoComptable.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoComptable.ForeColor = System.Drawing.Color.Black;
            this.txtNoComptable.Location = new System.Drawing.Point(1477, 92);
            this.txtNoComptable.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoComptable.MaxLength = 32767;
            this.txtNoComptable.Multiline = false;
            this.txtNoComptable.Name = "txtNoComptable";
            this.txtNoComptable.ReadOnly = true;
            this.txtNoComptable.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoComptable.Size = new System.Drawing.Size(365, 27);
            this.txtNoComptable.TabIndex = 458;
            this.txtNoComptable.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoComptable.UseSystemPasswordChar = false;
            // 
            // chkIntegration
            // 
            this.chkIntegration.AutoSize = true;
            this.chkIntegration.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkIntegration.Location = new System.Drawing.Point(1478, 33);
            this.chkIntegration.Name = "chkIntegration";
            this.chkIntegration.Size = new System.Drawing.Size(155, 23);
            this.chkIntegration.TabIndex = 570;
            this.chkIntegration.Text = "Intégrer la facture";
            this.chkIntegration.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 144F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.731F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 248F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.269F));
            this.tableLayoutPanel4.Controls.Add(this.ssFactFournHtTvaCpta, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtCommentaire, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdfactScann, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.label13, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.chkAjusteMtTVA, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtTTC, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.label14, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.cmbCptChargesBis, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 288);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1844, 87);
            this.tableLayoutPanel4.TabIndex = 3;
            // 
            // ssFactFournHtTvaCpta
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            appearance13.TextHAlignAsString = "Center";
            this.ssFactFournHtTvaCpta.DisplayLayout.Appearance = appearance13;
            this.ssFactFournHtTvaCpta.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssFactFournHtTvaCpta.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance14.TextHAlignAsString = "Center";
            this.ssFactFournHtTvaCpta.DisplayLayout.CaptionAppearance = appearance14;
            this.ssFactFournHtTvaCpta.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance15.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance15.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance15.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance15.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFactFournHtTvaCpta.DisplayLayout.GroupByBox.Appearance = appearance15;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFactFournHtTvaCpta.DisplayLayout.GroupByBox.BandLabelAppearance = appearance16;
            this.ssFactFournHtTvaCpta.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance17.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance17.BackColor2 = System.Drawing.SystemColors.Control;
            appearance17.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance17.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFactFournHtTvaCpta.DisplayLayout.GroupByBox.PromptAppearance = appearance17;
            this.ssFactFournHtTvaCpta.DisplayLayout.MaxColScrollRegions = 1;
            this.ssFactFournHtTvaCpta.DisplayLayout.MaxRowScrollRegions = 1;
            appearance18.BackColor = System.Drawing.SystemColors.Window;
            appearance18.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.ActiveCellAppearance = appearance18;
            appearance19.BackColor = System.Drawing.SystemColors.Highlight;
            appearance19.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.ActiveRowAppearance = appearance19;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance20.BackColor = System.Drawing.SystemColors.Window;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.CardAreaAppearance = appearance20;
            appearance21.BorderColor = System.Drawing.Color.Silver;
            appearance21.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.CellAppearance = appearance21;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.CellPadding = 0;
            appearance22.BackColor = System.Drawing.SystemColors.Control;
            appearance22.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance22.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance22.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance22.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.GroupByRowAppearance = appearance22;
            appearance23.TextHAlignAsString = "Left";
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.HeaderAppearance = appearance23;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance24.BackColor = System.Drawing.SystemColors.Window;
            appearance24.BorderColor = System.Drawing.Color.Silver;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.RowAppearance = appearance24;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssFactFournHtTvaCpta.DisplayLayout.Override.TemplateAddRowAppearance = appearance25;
            this.ssFactFournHtTvaCpta.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssFactFournHtTvaCpta.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssFactFournHtTvaCpta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssFactFournHtTvaCpta.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssFactFournHtTvaCpta.Location = new System.Drawing.Point(1059, 3);
            this.ssFactFournHtTvaCpta.Name = "ssFactFournHtTvaCpta";
            this.ssFactFournHtTvaCpta.Size = new System.Drawing.Size(782, 45);
            this.ssFactFournHtTvaCpta.TabIndex = 572;
            this.ssFactFournHtTvaCpta.Text = "ultraGrid1";
            this.ssFactFournHtTvaCpta.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssFactFournHtTvaCpta_InitializeLayout);
            this.ssFactFournHtTvaCpta.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssFactFournHtTvaCpta_BeforeRowsDeleted);
            // 
            // txtCommentaire
            // 
            this.txtCommentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaire.AutoWordSelection = false;
            this.txtCommentaire.BackColor = System.Drawing.Color.Transparent;
            this.txtCommentaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCommentaire.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaire.Location = new System.Drawing.Point(147, 3);
            this.txtCommentaire.Name = "txtCommentaire";
            this.txtCommentaire.ReadOnly = false;
            this.txtCommentaire.Size = new System.Drawing.Size(658, 45);
            this.txtCommentaire.TabIndex = 571;
            this.txtCommentaire.WordWrap = true;
            // 
            // cmdfactScann
            // 
            this.cmdfactScann.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdfactScann.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdfactScann.FlatAppearance.BorderSize = 0;
            this.cmdfactScann.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdfactScann.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdfactScann.ForeColor = System.Drawing.Color.White;
            this.cmdfactScann.Location = new System.Drawing.Point(147, 54);
            this.cmdfactScann.Name = "cmdfactScann";
            this.cmdfactScann.Size = new System.Drawing.Size(658, 30);
            this.cmdfactScann.TabIndex = 364;
            this.cmdfactScann.Tag = "2";
            this.cmdfactScann.Text = "Fact. Fournisseurs scannées";
            this.cmdfactScann.UseVisualStyleBackColor = false;
            this.cmdfactScann.Click += new System.EventHandler(this.cmdfactScann_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label13.Location = new System.Drawing.Point(3, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 19);
            this.label13.TabIndex = 397;
            this.label13.Text = "Commentaire";
            // 
            // chkAjusteMtTVA
            // 
            this.chkAjusteMtTVA.AutoSize = true;
            this.chkAjusteMtTVA.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkAjusteMtTVA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkAjusteMtTVA.Location = new System.Drawing.Point(811, 3);
            this.chkAjusteMtTVA.Name = "chkAjusteMtTVA";
            this.chkAjusteMtTVA.Size = new System.Drawing.Size(220, 45);
            this.chkAjusteMtTVA.TabIndex = 570;
            this.chkAjusteMtTVA.Text = "Ajuster les montants de TVA";
            this.chkAjusteMtTVA.UseVisualStyleBackColor = true;
            this.chkAjusteMtTVA.CheckedChanged += new System.EventHandler(this.chkAjusteMtTVA_CheckedChanged);
            // 
            // txtTTC
            // 
            this.txtTTC.AccAcceptNumbersOnly = false;
            this.txtTTC.AccAllowComma = false;
            this.txtTTC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTTC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTTC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTTC.AccHidenValue = "";
            this.txtTTC.AccNotAllowedChars = null;
            this.txtTTC.AccReadOnly = false;
            this.txtTTC.AccReadOnlyAllowDelete = false;
            this.txtTTC.AccRequired = false;
            this.txtTTC.BackColor = System.Drawing.Color.White;
            this.txtTTC.CustomBackColor = System.Drawing.Color.White;
            this.txtTTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTTC.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTTC.ForeColor = System.Drawing.Color.Black;
            this.txtTTC.Location = new System.Drawing.Point(1058, 53);
            this.txtTTC.Margin = new System.Windows.Forms.Padding(2);
            this.txtTTC.MaxLength = 32767;
            this.txtTTC.Multiline = false;
            this.txtTTC.Name = "txtTTC";
            this.txtTTC.ReadOnly = true;
            this.txtTTC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTTC.Size = new System.Drawing.Size(784, 27);
            this.txtTTC.TabIndex = 573;
            this.txtTTC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTTC.UseSystemPasswordChar = false;
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label14.Location = new System.Drawing.Point(1016, 51);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(37, 19);
            this.label14.TabIndex = 397;
            this.label14.Text = "TTC";
            // 
            // cmbCptChargesBis
            // 
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCptChargesBis.DisplayLayout.Appearance = appearance26;
            this.cmbCptChargesBis.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCptChargesBis.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance27.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance27.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance27.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance27.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCptChargesBis.DisplayLayout.GroupByBox.Appearance = appearance27;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCptChargesBis.DisplayLayout.GroupByBox.BandLabelAppearance = appearance28;
            this.cmbCptChargesBis.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance29.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance29.BackColor2 = System.Drawing.SystemColors.Control;
            appearance29.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance29.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCptChargesBis.DisplayLayout.GroupByBox.PromptAppearance = appearance29;
            this.cmbCptChargesBis.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCptChargesBis.DisplayLayout.MaxRowScrollRegions = 1;
            appearance30.BackColor = System.Drawing.SystemColors.Window;
            appearance30.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCptChargesBis.DisplayLayout.Override.ActiveCellAppearance = appearance30;
            appearance31.BackColor = System.Drawing.SystemColors.Highlight;
            appearance31.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCptChargesBis.DisplayLayout.Override.ActiveRowAppearance = appearance31;
            this.cmbCptChargesBis.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCptChargesBis.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCptChargesBis.DisplayLayout.Override.CardAreaAppearance = appearance32;
            appearance33.BorderColor = System.Drawing.Color.Silver;
            appearance33.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCptChargesBis.DisplayLayout.Override.CellAppearance = appearance33;
            this.cmbCptChargesBis.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCptChargesBis.DisplayLayout.Override.CellPadding = 0;
            appearance34.BackColor = System.Drawing.SystemColors.Control;
            appearance34.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance34.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance34.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance34.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCptChargesBis.DisplayLayout.Override.GroupByRowAppearance = appearance34;
            appearance35.TextHAlignAsString = "Left";
            this.cmbCptChargesBis.DisplayLayout.Override.HeaderAppearance = appearance35;
            this.cmbCptChargesBis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCptChargesBis.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance36.BackColor = System.Drawing.SystemColors.Window;
            appearance36.BorderColor = System.Drawing.Color.Silver;
            this.cmbCptChargesBis.DisplayLayout.Override.RowAppearance = appearance36;
            this.cmbCptChargesBis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCptChargesBis.DisplayLayout.Override.TemplateAddRowAppearance = appearance37;
            this.cmbCptChargesBis.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCptChargesBis.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCptChargesBis.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCptChargesBis.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbCptChargesBis.Location = new System.Drawing.Point(3, 54);
            this.cmbCptChargesBis.Name = "cmbCptChargesBis";
            this.cmbCptChargesBis.Size = new System.Drawing.Size(90, 27);
            this.cmbCptChargesBis.TabIndex = 578;
            this.cmbCptChargesBis.Visible = false;
            this.cmbCptChargesBis.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.cmbCptChargesBis_InitializeLayout);
            this.cmbCptChargesBis.AfterCloseUp += new System.EventHandler(this.cmbCptChargesBis_AfterCloseUp);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.16495F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.83505F));
            this.tableLayoutPanel5.Controls.Add(this.GridFactFournPied, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(2, 380);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1846, 125);
            this.tableLayoutPanel5.TabIndex = 574;
            // 
            // GridFactFournPied
            // 
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            appearance38.TextHAlignAsString = "Center";
            this.GridFactFournPied.DisplayLayout.Appearance = appearance38;
            this.GridFactFournPied.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridFactFournPied.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance39.TextHAlignAsString = "Center";
            this.GridFactFournPied.DisplayLayout.CaptionAppearance = appearance39;
            this.GridFactFournPied.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.GroupByBox.Appearance = appearance40;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournPied.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.GridFactFournPied.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournPied.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.GridFactFournPied.DisplayLayout.MaxColScrollRegions = 1;
            this.GridFactFournPied.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridFactFournPied.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridFactFournPied.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.GridFactFournPied.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridFactFournPied.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridFactFournPied.DisplayLayout.Override.CellAppearance = appearance46;
            this.GridFactFournPied.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridFactFournPied.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance48.TextHAlignAsString = "Left";
            this.GridFactFournPied.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.GridFactFournPied.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridFactFournPied.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            this.GridFactFournPied.DisplayLayout.Override.RowAppearance = appearance49;
            this.GridFactFournPied.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridFactFournPied.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridFactFournPied.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.GridFactFournPied.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridFactFournPied.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridFactFournPied.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridFactFournPied.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridFactFournPied.Location = new System.Drawing.Point(873, 3);
            this.GridFactFournPied.Name = "GridFactFournPied";
            this.GridFactFournPied.Size = new System.Drawing.Size(970, 119);
            this.GridFactFournPied.TabIndex = 573;
            this.GridFactFournPied.Text = "ultraGrid1";
            this.GridFactFournPied.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridFactFournPied_InitializeLayout);
            this.GridFactFournPied.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridFactFournPied_BeforeExitEditMode);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel10, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(864, 119);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.15493F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.84507F));
            this.tableLayoutPanel7.Controls.Add(this.txtNoBcmd, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 39);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(858, 43);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // txtNoBcmd
            // 
            this.txtNoBcmd.AccAcceptNumbersOnly = false;
            this.txtNoBcmd.AccAllowComma = false;
            this.txtNoBcmd.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBcmd.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBcmd.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBcmd.AccHidenValue = "";
            this.txtNoBcmd.AccNotAllowedChars = null;
            this.txtNoBcmd.AccReadOnly = false;
            this.txtNoBcmd.AccReadOnlyAllowDelete = false;
            this.txtNoBcmd.AccRequired = false;
            this.txtNoBcmd.BackColor = System.Drawing.Color.White;
            this.txtNoBcmd.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBcmd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBcmd.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoBcmd.ForeColor = System.Drawing.Color.Black;
            this.txtNoBcmd.Location = new System.Drawing.Point(509, 2);
            this.txtNoBcmd.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBcmd.MaxLength = 32767;
            this.txtNoBcmd.Multiline = false;
            this.txtNoBcmd.Name = "txtNoBcmd";
            this.txtNoBcmd.ReadOnly = false;
            this.txtNoBcmd.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBcmd.Size = new System.Drawing.Size(347, 27);
            this.txtNoBcmd.TabIndex = 503;
            this.txtNoBcmd.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBcmd.UseSystemPasswordChar = false;
            this.txtNoBcmd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoBcmd_KeyPress);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label15.Location = new System.Drawing.Point(3, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(440, 19);
            this.label15.TabIndex = 397;
            this.label15.Text = "Ajouter le bon de commande N° : (Valider par la touche Entrée)";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 139F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.cmdAvoir, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.cmdEnleverUnBcmd, 2, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 85);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(864, 43);
            this.tableLayoutPanel8.TabIndex = 365;
            // 
            // cmdAvoir
            // 
            this.cmdAvoir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdAvoir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAvoir.FlatAppearance.BorderSize = 0;
            this.cmdAvoir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAvoir.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdAvoir.ForeColor = System.Drawing.Color.White;
            this.cmdAvoir.Location = new System.Drawing.Point(142, 3);
            this.cmdAvoir.Name = "cmdAvoir";
            this.cmdAvoir.Size = new System.Drawing.Size(356, 30);
            this.cmdAvoir.TabIndex = 364;
            this.cmdAvoir.Tag = "2";
            this.cmdAvoir.Text = "Ajouter une ligne";
            this.cmdAvoir.UseVisualStyleBackColor = false;
            this.cmdAvoir.Click += new System.EventHandler(this.cmdAvoir_Click);
            // 
            // cmdEnleverUnBcmd
            // 
            this.cmdEnleverUnBcmd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdEnleverUnBcmd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEnleverUnBcmd.FlatAppearance.BorderSize = 0;
            this.cmdEnleverUnBcmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEnleverUnBcmd.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdEnleverUnBcmd.ForeColor = System.Drawing.Color.White;
            this.cmdEnleverUnBcmd.Location = new System.Drawing.Point(504, 3);
            this.cmdEnleverUnBcmd.Name = "cmdEnleverUnBcmd";
            this.cmdEnleverUnBcmd.Size = new System.Drawing.Size(357, 30);
            this.cmdEnleverUnBcmd.TabIndex = 364;
            this.cmdEnleverUnBcmd.Tag = "";
            this.cmdEnleverUnBcmd.Text = "Enlever un bon de commande";
            this.cmdEnleverUnBcmd.UseVisualStyleBackColor = false;
            this.cmdEnleverUnBcmd.Click += new System.EventHandler(this.cmdEnleverUnBcmd_Click);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.cmdRechercheBCMD, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(864, 36);
            this.tableLayoutPanel10.TabIndex = 366;
            // 
            // cmdRechercheBCMD
            // 
            this.cmdRechercheBCMD.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRechercheBCMD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheBCMD.FlatAppearance.BorderSize = 0;
            this.cmdRechercheBCMD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheBCMD.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdRechercheBCMD.ForeColor = System.Drawing.Color.White;
            this.cmdRechercheBCMD.Location = new System.Drawing.Point(146, 3);
            this.cmdRechercheBCMD.Name = "cmdRechercheBCMD";
            this.cmdRechercheBCMD.Size = new System.Drawing.Size(715, 30);
            this.cmdRechercheBCMD.TabIndex = 365;
            this.cmdRechercheBCMD.Tag = "2";
            this.cmdRechercheBCMD.Text = "Insérer des bons de commande";
            this.cmdRechercheBCMD.UseVisualStyleBackColor = false;
            this.cmdRechercheBCMD.Click += new System.EventHandler(this.cmdRechercheBCMD_Click);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.lblNoFacture, 0, 0);
            this.tableLayoutPanel9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 51);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(1844, 23);
            this.tableLayoutPanel9.TabIndex = 575;
            // 
            // lblNoFacture
            // 
            this.lblNoFacture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblNoFacture.AutoSize = true;
            this.lblNoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNoFacture.Location = new System.Drawing.Point(3, 2);
            this.lblNoFacture.Name = "lblNoFacture";
            this.lblNoFacture.Size = new System.Drawing.Size(1838, 19);
            this.lblNoFacture.TabIndex = 0;
            this.lblNoFacture.Text = "Facture N°";
            this.lblNoFacture.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // GridFactFournDetail
            // 
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            appearance51.TextHAlignAsString = "Center";
            this.GridFactFournDetail.DisplayLayout.Appearance = appearance51;
            this.GridFactFournDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridFactFournDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.TextHAlignAsString = "Center";
            this.GridFactFournDetail.DisplayLayout.CaptionAppearance = appearance52;
            this.GridFactFournDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance53.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance53.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance53.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance53.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.Appearance = appearance53;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance54;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance55.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance55.BackColor2 = System.Drawing.SystemColors.Control;
            appearance55.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance55.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance55;
            this.GridFactFournDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.GridFactFournDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            appearance56.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridFactFournDetail.DisplayLayout.Override.ActiveCellAppearance = appearance56;
            appearance57.BackColor = System.Drawing.SystemColors.Highlight;
            appearance57.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridFactFournDetail.DisplayLayout.Override.ActiveRowAppearance = appearance57;
            this.GridFactFournDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridFactFournDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.Override.CardAreaAppearance = appearance58;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            appearance59.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridFactFournDetail.DisplayLayout.Override.CellAppearance = appearance59;
            this.GridFactFournDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridFactFournDetail.DisplayLayout.Override.CellPadding = 0;
            appearance60.BackColor = System.Drawing.SystemColors.Control;
            appearance60.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance60.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance60.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance60.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.Override.GroupByRowAppearance = appearance60;
            appearance61.TextHAlignAsString = "Left";
            this.GridFactFournDetail.DisplayLayout.Override.HeaderAppearance = appearance61;
            this.GridFactFournDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridFactFournDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance62.BackColor = System.Drawing.SystemColors.Window;
            appearance62.BorderColor = System.Drawing.Color.Silver;
            this.GridFactFournDetail.DisplayLayout.Override.RowAppearance = appearance62;
            this.GridFactFournDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridFactFournDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance63.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridFactFournDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance63;
            this.GridFactFournDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridFactFournDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridFactFournDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridFactFournDetail.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridFactFournDetail.Location = new System.Drawing.Point(3, 510);
            this.GridFactFournDetail.Name = "GridFactFournDetail";
            this.GridFactFournDetail.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.GridFactFournDetail.Size = new System.Drawing.Size(1844, 444);
            this.GridFactFournDetail.TabIndex = 573;
            this.GridFactFournDetail.Text = "ultraGrid1";
            this.GridFactFournDetail.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridFactFournDetail_InitializeLayout);
            this.GridFactFournDetail.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridFactFournDetail_BeforeRowUpdate);
            this.GridFactFournDetail.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridFactFournDetail_BeforeExitEditMode);
            // 
            // UserDocFactFourn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocFactFourn";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "UserDocFactFourn";
            this.VisibleChanged += new System.EventHandler(this.UserDocFactFourn_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCptCharges)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssFactFournHtTvaCpta)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCptChargesBis)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournPied)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label lblDateCreation;
        public System.Windows.Forms.Label lblDateModif;
        public System.Windows.Forms.Label lblCreateur;
        public System.Windows.Forms.Label lblNomModif;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.LinkLabel lblGoFournisseur;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label lblNoComptable;
        public iTalk.iTalk_TextBox_Small2 txtRaisonFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtCompteFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtDateFacture;
        public iTalk.iTalk_TextBox_Small2 TxtNoFacture;
        public iTalk.iTalk_TextBox_Small2 txtNoComptable;
        public iTalk.iTalk_TextBox_Small2 txtDateEcheance;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCptCharges;
        public iTalk.iTalk_TextBox_Small2 lblModeReglement;
        public iTalk.iTalk_TextBox_Small2 lblTauxTva;
        public System.Windows.Forms.Button cmdRechercheRaisonSociale;
        public System.Windows.Forms.CheckBox chkIntegration;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Label label13;
        public System.Windows.Forms.CheckBox chkAjusteMtTVA;
        public iTalk.iTalk_RichTextBox txtCommentaire;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssFactFournHtTvaCpta;
        public iTalk.iTalk_TextBox_Small2 txtTTC;
        public System.Windows.Forms.Label label14;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridFactFournDetail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridFactFournPied;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public iTalk.iTalk_TextBox_Small2 txtNoBcmd;
        public System.Windows.Forms.Label label15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.Button cmdAvoir;
        public System.Windows.Forms.Button cmdEnleverUnBcmd;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button Cmd17;
        public System.Windows.Forms.Button cmdImprimer;
        public System.Windows.Forms.Button cmdfactScann;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        public iTalk.iTalk_TextBox_Small2 txtEcheance;
        public iTalk.iTalk_TextBox_Small2 txtMontantFacture;
        public iTalk.iTalk_TextBox_Small2 txtNoAutoFacture;
        public iTalk.iTalk_TextBox_Small2 txtCodeFourn;
        public iTalk.iTalk_TextBox_Small2 txtCodeFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtVerrou;
        public iTalk.iTalk_TextBox_Small2 txtModeRgt;
        public iTalk.iTalk_TextBox_Small2 txtTauxTVA;
        public iTalk.iTalk_TextBox_Small2 txtNbreJour;
        public iTalk.iTalk_TextBox_Small2 txtTypeEcheance;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label lblNoFacture;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCptChargesBis;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public System.Windows.Forms.Button cmdRechercheBCMD;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
