﻿namespace Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs.Forms
{
    partial class frmModeReglement
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.lblNomFournisseur = new System.Windows.Forms.Label();
            this.txtCleAuto = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.ComboModeRgt = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ComboModeRgt)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(12, 66);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(243, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Sélection du mode de règlement :";
            // 
            // lblNomFournisseur
            // 
            this.lblNomFournisseur.AutoSize = true;
            this.lblNomFournisseur.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNomFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNomFournisseur.Location = new System.Drawing.Point(102, 19);
            this.lblNomFournisseur.Name = "lblNomFournisseur";
            this.lblNomFournisseur.Size = new System.Drawing.Size(2, 21);
            this.lblNomFournisseur.TabIndex = 396;
            // 
            // txtCleAuto
            // 
            this.txtCleAuto.AccAcceptNumbersOnly = false;
            this.txtCleAuto.AccAllowComma = false;
            this.txtCleAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCleAuto.AccHidenValue = "";
            this.txtCleAuto.AccNotAllowedChars = null;
            this.txtCleAuto.AccReadOnly = false;
            this.txtCleAuto.AccReadOnlyAllowDelete = false;
            this.txtCleAuto.AccRequired = false;
            this.txtCleAuto.BackColor = System.Drawing.Color.Transparent;
            this.txtCleAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtCleAuto.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCleAuto.ForeColor = System.Drawing.Color.Black;
            this.txtCleAuto.Location = new System.Drawing.Point(15, 39);
            this.txtCleAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCleAuto.MaxLength = 32767;
            this.txtCleAuto.Multiline = false;
            this.txtCleAuto.Name = "txtCleAuto";
            this.txtCleAuto.ReadOnly = false;
            this.txtCleAuto.Size = new System.Drawing.Size(89, 23);
            this.txtCleAuto.TabIndex = 502;
            this.txtCleAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCleAuto.UseSystemPasswordChar = false;
            this.txtCleAuto.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(11, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 19);
            this.label1.TabIndex = 384;
            this.label1.Text = "Fournisseur :";
            // 
            // ComboModeRgt
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ComboModeRgt.DisplayLayout.Appearance = appearance1;
            this.ComboModeRgt.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ComboModeRgt.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboModeRgt.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboModeRgt.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ComboModeRgt.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboModeRgt.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ComboModeRgt.DisplayLayout.MaxColScrollRegions = 1;
            this.ComboModeRgt.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ComboModeRgt.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ComboModeRgt.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ComboModeRgt.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ComboModeRgt.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ComboModeRgt.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ComboModeRgt.DisplayLayout.Override.CellAppearance = appearance8;
            this.ComboModeRgt.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ComboModeRgt.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboModeRgt.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ComboModeRgt.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ComboModeRgt.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ComboModeRgt.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ComboModeRgt.DisplayLayout.Override.RowAppearance = appearance11;
            this.ComboModeRgt.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ComboModeRgt.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ComboModeRgt.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ComboModeRgt.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ComboModeRgt.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ComboModeRgt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ComboModeRgt.Location = new System.Drawing.Point(261, 60);
            this.ComboModeRgt.Name = "ComboModeRgt";
            this.ComboModeRgt.Size = new System.Drawing.Size(186, 27);
            this.ComboModeRgt.TabIndex = 503;
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(464, 52);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 504;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // frmModeReglement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(532, 99);
            this.Controls.Add(this.CmdSauver);
            this.Controls.Add(this.ComboModeRgt);
            this.Controls.Add(this.txtCleAuto);
            this.Controls.Add(this.lblNomFournisseur);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.MaximumSize = new System.Drawing.Size(548, 138);
            this.MinimumSize = new System.Drawing.Size(548, 138);
            this.Name = "frmModeReglement";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmModeReglement";
            this.Load += new System.EventHandler(this.frmModeReglement_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ComboModeRgt)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lblNomFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtCleAuto;
        public System.Windows.Forms.Label label1;
        public Infragistics.Win.UltraWinGrid.UltraCombo ComboModeRgt;
        public System.Windows.Forms.Button CmdSauver;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}