﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs.Forms
{
    public partial class frmRechercheBcmdFacture : Form
    {
        public frmRechercheBcmdFacture()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            chkAnnuler.CheckState = System.Windows.Forms.CheckState.Checked;
            this.Visible = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFermer_Click(object sender, EventArgs e)
        {
            GridBCmd.UpdateData();//TODO grid refresh
            chkAnnuler.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.Visible = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_ChargeBcmd()
        {
            object functionReturnValue = null;
            DataTable rsBcmd = default(DataTable);
            int i = 0;


            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (string.IsNullOrEmpty(txtSelect.Text))
                {
                    General.sSQL = "SELECT DISTINCT NoBonDeCommande,BonDeCommande.CodeImmeuble as [Immeuble],Estimation,Acheteur,ProduitTotalHT,CodeFourn FROM BonDeCommande ";
                    General.sSQL = General.sSQL + " LEFT JOIN FactFournPied ON BonDeCommande.NoBonDeCommande=FactFournPied.NoBCmd ";
                }
                else
                {
                    General.sSQL = txtSelect.Text;
                }
                General.sSQL = General.sSQL + " " + txtWhere.Text;

                if (GridBCmd.Rows.Count > 0)
                {
                    GridBCmd.Rows.ToList().ForEach(l => l.Delete(false));
                }

                if (rsBcmd == null)
                {
                    rsBcmd = new DataTable();
                }
                var ModAdo = new ModAdo();
                rsBcmd = ModAdo.fc_OpenRecordSet(General.sSQL);
                if (rsBcmd.Rows.Count > 0)
                {
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Selectionner", typeof(byte));
                    dt.Columns.Add("NoBonDeCommande");
                    dt.Columns.Add("Immeuble");
                    dt.Columns.Add("Montant");
                    dt.Columns.Add("Acheteur");
                    foreach (DataRow dr in rsBcmd.Rows)
                    {
                        if (chkEnleveDoublon.Checked == true)
                        {
                            if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT NoBCmd FROM FactFournPied WHERE NoBCmd=" + dr["NoBonDeCommande"] + " AND FactFournPied.NoAutoFacture=" + txtNoAutoFacture.Text), "") + ""))
                            {
                                if (General.nz(dr["ProduitTotalHT"], "0") + "" == "0")
                                {
                                    dt.Rows.Add(0, Convert.ToInt32(dr["NoBonDeCommande"] + ""), dr["Immeuble"] + "", General.nz(dr["Estimation"], "0") + "", dr["Acheteur"] + "");
                                }
                                else
                                {
                                    dt.Rows.Add(0, Convert.ToInt32(dr["NoBonDeCommande"] + ""), dr["Immeuble"] + "", General.nz(dr["ProduitTotalHT"], "0") + "", dr["Acheteur"] + "");
                                }
                            }
                        }
                        else
                        {
                            if (General.nz(dr["ProduitTotalHT"], "0") + "" == "0")
                            {
                                dt.Rows.Add(0, Convert.ToInt32(dr["NoBonDeCommande"] + ""), dr["Immeuble"] + "", General.nz(dr["Estimation"], "0") + "", dr["Acheteur"] + "");
                            }
                            else
                            {
                                dt.Rows.Add(0, Convert.ToInt32(dr["NoBonDeCommande"] + ""), dr["Immeuble"] + "", General.nz(dr["ProduitTotalHT"], "0") + "", dr["Acheteur"] + "");
                            }
                        }
                        System.Windows.Forms.Application.DoEvents();
                    }
                    GridBCmd.DataSource = dt;
                }
                rsBcmd = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                //Debug.Print(Err().Description);
                Erreurs.gFr_debug(ex, " Recherche des bons de commande à associer à une facture fournisseur ", true);
                return functionReturnValue;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmRechercheBcmdFacture_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            //Prépare le tableau qui permet de récupérer les bons de commande à ajouter
            General.SelectBcmdDetail = new string[2];
            fc_ChargeBcmd();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCmd_AfterExitEditMode(object sender, EventArgs e)
        {
            if (GridBCmd.ActiveCell.Column.Index == 0) //TODO
            {
                    if (General.SelectBcmdDetail.Length <= GridBCmd.ActiveRow.Index)
                    {
                        //Array.Resize(ref General.SelectBcmdDetail, GridBCmd.ActiveRow.Index);
                        Array.Resize(ref General.SelectBcmdDetail, GridBCmd.ActiveRow.Index+1);
                    }
                if (Convert.ToBoolean(GridBCmd.ActiveCell.Value) == false)
                {
                    General.SelectBcmdDetail[GridBCmd.ActiveRow.Index] = "";
                }
                else if (GridBCmd.ActiveRow.Index != Convert.ToDouble("-1"))
                {
                    General.SelectBcmdDetail[GridBCmd.ActiveRow.Index] = GridBCmd.ActiveRow.Cells[1].Value + "";
                }
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCmd_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns[0].Style = ColumnStyle.CheckBox;
        }
    }
}
