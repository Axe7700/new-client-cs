﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
namespace Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs.Forms
{
    public partial class frmModeReglement : Form
    {
        public frmModeReglement()
        {
            InitializeComponent();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmModeReglement_Load(object sender, EventArgs e)
        {
            DataTable rsModeRgt = new DataTable();
            var ModAdo = new ModAdo();
            rsModeRgt = ModAdo.fc_OpenRecordSet("SELECT * FROM CodeReglement order by Code");
            if (rsModeRgt.Rows.Count > 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("Code");
                dt.Columns.Add("Libelle");
                foreach(DataRow dr in rsModeRgt.Rows)
                {
                    dt.Rows.Add(dr["Code"], dr["Libelle"]);
                }
                ComboModeRgt.ValueMember = "Code";
                ComboModeRgt.DataSource = dt;
            }
            ModAdo.fc_CloseRecordset(rsModeRgt);
            rsModeRgt = null;
            ModAdo.Close();
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            if (ComboModeRgt.Text != "")
            {
                General.Execute("UPDATE FournisseurArticle SET  ModeRgt='" + ComboModeRgt.ActiveRow.GetCellValue("Libelle") + "' WHERE CleAuto=" + txtCleAuto.Text + "");
                this.Visible = false;
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un mode de règlement.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                ComboModeRgt.Focus();
            }
        }
    }
}
