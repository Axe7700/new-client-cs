﻿namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    partial class frmAvoir
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Command1 = new System.Windows.Forms.Button();
            this.optComplet = new System.Windows.Forms.RadioButton();
            this.CmdAppliiqer = new System.Windows.Forms.Button();
            this.optPartiel = new System.Windows.Forms.RadioButton();
            this.txtNoFacture = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTotPartiel = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(125, 172);
            this.Command1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(112, 43);
            this.Command1.TabIndex = 571;
            this.Command1.Text = "   Annuler";
            this.Command1.UseVisualStyleBackColor = false;
            // 
            // optComplet
            // 
            this.optComplet.AutoSize = true;
            this.optComplet.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optComplet.Location = new System.Drawing.Point(40, 69);
            this.optComplet.Name = "optComplet";
            this.optComplet.Size = new System.Drawing.Size(122, 23);
            this.optComplet.TabIndex = 570;
            this.optComplet.TabStop = true;
            this.optComplet.Text = "Avoir complet";
            this.optComplet.UseVisualStyleBackColor = true;
            // 
            // CmdAppliiqer
            // 
            this.CmdAppliiqer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdAppliiqer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdAppliiqer.FlatAppearance.BorderSize = 0;
            this.CmdAppliiqer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdAppliiqer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.CmdAppliiqer.ForeColor = System.Drawing.Color.White;
            this.CmdAppliiqer.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.CmdAppliiqer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdAppliiqer.Location = new System.Drawing.Point(263, 172);
            this.CmdAppliiqer.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.CmdAppliiqer.Name = "CmdAppliiqer";
            this.CmdAppliiqer.Size = new System.Drawing.Size(112, 43);
            this.CmdAppliiqer.TabIndex = 568;
            this.CmdAppliiqer.Text = "     Valider";
            this.CmdAppliiqer.UseVisualStyleBackColor = false;
            this.CmdAppliiqer.Click += new System.EventHandler(this.CmdAppliiqer_Click);
            // 
            // optPartiel
            // 
            this.optPartiel.AutoSize = true;
            this.optPartiel.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optPartiel.Location = new System.Drawing.Point(200, 69);
            this.optPartiel.Name = "optPartiel";
            this.optPartiel.Size = new System.Drawing.Size(109, 23);
            this.optPartiel.TabIndex = 572;
            this.optPartiel.TabStop = true;
            this.optPartiel.Text = "Avoir partiel";
            this.optPartiel.UseVisualStyleBackColor = true;
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.AccAcceptNumbersOnly = false;
            this.txtNoFacture.AccAllowComma = false;
            this.txtNoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoFacture.AccHidenValue = "";
            this.txtNoFacture.AccNotAllowedChars = null;
            this.txtNoFacture.AccReadOnly = false;
            this.txtNoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoFacture.AccRequired = false;
            this.txtNoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoFacture.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtNoFacture.Location = new System.Drawing.Point(156, 13);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtNoFacture.MaxLength = 32767;
            this.txtNoFacture.Multiline = false;
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.ReadOnly = false;
            this.txtNoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoFacture.Size = new System.Drawing.Size(219, 39);
            this.txtNoFacture.TabIndex = 574;
            this.txtNoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoFacture.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(36, 24);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(84, 19);
            this.label33.TabIndex = 573;
            this.label33.Text = "No facture";
            // 
            // txtTotPartiel
            // 
            this.txtTotPartiel.AccAcceptNumbersOnly = false;
            this.txtTotPartiel.AccAllowComma = false;
            this.txtTotPartiel.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotPartiel.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotPartiel.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotPartiel.AccHidenValue = "";
            this.txtTotPartiel.AccNotAllowedChars = null;
            this.txtTotPartiel.AccReadOnly = false;
            this.txtTotPartiel.AccReadOnlyAllowDelete = false;
            this.txtTotPartiel.AccRequired = false;
            this.txtTotPartiel.BackColor = System.Drawing.Color.White;
            this.txtTotPartiel.CustomBackColor = System.Drawing.Color.White;
            this.txtTotPartiel.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotPartiel.ForeColor = System.Drawing.Color.Black;
            this.txtTotPartiel.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.txtTotPartiel.Location = new System.Drawing.Point(156, 111);
            this.txtTotPartiel.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtTotPartiel.MaxLength = 32767;
            this.txtTotPartiel.Multiline = false;
            this.txtTotPartiel.Name = "txtTotPartiel";
            this.txtTotPartiel.ReadOnly = false;
            this.txtTotPartiel.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotPartiel.Size = new System.Drawing.Size(219, 39);
            this.txtTotPartiel.TabIndex = 576;
            this.txtTotPartiel.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotPartiel.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(36, 111);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 19);
            this.label1.TabIndex = 575;
            this.label1.Text = "Montant";
            // 
            // frmAvoir
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(403, 236);
            this.Controls.Add(this.txtTotPartiel);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtNoFacture);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.optPartiel);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.optComplet);
            this.Controls.Add(this.CmdAppliiqer);
            this.Font = new System.Drawing.Font("Ubuntu", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmAvoir";
            this.Text = "frmAvoir";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.RadioButton optComplet;
        public System.Windows.Forms.Button CmdAppliiqer;
        public System.Windows.Forms.RadioButton optPartiel;
        public iTalk.iTalk_TextBox_Small2 txtNoFacture;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtTotPartiel;
        public System.Windows.Forms.Label label1;
    }
}