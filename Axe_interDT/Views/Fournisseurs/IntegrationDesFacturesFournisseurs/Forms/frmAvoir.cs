﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    public partial class frmAvoir : Form
    {
        public frmAvoir()
        {
            InitializeComponent();
        }

        private void CmdAppliiqer_Click(object sender, EventArgs e)
        {
            if (optPartiel.Checked)
            {
                txtTotPartiel.Text = txtTotPartiel.Text.Trim();
                if (string.IsNullOrEmpty(txtTotPartiel.Text) || !General.IsNumeric(txtTotPartiel.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez sélectionné un avoir partiel, vous devez saisir un montant.", "Avoir", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez - vous faire un avoir partiel de " + txtTotPartiel.Text + " pour la facture " + txtNoFacture.Text, "Avoir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            else if (optComplet.Checked)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez - vous faire un avoir partiel pour la facture " + txtNoFacture.Text, "Avoir", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
            }
            
        }
    }
}
