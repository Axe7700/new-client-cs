﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs.Forms;
using Microsoft.VisualBasic;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs
{
    public partial class UserDocFactFourn : UserControl
    {
        DataTable rsTVAcal;
        private DataTable rsFactFournEntete;
        private DataTable rsFactFournDetail;
        private DataTable rsFactFournPied;
        private DataTable dtFactFournDetail;
        private DataTable dtFactFournPied;
        public UserDocFactFourn()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void FactFournAddNew()//TESTED
        {
            rsFactFournEntete = new DataTable();
            var ModAdo = new ModAdo();
            rsFactFournEntete = ModAdo.fc_OpenRecordSet("SELECT * FROM FactFournEntete WHERE NoAutoFacture=0" + " ORDER BY noAutoFacture");
            txtVerrou.Text = "0";
            var _with1 = rsFactFournEntete.NewRow();
            _with1["DateSaisie"] = DateTime.Now;
            lblDateCreation.Text = _with1["DateSaisie"] + "";

            _with1["FactureCreat"] = General.gsUtilisateur;
            lblCreateur.Text = General.gsUtilisateur;

            _with1["DateModif"] = _with1["DateSaisie"];
            lblDateModif.Text = _with1["DateSaisie"] + "";

            _with1["FactureModif"] = General.gsUtilisateur;

            lblNomModif.Text = General.gsUtilisateur;
            rsFactFournEntete.Rows.Add(_with1);
            //ModAdo.rsAdo = rsFactFournEntete;

            SqlCommandBuilder cb = new SqlCommandBuilder(ModAdo.SDArsAdo);
            ModAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            ModAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            ModAdo.SDArsAdo.InsertCommand = insertCmd;

            ModAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {
                if (e.StatementType == StatementType.Insert)
                {
                    txtNoAutoFacture.Text = e.Command.Parameters["@ID"].Value.ToString();
                }
            });

            ModAdo.Update();
            //txtNoAutoFacture.Text = _with1["NoAutoFacture"]+"";
            lblNoFacture.Text = "Facture N° " + txtNoAutoFacture.Text;

            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);
            //ModAdo.Close();
        }

        //TODO ==> Mohammed has write this function to initialise a datatable which used in multiple functions
        private DataTable getDataTableFactFournDetail()
        {
            if (dtFactFournDetail == null)
            {
                dtFactFournDetail = new DataTable();
                dtFactFournDetail.Columns.Add("NoAutoFacture");
                dtFactFournDetail.Columns.Add("NoLigne");
                dtFactFournDetail.Columns.Add("NoBcmd");
                dtFactFournDetail.Columns.Add("Ref. Fourniture");
                dtFactFournDetail.Columns.Add("Designation");
                dtFactFournDetail.Columns.Add("Qté. Commandée");
                dtFactFournDetail.Columns.Add("Qté. Livrée");
                dtFactFournDetail.Columns.Add("Qté. Facturée");
                dtFactFournDetail.Columns.Add("Prix");
                dtFactFournDetail.Columns.Add("Reste à Livrer");
                dtFactFournDetail.Columns.Add("Compte de Charge");
                dtFactFournDetail.Columns.Add("Compte TVA");
                dtFactFournDetail.Columns.Add("Prix unitaire");
                dtFactFournDetail.Columns.Add("TA_Taux");
            }
            return dtFactFournDetail;
        }

        //TODO ==> Mohammed has write this function to initialise a datatable which used in multiple functions
        private DataTable getDataTableFactFournPied()
        {
            if (dtFactFournPied == null)
            {
                dtFactFournPied = new DataTable();
                dtFactFournPied.Columns.Add("No Bon De Commande");
                dtFactFournPied.Columns.Add("Code Immeuble");
                dtFactFournPied.Columns.Add("Date de Livraison");
                dtFactFournPied.Columns.Add("Montant des achats");
                dtFactFournPied.Columns.Add("Acheteur");
            }
            return dtFactFournPied;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkAjusteMtTVA_CheckedChanged(object sender, EventArgs e)
        {
            fc_lockGridTVA();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCptCharges_AfterCloseUp(object sender, EventArgs e)
        {
            if (cmbCptCharges.ActiveRow != null)
            {
                lblTauxTva.Text = cmbCptCharges.ActiveRow.Cells["Compte TVA"].Value + "";
                txtTauxTVA.Text = cmbCptCharges.ActiveRow.Cells["TA_Taux"].Value + "";
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCptChargesBis_AfterCloseUp(object sender, EventArgs e)
        {
            if (cmbCptChargesBis.ActiveRow != null)
            {
                GridFactFournDetail.ActiveRow.Cells["Compte de Charge"].Value = cmbCptChargesBis.ActiveRow.GetCellValue("Compte de Charge");
                GridFactFournDetail.ActiveRow.Cells["Compte TVA"].Value = cmbCptChargesBis.ActiveRow.GetCellValue("Compte de Taxe");
                GridFactFournDetail.ActiveRow.Cells["TA_Taux"].Value = cmbCptChargesBis.ActiveRow.GetCellValue("Taux de TVA");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fnc_Clear();
            fc_DeVerrouillage();
            FactFournAddNew();
            fc_CalTVA();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fnc_Clear()
        {
            int i = 0;
            GridFactFournPied.Rows.ToList().ForEach(l => l.Delete(false));
            GridFactFournDetail.Rows.ToList().ForEach(l => l.Delete(false));
            //GridFactFournDetail.DataSource = null;
            //GridFactFournPied.DataSource = null;
            cmdRechercheRaisonSociale.Visible = true;
            cmbCptCharges.Enabled = true;
            txtNoComptable.Text = "";
            lblNoComptable.Visible = false;
            txtNoComptable.Visible = false;
            txtCodeFourn.Text = "";
            txtCodeFournisseur.Text = "";
            txtRaisonFournisseur.Text = "";
            txtDateFacture.Text = "";
            txtMontantFacture.Text = "";
            TxtNoFacture.Text = "";
            cmbCptCharges.Text = "";
            lblTauxTva.Text = "";
            txtNoAutoFacture.Text = "";
            txtCommentaire.Text = "";
            lblDateCreation.Text = "";
            lblCreateur.Text = "";
            lblDateModif.Text = "";
            lblNomModif.Text = "";
            lblNoFacture.Text = "";
            txtModeRgt.Text = "";
            txtNbreJour.Text = "";
            txtTypeEcheance.Text = "";
            txtEcheance.Text = "";
            lblModeReglement.Text = "";
            txtDateEcheance.Text = "";
            txtCompteFournisseur.Text = "";
            chkAjusteMtTVA.CheckState = System.Windows.Forms.CheckState.Unchecked;
            fc_lockGridTVA();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_lockGridTVA()
        {
            bool bLock = false;
            object oCol = null;
            if (chkIntegration.Enabled == false)
            {
                bLock = true;
                chkAjusteMtTVA.Enabled = false;
            }
            else
            {

                chkAjusteMtTVA.Enabled = true;
                if (chkAjusteMtTVA.CheckState == CheckState.Checked)
                {
                    bLock = false;
                }
                else if (chkAjusteMtTVA.CheckState == CheckState.Unchecked)
                {
                    bLock = true;
                }
            }

            if (bLock == true)
            {
                //ssFactFournHtTvaCpta.ForeColorEven = System.Drawing.Color.Blue;
                //ssFactFournHtTvaCpta.ForeColorOdd = System.Drawing.Color.Blue;
                ssFactFournHtTvaCpta.Rows.ToList().ForEach(l => l.Appearance.ForeColor = l.IsAlternate == true ? Color.Blue : Color.Blue);
            }
            else
            {
                //ssFactFournHtTvaCpta.ForeColorEven = System.Drawing.Color.Black;
                //ssFactFournHtTvaCpta.ForeColorOdd = System.Drawing.Color.Black;
                ssFactFournHtTvaCpta.Rows.ToList().ForEach(l => l.Appearance.ForeColor = l.IsAlternate == true ? Color.Black : Color.Black);
            }
            foreach (var oCol_loopVariable in ssFactFournHtTvaCpta.DisplayLayout.Bands[0].Columns)
            {
                if (bLock)
                    oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.Disabled;
                else
                    oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodeFournisseur.Text))//TESTED
            {
                fnc_DeleteFacture(txtNoAutoFacture.Text);
            }
            else
            {
                if (txtVerrou.Text != "1")//TESTED
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous annuler la facture en cours ?", "Annuler la facture", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)//TESTED
                    {
                        fnc_DeleteFacture(txtNoAutoFacture.Text);
                    }
                    else//TESTED
                    {
                        if (ModParametre.fc_RecupParam(this.Name) == true)
                        {
                            ChargeFactureEntete(ModParametre.sNewVar);
                        }
                    }
                }
                else//TESTED
                {
                    if (ModParametre.fc_RecupParam(this.Name) == true)
                    {
                        ChargeFactureEntete(ModParametre.sNewVar);
                    }
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoFacture"></param>
        /// <returns></returns>
        private object fnc_DeleteFacture(string NoFacture)//TESTED
        {
            object functionReturnValue = null;
            string sqlDelete = null;
            string MaxNoFacture = null;


            if (string.IsNullOrEmpty(NoFacture))
            {
                return functionReturnValue;
            }

            sqlDelete = "DELETE FROM FactFournPied WHERE NoAutoFacture=" + NoFacture;
            General.Execute(sqlDelete);

            sqlDelete = "DELETE FROM FactFournDetail WHERE NoAutoFacture=" + NoFacture;
            General.Execute(sqlDelete);

            sqlDelete = "DELETE FROM FactFournEntete WHERE NoAutoFacture=" + NoFacture;
            General.Execute(sqlDelete);

            fnc_Clear();
            ModAdo ModAdo = new ModAdo();
            MaxNoFacture = ModAdo.fc_ADOlibelle("SELECT MAX(NoAutoFacture) FROM FactFournEntete WHERE FactureCreat='" + General.gsUtilisateur + "' OR FactureModif='" + General.gsUtilisateur + "'");
            ModAdo.Close();
            if (string.IsNullOrEmpty(MaxNoFacture))
            {
                return functionReturnValue;
            }
            ChargeFactureEntete(MaxNoFacture);
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAvoir_Click(object sender, EventArgs e)
        {
            string strBcmd = null;
            string strRequete = null;
            var rsAvoir = default(DataTable);
            string strMaxNoLigne = null;
            bool Match = false;
            bool blnAjout = false;
            const string strDesignation = "Ligne pour contrôle ";
            int i = 0;
            blnAjout = true;
            strRequete = "SELECT NoBcmd FROM FactFournPied WHERE NoAutoFacture=" + txtNoAutoFacture.Text;
            rsAvoir = new DataTable();
            ModAdo ModAdo = new ModAdo();
            rsAvoir = ModAdo.fc_OpenRecordSet(strRequete);
            if (rsAvoir.Rows.Count > 0)//TESTED
            {
                if (rsAvoir.Rows.Count == 1)
                {
                    strBcmd = rsAvoir.Rows[0]["NoBcmd"] + "";
                }
                else//TESTED
                {
                    while (!Match)
                    {
                        strBcmd = Interaction.InputBox("Sur quel bon de commande souhaitez-vous ajouter cette ligne ?", "Ajout d'une ligne dans la facture fournisseur");
                        i = 0;
                        foreach (DataRow dr in rsAvoir.Rows)
                        {
                            if ((dr["NoBcmd"] + "") == strBcmd)
                            {
                                Match = true;
                                break; // TODO: might not be correct. Was : Exit For
                            }
                            else
                            {
                                Match = false;
                            }
                            i++;
                        }
                        if (!Match)//TESTED
                        {
                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande " + strBcmd + " que vous avez saisie n'est pas présent dans la facture fournisseur.\r\n Souhaitez vous en saisir un autre?", "Ajout d'une ligne dans la facture", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                blnAjout = false;
                                break; // TODO: might not be correct. Was : Exit Do
                            }
                        }
                    }
                }
            }
            if (blnAjout == true)//TESTED
            {
                if (!string.IsNullOrEmpty(strBcmd))//TESTED
                {
                    strMaxNoLigne = ModAdo.fc_ADOlibelle("SELECT MAX(BCD_NoLigne) AS MaxNoLigne FROM BCD_Detail WHERE BCD_Cle=" + strBcmd);
                    strMaxNoLigne = Convert.ToString(Convert.ToInt32(General.nz(strMaxNoLigne, "2")) + 1);
                    General.Execute("INSERT INTO BCD_Detail (BCD_Cle,BCD_references,BCD_Designation,BCD_Quantite,BCD_NoLigne) VALUES (" + strBcmd + ",'" + strMaxNoLigne + "','" + strDesignation + "\r\n" + strMaxNoLigne + "','" + "1" + "'," + strMaxNoLigne + ")");
                    dtFactFournDetail = getDataTableFactFournDetail();
                    dtFactFournDetail.Rows.Add(
                            Convert.ToInt32(txtNoAutoFacture.Text),
                            0,
                            Convert.ToInt32(strBcmd),
                            strMaxNoLigne,
                            strDesignation + "\r\n" + strMaxNoLigne,
                            1,
                            0,
                            1,
                            0,
                            0,
                            cmbCptCharges.Text,
                            lblTauxTva.Text,
                            0,
                            txtTauxTVA.Text
                        );
                    //Valeur du taux (19.6 ou 5.5)
                    //GridFactFournDetail.AddItem(strRequete);
                    GridFactFournDetail.DataSource = dtFactFournDetail;
                }
            }
            fc_SaveEntete();
            fc_SaveDetail();
            fc_SavePied();
            ChargeFactureDetailPied();
            fc_ModifBCMD();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEnleverUnBcmd_Click(object sender, EventArgs e)//TESTED
        {
            int i = 0;

            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas créer de nouvelle fiche fournisseur,voulez-vous en créer une ?", "Fiche fournisseur inexistante", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cmdAjouter_Click(cmdAjouter, null);
                }
                else
                {
                    return;
                }
            }
            var _with3 = new frmRechercheBcmdFacture();
            if (!string.IsNullOrEmpty(txtCodeFournisseur.Text))//TESTED
            {
                _with3.txtSelect.Text = "SELECT NoBonDeCommande,BonDeCommande.CodeImmeuble as [Immeuble],Estimation,Acheteur,ProduitTotalHT,CodeFourn,FactFournPied.NoBcmd FROM BonDeCommande ";
                _with3.txtSelect.Text = _with3.txtSelect.Text + " INNER JOIN FactFournPied ON BonDeCommande.NoBonDeCommande=FactFournPied.NoBCmd ";

                _with3.txtWhere.Text = " WHERE CodeFourn=" + txtCodeFournisseur.Text + " AND NoAutoFacture=" + txtNoAutoFacture.Text;
                _with3.ShowDialog();
            }
            else//TESTED
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le Fournisseur", "Recherche des bons de commande annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdRechercheRaisonSociale_Click(cmdRechercheRaisonSociale, null);
                return;
            }

            GridFactFournDetail.Rows.ToList().ForEach(r => r.Delete(false));
            GridFactFournPied.Rows.ToList().ForEach(r => r.Delete(false));
            if (_with3.chkAnnuler.CheckState == 0)//TESTED
            {
                for (i = 0; i < General.SelectBcmdDetail.Length; i++)
                {
                    if (!string.IsNullOrEmpty(General.SelectBcmdDetail[i]))
                    {
                        EffaceBcmdFacture(General.SelectBcmdDetail[i]);
                    }
                }
            }
            else
            {
                General.SelectBcmdDetail = new string[1];
            }
            ChargeFactureDetailPied();
            _with3.Close();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdfactScann_Click(object sender, EventArgs e)
        {
            string[] stNobonDeCommande = null;
            int i = 0;
            bool bIndex = false;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                i = 0;
                stNobonDeCommande = null;
                bIndex = false;
                for (i = 0; i <= GridFactFournPied.Rows.Count - 1; i++)//TESTED
                {
                    Array.Resize(ref stNobonDeCommande, i + 1);
                    stNobonDeCommande[i] = General.nz((GridFactFournPied.Rows[i].Cells["No Bon de Commande"].Value), "999999999") + "";
                    bIndex = true;
                }

                if (bIndex == true)
                {
                    General.fc_FindFactFourn(stNobonDeCommande, this.Handle.ToInt32());
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner au moins un bon de commande.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdfactScann_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheBCMD_Click(object sender, EventArgs e)//TESTED
        {
            int i = 0;
            frmRechercheBcmdFacture _with4;
            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas créer de nouvelle fiche fournisseur,voulez-vous en créer une ?", "Fiche fournisseur inexistante", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    cmdAjouter_Click(cmdAjouter, null);
                }
                else
                {
                    return;
                }
            }
            if (!string.IsNullOrEmpty(txtCodeFournisseur.Text))//TESTED
            {
                _with4 = new frmRechercheBcmdFacture();
                _with4.txtWhere.Text = " WHERE (CodeFourn=" + txtCodeFournisseur.Text + ") AND (FactFournPied.NoAutoFacture<>" + txtNoAutoFacture.Text + " OR FactFournPied.NoAutoFacture IS NULL)";
                _with4.txtWhere.Text = _with4.txtWhere.Text + " AND (CodeE='CE' OR CodeE='CF') ";
                _with4.txtWhere.Text = _with4.txtWhere.Text + " AND EtatFacturation<>'FT' ";
                _with4.chkEnleveDoublon.Checked = true;
                _with4.txtNoAutoFacture.Text = txtNoAutoFacture.Text;
                _with4.ShowDialog();
            }
            else//TESTED
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le Fournisseur", "Recherche des bons de commande annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdRechercheRaisonSociale_Click(cmdRechercheRaisonSociale, null);
                return;
            }
            if (_with4.chkAnnuler.CheckState == 0)//TESTED
            {
                for (i = 0; i <= General.SelectBcmdDetail.Length - 1; i++)
                {
                    if (!string.IsNullOrEmpty(General.SelectBcmdDetail[i]))
                    {
                        ChargeDetail(General.SelectBcmdDetail[i]);
                    }
                }
            }
            else
            {
                General.SelectBcmdDetail = new string[2];
            }
            _with4.Close();
            CmdSauver_Click(CmdSauver, null);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoBcmd"></param>
        /// <returns></returns>
        private object ChargeDetail(string NoBcmd)
        {
            object functionReturnValue = null;
            //Charge le détail du bon de commande en vérifiant si il n'existe
            //pas déjà une facture fournisseur associée à ce même numéro de bon de commande
            //afin d'en récupérer le reste à livrer pour les références des articles commandés

            DataTable rsCmdDetail = default(DataTable);
            DataTable rsCmdEntete = default(DataTable);
            DataTable rsFactFournDetail = default(DataTable);
            string sqlBcmd = null;
            string strAddItem = null;
            double dblPrixUnit = 0;
            if (!string.IsNullOrEmpty(NoBcmd))//TESTED
            {
                sqlBcmd = "SELECT * FROM BCD_DETAIL WHERE BCD_Cle=" + NoBcmd + " ORDER BY BCD_No ASC ";
            }
            else
            {
                return functionReturnValue;
            }
            rsCmdDetail = new DataTable();
            rsFactFournDetail = new DataTable();
            rsCmdEntete = new DataTable();
            var ModAdo = new ModAdo();
            rsCmdEntete = ModAdo.fc_OpenRecordSet("SELECT NoBonDeCommande,CodeImmeuble,Acheteur,DateLivraison FROM BonDeCommande where NoBonDeCommande=" + NoBcmd + "");
            dtFactFournPied = getDataTableFactFournPied();

            if (rsCmdEntete.Rows.Count > 0)//TESTED
            {
                foreach (DataRow dr in rsCmdEntete.Rows)
                {
                    dtFactFournPied.Rows.Add(dr["NoBonDeCommande"], dr["CodeImmeuble"], dr["DateLivraison"], "0.00", dr["Acheteur"]);
                }
            }
            GridFactFournPied.DataSource = dtFactFournPied;

            //ModAdo.fc_CloseRecordset(rsCmdEntete);
            rsCmdDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);
            if (rsCmdDetail.Rows.Count > 0)//TESTED
            {
                dtFactFournDetail = getDataTableFactFournDetail();
                foreach (DataRow dr in rsCmdDetail.Rows)
                {
                    if (General.nz(dr["BCD_Quantite"], "0") + "" != "0")
                    {
                        dblPrixUnit = Convert.ToDouble(General.nz(dr["BCD_PrixHT"], "0")) / Convert.ToDouble(General.nz(dr["BCD_Quantite"], "1"));
                    }
                    else
                    {
                        dblPrixUnit = 0;
                    }

                    if (!string.IsNullOrEmpty((dr["BCD_References"] + "")))//TESTED
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT BCD_No,BCD_PrixHT,SUM(FactFournDetail.QteLivre) AS SumQteFacture,";
                        sqlBcmd = sqlBcmd + " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.RefLigneCmd,BcmdLivraisonDetail.RefLigneCmd , ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_References,BCD_Detail.BCD_PrixHT ";
                        sqlBcmd = sqlBcmd + " FROM BCD_Detail LEFT JOIN ";
                        sqlBcmd = sqlBcmd + " FactFournDetail ON BCD_Detail.BCD_Cle=FactFournDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " LEFT JOIN BcmdLivraisonDetail ON ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Cle=BcmdLivraisonDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " WHERE BCD_Detail.BCD_Cle=" + NoBcmd + " AND BCD_Detail.BCD_References='" + StdSQLchaine.gFr_DoublerQuote(dr["BCD_References"] + "") + "'";
                        sqlBcmd = sqlBcmd + " GROUP BY BCD_Detail.BCD_No,BCD_Detail.BCD_PrixHT, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.RefLigneCmd,BcmdLivraisonDetail.RefLigneCmd , ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_References,BCD_Detail.BCD_PrixHT ";
                        sqlBcmd = sqlBcmd + " HAVING (FactFournDetail.RefLigneCmd = BCD_Detail.BCD_References) OR";
                        sqlBcmd = sqlBcmd + " (BcmdLivraisonDetail.RefLigneCmd = BCD_Detail.BCD_References)";

                        //            sqlBcmd = ""
                        //            sqlBcmd = "SELECT SUM(FactFournDetail.QteLivre) AS SumQteFacture,"
                        //            sqlBcmd = sqlBcmd & " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre "
                        //            sqlBcmd = sqlBcmd & " FROM FactFournDetail INNER JOIN BcmdLivraisonDetail ON "
                        //            sqlBcmd = sqlBcmd & " FactFournDetail.NoBcmd=BcmdLivraisonDetail.NoBcmd "
                        //            sqlBcmd = sqlBcmd & " WHERE FactFournDetail.NoBcmd=" & NoBcmd & " AND FactFournDetail.LigneCmd='" & gFr_DoublerQuote(rsCmdDetail!BCD_erences & "") & "'"
                        rsFactFournDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);


                        if (rsFactFournDetail.Rows.Count > 0)//TESTED
                        {
                            dtFactFournDetail.Rows.Add(
                                 txtNoAutoFacture.Text,
                                  //NoAutoFacture
                                  "0",
                                  //NoLigne
                                  NoBcmd,
                                  //NoBcmd
                                  dr["BCD_references"] + "",
                                  //. Fourniture
                                  dr["BCD_Designation"] + "",
                                  //Designation
                                  General.nz(dr["BCD_Quantite"], "0"),
                                  //Qté. Commandée
                                  General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0"),
                                  //Qté. Livrée
                                  //Si souhait de vérifier toutes les lignes :
                                  "0",
                                  //Qté. Facturée : par défaut on note les quantités déjà livrées - celles déjà facturées
                                  Convert.ToDouble(General.nz(dr["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")),
                                  Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteFacture"], "0")),
                                  //Reste à facturer

                                  cmbCptCharges.Text,
                                  //Compte de charge
                                  lblTauxTva.Text,
                                  //Compte TVA
                                  dblPrixUnit,
                                  //Prix unitaire
                                  txtTauxTVA.Text
                            //Taux de TVA
                            );
                            GridFactFournDetail.DataSource = dtFactFournDetail;
                        }
                        else//TESTED
                        {
                            dtFactFournDetail.Rows.Add(
                                txtNoAutoFacture.Text,
                                //NoAutoFacture
                                "0",
                                //NoLigne
                                NoBcmd,
                                //NoBcmd
                                dr["BCD_references"] + "",
                                //. Fourniture
                                dr["BCD_Designation"] + "",
                                //Designation
                                General.nz(dr["BCD_Quantite"], "0"),
                                //Quantité commandée
                                "0",
                                //Qté. Livrée
                                "0",
                                //Qté. Facturée
                                Convert.ToDouble(General.nz(dr["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")),
                                //Prix
                                General.nz(dr["BCD_Quantite"], "0"),
                                //Reste à Facturer
                                cmbCptCharges.Text,
                                //Compte de charges
                                lblTauxTva.Text,
                                //Compte de TVA
                                dblPrixUnit,
                                //Prix unitaire
                                txtTauxTVA.Text
                            //Taux de TVA
                            );
                            GridFactFournDetail.DataSource = dtFactFournDetail;
                        }
                    }
                    else//TESTED
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT BCD_No,BCD_PrixHT,SUM(FactFournDetail.QteLivre) AS SumQteFacture,";
                        sqlBcmd = sqlBcmd + " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " BcmdLivraisonDetail.DesignationLigneCmd,FactFournDetail.DesignationLigneCmd,";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Designation,BCD_Detail.BCD_PrixHT ";
                        sqlBcmd = sqlBcmd + " FROM BCD_Detail LEFT JOIN ";
                        sqlBcmd = sqlBcmd + " FactFournDetail ON BCD_Detail.BCD_Cle=FactFournDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " LEFT JOIN BcmdLivraisonDetail ON ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Cle=BcmdLivraisonDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " WHERE BCD_Detail.BCD_Cle=" + NoBcmd + " AND BCD_Detail.BCD_Designation='" + StdSQLchaine.gFr_DoublerQuote(dr["BCD_Designation"] + "") + "'";
                        sqlBcmd = sqlBcmd + " GROUP BY BCD_Detail.BCD_No,BCD_Detail.BCD_PrixHT, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " BcmdLivraisonDetail.DesignationLigneCmd,FactFournDetail.DesignationLigneCmd, ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Designation,BCD_Detail.BCD_PrixHT ";
                        sqlBcmd = sqlBcmd + " HAVING (BcmdLivraisonDetail.DesignationLigneCmd = BCD_Detail.BCD_Designation) OR";
                        sqlBcmd = sqlBcmd + " (FactFournDetail.DesignationLigneCmd = BCD_Detail.BCD_Designation)";

                        //            sqlBcmd = ""
                        //            sqlBcmd = "SELECT SUM(FactFournDetail.QteLivre) AS SumQteFacture,"
                        //            sqlBcmd = sqlBcmd & " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre "
                        //            sqlBcmd = sqlBcmd & " FROM FactFournDetail INNER JOIN BcmdLivraisonDetail ON "
                        //            sqlBcmd = sqlBcmd & " FactFournDetail.NoBcmd=BcmdLivraisonDetail.NoBcmd "
                        //            sqlBcmd = sqlBcmd & " WHERE FactFournDetail.NoBcmd=" & NoBcmd & " AND FactFournDetail.DesignationLigneCmd='" & gFr_DoublerQuote(rsCmdDetail!BCD_Designation & "") & "'"
                        rsFactFournDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);
                        //rsFactFournDetail.Close
                        //GridFactFournDetail.DataMode = ssDataModeAddItem
                        if (rsFactFournDetail.Rows.Count > 0)//TESTED
                        {
                            dtFactFournDetail.Rows.Add(
                                txtNoAutoFacture.Text,
                                //NoAutoFacture
                                "0",
                                //NoLigne
                                NoBcmd,
                                //NoBcmd
                                dr["BCD_references"] + "",
                                //. Fourniture
                                dr["BCD_Designation"] + "",
                                //Designation
                                General.nz(dr["BCD_Quantite"], "0"),
                                //Qté. Commandée
                                General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0"),
                                //Qté. Livrée
                                //Si souhait de vérifier toutes les lignes :
                                "0",
                                //Qté. Facturée : par défaut on note les quantités déjà livrées - celles déjà facturées
                                Convert.ToDouble(General.nz(dr["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")),
                                //Prix
                                Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteFacture"], "0")),
                                //Reste à facturer

                                cmbCptCharges.Text,
                                //Compte de charge
                                lblTauxTva.Text,
                                //Compte TVA
                                dblPrixUnit,
                                //Prix unitaire
                                txtTauxTVA.Text
                            //Taux de TVA
                            );
                            GridFactFournDetail.DataSource = dtFactFournDetail;
                        }
                        else//TESTED
                        {
                            dtFactFournDetail.Rows.Add(
                                txtNoAutoFacture.Text,
                                //NoAutoFacture
                                "0",
                                //NoLigne
                                NoBcmd,
                                //NoBcmd
                                dr["BCD_references"] + "",
                                //. Fourniture
                                dr["BCD_Designation"] + "",
                                //Designation
                                General.nz(dr["BCD_Quantite"], "0"),
                                //Quantité commandée
                                "0",
                                //Qté. Livrée
                                "0",
                                //Qté. Facturée
                                Convert.ToDouble(General.nz(dr["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")),
                                //Prix
                                General.nz(dr["BCD_Quantite"], "0"),
                                //Reste à Facturer
                                cmbCptCharges.Text,
                                //Compte de charges
                                lblTauxTva.Text,
                                //Compte de TVA
                                dblPrixUnit,
                                //Prix unitaire
                                txtTauxTVA.Text
                            //Taux de TVA
                            );
                            GridFactFournDetail.DataSource = dtFactFournDetail;
                        }
                    }
                }
            }
            ModAdo.fc_CloseRecordset(rsCmdDetail);
            ModAdo.fc_CloseRecordset(rsFactFournDetail);
            ModAdo.Close();
            rsCmdDetail = null;
            rsFactFournDetail = null;
            rsCmdEntete = null;
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoBcmd"></param>
        /// <returns></returns>
        public object EffaceBcmdFacture(string NoBcmd)
        {
            object functionReturnValue = null;
            string sqlBcmd = null;

            if (string.IsNullOrEmpty(NoBcmd))
            {
                return functionReturnValue;
            }

            General.Execute("DELETE FROM FactFournDetail WHERE NoBCmd=" + NoBcmd);
            General.Execute("DELETE FROM FactFournPied WHERE NoBCmd=" + NoBcmd);

            fnc_ModifCodeEtatBcmd(NoBcmd);
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object ChargeFactureDetailPied()
        {
            //attention: field in sqlBcmd sum(qte livre) exist in rsFactFournDetail datatable but it's displays dt datatable that's fill grid
            object functionReturnValue = null;

            DataTable rsCmdDetail = default(DataTable);
            DataTable rsCmdPied = default(DataTable);
            DataTable rsFactFournDetail = default(DataTable);
            string sqlBcmd = null;
            string strBonLivraison = null;

            double dblPrixUnit = 0;

            string strAddItem = null;
            rsCmdPied = new DataTable();
            rsCmdDetail = new DataTable();
            rsFactFournDetail = new DataTable();

            GridFactFournPied.Rows.ToList().ForEach(l => l.Delete(false));
            GridFactFournDetail.Rows.ToList().ForEach(l => l.Delete(false));
            //GridFactFournPied.DataSource = null;
            //GridFactFournDetail.DataSource = null;
            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                return functionReturnValue;
            }
            var ModAdo = new ModAdo();
            rsCmdPied = ModAdo.fc_OpenRecordSet("SELECT NoBonDeCommande,BonDeCommande.CodeImmeuble as Immeuble,Acheteur,DateLivraison,FactFournPied.Montant FROM BonDeCommande INNER JOIN FactFournPied ON BonDeCommande.NoBonDeCommande=FactFournPied.NoBCmd WHERE FactFournPied.NoAutoFacture=" + txtNoAutoFacture.Text + " ORDER BY NoBonDeCommande");
            dtFactFournPied = getDataTableFactFournPied();
            if (rsCmdPied.Rows.Count > 0) //TESTED
            {
                foreach (DataRow dr in rsCmdPied.Rows)
                {
                    dtFactFournPied.Rows.Add(dr["NoBonDeCommande"], dr["Immeuble"], dr["DateLivraison"], dr["Montant"],
                        dr["Acheteur"]);
                }
            }
            GridFactFournPied.DataSource = dtFactFournPied;
            ModAdo.fc_CloseRecordset(rsCmdPied);
            sqlBcmd = "SELECT * FROM FactFournDetail WHERE NoAutoFacture=" + txtNoAutoFacture.Text;
            rsCmdDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);

            /* Create Datatable*/
            dtFactFournDetail = getDataTableFactFournDetail();
            if (rsCmdDetail.Rows.Count > 0)//TESTED
            {
                foreach (DataRow dr in rsCmdDetail.Rows)
                {
                    strBonLivraison = ModAdo.fc_ADOlibelle("SELECT BcmdLivraisonDetail.NoBCmd FROM BcmdLivraisonDetail WHERE BcmdLivraisonDetail.NoBCmd=" + dr["NoBcmd"] + "");
                    if (!string.IsNullOrEmpty((dr["refLigneCmd"] + "")))//TESTED
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT BCD_No,BCD_PrixHT,SUM(FactFournDetail.QteLivre) AS SumQteFacture,";
                        sqlBcmd = sqlBcmd + " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre,BCD_Detail.BCD_Quantite , ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.RefLigneCmd,BcmdLivraisonDetail.RefLigneCmd , ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_References";
                        sqlBcmd = sqlBcmd + " FROM BCD_Detail LEFT JOIN ";
                        sqlBcmd = sqlBcmd + " FactFournDetail ON BCD_Detail.BCD_Cle=FactFournDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " LEFT JOIN BcmdLivraisonDetail ON ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Cle=BcmdLivraisonDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " WHERE FactFournDetail.NoBcmd=" + dr["NoBcmd"] + " AND FactFournDetail.RefLigneCmd='" + StdSQLchaine.gFr_DoublerQuote(dr["RefLigneCmd"] + "") + "'";
                        sqlBcmd = sqlBcmd + " GROUP BY BCD_Detail.BCD_No,BCD_Detail.BCD_PrixHT, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.RefLigneCmd,BcmdLivraisonDetail.RefLigneCmd , ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_References,BCD_Detail.BCD_Quantite ";
                        sqlBcmd = sqlBcmd + " HAVING ";
                        sqlBcmd = sqlBcmd + "(FactFournDetail.RefLigneCmd = BCD_Detail.BCD_References) ";
                        if (!string.IsNullOrEmpty(strBonLivraison))//TESTED
                        {
                            sqlBcmd = sqlBcmd + " AND (BcmdLivraisonDetail.RefLigneCmd = BCD_Detail.BCD_References)";
                        }

                        rsFactFournDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);

                        if (rsFactFournDetail.Rows.Count > 0)//TESTED
                        {
                            if (General.nz(rsFactFournDetail.Rows[0]["BCD_PrixHT"] + "", "0") + "" != "0")//TESTED
                            {
                                dblPrixUnit = Convert.ToDouble(General.nz(dr["QteCommande"], "0")) / Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["BCD_PrixHT"] + "", "0"));
                            }
                            else//TESTED
                            {
                                dblPrixUnit = 0;
                            }
                            dtFactFournDetail.Rows.Add(
                                txtNoAutoFacture.Text,
                                dr["NoLigne"],
                                dr["NoBcmd"],
                                dr["refLigneCmd"] + "",
                                dr["DesignationLigneCmd"] + "",
                                General.nz(dr["QteCommande"], "0"),
                              General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0"),
                               (General.nz(dr["QteLivre"], "0")),
                               (General.nz(dr["MontantLigne"], "0")),
                               Convert.ToDouble(General.nz(dr["QteCommande"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteFacture"], "0")),
                               dr["CG_NUM"],
                             dr["TaxeCG_Num"],
                               dblPrixUnit,
                               dr["TaxeTA_Taux"] + ""
                               );
                        }
                    }
                    else//TESTED
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT BCD_No,BCD_PrixHT,SUM(FactFournDetail.QteLivre) AS SumQteFacture,";
                        sqlBcmd = sqlBcmd + " SUM(BcmdLivraisonDetail.QteLivre) as SumQteLivre,BCD_Detail.BCD_Quantite , ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " BcmdLivraisonDetail.DesignationLigneCmd,FactFournDetail.DesignationLigneCmd,";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Designation";
                        sqlBcmd = sqlBcmd + " FROM BCD_Detail LEFT JOIN ";
                        sqlBcmd = sqlBcmd + " FactFournDetail ON BCD_Detail.BCD_Cle=FactFournDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " LEFT JOIN BcmdLivraisonDetail ON ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Cle=BcmdLivraisonDetail.NoBcmd ";
                        sqlBcmd = sqlBcmd + " WHERE FactFournDetail.NoBcmd=" + dr["NoBcmd"] + " AND FactFournDetail.DesignationLigneCmd='" + StdSQLchaine.gFr_DoublerQuote(dr["DesignationLigneCmd"] + "") + "'";
                        sqlBcmd = sqlBcmd + " GROUP BY BCD_Detail.BCD_No,BCD_Detail.BCD_PrixHT, ";
                        sqlBcmd = sqlBcmd + " FactFournDetail.NoLigne, BcmdLivraisonDetail.NoLigne, ";
                        sqlBcmd = sqlBcmd + " BcmdLivraisonDetail.DesignationLigneCmd,FactFournDetail.DesignationLigneCmd, ";
                        sqlBcmd = sqlBcmd + " BCD_Detail.BCD_Designation,BCD_Detail.BCD_Quantite ";
                        sqlBcmd = sqlBcmd + " HAVING ";
                        if (!string.IsNullOrEmpty(strBonLivraison))
                        {
                            sqlBcmd = sqlBcmd + " (BcmdLivraisonDetail.DesignationLigneCmd = BCD_Detail.BCD_Designation) AND";
                        }
                        sqlBcmd = sqlBcmd + " (FactFournDetail.DesignationLigneCmd = BCD_Detail.BCD_Designation)";

                        rsFactFournDetail = ModAdo.fc_OpenRecordSet(sqlBcmd);
                        //GridFactMoFournDetail.DataMode = ssDataModeAddItem
                        if (rsFactFournDetail.Rows.Count > 0)//TESTED
                        {
                            if (General.nz(rsFactFournDetail.Rows[0]["BCD_PrixHT"] + "", "0") + "" != "0")//TESTED
                            {
                                dblPrixUnit = Convert.ToDouble(General.nz(dr["QteCommande"], "0")) / Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["BCD_PrixHT"] + "", "0"));
                            }
                            else//TESTED
                            {
                                dblPrixUnit = 0;
                            }
                            dtFactFournDetail.Rows.Add(
                                txtNoAutoFacture.Text,
                                dr["NoLigne"],
                                dr["NoBcmd"],
                                dr["refLigneCmd"] + "",
                                dr["DesignationLigneCmd"] + "",
                                General.nz(dr["QteCommande"], "0"),
                                General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0"),
                                General.nz(dr["QteLivre"], "0"),
                                (General.nz(dr["MontantLigne"], "0")),
                                Convert.ToDouble(General.nz(dr["QteCommande"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteFacture"], "0")),
                               dr["CG_NUM"],
                               dr["TaxeCG_Num"],
                               dblPrixUnit,
                               dr["TaxeTA_Taux"] + ""
                               );
                        }
                        else//TESTED
                        {
                            //== Modif du 21 aout si aucun enregistrement trouvé à partir du bons de commande
                            //== alors la ligne a été saisie directement dans la facture
                            sqlBcmd = "SELECT     SUM(QteLivre) AS SumQteFacture, NoLigne," + " DesignationLigneCmd , LigneCmd , QteCommande" + " From FactFournDetail" + " WHERE     (NoBcmd = " + dr["NoBcmd"] + ")" + " AND (Noligne =" + dr["NoLigne"] + ")" + " GROUP BY NoLigne, DesignationLigneCmd";

                            dblPrixUnit = 0;
                            dtFactFournDetail.Rows.Add(
                                    txtNoAutoFacture.Text,
                                   dr["NoLigne"],
                                    dr["NoBcmd"],
                                    dr["refLigneCmd"] + "",
                                    dr["DesignationLigneCmd"] + "",
                                    General.nz(dr["QteCommande"], "0"),
                                    0,
                                   (General.nz(dr["QteLivre"], "0")),
                                   (General.nz(dr["MontantLigne"], "0")),
                                   Convert.ToDouble(0),
                                   dr["CG_NUM"],
                                   dr["TaxeCG_Num"],
                                   dblPrixUnit,
                                   dr["TaxeTA_Taux"] + ""
                                   );
                        }
                    }
                }
            }
            GridFactFournDetail.DataSource = dtFactFournDetail;
            ModAdo.fc_CloseRecordset(rsCmdDetail);
            rsFactFournDetail = null;
            rsCmdDetail = null;
            rsCmdPied = null;
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoFacture"></param>
        /// <returns></returns>
        private object ChargeFactureEntete(string NoFacture)//TESTED
        {
            object functionReturnValue = null;
            string sqlFactEntete = null;
            DataTable rsEntete = default(DataTable);

            fnc_Clear();

            //TODO ConnectionState in this if statement is Closed when i click on click search button.i can't search Facture
            if (string.IsNullOrEmpty(NoFacture) || !General.IsNumeric(NoFacture) || General.adocnn == null /*|| General.adocnn.State==ConnectionState.Closed*/)
            {
                return functionReturnValue;
            }
            sqlFactEntete = "SELECT * FROM FactFournEntete WHERE NoAutoFacture=" + NoFacture;

            rsEntete = new DataTable();
            ModAdo ModAdo = new ModAdo();

            rsEntete = ModAdo.fc_OpenRecordSet(sqlFactEntete);
            if (rsEntete.Rows.Count > 0)//TESTED
            {

                var _with5 = rsEntete;
                txtVerrou.Text = General.nz(_with5.Rows[0]["Verrou"], "0") + "";

                txtNoAutoFacture.Text = _with5.Rows[0]["NoAutoFacture"] + "";
                lblNoFacture.Text = "Facture N° " + txtNoAutoFacture.Text;
                txtCodeFournisseur.Text = _with5.Rows[0]["CodeFournisseur"] + "";
                txtCodeFourn.Text = ModAdo.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE CleAuto=" + General.nz((txtCodeFournisseur.Text), 0) + "");
                txtRaisonFournisseur.Text = ModAdo.fc_ADOlibelle("SELECT RaisonSocial FROM FournisseurArticle WHERE CleAuto=" + General.nz(_with5.Rows[0]["CodeFournisseur"], "''"));
                chkAjusteMtTVA.CheckState = (CheckState)Convert.ToInt16(General.nz(_with5.Rows[0]["AjusterMtTVA"], 0));
                fc_chargeCpteCharges();

                txtDateFacture.Text = _with5.Rows[0]["DateLivraison"] + "";
                txtMontantFacture.Text = _with5.Rows[0]["Montant"] + "";
                TxtNoFacture.Text = _with5.Rows[0]["RefFacture"] + "";
                cmbCptCharges.Value = _with5.Rows[0]["CG_NUM"] + "";
                //lblTauxTva.Text = cmbCptCharges.Columns["Compte TVA"].Value;
                if (cmbCptCharges.ActiveRow != null)
                    lblTauxTva.Text = cmbCptCharges.ActiveRow.Cells["Compte TVA"].Value + "";
                txtCommentaire.Text = _with5.Rows[0]["Commentaire"] + "";
                lblDateCreation.Text = _with5.Rows[0]["DateSaisie"] + "";
                lblCreateur.Text = _with5.Rows[0]["FactureCreat"] + "";
                lblDateModif.Text = _with5.Rows[0]["DateModif"] + "";
                lblNomModif.Text = _with5.Rows[0]["FactureModif"] + "";
                lblNoFacture.Text = "Facture N° " + _with5.Rows[0]["NoAutoFacture"] + "";
                txtCompteFournisseur.Text = _with5.Rows[0]["CT_NUM"] + "";
                txtDateEcheance.Text = _with5.Rows[0]["DateEcheance"] + "";
                txtModeRgt.Text = _with5.Rows[0]["ModeRgt"] + "";
                if (_with5.Rows[0]["ec_nofacture"] != null)//TESTED
                {
                    txtNoComptable.Text = _with5.Rows[0]["ec_nofacture"] + "";
                    lblNoComptable.Visible = true;
                    txtNoComptable.Visible = true;
                }
                else
                {
                    txtNoComptable.Text = "";
                    lblNoComptable.Visible = false;
                    txtNoComptable.Visible = false;
                }
                fc_Reglement(txtModeRgt.Text);
            }
            ModAdo.fc_CloseRecordset(rsEntete);
            cmdRechercheRaisonSociale.Visible = false;
            if (General.nz(txtVerrou.Text, "0") + "" == "0")//TESTED
            {
                cmbCptCharges.Enabled = true;
            }
            else//TESTED
            {
                cmbCptCharges.Enabled = false;
            }
            ChargeFactureDetailPied();
            //TODO . i move this lines to the end of this function because when grid does not have a datasource i can't apply Edit and NoEdit in columns
            if (!string.IsNullOrEmpty((txtVerrou.Text)) && Convert.ToDouble(txtVerrou.Text) == 1)//TESTED ===> add an other condition to catch an exception if Verrou is null
            {
                fc_Verrouillage();
            }
            else
            {
                fc_DeVerrouillage();
            }

            ModParametre.fc_SaveParamPosition(this.Name, this.Name, txtNoAutoFacture.Text);
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            try
            {
                string req = "SELECT  FactFournEntete.NoAutoFacture AS \"N° Facture\", " + " FactFournPied.NoBcmd as \"N° Bon de commande\"," + " FactFournPied.CodeImmeuble as \"Code Immeuble\"," + " FactFournEntete.EC_NoFacture as \"N°COMPTABLE\"," + " FournisseurArticle.Code as \"Code Fournisseur\"," + " FactFournEntete.RefFacture as \"Référence Facture\" " + " FROM FactFournEntete  LEFT JOIN FactFournPied ON ";
                req = req + " FactFournPied.NoAutoFacture=FactFournEntete.NoAutoFacture ";
                req = req + " INNER JOIN FournisseurArticle ON FactFournEntete.CodeFournisseur=FournisseurArticle.CleAuto ";
                var _with6 = new SearchTemplate(null, null, req, "", "") { Text = "Rechercher une facture fournisseur" };
                _with6.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (_with6.ugResultat.ActiveRow != null)
                    {
                        ChargeFactureEntete((_with6.ugResultat.ActiveRow.GetCellValue("N° Facture") + ""));
                        fc_CalTVA();
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);
                    }
                    _with6.Close();
                };
                _with6.ugResultat.KeyDown += (se, ev) =>
                {
                    if (_with6.ugResultat.ActiveRow != null)
                    {
                        ChargeFactureEntete((_with6.ugResultat.ActiveRow.GetCellValue("N° Facture") + ""));
                        fc_CalTVA();
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);
                    }
                    _with6.Close();
                };
                _with6.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheRaisonSociale_Click(object sender, EventArgs e)//TESTED
        {
            try
            {
                string req = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\"," + " Ville as \"Ville\" FROM fournisseurArticle";
                string where = "  (code <> '' and code is not null) " + " and (NePasAfficher is null or NePasAfficher = 0 ) ";
                var _with7 = new SearchTemplate(null, null, req, where, "") { Text = "Attacher un fournisseur à votre facture" };
                _with7.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    fc_ChargeFournisseur(_with7.ugResultat.ActiveRow.GetCellValue("Code") + "");
                    _with7.Close();
                };
                _with7.ugResultat.KeyPress += (se, ev) =>
                {
                    if ((short)ev.KeyChar == 13)
                    {
                        fc_ChargeFournisseur(_with7.ugResultat.ActiveRow.GetCellValue("Code") + "");
                        _with7.Close();
                    }
                };
                _with7.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="strCodeFourn"></param>
        private void fc_ChargeFournisseur(string strCodeFourn)
        {
            var ModAdo = new ModAdo();
            txtCodeFourn.Text = strCodeFourn;
            txtCodeFournisseur.Text = ModAdo.fc_ADOlibelle("SELECT cleauto FROM FournisseurArticle WHERE Code='" + strCodeFourn + "'");
            txtRaisonFournisseur.Text = ModAdo.fc_ADOlibelle("SELECT RaisonSocial FROM FournisseurArticle WHERE Code='" + strCodeFourn + "'");
            txtCompteFournisseur.Text = ModAdo.fc_ADOlibelle("SELECT NCompte FROM FournisseurArticle WHERE Cleauto=" + txtCodeFournisseur.Text + "");
            txtModeRgt.Text = ModAdo.fc_ADOlibelle("SELECT ModeRgt FROM FournisseurArticle WHERE CleAuto=" + txtCodeFournisseur.Text + "");
            fc_Reglement((txtModeRgt.Text));
            fc_CtrCt_Num();
            if (string.IsNullOrEmpty(txtCompteFournisseur.Text))
            {
                fc_VerifCreeCptFourn((txtCodeFournisseur.Text));
            }
            fc_chargeCpteCharges();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_CtrCt_Num()
        {
            string sSQL = null;

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                //MsgBox "Liaison comptabilité inexistante."
                return;
            }

            sSQL = "SELECT CT_Num From F_COMPTET " + " WHERE CT_Num='" + txtCompteFournisseur.Text + "'";
            SAGE.fc_OpenConnSage();
            var ModAdo = new ModAdo();
            sSQL = ModAdo.fc_ADOlibelle(sSQL, false, SAGE.adoSage);
            if (string.IsNullOrEmpty(sSQL))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention ce compte fournisseur n'existe pas en compta," + " veuillez vous rendre sur la fiche fournisseur et valider la fiche," + " un compte sera crée automatiquement.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="strCleAuto"></param>
        /// <returns></returns>
        private object fc_VerifCreeCptFourn(string strCleAuto)
        {
            object functionReturnValue = null;
            string strCpteFourn = null;
            string strCodeFourn = null;
            string strRequete = null;
            DataTable rsCptFourn = default(DataTable);
            string[,] tabFourn = null;
            int i = 0;

            if (string.IsNullOrEmpty(strCleAuto) || !General.IsNumeric(strCleAuto))
            {
                return functionReturnValue;
            }

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                SAGE.fc_OpenConnSage();
            }

            rsCptFourn = new DataTable();
            var ModAdo = new ModAdo();
            rsCptFourn = ModAdo.fc_OpenRecordSet("SELECT * FROM FournisseurArticle WHERE CleAuto=" + strCleAuto + "");
            if (rsCptFourn.Rows.Count > 0)
            {
                tabFourn = new string[3, rsCptFourn.Columns.Count + 1];
                for (i = 0; i <= rsCptFourn.Columns.Count - 1; i++)
                {
                    tabFourn[1, i] = rsCptFourn.Columns[i].ColumnName;
                    tabFourn[2, i] = rsCptFourn.Rows[0][i] + "";
                }
            }
            else
            {
                ModAdo.fc_CloseRecordset(rsCptFourn);
                rsCptFourn = null;
                return functionReturnValue;
            }


            strCpteFourn = SAGE.fc_FormatCompte(fc_RecupValue(tabFourn, "Code"), 2);

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                strCpteFourn = ModAdo.fc_ADOlibelle("SELECT CT_NUM FROM F_COMPTET WHERE CT_Num='" + strCpteFourn + "'", false, SAGE.adoSage);

                if (!string.IsNullOrEmpty(strCpteFourn))
                {
                    General.Execute("UPDATE FournisseurArticle SET NCompte='" + strCpteFourn + "' WHERE CleAuto=" + strCleAuto);
                    txtCompteFournisseur.Text = strCpteFourn;
                }

                SAGE.fc_CloseConnSage();
            }
            return functionReturnValue;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="tabTable"></param>
        /// <param name="strItem"></param>
        /// <returns></returns>
        private string fc_RecupValue(string[,] tabTable, string strItem)
        {
            string functionReturnValue = null;
            int i = 0;

            for (i = 0; i < tabTable.GetLength(1); i++)
            {
                if (General.UCase(tabTable[1, i]) == General.UCase(strItem))
                {
                    functionReturnValue = tabTable[2, i];
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return functionReturnValue;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_chargeCpteCharges()
        {
            object functionReturnValue = null;
            string sqlCpteCharges = null;
            var rsCpteCharges = default(DataTable);
            string TA_Taux = null;
            int i = 0;

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                return functionReturnValue;
                //MsgBox "Liaison comptabilité inexistante."
            }
            if (string.IsNullOrEmpty(txtCodeFournisseur.Text))
            {
                return functionReturnValue;
            }
            SAGE.fc_OpenConnSage();
            sqlCpteCharges = "SELECT CG_NUM,Intitule,TaxeCG_Num FROM CpteChargeFournisseur " + " WHERE cleAutoFournisseur=" + txtCodeFournisseur.Text + "";
            rsCpteCharges = new DataTable();
            var ModAdo = new ModAdo();
            rsCpteCharges = ModAdo.fc_OpenRecordSet(sqlCpteCharges);

            //cmbCptCharges.Rows.ToList().ForEach(l => l.Delete());
            cmbCptCharges.DataSource = null;
            //cmbDropDwnCptCharges.Rows.ToList().ForEach(l => l.Delete());//====> this control never used.
            if (rsCpteCharges.Rows.Count > 0)//TESTED
            {
                if (rsCpteCharges.Rows.Count == 1)//TESTED
                {
                    TA_Taux = General.Replace(ModAdo.fc_ADOlibelle("SELECT TA_Taux FROM F_Taxe WHERE CG_NUM='" + rsCpteCharges.Rows[0]["TaxeCG_Num"] + "" + "'", false, SAGE.adoSage), ",", ".");
                    txtTauxTVA.Text = TA_Taux;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Compte De Charges");
                    dt.Columns.Add("Intitulé");
                    dt.Columns.Add("Compte TVA");
                    dt.Columns.Add("TA_Taux");
                    dt.Rows.Add(rsCpteCharges.Rows[0]["CG_NUM"], rsCpteCharges.Rows[0]["Intitule"], rsCpteCharges.Rows[0]["TaxeCG_Num"], TA_Taux);
                    cmbCptCharges.DataSource = dt;
                    cmbCptCharges.ValueMember = "Compte De Charges";

                    //cmbDropDwnCptCharges.DataSource = dt;//===> this controle never used
                    //cmbDropDwnCptCharges.ValueMember = "CG_NUM";
                    cmbCptCharges.Value = rsCpteCharges.Rows[0]["CG_NUM"] + "";

                    lblTauxTva.Text = cmbCptCharges.ActiveRow.Cells["Compte TVA"].Value + "";
                }
                else //TESTED
                {
                    TA_Taux = General.Replace(ModAdo.fc_ADOlibelle("SELECT TA_Taux FROM F_Taxe WHERE CG_NUM='" + rsCpteCharges.Rows[0]["TaxeCG_Num"] + "" + "'", false, SAGE.adoSage), ",", ".");
                    txtTauxTVA.Text = TA_Taux;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Compte De Charges");
                    dt.Columns.Add("Intitulé");
                    dt.Columns.Add("Compte TVA");
                    dt.Columns.Add("TA_Taux");
                    dt.Rows.Add(rsCpteCharges.Rows[0]["CG_NUM"], rsCpteCharges.Rows[0]["Intitule"], rsCpteCharges.Rows[0]["TaxeCG_Num"], TA_Taux);
                    cmbCptCharges.ValueMember = "Compte De Charges";
                    cmbCptCharges.DataSource = dt;
                    cmbCptCharges.Value = rsCpteCharges.Rows[0]["CG_NUM"];

                    lblTauxTva.Text = cmbCptCharges.ActiveRow.GetCellValue("Compte TVA") + "";
                    foreach (DataRow dr in rsCpteCharges.Rows)
                    {
                        if (!dr.Equals(rsCpteCharges.Rows[0]))
                        {
                            TA_Taux = General.Replace(ModAdo.fc_ADOlibelle("SELECT TA_Taux FROM F_Taxe WHERE CG_NUM='" + rsCpteCharges.Rows[0]["TaxeCG_Num"] + "" + "'", false, SAGE.adoSage), ",", ".");

                            dt.Rows.Add(rsCpteCharges.Rows[0]["CG_NUM"], rsCpteCharges.Rows[0]["Intitule"], rsCpteCharges.Rows[0]["TaxeCG_Num"], TA_Taux);
                        }
                    }

                    cmbCptCharges.DataSource = dt;
                    cmbCptCharges.ValueMember = "Compte De Charges";
                    cmbCptCharges.Value = rsCpteCharges.Rows[0]["CG_NUM"] + "";
                    //the control cmbDropDwnCptCharges is never used
                    //cmbDropDwnCptCharges.DataSource = dt;
                    //cmbDropDwnCptCharges.ValueMember = "CG_NUM";

                }
            }
            else//TESTED
            {
                rsCpteCharges = null;
                //sqlCpteCharges = "SELECT DISTINCT F_COMPTEG.CG_NUM , F_COMPTEG.CG_INTITULE AS [Intitule], F_TAXE.CG_NUM AS TaxeCG_Num, F_TAXE.TA_TAUX AS TA_Taux FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                //& " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)=8 ) " _
                //& "  ORDER BY F_COMPTEG.CG_NUM"

                sqlCpteCharges = "SELECT DISTINCT F_COMPTEG.CG_NUM , F_COMPTEG.CG_INTITULE AS [Intitule], F_TAXE.CG_NUM AS TaxeCG_Num, F_TAXE.TA_TAUX AS TA_Taux FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" + " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)>=3 ) " + "  ORDER BY F_COMPTEG.CG_NUM";


                rsCpteCharges = ModAdo.fc_OpenRecordSet(sqlCpteCharges, null, "", SAGE.adoSage);
                if (rsCpteCharges.Rows.Count > 0)
                {
                    TA_Taux = General.Replace(ModAdo.fc_ADOlibelle("SELECT TA_Taux FROM F_Taxe WHERE CG_NUM='" + rsCpteCharges.Rows[0]["TaxeCG_Num"] + "" + "'", false, SAGE.adoSage), ",", ".");
                    txtTauxTVA.Text = TA_Taux;
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Compte De Charges");
                    dt.Columns.Add("Intitulé");
                    dt.Columns.Add("Compte TVA");
                    dt.Columns.Add("TA_Taux");
                    dt.Rows.Add(rsCpteCharges.Rows[0]["CG_NUM"], rsCpteCharges.Rows[0]["Intitule"], rsCpteCharges.Rows[0]["TaxeCG_Num"] + "", TA_Taux);
                    cmbCptCharges.DataSource = dt;
                    cmbCptCharges.ValueMember = "Compte De Charges";
                    cmbCptCharges.Value = rsCpteCharges.Rows[0]["CG_NUM"];
                    lblTauxTva.Text = cmbCptCharges.ActiveRow.GetCellValue("Compte TVA") + "";
                    foreach (DataRow dr in rsCpteCharges.Rows)
                    {
                        if (!dr.Equals(rsCpteCharges.Rows[0]))
                        {
                            TA_Taux = General.Replace(ModAdo.fc_ADOlibelle("SELECT TA_Taux FROM F_Taxe WHERE CG_NUM='" + dr["TaxeCG_Num"] + "" + "'", false, SAGE.adoSage), ",", ".");
                            dt.Rows.Add(dr["CG_NUM"], dr["Intitule"], dr["TaxeCG_Num"] + "", TA_Taux);
                        }

                    }
                    cmbCptCharges.DataSource = dt;
                    cmbCptCharges.ValueMember = "Compte De Charges";
                    cmbCptCharges.Value = rsCpteCharges.Rows[0]["CG_NUM"] + "";
                    //cmbDropDwnCptCharges.DataSource = dt;
                    //cmbDropDwnCptCharges.ValueMember = "CG_NUM";
                }
            }
            //ModAdo.Close();
            rsCpteCharges = null;
            //cmbDropDwnCptCharges.DataFieldList = "column 0";

            //== modif rachid du 20 aout 2005, la liste des fournisseurd
            //GridFactFournDetail.Columns("Compte de Charge").DropDownHwnd = cmbDropDwnCptCharges.hWnd
            SAGE.fc_CloseConnSage();
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)//TESTED
        {
            ssFactFournHtTvaCpta.UpdateData();

            if (!string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                fc_Save();
            }
            else
            {
                cmdAjouter_Click(cmdAjouter, new System.EventArgs());
                fc_Save();
            }

            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private bool fc_Save()
        {
            bool functionReturnValue = false;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {

                lblNomModif.Text = General.gsUtilisateur;
                lblDateModif.Text = Convert.ToString(DateTime.Now);

                if (string.IsNullOrEmpty(txtCodeFournisseur.Text))//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un fournisseur.", "Fournisseur inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRaisonFournisseur.Focus();
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(txtCompteFournisseur.Text))//TESTED
                {
                    fc_VerifCreeCptFourn((txtCodeFournisseur.Text));
                    if (string.IsNullOrEmpty(txtCompteFournisseur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fournisseur n'a pas de compte : impossible d'enregistrer cette facture", "Enregistrement annulé", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                }

                if (string.IsNullOrEmpty(txtDateFacture.Text))//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date de facture.", "Date de facture manquante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDateFacture.Focus();
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                else if (!General.IsDate(txtDateFacture.Text))//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi de date valide.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDateFacture.Focus();
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                else//TESTED
                {
                    txtDateFacture.Text = String.Format(txtDateFacture.Text, "dd/mm/yyyy");
                }
                if (string.IsNullOrEmpty(TxtNoFacture.Text))//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi de référence de facture", "Référence manquante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    TxtNoFacture.Focus();
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(cmbCptCharges.Text))//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi de compte de charge pour cette facture.", "Compte de charge manquant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbCptCharges.Focus();
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(txtModeRgt.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fournisseur n'a pas de mode de règlement associé.\r\n Veuillez en choisir un.", "Mode de règlement manquant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    var _with8 = new frmModeReglement();
                    _with8.lblNomFournisseur.Text = txtRaisonFournisseur.Text;
                    _with8.txtCleAuto.Text = txtCodeFournisseur.Text;
                    _with8.ShowDialog();
                    txtModeRgt.Text = _with8.ComboModeRgt.ActiveRow.GetCellValue("Libelle") + "";
                    fc_Reglement((txtModeRgt.Text));
                    if (General.IsDate(txtDateFacture.Text))
                    {
                        fc_CalculEcheance(Convert.ToDateTime(txtDateFacture.Text), Convert.ToInt32(General.nz((txtNbreJour.Text), "0")), (txtTypeEcheance.Text), Convert.ToInt32(General.nz(txtEcheance.Text, "1")));
                    }
                    _with8.Close();
                }
                var ModAdo = new ModAdo();
                if (string.IsNullOrEmpty(ModAdo.fc_ADOlibelle("SELECT FactFournDetail.NoAutoFacture FROM FactFournDetail INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=FactFournEntete.NoAutoFacture WHERE FactFournDetail.NoAutoFacture=" + txtNoAutoFacture.Text + " AND (NOT FactFournDetail.EC_NoCptCharge IS NULL OR NOT FactFournDetail.EC_NoCptTva IS NULL OR NOT FactFournEntete.EC_NoFacture IS NULL)")))//TESTED
                {
                    fc_SaveEntete();
                    fc_SaveDetail();
                    fc_SavePied();
                    ChargeFactureDetailPied();
                    fc_ModifBCMD();
                }

                //== modif rachid
                fc_CalTVA();

                if (chkIntegration.CheckState == CheckState.Checked)
                {
                    fc_IntegrerFacture((txtNoAutoFacture.Text));
                }

                cmdRechercheRaisonSociale.Visible = false;
                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                //Debug.Print(Err().Description);
                Erreurs.gFr_debug(ex, this.Name + " Erreur Save ");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_CalTTC()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            double dbTTC = 0;
            double dbHT = 0;
            double dbTVA = 0;
            sSQL = "SELECT TaxeCG_NUM, SUM(MontantLigne)  AS HT," + " SUM(MontantLigne) AS HT, SUM(MontantLigne) + SUM(MontantLigne)" + " * (TaxeTA_Taux / 100) AS TTC" + " From FactFournDetail" + " Where (NoAutoFacture = " + General.nz(txtNoAutoFacture.Text, 0) + ") And (QteLivre > 0)" + " GROUP BY TaxeCG_NUM, TaxeTA_Taux" + " Having (Not (Sum(MontantLigne) * (TaxeTA_Taux / 100) Is Null)) " + " And (Sum(MontantLigne) * (TaxeTA_Taux / 100) <> 0)" + " ORDER BY TaxeCG_NUM";
            ModAdo ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count > 0)
            {
                //=== calcul du ht
                dbHT = 0;
                foreach (DataRow dr in rs.Rows)
                {
                    dbHT = dbHT + General.FncArrondir(Convert.ToDouble(General.nz(dr["HT"], 0)));
                }
                //== calcul de la tva.
                sSQL = "SELECT     NoAutoFacture, CptTVA  AS TaxeCG_Num, SUM(TVA) AS TVA" + " From FactFournHtTvaCpta" + " GROUP BY NoAutoFacture, CptTVA" + " HAVING      (NoAutoFacture =" + General.nz(txtNoAutoFacture.Text, 0) + ")";
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow dr in rs.Rows)
                    {
                        dbTVA = dbTVA + General.FncArrondir(Convert.ToDouble(General.nz(dr["TVA"], 0)), 2);
                    }
                }
                dbTTC = dbHT + dbTVA;
            }
            txtTTC.Text = Convert.ToString(dbTTC);
            ModAdo.Close();
            rs = null;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        public void fc_CalTVA()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            sSQL = "SELECT TaxeCG_NUM, SUM(MontantLigne) * (TaxeTA_Taux / 100) AS TVA," + " SUM(MontantLigne) AS HT, SUM(MontantLigne) + SUM(MontantLigne)" + " * (TaxeTA_Taux / 100) AS TTC" + " From FactFournDetail" + " Where (NoAutoFacture = " + General.nz(txtNoAutoFacture.Text, 0) + ") And (QteLivre > 0)" + " GROUP BY TaxeCG_NUM, TaxeTA_Taux" + " Having (Not (Sum(MontantLigne) * (TaxeTA_Taux / 100) Is Null)) " + " And (Sum(MontantLigne) * (TaxeTA_Taux / 100) <> 0)" + " ORDER BY TaxeCG_NUM";

            ModAdo ModAdo = new ModAdo();

            rs = ModAdo.fc_OpenRecordSet(sSQL);

            ModAdo ModAdoForUpdate = new ModAdo();
            sSQL = "SELECT     NoAutoFacture, CptTVA,  TVA" + " From FactFournHtTvaCpta" + " WHERE  NoAutoFacture =" + General.nz(txtNoAutoFacture.Text, 0);

            rsTVAcal = ModAdoForUpdate.fc_OpenRecordSet(sSQL);
            if (chkAjusteMtTVA.CheckState == 0)//TESTED
            {
                for (int i = 0; i < rsTVAcal.Rows.Count; i++)
                {
                    rsTVAcal.Rows[i].Delete();
                    //ModAdoForUpdate.Update();
                }
                foreach (DataRow dr in rs.Rows)
                {
                    var dr1 = rsTVAcal.NewRow();
                    dr1["CptTVA"] = dr["TaxeCG_Num"];
                    dr1["TVA"] = General.FncArrondir(Convert.ToDouble(dr["TVA"]), 2);
                    // rsAdd!HT = FncArrondir(rs!HT, 2)
                    //  rsTVAcal!TTC = FncArrondir(rs!TTC, 2)
                    dr1["NoAutoFacture"] = txtNoAutoFacture.Text;
                    /*rsTVAcal.Update();*/
                    rsTVAcal.Rows.Add(dr1);

                }
                ModAdoForUpdate.Update();
            }

            //ssFactFournHtTvaCpta.DataSource = rs;
            ssFactFournHtTvaCpta.DataSource = rsTVAcal;
            fc_CalTTC();
            fc_lockGridTVA();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactFournHtTvaCpta_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="strCodeReglement"></param>
        private void fc_Reglement(string strCodeReglement)
        {
            string strReq = null;
            var rsReglement = default(DataTable);
            strReq = "SELECT NombreJour,TypeEcheance,Echeance FROM CodeReglement WHERE Libelle='" + strCodeReglement + "'";
            rsReglement = new DataTable();
            ModAdo ModAdo = new ModAdo();
            rsReglement = ModAdo.fc_OpenRecordSet(strReq);

            if (rsReglement.Rows.Count > 0)//TESTED
            {
                txtNbreJour.Text = rsReglement.Rows[0]["NombreJour"] + "";
                txtTypeEcheance.Text = rsReglement.Rows[0]["TypeEcheance"] + "";
                txtEcheance.Text = rsReglement.Rows[0]["Echeance"] + "";
            }
            lblModeReglement.Text = strCodeReglement;
            ModAdo.Close();
            rsReglement = null;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public object fc_SaveEntete()
        {
            object functionReturnValue = null;
            string sqlEntete = null;
            string sqlSage = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sqlEntete = "UPDATE FactFournEntete SET CodeFournisseur=" + General.nz((txtCodeFournisseur.Text), "''") + ",";
                sqlEntete = sqlEntete + " refFacture='" + TxtNoFacture.Text + "', ";
                //    sqlEntete = sqlEntete & " Montant=" & nz(txtMontantFacture.Text, "0") & ", "
                sqlEntete = sqlEntete + " CG_Num='" + cmbCptCharges.Text + "', ";
                sqlEntete = sqlEntete + " TaxeCG_Num='" + lblTauxTva.Text + "', ";
                sqlEntete = sqlEntete + " DateLivraison='" + General.nz(txtDateFacture.Text, DateTime.Today) + "',";
                sqlEntete = sqlEntete + " Commentaire='" + txtCommentaire.Text + "', ";
                sqlEntete = sqlEntete + " DateModif='" + lblDateModif.Text + "', ";
                sqlEntete = sqlEntete + " FactureModif='" + lblNomModif.Text + "', ";
                sqlEntete = sqlEntete + " CT_NUM='" + txtCompteFournisseur.Text + "', ";
                sqlEntete = sqlEntete + " ModeRgt='" + txtModeRgt.Text + "', ";
                sqlEntete = sqlEntete + " AjusterMtTVA='" + General.nz(Convert.ToInt32(chkAjusteMtTVA.CheckState), 0) + "', ";
                sqlEntete = sqlEntete + " DateEcheance='" + txtDateEcheance.Text + "' ";
                sqlEntete = sqlEntete + " WHERE NoAutoFacture=" + txtNoAutoFacture.Text;

                General.Execute(sqlEntete);
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "Erreur SaveEntete ");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public object fc_SaveDetail()
        {
            object functionReturnValue = null;
            string sqlDetail = null;
            string TA_Taux = null;
            int i = 0;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sqlDetail = "DELETE FROM FactFournDetail WHERE FactFournDetail.NoAutoFacture=" + txtNoAutoFacture.Text + " ";
                General.Execute(sqlDetail);
                i = 0;
                if (GridFactFournDetail.Rows.Count > 0)
                {
                    //while (!(i == GridFactFournDetail.Rows))
                    foreach (var row in GridFactFournDetail.Rows)
                    {
                        i = i + 1;
                        sqlDetail = "INSERT INTO FactFournDetail (NoAutoFacture,NoLigne,NoBCmd,RefLigneCmd,DesignationLigneCmd,QteCommande,MontantLigne,CG_NUM,TaxeCG_Num,TaxeTA_Taux,QteLivre) ";
                        sqlDetail = sqlDetail + " VALUES ( ";
                        sqlDetail = sqlDetail + txtNoAutoFacture.Text + ",";
                        sqlDetail = sqlDetail + "" + i + ",";
                        sqlDetail = sqlDetail + "'" + row.Cells["NoBcMd"].Value + "" + "',";
                        sqlDetail = sqlDetail + "'" + StdSQLchaine.gFr_DoublerQuote(row.Cells["Ref. Fourniture"].Value + "") + "',";
                        sqlDetail = sqlDetail + "'" + StdSQLchaine.gFr_DoublerQuote(row.Cells["Designation"].Value + "") + "',";
                        sqlDetail = sqlDetail + "'" + row.Cells["Qté. Commandée"].Value + "" + "',";
                        sqlDetail = sqlDetail + "'" + General.Replace(row.Cells["Prix"].Value + "", ",", ".") + "',";
                        sqlDetail = sqlDetail + "'" + row.Cells["Compte De Charge"].Value + "" + "',";
                        sqlDetail = sqlDetail + "'" + row.Cells["Compte TVA"].Value + "" + "',";
                        sqlDetail = sqlDetail + "" + General.Replace(General.nz((row.Cells["TA_Taux"].Value + ""), "0"), ",", ".") + ",";// on replace
                        sqlDetail = sqlDetail + "'" + row.Cells["Qté. Facturée"].Value + "" + "'";
                        sqlDetail = sqlDetail + ")";
                        General.Execute(sqlDetail);

                    }
                    GridFactFournDetail.UpdateData();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Erreur SaveDetail ");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="strNoAutoFacture"></param>
        /// <returns></returns>
        private object fc_IntegrerFacture(string strNoAutoFacture)
        {
            object functionReturnValue = null;

            const string c1 = "1";
            //Plan analytique Activité (plan principal)
            const string c2 = "2";
            //Plan analytique Affaire
            const string c3 = "3";
            //Plan analytique Immeuble

            string strCT_Num = null;
            //Nom du compte Fournisseur
            string strCG_Num = null;
            //Numéro du compte fournisseur
            string strCA_Num = null;
            //Nom du compte analytique (pour chaque immeuble
            //et pour chaque affaire)
            string strCloture = null;
            //Etat du journal d'achat
            string strJo_Num = null;
            //Nom du journal des Achats
            string strJM_Date = null;
            //Date du journal des achats
            string[] tabCptCharges = null;
            //Tableau comptenant les différents
            //comptes de charges
            string[] tabCptTVA = null;
            //Tableau comptenant les différents
            //comptes de TVA
            string[] TabCptFournisseur = null;
            //Tableau comptenant les différents
            //comptes de Charges
            string[] tabMontant = null;
            //Tableau des montants
            string[] tabCptAnalytiqueImm = null;
            //Tableau des comptes analytiques Immeuble
            string[] tabCptAnalytiqueAffaire = null;
            //Tableau des comptes analytiques Affaire
            string[] tabCptAnalytiqueActivite = null;
            //Tableau des comptes analytiques Activité
            string[] tabCA_Num = null;
            //Tableau des comptes analytiques
            string[] tabEC_No = null;
            //Tableau de lien avec les champs
            //EC_No
            string[] tabTauxBase = null;
            //Base du taux : 19.6% par exemple
            string sAnalytiqueActivite = null;


            string strReqIns = null;
            string strRequete = null;
            int i = 0;
            int nbRecAff = 0;
            int MaxRT_No = 0;
            //Pour l'intégration dans le registre de taxe
            bool Ecrit = false;
            bool blnTrans = false;
            int intDeroule = 0;

            string NoFactureSage = null;
            //Sert uniquement à afficher le numéro
            //d'enregistrement sage à la fin de l'intégration
            string sSQLn = null;

            DataTable rsCpta = default(DataTable);
            try
            {
                SqlCommand cmd;
                double dbTTC = 0;
                double dbHT = 0;
                double dbTVA = 0;
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante..");
                    return functionReturnValue;
                }

                intDeroule = 0;
                //Démarrage et ouverture de la connexion Sage

                Ecrit = false;
                blnTrans = false;
                dbHT = 0;
                dbTTC = 0;
                dbTVA = 0;

                SAGE.fc_OpenConnSage();
                intDeroule = 1;
                //Liaison au journal de charge : lecture et éventuellement écriture

                // Récupération du compte de vente (propre à chaque société)
                strJo_Num = General.gfr_liaison("JournalCharges");
                //Définition de la date du journal par rapport à celle de la facture
                strJM_Date = "01/" + Convert.ToDateTime(txtDateFacture.Text).Month + "/" + Convert.ToDateTime(txtDateFacture.Text).Year;
                strJM_Date = Convert.ToDateTime(strJM_Date).ToString("dd/MM/yyyy");
                //Numéro de compte asocié au plan du compte fournisseur
                var ModAdo = new ModAdo();
                strCG_Num = ModAdo.fc_ADOlibelle("SELECT CG_NumPrinc FROM F_CompteT WHERE CT_Num='" + txtCompteFournisseur.Text + "'", false, SAGE.adoSage);
                if (string.IsNullOrEmpty(ModAdo.fc_ADOlibelle("SELECT JO_Num FROM F_JMOUV WHERE JO_NUM='" + strJo_Num + "' AND JM_DATE='" + strJM_Date + "'", false, SAGE.adoSage)))//TESTED
                {
                    cmd = new SqlCommand("INSERT INTO F_JMOUV(JO_Num, JM_Date, JM_Cloture, JM_Impression)" + " VALUES('" + strJo_Num + "', '" + strJM_Date + "', 0, 0)", SAGE.adoSage);
                    cmd.ExecuteNonQuery();
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ouverture du journal d'achat pour la période du " + strJM_Date, "Ouverture du journal d'achat", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    strCloture = "0";
                }
                else//TESTED
                {
                    //Récupération de l'état de clôture du journal
                    strCloture = ModAdo.fc_ADOlibelle("SELECT JM_Cloture FROM F_JMOUV WHERE JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'");
                }
                if (strCloture == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le journal d'achat " + strJo_Num + " du " + strJM_Date + " est clôturé \r\n Cette facture ne peut être intégrée pour la date inscrite", "Intégration annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtDateFacture.Focus();
                    return functionReturnValue;
                }
                else if (string.IsNullOrEmpty(strCloture))//TESTED
                {
                    //Ecriture du JM_Cloture à 0 car celui-ci a la valeur <Null>
                    cmd = new SqlCommand("UPDATE F_JMOUV SET JM_Cloture=0 WHERE  JO_Num='" + strJo_Num + "' AND JM_Date='" + strJM_Date + "'", SAGE.adoSage);
                    nbRecAff = cmd.ExecuteNonQuery();
                }

                //Début de la transaction Sage (sur adoSage) afin de pouvoir revenir en arrière
                //en cas de problèmes

                intDeroule = 2;
                cmd = new SqlCommand("BEGIN TRANSACTION Trans1" + General.Replace(General.gsUtilisateur, " ", ""), SAGE.adoSage);
                cmd.ExecuteNonQuery();
                blnTrans = true;
                rsCpta = new DataTable();
                strRequete = "SELECT CG_NUM,SUM(MontantLigne) AS Total FROM FactFournDetail ";
                strRequete = strRequete + " WHERE NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (EC_NoCptCharge IS NULL) AND QteLivre>=0 ";//TODO change this to >
                strRequete = strRequete + " GROUP BY CG_NUM ";
                strRequete = strRequete + " HAVING (NOT (SUM(MontantLigne) IS NULL)) AND (SUM(MontantLigne) <> 0)";
                strRequete = strRequete + " ORDER BY CG_NUM ";
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);
                if (rsCpta.Rows.Count > 0)//TESTED
                {
                    Ecrit = true;
                    i = 0;
                    Array.Resize(ref tabCptCharges, 1);
                    Array.Resize(ref tabMontant, 1);
                    foreach (DataRow dr in rsCpta.Rows)//TESTED
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }
                        tabCptCharges[i] = dr["CG_NUM"] + "";
                        tabMontant[i] = dr["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbHT = dbHT + Convert.ToDouble(tabMontant[i]);
                        i = i + 1;
                    }
                    ModAdo.fc_CloseRecordset(rsCpta);

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + TxtNoFacture.Text + "'";

                    rsCpta = ModAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);

                    for (i = 0; i <= tabCptCharges.Length - 1; i++)//TESTED
                    {
                        if (tabCptCharges[i] != null)
                        {
                            var _with13 = rsCpta.NewRow();
                            _with13["JO_Num"] = strJo_Num;
                            _with13["EC_NoLink"] = 0;
                            _with13["JM_Date"] = strJM_Date;
                            _with13["EC_Jour"] = Convert.ToDateTime(txtDateFacture.Text).Day;
                            _with13["EC_Date"] = string.Format(txtDateFacture.Text, General.FormatDateSansHeureSQL);
                            _with13["EC_Piece"] = General.Left(TxtNoFacture.Text, 13);
                            _with13["EC_RefPiece"] = General.Left(TxtNoFacture.Text, 17);
                            _with13["CG_NUM"] = tabCptCharges[i];
                            _with13["EC_Intitule"] = General.Left(txtRaisonFournisseur.Text, 35);
                            _with13["N_Reglement"] = 0;
                            _with13["EC_Parite"] = 6.55957;
                            _with13["EC_Quantite"] = 0;
                            _with13["N_Devise"] = 2;
                            //1:Franc 2:Euro 0:Aucun
                            _with13["EC_Sens"] = 0;
                            _with13["EC_No"] = 0;
                            _with13["EC_Montant"] = tabMontant[i];
                            _with13["EC_Lettre"] = 0;
                            _with13["EC_Point"] = 0;
                            _with13["EC_Impression"] = 0;
                            _with13["EC_Cloture"] = 0;
                            _with13["EC_CType"] = 0;
                            _with13["EC_Rappel"] = 0;
                            rsCpta.Rows.Add(_with13);
                            ModAdo.Update();
                            //TODO .triger set the value of column "EC_NO" by get MAX of("EC_NO") + 1 .i use this code to get this value. in vb6 they use just fucntion update();
                            //begin
                            string req = "select MAX(EC_NO) from F_ECRITUREC";
                            SqlCommand cmd1 = new SqlCommand(req, SAGE.adoSage);
                            if (SAGE.adoSage.State != ConnectionState.Open)
                                SAGE.fc_OpenConnSage();
                            _with13["EC_NO"] = cmd1.ExecuteScalar();
                            //end
                            nbRecAff = General.Execute("UPDATE FactFournDetail SET EC_NoCptCharge=" + _with13["EC_No"] + "" + " WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND CG_NUM='" + tabCptCharges[i] + "'");
                        }
                    }
                }
                ModAdo.fc_CloseRecordset(rsCpta);
                if (chkAjusteMtTVA.CheckState == CheckState.Unchecked)//TESTED
                {

                    strRequete = "SELECT CG_Num,TaxeCG_NUM,(SUM(MontantLigne)*(TaxeTA_Taux/100)) AS Total FROM FactFournDetail ";
                    strRequete = strRequete + " WHERE NoAutoFacture=" + strNoAutoFacture + " AND ";
                    strRequete = strRequete + " (EC_NoCptTva IS NULL) AND QteLivre>0";
                    strRequete = strRequete + " GROUP BY CG_Num,TaxeCG_NUM,TaxeTA_Taux ";
                    strRequete = strRequete + " HAVING (NOT (SUM(MontantLigne)*(TaxeTA_Taux/100) IS NULL)) AND (SUM(MontantLigne)*(TaxeTA_Taux/100) <> 0)";
                    strRequete = strRequete + " ORDER BY TaxeCG_NUM,CG_Num";
                }
                else
                {
                    strRequete = "SELECT     NoAutoFacture, CptTVA  AS TaxeCG_Num, SUM(TVA) AS TOTAL" + " From FactFournHtTvaCpta" + " GROUP BY NoAutoFacture, CptTVA" + " HAVING      (NoAutoFacture =" + General.nz(txtNoAutoFacture, 0) + ")";
                }
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);
                if (rsCpta.Rows.Count > 0)//TESTED
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabCptCharges = new string[1];
                    tabMontant = new string[1];
                    foreach (DataRow dr in rsCpta.Rows)//tested
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabCptCharges, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                        }
                        tabCptTVA[i] = dr["TaxeCG_Num"] + "";
                        tabMontant[i] = dr["Total"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        dbTVA = Convert.ToDouble(tabMontant[i]) + dbTVA;
                        if (chkAjusteMtTVA.CheckState == CheckState.Unchecked)
                        {
                            tabCptCharges[i] = dr["CG_NUM"] + "";
                        }
                        else if (chkAjusteMtTVA.CheckState == CheckState.Checked)
                        {
                            tabCptCharges[i] = "";
                        }
                        i = i + 1;
                    }

                    ModAdo.fc_CloseRecordset(rsCpta);

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont,  EC_Intitule,";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + TxtNoFacture.Text + "'";

                    rsCpta = ModAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)
                    {
                        var _with14 = rsCpta.NewRow();
                        _with14["JO_Num"] = strJo_Num;
                        _with14["EC_NoLink"] = 0;
                        _with14["JM_Date"] = strJM_Date;
                        _with14["EC_Jour"] = Convert.ToDateTime(txtDateFacture.Text).Day;
                        _with14["EC_Date"] = string.Format(txtDateFacture.Text, General.FormatDateSansHeureSQL);
                        _with14["EC_Piece"] = General.Left(TxtNoFacture.Text, 13);
                        _with14["EC_RefPiece"] = General.Left(TxtNoFacture.Text, 17);
                        _with14["CG_NUM"] = tabCptTVA[i];
                        //!CG_NumCont = tabCptCharges(i)
                        _with14["EC_Intitule"] = General.Left(txtRaisonFournisseur.Text, 35);
                        _with14["N_Reglement"] = 0;
                        _with14["EC_Parite"] = 6.55957;
                        _with14["EC_Quantite"] = 0;
                        _with14["N_Devise"] = 2;
                        _with14["EC_Sens"] = 0;
                        _with14["EC_Montant"] = tabMontant[i];
                        _with14["EC_Lettre"] = 0;
                        _with14["EC_Point"] = 0;
                        _with14["EC_Impression"] = 0;
                        _with14["EC_Cloture"] = 0;
                        _with14["EC_CType"] = 0;
                        _with14["EC_Rappel"] = 0;
                        _with14["EC_No"] = 0;
                        rsCpta.Rows.Add(_with14);
                        ModAdo.Update();
                        //TODO .triger set the value of column "EC_NO" by get MAX of("EC_NO") + 1 .i use this code to get this value. in vb6 they use just fucntion update();
                        //begin
                        string req = "select MAX(EC_NO) from F_ECRITUREC";
                        SqlCommand cmd1 = new SqlCommand(req, SAGE.adoSage);
                        if (SAGE.adoSage.State != ConnectionState.Open)
                            SAGE.fc_OpenConnSage();
                        _with14["EC_NO"] = cmd1.ExecuteScalar();
                        //end
                        General.Execute("UPDATE FactFournDetail SET EC_NoCptTva=" + _with14["EC_No"] + " WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND TaxeCG_NUM='" + tabCptTVA[i] + "' AND " + " CG_Num='" + tabCptCharges[i] + "'");
                    }
                }
                //       1-3 - Une ligne pour le compte Fournisseur avec montant total TTC
                //        Calcul du nombre de compte de TVA et stockage de ceux-ci dans un tableau
                //        Récupération des montants TTC pour chaque compte de Charge
                ModAdo.fc_CloseRecordset(rsCpta);
                strRequete = "SELECT FactFournDetail.CG_Num,(SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) AS Total FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=FactFournEntete.NoAutoFacture ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournEntete.EC_NoFacture IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY FactFournDetail.CG_Num,FactFournDetail.TaxeTA_Taux ";
                strRequete = strRequete + " HAVING (NOT (SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) IS NULL) AND ((SUM(FactFournDetail.MontantLigne)+(SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))) <> 0)";
                strRequete = strRequete + " ORDER BY FactFournDetail.CG_Num";
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);


                if (chkAjusteMtTVA.CheckState == CheckState.Checked)
                {
                    dbTTC = dbHT + dbTVA;
                }
                if (rsCpta.Rows.Count > 0)//TESTED
                {
                    Ecrit = true;
                    i = 0;
                    TabCptFournisseur = new string[1];
                    tabMontant = new string[1];
                    if (rsCpta.Rows.Count > 0)
                    {
                        TabCptFournisseur[0] = rsCpta.Rows[0]["CG_NUM"] + "";//TODO==> to look after
                    }
                    tabMontant[0] = Convert.ToString(0);
                    foreach (DataRow dr in rsCpta.Rows)
                    {
                        tabMontant[0] = Convert.ToString(Convert.ToDouble(tabMontant[0]) + General.FncArrondir(Convert.ToDouble(General.nz(dr["Total"], "0")), 2));
                    }
                    tabMontant[0] = Convert.ToString(FncArrondirFact(tabMontant[0]));
                    ModAdo.fc_CloseRecordset(rsCpta);

                    strReqIns = "SELECT ";
                    strReqIns = strReqIns + "EC_No, JO_Num,   EC_NoLink,   JM_Date, EC_Jour, EC_Date, EC_Piece,";
                    strReqIns = strReqIns + "EC_RefPiece,   CG_Num, CG_NumCont, CT_Num,  EC_Intitule,EC_Echeance, ";
                    strReqIns = strReqIns + "N_Reglement, EC_Parite,   EC_Quantite, N_Devise,";
                    strReqIns = strReqIns + "EC_Sens, EC_Montant,  EC_Lettre, EC_Point,";
                    strReqIns = strReqIns + "EC_Impression,   EC_Cloture,  EC_CType,    EC_Rappel";
                    strReqIns = strReqIns + " FROM F_ECRITUREC WHERE JO_NUM='" + strJo_Num + "'";
                    strReqIns = strReqIns + " AND EC_PIECE='" + TxtNoFacture.Text + "'";

                    rsCpta = ModAdo.fc_OpenRecordSet(strReqIns, null, "", SAGE.adoSage);
                    for (i = 0; i <= TabCptFournisseur.Length - 1; i++)//TESTED
                    {
                        var _with15 = rsCpta.NewRow();
                        _with15["JO_Num"] = strJo_Num;
                        _with15["EC_NoLink"] = 0;
                        _with15["JM_Date"] = strJM_Date;
                        _with15["EC_Jour"] = Convert.ToDateTime(txtDateFacture.Text).Day;
                        _with15["EC_Date"] = string.Format(txtDateFacture.Text, General.FormatDateSansHeureSQL);
                        _with15["EC_Piece"] = General.Left(TxtNoFacture.Text, 13);
                        _with15["EC_RefPiece"] = General.Left(TxtNoFacture.Text, 17);
                        _with15["CG_NUM"] = strCG_Num;
                        // !CG_NumCont = TabCptFournisseur(i)
                        _with15["CT_NUM"] = txtCompteFournisseur.Text;
                        _with15["EC_Intitule"] = General.Left(txtRaisonFournisseur.Text, 35);
                        _with15["EC_Echeance"] = string.Format(txtDateEcheance.Text, General.FormatDateSansHeureSQL);
                        _with15["N_Reglement"] = 0;
                        _with15["EC_Parite"] = 6.55957;
                        _with15["EC_Quantite"] = 0;
                        _with15["N_Devise"] = 2;
                        _with15["EC_Sens"] = 1;
                        if (chkAjusteMtTVA.CheckState == CheckState.Unchecked)
                        {
                            _with15["EC_Montant"] = tabMontant[i];
                        }
                        else
                        {
                            _with15["EC_Montant"] = dbTTC;
                        }
                        _with15["EC_Lettre"] = 0;
                        _with15["EC_Point"] = 0;
                        _with15["EC_Impression"] = 0;
                        _with15["EC_Cloture"] = 0;
                        _with15["EC_CType"] = 0;
                        _with15["EC_Rappel"] = 0;
                        _with15["EC_No"] = 0;
                        rsCpta.Rows.Add(_with15);

                        ModAdo.Update();
                        //TODO .triger set the value of column "EC_NO" by get MAX of("EC_NO") + 1 .i use this code to get this value. in vb6 they use just fucntion update();
                        //begin
                        string req = "select MAX(EC_NO) from F_ECRITUREC";
                        SqlCommand cmd1 = new SqlCommand(req, SAGE.adoSage);
                        if (SAGE.adoSage.State != ConnectionState.Open)
                            SAGE.fc_OpenConnSage();
                        _with15["EC_NO"] = cmd1.ExecuteScalar();
                        NoFactureSage = _with15["EC_No"] + "";
                        //end
                        txtNoComptable.Text = NoFactureSage;
                        nbRecAff = General.Execute("UPDATE FactFournEntete SET EC_NoFacture=" + _with15["EC_No"] + " WHERE NoAutoFacture=" + txtNoAutoFacture.Text);
                    }
                }
                ModAdo.fc_CloseRecordset(rsCpta);

                //   2 - Table F_EcritureA
                //       2-1 - Une ligne par compte analytique immeuble (montant HT des
                //                   charges de l'immeuble)

                strRequete = "SELECT Immeuble.CodeImmeuble, ";
                strRequete = strRequete + " LEFT(Immeuble.NCompte,13) AS CptAnalytique, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande ON BonDeCommande.NoBonDeCommande=";
                strRequete = strRequete + " FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN Immeuble ON BonDeCommande.CodeImmeuble=";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueImm IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY Immeuble.NCompte,FactFournDetail.EC_NoCptCharge,";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                strRequete = strRequete + " ORDER BY Immeuble.CodeImmeuble ";
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)//TESTED
                {
                    Ecrit = true;
                    i = 0;
                    tabCptAnalytiqueImm = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];
                    foreach (DataRow dr in rsCpta.Rows)//TESTED
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptAnalytiqueImm, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabCptAnalytiqueImm[i] = dr["CodeImmeuble"] + "";
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";
                        tabCA_Num[i] = dr["CptAnalytique"] + "";
                        i = i + 1;
                    }


                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";
                    for (i = 0; i <= tabCptAnalytiqueImm.Length - 1; i++)//TESTED
                    {

                        strRequete = tabEC_No[i] + ",";
                        //EC_No
                        strRequete = strRequete + c3 + ",";
                        //N_Analytique
                        strRequete = strRequete + (i + 1) + ",";
                        //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                        //CA_Num
                        strRequete = strRequete + tabMontant[i] + ",";
                        //EA_Montant
                        strRequete = strRequete + 0;
                        //EA_Quantite
                        strRequete = strRequete + ")";
                        cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        cmd.ExecuteNonQuery();
                        General.Execute("UPDATE FactFournDetail SET AnalytiqueImm='1' WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");
                    }

                }
                ModAdo.fc_CloseRecordset(rsCpta);
                //       2-2 - Une ligne par compte analytique Affaire (montant HT des affaires
                //                   liées aux différents immeubles)

                strRequete = "SELECT Immeuble.CodeImmeuble, ";
                strRequete = strRequete + " LEFT(Immeuble.NCompte,13) AS CptAnalytique, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant, ";
                strRequete = strRequete + " BonDeCommande.NumFicheStandard ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande ON BonDeCommande.NoBonDeCommande=";
                strRequete = strRequete + " FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN Immeuble ON BonDeCommande.CodeImmeuble=";
                strRequete = strRequete + " Immeuble.CodeImmeuble ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueAffaire IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY Immeuble.NCompte,FactFournDetail.EC_NoCptCharge,";
                strRequete = strRequete + " Immeuble.CodeImmeuble, BonDeCommande.NumFicheStandard ";
                strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                strRequete = strRequete + " ORDER BY Immeuble.CodeImmeuble ";
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);
                if (rsCpta.Rows.Count > 0)
                {
                    Ecrit = true;
                    i = 0;
                    tabCptAnalytiqueAffaire = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];
                    foreach (DataRow dr in rsCpta.Rows)
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptAnalytiqueAffaire, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabCptAnalytiqueAffaire[i] = dr["Numfichestandard"] + "";
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";
                        tabCA_Num[i] = "AF" + dr["Numfichestandard"] + "";
                        i = i + 1;
                    }
                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";
                    for (i = 0; i <= tabCptAnalytiqueAffaire.Length - 1; i++)
                    {

                        strRequete = tabEC_No[i] + ",";
                        //EC_No
                        strRequete = strRequete + c2 + ",";
                        //N_Analytique
                        strRequete = strRequete + (i + 1) + ",";
                        //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                        //CA_Num
                        strRequete = strRequete + tabMontant[i] + ",";
                        //EA_Montant
                        strRequete = strRequete + 0;
                        //EA_Quantite
                        strRequete = strRequete + ")";
                        cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        cmd.ExecuteNonQuery();
                        General.Execute("UPDATE FactFournDetail SET AnalytiqueAffaire='1' WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");
                    }
                }
                ModAdo.fc_CloseRecordset(rsCpta);
                //       2-3 - Une ligne par compte analytique Activité (montant HT des activités
                //                   liées aux différents appels
                //                   (récupéré par les bons de commande))

                strRequete = "SELECT GestionStandard.AnalytiqueActivite, ";
                strRequete = strRequete + " FactFournDetail.EC_NoCptCharge AS EC_No,";
                strRequete = strRequete + " SUM(FactFournDetail.MontantLigne) AS Montant ";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN BonDeCommande";
                strRequete = strRequete + "  ON BonDeCommande.NoBonDeCommande = FactFournDetail.NoBCmd ";
                strRequete = strRequete + " INNER JOIN GestionStandard ";
                strRequete = strRequete + " ON BonDeCommande.NumFicheStandard = GestionStandard.NumFicheStandard ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.AnalytiqueActivite IS NULL) AND QteLivre>0 ";
                //== modif rachid l analytique activité depend seulement si il existe une intervention.
                //strRequete = strRequete & " AND (NOT GestionStandard.AnalytiqueActivite IS NULL)"
                strRequete = strRequete + " GROUP BY GestionStandard.AnalytiqueActivite,FactFournDetail.EC_NoCptCharge";
                strRequete = strRequete + " HAVING (NOT SUM(FactFournDetail.MontantLigne) IS NULL) AND SUM(FactFournDetail.MontantLigne)<>0 ";
                strRequete = strRequete + " ORDER BY GestionStandard.AnalytiqueActivite ";

                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);

                if (rsCpta.Rows.Count > 0)//TESTED
                {
                    Ecrit = true;
                    i = 0;
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabCA_Num = new string[1];
                    foreach (DataRow dr in rsCpta.Rows)//TESTED
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabCA_Num, i + 1);
                        }
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";

                        //== modif rachid du 1 aout 2005,l analytique activite se base
                        //== sur l'existence d'un devis ou pas.

                        //'tabCA_Num(i) = rsCpta!AnalytiqueActivite & ""
                        sSQLn = "SELECT DevisEnTete.NumeroDevis" + " FROM BonDeCommande INNER JOIN" + " FactFournDetail " + " ON BonDeCommande.NoBonDeCommande = FactFournDetail.NoBcmd " + " INNER JOIN" + " GestionStandard" + " ON BonDeCommande.NumFicheStandard = GestionStandard.NumFicheStandard" + " INNER JOIN DevisEnTete " + " ON GestionStandard.NumFicheStandard = DevisEnTete.NumFicheStandard" + " WHERE FactFournDetail.NoAutoFacture =" + strNoAutoFacture;

                        sSQLn = ModAdo.fc_ADOlibelle(sSQLn, true);
                        if (string.IsNullOrEmpty(sSQLn))//tested
                        {
                            tabCA_Num[i] = ModAdo.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA " + " WHERE ActiviteTravaux='1'", false, SAGE.adoSage);
                        }
                        else
                        {
                            tabCA_Num[i] = ModAdo.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA " + " WHERE ActiviteDevis='1'", false, SAGE.adoSage);
                        }

                        i = i + 1;
                    }

                    strReqIns = "INSERT INTO F_ECRITUREA (";
                    strReqIns = strReqIns + "EC_No,";
                    strReqIns = strReqIns + "N_Analytique,";
                    strReqIns = strReqIns + "EA_Ligne,";
                    strReqIns = strReqIns + "CA_Num,";
                    strReqIns = strReqIns + "EA_Montant,";
                    strReqIns = strReqIns + "EA_Quantite";
                    strReqIns = strReqIns + ") VALUES (";

                    for (i = 0; i <= tabCA_Num.Length - 1; i++)//TESTED
                    {

                        strRequete = tabEC_No[i] + ",";
                        //EC_No
                        strRequete = strRequete + c1 + ",";
                        //N_Analytique
                        strRequete = strRequete + (i + 1) + ",";
                        //EA_Ligne
                        strRequete = strRequete + "'" + tabCA_Num[i] + "',";
                        //CA_Num
                        strRequete = strRequete + tabMontant[i] + ",";
                        //EA_Montant
                        strRequete = strRequete + 0;
                        //EA_Quantite
                        strRequete = strRequete + ")";
                        cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                        cmd.ExecuteNonQuery();
                        General.Execute("UPDATE FactFournDetail SET AnalytiqueActivite='1' WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND EC_NoCptCharge='" + tabEC_No[i] + "'");

                    }
                }
                ModAdo.fc_CloseRecordset(rsCpta);
                //   3 - Table F_RegTaxe
                //       3-1 - Une ligne par Compte de taxe (montant de la TVA)

                strRequete = "SELECT FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux, ";
                strRequete = strRequete + " (SUM(FactFournDetail.MontantLigne)*(TaxeTA_Taux/100)) AS Montant, ";
                strRequete = strRequete + " FactFournEntete.EC_NoFacture AS EC_No";
                strRequete = strRequete + " FROM FactFournDetail ";
                strRequete = strRequete + " INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=";
                strRequete = strRequete + " FactFournEntete.NoAutoFacture ";
                strRequete = strRequete + " WHERE FactFournDetail.NoAutoFacture=" + strNoAutoFacture + " AND ";
                strRequete = strRequete + " (FactFournDetail.RegistreTaxe IS NULL) AND QteLivre>0 ";
                strRequete = strRequete + " GROUP BY FactFournEntete.EC_NoFacture, ";
                strRequete = strRequete + " FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux ";
                strRequete = strRequete + " HAVING (NOT (SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100)) IS NULL) AND (SUM(FactFournDetail.MontantLigne)*(FactFournDetail.TaxeTA_Taux/100))<>0 ";
                strRequete = strRequete + " ORDER BY FactFournEntete.EC_NoFacture, ";
                strRequete = strRequete + " FactFournDetail.TaxeCG_NUM, FactFournDetail.TaxeTA_Taux ";
                rsCpta = ModAdo.fc_OpenRecordSet(strRequete);
                if (rsCpta.Rows.Count > 0)//tested
                {
                    Ecrit = true;
                    i = 0;
                    tabCptTVA = new string[1];
                    tabMontant = new string[1];
                    tabEC_No = new string[1];
                    tabTauxBase = new string[1];
                    foreach (DataRow dr in rsCpta.Rows)//tested
                    {
                        if (i > 0)
                        {
                            Array.Resize(ref tabCptTVA, i + 1);
                            Array.Resize(ref tabMontant, i + 1);
                            Array.Resize(ref tabEC_No, i + 1);
                            Array.Resize(ref tabTauxBase, i + 1);
                        }
                        tabCptTVA[i] = dr["TaxeCG_Num"] + "";
                        tabMontant[i] = dr["Montant"] + "";
                        tabMontant[i] = Convert.ToString(FncArrondirFact(tabMontant[i]));
                        tabEC_No[i] = dr["EC_No"] + "";
                        tabTauxBase[i] = dr["TaxeTA_Taux"] + "";
                        i = i + 1;
                    }
                    strReqIns = "INSERT INTO F_REGTAXE (";
                    strReqIns = strReqIns + "EC_No,RT_No,RT_Type, RT_DateReg,  RT_DatePiece,";
                    strReqIns = strReqIns + "CT_Num,";
                    for (i = 0; i <= tabCptTVA.Length - 1; i++)//tested
                    {
                        strReqIns = strReqIns + "TA_Provenance0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "CG_Num0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_TTaux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Taux0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "RT_Base0" + (i + 1) + ",";
                        strReqIns = strReqIns + "RT_Montant0" + (i + 1) + ", ";
                        strReqIns = strReqIns + "TA_Code0" + (i + 1) + ",";
                        if (i == 4)
                            break; // TODO: might not be correct. Was : Exit For
                                   //Pas plus de 5 boucles :
                                   //limité par SAGE
                    }

                    strReqIns = strReqIns + "JM_Date ";
                    strReqIns = strReqIns + ") VALUES ( ";
                    i = 0;
                    strRequete = tabEC_No[i] + ",";//EC_No

                    MaxRT_No = Convert.ToInt32(General.nz(ModAdo.fc_ADOlibelle("SELECT MAX(RT_No) AS Maximum FROM F_REGTAXE", false, SAGE.adoSage), "0"));
                    MaxRT_No = MaxRT_No + 1;
                    strRequete = strRequete + MaxRT_No + ",";//RT_No

                    strRequete = strRequete + "0,";//RT_Type

                    strRequete = strRequete + "'" + DateTime.Today + "',";//RT_DateReg

                    strRequete = strRequete + "'" + txtDateFacture.Text + "',";//RT_DatePiece

                    strRequete = strRequete + "'" + General.Left(txtCompteFournisseur.Text, 17) + "',";//CT_Num


                    for (i = 0; i <= tabCptTVA.Length - 1; i++)//tested
                    {
                        strRequete = strRequete + "0,";
                        //TA_Provenance0i
                        strRequete = strRequete + "'" + General.Left(tabCptTVA[i], 13) + "',";
                        //CG_Num0i
                        strRequete = strRequete + "0,";
                        //TA_TTaux0i
                        strRequete = strRequete + General.nz(tabTauxBase[i], "0") + ",";
                        //TA_Taux0i
                        strRequete = strRequete + "0,";
                        //RT_Base0i
                        strRequete = strRequete + General.nz(tabMontant[i], "0") + ",";
                        //RT_Montant0i
                        strRequete = strRequete + "'1',";
                        //TA_Code0i
                        if (i == 4)
                            break; // TODO: might not be correct. Was : Exit For
                                   //Pas plus de 5 boucles :
                                   //limité par SAGE
                    }

                    strRequete = strRequete + "'" + strJM_Date + "' ";
                    strRequete = strRequete + ")";
                    cmd = new SqlCommand(strReqIns + strRequete, SAGE.adoSage);
                    cmd.ExecuteNonQuery();
                    General.Execute("UPDATE FactFournDetail SET RegistreTaxe='1' WHERE " + "NoAutoFacture=" + txtNoAutoFacture.Text + "");
                }
                ModAdo.fc_CloseRecordset(rsCpta);
                if (Ecrit == true)//TESTED
                {
                    cmd = new SqlCommand("COMMIT TRANSACTION Trans1" + General.Replace(General.gsUtilisateur, " ", ""), SAGE.adoSage);
                    cmd.ExecuteNonQuery();
                    General.Execute("UPDATE FactFournEntete SET Verrou='1' WHERE NoAutoFacture=" + txtNoAutoFacture.Text);
                    lblNoComptable.Visible = true;
                    txtNoComptable.Visible = true;
                    fc_Verrouillage();
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Facture intégrée.\r\n N° comptable : " + NoFactureSage, "Intégration de la facture en comptabilité", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisie de facture valide (quantités facturées à 0, pas de comptes de charges ...).\n\r Intégration annulée.", "Intégration de la facture en compta", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chkIntegration.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    cmd = new SqlCommand("ROLLBACK TRANSACTION Trans1" + General.Replace(General.gsUtilisateur, " ", ""), SAGE.adoSage);
                    cmd.ExecuteNonQuery();
                    General.Execute("UPDATE    FactFournDetail SET EC_NoCptCharge = NULL, EC_NoCptTva = NULL, AnalytiqueAffaire = NULL, AnalytiqueImm = NULL, RegistreTaxe = NULL, AnalytiqueActivite = NULL WHERE (NoAutoFacture=" + txtNoAutoFacture.Text + ")");
                    General.Execute("UPDATE FactFournEntete SET EC_NoFacture=NULL WHERE NoAutoFacture=" + txtNoAutoFacture.Text + "");
                    //    adocnn.Execute "ROLLBACK TRANSACTION Trans2" & gsUtilisateur
                }
                SAGE.fc_CloseConnSage();
                if ((rsCpta != null))
                {
                    ModAdo.fc_CloseRecordset(rsCpta);
                    rsCpta = null;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                if (intDeroule == 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le journal d'achat est ouvert sur un autre poste.\r\n Son accés étant impossible, l'intégration de la facture est impossible.", "Intégration annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Erreurs.gFr_debug(ex, this.Name + " fc_IntegrerFacture ", true);
                }
                else
                {
                    if (blnTrans == true)
                    {
                        var cmd = new SqlCommand("ROLLBACK TRANSACTION Trans1" + General.Replace(General.gsUtilisateur, " ", ""), SAGE.adoSage);
                        cmd.ExecuteNonQuery();
                    }
                    General.Execute("UPDATE    FactFournDetail SET EC_NoCptCharge = NULL, EC_NoCptTva = NULL, AnalytiqueAffaire = NULL, AnalytiqueImm = NULL, RegistreTaxe = NULL, AnalytiqueActivite = NULL WHERE (NoAutoFacture=" + txtNoAutoFacture.Text + ")");
                    General.Execute("UPDATE FactFournEntete SET EC_NoFacture=NULL WHERE NoAutoFacture=" + txtNoAutoFacture.Text + "");
                    //        Debug.Print err.Description
                    Erreurs.gFr_debug(ex, this.Name + " Erreur IntegrerFacture ");
                }
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public object fc_SavePied()
        {
            object functionReturnValue = null;

            string sqlPied = null;
            int i = 0;
            string TotalAchats = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sqlPied = "DELETE FROM FactFournPied WHERE FactFournPied.NoAutoFacture=" + txtNoAutoFacture.Text + " ";
                General.Execute(sqlPied);
                i = 0;
                if (GridFactFournPied.Rows.Count > 0)
                {
                    //while (!(i == GridFactFournPied.Rows))
                    ModAdo ModAdo = new ModAdo();
                    foreach (var row in GridFactFournPied.Rows)
                    {
                        i = i + 1;
                        TotalAchats = ModAdo.fc_ADOlibelle("SELECT SUM(MontantLigne) AS Total FROM FactFournDetail WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND QteLivre>0 AND NoBcmd=" + row.Cells["No Bon De Commande"].Value);
                        sqlPied = "INSERT INTO FactFournPied (NoAutoFacture,NoBCmd,CodeImmeuble,Montant) ";
                        sqlPied = sqlPied + " VALUES ( ";
                        sqlPied = sqlPied + txtNoAutoFacture.Text + ",";
                        sqlPied = sqlPied + "'" + row.Cells["No Bon De Commande"].Text + "',";
                        sqlPied = sqlPied + "'" + row.Cells["Code Immeuble"].Text + "',";
                        sqlPied = sqlPied + "'" + General.nz(TotalAchats, "0") + "'";
                        sqlPied = sqlPied + ")";
                        General.Execute(sqlPied);
                        fnc_CreateCptAnalytiqueAff(row.Cells["No Bon De Commande"].Text + "", row.Cells["Code Immeuble"].Text + "");
                    }
                    GridFactFournPied.UpdateData();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Erreur SavePied ");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_ModifBCMD()
        {
            object functionReturnValue = null;
            int i = 0;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                i = 0;
                if (GridFactFournPied.Rows.Count > 0)
                {
                    int q = 0;
                    foreach (var row in GridFactFournPied.Rows)
                    {
                        i = i + 1;
                        fnc_ModifCodeEtatBcmd(row.Cells["No Bon de Commande"].Value + "");
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Erreur SavePied ");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoBcmd"></param>
        private void fnc_ModifCodeEtatBcmd(string NoBcmd)
        {
            string sqlBcmd = null;
            string sqlPied = null;
            double QteLivree = 0;
            double QteCommandee = 0;
            DataTable rsModif = default(DataTable);
            int nbRecAff = 0;
            bool qteEgal = false;

            rsModif = new DataTable();

            //    sqlPied = "SELECT SUM(FactFournDetail.QteCommande) AS QuantiteCommande FROM FactFournDetail WHERE NoBCmd=" & NoBcmd & " AND NoAutoFacture=" & txtNoAutoFacture.Text
            sqlPied = "SELECT SUM(FactFournDetail.QteLivre) AS QteLivre ,BCD_Detail.BCD_Quantite, FactFournDetail.DesignationLigneCmd, BCD_Detail.BCD_Designation  FROM FactFournDetail INNER JOIN BCD_Detail ON FactFournDetail.NoBcmd=BCD_Detail.BCD_Cle  WHERE FactFournDetail.NoBCmd=" + NoBcmd;
            sqlPied = sqlPied + " GROUP BY BCD_DETAIL.BCD_Quantite ,FactFournDetail.DesignationLigneCmd, BCD_Detail.BCD_Designation ";
            var ModAdo = new ModAdo();
            rsModif = ModAdo.fc_OpenRecordSet(sqlPied);

            if (rsModif.Rows.Count > 0)//TESTED
            {
                foreach (DataRow dr in rsModif.Rows)
                {
                    if ((dr["DesignationLigneCmd"] + "") == (dr["BCD_Designation"] + ""))
                    {
                        if (Convert.ToDouble(General.nz(dr["BCD_Quantite"], "0")) <= Convert.ToDouble(General.nz(dr["QteLivre"], "0")))
                        {
                            qteEgal = true;
                        }
                        else
                        {
                            qteEgal = false;
                            break; // TODO: might not be correct. Was : Exit Do
                        }
                    }
                }
            }
            else//TESTED
            {
                qteEgal = false;
            }
            rsModif = null;

            if (qteEgal == false)//TESTED
            {
                sqlBcmd = "UPDATE BonDeCommande SET EtatFacturation='FP',CodeE='CE' WHERE NoBonDeCommande=" + NoBcmd;
            }
            else//TESTED
            {
                sqlPied = "";
                sqlPied = "SELECT EtatLivraison FROM BonDeCommande WHERE NoBonDeCommande=" + NoBcmd;
                rsModif = ModAdo.fc_OpenRecordSet(sqlPied);
                if (rsModif.Rows.Count > 0)
                {
                    if ((rsModif.Rows[0]["EtatLivraison"] + "") == "LT")
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatFacturation='FT',CodeE='CS',Verrou=1" + " WHERE NoBonDeCommande=" + NoBcmd;
                    }
                    else
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatFacturation='FT',CodeE='CE',Verrou=1" + " WHERE NoBonDeCommande=" + NoBcmd;
                    }
                }
                ModAdo.fc_CloseRecordset(rsModif);
            }

            //General.adocnn.Execute(sqlBcmd, nbRecAff);
            nbRecAff = General.Execute(sqlBcmd);
            rsModif = null;
            ModAdo.Close();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoBcmd"></param>
        /// <param name="CodeImmeuble"></param>
        /// <returns></returns>
        private object fnc_CreateCptAnalytiqueAff(string NoBcmd, string CodeImmeuble)
        {
            object functionReturnValue = null;

            string NoAffaire = null;
            string strReq = null;
            string strCpteAffaire = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (string.IsNullOrEmpty(NoBcmd))
                {
                    return functionReturnValue;
                }
                ModAdo ModAdo = new ModAdo();
                NoAffaire = ModAdo.fc_ADOlibelle("SELECT NumFicheStandard FROM BonDeCommande WHERE NoBonDeCommande=" + NoBcmd + "");
                SAGE.VerifCreeAnalytique(NoAffaire, NoAffaire, CodeImmeuble);
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Erreur de CreateCptAnalytique ");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFactFournDetail_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            double DblDiff = 0;

            //// ERROR: Not supported in C#: OnErrorStatement

            var _with16 = GridFactFournDetail;
            try
            {
                // Qté.Livrée.
                if (GridFactFournPied.Rows.Count == 0) return;

                if (GridFactFournDetail.ActiveCell.Column.Index == 7)
                {
                    DblDiff = Convert.ToDouble(General.nz(_with16.ActiveRow.Cells["Qté. Facturée"].Text, "0")) - Convert.ToDouble(General.nz(_with16.ActiveRow.Cells["Qté. Facturée"].Value, "0"));
                    if (General.IsNumeric(_with16.ActiveRow.Cells["Qté. Facturée"].Text))
                    {
                        //GridFactFournDetail.Columns("Reste à livrer").Text = CDbl(nz(GridFactFournDetail.Columns("Reste à livrer").Text, "0")) - CDbl(nz(GridFactFournDetail.Columns("Qté. Livrée").Text, "0"))
                        _with16.ActiveRow.Cells["Reste à livrer"].Value = Convert.ToString(Convert.ToDouble(General.nz((_with16.ActiveRow.Cells["Reste à livrer"].Text), "0")) - DblDiff);
                    }
                    else
                    {
                        _with16.ActiveRow.Cells["Qté. Facturée"].Value = "0";
                    }
                    // Qté. Facturée.
                }
                else if (GridFactFournDetail.ActiveCell.Column.Index == 7)
                {
                    if (!General.IsNumeric(_with16.ActiveRow.Cells["Prix"].Text + ""))
                    {
                        _with16.ActiveRow.Cells["Prix"].Value = "0.00";
                    }
                    // reste à livrer.
                }
                else if (GridFactFournDetail.ActiveCell.Column.Index == 9)
                {
                    ModAdo ModAdo = new ModAdo();
                    _with16.ActiveRow.Cells["Compte TVA"].Value = ModAdo.fc_ADOlibelle("SELECT TaxeCG_Num " + " FROM CpteChargeFournisseur " + " WHERE cleAutoFournisseur=" + txtCodeFournisseur.Text + " AND CG_NUM='" + _with16.ActiveRow.Cells["Compte de Charge"].Text + "'");
                }
                //== controle si le numéro de bon de commande est present dans la grille
                //== GridFactFournPied.



                if (GridFactFournDetail.ActiveRow.Cells["NoBCmd"].Column.Index == GridFactFournDetail.ActiveCell.Column.Index)
                {
                    if (!General.IsNumeric(GridFactFournDetail.ActiveRow.Cells["NoBCmd"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce numéro de bon de commande n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }

                    if (fc_CtrBonDeCommande(Convert.ToInt32(General.nz((GridFactFournDetail.ActiveRow.Cells["NoBCmd"].Text), 0))) == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce numéro de bon de commande n'est pas présent ans la sélection.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }

                //== controle si le compte de charge est correct si oui
                //== retourne le compte de classe 4 et le taux de tva correct.
                if (GridFactFournDetail.ActiveCell.Column.Index == GridFactFournDetail.ActiveRow.Cells["Compte de Charge"].Column.Index)
                {
                    if (string.IsNullOrEmpty(GridFactFournDetail.ActiveRow.Cells["Compte de Charge"].Text))
                    {
                        fc_RetTva((GridFactFournDetail.ActiveRow.Cells["Compte de Charge"].Text));
                    }

                    //       If fc_RetCptCharge(.Columns("Compte de Charge").Text) = "" Then
                    //            MsgBox "Ce compte de charge n'existe pas.", _
                    //'                    vbInformation, "Erreur de saisie"
                    //            Cancel = True
                    //            Exit Sub
                    //        Else
                    //            fc_RetTva .Columns("Compte de Charge").Text
                    //        End If
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridFactFournDetail_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sCptCharge"></param>
        private void fc_RetTva(string sCptCharge)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                //MsgBox "Liaison comptabilité inexistante."
                return;
            }
            sSQL = "SELECT DISTINCT F_COMPTEG.CG_NUM , F_TAXE.CG_NUM AS TaxeCG_Num," + " F_TAXE.TA_TAUX AS TA_Taux" + " FROM F_COMPTEG" + " INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO)" + " ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" + " WHERE F_COMPTEG.CG_NUM = '" + sCptCharge + "'";

            rs = new DataTable();
            SAGE.fc_OpenConnSage();
            ModAdo ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(sSQL, null, "", SAGE.adoSage);
            if (rs.Rows.Count > 0)
            {
                GridFactFournDetail.ActiveRow.Cells["Compte TVA"].Value = General.nz(rs.Rows[0]["TaxeCG_Num"], "");
                GridFactFournDetail.ActiveRow.Cells["Ta_Taux"].Value = General.nz(rs.Rows[0]["TA_Taux"], "");
            }
            ModAdo.fc_CloseRecordset(rs);
            rs = null;
        }

        //Unused Fucntion
        private string fc_RetCptCharge(string sCpt)
        {
            string functionReturnValue = null;
            int i = 0;
            var _with18 = cmbCptChargesBis;
            functionReturnValue = "";
            for (i = 0; i < _with18.Rows.Count; i++)
            {
                if (General.UCase(_with18.Rows[i].Cells[0].Text) == General.UCase(sCpt))
                {
                    functionReturnValue = _with18.Rows[i].Cells[0].Text;
                    break; // TODO: might not be correct. Was : Exit For
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="lNoBCD"></param>
        /// <returns></returns>
        private int fc_CtrBonDeCommande(int lNoBCD)
        {
            int functionReturnValue = 0;
            //== cette procedure compare le n° de bon de commande envoyé
            //== en parametre avec les n° de bon de commande presents dans
            //== GridFactFournPied.

            int i = 0;

            var _with19 = GridFactFournPied;
            functionReturnValue = 0;
            for (i = 0; i < _with19.Rows.Count; i++)
            {
                if (General.IsNumeric(_with19.Rows[i].Cells["No Bon de Commande"].Text))
                {
                    if (lNoBCD == Convert.ToInt32(General.nz((_with19.Rows[i].Cells["No Bon de Commande"].Text), 0)))
                    {
                        functionReturnValue = Convert.ToInt32(_with19.Rows[i].Cells["No Bon de Commande"].Text);
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFactFournDetail_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)//TESTED
        {
            int i = 0;
            var _with20 = GridFactFournDetail;

            //======= controle si il existe un n° de bon de commande
            if (string.IsNullOrEmpty(e.Row.Cells["NoBCmd"].Text))
            {
                //== si aucun bon trouvé on retourne un numéro de bon dans la mesure
                //== du possible.
                i = fc_RetNoBon();
                if (i > 0)
                {
                    e.Row.Cells["NoBCmd"].Value = i;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un n° de bon de commande.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }
            if (string.IsNullOrEmpty(e.Row.Cells["Compte de Charge"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un compte de charge.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private int fc_RetNoBon()
        {
            int functionReturnValue = 0;
            //== cette procedure retourne un numéro de bon de commande si il n'y a qu'un
            //== seul bon de commande dans la grille GridFactFournPied sinon
            //== celle ci retourne 0

            int i = 0;
            var _with21 = GridFactFournPied;

            i = _with21.Rows.Count;
            if (i == 1)
            {
                functionReturnValue = Convert.ToInt32(General.nz((_with21.ActiveRow.GetCellValue("No Bon de Commande")), 0));
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFactFournPied_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridFactFournPied.ActiveCell.Column.Index == 3)
            {
                if (!General.IsNumeric(GridFactFournPied.ActiveRow.Cells["Montant des achats"].Text) || string.IsNullOrEmpty(GridFactFournPied.ActiveRow.Cells["Montant des achats"].Text))
                {
                    GridFactFournPied.ActiveRow.Cells["Montant des achats"].Value = "0";
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateFacture_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (General.IsDate(txtDateFacture.Text))
            {
                txtDateFacture.Text = string.Format(txtDateFacture.Text, General.FormatDateSansHeureSQL);
                fc_CalculEcheance(Convert.ToDateTime(txtDateFacture.Text), Convert.ToInt32(General.nz((txtNbreJour.Text), "0")), (txtTypeEcheance.Text), Convert.ToInt32(General.nz((txtEcheance.Text), "1")));
            }
            e.Cancel = Cancel;
        }


        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="DateFacture"></param>
        /// <param name="nbrJours"></param>
        /// <param name="strTypeEcheance"></param>
        /// <param name="Echeance"></param>
        private void fc_CalculEcheance(System.DateTime DateFacture, int nbrJours, string strTypeEcheance, int Echeance)
        {
            System.DateTime NouvDate = default(System.DateTime);

            switch (strTypeEcheance)
            {
                //paiement 45 jours
                case "N":
                    //paiement en nombre de jours nets
                    if (DateFacture.Day == 1)
                    {
                        txtDateEcheance.Text = Convert.ToString(DateFacture.AddDays(nbrJours));
                    }
                    else
                    {
                        NouvDate = Convert.ToDateTime("01/" + DateFacture.AddMonths(1).Month + "/" + DateFacture.AddMonths(1).Year);
                        txtDateEcheance.Text = Convert.ToString(NouvDate.AddDays(nbrJours));
                    }
                    break;

                //paiement 45 jours le 15
                case "L":
                    //paiement en nombre de jours selon l'échéance
                    if (DateFacture.Day == 1)
                    {
                        NouvDate = DateFacture.AddDays(nbrJours);
                        if (NouvDate.Day <= Echeance)
                        {
                            txtDateEcheance.Text = Echeance + "/" + NouvDate.Month + "/" + NouvDate.Year;
                        }
                        else
                        {
                            txtDateEcheance.Text = Echeance + "/" + NouvDate.AddMonths(1).Month + "/" + NouvDate.AddMonths(1).Year;
                        }
                    }
                    else
                    {
                        NouvDate = Convert.ToDateTime("01/" + DateFacture.AddMonths(1).Month + "/" + DateFacture.AddMonths(1).Year);
                        NouvDate = NouvDate.AddDays(nbrJours);
                        if (NouvDate.Day <= Echeance)
                        {
                            txtDateEcheance.Text = Echeance + "/" + NouvDate.Month + "/" + NouvDate.Year;
                        }
                        else
                        {
                            txtDateEcheance.Text = Echeance + "/" + NouvDate.AddMonths(1).Month + "/" + NouvDate.AddMonths(1).Year;
                        }
                    }
                    break;

                //paiement 45 jours fin de mois le 10
                case "F"://TESTED
                    //Fin de mois
                    if (DateFacture.Day == 1)
                    {
                        NouvDate = NouvDate.AddDays(nbrJours);
                    }
                    else
                    {
                        NouvDate = Convert.ToDateTime("01/" + DateFacture.AddMonths(1).Month + "/" + DateFacture.AddMonths(1).Year);
                    }

                    NouvDate = NouvDate.AddDays(nbrJours);
                    NouvDate = Convert.ToDateTime(General.CalculMaxiJourMois(Convert.ToInt16(NouvDate.Month)) + "/" + NouvDate.Month + "/" + NouvDate.Year);

                    if (Echeance != 0)
                    {
                        txtDateEcheance.Text = Echeance + "/" + NouvDate.AddMonths(1).Month + "/" + NouvDate.AddMonths(1).Year;
                    }
                    else
                    {
                        txtDateEcheance.Text = Convert.ToString(NouvDate.AddMonths(1));
                    }
                    break;
            }

            if (General.IsDate(txtDateEcheance.Text))
            {
                txtDateEcheance.Text = String.Format(txtDateEcheance.Text, General.FormatDateSansHeureSQL);
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNoBcmd_KeyPress(object sender, KeyPressEventArgs e)
        {
            int KeyAscii = Convert.ToInt32(e.KeyChar);
            if (KeyAscii == 13)
            {

                if (!string.IsNullOrEmpty(txtNoBcmd.Text) && General.IsNumeric(txtNoBcmd.Text))
                {

                    if (fc_VerifAddBcmd((txtNoBcmd.Text)) == false)
                    {
                        txtNoBcmd.Text = "";
                        return;
                    }
                    ChargeDetail((txtNoBcmd.Text));
                    txtNoBcmd.Text = "";
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(ModAdo.fc_ADOlibelle("SELECT FactFournDetail.NoAutoFacture FROM FactFournDetail INNER JOIN FactFournEntete ON FactFournDetail.NoAutoFacture=FactFournEntete.NoAutoFacture WHERE FactFournDetail.NoAutoFacture=" + txtNoAutoFacture.Text + " AND (NOT FactFournDetail.EC_NoCptCharge IS NULL OR NOT FactFournDetail.EC_NoCptTva IS NULL OR NOT FactFournEntete.EC_NoFacture IS NULL)")))
                    {
                        fc_SaveEntete();
                        fc_SaveDetail();
                        fc_SavePied();
                        ChargeFactureDetailPied();
                        fc_ModifBCMD();
                    }

                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="NoBcmd"></param>
        /// <returns></returns>
        private bool fc_VerifAddBcmd(string NoBcmd)
        {
            bool functionReturnValue = false;
            string strRequete = null;
            DataTable rsAddBcmd = default(DataTable);
            string strCdFourn = null;
            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez d'abord cliquer sur le bouton ajouter pour créer une nouvelle facture fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return functionReturnValue;
            }
            var ModAdo = new ModAdo();
            strRequete = "SELECT DISTINCT BonDeCommande.CodeE,BonDeCommande.EtatFacturation,CodeFourn FROM BonDeCommande ";
            strRequete = strRequete + " LEFT JOIN FactFournPied ON BonDeCommande.NoBonDeCommande=FactFournPied.NoBCmd ";
            strRequete = strRequete + " WHERE (NoBonDeCommande=" + NoBcmd + ") AND (FactFournPied.NoAutoFacture<>" + txtNoAutoFacture.Text + " OR FactFournPied.NoAutoFacture IS NULL)";

            rsAddBcmd = new DataTable();
            rsAddBcmd = ModAdo.fc_OpenRecordSet(strRequete);

            if (rsAddBcmd.Rows.Count > 0)
            {
                strCdFourn = ModAdo.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE cleAuto=" + General.nz(rsAddBcmd.Rows[0]["CodeFourn"], "0"));
                if (string.IsNullOrEmpty(txtCodeFourn.Text))
                {
                    fc_ChargeFournisseur(strCdFourn);
                }
                else if (txtCodeFourn.Text != strCdFourn)//TESTED
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter n'appartient pas au fournisseur que vous avez déjà saisi (" + txtRaisonFournisseur.Text + ")\r\n Ce bon de commande ne peut donc pas être ajouté", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }


                if ((rsAddBcmd.Rows[0]["CodeE"] + "") == "CS")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est soldé.\r\n Ce bon ne peut donc pas être ajouté.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                if ((rsAddBcmd.Rows[0]["CodeE"] + "") == "CA")
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est en cours de saisie.\r\n Souhaitez-vous l'ajouter quand même ?", "Ajout d'un bon de commande", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                }

                if ((rsAddBcmd.Rows[0]["EtatFacturation"] + "") == "FT")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est facturé en totalité.\r\n Ce bon ne peut donc pas être ajouté.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                functionReturnValue = true;
            }
            else//TESTED
            {
                if (ModAdo.fc_ADOlibelle("SELECT FactFournPied.NoBcmd FROM FactFournPied WHERE NoAutoFacture=" + txtNoAutoFacture.Text + " AND NoBCmd=" + NoBcmd + "") != NoBcmd)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce bon de comande n'existe pas.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce bon de commande est déjà inséré dans votre facture.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="n"></param>
        /// <param name="p"></param>
        /// <returns></returns>
        public double FncArrondirFact(object n, int p = 2)
        {
            return Math.Round(Convert.ToDouble(n), p);
            string stemp = null;
            string SA = null;
            string str_Renamed = null;
            string sSep = null;
            string sResult = null;
            string sZero = null;
            object vN = null;
            object sSplit = null;
            int k = 0;
            bool fDecPart = false;
            fDecPart = true;
            vN = n;
            //sSep = General.Mid(Convert.ToString(1 / 10), 2, 1);
            sSep = ".";
            sZero = "";
            n = Convert.ToDouble(n);
            str_Renamed = Convert.ToString(n);
            if (str_Renamed.Contains(sSep))//tested //Strings.InStr(str_Renamed, sSep) > 0 ===> 
            {
                sSplit = General.Split(str_Renamed, sSep)[1];
                if (sSplit.ToString().Length > p)//tested
                {
                    sSplit = General.Left(sSplit + "", p + 1);
                    if (Convert.ToInt32(General.Right(sSplit + "", 1)) >= 5)
                    {
                        SA = Convert.ToString(Convert.ToInt32(General.Mid(sSplit + "", p, 1)) + 1);
                        stemp = General.Left(sSplit + "", p - 1);
                        while ((General.Mid(sSplit + "", p, 1) == "9"))
                        {
                            if (p > 1)
                            {
                                if (p == 2)//TESTED
                                {
                                    stemp = "";
                                }
                                else//TESTED
                                {
                                    stemp = General.Left(sSplit + "", p - 2);
                                }
                                SA = Convert.ToString(Convert.ToInt32(General.Mid(sSplit + "", p - 1, 1)) + 1);
                                sZero = sZero + "0";
                            }
                            else//TESTED
                            {
                                fDecPart = false;
                                break;
                            }
                            p = p - 1;
                        }
                        if (fDecPart)//tested
                        {
                            sResult = Convert.ToString(Convert.ToDouble(General.Split(str_Renamed, sSep)[0] + sSep + stemp + SA + sZero));
                        }
                        else//TESTED
                        {
                            sSplit = General.Split(str_Renamed, sSep)[0];
                            p = sSplit.ToString().Length;
                            SA = Convert.ToString(Convert.ToInt32(General.Mid(sSplit + "", p, 1)) + 1);
                            stemp = General.Left(sSplit + "", p - 1);
                            sZero = "";
                            while ((General.Mid(sSplit + "", p, 1) == "9") && p > 1)
                            {
                                if (p > 2)//TESTED
                                {
                                    stemp = General.Left(sSplit + "", p - 2);
                                }
                                else
                                {
                                    stemp = "";
                                }
                                SA = Convert.ToString(Convert.ToInt32(General.Mid(sSplit + "", p - 1, 1)) + 1);
                                sZero = sZero + "0";
                                p = p - 1;
                            }
                            sResult = Convert.ToString(Convert.ToDouble(stemp + SA + sZero));
                        }
                    }
                    else//tested
                    {
                        sResult = Convert.ToString(Convert.ToDouble(General.Split(str_Renamed, sSep)[0] + sSep + General.Left(sSplit + "", p)));
                    }
                }
                else//tested
                {
                    sResult = vN + "";
                }
            }
            else//tested
            {
                sResult = vN + "";
            }
            return Convert.ToDouble(sResult);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocFactFourn_VisibleChanged(object sender, EventArgs e)//TESTED
        {
            if (!Visible)
                return;

            General.gsUtilisateur = General.fncUserName();
            General.open_conn();

            if (ModParametre.fc_RecupParam(this.Name))//tested
            {
                ChargeFactureEntete(ModParametre.sNewVar);
                fc_LoadCpteCharges();
                fc_CalTVA();
            }
            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else//TESTED
            {
                SAGE.fc_OpenConnSage();
            }
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocFactFourn");
            //lblNavigation[0].Text = General.sNomLien0;
            //lblNavigation[1].Text = General.sNomLien1;
            //lblNavigation[2].Text = General.sNomLien2;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadCpteCharges()
        {
            DataTable adotemp = default(DataTable);
            string sSQL = null;
            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                //MsgBox "Liaison comptabilité inexistante."
                return;
            }
            SAGE.fc_OpenConnSage();
            adotemp = new DataTable();
            sSQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable]," + " F_COMPTEG.CG_INTITULE AS [Intitulé]," + " F_TAXE.CG_NUM, F_TAXE.TA_TAUX " + " FROM F_COMPTEG INNER JOIN " + " (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) " + " ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" + " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)>=3 ) " + "  ORDER BY F_COMPTEG.CG_NUM";
            var ModAdo = new ModAdo();
            adotemp = ModAdo.fc_OpenRecordSet(sSQL, null, "", SAGE.adoSage);
            DataTable dt = new DataTable();
            dt.Columns.Add("Compte De Charge");
            dt.Columns.Add("Intitulé");
            dt.Columns.Add("Compte de Taxe");
            dt.Columns.Add("Taux de TVA");
            if (adotemp.Rows.Count > 0)
            {
                cmbCptChargesBis.DataSource = null;

                foreach (DataRow dr in adotemp.Rows)
                {
                    var dr1 = dt.NewRow();
                    dr1["Compte De Charge"] = dr["Code comptable"];
                    dr1["Intitulé"] = dr["Intitulé"];
                    dr1["Compte de Taxe"] = dr["CG_NUM"];
                    dr1["Taux de TVA"] = dr["TA_Taux"];
                    dt.Rows.Add(dr1);

                }
            }
            cmbCptChargesBis.DataSource = dt;
            cmbCptChargesBis.ValueMember = "Compte De Charge";
            //cmbCptChargesBis.DataFieldList = "Column 0";
            //GridFactFournDetail.Columns["Compte de Charge"].DropDownHwnd = cmbCptChargesBis.hWnd;
            adotemp = null;
            SAGE.fc_CloseConnSage();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        public void fc_DeVerrouillage()
        {
            int i = 0;

            cmdSupprimer.Enabled = true;
            CmdSauver.Enabled = true;
            chkIntegration.CheckState = CheckState.Unchecked;
            chkIntegration.Enabled = true;
            cmdRechercheBCMD.Enabled = true;
            cmdEnleverUnBcmd.Enabled = true;
            cmdAvoir.Enabled = true;
            txtDateEcheance.ReadOnly = false;
            txtNoBcmd.ReadOnly = false;
            txtDateFacture.ReadOnly = false;
            txtMontantFacture.ReadOnly = false;
            txtRaisonFournisseur.ReadOnly = false;
            TxtNoFacture.ReadOnly = false;
            foreach (var oCol_loopVariable in GridFactFournDetail.DisplayLayout.Bands[0].Columns)
            {
                oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                oCol_loopVariable.CellAppearance.ForeColor = Color.Black;
            }
            GridFactFournDetail.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            GridFactFournDetail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;

            foreach (var oCol_loopVariable in GridFactFournPied.DisplayLayout.Bands[0].Columns)
            {
                oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                oCol_loopVariable.CellAppearance.ForeColor = Color.Black;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        public void fc_Verrouillage()
        {
            int i = 0;
            cmdSupprimer.Enabled = false;
            CmdSauver.Enabled = false;
            chkIntegration.CheckState = CheckState.Unchecked;
            chkIntegration.Enabled = false;
            cmdRechercheBCMD.Enabled = false;
            cmdEnleverUnBcmd.Enabled = false;
            cmdAvoir.Enabled = false;
            txtDateEcheance.ReadOnly = true;
            txtNoBcmd.ReadOnly = true;
            txtDateFacture.ReadOnly = true;
            txtMontantFacture.ReadOnly = true;
            txtRaisonFournisseur.ReadOnly = true;
            TxtNoFacture.ReadOnly = true;

            foreach (var oCol_loopVariable in GridFactFournDetail.DisplayLayout.Bands[0].Columns)
            {
                oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                oCol_loopVariable.CellAppearance.ForeColor = Color.Blue;
                if (oCol_loopVariable.Key.ToLower() == "Compte De Charge".ToLower())
                    oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            }
            foreach (var oCol_loopVariable in GridFactFournPied.DisplayLayout.Bands[0].Columns)
            {
                oCol_loopVariable.CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                oCol_loopVariable.CellAppearance.ForeColor = Color.Blue;
            }
            GridFactFournDetail.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            GridFactFournDetail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblGoFournisseur_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.saveInReg("UserBCmdHead", "NoFNS", txtCodeFourn.Text);
            ModParametre.fc_SaveParamPosition(this.Name, Variable.Cuserdocfourn, "");
            View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
        }

        private void Cmd17_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFactFournDetail_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridFactFournDetail.DisplayLayout.Bands[0].Columns["NoLigne"].Hidden = true;
            GridFactFournDetail.DisplayLayout.Bands[0].Columns["NoBcmd"].Header.Caption = "No De Bon Commande";
            GridFactFournDetail.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            GridFactFournDetail.DisplayLayout.Bands[0].Columns["Prix unitaire"].Hidden = true;
            GridFactFournDetail.DisplayLayout.Bands[0].Columns["Compte De Charge"].ValueList = cmbCptChargesBis;
        }

        private void cmbCptCharges_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //cmbCptCharges.DisplayLayout.Bands[0].Columns["CG_NUM"].Hidden = false;
        }

        private void cmbCptChargesBis_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //cmbCptChargesBis.DisplayLayout.Bands[0].Columns["Code comptable"].Header.Caption = "Compte De Charge";
            //cmbCptChargesBis.DisplayLayout.Bands[0].Columns["CG_NUM"].Header.Caption = "Compte de Taxe";
            //cmbCptChargesBis.DisplayLayout.Bands[0].Columns["TA_Taux"].Header.Caption = "Taux de TVA";
        }

        private void GridFactFournPied_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFactFournHtTvaCpta_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssFactFournHtTvaCpta.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            ssFactFournHtTvaCpta.DisplayLayout.Bands[0].Columns["CptTVA"].Header.Caption = "Cpte TVA";
            ssFactFournHtTvaCpta.DisplayLayout.Bands[0].Columns["TVA"].Header.Caption = "Montant TVA";
        }
    }
}
