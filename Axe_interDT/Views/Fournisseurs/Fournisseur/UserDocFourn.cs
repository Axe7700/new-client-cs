﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using iTalk;
using System.Data.SqlClient;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.Fournisseurs.Fournisseur
{
    public partial class UserDocFourn : UserControl
    {
        public UserDocFourn()
        {
            InitializeComponent();
        }
        string[] tabAgence;
        string[] tabAgenceFD;
        double nInt;
        string sNoFNS;
        string[] tabTemp;
        DataTable rsCpteChargesFourn;
        ModAdo rsCpteChargesFournModAdo;
        bool blnModeAjout;

        private void fc_Rechercher()
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "SELECT Code as \"code\", Nom as \"Raison sociale\", Adresse as \"Adresse\", Raisonsocial as \"RaisonSociale\" FROM fournisseurArticle ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  des fournisseurs" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    SelectFns(sCode);
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        SelectFns(sCode);
                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
                return;
            }

        }
        private void SelectFns(string iargfns)
        {
            object sSlqtemp = null;
            //Routine chargé de remplir la grille à partir d'un code client
            string sSQL = null;
            string stemp = null;
            DataTable rs = default(DataTable);
            string sqlFourn = null;

            sSQL = "SELECT fournisseurArticle.cleauto,fournisseurArticle.agree,fournisseurArticle.Code,fournisseurArticle.Nom,"
                + "fournisseurArticle.Adresse,fournisseurArticle.Raisonsocial,"
                + " fournisseurArticle.Ville,fournisseurArticle.Contact,fournisseurArticle.Type,fournisseurArticle.Portable,fournisseurArticle.HDM,"
                + "fournisseurArticle.HDA,fournisseurArticle.HFM,fournisseurArticle.HFA,fournisseurArticle.Codepostal,fournisseurArticle.Condition,"
                + "fournisseurArticle.Commentaire,fournisseurArticle.Telephone,fournisseurArticle.Fax, fournisseurArticle.Email,fournisseurArticle.NePasAfficher,"
                + "fournisseurArticle.CodeFournGecet,fournisseurArticle.SansCodeGecet,fournisseurArticle.Ncompte,"
                + " fournisseurArticle.ModeRgt,FNS_Agences.FNS_No,FNS_Agences.FNS_Contact,FNS_Agences.FNS_Qualite,"
                + " FNS_Agences.FNS_Adresse,FNS_Agences.FNS_Ville,FNS_Agences.FNS_Portable,FNS_Agences.FNS_CodePostal,FNS_Agences.FNS_Tel,"
                + " FNS_Agences.FNS_Fax,FNS_Agences.FNS_Email,FNS_Agences.FNS_cpt,FNS_Agences.FNS_rgt,FNS_Agences.FNS_HDM,FNS_Agences.FNS_HDA,"
                + "FNS_Agences.FNS_HFM,FNS_Agences.FNS_HFA,FNS_Agences.FNS_Agence"
                + " From fournisseurArticle LEFT JOIN FNS_Agences on fournisseurArticle.cleauto=FNS_Agences.FNS_cle"
                + " WHERE fournisseurArticle.Code= '" + StdSQLchaine.gFr_DoublerQuote(iargfns) + "'";

            ModAdo ModAdors = new ModAdo();
            rs = ModAdors.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count == 0)
                return;

            hidCleAuto.Text = rs.Rows[0]["cleauto"].ToString();

            ctr00B.Text = General.nz(rs.Rows[0]["Code"], "").ToString();

            ctr00.Text = General.nz(rs.Rows[0]["Nom"], "").ToString();

            ctr10.Text = General.nz(rs.Rows[0]["Raisonsocial"], "").ToString();

            ctr20.Text = General.nz(rs.Rows[0]["Adresse"], "").ToString();

            ctr30.Text = General.nz(rs.Rows[0]["Ville"], "").ToString();

            ctr40.Text = General.nz(rs.Rows[0]["Codepostal"], "").ToString();

            ctr50.Text = General.nz(rs.Rows[0]["ModeRgt"], "").ToString();

            ctr60.Text = General.nz(rs.Rows[0]["Condition"], "").ToString();

            ctr01.Text = General.nz(rs.Rows[0]["Telephone"], "").ToString();

            ctrB22.Text = General.nz(rs.Rows[0]["Portable"], "").ToString();

            ctrB32.Text = General.nz(rs.Rows[0]["Contact"], "").ToString();

            ctr11.Text = General.nz(rs.Rows[0]["Fax"], "").ToString();

            ctr21.Text = General.nz(rs.Rows[0]["Email"], "").ToString();

            ctr31.Text = General.nz(rs.Rows[0]["Commentaire"], "").ToString();

            if (rs.Rows[0]["agree"].ToString() == "1")
            {
                CtrB41.CheckState = System.Windows.Forms.CheckState.Checked;
            }
            else if (rs.Rows[0]["agree"].ToString() == "0")
            {
                CtrB41.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }

            txtNCompte.Text = General.nz(rs.Rows[0]["Ncompte"], "").ToString();

            txtHDM.Text = General.nz(rs.Rows[0]["HDM"], "").ToString();

            txtHDA.Text = General.nz(rs.Rows[0]["HDA"], "").ToString();

            txtHFM.Text = General.nz(rs.Rows[0]["HFM"], "").ToString();

            txtHFA.Text = General.nz(rs.Rows[0]["HFA"], "").ToString();

            cmbFourn.Text = General.nz(rs.Rows[0]["CodeFournGecet"], "").ToString();

            chkNePasAfficher.Checked = General.nz(rs.Rows[0]["NePasAfficher"], 0).ToString() == "1";

            chkSansCodeGecet.Checked = General.nz(rs.Rows[0]["SansCodeGecet"], 0).ToString() == "1";

            if (chkSansCodeGecet.CheckState == CheckState.Checked)
            {
                cmbFourn.Enabled = false;
            }
            else
            {
                cmbFourn.Enabled = true;
            }


            //GeneralXav.Sub_Type(CtrB30, tabTemp, General.nz( rs.Rows[0]["Type"),  0));TODO

            sSlqtemp = "";
            foreach (DataRow rsRow in rs.Rows)
            {

                stemp = "!" + General.nz(rsRow["FNS_Agence"], "") + "!;" + "!" + General.nz(rsRow["FNS_Contact"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_Qualite"], "") + "!;" + "!" + General.nz(rsRow["FNS_Adresse"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_Codepostal"], "") + "!;" + "!" + General.nz(rsRow["FNS_Ville"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_Tel"], "") + "!;" + "!" + General.nz(rsRow["FNS_Portable"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_Fax"], "") + "!;" + "!" + General.nz(rsRow["FNS_Email"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_No"], "") + "!;" + "!" + General.nz(rsRow["FNS_Cpt"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_Rgt"], "") + "!;" + "!" + General.nz(rsRow["FNS_HDM"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_HFM"], "") + "!;" + "!" + General.nz(rsRow["FNS_HDA"], "") + "!;"
                    + "!" + General.nz(rsRow["FNS_HFA"], "") + "!;";

                if (stemp != "!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;!!;")

                    Grid0.DataSource = rs;
                // rs.MoveNext();
            }
            //If sTemp <> "!!;!!;!!;!!;!!;!!;!!;!!;!!;" Then
            // FNS.Text = True
            //End If


            if ((rsCpteChargesFourn != null))
            {
                rsCpteChargesFourn.Dispose();

            }
            else
            {
                rsCpteChargesFourn = new DataTable();
            }

            sqlFourn = "SELECT CG_Num,Intitule,TaxeCG_Num,TA_Taux,CleAutoFournisseur FROM CpteChargeFournisseur WHERE CleAutoFournisseur=" + hidCleAuto.Text + "";

            rsCpteChargesFourn = rsCpteChargesFournModAdo.fc_OpenRecordSet(sqlFourn);
            GridCptCharges.DataSource = rsCpteChargesFourn; ;

        }


        private void chkNePasAfficher_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkSansCodeGecet_CheckedChanged(object sender, EventArgs e)
        {

            if (chkSansCodeGecet.CheckState == CheckState.Checked)
            {
                cmbFourn.Text = "";
                cmbFourn.Enabled = false;
            }
            else
            {
                cmbFourn.Enabled = true;
            }
        }

        private void cmbFourn_AfterCloseUp(object sender, EventArgs e)
        {

        }

        private void cmbFourn_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sqlSelect = null;

            if (cmbFourn.Rows.Count == 0)
            {
                sqlSelect = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix  where fournisseur <>'Fourn non connu' ORDER BY Dtiprix.Fournisseur";

                sheridan.InitialiseCombo(cmbFourn, sqlSelect, "Fournisseur", false, ModParametre.adoGecet);
            }
        }

        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            int fok = 0;
            bool bAdd = false;
            bool bUpdate = false;
            bool bDelete = false;

            //=== controle si l'utilisateur a les droits d'ajout.
            ModAutorisation.fc_Droit(Variable.Cuserdocfourn, Variable.Cuserdocfourn, ref bAdd, ref bUpdate, ref bDelete);
            if (bAdd == false)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, vous n'avez pas l'autorisation d'ajouter un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }

            if (CmdSauver.Enabled == true)
            {
                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            }

            if (fok == 6)
            {
                Save();
                clear();

            }
            else if (fok == 2)
            {
                blnModeAjout = false;
                return;
            }
            else
            {
                clear();

            }

            fc_BloqueForm(General.cAjouter);
            blnModeAjout = true;
        }

        private void fc_BloqueForm(string sCommande = "")
        {
            try
            {
                object blnAjout = null;

                var _with2 = this;

                //== bloque systematiquement le boutton supprimer.
                cmdSupprimer.Enabled = false;
                CmdSauver.Enabled = true;
                //=== mode ajout.
                if (General.UCase(sCommande) == General.UCase(General.cAjouter))
                {

                    _with2.CmdRechercher.Enabled = false;
                    _with2.cmdRechercheImmeuble.Enabled = false;
                    _with2.CmdRechercher.Enabled = false;
                    _with2.cmdAnnuler.Enabled = true;
                    _with2.cmdAjouter.Enabled = false;

                    //            .cmdRechercheRaisonSocial.Enabled = False
                    //            .cmdAjouter.Enabled = True
                    //            .CmdRechercher.Enabled = False
                    //            .CmdSauver.Enabled = True
                    //active le mode modeAjout

                    blnAjout = true;

                }
                else
                {
                    _with2.cmdRechercheImmeuble.Enabled = true;
                    _with2.CmdRechercher.Enabled = true;
                    _with2.cmdAnnuler.Enabled = false;
                    _with2.cmdAjouter.Enabled = true;
                    //            .cmdRechercheClient.Enabled = True
                    //            .cmdRechercheImmeuble.Enabled = True
                    //             .cmdRechercheRaisonSocial.Enabled = True
                    //            .CmdRechercher.Enabled = True
                    //            .CmdSauver.Enabled = False
                    //            'desactive le mode modeAjout

                    blnAjout = false;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BloqueForm");
            }

        }
        private void clear()
        {

            foreach (Control C in this.Controls)
            {

                if (C is iTalk_TextBox_Small2 || C is ComboBox)
                {
                    C.Text = "";
                }
            }

            cmbFourn.Text = "";
            hidCleAuto.Text = "";
            chkNePasAfficher.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkSansCodeGecet.CheckState = System.Windows.Forms.CheckState.Unchecked;
            cmbFourn.Enabled = true;
            //txtNCompte.Locked = True
            //Grid0.RemoveAll();
            nInt = 1;
            tabAgence = (string[])Microsoft.VisualBasic.CompilerServices.Utils.CopyArray(tabAgence, new string[17, 1]);
            Array.Resize(ref tabAgenceFD, 1);
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            int fok = 0;
            fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez-vous l'annulation de cette fiche", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fok == 6)
            {
                clear();
            }
        }

        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
        }
        private bool CheckNeedFields()
        {
            bool functionReturnValue = false;
            string strTEMP = null;
            object oTemp = null;
            string sSQL = null;
            strTEMP = "";
            //If ctr00.Text = "" Then
            //    strTEMP = Lbl00.Caption
            //Le nom du directeur n'est pas pour l'instant obligatoire
            //strTEMP = ""
            //== code fournisseur
            if (string.IsNullOrEmpty(ctr00B.Text))
            {
                //strTEMP = Label5.Text;
                //ElseIf ctr20.Text = "" Then '== adresse
                //     strTEMP = Lbl20.Caption
                //ElseIf ctr30.Text = "" Then '== ville
                //    strTEMP = Lbl30.Caption
                //ElseIf ctr40.Text = "" Then '== code postal
                //    strTEMP = Lbl40(0).Caption
                //ElseIf ctr50.Text = "" Then '== mode de reglement
                //    strTEMP = Lbl50.Caption
                //ElseIf ctr01.Text = "" Then '==telephone
                //    strTEMP = Lbl01.Caption
            }


            if (string.IsNullOrEmpty(cmbFourn.Text) && General.nz((chkSansCodeGecet.CheckState), 0).ToString() != "1" && string.IsNullOrEmpty(strTEMP))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code fournisseur GECET correspondant" + " au fournisseur que vous saisissez actuellement, si il s'agit d'un soustraitant " + "ou autre cliquez sur la case à cocher 'Sans correspondance avec Gecet'", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = false;
                cmbFourn.Focus();
                return functionReturnValue;

            }
            else if (General.nz((chkSansCodeGecet.CheckState), 0).ToString() != "1")
            {

                sSQL = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix" + " WHERE Dtiprix.Fournisseur ='" + StdSQLchaine.gFr_DoublerQuote(cmbFourn.Text) + "'" + " ORDER BY Dtiprix.Fournisseur";
                using (var tmpADO = new ModAdo())
                {
                    sSQL = tmpADO.fc_ADOlibelle(sSQL);

                    if (string.IsNullOrEmpty(sSQL))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fournisseur Gecet incorrecte, Veuillez le sélectionner dans la liste.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                        cmbFourn.Focus();
                        return functionReturnValue;
                    }
                }
            }

            if (string.IsNullOrEmpty(strTEMP))
            {
                functionReturnValue = true;
                return functionReturnValue;

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champs " + strTEMP + " n'est pas renseigné.");
                functionReturnValue = false;
            }
            return functionReturnValue;
        }

        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
        }
        private void Save()
        {
            string sCompteTiers = null;
            bool bTemp = false;
            string stemp = null;
            DataTable rs = default(DataTable);
            ModAdo ModAdors = new ModAdo();
            string sSQLTe = null;
            if (chkSansCodeGecet.CheckState == CheckState.Checked)
            {
                cmbFourn.Text = "";
            }
            bTemp = CheckNeedFields();

            Grid0.UpdateData();
            double i = 0;
            if (bTemp)
            {
                //insertion des informations générales sur le fournisseur


                if (string.IsNullOrEmpty(txtNCompte.Text))
                {
                    string sCtr00b = ctr00B.Text;
                    string sCtr10b = ctr10.Text;
                    txtNCompte.Text = SAGE.fc_prcAjoutTiers(2, ref sCtr00b, ref sCtr10b, 
                        ref sCtr10b, (ctr20.Text), "", (ctr40.Text),
                        (ctr30.Text), (ctrB32.Text), (ctr01.Text),
                    (ctr11.Text), 2, (ctr21.Text));
                    sCtr00b = ctr00B.Text= sCtr00b;
                    sCtr10b = ctr10.Text= sCtr10b;
                }
                else
                {
                    //===@@@ modif du 29 05 2017, desactive Sage.
                    if (General.sDesActiveSage == "1")
                    {

                    }
                    else
                    {
                        //== modif du 23 aout controle que le compte present dans txtNCompte existe bien
                        //== sinon demande confirmation de creation du compte.

                        sSQLTe = "SELECT CT_Num From F_COMPTET WHERE CT_Num='" + StdSQLchaine.gFr_DoublerQuote(SAGE.fc_FormatCompte(sCompteTiers, 2)) + "'";
                        SAGE.fc_OpenConnSage();
                        rs = new DataTable();
                        SqlDataAdapter rsSDA = new SqlDataAdapter(sSQLTe, SAGE.adoSage);
                        rsSDA.Fill(rs);

                        if (rs.Rows.Count == 0)
                        {
                            DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le compte " + txtNCompte.Text + " n'existe pas en compta, Voulez-vous le créer?", "Création de compte", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (dg == DialogResult.Yes)
                            {
                                string ncompte = txtNCompte.Text;
                                string sCtr10 = ctr10.Text;
                                txtNCompte.Text = SAGE.fc_prcAjoutTiers(2, ref ncompte, ref sCtr10,ref sCtr10,
                                    (ctr20.Text), "", (ctr40.Text), (ctr30.Text), (ctrB32.Text), (ctr01.Text),
                                (ctr11.Text), 2, (ctr21.Text));
                                txtNCompte.Text = ncompte;
                                ctr10.Text = sCtr10;
                            }
                        }
                        rs.Dispose();

                        rs = null;
                        SAGE.fc_CloseConnSage();
                    }
                }

                /*    if (!string.IsNullOrEmpty(Err().Description))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible de mettre la base sage à jour, vérifier si votre connexion DTODBC existe");
                        return;
                    }
                    if (string.IsNullOrEmpty(hidCleAuto.Text))
                    {
                        stemp = "Select * From Fournisseurarticle Where Fournisseurarticle.CleAuto = 0";                  
                        rs = ModAdors.fc_OpenRecordSet(stemp);
                        rs.Rows.Add();
                    }
                    else
                    {
                        stemp = "Select * From Fournisseurarticle Where Fournisseurarticle.CleAuto = " + Convert.ToInt16(hidCleAuto.Text);
                        rs = ModAdors.fc_OpenRecordSet(stemp);
                    }
                    rs.Rows[0]["Nom"] = ctr00.Text;
                    rs.Rows[0]["Raisonsocial"] = ctr10.Text;
                    rs.Rows[0]["Adresse"] = ctr20.Text;
                    rs.Rows[0]["Ville"] = ctr30.Text;
                    rs.Rows[0]["Codepostal"] = ctr40.Text;
                    rs.Rows[0]["Telephone"] = ctr01.Text;
                    rs.Rows[0]["Portable"] = ctrB22.Text;
                    rs.Rows[0]["Contact"] = ctrB32.Text;
                    rs.Rows[0]["Fax"] = ctr11.Text;
                    rs.Rows[0]["Email"] = ctr21.Text;
                    rs.Rows[0]["Modergt"] = ctr50.Text;
                    rs.Rows[0]["Condition"] = ctr60.Text;
                    rs.Rows[0]["Commentaire"] = ctr31.Text;
                    rs.Rows[0]["HDM"] = txtHDM.Text;
                    rs.Rows[0]["HDA"] = txtHDA.Text;
                    rs.Rows[0]["HFM"] = txtHFM.Text;
                    rs.Rows[0]["HFA"] = txtHFA.Text;
                    rs.Rows[0]["Code"] = ctr00B.Text;
                    rs.Rows[0]["Type"] = Convert.ToInt32(CtrB30.SelectedRow);//verfier
                    rs.Rows[0]["Agree"] = CtrB41.CheckState;
                    rs.Rows[0]["NCompte"] = txtNCompte.Text;
                    rs.Rows[0]["CodeFournGecet"] = General.nz( cmbFourn.Text,  System.DBNull.Value);
                    rs.Rows[0]["NePasAfficher"] = chkNePasAfficher.CheckState;
                    rs.Rows[0]["SansCodeGecet"] = chkSansCodeGecet.CheckState;

                    ModAdors.Update();

                    hidCleAuto.Text = rs.Rows[0]["Cleauto"].ToString();

                    ModAdo.fc_CloseRecordset(rs);

                    //Insertion des lignes agences
                    for (i = 0; i <= tabAgence.Length - 1; i++)
                    {
                        if (Convert.ToInt16(tabAgence[10, i]) < 0)
                        {

                            stemp = "Insert into FNS_Agences(FNS_Agence,FNS_Contact,FNS_Qualite,FNS_Adresse,FNS_Codepostal," 
                                + " FNS_Ville,FNS_tel,FNS_Portable,FNS_Fax,FNS_Email,FNS_cle,FNS_Rgt,FNS_Cpt,FNS_HDM,FNS_HFM,FNS_HDA,FNS_HFA) "
                                + " Values('" + StdSQLchaine.gFr_DoublerQuote(tabAgence[0, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[1, i]) 
                                + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[2, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[3, i]) + "','"
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[4, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[5, i]) + "','" 
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[6, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[7, i]) + "','" 
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[8, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[9, i]) + "'," 
                                + Convert.ToInt16(hidCleAuto.Text) + ",'" + StdSQLchaine.gFr_DoublerQuote(tabAgence[12, i]) + "','" 
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[11, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[13, i]) + "','"
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[14, i]) + "','" + StdSQLchaine.gFr_DoublerQuote(tabAgence[15, i]) + "','"
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[16, i]) + "')";
                        }
                        else
                        {

                            stemp = "Update FNS_Agences set FNS_Agence = ' " + StdSQLchaine.gFr_DoublerQuote(tabAgence[0, i]) + "', FNS_Contact= '" 
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[1, i]) + "',FNS_Qualite='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[2, i]) 
                                + "',FNS_Adresse='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[3, i]) + "',FNS_Codepostal='" 
                                + StdSQLchaine.gFr_DoublerQuote(tabAgence[4, i]) + "',FNS_Ville='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[5, i]) 
                                + "',FNS_tel='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[6, i]) + "',FNS_Portable='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[7, i])
                                + "',FNS_Fax='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[8, i]) + "',FNS_Email='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[9, i]) 
                                + "',FNS_Rgt='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[12, i]) + "',FNS_Cpt='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[11, i]) 
                                + "',FNS_HDM='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[13, i]) + "',FNS_HFM='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[14, i]) 
                                + "',FNS_HDA='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[15, i]) + "',FNS_HFA='" + StdSQLchaine.gFr_DoublerQuote(tabAgence[16, i]) 
                                + "' where FNS_No =" + Convert.ToInt16(tabAgence[10, i]);
                        }

                        General.Execute(stemp);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(Err().Description);
                    }
                    for (i = 0; i <= Information.UBound(tabAgenceFD) - 1; i++)
                    {
                        General.Execute("Delete From FNS_Agences where FNS_No=" + Convert.ToInt16(tabAgenceFD[i]));
                        rs = General.Execute("Select * From BondeCommande Inner join FNS_Agences on BondeCommande.ContactDivers=FNS_Agences.FNS_Contact where BondeCommande.CodeAg=" 
                            + Convert.ToInt16(tabAgenceFD[i]));

                        if (rs.Rows.Count >0)
                        {
                            stemp = "Update BondeCommande set BondeCommande.CodeAg=0,bondeCommande.ContactDivers='' where BondeCommande.CodeAg=" 
                                + Convert.ToInt16(tabAgenceFD[i]);
                        }
                        else
                        {
                            stemp = "Update BondeCommande set BondeCommande.CodeAg=0 where BondeCommande.CodeAg=" + Convert.ToInt16(tabAgenceFD[i]);
                        }
                        General.Execute(stemp);
                    }

                    tabAgence = new string[17, 1];

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Votre fiche fournisseur est enregistrée");
                    blnModeAjout = false;
                }
                else
                {
                    blnModeAjout = false;
                    return;
                }

                //Update de la base sage
                if (Err().Description == "Les données chaîne ou binaires seront tronquées.")
                {
                   Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un champs comporte trop de caractères.");
                }*/
            }
        }
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            Save();
        }

        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            int fok = 0;


            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppression impossible", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;

            fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez-vous la suppression de cette fiche", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fok == 6)
            {
                Delete();
                clear();

            }
        }

        private void ctr10_TextChanged(object sender, EventArgs e)
        {

        }

        private void ctr00B_TextChanged(object sender, EventArgs e)
        {

        }

        private void ctr00B_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string strCodeFournisseur = null;
            if (KeyAscii == 13 && blnModeAjout == false)
            {

                General.sSQL = "SELECT FournisseurArticle.Code FROM FournisseurArticle Where fournisseurArticle.Code= '" + StdSQLchaine.gFr_DoublerQuote(ctr00B.Text) + "'";
                var tmpAdo = new ModAdo();
                strCodeFournisseur = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), "").ToString();
                if (!string.IsNullOrEmpty(strCodeFournisseur))
                {
                    SelectFns(strCodeFournisseur);
                }
                else
                {

                    fc_Rechercher();
                }

            }
            e.KeyChar = (char)KeyAscii;
            if (KeyAscii == 0)
            {
                e.Handled = true;
            }
        }

        private void ctr00B_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            string stemp = null;
            if (blnModeAjout == true)
            {
                goto EventExitSub;
            }
            stemp = fct_FastLookup(ctr00B, "FournisseurArticle", "code");

            if (!string.IsNullOrEmpty(stemp))
            {
                SelectFns(stemp);
            }
            else
            {
                Cancel = false;
            }
            EventExitSub:
            e.Cancel = Cancel;
        }
        private string fct_FastLookup(iTalk_TextBox_Small2 octrl, string stable, string sfield)
        {
            string functionReturnValue = null;
            DataTable rsTemp = default(DataTable);
            string stemp = null;
            double nTemp = 0;
            functionReturnValue = "";

            stemp = "select " + sfield + " from " + stable + " where " + sfield + " like '" + StdSQLchaine.gFr_DoublerQuote(octrl.Text) + "%'";
            using (var tmpADO = new ModAdo())
            {
                rsTemp = tmpADO.fc_OpenRecordSet(stemp);
                if (rsTemp.Rows.Count > 0)
                {
                    //rsTemp.MoveLast();
                    if (rsTemp.Rows.Count == 1)

                        functionReturnValue = rsTemp.Rows[0].ToString();
                }
                ModAdo.fc_CloseRecordset(rsTemp);
            }
            return functionReturnValue;
        }

        private void Grid0_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {

            /* double k = 0;
             double j = 0;
             double e = 0;
             int nMax = 0;
             k = Information.UBound(tabAgence, 2);
             nMax = k;
             j = 0;
             e = 0;
             if (hidFNSAdd.Text == "1")
             {
                 if (string.IsNullOrEmpty(hidCleAuto.Text) & string.IsNullOrEmpty(ctr00B.Text))
                     return;
                 if (Convert.ToInt16(Grid0.Columns[10].Value) > 0)
                 {
                     for (j = 0; j <= Information.UBound(tabAgence, 1); j++)
                     {

                         tabAgence[j, k] = Grid0.Columns[j].Value;
                     }
                     hidFNSAdd.Text = "10";
                 }
                 else
                 {
                     for (j = 0; j <= Information.UBound(tabAgence, 2); j++)
                     {
                         if (Grid0.Columns[10].Value == tabAgence[10, j])
                         {
                             k = j;
                             e = -1;
                             break; // TODO: might not be correct. Was : Exit For
                         }
                     }
                     for (j = 0; j <= Information.UBound(tabAgence, 1); j++)
                     {

                         tabAgence[j, k] = Grid0.Columns[j].Value;
                     }
                     hidFNSAdd.Text = "11";
                 }
                 CmdSauver.Enabled = true;
                 tabAgence = (string[])Microsoft.VisualBasic.CompilerServices.Utils.CopyArray(tabAgence, new string[Information.UBound(tabAgence, 1) + 1, nMax + 1 + e + 1]);
             }*/
        }

        private void Grid0_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            int bw = 0;

            bw = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (bw == 6)
            {

            }
            else
            {
                e.Cancel = true;
            }
            /*  string[] tabTemp = null;
              double iMax1 = 0;
              double imax2 = 0;
              if (bw == 6)
              {
                  for (i = 0; i <= Grid0.SelBookmarks.Count - 1; i++)
                  {

                      if (!string.IsNullOrEmpty(Grid0.Columns["No"].CellValue(Grid0.SelBookmarks[i])))
                      {

                          if (Convert.ToInt16(Grid0.Columns["No"].CellValue(Grid0.SelBookmarks[i])) >= 0)
                          {

                              tabAgenceFD[i] = Grid0.Columns["No"].CellValue(Grid0.SelBookmarks[i]);
                              e = 0;
                          }
                          else
                          {
                              for (j = 0; j <= Information.UBound(tabAgence, 2); j++)
                              {

                                  if (Grid0.Columns["No"].CellValue(Grid0.SelBookmarks[i]) == tabAgence[9, j])
                                  {

                                      k = j;
                                      break; 
                                  }
                              }

                              {
                                  iMax1 = Information.UBound(tabAgence, 1);
                                  imax2 = Information.UBound(tabAgence, 2);
                                  tabTemp = new string[iMax1 + 1, imax2];
                                  for (j = 0; j <= iMax1; j++)
                                  {
                                      for (l = 0; l <= imax2 - 1; l++)
                                      {

                                          if (l != k)
                                              tabTemp[j, l] = tabAgence[j, l];
                                      }
                                  }
                                  tabAgence = new string[iMax1 + 1, imax2];
                                  for (j = 0; j <= iMax1; j++)
                                  {
                                      for (l = 0; l <= imax2 - 1; l++)
                                      {
                                          tabAgence[j, l] = tabTemp[j, l];
                                      }
                                  }
                              }
                          }
                      }
                  }
                  Array.Resize(ref tabAgenceFD, Information.UBound(tabAgenceFD) + 1 + e + 1);

              }
              else
              {
                  e.cancel = true;
              }*/

        }

        private void Grid0_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (string.IsNullOrEmpty(Grid0.ActiveRow.Cells[0].Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne Agence.");
                e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(Grid0.ActiveRow.Cells[3].Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne adresse.");
                e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(Grid0.ActiveRow.Cells[4].Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne ville.");
                e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(Grid0.ActiveRow.Cells[5].Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne téléphone.");
                e.Cancel = true;
            }
            else
            {
                hidFNSAdd.Text = "1";
                if (string.IsNullOrEmpty(Grid0.ActiveRow.Cells[0].Value.ToString()))
                {
                    Grid0.ActiveRow.Cells[10].Value = -nInt;
                    nInt = nInt + 1;
                    e.Cancel = false;
                }

            }
        }

        private void llabel12_Click(object sender, EventArgs e)
        {
            int fok = 0;
            if (CmdSauver.Enabled == true)
            {
                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fok == 6)
                    Save();
            }
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserBCLivraison, "");
            // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCLivraison);
        }

        private void llabel13_Click(object sender, EventArgs e)
        {
            int fok = 0;
            if (CmdSauver.Enabled == true)
            {
                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fok == 6)
                    Save();
            }
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUderBCmdBody, "");

            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);
        }
        private void codeRgt(UltraCombo oargctrl)
        {
            DataTable rs = default(DataTable);
            string sSQL = "";
            DataTable oargctrlDT = new DataTable();
            sSQL = " Select Libelle from codereglement";
            using (var TmpAdo = new ModAdo())

                rs = TmpAdo.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count > 0)
            {
                foreach (DataRow rsRow in rs.Rows)
                {

                    oargctrlDT.Rows.Add(rsRow["Libelle"]);
                    oargctrl.DataSource = oargctrlDT;
                    // rs.MoveNext();
                }
            }
            ModAdo.fc_CloseRecordset(rs);

        }

        private void UserDocFourn_VisibleChanged(object sender, EventArgs e)
        {
            bool bTemp = false;
            DataTable adotemp = default(DataTable);
            SqlDataAdapter adotempSDA;

            if (ModMain.bActivate == true)
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                General.open_conn();
                blnModeAjout = false;


                /*if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                {
                    imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                    //        If IsNumeric(imgTop) Then
                    //            imgLogoSociete.Top = imgTop
                    //        End If
                    //        If IsNumeric(imgLeft) Then
                    //            imgLogoSociete.Left = imgLeft
                    //        End If
                }

           
                if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
                {
                    lblNomSociete.Text = General.NomSousSociete;
                    System.Windows.Forms.Application.DoEvents();
                }*/

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    SAGE.fc_OpenConnSage();
                    adotemp = new DataTable();

                    //SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable]," _
                    //& " F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                    //& " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)=8 ) " _
                    //& "  ORDER BY F_COMPTEG.CG_NUM"

                    General.SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable],"
                        + " F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX "
                        + "FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                        + " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)>= 3) "
                        + "  ORDER BY F_COMPTEG.CG_NUM";

                    adotempSDA = new SqlDataAdapter(General.SQL, SAGE.adoSage);

                    //    adotemp.Open SQL, adocnn
                    adotempSDA.Fill(adotemp);

                    var cmbCptChargesDT = new DataTable();
                    if (adotemp.Rows.Count > 0)
                    {
                        foreach (DataRow adotempRow in adotemp.Rows)
                        {
                            General.SQL = adotempRow["Code comptable"] + "" + "\n";
                            General.SQL = General.SQL + adotempRow["Intitulé"] + "" + "\n";
                            General.SQL = General.SQL + adotempRow["CG_NUM"] + "" + "\n";
                            General.SQL = General.SQL + adotempRow["TA_Taux"] + "" + "\n";

                            cmbCptChargesDT.Rows.Add(General.SQL);
                            cmbCptCharges.DataSource = cmbCptChargesDT;
                        }
                        adotemp.Dispose();
                    }

                    SAGE.fc_CloseConnSage();
                }


                //cmbCptCharges.DataFieldList = "Column 0";

                GridCptCharges.DisplayLayout.Bands[0].Columns["Compte de charge"].ValueList = cmbCptCharges;

                tabAgence = (string[])Microsoft.VisualBasic.CompilerServices.Utils.CopyArray(tabAgence, new string[17, 1]);
                Array.Resize(ref tabAgenceFD, 1);
                bTemp = true;
                ModParametre.fc_RecupParam(this.Name);
                if (!(ModParametre.sFicheAppelante == "UserBCmdHead" || ModParametre.sFicheAppelante == "UderBCmdBody"
                    || ModParametre.sFicheAppelante == "UserDocFourn" || ModParametre.sFicheAppelante == "UserDocFactFourn"
                    || ModParametre.sFicheAppelante == "UserReceptMarchandise" ||
                    General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserPreCommande2)))
                {

                    General.saveInReg("UserBCmdHead", "NoBcmd", "");
                    General.saveInReg("UserBCmdHead", "NoFNS", "");
                    if (ModParametre.sFicheAppelante == "UserDocFichePrincipal" ||
                        string.IsNullOrEmpty(General.getFrmReg(General.cFrNomApp, "UserDocFourn", "sFicheAppelante")))
                    {
                        General.saveInReg("UserDocFourn", "sFicheAppelante", "");
                        General.saveInReg("UserBCmdHead", "NoInt", "-1");
                        General.saveInReg("UserBCmdHead", "NoImmeuble", "");
                        //Label12.Top = 1560
                        //Label1.Top = 2280
                        //Label13.Visible = False
                        //Label22.Visible = False
                        cmdIntervention.Visible = false;
                        Label22.Enabled = false;
                    }
                }
                //== modif rachid ajout de la fiche precommande.
                if (General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserPreCommande))
                {
                    sNoFNS = General.getFrmReg(Variable.cUserPreCommande2, "CodeFournisseur", "");
                    Label22.Enabled = false;
                    llabel13.Enabled = false;
                }
                else
                {

                    sNoFNS = General.nz(General.getFrmReg("UserBCmdHead", "NoFNS"), "").ToString();
                }

                if (!string.IsNullOrEmpty(sNoFNS))
                    bTemp = false;

                if (General.nz(General.getFrmReg("UserBCmdHead", "NoBcmd"), -1).ToString() != "-1")
                    llabel13.Enabled = true;
                nInt = 1;

                hidCleAuto.Text = sNoFNS;
                codeRgt(ssDropcodeReglmt);

                Grid0.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                Grid0.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                Grid0.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;

                Array.Resize(ref tabTemp, 2);
                tabTemp[0] = "Sous Traitant";
                tabTemp[1] = "Fournisseur";
                ctr50.Rows.Dispose();

                codeRgt(ctr50);
                CtrB30.Rows.Dispose();
                // GeneralXav.Sub_Type(CtrB30, tabTemp);
                // txtG00.DataFieldList = "column 0";
                //Grid0.Columns[12].DropDownHwnd = txtG00;todo
                //   Grid0.RemoveAll();
                //== modif 09 11 2007.
                ///'Init True, False, False, False, False, True, bTemp
                fc_BloqueForm();
                SelectFns(sNoFNS);
                ModParametre.fc_OpenConnGecet();

                /* lblNavigation[0].Text = General.sNomLien0;
                 lblNavigation[1].Text = General.sNomLien1;
                 lblNavigation[2].Text = General.sNomLien2;*/
            }
            ModMain.bActivate = false;
        }
        private void Delete()
        {
            string SQL = "";
            SQL = "Delete from FournisseurArticle where CleAUto=" + Convert.ToInt16(hidCleAuto.Text);
            General.Execute(SQL);
        }

        private void cmbFourn_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns[0].Width = 200;
        }
    }
}
