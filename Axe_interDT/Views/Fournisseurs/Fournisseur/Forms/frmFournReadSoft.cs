﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.Fournisseur.Forms
{
    public partial class frmFournReadSoft : Form
    {
        DataTable rsSupl;
        ModAdo rsSuplModAdo=new ModAdo();
        SqlCommandBuilder rsSuplSCB;
        public frmFournReadSoft()
        {
            InitializeComponent();
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            ssSupplier.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFournReadSoft_Activated(object sender, EventArgs e)
        {
          
        }
        private void frmFournReadSoft_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            ReadSoft.fc_ConnectInvoices();
            fc_Supplier();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_Supplier()
        {
            string sSQL = "";
            sSQL = "SELECT   supplierid, suppliernumber, corporategroupid,"
                + " name1, name2, description, pobox, street, " 
                + " streetsupplement, postalcode, city, telephonenumber, "
                + " faxnumber,location ,countrycoded  "
                + " FROM    zrs_supplier"
                + " ORDER BY name1";

            rsSupl = new DataTable();
            rsSupl = rsSuplModAdo.fc_OpenRecordSet(sSQL, ssSupplier, "supplierid", ReadSoft.adoInvoice);       
            ssSupplier.DataSource=rsSupl;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssSupplier_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ssSupplier.ActiveRow.Cells["countrycoded"].Text))
                {
                    ssSupplier.ActiveRow.Cells["countrycoded"].Value = "FRA";
                }

                //If .Columns("location").value = "" Then
                ssSupplier.ActiveRow.Cells["location"].Value = "";
                // End If
                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";ssSupplier_BeforeColUpdate");
            }
      

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssSupplier_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["corporategroupid"].Text))
            {
                e.Row.Cells["corporategroupid"].Value = 1;
            }

            if (string.IsNullOrEmpty(e.Row.Cells["Name1"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La Raison sociale est obligatoire.", "",MessageBoxButtons.OK,MessageBoxIcon.Information);
                e.Cancel = true;
            }
            if (e.Row.Cells["suppliernumber"].DataChanged)
            {
                if (e.Row.Cells["suppliernumber"].OriginalValue.ToString() != e.Row.Cells["suppliernumber"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM zrs_supplier WHERE suppliernumber= '{e.Row.Cells["suppliernumber"].Text}'",false, ReadSoft.adoInvoice));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssSupplier_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dg == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssSupplier_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            rsSuplModAdo.Update();
        }

        private void ssSupplier_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsSuplModAdo.Update();
        }
       
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssSupplier_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
          
            ssSupplier.DisplayLayout.Bands[0].Columns["supplierid"].Hidden = true;
            ssSupplier.DisplayLayout.Bands[0].Columns["suppliernumber"].Header.Caption ="Code";
            ssSupplier.DisplayLayout.Bands[0].Columns["corporategroupid"].Hidden=true;
            ssSupplier.DisplayLayout.Bands[0].Columns["name1"].Header.Caption ="Raison social";
            ssSupplier.DisplayLayout.Bands[0].Columns["name2"].Hidden = true;
            ssSupplier.DisplayLayout.Bands[0].Columns["description"].Header.Caption = "Commentaire";
            ssSupplier.DisplayLayout.Bands[0].Columns["pobox"].Hidden = true;
            ssSupplier.DisplayLayout.Bands[0].Columns["street"].Header.Caption ="Adresse";
            ssSupplier.DisplayLayout.Bands[0].Columns["streetsupplement"].Hidden = true;
            ssSupplier.DisplayLayout.Bands[0].Columns["postalcode"].Header.Caption ="CP";
            ssSupplier.DisplayLayout.Bands[0].Columns["city"].Header.Caption ="Ville";
            ssSupplier.DisplayLayout.Bands[0].Columns["telephonenumber"].Header.Caption ="Tel";
            ssSupplier.DisplayLayout.Bands[0].Columns["faxnumber"].Header.Caption ="Fax";
           
        }

        private void frmFournReadSoft_FormClosed(object sender, FormClosedEventArgs e)
        {
            ReadSoft.fc_CloseConnInvoices();             
            rsSupl = null;
        }

      
    }
}
