﻿namespace Axe_interDT.Views.Fournisseurs.Fournisseur.Forms
{
    partial class frmFournReadSoft
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.ssSupplier = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ssSupplier)).BeginInit();
            this.SuspendLayout();
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(563, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 375;
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // ssSupplier
            // 
            this.ssSupplier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ssSupplier.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.ssSupplier.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ssSupplier.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ssSupplier.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssSupplier.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssSupplier.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssSupplier.DisplayLayout.UseFixedHeaders = true;
            this.ssSupplier.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssSupplier.Location = new System.Drawing.Point(12, 45);
            this.ssSupplier.Name = "ssSupplier";
            this.ssSupplier.Size = new System.Drawing.Size(601, 461);
            this.ssSupplier.TabIndex = 572;
            this.ssSupplier.Text = "Fournisseur READ SOFT";
            this.ssSupplier.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssSupplier_InitializeLayout);
            this.ssSupplier.AfterRowsDeleted += new System.EventHandler(this.ssSupplier_AfterRowsDeleted);
            this.ssSupplier.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssSupplier_AfterRowUpdate);
            this.ssSupplier.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.ssSupplier_BeforeRowUpdate);
            this.ssSupplier.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssSupplier_BeforeExitEditMode);
            this.ssSupplier.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssSupplier_BeforeRowsDeleted);
            // 
            // frmFournReadSoft
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(625, 509);
            this.Controls.Add(this.ssSupplier);
            this.Controls.Add(this.CmdSauver);
            this.MaximumSize = new System.Drawing.Size(641, 548);
            this.MinimumSize = new System.Drawing.Size(641, 548);
            this.Name = "frmFournReadSoft";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmFournReadSoft";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmFournReadSoft_FormClosed);
            this.Load += new System.EventHandler(this.frmFournReadSoft_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ssSupplier)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button CmdSauver;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssSupplier;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}