﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Fournisseurs.Fournisseur.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinGrid;
using System;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Fournisseurs.Fournisseur
{
    public partial class UserDocFournisseur : UserControl
    {
        string[] tabAgence;
        string[] tabAgenceFD;
        double nInt;
        string sNoFNS;
        string[] tabTemp;

        DataTable rsCpteChargesFourn;
        ModAdo rsCpteChargesFournModado = new ModAdo();
        DataTable rsFNS;
        ModAdo rsFNSModado = new ModAdo();
        bool bAjout;

        ModAdo ModAdors = new ModAdo();
        public UserDocFournisseur()
        {
            InitializeComponent();
        }

        private void fc_Rechercher()
        {
            try
            {
                string sCode = "";
                string where = "";
                string requete = "";
                if (General.sModuleReadSoft == "1")
                {

                    requete = "SELECT  Code as \"code\", CleAuto as \"N°Fiche\" ,  Raisonsocial as \"RaisonSociale\" ," + " Adresse as \"Adresse\", Ville as \"Ville\", NePasAfficher as \"NePasAfficher\" " + " FROM fournisseurArticle ";
                }
                else
                {
                    requete = "SELECT Code as \"code\",CleAuto as  \"N°Fiche\" ,  Raisonsocial as \"RaisonSociale\" ," + " Adresse as \"Adresse\", Ville as \"Ville]\", Nom as \"Relation commercial\" " + " FROM fournisseurArticle ";
                }
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche  des fournisseurs" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells["N°Fiche"].Value.ToString();
                    SelectFns(Convert.ToInt16(sCode));//tested
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells["N°Fiche"].Value.ToString();
                        SelectFns(Convert.ToInt16(sCode));
                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
                return;
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="iargfns"></param>
        private void SelectFns(int iargfns)
        {
            string sSQL = null;
            string sTyfo = null;
            DataTable rs = default(DataTable);
            string stemp = null;
            string sqlFourn = null;
            int lCleFourn = 0;
            int lType = 0;

            fc_BloqueForm();

            sSQL = "SELECT Cleauto, Agree, Code, Nom, Adresse, Raisonsocial,"
                + " Ville, Contact, Type, Portable, HDM, HDA, HFM, HFA, CodePostal,"
                + " Condition, Commentaire, " + " Telephone , Fax, Email, NePasAfficher, CodeFournGecet,"
                + " SansCodeGecet, NCompte, ModeRgt, CodeReadSoft, CalculRdTTC, analytique, TYFO_Code , ServiceAnalytique "
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                + ", BCD_Obligatoire "
                //===> Fin Modif Mondir
                + " From fournisseurArticle" + " WHERE fournisseurArticle.Cleauto= '" + StdSQLchaine.gFr_DoublerQuote(iargfns.ToString()) + "'";



            rs = ModAdors.fc_OpenRecordSet(sSQL);
            lCleFourn = 0;
            lType = 1;
            //==== 1 = fournisseur, 0 = sous traitance.

            if (rs.Rows.Count > 0)
            {
                lCleFourn = Convert.ToInt16(General.nz(rs.Rows[0]["cleAuto"], 0));
                hidCleAuto.Text = Convert.ToString(lCleFourn);

                txtCode.Text = General.nz(rs.Rows[0]["Code"], "").ToString();

                txtNom.Text = General.nz(rs.Rows[0]["Nom"], "").ToString();

                txtRaisonsocial.Text = General.nz(rs.Rows[0]["Raisonsocial"], "").ToString();

                txtAdresse.Text = General.nz(rs.Rows[0]["Adresse"], "").ToString();

                txtVille.Text = General.nz(rs.Rows[0]["Ville"], "").ToString();
                txtCodepostal.Text = General.nz(rs.Rows[0]["Codepostal"], "").ToString();

                cmbModeRgt.Text = General.nz(rs.Rows[0]["ModeRgt"], "").ToString();

                txtCondition.Text = General.nz(rs.Rows[0]["Condition"], "").ToString();

                txtTelephone.Text = General.nz(rs.Rows[0]["Telephone"], "").ToString();

                txtPortable.Text = General.nz(rs.Rows[0]["Portable"], "").ToString();

                txtContact.Text = General.nz(rs.Rows[0]["Contact"], "").ToString();

                txtFax.Text = General.nz(rs.Rows[0]["Fax"], "").ToString();

                txtEmail.Text = General.nz(rs.Rows[0]["Email"], "").ToString();

                txtCommentaire.Text = General.nz(rs.Rows[0]["Commentaire"], "").ToString();

                if (rs.Rows[0]["agree"].ToString() == "1")
                {
                    chkagree.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else if (rs.Rows[0]["agree"].ToString() == "0")
                {
                    chkagree.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }


                txtNCompte.Text = General.nz(rs.Rows[0]["Ncompte"], "").ToString();

                txtHDM.Text = General.nz(rs.Rows[0]["HDM"], "").ToString();

                txtHDA.Text = General.nz(rs.Rows[0]["HDA"], "").ToString();

                txtHFM.Text = General.nz(rs.Rows[0]["HFM"], "").ToString();

                txtHFA.Text = General.nz(rs.Rows[0]["HFA"], "").ToString();

                cmbFourn.Text = General.nz(rs.Rows[0]["CodeFournGecet"], "").ToString();

                chkNePasAfficher.Checked = General.nz(rs.Rows[0]["NePasAfficher"], "").ToString() == "1";

                chkSansCodeGecet.Checked = General.nz(rs.Rows[0]["SansCodeGecet"], "").ToString() == "1";

                chkCalculRdTTC.Checked = General.nz(rs.Rows[0]["CalculRdTTC"], "").ToString() == "1";

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                if (Convert.ToInt32(General.nz(rs.Rows[0]["BCD_Obligatoire"], 0)) == 1)
                {
                    optObligatoire.Checked = true;
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["BCD_Obligatoire"], 0)) == 2)
                {
                    OptNonObligatoire.Checked = true;
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["BCD_Obligatoire"], 0)) == 3)
                {
                    OptLesDeux.Checked = true;
                }
                else
                {
                    optObligatoire.Checked = false;
                    OptNonObligatoire.Checked = false;
                    OptLesDeux.Checked = false;
                }
                //===> Fin Modif Mondir


                if (chkSansCodeGecet.CheckState == CheckState.Checked)
                {
                    cmbFourn.Enabled = false;
                }
                else
                {
                    cmbFourn.Enabled = true;
                }
                string stype = General.nz(rs.Rows[0]["Type"], 0).ToString();
                int type = Convert.ToInt16(stype);

                GeneralXav.Sub_Type(cmbType, tabTemp, type);

                if (General.sModuleReadSoft == "1")//tested
                {
                    cmbCodeReadSoft.Text = rs.Rows[0]["CodeReadSoft"] + "";
                    cmdAnalytique.Text = rs.Rows[0]["analytique"] + "";
                    fc_LibService();
                }

                if (General.sServiceAnalytique == "1")//tested
                {
                    cmbService.Text = rs.Rows[0]["ServiceAnalytique"] + "";
                }



                if (!string.IsNullOrEmpty(General.nz(rs.Rows[0]["TYFO_Code"], "").ToString()))//tested
                {
                    sTyfo = "SELECT  TYFO_Libelle From TYFO_TypeFourn WHERE  TYFO_Code = '" + rs.Rows[0]["TYFO_Code"] + "' ";
                    using (var tmpAdo = new ModAdo())
                        sTyfo = tmpAdo.fc_ADOlibelle(sTyfo);
                    cmbTYFO.Text = sTyfo;
                    //cmbTYFO.ListIndex = rs!TYFO_Code
                }
                else
                {
                    cmbTYFO.Text = "";
                }

            }
            else
            {
                clear();
            }

            //== 1 = fournisseur, 0 = sous traitance.
            // Call Sub_Type(cmbType, tabTemp, 1)


            rs.Dispose();

            rs = null;

            //====affiche les agences.
            fc_FNS_Agences(lCleFourn);//tested

            //==== affiche les comptes de charges lié aux fournisseurs.
            fc_CpteCharge(lCleFourn);

            return;
        }
        /// <summary>
        /// testeed
        /// </summary>
        /// <param name="cleAuto"></param>
        private void fc_CpteCharge(int cleAuto)
        {
            string sqlFourn = null;

            //==== affiche la grille des comptes de charges lié au fournisseur.
            if (rsCpteChargesFournModado != null)
            {
                rsCpteChargesFournModado.Close();

            }
            else
            {
                rsCpteChargesFournModado = new ModAdo();
            }

            sqlFourn = "SELECT CG_Num,Intitule,TaxeCG_Num,TA_Taux,CleAutoFournisseur "
                + " FROM CpteChargeFournisseur WHERE CleAutoFournisseur=" + cleAuto + "";

            rsCpteChargesFourn = rsCpteChargesFournModado.fc_OpenRecordSet(sqlFourn);
            GridCptCharges.DataSource = rsCpteChargesFourn;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lFNS_Cle"></param>
        private void fc_FNS_Agences(int lFNS_Cle)
        {
            string sSQL = null;

            sSQL = "SELECT  Fns_No,Fns_Agence, Fns_Contact, Fns_Qualite, Fns_Adresse,"
                + " Fns_Codepostal, Fns_Ville, Fns_Tel, FNS_Portable, Fns_Fax,"
                + " Fns_Email,  Fns_Cpt, " + " Fns_Rgt , FNS_HDM, FNS_HFM, FNS_HDA, FNS_HFA, Fns_Cle "
                + " FROM         FNS_Agences" + " WHERE FNS_Cle =" + lFNS_Cle;

            rsFNS = rsFNSModado.fc_OpenRecordSet(sSQL, ssFNS, "Fns_No");

            ssFNS.DataSource = rsFNS;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSansCodeGecet_CheckedChanged(object sender, EventArgs e)
        {
            if (chkSansCodeGecet.CheckState == CheckState.Checked)
            {
                cmbFourn.Text = "";
                cmbFourn.Enabled = false;
            }
            else
            {
                cmbFourn.Enabled = true;
            }
        }
        private void chkSansCodeGecet_CheckStateChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeReadSoft_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sqlSelect = "";
            ReadSoft.fc_ConnectInvoices();
            sqlSelect = "SELECT     suppliernumber FROM   zrs_supplier order by suppliernumber";
            sheridan.InitialiseCombo(cmbCodeReadSoft, sqlSelect, "suppliernumber", false, ReadSoft.adoInvoice);
            ReadSoft.fc_CloseConnInvoices();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbFourn_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sqlSelect = "";

            if (cmbFourn.Rows.Count == 0)
            {
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //sqlSelect = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix  where fournisseur <>'Fourn non connu' ORDER BY Dtiprix.Fournisseur";
                sqlSelect = "SELECT DISTINCT NOM_FOURNISSEUR From ArticleTarif ORDER BY NOM_FOURNISSEUR";
                //===> Fin Modif Mondir

                //===> Mondir le 13.01.2021 changer la colonne en NOM_FOURNISSEUR et non Fournisseur car la requette a été modifiée dans la version V12.11.2020
                sheridan.InitialiseCombo(cmbFourn, sqlSelect, "NOM_FOURNISSEUR", false, ModParametre.adoGecet);

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_AfterCloseUp(object sender, EventArgs e)
        {

            fc_LibService();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            fc_LibService();
            e.Cancel = Cancel;
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LibService()
        {
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                SqlDataAdapter rsSDA;

                if (string.IsNullOrEmpty(cmbService.Text))//tested
                {
                    lblLibService.Text = "";
                    return;
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    lblLibService.Text = "";
                    return;
                }

                SAGE.fc_OpenConnSage();

                sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";
                rs = new DataTable();

                rsSDA = new SqlDataAdapter(sSQL, SAGE.adoSage);
                rsSDA.Fill(rs);

                if (rs.Rows.Count > 0)
                {
                    lblLibService.Text = rs.Rows[0]["CA_Intitule"] + "";
                }

                rs.Dispose();
                rs = null;
                SAGE.fc_CloseConnSage();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LibService");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlService()
        {
            bool functionReturnValue = false;
            try
            {

                //===  retourne true si tout est correct.
                DataTable rs = default(DataTable);
                SqlDataAdapter rsSDA;
                functionReturnValue = true;

                //If cmbService = "" Then
                //    MsgBox "Le code service est désormais obligatoire, veuillez sélectionner un code service dans la liste déroulante.", vbInformation, "Saisie incomplète."
                //    fc_CtrlService = False
                //    Exit Function
                //End If

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    if (!string.IsNullOrEmpty(cmbService.Text))
                    {
                        //===  controle si le code analytique service est correct.
                        SAGE.fc_OpenConnSage();
                        General.sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";

                        rs = new DataTable();
                        rsSDA = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                        rsSDA.Fill(rs);

                        if (rs.Rows.Count == 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code service " + cmbService.Text + " n'existe pas, veuillez en sélectionner un autre code service dans la liste déroulante.");

                            functionReturnValue = false;
                        }

                        rs.Dispose();
                        rs = null;
                        SAGE.fc_CloseConnSage();
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlService");
                return functionReturnValue;
            }

        }

        private void cmbService_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                string sSQL = "";

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    sSQL = "SELECT     CA_Num as Code, CA_Intitule AS Intitulé " + " From F_COMPTEA" + " ";
                    sSQL = sSQL + ModAnalytique.fc_WhereServiceAnalytique(" Where N_Analytique = 6");
                    sSQL = sSQL + " ORDER BY CA_Num";

                    SAGE.fc_OpenConnSage();
                    sheridan.InitialiseCombo(cmbService, sSQL, "Code", false, SAGE.adoSage);

                    SAGE.fc_CloseConnSage();

                    // cmbService.Columns[1].Width = 3000;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbService_DropDown");
            }


        }



        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            double fok = 0;
            bool bAdd = false;
            bool bUpdate = false;
            bool bDelete = false;

            //=== controle si l'utilisateur a les droits d'ajout.
            ModAutorisation.fc_Droit(Variable.Cuserdocfourn, Variable.Cuserdocfourn, ref bAdd, ref bUpdate, ref bDelete);

            if (!bAdd)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, vous n'avez pas l'autorisation d'ajouter un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }

            //    If CmdSauver.Enabled = True Then
            //        fok = MsgBox("Souhaitez vous enregistrer les modifications.", vbYesNoCancel)
            //    End If

            //  If fok = 6 Then
            //          Save
            clear();
            //   ElseIf fok = 2 Then
            //
            //           Exit Sub
            //   Else
            //           clear
            //   End If

            //=== bloque des controles.
            fc_BloqueForm(General.cAjouter);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {

            try
            {
                bAjout = false;

                var _with2 = this;

                //=== bloque systématiquement le boutton supprimer.
                _with2.cmdSupprimer.Enabled = false;
                CmdSauver.Enabled = true;
                cmdAjouter.Enabled = true;
                cmdRechercheImmeuble.Enabled = true;
                CmdRechercher.Enabled = true;

                //=== mode ajout.
                if (General.UCase(sCommande) == General.UCase(General.cAjouter))
                {
                    bAjout = true;
                    _with2.CmdRechercher.Enabled = false;
                    _with2.cmdRechercheImmeuble.Enabled = false;
                    _with2.CmdRechercher.Enabled = false;
                    _with2.cmdAnnuler.Enabled = true;
                    _with2.cmdAjouter.Enabled = false;
                    //            .cmdRechercheRaisonSocial.Enabled = False
                    //            .cmdAjouter.Enabled = True
                    //            .CmdRechercher.Enabled = False
                    //            .CmdSauver.Enabled = True
                    //active le mode modeAjout
                    //        blnAjout = True

                }
                else
                {

                    _with2.cmdAnnuler.Enabled = false;
                    //            .cmdRechercheClient.Enabled = True
                    //            .cmdRechercheImmeuble.Enabled = True
                    //             .cmdRechercheRaisonSocial.Enabled = True
                    //            .CmdRechercher.Enabled = True
                    //            .CmdSauver.Enabled = False
                    //            'desactive le mode modeAjout
                    //         blnAjout = False
                }


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BloqueForm");

            }

        }
        public void ClearTextBox(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    ClearTextBox(CC);

            if (C is iTalk.iTalk_TextBox_Small2 || C is UltraCombo || C is iTalk.iTalk_RichTextBox)
            {
                C.Text = "";
            }
        }
        private void clear()
        {
            ClearTextBox(this);

            cmbFourn.Text = "";
            hidCleAuto.Text = "";
            chkNePasAfficher.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkSansCodeGecet.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCalculRdTTC.CheckState = System.Windows.Forms.CheckState.Unchecked;
            cmbFourn.Enabled = true;
            lblLibService.Text = "";

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            optObligatoire.Checked = false;
            OptNonObligatoire.Checked = false;
            OptLesDeux.Checked = false;
            //===> Fin Modif Mondir

            //txtNCompte.Locked = True
            //Grid0.RemoveAll
            //nInt = 1
            //tabAgence = (string[])Microsoft.VisualBasic.CompilerServices.Utils.CopyArray(tabAgence, new string[17, 1]);
            // Array.Resize(ref tabAgenceFD, 1);

            fc_FNS_Agences(0);
            fc_CpteCharge(0);

        }

        private void cmdAnalytique_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = "";

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                if (cmdAnalytique.Rows.Count == 0)
                {
                    SAGE.fc_OpenConnSage();
                    sSQL = "SELECT distinct F_COMPTEA.CA_NUM From F_COMPTEA WHERE F_COMPTEA.N_ANALYTIQUE=1";
                    sheridan.InitialiseCombo(cmdAnalytique, sSQL, "CA_NUM", false, SAGE.adoSage);
                    SAGE.fc_CloseConnSage();
                }
            }
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            int fok = 0;
            fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez-vous l'annulation de cette fiche", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (fok == 6)
            {
                clear();
                fc_BloqueForm();

            }
        }

        private void cmdFournreadSoft_Click(object sender, EventArgs e)
        {
            frmFournReadSoft frmFRS = new frmFournReadSoft();
            frmFRS.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
        }
        private bool CheckNeedFields()
        {
            bool functionReturnValue = false;
            string sSQL = null;
            //==== controle de saisies, retourne true si les donées sont correct.
            functionReturnValue = true;
            //== code fournisseur
            if (string.IsNullOrEmpty(txtCode.Text))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code fournisseur est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                return functionReturnValue;

            }


            if (string.IsNullOrEmpty(cmbFourn.Text) && General.nz((chkSansCodeGecet.Checked ? 1 : 0), "0").ToString() != "1")//tested
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code fournisseur GECET correspondant" + " au fournisseur que vous saisissez actuellement, si il s'agit d'un soustraitant " + "ou autre cliquez sur la case à cocher 'Sans correspondance avec Gecet'", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                cmbFourn.Focus();
                return functionReturnValue;


            }
            else if (General.nz((chkSansCodeGecet.Checked ? 1 : 0), "0").ToString() != "1")
            {


                sSQL = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix" + " WHERE Dtiprix.Fournisseur ='"
                    + StdSQLchaine.gFr_DoublerQuote(cmbFourn.Text) + "'" + " ORDER BY Dtiprix.Fournisseur";
                using (var tmpAdo = new ModAdo())

                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fournisseur Gecet incorrecte, Veuillez le sélectionner dans la liste.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = false;
                    cmbFourn.Focus();
                    return functionReturnValue;
                }
            }
            else if (GridCptCharges.Rows.Count == 0)//tested
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez  sélectionner un compte de charge dans la grille \"Compte de charge \".", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                return functionReturnValue;

            }
            else if (string.IsNullOrEmpty(cmbModeRgt.Text))//tested
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le mode de réglement est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                return functionReturnValue;

            }
            else if (string.IsNullOrEmpty(cmbType.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type de fournisseur est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                return functionReturnValue;
            }
            else if (General.sModuleReadSoft == "1" && string.IsNullOrEmpty(cmbCodeReadSoft.Text))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code fournisseur ReadSoft est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = false;
                return functionReturnValue;
            }
            return functionReturnValue;

        }
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
            ModAutorisation.fc_DroitParFiche(this.Name);
        }
        private void Save()
        {
            try
            {
                bool bTemp = false;
                string stemp = null;
                DataTable rs = default(DataTable);
                SqlDataAdapter rsSDA;
                string sSQLTe = null;
                string sTyfo = null;

                if (chkSansCodeGecet.CheckState == CheckState.Checked)//tested
                {
                    cmbFourn.Text = "";
                }

                bTemp = CheckNeedFields();//tested
                if (bTemp == false)//tested
                {
                    return;
                }
                else//tested
                {
                    fc_BloqueForm();
                }

                //===  controle si le code service est correct.
                if (fc_CtrlService() == false)//tested
                {
                    return;
                }

                //===== valide les grilles.

                GridCptCharges.UpdateData();

                ssFNS.UpdateData();

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                if (!optObligatoire.Checked && !OptNonObligatoire.Checked && !OptLesDeux.Checked)
                {
                    CustomMessageBox.Show("Vous devez spécifier si le numéro de bon de commande est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //===> Fin Modif Mondir

                //insertion des informations générales sur le fournisseur.

                //err().Clear();

                //===== création du compte en comptabilité.
                if (string.IsNullOrEmpty(txtNCompte.Text))//todo la fct fc_prcAjoutTiers leve exeception
                {
                    string sCode = txtCode.Text;
                    string sraisonSocial = txtRaisonsocial.Text;

                    txtNCompte.Text = SAGE.fc_prcAjoutTiers(2, ref sCode, ref sraisonSocial, ref sraisonSocial,
                        txtAdresse.Text, "", txtCodepostal.Text, txtVille.Text, txtContact.Text, txtTelephone.Text,
                    txtFax.Text, 2, txtEmail);
                    //Commented by Mondir ===> No Needs to change the text of the controls, and this caused a bug ===> https://groupe-dt.mantishub.io/view.php?id=1652
                    //Passing by reference a text of a TextBox will not change the text of this control in VB6 !
                    //txtCode.Text= sCode;

                }
                else//tested
                {

                    //====== modif du 23 aout controle que le compte present dans txtNCompte existe bien
                    //====== sinon demande confirmation de creation du compte.
                    //===@@@ modif du 29 05 2017, desactive Sage.
                    if (General.sDesActiveSage == "1")
                    {

                    }
                    else//tested
                    {
                        try
                        {
                            sSQLTe = "SELECT CT_Num From F_COMPTET WHERE CT_Num='" + StdSQLchaine.gFr_DoublerQuote(SAGE.fc_FormatCompte(txtNCompte.Text, 2)) + "'";
                            SAGE.fc_OpenConnSage();
                            rs = new DataTable();
                            rsSDA = new SqlDataAdapter(sSQLTe, SAGE.adoSage);
                            rsSDA.Fill(rs);
                            if (rs.Rows.Count == 0)
                            {
                                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le compte " + txtNCompte.Text + " n'existe pas en compta, Voulez-vous le créer?", "Création de compte", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dg == DialogResult.Yes)
                                {
                                    string nCompte = txtNCompte.Text;
                                    string raisonsocial = txtRaisonsocial.Text;

                                    txtNCompte.Text = SAGE.fc_prcAjoutTiers(2, ref nCompte, ref raisonsocial, ref raisonsocial,
                                        txtAdresse.Text, "", txtCodepostal.Text, txtVille.Text, txtContact.Text, txtTelephone.Text,
                                    txtFax.Text, 2, txtEmail);
                                    txtNCompte.Text = nCompte;

                                }
                            }
                            else
                            {
                                rs.Dispose();

                                rs = null;
                            }

                            SAGE.fc_CloseConnSage();
                        }
                        catch (Exception ex)
                        {
                            Program.SaveException(ex);
                            if (!string.IsNullOrEmpty(ex.Message))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible de mettre la base sage à jour, vérifier si votre connexion DTODBC existe");
                                return;
                            }
                        }
                    }
                }
                //==== mise à jour de la table des fournisseurs (table fournisseur article)

                stemp = "Select * From Fournisseurarticle Where Fournisseurarticle.CleAuto " + " = " + General.nz(hidCleAuto.Text, 0);
                rs = ModAdors.fc_OpenRecordSet(stemp);

                SqlCommandBuilder cb = new SqlCommandBuilder(ModAdors.SDArsAdo);
                ModAdors.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                ModAdors.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                ModAdors.SDArsAdo.InsertCommand = insertCmd;

                ModAdors.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {
                    if (e.StatementType == StatementType.Insert)
                    {
                        if (string.IsNullOrEmpty(hidCleAuto.Text))
                        {
                            rs.Rows[0]["Cleauto"] = e.Command.Parameters["@ID"].Value;

                        }
                    }
                });

                if (rs.Rows.Count == 0)
                {
                    rs.Rows.Add();
                }

                rs.Rows[0]["Nom"] = txtNom.Text;
                rs.Rows[0]["Raisonsocial"] = txtRaisonsocial.Text;
                rs.Rows[0]["Adresse"] = txtAdresse.Text;
                rs.Rows[0]["Ville"] = txtVille.Text;
                rs.Rows[0]["Codepostal"] = txtCodepostal.Text;
                rs.Rows[0]["Telephone"] = txtTelephone.Text;
                rs.Rows[0]["Portable"] = txtPortable.Text;
                rs.Rows[0]["Contact"] = txtContact.Text;
                rs.Rows[0]["Fax"] = txtFax.Text;
                rs.Rows[0]["Email"] = txtEmail.Text;
                rs.Rows[0]["Modergt"] = cmbModeRgt.Text;
                rs.Rows[0]["Condition"] = txtCondition.Text;
                rs.Rows[0]["Commentaire"] = txtCommentaire.Text;
                rs.Rows[0]["HDM"] = txtHDM.Text;
                rs.Rows[0]["HDA"] = txtHDA.Text;
                rs.Rows[0]["HFM"] = txtHFM.Text;
                rs.Rows[0]["HFA"] = txtHFA.Text;
                rs.Rows[0]["Code"] = General.Trim(General.UCase(txtCode.Text));
                int type;
                if (cmbType.ActiveRow == null)
                    type = -1;
                else
                    type = cmbType.ActiveRow.Index;
                rs.Rows[0]["Type"] = type;
                if (!string.IsNullOrEmpty(cmbTYFO.Text))
                {

                    sTyfo = "SELECT  TYFO_Code From TYFO_TypeFourn WHERE   TYFO_Libelle= '" + StdSQLchaine.gFr_DoublerQuote(cmbTYFO.Text) + "' ";
                    var tmpAdors = new ModAdo();
                    sTyfo = tmpAdors.fc_ADOlibelle(sTyfo);
                    if (string.IsNullOrEmpty(sTyfo))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'analytique secteur " + cmbTYFO.Text + " n'existe pas", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    rs.Rows[0]["TYFO_Code"] = sTyfo;

                    // rs("TYFO_Code") = nz(cmbTYFO.ListIndex + 1, Null)
                }
                else
                {

                    rs.Rows[0]["TYFO_Code"] = System.DBNull.Value;

                }
                rs.Rows[0]["Agree"] = chkagree.CheckState;
                rs.Rows[0]["NCompte"] = txtNCompte.Text;

                rs.Rows[0]["CodeFournGecet"] = General.nz(cmbFourn.Text, System.DBNull.Value);
                rs.Rows[0]["NePasAfficher"] = chkNePasAfficher.CheckState;
                rs.Rows[0]["SansCodeGecet"] = chkSansCodeGecet.CheckState;
                rs.Rows[0]["CalculRdTTC"] = chkCalculRdTTC.CheckState;

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                if (optObligatoire.Checked)
                {
                    rs.Rows[0]["BCD_Obligatoire"] = 1;
                }
                else if (OptNonObligatoire.Checked)
                {
                    rs.Rows[0]["BCD_Obligatoire"] = 2;
                }
                else if (OptLesDeux.Checked)
                {
                    rs.Rows[0]["BCD_Obligatoire"] = 3;
                }
                //===> Fin Modif Mondir

                if (General.sModuleReadSoft == "1")
                {
                    rs.Rows[0]["CodeReadSoft"] = cmbCodeReadSoft.Text;
                    rs.Rows[0]["analytique"] = cmdAnalytique.Text;
                }

                if (General.sServiceAnalytique == "1")
                {
                    rs.Rows[0]["ServiceAnalytique"] = cmbService.Text;
                }


                ModAdors.Update();

                hidCleAuto.Text = rs.Rows[0]["Cleauto"].ToString();

                ModAdors.Close();


                //==== associe l'id fournisseur aux comptes de charges.
                if (rsCpteChargesFourn.Rows.Count > 0)//tested
                {
                    foreach (DataRow rsCpteChargesFournRow in rsCpteChargesFourn.Rows)
                    {
                        rsCpteChargesFournRow["cleAutoFournisseur"] = Convert.ToInt32(hidCleAuto.Text);
                        rsCpteChargesFournModado.Update();

                    }
                }

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Votre fiche fournisseur est enregistrée", "Fiche fournisseur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                View.Theme.Theme.AfficheAlertSucces();
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                if (ex.Message == "Les données chaîne ou binaires seront tronquées.")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un champs comporte trop de caractères.");
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source);
                }
            }
        }
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            Save();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppression impossible.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
            return;
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="oargctrl"></param>

        private void codeRgt(UltraCombo oargctrl)
        {
            DataTable rs = default(DataTable);
            DataTable oargctrlDT = new DataTable();
            oargctrlDT.Columns.Add("Libelle");
            var NewRow = oargctrlDT.NewRow();
            string sSQL = null;
            sSQL = " Select Libelle from codereglement";

            using (var tmpAdors = new ModAdo())
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count > 0)
            {
                foreach (DataRow rsRow in rs.Rows)
                {
                    NewRow["Libelle"] = rsRow["Libelle"];
                    oargctrlDT.Rows.Add(NewRow.ItemArray);
                    oargctrl.DataSource = oargctrlDT;
                    // rs.MoveNext();
                }
            }
            ModAdo.fc_CloseRecordset(rs);
        }

        private void llabel12_Click(object sender, EventArgs e)
        {
            int fok = 0;
            if (CmdSauver.Enabled == true)
            {
                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fok == 6)
                    Save();
            }
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserBCLivraison, "");
            //View.Theme.Theme.Navigate(typeof(UserBCLivraison));
            // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCLivraison);
        }

        private void llabel13_Click(object sender, EventArgs e)
        {
            int fok = 0;
            if (CmdSauver.Enabled == true)
            {
                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fok == 6)
                    Save();
            }
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUderBCmdBody, "");
            //  View.Theme.Theme.Navigate(typeof(UderBCmdBody));
            // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);
        }

        private void Label22_Click(object sender, EventArgs e)
        {
            double fok = 0;
            if (CmdSauver.Enabled == true)
            {

                fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (fok == 6)
                    Save();
            }
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserBCmdHead, "");
            //==Fonction en cours de developpement.
            if (General.sPrecommande == "1")
            {
                General.saveInReg(Variable.cUserPreCommande, "Origine", this.Name);
                //  View.Theme.Theme.Navigate(typeof(USERPRECOMMANDE));
                // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUSERPRECOMMANDE);
            }
            else
            {
                //  View.Theme.Theme.Navigate(typeof(UserBCmdHead));
                //  ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCmdHead);
            }
        }



        private void UserDocFournisseur_VisibleChanged(object sender, EventArgs e)
        {
            //Dim bTemp As Boolean
            DataTable adotemp = default(DataTable);
            SqlDataAdapter adotempSDA;
            DataTable rsTYFO = default(DataTable);
            int lCleAutoFourn = 0;
            int xxx = 0;
            string sSQLTYFO = null;
            DataTable cmbCptChargesDT;

            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocFournisseur");


            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

            //====== ouvre la conn
            General.open_conn();

            if (General.sDesActiveSage == "1")
            {


                //====== ouverture de la connection vers sage
            }
            else
            {
                SAGE.fc_OpenConnSage();

                //====== extrait tous les comptes d'achats dans sage.               
                adotemp = new DataTable();

                //SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable]," _
                //& " F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                //& " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)=8 ) " _
                //& "  ORDER BY F_COMPTEG.CG_NUM"

                General.SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable],"
                    + " F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG INNER JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                    + " WHERE (F_COMPTEG.CG_NUM Like '6%' AND LEN(F_COMPTEG.CG_NUM)>= 3 ) " + "  ORDER BY F_COMPTEG.CG_NUM";


                // adotemp.Open(General.SQL, SAGE.adoSage);
                adotempSDA = new SqlDataAdapter(General.SQL, SAGE.adoSage);
                adotempSDA.Fill(adotemp);

                cmbCptChargesDT = new DataTable();
                cmbCptChargesDT.Columns.Add("Compte de charge");
                cmbCptChargesDT.Columns.Add("Intitulé");
                cmbCptChargesDT.Columns.Add("Compte de Taxe");
                cmbCptChargesDT.Columns.Add("Taux de TVA");

                var NewRow = cmbCptChargesDT.NewRow();

                if (adotemp.Rows.Count > 0)
                {
                    foreach (DataRow adotempRow in adotemp.Rows)
                    {
                        NewRow["Compte de charge"] = adotempRow["Code comptable"];
                        NewRow["Intitulé"] = adotempRow["Intitulé"];
                        NewRow["Compte de Taxe"] = adotempRow["CG_NUM"];
                        int taux = Convert.ToInt32(adotempRow["TA_Taux"]);
                        NewRow["Taux de TVA"] = taux;
                        cmbCptChargesDT.Rows.Add(NewRow.ItemArray);
                        cmbCptCharges.DataSource = cmbCptChargesDT;

                    }

                }
                adotemp.Dispose();
                //======= ferme la connection sage.
                SAGE.fc_CloseConnSage();

            }

            //======= recherche la fiche appelante et positionne selon le cas sur la bonne fiche fournisseur.

            ModParametre.fc_RecupParam(Variable.Cuserdocfourn);

            if (!(ModParametre.sFicheAppelante == "UserBCmdHead" ||
                ModParametre.sFicheAppelante == "UderBCmdBody" ||
                ModParametre.sFicheAppelante == "UserDocFourn" ||
                ModParametre.sFicheAppelante == "UserDocFactFourn" ||
                ModParametre.sFicheAppelante == "UserReceptMarchandise" ||
                General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserPreCommande2)))
            {

                General.saveInReg("UserBCmdHead", "NoBcmd", "");
                General.saveInReg("UserBCmdHead", "NoFNS", "");

                if (ModParametre.sFicheAppelante == "UserDocFichePrincipal" ||
                    string.IsNullOrEmpty(General.getFrmReg("UserDocFourn", "sFicheAppelante")))
                {
                    General.saveInReg("UserDocFourn", "sFicheAppelante", "");
                    General.saveInReg("UserBCmdHead", "NoInt", "-1");
                    General.saveInReg("UserBCmdHead", "NoImmeuble", "");
                    cmdIntervention.Visible = false;
                    Label22.Enabled = false;
                }
            }

            //== modif rachid ajout de la fiche precommande.
            if (General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserPreCommande2))
            {
                //=== modif du 19 novembre 2007, le code fournisseur doit etre la cléauto
                //== et non le code fournisseur.

                // sNoFNS = GetSetting(cFrNomApp, cUserPreCommande2, "CodeFournisseur", "")
                sNoFNS = General.getFrmReg(Variable.cUserPreCommande2, "CleAutoFourn", "");
                if (!string.IsNullOrEmpty(sNoFNS))
                {
                    if (!General.IsNumeric(sNoFNS))
                    {
                        lCleAutoFourn = 0;
                    }
                    else
                    {
                        lCleAutoFourn = Convert.ToInt32(sNoFNS);
                    }

                }

                Label22.Enabled = false;
                llabel13.Enabled = false;
            }
            else
            {

                sNoFNS = General.nz(General.getFrmReg("UserBCmdHead", "NoFNS"), "").ToString();
            }


            if (General.nz(General.getFrmReg("UserBCmdHead", "NoBcmd"), -1).ToString() != "-1")
            {
                llabel13.Enabled = true;
            }
            nInt = 1;

            //===== affiche le code unique du fournisseur.
            hidCleAuto.Text = sNoFNS;

            //==== alimente le dropdown  de la grille des fournisseurs concernant les
            //==== code de reglements
            //ssDropcodeReglmt.RemoveAll();
            codeRgt(ssDropcodeReglmt);//tested

            // ssDropcodeReglmt.DataFieldList = "column 0";
            //  ssFNS.DisplayLayout.Bands[0].Columns["Fns_Rgt"].ValueList= ssDropcodeReglmt;//hidden


            //===== extrait et affiche les codes de reglements liée au fournisseur.
            //   cmbModeRgt.Items.Clear();
            codeRgt(cmbModeRgt);//tested

            //===== affiche le type de fournisseur( fournisseur ou sous traitant).
            Array.Resize(ref tabTemp, 2);
            tabTemp[0] = "Sous Traitant";
            tabTemp[1] = "Fournisseur";
            GeneralXav.Sub_Type(cmbType, tabTemp);//tested

            //==== affiche le type de fournisseur(sert pour l'ananlytique)==>tested
            sSQLTYFO = "SELECT     TYFO_Code, TYFO_Libelle" + " From TYFO_TypeFourn" + " ORDER BY TYFO_Libelle";
            ModAdo rsTYFOModAdo = new ModAdo();
            rsTYFO = rsTYFOModAdo.fc_OpenRecordSet(sSQLTYFO);
            DataTable cmbTYFODT = new DataTable();
            cmbTYFODT.Columns.Add("Type");
            var row = cmbTYFODT.NewRow();
            xxx = 0;

            if (rsTYFO.Rows.Count > 0)
            {
                foreach (DataRow rsTYFORow in rsTYFO.Rows)
                {
                    row["Type"] = rsTYFORow["TYFO_Libelle"].ToString();
                    cmbTYFODT.Rows.Add(row.ItemArray);
                    xxx = xxx + 1;
                    cmbTYFO.DataSource = cmbTYFODT;

                }

            }
            rsTYFO.Dispose();
            rsTYFO = null;

            //===== Ative ou desactive des objets (boutons etc...)
            fc_BloqueForm();

            //===== affiche les enregistremts liée aux founisseurs.
            SelectFns(lCleAutoFourn);

            //====== ouvre la connection vers la base Gecet.
            ModParametre.fc_OpenConnGecet();

            //====== lie le drop down des comptes de charges dans la grille des comptes de charges
            //====== lié au fournisseur.
            // cmbCptCharges.DataFieldList = "Column 0";



            //====== module read soft
            if (General.sModuleReadSoft != "1")//tested
            {
                lblCapReadSoft.Visible = false;
                cmbCodeReadSoft.Visible = false;
                cmdFournreadSoft.Visible = false;
                cmdAnalytique.Visible = false;
            }

            if (General.sServiceAnalytique == "1")//tested
            {

                lblService.Visible = true;
                cmbService.Visible = true;
                lblLibService.Visible = true;
            }
            else
            {
                lblService.Visible = false;
                cmbService.Visible = false;
                lblLibService.Visible = false;
            }

            ModAutorisation.fc_DroitParFiche(this.Name);
        }

        private void txtCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string strCodeFournisseur = null;
            if (KeyAscii == 13 & bAjout == false)
            {
                // sSQL = "SELECT FournisseurArticle.Code FROM FournisseurArticle " _
                //& " Where fournisseurArticle.Code= '" & gFr_DoublerQuote(txtCode) & "'"

                //strCodeFournisseur = nz(fc_ADOlibelle(sSQL), "")

                // If strCodeFournisseur <> "" Then
                //     SelectFns strCodeFournisseur
                //  Else

                fc_Rechercher();
                //  End If

            }
            e.KeyChar = (char)(KeyAscii);
            if (KeyAscii == 0)
            {
                e.Handled = true;
            }
        }

        private void txtCode_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            string sSQL = "";
            string sCode = "";
            int i = 0;

            sSQL = "SELECT     COUNT(Code) AS Expr1 FROM         fournisseurArticle"
                + " WHERE code ='" + StdSQLchaine.gFr_DoublerQuote(txtCode.Text) + "'";

            var tmpAdo = new ModAdo();
            i = Convert.ToInt16(General.nz(tmpAdo.fc_ADOlibelle(sSQL), 0));

            if (i > 0)
            {
                //=== avertissement mode ajout et fournisseur existant.
                if (bAjout == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fournisseur déjà existant.", "Fournisseur existant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                //SelectFns sSQL

                if (i > 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il existe plusieurs fournisseurs avec le code " + txtCode.Text + "," + " sélectionner en un dans la liste.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //RechercheMultiCritere.Champs0.Text = txtCode.Text + "*";
                    fc_Rechercher();
                }
                else
                {
                    sSQL = tmpAdo.fc_ADOlibelle("SELECT CleAuto FROM fournisseurArticle WHERE code ='" + txtCode.Text + "'");
                    SelectFns(Convert.ToInt32(sSQL));

                }
            }
            else if (i == 0 & !string.IsNullOrEmpty(txtCode.Text) & bAjout == false)
            {
                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fournisseur n'existe pas, voulez vous le créer ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dg == DialogResult.Yes)
                {
                    sCode = txtCode.Text;
                    cmdAjouter_Click(cmdAjouter, new System.EventArgs());
                    txtCode.Text = sCode;
                }
                else
                {
                    clear();
                }
            }

            e.Cancel = Cancel;
        }

        private void GridCptCharges_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            GridCptCharges.DisplayLayout.Bands[0].Columns["CG_Num"].ValueList = cmbCptCharges;
            GridCptCharges.DisplayLayout.Bands[0].Columns["CG_Num"].Header.Caption = "Compte de charge";
            GridCptCharges.DisplayLayout.Bands[0].Columns["Intitule"].Header.Caption = "Designation";
            GridCptCharges.DisplayLayout.Bands[0].Columns["TA_Taux"].Hidden = true;
            GridCptCharges.DisplayLayout.Bands[0].Columns["TaxeCG_Num"].Header.Caption = "Compte TVA associé";
            GridCptCharges.DisplayLayout.Bands[0].Columns["CleAutoFournisseur"].Hidden = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCptCharges_CellChange(object sender, CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.ToUpper() == "CG_NUM".ToUpper())
                {
                    var row = cmbCptCharges.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                    if (row != null)
                    {
                        GridCptCharges.ActiveRow.Cells["Intitule"].Value = row.Cells[1].Value;
                        GridCptCharges.ActiveRow.Cells["TaxeCG_Num"].Value = row.Cells[2].Value;
                        GridCptCharges.ActiveRow.Cells["TA_Taux"].Value = row.Cells[3].Value;
                        GridCptCharges.ActiveRow.Cells["CleAutoFournisseur"].Value = Convert.ToInt32(General.nz(hidCleAuto.Text, 0));
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridCptCharges_CellChange");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCptCharges_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CleAutoFournisseur"].Value.ToString()))//tested
            {
                e.Row.Cells["CleAutoFournisseur"].Value = General.nz(hidCleAuto.Text, 0);
            }
            if (e.Row.Cells["CG_NUM"].DataChanged)//tested
            {
                if (e.Row.Cells["CG_NUM"].OriginalValue.ToString() != e.Row.Cells["CG_NUM"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM CpteChargeFournisseur WHERE CG_NUM= '{e.Row.Cells["CG_NUM"].Text}' and CleAutoFournisseur='{e.Row.Cells["CleAutoFournisseur"].Text}'  "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCptCharges_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsCpteChargesFournModado.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCptCharges_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsCpteChargesFournModado.Update();
        }
        private void ssFNS_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Agence"].Header.Caption = "Agence";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Contact"].Header.Caption = "Contact";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Qualite"].Hidden = true;
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Adresse"].Header.Caption = "Adresse";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Codepostal"].Header.Caption = "CP";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Ville"].Header.Caption = "Ville";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Tel"].Header.Caption = "Tel";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Portable"].Header.Caption = "Portable";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Fax"].Header.Caption = "Fax";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Email"].Header.Caption = "Email";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_HDM"].Header.Caption = "H.OuvMatin";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_HFM"].Header.Caption = "H.FermMatin";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_HDA"].Header.Caption = "H.OuvAM";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_HFA"].Header.Caption = "H.FermAP";
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_No"].Hidden = true;
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Cle"].Hidden = true;
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Rgt"].Hidden = true;
            ssFNS.DisplayLayout.Bands[0].Columns["Fns_Cpt"].Hidden = true;
        }
        private void ssFNS_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dg == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        private void ssFNS_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsFNSModado.Update();
        }

        private void ssFNS_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsFNSModado.Update();
        }



        private void ssFNS_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCptCharges_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {
            if (txtCode.Text == "")
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez d'abord sélectionner un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            return;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFNS_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {
            if (txtCode.Text == "")
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez d'abord sélectionner un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            return;
        }

        private void cmbService_BeforeDropDownResizeStart(object sender, EventArgs e)
        {

        }

        private void cmbService_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Intitulé"].Width = 200;
        }

        private void tableLayoutPanel7_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmbTYFO_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns[0].Width = 300;
        }

        private void ssFNS_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["Fns_Agence"].Text.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne Agence.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(e.Row.Cells["fns_Adresse"].Text.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne adresse.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                //  e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(e.Row.Cells["fns_ville"].Text.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne ville.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                // e.Cancel = true;
            }
            else if (string.IsNullOrEmpty(e.Row.Cells["fns_tel"].Text.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Renseignez la colonne téléphone.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                // e.Cancel = true;
            }
            if (string.IsNullOrEmpty(e.Row.Cells["FNS_cle"].Text))
            {
                e.Row.Cells["FNS_cle"].Value = hidCleAuto.Text;
            }
        }
    }

}
