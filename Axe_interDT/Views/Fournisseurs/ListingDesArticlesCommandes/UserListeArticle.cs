﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;
using Axe_interDT.Shared;
using Microsoft.VisualBasic;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.Fournisseurs.ListingDesArticlesCommandes
{
    public partial class UserListeArticle : UserControl
    {
        public UserListeArticle()
        {
            InitializeComponent();
        }
        string sSQL;
        DataTable rsListArt;


        private void cmdExportExcel_Click(Object eventSender, EventArgs eventArgs)
        {
            Excel.Application oXL;

            Excel.Workbook oWB = default(Excel.Workbook);
            Excel.Worksheet oSheet = default(Excel.Worksheet);
            Excel.Range oRng = default(Excel.Range);
            Excel.Range oResizeRange = default(Excel.Range);
            DataTable rsExport = default(DataTable);
            // ADODB.Field oField = default(ADODB.Field);

            int i = 0;
            int j = 0;
            string sMatricule = null;
            int lLigne = 0;

            //try
            //{
            Cursor.Current = Cursors.WaitCursor;
            // Start Excel and get Application object.

            oXL = new Excel.Application();
            oXL.Visible = false;
            // Get a new workbook.

            ModAdo rs = new ModAdo();
            oWB = oXL.Workbooks.Add();
            oSheet = oWB.ActiveSheet;

            rsExport = ssListeAricle.DataSource as DataTable;


            if (rsExport.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            //== affiche les noms des champs.

            i = 1;
            foreach (DataColumn oField in rsExport.Columns)
            {
                oSheet.Cells[1, i].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                oSheet.Cells[1, i].value = oField.ColumnName;
                i = i + 1;
            }
            Cursor.Current = Cursors.WaitCursor;
            // Starting at E1, fill headers for the number of columns selected.
            oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
            oResizeRange.Interior.ColorIndex = 15;
            //formate la ligne en gras.
            oResizeRange.Font.Bold = true;
            oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
            oResizeRange.Borders.Weight = Excel.XlBorderWeight.xlThin;
            lLigne = 3;

            Cursor.Current = Cursors.WaitCursor;

            foreach (DataRow rsExportRows in rsExport.Rows)
            {
                j = 1;
                //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[lLigne, j + 0].value = rsExportRows[oField.ColumnName];
                    j = j + 1;
                }
                lLigne = lLigne + 1;
            }
            ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
            //formate la ligne en gras.

            oResizeRange.Font.Bold = true;

            // Starting at E1, fill headers for the number of columns selected.


            oXL.Visible = true;
            oXL.UserControl = true;
            oRng = null;
            oSheet = null;
            oWB = null;
            //oXL.Quit();
            oXL = null;

            Cursor.Current = Cursors.Arrow;
            //_with14.Close();
            rsExport = null;
            return;

            //////////////////////////////////////// 

            ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
            //formate la ligne en gras.
            oResizeRange.Font.Bold = true;

            oXL.Visible = true;
            oXL.UserControl = true;
            oRng = null;
            oSheet = null;
            oWB = null;
            oXL = null;
            Cursor.Current = Cursors.Arrow;
            rsExport = null;

            // MsgBox "Expotation terminée", vbInformation, "Opération réussie"

            Cursor.Current = Cursors.Arrow;
            return;
            //}
            //catch (Exception ex)
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: ", "" + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            //}
        }
        private void UserListeArticle_VisibleChanged(object sender, EventArgs e)
        {

            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserListeArticle");
            General.open_conn();
        }
        private void cmdDelD_Click(Object eventSender, EventArgs eventArgs)
        {
            string sWhere = null;
            string sOrder = null;
            string[] tTAb = null;
            int i = 0;
            int X = 0;
            int o = 0;
            int p = 0;
            string sT = null;
            string st2 = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {

                sSQL = "SELECT fournisseurArticle.Raisonsocial, BonDeCommande.CodeFourn,"
                    + " fournisseurArticle.Code, " + " BonDeCommande.DateCommande, "
                    + " BonDeCommande.DateLivraison, BCD_Detail.BCD_References,"
                    + " BCD_Detail.BCD_Designation, BCD_Detail.BCD_Quantite,"
                    + " BCD_Detail.BCD_PrixHT, "
                    + " BonDeCommande.NoBonDeCommande,Intervention.CodeImmeuble,Intervention.numfichestandard "
                    + " FROM BonDeCommande INNER JOIN" + " fournisseurArticle "
                    + " ON BonDeCommande.CodeFourn = fournisseurArticle.Cleauto "
                    + " INNER JOIN Intervention"
                    + " ON BonDeCommande.NoIntervention = Intervention.NoIntervention"
                    + " INNER JOIN BCD_Detail " + " ON BonDeCommande.NoBonDeCommande = BCD_Detail.BCD_Cle";

                sWhere = " Where ";

                if (!string.IsNullOrEmpty(txtDesignation.Text))
                {
                    //        If InStr(UCase(txtDesignation), " OU ") Then
                    //            x = 0
                    //            i = 0
                    //            o = 1
                    //            P = 0
                    //            Do
                    //
                    //                    i = InStr(o, UCase(txtDesignation), " OU ")
                    //                    sT = Mid(txtDesignation, o, Abs((i - 1) - P))
                    //                    o = i + 4
                    //                    If InStr(o, sT, " OU ") Then
                    //
                    //                    Else
                    //                        P = P + Len(sT)
                    //                    End If
                    //                    ReDim Preserve tTab(x)
                    //                    tTab(x) = Trim(sT)
                    //                    x = x + 1
                    //
                    //
                    //            Loop
                    //        Else
                    sWhere = sWhere + " BCD_Detail.BCD_Designation like '"
                        + StdSQLchaine.gFr_DoublerQuote(fc_Decompose(txtDesignation.Text)) + "%'  And   ";
                    //  End If
                }

                if (!string.IsNullOrEmpty(txtNumFicheStandardDu.Text))
                {
                    sWhere = sWhere + " Intervention.NumFicheStandard >= "
                        + General.nz(txtNumFicheStandardDu, 0) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txtNumFicheStandardAu.Text))
                {
                    sWhere = sWhere + " Intervention.NumFicheStandard <= "
                        + General.nz(txtNumFicheStandardAu, 0) + "  And   ";
                }

                if (!string.IsNullOrEmpty(txtNoBonDeCommandeDu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoBonDeCommande >= "
                        + Convert.ToDouble(txtNoBonDeCommandeDu.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txtNoBonDeCommandeAu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoBonDeCommande <= "
                        + Convert.ToDouble(txtNoBonDeCommandeAu.Text) + "  And   ";
                }

                if (!string.IsNullOrEmpty(txtNoInterventionDu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoIntervention >= "
                        + Convert.ToDouble(txtNoInterventionDu.Text) + "  And   ";
                }
                if (!string.IsNullOrEmpty(txtNoInterventionAu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.NoIntervention <= "
                        + Convert.ToDouble(txtNoInterventionAu.Text) + "  And   ";
                }

                if (!string.IsNullOrEmpty(txtcodeFourn.Text))
                {
                    sWhere = sWhere + " FournisseurArticle.code='"
                        + StdSQLchaine.gFr_DoublerQuote(txtcodeFourn.Text) + "'  And   ";
                }

                if (!string.IsNullOrEmpty(txtDateCommandeDu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateCommande >= '"
                        + string.Format(txtDateCommandeDu.Text, "dd/mm/yy HH:mm:ss") + "' And   ";
                }
                if (!string.IsNullOrEmpty(txtDateCommandeAu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateCommande <= '"
                        + string.Format(txtDateCommandeAu.Text + " 23:59:59", "dd/MM/yy HH:mm:ss") + "' And   ";
                }

                if (!string.IsNullOrEmpty(txtDateLivraisonDu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateLivraison >= '"
                        + string.Format(txtDateLivraisonDu.Text, "dd/MM/yy HH:mm:ss") + "' And  ";
                }
                if (!string.IsNullOrEmpty(txtDateLivraisonAu.Text))
                {
                    sWhere = sWhere + " BonDeCommande.DateLivraison <= '"
                        + string.Format(txtDateLivraisonAu.Text + " 23:59:59", "dd/MM/yy HH:mm:ss") + "' And   ";
                }

                if (!string.IsNullOrEmpty(txtSignataire.Text))
                {
                    sWhere = sWhere + " BonDeCommande.Signataire='"
                        + StdSQLchaine.gFr_DoublerQuote(txtSignataire.Text) + "' And   ";
                }

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    sWhere = sWhere + "BonDeCommande.Codeimmeuble='"
                        + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' And   ";
                }

                if (!string.IsNullOrEmpty(txtTechnicien.Text))
                {
                    sWhere = sWhere + " technicien = '"
                        + StdSQLchaine.gFr_DoublerQuote(txtTechnicien.Text) + "' And   ";
                }

                sWhere = Strings.Left(sWhere, Strings.Len(sWhere) - 7);
                sOrder = " Order by NoBonDeCommande, BCD_NoLigne";

                sSQL = sSQL + sWhere;
                ModAdo ModAdo1 = new ModAdo();
                rsListArt = ModAdo1.fc_OpenRecordSet(sSQL);
                //ssListeAricle.ReBind();
                //ssListeAricle.Rows = rsListArt.RecordCount;
                ssListeAricle.DataSource = rsListArt;
                lblTotal.Text = "Total des articles :" + rsListArt.Rows.Count;

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";cmdDelD_Click");
            }
        }
        public string fc_Decompose(string sWord)
        {
            sWord = Strings.Replace(Strings.UCase(sWord), " ET ", "%");
            sWord = Strings.Replace(Strings.UCase(sWord), " OU ", "%");

            sWord = Strings.Replace(sWord, "*", "%");
            sWord = Strings.Replace(sWord, " ", "%");


            return sWord;

        }
        private void cmdRechercheFourn_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCode = null;
            double nCode = 0;

            //try
            //{
            string requete = "";
            string where_order = "";



            //.stxt = "SELECT  Code as [Code], Raisonsocial as [RaisonSociale], Ville as [Ville] , FNS_Agences.FNS_No As [NoIntAgence], FNS_Agences.FNS_Ville as [Agence Ville], FNS_Agences.FNS_Agence As [Agence] FROM fournisseurArticle Left Join FNS_Agences On FournisseurArticle.Cleauto = FNS_Agences.FNS_cle"
            requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\"  FROM fournisseurArticle ";
            where_order =/*WHERE*/ "(code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 ) ";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Filtrer vos bon de commandes par fournisseur" };
            //.Move CInt(Frame9.Width + Frame9.Left) + 200, 1400
            fg.ugResultat.DoubleClickRow += (se, ev) =>
                {



                    // charge les enregistrements de l'immeuble
                    txtcodeFourn.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblFournisseur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    // Text1.Text = .dbgrid1.Columns(3).value

                    fg.Dispose();
                    fg.Close();
                };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();

            return;
            //}
            //catch (Exception e)
            //{
            //    Erreurs.gFr_debug(e, this.Name + ";cmdRechercheFourn_Click");
            //    return;
            //}



        }

        private void cmdRechercheIm_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCode = null;
            try
            {
                string requete = "";
                string where = "";

                //  RechercheMultiCritere.Width = 12950
                //RechercheMultiCritere.Height = CInt(Line21(0).Y1) + 6000
                requete = "SELECT CodeImmeuble as \"CodeImmeuble \", AngleRue as \"AngleRue\", Adresse as \"Adresse\", Ville as \"Ville\", CodePostal as \"CodePostal\" From Immeuble";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Filtrer vos bon de commandes par immeuble" };

                //.Move CInt(Frame9.Width + Frame9.Left) + 200, 1400
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    sCode = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                    txtCodeImmeuble.Text = sCode;

                    fg.Dispose();
                    fg.Close();
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
                return;
            }


        }

        private void cmdSignataire_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCode = null;
            try
            {
                string requete = "";
                string where = "";


                //  RechercheMultiCritere.Width = 12950
                //RechercheMultiCritere.Height = CInt(Line21(0).Y1) + 6000
                requete = "Select USR_Nt \"Login\" ,USR_Name \"Nom\" from USR_Users";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Filtrer vos bon de commandes par immeuble" };

                //.Move CInt(Frame9.Width + Frame9.Left) + 200, 1400      


                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    txtSignataire.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblSignataire.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";cmdSignataire_Click");
                return;
            }

        }

        private void Command1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCode = null;
            double nCode = 0;

            try
            {
                string requete = "";
                string where = "";


                //  RechercheMultiCritere.Width = 12950
                //RechercheMultiCritere.Height = CInt(Line21(0).Y1) + 6000
                requete = "SELECT  matricule as \"Matricule\", Nom as \"Nom\"," + " Prenom as \"Ville\" "
                    + " FROM personnel ";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Filtrer vos bon de commandes par fournisseur" };


                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtTechnicien.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
                return;
            }

        }
        //not exsist
        //private void Label7_Click()
        //{
        //    fc_clean();
        //}

        private void lblClean_Click(Object eventSender, EventArgs eventArgs)
        {
            fc_clean(this);

        }
        private void fc_clean(Control c)
        {
            foreach (Control cn in c.Controls)
            {
                fc_clean(cn);
                if (c is iTalk.iTalk_TextBox_Small2)
                    c.Text = "";
            }
            lblIntervenant.Text = "";
            lblFournisseur.Text = "";
            lblSignataire.Text = "";
        }

        private void ssListeAricle_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssListeAricle.EventManager.AllEventsEnabled = false;
            ssListeAricle.DisplayLayout.Bands[0].Columns["CodeFourn"].Hidden = true;
            ssListeAricle.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;
            ssListeAricle.DisplayLayout.Bands[0].Columns["BCD_References"].Header.Caption = "Réferences";
            ssListeAricle.DisplayLayout.Bands[0].Columns["BCD_Designation"].Header.Caption = "Designation";
            ssListeAricle.DisplayLayout.Bands[0].Columns["BCD_Designation"].Width = 300;
            ssListeAricle.DisplayLayout.Bands[0].Columns["BCD_Quantite"].Header.Caption = "Qte";
            ssListeAricle.DisplayLayout.Bands[0].Columns["BCD_PrixHt"].Header.Caption = "PrixHt";
            ssListeAricle.DisplayLayout.Bands[0].Columns["numfichestandard"].Header.Caption = "Affaire/Appel";
            ssListeAricle.EventManager.AllEventsEnabled = true;

        }

        private void txtDateCommandeDu_Validating(object sender, CancelEventArgs e)
        {
            if(!string.IsNullOrEmpty(txtDateCommandeDu.Text) && !General.IsDate(txtDateCommandeDu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide ", "" , MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateCommandeDu.Text = "";
                txtDateCommandeDu.Focus();
                return;
            }
        }

        private void txtDateCommandeAu_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateCommandeAu.Text) && !General.IsDate(txtDateCommandeAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateCommandeAu.Text = "";
                txtDateCommandeAu.Focus();
                return;
            }

        }

        private void txtDateLivraisonDu_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateLivraisonDu.Text) && !General.IsDate(txtDateLivraisonDu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateLivraisonDu.Text = "";
                txtDateLivraisonDu.Focus();
                return;
            }
        }

        private void txtDateLivraisonAu_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateLivraisonAu.Text) && !General.IsDate(txtDateLivraisonAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateLivraisonAu.Text = "";
                txtDateLivraisonAu.Focus();
                return;
            }
        }
    }
}
