namespace Axe_interDT.Views.Fournisseurs.ListingDesArticlesCommandes
{
    partial class UserListeArticle
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdDelD = new System.Windows.Forms.Button();
            this.lblClean = new System.Windows.Forms.Button();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.ssListeAricle = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheIm = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdRechercheFourn = new System.Windows.Forms.Button();
            this.txtcodeFourn = new iTalk.iTalk_TextBox_Small2();
            this.txtTechnicien = new iTalk.iTalk_TextBox_Small2();
            this.txtDesignation = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.lblSignataire = new System.Windows.Forms.Label();
            this.lbl104 = new System.Windows.Forms.Label();
            this.lbl100 = new System.Windows.Forms.Label();
            this.lbl106 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lbl101 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbl00 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbl11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNumFicheStandardDu = new iTalk.iTalk_TextBox_Small2();
            this.txtNumFicheStandardAu = new iTalk.iTalk_TextBox_Small2();
            this.txtDateCommandeDu = new iTalk.iTalk_TextBox_Small2();
            this.txtDateCommandeAu = new iTalk.iTalk_TextBox_Small2();
            this.txtNoBonDeCommandeDu = new iTalk.iTalk_TextBox_Small2();
            this.txtNoInterventionDu = new iTalk.iTalk_TextBox_Small2();
            this.txtNoBonDeCommandeAu = new iTalk.iTalk_TextBox_Small2();
            this.txtNoInterventionAu = new iTalk.iTalk_TextBox_Small2();
            this.txtDateLivraisonDu = new iTalk.iTalk_TextBox_Small2();
            this.txtSignataire = new iTalk.iTalk_TextBox_Small2();
            this.txtDateLivraisonAu = new iTalk.iTalk_TextBox_Small2();
            this.cmdSignataire = new System.Windows.Forms.Button();
            this.lbl01 = new System.Windows.Forms.Label();
            this.lbl105 = new System.Windows.Forms.Label();
            this.lblFournisseur = new System.Windows.Forms.Label();
            this.lblIntervenant = new System.Windows.Forms.Label();
            this.lbl108 = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssListeAricle)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ssListeAricle, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdDelD);
            this.flowLayoutPanel1.Controls.Add(this.lblClean);
            this.flowLayoutPanel1.Controls.Add(this.cmdExportExcel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1652, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(195, 39);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // cmdDelD
            // 
            this.cmdDelD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDelD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDelD.FlatAppearance.BorderSize = 0;
            this.cmdDelD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDelD.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDelD.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdDelD.Location = new System.Drawing.Point(2, 2);
            this.cmdDelD.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDelD.Name = "cmdDelD";
            this.cmdDelD.Size = new System.Drawing.Size(60, 35);
            this.cmdDelD.TabIndex = 382;
            this.cmdDelD.Tag = "";
            this.cmdDelD.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdDelD, "Rechercher");
            this.cmdDelD.UseVisualStyleBackColor = false;
            this.cmdDelD.Click += new System.EventHandler(this.cmdDelD_Click);
            // 
            // lblClean
            // 
            this.lblClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.lblClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblClean.FlatAppearance.BorderSize = 0;
            this.lblClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblClean.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblClean.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.lblClean.Location = new System.Drawing.Point(66, 2);
            this.lblClean.Margin = new System.Windows.Forms.Padding(2);
            this.lblClean.Name = "lblClean";
            this.lblClean.Size = new System.Drawing.Size(60, 35);
            this.lblClean.TabIndex = 383;
            this.lblClean.Tag = "";
            this.lblClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.lblClean, "Clean");
            this.lblClean.UseVisualStyleBackColor = false;
            this.lblClean.Click += new System.EventHandler(this.lblClean_Click);
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExportExcel.Location = new System.Drawing.Point(130, 2);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(60, 35);
            this.cmdExportExcel.TabIndex = 384;
            this.cmdExportExcel.Tag = "";
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExportExcel, "Exporter Excel");
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // ssListeAricle
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssListeAricle.DisplayLayout.Appearance = appearance1;
            this.ssListeAricle.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssListeAricle.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssListeAricle.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssListeAricle.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssListeAricle.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssListeAricle.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssListeAricle.DisplayLayout.MaxColScrollRegions = 1;
            this.ssListeAricle.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssListeAricle.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssListeAricle.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssListeAricle.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssListeAricle.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssListeAricle.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssListeAricle.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssListeAricle.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssListeAricle.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssListeAricle.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssListeAricle.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssListeAricle.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssListeAricle.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssListeAricle.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssListeAricle.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssListeAricle.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssListeAricle.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssListeAricle.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssListeAricle.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssListeAricle.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssListeAricle.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssListeAricle.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssListeAricle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssListeAricle.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssListeAricle.Location = new System.Drawing.Point(3, 238);
            this.ssListeAricle.Name = "ssListeAricle";
            this.ssListeAricle.Size = new System.Drawing.Size(1844, 716);
            this.ssListeAricle.TabIndex = 412;
            this.ssListeAricle.Text = "ultraGrid1";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 8;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheIm, 6, 3);
            this.tableLayoutPanel2.Controls.Add(this.Command1, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheFourn, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtcodeFourn, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtTechnicien, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtDesignation, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtCodeImmeuble, 5, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblSignataire, 7, 2);
            this.tableLayoutPanel2.Controls.Add(this.lbl104, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.lbl100, 4, 4);
            this.tableLayoutPanel2.Controls.Add(this.lbl106, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.label9, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.lbl101, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label7, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.label5, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbl00, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.lbl11, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNumFicheStandardDu, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNumFicheStandardAu, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCommandeDu, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateCommandeAu, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNoBonDeCommandeDu, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtNoInterventionDu, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtNoBonDeCommandeAu, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtNoInterventionAu, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtDateLivraisonDu, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtSignataire, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtDateLivraisonAu, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdSignataire, 6, 2);
            this.tableLayoutPanel2.Controls.Add(this.lbl01, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbl105, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.lblFournisseur, 3, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblIntervenant, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.lbl108, 0, 4);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 48);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 151);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // cmdRechercheIm
            // 
            this.cmdRechercheIm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIm.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIm.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheIm.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheIm.Location = new System.Drawing.Point(1455, 93);
            this.cmdRechercheIm.Name = "cmdRechercheIm";
            this.cmdRechercheIm.Size = new System.Drawing.Size(25, 18);
            this.cmdRechercheIm.TabIndex = 532;
            this.cmdRechercheIm.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheIm, "Recherche de l\'immeuble");
            this.cmdRechercheIm.UseVisualStyleBackColor = false;
            this.cmdRechercheIm.Click += new System.EventHandler(this.cmdRechercheIm_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command1.Location = new System.Drawing.Point(565, 123);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(23, 18);
            this.Command1.TabIndex = 531;
            this.Command1.Tag = "";
            this.toolTip1.SetToolTip(this.Command1, "Recherche des intervenants");
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdRechercheFourn
            // 
            this.cmdRechercheFourn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheFourn.FlatAppearance.BorderSize = 0;
            this.cmdRechercheFourn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheFourn.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheFourn.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheFourn.Location = new System.Drawing.Point(565, 93);
            this.cmdRechercheFourn.Name = "cmdRechercheFourn";
            this.cmdRechercheFourn.Size = new System.Drawing.Size(23, 18);
            this.cmdRechercheFourn.TabIndex = 530;
            this.cmdRechercheFourn.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheFourn, "Recherche des fournisseurs");
            this.cmdRechercheFourn.UseVisualStyleBackColor = false;
            this.cmdRechercheFourn.Click += new System.EventHandler(this.cmdRechercheFourn_Click);
            // 
            // txtcodeFourn
            // 
            this.txtcodeFourn.AccAcceptNumbersOnly = false;
            this.txtcodeFourn.AccAllowComma = false;
            this.txtcodeFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcodeFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcodeFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcodeFourn.AccHidenValue = "";
            this.txtcodeFourn.AccNotAllowedChars = null;
            this.txtcodeFourn.AccReadOnly = false;
            this.txtcodeFourn.AccReadOnlyAllowDelete = false;
            this.txtcodeFourn.AccRequired = false;
            this.txtcodeFourn.BackColor = System.Drawing.Color.White;
            this.txtcodeFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtcodeFourn.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcodeFourn.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtcodeFourn.ForeColor = System.Drawing.Color.Black;
            this.txtcodeFourn.Location = new System.Drawing.Point(218, 92);
            this.txtcodeFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtcodeFourn.MaxLength = 32767;
            this.txtcodeFourn.Multiline = false;
            this.txtcodeFourn.Name = "txtcodeFourn";
            this.txtcodeFourn.ReadOnly = false;
            this.txtcodeFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcodeFourn.Size = new System.Drawing.Size(342, 27);
            this.txtcodeFourn.TabIndex = 6;
            this.txtcodeFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcodeFourn.UseSystemPasswordChar = false;
            // 
            // txtTechnicien
            // 
            this.txtTechnicien.AccAcceptNumbersOnly = false;
            this.txtTechnicien.AccAllowComma = false;
            this.txtTechnicien.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTechnicien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTechnicien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTechnicien.AccHidenValue = "";
            this.txtTechnicien.AccNotAllowedChars = null;
            this.txtTechnicien.AccReadOnly = false;
            this.txtTechnicien.AccReadOnlyAllowDelete = false;
            this.txtTechnicien.AccRequired = false;
            this.txtTechnicien.BackColor = System.Drawing.Color.White;
            this.txtTechnicien.CustomBackColor = System.Drawing.Color.White;
            this.txtTechnicien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTechnicien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTechnicien.ForeColor = System.Drawing.Color.Black;
            this.txtTechnicien.Location = new System.Drawing.Point(218, 122);
            this.txtTechnicien.Margin = new System.Windows.Forms.Padding(2);
            this.txtTechnicien.MaxLength = 32767;
            this.txtTechnicien.Multiline = false;
            this.txtTechnicien.Name = "txtTechnicien";
            this.txtTechnicien.ReadOnly = false;
            this.txtTechnicien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTechnicien.Size = new System.Drawing.Size(342, 27);
            this.txtTechnicien.TabIndex = 7;
            this.txtTechnicien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTechnicien.UseSystemPasswordChar = false;
            // 
            // txtDesignation
            // 
            this.txtDesignation.AccAcceptNumbersOnly = false;
            this.txtDesignation.AccAllowComma = false;
            this.txtDesignation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDesignation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDesignation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDesignation.AccHidenValue = "";
            this.txtDesignation.AccNotAllowedChars = null;
            this.txtDesignation.AccReadOnly = false;
            this.txtDesignation.AccReadOnlyAllowDelete = false;
            this.txtDesignation.AccRequired = false;
            this.txtDesignation.BackColor = System.Drawing.Color.White;
            this.txtDesignation.CustomBackColor = System.Drawing.Color.White;
            this.txtDesignation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDesignation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDesignation.ForeColor = System.Drawing.Color.Black;
            this.txtDesignation.Location = new System.Drawing.Point(1108, 122);
            this.txtDesignation.Margin = new System.Windows.Forms.Padding(2);
            this.txtDesignation.MaxLength = 32767;
            this.txtDesignation.Multiline = false;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.ReadOnly = false;
            this.txtDesignation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDesignation.Size = new System.Drawing.Size(342, 27);
            this.txtDesignation.TabIndex = 14;
            this.txtDesignation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDesignation.UseSystemPasswordChar = false;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(1108, 92);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(342, 27);
            this.txtCodeImmeuble.TabIndex = 13;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // lblSignataire
            // 
            this.lblSignataire.AutoSize = true;
            this.lblSignataire.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSignataire.Location = new System.Drawing.Point(1501, 60);
            this.lblSignataire.Name = "lblSignataire";
            this.lblSignataire.Size = new System.Drawing.Size(0, 19);
            this.lblSignataire.TabIndex = 534;
            // 
            // lbl104
            // 
            this.lbl104.AutoSize = true;
            this.lbl104.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl104.Location = new System.Drawing.Point(3, 90);
            this.lbl104.Name = "lbl104";
            this.lbl104.Size = new System.Drawing.Size(91, 19);
            this.lbl104.TabIndex = 522;
            this.lbl104.Text = "Fournisseur";
            // 
            // lbl100
            // 
            this.lbl100.AutoSize = true;
            this.lbl100.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl100.Location = new System.Drawing.Point(947, 120);
            this.lbl100.Name = "lbl100";
            this.lbl100.Size = new System.Drawing.Size(92, 19);
            this.lbl100.TabIndex = 524;
            this.lbl100.Text = "Designation";
            // 
            // lbl106
            // 
            this.lbl106.AutoSize = true;
            this.lbl106.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl106.Location = new System.Drawing.Point(947, 90);
            this.lbl106.Name = "lbl106";
            this.lbl106.Size = new System.Drawing.Size(78, 19);
            this.lbl106.TabIndex = 523;
            this.lbl106.Text = "Immeuble";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(565, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 30);
            this.label9.TabIndex = 522;
            this.label9.Text = "Au ";
            // 
            // lbl101
            // 
            this.lbl101.AutoSize = true;
            this.lbl101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl101.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl101.Location = new System.Drawing.Point(3, 60);
            this.lbl101.Name = "lbl101";
            this.lbl101.Size = new System.Drawing.Size(210, 30);
            this.lbl101.TabIndex = 521;
            this.lbl101.Text = "Du no d\'intervention";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1455, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 30);
            this.label7.TabIndex = 520;
            this.label7.Text = "Et le";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(565, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 30);
            this.label5.TabIndex = 518;
            this.label5.Text = "Au ";
            // 
            // lbl00
            // 
            this.lbl00.AutoSize = true;
            this.lbl00.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl00.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl00.Location = new System.Drawing.Point(3, 30);
            this.lbl00.Name = "lbl00";
            this.lbl00.Size = new System.Drawing.Size(210, 30);
            this.lbl00.TabIndex = 517;
            this.lbl00.Text = "Du no de bon de commande :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1455, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(40, 30);
            this.label3.TabIndex = 508;
            this.label3.Text = "Et le";
            // 
            // lbl11
            // 
            this.lbl11.AutoSize = true;
            this.lbl11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl11.Location = new System.Drawing.Point(947, 0);
            this.lbl11.Name = "lbl11";
            this.lbl11.Size = new System.Drawing.Size(156, 30);
            this.lbl11.TabIndex = 507;
            this.lbl11.Text = "Commandée entre le";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(565, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 30);
            this.label1.TabIndex = 506;
            this.label1.Text = "Au ";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(210, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Du no fiche d\'appel/affaire";
            // 
            // txtNumFicheStandardDu
            // 
            this.txtNumFicheStandardDu.AccAcceptNumbersOnly = false;
            this.txtNumFicheStandardDu.AccAllowComma = false;
            this.txtNumFicheStandardDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumFicheStandardDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumFicheStandardDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumFicheStandardDu.AccHidenValue = "";
            this.txtNumFicheStandardDu.AccNotAllowedChars = null;
            this.txtNumFicheStandardDu.AccReadOnly = false;
            this.txtNumFicheStandardDu.AccReadOnlyAllowDelete = false;
            this.txtNumFicheStandardDu.AccRequired = false;
            this.txtNumFicheStandardDu.BackColor = System.Drawing.Color.White;
            this.txtNumFicheStandardDu.CustomBackColor = System.Drawing.Color.White;
            this.txtNumFicheStandardDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumFicheStandardDu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNumFicheStandardDu.ForeColor = System.Drawing.Color.Black;
            this.txtNumFicheStandardDu.Location = new System.Drawing.Point(218, 2);
            this.txtNumFicheStandardDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumFicheStandardDu.MaxLength = 32767;
            this.txtNumFicheStandardDu.Multiline = false;
            this.txtNumFicheStandardDu.Name = "txtNumFicheStandardDu";
            this.txtNumFicheStandardDu.ReadOnly = false;
            this.txtNumFicheStandardDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumFicheStandardDu.Size = new System.Drawing.Size(342, 27);
            this.txtNumFicheStandardDu.TabIndex = 0;
            this.txtNumFicheStandardDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumFicheStandardDu.UseSystemPasswordChar = false;
            // 
            // txtNumFicheStandardAu
            // 
            this.txtNumFicheStandardAu.AccAcceptNumbersOnly = false;
            this.txtNumFicheStandardAu.AccAllowComma = false;
            this.txtNumFicheStandardAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumFicheStandardAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumFicheStandardAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumFicheStandardAu.AccHidenValue = "";
            this.txtNumFicheStandardAu.AccNotAllowedChars = null;
            this.txtNumFicheStandardAu.AccReadOnly = false;
            this.txtNumFicheStandardAu.AccReadOnlyAllowDelete = false;
            this.txtNumFicheStandardAu.AccRequired = false;
            this.txtNumFicheStandardAu.BackColor = System.Drawing.Color.White;
            this.txtNumFicheStandardAu.CustomBackColor = System.Drawing.Color.White;
            this.txtNumFicheStandardAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumFicheStandardAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNumFicheStandardAu.ForeColor = System.Drawing.Color.Black;
            this.txtNumFicheStandardAu.Location = new System.Drawing.Point(600, 2);
            this.txtNumFicheStandardAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumFicheStandardAu.MaxLength = 32767;
            this.txtNumFicheStandardAu.Multiline = false;
            this.txtNumFicheStandardAu.Name = "txtNumFicheStandardAu";
            this.txtNumFicheStandardAu.ReadOnly = false;
            this.txtNumFicheStandardAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumFicheStandardAu.Size = new System.Drawing.Size(342, 27);
            this.txtNumFicheStandardAu.TabIndex = 1;
            this.txtNumFicheStandardAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumFicheStandardAu.UseSystemPasswordChar = false;
            // 
            // txtDateCommandeDu
            // 
            this.txtDateCommandeDu.AccAcceptNumbersOnly = false;
            this.txtDateCommandeDu.AccAllowComma = false;
            this.txtDateCommandeDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateCommandeDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateCommandeDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateCommandeDu.AccHidenValue = "";
            this.txtDateCommandeDu.AccNotAllowedChars = null;
            this.txtDateCommandeDu.AccReadOnly = false;
            this.txtDateCommandeDu.AccReadOnlyAllowDelete = false;
            this.txtDateCommandeDu.AccRequired = false;
            this.txtDateCommandeDu.BackColor = System.Drawing.Color.White;
            this.txtDateCommandeDu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateCommandeDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateCommandeDu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateCommandeDu.ForeColor = System.Drawing.Color.Black;
            this.txtDateCommandeDu.Location = new System.Drawing.Point(1108, 2);
            this.txtDateCommandeDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateCommandeDu.MaxLength = 32767;
            this.txtDateCommandeDu.Multiline = false;
            this.txtDateCommandeDu.Name = "txtDateCommandeDu";
            this.txtDateCommandeDu.ReadOnly = false;
            this.txtDateCommandeDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateCommandeDu.Size = new System.Drawing.Size(342, 27);
            this.txtDateCommandeDu.TabIndex = 8;
            this.txtDateCommandeDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateCommandeDu.UseSystemPasswordChar = false;
            this.txtDateCommandeDu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateCommandeDu_Validating);
            // 
            // txtDateCommandeAu
            // 
            this.txtDateCommandeAu.AccAcceptNumbersOnly = false;
            this.txtDateCommandeAu.AccAllowComma = false;
            this.txtDateCommandeAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateCommandeAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateCommandeAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateCommandeAu.AccHidenValue = "";
            this.txtDateCommandeAu.AccNotAllowedChars = null;
            this.txtDateCommandeAu.AccReadOnly = false;
            this.txtDateCommandeAu.AccReadOnlyAllowDelete = false;
            this.txtDateCommandeAu.AccRequired = false;
            this.txtDateCommandeAu.BackColor = System.Drawing.Color.White;
            this.txtDateCommandeAu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateCommandeAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateCommandeAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateCommandeAu.ForeColor = System.Drawing.Color.Black;
            this.txtDateCommandeAu.Location = new System.Drawing.Point(1500, 2);
            this.txtDateCommandeAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateCommandeAu.MaxLength = 32767;
            this.txtDateCommandeAu.Multiline = false;
            this.txtDateCommandeAu.Name = "txtDateCommandeAu";
            this.txtDateCommandeAu.ReadOnly = false;
            this.txtDateCommandeAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateCommandeAu.Size = new System.Drawing.Size(342, 27);
            this.txtDateCommandeAu.TabIndex = 9;
            this.txtDateCommandeAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateCommandeAu.UseSystemPasswordChar = false;
            this.txtDateCommandeAu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateCommandeAu_Validating);
            // 
            // txtNoBonDeCommandeDu
            // 
            this.txtNoBonDeCommandeDu.AccAcceptNumbersOnly = false;
            this.txtNoBonDeCommandeDu.AccAllowComma = false;
            this.txtNoBonDeCommandeDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBonDeCommandeDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBonDeCommandeDu.AccHidenValue = "";
            this.txtNoBonDeCommandeDu.AccNotAllowedChars = null;
            this.txtNoBonDeCommandeDu.AccReadOnly = false;
            this.txtNoBonDeCommandeDu.AccReadOnlyAllowDelete = false;
            this.txtNoBonDeCommandeDu.AccRequired = false;
            this.txtNoBonDeCommandeDu.BackColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeDu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBonDeCommandeDu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBonDeCommandeDu.ForeColor = System.Drawing.Color.Black;
            this.txtNoBonDeCommandeDu.Location = new System.Drawing.Point(218, 32);
            this.txtNoBonDeCommandeDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBonDeCommandeDu.MaxLength = 32767;
            this.txtNoBonDeCommandeDu.Multiline = false;
            this.txtNoBonDeCommandeDu.Name = "txtNoBonDeCommandeDu";
            this.txtNoBonDeCommandeDu.ReadOnly = false;
            this.txtNoBonDeCommandeDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBonDeCommandeDu.Size = new System.Drawing.Size(342, 27);
            this.txtNoBonDeCommandeDu.TabIndex = 2;
            this.txtNoBonDeCommandeDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBonDeCommandeDu.UseSystemPasswordChar = false;
            // 
            // txtNoInterventionDu
            // 
            this.txtNoInterventionDu.AccAcceptNumbersOnly = false;
            this.txtNoInterventionDu.AccAllowComma = false;
            this.txtNoInterventionDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInterventionDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInterventionDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInterventionDu.AccHidenValue = "";
            this.txtNoInterventionDu.AccNotAllowedChars = null;
            this.txtNoInterventionDu.AccReadOnly = false;
            this.txtNoInterventionDu.AccReadOnlyAllowDelete = false;
            this.txtNoInterventionDu.AccRequired = false;
            this.txtNoInterventionDu.BackColor = System.Drawing.Color.White;
            this.txtNoInterventionDu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInterventionDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoInterventionDu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoInterventionDu.ForeColor = System.Drawing.Color.Black;
            this.txtNoInterventionDu.Location = new System.Drawing.Point(218, 62);
            this.txtNoInterventionDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInterventionDu.MaxLength = 32767;
            this.txtNoInterventionDu.Multiline = false;
            this.txtNoInterventionDu.Name = "txtNoInterventionDu";
            this.txtNoInterventionDu.ReadOnly = false;
            this.txtNoInterventionDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInterventionDu.Size = new System.Drawing.Size(342, 27);
            this.txtNoInterventionDu.TabIndex = 4;
            this.txtNoInterventionDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInterventionDu.UseSystemPasswordChar = false;
            // 
            // txtNoBonDeCommandeAu
            // 
            this.txtNoBonDeCommandeAu.AccAcceptNumbersOnly = false;
            this.txtNoBonDeCommandeAu.AccAllowComma = false;
            this.txtNoBonDeCommandeAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBonDeCommandeAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBonDeCommandeAu.AccHidenValue = "";
            this.txtNoBonDeCommandeAu.AccNotAllowedChars = null;
            this.txtNoBonDeCommandeAu.AccReadOnly = false;
            this.txtNoBonDeCommandeAu.AccReadOnlyAllowDelete = false;
            this.txtNoBonDeCommandeAu.AccRequired = false;
            this.txtNoBonDeCommandeAu.BackColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeAu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBonDeCommandeAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBonDeCommandeAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBonDeCommandeAu.ForeColor = System.Drawing.Color.Black;
            this.txtNoBonDeCommandeAu.Location = new System.Drawing.Point(600, 32);
            this.txtNoBonDeCommandeAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBonDeCommandeAu.MaxLength = 32767;
            this.txtNoBonDeCommandeAu.Multiline = false;
            this.txtNoBonDeCommandeAu.Name = "txtNoBonDeCommandeAu";
            this.txtNoBonDeCommandeAu.ReadOnly = false;
            this.txtNoBonDeCommandeAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBonDeCommandeAu.Size = new System.Drawing.Size(342, 27);
            this.txtNoBonDeCommandeAu.TabIndex = 3;
            this.txtNoBonDeCommandeAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBonDeCommandeAu.UseSystemPasswordChar = false;
            // 
            // txtNoInterventionAu
            // 
            this.txtNoInterventionAu.AccAcceptNumbersOnly = false;
            this.txtNoInterventionAu.AccAllowComma = false;
            this.txtNoInterventionAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInterventionAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInterventionAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInterventionAu.AccHidenValue = "";
            this.txtNoInterventionAu.AccNotAllowedChars = null;
            this.txtNoInterventionAu.AccReadOnly = false;
            this.txtNoInterventionAu.AccReadOnlyAllowDelete = false;
            this.txtNoInterventionAu.AccRequired = false;
            this.txtNoInterventionAu.BackColor = System.Drawing.Color.White;
            this.txtNoInterventionAu.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInterventionAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoInterventionAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoInterventionAu.ForeColor = System.Drawing.Color.Black;
            this.txtNoInterventionAu.Location = new System.Drawing.Point(600, 62);
            this.txtNoInterventionAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInterventionAu.MaxLength = 32767;
            this.txtNoInterventionAu.Multiline = false;
            this.txtNoInterventionAu.Name = "txtNoInterventionAu";
            this.txtNoInterventionAu.ReadOnly = false;
            this.txtNoInterventionAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInterventionAu.Size = new System.Drawing.Size(342, 27);
            this.txtNoInterventionAu.TabIndex = 5;
            this.txtNoInterventionAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInterventionAu.UseSystemPasswordChar = false;
            // 
            // txtDateLivraisonDu
            // 
            this.txtDateLivraisonDu.AccAcceptNumbersOnly = false;
            this.txtDateLivraisonDu.AccAllowComma = false;
            this.txtDateLivraisonDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateLivraisonDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateLivraisonDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateLivraisonDu.AccHidenValue = "";
            this.txtDateLivraisonDu.AccNotAllowedChars = null;
            this.txtDateLivraisonDu.AccReadOnly = false;
            this.txtDateLivraisonDu.AccReadOnlyAllowDelete = false;
            this.txtDateLivraisonDu.AccRequired = false;
            this.txtDateLivraisonDu.BackColor = System.Drawing.Color.White;
            this.txtDateLivraisonDu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateLivraisonDu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateLivraisonDu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateLivraisonDu.ForeColor = System.Drawing.Color.Black;
            this.txtDateLivraisonDu.Location = new System.Drawing.Point(1108, 32);
            this.txtDateLivraisonDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateLivraisonDu.MaxLength = 32767;
            this.txtDateLivraisonDu.Multiline = false;
            this.txtDateLivraisonDu.Name = "txtDateLivraisonDu";
            this.txtDateLivraisonDu.ReadOnly = false;
            this.txtDateLivraisonDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateLivraisonDu.Size = new System.Drawing.Size(342, 27);
            this.txtDateLivraisonDu.TabIndex = 10;
            this.txtDateLivraisonDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateLivraisonDu.UseSystemPasswordChar = false;
            this.txtDateLivraisonDu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateLivraisonDu_Validating);
            // 
            // txtSignataire
            // 
            this.txtSignataire.AccAcceptNumbersOnly = false;
            this.txtSignataire.AccAllowComma = false;
            this.txtSignataire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSignataire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSignataire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSignataire.AccHidenValue = "";
            this.txtSignataire.AccNotAllowedChars = null;
            this.txtSignataire.AccReadOnly = false;
            this.txtSignataire.AccReadOnlyAllowDelete = false;
            this.txtSignataire.AccRequired = false;
            this.txtSignataire.BackColor = System.Drawing.Color.White;
            this.txtSignataire.CustomBackColor = System.Drawing.Color.White;
            this.txtSignataire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSignataire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtSignataire.ForeColor = System.Drawing.Color.Black;
            this.txtSignataire.Location = new System.Drawing.Point(1108, 62);
            this.txtSignataire.Margin = new System.Windows.Forms.Padding(2);
            this.txtSignataire.MaxLength = 32767;
            this.txtSignataire.Multiline = false;
            this.txtSignataire.Name = "txtSignataire";
            this.txtSignataire.ReadOnly = false;
            this.txtSignataire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSignataire.Size = new System.Drawing.Size(342, 27);
            this.txtSignataire.TabIndex = 12;
            this.txtSignataire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSignataire.UseSystemPasswordChar = false;
            // 
            // txtDateLivraisonAu
            // 
            this.txtDateLivraisonAu.AccAcceptNumbersOnly = false;
            this.txtDateLivraisonAu.AccAllowComma = false;
            this.txtDateLivraisonAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateLivraisonAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateLivraisonAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateLivraisonAu.AccHidenValue = "";
            this.txtDateLivraisonAu.AccNotAllowedChars = null;
            this.txtDateLivraisonAu.AccReadOnly = false;
            this.txtDateLivraisonAu.AccReadOnlyAllowDelete = false;
            this.txtDateLivraisonAu.AccRequired = false;
            this.txtDateLivraisonAu.BackColor = System.Drawing.Color.White;
            this.txtDateLivraisonAu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateLivraisonAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateLivraisonAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateLivraisonAu.ForeColor = System.Drawing.Color.Black;
            this.txtDateLivraisonAu.Location = new System.Drawing.Point(1500, 32);
            this.txtDateLivraisonAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateLivraisonAu.MaxLength = 32767;
            this.txtDateLivraisonAu.Multiline = false;
            this.txtDateLivraisonAu.Name = "txtDateLivraisonAu";
            this.txtDateLivraisonAu.ReadOnly = false;
            this.txtDateLivraisonAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateLivraisonAu.Size = new System.Drawing.Size(342, 27);
            this.txtDateLivraisonAu.TabIndex = 11;
            this.txtDateLivraisonAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateLivraisonAu.UseSystemPasswordChar = false;
            this.txtDateLivraisonAu.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateLivraisonAu_Validating);
            // 
            // cmdSignataire
            // 
            this.cmdSignataire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSignataire.FlatAppearance.BorderSize = 0;
            this.cmdSignataire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSignataire.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSignataire.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSignataire.Location = new System.Drawing.Point(1455, 63);
            this.cmdSignataire.Name = "cmdSignataire";
            this.cmdSignataire.Size = new System.Drawing.Size(25, 20);
            this.cmdSignataire.TabIndex = 524;
            this.cmdSignataire.Tag = "";
            this.toolTip1.SetToolTip(this.cmdSignataire, "Recherche de signataire");
            this.cmdSignataire.UseVisualStyleBackColor = false;
            this.cmdSignataire.Click += new System.EventHandler(this.cmdSignataire_Click);
            // 
            // lbl01
            // 
            this.lbl01.AutoSize = true;
            this.lbl01.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl01.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl01.Location = new System.Drawing.Point(947, 30);
            this.lbl01.Name = "lbl01";
            this.lbl01.Size = new System.Drawing.Size(156, 30);
            this.lbl01.TabIndex = 523;
            this.lbl01.Text = "Livrée entre le";
            // 
            // lbl105
            // 
            this.lbl105.AutoSize = true;
            this.lbl105.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl105.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl105.Location = new System.Drawing.Point(947, 60);
            this.lbl105.Name = "lbl105";
            this.lbl105.Size = new System.Drawing.Size(156, 30);
            this.lbl105.TabIndex = 519;
            this.lbl105.Text = "Signataire";
            // 
            // lblFournisseur
            // 
            this.lblFournisseur.AutoSize = true;
            this.lblFournisseur.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFournisseur.Location = new System.Drawing.Point(601, 90);
            this.lblFournisseur.Name = "lblFournisseur";
            this.lblFournisseur.Size = new System.Drawing.Size(0, 19);
            this.lblFournisseur.TabIndex = 535;
            // 
            // lblIntervenant
            // 
            this.lblIntervenant.AutoSize = true;
            this.lblIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntervenant.Location = new System.Drawing.Point(601, 120);
            this.lblIntervenant.Name = "lblIntervenant";
            this.lblIntervenant.Size = new System.Drawing.Size(0, 19);
            this.lblIntervenant.TabIndex = 536;
            // 
            // lbl108
            // 
            this.lbl108.AutoSize = true;
            this.lbl108.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl108.Location = new System.Drawing.Point(3, 120);
            this.lbl108.Name = "lbl108";
            this.lbl108.Size = new System.Drawing.Size(90, 19);
            this.lbl108.TabIndex = 525;
            this.lbl108.Text = "Intervenant";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 194F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 294F));
            this.tableLayoutPanel4.Controls.Add(this.lblTotal, 3, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 205);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(759, 27);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotal.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(481, 0);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(288, 32);
            this.lblTotal.TabIndex = 525;
            this.lblTotal.Text = "Total des articles";
            // 
            // UserListeArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserListeArticle";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Listing des articles commandés";
            this.VisibleChanged += new System.EventHandler(this.UserListeArticle_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssListeAricle)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 txtNumFicheStandardDu;
        public iTalk.iTalk_TextBox_Small2 txtNumFicheStandardAu;
        public iTalk.iTalk_TextBox_Small2 txtDateCommandeDu;
        public iTalk.iTalk_TextBox_Small2 txtDateCommandeAu;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label lbl11;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtNoBonDeCommandeDu;
        public iTalk.iTalk_TextBox_Small2 txtNoInterventionDu;
        public iTalk.iTalk_TextBox_Small2 txtNoBonDeCommandeAu;
        public iTalk.iTalk_TextBox_Small2 txtNoInterventionAu;
        public iTalk.iTalk_TextBox_Small2 txtDateLivraisonDu;
        public iTalk.iTalk_TextBox_Small2 txtSignataire;
        public iTalk.iTalk_TextBox_Small2 txtDateLivraisonAu;
        public System.Windows.Forms.Label lbl01;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lbl101;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label lbl105;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label lbl00;
        public System.Windows.Forms.Button cmdSignataire;
        public System.Windows.Forms.Label lbl108;
        public System.Windows.Forms.Label lbl104;
        public System.Windows.Forms.Label lbl106;
        public System.Windows.Forms.Label lbl100;
        public iTalk.iTalk_TextBox_Small2 txtTechnicien;
        public iTalk.iTalk_TextBox_Small2 txtcodeFourn;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtDesignation;
        public System.Windows.Forms.Button cmdRechercheFourn;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button cmdRechercheIm;
        public System.Windows.Forms.Label lblTotal;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssListeAricle;
        public System.Windows.Forms.Label lblSignataire;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdDelD;
        public System.Windows.Forms.Button lblClean;
        public System.Windows.Forms.Button cmdExportExcel;
        public System.Windows.Forms.Label lblFournisseur;
        public System.Windows.Forms.Label lblIntervenant;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
