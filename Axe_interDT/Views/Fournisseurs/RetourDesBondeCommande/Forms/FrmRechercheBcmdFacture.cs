﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Collections;
using System.Diagnostics;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.Fournisseurs.RetourdesBondeCommande
{
    public partial class FrmRechercheBcmdFacture : Form
    {
        ModAdo ModAdo1 = new ModAdo();
        public FrmRechercheBcmdFacture()
        {
            InitializeComponent();
        }
        private void cmdannuler_Click(Object eventSender, EventArgs eventArgs)
        {
            chkAnnuler.CheckState = CheckState.Checked;
            this.Visible = false;
        }

        private void cmdFermer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            
            GridBCmd.UpdateData();
            chkAnnuler.CheckState = CheckState.Unchecked;
            this.Visible = false;
        }


        private void fc_ChargeBcmd()
        {

            DataTable rsBcmd = new DataTable();
            int i = 0;

            try
            {


                if (string.IsNullOrEmpty(txtSelect.Text))
                {
                    General.sSQL = "SELECT DISTINCT NoBonDeCommande,BonDeCommande.CodeImmeuble as [Immeuble],Estimation,Acheteur,ProduitTotalHT,CodeFourn FROM BonDeCommande ";
                    General.sSQL = General.sSQL + " LEFT JOIN FactFournPied ON BonDeCommande.NoBonDeCommande=FactFournPied.NoBCmd ";
                }
                else
                {
                    General.sSQL = txtSelect.Text;
                }
                General.sSQL = General.sSQL + " " + txtWhere.Text;

                //if (GridBCmd.Rows.Count > 0)
                //{
                //    //for (i = 0; i <= GridBCmd.Rows.Count - 1; i++)
                //    //{
                //    //    GridBCmd.RemoveItem(0);

           // }
                if (GridBCmd.Rows.Count > 0)
            {
                GridBCmd.DataSource = null;
            }
            if (rsBcmd == null)
                {
                    rsBcmd = new DataTable();
                }
                if (rsBcmd != null)
                {
                    rsBcmd.Dispose();
                }
                //if (rsBcmd.State == ADODB.ObjectStateEnum.adStateOpen)//Todo
                //{
                //    rsBcmd.Close();
                //}

                rsBcmd = ModAdo1.fc_OpenRecordSet(General.sSQL);

                if (rsBcmd.Rows.Count > 0)
                {
                    DataTable table = new DataTable();

                    table.Columns.Add("Selectionner", typeof(bool));
                    table.Columns.Add("No Bon De Commande");
                    table.Columns.Add("Immeuble");
                    table.Columns.Add("Montant");
                    table.Columns.Add("Acheteur");


                    var NR = table.NewRow();

                    foreach (DataRow R in rsBcmd.Rows)
                    {
                        if (chkEnleveDoublon.Checked == true)
                        {
                            if (string.IsNullOrEmpty(General.nz(ModAdo1.fc_ADOlibelle("SELECT NoBCmd FROM FactFournPied WHERE NoBCmd=" + R["NoBonDeCommande"] + " AND FactFournPied.NoAutoFacture=" + txtNoAutoFacture.Text), "").ToString()))
                            {
                                if (General.nz(R["ProduitTotalHT"], "0").ToString() == "0")
                                {

                                    NR["Selectionner"] = 0;
                                    NR["No Bon De Commande"] = R["NoBonDeCommande"];
                                    NR["Immeuble"] = R["Immeuble"] + "";
                                    NR["Montant"] = General.nz(R["Estimation"], "0");
                                    NR["Acheteur"] = R["Acheteur"] + "";

                                    table.Rows.Add(NR.ItemArray);
                                    GridBCmd.DataSource = table;
                                }
                                else
                                {

                                    NR["Selectionner"] = 0;
                                    NR["No Bon De Commande"] = R["NoBonDeCommande"];
                                    NR["Immeuble"] = R["Immeuble"] + "";
                                    NR["Montant"] = General.nz(R["ProduitTotalHT"], "0");
                                    NR["Acheteur"] = R["Acheteur"] + "";

                                    table.Rows.Add(NR.ItemArray);
                                    GridBCmd.DataSource = table;

                                }
                            }
                        }
                        else
                        {
                            if (General.nz(R["ProduitTotalHT"], "0").ToString() == "0")
                            {

                                NR["Selectionner"] = 0;
                                NR["No Bon De Commande"] = R["NoBonDeCommande"];
                                NR["Immeuble"] = R["Immeuble"] + "";
                                NR["Montant"] = General.nz(R["Estimation"], "0");
                                NR["Acheteur"] = R["Acheteur"] + "";

                                table.Rows.Add(NR.ItemArray);
                                GridBCmd.DataSource = table;
                            }
                            else
                            {

                                NR["Selectionner"] = 0;
                                NR["No Bon De Commande"] = R["NoBonDeCommande"];
                                NR["Immeuble"] = R["Immeuble"] + "";
                                NR["Montant"] = General.nz(R["ProduitTotalHT"], "0");
                                NR["Acheteur"] = R["Acheteur"] + "";

                                table.Rows.Add(NR.ItemArray);
                                GridBCmd.DataSource = table;
                            }
                        }

                        Application.DoEvents();
                    }
                }

                //if (rsBcmd.State == ADODB.ObjectStateEnum.adStateOpen)
                //{
                //    rsBcmd.Close();
                //}
                rsBcmd.Dispose();
                rsBcmd = null;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Recherche des bons de commande à associer à une facture fournisseur ");
            }
        }
        private void GridBCmd_AfterExitEditMode(object sender, EventArgs e)//GridBCmd_AfterColUpdate
        {
            // GridBCmd.ActiveCell.Column.Index == GridBCmd.ActiveRow.Cells["Qté. Livrée"].Column.Index
            if (GridBCmd.ActiveCell.Column.Index == 0)
            {
                if (General.SelectBcmdDetail.Length < GridBCmd.Rows.Count)
                {
                    Array.Resize(ref General.SelectBcmdDetail, GridBCmd.Rows.Count + 1);
                }
                if (GridBCmd.ActiveRow.Cells[0].Text == "0")
                {
                    General.SelectBcmdDetail[GridBCmd.Rows.Count] = "";
                }
                else if (GridBCmd.Rows.Count != -1)
                {
                    General.SelectBcmdDetail[GridBCmd.ActiveRow.Index] = GridBCmd.ActiveRow.Cells[1].Text;
                }
            }
        }

        private void FrmRechercheBcmdFacture_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            //Prépare le tableau qui permet de récupérer les bons de commande à ajouter
            General.SelectBcmdDetail = new string[2];           
            fc_ChargeBcmd();
        }
    }
}
