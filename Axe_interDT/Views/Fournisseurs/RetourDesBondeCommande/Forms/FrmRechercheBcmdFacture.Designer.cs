﻿namespace Axe_interDT.Views.Fournisseurs.RetourdesBondeCommande
{
    partial class FrmRechercheBcmdFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.GridBCmd = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.chkEnleveDoublon = new System.Windows.Forms.RadioButton();
            this.chkAnnuler = new System.Windows.Forms.CheckBox();
            this.cmdFermer = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNoAutoFacture = new iTalk.iTalk_TextBox_Small2();
            this.txtSelect = new iTalk.iTalk_TextBox_Small2();
            this.txtWhere = new iTalk.iTalk_TextBox_Small2();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridBCmd)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraPanel1
            // 
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.GridBCmd);
            this.ultraPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraPanel1.Location = new System.Drawing.Point(-7, 0);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(624, 382);
            this.ultraPanel1.TabIndex = 0;
            // 
            // GridBCmd
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridBCmd.DisplayLayout.Appearance = appearance1;
            this.GridBCmd.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridBCmd.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridBCmd.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridBCmd.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridBCmd.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridBCmd.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridBCmd.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridBCmd.DisplayLayout.MaxColScrollRegions = 1;
            this.GridBCmd.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridBCmd.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridBCmd.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridBCmd.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridBCmd.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridBCmd.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridBCmd.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridBCmd.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridBCmd.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridBCmd.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridBCmd.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridBCmd.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridBCmd.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridBCmd.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridBCmd.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridBCmd.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridBCmd.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridBCmd.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridBCmd.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridBCmd.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridBCmd.Location = new System.Drawing.Point(-9, 0);
            this.GridBCmd.Name = "GridBCmd";
            this.GridBCmd.Size = new System.Drawing.Size(624, 382);
            this.GridBCmd.TabIndex = 412;
            this.GridBCmd.AfterExitEditMode += new System.EventHandler(this.GridBCmd_AfterExitEditMode);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.22222F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.77778F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel1.Controls.Add(this.chkEnleveDoublon, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkAnnuler, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmdFermer, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmdAnnuler, 2, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(-7, 389);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(435, 40);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // chkEnleveDoublon
            // 
            this.chkEnleveDoublon.AutoSize = true;
            this.chkEnleveDoublon.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEnleveDoublon.Location = new System.Drawing.Point(3, 3);
            this.chkEnleveDoublon.Name = "chkEnleveDoublon";
            this.chkEnleveDoublon.Size = new System.Drawing.Size(93, 23);
            this.chkEnleveDoublon.TabIndex = 571;
            this.chkEnleveDoublon.TabStop = true;
            this.chkEnleveDoublon.Text = "EnleveDoublon";
            this.chkEnleveDoublon.UseVisualStyleBackColor = true;
            this.chkEnleveDoublon.Visible = false;
            // 
            // chkAnnuler
            // 
            this.chkAnnuler.AutoSize = true;
            this.chkAnnuler.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAnnuler.Location = new System.Drawing.Point(102, 3);
            this.chkAnnuler.Name = "chkAnnuler";
            this.chkAnnuler.Size = new System.Drawing.Size(83, 23);
            this.chkAnnuler.TabIndex = 570;
            this.chkAnnuler.Text = "Annuler";
            this.chkAnnuler.UseVisualStyleBackColor = true;
            this.chkAnnuler.Visible = false;
            // 
            // cmdFermer
            // 
            this.cmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFermer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdFermer.FlatAppearance.BorderSize = 0;
            this.cmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFermer.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFermer.ForeColor = System.Drawing.Color.White;
            this.cmdFermer.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFermer.Location = new System.Drawing.Point(322, 2);
            this.cmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFermer.Name = "cmdFermer";
            this.cmdFermer.Size = new System.Drawing.Size(111, 36);
            this.cmdFermer.TabIndex = 573;
            this.cmdFermer.Tag = "";
            this.cmdFermer.Text = "    Fermer";
            this.toolTip1.SetToolTip(this.cmdFermer, "Ferme en prenant en compte les éventuels bons de commande sélectionnés");
            this.cmdFermer.UseVisualStyleBackColor = false;
            this.cmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(217, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(99, 35);
            this.cmdAnnuler.TabIndex = 572;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.Text = "      Annuler";
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Ferme sans prendre en compte les éventuels bons de commande sélectionnés");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdannuler_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.txtNoAutoFacture, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtSelect, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtWhere, 1, 0);
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(432, 383);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(173, 46);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // txtNoAutoFacture
            // 
            this.txtNoAutoFacture.AccAcceptNumbersOnly = false;
            this.txtNoAutoFacture.AccAllowComma = false;
            this.txtNoAutoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoAutoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoAutoFacture.AccHidenValue = "";
            this.txtNoAutoFacture.AccNotAllowedChars = null;
            this.txtNoAutoFacture.AccReadOnly = false;
            this.txtNoAutoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoAutoFacture.AccRequired = false;
            this.txtNoAutoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNoAutoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoAutoFacture.Location = new System.Drawing.Point(2, 25);
            this.txtNoAutoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoAutoFacture.MaxLength = 32767;
            this.txtNoAutoFacture.Multiline = false;
            this.txtNoAutoFacture.Name = "txtNoAutoFacture";
            this.txtNoAutoFacture.ReadOnly = false;
            this.txtNoAutoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoAutoFacture.Size = new System.Drawing.Size(64, 27);
            this.txtNoAutoFacture.TabIndex = 507;
            this.txtNoAutoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoAutoFacture.UseSystemPasswordChar = false;
            this.txtNoAutoFacture.Visible = false;
            // 
            // txtSelect
            // 
            this.txtSelect.AccAcceptNumbersOnly = false;
            this.txtSelect.AccAllowComma = false;
            this.txtSelect.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelect.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelect.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSelect.AccHidenValue = "";
            this.txtSelect.AccNotAllowedChars = null;
            this.txtSelect.AccReadOnly = false;
            this.txtSelect.AccReadOnlyAllowDelete = false;
            this.txtSelect.AccRequired = false;
            this.txtSelect.BackColor = System.Drawing.Color.White;
            this.txtSelect.CustomBackColor = System.Drawing.Color.White;
            this.txtSelect.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSelect.ForeColor = System.Drawing.Color.Black;
            this.txtSelect.Location = new System.Drawing.Point(2, 2);
            this.txtSelect.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelect.MaxLength = 32767;
            this.txtSelect.Multiline = false;
            this.txtSelect.Name = "txtSelect";
            this.txtSelect.ReadOnly = false;
            this.txtSelect.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelect.Size = new System.Drawing.Size(64, 27);
            this.txtSelect.TabIndex = 505;
            this.txtSelect.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelect.UseSystemPasswordChar = false;
            this.txtSelect.Visible = false;
            // 
            // txtWhere
            // 
            this.txtWhere.AccAcceptNumbersOnly = false;
            this.txtWhere.AccAllowComma = false;
            this.txtWhere.AccBackgroundColor = System.Drawing.Color.White;
            this.txtWhere.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtWhere.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtWhere.AccHidenValue = "";
            this.txtWhere.AccNotAllowedChars = null;
            this.txtWhere.AccReadOnly = false;
            this.txtWhere.AccReadOnlyAllowDelete = false;
            this.txtWhere.AccRequired = false;
            this.txtWhere.BackColor = System.Drawing.Color.White;
            this.txtWhere.CustomBackColor = System.Drawing.Color.White;
            this.txtWhere.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtWhere.ForeColor = System.Drawing.Color.Black;
            this.txtWhere.Location = new System.Drawing.Point(88, 2);
            this.txtWhere.Margin = new System.Windows.Forms.Padding(2);
            this.txtWhere.MaxLength = 32767;
            this.txtWhere.Multiline = false;
            this.txtWhere.Name = "txtWhere";
            this.txtWhere.ReadOnly = false;
            this.txtWhere.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtWhere.Size = new System.Drawing.Size(64, 27);
            this.txtWhere.TabIndex = 506;
            this.txtWhere.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtWhere.UseSystemPasswordChar = false;
            this.txtWhere.Visible = false;
            // 
            // FrmRechercheBcmdFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(617, 434);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.ultraPanel1);
            this.Name = "FrmRechercheBcmdFacture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmRechercheBcmdFacture";
            this.Load += new System.EventHandler(this.FrmRechercheBcmdFacture_Load);
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridBCmd)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridBCmd;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.CheckBox chkAnnuler;
        public System.Windows.Forms.RadioButton chkEnleveDoublon;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdFermer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 txtNoAutoFacture;
        public iTalk.iTalk_TextBox_Small2 txtSelect;
        public iTalk.iTalk_TextBox_Small2 txtWhere;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}