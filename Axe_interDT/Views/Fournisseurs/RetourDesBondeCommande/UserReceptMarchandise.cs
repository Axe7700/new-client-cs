﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;
using System.Collections;
using System.Diagnostics;
using VB = Microsoft.VisualBasic;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Fournisseurs.RetourDesBondeCommande;
using System.Data.SqlClient;
using Axe_interDT.Views.Fournisseurs;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Fournisseurs.RetourdesBondeCommande;

namespace Axe_interDT.Views.Fournisseurs.RetourDesBondeCommande
{
    public partial class UserReceptMarchandise : UserControl
    {
        public UserReceptMarchandise()
        {
            InitializeComponent();
        }
        private DataTable rsFactFournEntete;
        private DataTable rsFactFournDetail;
        private DataTable rsFactFournPied;

        ModAdo ModAdors = new ModAdo();

        //tested
        private void FactFournAddNew()
        {
            rsFactFournEntete = new DataTable();

            rsFactFournEntete = ModAdors.fc_OpenRecordSet("SELECT * FROM BcmdLivraisonEntete WHERE NoAutoLivraison=0 ORDER BY NoAutoLivraison");

            int NoAutoLivraison = 0;
            SqlCommandBuilder cb = new SqlCommandBuilder(ModAdors.SDArsAdo);
            ModAdors.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
            ModAdors.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
            SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
            insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
            insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
            ModAdors.SDArsAdo.InsertCommand = insertCmd;

            ModAdors.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
            {

                if (e.StatementType == StatementType.Insert)
                {

                    NoAutoLivraison = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());

                }
            });

            DataRow dr = rsFactFournEntete.NewRow();

            dr["CreeLe"] = DateTime.Now;
            lblDateCreation.Text = dr["DateSaisie"].ToString();
            dr["CreePar"] = General.gsUtilisateur;
            lblCreateur.Text = General.gsUtilisateur;
            dr["ModifieLe"] = dr["DateSaisie"];
            lblDateModif.Text = dr["DateSaisie"].ToString();
            dr["ModifiePar"] = General.gsUtilisateur;
            lblNomModif.Text = General.gsUtilisateur;
            dr["DateLivraison"] = DateAndTime.Today.ToShortDateString();
            txtDateLivraison.Text = DateAndTime.Today.ToShortDateString();
            rsFactFournEntete.Rows.Add(dr);
            ModAdors.Update();
            txtNoAutoFacture.Text = NoAutoLivraison.ToString();
            lblNoFacture.Text = txtNoAutoFacture.Text;

            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserReceptMarchandise, txtNoAutoFacture.Text);

        }

        //cmbCptCharges not exist

        //private void cmbCptCharges_Click()
        //{
        //    lblTauxTva.text = cmbCptCharges.Columns["Compte TVA"].value;
        //}

        //private void cmbDropDwnCptCharges_Click()
        //{
        //    GridFactFournDetail.Columns["Compte TVA"].value = cmbDropDwnCptCharges.Columns("Compte de TVA").Text;
        //}

        //tested
        private void cmdAjouter_Click(Object eventSender, EventArgs eventArgs)
        {
            fnc_Clear();
            FactFournAddNew();
        }
        //tested
        private void fnc_Clear()
        {
            if (GridFactFournPied.Rows.Count > 0)
            {
                GridFactFournPied.DataSource = null;
            }
            if (GridFactFournDetail.Rows.Count > 0)
            {
                GridFactFournDetail.DataSource = null;
            }

            cmdRechercheRaisonSociale.Visible = true;
            txtCodeFourn.Text = "";
            txtNbrColi.Text = "";
            txtCodeFournisseur.Text = "";
            txtCompteFournisseur.Text = "";
            txtRaisonFournisseur.Text = "";
            txtNoAutoFacture.Text = "";
            txtCommentaire.Text = "";
            lblDateCreation.Text = "";
            lblCreateur.Text = "";
            lblDateModif.Text = "";
            lblNomModif.Text = "";
            lblNoFacture.Text = "";
            txtDateLivraison.Text = "";
            txtNoFacture.Text = "";
        }
        //tested
        private void cmdAnnuler_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            if (string.IsNullOrEmpty(txtCodeFournisseur.Text))
            {
                fnc_DeleteFacture(txtNoAutoFacture.Text);
            }
            else if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous annuler ce bon de livraison ?", "Annuler un bon de livraison", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                fnc_DeleteFacture(txtNoAutoFacture.Text);
            }
            else
            {
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    ChargeFactureEntete(ref ModParametre.sNewVar);
                }
            }
        }
        //tested
        private void fnc_DeleteFacture(string NoFacture)
        {

            string sqlDelete = null;
            string MaxNoFacture = null;


            if (string.IsNullOrEmpty(NoFacture))
            {
                return;
            }

            sqlDelete = "DELETE FROM BcmdLivraisonDetail WHERE NoAutoLivraison=" + NoFacture;
            General.Execute(sqlDelete);

            sqlDelete = "DELETE FROM BcmdLivraisonEntete WHERE NoAutoLivraison=" + NoFacture;
            General.Execute(sqlDelete);

            fnc_Clear();

            MaxNoFacture = ModAdors.fc_ADOlibelle("SELECT MAX(NoAutoLivraison) FROM BcmdLivraisonEntete WHERE CreePar='" + General.gsUtilisateur + "' OR ModifiePar='" + General.gsUtilisateur + "'");

            if (string.IsNullOrEmpty(MaxNoFacture))
            {
                return;
            }

            ChargeFactureEntete(ref MaxNoFacture);

        }
        //tested
        private void cmdEnleverUnBcmd_Click(Object eventSender, EventArgs eventArgs)
        {
            int i = 0;


            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas créer de nouvelle fiche fournisseur,voulez-vous en créer une ?", "Fiche fournisseur inexistante", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    cmdAjouter_Click(cmdAjouter, new System.EventArgs());
                }
                else
                {
                    return;
                }
            }
            FrmRechercheBcmdFacture _with2 = new FrmRechercheBcmdFacture();
            if (!string.IsNullOrEmpty(txtCodeFournisseur.Text))
            {

                _with2.txtSelect.Text = "SELECT DISTINCT NoBonDeCommande,BonDeCommande.CodeImmeuble as [Immeuble],Estimation,Acheteur,ProduitTotalHT,CodeFourn,BcmdLivraisonDetail.NoBcmd FROM BonDeCommande ";
                _with2.txtSelect.Text = _with2.txtSelect.Text + " INNER JOIN BcmdLivraisonDetail ON BonDeCommande.NoBonDeCommande=BcmdLivraisonDetail.NoBCmd ";
                _with2.Text = "Enlever un bon de commande d'un bon de livraison";
                _with2.txtWhere.Text = " WHERE CodeFourn=" + txtCodeFournisseur.Text + " AND NoAutoLivraison=" + txtNoAutoFacture.Text;
                _with2.ShowDialog();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le Fournisseur", "Recherche des bons de commande annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdRechercheRaisonSociale_Click(cmdRechercheRaisonSociale, new EventArgs());
                return;
            }

            GridFactFournPied.DataSource = null;
            GridFactFournDetail.DataSource = null;

            if (_with2.chkAnnuler.CheckState == CheckState.Unchecked)
            {
                for (i = 0; i <= Information.UBound(General.SelectBcmdDetail); i++)
                {
                    if (!string.IsNullOrEmpty(General.SelectBcmdDetail[i]))
                    {
                        EffaceBcmdFacture(ref General.SelectBcmdDetail[i]);
                    }
                }
            }
            else
            {
                General.SelectBcmdDetail = new string[2];
            }
            ChargeFactureDetailPied();
            _with2.Close();

        }
        //tested
        private void cmdRechercheBcmd_Click(Object eventSender, EventArgs eventArgs)
        {

            int i = 0;

            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas créer de nouvelle fiche fournisseur,voulez-vous en créer une ?", "Fiche fournisseur inexistante", MessageBoxButtons.YesNo) == DialogResult.Yes)
                {
                    cmdAjouter_Click(cmdAjouter, new EventArgs());
                }
                else
                {
                    return;
                }
            }

            var _with3 = new FrmRechercheBcmdFacture();
            if (!string.IsNullOrEmpty(txtCodeFournisseur.Text))
            {

                _with3.txtWhere.Text = " WHERE CodeFourn=" + txtCodeFournisseur.Text + " AND (FactFournPied.NoAutoFacture<>"
                    + txtNoAutoFacture.Text + " OR FactFournPied.NoAutoFacture IS NULL) AND (CodeE='CA' OR CodeE='CE' OR CodeE='CF') AND EtatLivraison<>'LT' ";
                _with3.Text = "Associer un bon de commande à un bon de livraison";
                _with3.chkEnleveDoublon.Checked = true;
                _with3.txtNoAutoFacture.Text = txtNoAutoFacture.Text;
                _with3.StartPosition = FormStartPosition.CenterParent;
                _with3.ShowDialog();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le Fournisseur", "Recherche des bons de commande annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                cmdRechercheRaisonSociale_Click(cmdRechercheRaisonSociale, new EventArgs());
                return;
            }


            //    GridFactFournDetail.DataSource = null;


            if (_with3.chkAnnuler.CheckState == 0)
            {
                if (General.SelectBcmdDetail != null)
                {
                    for (i = 0; i <= Information.UBound(General.SelectBcmdDetail); i++)
                    {
                        if (!string.IsNullOrEmpty(General.SelectBcmdDetail[i]))
                        {
                            //tested
                            ChargeDetail(General.SelectBcmdDetail[i]);
                        }
                    }
                }
            }


            //tested
            CmdSauver_Click(CmdSauver, new EventArgs());

        }
        //tested
        private void ChargeDetail(string NoBcmd)
        {

            //Charge le détail du bon de commande en vérifiant si il n'existe
            //pas déjà une facture fournisseur associée à ce même numéro de bon de commande
            //afin d'en récupérer le reste à livrer pour les références des articles commandés

            DataTable rsCmdDetail;
            DataTable rsFactFournDetail;
            DataTable rsCmdEntete;
            string sqlBcmd = null;

            if (!string.IsNullOrEmpty(NoBcmd))
            {
                sqlBcmd = "SELECT * FROM BCD_DETAIL WHERE BCD_Cle=" + NoBcmd + " ORDER BY BCD_No ASC ";
            }
            else
            {
                return;
            }

            rsCmdDetail = new DataTable();
            rsFactFournDetail = new DataTable();
            rsCmdEntete = new DataTable();

            rsCmdEntete = ModAdors.fc_OpenRecordSet("SELECT NoBonDeCommande,CodeImmeuble,Acheteur,DateLivraison,CodeE " + " FROM BonDeCommande " + " where NoBonDeCommande=" + NoBcmd + "");
            DataTable dt1;
            if (GridFactFournPied.Rows.Count > 0)
            {
                dt1 = GridFactFournPied.DataSource as DataTable;
            }
            else
            {
                dt1 = new DataTable();
                dt1.Columns.Add("No Bon De Commande");
                dt1.Columns.Add("Code Immeuble");
                dt1.Columns.Add("Date Livraison");
                dt1.Columns.Add("Acheteur");
                dt1.Columns.Add("Code Etat");
            }

            //dt1.Columns.Add("No Bon De Commande");
            //dt1.Columns.Add("Code Immeuble");
            //dt1.Columns.Add("Date Livraison");
            //dt1.Columns.Add("Acheteur");
            //dt1.Columns.Add("Code Etat");

            var dr1 = dt1.NewRow();

            if (rsCmdEntete.Rows.Count > 0)
            {
                foreach (DataRow row in rsCmdEntete.Rows)
                {

                    dr1["No Bon De Commande"] = row["NoBonDeCommande"];
                    dr1["Code Immeuble"] = row["CodeImmeuble"];
                    dr1["Date Livraison"] = row["DateLivraison"];
                    dr1["Acheteur"] = row["Acheteur"];
                    dr1["Code Etat"] = row["CodeE"];

                    dt1.Rows.Add(dr1.ItemArray);
                    GridFactFournPied.DataSource = dt1;

                }
            }
            rsCmdEntete.Clear();


            rsCmdDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);

            if (rsCmdDetail.Rows.Count > 0)
            {
                DataTable dt;
                if (GridFactFournDetail.Rows.Count > 0)
                {
                    dt = GridFactFournDetail.DataSource as DataTable;
                }
                else
                {
                    dt = new DataTable();
                    dt.Columns.Add("NoAutoFacture");
                    dt.Columns.Add("NoLigne");
                    dt.Columns.Add("No Bon de Commande");
                    dt.Columns.Add("Réf.Fourniture");
                    dt.Columns.Add("Designation");
                    dt.Columns.Add("Qté.Commandée");
                    dt.Columns.Add("Qté.livrée");
                    dt.Columns.Add("Reste à livrer");
                    dt.Columns.Add("BCD_NO");
                }
                //var dt =GridFactFournDetail.DataSource as DataTable;
                // DataTable dt = new DataTable();
                //dt.Columns.Add("NoAutoFacture");
                //dt.Columns.Add("NoLigne");
                //dt.Columns.Add("No Bon de Commande");
                //dt.Columns.Add("Réf.Fourniture");
                //dt.Columns.Add("Designation");
                //dt.Columns.Add("Qté.Commandée");
                //dt.Columns.Add("Qté.livrée");
                //dt.Columns.Add("Reste à livrer");
                //dt.Columns.Add("BCD_NO");

                var dr2 = dt.NewRow();
                foreach (DataRow r in rsCmdDetail.Rows)
                {
                    if (!string.IsNullOrEmpty(General.nz(r["BCD_References"], "").ToString()))
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT SUM(QteLivre) as SumQteLivre " + " FROM BcmdLivraisonDetail "
                            + " WHERE NoBcmd=" + NoBcmd + " AND RefLigneCmd='"
                            + StdSQLchaine.gFr_DoublerQuote(r["BCD_References"].ToString()) + "'";

                        rsFactFournDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);
                        if (rsFactFournDetail.Rows.Count > 0)
                        {

                            dr2["NoAutoFacture"] = txtNoAutoFacture.Text;
                            dr2["NoLigne"] = "0";
                            dr2["No Bon de Commande"] = NoBcmd;
                            dr2["Réf.Fourniture"] = r["BCD_References"];
                            dr2["Designation"] = r["BCD_Designation"];
                            dr2["Qté.Commandée"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["Qté.livrée"] = "0";
                            dr2["Reste à livrer"] = (Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0")));
                            dr2["BCD_NO"] = r["BCD_NO"];
                            dt.Rows.Add(dr2.ItemArray);
                            GridFactFournDetail.DataSource = dt;
                        }
                        else
                        {
                            dr2["NoAutoFacture"] = txtNoAutoFacture.Text;
                            dr2["NoLigne"] = "0";
                            dr2["No Bon de Commande"] = NoBcmd;
                            dr2["Réf.Fourniture"] = r["BCD_References"];
                            dr2["Designation"] = r["BCD_Designation"];
                            dr2["Qté.Commandée"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["Qté.livrée"] = "0";
                            dr2["Reste à livrer"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["BCD_NO"] = r["BCD_NO"];

                            dt.Rows.Add(dr2.ItemArray);
                            GridFactFournDetail.DataSource = dt;

                        }
                    }
                    else
                    {
                        //Recherche des factures fournisseur faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT SUM(QteLivre) as SumQteLivre FROM BcmdLivraisonDetail" + " WHERE NoBcmd=" + NoBcmd + " AND DesignationLigneCmd='"
                            + StdSQLchaine.gFr_DoublerQuote(r["BCD_Designation"].ToString()) + "'";
                        rsFactFournDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);
                        //GridFactFournDetail.DataMode = ssDataModeAddItem
                        if (rsFactFournDetail.Rows.Count > 0)
                        {
                            dr2["NoAutoFacture"] = txtNoAutoFacture.Text;
                            dr2["NoLigne"] = "0";
                            dr2["No Bon de Commande"] = NoBcmd;
                            dr2["Réf.Fourniture"] = r["BCD_References"];
                            dr2["Designation"] = r["BCD_Designation"];
                            dr2["Qté.Commandée"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["Qté.livrée"] = "0";
                            dr2["Reste à livrer"] = (Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")) - Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0")));
                            dr2["BCD_NO"] = r["BCD_NO"];

                            dt.Rows.Add(dr2.ItemArray);
                            GridFactFournDetail.DataSource = dt;
                        }
                        else
                        {
                            dr2["NoAutoFacture"] = txtNoAutoFacture.Text;
                            dr2["NoLigne"] = "0";
                            dr2["No Bon de Commande"] = NoBcmd;
                            dr2["Réf.Fourniture"] = r["BCD_References"];
                            dr2["Designation"] = r["BCD_Designation"];
                            dr2["Qté.Commandée"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["Qté.livrée"] = "0";
                            dr2["Reste à livrer"] = General.nz(r["BCD_Quantite"], "0");
                            dr2["BCD_NO"] = r["BCD_NO"];

                            dt.Rows.Add(dr2.ItemArray);
                            GridFactFournDetail.DataSource = dt;

                        }
                    }

                    //rsFactFournDetail.Dispose();

                }
            }



            //rsCmdDetail.Dispose();
            ////rsFactFournDetail.Dispose();          
            //rsCmdDetail = null;
            //rsFactFournDetail = null;
            //rsCmdEntete = null;
            //return;

        }
        //tested
        public void EffaceBcmdFacture(ref string NoBcmd)
        {

            string sqlBcmd = null;
            DataTable rsBcmd;

            if (string.IsNullOrEmpty(NoBcmd))
            {
                return;
            }

            rsBcmd = new DataTable();

            sqlBcmd = " SELECT SUM(BcmdLivraisonDetail.QteLivre) AS SumQteLivre FROM BcmdLivraisonDetail WHERE NoBcmd=" + NoBcmd;
            rsBcmd = ModAdors.fc_OpenRecordSet(sqlBcmd);

            if (rsBcmd.Rows.Count > 0)
            {
                if (Convert.ToDouble(General.nz(rsBcmd.Rows[0]["SumQteLivre"], "0")) > 0)
                {
                    General.Execute("UPDATE BonDeCommande SET CodeE='CE' WHERE NoBonDeCommande=" + NoBcmd);
                }
                else
                {
                    General.Execute("UPDATE BonDeCommande SET CodeE='CE',EtatLivraison='NL' WHERE NoBonDeCommande=" + NoBcmd);
                }
            }

            rsBcmd.Clear();
            rsBcmd = null;

            General.Execute("DELETE FROM BcmdLivraisonDetail WHERE NoBCmd=" + NoBcmd + " AND NoAutoLivraison=" + txtNoAutoFacture.Text);
            return;

        }
        //tested
        private void ChargeFactureDetailPied()
        {


            DataTable rsCmdDetail;
            DataTable rsCmdPied;
            DataTable rsFactFournDetail;
            string sqlBcmd = null;
            //int i = 0;

            rsCmdPied = new DataTable();
            rsCmdDetail = new DataTable();
            rsFactFournDetail = new DataTable();

            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                return;
            }

            rsCmdPied = ModAdors.fc_OpenRecordSet("SELECT DISTINCT NoBonDeCommande,BonDeCommande.CodeImmeuble as Immeuble,"
                + " Acheteur," + " DateLivraison,CodeE " + " FROM BonDeCommande INNER JOIN BcmdLivraisonDetail "
                + " ON BonDeCommande.NoBonDeCommande=BcmdLivraisonDetail.NoBCmd " + " WHERE BcmdLivraisonDetail.NoAutoLivraison="
                + txtNoAutoFacture.Text + " ORDER BY NoBonDeCommande");

            if (GridFactFournPied.Rows.Count > 0)
            {
                GridFactFournPied.DataSource = null;

            }

            if (GridFactFournDetail.Rows.Count > 0)
            {
                GridFactFournDetail.DataSource = null;
            }

            DataTable table = new DataTable();

            table.Columns.Add("No Bon De Commande");
            table.Columns.Add("Code Immeuble");
            table.Columns.Add("Date Livraison");
            table.Columns.Add("Acheteur");
            table.Columns.Add("Code Etat");

            var rs = table.NewRow();

            if (rsCmdPied.Rows.Count > 0)
            {
                foreach (DataRow r in rsCmdPied.Rows)
                {

                    rs["No Bon De Commande"] = r["NoBonDeCommande"];
                    rs["Code Immeuble"] = r["Immeuble"];
                    rs["Date Livraison"] = r["DateLivraison"];
                    rs["Acheteur"] = r["Acheteur"];
                    rs["Code Etat"] = r["CodeE"];
                    table.Rows.Add(rs.ItemArray);
                    GridFactFournPied.DataSource = table;

                }

            }

            rsCmdPied.Clear();

            sqlBcmd = "SELECT * FROM BcmdLivraisonDetail WHERE NoAutoLivraison=" + txtNoAutoFacture.Text;
            rsCmdDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);


            DataTable table1 = new DataTable();
            table1.Columns.Add("NoAutoFacture");
            table1.Columns.Add("NoLigne");
            table1.Columns.Add("No Bon de Commande");
            table1.Columns.Add("Réf.Fourniture");
            table1.Columns.Add("Designation");
            table1.Columns.Add("Qté.Commandée");
            table1.Columns.Add("Qté.livrée");
            table1.Columns.Add("Reste à livrer");
            table1.Columns.Add("BCD_NO");

            var rs1 = table1.NewRow();

            if (rsCmdDetail.Rows.Count > 0)
            {

                foreach (DataRow dr in rsCmdDetail.Rows)
                {
                    if (General.nz(dr["RefLigneCmd"], "") != null)
                    {

                        //Recherche des bons de livraison faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT SUM(QteLivre) as SumQteLivre " + " FROM BcmdLivraisonDetail " + " WHERE NoBcmd="
                            + dr["NoBcmd"] + " AND RefLigneCmd='"
                            + StdSQLchaine.gFr_DoublerQuote(dr["RefLigneCmd"].ToString()) + "'";

                        rsFactFournDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);
                        if (rsFactFournDetail.Rows.Count > 0)
                        {

                            rs1["NoAutoFacture"] = txtNoAutoFacture.Text;
                            rs1["NoLigne"] = dr["NoLigne"].ToString();
                            rs1["No Bon de Commande"] = dr["NoBcmd"].ToString();
                            rs1["Réf.Fourniture"] = dr["RefLigneCmd"].ToString();
                            rs1["Designation"] = dr["DesignationLigneCmd"].ToString();
                            rs1["Qté.Commandée"] = General.nz(dr["QteCommande"], "0").ToString();
                            rs1["Qté.livrée"] = General.nz(dr["QteLivre"], "0").ToString();
                            rs1["Reste à livrer"] = (Convert.ToDouble(General.nz(dr["QteCommande"], "0"))) - (Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0")));
                            rs1["BCD_NO"] = General.nz(dr["idBcmd"], "0").ToString();

                            table1.Rows.Add(rs1.ItemArray);
                            GridFactFournDetail.DataSource = table1;
                        }
                        else
                        {
                            rs1["NoAutoFacture"] = txtNoAutoFacture.Text;
                            rs1["NoLigne"] = dr["NoLigne"].ToString();
                            rs1["No Bon de Commande"] = dr["NoBcmd"].ToString();
                            rs1["Réf.Fourniture"] = dr["RefLigneCmd"].ToString();
                            rs1["Designation"] = dr["DesignationLigneCmd"].ToString();
                            rs1["Qté.Commandée"] = General.nz(dr["QteCommande"], "0").ToString();
                            rs1["Qté.livrée"] = General.nz(dr["QteLivre"], "0").ToString();
                            rs1["Reste à livrer"] = General.nz(dr["QteCommande"], "0").ToString();
                            rs1["BCD_NO"] = dr["idBcmd"].ToString();

                            table1.Rows.Add(rs1.ItemArray);
                            GridFactFournDetail.DataSource = table1;
                        }
                    }
                    else
                    {

                        //Recherche des bons de livraison faisant références au même bon de Cmd
                        sqlBcmd = "";
                        sqlBcmd = "SELECT SUM(QteLivre) as SumQteLivre " + " FROM BcmdLivraisonDetail " + " WHERE NoBcmd=" + dr["NoBcmd"] + " AND DesignationLigneCmd='" + StdSQLchaine.gFr_DoublerQuote(dr["DesignationLigneCmd"].ToString()) + "'";
                        rsFactFournDetail = ModAdors.fc_OpenRecordSet(sqlBcmd);

                        if (rsFactFournDetail.Rows.Count > 0)
                        {


                            rs1["NoAutoFacture"] = txtNoAutoFacture.Text;
                            rs1["NoLigne"] = dr["NoLigne"].ToString();
                            rs1["No Bon de Commande"] = dr["NoBcmd"].ToString();
                            rs1["Réf.Fourniture"] = dr["RefLigneCmd"].ToString();
                            rs1["Designation"] = dr["DesignationLigneCmd"].ToString();
                            rs1["Qté.Commandée"] = General.nz(dr["QteCommande"], "0").ToString();
                            rs1["Qté.livrée"] = General.nz(dr["QteLivre"], "0").ToString();
                            rs1["Reste à livrer"] = (Convert.ToDouble(General.nz(dr["QteCommande"], "0"))) - (Convert.ToDouble(General.nz(rsFactFournDetail.Rows[0]["SumQteLivre"], "0")));
                            rs1["BCD_NO"] = General.nz(dr["idBcmd"], "0").ToString();

                            table1.Rows.Add(rs1.ItemArray);
                            GridFactFournDetail.DataSource = table1;

                        }
                        else
                        {


                            rs1["NoAutoFacture"] = txtNoAutoFacture.Text;
                            rs1["NoLigne"] = dr["NoLigne"].ToString();
                            rs1["No Bon de Commande"] = dr["NoBcmd"].ToString();
                            rs1["Réf.Fourniture"] = dr["RefLigneCmd"].ToString();
                            rs1["Designation"] = dr["DesignationLigneCmd"].ToString();
                            rs1["Qté.Commandée"] = General.nz(dr["QteCommande"], "0").ToString();
                            rs1["Qté.livrée"] = General.nz(dr["QteLivre"], "0").ToString();
                            rs1["Reste à livrer"] = Convert.ToDouble(General.nz(dr["QteCommande"], "0"));
                            rs1["BCD_NO"] = General.nz(dr["idBcmd"], "0").ToString();


                            table1.Rows.Add(rs1.ItemArray);
                            GridFactFournDetail.DataSource = table1;

                        }
                    }

                    //rsFactFournDetail.Dispose();

                }
            }

            rsCmdDetail.Dispose();
            rsFactFournDetail.Dispose();

            rsCmdDetail = null;
            rsFactFournDetail = null;
            rsCmdPied = null;
            return;

        }
        //tested
        private void ChargeFactureEntete(ref string NoFacture)
        {

            string sqlFactEntete = null;
            DataTable rsEntete;

            fnc_Clear();

            if (string.IsNullOrEmpty(NoFacture) || !General.IsNumeric(NoFacture))
            {
                return;
            }

            sqlFactEntete = "SELECT * FROM BcmdLivraisonEntete WHERE NoAutoLivraison=" + NoFacture;

            rsEntete = new DataTable();

            rsEntete = ModAdors.fc_OpenRecordSet(sqlFactEntete);

            if (rsEntete.Rows.Count > 0)
            {

                txtNoAutoFacture.Text = rsEntete.Rows[0]["NoAutoLivraison"] + "";
                txtCodeFournisseur.Text = rsEntete.Rows[0]["CodeFournisseur"] + "";
                txtCodeFourn.Text = ModAdors.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE CleAuto=" + General.nz(rsEntete.Rows[0]["CodeFournisseur"], "''"));
                txtRaisonFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT RaisonSocial FROM FournisseurArticle WHERE CleAuto=" + General.nz(rsEntete.Rows[0]["CodeFournisseur"], "''"));
                txtDateLivraison.Text = Convert.ToDateTime(rsEntete.Rows[0]["DateLivraison"]).ToShortDateString() + "";
                txtNoFacture.Text = rsEntete.Rows[0]["RefBonLivraison"] + "";
                txtCommentaire.Text = rsEntete.Rows[0]["Commentaire"] + "";
                txtNbrColi.Text = rsEntete.Rows[0]["NbrColi"] + "";
                lblDateCreation.Text = rsEntete.Rows[0]["CreeLe"] + "";
                lblCreateur.Text = rsEntete.Rows[0]["CreePar"] + "";
                lblDateModif.Text = rsEntete.Rows[0]["ModifieLe"] + "";
                lblNomModif.Text = rsEntete.Rows[0]["ModifiePar"] + "";
                lblNoFacture.Text = rsEntete.Rows[0]["NoAutoLivraison"] + "";
            }

            ChargeFactureDetailPied();

            ModParametre.fc_SaveParamPosition(this.Name, this.Name, txtNoAutoFacture.Text);

            cmdRechercheRaisonSociale.Visible = false;
            return;


        }
        //tested
        private void CmdRechercher_Click(Object eventSender, EventArgs eventArgs)
        {
            string requete;
            string where_order = "";
            try
            {
                requete = "SELECT DISTINCT  BcmdLivraisonEntete.NoAutoLivraison as \"N° Bon de Livraison\", BcmdLivraisonDetail.NoBcmd as \"N° Bon de commande\",  FournisseurArticle.RaisonSocial as \"Fournisseur\", FournisseurArticle.Code as \"Code Fournisseur\" FROM BcmdLivraisonEntete "
                     + " LEFT OUTER JOIN BcmdLivraisonDetail ON BcmdLivraisonEntete.NoAutoLivraison=BcmdLivraisonDetail.NoAutoLivraison "
                     + " LEFT JOIN BonDeCommande ON BcmdLivraisonDetail.NoBcmd=BonDeCommande.NoBonDeCommande "
                     + " INNER JOIN FournisseurArticle ON BonDeCommande.CodeFourn=FournisseurArticle.CleAuto ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Rechercher un bon de livraison" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    string sCode = null;
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    ChargeFactureEntete(ref sCode);
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);

                    fg.Dispose();
                    fg.Close();
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";Command1_Click");
                return;
            }
        }
        //tested
        private void cmdRechercheRaisonSociale_Click(Object eventSender, EventArgs eventArgs)
        {

            try
            {
                string sCode = null;
                string where_order = "";

                string requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\" FROM fournisseurArticle";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un fournisseur à votre facture" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtCodeFourn.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT cleauto FROM FournisseurArticle WHERE Code='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'");
                    txtRaisonFournisseur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeFourn.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCodeFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT cleauto FROM FournisseurArticle WHERE Code='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'");
                        txtRaisonFournisseur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
                return;
            }



        }
        //tested
        private void CmdSauver_Click(Object eventSender, EventArgs eventArgs)
        {

            if (!string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                fc_Save();
            }
            else
            {
                cmdAjouter_Click(cmdAjouter, new EventArgs());
                fc_Save();
            }

            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFactFourn, txtNoAutoFacture.Text);

        }
        //tested
        private bool fc_Save()
        {

            try
            {
                lblNomModif.Text = General.gsUtilisateur;
                lblDateModif.Text = Convert.ToString(DateAndTime.Now);

                if (string.IsNullOrEmpty(txtCodeFournisseur.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un fournisseur.", "Fournisseur inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtRaisonFournisseur.Focus();

                    return false;
                }

                fc_SaveEntete();
                fc_SaveDetail();
                fc_ModifBCMD();

                cmdRechercheRaisonSociale.Visible = false;
                return true;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " Erreur fc_Save ");
                return false;
            }
        }
        //tested
        public void fc_SaveEntete()
        {

            string sqlEntete = null;
            string sqlSage = null;

            try
            {
                sqlEntete = "UPDATE BcmdLivraisonEntete SET CodeFournisseur=" + General.nz((txtCodeFournisseur.Text), "''") + ",";
                sqlEntete = sqlEntete + " RefBonLivraison='" + txtNoFacture.Text + "', ";
                sqlEntete = sqlEntete + " DateLivraison='" + General.nz(txtDateLivraison, DateAndTime.Today) + "',";
                sqlEntete = sqlEntete + " Commentaire='" + txtCommentaire.Text + "', ";
                sqlEntete = sqlEntete + " ModifieLe='" + lblDateModif.Text + "', ";
                sqlEntete = sqlEntete + " ModifiePar='" + lblNomModif.Text + "', ";
                sqlEntete = sqlEntete + " NbrColi='" + txtNbrColi.Text + "' ";
                sqlEntete = sqlEntete + " WHERE NoAutoLivraison=" + txtNoAutoFacture.Text;


                General.Execute(sqlEntete);
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "Erreur SaveEntete ");
                return;
            }
        }
        //tested
        public void fc_SaveDetail()
        {

            string sqlDetail = null;
            int i = 0;

            try
            {

                sqlDetail = "DELETE FROM BcmdLivraisonDetail WHERE BcmdLivraisonDetail.NoAutoLivraison=" + txtNoAutoFacture.Text + " ";
                General.Execute(sqlDetail);

                i = 0;

                if (GridFactFournDetail.Rows.Count > 0)
                {

                    // while (!(i == GridFactFournDetail.Rows))
                    foreach (UltraGridRow Dr in GridFactFournDetail.Rows)
                    {
                        i = i + 1;
                        sqlDetail = "INSERT INTO BcmdLivraisonDetail (NoAutoLivraison,NoLigne,NoBCmd,idBcmd,RefLigneCmd,DesignationLigneCmd,QteCommande,QteLivre) ";
                        sqlDetail = sqlDetail + " VALUES ( ";
                        sqlDetail = sqlDetail + txtNoAutoFacture.Text + ",";
                        sqlDetail = sqlDetail + "" + i + ",";
                        sqlDetail = sqlDetail + "" + Dr.Cells["No Bon de Commande"].Value + ",";
                        sqlDetail = sqlDetail + "'" + Dr.Cells["BCD_NO"].Value + "',";
                        sqlDetail = sqlDetail + "'" + StdSQLchaine.gFr_DoublerQuote(Dr.Cells["Réf.Fourniture"].Value.ToString()) + "',";
                        sqlDetail = sqlDetail + "'" + StdSQLchaine.gFr_DoublerQuote(Dr.Cells["Designation"].Value.ToString()) + "',";
                        sqlDetail = sqlDetail + "'" + Dr.Cells["Qté.Commandée"].Value + "',";
                        sqlDetail = sqlDetail + "'" + Dr.Cells["Qté.livrée"].Value + "'";
                        sqlDetail = sqlDetail + ")";


                        General.Execute(sqlDetail);

                    }
                }
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " Erreur SaveDetail ");
            }
        }
        //tested
        private void fc_ModifBCMD()
        {

            //int i = 0;
            try
            {
                //i = 0;
                if (GridFactFournPied.Rows.Count > 0)
                {

                    foreach (UltraGridRow Dr in GridFactFournPied.Rows)
                    {
                        // i = i + 1;
                        //tested
                        fnc_ModifCodeEtatBcmd(Dr.Cells["No Bon de Commande"].Value.ToString());
                    }
                }

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " Erreur SavePied ");
            }

        }
        //tested
        private void fnc_ModifCodeEtatBcmd(string NoBcmd)
        {
            string sqlBcmd = null;
            string sqlPied = null;
            string sSQL = null;
            DataTable rsModif = new DataTable();
            DataTable rsTot = new DataTable();
            bool Match = false;
            double dbTotLivree = 0;
            double dbQteCommande = 0;
            bool bLivPartiel = false;

            sqlPied = "SELECT DISTINCT idBcmd FROM BcmdLivraisonDetail" + " WHERE NoBCmd = " + NoBcmd;

            rsModif = ModAdors.fc_OpenRecordSet(sqlPied);

            //== Match est true si le bon est livrée partiellement.
            Match = false;

            foreach (DataRow r in rsModif.Rows)
            {
                if (Convert.ToInt32(General.nz(r["idBcmd"], 0)) != 0)
                {

                    //== recupere toutes lignes de livraison pour une meme ligne de commande en cas de livraison partielle.
                    sSQL = "SELECT QteCommande,QteLivre FROM BcmdLivraisonDetail WHERE idBcmd = " + General.nz(r["idBcmd"], 0);
                    rsTot = ModAdors.fc_OpenRecordSet(sSQL);
                    dbTotLivree = 0;
                    if (rsTot.Rows.Count > 0)
                    {
                        dbQteCommande = Convert.ToDouble(General.nz(rsTot.Rows[0]["QteCommande"], 0));
                        foreach (DataRow dr in rsTot.Rows)
                            //== cumul les quantités livrées.
                            dbTotLivree = dbTotLivree + Convert.ToDouble(General.nz(dr["QteLivre"], 0));


                    }

                    if (dbTotLivree < dbQteCommande && Match == false)
                    {
                        //== si les quantités livrées sont diff. des quantités commandées, on peut considerer que la livraison
                        //== n'est pas lvrée totalement.
                        Match = true;

                    }

                    //== bLivPartiel permet de controler qu il y a au moins une livraison.
                    if (dbTotLivree > 0)
                    {
                        bLivPartiel = true;
                        if (Match == true && bLivPartiel == true)
                            break; // TODO: might not be correct. Was : Exit Do
                    }

                }

            }

            rsModif.Clear();
            rsTot.Clear();
            rsTot = null;


            if (bLivPartiel == false && Match == true)
            {
                sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='NL',CodeE='CE',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;

            }
            else if (Match == true)
            {
                sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LP',CodeE='CE',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
            }
            else
            {
                sqlPied = "";
                sqlPied = "SELECT EtatFacturation FROM BonDeCommande WHERE NoBonDeCommande=" + NoBcmd;
                rsModif = ModAdors.fc_OpenRecordSet(sqlPied);
                if (rsModif.Rows.Count > 0)
                {
                    if (Strings.UCase(rsModif.Rows[0]["EtatFacturation"] + "") == "FT")
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LT',CodeE='CS',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
                    }
                    else
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LT',CodeE='CE',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
                    }
                }
                rsModif.Clear();
            }

            General.Execute(sqlBcmd);

            //if (rsModif.State == ADODB.ObjectStateEnum.adStateOpen)
            //{
            //    rsModif.Close();
            //}
            rsModif.Dispose();
            rsModif = null;

        }
        //non etuliser
        private void fnc_ModifCodeEtatBcmdOld(ref string NoBcmd)
        {
            string sqlBcmd = null;
            string sqlPied = null;
            DataTable rsModif;
            bool Match = false;
            rsModif = new DataTable();

            sqlPied = "SELECT BcmdLivraisonDetail.QteCommande AS QuantiteCommande," + " (BcmdLivraisonDetail.QteLivre) AS QuantiteLivre FROM BcmdLivraisonDetail WHERE NoBCmd=" + NoBcmd + " ";

            rsModif = ModAdors.fc_OpenRecordSet(sqlPied);
            Match = false;
            if (rsModif.Rows.Count > 0)
            {
                foreach (DataRow r in rsModif.Rows)
                {
                    if (Convert.ToDouble(General.nz(r["QuantiteCommande"], "0")) > Convert.ToDouble(General.nz(r["QuantiteLivre"], "0")))
                    {
                        Match = true;
                        break; // TODO: might not be correct. Was : Exit Do
                    }

                }
            }
            rsModif.Clear();


            if (Match == true)
            {
                sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LP',CodeE='CE',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
            }
            else
            {
                sqlPied = "";
                sqlPied = "SELECT EtatFacturation FROM BonDeCommande WHERE NoBonDeCommande=" + NoBcmd;
                rsModif = ModAdors.fc_OpenRecordSet(sqlPied);
                if (rsModif.Rows.Count > 0)
                {
                    if ((rsModif.Rows[0]["EtatFacturation"] + "") == "FT")
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LT',CodeE='CS',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
                    }
                    else
                    {
                        sqlBcmd = "UPDATE BonDeCommande SET EtatLivraison='LT',CodeE='CE',Verrou=1 WHERE NoBonDeCommande=" + NoBcmd;
                    }
                }
                rsModif.Clear();
            }

            General.Execute(sqlBcmd);

            rsModif.Dispose();
            rsModif = null;

        }
        //non etuliser
        private void fnc_CreateCptAnalytiqueAff(ref string NoBcmd, ref string CodeImmeuble)
        {
            string NoAffaire = null;
            string strReq = null;
            string strCpteAffaire = null;

            try
            {

                if (string.IsNullOrEmpty(NoBcmd))
                {
                    return;
                }
                NoAffaire = ModAdors.fc_ADOlibelle("SELECT NumFicheStandard FROM BonDeCommande WHERE NoBonDeCommande=" + NoBcmd + "");
                SAGE.VerifCreeAnalytique(NoAffaire, NoAffaire, CodeImmeuble);
                return;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " Erreur de CreateCptAnalytique ");
                return;
            }



        }
        //tested
        private void GridFactFournDetail_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            double DblDiff = 0;
            double NewValue = 0;
            int i = 0;

            var _with7 = GridFactFournDetail;
            if (General.IsNumeric(_with7.ActiveRow.Cells["Qté.Livrée"].Text))
            {
                for (i = 0; i <= General.Len(GridFactFournDetail.ActiveRow.Cells["Qté.Livrée"].Text); i++)
                {
                    if (General.Left(_with7.ActiveRow.Cells["Qté.Livrée"].Text, 1) == "0")
                    {
                        _with7.ActiveRow.Cells["Qté.Livrée"].Value = General.Right(_with7.ActiveRow.Cells["Qté.Livrée"].Text, General.Len(_with7.ActiveRow.Cells["Qté.Livrée"].Text) - 1);
                    }
                    else
                    {
                        break;
                    }
                }
                _with7.ActiveRow.Cells["Qté.Livrée"].Value = General.nz((_with7.ActiveRow.Cells["Qté.Livrée"].Text), "0");
                NewValue = Convert.ToDouble(General.nz((_with7.ActiveRow.Cells["Qté.Livrée"].Text), "0"));
            }
            else
            {
                //  Cancel = 1
                //  Exit Sub
            }

            //Qté. Livrée
            if (_with7.ActiveCell.Column.Index == _with7.ActiveRow.Cells["Qté.Livrée"].Column.Index)
            {
                if (!string.IsNullOrEmpty(_with7.ActiveRow.Cells["Qté.Livrée"].Text))
                {
                    DblDiff = NewValue - Convert.ToDouble(General.nz(_with7.ActiveRow.Cells["Qté.Livrée"].Value, "0"));
                    if (General.IsNumeric(_with7.ActiveRow.Cells["Qté.Livrée"].Text))
                    {
                        //GridFactFournDetail.Columns("Reste à livrer").Text = CDbl(nz(GridFactFournDetail.Columns("Reste à livrer").Text, "0")) - CDbl(nz(GridFactFournDetail.Columns("Qté. Livrée").Text, "0"))
                        _with7.ActiveRow.Cells["Reste à livrer"].Value = Convert.ToString(Convert.ToDouble(General.nz((_with7.ActiveRow.Cells["Reste à livrer"].Text), "0")) - DblDiff);
                    }
                    else
                    {
                        _with7.ActiveRow.Cells["Qté.Livrée"].Value = "0";
                    }

                }

            }
            //int j = 0;
            //var _with8 = GridFactFournDetail;

            ////======= controle si il existe un n° de bon de commande
            //if (string.IsNullOrEmpty(_with8.ActiveRow.Cells["No Bon de Commande"].Text))
            //{
            //    //== si aucun bon trouvé on retourne un numéro de bon dans la mesure
            //    //== du possible.
            //    j = fc_RetNoBon();
            //    if (j > 0)
            //    {
            //        _with8.ActiveRow.Cells["No Bon de Commande"].Value = j;
            //    }
            //    else
            //    {
            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un n° de bon de commande.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //        //  e.Cancel = false;
            //        //  return;
            //    }
            //}
            //Todo le colonne "Prix" et "Compte TVA" n'existent pas dans la grid dans la version du vb6 declenche un beug

            //else if (_with7.ActiveCell.Column.Index == _with7.ActiveRow.Cells["Prix"].Column.Index)
            //{
            //    if (!General.IsNumeric(_with7.ActiveRow.Cells["Prix"].Text))
            //    {
            //        _with7.ActiveRow.Cells["Prix"].Value = "0.00";
            //    }

            //}
            //else if (_with7.ActiveCell.Column.Index == _with7.ActiveRow.Cells["Compte TVA"].Column.Index)
            //{
            //    _with7.ActiveRow.Cells["Compte TVA"].Value = ModAdors.fc_ADOlibelle("SELECT TaxeCG_Num FROM CpteChargeFournisseur" + " WHERE cleAutoFournisseur=" + txtCodeFournisseur.Text + " AND CG_NUM='" + _with7.ActiveRow.Cells["Compte de Charge"].Text + "'");

            //}
        }
        //tested
        private int fc_RetNoBon()
        {
            int functionReturnValue = 0;
            //== cette procedure retourne un numéro de bon de commande si il n'y a qu'un
            //== seul bon de commande dans la grille GridFactFournPied sinon
            //== celle ci retourne 0

            int i = 0;


            i = GridFactFournPied.Rows.Count;
            if (i == 1)
            {

                functionReturnValue = Convert.ToInt16(General.nz((GridFactFournPied.ActiveRow.Cells["No Bon de Commande"].Value), 0));
            }
            return functionReturnValue;


        }
        //tested
        private void GridFactFournDetail_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int i = 0;
           // e.Row.Cells["Réf.Fourniture"].Appearance.BackColor = Color.Pink;          
            //e.Row.Cells["Réf.Fourniture"].Appearance.ForeColor = Color.White;

            if (!string.IsNullOrEmpty(e.Row.Cells["NoAutoFacture"].Text) || !string.IsNullOrEmpty(e.Row.Cells["NoLigne"].Text) ||
                !string.IsNullOrEmpty(e.Row.Cells["No Bon de Commande"].Text) || !string.IsNullOrEmpty(e.Row.Cells["Réf.Fourniture"].Text) ||
                !string.IsNullOrEmpty(e.Row.Cells["Designation"].Text) || !string.IsNullOrEmpty(e.Row.Cells["Qté.Commandée"].Text) ||
                 !string.IsNullOrEmpty(e.Row.Cells["BCD_NO"].Text)
                && string.IsNullOrEmpty(e.Row.Cells["Qté.livrée"].Text) && !string.IsNullOrEmpty(e.Row.Cells["Reste à livrer"].Text) 
                )
            {
                if (Convert.ToInt16(General.nz((e.Row.Cells["Reste à livrer"].Value), 0)) == 0 && Convert.ToInt16(General.nz((e.Row.Cells["Qté.livrée"].Value), 0)) == 0)
                {
                    //for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                    //{
                    //    e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(255, 128, 128);
                    //    e.Row.Cells[i].Appearance.ForeColor = Color.White;
                    //}
                    e.Row.Appearance.BackColor = Color.FromArgb(255, 128, 128);
                    e.Row.Appearance.ForeColor = Color.White;
                }
                else
                {                   
                    e.Row.Appearance.ResetBackColor();
                    e.Row.Appearance.ResetForeColor();                  
                }
            }
            //}

        }

        //la colonne ["Montant des achats"] n'existe pas dans la gride

        //private void GridFactFournPied_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        //{
        //    //_with7.ActiveCell.Column.Index == _with7.ActiveRow.Cells["Qté. Livrée"].Column.Index
        //    if (GridFactFournPied.ActiveCell.Column.Index == GridFactFournPied.ActiveRow.Cells["Montant des achats"].Column.Index)
        //    {
        //        if (!Information.IsNumeric(GridFactFournPied.ActiveRow.Cells["Montant des achats"].Text) || string.IsNullOrEmpty(GridFactFournPied.ActiveRow.Cells["Montant des achats"].Text))
        //        {
        //            GridFactFournPied.ActiveRow.Cells["Montant des achats"].Value = "0";
        //        }
        //    }
        //}

        //tested
        private void txtNoBcmdd_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;
            if (KeyAscii == 13)
            {
                if (!string.IsNullOrEmpty(txtNoBcmdd.Text) && General.IsNumeric(txtNoBcmdd.Text))
                {
                    if (fc_VerifAddBcmd(txtNoBcmdd.Text) == false)
                    {
                        txtNoBcmdd.Text = "";
                    }
                    ChargeDetail(txtNoBcmdd.Text);
                    txtNoBcmdd.Text = "";

                    fc_SaveEntete();
                    fc_SaveDetail();
                    ChargeFactureDetailPied();
                    fc_ModifBCMD();

                }
                KeyAscii = 0;
            }

            //eventArgs.KeyChar = Strings.Chr(KeyAscii);
            //if (KeyAscii == 0)
            //{
            //    eventArgs.Handled = true;
            //}
        }
        //tested
        private bool fc_VerifAddBcmd(string NoBcmd)
        {

            string strRequete = null;
            DataTable rsAddBcmd;
            string strCdFourn = null;

            if (string.IsNullOrEmpty(txtNoAutoFacture.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillesz d'abord cliquer sur le bouton ajouter pour " + " créer une nouvelle fiche de réception de marchandise.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }

            strRequete = "SELECT DISTINCT BonDeCommande.CodeE,BonDeCommande.EtatLivraison,CodeFourn FROM BonDeCommande ";

            strRequete = strRequete + " LEFT JOIN BCmdLivraisonDetail ON " + " BonDeCommande.NoBonDeCommande=BCmdLivraisonDetail.NoBCmd ";

            strRequete = strRequete + " WHERE (NoBonDeCommande=" + NoBcmd + ") " + " AND (BCmdLivraisonDetail.NoAutoLivraison<>" + txtNoAutoFacture.Text + " OR BCmdLivraisonDetail.NoAutoLivraison IS NULL)";

            rsAddBcmd = new DataTable();
            rsAddBcmd = ModAdors.fc_OpenRecordSet(strRequete);

            if (rsAddBcmd.Rows.Count > 0)
            {

                strCdFourn = ModAdors.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE cleAuto=" + General.nz(rsAddBcmd.Rows[0]["CodeFourn"], "0"));
                if (string.IsNullOrEmpty(txtCodeFourn.Text))
                {
                    fc_ChargeFournisseur(strCdFourn);
                }
                else if (txtCodeFourn.Text != strCdFourn)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter n'appartient pas au fournisseur que vous avez déjà saisi (" + txtRaisonFournisseur.Text + ")" + Constants.vbCrLf + "Ce bon de commande ne peut donc pas être ajouté", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return false;
                }


                if ((rsAddBcmd.Rows[0]["CodeE"] + "") == "CS")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est soldé." + Constants.vbCrLf + "Ce bon ne peut donc pas être ajouté.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return false;
                }

                if ((rsAddBcmd.Rows[0]["CodeE"] + "") == "CA")
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est en cours de saisie." + Constants.vbCrLf + "Souhaitez-vous l'ajouter quand même ?", "Ajout d'un bon de commande", MessageBoxButtons.YesNo) == DialogResult.No)
                    {

                        return false;
                    }
                }

                if ((rsAddBcmd.Rows[0]["EtatLivraison"] + "") == "LT")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le bon de commande que vous souhaitez ajouter est livré en totalité." + Constants.vbCrLf + "Ce bon ne peut donc pas être ajouté.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    return false;
                }

                return true;
            }
            else
            {
                if (ModAdors.fc_ADOlibelle("SELECT BCmdLivraisonDetail.NoBcmd FROM BCmdLivraisonDetail WHERE NoAutoLivraison=" + txtNoAutoFacture.Text + " AND NoBCmd=" + NoBcmd + "") != NoBcmd)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce bon de comande n'existe pas.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce bon de commande est déjà inséré dans votre bon de livraison.", "Ajout d'un bon de commande", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return false;
            }

            //ModAdors.Dispose();
            rsAddBcmd = null;


        }
        //tested
        private void fc_ChargeFournisseur(string strCodeFourn)
        {


            txtCodeFourn.Text = strCodeFourn;
            txtCodeFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT cleauto FROM FournisseurArticle WHERE Code='" + strCodeFourn + "'");
            txtRaisonFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT RaisonSocial FROM FournisseurArticle WHERE Code='" + strCodeFourn + "'");
            txtCompteFournisseur.Text = ModAdors.fc_ADOlibelle("SELECT NCompte FROM FournisseurArticle WHERE Cleauto=" + txtCodeFournisseur.Text + "");

        }
        //tested
        private void UserReceptMarchandise_VisibleChanged(object sender, EventArgs e)
        {

            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserReceptMarchandise");
            General.gsUtilisateur = General.fncUserName();
            General.open_conn();

            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                ChargeFactureEntete(ref ModParametre.sNewVar);
            }

        }
        //tested
        private void GridFactFournDetail_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.GridFactFournDetail.DisplayLayout.Bands[0].Columns["NoAutoFacture"].Hidden = true;
            this.GridFactFournDetail.DisplayLayout.Bands[0].Columns["NoLigne"].Hidden = true;
            this.GridFactFournDetail.DisplayLayout.Bands[0].Columns["BCD_NO"].Hidden = true;
            this.GridFactFournDetail.DisplayLayout.Bands[0].Columns["Qté.Commandée"].CellActivation = Activation.NoEdit;

        }
        //tested
        private void GridFactFournPied_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            this.GridFactFournPied.DisplayLayout.Bands[0].Columns["Date Livraison"].Hidden = true;
            this.GridFactFournPied.DisplayLayout.Bands[0].Columns["No Bon De Commande"].CellActivation = Activation.NoEdit;
            this.GridFactFournPied.DisplayLayout.Bands[0].Columns["Code Immeuble"].CellActivation = Activation.NoEdit;
            this.GridFactFournPied.DisplayLayout.Bands[0].Columns["Date Livraison"].CellActivation = Activation.NoEdit;
            this.GridFactFournPied.DisplayLayout.Bands[0].Columns["Acheteur"].CellActivation = Activation.NoEdit;
        }

        private void lblGoFournisseur_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.saveInReg("UserBCmdHead", "NoFNS", txtCodeFourn.Text);
            ModParametre.fc_SaveParamPosition(this, Variable.Cuserdocfourn);
            View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
        }

        private void GridFactFournDetail_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            int j = 0;
            var _with8 = GridFactFournDetail;

            //======= controle si il existe un n° de bon de commande
            if (string.IsNullOrEmpty(e.Row.Cells["No Bon de Commande"].Text))
            {
                //== si aucun bon trouvé on retourne un numéro de bon dans la mesure
                //== du possible.
                j = fc_RetNoBon();
                if (j > 0)
                {
                    e.Row.Cells["No Bon de Commande"].Value = j;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un n° de bon de commande.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                     e.Cancel = true;
                     return;
                }
            }
        }
    }
}
