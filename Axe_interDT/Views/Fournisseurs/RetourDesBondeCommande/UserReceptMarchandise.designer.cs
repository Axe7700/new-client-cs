namespace Axe_interDT.Views.Fournisseurs.RetourDesBondeCommande
{
    partial class UserReceptMarchandise
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtCompteFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeFourn = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblDateCreation = new System.Windows.Forms.Label();
            this.lblDateModif = new System.Windows.Forms.Label();
            this.lblCreateur = new System.Windows.Forms.Label();
            this.lblNomModif = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheRaisonSociale = new System.Windows.Forms.Button();
            this.txtRaisonFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.lblGoFournisseur = new System.Windows.Forms.LinkLabel();
            this.txtDateLivraison = new iTalk.iTalk_TextBox_Small2();
            this.txtNoFacture = new iTalk.iTalk_TextBox_Small2();
            this.txtNbrColi = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCommentaire = new iTalk.iTalk_RichTextBox();
            this.txtNoAutoFacture = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.GridFactFournPied = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdEnleverUnBcmd = new System.Windows.Forms.Button();
            this.cmdRechercheBCMD = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNoBcmdd = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblNoFacture = new System.Windows.Forms.Label();
            this.ultraPanel2 = new Infragistics.Win.Misc.UltraPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.GridFactFournDetail = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.txtNoBcmd = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournPied)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.ultraPanel2.ClientArea.SuspendLayout();
            this.ultraPanel2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournDetail)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdEditer);
            this.flowLayoutPanel1.Controls.Add(this.cmdSupprimer);
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(925, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(925, 45);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdEditer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdEditer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.CmdEditer.Location = new System.Drawing.Point(2, 2);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(60, 35);
            this.CmdEditer.TabIndex = 0;
            this.CmdEditer.Tag = "";
            this.CmdEditer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdEditer, "Imprimer");
            this.CmdEditer.UseVisualStyleBackColor = false;
            this.CmdEditer.Visible = false;
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(66, 2);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 1;
            this.cmdSupprimer.Tag = "";
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSupprimer, "Supprimer");
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(130, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 2;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(194, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 3;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(258, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 4;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(322, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 5;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDateCreation, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDateModif, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblCreateur, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblNomModif, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(1, 46);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1848, 91);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 27);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(180, 27);
            this.label2.TabIndex = 388;
            this.label2.Text = "Modifiée le :";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(180, 27);
            this.label33.TabIndex = 0;
            this.label33.Text = "Créée le :";
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.txtCompteFournisseur);
            this.flowLayoutPanel2.Controls.Add(this.txtCodeFourn);
            this.flowLayoutPanel2.Controls.Add(this.txtCodeFournisseur);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(3, 57);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(180, 31);
            this.flowLayoutPanel2.TabIndex = 389;
            this.flowLayoutPanel2.Visible = false;
            // 
            // txtCompteFournisseur
            // 
            this.txtCompteFournisseur.AccAcceptNumbersOnly = false;
            this.txtCompteFournisseur.AccAllowComma = false;
            this.txtCompteFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCompteFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCompteFournisseur.AccHidenValue = "";
            this.txtCompteFournisseur.AccNotAllowedChars = null;
            this.txtCompteFournisseur.AccReadOnly = false;
            this.txtCompteFournisseur.AccReadOnlyAllowDelete = false;
            this.txtCompteFournisseur.AccRequired = false;
            this.txtCompteFournisseur.BackColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtCompteFournisseur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCompteFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtCompteFournisseur.Location = new System.Drawing.Point(2, 2);
            this.txtCompteFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtCompteFournisseur.MaxLength = 32767;
            this.txtCompteFournisseur.Multiline = false;
            this.txtCompteFournisseur.Name = "txtCompteFournisseur";
            this.txtCompteFournisseur.ReadOnly = false;
            this.txtCompteFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCompteFournisseur.Size = new System.Drawing.Size(56, 27);
            this.txtCompteFournisseur.TabIndex = 0;
            this.txtCompteFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCompteFournisseur.UseSystemPasswordChar = false;
            this.txtCompteFournisseur.Visible = false;
            // 
            // txtCodeFourn
            // 
            this.txtCodeFourn.AccAcceptNumbersOnly = false;
            this.txtCodeFourn.AccAllowComma = false;
            this.txtCodeFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFourn.AccHidenValue = "";
            this.txtCodeFourn.AccNotAllowedChars = null;
            this.txtCodeFourn.AccReadOnly = false;
            this.txtCodeFourn.AccReadOnlyAllowDelete = false;
            this.txtCodeFourn.AccRequired = false;
            this.txtCodeFourn.BackColor = System.Drawing.Color.White;
            this.txtCodeFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFourn.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeFourn.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFourn.Location = new System.Drawing.Point(62, 2);
            this.txtCodeFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFourn.MaxLength = 32767;
            this.txtCodeFourn.Multiline = false;
            this.txtCodeFourn.Name = "txtCodeFourn";
            this.txtCodeFourn.ReadOnly = false;
            this.txtCodeFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFourn.Size = new System.Drawing.Size(56, 27);
            this.txtCodeFourn.TabIndex = 1;
            this.txtCodeFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFourn.UseSystemPasswordChar = false;
            this.txtCodeFourn.Visible = false;
            // 
            // txtCodeFournisseur
            // 
            this.txtCodeFournisseur.AccAcceptNumbersOnly = false;
            this.txtCodeFournisseur.AccAllowComma = false;
            this.txtCodeFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFournisseur.AccHidenValue = "";
            this.txtCodeFournisseur.AccNotAllowedChars = null;
            this.txtCodeFournisseur.AccReadOnly = false;
            this.txtCodeFournisseur.AccReadOnlyAllowDelete = false;
            this.txtCodeFournisseur.AccRequired = false;
            this.txtCodeFournisseur.BackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFournisseur.Location = new System.Drawing.Point(122, 2);
            this.txtCodeFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFournisseur.MaxLength = 32767;
            this.txtCodeFournisseur.Multiline = false;
            this.txtCodeFournisseur.Name = "txtCodeFournisseur";
            this.txtCodeFournisseur.ReadOnly = false;
            this.txtCodeFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFournisseur.Size = new System.Drawing.Size(56, 27);
            this.txtCodeFournisseur.TabIndex = 2;
            this.txtCodeFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFournisseur.UseSystemPasswordChar = false;
            this.txtCodeFournisseur.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(997, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(39, 27);
            this.label1.TabIndex = 385;
            this.label1.Text = "Par :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(997, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 27);
            this.label3.TabIndex = 387;
            this.label3.Text = "Par :";
            // 
            // lblDateCreation
            // 
            this.lblDateCreation.AutoSize = true;
            this.lblDateCreation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateCreation.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateCreation.Location = new System.Drawing.Point(189, 0);
            this.lblDateCreation.Name = "lblDateCreation";
            this.lblDateCreation.Size = new System.Drawing.Size(802, 27);
            this.lblDateCreation.TabIndex = 1;
            // 
            // lblDateModif
            // 
            this.lblDateModif.AutoSize = true;
            this.lblDateModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateModif.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateModif.Location = new System.Drawing.Point(189, 27);
            this.lblDateModif.Name = "lblDateModif";
            this.lblDateModif.Size = new System.Drawing.Size(802, 27);
            this.lblDateModif.TabIndex = 2;
            // 
            // lblCreateur
            // 
            this.lblCreateur.AutoSize = true;
            this.lblCreateur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCreateur.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCreateur.Location = new System.Drawing.Point(1041, 2);
            this.lblCreateur.Margin = new System.Windows.Forms.Padding(2);
            this.lblCreateur.Name = "lblCreateur";
            this.lblCreateur.Size = new System.Drawing.Size(805, 23);
            this.lblCreateur.TabIndex = 3;
            // 
            // lblNomModif
            // 
            this.lblNomModif.AutoSize = true;
            this.lblNomModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNomModif.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNomModif.Location = new System.Drawing.Point(1041, 29);
            this.lblNomModif.Margin = new System.Windows.Forms.Padding(2);
            this.lblNomModif.Name = "lblNomModif";
            this.lblNomModif.Size = new System.Drawing.Size(805, 23);
            this.lblNomModif.TabIndex = 4;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 5;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheRaisonSociale, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtRaisonFournisseur, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label6, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label4, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblGoFournisseur, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtDateLivraison, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtNoFacture, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtNbrColi, 4, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 138);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1850, 64);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // cmdRechercheRaisonSociale
            // 
            this.cmdRechercheRaisonSociale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheRaisonSociale.FlatAppearance.BorderSize = 0;
            this.cmdRechercheRaisonSociale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheRaisonSociale.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheRaisonSociale.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheRaisonSociale.Location = new System.Drawing.Point(866, 0);
            this.cmdRechercheRaisonSociale.Margin = new System.Windows.Forms.Padding(0);
            this.cmdRechercheRaisonSociale.Name = "cmdRechercheRaisonSociale";
            this.cmdRechercheRaisonSociale.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheRaisonSociale.TabIndex = 506;
            this.cmdRechercheRaisonSociale.UseVisualStyleBackColor = false;
            this.cmdRechercheRaisonSociale.Click += new System.EventHandler(this.cmdRechercheRaisonSociale_Click);
            // 
            // txtRaisonFournisseur
            // 
            this.txtRaisonFournisseur.AccAcceptNumbersOnly = false;
            this.txtRaisonFournisseur.AccAllowComma = false;
            this.txtRaisonFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRaisonFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRaisonFournisseur.AccHidenValue = "";
            this.txtRaisonFournisseur.AccNotAllowedChars = null;
            this.txtRaisonFournisseur.AccReadOnly = false;
            this.txtRaisonFournisseur.AccReadOnlyAllowDelete = false;
            this.txtRaisonFournisseur.AccRequired = false;
            this.txtRaisonFournisseur.BackColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtRaisonFournisseur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRaisonFournisseur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtRaisonFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtRaisonFournisseur.Location = new System.Drawing.Point(131, 2);
            this.txtRaisonFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtRaisonFournisseur.MaxLength = 100;
            this.txtRaisonFournisseur.Multiline = false;
            this.txtRaisonFournisseur.Name = "txtRaisonFournisseur";
            this.txtRaisonFournisseur.ReadOnly = false;
            this.txtRaisonFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRaisonFournisseur.Size = new System.Drawing.Size(733, 27);
            this.txtRaisonFournisseur.TabIndex = 100;
            this.txtRaisonFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRaisonFournisseur.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(123, 34);
            this.label5.TabIndex = 385;
            this.label5.Text = "Date de livraison";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(897, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(213, 30);
            this.label6.TabIndex = 0;
            this.label6.Text = "Référence du bon de livraison";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(897, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 34);
            this.label4.TabIndex = 4;
            this.label4.Text = "Nombre de colis";
            // 
            // lblGoFournisseur
            // 
            this.lblGoFournisseur.AutoSize = true;
            this.lblGoFournisseur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoFournisseur.Location = new System.Drawing.Point(3, 0);
            this.lblGoFournisseur.Name = "lblGoFournisseur";
            this.lblGoFournisseur.Size = new System.Drawing.Size(123, 30);
            this.lblGoFournisseur.TabIndex = 387;
            this.lblGoFournisseur.TabStop = true;
            this.lblGoFournisseur.Text = "Fournisseur";
            this.lblGoFournisseur.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoFournisseur_LinkClicked);
            // 
            // txtDateLivraison
            // 
            this.txtDateLivraison.AccAcceptNumbersOnly = false;
            this.txtDateLivraison.AccAllowComma = false;
            this.txtDateLivraison.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateLivraison.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateLivraison.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateLivraison.AccHidenValue = "";
            this.txtDateLivraison.AccNotAllowedChars = null;
            this.txtDateLivraison.AccReadOnly = false;
            this.txtDateLivraison.AccReadOnlyAllowDelete = false;
            this.txtDateLivraison.AccRequired = false;
            this.txtDateLivraison.BackColor = System.Drawing.Color.White;
            this.txtDateLivraison.CustomBackColor = System.Drawing.Color.White;
            this.txtDateLivraison.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateLivraison.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateLivraison.ForeColor = System.Drawing.Color.Black;
            this.txtDateLivraison.Location = new System.Drawing.Point(131, 32);
            this.txtDateLivraison.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateLivraison.MaxLength = 32767;
            this.txtDateLivraison.Multiline = false;
            this.txtDateLivraison.Name = "txtDateLivraison";
            this.txtDateLivraison.ReadOnly = false;
            this.txtDateLivraison.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateLivraison.Size = new System.Drawing.Size(733, 27);
            this.txtDateLivraison.TabIndex = 101;
            this.txtDateLivraison.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateLivraison.UseSystemPasswordChar = false;
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.AccAcceptNumbersOnly = false;
            this.txtNoFacture.AccAllowComma = false;
            this.txtNoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoFacture.AccHidenValue = "";
            this.txtNoFacture.AccNotAllowedChars = null;
            this.txtNoFacture.AccReadOnly = false;
            this.txtNoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoFacture.AccRequired = false;
            this.txtNoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoFacture.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoFacture.Location = new System.Drawing.Point(1115, 2);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoFacture.MaxLength = 100;
            this.txtNoFacture.Multiline = false;
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.ReadOnly = false;
            this.txtNoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoFacture.Size = new System.Drawing.Size(733, 27);
            this.txtNoFacture.TabIndex = 102;
            this.txtNoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoFacture.UseSystemPasswordChar = false;
            // 
            // txtNbrColi
            // 
            this.txtNbrColi.AccAcceptNumbersOnly = false;
            this.txtNbrColi.AccAllowComma = false;
            this.txtNbrColi.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNbrColi.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNbrColi.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNbrColi.AccHidenValue = "";
            this.txtNbrColi.AccNotAllowedChars = null;
            this.txtNbrColi.AccReadOnly = false;
            this.txtNbrColi.AccReadOnlyAllowDelete = false;
            this.txtNbrColi.AccRequired = false;
            this.txtNbrColi.BackColor = System.Drawing.Color.White;
            this.txtNbrColi.CustomBackColor = System.Drawing.Color.White;
            this.txtNbrColi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNbrColi.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNbrColi.ForeColor = System.Drawing.Color.Black;
            this.txtNbrColi.Location = new System.Drawing.Point(1115, 32);
            this.txtNbrColi.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbrColi.MaxLength = 32767;
            this.txtNbrColi.Multiline = false;
            this.txtNbrColi.Name = "txtNbrColi";
            this.txtNbrColi.ReadOnly = false;
            this.txtNbrColi.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNbrColi.Size = new System.Drawing.Size(733, 27);
            this.txtNbrColi.TabIndex = 103;
            this.txtNbrColi.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNbrColi.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 129F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtCommentaire, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtNoAutoFacture, 4, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 202);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1850, 54);
            this.tableLayoutPanel3.TabIndex = 3;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(123, 54);
            this.label7.TabIndex = 384;
            this.label7.Text = "Commentaire";
            // 
            // txtCommentaire
            // 
            this.txtCommentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaire.AutoWordSelection = false;
            this.txtCommentaire.BackColor = System.Drawing.Color.Transparent;
            this.txtCommentaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCommentaire.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaire.Location = new System.Drawing.Point(132, 3);
            this.txtCommentaire.Name = "txtCommentaire";
            this.txtCommentaire.ReadOnly = false;
            this.txtCommentaire.Size = new System.Drawing.Size(731, 48);
            this.txtCommentaire.TabIndex = 0;
            this.txtCommentaire.WordWrap = true;
            // 
            // txtNoAutoFacture
            // 
            this.txtNoAutoFacture.AccAcceptNumbersOnly = false;
            this.txtNoAutoFacture.AccAllowComma = false;
            this.txtNoAutoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoAutoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoAutoFacture.AccHidenValue = "";
            this.txtNoAutoFacture.AccNotAllowedChars = null;
            this.txtNoAutoFacture.AccReadOnly = false;
            this.txtNoAutoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoAutoFacture.AccRequired = false;
            this.txtNoAutoFacture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNoAutoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoAutoFacture.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoAutoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoAutoFacture.Location = new System.Drawing.Point(1115, 25);
            this.txtNoAutoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoAutoFacture.MaxLength = 32767;
            this.txtNoAutoFacture.Multiline = false;
            this.txtNoAutoFacture.Name = "txtNoAutoFacture";
            this.txtNoAutoFacture.ReadOnly = false;
            this.txtNoAutoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoAutoFacture.Size = new System.Drawing.Size(127, 27);
            this.txtNoAutoFacture.TabIndex = 1;
            this.txtNoAutoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoAutoFacture.UseSystemPasswordChar = false;
            this.txtNoAutoFacture.Visible = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.75709F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 502F));
            this.tableLayoutPanel4.Controls.Add(this.GridFactFournPied, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 259);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 161F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1844, 161);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // GridFactFournPied
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridFactFournPied.DisplayLayout.Appearance = appearance1;
            this.GridFactFournPied.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridFactFournPied.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridFactFournPied.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournPied.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridFactFournPied.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournPied.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridFactFournPied.DisplayLayout.MaxColScrollRegions = 1;
            this.GridFactFournPied.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridFactFournPied.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridFactFournPied.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridFactFournPied.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridFactFournPied.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridFactFournPied.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridFactFournPied.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridFactFournPied.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridFactFournPied.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridFactFournPied.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridFactFournPied.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournPied.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridFactFournPied.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridFactFournPied.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridFactFournPied.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridFactFournPied.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridFactFournPied.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridFactFournPied.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridFactFournPied.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridFactFournPied.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridFactFournPied.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridFactFournPied.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridFactFournPied.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.GridFactFournPied.Location = new System.Drawing.Point(1345, 3);
            this.GridFactFournPied.Name = "GridFactFournPied";
            this.GridFactFournPied.Size = new System.Drawing.Size(496, 155);
            this.GridFactFournPied.TabIndex = 1;
            this.GridFactFournPied.Text = "GridFactFournPied";
            this.GridFactFournPied.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridFactFournPied_InitializeLayout);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.cmdEnleverUnBcmd, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.cmdRechercheBCMD, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1336, 155);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // cmdEnleverUnBcmd
            // 
            this.cmdEnleverUnBcmd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEnleverUnBcmd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdEnleverUnBcmd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdEnleverUnBcmd.FlatAppearance.BorderSize = 0;
            this.cmdEnleverUnBcmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEnleverUnBcmd.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdEnleverUnBcmd.ForeColor = System.Drawing.Color.White;
            this.cmdEnleverUnBcmd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdEnleverUnBcmd.Location = new System.Drawing.Point(2, 99);
            this.cmdEnleverUnBcmd.Margin = new System.Windows.Forms.Padding(2);
            this.cmdEnleverUnBcmd.Name = "cmdEnleverUnBcmd";
            this.cmdEnleverUnBcmd.Size = new System.Drawing.Size(1332, 54);
            this.cmdEnleverUnBcmd.TabIndex = 1;
            this.cmdEnleverUnBcmd.Text = "Enlever un bon de commande";
            this.cmdEnleverUnBcmd.UseVisualStyleBackColor = false;
            this.cmdEnleverUnBcmd.Click += new System.EventHandler(this.cmdEnleverUnBcmd_Click);
            // 
            // cmdRechercheBCMD
            // 
            this.cmdRechercheBCMD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRechercheBCMD.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRechercheBCMD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdRechercheBCMD.FlatAppearance.BorderSize = 0;
            this.cmdRechercheBCMD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheBCMD.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdRechercheBCMD.ForeColor = System.Drawing.Color.White;
            this.cmdRechercheBCMD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRechercheBCMD.Location = new System.Drawing.Point(2, 2);
            this.cmdRechercheBCMD.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRechercheBCMD.Name = "cmdRechercheBCMD";
            this.cmdRechercheBCMD.Size = new System.Drawing.Size(1332, 26);
            this.cmdRechercheBCMD.TabIndex = 2;
            this.cmdRechercheBCMD.Text = "Recherche des bons de commande";
            this.cmdRechercheBCMD.UseVisualStyleBackColor = false;
            this.cmdRechercheBCMD.Click += new System.EventHandler(this.cmdRechercheBcmd_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.txtNoBcmdd, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1336, 67);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // txtNoBcmdd
            // 
            this.txtNoBcmdd.AccAcceptNumbersOnly = false;
            this.txtNoBcmdd.AccAllowComma = false;
            this.txtNoBcmdd.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBcmdd.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBcmdd.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBcmdd.AccHidenValue = "";
            this.txtNoBcmdd.AccNotAllowedChars = null;
            this.txtNoBcmdd.AccReadOnly = false;
            this.txtNoBcmdd.AccReadOnlyAllowDelete = false;
            this.txtNoBcmdd.AccRequired = false;
            this.txtNoBcmdd.BackColor = System.Drawing.Color.White;
            this.txtNoBcmdd.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBcmdd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoBcmdd.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBcmdd.ForeColor = System.Drawing.Color.Black;
            this.txtNoBcmdd.Location = new System.Drawing.Point(670, 2);
            this.txtNoBcmdd.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBcmdd.MaxLength = 32767;
            this.txtNoBcmdd.Multiline = false;
            this.txtNoBcmdd.Name = "txtNoBcmdd";
            this.txtNoBcmdd.ReadOnly = false;
            this.txtNoBcmdd.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBcmdd.Size = new System.Drawing.Size(664, 27);
            this.txtNoBcmdd.TabIndex = 0;
            this.txtNoBcmdd.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBcmdd.UseSystemPasswordChar = false;
            this.txtNoBcmdd.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoBcmdd_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(662, 67);
            this.label8.TabIndex = 1;
            this.label8.Text = "Ajouter le bon de commande N° : (Valider par la touche Entrée)";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(56, 19);
            this.label9.TabIndex = 0;
            this.label9.Text = "Bon N°";
            // 
            // lblNoFacture
            // 
            this.lblNoFacture.AutoSize = true;
            this.lblNoFacture.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNoFacture.Location = new System.Drawing.Point(94, 0);
            this.lblNoFacture.Name = "lblNoFacture";
            this.lblNoFacture.Size = new System.Drawing.Size(0, 19);
            this.lblNoFacture.TabIndex = 1;
            // 
            // ultraPanel2
            // 
            // 
            // ultraPanel2.ClientArea
            // 
            this.ultraPanel2.ClientArea.Controls.Add(this.label9);
            this.ultraPanel2.ClientArea.Controls.Add(this.lblNoFacture);
            this.ultraPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraPanel2.Font = new System.Drawing.Font("Ubuntu", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraPanel2.Location = new System.Drawing.Point(3, 3);
            this.ultraPanel2.Name = "ultraPanel2";
            this.ultraPanel2.Size = new System.Drawing.Size(919, 39);
            this.ultraPanel2.TabIndex = 396;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.GridFactFournDetail, 0, 5);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel4, 0, 4);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel8, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 6;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 64F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 167F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel7.TabIndex = 397;
            // 
            // GridFactFournDetail
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridFactFournDetail.DisplayLayout.Appearance = appearance13;
            this.GridFactFournDetail.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridFactFournDetail.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridFactFournDetail.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridFactFournDetail.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridFactFournDetail.DisplayLayout.MaxColScrollRegions = 1;
            this.GridFactFournDetail.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridFactFournDetail.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridFactFournDetail.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridFactFournDetail.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridFactFournDetail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridFactFournDetail.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridFactFournDetail.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridFactFournDetail.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridFactFournDetail.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridFactFournDetail.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridFactFournDetail.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridFactFournDetail.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridFactFournDetail.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridFactFournDetail.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridFactFournDetail.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridFactFournDetail.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridFactFournDetail.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridFactFournDetail.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridFactFournDetail.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridFactFournDetail.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridFactFournDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridFactFournDetail.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.GridFactFournDetail.Location = new System.Drawing.Point(3, 426);
            this.GridFactFournDetail.Name = "GridFactFournDetail";
            this.GridFactFournDetail.Size = new System.Drawing.Size(1844, 528);
            this.GridFactFournDetail.TabIndex = 1;
            this.GridFactFournDetail.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridFactFournDetail_InitializeLayout);
            this.GridFactFournDetail.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridFactFournDetail_InitializeRow);
            this.GridFactFournDetail.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridFactFournDetail_BeforeRowUpdate);
            this.GridFactFournDetail.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridFactFournDetail_BeforeExitEditMode);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.ultraPanel2, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1850, 45);
            this.tableLayoutPanel8.TabIndex = 4;
            // 
            // txtNoBcmd
            // 
            this.txtNoBcmd.AccAcceptNumbersOnly = false;
            this.txtNoBcmd.AccAllowComma = false;
            this.txtNoBcmd.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoBcmd.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoBcmd.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoBcmd.AccHidenValue = "";
            this.txtNoBcmd.AccNotAllowedChars = null;
            this.txtNoBcmd.AccReadOnly = false;
            this.txtNoBcmd.AccReadOnlyAllowDelete = false;
            this.txtNoBcmd.AccRequired = false;
            this.txtNoBcmd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtNoBcmd.BackColor = System.Drawing.Color.White;
            this.txtNoBcmd.CustomBackColor = System.Drawing.Color.White;
            this.txtNoBcmd.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoBcmd.ForeColor = System.Drawing.Color.Black;
            this.txtNoBcmd.Location = new System.Drawing.Point(174, 2);
            this.txtNoBcmd.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoBcmd.MaxLength = 32767;
            this.txtNoBcmd.Multiline = false;
            this.txtNoBcmd.Name = "txtNoBcmd";
            this.txtNoBcmd.ReadOnly = false;
            this.txtNoBcmd.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoBcmd.Size = new System.Drawing.Size(65, 27);
            this.txtNoBcmd.TabIndex = 505;
            this.txtNoBcmd.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoBcmd.UseSystemPasswordChar = false;
            // 
            // UserReceptMarchandise
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tableLayoutPanel7);
            this.Name = "UserReceptMarchandise";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Réception de marchandise";
            this.VisibleChanged += new System.EventHandler(this.UserReceptMarchandise_VisibleChanged);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournPied)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ultraPanel2.ClientArea.ResumeLayout(false);
            this.ultraPanel2.ClientArea.PerformLayout();
            this.ultraPanel2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridFactFournDetail)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button CmdEditer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 txtCompteFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtCodeFourn;
        public iTalk.iTalk_TextBox_Small2 txtCodeFournisseur;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.LinkLabel lblGoFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtRaisonFournisseur;
        public iTalk.iTalk_TextBox_Small2 txtDateLivraison;
        public iTalk.iTalk_TextBox_Small2 txtNoFacture;
        public iTalk.iTalk_TextBox_Small2 txtNbrColi;
        public System.Windows.Forms.Button cmdRechercheRaisonSociale;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label7;
        private iTalk.iTalk_RichTextBox txtCommentaire;
        public iTalk.iTalk_TextBox_Small2 txtNoAutoFacture;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridFactFournPied;
        public System.Windows.Forms.Button cmdRechercheBCMD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtNoBcmd;
        public System.Windows.Forms.Button cmdEnleverUnBcmd;
        public System.Windows.Forms.Label lblDateCreation;
        public System.Windows.Forms.Label lblDateModif;
        public System.Windows.Forms.Label lblCreateur;
        public System.Windows.Forms.Label lblNomModif;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblNoFacture;
        private Infragistics.Win.Misc.UltraPanel ultraPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridFactFournDetail;
        public iTalk.iTalk_TextBox_Small2 txtNoBcmdd;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
    }
}
