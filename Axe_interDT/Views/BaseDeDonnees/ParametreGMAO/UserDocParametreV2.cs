﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
//using Microsoft.VisualBasic;
//using Microsoft.VisualBasic;
using BeforeExitEditModeEventArgs = Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.BaseDeDonnees.ParametreGMAO
{
    public partial class UserDocParametreV2 : UserControl
    {
        DataTable rsCAI;
        DataTable rsGAM;
        ModAdo ModAdoGam = null;
        ModAdo ModAdoEQU = null;
        ModAdo ModAdoCAI = null;
        ModAdo ModAdoCTR = null;
        ModAdo ModAdoMOY = null;
        ModAdo ModAdoANO = null;
        ModAdo ModAdoOPE = null;
        DataTable rsEQU;
        DataTable rsPEG;
        DataTable rsFAC;
        DataTable rsMAE;
        DataTable rsTPT;
        DataTable rsMATF;
        DataTable rsMarque;
        DataTable rsCtr;
        DataTable rsMOY;
        DataTable rsANO;
        DataTable rsOPE;


        int lFAC_NoLigne;
        int lCAI;
        int lEQU;
        int lGAI;
        string sOrderByTypeVisite;
        string sOrderByGamme;

        string sAscDesc;
        const short cColWidth = 6000;
        public UserDocParametreV2()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdANO_Click(object sender, EventArgs e)//TESTED
        {
            fc_LoadANO();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadANO()//TESTED
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "SELECT     ANO_ID, ANO_Libelle" + " From ANO_AnomalieGmao ";

                if (!string.IsNullOrEmpty(Text2.Text))
                {
                    sSQL = sSQL + " WHERE ANO_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "'";
                }

                sSQL = sSQL + " ORDER BY ANO_Libelle ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAdoANO = new ModAdo();
                rsANO = ModAdoANO.fc_OpenRecordSet(sSQL, GridANO, "ANO_ID");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridANO.DataSource = rsANO;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadANO");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdCTR_Click(object sender, EventArgs e)//TESTED
        {
            fc_LoadCTR();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadCTR()//TESTED
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     CTR_ID, CTR_Libelle" + " From CTR_ControleGmao ";

                if (!string.IsNullOrEmpty(Text1.Text))
                {
                    sSQL = sSQL + " WHERE CTR_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text1.Text) + "'";
                }

                sSQL = sSQL + " ORDER BY CTR_Libelle";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAdoCTR = new ModAdo();
                rsCtr = ModAdoCTR.fc_OpenRecordSet(sSQL, GridCTR, "CTR_ID");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                GridCTR.DataSource = rsCtr;
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadCTR");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMOY_Click(object sender, EventArgs e)//TESTED
        {
            fc_LoadMoy();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadMoy()//TESTED
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     MOY_ID, MOY_Libelle From MOY_MoyenGmao ";

                if (!string.IsNullOrEmpty(Text3.Text))
                {
                    sSQL = sSQL + " WHERE MOY_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(Text3.Text) + "'";
                }

                sSQL = sSQL + " ORDER BY MOY_Libelle";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAdoMOY = new ModAdo();
                rsMOY = ModAdoMOY.fc_OpenRecordSet(sSQL, GridMoy, "MOY_ID");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                GridMoy.DataSource = rsMOY;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadMoy");
            }

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdOPE_Click(object sender, EventArgs e)//TESTED
        {
            fc_LoadOPE();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadOPE()//TESTED
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                sSQL = "SELECT   OPE_ID,  OPE_Libelle " + " From OPE_OperationGmao ";

                if (!string.IsNullOrEmpty(Text4.Text))
                {
                    sSQL = sSQL + " WHERE OPE_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(Text4.Text) + "'";
                }

                sSQL = sSQL + "     ORDER BY OPE_Libelle ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAdoOPE = new ModAdo();
                rsOPE = ModAdoOPE.fc_OpenRecordSet(sSQL, GridOPE, "OPE_ID");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                GridOPE.DataSource = rsOPE;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadOPE");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            GridMoy.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen";
            GridMoy.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
            GridMoy.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
        }
        /// <summary>
        /// TESTDE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void GridANO_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            GridANO.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            GridANO.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
            GridMoy.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            GridOPE.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            GridOPE.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
            GridOPE.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command13_Click(object sender, EventArgs e)//TESTED
        {
            Text1_KeyPress(Text1, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text1_KeyPress(object sender, KeyPressEventArgs e)//TESTED
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT CTR_Libelle From CTR_ControleGmao " + " WHERE CTR_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text1.Text) + "'"), "").ToString()))
                    {
                        string req = "SELECT     CTR_Libelle AS \"Contrôle\" FROM         CTR_ControleGmao";
                        var _with52 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'un contrôle" };
                        _with52.SetValues(new Dictionary<string, string> { { "CTR_Libelle", Text1.Text } });
                        _with52.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            Text1.Text = _with52.ugResultat.ActiveRow.GetCellValue("Contrôle") + "";
                            _with52.Close();
                        };
                        _with52.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13 && _with52.ugResultat.ActiveRow != null)
                            {
                                Text1.Text = _with52.ugResultat.ActiveRow.GetCellValue("Contrôle") + "";
                                _with52.Close();
                            }

                        };
                        _with52.ShowDialog();


                    }
                    CmdCTR_Click(CmdCTR, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text1_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command15_Click(object sender, EventArgs e)//TESTED
        {
            //ModAdoCTR.Update();
            //ModAdoMOY.Update();
            GridMoy.UpdateData();
            GridCTR.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command17_Click(object sender, EventArgs e)//TESTED
        {
            //ModAdoANO.Update();
            //ModAdoOPE.Update();
            GridANO.UpdateData();
            GridOPE.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command19_Click(object sender, EventArgs e)//tested
        {
            Text2_KeyPress(Text2, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text2_KeyPress(object sender, KeyPressEventArgs e)//TESTED
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT ANO_Libelle From  ANO_AnomalieGmao " + " WHERE ANO_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text3.Text) + "'"), "").ToString()))
                    {
                        string req = "SELECT     ANO_Libelle AS \"Anomalie\" FROM          ANO_AnomalieGmao";
                        var _with52 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'une anomalie " };
                        _with52.SetValues(new Dictionary<string, string> { { "ANO_Libelle", Text1.Text } });
                        _with52.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            Text2.Text = _with52.ugResultat.ActiveRow.GetCellValue("Anomalie") + "";
                            _with52.Close();
                        };
                        _with52.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13 && _with52.ugResultat.ActiveRow != null)
                            {
                                Text2.Text = _with52.ugResultat.ActiveRow.GetCellValue("Anomalie") + "";
                                _with52.Close();
                            }
                        };
                        _with52.ShowDialog();

                    }
                    cmdANO_Click(cmdANO, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text2_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command20_Click(object sender, EventArgs e)//tested
        {
            Text3_KeyPress(Text3, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text3_KeyPress(object sender, KeyPressEventArgs e)//TESTED
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT MOY_Libelle From MOY_MoyenGmao " + " WHERE MOY_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text3.Text) + "'"), "").ToString()))
                    {
                        string req = "SELECT     MOY_Libelle AS \"Moyen\" FROM          MOY_MoyenGmao";
                        var _with52 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'un moyen" };
                        _with52.SetValues(new Dictionary<string, string> { { "MOY_Libelle", Text1.Text } });

                        _with52.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            Text3.Text = _with52.ugResultat.ActiveRow.GetCellValue("Moyen") + "";
                            _with52.Close();
                        };
                        _with52.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13 && _with52.ugResultat.ActiveRow != null)
                            {
                                Text3.Text = _with52.ugResultat.ActiveRow.GetCellValue("Moyen") + "";
                                _with52.Close();
                            }

                        };
                        _with52.ShowDialog();

                    }
                    cmdMOY_Click(cmdMOY, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text3_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command22_Click(object sender, EventArgs e)//tested
        {
            Text4_KeyPress(Text4, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text4_KeyPress(object sender, KeyPressEventArgs e)//TESTED
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT OPE_Libelle From  OPE_OperationGmao " + " WHERE OPE_Libelle = '" + StdSQLchaine.gFr_DoublerQuote(Text4.Text) + "'"), "").ToString()))
                    {
                        string req = "SELECT     OPE_Libelle AS \"Opération\" FROM          OPE_OperationGmao ";
                        var _with52 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'une opération" };
                        _with52.SetValues(new Dictionary<string, string> { { "OPE_Libelle", Text1.Text } });
                        _with52.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            Text4.Text = _with52.ugResultat.ActiveRow.GetCellValue("Opération") + "";
                            _with52.Close();
                        };
                        _with52.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13 && _with52.ugResultat.ActiveRow != null)
                            {
                                Text4.Text = _with52.ugResultat.ActiveRow.GetCellValue("Opération") + "";
                                _with52.Close();
                            }
                        };
                        _with52.ShowDialog();

                    }
                    cmdOPE_Click(cmdOPE, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text4_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocParametreV2_Load(object sender, EventArgs e)
        {
            lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
            if (ssGridGam.ActiveRow != null)
                txtNiv1.Text = ssGridGam.ActiveRow.Cells["GAM_Libelle"].Text;
            if (ssCAI_CategoriInterv.ActiveRow != null)
            {
                lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> " + ssCAI_CategoriInterv.ActiveRow.Cells["cai_libelle"].Text;
                txtNiv2.Text = ssCAI_CategoriInterv.ActiveRow.Cells["cai_libelle"].Text;

                ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption + " (" + modP2.clibNiv1 + ")";
                ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption + " (" + modP2.clibNiv1 + ")";
            }
            //if (ssEQU_EquipementP2.ActiveRow != null)
            //{
            //    ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption + " (" + modP2.clibNiv1 + ")";
            //    ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption + " (" + modP2.clibNiv1 + ")";
            //}
            SSTab1.Tabs[1].Visible = false;
            SSTab1.Tabs[2].Visible = false;
            SSTab1.Tabs[3].Visible = false;
            SSTab1.Tabs[4].Visible = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)//TESTED
        {
            FC_UpdateGrille();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        public void FC_UpdateGrille()//TESTED
        {
            ssCAI_CategoriInterv.UpdateData();
            ssEQU_EquipementP2.UpdateData();
            ssNAP2.UpdateData();
            ssMAE_Materiel.UpdateData();
            ssTPT_TypePrestation.UpdateData();
            ssMATF.UpdateData();
            ssMarqueMateriel.UpdateData();
            ssGridGam.UpdateData();
            //ssEQU_EquipementP2.UpdateData();
            //ssNAP2.UpdateData();
            //ssPEG_PeriodeGamme.UpdateData();
            //ssMAE_Materiel.UpdateData();
            //ssTPT_TypePrestation.UpdateData();
            //ssMATF.UpdateData();
            //ssMarqueMateriel.UpdateData();
            //ssGridGam.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command16_Click(object sender, EventArgs e)//TESTED
        {
            string sSQL = null;
            DataTable rs = default(DataTable);

            sSQL = "SELECT    GMAP_Noauto,GMAP_TypeParam, GMAP_Valeur From GAMP_ParamGmao";//TODO .i added 'GMAP_Noauto' to the REQ be because ModAdo.update() need Primary Key of the table to execute update QUERY on the table.
            var ModAdo = new ModAdo();
            rs = ModAdo.fc_OpenRecordSet(sSQL);
            var rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamPaques + "'");
            if (rows.Length > 0)///TESTED
            {
                foreach (DataRow row in rs.Rows)
                {
                    if (row == rows[0])
                        row["GMAP_Valeur"] = General.nz(Convert.ToInt16(chkPaque.CheckState), 0);
                }
            }
            else
            {
                var dr = rs.NewRow();
                dr["GMAP_TypeParam"] = modP2.cParamPaques;
                dr["GMAP_Valeur"] = Convert.ToInt16(chkPaque.CheckState);
                rs.Rows.Add(dr);
            }
            ModAdo.Update();

            rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamJoursFériées + "'");
            if (rows.Length > 0)
            {
                foreach (DataRow row in rs.Rows)
                {
                    if (row == rows[0])
                        row["GMAP_Valeur"] = General.nz(Convert.ToInt16(chkFeriee.CheckState), 0);
                }
            }
            else
            {
                var dr = rs.NewRow();
                dr["GMAP_TypeParam"] = modP2.cParamJoursFériées;
                dr["GMAP_Valeur"] = Convert.ToInt16(chkFeriee.CheckState);
                rs.Rows.Add(dr);
            }
            ModAdo.Update();

            rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamSamedi + "'");
            if (rows.Length > 0)
            {
                foreach (DataRow row in rs.Rows)
                {
                    if (row == rows[0])
                        row["GMAP_Valeur"] = General.nz(Convert.ToInt16(chkSamedi.CheckState), 0);
                }
            }
            else
            {
                var dr = rs.NewRow();
                dr["GMAP_TypeParam"] = modP2.cParamSamedi;
                dr["GMAP_Valeur"] = Convert.ToInt16(chkSamedi.CheckState);
                rs.Rows.Add(dr);
            }
            ModAdo.Update();


            rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamDimanche + "'");
            if (rows.Length > 0)
            {
                foreach (DataRow row in rs.Rows)
                {
                    if (row == rows[0])
                        row["GMAP_Valeur"] = General.nz(Convert.ToInt16(chkDimanche.CheckState), 0);
                }
            }
            else
            {
                var dr = rs.NewRow();
                dr["GMAP_TypeParam"] = modP2.cParamDimanche;
                dr["GMAP_Valeur"] = Convert.ToInt16(chkDimanche.CheckState);
                rs.Rows.Add(dr);
            }
            ModAdo.Update();

            rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamPentecote + "'");
            if (rows.Length > 0)
            {
                foreach (DataRow row in rs.Rows)
                {
                    if (row == rows[0])
                        row["GMAP_Valeur"] = General.nz(Convert.ToInt16(chkPentecote.CheckState), 0);
                }
            }
            else
            {
                var dr = rs.NewRow();
                dr["GMAP_TypeParam"] = modP2.cParamPentecote;
                dr["GMAP_Valeur"] = Convert.ToInt16(chkPentecote.CheckState);
                rs.Rows.Add(dr);
            }
            ModAdo.Update();
            ModAdo.fc_CloseRecordset(rs);
            rs = null;

            UpdateLiaison("PartGMAO", General.sPartGMAO);
            UpdateLiaison("Nb Heures Totales", (txtNbHeuresTravail.Text));
            General.sNbHeuresTotales = txtNbHeuresTravail.Text;
            UpdateLiaison("Tolerance Entretien", (txtTolerance.Text));
            General.sToleranceEntretien = txtTolerance.Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sCritere"></param>
        /// <param name="sAdresse"></param>
        /// <param name="aCode"></param>
        /// <returns></returns>
        private bool UpdateLiaison(string sCritere, string sAdresse, string aCode = "")//TESTED
        {
            bool functionReturnValue = false;
            DataTable rsLiaison = default(DataTable);
            string sReq = null;

            //sCritere correspond à la colonne Code de la table Lien
            //sAdresse corredpond à la colonne adresse de la table Lien
            //aCode correspond à la colonne aCode de la table Lien
            try
            {
                functionReturnValue = false;

                rsLiaison = new DataTable();
                var ModAdo = new ModAdo();
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    rsLiaison = ModAdo.fc_OpenRecordSet("SELECT Adresse FROM lienV2 WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sCritere) + "' AND CodeUO = '" + General.strCodeUO + "'");
                else
                    rsLiaison = ModAdo.fc_OpenRecordSet("SELECT Adresse FROM lien WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sCritere) + "' AND CodeUO = '" + General.strCodeUO + "'");

                if (rsLiaison.Rows.Count > 0)
                {

                    sReq = "UPDATE Lien SET Adresse = '" + StdSQLchaine.gFr_DoublerQuote(sAdresse) + "'";

                    if (string.IsNullOrEmpty(aCode))
                    {
                        sReq = sReq + ", aCode = '" + StdSQLchaine.gFr_DoublerQuote(aCode) + "'";
                    }
                    sReq = sReq + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sCritere) + "'";
                    sReq = sReq + " AND CodeUO = '" + General.strCodeUO + "'";

                    General.Execute(sReq);

                    functionReturnValue = true;
                }
                ModAdo.fc_CloseRecordset(rsLiaison);
                rsLiaison = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "General;UpdateLiaison;");
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_PrestationMateriel()//TESTED
        {
            string sWhere = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                General.sSQL = "SELECT     TPT_NoAuto, TPT_Code, TPT_P3" + " FROM         TPT_TypePrestation";


                sWhere = "";
                if (!string.IsNullOrEmpty(txtTPT_Code.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "  WHERE TPT_Code LIKE '" +
                                 General.Replace(StdSQLchaine.gFr_DoublerQuote(txtTPT_Code.Text), "*", "%") + "%'";
                    }
                    else
                    {
                        sWhere = " AND TPT_Code LIKE '" +
                                 General.Replace(StdSQLchaine.gFr_DoublerQuote(txtTPT_Code.Text), "*", "%") + "%'";
                    }
                }
                var ModAdo = new ModAdo();
                rsTPT = ModAdo.fc_OpenRecordSet(General.sSQL + sWhere);
                ssTPT_TypePrestation.DataSource = rsTPT;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_PrestationMateriel");

            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocParametreV2_VisibleChanged(object sender, EventArgs e)//TESTED
        {
            try
            {
                if (!Visible) return;
                System.Windows.Forms.Control objcontrole = null;
               
                //===Grille des gammes.
                fc_Gamme();

                //==== grille des types de visites.
                fc_TypeVisite();
                fc_EQU_EquipementP2();

                //=== affichage de la périodicité.
                fc_ssDropTPP_TypePeriodeP2();

                //=== alimente la table des mois.
                fc_DropMois();

                //=== alimente la table des jours.
                fc_DropJour();

                //=== alimente la table des intervenants.
                fc_DropMaticule();

                //=== alimente les jours de la semaine.
                fc_DropSemaine();

                //==== type de materiel

                //fc_MAE_NoAuto();TODO this function  has return is first line
                //=== type de prestation.
                fc_PrestationMateriel();

                //'=== table des circuit.
                //fc_Circuit(); //TODO this fucntion has return in first line

                //=== table des marques de matériel.
                fc_MarqueMateriel();

                //=== jours fériées.
                fc_GAMP();

                //=== table des controles.
                fc_LoadCTR();

                //=== table des moyens.
                fc_LoadMoy();

                //=== Table des anomalies.
                fc_LoadANO();

                //=== Table des opérations.
                fc_LoadOPE();

                fc_DropGAMO();

                //TODO i change these lines from load to visibility_changed
                //lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
                //if (ssGridGam.ActiveRow != null)
                //    txtNiv1.Text = ssGridGam.ActiveRow.Cells["GAM_Libelle"].Text;
                //if (ssCAI_CategoriInterv.ActiveRow != null)
                //{
                //    lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> " + ssCAI_CategoriInterv.ActiveRow.Cells["cai_libelle"].Text;
                //    txtNiv2.Text = ssCAI_CategoriInterv.ActiveRow.Cells["cai_libelle"].Text;

                //    ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption + " (" + modP2.clibNiv1 + ")";
                //    ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption + " (" + modP2.clibNiv1 + ")";
                //}
                ////if (ssEQU_EquipementP2.ActiveRow != null)
                ////{
                ////    ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption + " (" + modP2.clibNiv1 + ")";
                ////    ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption + " (" + modP2.clibNiv1 + ")";
                ////}
                //SSTab1.Tabs[1].Visible = false;
                //SSTab1.Tabs[2].Visible = false;
                //SSTab1.Tabs[3].Visible = false;
                //SSTab1.Tabs[4].Visible = false;
                //View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocParametreV2");
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Form_Activate;");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Gamme()//TESTED
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sSQL = "SELECT     GAM_ID, GAM_Code, GAM_Libelle,GAM_Compteur,GAM_CompteurObli,  GAM_Noligne   " +
                       " FROM         GAM_Gamme  ";

                if (string.IsNullOrEmpty(sOrderByGamme))
                {
                    sOrderByGamme = " ORDER BY GAM_Noligne ASC";
                    sAscDesc = "ASC";
                }

                sSQL = sSQL + sOrderByGamme;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                if (ModAdoGam == null)
                    ModAdoGam = new ModAdo();
                rsGAM = ModAdoGam.fc_OpenRecordSet(sSQL, ssGridGam, "GAM_ID");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                ssGridGam.DataSource = rsGAM;
                ssGridGam.Visible = false;
                lGAI = 0;
                if (rsGAM.Rows.Count > 0)//TESTED
                {
                    foreach (DataRow dr in rsGAM.Rows)
                    {
                        lGAI += 1;
                        dr["GAM_Noligne"] = lGAI;
                        //string req = "Update GAM_Gamme set GAM_Noligne='" + lGAI + "' where GAM_ID='" + dr["GAM_ID"] +
                        //"'";//TODO i change ModAdo.update() to QUERY
                        //General.Execute(req);
                        //
                    }
                    ModAdoGam.Update();
                    lGAI = rsGAM.Rows.Count;
                }
                ssGridGam.DataSource = rsGAM;
                ssGridGam.Visible = true;
                lblniv0.Text = modP2.clibNiv0;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Gamme");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_TypeVisite()//TESTED
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (ssGridGam.ActiveRow != null)//TESTED
                {
                    sSQL = "SELECT  CAI_Noauto, CAI_Code, CAI_Libelle, CAI_NoLigne, CAI_Releve, GAM_ID " + " FROM  CAI_CategoriInterv ";

                    sSQL = sSQL + " WHERE GAM_ID =" + General.nz(ssGridGam.ActiveRow.Cells["GAM_ID"].Value, 0);

                    if (string.IsNullOrEmpty(sOrderByTypeVisite))
                    {
                        sOrderByTypeVisite = " ORDER BY CAI_CODE ASC";
                        sAscDesc = "ASC";
                    }

                    sSQL = sSQL + sOrderByTypeVisite;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                    if (ModAdoCAI == null)
                        ModAdoCAI = new ModAdo();
                    rsCAI = ModAdoCAI.fc_OpenRecordSet(sSQL, ssCAI_CategoriInterv, "CAI_Noauto");

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                    ssCAI_CategoriInterv.DataSource = rsCAI;

                    ssCAI_CategoriInterv.Visible = false;

                    lCAI = 0;
                    var _with10 = rsCAI;
                    if (rsCAI.Rows.Count > 0)//TESTED
                    {
                        foreach (DataRow dr in rsCAI.Rows)
                        {
                            lCAI = lCAI + 1;
                            dr["CAI_NoLigne"] = lCAI;
                            //string req = "update CAI_CategoriInterv set CAI_NoLigne='" + lCAI + "' where CAI_Noauto='" + dr["CAI_Noauto"] + "'";
                            //General.Execute(req);//TODO i change ModAdo.update() to QUERY
                            //
                        }
                        ModAdoCAI.Update();
                        lCAI = rsCAI.Rows.Count;
                    }
                    ssCAI_CategoriInterv.DataSource = rsCAI;

                    ssCAI_CategoriInterv.Visible = true;

                    //lblNiv1 = clibNiv1 & "=> " '& ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
                    //& ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    //txtNiv1 = ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    txtNiv1.Text = ssGridGam.ActiveRow.Cells["GAM_Libelle"].Text;
                    //fc_EQU_EquipementP2
                    return;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_EQU_EquipementP2()//TESTED
        {
            int i = 0;
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (ssCAI_CategoriInterv.ActiveRow != null)//TESTED
                {
                    ssEQU_EquipementP2.UpdateData();

                    //lblNiv1 = clibNiv1 & "=> " '& ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
                    //txtNiv1 = ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    if (ssGridGam.ActiveRow != null)
                        txtNiv1.Text = ssGridGam.ActiveRow.Cells["GAM_Libelle"].Text;

                    //lblNiv2 = cLibNv2bis & "=> " '& ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> ";
                    //& ssCAI_CategoriInterv.Columns("cai_libelle").Text
                    txtNiv2.Text = ssCAI_CategoriInterv.ActiveRow.Cells["cai_libelle"].Text;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                    sSQL = "SELECT     EQU_Code, EQU_Libelle, CAI_Noauto," +
                           " CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle, " +
                           " EQU_NoAuto, EQU_NoLigne,  Duree, DureePlanning, CompteurObli, EQU_P1Compteur, EQU_Observation " +
                           " From EQU_EquipementP2" + " WHERE CAI_Noauto = " +
                           General.nz((ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoAuto"].Value), 0) +
                           " ORDER BY EQU_NoLigne";


                    lEQU = 0;
                    //ssEQU_EquipementP2.Visible = False
                    if (ModAdoEQU == null)
                        ModAdoEQU = new ModAdo();
                    rsEQU = ModAdoEQU.fc_OpenRecordSet(sSQL, ssEQU_EquipementP2, "EQU_NoAuto");

                    if (rsEQU.Rows.Count > 0)
                    {
                        foreach (DataRow dr in rsEQU.Rows)
                        {
                            lEQU = lEQU + 1;
                            dr["EQU_NoLigne"] = lEQU;
                            //string req = "UPDATE EQU_EquipementP2 set EQU_NoLigne='" + lEQU + "' where EQU_NoAuto='" + dr["EQU_NoAuto"] + "'";
                            //General.Execute(req);//TODO i change ModAdo.update() to QUERY
                        }
                        ModAdoEQU.Update();
                    }

                    ssEQU_EquipementP2.DataSource = rsEQU;
                    if (string.IsNullOrEmpty(ssCAI_CategoriInterv.ActiveRow.Cells["cai_NoAuto"].Text))
                    {
                        ssEQU_EquipementP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    }
                    else//TESSTED
                    {
                        ssEQU_EquipementP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    }


                    fc_Rowcolchange();

                    ssEQU_EquipementP2.Visible = true;
                    //fc_DropGAMO();
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_EQU_EquipementP2;");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Rowcolchange()//TESTED
        {
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                ssNAP2.UpdateData(); //=====> parent visible=false always
                ssPEG_PeriodeGamme.UpdateData();

                //fc_PEI_PeriodeGammeImm(General.nz(ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text,0)); unused function
                fc_FacArticle();
                //nz(ssEQU_EquipementP2.Columns("EQU_NoAuto").Text, 0)


                if (ssEQU_EquipementP2.ActiveRow != null)
                {
                    if (string.IsNullOrEmpty(ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text))
                    {
                        ssPEG_PeriodeGamme.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                        //ssNAP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;=====> parent visible=false always
                    }
                    else
                    {
                        ssPEG_PeriodeGamme.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                        //ssNAP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    }
                }

                //Controles has visible false
                //lblNiv3a.Text = modP2.cLibNiv3 + "=> ";
                //& ssEQU_EquipementP2.Columns("EQU_Libelle").Text
                // txtNiv3a.Text = ssEQU_EquipementP2.Columns["EQU_Libelle"].Text;
                // lblNiv3b.Text = modP2.cLibNiv4 + "=> ";
                //& ssEQU_EquipementP2.Columns("EQU_Libelle").Text
                // txtNiv3b.Text = ssEQU_EquipementP2.Columns["EQU_Libelle"].Text;
                //  fc_ColorLigneEncours ssEQU_EquipementP2Imm

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Rowcolchange;");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_FacArticle()//TESTED
        {
            try
            {
                //    sSQL = "SELECT  CodeArticle, Designation1, FAC_NoAuto, EQU_NoAuto, FAC_NoLigne" _
                //'            & " From FacArticle  " _
                //'            & " WHERE FacArticle.EQU_NoAuto = " & IIf(ssEQU_EquipementP2.Columns("EQU_NoAuto").Text = "", _
                //'                    0, ssEQU_EquipementP2.Columns("EQU_NoAuto").Text) & " order by fac_NoLigne , Designation1"
                if (ssEQU_EquipementP2.ActiveRow != null)
                {
                    General.sSQL = "SELECT     NAP2_Noauto, NAP2_Libelle, MAP2_NoLigne, EQU_NoAuto" +
                                " From NAP2_NatureOpP2" +
                                " WHERE     (EQU_NoAuto = " +
                                (string.IsNullOrEmpty(ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text)
                                    ? "0"
                                    : ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text) +
                                ") order by MAP2_NoLigne , NAP2_Libelle";
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                    var ModAdo = new ModAdo();
                    rsFAC = ModAdo.fc_OpenRecordSet(General.sSQL);
                    lFAC_NoLigne = 0;
                    if (rsFAC.Rows.Count > 0)
                    {
                        foreach (DataRow dr in rsFAC.Rows)
                        {
                            lFAC_NoLigne = lFAC_NoLigne + 1;
                            dr["MAP2_NoLigne"] = lFAC_NoLigne;
                            //string req = "update NAP2_NatureOpP2 set MAP2_NoLigne='" + lFAC_NoLigne + "' where NAP2_Noauto='" + dr["MAP2_NoLigne"] + "'";
                            //General.Execute(req);
                            //
                        }
                        ModAdo.Update();
                    }
                    ssNAP2.Visible = false;
                    ssNAP2.DataSource = rsFAC;
                    //ssNAP2.Visible = true;
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_FacArticle;");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_ssDropTPP_TypePeriodeP2()//TESTED
        {
            //Combo Liée a la colonne TPP_Code.
            General.sSQL = "";
            General.sSQL = "SELECT  TPP_Code, TPP_JD, TPP_MD, TPP_JF, TPP_MF, SEM_Code" + " From TPP_TypePeriodeP2";
            if (ssDropTPP_TypePeriodeP2.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ssDropTPP_TypePeriodeP2), General.sSQL, "TPP_Code", true);
            }
            //this.ssPEG_PeriodeGamme.Columns["TPP_Code"].DropDownHwnd = this.ssDropTPP_TypePeriodeP2.hWnd;//TODO function that fill ssPEG_PeriodeGamme has return in first line
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropSemaine()//TESTED
        {
            //ce control liée a la colonne SEM_Code du grille ssPEG_PeriodeGamme
            string[] tsSem = new string[8];
            int i = 0;

            tsSem[0] = "";
            tsSem[1] = modP2.cSDimanche;
            tsSem[2] = modP2.cSLundi;
            tsSem[3] = modP2.cSMardi;
            tsSem[4] = modP2.cSMercredi;
            tsSem[5] = modP2.cSJeudi;
            tsSem[6] = modP2.cSVendredi;
            tsSem[7] = modP2.cSSamedi;

            //--dimanche est le premier jour de la semaine dans la fonction weekday par défaut.
            sheridan.AlimenteCmbAdditemTableau(ssDropSemaine, tsSem, "");
            ssDropSemaine.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Jours";

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropMaticule()//TESTED
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom,Personnel.prenom, Qualification.CodeQualif," + " Qualification.Qualification " + " FROM Qualification RIGHT JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif" + " ";
            if (ssDropMatricule.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ssDropMatricule), General.sSQL, "Matricule", true);
                ssDropMatricule.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
            }
            //Me.ssPEG_PeriodeGamme.Columns("PEG_Tech").DropDownHwnd = Me.ssDropMatricule.hWnd 
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropJour()//TESTED
        {
            //control liée a la colonne PEG_JD,PEG_JF du grille ssPEG_PeriodeGamme
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[31];
            for (i = 0; i <= 30; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }

            sheridan.AlimenteCmbAdditemTableau(ssDropJour, tabJour, "");
            //this.ssPEG_PeriodeGamme.Columns["PEG_JD"].DropDownHwnd = this.ssDropJour.hWnd;//TODO function that fill ssPEG_PeriodeGamme grid has return in first line
            //this.ssPEG_PeriodeGamme.Columns["PEG_JF"].DropDownHwnd = this.ssDropJour.hWnd;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropMois()//TESTED
        {
            //control liée a la colonne PEG_MD,PEG_MF du grille ssPEG_PeriodeGamme
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[12];
            for (i = 0; i <= 11; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }
            sheridan.AlimenteCmbAdditemTableau(ssDropMois, tabJour, "");
            //this.ssPEG_PeriodeGamme.Columns["PEG_MD"].DropDownHwnd = this.ssDropMois.hWnd;//TODO function that fill ssPEG_PeriodeGamme has return in first line
            //this.ssPEG_PeriodeGamme.Columns["PEG_MF"].DropDownHwnd = this.ssDropMois.hWnd;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_GAMP()//TESTED
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            try
            {
                sSQL = "SELECT GMAP_TypeParam, GMAP_Valeur From GAMP_ParamGmao";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                var rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamPaques + "'");
                if (rows.Length > 0)//TESTED
                    chkPaque.CheckState = (CheckState)Convert.ToInt16(General.nz(rows[0]["GMAP_Valeur"], 0));
                else
                    chkPaque.CheckState = CheckState.Unchecked;
                rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamJoursFériées + "'");
                chkFeriee.CheckState = (CheckState)Convert.ToInt16(General.nz(rows[0]["GMAP_Valeur"], 0));

                rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamSamedi + "'");
                chkSamedi.CheckState = (CheckState)Convert.ToInt16(General.nz(rows[0]["GMAP_Valeur"], 0));

                rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamDimanche + "'");
                chkDimanche.CheckState = (CheckState)Convert.ToInt16(General.nz(rows[0]["GMAP_Valeur"], 0));

                rows = rs.Select(" GMAP_TypeParam ='" + modP2.cParamPentecote + "'");
                chkPentecote.CheckState = (CheckState)Convert.ToInt16(General.nz(rows[0]["GMAP_Valeur"], 0));
                ModAdo.Close();
                rs = null;
                txtTolerance.Text = General.sToleranceEntretien;
                SliderEntretien.Value = 100 - Convert.ToInt32(General.sPartGMAO);
                txtNbHeuresTravail.Text = General.sNbHeuresTotales;

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropGAMO()//TESTED
        {

            fc_DropCtr();

            fc_DropMoy();

            fc_DropAno();

            fc_DropOpe();

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropCtr()//TESTED
        {
            //ce controle est liée a la colonne CTR_Libelle d grille ssEQU_EquipementP2
            string sSQL = null;
            try
            {
                sSQL = "";
                sSQL = "SELECT     CTR_Libelle as Contrôle, CTR_ID" + " From CTR_ControleGmao " + " ORDER BY CTR_Libelle ";

                sheridan.InitialiseCombo(DropCTR, sSQL, "Contrôle", true);
                //ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CTR_Libelle"].ValueList = DropCTR;//i move this lines to initialize_laayout
                DropCTR.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
                //DropCTR.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropCtr");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropMoy()//TESTED
        {
            //ce controle est liée a la colonne MOY_Libelle d grille ssEQU_EquipementP2
            string sSQL = null;
            try
            {
                sSQL = "";
                sSQL = "SELECT     MOY_Libelle as Moyen, MOY_ID From MOY_MoyenGmao " + " ORDER BY MOY_Libelle";

                sheridan.InitialiseCombo(DropMOY, sSQL, "Moyen", true);
                //if (ssEQU_EquipementP2.Rows.Count > 0)
                //ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["MOY_Libelle"].ValueList = DropMOY;//TODO i move this  line to initialize laayout
                DropMOY.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
                //DropMOY.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropMoy");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropAno()//TESTED
        {
            //ce controle est liée a la colonne ANO_Libelle d grille ssEQU_EquipementP2
            string sSQL = null;
            try
            {
                sSQL = "";
                sSQL = "SELECT     ANO_Libelle as Anomalie , ANO_ID From ANO_AnomalieGmao " + " ORDER BY ANO_Libelle";

                sheridan.InitialiseCombo(DropANO, sSQL, "Anomalie", true);
                //if (ssEQU_EquipementP2.Rows.Count > 0)
                /*ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["ANO_Libelle"].ValueList = DropANO;*///TODO i move this line to initialize laayout
                DropANO.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
                //DropANO.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropAno");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropOpe()//TESTED
        {
            //ce controle est liée a la colonne OPE_Libelle d grille ssEQU_EquipementP2
            string sSQL = null;
            try
            {
                sSQL = "";
                sSQL = "SELECT     OPE_Libelle as Opération, OPE_ID From OPE_OperationGmao " + " ORDER BY OPE_Libelle ";

                sheridan.InitialiseCombo(DropOPE, sSQL, "Opération", true);
                //if (ssEQU_EquipementP2.Rows.Count > 0)
                //ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["OPE_Libelle"].ValueList = DropOPE;//TODOi move this line to initialize_laayout
                DropOPE.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
                //DropOPE.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropOpe");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_Code"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_Libelle"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CAI_Noauto"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_NoAuto"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["Duree"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["DureePlanning"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CompteurObli"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_P1Compteur"].Hidden = true;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_Observation"].Header.Caption = "Observation";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["EQU_NoLigne"].Header.Caption = "No Ligne";
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["CTR_Libelle"].ValueList = DropCTR;
            //DropCTR.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["OPE_Libelle"].ValueList = DropOPE;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["ANO_Libelle"].ValueList = DropANO;
            ssEQU_EquipementP2.DisplayLayout.Bands[0].Columns["MOY_Libelle"].ValueList = DropMOY;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridANO_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (GridANO.ActiveCell != null)
                {
                    GridANO.ActiveCell.Value = General.Trim(GridANO.ActiveCell.Text);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridANO_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (GridCTR.ActiveCell != null)
                {
                    GridCTR.ActiveCell.Value = General.Trim(GridCTR.ActiveCell.Text);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridCTR_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (GridMoy.ActiveCell != null)
                {

                    GridMoy.ActiveCell.Value = General.Trim(GridMoy.ActiveCell.Text);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridMoy_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (GridOPE.ActiveCell != null)
                {

                    GridOPE.ActiveCell.Value = General.Trim(GridOPE.ActiveCell.Text);
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridOPE_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (ssCAI_CategoriInterv.ActiveRow != null)
                {
                    if (string.IsNullOrEmpty(ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoLigne"].Text))
                    {
                        lCAI = lCAI + 1;
                        ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoLigne"].Value = lCAI;
                    }
                    if (ssCAI_CategoriInterv.ActiveCell.Column.Index == ssCAI_CategoriInterv.ActiveRow.Cells["CAI_CODE"].Column.Index)
                    {

                        ssCAI_CategoriInterv.ActiveRow.Cells["CAI_Code"].Value = General.UCase(ssCAI_CategoriInterv.ActiveRow.Cells["CAI_Code"].Text);
                    }


                    if (string.IsNullOrEmpty(ssCAI_CategoriInterv.ActiveRow.Cells["GAM_ID"].Text))
                    {
                        ssCAI_CategoriInterv.ActiveRow.Cells["GAM_ID"].Value = ssGridGam.ActiveRow.Cells["GAM_ID"].Value;
                    }
                    ssCAI_CategoriInterv.ActiveRow.Cells[ssCAI_CategoriInterv.ActiveCell.Column.Index].Value = General.Trim(ssCAI_CategoriInterv.ActiveRow.Cells[ssCAI_CategoriInterv.ActiveCell.Column.Index].Text);


                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssCAI_CategoriInterv_BeforeColUpdate");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_BeforeRowUpdate(object sender, CancelableRowEventArgs e)//TESTED
        {
            if (ssCAI_CategoriInterv.ActiveRow != null)
            {
                if (string.IsNullOrEmpty(e.Row.Cells["CAI_Code"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un Code.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                if (string.IsNullOrEmpty(e.Row.Cells["CAI_Libelle"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un libellé.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_AfterRowActivate(object sender, EventArgs e)//TESTED
        {
            fc_EQU_EquipementP2();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            string sCai_Code = null;

            e.DisplayPromptMsg = false;

            var ModAdo = new ModAdo();
            sCai_Code = ModAdo.fc_ADOlibelle("SELECT CAI_Code FROM ICC_IntervCategorieContrat WHERE CAI_Code = '" + StdSQLchaine.gFr_DoublerQuote(ssCAI_CategoriInterv.ActiveRow.Cells["CAI_Code"].Text) + "'");

            if (string.IsNullOrEmpty(sCai_Code))
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce type de visite est utilisé dans au moins une fiche GMAO. Il ne peut donc pas être supprimé.", "Suppression d'un type de visite annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            if (string.IsNullOrEmpty(ssEQU_EquipementP2.ActiveRow.Cells["CAI_NoAuto"].Text))
            {
                ssEQU_EquipementP2.ActiveRow.Cells["CAI_NoAuto"].Value = ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoAuto"].Text;
            }

            //    If Trim(.ActiveRow.Cells["EQU_Libelle"].Value) = "" Then
            //            MsgBox "Vous ne pouvez pas saisir d'opération sans libellé.", vbInformation, "Opération non conforme"
            //            Cancel = True
            //    End If

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["CompteurObli"].Column.Index)
            {
                if (ssEQU_EquipementP2.ActiveRow.Cells["CompteurObli"].Value + "" == "-1")
                {
                    ssEQU_EquipementP2.ActiveRow.Cells["CompteurObli"].Value = 1;
                }
            }

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["EQU_P1Compteur"].Column.Index)
            {
                if (ssEQU_EquipementP2.ActiveRow.Cells["EQU_P1Compteur"].Value + "" == "-1")
                {
                    ssEQU_EquipementP2.ActiveRow.Cells["EQU_P1Compteur"].Value = 1;
                }
            }

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["CTR_Libelle"].Column.Index)//TESTED
            {
                ssEQU_EquipementP2.ActiveRow.Cells["CTR_ID"].Value = General.nz(ModP2v2.fc_IdControle((ssEQU_EquipementP2.ActiveRow.Cells["CTR_Libelle"].Text + "")), 0);
            }

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["MOY_Libelle"].Column.Index)//TESTED
            {
                ssEQU_EquipementP2.ActiveRow.Cells["MOY_ID"].Value = General.nz(ModP2v2.fc_IdMoyen((ssEQU_EquipementP2.ActiveRow.Cells["MOY_Libelle"].Text + "")), 0);
            }

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["ANO_Libelle"].Column.Index)//TESTED
            {
                ssEQU_EquipementP2.ActiveRow.Cells["ANO_ID"].Value = General.nz(ModP2v2.fc_IdAnomalie((ssEQU_EquipementP2.ActiveRow.Cells["ANO_Libelle"].Text + "")), 0);
            }

            if (ssEQU_EquipementP2.ActiveCell.Column.Index == ssEQU_EquipementP2.ActiveRow.Cells["OPE_Libelle"].Column.Index)//TESTED
            {
                ssEQU_EquipementP2.ActiveRow.Cells["OPE_ID"].Value = General.nz(ModP2v2.fc_IDOperation((ssEQU_EquipementP2.ActiveRow.Cells["OPE_Libelle"].Text + "")), 0);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_BeforeRowUpdate(object sender, CancelableRowEventArgs e)//TESTED
        {
            if (ssEQU_EquipementP2.ActiveRow != null)
            {
                if (string.IsNullOrEmpty(e.Row.Cells["EQU_NoLigne"].Text))
                {
                    lEQU = lEQU + 1;
                    e.Row.Cells["EQU_NoLigne"].Value = lEQU;
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            General.sSQL = "IF EXISTS ( SELECT 'a' FROM EQM_EquipementP2Imm WHERE EQU_NoAuto = " + ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text + ")";
            General.sSQL = General.sSQL + " SELECT '1' ELSE SELECT '0'";
            var ModAdo = new ModAdo();
            if (ModAdo.fc_ADOlibelle(General.sSQL) == "1")
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas supprimer cette opération car elle est présente dans au moins une fiche GMAO.", "Suppression impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)//TESTED
        {
            try
            {
                if (ssGridGam.ActiveRow != null)
                {
                    if (!string.IsNullOrEmpty(ssGridGam.ActiveRow.Cells[ssGridGam.ActiveCell.Column.Index].Text.ToString()))
                        ssGridGam.ActiveRow.Cells[ssGridGam.ActiveCell.Column.Index].Value = General.Trim(ssGridGam.ActiveRow.Cells[ssGridGam.ActiveCell.Column.Index].Text);
                    if (ssGridGam.ActiveCell != null && ssGridGam.ActiveCell.Column.Index == ssGridGam.ActiveRow.Cells["GAM_Compteur"].Column.Index)
                    {
                        if (ssGridGam.ActiveRow.Cells["GAM_Compteur"].Text == "-1")
                        {
                            ssGridGam.ActiveRow.Cells["GAM_Compteur"].Value = 1;
                        }
                    }

                    if (ssGridGam.ActiveCell != null && ssGridGam.ActiveCell.Column.Index == ssGridGam.ActiveRow.Cells["GAM_CompteurObli"].Column.Index)
                    {
                        if (ssGridGam.ActiveRow.Cells["GAM_CompteurObli"].Text == "-1")
                        {
                            ssGridGam.ActiveRow.Cells["GAM_CompteurObli"].Value = 1;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridGam_BeforeColUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridANO_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)//TESTED
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_AfterRowActivate(object sender, EventArgs e)//TESTED
        {
            fc_TypeVisite();
            fc_EQU_EquipementP2();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_AfterRowActivate(object sender, EventArgs e)//TESTED
        {
            fc_Rowcolchange();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_ID"].Hidden = true;
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_Libelle"].Header.Caption = "Libelle";
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_Code"].Header.Caption = "Code";
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_Compteur"].Header.Caption = "Relever les Compteurs";
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_CompteurObli"].Header.Caption = "Compteur obligatoire";
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_CompteurObli"].Style = ColumnStyle.CheckBox;
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_Compteur"].Style = ColumnStyle.CheckBox;
            ssGridGam.DisplayLayout.Bands[0].Columns["GAM_Noligne"].Header.Caption = "N° Ligne";
            ssGridGam.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Noauto"].Hidden = true;
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_NoLigne"].Hidden = true;
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Releve"].Hidden = true;
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["GAM_ID"].Hidden = true;
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = "Code";
            ssCAI_CategoriInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = "Libelle";
            ssCAI_CategoriInterv.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_InitializeRow(object sender, InitializeRowEventArgs e)//TESTED

        {
            if (e.ReInitialize)
                return;
            if (e.Row.GetCellValue("GAM_Compteur") != DBNull.Value)
            {
                if (Convert.ToBoolean(e.Row.Cells["GAM_Compteur"].Value))
                    e.Row.Cells["GAM_Compteur"].Value = true;
                else
                    e.Row.Cells["GAM_Compteur"].Value = false;
            }
            else
                e.Row.Cells["GAM_Compteur"].Value = false;


            if (e.Row.GetCellValue("GAM_CompteurObli") != DBNull.Value)
            {
                if (Convert.ToBoolean(e.Row.Cells["GAM_CompteurObli"].Value))
                    e.Row.Cells["GAM_CompteurObli"].Value = true;
                else
                    e.Row.Cells["GAM_CompteurObli"].Value = false;
            }
            else
                e.Row.Cells["GAM_CompteurObli"].Value = false;
            ssGridGam.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_InitializeLayout(object sender, InitializeLayoutEventArgs e)//TESTED
        {
            GridCTR.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle";
            GridCTR.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
            GridCTR.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            if (ssGridGam.ActiveRow != null)
            {
                if (ssGridGam.ActiveRow.Cells["GAM_Code"].Text != "")
                //to catch an exception "GAM_Code" can not be null
                {
                    ModAdoGam.Update();
                    ssGridGam.UpdateData();
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoCAI.Update();
            ssCAI_CategoriInterv.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoEQU.Update();
            ssEQU_EquipementP2.UpdateData();
        }

        private void ssCAI_CategoriInterv_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + ssCAI_CategoriInterv.ActiveRow.Cells[e.DataErrorInfo.Cell.Column.Index].Column.Header.Caption;
                MessageBox.Show(e.ErrorText);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_Click(object sender, EventArgs e)//TESTED
        {
            fc_DropGAMO();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoCTR.Update();
            GridCTR.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoMOY.Update();
            GridMoy.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridANO_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoANO.Update();
            GridANO.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_AfterRowUpdate(object sender, RowEventArgs e)//TESTED
        {
            ModAdoOPE.Update();
            GridOPE.UpdateData();
        }




        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["GAM_Code"].Text))
            {
                MessageBox.Show("Vous devez saisir un code", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["GAM_Code"].DataChanged)
            {
                if (e.Row.Cells["GAM_Code"].OriginalValue.ToString() != e.Row.Cells["GAM_Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM GAM_Gamme WHERE GAM_Code= '{e.Row.Cells["GAM_Code"].Text}' "));
                        if (inDb > 0)
                        {
                            MessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CTR_Libelle"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un controle", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["CTR_Libelle"].DataChanged)
                if (e.Row.Cells["CTR_Libelle"].OriginalValue.ToString() != e.Row.Cells["CTR_Libelle"].Text)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM CTR_ControleGmao WHERE CTR_Libelle= '{e.Row.Cells["CTR_Libelle"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["MOY_Libelle"].Text))
            {
                MessageBox.Show("Vous devez saisir un moyen", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["MOY_Libelle"].DataChanged)
                if (e.Row.Cells["MOY_Libelle"].OriginalValue.ToString() != e.Row.Cells["MOY_Libelle"].Text)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM MOY_MoyenGmao WHERE MOY_Libelle= '{e.Row.Cells["MOY_Libelle"].Text}' "));
                        if (inDb > 0)
                        {
                            MessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridANO_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["ANO_Libelle"].Text))
            {
                MessageBox.Show("Vous devez saisir un anomalie", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }

            if (e.Row.Cells["ANO_Libelle"].DataChanged)
                if (e.Row.Cells["ANO_Libelle"].OriginalValue.ToString() != e.Row.Cells["ANO_Libelle"].Text)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ANO_AnomalieGmao WHERE ANO_Libelle= '{e.Row.Cells["ANO_Libelle"].Text}' "));
                        if (inDb > 0)
                        {
                            MessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["OPE_Libelle"].Text))
            {
                MessageBox.Show("Vous devez saisir une opération", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["OPE_Libelle"].DataChanged)
                if (e.Row.Cells["OPE_Libelle"].OriginalValue.ToString() != e.Row.Cells["OPE_Libelle"].Text)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM OPE_OperationGmao WHERE OPE_Libelle= '{e.Row.Cells["OPE_Libelle"].Text}' "));
                        if (inDb > 0)
                        {
                            MessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGam_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoGam.Update();
            ssGridGam.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCAI_CategoriInterv_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoCAI.Update();
            ssCAI_CategoriInterv.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoEQU.Update();
            ssEQU_EquipementP2.UpdateData();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCTR_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoCTR.Update();
            GridCTR.UpdateData();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMoy_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoMOY.Update();
            GridMoy.UpdateData();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridANO_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoANO.Update();
            GridANO.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridOPE_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoOPE.Update();
            GridOPE.UpdateData();
        }



        /*========================> sstab(1),sstab(2),sstab(3),sstab(4) visible false <=============================*/
        /*
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         * 
         **********/

        private void cmdRechercheMatricule_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    MAE_Code as \"Code\", MAE_Libelle as \"Libelle\"" + " FROM         MAE_Materiel";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche du type de matériel" };
                //with3.txtNomFicheAppelante.Text = this.Name + "cmdRechercheMatricule_Click"; 
                with3.SetValues(new Dictionary<string, string> { { "MAE_Code", txtMAE_Code.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMAE_Code.Text = with3.ugResultat.ActiveRow.Cells["CODE"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheMatricule_Click");
            }
        }

        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            // fc_MAE_NoAuto();//this function has return in the first line
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    MAE_Code as \"Code\", MAE_Libelle as \"Libelle\"" + " FROM         MAE_Materiel";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche du type de matériel" };
                //with3.txtNomFicheAppelante.Text = this.Name + "cmdRechercheMatricule_Click";
                with3.SetValues(new Dictionary<string, string> { { "MAE_Libelle", txtMAE_Libelle.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMAE_Libelle.Text = with3.ugResultat.ActiveRow.Cells["Libelle"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }

        private void Command10_Click(object sender, EventArgs e)
        {
            fc_MarqueMateriel();
        }
        private void fc_MarqueMateriel()
        {
            string sWhere = null;
            General.sSQL = "SELECT     Marque" + " FROM         MarqueMateriel";

            sWhere = "";
            if (!string.IsNullOrEmpty(txtMarque.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = "  WHERE Marque LIKE '" + General.Replace(StdSQLchaine.gFr_DoublerQuote(txtMarque.Text), "*", "%") + "%'";
                }
                else
                {
                    sWhere = " AND Marque LIKE '" + General.Replace(StdSQLchaine.gFr_DoublerQuote(txtMarque.Text), "*", "%") + "%'";
                }
            }
            var ModAdo = new ModAdo();
            rsMarque = ModAdo.fc_OpenRecordSet(General.sSQL + sWhere);
            ssMarqueMateriel.DataSource = rsMarque;
        }

        private void Command11_Click(object sender, EventArgs e)
        {
            FC_UpdateGrille();
        }

        private void Command12_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    MATF_Code as \"Code\", MATF_Libelle as \"Libelle\"" + " FROM   MATF_familleMateriel";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche famille matériel" };
                //with3.txtNomFicheAppelante.Text = this.Name + "cmdRechercheMatricule_Click"; this
                with3.SetValues(new Dictionary<string, string> { { "MATF_Code", txtMATF_Code.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMATF_Code.Text = with3.ugResultat.ActiveRow.Cells["Code"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheMatricule_Click");
            }
        }

        private void Command2_Click(object sender, EventArgs e)
        {
            FC_UpdateGrille();
        }

        private void Command3_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    TPT_Code as \"Prestation\" FROM   TPT_TypePrestation";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche des prestations" };
                //with3.txtNomFicheAppelante.Text = this.Name + "cmdRechercheMatricule_Click"; this
                with3.SetValues(new Dictionary<string, string> { { "TPT_Code", txtTPT_Code.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtTPT_Code.Text = with3.ugResultat.ActiveRow.Cells["Prestation"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command3_Click");
            }
        }

        private void Command4_Click(object sender, EventArgs e)
        {
            FC_UpdateGrille();
        }

        private void Command5_Click(object sender, EventArgs e)
        {
            fc_PrestationMateriel();
        }

        private void ssDropFamille_AfterCloseUp(object sender, EventArgs e)
        {
            if (ssDropFamille.ActiveRow != null)
                ssMAE_Materiel.ActiveRow.Cells["Designation"].Value = ssDropFamille.ActiveRow.Cells[1].Text;
        }

        private void GridVentilEntretien_AfterExitEditMode(object sender, EventArgs e)
        {
            //fc_VentilEntretienGMAO(ref Convert.ToString(Convert.ToDouble(txtNbHeuresTravail.Text) * (Convert.ToInt32(General.sPartGMAO) / 100)));===> unused function
        }

        private void GridVentilEntretien_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            string sReq = null;

            if (GridVentilEntretien.ActiveCell != null && GridVentilEntretien.ActiveCell.Column.Index + 1 <= 12)
            {
                sReq = "UPDATE GMAO_VentilationEntretien SET ";
                sReq = sReq + " VentilationEnt = " + GridVentilEntretien.ActiveCell.Value;
                sReq = sReq + " WHERE CodeUO = '" + General.strCodeUO + "'";
                sReq = sReq + " AND Mois = '" + GridVentilEntretien.ActiveCell.Column.Index + 1 + "'";
                General.Execute(sReq);
            }
        }

        private void GridVentilEntretien_KeyPress(object sender, KeyPressEventArgs e)
        {
            double dblTotal = 0;
            short i = 0;
            if (GridVentilEntretien.Rows.Count > 0)
                e.KeyChar = '0';
            else
            {
                if (GridVentilEntretien.ActiveCell != null && GridVentilEntretien.ActiveCell.Column.Index == GridVentilEntretien.ActiveRow.Cells["Total"].Column.Index)
                {
                    if ((short)e.KeyChar == 13)
                    {
                        if (General.IsNumeric(GridVentilEntretien.ActiveRow.Cells["Total"].Text))
                        {
                            dblTotal = Convert.ToDouble(GridVentilEntretien.ActiveRow.Cells["Total"].Text);
                            dblTotal = dblTotal / 12;
                            for (i = 0; i <= 11; i++)
                            {
                                GridVentilEntretien.ActiveRow.Cells[i].Value = Convert.ToString(General.FncArrondir(dblTotal));
                            }

                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous deves saisir une valeur numérique.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }
            }
        }

        private void SliderEntretien_ValueChanged(object sender, EventArgs e)
        {
            fc_AfficheVentillationEntretien();
        }
        private void fc_AfficheVentillationEntretien()
        {
            double nbHeuresDep = 0;
            double nbHeuresEnt = 0;

            ShapeDepannage.Width = ((SliderEntretien.Width) * SliderEntretien.Value / 100);
            lblDepannageValue.Text = SliderEntretien.Value + " %";
            ShapeEntretien.Width = ((SliderEntretien.Width) * (100 - SliderEntretien.Value) / 100);
            ShapeEntretien.Left = (SliderEntretien.Left) + (SliderEntretien.Width) - (ShapeEntretien.Width);

            lblEntretienValue.Text = (100 - SliderEntretien.Value) + " %";

            General.sPartGMAO = Convert.ToString(100 - SliderEntretien.Value);

            if (General.IsNumeric(txtNbHeuresTravail.Text))
            {
                nbHeuresEnt = General.FncArrondir(Convert.ToDouble(txtNbHeuresTravail.Text) * (100 - SliderEntretien.Value) / 100);
                lblEntretienValue.Text = (100 - SliderEntretien.Value) + " %" + "\r\n" + "Soit : " + nbHeuresEnt + " h";

                nbHeuresDep = Convert.ToDouble(txtNbHeuresTravail.Text) - nbHeuresEnt;
                lblDepannageValue.Text = SliderEntretien.Value + " %" + "\r\n" + "Soit : " + nbHeuresDep + " h";

                //fc_VentilEntretienGMAO(ref Convert.ToString(Convert.ToDouble(txtNbHeuresTravail.Text) * (Convert.ToInt32(General.sPartGMAO) / 100)));==> unused function

            }

        }

        private void ssPEG_PeriodeGamme_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            int i = 0;
            int lTempo = 0;
            System.DateTime sDeb = default(System.DateTime);
            System.DateTime sFin = default(System.DateTime);
            double dVisite = 0;
            bool bExist = false;

            bExist = false;

            //===  mise à des périodes dans la grille en fonction du tableau des périodicité.
            if (ssPEG_PeriodeGamme.ActiveCell != null && ssPEG_PeriodeGamme.ActiveCell.Column.Index == ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_CODE"].Column.Index)
            {
                ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Value = "";
                ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Value = "";
                ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Value = "";
                ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Value = "";
                ssPEG_PeriodeGamme.ActiveRow.Cells["SEM_Code"].Value = "";
                for (i = 0; i <= ssPEG_PeriodeGamme.Rows.Count - 1; i++)
                {
                    if (ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_CODE"].Value == ssPEG_PeriodeGamme.Rows[i].Cells["TPP_CODE"].Value)
                    {
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Value = Convert.ToInt32(ssPEG_PeriodeGamme.Rows[i].Cells["PEG_JD"].Value);
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Value = Convert.ToInt32(ssPEG_PeriodeGamme.Rows[i].Cells["PEG_JF"].Value);
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Value = Convert.ToInt32(ssPEG_PeriodeGamme.Rows[i].Cells["PEG_MD"].Value);
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Value = Convert.ToInt32(ssPEG_PeriodeGamme.Rows[i].Cells["PEG_MF"].Value);
                        //ssPEI_PeriodeGammeImm.Columns("SEM_Code"].Value = .Columns("SEM_Code"].Value
                        bExist = true;
                        break;
                    }
                }
                if (bExist == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le code périodicité " + ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_CODE"].Value + " n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            //=== cle etrangére.
            if (string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["EQU_NoAuto"].Text))
            {
                ssPEG_PeriodeGamme.ActiveRow.Cells["EQU_NoAuto"].Value = ssEQU_EquipementP2.ActiveRow.Cells["EQU_NoAuto"].Text;
            }


            //=== controle si les jours sont cohérent (exemple 30 fervrier n'est pas une date.
            if (!string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Text))
            {
                do
                {
                    if (!General.IsDate(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Text + "/" + ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Text + "/" + DateTime.Today.Year))
                    {
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Value = Convert.ToInt16(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Value) - 1;
                    }
                    else
                    {
                        break;
                    }
                } while (true);
            }
            if (!string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Text))
            {

                lTempo = DateTime.Today.Year;

                do
                {
                    if (!General.IsDate(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Text + "/" + ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Text + "/" + lTempo))
                    {
                        ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Value = Convert.ToInt16(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Value) - 1;
                    }
                    else
                    {
                        break;
                    }
                } while (true);
            }
            if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_ForceVisite"].Text) == General.UCase("A"))
            {
                if (!string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Text) && !string.IsNullOrEmpty(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text))
                {

                    sDeb = Convert.ToDateTime(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JD"].Text + "/" + ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MD"].Text + "/" + DateTime.Today.Year);
                    //    If ssPEG_PeriodeGamme.Columns("PEG_ANF"].Value = "+1" Then
                    //           lTempo = Year(Date) + 1
                    //   Else
                    lTempo = DateTime.Today.Year;
                    //  End If
                    sFin = Convert.ToDateTime(ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_JF"].Text + "/" + ssPEG_PeriodeGamme.ActiveRow.Cells["PEG_MF"].Text + "/" + lTempo);
                    if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("Hebdomadaire"))
                    {
                        ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate((sDeb - sFin).TotalDays / 7);
                    }
                    else if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("Mensuel"))
                    {
                        dVisite = (((sFin.Year - sDeb.Year) * 12) + (sFin.Month - sDeb.Month)) + 1;
                        //dVisite = DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, sDeb, sFin) + 1;
                        ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = dVisite;
                    }
                    else if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("BiMensuel"))
                    {
                        //dVisite = DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, sDeb, sFin) + 1;
                        dVisite = (((sFin.Year - sDeb.Year) * 12) + (sFin.Month - sDeb.Month)) + 1;
                        if (Math.Truncate(dVisite / 2) == (dVisite / 2))
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 2);
                        }
                        else
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 2) + 1;
                        }
                    }
                    else if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("Trimestriel"))
                    {
                        //dVisite = DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, sDeb, sFin) + 1;
                        dVisite = (((sFin.Year - sDeb.Year) * 12) + (sFin.Month - sDeb.Month)) + 1;
                        if (Math.Truncate(dVisite / 3) == (dVisite / 3))
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 3);
                        }
                        else
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 3) + 1;
                        }
                    }
                    else if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("Semestriel"))
                    {
                        //dVisite = DateAndTime.DateDiff(Microsoft.VisualBasic.DateInterval.Month, sDeb, sFin) + 1;
                        dVisite = (((sFin.Year - sDeb.Year) * 12) + (sFin.Month - sDeb.Month)) + 1;
                        if (Math.Truncate(dVisite / 6) == (dVisite / 6))
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 6);
                        }
                        else
                        {
                            ssPEG_PeriodeGamme.ActiveRow.Cells["peg_Visite"].Value = Math.Truncate(dVisite / 6) + 1;
                        }
                    }
                    else if (General.UCase(ssPEG_PeriodeGamme.ActiveRow.Cells["TPP_Code"].Text) == General.UCase("Annuel"))
                    {
                        dVisite = 1;
                        ssPEG_PeriodeGamme.ActiveRow.Cells["peI_Visite"].Value = dVisite;
                    }
                }
            }
        }

        private void ssPEG_PeriodeGamme_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssPEG_PeriodeGamme_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + ssPEG_PeriodeGamme.ActiveRow.Cells[e.DataErrorInfo.Cell.Column.Index].Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssMAE_Materiel_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssMAE_Materiel.ActiveCell != null && ssMAE_Materiel.ActiveCell.Column.Index == ssMAE_Materiel.ActiveRow.Cells["CodeFamille"].Column.Index)
            {
                if (!string.IsNullOrEmpty(ssMAE_Materiel.ActiveRow.Cells["CodeFamille"].Text))
                {
                    General.sSQL = "SELECT 'a' FROM MATF_familleMateriel WHERE MATF_Code = '" + ssMAE_Materiel.ActiveRow.Cells["CodeFamille"].Text + "'";
                    var ModAdo = new ModAdo();
                    if (ModAdo.fc_ADOlibelle(General.sSQL) == "a")
                    {
                        General.sSQL = "SELECT MATF_Libelle FROM MATF_familleMateriel WHERE MATF_Code = '" + ssMAE_Materiel.ActiveRow.Cells["CodeFamille"].Text + "'";
                        ssMAE_Materiel.ActiveRow.Cells["Designation"].Value = ModAdo.fc_ADOlibelle(General.sSQL);
                    }
                    else
                    {
                        ssMAE_Materiel.ActiveRow.Cells["Designation"].Value = "";
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code famille existant.", "Code famille inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                    }
                }
                else
                {
                    ssMAE_Materiel.ActiveRow.Cells["Designation"].Value = "";
                }
            }
        }

        private void ssMAE_Materiel_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + ssMAE_Materiel.ActiveRow.Cells[e.DataErrorInfo.Cell.Column.Index].Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssMAE_Materiel_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (ssMAE_Materiel.ActiveRow != null)
                if (string.IsNullOrEmpty(e.Row.Cells["MAE_Code"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code est obligatoire. Veuillez saisir un code ou annuler votre saisie" + " en appuyant 2 fois sur le boutton Echap(Esc).", "Code Obligatoire", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
        }

        private void ssMAE_Materiel_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;

        }

        private void SSTab1_MouseMove(object sender, MouseEventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            ///====> FRMS doesn"t exist in vb
            //frmAdministration.Visible = false;
            //frmStandard.Visible = false;
            //frmExploitation.Visible = false;
            //frmTravaux.Visible = false;
            //frmCommercial.Visible = false;
            //frmFioul.Visible = false;
        }

        private void ssTPT_TypePrestation_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssTPT_TypePrestation.ActiveCell != null && ssTPT_TypePrestation.ActiveCell.Column.Index == ssTPT_TypePrestation.ActiveRow.Cells["TPT_P3"].Column.Index)
            {
                if (ssTPT_TypePrestation.ActiveRow.Cells["TPT_P3"].Text == "-1")
                {
                    ssTPT_TypePrestation.ActiveRow.Cells["TPT_P3"].Value = 1;

                }

            }
        }

        private void ssTPT_TypePrestation_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        private void ssTPT_TypePrestation_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + ssTPT_TypePrestation.ActiveRow.Cells[e.DataErrorInfo.Cell.Column.Index].Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssMATF_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Printing)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + ssMATF.ActiveRow.Cells[e.DataErrorInfo.Cell.Column.Index].Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssMATF_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            string sMATF_Code = null;

            e.Cancel = false;

            e.DisplayPromptMsg = false;
            var ModAdo = new ModAdo();
            sMATF_Code = ModAdo.fc_ADOlibelle("SELECT MATF_Code FROM DEPI_DescriptifChaufferieImm WHERE MATF_Code = '" + StdSQLchaine.gFr_DoublerQuote(ssMATF.ActiveRow.Cells["MATF_Code"].Text) + "'");
            if (!string.IsNullOrEmpty(sMATF_Code))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cette famille de matériel est utilisée dans au moins une fiche de descriptif chaufferie. Elle ne peut donc pas être supprimée.", "Suppression d'une famille matériel annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
            else
            {
                sMATF_Code = ModAdo.fc_ADOlibelle("SELECT MATF_Code FROM MAE_Materiel WHERE MATF_Code = '" + StdSQLchaine.gFr_DoublerQuote(ssMATF.ActiveRow.Cells["MATF_Code"].Text) + "'");
                if (!string.IsNullOrEmpty(sMATF_Code))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cette famille de matériel est associée à au moins un type de matériel. Elle ne peut donc pas être supprimée.", "Suppression d'une famille matériel annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
            }
            if (e.Cancel == false)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    e.Cancel = true;
                }
            }
        }

        private void ssMarqueMateriel_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void txtNbHeuresTravail_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 27)
            {
                txtNbHeuresTravail.Text = General.sNbHeuresTotales;
                KeyAscii = 0;
            }
            else if (KeyAscii == 13)
            {
                fc_AfficheVentillationEntretien();
                KeyAscii = 0;
            }
        }

        private void txtNbHeuresTravail_TextChanged(object sender, EventArgs e)
        {
            fc_AfficheVentillationEntretien();
        }

        private void txtTolerance_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 27)
            {
                txtTolerance.Text = General.sToleranceEntretien;
                KeyAscii = 0;
            }
            else if (KeyAscii == 13)
            {
                fc_AfficheVentillationEntretien();
                KeyAscii = 0;
            }
        }

        private void txtNbHeuresTravail_Validating(object sender, CancelEventArgs e)
        {
            if (!General.IsNumeric(txtNbHeuresTravail.Text))
            {
                txtNbHeuresTravail.Text = General.sNbHeuresTotales;
            }
        }

        private void txtTolerance_Validating(object sender, CancelEventArgs e)
        {
            if (!General.IsNumeric(txtTolerance.Text))
            {
                txtTolerance.Text = General.sToleranceEntretien;
            }
            else
            {
                if (Convert.ToDouble(txtTolerance.Text) > 100)
                {
                    txtTolerance.Text = "100";
                }
                if (Convert.ToDouble(txtTolerance.Text) < 0)
                {
                    txtTolerance.Text = Convert.ToString(Convert.ToDouble(txtTolerance.Text) * -1);
                }
                if (Convert.ToDouble(txtTolerance.Text) > 100)
                {
                    txtTolerance.Text = "100";
                }
            }
        }

        private void fc_LoadGAM()
        {
            string sSQL = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                sSQL = "";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var ModAdo = new ModAdo();
                rsGAM = ModAdo.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridGam.DataSource = rsGAM;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }
        }

        private void Command6_Click(object sender, EventArgs e)
        {
            //fc_Circuit();//unused function
        }

        private void Command7_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    MATF_Code as \"Code\", MATF_Libelle as \"Libelle\"" + " FROM   MATF_familleMateriel";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche famille matériel" };
                //with3.txtNomFicheAppelante.Text = this.Name + "cmdRechercheMatricule_Click";
                with3.SetValues(new Dictionary<string, string> { { "MATF_Libelle", txtMATF_Libelle.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMATF_Libelle.Text = with3.ugResultat.ActiveRow.Cells["Libelle"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command7_Click");
            }
        }

        private void Command8_Click(object sender, EventArgs e)
        {
            FC_UpdateGrille();
        }

        private void Command9_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT    Marque as \"Marque\" FROM   MarqueMateriel";
                var with3 = new SearchTemplate(null, null, req, "", "") { Text = "Recherche de Marque de matériel" };
                //with3.txtNomFicheAppelante.Text = this.Name + "Command4_Click";
                with3.SetValues(new Dictionary<string, string> { { "Marque", txtMarque.Text } });
                with3.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMarque.Text = with3.ugResultat.ActiveRow.Cells["Marque"].Value + "";
                    with3.Close();
                };
                with3.ShowDialog();
                with3.Close();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command9_Click");
            }
        }

        private void Text3_TextChanged(object sender, EventArgs e)
        {

        }

        private void ultraGroupBox6_Click(object sender, EventArgs e)
        {

        }

        private void Text1_TextChanged(object sender, EventArgs e)
        {

        }

        private void ultraGroupBox5_Click(object sender, EventArgs e)
        {

        }

        private void label11_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel22_Paint(object sender, PaintEventArgs e)
        {

        }

        //this code already exist in infragistics
        //private void ssCAI_CategoriInterv_HeadClick(System.Object eventSender, AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_HeadClickEvent eventArgs)
        //{
        //    sOrderByTypeVisite = "ORDER BY " + ssCAI_CategoriInterv.Columns[eventArgs.colIndex].Name + " ";
        //    if (sAscDesc == "ASC")
        //    {
        //        sAscDesc = "DESC";
        //    }
        //    else
        //    {
        //        sAscDesc = "ASC";
        //    }

        //    sOrderByTypeVisite = sOrderByTypeVisite + sAscDesc;

        //    fc_TypeVisite();

        //}
    }
}