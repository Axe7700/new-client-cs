﻿namespace Axe_interDT.Views.BaseDeDonnees.Groupe.Forms
{
    partial class frmAddContact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.txtTypeContact = new iTalk.iTalk_TextBox_Small2();
            this.txtReferentiel = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdInsererContact = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtID_Contact = new iTalk.iTalk_TextBox_Small2();
            this.txtNom = new iTalk.iTalk_TextBox_Small2();
            this.txtPrenom = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresse = new iTalk.iTalk_TextBox_Small2();
            this.txtCP = new iTalk.iTalk_TextBox_Small2();
            this.txtVille = new iTalk.iTalk_TextBox_Small2();
            this.cmbCivilite = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmbCodeQualif = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtEmail = new iTalk.iTalk_TextBox_Small2();
            this.txtTel = new iTalk.iTalk_TextBox_Small2();
            this.txtPortable = new iTalk.iTalk_TextBox_Small2();
            this.txtFax = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleQualif = new iTalk.iTalk_TextBox_Small2();
            this.chkInactif = new System.Windows.Forms.CheckBox();
            this.txtNote = new iTalk.iTalk_RichTextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtCreeLe = new iTalk.iTalk_TextBox_Small2();
            this.txtModifierLe = new iTalk.iTalk_TextBox_Small2();
            this.txtCreePar = new iTalk.iTalk_TextBox_Small2();
            this.txtModifierPar = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCivilite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeQualif)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 75F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(768, 541);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 53.28084F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 46.71916F));
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(762, 69);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.txtTypeContact);
            this.flowLayoutPanel1.Controls.Add(this.txtReferentiel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(400, 63);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // txtTypeContact
            // 
            this.txtTypeContact.AccAcceptNumbersOnly = false;
            this.txtTypeContact.AccAllowComma = false;
            this.txtTypeContact.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTypeContact.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTypeContact.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTypeContact.AccHidenValue = "";
            this.txtTypeContact.AccNotAllowedChars = null;
            this.txtTypeContact.AccReadOnly = false;
            this.txtTypeContact.AccReadOnlyAllowDelete = false;
            this.txtTypeContact.AccRequired = false;
            this.txtTypeContact.BackColor = System.Drawing.Color.White;
            this.txtTypeContact.CustomBackColor = System.Drawing.Color.White;
            this.txtTypeContact.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTypeContact.ForeColor = System.Drawing.Color.Black;
            this.txtTypeContact.Location = new System.Drawing.Point(2, 2);
            this.txtTypeContact.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeContact.MaxLength = 32767;
            this.txtTypeContact.Multiline = false;
            this.txtTypeContact.Name = "txtTypeContact";
            this.txtTypeContact.ReadOnly = false;
            this.txtTypeContact.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTypeContact.Size = new System.Drawing.Size(218, 27);
            this.txtTypeContact.TabIndex = 502;
            this.txtTypeContact.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTypeContact.UseSystemPasswordChar = false;
            this.txtTypeContact.Visible = false;
            // 
            // txtReferentiel
            // 
            this.txtReferentiel.AccAcceptNumbersOnly = false;
            this.txtReferentiel.AccAllowComma = false;
            this.txtReferentiel.AccBackgroundColor = System.Drawing.Color.White;
            this.txtReferentiel.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtReferentiel.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtReferentiel.AccHidenValue = "";
            this.txtReferentiel.AccNotAllowedChars = null;
            this.txtReferentiel.AccReadOnly = false;
            this.txtReferentiel.AccReadOnlyAllowDelete = false;
            this.txtReferentiel.AccRequired = false;
            this.txtReferentiel.BackColor = System.Drawing.Color.White;
            this.txtReferentiel.CustomBackColor = System.Drawing.Color.White;
            this.txtReferentiel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtReferentiel.ForeColor = System.Drawing.Color.Black;
            this.txtReferentiel.Location = new System.Drawing.Point(2, 33);
            this.txtReferentiel.Margin = new System.Windows.Forms.Padding(2);
            this.txtReferentiel.MaxLength = 32767;
            this.txtReferentiel.Multiline = false;
            this.txtReferentiel.Name = "txtReferentiel";
            this.txtReferentiel.ReadOnly = false;
            this.txtReferentiel.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtReferentiel.Size = new System.Drawing.Size(218, 27);
            this.txtReferentiel.TabIndex = 503;
            this.txtReferentiel.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtReferentiel.UseSystemPasswordChar = false;
            this.txtReferentiel.Visible = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.cmdInsererContact);
            this.flowLayoutPanel2.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel2.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel2.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(409, 3);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(350, 63);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // cmdInsererContact
            // 
            this.cmdInsererContact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdInsererContact.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdInsererContact.FlatAppearance.BorderSize = 0;
            this.cmdInsererContact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdInsererContact.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdInsererContact.ForeColor = System.Drawing.Color.White;
            this.cmdInsererContact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdInsererContact.Location = new System.Drawing.Point(2, 2);
            this.cmdInsererContact.Margin = new System.Windows.Forms.Padding(2);
            this.cmdInsererContact.Name = "cmdInsererContact";
            this.cmdInsererContact.Size = new System.Drawing.Size(150, 35);
            this.cmdInsererContact.TabIndex = 510;
            this.cmdInsererContact.Text = "Inserer ce contact";
            this.cmdInsererContact.UseVisualStyleBackColor = false;
            this.cmdInsererContact.Click += new System.EventHandler(this.cmdInsererContact_Click);
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(156, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 511;
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(220, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 512;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(284, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 513;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 102F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 99F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 83F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 145F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel3.Controls.Add(this.label12, 0, 12);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.txtID_Contact, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtNom, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtPrenom, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtAdresse, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtCP, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.txtVille, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.cmbCivilite, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmbCodeQualif, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.txtEmail, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.txtTel, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.txtPortable, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.txtFax, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.txtLibelleQualif, 2, 7);
            this.tableLayoutPanel3.Controls.Add(this.chkInactif, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtNote, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.label13, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.label14, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.label15, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.label16, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtCreeLe, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtModifierLe, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtCreePar, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtModifierPar, 6, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 75);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 13;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 9F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(768, 466);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 372);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(44, 19);
            this.label12.TabIndex = 508;
            this.label12.Text = "Note";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 31);
            this.label33.TabIndex = 384;
            this.label33.Text = "ID contact";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 31);
            this.label1.TabIndex = 385;
            this.label1.Text = "Civilité";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 31);
            this.label2.TabIndex = 386;
            this.label2.Text = "Nom";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(96, 31);
            this.label6.TabIndex = 390;
            this.label6.Text = "Prénom";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 124);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 31);
            this.label5.TabIndex = 389;
            this.label5.Text = "Adresse";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 155);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 31);
            this.label4.TabIndex = 388;
            this.label4.Text = "CP";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 31);
            this.label3.TabIndex = 387;
            this.label3.Text = "Ville";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 217);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 31);
            this.label10.TabIndex = 394;
            this.label10.Text = "Code Qualif";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 248);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 31);
            this.label9.TabIndex = 393;
            this.label9.Text = "Email";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 279);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(96, 31);
            this.label11.TabIndex = 395;
            this.label11.Text = "Téléphone";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 310);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(96, 31);
            this.label8.TabIndex = 392;
            this.label8.Text = "Portable";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 341);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(96, 31);
            this.label7.TabIndex = 391;
            this.label7.Text = "Fax";
            // 
            // txtID_Contact
            // 
            this.txtID_Contact.AccAcceptNumbersOnly = false;
            this.txtID_Contact.AccAllowComma = false;
            this.txtID_Contact.AccBackgroundColor = System.Drawing.Color.White;
            this.txtID_Contact.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtID_Contact.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtID_Contact.AccHidenValue = "";
            this.txtID_Contact.AccNotAllowedChars = null;
            this.txtID_Contact.AccReadOnly = true;
            this.txtID_Contact.AccReadOnlyAllowDelete = false;
            this.txtID_Contact.AccRequired = false;
            this.txtID_Contact.BackColor = System.Drawing.Color.White;
            this.txtID_Contact.CustomBackColor = System.Drawing.Color.White;
            this.txtID_Contact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtID_Contact.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtID_Contact.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtID_Contact.Location = new System.Drawing.Point(104, 2);
            this.txtID_Contact.Margin = new System.Windows.Forms.Padding(2);
            this.txtID_Contact.MaxLength = 32767;
            this.txtID_Contact.Multiline = false;
            this.txtID_Contact.Name = "txtID_Contact";
            this.txtID_Contact.ReadOnly = false;
            this.txtID_Contact.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtID_Contact.Size = new System.Drawing.Size(95, 27);
            this.txtID_Contact.TabIndex = 0;
            this.txtID_Contact.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtID_Contact.UseSystemPasswordChar = false;
            // 
            // txtNom
            // 
            this.txtNom.AccAcceptNumbersOnly = false;
            this.txtNom.AccAllowComma = false;
            this.txtNom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNom.AccHidenValue = "";
            this.txtNom.AccNotAllowedChars = null;
            this.txtNom.AccReadOnly = false;
            this.txtNom.AccReadOnlyAllowDelete = false;
            this.txtNom.AccRequired = false;
            this.txtNom.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtNom, 2);
            this.txtNom.CustomBackColor = System.Drawing.Color.White;
            this.txtNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNom.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNom.ForeColor = System.Drawing.Color.Black;
            this.txtNom.Location = new System.Drawing.Point(104, 64);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.MaxLength = 32767;
            this.txtNom.Multiline = false;
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = false;
            this.txtNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNom.Size = new System.Drawing.Size(204, 27);
            this.txtNom.TabIndex = 2;
            this.txtNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNom.UseSystemPasswordChar = false;
            // 
            // txtPrenom
            // 
            this.txtPrenom.AccAcceptNumbersOnly = false;
            this.txtPrenom.AccAllowComma = false;
            this.txtPrenom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPrenom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPrenom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPrenom.AccHidenValue = "";
            this.txtPrenom.AccNotAllowedChars = null;
            this.txtPrenom.AccReadOnly = false;
            this.txtPrenom.AccReadOnlyAllowDelete = false;
            this.txtPrenom.AccRequired = false;
            this.txtPrenom.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtPrenom, 2);
            this.txtPrenom.CustomBackColor = System.Drawing.Color.White;
            this.txtPrenom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPrenom.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtPrenom.ForeColor = System.Drawing.Color.Black;
            this.txtPrenom.Location = new System.Drawing.Point(104, 95);
            this.txtPrenom.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrenom.MaxLength = 32767;
            this.txtPrenom.Multiline = false;
            this.txtPrenom.Name = "txtPrenom";
            this.txtPrenom.ReadOnly = false;
            this.txtPrenom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPrenom.Size = new System.Drawing.Size(204, 27);
            this.txtPrenom.TabIndex = 3;
            this.txtPrenom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPrenom.UseSystemPasswordChar = false;
            // 
            // txtAdresse
            // 
            this.txtAdresse.AccAcceptNumbersOnly = false;
            this.txtAdresse.AccAllowComma = false;
            this.txtAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresse.AccHidenValue = "";
            this.txtAdresse.AccNotAllowedChars = null;
            this.txtAdresse.AccReadOnly = false;
            this.txtAdresse.AccReadOnlyAllowDelete = false;
            this.txtAdresse.AccRequired = false;
            this.txtAdresse.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtAdresse, 2);
            this.txtAdresse.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAdresse.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse.Location = new System.Drawing.Point(104, 126);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.MaxLength = 32767;
            this.txtAdresse.Multiline = false;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.ReadOnly = false;
            this.txtAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse.Size = new System.Drawing.Size(204, 27);
            this.txtAdresse.TabIndex = 4;
            this.txtAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse.UseSystemPasswordChar = false;
            // 
            // txtCP
            // 
            this.txtCP.AccAcceptNumbersOnly = false;
            this.txtCP.AccAllowComma = false;
            this.txtCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCP.AccHidenValue = "";
            this.txtCP.AccNotAllowedChars = null;
            this.txtCP.AccReadOnly = false;
            this.txtCP.AccReadOnlyAllowDelete = false;
            this.txtCP.AccRequired = false;
            this.txtCP.BackColor = System.Drawing.Color.White;
            this.txtCP.CustomBackColor = System.Drawing.Color.White;
            this.txtCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCP.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCP.ForeColor = System.Drawing.Color.Black;
            this.txtCP.Location = new System.Drawing.Point(104, 157);
            this.txtCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtCP.MaxLength = 32767;
            this.txtCP.Multiline = false;
            this.txtCP.Name = "txtCP";
            this.txtCP.ReadOnly = false;
            this.txtCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCP.Size = new System.Drawing.Size(95, 27);
            this.txtCP.TabIndex = 5;
            this.txtCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCP.UseSystemPasswordChar = false;
            // 
            // txtVille
            // 
            this.txtVille.AccAcceptNumbersOnly = false;
            this.txtVille.AccAllowComma = false;
            this.txtVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVille.AccHidenValue = "";
            this.txtVille.AccNotAllowedChars = null;
            this.txtVille.AccReadOnly = false;
            this.txtVille.AccReadOnlyAllowDelete = false;
            this.txtVille.AccRequired = false;
            this.txtVille.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtVille, 2);
            this.txtVille.CustomBackColor = System.Drawing.Color.White;
            this.txtVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVille.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtVille.ForeColor = System.Drawing.Color.Black;
            this.txtVille.Location = new System.Drawing.Point(104, 188);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.MaxLength = 32767;
            this.txtVille.Multiline = false;
            this.txtVille.Name = "txtVille";
            this.txtVille.ReadOnly = false;
            this.txtVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVille.Size = new System.Drawing.Size(204, 27);
            this.txtVille.TabIndex = 6;
            this.txtVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVille.UseSystemPasswordChar = false;
            // 
            // cmbCivilite
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCivilite.DisplayLayout.Appearance = appearance1;
            this.cmbCivilite.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCivilite.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCivilite.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCivilite.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbCivilite.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCivilite.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbCivilite.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCivilite.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCivilite.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCivilite.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbCivilite.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCivilite.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCivilite.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCivilite.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbCivilite.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCivilite.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCivilite.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbCivilite.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbCivilite.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCivilite.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbCivilite.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbCivilite.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCivilite.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbCivilite.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCivilite.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCivilite.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCivilite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCivilite.Location = new System.Drawing.Point(105, 34);
            this.cmbCivilite.Name = "cmbCivilite";
            this.cmbCivilite.Size = new System.Drawing.Size(93, 22);
            this.cmbCivilite.TabIndex = 1;
            this.cmbCivilite.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCivilite_BeforeDropDown);
            // 
            // cmbCodeQualif
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCodeQualif.DisplayLayout.Appearance = appearance13;
            this.cmbCodeQualif.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCodeQualif.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeQualif.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeQualif.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbCodeQualif.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeQualif.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbCodeQualif.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCodeQualif.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCodeQualif.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCodeQualif.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbCodeQualif.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCodeQualif.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeQualif.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCodeQualif.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbCodeQualif.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCodeQualif.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeQualif.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbCodeQualif.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbCodeQualif.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCodeQualif.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbCodeQualif.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbCodeQualif.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCodeQualif.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbCodeQualif.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCodeQualif.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCodeQualif.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCodeQualif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeQualif.Location = new System.Drawing.Point(105, 220);
            this.cmbCodeQualif.Name = "cmbCodeQualif";
            this.cmbCodeQualif.Size = new System.Drawing.Size(93, 22);
            this.cmbCodeQualif.TabIndex = 7;
            this.cmbCodeQualif.AfterCloseUp += new System.EventHandler(this.cmbCodeQualif_AfterCloseUp);
            this.cmbCodeQualif.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCodeQualif_BeforeDropDown);
            // 
            // txtEmail
            // 
            this.txtEmail.AccAcceptNumbersOnly = false;
            this.txtEmail.AccAllowComma = false;
            this.txtEmail.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEmail.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEmail.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEmail.AccHidenValue = "";
            this.txtEmail.AccNotAllowedChars = null;
            this.txtEmail.AccReadOnly = false;
            this.txtEmail.AccReadOnlyAllowDelete = false;
            this.txtEmail.AccRequired = false;
            this.txtEmail.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtEmail, 2);
            this.txtEmail.CustomBackColor = System.Drawing.Color.White;
            this.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEmail.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(104, 250);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.MaxLength = 32767;
            this.txtEmail.Multiline = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = false;
            this.txtEmail.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.Size = new System.Drawing.Size(204, 27);
            this.txtEmail.TabIndex = 8;
            this.txtEmail.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEmail.UseSystemPasswordChar = false;
            // 
            // txtTel
            // 
            this.txtTel.AccAcceptNumbersOnly = false;
            this.txtTel.AccAllowComma = false;
            this.txtTel.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTel.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTel.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTel.AccHidenValue = "";
            this.txtTel.AccNotAllowedChars = null;
            this.txtTel.AccReadOnly = false;
            this.txtTel.AccReadOnlyAllowDelete = false;
            this.txtTel.AccRequired = false;
            this.txtTel.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtTel, 2);
            this.txtTel.CustomBackColor = System.Drawing.Color.White;
            this.txtTel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTel.ForeColor = System.Drawing.Color.Black;
            this.txtTel.Location = new System.Drawing.Point(104, 281);
            this.txtTel.Margin = new System.Windows.Forms.Padding(2);
            this.txtTel.MaxLength = 32767;
            this.txtTel.Multiline = false;
            this.txtTel.Name = "txtTel";
            this.txtTel.ReadOnly = false;
            this.txtTel.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTel.Size = new System.Drawing.Size(204, 27);
            this.txtTel.TabIndex = 9;
            this.txtTel.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTel.UseSystemPasswordChar = false;
            // 
            // txtPortable
            // 
            this.txtPortable.AccAcceptNumbersOnly = false;
            this.txtPortable.AccAllowComma = false;
            this.txtPortable.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPortable.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPortable.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPortable.AccHidenValue = "";
            this.txtPortable.AccNotAllowedChars = null;
            this.txtPortable.AccReadOnly = false;
            this.txtPortable.AccReadOnlyAllowDelete = false;
            this.txtPortable.AccRequired = false;
            this.txtPortable.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtPortable, 2);
            this.txtPortable.CustomBackColor = System.Drawing.Color.White;
            this.txtPortable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPortable.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtPortable.ForeColor = System.Drawing.Color.Black;
            this.txtPortable.Location = new System.Drawing.Point(104, 312);
            this.txtPortable.Margin = new System.Windows.Forms.Padding(2);
            this.txtPortable.MaxLength = 32767;
            this.txtPortable.Multiline = false;
            this.txtPortable.Name = "txtPortable";
            this.txtPortable.ReadOnly = false;
            this.txtPortable.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPortable.Size = new System.Drawing.Size(204, 27);
            this.txtPortable.TabIndex = 10;
            this.txtPortable.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPortable.UseSystemPasswordChar = false;
            // 
            // txtFax
            // 
            this.txtFax.AccAcceptNumbersOnly = false;
            this.txtFax.AccAllowComma = false;
            this.txtFax.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFax.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFax.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFax.AccHidenValue = "";
            this.txtFax.AccNotAllowedChars = null;
            this.txtFax.AccReadOnly = false;
            this.txtFax.AccReadOnlyAllowDelete = false;
            this.txtFax.AccRequired = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtFax, 2);
            this.txtFax.CustomBackColor = System.Drawing.Color.White;
            this.txtFax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFax.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFax.ForeColor = System.Drawing.Color.Black;
            this.txtFax.Location = new System.Drawing.Point(104, 343);
            this.txtFax.Margin = new System.Windows.Forms.Padding(2);
            this.txtFax.MaxLength = 32767;
            this.txtFax.Multiline = false;
            this.txtFax.Name = "txtFax";
            this.txtFax.ReadOnly = false;
            this.txtFax.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFax.Size = new System.Drawing.Size(204, 27);
            this.txtFax.TabIndex = 11;
            this.txtFax.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFax.UseSystemPasswordChar = false;
            // 
            // txtLibelleQualif
            // 
            this.txtLibelleQualif.AccAcceptNumbersOnly = false;
            this.txtLibelleQualif.AccAllowComma = false;
            this.txtLibelleQualif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleQualif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleQualif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleQualif.AccHidenValue = "";
            this.txtLibelleQualif.AccNotAllowedChars = null;
            this.txtLibelleQualif.AccReadOnly = true;
            this.txtLibelleQualif.AccReadOnlyAllowDelete = false;
            this.txtLibelleQualif.AccRequired = false;
            this.txtLibelleQualif.BackColor = System.Drawing.Color.White;
            this.txtLibelleQualif.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleQualif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleQualif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibelleQualif.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLibelleQualif.Location = new System.Drawing.Point(203, 219);
            this.txtLibelleQualif.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleQualif.MaxLength = 32767;
            this.txtLibelleQualif.Multiline = false;
            this.txtLibelleQualif.Name = "txtLibelleQualif";
            this.txtLibelleQualif.ReadOnly = false;
            this.txtLibelleQualif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleQualif.Size = new System.Drawing.Size(105, 27);
            this.txtLibelleQualif.TabIndex = 515;
            this.txtLibelleQualif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleQualif.UseSystemPasswordChar = false;
            // 
            // chkInactif
            // 
            this.chkInactif.AutoSize = true;
            this.chkInactif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkInactif.Location = new System.Drawing.Point(204, 3);
            this.chkInactif.Name = "chkInactif";
            this.chkInactif.Size = new System.Drawing.Size(81, 23);
            this.chkInactif.TabIndex = 570;
            this.chkInactif.Text = "INACTIF";
            this.chkInactif.UseVisualStyleBackColor = true;
            // 
            // txtNote
            // 
            this.txtNote.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNote.AutoWordSelection = false;
            this.txtNote.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtNote, 3);
            this.txtNote.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNote.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNote.ForeColor = System.Drawing.Color.Black;
            this.txtNote.Location = new System.Drawing.Point(105, 375);
            this.txtNote.Name = "txtNote";
            this.txtNote.ReadOnly = false;
            this.txtNote.Size = new System.Drawing.Size(285, 88);
            this.txtNote.TabIndex = 12;
            this.txtNote.WordWrap = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(313, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(77, 31);
            this.label13.TabIndex = 572;
            this.label13.Text = "Crée Le";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(313, 31);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 31);
            this.label14.TabIndex = 573;
            this.label14.Text = "Modifier Le";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(541, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 31);
            this.label15.TabIndex = 574;
            this.label15.Text = "Crée Par";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(541, 31);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 31);
            this.label16.TabIndex = 575;
            this.label16.Text = "Modifier Par";
            // 
            // txtCreeLe
            // 
            this.txtCreeLe.AccAcceptNumbersOnly = false;
            this.txtCreeLe.AccAllowComma = false;
            this.txtCreeLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCreeLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCreeLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCreeLe.AccHidenValue = "";
            this.txtCreeLe.AccNotAllowedChars = null;
            this.txtCreeLe.AccReadOnly = true;
            this.txtCreeLe.AccReadOnlyAllowDelete = false;
            this.txtCreeLe.AccRequired = false;
            this.txtCreeLe.BackColor = System.Drawing.Color.White;
            this.txtCreeLe.CustomBackColor = System.Drawing.Color.White;
            this.txtCreeLe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCreeLe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCreeLe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCreeLe.Location = new System.Drawing.Point(395, 2);
            this.txtCreeLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreeLe.MaxLength = 32767;
            this.txtCreeLe.Multiline = false;
            this.txtCreeLe.Name = "txtCreeLe";
            this.txtCreeLe.ReadOnly = false;
            this.txtCreeLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCreeLe.Size = new System.Drawing.Size(141, 27);
            this.txtCreeLe.TabIndex = 576;
            this.txtCreeLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCreeLe.UseSystemPasswordChar = false;
            // 
            // txtModifierLe
            // 
            this.txtModifierLe.AccAcceptNumbersOnly = false;
            this.txtModifierLe.AccAllowComma = false;
            this.txtModifierLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtModifierLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtModifierLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtModifierLe.AccHidenValue = "";
            this.txtModifierLe.AccNotAllowedChars = null;
            this.txtModifierLe.AccReadOnly = true;
            this.txtModifierLe.AccReadOnlyAllowDelete = false;
            this.txtModifierLe.AccRequired = false;
            this.txtModifierLe.BackColor = System.Drawing.Color.White;
            this.txtModifierLe.CustomBackColor = System.Drawing.Color.White;
            this.txtModifierLe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModifierLe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtModifierLe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtModifierLe.Location = new System.Drawing.Point(395, 33);
            this.txtModifierLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtModifierLe.MaxLength = 32767;
            this.txtModifierLe.Multiline = false;
            this.txtModifierLe.Name = "txtModifierLe";
            this.txtModifierLe.ReadOnly = false;
            this.txtModifierLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtModifierLe.Size = new System.Drawing.Size(141, 27);
            this.txtModifierLe.TabIndex = 577;
            this.txtModifierLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtModifierLe.UseSystemPasswordChar = false;
            // 
            // txtCreePar
            // 
            this.txtCreePar.AccAcceptNumbersOnly = false;
            this.txtCreePar.AccAllowComma = false;
            this.txtCreePar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCreePar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCreePar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCreePar.AccHidenValue = "";
            this.txtCreePar.AccNotAllowedChars = null;
            this.txtCreePar.AccReadOnly = true;
            this.txtCreePar.AccReadOnlyAllowDelete = false;
            this.txtCreePar.AccRequired = false;
            this.txtCreePar.BackColor = System.Drawing.Color.White;
            this.txtCreePar.CustomBackColor = System.Drawing.Color.White;
            this.txtCreePar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCreePar.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCreePar.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCreePar.Location = new System.Drawing.Point(629, 2);
            this.txtCreePar.Margin = new System.Windows.Forms.Padding(2);
            this.txtCreePar.MaxLength = 32767;
            this.txtCreePar.Multiline = false;
            this.txtCreePar.Name = "txtCreePar";
            this.txtCreePar.ReadOnly = false;
            this.txtCreePar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCreePar.Size = new System.Drawing.Size(137, 27);
            this.txtCreePar.TabIndex = 579;
            this.txtCreePar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCreePar.UseSystemPasswordChar = false;
            // 
            // txtModifierPar
            // 
            this.txtModifierPar.AccAcceptNumbersOnly = false;
            this.txtModifierPar.AccAllowComma = false;
            this.txtModifierPar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtModifierPar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtModifierPar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtModifierPar.AccHidenValue = "";
            this.txtModifierPar.AccNotAllowedChars = null;
            this.txtModifierPar.AccReadOnly = true;
            this.txtModifierPar.AccReadOnlyAllowDelete = false;
            this.txtModifierPar.AccRequired = false;
            this.txtModifierPar.BackColor = System.Drawing.Color.White;
            this.txtModifierPar.CustomBackColor = System.Drawing.Color.White;
            this.txtModifierPar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModifierPar.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtModifierPar.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtModifierPar.Location = new System.Drawing.Point(629, 33);
            this.txtModifierPar.Margin = new System.Windows.Forms.Padding(2);
            this.txtModifierPar.MaxLength = 32767;
            this.txtModifierPar.Multiline = false;
            this.txtModifierPar.Name = "txtModifierPar";
            this.txtModifierPar.ReadOnly = false;
            this.txtModifierPar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtModifierPar.Size = new System.Drawing.Size(137, 27);
            this.txtModifierPar.TabIndex = 578;
            this.txtModifierPar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtModifierPar.UseSystemPasswordChar = false;
            // 
            // frmAddContact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(768, 541);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(784, 580);
            this.MinimumSize = new System.Drawing.Size(784, 580);
            this.Name = "frmAddContact";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAddContact";
            this.VisibleChanged += new System.EventHandler(this.frmAddContact_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCivilite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeQualif)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 txtTypeContact;
        public iTalk.iTalk_TextBox_Small2 txtReferentiel;
        public System.Windows.Forms.Button cmdInsererContact;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button CmdSauver;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtID_Contact;
        public iTalk.iTalk_TextBox_Small2 txtVille;
        public iTalk.iTalk_TextBox_Small2 txtNom;
        public iTalk.iTalk_TextBox_Small2 txtPrenom;
        public iTalk.iTalk_TextBox_Small2 txtAdresse;
        public iTalk.iTalk_TextBox_Small2 txtCP;
        private System.Windows.Forms.Label label12;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCivilite;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCodeQualif;
        public iTalk.iTalk_TextBox_Small2 txtEmail;
        public iTalk.iTalk_TextBox_Small2 txtTel;
        public iTalk.iTalk_TextBox_Small2 txtPortable;
        public iTalk.iTalk_TextBox_Small2 txtFax;
        public iTalk.iTalk_TextBox_Small2 txtLibelleQualif;
        private System.Windows.Forms.CheckBox chkInactif;
        private iTalk.iTalk_RichTextBox txtNote;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        public iTalk.iTalk_TextBox_Small2 txtCreeLe;
        public iTalk.iTalk_TextBox_Small2 txtModifierLe;
        public iTalk.iTalk_TextBox_Small2 txtCreePar;
        public iTalk.iTalk_TextBox_Small2 txtModifierPar;
    }
}