﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.BaseDeDonnees.Groupe.Forms
{
    public partial class frmAddContact : Form
    {
        public frmAddContact()
        {
            InitializeComponent();
        }
        bool bLoadFormModal;
        /// <summary>
        /// Tested dans le cas txtTypeContact.Text="CONTACTGROUPE" pour la fiche UserDocGroupe
        /// 
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdInsererContact_Click(object sender, EventArgs e)
        {
            string stemp = null;
            string sSQL = null;
            int lReturn = 0;

            try { 
            if (string.IsNullOrEmpty(txtID_Contact.Text))//tested pour la fiche UserDocGroupe
                {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider avant d'insérer ce contact.");
                return;
            }


            stemp = "Voulez vous insérer ce contact dans la fiche ";

            //fc_sauver

            if (string.IsNullOrEmpty(txtTypeContact.Text))//Tested pour la fiche UserDocGroupe
                {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur inattendue est survenue, l'insertion du contact est annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

            }
            else if (txtTypeContact.Text.ToUpper().ToString() == General.cContactSite.ToUpper().ToString())
            {
                stemp = stemp + "site du " + txtReferentiel.Text + "?";
                DialogResult dialogresult = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(stemp, "Insérer un contact", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogresult == DialogResult.No)
                {
                    return;
                }

                General.fc_InsertContact(General.cContactSite, Convert.ToInt32(txtID_Contact.Text), txtReferentiel.Text);

            }
            else if (txtTypeContact.Text.ToUpper().ToString() == General.cContactClient.ToUpper().ToString())
            {
                stemp = stemp + "client du " + txtReferentiel.Text + "?";
                DialogResult dialogresult = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(stemp, "Insérer un contact", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogresult == DialogResult.No)
                    {
                        return;
                    }

                    General.fc_InsertContact(General.cContactClient, Convert.ToInt32(txtID_Contact.Text), txtReferentiel.Text);


            }
            else if (txtTypeContact.Text.ToUpper().ToString() == General.cContactGroupe.ToUpper().ToString())//Testeed
            {
                stemp = stemp + "client du " + txtReferentiel.Text + "?";
                    DialogResult dialogresult = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(stemp, "Insérer un contact", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogresult == DialogResult.No)
                    {
                        return;
                    }

                    General.fc_InsertContact(General.cContactGroupe, Convert.ToInt32(txtID_Contact.Text), txtReferentiel.Text);

            }

            this.Close();
            return;
        }
           catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";cmdInsererContact_Click");
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            string scode = null;
            string requete;
            string where = "";
            try
            {
              
                bLoadFormModal = true;
                requete = "SELECT     Contact.ID_Contact as \"ID_Contact\", " + " Contact.Nom as \"Nom\", Contact.Prenom as \"Prenom\", " + " Qualification.Qualification as \"Qualification\"" + " FROM         Contact LEFT OUTER JOIN" + " Qualification ON Contact.CodeQualif = Qualification.CodeQualif";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche des contacts" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                 {
                     scode = fg.ugResultat.ActiveRow.Cells["ID_Contact"].Value.ToString();
                     fc_LoadContact(Convert.ToInt16(General.nz(scode, 0)));
                     fg.Dispose(); fg.Close();
                 };
               fg.ugResultat.KeyDown += (se, ev) =>
               {
                   if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                   {
                       scode = fg.ugResultat.ActiveRow.Cells["ID_Contact"].Value.ToString();
                       fc_LoadContact(Convert.ToInt16(General.nz(scode, 0)));
                   }
                   fg.Dispose();fg.Close();
               };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                return;
            }
            catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
          
            return;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_clear();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_clear()
        {

            txtID_Contact.Text = "";
            txtNom.Text = "";
            txtPrenom.Text = "";
            txtAdresse.Text = "";
            txtCP.Text = "";
            txtVille.Text = "";
            txtEmail.Text = "";
            txtTel.Text = "";
            txtPortable.Text = "";
            txtFax.Text = "";
            txtNote.Text = "";
            txtCreeLe.Text = "";
            txtCreePar.Text = "";
            txtModifierLe.Text = "";
            txtModifierPar.Text = "";
            cmbCivilite.Text = "";
            cmbCodeQualif.Text = "";
            txtLibelleQualif.Text = "";
            chkInactif.CheckState = System.Windows.Forms.CheckState.Unchecked;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_sauver();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_sauver()
        {
            try {
            string sSQL = null;
            DataTable rs = default(DataTable);

                
                ModAdo modAdoDTrs = new ModAdo();
            System.DateTime dtNow = default(System.DateTime);
           
                dtNow = DateTime.Now;
            sSQL = "SELECT ID_Contact,Civilite, Nom, Prenom,"
                    + " CodeQualif, Email, Tel, Portable, Fax, Note, Adresse," 
                    + " CP, Ville, CreeLe, CreePar, " + " ModifierLe , ModifierPar, Inactif " +
                    " FROM         Contact";

            if (string.IsNullOrEmpty(txtID_Contact.Text))//tested
            {
                sSQL = sSQL + " WHERE ID_Contact =0";
            }
            else//tested
            {
                sSQL = sSQL + " WHERE ID_Contact =" + txtID_Contact.Text;
            }
         
                 rs = modAdoDTrs.fc_OpenRecordSet(sSQL);
                 SqlCommandBuilder cb = new SqlCommandBuilder(modAdoDTrs.SDArsAdo);
                 modAdoDTrs.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                 modAdoDTrs. SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                 SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                 insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                 insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                 modAdoDTrs.SDArsAdo.InsertCommand = insertCmd;

                 modAdoDTrs.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                 {
                     if (e.StatementType == StatementType.Insert)
                     {
                         if (string.IsNullOrEmpty(txtID_Contact.Text))
             {

                             txtID_Contact.Text =
                               e.Command.Parameters["@ID"].Value.ToString();
}
                     }
                 });
               
                var NewRow = rs.NewRow(); 
            if (string.IsNullOrEmpty(txtID_Contact.Text))
            {
                    //rs.AddNew();
                    //NewRow = rs.NewRow();
                    rs.Rows.Add(NewRow);
                }


                //txtID_Contact = rs!ID_Contact & ""

                rs.Rows[0]["Civilite"] = General.nz(cmbCivilite.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Nom"] = General.nz(txtNom.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Prenom"] = General.nz(txtPrenom.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Adresse"] = General.nz(txtAdresse.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["CP"] = General.nz(txtCP.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Ville"] = General.nz(txtVille.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Email"] = General.nz(txtEmail.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Tel"] = General.nz(txtTel.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Portable"] = General.nz(txtPortable.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["fax"] = General.nz(txtFax.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Note"] = General.nz(txtNote.Text, System.DBNull.Value).ToString().Trim();
                if (string.IsNullOrEmpty(txtCreeLe.Text))
            {                   
                    rs.Rows[0]["CreeLe"] = dtNow;
                    txtCreeLe.Text = rs.Rows[0]["CreeLe"].ToString();
                }
            if (string.IsNullOrEmpty(txtCreePar.Text))
            {
                 
                   rs.Rows[0]["CreePar"] = General.fncUserName();
                   txtCreePar.Text = rs.Rows[0]["CreePar"].ToString();
                }

                txtModifierLe.Text = Convert.ToString(dtNow);
                rs.Rows[0]["ModifierLe"] = txtModifierLe.Text;            
                rs.Rows[0]["ModifierLe"] = txtModifierLe.Text;
                txtModifierPar.Text = General.fncUserName();             
                rs.Rows[0]["ModifierPar"]= txtModifierPar.Text;
                rs.Rows[0]["CodeQualif"] = General.nz(cmbCodeQualif.Text, System.DBNull.Value).ToString().Trim();
                rs.Rows[0]["Inactif"] = chkInactif.CheckState;
                //rs.Rows.Add(NewRow);
                int xx =modAdoDTrs.Update();
             
          /*      if (string.IsNullOrEmpty(txtID_Contact.Text))
            {
                   // rs.MoveLast();
                   string req = "select Max(ID_Contact) From   Contact ";
                  string LastID= modAdoDTrs.fc_ADOlibelle(req);
                    txtID_Contact.Text = LastID;
                }
                */
            modAdoDTrs.Dispose();
                rs = null;
            return;
                
            }
        catch(Exception e){
              Erreurs.gFr_debug(e,this.Name + ";fc_sauver");
            }
          
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmAddContact_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
           
                if (!string.IsNullOrEmpty(txtID_Contact.Text))//tested
                {
                 
                    fc_LoadContact(Convert.ToInt16(General.nz(txtID_Contact.Text,0)));
                }
                else
                {
                    //fc_clear
                }
            
            

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lID_contact"></param>
        private void fc_LoadContact(int lID_contact)
        {
            try { 
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdorsDT = new ModAdo(); 
            
            sSQL = "SELECT ID_Contact,  Civilite, Nom, Prenom," 
                    + " CodeQualif, Email, Tel, Portable, Fax, Note, PresidentCS, Adresse," 
                    + " CP, Ville, CreeLe, CreePar, " 
                    + " ModifierLe , ModifierPar, Inactif " 
                    + " FROM         Contact" + " WHERE ID_Contact =" + lID_contact;
            rs = modAdorsDT.fc_OpenRecordSet(sSQL);
           
            txtID_Contact.Text = rs.Rows[0]["ID_Contact"]+ "";
            cmbCivilite.Text = rs.Rows[0]["Civilite"]+ "";
            txtNom.Text = rs.Rows[0]["Nom"]+ "";
            txtPrenom.Text = rs.Rows[0]["Prenom"] + "";
            txtAdresse.Text = rs.Rows[0]["Adresse"] + "";
            txtCP.Text = rs.Rows[0]["CP"]+ "";
            txtVille.Text = rs.Rows[0]["Ville"] + "";
            txtEmail.Text = rs.Rows[0]["Email"] + "";
            txtTel.Text = rs.Rows[0]["Tel"]+ "";
            txtPortable.Text = rs.Rows[0]["Portable"]+ "";
            txtFax.Text = rs.Rows[0]["fax"] + "";
            txtNote.Text = rs.Rows[0]["Note"]+ "";
            txtCreeLe.Text = rs.Rows[0]["CreeLe"] + "";
            txtCreePar.Text = rs.Rows[0]["CreePar"]+ "";
            txtModifierLe.Text = rs.Rows[0]["ModifierLe"] + "";
            txtModifierPar.Text = rs.Rows[0]["ModifierPar"] + "";
            cmbCodeQualif.Text = rs.Rows[0]["CodeQualif"] + "";
            chkInactif.Checked = General.nz( rs.Rows[0]["Inactif"],0).ToString()=="1";

            modAdorsDT.Dispose();
           // modAdorsDT = null;
            return;
        }
          catch(Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + ";fc_LoadContact");
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LibelleQualif()
        {
            string sSQL = null;

            
            if (!string.IsNullOrEmpty(cmbCodeQualif.Text))//tested
            {
                sSQL = "SELECT  Qualification.Qualification" + " FROM Qualification where CodeQualif = '" + StdSQLchaine.gFr_DoublerQuote(cmbCodeQualif.Text) + "'";
                ModAdo tmpModAdo = new ModAdo();
                sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                txtLibelleQualif.Text = sSQL;
            }
            else//tested
            {

                txtLibelleQualif.Text = "";
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCivilite_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            sSQL = "SELECT     Civilte" + " From Civilite";
            sheridan.InitialiseCombo(cmbCivilite, sSQL, "Civilte");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeQualif_AfterCloseUp(object sender, EventArgs e)
        {
            fc_LibelleQualif();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeQualif_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            sSQL = "SELECT Qualification.CodeQualif , Qualification.Qualification" + " FROM Qualification where Correspondant_COR = 1";
            sheridan.InitialiseCombo(cmbCodeQualif, sSQL, "CodeQualif");
        }

        
    }
}
