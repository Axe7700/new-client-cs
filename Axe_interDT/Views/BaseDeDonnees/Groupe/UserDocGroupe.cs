﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using System.Data.SqlClient;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.BaseDeDonnees.Groupe
{
    public partial class UserDocGroupe : UserControl
    {

        private  bool blModif;
        private bool blnAjout;
        DataTable rsClient;
        private ModAdo modAdorsClient;
        DataTable rsContactGroupe;
        private ModAdo modAdorsContactGroupe;
        public UserDocGroupe()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEcritures_Click(object sender, EventArgs e)
        {
           fc_ChargeFinance();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ChargeFinance()
        {
            DataTable rsAdoFinance;
            double MontantCredit = 0;
            double MontantDebit = 0;
            string compteTiers = null;

            string DateMini = null;
            string DateMaxi = null;

            int JourMaxiMois = 0;
            string sDateFacture = null;
            string sCritere = null;
            string sUserNameT = null;

            string BaseClientSage = null;
            string BaseClientSQL = null;

            try { 

            if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
            {
                sCritere = General.cCritereFinance;
            }
            else
            {
                sCritere = "";
            }

            JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));
                
           if (General.gfr_liaison("AfficheExercices","1") == "1")
            {
              //verifier cette visibilité
               // Frame7.Visible=true;
                    if (cmbExercices.ActiveRow != null)
                    {
                        Frame7.Visible = true;
                        DateMini = cmbExercices.ActiveRow.Cells["Du"].Value.ToString();
                        DateMaxi = cmbExercices.ActiveRow.Cells["Au"].Value.ToString();
                    }
}              
           /* else
            {
                Frame7.Visible = false;
            }
              
            if (Frame7.Visible == true)
            {
                    if (cmbExercices.ActiveRow != null)
                    {
                        DateMini = cmbExercices.ActiveRow.Cells["Du"].Value.ToString();
                        DateMaxi = cmbExercices.ActiveRow.Cells["Au"].Value.ToString();
                    }
            }*/
            else
            {
                    Frame7.Visible = false;
                if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                {
                    DateMini = "01/" + "01" + "/" + DateTime.Now.Year;
                    DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + DateTime.Now.Year;
                }
                ///teste
                else
                {
                    if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= Convert.ToInt16(General.MoisFinExercice))//Tested
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year - 1);
                        //MoisFinExercice + 1
                        DateMaxi = JourMaxiMois + "/" + (Convert.ToInt16(General.MoisFinExercice)).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year );
                            //MoisFinExercice
                        }
                    //tested
                    else
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice - 1
                        DateMaxi = JourMaxiMois + "/" + (Convert.ToInt16(General.MoisFinExercice)).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year+ 1);
                        //MoisFinExercice
                    }
                }
            }


            rsAdoFinance = new DataTable();
            SAGE.fc_OpenConnSage();

            var SourceDT = new DataTable();
            SourceDT.Columns.Add("Code gérant");
            SourceDT.Columns.Add("Compte Immeuble");
            SourceDT.Columns.Add("Journal");
            SourceDT.Columns.Add("Date Journal");
            SourceDT.Columns.Add("Date Facture");
            SourceDT.Columns.Add("Référence facture");
            SourceDT.Columns.Add("Echéance");
            SourceDT.Columns.Add("Débit");
            SourceDT.Columns.Add("Crédit");
            SSGridSituation.DataSource = SourceDT;

            if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
            {
                ///''  fc_MajCompteTiersGdp "", txtCode1
            }

            //=== modif du 02/07/2015, création dans sSQL server d'une table temporaire.
            sUserNameT = General.fncUserName();

            General.sSQL = "DELETE FROM TempCT_NumGroupe WHERE UserName = '" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "'";

            //  SAGE.adoSage.Execute(General.sSQL); ==>
            var SCsSql = new SqlCommand(General.sSQL, SAGE.adoSage);
            SCsSql.ExecuteNonQuery();

            BaseClientSage = "[" + SAGE.adoSage.Database + "]";
            BaseClientSQL = "[" + General.adocnn.Database + "]";

            General.sSQL = "INSERT INTO " 
                    + BaseClientSage + ".dbo.TempCT_NumGroupe" 
                    + "(" + BaseClientSage + ".dbo.CT_Num," 
                    + BaseClientSage + ".dbo.UserName," 
                    + BaseClientSage + ".dbo.CRCL_Code," 
                    + BaseClientSage + ".dbo.Codeimmeuble,"
                    + BaseClientSage + ".dbo.Code1)";
            General.sSQL = General.sSQL + "SELECT "
                    + BaseClientSQL + ".dbo.Immeuble.NCompte," + "'" 
                    + sUserNameT + "' AS UserName,"
                    + BaseClientSQL + ".dbo.CRCL_GroupeClient.CRCL_Code," 
                    + BaseClientSQL + ".dbo.Immeuble.Codeimmeuble," 
                    + BaseClientSQL + ".dbo.Immeuble.Code1" 
                    + " FROM         "
                    + BaseClientSQL + ".dbo.Immeuble INNER JOIN " 
                    + BaseClientSQL + ".dbo.Table1 "
                    + " ON "
                    + BaseClientSQL + ".dbo.Immeuble.Code1 ="
                    + BaseClientSQL + ".dbo.Table1.Code1 INNER JOIN "
                    + BaseClientSQL + ".dbo.CRCL_GroupeClient ON "
                    + BaseClientSQL + ".dbo. Table1.CRCL_Code = " 
                    + BaseClientSQL + ".dbo. CRCL_GroupeClient.CRCL_Code"
                    + " WHERE "
                    + BaseClientSQL + ".dbo.CRCL_GroupeClient.CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Code.Text) + "'";
            int xx=General.Execute(General.sSQL);

            //=== Fin modif du 02/07/2015,

            //If optCptGerant = True Then
            //
            //        SSGridSituation.Columns("Code1").Visible = False
            //Else
            //        SSGridSituation.Columns("Code1").Visible = True
            //End If

            if (optEcrituresNonLetrees.Checked == true)
            {
                General.sSQL = "SELECT F_ECRITUREC.CT_NUM,EC_PIECE,EC_SENS,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date," + " TempCT_NumGroupe.Code1, TempCT_NumGroupe.Codeimmeuble" + " EC_Jour FROM F_ECRITUREC INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE ";
                General.sSQL = General.sSQL + " JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    
                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND ";

                }

                General.sSQL = General.sSQL + "  TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + " ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                General.sSQL = General.sSQL + " ORDER BY F_ECRITUREC.CT_NUM, EC_DATE ASC";

            }
            else
            {
                General.sSQL = "SELECT F_ECRITUREC.CT_NUM,EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date, EC_Jour, " + " TempCT_NumGroupe.Code1, TempCT_NumGroupe.Codeimmeuble" + " FROM F_ECRITUREC INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";

                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND";
                }

                General.sSQL = General.sSQL + " TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + " ((F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") or " + " (F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                General.sSQL = General.sSQL + " ORDER BY F_ECRITUREC.CT_NUM, EC_DATE ASC";

            }

                //rsAdoFinance.Close();
                // rsAdoFinance.Open(General.sSQL, SAGE.adoSage);
                //SSGridSituation.RemoveAll();
            rsAdoFinance = new DataTable();
            SqlDataAdapter SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
            SDArsAdoFinance.Fill(rsAdoFinance);
            

            if (rsAdoFinance.Rows.Count > 0)
            {
                //rsAdoFinance.MoveFirst();

                foreach (DataRow rsAdoFinanceRow in rsAdoFinance.Rows)
                {
                    if (rsAdoFinanceRow["JO_Num"].ToString().ToUpper() == General.cAN.ToString().ToUpper())
                    {
                        sDateFacture = rsAdoFinanceRow["EC_ECHEANCE"] + "";
                    }
                    else
                    {
                            int c = rsAdoFinanceRow["EC_Jour"].ToString().Length;
                            string cc = rsAdoFinanceRow["JM_Date"].ToString();
                            int ccc = rsAdoFinanceRow["JM_Date"].ToString().Length - 2;

                        sDateFacture = rsAdoFinanceRow["EC_Jour"].ToString().Length 
                                + General.Mid( rsAdoFinanceRow["JM_Date"].ToString(), 
                                3,rsAdoFinanceRow["JM_Date"].ToString().Length - 2);

                     }
                        if (!string.IsNullOrEmpty(sDateFacture) && General.IsDate(sDateFacture))
                            sDateFacture = Convert.ToDateTime(sDateFacture).ToString("dd/MM/yyyy");
                        else
                            sDateFacture = "";

                        string dateJournal = rsAdoFinanceRow["JM_Date"].ToString();
                        if (!string.IsNullOrEmpty(dateJournal) && General.IsDate(dateJournal))
                            dateJournal = Convert.ToDateTime(dateJournal).ToString("dd/MM/yyyy");
                        else
                            dateJournal = "";

                        string ec_Echeance = rsAdoFinanceRow["EC_Echeance"].ToString();
                        if (!string.IsNullOrEmpty(ec_Echeance) && General.IsDate(ec_Echeance))
                            ec_Echeance = Convert.ToDateTime(ec_Echeance).ToString("dd/MM/yyyy");
                        else
                            ec_Echeance = "";


                        //Colonne du montant dans les crédits
                        if (rsAdoFinanceRow["EC_Sens"].ToString() == "1")
                    {
                        //SSGridSituation.AddItem(rsAdoFinance.Fields("Code1").Value + Constants.vbTab + rsAdoFinance.Fields("CT_NUM").Value + Constants.vbTab + rsAdoFinance.Fields("JO_Num").Value + Constants.vbTab + rsAdoFinance.Fields("JM_Date").Value + Constants.vbTab + sDateFacture + Constants.vbTab + rsAdoFinance.Fields("EC_Piece").Value + Constants.vbTab + rsAdoFinance.Fields("EC_Echeance").Value + Constants.vbTab + "" + Constants.vbTab + rsAdoFinance.Fields("EC_Montant").Value);
                        var Row = SourceDT.NewRow();
                        Row["Code Gérant"] = rsAdoFinanceRow["Code1"];
                        Row["Compte Immeuble"] = rsAdoFinanceRow["CT_NUM"];
                        Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                        Row["Date Journal"] = dateJournal;                         
                        Row["Date Facture"] = sDateFacture;
                        Row["Référence facture"] = rsAdoFinanceRow["EC_PIECE"];
                        Row["Echéance"] = ec_Echeance;                        
                        Row["Débit"] = null;
                        Row["Crédit"] = rsAdoFinanceRow["EC_MONTANT"];
                            string credit = rsAdoFinanceRow["EC_Montant"].ToString();
                            if (!string.IsNullOrEmpty(credit) && General.IsNumeric(credit))
                                credit = Convert.ToDouble(credit).ToString("#.##");
                            else
                                credit = "";
                       Row["Crédit"] = credit;
                       SourceDT.Rows.Add(Row);
                       SSGridSituation.DataSource = SourceDT;

                    }
                    else
                    {
                        // SSGridSituation.AddItem(rsAdoFinance.Fields("Code1").Value + Constants.vbTab + rsAdoFinance.Fields("CT_NUM").Value + Constants.vbTab + rsAdoFinance.Fields("JO_Num").Value + Constants.vbTab + rsAdoFinance.Fields("JM_Date").Value + Constants.vbTab + sDateFacture + Constants.vbTab + rsAdoFinance.Fields("EC_Piece").Value + Constants.vbTab + rsAdoFinance.Fields("EC_Echeance").Value + Constants.vbTab + rsAdoFinance.Fields("EC_Montant").Value + Constants.vbTab + "");
                        var Row = SourceDT.NewRow();
                        Row["Code Gérant"] = rsAdoFinanceRow["Code1"];
                        Row["Compte Immeuble"] = rsAdoFinanceRow["CT_NUM"];
                        Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                        Row["Date Journal"] = dateJournal;                          
                        Row["Date Facture"] = sDateFacture;
                        Row["Référence facture"] = rsAdoFinanceRow["EC_PIECE"];
                        Row["Echéance"] = ec_Echeance;                          
                        Row["Débit"] = rsAdoFinanceRow["EC_MONTANT"];
                        string debit = rsAdoFinanceRow["EC_Montant"].ToString();
                           if (!string.IsNullOrEmpty(debit) && General.IsNumeric(debit))
                                debit = Convert.ToDouble(debit).ToString("#.##");
                            else
                                debit = "";
                       Row["Débit"] = debit;
                       Row["Crédit"] = null;
                       SourceDT.Rows.Add(Row);
                       SSGridSituation.DataSource = SourceDT;

                    }
                    //  rsAdoFinance.MoveNext();
                }
            }
            rsAdoFinance.Dispose();

            if (optEcrituresNonLetrees.Checked == true)
            {


                General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";

                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND";
                }

                General.sSQL = General.sSQL + "  TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + "  ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + " )" + " or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

            }
            else
            {
                General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM  ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";

                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND";
                }

               
                General.sSQL = General.sSQL + "  TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + "  ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + " )" + " or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

            }
            // rsAdoFinance.Open(General.sSQL, SAGE.adoSage);

            rsAdoFinance = new DataTable();
            SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
            SDArsAdoFinance.Fill(rsAdoFinance);

            if (rsAdoFinance.Rows.Count > 0)
            {
                MontantCredit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
            }

            rsAdoFinance.Dispose();

            if (optEcrituresNonLetrees.Checked == true)
            {

                General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM  ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";

                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND";

                }

                General.sSQL = General.sSQL + "  TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + "  ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ")" + " or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
            }
            else
            {

                General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                General.sSQL = General.sSQL + " INNER JOIN TempCT_NumGroupe ON F_ECRITUREC.CT_Num = TempCT_NumGroupe.CT_Num";
                General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";

                if (optCptGerant.Checked == true)
                {
                    General.sSQL = General.sSQL + " TempCT_NumGroupe.Code1 ='" + ComboComptes.Text + "' AND";

                }

                General.sSQL = General.sSQL + "  TempCT_NumGroupe.UserName ='" + StdSQLchaine.gFr_DoublerQuote(sUserNameT) + "' AND";
                General.sSQL = General.sSQL + "  ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

            }

            //rsAdoFinance.Open(General.sSQL, SAGE.adoSage);
            rsAdoFinance = new DataTable();
            SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
            SDArsAdoFinance.Fill(rsAdoFinance);

            if (rsAdoFinance.Rows.Count > 0)
            {
                MontantDebit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
            }
            rsAdoFinance.Dispose();
           
            txtSoldeCompte.Text = Convert.ToString(MontantDebit - MontantCredit);
            txtSoldeCompte.Visible = true;
            txtTotalFacture.Text = Convert.ToString(MontantDebit);
            txtTotalFacture.Visible = true;
            txtTotalRegle.Text = Convert.ToString(MontantCredit);
            txtTotalRegle.Visible = true;
            SAGE.fc_CloseConnSage();
         

        }
          catch (Exception e) {
                Erreurs.gFr_debug(e,this.Name + " Erreur Liaison Sage : ");
            }
            
            
             }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmd0_Click(object sender, EventArgs e)
        {
            var cmd = sender as Button;
            int Index = Convert.ToInt32(cmd.Tag);

            string sCode = null;


            switch (Index)
            {
                case 0:
                    //var _with1 = RechercheMultiCritere;
                    string where = "";
                    string req = "SELECT     Contact.ID_Contact as \"ID_Contact\", "
                        + " Contact.Nom as \"Nom\", Contact.Prenom as \"Prenom\", " 
                        + " Qualification.Qualification as \"Qualification\" " 
                        + " FROM         Contact LEFT OUTER JOIN"
                        + " Qualification ON Contact.CodeQualif = Qualification.CodeQualif";
                    SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des contacts" };
                    fg.ugResultat.DoubleClickRow += (se, ev) => {

                        sCode = fg.ugResultat.ActiveRow.Cells["ID_Contact"].Value.ToString();
                        General.fc_InsertContact(General.cContactGroupe,Convert.ToInt32(sCode), txtCRCL_Code.Text); //tested
                        fc_ContactGroupe(txtCRCL_Code.Text);
                        
                        fg.Dispose(); fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                        sCode = fg.ugResultat.ActiveRow.Cells["ID_Contact"].Value.ToString();
                        General.fc_InsertContact( General.cContactGroupe, Convert.ToInt32(sCode), txtCRCL_Code.Text); 
                       fc_ContactGroupe(txtCRCL_Code.Text);
                    
                        fg.Dispose(); fg.Close();
                        }
                    };

                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();

                    break;

                case 1:

                    if (string.IsNullOrEmpty(txtCRCL_Code.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Insertion impossible, groupe inexistant.", "Opération annulée",MessageBoxButtons.OK,MessageBoxIcon.Information);
                        return;
                    }
                    Forms.frmAddContact frmaddcontact = new Forms.frmAddContact();

                     frmaddcontact.txtTypeContact.Text = General.cContactGroupe;
                     frmaddcontact.txtReferentiel.Text = txtCRCL_Code.Text;
                     frmaddcontact.txtAdresse.Text = txtCRCL_Adresse1.Text;
                     frmaddcontact.txtCP.Text = txtCRCL_CP.Text;
                     frmaddcontact.txtVille.Text = txtCRCL_Ville.Text;

                     frmaddcontact.ShowDialog();
                     fc_ContactGroupe(txtCRCL_Code.Text);

                    break;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLogo_Click(object sender, EventArgs e)
        {
            string sDesPathFile = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //sDesPathFile = sCheminDossier & "\" & txtCodeImmeuble & "\contrats\" & cSociete & "\"
                //fc_CreeDossier
                //fc_DeplaceFichier cd1.filename, sDesPathFile
                txtCRCL_CheminLogo.Text = openFileDialog1.FileName;

            }

    }
    /// <summary>
    /// Tested
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_sauver();
            fc_SavePos();
        }
   /// <summary>
   /// Tested
   /// </summary>
        private void fc_sauver()
        {
            string sCode = null;
            int lMsgbox = 0;

            sCode = txtCRCL_Code.Text;
            // controle si code client saisie
            if (string.IsNullOrEmpty(txtCRCL_Code.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code groupe.", "Validation annulée", MessageBoxButtons.OK);
                return;
            }

            if (string.IsNullOrEmpty(txtSelCRCL_Code.Text))
            {
                if (FC_insert() == false)
                {
                    return;
                }
                else
                {
                    blnAjout = false;
                }
            }
            else
            {
                fc_maj(ref sCode);
            }
            
            ssGridClient.UpdateData();
            
            //=== debloque les controles.
            fc_DeBloqueForm();
            blModif = false;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm( string sCommande = "")
        {
            //.SSTab1.Tab = 0
            try { 
            SSTab1.Enabled = true;
            cmdAjouter.Enabled = true;
            CmdRechercher.Enabled = true;
            cmdSupprimer.Enabled = true;
            CmdSauver.Enabled = true;
            cmdRechercheGroupe.Enabled = true; 
           cmdRechercheRaisonSociale.Enabled = true; 

            // desactive le mode Ajout
            blnAjout = false;

            return;
            }
            catch(Exception e) { Erreurs.gFr_debug(e,this.Name + "fc_DeBloqueForm"); }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_SavePos()
        {
            /* Interaction.SaveSetting(General.cFrNomApp, Variable.cUserDocGroupe, "txtCRCL_Code", txtCRCL_Code.Text);*/
            General.saveInReg(Variable.cUserDocGroupe, "txtCRCL_Code", txtCRCL_Code.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>

         private bool FC_insert()
          {
            bool functionReturnValue = false;
            // creation d' un nouveau client
            DataTable rsExtranet = new DataTable();
            functionReturnValue = true;
           
            if (txtCRCL_Code.Text.Contains("'"))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi un code groupe avec une apostrophe.Veuillez le modifier.", "Création annulée", MessageBoxButtons.OKCancel);
                txtCRCL_Code.Focus();
                return functionReturnValue;
            }

            if (txtCRCL_Code.Text.Contains(" "))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi un code groupe avec une espace.Veuillez le modifier.", "Création annulée", MessageBoxButtons.OKCancel);
                txtCRCL_Code.Focus();
                return functionReturnValue;
            }

            General.SQL = "INSERT INTO CRCL_GroupeClient ( CRCL_Code, CRCL_RaisonSocial)";
            General.SQL = General.SQL + " VALUES (";
            General.SQL = General.SQL + "'" +StdSQLchaine.gFr_DoublerQuote(txtCRCL_Code.Text).Trim().ToString() + "',";
            General.SQL = General.SQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_RaisonSocial.Text).Trim().ToString() + "'";
            General.SQL = General.SQL + ")";

            General.Execute(General.SQL);

            txtSelCRCL_Code.Text = txtCRCL_Code.Text;  

            // debloque les onglets.
            fc_DeBloqueForm();
            return functionReturnValue;



          }  

        private void fc_Ajouter()
        {
           //var _with14 = this;
            if (blModif == true)
            {
                DialogResult dialogresult = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", " ", MessageBoxButtons.YesNoCancel);
                if (dialogresult == DialogResult.Yes)
                {
                    fc_sauver();
                }
                else if(dialogresult == DialogResult.No)
                {
                    return;
                }   
               
            }

            fc_Clear();
           fc_BloqueForm("Ajouter");
            fc_InitialiseGrille();
        }
           private void fc_BloqueForm( string sCommande = "")
             {

            try {
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                SSTab1.Enabled = false;
                cmdSupprimer.Enabled = false;
                //mode ajout
                if (sCommande.ToString().ToUpper()== "Ajouter".ToUpper())
                {
                    cmdAjouter.Enabled = true;
                    CmdRechercher.Enabled = false;
                    CmdSauver.Enabled = true;
                    cmdRechercheGroupe.Enabled = false; 
                    cmdRechercheRaisonSociale.Enabled = false; 
                    // Active le mode Ajout
                    blnAjout = true;
                }
                else
                {
                    // desactive le mode Ajout
                    blnAjout = false;
                    cmdRechercheGroupe.Enabled = true;
                    cmdRechercheRaisonSociale.Enabled = true;
                    CmdRechercher.Enabled = true;
                    CmdSauver.Enabled = false;
                }

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + "fc_BloqueForm");
            }
                
            }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_InitialiseGrille()
        {
            //    fc_CloseRecordset rsCorrespondants
            //    fc_CloseRecordset rsIntervenant
            //    fc_CloseRecordset rsintervention
            //    fc_CloseRecordset rsDevis
            //
            //    With Me
            //        .ssGridGestionnaire.ReBind
            //        .SSINTERVENANT.ReBind
            //        .ssIntervention.ReBind
            //        .ssDevis.ReBind
            //        .GridCLRE.ReBind
            //    End With
         fc_LoadGridClient("@@@@@@@");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_LoadGridClient(string sCode)
        {
            string sSql = null;
            try
            {

                sSql = "SELECT     Code1 AS \"Code Client\", Table1.Nom AS \"Raison sociale\","
                    + " Adresse1, CodePostal, Ville" 
                    + " , Personnel.Matricule," 
                    + " personnel.Nom" 
                    + " FROM         Table1 LEFT JOIN" 
                    + " Personnel ON Table1.commercial = Personnel.Matricule" 
                    + " " + " WHERE CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                modAdorsClient = new ModAdo();
                rsClient = modAdorsClient.fc_OpenRecordSet(sSql);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridClient.DataSource = rsClient;
         
                return;
            }
            catch(Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + ";fc_LoadGridClient");
            }
          
        }/// <summary>
        /// Tested
        /// </summary>
        private void fc_annuler()
        {
       
            if (blModif == true)
            {
                switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez - vous enregistrer vos modifications ? ", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_Clear();
            fc_BloqueForm();
         

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Rechercher()
        {
            try {
            string sCode = null;
            string where_order = "";
            
            string requete = "SELECT   CRCL_Code AS \"Code\" ," + " CRCL_RaisonSocial AS \"Raison Sociale\"," + " CRCL_Adresse1 AS \"Adresse\"," + " CRCL_CP AS \"Code Postal\"," + " CRCL_Ville As \"Ville\"" + " FROM  CRCL_GroupeClient";    
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order,"") { Text = "Recherche  d'un groupe" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
             {
                //===charge les enregistrements de l'immeuble
               
                sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                fc_ChargeEnregistrement(sCode);
                 //J'ai ajouter cette fonction pour charger le combo du client en meme temps  avec le chargement  d'un  client recherché
                 fc_ChargeClient();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                //===charge les enregistrements de l'immeuble
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                    fc_ChargeEnregistrement(sCode);
                    fc_ChargeClient();
                    fg.Dispose();
                    fg.Close();
                }
            };

           fg.StartPosition = FormStartPosition.CenterParent;
           fg.ShowDialog();

            }
            catch(Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + ";fc_Rechercher");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_maj(ref string sCode)
        {
            try
            {

           
            string sSql = null;
            DataTable rs = new DataTable();

            sSql = "UPDATE    CRCL_GroupeClient" + " SET    " + " " 
                    + " CRCL_RaisonSocial = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_RaisonSocial.Text) + "',"
                    + " CRCL_Adresse1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Adresse1.Text) + "',"
                    + " CRCL_Adresse2 = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Adresse2.Text) + "',"
                    + " CRCL_CP = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_CP.Text) + "'," 
                    + " CRCL_Ville = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Ville.Text) + "',"
                    + " CRCL_ContratCadre =" + General.nz(chkCRCL_ContratCadre.Checked ? 1 : 0, "0") + "," 
                    + " CRCL_Tel = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Tel.Text) + "'," 
                    + " CRCL_Fax = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Fax.Text) + "'," 
                    + " CRCL_Email = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Email.Text) + "',"
                    + " CRCL_Login = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Login.Text) + "'," 
                    + " CRCL_CheminLogo = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_CheminLogo.Text) + "'," 
                    + " CRCL_MDP = '" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_MDP.Text) + "'," 
                    + " CRCL_Info = '" + StdSQLchaine.gFr_DoublerQuote(TXTCRCL_Info.Text) + "'," 
                    + " CRCL_FNAIM =" + General.nz(CHKCRCL_FNAIM.Checked ? 1:0, "0") + "," 
                    + " CRCL_UNIS =" + General.nz(CHKCRCL_UNIS.Checked ? 1:0, "0") + "" 
                    + " WHERE  CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            General.Execute(sSql);
            sSql = "SELECT     CRCL_Code,CRCL_Logo" + " From CRCL_GroupeClient" + 
                    " WHERE   CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
 
            SqlDataAdapter SDArs = new SqlDataAdapter(sSql,General.adocnn);
           SqlCommandBuilder SCBrs = new SqlCommandBuilder(SDArs);
            SDArs.Fill(rs);
            if (!string.IsNullOrEmpty(txtCRCL_CheminLogo.Text))
            {
                ModuleAPI.SaveBitmap(SDArs, "CRCL_Logo", txtCRCL_CheminLogo.Text);
            }
            else
                {//  rs.Fields("CRCL_Logo").Value = System.DBNull.Value;
                 // rs.Update();
                    rs.Rows[0]["CRCL_Logo"] = System.DBNull.Value;
                    SDArs.Update(rs);
            }

            rs.Dispose();
            rs = null;
            return;
            }
            catch(Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + ";fc_Update");
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Clear()
        {
            
            txtCRCL_Code.Text = "";
            txtSelCRCL_Code.Text = ""; 
            txtCRCL_RaisonSocial.Text = "";
            txtCRCL_Adresse1.Text = "";
            txtCRCL_Adresse2.Text = "";
            txtCRCL_CP.Text = "";
            txtCRCL_Ville.Text = "";
            chkCRCL_ContratCadre.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtCRCL_Tel.Text = "";
            txtCRCL_Fax.Text = "";
            txtCRCL_Email.Text = "";
            txtCRCL_Login.Text = "";
            txtCRCL_MDP.Text = "";
            TXTCRCL_Info.Text = "";
            CHKCRCL_FNAIM.CheckState = System.Windows.Forms.CheckState.Unchecked;
            CHKCRCL_UNIS.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtCRCL_CheminLogo.Text = "";
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_Ajouter();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
            fc_SavePos();
        }

     
        private void optGroupe_CheckedChanged(object sender, EventArgs e)
        {
            if (optGroupe.Checked)
            {
                ComboComptes.Text = "";
            }
        }

        private void optToutesEcritures_CheckedChanged(object sender, EventArgs e)
        {
             if (optToutesEcritures.Checked)
             {
                 fc_ChargeFinance();
             }
        }

        private void optEcrituresNonLetrees_CheckedChanged(object sender, EventArgs e)
        {
           if (optEcrituresNonLetrees.Checked)
             {
                 fc_ChargeFinance();
             }
        }
  /// <summary>
  /// Tested
  /// </summary>
  /// <param name="sCode"></param>
    private void fc_ChargeEnregistrement(string sCode)
    {
        string sSql = "";

        try { 

        DataTable rs = new DataTable();
        ModAdo modAdors = new ModAdo();
        sSql = "SELECT CRCL_Noauto, CRCL_Code, CRCL_RaisonSocial, CRCL_Adresse1, " 
                    + " CRCL_Adresse2, CRCL_CP, CRCL_Ville, CRCL_ContratCadre, CRCL_Tel," 
                    + " CRCL_Fax, " 
                    + " CRCL_Email , CRCL_Login, CRCL_MDP, CRCL_Info, CRCL_FNAIM, CRCL_UNIS, CRCL_CheminLogo "
                    + " From CRCL_GroupeClient" 
                    + " WHERE CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
        rs = modAdors.fc_OpenRecordSet(sSql);

        if (rs.Rows.Count == 0)
        {
            fc_Clear();
            rs.Dispose();
            rs = null;
            return;
        }

        txtCRCL_Code.Text = rs.Rows[0]["CRCL_Code"].ToString();
        txtSelCRCL_Code.Text = rs.Rows[0]["CRCL_Code"].ToString();
        txtCRCL_RaisonSocial.Text = rs.Rows[0]["CRCL_RaisonSocial"].ToString();
        txtCRCL_Adresse1.Text = rs.Rows[0]["CRCL_Adresse1"].ToString();
        txtCRCL_Adresse2.Text = rs.Rows[0]["CRCL_Adresse2"].ToString();
        txtCRCL_CP.Text = rs.Rows[0]["CRCL_CP"].ToString();
        txtCRCL_Ville.Text = rs.Rows[0]["CRCL_Ville"].ToString();
     
        chkCRCL_ContratCadre.Checked = General.nz(rs.Rows[0]["CRCL_ContratCadre"],"0").ToString()=="1";
        txtCRCL_Tel.Text = rs.Rows[0]["CRCL_Tel"].ToString();
        txtCRCL_Fax.Text = rs.Rows[0]["CRCL_Fax"].ToString();
        txtCRCL_Email.Text = rs.Rows[0]["CRCL_Email"].ToString();
        txtCRCL_Login.Text = rs.Rows[0]["CRCL_Login"].ToString();
        txtCRCL_MDP.Text = rs.Rows[0]["CRCL_MDP"].ToString();
        TXTCRCL_Info.Text = rs.Rows[0]["CRCL_Info"].ToString();
        txtCRCL_CheminLogo.Text = rs.Rows[0]["CRCL_CheminLogo"].ToString();
  
        CHKCRCL_FNAIM.Checked = General.nz(rs.Rows[0]["CRCL_FNAIM"],"0").ToString()=="1";
        CHKCRCL_UNIS.Checked = General.nz(rs.Rows[0]["CRCL_UNIS"],"0").ToString()=="1";

        fc_LoadGridClient(sCode);
        rs.Dispose();
        rs = null;
        fc_ContactGroupe(txtCRCL_Code.Text); 
        fc_DeBloqueForm("");
        }
        catch(Exception e)
        {
            Erreurs.gFr_debug(e,this.Name + ";fc_ChargeEnregistrement");
        }
        return;
       
    }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCRCL_Code"></param>
        private void fc_ContactGroupe(string sCRCL_Code)
        {
            try
            {
                string sSql = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSql = "SELECT     Contact.ID_Contact, Contact.Civilite, Contact.Prenom, Contact.Nom, Contact.CodeQualif, " 
                    + " Contact.Email, Contact.Tel, Contact.Portable, Contact.Fax, " 
                    + " contact.Note , contact.PresidentCS, contact.Adresse, contact.CP, contact.Ville, " 
                    + " ContactGroupe.CRCL_Code" 
                    + " FROM         Contact INNER JOIN" 
                    + " ContactGroupe ON Contact.ID_Contact = ContactGroupe.ID_Contact LEFT OUTER JOIN" 
                    + " Qualification ON Contact.CodeQualif = Qualification.CodeQualif" 
                    + " WHERE ContactGroupe.CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(sCRCL_Code) + "'";
                modAdorsContactGroupe = new ModAdo();
                rsContactGroupe = modAdorsContactGroupe.fc_OpenRecordSet(sSql);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridContactGroupe.DataSource=rsContactGroupe;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;                
                return;
            }
            catch(Exception e)
            {
                Erreurs.gFr_debug(e,this.Name + ";fc_ContactGroupe");
            }
            
        }

      
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheGroupe_Click(object sender, EventArgs e)
        {
           txtCRCL_Code_KeyPress(txtCRCL_Code, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCRCL_Code_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            DataTable rsRechCode1 ;
            ModAdo modAdorsRechCode1;
            string sSqlCode1 = null;

            if (KeyAscii == 13 && blnAjout == false)
            {

                sSqlCode1 = "SELECT CRCL_Code FROM  CRCL_GroupeClient " + " WHERE CRCL_Code like '%" + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Code.Text) + "%'";

                rsRechCode1 = new DataTable();
                modAdorsRechCode1 = new ModAdo();
                rsRechCode1 = modAdorsRechCode1.fc_OpenRecordSet(sSqlCode1);


                if (rsRechCode1.Rows.Count > 1 || rsRechCode1.Rows.Count== 0)
                {
                  General.blControle = true;

                    // fc_Rechercher();  remplacer tt le code de cette fonction par le code suivant
                    string sCode = null;
                    string where_order = "";

                    string requete = "SELECT   CRCL_Code AS \"Code\" ," + " CRCL_RaisonSocial AS \"Raison Sociale\"," + " CRCL_Adresse1 AS \"Adresse\"," + " CRCL_CP AS \"Code Postal\"," + " CRCL_Ville As \"Ville\"" + " FROM  CRCL_GroupeClient";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche  d'un groupe" };
                    fg.SetValues(new Dictionary<string, string> { { "CRCL_Code", txtCRCL_Code.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        //===charge les enregistrements de l'immeuble

                        sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        //J'ai ajouter cette fonction pour charger le combo du client en meme temps  avec le chargement  d'un  client recherché
                        fc_ChargeClient(); 
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        //===charge les enregistrements de l'immeuble
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                            fc_ChargeEnregistrement(sCode);
                            fc_ChargeClient();
                            fg.Dispose();
                            fg.Close();
                        }
                    };

                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();

                }
                else if (rsRechCode1.Rows.Count == 1)
                {
                    fc_ChargeEnregistrement(txtCRCL_Code.Text);
                }

                KeyAscii = 0;
                rsRechCode1.Dispose();
                rsRechCode1 = null;
            }
          e.KeyChar =(Char)KeyAscii; 
            if (KeyAscii == 0)
            {
                e.Handled = true;
            }
        }

       /// <summary>
       /// Tested
       /// </summary>
        private void fc_ChargeClient()
        {
            string sSql = null;
            DataTable rs = new DataTable();
            ModAdo modAdors = new ModAdo();
            int i = 0;

            sSql = "SELECT Code1, Nom " + " FROM  Table1 "
                + "   " + " WHERE CRCL_Code = '" 
                + StdSQLchaine.gFr_DoublerQuote(txtCRCL_Code.Text) + "'" + " order by Code1";
            rs = modAdors.fc_OpenRecordSet(sSql);

            var ComboComptesSource = new DataTable();
            ComboComptesSource.Columns.Add("Code gérant");
            ComboComptesSource.Columns.Add("Nom");
           if(rs.Rows.Count == 0)
            {
                ComboComptes.DataSource = ComboComptesSource;
            }

                /*  if (ComboComptes.Rows.Count > 0)
                     {
                         for (i = 0; i <= ComboComptes.Rows.Count - 1; i++)
                         {
                           ComboComptes.RemoveItem(0);
                         }
                     }*/

                if (rs.Rows.Count>0)
               {

                   //rs.MoveFirst();
                   foreach (DataRow rsRow in rs.Rows)
                   {
                    //ComboComptes.AddItem(rs.Fields("Code1").Value + Constants.vbTab + rs.Fields("Nom").Value);
                    //rs.MoveNext();
                    ComboComptesSource.Rows.Add(rsRow["Code1"], rsRow["Nom"]);
                    ComboComptes.DataSource = ComboComptesSource;
                   }
               }

            rs.Dispose();
            rs = null;
            
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMoisFin"></param>
        /// <returns></returns>
        private object fc_ChargeExercices(string sMoisFin)
        {
            object returnFunction = null;
            string sqlExercices = null;
            DataTable rsExercices ;
            SqlDataAdapter SDArsExercices;
            string strAddItem = null;
            string JourMaxiMois = null;
            string DateMini = null;
            string DateMaxi = null;
            int i = 0;
            bool blnMatch = false;

            blnMatch = false;
            SAGE.fc_OpenConnSage();
            rsExercices = new DataTable();
           
            sqlExercices = "SELECT DISTINCT YEAR(JM_Date) AS Annee";
            sqlExercices = sqlExercices + " FROM F_JMOUV";
            sqlExercices = sqlExercices + " ORDER BY YEAR(JM_Date) DESC";

            SDArsExercices = new SqlDataAdapter(sqlExercices, SAGE.adoSage);
            SDArsExercices.Fill(rsExercices);

            JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt16(sMoisFin)));

            if (sMoisFin == "12" || !General.IsNumeric(sMoisFin))
            {
                DateMini = "01/" + "01" + "/";
                DateMaxi = JourMaxiMois + "/12/";
            }
            else
            {
                if (sMoisFin == "12")
                {
                    DateMini = "01/" + "01" + "/";
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString("00") + "/";
                    //MoisFinExercice
                }
                else
                {
                    DateMini = "01/" + (Convert.ToInt16(sMoisFin) + 1).ToString("00") + "/";
                    //MoisFinExercice - 1
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString ("00") + "/";
                    //MoisFinExercice
                }
            }
            var cmbExercicesSource = new DataTable();
            cmbExercicesSource.Columns.Add("Exercice");
            cmbExercicesSource.Columns.Add("Du");
            cmbExercicesSource.Columns.Add("Au");
           
            var NewRow = cmbExercicesSource.NewRow();

            if (rsExercices.Rows.Count>0)
            {
                //rsExercices.MoveFirst();

                foreach(DataRow rsExercicesRows in rsExercices.Rows)
                {
                    if (Convert.ToInt16(rsExercicesRows["Annee"])>=DateTime.Now.Year)
                    {
                        if (sMoisFin != "12")
                        {
                          
                            if (blnMatch == false && Convert.ToInt16(General.nz(sMoisFin,"1")) < Convert.ToInt16(DateTime.Now.Month))
                            {

                                //strAddItem = rsExercices.Fields("Annee").Value + "/" + (rsExercices.Fields("Annee").Value + 1) + Constants.vbTab + DateMini + (rsExercices.Fields("Annee")).Value + Constants.vbTab + DateMaxi + (rsExercices.Fields("Annee").Value + 1);
                                //  cmbExercices.AddItem(strAddItem);
                                cmbExercicesSource.Rows.Add(rsExercicesRows["Annee"]+ "/"+(Convert.ToInt16(rsExercicesRows["Annee"])+1), DateMini+ rsExercicesRows["Annee"], DateMaxi+ (Convert.ToInt16(rsExercicesRows["Annee"])+1));
                                cmbExercices.DataSource = cmbExercicesSource;
                                blnMatch = true;
                            }
                         
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt16(rsExercicesRows["Annee"]) - 1) + "/" + rsExercicesRows["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt16(rsExercicesRows["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRows["Annee"];
                        }
                        else if (sMoisFin == "12")
                        {
                            //strAddItem = rsExercices.Fields("Annee").Value + Constants.vbTab + DateMini + rsExercices.Fields("Annee").Value + Constants.vbTab + DateMaxi + rsExercices.Fields("Annee").Value;
                           NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = rsExercicesRows["Annee"];
                            NewRow["Du"] = DateMini + rsExercicesRows["Annee"];
                            NewRow["Au"] = DateMaxi + rsExercicesRows["Annee"];
                        }
                    }
                    else
                    {
                        if (sMoisFin == "12")
                        {
                            // strAddItem = rsExercices.Fields("Annee").Value + Constants.vbTab + DateMini + rsExercices.Fields("Annee").Value + Constants.vbTab + DateMaxi + rsExercices.Fields("Annee").Value;
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = rsExercicesRows["Annee"];
                            NewRow["Du"] = DateMini + rsExercicesRows["Annee"];
                            NewRow["Au"] = DateMaxi + rsExercicesRows["Annee"];
                        }
                        else
                        {
                            //strAddItem = rsExercices.Fields("Annee").Value - 1 + "/" + (rsExercices.Fields("Annee")).Value + Constants.vbTab + DateMini + (rsExercices.Fields("Annee").Value - 1) + Constants.vbTab + DateMaxi + (rsExercices.Fields("Annee")).Value;
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt16(rsExercicesRows["Annee"]) - 1) + "/" + rsExercicesRows["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt16(rsExercicesRows["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRows["Annee"];
                        }
                    }
                    // Debug.Print strAddItem
                    //  cmbExercices.AddItem(strAddItem);
                    //rsExercices.MoveNext();
                    cmbExercicesSource.Rows.Add(NewRow);
                    cmbExercices.DataSource = cmbExercicesSource;

                }
            }

            for (i = 0; i <= cmbExercices.Rows.Count - 1; i++)
            {
               
                cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"].Value;
                string du=cmbExercices.Rows[i].Cells["Du"].Value.ToString();
                string au=cmbExercices.Rows[i].Cells["Au"].Value.ToString();
                if (General.IsDate(du) && General.IsDate(au))
                {
                    
                    if (Convert.ToDateTime(du) <= DateTime.Today && Convert.ToDateTime(au) >= DateTime.Today)
                    {
                      
                        cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"].Value;
                        cmbExercices.ActiveRow.Cells[1].Value = cmbExercices.Rows[i].Cells[1].Value;
                        cmbExercices.ActiveRow.Cells[2].Value = cmbExercices.Rows[i].Cells[2].Value;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                if (i == cmbExercices.Rows.Count - 1)
                {
                      cmbExercices.Value = cmbExercices.Rows[0].Cells["Exercice"].Value;
                }
            }

            SAGE.fc_CloseConnSage();
            return returnFunction;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridContactGroupe_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {

            try { 

            if (string.IsNullOrEmpty(ssGridContactGroupe.ActiveRow.Cells["ID_Contact"].Value.ToString()))//Tested
            {

                return;
            }

            if (string.IsNullOrEmpty(txtCRCL_Code.Text))
            {
               Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Insertion impossible, groupe inexistant.", "Opération annulée",MessageBoxButtons.OK,MessageBoxIcon.Information);
                return;
            }

             Forms.frmAddContact frmcontact = new Forms.frmAddContact();

                frmcontact.txtTypeContact.Text = General.cContactGroupe;
                frmcontact.txtReferentiel.Text = txtCRCL_Code.Text;
                frmcontact.txtID_Contact.Text = General.nz(ssGridContactGroupe.ActiveRow.Cells["ID_Contact"].Value,0).ToString(); 
                frmcontact.ShowDialog();   

                fc_ContactGroupe(txtCRCL_Code.Text);

            return;
            }
            catch(Exception ex)
            {
              
                Erreurs.gFr_debug(ex,this.Name + ";ssGridContactGroupe_DblClick");
            }
           
        }

       
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCRCL_Code_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            string sCode = null;
            if (General.blControle == false)
            {
                if (txtCRCL_Code.Text != txtSelCRCL_Code.Text)
                {
                    // controle si codev client Existant
                    sCode = txtCRCL_Code.Text;
                    General.sSQL = "SELECT CRCL_Code " + " From CRCL_GroupeClient" + " WHERE CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                    ModAdo modadorstmp = new ModAdo();
                    General.rstmp = modadorstmp.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count >0)
                    {
                        // si enregistrement éxiste déja
                        if (blnAjout == true)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Groupe déjà existant","",MessageBoxButtons.OKCancel);
                        }
                        sCode = General.rstmp.Rows[0]["CRCL_Code"].ToString();
                        fc_ChargeEnregistrement(sCode);
                        txtCRCL_Code.Text = sCode;

                    }
                    else if (blnAjout == false && !string.IsNullOrEmpty(txtCRCL_Code.Text))
                    {
                        
                        try
                        {
                            
                            string where = "";
                            string req = "SELECT   CRCL_Code AS \"Code\"," + " CRCL_RaisonSocial AS \"Raison Sociale\"," + " CRCL_Adresse1 AS \"Adresse\"," + " CRCL_CP AS \"Code Postal\"," + " CRCL_Ville As \"Ville\" " + " FROM  CRCL_GroupeClient";

                            SearchTemplate fg = new SearchTemplate(this, null, req, where,"") { Text = "Recherche  d'un groupe" };
                            fg.SetValues(new Dictionary<string, string> { { "Code Groupe", txtCRCL_Code.Text } });
                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                //===charge les enregistrements de l'immeuble

                                sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                                fc_ChargeEnregistrement(sCode);
                                fg.Dispose();
                                fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                //===charge les enregistrements de l'immeuble

                                sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                                fc_ChargeEnregistrement(sCode);
                                fg.Dispose();
                                fg.Close();
                            };
                            fg.StartPosition = FormStartPosition.CenterParent;
                            fg.ShowDialog();
                        }
                        catch (Exception ex)
                        {
                            Erreurs.gFr_debug(ex, this.Name + ";fc_Rechercher");
                        }
                    }
                }
            }
            e.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocGroupe_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            
            View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocGroupe");
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                General.open_conn();
            /* 
             *  If LogoSociete <> "" Or Not IsNull(LogoSociete) Then
                 imgLogoSociete.Picture = LoadPicture(LogoSociete)

                 End If
    
               If NomSousSociete <> "" And Not IsNull(NomSousSociete) Then
                  lblNomSociete.Caption = NomSousSociete
                 DoEvents
               End If */

            txtCRCL_Code.Text = General.getFrmReg(Variable.cUserDocGroupe, "txtCRCL_Code", "");

                //=== positionne sur le dernier enregistrement.
                if (!string.IsNullOrEmpty(txtCRCL_Code.Text))//tested
                {
                    //===charge les enregistrements
                    fc_ChargeEnregistrement(txtCRCL_Code.Text);
                }
                else//Tested
                {
                    fc_Clear();
                }
                // fc_ChargeEnregistrement "BEDOS"
                // alimente la liste déroulante de la grille ssintervenant avec la table personnel

                //=== alimente le combo avec les exercices comptables.
                fc_ChargeExercices(General.MoisFinExercice);

                //=== aliment le combo avec les codes clients.
                fc_ChargeClient();

            }

      /// <summary>
      /// Tested
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>

        private void optEcrituresNonLetrees_Click(object sender, EventArgs e)
        {
            fc_ChargeFinance();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optToutesEcritures_Click(object sender, EventArgs e)
        {
            fc_ChargeFinance();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optGroupe_Click(object sender, EventArgs e)
        {
            ComboComptes.Text = "";
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridContactGroupe_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            DialogResult dr = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).","", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            var r = ssGridContactGroupe.ActiveRow.Cells["ID_Contact"].Value;
            string req = $"Delete From Contact where ID_Contact ='{r}'";
            var x = General.Execute(req);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocGroupe_Load(object sender, EventArgs e)
        {
            SSTab2.Tabs[1].Visible = false;
            SSTab2.TabsPerRow = 1;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridClient_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridClient.DisplayLayout.Bands[0].Columns["Matricule"].Header.Caption = "Matricule commercial";
            ssGridClient.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Nom commercial";
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridClient_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, ssGridClient.ActiveRow.Cells["code client"].Text);
            // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocClient); 
            View.Theme.Theme.Navigate(typeof(UserDocClient));
        }

        private void ssGridContactGroupe_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["ID_Contact"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Civilite"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Civilite"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Prenom"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Prenom"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Nom"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Nom"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["CodeQualif"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CodeQualif"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Email"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Email"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Tel"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Tel"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Portable"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Portable"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Fax"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Fax"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Note"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Note"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["PresidentCS"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["PresidentCS"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Adresse"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Adresse"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["CP"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CP"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Ville"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Ville"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["CRCL_Code"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CRCL_Code"].CellActivation = Activation.NoEdit;
        }

      
    }
    }
