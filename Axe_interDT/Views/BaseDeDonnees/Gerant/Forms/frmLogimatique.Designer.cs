﻿namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    partial class frmLogimatique
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.cmdRecherche = new System.Windows.Forms.Button();
            this.chkExport = new System.Windows.Forms.CheckBox();
            this.GridSource = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.GridExport = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdFermer = new System.Windows.Forms.Button();
            this.lblDescriptionExport = new System.Windows.Forms.Label();
            this.lblNbExport = new System.Windows.Forms.Label();
            this.lblNbExport1 = new System.Windows.Forms.Label();
            this.lblDescriptionExport1 = new System.Windows.Forms.Label();
            this.cmdExporter = new System.Windows.Forms.Button();
            this.txtTypeExport = new iTalk.iTalk_TextBox_Small2();
            this.txtCode = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExport)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(4, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Filtre Code";
            // 
            // cmdRecherche
            // 
            this.cmdRecherche.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRecherche.FlatAppearance.BorderSize = 0;
            this.cmdRecherche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRecherche.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRecherche.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRecherche.Location = new System.Drawing.Point(301, 8);
            this.cmdRecherche.Name = "cmdRecherche";
            this.cmdRecherche.Size = new System.Drawing.Size(25, 20);
            this.cmdRecherche.TabIndex = 504;
            this.cmdRecherche.UseVisualStyleBackColor = false;
            this.cmdRecherche.Click += new System.EventHandler(this.cmdRecherche_Click);
            // 
            // chkExport
            // 
            this.chkExport.AutoSize = true;
            this.chkExport.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkExport.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkExport.Location = new System.Drawing.Point(358, 7);
            this.chkExport.Name = "chkExport";
            this.chkExport.Size = new System.Drawing.Size(254, 23);
            this.chkExport.TabIndex = 570;
            this.chkExport.Text = "Joindre les immeubles du gérant";
            this.chkExport.UseVisualStyleBackColor = true;
            // 
            // GridSource
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridSource.DisplayLayout.Appearance = appearance1;
            this.GridSource.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridSource.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSource.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSource.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridSource.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSource.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridSource.DisplayLayout.MaxColScrollRegions = 1;
            this.GridSource.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridSource.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridSource.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridSource.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridSource.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridSource.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridSource.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridSource.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridSource.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridSource.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridSource.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridSource.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSource.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridSource.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridSource.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridSource.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridSource.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridSource.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridSource.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridSource.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridSource.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridSource.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridSource.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridSource.Location = new System.Drawing.Point(8, 40);
            this.GridSource.Name = "GridSource";
            this.GridSource.Size = new System.Drawing.Size(299, 381);
            this.GridSource.TabIndex = 571;
            this.GridSource.Text = "Source";
            this.GridSource.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridSource_InitializeLayout);
            this.GridSource.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridSource_DoubleClickRow);
            // 
            // GridExport
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridExport.DisplayLayout.Appearance = appearance13;
            this.GridExport.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridExport.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridExport.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridExport.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridExport.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridExport.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridExport.DisplayLayout.MaxColScrollRegions = 1;
            this.GridExport.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridExport.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridExport.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridExport.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridExport.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridExport.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridExport.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridExport.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridExport.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridExport.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridExport.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridExport.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridExport.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridExport.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridExport.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridExport.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridExport.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridExport.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridExport.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridExport.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridExport.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridExport.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridExport.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridExport.Location = new System.Drawing.Point(313, 34);
            this.GridExport.Name = "GridExport";
            this.GridExport.Size = new System.Drawing.Size(299, 387);
            this.GridExport.TabIndex = 572;
            this.GridExport.Text = "Code à exporter";
            this.GridExport.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridExport_InitializeLayout);
            this.GridExport.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridExport_BeforeRowsDeleted);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(15, 431);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(85, 35);
            this.cmdAnnuler.TabIndex = 573;
            this.cmdAnnuler.Text = "Annuler";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            // 
            // cmdFermer
            // 
            this.cmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFermer.FlatAppearance.BorderSize = 0;
            this.cmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFermer.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdFermer.ForeColor = System.Drawing.Color.White;
            this.cmdFermer.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFermer.Location = new System.Drawing.Point(104, 431);
            this.cmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFermer.Name = "cmdFermer";
            this.cmdFermer.Size = new System.Drawing.Size(85, 35);
            this.cmdFermer.TabIndex = 574;
            this.cmdFermer.Text = "   Annuler";
            this.cmdFermer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdFermer.UseVisualStyleBackColor = false;
            this.cmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // lblDescriptionExport
            // 
            this.lblDescriptionExport.AutoSize = true;
            this.lblDescriptionExport.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblDescriptionExport.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblDescriptionExport.Location = new System.Drawing.Point(203, 427);
            this.lblDescriptionExport.Name = "lblDescriptionExport";
            this.lblDescriptionExport.Size = new System.Drawing.Size(241, 19);
            this.lblDescriptionExport.TabIndex = 575;
            this.lblDescriptionExport.Text = "Nombre d\'immeubles à exporter :";
            // 
            // lblNbExport
            // 
            this.lblNbExport.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNbExport.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNbExport.Location = new System.Drawing.Point(450, 427);
            this.lblNbExport.Name = "lblNbExport";
            this.lblNbExport.Size = new System.Drawing.Size(95, 17);
            this.lblNbExport.TabIndex = 576;
            this.lblNbExport.Text = "0";
            // 
            // lblNbExport1
            // 
            this.lblNbExport1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNbExport1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblNbExport1.Location = new System.Drawing.Point(450, 451);
            this.lblNbExport1.Name = "lblNbExport1";
            this.lblNbExport1.Size = new System.Drawing.Size(95, 17);
            this.lblNbExport1.TabIndex = 578;
            this.lblNbExport1.Text = "0";
            this.lblNbExport1.Visible = false;
            // 
            // lblDescriptionExport1
            // 
            this.lblDescriptionExport1.AutoSize = true;
            this.lblDescriptionExport1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblDescriptionExport1.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lblDescriptionExport1.Location = new System.Drawing.Point(203, 451);
            this.lblDescriptionExport1.Name = "lblDescriptionExport1";
            this.lblDescriptionExport1.Size = new System.Drawing.Size(227, 19);
            this.lblDescriptionExport1.TabIndex = 577;
            this.lblDescriptionExport1.Text = "Nombre de gérants à exporter :";
            this.lblDescriptionExport1.Visible = false;
            // 
            // cmdExporter
            // 
            this.cmdExporter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExporter.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdExporter.FlatAppearance.BorderSize = 0;
            this.cmdExporter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExporter.Font = new System.Drawing.Font("Ubuntu", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExporter.ForeColor = System.Drawing.Color.White;
            this.cmdExporter.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.cmdExporter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExporter.Location = new System.Drawing.Point(527, 431);
            this.cmdExporter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExporter.Name = "cmdExporter";
            this.cmdExporter.Size = new System.Drawing.Size(85, 35);
            this.cmdExporter.TabIndex = 579;
            this.cmdExporter.Text = "     Exporter";
            this.cmdExporter.UseVisualStyleBackColor = false;
            this.cmdExporter.Click += new System.EventHandler(this.cmdExporter_Click);
            // 
            // txtTypeExport
            // 
            this.txtTypeExport.AccAcceptNumbersOnly = false;
            this.txtTypeExport.AccAllowComma = false;
            this.txtTypeExport.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTypeExport.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTypeExport.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTypeExport.AccHidenValue = "";
            this.txtTypeExport.AccNotAllowedChars = null;
            this.txtTypeExport.AccReadOnly = false;
            this.txtTypeExport.AccReadOnlyAllowDelete = false;
            this.txtTypeExport.AccRequired = false;
            this.txtTypeExport.BackColor = System.Drawing.Color.White;
            this.txtTypeExport.CustomBackColor = System.Drawing.Color.White;
            this.txtTypeExport.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTypeExport.ForeColor = System.Drawing.Color.Black;
            this.txtTypeExport.Location = new System.Drawing.Point(94, 441);
            this.txtTypeExport.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeExport.MaxLength = 32767;
            this.txtTypeExport.Multiline = false;
            this.txtTypeExport.Name = "txtTypeExport";
            this.txtTypeExport.ReadOnly = false;
            this.txtTypeExport.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTypeExport.Size = new System.Drawing.Size(28, 27);
            this.txtTypeExport.TabIndex = 580;
            this.txtTypeExport.Text = "0";
            this.txtTypeExport.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTypeExport.UseSystemPasswordChar = false;
            this.txtTypeExport.Visible = false;
            // 
            // txtCode
            // 
            this.txtCode.AccAcceptNumbersOnly = false;
            this.txtCode.AccAllowComma = false;
            this.txtCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode.AccHidenValue = "";
            this.txtCode.AccNotAllowedChars = null;
            this.txtCode.AccReadOnly = false;
            this.txtCode.AccReadOnlyAllowDelete = false;
            this.txtCode.AccRequired = false;
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.CustomBackColor = System.Drawing.Color.White;
            this.txtCode.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCode.ForeColor = System.Drawing.Color.Black;
            this.txtCode.Location = new System.Drawing.Point(94, 8);
            this.txtCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode.MaxLength = 32767;
            this.txtCode.Multiline = false;
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = false;
            this.txtCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode.Size = new System.Drawing.Size(202, 27);
            this.txtCode.TabIndex = 503;
            this.txtCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode.UseSystemPasswordChar = false;
            this.txtCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCode_KeyPress);
            // 
            // frmLogimatique
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(620, 474);
            this.Controls.Add(this.txtTypeExport);
            this.Controls.Add(this.cmdExporter);
            this.Controls.Add(this.lblNbExport1);
            this.Controls.Add(this.lblDescriptionExport1);
            this.Controls.Add(this.lblNbExport);
            this.Controls.Add(this.lblDescriptionExport);
            this.Controls.Add(this.cmdFermer);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.GridExport);
            this.Controls.Add(this.GridSource);
            this.Controls.Add(this.chkExport);
            this.Controls.Add(this.cmdRecherche);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(636, 513);
            this.MinimumSize = new System.Drawing.Size(636, 513);
            this.Name = "frmLogimatique";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Export";
            this.Activated += new System.EventHandler(this.frmLogimatique_Activated);
            this.Deactivate += new System.EventHandler(this.frmLogimatique_Deactivate);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmLogimatique_FormClosing);
            this.Load += new System.EventHandler(this.frmLogimatique_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridExport)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtCode;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button cmdRecherche;
        private System.Windows.Forms.CheckBox chkExport;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridSource;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridExport;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdFermer;
        private System.Windows.Forms.Label lblDescriptionExport;
        private System.Windows.Forms.Label lblNbExport;
        private System.Windows.Forms.Label lblNbExport1;
        private System.Windows.Forms.Label lblDescriptionExport1;
        public System.Windows.Forms.Button cmdExporter;
        public iTalk.iTalk_TextBox_Small2 txtTypeExport;
    }
}