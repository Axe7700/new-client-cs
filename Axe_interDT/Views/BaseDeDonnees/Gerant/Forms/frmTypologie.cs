﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    public partial class frmTypologie : Form
    {
        private DataTable rsTypologie;
        private ModAdo modAdorsTypologie;
        public frmTypologie()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmTypologie_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            modAdorsTypologie = new ModAdo();
            rsTypologie = modAdorsTypologie.fc_OpenRecordSet("SELECT CodeTypologie,Designation FROM TypeTypologie ORDER BY CodeTypologie", gridTypologie, "CodeTypologie");
            gridTypologie.DataSource = rsTypologie;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_Leave(object sender, EventArgs e)
        {
            gridTypologie.UpdateData();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsTypologie.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CodeTypologie"].Hidden = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            chkSelect.Checked = true;
            Visible = false;
            Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridTypologie_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsTypologie.Update();
        }
    }
}
