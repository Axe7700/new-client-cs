﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    public partial class frmVisu : Form
    {
        public frmVisu()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            this.Visible = false;
            Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            int i = 0;
            if (GridVisu.ActiveRow == null)
                return;
            if (chkAnnuler.CheckState == 0)
            {
                General.tabCodeSelect = new General.tpCodeSelect[2];
                for (i = 0; i < GridVisu.Rows.Count - 1; i++)
                {
                    if (Convert.ToInt32(GridVisu.ActiveRow.Cells[0].Value) != 0)
                    {
                        if (General.tabCodeSelect.Length < i)
                        {
                            Array.Resize(ref General.tabCodeSelect, i + 1);
                        }
                        General.tabCodeSelect[i].sChamp = GridVisu.DisplayLayout.Bands[0].Columns[1].Key;
                        General.tabCodeSelect[i].sCode = GridVisu.ActiveRow.Cells[1].Value.ToString();
                        General.tabCodeSelect[i].sSelect = short.Parse(GridVisu.ActiveRow.Cells[0].Value.ToString());
                    }
                }
            }
            this.Visible = false;
            Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmVisu_Activated(object sender, EventArgs e)
        {
            
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Select()
        {
            int i = 0;
            int j = 0;
            DataTable rsTemp = default(DataTable);
            ModAdo rsTempModAdo;
            int sBaseColumnName = 0;
            int sBaseTableName = 0;

            sBaseTableName = 3;
            sBaseColumnName = 1;

            if (!string.IsNullOrEmpty(txtSelect.Text))
            {

                GridVisu.Visible = true;
                //GridVisu.FieldDelimiter = "!";
                //GridVisu.FieldSeparator = ";";
                rsTempModAdo = new ModAdo();
                rsTemp = rsTempModAdo.fc_OpenRecordSet(txtSelect.Text + " " + txtWhere.Text + " " + txtOrder.Text);
                GridVisu.DataSource = null;
                GridVisu.Visible = false;

                GridVisu.DataSource = rsTemp;

                GridVisu.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Sélection";
                for (i = 0; i <= rsTemp.Columns.Count - 1; i++)
                {
                    GridVisu.DisplayLayout.Bands[0].Columns[i].Header.Caption = rsTemp.Columns[i].ColumnName;
                    //            For j = 0 To rsTemp.Fields(i).Properties.Count - 1
                    //                Debug.Print rsTemp.Fields(i).Properties(j).Name & " / " & rsTemp.Fields(i).Properties(j).value
                    //            Next
                }

                if (i < GridVisu.DisplayLayout.Bands[0].Columns.Count - 1)
                {
                    for (i = i + 1; i <= GridVisu.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    {
                        GridVisu.DisplayLayout.Bands[0].Columns[i].Hidden = true;
                    }
                }

                for (i = 0; i <= GridVisu.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    if (GridVisu.DisplayLayout.Bands[0].Columns[i].Header.Caption.ToUpper() != "Sélection".ToUpper())
                    {
                        GridVisu.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.ActivateOnly;
                    }
                }

                GridVisu.DisplayLayout.Bands[0].Columns[0].Style = ColumnStyle.CheckBox;
                for (i = 0; i < GridVisu.Rows.Count; i++)
                    GridVisu.Rows[i].Cells[0].Value = true;
                GridVisu.UpdateData();
                GridVisu.Visible = true;
                //rsTempModAdo.Dispose();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmVisu_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_Select();
        }
    }
}
