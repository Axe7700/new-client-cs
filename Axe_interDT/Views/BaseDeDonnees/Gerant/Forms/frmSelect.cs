﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    public partial class frmSelect : Form
    {
        bool blnActivate;
        public frmSelect()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            chkAnnuler.Checked = true;
            Visible = false;
            Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisu_Click(object sender, EventArgs e)
        {
            int i = 0;

            GridSelect.UpdateData();
            frmVisu frmVisu = new frmVisu();
            frmVisu.txtSelect.Text = this.txtSelect.Text;
            for (i = 0; i < General.tabCodeSelect.Length; i++)
            {
                if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                {
                    if (string.IsNullOrEmpty(frmVisu.txtWhere.Text))
                    {
                        frmVisu.txtWhere.Text = " WHERE " + General.tabCodeSelect[i].sChamp + "='" + General.tabCodeSelect[i].sCode + "'";
                    }
                    else
                    {
                        frmVisu.txtWhere.Text = frmVisu.txtWhere.Text + " OR " + General.tabCodeSelect[i].sChamp + "='" + General.tabCodeSelect[i].sCode + "'";
                    }
                }
            }
            if (string.IsNullOrEmpty(frmVisu.txtWhere.Text))
            {
                frmVisu.txtWhere.Text = " WHERE " + GridSelect.DisplayLayout.Bands[0].Columns[1].Key + "=''";
            }
            frmVisu.txtOrder.Text = this.txtOrder.Text;

            frmVisu.ShowDialog();


            frmVisu.Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            chkAnnuler.CheckState = System.Windows.Forms.CheckState.Unchecked;
            GridSelect.UpdateData();
            this.Visible = false;
            Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSelect_Activated(object sender, EventArgs e)
        {
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Select()
        {
            int i = 0;
            int j = 0;
            DataTable rsTemp = default(DataTable);
            ModAdo modAdorsTemp;
            int sBaseColumnName = 0;
            int sBaseTableName = 0;
            string sSelect = null;
            string sWhere = null;

            sBaseTableName = 3;
            sBaseColumnName = 1;

            if (!string.IsNullOrEmpty(txtSelect.Text))
            {

                GridSelect.Visible = true;

                rsTemp = new DataTable();
                modAdorsTemp = new ModAdo();
                sWhere = txtWhere.Text;

                if (!string.IsNullOrEmpty(txtFiltreCode.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE " + GridSelect.DisplayLayout.Bands[0].Columns[1].Key + fc_ReturnType((GridSelect.DisplayLayout.Bands[0].Columns[1].DataType),
                            (txtFiltreCode.Text));
                    }
                    else
                    {
                        sWhere = sWhere + " AND " + GridSelect.DisplayLayout.Bands[0].Columns[1].Key + fc_ReturnType(GridSelect.DisplayLayout.Bands[0].Columns[1].DataType,
                         txtFiltreCode.Text);
                    }
                }

                if (!string.IsNullOrEmpty(txtFiltre.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE " + GridSelect.DisplayLayout.Bands[0].Columns[2].Key + fc_ReturnType(GridSelect.DisplayLayout.Bands[0].Columns[2].DataType, txtFiltre.Text);
                    }
                    else
                    {
                        sWhere = sWhere + " AND " + GridSelect.DisplayLayout.Bands[0].Columns[2].Key + fc_ReturnType(GridSelect.DisplayLayout.Bands[0].Columns[2].DataType, txtFiltre.Text);
                    }
                }
                modAdorsTemp = new ModAdo();
                rsTemp = modAdorsTemp.fc_OpenRecordSet(txtSelect.Text + " " + sWhere + " " + txtOrder.Text);
                foreach (DataRow Row in rsTemp.Rows)
                    Row[0] = false;
                GridSelect.DataSource = rsTemp;
                for (i = 0; i <= rsTemp.Columns.Count - 1; i++)
                {
                    //GridSelect.DisplayLayout.Bands[0].Columns[i].Header.Caption = rsTemp.Columns[i].ColumnName;
                    //GridSelect.DisplayLayout.Bands[0].Columns[i + 1].Key = rsTemp.TableName + "." + rsTemp.Columns[i + 1].ColumnName;
                    //            For j = 0 To rsTemp.Fields(i).Properties.Count - 1
                    //                Debug.Print rsTemp.Fields(i).Properties(j).Name & " / " & rsTemp.Fields(i).Properties(j).value
                    //            Next
                }

                if (i < GridSelect.DisplayLayout.Bands[0].Columns.Count - 1)
                {
                    for (i = i + 1; i <= GridSelect.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    {
                        GridSelect.DisplayLayout.Bands[0].Columns[i].Hidden = true;
                    }
                }

                for (i = 0; i <= GridSelect.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    if (GridSelect.DisplayLayout.Bands[0].Columns[i].Header.Caption.ToUpper() != "Sélection".ToUpper())
                    {
                        GridSelect.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.ActivateOnly;
                    }
                }

                GridSelect.DisplayLayout.Bands[0].Columns[0].Style = ColumnStyle.CheckBox;
                GridSelect.Visible = true;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lngType"></param>
        /// <param name="sCritere"></param>
        /// <returns></returns>
        private object fc_ReturnType(Type lngType, string sCritere)
        {
            object functionReturnValue = null;
            object AdVector = null;
            object adByRef = null;
            if (lngType == typeof(Array))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(int))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(byte))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(bool))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(char))
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            else if (lngType == typeof(DateTime))
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            else if (lngType == typeof(TimeSpan))
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            else if (lngType == typeof(decimal))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(double))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else if (lngType == typeof(TimeSpan))
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            else if (lngType == typeof(string))
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            else if (lngType == typeof(float))
            {
                functionReturnValue = "=" + sCritere + "";
            }
            else
            {
                functionReturnValue = " LIKE '%" + sCritere + "%'";
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSelect_FormClosed(object sender, FormClosedEventArgs e)
        {
            blnActivate = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSelect_AfterCellUpdate(object sender, CellEventArgs e)
        {
            int i = 0;
            bool blnMatch = false;

            blnMatch = false;
            for (i = 0; i < General.tabCodeSelect.Length; i++)
            {
                if (General.tabCodeSelect[i].sCode == GridSelect.ActiveRow.Cells[1].Text)
                {
                    General.tabCodeSelect[i].sSelect = Convert.ToBoolean(GridSelect.ActiveRow.Cells[0].Value.ToString()) ? (short)1 : (short)0;
                    blnMatch = true;
                    break;
                }
            }

            if (blnMatch == false)
            {
                Array.Resize(ref General.tabCodeSelect, General.tabCodeSelect.Length + 1);
                General.tabCodeSelect[General.tabCodeSelect.Length - 1].sCode = GridSelect.ActiveRow.Cells[1].Text;
                General.tabCodeSelect[General.tabCodeSelect.Length - 1].sSelect = Convert.ToBoolean(GridSelect.ActiveRow.Cells[0].Value.ToString()) ? (short)1 : (short)0;
                General.tabCodeSelect[General.tabCodeSelect.Length - 1].sChamp = GridSelect.ActiveRow.Cells[1].Column.Key;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltre_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                if (string.IsNullOrEmpty(txtFiltre.Text))
                {
                    txtFiltreWhere.Text = "";
                }
                else
                {
                    if (string.IsNullOrEmpty(txtWhere.Text))
                    {
                        txtFiltreWhere.Text = " WHERE " + GridSelect.ActiveRow.Cells[2].Column.Key + " LIKE '%" + txtFiltre.Text + "%'";
                    }
                    else
                    {
                        txtFiltreWhere.Text = " AND " + GridSelect.ActiveRow.Cells[2].Column.Key + " LIKE '%" + txtFiltre.Text + "%'";
                    }
                }
                fc_Select();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;

            if (KeyAscii == 13)
            {
                if (string.IsNullOrEmpty(txtFiltreCode.Text))
                {
                    txtFiltreWhere.Text = "";
                }
                else
                {
                    if (string.IsNullOrEmpty(txtWhere.Text))
                    {
                        txtFiltreWhere.Text = " WHERE " + GridSelect.DisplayLayout.Bands[0].Columns[1].Key + " LIKE '%" + txtFiltreCode.Text + "%'";
                    }
                    else
                    {
                        txtFiltreWhere.Text = " AND " + GridSelect.DisplayLayout.Bands[0].Columns[1].Key + " LIKE '%" + txtFiltreCode.Text + "%'";
                    }
                }
                fc_Select();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSelect_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            DataTable rsTemp = default(DataTable);
            ModAdo modAdorsTemp = new ModAdo();
            //Load Schema

            //GridSelect.DataSource = modAdorsTemp.fc_OpenRecordSet("SELECT '' As Sélection, Personnel.Matricule AS [Matricule],Personnel.Nom,Personnel.Prenom,SpecifQualif.LibelleQualif AS Qualification FROM" +
            //    " Personnel LEFT JOIN SpecifQualif ON Personnel.CodeQualif=SpecifQualif.CodeQualif WHERE Personnel.Matricule = '00000000000'");

            General.tabCodeSelect = new General.tpCodeSelect[2];
            fc_Select();
            blnActivate = true;
        }
    }
}
