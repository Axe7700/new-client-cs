﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using CrystalDecisions.Shared;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    public partial class frmLogimatique : Form
    {
        private DataTable rsSource;
        private ModAdo modAdorsSource;
        string sExclure;
        string sSelectNb;

        const string cFinDeLigne = "";
        public frmLogimatique()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFermer_Click(object sender, EventArgs e)
        {
            Visible = false;
            Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWhere"></param>
        /// <param name="bSuppression"></param>
        /// <returns></returns>
        private bool fc_ExportImmToTxt(string sWhere, bool bSuppression)
        {
            bool functionReturnValue = false;
            string sSQL = null;
            string sEntete = null;
            string sCorps = null;
            string sFichierP2IMM = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors;
            StreamWriter tsText = null;

            try
            {

                functionReturnValue = true;

                sFichierP2IMM = General.gfr_liaison("Fichier P2IMMEUB", "\\\\DTNT400\\Logim-Fic\\Import\\P2IMMEUB.txt");

                if (Dossier.fc_ControleFichier(sFichierP2IMM) == true)
                {
                    if (bSuppression == true)
                    {
                        Dossier.fc_SupprimeFichier(sFichierP2IMM);
                    }
                    else
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il existe déjà un fichier, voulez-vous le supprimer ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Dossier.fc_SupprimeFichier(sFichierP2IMM);
                        }
                        else
                        {
                            functionReturnValue = false;
                            return functionReturnValue;
                        }
                    }
                }


                sSQL = "SELECT DISTINCT " + " LEFT(CodeImmeuble, 40) AS CodeImmeuble, LEFT(Code1, 40) AS Code1, LEFT(CodeTVA, 40) AS CodeTVA, LEFT(NCompte, 40) AS NCompte, " + " LEFT(AngleRue, 40) AS AngleRue, LEFT(Adresse, 40) AS Adresse, LEFT(Ville, 30) AS Ville, LEFT(CodePostal, 5) AS CodePostal, LEFT(Gardien, 40) " + " AS Gardien, LEFT(AdresseGardien, 40) AS AdresseGardien, LEFT(TelGardien, 40) AS TelGardien, LEFT(FaxGardien, 40) AS FaxGardien, " + " '' AS SpecifAcces, '' AS CodeAcces1, '' AS CodeAcces2, '' " + " AS BoiteAclef, '' AS SitBoiteAclef, '' AS ClefAcces, '' AS Observations, " + " LEFT(CommentaireFacture1, 40) AS CommentaireFacture1, LEFT(CommentaireFacture2, 40) AS CommentaireFacture2, LEFT(CodeGestionnaire, 40) " + " AS CodeGestionnaire, LEFT(HorairesLoge, 40) AS HorairesLoge, LEFT(horaires, 40) AS Horaires, LEFT(CodeCommercial, 40) AS CodeCommercial, " + " '' AS CodeReglement" + " FROM Immeuble" + sWhere;

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {

                    //===== prepare en tete de fichier.
                    sEntete = "\"CodeImmeuble,Code1,CodeTVA,NCompte,AngleRue,Adresse,Ville,CodePostal,Gardien,AdresseGardien," + " TelGardien,FaxGardien,SpecifAcces,CodeAcces1,CodeAcces2,BoiteAclef,SitBoiteAclef,ClefAcces,Observations," + " CommentaireFacture1,CommentaireFacture2,CodeGestionnaire,HorairesLoge,Horaires,CodeCommercial,CodeReglement\"";

                    sEntete = sEntete.Replace(",", "\"" + "\t" + "\"");

                    //sEntete = sEntete

                    tsText = new StreamWriter(sFichierP2IMM, true);

                    //==== ecris dans le fichier les entetes.
                    tsText.WriteLine(sEntete + cFinDeLigne);

                    foreach (DataRow rsRow in rs.Rows)
                    {
                        sCorps = "\"" + rsRow["CodeImmeuble"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Code1"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodeTVA"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Ncompte"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["AngleRue"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Adresse"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Ville"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodePostal"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Gardien"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["AdresseGardien"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["TelGardien"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["FaxGardien"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["SpecifAcces"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["codeAcces1"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodeAcces2"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["BoiteAClef"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["SitBoiteAClef"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["ClefAcces"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Observations"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CommentaireFacture1"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CommentaireFacture2"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodeGestionnaire"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["HorairesLoge"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Horaires"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodeCommercial"] + "\"" + "\t";
                        //==== attention mettre un retour charoit pour le cerneir champs.
                        sCorps = sCorps + "\"" + rsRow["CodeReglement"] + "\"";
                        //

                        tsText.WriteLine(sCorps + cFinDeLigne);
                    }

                    //tsText.WriteLine vbNullChar & vbNullChar

                    tsText.Close();
                    tsText.Dispose();

                }

                modAdors.Dispose();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ExportImmToTxt");
                throw;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWhere"></param>
        /// <param name="bSuppression"></param>
        /// <returns></returns>
        private bool fc_ExporClientToTxt(string sWhere, bool bSuppression)
        {
            bool functionReturnValue = false;
            string sSQL = null;
            string sEntete = null;
            string sCorps = null;
            string sFichierP2CLI = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors;
            StreamWriter tsText = null;
            try
            {

                functionReturnValue = true;

                sFichierP2CLI = General.gfr_liaison("Fichier P2CLIENT", "\\\\DTNT400\\LogimFic\\Import\\P2CLIENT.txt");


                if (Dossier.fc_ControleFichier(sFichierP2CLI) == true)
                {
                    if (bSuppression == true)
                    {
                        Dossier.fc_SupprimeFichier(sFichierP2CLI);
                    }
                    else
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il existe déjà un fichier, voulez-vous le supprimer ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Dossier.fc_SupprimeFichier(sFichierP2CLI);
                        }
                        else
                        {
                            functionReturnValue = false;
                            return functionReturnValue;
                        }
                    }
                }



                sSQL = " select DISTINCT LEFT([Table1].[Code1],40) as Code1, left([Table1].[Nom],40) as Nom, left([Table1].[Adresse1],40) as Adresse1," + " left([Table1].[Adresse2],40) as Adresse2, left([Table1].[Ville],30) as Ville, left([Table1].[CodePostal],5) as CodePostal, left([Table1].[Telephone],20) as Tel," + " left([Table1].[Fax],20) as Fax, left([Table1].[eMail],30) as EMail, '' AS Observations" + " FROM ([Table1] " + "INNER JOIN Immeuble ON Table1.Code1=Immeuble.Code1)" + sWhere;


                //    oCustomTask1.SourceSQLStatement = "select DISTINCT LEFT([Table1].[Code1],40) as Code1, left([Table1].[Nom],40) as Nom, left([Table1].[Adresse1],40) as Adresse1, left([Table1].[Adresse2],40) as Adresse2, left([Table1].[Ville],30) as Ville, left([Table1].[CodePostal],5) as CodePostal, left(["
                //    oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "Table1].[Telephone],20) as Tel, left([Table1].[Fax],20) as Fax, left([Table1].[eMail],30) as EMail, '' AS Observations" & vbCrLf
                //    oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "FROM ([Table1]" & vbCrLf
                //    oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & "INNER JOIN Immeuble ON Table1.Code1=Immeuble.Code1)" & vbCrLf
                //
                //    oCustomTask1.SourceSQLStatement = oCustomTask1.SourceSQLStatement & " " & strWhere

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {

                    //===== prepare en tete de fichier.
                    sEntete = "\"Code1,Nom,Adresse1,Adresse2,Ville,CodePostal,Tel,Fax,EMail,Observations\"";

                    sEntete = sEntete.Replace(",", "\"" + "\t" + "\"");

                    sEntete = sEntete + cFinDeLigne;

                    tsText = new StreamWriter(sFichierP2CLI, true);

                    //==== ecris dans le fichier les entetes.
                    tsText.WriteLine(sEntete);

                    foreach (DataRow rsRow in rs.Rows)
                    {

                        sCorps = "\"" + rsRow["Code1"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Nom"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Adresse1"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Adresse2"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Ville"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["CodePostal"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Tel"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["fax"] + "\"" + "\t";
                        sCorps = sCorps + "\"" + rsRow["Email"] + "\"" + "\t";
                        //==== attention mettre un retour charoit pour le cerneir champs.
                        sCorps = sCorps + "\"" + rsRow["Observations"] + "\"";
                        //

                        tsText.WriteLine(sCorps + cFinDeLigne);
                    }

                    tsText.Close();
                    tsText.Dispose();
                }

                modAdors.Dispose();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ExporClientToTxt");
                throw;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExporter_Click(object sender, EventArgs e)
        {
            string sWhere = null;
            string sWhereImCli = null;
            int i = 0;
            DataTable rsImm = default(DataTable);
            ModAdo modAdorsImm = null;
            try
            {
                sWhere = "";
                if (txtTypeExport.Text == "0")
                {
                    for (i = 0; i <= GridExport.Rows.Count - 1; i++)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(GridExport.Rows[i].Cells["Code"].Text) + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(GridExport.Rows[i].Cells["Code"].Text) + "'";
                        }
                        if ((int)chkExport.CheckState == Convert.ToInt32("0") || (int)chkExport.CheckState == Convert.ToInt32("1"))
                        {
                            using (var tmpModAdo = new ModAdo())
                            {
                                if (string.IsNullOrEmpty(sWhereImCli))
                                {

                                    sWhereImCli = " WHERE Table1.Code1='" +
                                                  StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle(
                                                      "SELECT Code1 FROM Immeuble WHERE CodeImmeuble='" +
                                                      GridExport.Rows[i].Cells["Code"].Text + "'")) + "'";
                                }
                                else
                                {
                                    sWhereImCli = sWhereImCli + " OR Table1.Code1='" +
                                                  StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle(
                                                      "SELECT Code1 FROM Immeuble WHERE CodeImmeuble='" +
                                                      GridExport.Rows[i].Cells["Code"].Text + "'")) + "'";
                                }
                            }
                        }
                    }

                    //=== modif du 27/01/2014, export des fichiers sans utiliser le mode DTS.
                    if (General.sExportTxtToLogim == "1")
                    {
                        if (fc_ExportImmToTxt(sWhere, false) == true)
                        {
                            if (((int)chkExport.CheckState == 0 || (int)chkExport.CheckState == 1) && !string.IsNullOrEmpty(sWhereImCli))
                            {
                                fc_ExporClientToTxt(sWhereImCli, true);
                            }
                        }
                    }
                    else
                    {
                        //TODO : Mondir - Must Develope This Fonction
                        //P2IMMEUB.ExportImmeubleLogimatique(ref sWhere); ====================> Test Again
                        if (((int)chkExport.CheckState == 0 || (int)chkExport.CheckState == 1) && !string.IsNullOrEmpty(sWhereImCli))
                        {
                            //TODO : Mondir - Must Develope This Fonction ====================> Test Again
                            //P2CLIENT.ExportClientLogimatique(ref sWhereImCli);
                        }
                    }
                }
                else if (txtTypeExport.Text == "1")
                {
                    for (i = 0; i <= GridExport.Rows.Count - 1; i++)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Table1.Code1='" + StdSQLchaine.gFr_DoublerQuote(GridExport.Rows[i].Cells["Code"].Text) + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR Table1.Code1='" + StdSQLchaine.gFr_DoublerQuote(GridExport.Rows[i].Cells["Code"].Text) + "'";
                        }
                        if ((int)chkExport.CheckState == 0 || (int)chkExport.CheckState == 1)
                        {
                            if ((rsImm != null))
                            {
                                rsImm = new DataTable();
                            }
                            modAdorsImm = new ModAdo();
                            rsImm = modAdorsImm.fc_OpenRecordSet("SELECT CodeImmeuble FROM Immeuble WHERE Code1='" +
                                StdSQLchaine.gFr_DoublerQuote(GridExport.Rows[i].Cells["Code"].Text) + "'");
                            if (rsImm.Rows.Count > 0)
                            {
                                foreach (DataRow rsImmRow in rsImm.Rows)
                                {
                                    if (string.IsNullOrEmpty(sWhereImCli))
                                    {
                                        sWhereImCli = " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(rsImmRow["CodeImmeuble"] + "") + "'";
                                    }
                                    else
                                    {
                                        sWhereImCli = sWhereImCli + " OR Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(rsImmRow["CodeImmeuble"] + "") + "'";
                                    }
                                }
                            }
                        }
                    }

                    ///=====================> Tested
                    if (General.sExportTxtToLogim == "1")
                    {
                        if (fc_ExporClientToTxt(sWhere, false) == true)
                        {
                            if (((int)chkExport.CheckState == 0 || (int)chkExport.CheckState == 1) && !string.IsNullOrEmpty(sWhereImCli))
                            {
                                fc_ExportImmToTxt(sWhereImCli, true);
                            }
                        }
                    }
                    else
                    {
                        //TODO : Mondir - Must Develope This Fonction ====================> Test Again
                        //P2CLIENT.ExportClientLogimatique(ref sWhere);
                        if (((int)chkExport.CheckState == 0 || (int)chkExport.CheckState == 1) && !string.IsNullOrEmpty(sWhereImCli))
                        {
                            //TODO : Mondir - Must Develope This Fonction ====================> Test Again
                            //P2IMMEUB.ExportImmeubleLogimatique(ref sWhereImCli);
                        }
                    }

                    //        If (chkExport.value = "-1" Or chkExport.value = "1") And sWhereImCli <> "" Then
                    //                If sExportTxtToLogim = "1" Then
                    //                        fc_ExportImmToTxt sWhereImCli
                    //                Else
                    //                        ExportImmeubleLogimatique sWhereImCli
                    //                End If
                    //        End If
                }

                modAdorsImm?.Dispose();

                sExclure = "";
                var DT = GridExport.DataSource as DataTable;
                DT.Rows.Clear();
                GridExport.DataSource = DT;
                fc_GetCode();
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Exportation terminée", "Export Logimatique", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, " frmLogimatique cmdExporter_Click ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_GetCode()
        {
            string sSelect = null;
            string sWhere = null;

            try
            {

                //0 : Immeubles
                if (txtTypeExport.Text == "0")
                {
                    sSelect = "SELECT CodeImmeuble AS 'Code' FROM Immeuble ";
                    if (!string.IsNullOrEmpty(txtCode.Text))
                    {
                        sWhere = " WHERE CodeImmeuble LIKE '%" + txtCode.Text + "%'";
                    }
                    if (!string.IsNullOrEmpty(sExclure))
                    {
                        if (!string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = sWhere + " AND " + sExclure;
                        }
                        else
                        {
                            sWhere = " WHERE " + sExclure;
                        }
                    }
                    modAdorsSource = new ModAdo();
                    rsSource = modAdorsSource.fc_OpenRecordSet(sSelect + sWhere + " ORDER BY CodeImmeuble");

                    //1 : Gérants
                }
                /// =================> Tested
                else if (txtTypeExport.Text == "1")
                {
                    if (!string.IsNullOrEmpty(txtCode.Text))
                    {
                        sWhere = " WHERE Code1 LIKE '%" + txtCode.Text + "%'";
                    }

                    if (!string.IsNullOrEmpty(sExclure))
                    {
                        if (!string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = sWhere + " AND " + sExclure;
                        }
                        else
                        {
                            sWhere = " WHERE " + sExclure;
                        }
                    }

                    sSelect = "SELECT Code1 AS 'Code' FROM Table1 ";
                    modAdorsSource = new ModAdo();
                    rsSource = modAdorsSource.fc_OpenRecordSet(sSelect + sWhere + " ORDER BY Code1");
                }

                GridSource.DataSource = rsSource;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, " frmLogimatique fc_GetCode ");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRecherche_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "";
                string where_order = "";
                string sText = "";
                if (txtTypeExport.Text == "0")
                {
                    requete = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," +
                              " Ville as \"Ville\" ," +
                              " anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble";
                    sText = "Recherche d'un immeuble";
                }
                else if (txtTypeExport.Text == "1")
                {
                    requete = "SELECT Code1 as \"Code Gérant\"," + " Adresse1 as \"Adresse\"," +
                              " Ville as \"Ville\" , CodePostal AS \"Code Postal\" FROM Table1 ";
                    sText = "Recherche d'un gérant";
                }
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = sText };
                fg.SetValues(new Dictionary<string, string>
                {
                    {txtTypeExport.Text == "0" ? "CodeImmeuble" : "Code1", txtCode.Text}
                });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_GetCode();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_GetCode();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ee)
            {
                Erreurs.gFr_debug(ee, " frmLogimatique  cmdrecherche_Click ");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogimatique_Activated(object sender, EventArgs e)
        {




        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogimatique_Deactivate(object sender, EventArgs e)
        {
            //sExclure = "";
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogimatique_FormClosing(object sender, FormClosingEventArgs e)
        {
            modAdorsSource?.Dispose();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridExport_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Etes-vous sûr de ne pas vouloir exporter le (les) enregistrement(s) sélectionné(s)", "Suppression d'un export", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                e.Cancel = false;
                if (txtTypeExport.Text == "0")
                {
                    fc_RecupCode("CodeImmeuble<>'" + e.Rows[0].Cells["Code"].Text + "'");
                }
                else if (txtTypeExport.Text == "1")
                {
                    fc_RecupCode("Code1<>'" + e.Rows[0].Cells["Code"].Text + "'");
                }
                fc_GetCode();
                lblNbExport.Text = Convert.ToString(Convert.ToInt32(lblNbExport.Text) - 1);
            }
            else
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="scode"></param>
        private void fc_RecupCode(string scode)
        {
            if (sExclure.Contains("AND " + scode))
            {
                sExclure = sExclure.Replace("AND " + scode + " ", "");
            }
            else if (sExclure.Contains(scode))
            {
                sExclure = sExclure.Replace(scode + " AND ", "");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSource_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            DataTable rsImmCli = default(DataTable);

            var DataSource = GridExport.DataSource as DataTable;
            DataSource.Rows.Add("1", e.Row.Cells["Code"].Text);
            GridExport.DataSource = DataSource;
            if (txtTypeExport.Text == "0")
            {
                if (string.IsNullOrEmpty(sExclure))
                {
                    sExclure = " CodeImmeuble<>'" + e.Row.Cells["Code"].Text + "'";
                }
                else
                {
                    sExclure = sExclure + " AND CodeImmeuble<>'" + e.Row.Cells["Code"].Text + "'";
                }

                //        Set rsImmCli = New ADODB.Recordset
                //        If sSelectNb <> "" Then
                //            sSelectNb = sSelectNb & " OR CodeImmeuble='" & GridSource.Columns("Code").Text & "'"
                //        Else
                //            sSelectNb = " WHERE CodeImmeuble='" & GridSource.Columns("Code").Text & "'"
                //        End If
                //        rsImmCli.Open "SELECT DISTINCT Table1.Code1 FROM Table1 INNER JOIN Immeuble ON Table1.Code1=Immeuble.Code1 " & sSelectNb, adocnn, adOpenStatic, adLockOptimistic
                //        If Not rsImmCli.EOF And Not rsImmCli.BOF Then
                //            rsImmCli.MoveFirst
                //            lblNbExport1.Caption = "0"
                //            Do While Not rsImmCli.EOF
                //                lblNbExport1.Caption = CStr(CLng(lblNbExport1.Caption) + 1)
                //                rsImmCli.MoveNext
                //            Loop
                //            DoEvents
                //        End If
                //        rsImmCli.Close
                //        Set rsImmCli = Nothing

            }
            else if (txtTypeExport.Text == "1")
            {
                if (string.IsNullOrEmpty(sExclure))
                {
                    sExclure = " Code1<>'" + e.Row.Cells["Code"].Text + "'";
                }
                else
                {
                    sExclure = sExclure + " AND Code1<>'" + e.Row.Cells["Code"].Text + "'";
                }

                //        Set rsImmCli = New ADODB.Recordset
                //        If sSelectNb <> "" Then
                //            sSelectNb = sSelectNb & " OR Code1='" & GridSource.Columns("Code").Text & "'"
                //        Else
                //            sSelectNb = " WHERE Code1='" & GridSource.Columns("Code").Text & "'"
                //        End If
                //        rsImmCli.Open "SELECT Immeuble.CodeImmeuble FROM Immeuble " & sSelectNb, adocnn, adOpenStatic, adLockOptimistic
                //        lblNbExport1.Caption = CStr(rsImmCli.RecordCount)
                //        rsImmCli.Close
                //        Set rsImmCli = Nothing

            }
            lblNbExport.Text = Convert.ToString(Convert.ToInt32(lblNbExport.Text) + 1);
            fc_GetCode();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                fc_GetCode();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmLogimatique_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_GetCode();

            var DataSource = new DataTable();
            DataSource.Columns.Add("Exporter");
            DataSource.Columns.Add("Code");
            GridExport.DataSource = DataSource;

            sExclure = "";
            sSelectNb = "";
            if (txtTypeExport.Text == "0")
            {
                GridExport.DisplayLayout.Bands[0].Columns["Exporter"].Hidden = true;
                GridExport.DisplayLayout.Bands[0].Columns["Code"].Header.Caption = "Code immeuble";
                GridSource.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Code immeuble";
                chkExport.Text = "Joindre le gérant des immeubles";
                this.Text = "Exporter des immeubles vers Logimatique";
                lblDescriptionExport.Text = "Nombre d'immeubles à exporter :";
                lblNbExport.Text = "0";
                lblDescriptionExport1.Text = "Nombre de gérants à exporter :";
                lblNbExport1.Text = "0";

            }
            else if (txtTypeExport.Text == "1")
            {
                GridExport.DisplayLayout.Bands[0].Columns["Exporter"].Hidden = true;
                GridExport.DisplayLayout.Bands[0].Columns["Code"].Header.Caption = "Code gérant";
                GridSource.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Code gérant";
                chkExport.Text = "Joindre les immeubles du gérant";
                this.Text = "Exporter des gérants vers Logimatique";
                lblDescriptionExport.Text = "Nombre de clients à exporter :";
                lblNbExport.Text = "0";
                lblDescriptionExport1.Text = "Nombre d'immeubles à exporter :";
                lblNbExport1.Text = "0";
            }
        }

        private void GridSource_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for (int i = 0; i < e.Layout.Bands[0].Columns.Count; i++)
                e.Layout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }

        private void GridExport_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for (int i = 0; i < e.Layout.Bands[0].Columns.Count; i++)
                e.Layout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }
    }
}
