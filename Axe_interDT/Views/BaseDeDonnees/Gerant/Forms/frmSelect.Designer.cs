﻿namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    partial class frmSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.chkAnnuler = new System.Windows.Forms.CheckBox();
            this.GridSelect = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.txtOrder = new iTalk.iTalk_TextBox_Small2();
            this.txtWhere = new iTalk.iTalk_TextBox_Small2();
            this.txtSelect = new iTalk.iTalk_TextBox_Small2();
            this.txtFiltreWhere = new iTalk.iTalk_TextBox_Small2();
            this.txtFiltre = new iTalk.iTalk_TextBox_Small2();
            this.txtFiltreCode = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridSelect)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(2, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(85, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Filtre Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(235, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(45, 19);
            this.label1.TabIndex = 504;
            this.label1.Text = "Filtre";
            // 
            // chkAnnuler
            // 
            this.chkAnnuler.AutoSize = true;
            this.chkAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.chkAnnuler.Location = new System.Drawing.Point(53, 43);
            this.chkAnnuler.Name = "chkAnnuler";
            this.chkAnnuler.Size = new System.Drawing.Size(72, 20);
            this.chkAnnuler.TabIndex = 578;
            this.chkAnnuler.Text = "Annuler";
            this.chkAnnuler.UseVisualStyleBackColor = true;
            this.chkAnnuler.Visible = false;
            // 
            // GridSelect
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridSelect.DisplayLayout.Appearance = appearance1;
            this.GridSelect.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridSelect.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridSelect.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSelect.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSelect.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridSelect.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridSelect.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridSelect.DisplayLayout.MaxColScrollRegions = 1;
            this.GridSelect.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridSelect.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridSelect.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridSelect.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridSelect.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridSelect.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridSelect.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridSelect.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridSelect.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridSelect.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridSelect.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridSelect.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridSelect.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridSelect.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridSelect.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridSelect.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridSelect.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridSelect.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridSelect.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridSelect.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridSelect.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridSelect.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridSelect.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridSelect.Location = new System.Drawing.Point(6, 48);
            this.GridSelect.Name = "GridSelect";
            this.GridSelect.Size = new System.Drawing.Size(685, 416);
            this.GridSelect.TabIndex = 579;
            this.GridSelect.Text = "ultraGrid1";
            this.GridSelect.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridSelect_AfterCellUpdate);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(607, 9);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(85, 35);
            this.Command1.TabIndex = 573;
            this.Command1.Text = "      Valider.";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(518, 9);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(85, 35);
            this.cmdAnnuler.TabIndex = 572;
            this.cmdAnnuler.Text = "Annuler";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdVisu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdVisu.ForeColor = System.Drawing.Color.White;
            this.cmdVisu.Image = global::Axe_interDT.Properties.Resources.Eye_16x16;
            this.cmdVisu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisu.Location = new System.Drawing.Point(429, 9);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(85, 35);
            this.cmdVisu.TabIndex = 571;
            this.cmdVisu.Text = "     Visu";
            this.cmdVisu.UseVisualStyleBackColor = false;
            this.cmdVisu.Click += new System.EventHandler(this.cmdVisu_Click);
            // 
            // txtOrder
            // 
            this.txtOrder.AccAcceptNumbersOnly = false;
            this.txtOrder.AccAllowComma = false;
            this.txtOrder.AccBackgroundColor = System.Drawing.Color.White;
            this.txtOrder.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtOrder.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtOrder.AccHidenValue = "";
            this.txtOrder.AccNotAllowedChars = null;
            this.txtOrder.AccReadOnly = false;
            this.txtOrder.AccReadOnlyAllowDelete = false;
            this.txtOrder.AccRequired = false;
            this.txtOrder.BackColor = System.Drawing.Color.White;
            this.txtOrder.CustomBackColor = System.Drawing.Color.White;
            this.txtOrder.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtOrder.ForeColor = System.Drawing.Color.Black;
            this.txtOrder.Location = new System.Drawing.Point(15, 130);
            this.txtOrder.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrder.MaxLength = 32767;
            this.txtOrder.Multiline = false;
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.ReadOnly = false;
            this.txtOrder.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtOrder.Size = new System.Drawing.Size(20, 27);
            this.txtOrder.TabIndex = 577;
            this.txtOrder.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtOrder.UseSystemPasswordChar = false;
            this.txtOrder.Visible = false;
            // 
            // txtWhere
            // 
            this.txtWhere.AccAcceptNumbersOnly = false;
            this.txtWhere.AccAllowComma = false;
            this.txtWhere.AccBackgroundColor = System.Drawing.Color.White;
            this.txtWhere.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtWhere.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtWhere.AccHidenValue = "";
            this.txtWhere.AccNotAllowedChars = null;
            this.txtWhere.AccReadOnly = false;
            this.txtWhere.AccReadOnlyAllowDelete = false;
            this.txtWhere.AccRequired = false;
            this.txtWhere.BackColor = System.Drawing.Color.White;
            this.txtWhere.CustomBackColor = System.Drawing.Color.White;
            this.txtWhere.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtWhere.ForeColor = System.Drawing.Color.Black;
            this.txtWhere.Location = new System.Drawing.Point(11, 104);
            this.txtWhere.Margin = new System.Windows.Forms.Padding(2);
            this.txtWhere.MaxLength = 32767;
            this.txtWhere.Multiline = false;
            this.txtWhere.Name = "txtWhere";
            this.txtWhere.ReadOnly = false;
            this.txtWhere.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtWhere.Size = new System.Drawing.Size(20, 27);
            this.txtWhere.TabIndex = 576;
            this.txtWhere.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtWhere.UseSystemPasswordChar = false;
            this.txtWhere.Visible = false;
            // 
            // txtSelect
            // 
            this.txtSelect.AccAcceptNumbersOnly = false;
            this.txtSelect.AccAllowComma = false;
            this.txtSelect.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelect.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelect.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSelect.AccHidenValue = "";
            this.txtSelect.AccNotAllowedChars = null;
            this.txtSelect.AccReadOnly = false;
            this.txtSelect.AccReadOnlyAllowDelete = false;
            this.txtSelect.AccRequired = false;
            this.txtSelect.BackColor = System.Drawing.Color.White;
            this.txtSelect.CustomBackColor = System.Drawing.Color.White;
            this.txtSelect.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSelect.ForeColor = System.Drawing.Color.Black;
            this.txtSelect.Location = new System.Drawing.Point(26, 78);
            this.txtSelect.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelect.MaxLength = 32767;
            this.txtSelect.Multiline = false;
            this.txtSelect.Name = "txtSelect";
            this.txtSelect.ReadOnly = false;
            this.txtSelect.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelect.Size = new System.Drawing.Size(20, 27);
            this.txtSelect.TabIndex = 575;
            this.txtSelect.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelect.UseSystemPasswordChar = false;
            this.txtSelect.Visible = false;
            // 
            // txtFiltreWhere
            // 
            this.txtFiltreWhere.AccAcceptNumbersOnly = false;
            this.txtFiltreWhere.AccAllowComma = false;
            this.txtFiltreWhere.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFiltreWhere.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFiltreWhere.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFiltreWhere.AccHidenValue = "";
            this.txtFiltreWhere.AccNotAllowedChars = null;
            this.txtFiltreWhere.AccReadOnly = false;
            this.txtFiltreWhere.AccReadOnlyAllowDelete = false;
            this.txtFiltreWhere.AccRequired = false;
            this.txtFiltreWhere.BackColor = System.Drawing.Color.White;
            this.txtFiltreWhere.CustomBackColor = System.Drawing.Color.White;
            this.txtFiltreWhere.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtFiltreWhere.ForeColor = System.Drawing.Color.Black;
            this.txtFiltreWhere.Location = new System.Drawing.Point(15, 52);
            this.txtFiltreWhere.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiltreWhere.MaxLength = 32767;
            this.txtFiltreWhere.Multiline = false;
            this.txtFiltreWhere.Name = "txtFiltreWhere";
            this.txtFiltreWhere.ReadOnly = false;
            this.txtFiltreWhere.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFiltreWhere.Size = new System.Drawing.Size(20, 27);
            this.txtFiltreWhere.TabIndex = 574;
            this.txtFiltreWhere.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFiltreWhere.UseSystemPasswordChar = false;
            this.txtFiltreWhere.Visible = false;
            // 
            // txtFiltre
            // 
            this.txtFiltre.AccAcceptNumbersOnly = false;
            this.txtFiltre.AccAllowComma = false;
            this.txtFiltre.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFiltre.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFiltre.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFiltre.AccHidenValue = "";
            this.txtFiltre.AccNotAllowedChars = null;
            this.txtFiltre.AccReadOnly = false;
            this.txtFiltre.AccReadOnlyAllowDelete = false;
            this.txtFiltre.AccRequired = false;
            this.txtFiltre.BackColor = System.Drawing.Color.White;
            this.txtFiltre.CustomBackColor = System.Drawing.Color.White;
            this.txtFiltre.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFiltre.ForeColor = System.Drawing.Color.Black;
            this.txtFiltre.Location = new System.Drawing.Point(285, 16);
            this.txtFiltre.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiltre.MaxLength = 32767;
            this.txtFiltre.Multiline = false;
            this.txtFiltre.Name = "txtFiltre";
            this.txtFiltre.ReadOnly = false;
            this.txtFiltre.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFiltre.Size = new System.Drawing.Size(140, 27);
            this.txtFiltre.TabIndex = 1;
            this.txtFiltre.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFiltre.UseSystemPasswordChar = false;
            this.txtFiltre.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltre_KeyPress);
            // 
            // txtFiltreCode
            // 
            this.txtFiltreCode.AccAcceptNumbersOnly = false;
            this.txtFiltreCode.AccAllowComma = false;
            this.txtFiltreCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFiltreCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFiltreCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFiltreCode.AccHidenValue = "";
            this.txtFiltreCode.AccNotAllowedChars = null;
            this.txtFiltreCode.AccReadOnly = false;
            this.txtFiltreCode.AccReadOnlyAllowDelete = false;
            this.txtFiltreCode.AccRequired = false;
            this.txtFiltreCode.BackColor = System.Drawing.Color.White;
            this.txtFiltreCode.CustomBackColor = System.Drawing.Color.White;
            this.txtFiltreCode.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFiltreCode.ForeColor = System.Drawing.Color.Black;
            this.txtFiltreCode.Location = new System.Drawing.Point(94, 16);
            this.txtFiltreCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiltreCode.MaxLength = 32767;
            this.txtFiltreCode.Multiline = false;
            this.txtFiltreCode.Name = "txtFiltreCode";
            this.txtFiltreCode.ReadOnly = false;
            this.txtFiltreCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFiltreCode.Size = new System.Drawing.Size(140, 27);
            this.txtFiltreCode.TabIndex = 0;
            this.txtFiltreCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFiltreCode.UseSystemPasswordChar = false;
            this.txtFiltreCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltreCode_KeyPress);
            // 
            // frmSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(697, 468);
            this.Controls.Add(this.GridSelect);
            this.Controls.Add(this.chkAnnuler);
            this.Controls.Add(this.txtOrder);
            this.Controls.Add(this.txtWhere);
            this.Controls.Add(this.txtSelect);
            this.Controls.Add(this.txtFiltreWhere);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.cmdVisu);
            this.Controls.Add(this.txtFiltre);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFiltreCode);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(713, 507);
            this.MinimumSize = new System.Drawing.Size(713, 507);
            this.Name = "frmSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Sélection";
            this.Activated += new System.EventHandler(this.frmSelect_Activated);
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmSelect_FormClosed);
            this.Load += new System.EventHandler(this.frmSelect_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridSelect)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtFiltreCode;
        public iTalk.iTalk_TextBox_Small2 txtFiltre;
        public System.Windows.Forms.Button cmdVisu;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button Command1;
        public iTalk.iTalk_TextBox_Small2 txtFiltreWhere;
        public iTalk.iTalk_TextBox_Small2 txtSelect;
        public iTalk.iTalk_TextBox_Small2 txtWhere;
        public iTalk.iTalk_TextBox_Small2 txtOrder;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.CheckBox chkAnnuler;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridSelect;
    }
}