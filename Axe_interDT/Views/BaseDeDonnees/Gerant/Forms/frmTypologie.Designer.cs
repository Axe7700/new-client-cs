﻿namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    partial class frmTypologie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.gridTypologie = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.chkSelect = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.gridTypologie)).BeginInit();
            this.SuspendLayout();
            // 
            // gridTypologie
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridTypologie.DisplayLayout.Appearance = appearance1;
            this.gridTypologie.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ExtendLastColumn;
            this.gridTypologie.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridTypologie.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.gridTypologie.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridTypologie.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.gridTypologie.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridTypologie.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.gridTypologie.DisplayLayout.MaxColScrollRegions = 1;
            this.gridTypologie.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridTypologie.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridTypologie.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.gridTypologie.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.gridTypologie.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.gridTypologie.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.gridTypologie.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridTypologie.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.gridTypologie.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridTypologie.DisplayLayout.Override.CellAppearance = appearance8;
            this.gridTypologie.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridTypologie.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.gridTypologie.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.gridTypologie.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.gridTypologie.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridTypologie.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.gridTypologie.DisplayLayout.Override.RowAppearance = appearance11;
            this.gridTypologie.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.gridTypologie.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridTypologie.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.gridTypologie.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridTypologie.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridTypologie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridTypologie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.gridTypologie.Location = new System.Drawing.Point(0, 0);
            this.gridTypologie.Name = "gridTypologie";
            this.gridTypologie.Size = new System.Drawing.Size(498, 211);
            this.gridTypologie.TabIndex = 412;
            this.gridTypologie.Text = "ultraGrid1";
            this.gridTypologie.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.gridTypologie_InitializeLayout);
            this.gridTypologie.AfterRowsDeleted += new System.EventHandler(this.gridTypologie_AfterRowsDeleted);
            this.gridTypologie.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.gridTypologie_AfterRowUpdate);
            this.gridTypologie.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.gridTypologie_BeforeRowsDeleted);
            this.gridTypologie.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.gridTypologie_DoubleClickRow);
            this.gridTypologie.Leave += new System.EventHandler(this.gridTypologie_Leave);
            // 
            // chkSelect
            // 
            this.chkSelect.AutoSize = true;
            this.chkSelect.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.chkSelect.Location = new System.Drawing.Point(4, 190);
            this.chkSelect.Margin = new System.Windows.Forms.Padding(6, 3, 3, 3);
            this.chkSelect.Name = "chkSelect";
            this.chkSelect.Size = new System.Drawing.Size(171, 21);
            this.chkSelect.TabIndex = 570;
            this.chkSelect.Text = "Typologie sélectionnée";
            this.chkSelect.UseVisualStyleBackColor = true;
            this.chkSelect.Visible = false;
            // 
            // frmTypologie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(498, 211);
            this.Controls.Add(this.chkSelect);
            this.Controls.Add(this.gridTypologie);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(514, 383);
            this.MinimumSize = new System.Drawing.Size(514, 250);
            this.Name = "frmTypologie";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion de la typologie d\'un gérant";
            this.Load += new System.EventHandler(this.frmTypologie_Load);
            ((System.ComponentModel.ISupportInitialize)(this.gridTypologie)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.CheckBox chkSelect;
        public Infragistics.Win.UltraWinGrid.UltraGrid gridTypologie;
    }
}