﻿namespace Axe_interDT.Views.BaseDeDonnees.Gerant.Forms
{
    partial class frmVisu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.chkAnnuler = new System.Windows.Forms.CheckBox();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.GridVisu = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtOrder = new iTalk.iTalk_TextBox_Small2();
            this.txtWhere = new iTalk.iTalk_TextBox_Small2();
            this.txtSelect = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridVisu)).BeginInit();
            this.SuspendLayout();
            // 
            // chkAnnuler
            // 
            this.chkAnnuler.AutoSize = true;
            this.chkAnnuler.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.chkAnnuler.Location = new System.Drawing.Point(12, 12);
            this.chkAnnuler.Name = "chkAnnuler";
            this.chkAnnuler.Size = new System.Drawing.Size(74, 21);
            this.chkAnnuler.TabIndex = 570;
            this.chkAnnuler.Text = "Annuler";
            this.chkAnnuler.UseVisualStyleBackColor = true;
            this.chkAnnuler.Visible = false;
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(521, 5);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(85, 35);
            this.cmdAnnuler.TabIndex = 574;
            this.cmdAnnuler.Text = "Annuler";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(610, 5);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(85, 35);
            this.Command1.TabIndex = 575;
            this.Command1.Text = "      Valider.";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // GridVisu
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridVisu.DisplayLayout.Appearance = appearance1;
            this.GridVisu.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridVisu.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridVisu.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVisu.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVisu.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridVisu.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVisu.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridVisu.DisplayLayout.MaxColScrollRegions = 1;
            this.GridVisu.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridVisu.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridVisu.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridVisu.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridVisu.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridVisu.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridVisu.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridVisu.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridVisu.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridVisu.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVisu.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridVisu.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridVisu.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridVisu.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridVisu.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridVisu.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridVisu.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridVisu.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridVisu.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridVisu.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridVisu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridVisu.Location = new System.Drawing.Point(7, 45);
            this.GridVisu.Name = "GridVisu";
            this.GridVisu.Size = new System.Drawing.Size(688, 270);
            this.GridVisu.TabIndex = 576;
            this.GridVisu.Text = "ultraGrid1";
            // 
            // txtOrder
            // 
            this.txtOrder.AccAcceptNumbersOnly = false;
            this.txtOrder.AccAllowComma = false;
            this.txtOrder.AccBackgroundColor = System.Drawing.Color.White;
            this.txtOrder.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtOrder.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtOrder.AccHidenValue = "";
            this.txtOrder.AccNotAllowedChars = null;
            this.txtOrder.AccReadOnly = false;
            this.txtOrder.AccReadOnlyAllowDelete = false;
            this.txtOrder.AccRequired = false;
            this.txtOrder.BackColor = System.Drawing.Color.White;
            this.txtOrder.CustomBackColor = System.Drawing.Color.White;
            this.txtOrder.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtOrder.ForeColor = System.Drawing.Color.Black;
            this.txtOrder.Location = new System.Drawing.Point(185, 12);
            this.txtOrder.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrder.MaxLength = 32767;
            this.txtOrder.Multiline = false;
            this.txtOrder.Name = "txtOrder";
            this.txtOrder.ReadOnly = false;
            this.txtOrder.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtOrder.Size = new System.Drawing.Size(33, 27);
            this.txtOrder.TabIndex = 573;
            this.txtOrder.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtOrder.UseSystemPasswordChar = false;
            this.txtOrder.Visible = false;
            // 
            // txtWhere
            // 
            this.txtWhere.AccAcceptNumbersOnly = false;
            this.txtWhere.AccAllowComma = false;
            this.txtWhere.AccBackgroundColor = System.Drawing.Color.White;
            this.txtWhere.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtWhere.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtWhere.AccHidenValue = "";
            this.txtWhere.AccNotAllowedChars = null;
            this.txtWhere.AccReadOnly = false;
            this.txtWhere.AccReadOnlyAllowDelete = false;
            this.txtWhere.AccRequired = false;
            this.txtWhere.BackColor = System.Drawing.Color.White;
            this.txtWhere.CustomBackColor = System.Drawing.Color.White;
            this.txtWhere.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtWhere.ForeColor = System.Drawing.Color.Black;
            this.txtWhere.Location = new System.Drawing.Point(148, 11);
            this.txtWhere.Margin = new System.Windows.Forms.Padding(2);
            this.txtWhere.MaxLength = 32767;
            this.txtWhere.Multiline = false;
            this.txtWhere.Name = "txtWhere";
            this.txtWhere.ReadOnly = false;
            this.txtWhere.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtWhere.Size = new System.Drawing.Size(33, 27);
            this.txtWhere.TabIndex = 572;
            this.txtWhere.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtWhere.UseSystemPasswordChar = false;
            this.txtWhere.Visible = false;
            // 
            // txtSelect
            // 
            this.txtSelect.AccAcceptNumbersOnly = false;
            this.txtSelect.AccAllowComma = false;
            this.txtSelect.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelect.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelect.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSelect.AccHidenValue = "";
            this.txtSelect.AccNotAllowedChars = null;
            this.txtSelect.AccReadOnly = false;
            this.txtSelect.AccReadOnlyAllowDelete = false;
            this.txtSelect.AccRequired = false;
            this.txtSelect.BackColor = System.Drawing.Color.White;
            this.txtSelect.CustomBackColor = System.Drawing.Color.White;
            this.txtSelect.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSelect.ForeColor = System.Drawing.Color.Black;
            this.txtSelect.Location = new System.Drawing.Point(111, 11);
            this.txtSelect.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelect.MaxLength = 32767;
            this.txtSelect.Multiline = false;
            this.txtSelect.Name = "txtSelect";
            this.txtSelect.ReadOnly = false;
            this.txtSelect.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelect.Size = new System.Drawing.Size(33, 27);
            this.txtSelect.TabIndex = 571;
            this.txtSelect.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelect.UseSystemPasswordChar = false;
            this.txtSelect.Visible = false;
            // 
            // frmVisu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(700, 321);
            this.Controls.Add(this.GridVisu);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.txtOrder);
            this.Controls.Add(this.txtWhere);
            this.Controls.Add(this.txtSelect);
            this.Controls.Add(this.chkAnnuler);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(716, 360);
            this.MinimumSize = new System.Drawing.Size(716, 360);
            this.Name = "frmVisu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Visualisation de la séléction";
            this.Activated += new System.EventHandler(this.frmVisu_Activated);
            this.Load += new System.EventHandler(this.frmVisu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridVisu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkAnnuler;
        public iTalk.iTalk_TextBox_Small2 txtSelect;
        public iTalk.iTalk_TextBox_Small2 txtWhere;
        public iTalk.iTalk_TextBox_Small2 txtOrder;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button Command1;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridVisu;
    }
}