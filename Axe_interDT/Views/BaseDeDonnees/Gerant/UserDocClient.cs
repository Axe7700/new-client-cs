﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Gerant.Forms;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant
{
    public partial class UserDocClient : UserControl
    {

        /// <summary>
        /// The Menu
        /// </summary>

        DataTable rsCorrespondants;
        private ModAdo modAdorsCorrespondants;
        DataTable rsIntervenant;
        private ModAdo modAdorsIntervenant;
        DataTable rsintervention;
        private ModAdo modAdorsintervention;
        DataTable rsDevis;
        private ModAdo modAdorsDevis;
        DataTable rsImmeuble;
        private ModAdo modAdorsImmeuble;
        DataTable rsCLRE;
        private ModAdo modAdorsCLRE;
        DataTable rsGdpCombo;
        private ModAdo modAdorsGdpCombo;
        private DataTable DATA1;
        private SqlDataAdapter SDADATA1;
        private SqlCommandBuilder SCBDATA1;

        private DataTable Adodc17;
        private SqlDataAdapter SDAAdodc17;
        private SqlCommandBuilder SCBAdodc17;

        private DataTable Adodc20;
        private SqlDataAdapter SDAAdodc20;
        private SqlCommandBuilder SCBAdodc20;

        private DataTable Adodc16;
        private SqlDataAdapter SDAAdodc16;
        private SqlCommandBuilder SCBAdodc16;

        private DataTable Adodc19;
        private SqlDataAdapter SDAAdodc19;
        private SqlCommandBuilder SCBAdodc19;

        private DataTable Adodc18;
        private SqlDataAdapter SDAAdodc18;
        private SqlCommandBuilder SCBAdodc18;

        private DataTable Adodc15;
        private SqlDataAdapter SDAAdodc15;
        private SqlCommandBuilder SCBAdodc15;

        string CodeFicheAppelante;

        string BisStrRequete;
        string strdatetemp;
        string SQL;
        string ok;
        string[] tabstrCompte;

        DataTable adors;
        private SqlDataAdapter SDAadors;
        private SqlCommandBuilder SCBadors;

        int intIndex;

        DateTime dateTemp;

        bool blModif;
        bool blnAjout;
        bool bLoadChange;
        bool boolAutorise;
        public UserDocClient()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmd0_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            int Index = Convert.ToInt32(btn.Tag);
            string sCode = null;


            try
            {
                switch (Index)
                {

                    case 0:
                        //=== recherche du code groupe.

                        string requete = "SELECT     CRCL_Code AS \"Code\", CRCL_RaisonSocial AS \"Raison Sociale\", CRCL_Adresse1 AS \"Adresse\", CRCL_CP AS \"Code Postal\", CRCL_Ville As \"Ville\"  FROM         CRCL_GroupeClient";
                        string where_order = "";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un groupe" };
                        fg.SetValues(new Dictionary<string, string> { { "CRCL_Code", txt0.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                            txt0.Text = sCode;
                            fg.Dispose(); fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                sCode = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                                txt0.Text = sCode;
                                fg.Dispose(); fg.Close();
                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                        break;

                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmd_Click erreurGroupe");
            }

        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocClient_Load(object sender, EventArgs e)
        {
            fc_Droit();
            View.Theme.Theme.MainForm.FormClosing += (se, ev) =>
            {
                //VisibleExecuted = false;
                if (!string.IsNullOrEmpty(txtCode1.Text))
                {
                    // stocke la position de la fiche client
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                }
            };
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fc_Droit()
        {
            string sSQL = null;
            string sDroitcompt = null;

            try
            {
                if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                {

                    //===  controlesi l'utilisateur a droit de saisir le commercial..
                    using (var tmpModAdo = new ModAdo())
                        sDroitcompt = tmpModAdo.fc_ADOlibelle("SELECT AUT_Nom From AUT_Autorisation " + " WHERE  (AUT_Objet = '" + StdSQLchaine.gFr_DoublerQuote(Variable.cSaisieCommercial) + "') AND (AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')" + " AND (AUT_Formulaire = '" + StdSQLchaine.gFr_DoublerQuote(Variable.cUserDocClient) + "') ");

                    if (!string.IsNullOrEmpty(sDroitcompt))
                    {
                        SSOleDBCmbCommercial.Enabled = true;
                    }
                    else
                    {
                        SSOleDBCmbCommercial.Enabled = false;
                    }

                }
                else
                {
                    SSOleDBCmbCommercial.Enabled = true;

                }

                fc_VisibleLong();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_Droit");
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fc_VisibleLong()
        {
            try
            {
                if (General.sAfficheCodeLong == "1")
                {
                    txt3.Visible = true;
                    Label43.Visible = true;

                }
                else
                {
                    txt3.Visible = false;
                    Label43.Visible = false;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name);
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label5744_Click(object sender, EventArgs e)
        {
            var btn = sender as Label;
            int Index = Convert.ToInt32(btn.Tag);
            switch (Index)
            {
                case 44:
                    if (!string.IsNullOrEmpty(txt0.Text))
                    {
                        General.saveInReg(Variable.cUserDocGroupe, "txtCRCL_Code", txt0.Text);
                        View.Theme.Theme.Navigate(typeof(Groupe.UserDocGroupe));
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, il n'y a pas de groupe associé à ce client.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    break;

            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDevis_Click(object sender, EventArgs e)
        {
            try
            {
                string sSQL = null;
                string sWhere = null;

                //  sSql = "SELECT DevisEnTete.DateAcceptation, DevisEnTete.CodeImmeuble," _
                //& " DevisEnTete.NumeroDevis, DevisEnTete.TitreDevis, DevisEnTete.CodeEtat," _
                //& " TypeCodeEtat.LibelleCodeEtat, DevisEnTete.CodeDeviseur, Personnel.Nom" _
                //& " FROM (DevisEnTete LEFT JOIN Personnel ON DevisEnTete.CodeDeviseur =" _
                //& " Personnel.Matricule) LEFT JOIN TypeCodeEtat ON DevisEnTete.CodeEtat" _
                //& " = TypeCodeEtat.CodeEtat"

                //    sSQl = "SELECT DevisEnTete.DateAcceptation, DevisEnTete.CodeImmeuble," _
                //'        & " DevisEnTete.NumeroDevis, DevisEnTete.TitreDevis, DevisEnTete.CodeEtat, " _
                //'        & " TypeCodeEtat.LibelleCodeEtat, DevisEnTete.CodeDeviseur, Personnel.Nom" _
                //'        & " FROM ((DevisEnTete LEFT JOIN Personnel ON DevisEnTete.CodeDeviseur " _
                //'        & " = Personnel.Matricule) LEFT JOIN TypeCodeEtat ON DevisEnTete.CodeEtat" _
                //'        & " = TypeCodeEtat.CodeEtat) INNER JOIN Immeuble ON DevisEnTete.CodeImmeuble" _
                //'        & " = Immeuble.CodeImmeuble"

                sSQL = "SELECT DevisEntete.CodeImmeuble,DevisEntete.NumeroDevis,DevisEntete.CodeEtat,";
                sSQL = sSQL + "DevisEntete.TitreDevis,DevisEntete.DateCreation,DevisEntete.CodeDeviseur,";
                sSQL = sSQL + "Immeuble.Code1,DevisEntete.Observations,DevisEntete.CodeTitre FROM DevisEntete LEFT JOIN Immeuble ";
                sSQL = sSQL + " ON DevisEntete.CodeImmeuble = Immeuble.CodeImmeuble ";
                sSQL = sSQL + " INNER JOIN Table1 ";
                sSQL = sSQL + " ON Immeuble.Code1=Table1.Code1 ";

                sWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                if (!string.IsNullOrEmpty(cmbDevisImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisImmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " And DevisEnTete.codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisImmeuble.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(cmbDevisIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.CodeDeviseur='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisIntervenant.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and DevisEnTete.CodeDeviseur='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisIntervenant.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(cmbDevisStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisStatus.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  DevisEnTete.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisStatus.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.DateAcceptation>='" + Convert.ToDateTime(txtDevisDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  DevisEnTete.DateAcceptation>='" + Convert.ToDateTime(txtDevisDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.DateAcceptation<='" + Convert.ToDateTime(txtDevisAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  DevisEnTete.DateAcceptation<='" + Convert.ToDateTime(txtDevisAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txt1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where DevisEnTete.TitreDevis like '%" + StdSQLchaine.gFr_DoublerQuote(txt1.Text) + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  DevisEnTete.TitreDevis like '%" + StdSQLchaine.gFr_DoublerQuote(txt1.Text) + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(txt101.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.NoEnregistrement='" + txt101.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + txt101.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(sWhere))
                {
                    sSQL = sSQL + sWhere;
                }

                modAdorsDevis = new ModAdo();
                rsDevis = modAdorsDevis.fc_OpenRecordSet(sSQL);
                this.ssDevis.DataSource = rsDevis;
                string noDevis = General.getFrmReg(Variable.cUserDocClient, "NumeroDevis", "");
                var row = ssDevis.Rows.Where(l => l.Cells["NumeroDevis"].Value.ToString() == noDevis).FirstOrDefault();
                if (row != null)
                {
                    row.Activate();
                    row.Selected = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbDevis_Click");
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDevisImmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse," + " Immeuble.CodePostal, Immeuble.Ville" + " From Immeuble" + " WHERE Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
            sheridan.InitialiseCombo(cmbDevisImmeuble, General.sSQL, "codeImmeuble");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDevisIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom" + "  FROM Personnel INNER JOIN SpecifQualif ON" + " Personnel.CodeQualif = SpecifQualif.CodeQualif " + " order by nom ";

            sheridan.InitialiseCombo(cmbDevisIntervenant, General.sSQL, "Matricule");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDevisStatus_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle" + " FROM DevisCodeEtat";
            sheridan.InitialiseCombo(cmbDevisStatus, General.sSQL, "Code");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieA_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Code, Libelle " + " FROM Energie order by Code";

            sheridan.InitialiseCombo(cmbEnergieDe, General.sSQL, "Code");
            sheridan.InitialiseCombo(cmbEnergieA, General.sSQL, "Code");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cmbEnergieDe.Text == "XXXXXX")
            {
                cmbEnergieDe.Text = "";
                cmdVisu2.Visible = false;
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieA_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbEnergieDe.Text))
            {
                cmbEnergieDe.Text = cmbEnergieA.Text;
            }
            if (!string.IsNullOrEmpty(cmbEnergieA.Text))
            {
                txtReqEnergie.Text = "{Energie.Code}<='" + cmbEnergieA.Text + "'" + (!string.IsNullOrEmpty(cmbEnergieDe.Text) ? " AND {Energie.Code}>='" + StdSQLchaine.gFr_DoublerQuote(cmbEnergieDe.Text) + "'" : "");
            }
            else
            {
                txtReqEnergie.Text = (!string.IsNullOrEmpty(cmbEnergieDe.Text) ? "{Energie.Code}>='" + StdSQLchaine.gFr_DoublerQuote(cmbEnergieDe.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieDe_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Code, Libelle " + " FROM Energie order by Code";

            sheridan.InitialiseCombo(cmbEnergieDe, General.sSQL, "Code", false);
            sheridan.InitialiseCombo(cmbEnergieA, General.sSQL, "Code", false);
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (cmbEnergieA.Text == "XXXXXX")
            {
                cmbEnergieA.Text = "";
                cmdVisu2.Visible = false;
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergieDe_Validating(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(cmbEnergieA.Text))
            {
                cmbEnergieA.Text = cmbEnergieDe.Text;
            }
            if (!string.IsNullOrEmpty(cmbEnergieDe.Text))
            {
                txtReqEnergie.Text = "{Energie.Code}>='" + cmbEnergieDe.Text + "'" + (!string.IsNullOrEmpty(cmbEnergieA.Text) ? " AND {Energie.Code}<='" + StdSQLchaine.gFr_DoublerQuote(cmbEnergieA.Text) + "'" : "");
            }
            else
            {
                txtReqEnergie.Text = (!string.IsNullOrEmpty(cmbEnergieA.Text) ? "{Energie.Code}<='" + StdSQLchaine.gFr_DoublerQuote(cmbEnergieA.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_Validating(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Nom" + " From Personnel" + " WHERE (((Personnel.Matricule)='" + cmbIntervenant.Text + "'))";
            using (var tmpModAdo = new ModAdo())
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
            if (General.rstmp.Rows.Count > 0)
            {
                lbllibIntervenant.Text = General.nz(General.rstmp.Rows[0]["Nom"], "").ToString();
            }
            else
            {
                lbllibIntervenant.Text = "";
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            string sWhere = null;

            //sSQl = "SELECT Intervention.DateRealise, Intervention.NoIntervention," _
            //& " Intervention.codeImmeuble ,Intervention.CodeEtat, TypeCodeEtat.LibelleCodeEtat," _
            //& " Intervention.Intervenant, Personnel.Nom, Intervention.Commentaire," _
            //& " Intervention.CptREndu_INT" _
            //& " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat" _
            //& " = Intervention.CodeEtat) RIGHT JOIN Personnel ON Intervention.Intervenant" _
            //& " = Personnel.Matricule"
            //    sSQl = "SELECT Intervention.DateRealise, Intervention.NoIntervention, Intervention.CodeImmeuble," _
            //'    & " Intervention.CodeEtat, TypeCodeEtat.LibelleCodeEtat, Intervention.Intervenant," _
            //'    & " Personnel.Nom, Intervention.Commentaire, Intervention.CptREndu_INT, Intervention.Duree" _
            //'    & " FROM Immeuble INNER JOIN ((TypeCodeEtat RIGHT JOIN Intervention ON" _
            //'    & " TypeCodeEtat.CodeEtat = Intervention.CodeEtat) LEFT JOIN Personnel ON" _
            //'    & " Intervention.Intervenant = Personnel.Matricule) ON Immeuble.CodeImmeuble = Intervention.CodeImmeuble"

            General.sSQL = "SELECT Intervention.DateRealise,Intervention.Intervenant," + " Intervention.CodeImmeuble, Immeuble.CodePostal, Intervention.CodeEtat, " + " Intervention.Designation, Intervention.Commentaire,   Intervention.Article, " + " Personnel.Nom,Personnel.CSecteur , Intervention.CptREndu_INT, TypeCodeEtat.LibelleCodeEtat, Intervention.NoIntervention" + " , INT_Rapport1,Wave,Duree, Immeuble.CodeDepanneur, Intervention.INT_AnaActivite, Table1.Prioritaire,FactureManuellePied.MontantHT,Immeuble.Prioritaire AS Prior " + " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat)" + " LEFT JOIN Personnel ON Intervention.Intervenant = Personnel.Matricule INNER JOIN IMMEUBLE ON Intervention.CodeImmeuble=Immeuble.CodeImmeuble " + " INNER JOIN Table1 ON Immeuble.Code1=Table1.Code1 ";

            General.sSQL = General.sSQL + " LEFT OUTER JOIN FactureManuelleEntete ON " + " Intervention.NoFacture=FactureManuelleEntete.NoFacture " + " LEFT OUTER JOIN FactureManuellePied ON " + " FactureManuelleEntete.CleFactureManuelle=FactureManuellePied.CleFactureManuelle " + " INNER JOIN GestionStandard ON " + " Intervention.NumFicheStandard=GestionStandard.NumFicheStandard " + " LEFT OUTER JOIN DevisEntete ON " + " GestionStandard.NoDevis=DevisEntete.NumeroDevis ";

            sWhere = " WHERE Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
            if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " And Intervention.codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                }
            }


            if (!string.IsNullOrEmpty(cmbArticle.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Article= '" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.Article= '" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'";
                }
            }

            if (!string.IsNullOrEmpty(cmbIntervenant.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " and Intervention.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                }
            }
            if (!string.IsNullOrEmpty(cmbStatus.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtinterDe.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.daterealise>='" + Convert.ToDateTime(txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.daterealise>='" + Convert.ToDateTime(txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtIntereAu.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.daterealise<='" + Convert.ToDateTime(txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.daterealise<='" + Convert.ToDateTime(txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txt4.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Commentaire like '%" + StdSQLchaine.gFr_DoublerQuote(txt4.Text) + "%'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.Commentaire like '%" + StdSQLchaine.gFr_DoublerQuote(txt4.Text) + "%'";
                }
            }

            if (!string.IsNullOrEmpty(txt100.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " WHERE DevisEntete.NoEnregistrement='" + txt100.Text + "'";
                }
                else
                {
                    sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + txt100.Text + "'";
                }
            }

            if (!string.IsNullOrEmpty(sWhere))
            {
                General.sSQL = General.sSQL + sWhere;
            }
            BisStrRequete = General.sSQL;
            General.sSQL = General.sSQL + " order by DateRealise desc";
            modAdorsintervention = new ModAdo();
            rsintervention = modAdorsintervention.fc_OpenRecordSet(General.sSQL);
            this.ssIntervention.DataSource = rsintervention;
            string sNoIntervention = General.getFrmReg(Variable.cUserDocClient, "NoIntervention", "");
            var row = ssIntervention.Rows.Where(l => l.Cells["NoIntervention"].Value.ToString() == sNoIntervention).FirstOrDefault();
            if (row != null)
            {
                row.Activate();
                row.Selected = true;
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbModeTrans_TAB_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //initialise les listes déroulantes en fonction du client
            General.sSQL = "SELECT MOT_TRANSMISSION.CodeTransmission_MOT," + " MOT_TRANSMISSION.Libelle_MOT" + " FROM MOT_TRANSMISSION";

            sheridan.InitialiseCombo(cmbModeTrans_TAB, General.sSQL, "CodeTransmission_MOT");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fc_AfficheTrasmission(string sCode)
        {
            // recherche du libelle du code de reglement. renvoie true si celui ci existe.
            //si blAfficheMessage est à true affichage du message d'erreur.
            if (!string.IsNullOrEmpty(sCode))
            {
                using (var tmpModAdo = new ModAdo())
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT MOT_TRANSMISSION.CodeTransmission_MOT," + " MOT_TRANSMISSION.Libelle_MOT" + " FROM MOT_TRANSMISSION" + " where CodeTransmission_MOT='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        lblLibModeTrans_TAB.Text = General.nz(General.rstmp.Rows[0]["Libelle_MOT"], "").ToString();

                    }
                    else
                    {
                        lblLibModeTrans_TAB.Text = "";
                    }
                }
            }
            else
            {
                lblLibModeTrans_TAB.Text = "";
            }
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbModeTrans_TAB_Validating(object sender, CancelEventArgs e)
        {
            fc_AfficheTrasmission(cmbModeTrans_TAB.Text);
        }

        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypologie_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeTypologie AS Code, Designation " + " FROM TypeTypologie order by CodeTypologie";

            sheridan.InitialiseCombo(cmbTypologie, General.sSQL, "Code");
        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTypologie_Click(object sender, EventArgs e)
        {
            frmTypologie frmTypologie = new frmTypologie();
            frmTypologie.ShowDialog();
            if (frmTypologie.chkSelect.Checked)
            {
                cmbTypologie.Value = frmTypologie.gridTypologie.ActiveRow.Cells["CodeTypologie"].Value;
            }
            frmTypologie.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_Ajouter();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Ajouter()
        {
            if (blModif == true)
            {
                switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_clear();
            fc_BloqueForm("Ajouter");
            fc_InitialiseGrille();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_sauver()
        {
            string sCode = null;
            string sSQLTemp = null;
            int lMsgbox = 0;

            sCode = txtCode1.Text;
            // controle si code client saisie
            if (string.IsNullOrEmpty(sCode))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code client.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (string.IsNullOrEmpty(SSOleDBCmbCommercial.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le commercial est désormais obligatoire.");

            }
            //=== code groupe.
            if (!string.IsNullOrEmpty(txt0.Text))
            {
                sSQLTemp = "SELECT   CRCL_Code" + " From CRCL_GroupeClient" + " WHERE CRCL_Code = '" + StdSQLchaine.gFr_DoublerQuote(txt0.Text) + "'";
                using (var tmpModAdo = new ModAdo())
                    sSQLTemp = tmpModAdo.fc_ADOlibelle(sSQLTemp);

                if (string.IsNullOrEmpty(sSQLTemp))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, Ce groupe n'existe pas.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }

            //=== met un code reglement par defaut si celui-ci n'existe pas.
            fc_CtrCodeReglement();

            if (string.IsNullOrEmpty(txtSelCode1.Text))
            {
                if (FC_insert() == false)
                {
                    return;
                }
                else
                {
                    blnAjout = false;
                }
            }
            else
            {
                fc_maj(sCode);
            }

            ssGridGestionnaire.UpdateData();
            SSINTERVENANT.UpdateData();
            ssIntervention.UpdateData();
            ssDevis.UpdateData();
            GridCLRE.UpdateData();

            // debloque les controles du formulaires
            fc_DeBloqueForm();
            // creation des dossiers
            fc_CreeDossier();
            // deflage le controle de modifs
            View.Theme.Theme.AfficheAlertSucces();


            blModif = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_CtrCodeReglement()
        {

            //== met la valeur par defaut pour le code reglement
            if (string.IsNullOrEmpty(txtCodeReglement.Text))
            {
                txtCodeReglement.Text = General.sCodeReglement;
                fc_AfficheReglement(txtCodeReglement.Text);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blAfficheMessage"></param>
        /// <returns></returns>
        private bool fc_AfficheReglement(string sCode, bool blAfficheMessage = false)
        {
            bool functionReturnValue = false;
            // recherche du libelle du code de reglement. renvoie true si cekui ci existe.
            //si blAfficheMessage est à true affichage du message d'erreur.
            if (!string.IsNullOrEmpty(sCode))
            {
                using (var tmpModAdo = new ModAdo())
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT CodeReglement.Libelle  FROM CodeReglement" + " where Code='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        lblLibCodeReglement.Text = General.nz(General.rstmp.Rows[0]["Libelle"], "").ToString();
                        functionReturnValue = false;
                    }
                    else
                    {
                        lblLibCodeReglement.Text = "";
                        if (blAfficheMessage == true)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce type de réglement n'éxiste pas", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            SSTab1.SelectedTab = SSTab1.Tabs[2];
                            functionReturnValue = true;
                            txtCodeReglement.Focus();
                        }
                    }
                }
            }
            else
            {
                lblLibCodeReglement.Text = "";
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool FC_insert()
        {
            bool functionReturnValue = false;
            // creation d' un nouveau client
            DataTable rsExtranet;
            functionReturnValue = true;

            if (txtCode1.Text.Contains("'"))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisie un code client avec une apostrophe." + "\n" + "Veuillez le modifier.", "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCode1.Focus();
                return functionReturnValue;
            }

            if (txtCode1.Text.Contains(" "))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisie un code client avec une espace." + "\n" + "Veuillez le modifier.", "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCode1.Focus();
                return functionReturnValue;
            }

            SQL = "INSERT INTO Table1 ( Code1, Nom)";
            SQL = SQL + " VALUES (";
            SQL = SQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "',";
            SQL = SQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtNom.Text) + "'";
            SQL = SQL + ")";
            int xx = General.Execute(SQL);
            // debloque les onglets.
            fc_DeBloqueForm();

            using (var tmpModAdo = new ModAdo())
                //// ======================================> Tested
                if (string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT Login.Login FROM Login WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'")))
                {
                    SQL = "INSERT INTO Login ( Login,PassWord,Gerant)";
                    SQL = SQL + " VALUES (";
                    SQL = SQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "',";
                    SQL = SQL + "'" + "zao" + "',";
                    SQL = SQL + "'" + "1" + "'";
                    SQL = SQL + ")";
                    xx = General.Execute(SQL);
                    txtLogin.Text = txtCode1.Text;
                    txtMotPasse.Text = "zao";
                    txtSelCode1.Text = txtCode1.Text;
                }
                else
                {
                    using (var modAdorsExtranet = new ModAdo())
                    {
                        rsExtranet = modAdorsExtranet.fc_OpenRecordSet("SELECT Login.Login,Login.PassWord FROM Login WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");
                        if (rsExtranet.Rows.Count > 0)
                        {
                            txtLogin.Text = rsExtranet.Rows[0]["Login"] + "";
                            txtMotPasse.Text = rsExtranet.Rows[0]["Password"] + "";
                        }
                    }
                    rsExtranet = null;
                }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm(string sCommande = "")
        {
            try
            {
                //.SSTab1.Tab = 0
                SSTab1.Enabled = true;
                cmdAjouter.Enabled = true;
                CmdRechercher.Enabled = true;
                cmdSupprimer.Enabled = true;
                CmdSauver.Enabled = true;
                cmdRechercheClient.Enabled = true;
                cmdRechercheRaisonSociale.Enabled = true;
                // desactive le mode Ajout
                blnAjout = false;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_DeBloqueForm");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_MAJ_Date()
        {
            string sSQL = null;

            try
            {
                sSQL = "UPDATE    TempFECRITURE" + " Set EC_DATE = FactureManuelleEnTete.DateFacture" + " FROM         FactureManuelleEnTete INNER JOIN" + " TempFECRITURE ON FactureManuelleEnTete.NoFacture = TempFECRITURE.EC_PIECE" + " WHERE     (TempFECRITURE.JO_NUM = N'AN')";

                int xx = General.Execute(sSQL);

                sSQL = "UPDATE    TempFECRITURE" + " Set EC_DATE = FactEnTete.DateFacture" + " FROM         TempFECRITURE INNER JOIN" + " FactEnTete ON TempFECRITURE.EC_PIECE = FactEnTete.NoFacture" + " WHERE     (TempFECRITURE.JO_NUM = N'AN')";

                xx = General.Execute(sSQL);

                sSQL = "UPDATE    TempFECRITURE" + " SET              EC_DATE = FODE_FactFodEnTete.FODE_DatePiece " + " FROM         FODE_FactFodEnTete INNER JOIN " + " TempFECRITURE ON FODE_FactFodEnTete.FODE_Nofacture = TempFECRITURE.EC_PIECE" + " WHERE     (TempFECRITURE.JO_NUM = N'AN')";

                xx = General.Execute(sSQL);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_MAJ_Date");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_maj(string sCode)
        {
            // modification des données du client.
            try
            {
                SQL = "UPDATE Table1 SET ";
                //    SQL = SQL & "Table1.Code1 ='" & gFr_DoublerQuote(.txtCode1) & "',"
                SQL = SQL + " Table1.Nom ='" + StdSQLchaine.gFr_DoublerQuote(txtNom.Text) + "',";
                SQL = SQL + " Table1.Adresse1 ='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse1.Text) + "',";
                SQL = SQL + " Table1.Adresse2 ='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse2.Text) + "',";
                SQL = SQL + " Table1.Codepostal ='" + StdSQLchaine.gFr_DoublerQuote(txtCodePostal.Text) + "',";
                SQL = SQL + " Table1.Ville ='" + StdSQLchaine.gFr_DoublerQuote(txtVille.Text) + "',";
                SQL = SQL + " Table1.Fax ='" + StdSQLchaine.gFr_DoublerQuote(txtFax.Text) + "',";
                SQL = SQL + " Table1.eMail ='" + StdSQLchaine.gFr_DoublerQuote(txtEmail.Text) + "',";
                SQL = SQL + " Table1.Observations ='" + StdSQLchaine.gFr_DoublerQuote(txtObservations.Text) + "',";
                SQL = SQL + " Table1.TransmissionBI_tab ='" + General.nz(chkTransmissionBI_TAB.CheckState == CheckState.Checked ? 1 : 0, 0) + "',";
                SQL = SQL + " Table1.ModeTRans_TAB ='" + StdSQLchaine.gFr_DoublerQuote(cmbModeTrans_TAB.Text) + "',";
                SQL = SQL + " Table1.CodeReglement='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeReglement.Text), "") + "',";
                SQL = SQL + " Table1.OSoblig_TAB ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkOSOblig_TAB.Checked ? "1" : "0"), 0) + "',";
                SQL = SQL + " Table1.EditRel_TAB ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkEditRel_TAB.Checked ? "1" : "0"), 0) + "',";
                //SQL = SQL + " Table1.RemApplicable_TAB ='" + StdSQLchaine.gFr_DoublerQuote(txtRemApplicable_TAB.Text) + "',";
                SQL = SQL + " Table1.RemApplicable_TAB ='" + StdSQLchaine.gFr_DoublerQuote(txt5.Text) + "',";
                // TODO : Mondir - Must Look For The Controle txtMotDePasse_TAB, It Is Hidden On VB6
                //SQL = SQL + " Table1.MotDePasse_TAB ='" + StdSQLchaine.gFr_DoublerQuote(txtMotDePasse_TAB.Text) + "',";
                SQL = SQL + " Table1.telephone ='" + StdSQLchaine.gFr_DoublerQuote(txtTelephone.Text) + "',";
                SQL = SQL + " Table1.AdresseModifiee =" + (chkAdresseModifiee.Checked ? 1 : 0) + ",";
                SQL = SQL + " Table1.CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txt0.Text) + "',";

                SQL = SQL + " Table1.FNAIM =" + (chkSyndic0.Checked ? 1 : 0) + ",";
                SQL = SQL + " Table1.CSAB =" + (chkSyndic2.Checked ? 1 : 0) + ",";
                SQL = SQL + " Table1.CNAB =" + (chkSyndic1.Checked ? 1 : 0) + ",";
                SQL = SQL + " Table1.UNIS =" + (chkSyndic3.Checked ? 1 : 0) + ",";

                SQL = SQL + " Table1.CoteAmour ='" + General.nz(cmbCoteAmour.Value, "5") + "',";
                SQL = SQL + " Table1.ListeRouge ='" + General.nz(chkListeRouge.Checked ? 1 : 0, "5") + "',";
                SQL = SQL + " Table1.Typologie ='" + General.nz(cmbTypologie.Value, "5") + "',";
                SQL = SQL + " Table1.Prioritaire =" + (chkPrioritaire.CheckState == CheckState.Checked ? "1" : "0") + ",";
                SQL = SQL + " Table1.Commercial ='" + StdSQLchaine.gFr_DoublerQuote(SSOleDBCmbCommercial.Text) + "'";
                SQL = SQL + " WHERE Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                int xx = General.Execute(SQL);

                SQL = "";
                SQL = "UPDATE Login SET ";
                SQL = SQL + " PassWord='" + StdSQLchaine.gFr_DoublerQuote(txtMotPasse.Text) + "',";
                SQL = SQL + " Gerant='" + "1" + "'";
                SQL = SQL + " WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(txtLogin.Text) + "'";
                xx = General.Execute(SQL);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " fc_maj ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_CreeDossier()
        {
            //- creation du dossier client
            try
            {
                if (!string.IsNullOrEmpty(txtCode1.Text))
                {

                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\correspondance");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\correspondance\\in");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\correspondance\\out");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\fax");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\fax\\in");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + txtCode1.Text + "\\fax\\out");

                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_CreeDossier");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_clear()
        {
            int i = 0;
            this.txtCode1.Text = "";
            this.txtSelCode1.Text = "";
            //Me.txtAncienCode = ""
            this.txtNom.Text = "";
            //Me.txtNouveauNom =""
            this.txtAdresse1.Text = "";
            this.txtAdresse2.Text = "";
            //Me.txtAdresse3 = ""
            this.txtVille.Text = "";
            this.txtCodePostal.Text = "";
            this.txtTelephone.Text = "";
            txt0.Text = "";
            txt3.Text = "";
            this.txtFax.Text = "";
            this.txtEmail.Text = "";
            this.txtCodeReglement.Text = "";
            lblLibCodeReglement.Text = "";
            //Me.txtNom1 = ""
            //Me.txtTel1 = ""
            //Me.txtNom2 = ""
            //Me.txtTel2 =""
            //Me.txtNom3 = ""
            //Me.txtTel3 = ""
            //Me.txtNomFacturation = ""
            //Me.txtAdrFacturation1 = ""
            //Me.txtAdrFacturation2 = ""
            //Me.txtAdrFacturation3 = ""
            //Me.txtVilleFacturation = ""
            //Me.txtCPFacturation = ""
            //Me.txtTelFacturation = ""
            //Me.txtFaxFacturation = ""
            //Me.txteMailFact = ""
            //Me.txtCodeReglement = ""
            //Me.txtCodeStatistique = ""
            //Me.txtCaContrat = ""
            //Me.txtCaTravaux = ""
            //Me.txtCaComb = ""
            //Me.txtCaContratN1 = ""
            //Me.txtCaTravauxN1 = ""
            //Me.txtCaCombN1 =""
            //Me.txtVisitesPeriod = ""
            //Me.txtBeArchive = ""
            //Me.txtConfrere = ""
            //Me.txtCoteAmour = ""
            //Me.txtPrioritaire = ""
            //Me.txtRemiseClassiq = """
            //Me.txtPotentiel = ""
            //Me.txtProchVisit =""
            //Me.txtDerVisit = ""
            //Me.txtHorairStand = ""
            //Me.txtHorairVisit = ""
            //Me.txtCondRegle = ""
            //Me.txtCondRelance =""
            //Me.txtDelegation = ""
            //Me.txtcommercial =""
            //Me.txtResponsable = ""
            //Me.txtAssistante = ""
            //Me.txt1AnneeRelation = ""
            this.txtObservations.Text = "";
            //Me.txtComGerant1 = ""
            //Me.txtComGerant2 = ""
            //- onglet 2
            this.chkAdresseModifiee.Checked = false;
            chkSyndic0.Checked = false;
            chkSyndic2.Checked = false;
            chkSyndic1.Checked = false;
            chkSyndic3.Checked = false;
            this.chkTransmissionBI_TAB.Checked = false;
            this.cmbModeTrans_TAB.Text = "";
            this.chkOSOblig_TAB.Checked = false;
            this.chkEditRel_TAB.Checked = false;
            this.txt5.Text = "";
            // TODO : Mondir Find This Controle txtMotDePasse_TAB, It Is Hidden In VB6
            //this.txtMotDePasse_TAB.Text = "";
            //Me.txtInformAdmin_TAB = ""
            cmdChangeAdresse.BackColor = Color.FromArgb(85, 115, 128);
            SSOleGridImmeuble.DataSource = null;

            SSOleDBCmbCommercial.Text = "";
            Label5742.Text = "";
            Label5740.Text = "";
            fc_InitialiseGrille();
            blModif = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_InitialiseGrille()
        {
            ModAdo.fc_CloseRecordset(rsCorrespondants);
            ModAdo.fc_CloseRecordset(rsIntervenant);
            ModAdo.fc_CloseRecordset(rsintervention);
            ModAdo.fc_CloseRecordset(rsDevis);


            ssGridGestionnaire.DataSource = null;
            SSINTERVENANT.DataSource = null;
            ssIntervention.DataSource = null;
            ssDevis.DataSource = null;
            GridCLRE.DataSource = null;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {
            try
            {
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                SSTab1.Enabled = false;
                cmdSupprimer.Enabled = false;
                // mode ajout
                if (sCommande.ToUpper() == "Ajouter".ToUpper())
                {
                    cmdAjouter.Enabled = true;
                    CmdRechercher.Enabled = false;
                    CmdSauver.Enabled = true;
                    cmdRechercheClient.Enabled = false;
                    cmdRechercheRaisonSociale.Enabled = false;
                    // Active le mode Ajout
                    blnAjout = true;
                }
                else
                {
                    // desactive le mode Ajout
                    blnAjout = false;
                    cmdRechercheClient.Enabled = true;
                    cmdRechercheRaisonSociale.Enabled = true;
                    CmdRechercher.Enabled = true;
                    CmdSauver.Enabled = false;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_BloqueForm");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_annuler()
        {
            if (blModif == true)
            {
                switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            ///================================> Tested
            fc_clear();
            fc_BloqueForm();


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdChangeAdresse_Click(object sender, EventArgs e)
        {
            string sqlUpdate = null;
            int nbRecAff = 0;

            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez modifier l'adresse de facturation de tous les immeubles de ce gérant. Souhaitez-vous continuer ?", "Modification de l'adresse de facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                fc_sauver();
                sqlUpdate = "UPDATE Immeuble SET ";
                sqlUpdate = sqlUpdate + "AdresseFact1_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtNom.Text) + "',";
                sqlUpdate = sqlUpdate + "AdresseFact2_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse1.Text) + "',";
                sqlUpdate = sqlUpdate + "AdresseFact3_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse2.Text) + "',";
                sqlUpdate = sqlUpdate + "CodePostal_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodePostal.Text) + "',";
                sqlUpdate = sqlUpdate + "VilleFact_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtVille.Text) + "' ";
                sqlUpdate = sqlUpdate + " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                nbRecAff = General.Execute(sqlUpdate);
                int xx = General.Execute("UPDATE Table1 SET AdresseModifiee=0 WHERE Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");
                chkAdresseModifiee.CheckState = CheckState.Unchecked;
                cmdChangeAdresse.BackColor = Color.FromArgb(85, 115, 128);
                if (nbRecAff > 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(nbRecAff + " immeubles ont été modifiés", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (nbRecAff == 1)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(nbRecAff + " immeuble a été modifié", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun immeuble a été modifié", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdChangeCommercial_Click(object sender, EventArgs e)
        {
            string sqlUpdate = null;
            int nbRecAff = 0;

            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez modifier le commercial de tous les immeubles de ce gérant." + " Souhaitez-vous continuer ?", "Modification de l'adresse de facturation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {

                fc_sauver();

                //===> Mondir le 22.03.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2326
                //===> Used this, coz we use a trigger to save changes on this table
                using (var tmpModAdo = new ModAdo())
                {
                    nbRecAff = tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Immeuble WHERE Code1 = '{StdSQLchaine.gFr_DoublerQuote(txtCode1.Text)}'").ToInt();
                }

                sqlUpdate = "UPDATE Immeuble SET ";
                sqlUpdate = sqlUpdate + " CodeCommercial='" + StdSQLchaine.gFr_DoublerQuote(SSOleDBCmbCommercial.Text) + "'";
                sqlUpdate = sqlUpdate + " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";

                //===> Mondir le 22.03.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2326
                //===> Commented to fix 
                //nbRecAff = General.Execute(sqlUpdate);
                General.Execute(sqlUpdate);


                if (nbRecAff > 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(nbRecAff + " immeubles ont été modifiés", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (nbRecAff == 1)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(nbRecAff + " immeuble a été modifié", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun immeuble a été modifié", "Mise à jour de l'adresse de facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                fc_ChargeEnregistrement(txtCode1.Text);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeEnregistrement(string sCode)
        {

            DataTable rsEcritureGerant = default(DataTable);
            SqlDataAdapter SDArsEcritureGerant;
            int i = 0;

            General.sSQL = "select * from table1 where code1='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            if (General.rstmp != null)
            {
                General.rstmp = new DataTable();
            }

            //Set rstmp = fc_OpenRecordSet(sSQl)
            using (var tmpModAdo = new ModAdo())
            {
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    this.txtCode1.Text = General.nz(General.rstmp.Rows[0]["Code1"], "").ToString();

                    this.txtSelCode1.Text = General.nz(General.rstmp.Rows[0]["Code1"], "").ToString();
                    //Me.txtAncienCode = nz(rstmp!AncienCode, "")
                    this.txtNom.Text = General.nz(General.rstmp.Rows[0]["Nom"], "").ToString();
                    //Me.txtNouveauNom = nz(rstmp!NouveauNom, "")
                    this.txtAdresse1.Text = General.nz(General.rstmp.Rows[0]["Adresse1"], "").ToString();
                    this.txtAdresse2.Text = General.nz(General.rstmp.Rows[0]["Adresse2"], "").ToString();
                    //Me.txtAdresse3 = nz(rstmp!Adresse3, "")
                    this.txtVille.Text = General.nz(General.rstmp.Rows[0]["Ville"], "").ToString();
                    this.txtCodePostal.Text = General.nz(General.rstmp.Rows[0]["CodePostal"], "").ToString();
                    this.chkAdresseModifiee.Checked = General.nz(General.rstmp.Rows[0]["AdresseModifiee"], "0").ToString() == "1";
                    chkSyndic0.Checked = General.nz(General.rstmp.Rows[0]["FNAIM"], "0").ToString() == "1";
                    chkSyndic2.Checked = General.nz(General.rstmp.Rows[0]["CSAB"], "0").ToString() == "1";
                    chkSyndic1.Checked = General.nz(General.rstmp.Rows[0]["CNAB"], "0").ToString() == "1";
                    chkSyndic3.Checked = General.nz(General.rstmp.Rows[0]["UNIS"], "0").ToString() == "1";

                    if (this.chkAdresseModifiee.Checked)
                    {
                        cmdChangeAdresse.BackColor = ColorTranslator.FromOle(0x8080ff);
                    }
                    else
                    {
                        cmdChangeAdresse.BackColor = Color.FromArgb(85, 115, 128);
                    }
                    this.txtTelephone.Text = General.nz(General.rstmp.Rows[0]["Telephone"], "").ToString();
                    txt3.Text = General.rstmp.Rows[0]["CodeLong2"] + "";
                    txt0.Text = General.nz(General.rstmp.Rows[0]["CRCL_Code"], "").ToString();

                    this.txtFax.Text = General.nz(General.rstmp.Rows[0]["fax"], "").ToString();
                    this.txtEmail.Text = General.nz(General.rstmp.Rows[0]["Email"], "").ToString();
                    //Me.txtNom1 = nz(rstmp!Nom1, "")
                    //Me.txtTel1 = nz(rstmp!Tel1, "")
                    //Me.txtNom2 = nz(rstmp!Nom2, "")
                    //Me.txtTel2 = nz(rstmp!Tel2, "")
                    //Me.txtNom3 = nz(rstmp!Nom3, "")
                    //Me.txtTel3 = nz(rstmp!Tel3, "")
                    //Me.txtNomFacturation = nz(rstmp!NomFacturation, "")
                    //Me.txtAdrFacturation1 = nz(rstmp!AdrFacturation1, "")
                    //Me.txtAdrFacturation2 = nz(rstmp!AdrFacturation2, "")
                    //Me.txtAdrFacturation3 = nz(rstmp!AdrFacturation3, "")
                    //Me.txtVilleFacturation = nz(rstmp!VilleFacturation, "")
                    //Me.txtCPFacturation = nz(rstmp!CPFacturation, "")
                    //Me.txtTelFacturation = nz(rstmp!TelFacturation, "")
                    //Me.txtFaxFacturation = nz(rstmp!FaxFacturation, "")
                    //Me.txteMailFact = nz(rstmp!eMailFact, "")
                    this.txtCodeReglement.Text = General.nz(General.rstmp.Rows[0]["CodeReglement"], "").ToString();
                    //Me.txtCodeStatistique = nz(rstmp!CodeStatistique, "")
                    //Me.txtCaContrat = nz(rstmp!CaContrat, "0.00")
                    //Me.txtCaTravaux = nz(rstmp!CaTravaux, "0.00")
                    //Me.txtCaComb = nz(rstmp!CaComb, "0.00")
                    //Me.txtCaContratN1 = nz(rstmp!CaContratN1, "0.00")
                    //Me.txtCaTravauxN1 = nz(rstmp!CaTravauxN1, "0.00")
                    //Me.txtCaCombN1 = nz(rstmp!CaCombN1, "0.00")
                    //Me.txtVisitesPeriod = nz(rstmp!VisitesPeriod, "")
                    //Me.txtBeArchive = nz(rstmp!BeArchive, "")
                    //Me.txtConfrere = nz(rstmp!Confrere, "")
                    this.cmbCoteAmour.Text = General.nz(General.rstmp.Rows[0]["CoteAmour"], "5").ToString();
                    this.chkPrioritaire.Checked = General.nz(General.rstmp.Rows[0]["Prioritaire"], "0").ToString() == "1";
                    this.chkListeRouge.Checked = General.nz(General.rstmp.Rows[0]["ListeRouge"], "0").ToString() == "1";
                    this.cmbTypologie.Value = General.nz(General.rstmp.Rows[0]["Typologie"], "0").ToString();
                    //Me.txtRemiseClassiq = nz(rstmp!RemiseClassiq, "")
                    //Me.txtPotentiel = nz(rstmp!Potentiel, "")
                    //Me.txtProchVisit = nz(rstmp!ProchVisit, "0.00")
                    //Me.txtDerVisit = nz(rstmp!DerVisit, "0.00")
                    //Me.txtHorairStand = nz(rstmp!HorairStand, "")
                    //Me.txtHorairVisit = nz(rstmp!HorairVisit, "")
                    //Me.txtCondRegle = nz(rstmp!CondRegle, "")
                    //Me.txtCondRelance = nz(rstmp!CondRelance, "")
                    //Me.txtDelegation = nz(rstmp!Delegation, "")
                    //Me.txtcommercial = nz(rstmp!commercial, "")
                    //Me.txtResponsable = nz(rstmp!Responsable, "")
                    //Me.txtAssistante = nz(rstmp!Assistante, "")
                    //Me.txt1AnneeRelation = nz(rstmp![1AnneeRelation], "")
                    this.txtObservations.Text = General.nz(General.rstmp.Rows[0]["Observations"], "").ToString();
                    //Me.txtComGerant1 = nz(rstmp!ComGerant1, "")
                    //Me.txtComGerant2 = nz(rstmp!ComGerant2, "")
                    //- onglet 2
                    this.SSOleDBCmbCommercial.Value = General.nz(General.rstmp.Rows[0]["Commercial"], "");
                    if (General.rstmp.Rows[0]["Commercial"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["Commercial"].ToString()))
                    {
                        using (var tmpModAdo2 = new ModAdo())
                        {

                            this.Label5742.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" +
                                                     this.SSOleDBCmbCommercial.Value + "'");
                            this.Label5740.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" +
                                                     this.SSOleDBCmbCommercial.Value + "'");
                        }
                    }
                    else
                    {
                        this.Label5742.Text = "";
                        this.Label5740.Text = "";
                    }
                    if (General.nz(General.rstmp.Rows[0]["TransmissionBI_tab"], "0").ToString() == "-1")
                    {
                        this.chkTransmissionBI_TAB.CheckState = CheckState.Unchecked;
                    }
                    else
                    {
                        this.chkTransmissionBI_TAB.Checked = General.nz(General.rstmp.Rows[0]["TransmissionBI_tab"], "0").ToString() == "1";
                    }


                    this.cmbModeTrans_TAB.Text = General.nz(General.rstmp.Rows[0]["ModeTRans_TAB"], "").ToString();
                    this.chkOSOblig_TAB.Checked = General.nz(General.rstmp.Rows[0]["OSoblig_TAB"], "0").ToString() == "1";
                    this.chkEditRel_TAB.Checked = General.nz(General.rstmp.Rows[0]["EditRel_TAB"], "0").ToString() == "1";
                    this.txt5.Text = General.nz(General.rstmp.Rows[0]["RemApplicable_TAB"], "").ToString();
                    // TODO : Mondir - This Controle is Hidden, Look In The VB6 - txtMotDePasse_TAB
                    //this.txtMotDePasse_TAB.Text = General.nz(General.rstmp.Rows[0]["MotDePasse_TAB"], "").ToString();
                    // Me.txtInformAdmin_TAB = nz(rstmp!InformAdmin_TAB, "")

                    fc_AfficheTrasmission(cmbModeTrans_TAB.Text);

                    fc_DeBloqueForm();

                    FC_ChargeOnglet();

                    General.sSQL = "SELECT Immeuble.CodeImmeuble ," + " Immeuble.NCompte " + " FROM Immeuble  " + " WHERE Immeuble.Code1= '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'" + " order by Immeuble.CodeImmeuble";
                    rsEcritureGerant = new DataTable();
                    SDArsEcritureGerant = new SqlDataAdapter(General.sSQL, General.adocnn);
                    SDArsEcritureGerant.Fill(rsEcritureGerant);

                    ComboComptes.DataSource = null;

                    if (rsEcritureGerant.Rows.Count > 0)
                    {
                        var SourceDB = new DataTable();
                        SourceDB.Columns.Add("Immeuble");
                        SourceDB.Columns.Add("Compte");
                        foreach (DataRow rsEcritureGerantRow in rsEcritureGerant.Rows)
                        {
                            SourceDB.Rows.Add(rsEcritureGerantRow["CodeImmeuble"], rsEcritureGerantRow["Ncompte"]);
                        }
                        ComboComptes.DataSource = SourceDB;
                        ComboComptes.ValueMember = "Compte";
                        ComboComptes.DisplayMember = "Compte";
                    }

                    rsEcritureGerant.Dispose();
                    rsEcritureGerant = null;

                    fc_ChargeLogin(txtCode1.Text);
                    fc_AfficheReglement(txtCodeReglement.Text);
                }
                else
                {
                    fc_clear();
                }

                //
                //
                // Changed By Mondir - Menu Changed - Look at ===> 
                //
                //
                //var BackButton = View.Theme.Theme.MainMenu.Controls.Cast<Control>().First(c => c.Name == "PanelItems")
                //.Controls.Cast<MainMenuItem>().First(c => c.Type == MainMenuItem.MainMenuItemtype.BackButton);

                //switch (ModParametre.sFicheAppelante)
                //{
                //    case Variable.cUserIntervention:
                //        BackButton.LabelItemName.Text = "Intervention";
                //        CodeFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
                //        BackButton.Visible = true;
                //        break;
                //    case Variable.cUserDocStandard:
                //        var menuAppel = View.Theme.Theme.MainMenu.CommunMenu.First(c => c.ItemName == "Fiche d'appel");
                //        menuAppel.Visible = false;
                //        BackButton.LabelItemName.Text = "Fiche d'appel";
                //        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocStandard, "NewVar", "");
                //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
                //        BackButton.Visible = true;
                //        break;
                //    case Variable.cUserDocDevis:
                //        BackButton.LabelItemName.Text = "Devis";
                //        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
                //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
                //        BackButton.Visible = true;
                //        break;
                //    default:
                //        BackButton.Visible = false;
                //        break;
                //}

                fc_ChargeImmeubleV2();

                blnAjout = false;
                blModif = false;
            }
        }


        private void fc_ChargeImmeubleV2()
        {

            try
            {
                SSOleGridImmeuble.DataSource = null;
                //DataTable SSOleGridImmeubleSource = new DataTable();
                //SSOleGridImmeubleSource.Columns.Add("Code Immeuble");
                //SSOleGridImmeubleSource.Columns.Add("Gestionnaire");
                //SSOleGridImmeubleSource.Columns.Add("Energie");
                //SSOleGridImmeubleSource.Columns.Add("Titre du contrat");
                //SSOleGridImmeubleSource.Columns.Add("P1", typeof(bool));
                //SSOleGridImmeubleSource.Columns.Add("P2", typeof(bool));
                //SSOleGridImmeubleSource.Columns.Add("P3", typeof(bool));
                //SSOleGridImmeubleSource.Columns.Add("P4", typeof(bool));
                //SSOleGridImmeubleSource.Columns.Add("Commercial");

                //===> Mondir le 07.06.2021, https://groupe-dt.mantishub.io/view.php?id=2479
                //===> Commented query is old
                //var req = $@"SELECT Immeuble.CodeImmeuble, Immeuble.CodeGestionnaire, Energie.Libelle AS EnergieLibelle, '' AS 'Titre Contrat', 
                //        '' AS 'P1', '' AS 'P2', '' AS 'P3', '' AS 'P4', Personnel.Nom + ' ' + Personnel.Prenom AS 'Commercial', '' AS 'Résilie' FROM Immeuble
                //        LEFT OUTER JOIN  TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble
                //        LEFT OUTER JOIN  Energie ON TechReleve.CodeEnergie1 = Energie.Code
                //        LEFT OUTER JOIN  Personnel ON Immeuble.CodeCommercial = Personnel.Matricule
                //        WHERE Immeuble.Code1='{StdSQLchaine.gFr_DoublerQuote(txtCode1.Text)}'";

                var req = $@"SELECT Immeuble.CodeImmeuble, Immeuble.CodeGestionnaire, Energie.Libelle AS EnergieLibelle, '' AS 'Titre Contrat', 
                        '' AS 'P1', '' AS 'P2', '' AS 'P3', '' AS 'P4', 'Commercial' =
						CASE
							WHEN P1.Matricule IS NOT NULL THEN P1.Nom + ' ' + P1.Prenom
							WHEN P2.Matricule IS NOT NULL THEN P2.Nom + ' ' + P2.Prenom
						END
						, '' AS 'Résilie'
						FROM Immeuble
						JOIN Table1 ON Table1.Code1 = Immeuble.Code1
                        LEFT OUTER JOIN  TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble
                        LEFT OUTER JOIN  Energie ON TechReleve.CodeEnergie1 = Energie.Code
                        LEFT OUTER JOIN  Personnel AS P1 ON Immeuble.CodeCommercial = P1.Matricule
						LEFT OUTER JOIN  Personnel AS P2 ON Table1.Commercial = P2.Matricule
                        WHERE Immeuble.Code1='{StdSQLchaine.gFr_DoublerQuote(txtCode1.Text)}'";
                //===> Fin Modif Mondir

                if (rsImmeuble == null)
                {
                    rsImmeuble = new DataTable();
                    modAdorsImmeuble = new ModAdo();
                }

                rsImmeuble = modAdorsImmeuble.fc_OpenRecordSet(req);

                if (rsImmeuble.Rows.Count > 0)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        foreach (DataRow row in rsImmeuble.Rows)
                        {
                            var codeImmeuble = row["CodeImmeuble"].ToString();

                            row["P1"] = false;
                            row["P2"] = false;
                            row["P3"] = false;
                            row["P4"] = false;

                            //Get All Contracts
                            var req2 = $"SELECT * FROM Contrat WHERE CodeImmeuble = '{codeImmeuble}' AND Avenant = 0 ORDER BY DateFin DESC";
                            var dtContrats = tmpModAdo.fc_OpenRecordSet(req2);
                            //Check if there is no contracts
                            if (dtContrats.Rows.Count == 0)
                            {
                                //No background color for thos line
                                row["Résilie"] = -1;
                            }
                            else
                            {
                                //get all active contracts
                                var dtContratActif = ModContrat.fc_GetActiveContract(codeImmeuble);
                                if (dtContratActif.Rows.Count == 0)
                                {
                                    //this means thats there is some contracts in database but not active
                                    //set the name of the last contract
                                    //this will show a red line
                                    row["Titre Contrat"] = dtContrats.Rows[0]["LibelleCont1"];
                                    row["Résilie"] = 1;
                                }
                                else
                                {
                                    //Show active contratcs
                                    //this will show a green line
                                    row["P1"] = dtContratActif.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString() == "P1");
                                    row["P2"] = dtContratActif.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString() == "P2");
                                    row["P3"] = dtContratActif.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString() == "P3");
                                    row["P4"] = dtContratActif.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString() == "P4");
                                    row["Résilie"] = 2;
                                }
                            }
                        }
                    }
                }

                SSOleGridImmeuble.DataSource = rsImmeuble;


                //===> Mondir le 14.05.2021 https://groupe-dt.mantishub.io/view.php?id=2429#c6043
                //=== compte le nombre d'immeuble par commercial.
                var sReq = "SELECT DISTINCT Immeuble.CodeCommercial, Personnel.Nom, Personnel.Prenom, COUNT(*) AS tot" + " FROM         Immeuble LEFT OUTER JOIN" +
                       " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule" + " WHERE     (Immeuble.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "')" +
                       " GROUP BY Immeuble.CodeCommercial, Personnel.Nom, Personnel.Prenom ORDER BY tot";

                var modAdorsTot = new ModAdo();
                var rsTot = modAdorsTot.fc_OpenRecordSet(sReq);

                txtTotalComm.Text = "";

                foreach (DataRow rsTotRow in rsTot.Rows)
                {

                    var stemp = Convert.ToInt32(General.nz(rsTotRow["tot"], 0)).ToString("00");

                    txtTotalComm.Text = txtTotalComm.Text + stemp + " " + rsTotRow["Nom"] + " " + rsTotRow["Prenom"] + "\n";

                }
                modAdorsTot.Dispose();
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Code1"></param>
        /// <returns></returns>
        public object fc_ChargeImmeuble(string Code1)
        {
            object functionReturnValue = null;
            string sReq = null;
            DataTable rsTot = default(DataTable);
            ModAdo modAdorsTot;
            string stemp = null;

            try
            {
                int i = 0;

                General.sSQL = "";
                General.sSQL = "SELECT DISTINCT Immeuble.CodeImmeuble, Immeuble.CodeGestionnaire,Gestionnaire.NomGestion,Contrat.NumContrat,Contrat.Avenant,Contrat.LibelleCont1,Contrat.LibelleCont2,TechReleve.CodeEnergie1,Energie.Libelle AS EnergieLibelle FROM Immeuble LEFT JOIN Gestionnaire ON Immeuble.CodeGestionnaire=Gestionnaire.CodeGestinnaire LEFT JOIN Contrat ON Immeuble.CodeImmeuble=Contrat.CodeImmeuble ";
                General.sSQL = General.sSQL + " LEFT JOIN TechReleve ON Immeuble.CodeImmeuble=TechReleve.CodeImmeuble ";
                General.sSQL = General.sSQL + " LEFT JOIN Energie ON TechReleve.CodeEnergie1=Energie.Code ";
                General.sSQL = General.sSQL + " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                General.sSQL = General.sSQL + " ORDER BY Immeuble.CodeImmeuble ";


                General.sSQL = "SELECT DISTINCT " + " Immeuble.CodeImmeuble, Immeuble.CodeGestionnaire, Gestionnaire.NomGestion," + " Contrat.NumContrat, Contrat.Avenant, Contrat.LibelleCont1," + " Contrat.LibelleCont2, TechReleve.CodeEnergie1, Energie.Libelle AS EnergieLibelle, " + " Personnel.Nom, Personnel.Prenom";
                //sSQL = sSQL & " FROM         Immeuble LEFT OUTER JOIN" _
                //& " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule LEFT OUTER JOIN" _
                //& " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire LEFT OUTER JOIN" _
                //& " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble LEFT OUTER JOIN" _
                //& " TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble LEFT OUTER JOIN" _
                //& " Energie ON TechReleve.CodeEnergie1 = Energie.Code"

                General.sSQL = General.sSQL + " FROM         Immeuble LEFT OUTER JOIN" + " Gestionnaire ON Immeuble.Code1 = Gestionnaire.Code1 AND Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire LEFT OUTER JOIN " + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule LEFT OUTER JOIN " + " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble LEFT OUTER JOIN " + " TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble LEFT OUTER JOIN " + " Energie ON TechReleve.CodeEnergie1 = Energie.Code";

                General.sSQL = General.sSQL + " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                General.sSQL = General.sSQL + " ORDER BY Immeuble.CodeImmeuble ";

                //    sSQl = "SELECT DISTINCT "
                //    sSQl = sSQl & " Immeuble.CodeImmeuble, Immeuble.CodeGestionnaire, Gestionnaire.NomGestion," _
                //'    & " Contrat.NumContrat, Contrat.Avenant, Contrat.LibelleCont1,Contrat.LibelleCont2," _
                //'    & " TechReleve.CodeEnergie1, Energie.Libelle AS EnergieLibelle" _
                //'    & " FROM Immeuble LEFT OUTER JOIN" _
                //'    & " Gestionnaire ON Immeuble.Code1 = Gestionnaire.Code1 AND" _
                //'    & " RTRIM(LTRIM(Immeuble.CodeGestionnaire))" _
                //'    & " = RTRIM(LTRIM(Gestionnaire.CodeGestinnaire)) LEFT OUTER JOIN" _
                //'    & " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble LEFT OUTER JOIN" _
                //'    & " TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble LEFT OUTER JOIN" _
                //'    & " Energie ON TechReleve.CodeEnergie1 = Energie.Code" _
                //'    & " WHERE Immeuble.Code1='" & gFr_DoublerQuote(txtCode1.Text) & "'" _
                //'    & " ORDER BY Immeuble.CodeImmeuble"


                SSOleGridImmeuble.DataSource = null;
                DataTable SSOleGridImmeubleSource = new DataTable();
                SSOleGridImmeubleSource.Columns.Add("Code Immeuble");
                SSOleGridImmeubleSource.Columns.Add("Gestionnaire");
                SSOleGridImmeubleSource.Columns.Add("Energie");
                SSOleGridImmeubleSource.Columns.Add("Titre du contrat");
                SSOleGridImmeubleSource.Columns.Add("Contrat", typeof(bool));
                SSOleGridImmeubleSource.Columns.Add("Resilie", typeof(bool));
                SSOleGridImmeubleSource.Columns.Add("Commercial");

                if (rsImmeuble == null)
                {
                    rsImmeuble = new DataTable();
                    modAdorsImmeuble = new ModAdo();
                }

                rsImmeuble = modAdorsImmeuble.fc_OpenRecordSet(General.sSQL);



                if (rsImmeuble.Rows.Count > 0)
                {
                    DataRow contratResilie = null;

                    foreach (DataRow rsImmeubleRow in rsImmeuble.Rows)
                    {

                        //===> Mondir le 07.05.2021, https://groupe-dt.mantishub.io/view.php?id=2429#c5971
                        if (rsImmeubleRow["avenant"].ToString() == "0")
                        {
                            var contratActif = new ModAdo().fc_ADOlibelle("SELECT Contrat.NumContrat FROM Contrat WHERE (Contrat.Resiliee=0 OR Contrat.Resiliee IS NULL)" +
                            $" AND Contrat.CodeImmeuble='{StdSQLchaine.gFr_DoublerQuote(rsImmeubleRow["CodeImmeuble"].ToString())}' AND Contrat.NumContrat = '{rsImmeubleRow["NumContrat"]}' " +
                            $"AND Contrat.Avenant = '{rsImmeubleRow["avenant"]}'");

                            if (!string.IsNullOrEmpty(contratActif))
                            {
                                SSOleGridImmeubleSource.Rows.Add(rsImmeubleRow["CodeImmeuble"], rsImmeubleRow["NomGestion"], rsImmeubleRow["EnergieLibelle"], rsImmeubleRow["LibelleCont1"] + " " +
                                                                    rsImmeubleRow["LibelleCont2"], true, false, rsImmeubleRow["Nom"] + " " + rsImmeubleRow["Prenom"]);
                                SSOleGridImmeuble.DataSource = SSOleGridImmeubleSource;
                            }
                            else if (contratResilie == null)
                            {
                                contratResilie = rsImmeubleRow;
                            }
                        }

                        //if (rsImmeubleRow["NumContrat"] != DBNull.Value && !string.IsNullOrEmpty(rsImmeubleRow["NumContrat"].ToString()))
                        //{

                        //    if (rsImmeubleRow["avenant"].ToString() == "0")
                        //    {
                        //===> Mondir le 06.05.2021, added AND Contrat.NumContrat = '{rsImmeubleRow["NumContrat"]}' " + $"AND Contrat.Avenant = '{rsImmeubleRow["avenant"]}' https://groupe-dt.mantishub.io/view.php?id=2429
                        //if (string.IsNullOrEmpty(new ModAdo().fc_ADOlibelle("SELECT Contrat.NumContrat FROM Contrat WHERE (Contrat.Resiliee=0 OR Contrat.Resiliee IS NULL)" +
                        //    $" AND Contrat.CodeImmeuble='{StdSQLchaine.gFr_DoublerQuote(rsImmeubleRow["CodeImmeuble"].ToString())}' AND Contrat.NumContrat = '{rsImmeubleRow["NumContrat"]}' " +
                        //    $"AND Contrat.Avenant = '{rsImmeubleRow["avenant"]}'")))
                        //{
                        //    //===> Mondir le 19.11.2020 pour corrigé le bug https://groupe-dt.mantishub.io/view.php?id=2083
                        //    //Added  rsImmeubleRow["Nom"] + " " + rsImmeubleRow["Prenom"]
                        //    SSOleGridImmeubleSource.Rows.Add(rsImmeubleRow["CodeImmeuble"].ToString(), rsImmeubleRow["NomGestion"], rsImmeubleRow["EnergieLibelle"], rsImmeubleRow["LibelleCont1"] + " " +
                        //       rsImmeubleRow["LibelleCont2"], true, true, rsImmeubleRow["Nom"] + " " + rsImmeubleRow["Prenom"]);
                        //    SSOleGridImmeuble.DataSource = SSOleGridImmeubleSource;
                        //}
                        //else
                        //{
                        //    SSOleGridImmeubleSource.Rows.Add(rsImmeubleRow["CodeImmeuble"], rsImmeubleRow["NomGestion"], rsImmeubleRow["EnergieLibelle"], rsImmeubleRow["LibelleCont1"] + " " +
                        //                                     rsImmeubleRow["LibelleCont2"], true, false, rsImmeubleRow["Nom"] + " " + rsImmeubleRow["Prenom"]);
                        //    SSOleGridImmeuble.DataSource = SSOleGridImmeubleSource;
                        //}
                        //    }
                        //}
                        //else
                        //{
                        //SSOleGridImmeubleSource.Rows.Add(rsImmeubleRow["CodeImmeuble"], rsImmeubleRow["NomGestion"],
                        //    rsImmeubleRow["EnergieLibelle"], "", false, false,
                        //    rsImmeubleRow["Nom"] + " " + rsImmeubleRow["Prenom"]);
                        //SSOleGridImmeuble.DataSource = SSOleGridImmeubleSource;
                        //}
                    }

                    //===> Mondir le 07.05.2021 https://groupe-dt.mantishub.io/view.php?id=2429#c5977
                    if (SSOleGridImmeuble.Rows.Count == 0 && contratResilie != null)
                    {
                        SSOleGridImmeubleSource.Rows.Add(contratResilie["CodeImmeuble"], contratResilie["NomGestion"], contratResilie["EnergieLibelle"], contratResilie["LibelleCont1"] + " " +
                                                                         contratResilie["LibelleCont2"], true, true, contratResilie["Nom"] + " " + contratResilie["Prenom"]);
                        SSOleGridImmeuble.DataSource = SSOleGridImmeubleSource;
                    }
                    //===> Fin Modif Mondir
                }



                modAdorsImmeuble?.Dispose();

                //=== compte le nombre d'immeuble par commercial.
                sReq = "SELECT DISTINCT Immeuble.CodeCommercial, Personnel.Nom, Personnel.Prenom, COUNT(*) AS tot" + " FROM         Immeuble LEFT OUTER JOIN" +
                       " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule" + " WHERE     (Immeuble.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "')" +
                       " GROUP BY Immeuble.CodeCommercial, Personnel.Nom, Personnel.Prenom ORDER BY tot";

                modAdorsTot = new ModAdo();
                rsTot = modAdorsTot.fc_OpenRecordSet(sReq);

                txtTotalComm.Text = "";

                foreach (DataRow rsTotRow in rsTot.Rows)
                {

                    stemp = Convert.ToInt32(General.nz(rsTotRow["tot"], 0)).ToString("00");

                    txtTotalComm.Text = txtTotalComm.Text + stemp + " " + rsTotRow["Nom"] + " " + rsTotRow["Prenom"] + "\n";

                }
                modAdorsTot.Dispose();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ChargeImmeuble");
                throw;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <returns></returns>
        public object fc_ChargeLogin(string sCode)
        {
            DataTable rslogin = default(DataTable);
            SqlDataAdapter SDArslogin;
            SqlCommandBuilder SCBrslogin;

            this.txtLogin.Text = sCode;
            Application.DoEvents();
            rslogin = new DataTable();
            SDArslogin = new SqlDataAdapter("SELECT Login.Login,Login.PassWord,Login.Gerant FROM Login WHERE Login.Login='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "' AND Gerant=1", General.adocnn);
            SDArslogin.Fill(rslogin);

            if (rslogin.Rows.Count > 0)
            {
                txtMotPasse.Text = rslogin.Rows[0]["Password"] + "";
            }
            else
            {
                var NewRow = rslogin.NewRow();
                NewRow["Login"] = sCode;
                NewRow["Gerant"] = 1;
                this.txtMotPasse.Text = "zao";
                NewRow["Password"] = "zao";
                rslogin.Rows.Add(NewRow);
                SCBrslogin = new SqlCommandBuilder(SDArslogin);
                int xx = SDArslogin.Update(rslogin);
            }

            rslogin.Dispose();
            SDArslogin.Dispose();

            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeGrilleGestionnaire(string sCode)
        {
            // sSQl = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion," _
            //& " Gestionnaire.CodeQualif_Qua, Qualification.Qualification," _
            //& " Gestionnaire.TelGestion, Gestionnaire.TelPortable_GES," _
            //& " Gestionnaire.FaxGestion, Gestionnaire.EMail_GES," _
            //& " Gestionnaire.Horaire_GES, Gestionnaire.MotdePasse_GES" _
            //& " , Gestionnaire.code1" _
            //& " FROM Gestionnaire INNER JOIN Qualification ON " _
            //& " Gestionnaire.CodeQualif_Qua = Qualification.CodeQualif"

            General.sSQL = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion," + " Gestionnaire.CodeQualif_Qua, Gestionnaire.Qualification as Qualification," + " Gestionnaire.TelGestion, Gestionnaire.TelPortable_GES,  Gestionnaire.TelBureau, " + " Gestionnaire.FaxGestion,  Gestionnaire.EMail_GES," + " Gestionnaire.Horaire_GES, Gestionnaire.MotdePasse_GES, Gestionnaire.code1" + " , AncienEmployeur, ID_LOGIN " + " FROM Gestionnaire ";
            General.sSQL = General.sSQL + " WHERE Gestionnaire.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";

            //    sSQl = sSQl & " and Gestionnaire.codeQualif_qua = '" & cGestionnaire & "'"

            modAdorsCorrespondants = new ModAdo();
            rsCorrespondants = modAdorsCorrespondants.fc_OpenRecordSet(General.sSQL);
            this.ssGridGestionnaire.DataSource = rsCorrespondants;



        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropQualifGest()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Qualification.CodeQualif,Qualification.Qualification " + " FROM Qualification WHERE Correspondant_COR=1";

            if (ComboQualifGest.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ComboQualifGest, General.sSQL, "CodeQualif", true);

            }
            this.ssGridGestionnaire.DisplayLayout.Bands[0].Columns["CodeQualif_QUA"].ValueList = this.ComboQualifGest;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeGrilleIntervenant(string sCode)
        {

            // sSQl = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.CodeQualif," _
            //& " Qualification.Qualification, Personnel.NumRadio, Personnel.Note_NOT" _
            //& " , Intervenant_INT.CODE1_TAB" _
            //& " FROM Qualification INNER JOIN (Intervenant_INT INNER JOIN Personnel" _
            //& " ON Intervenant_INT.Matricule_per = Personnel.Matricule)" _
            //& " ON Qualification.CodeQualif = Personnel.CodeQualif"


            General.sSQL = "";
            General.sSQL = "SELECT Intervenant_INT.Matricule_per, '' as Nom, '' as CodeQualif," + " '' as Qualification, '' as NumRadio, '' as Note_NOT" + " , Intervenant_INT.CODE1_TAB" + " FROM Intervenant_INT ";
            //sSQl = "SELECT Intervenant_INT.Matricule_per " _
            //& " , Intervenant_INT.CODE1_TAB" _
            //& " FROM Intervenant_INT "
            General.sSQL = General.sSQL + " where Intervenant_INT.code1_tab ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";

            modAdorsIntervenant = new ModAdo();
            rsIntervenant = modAdorsIntervenant.fc_OpenRecordSet(General.sSQL);
            this.SSINTERVENANT.DataSource = rsIntervenant;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object fc_OngletCommercial()
        {
            object functionReturnValue = null;
            DataTable rsFactures = default(DataTable);
            SqlDataAdapter SDArsFactures;
            ModAdo modAdoDataTable;
            string sqlSelect = null;
            string DateMini = null;
            string DateMaxi = null;
            DataTable rsCptTiers = default(DataTable);
            int JourMaxiMois = 0;
            string sWhereCpt = null;

            //===> Mondir le 22.09.2020, pour intégrer les modfis de la version V21.09.2020 de VB6
            DateTime dtDateNmoins1;
            //===> Fin Modif Mondir

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                return functionReturnValue;
            }
            optSelectCom0.Checked = true;
            txtGerantDe.Text = txtCode1.Text;
            txtGerantA.Text = txtCode1.Text;
            txtGerantDe_Validating(txtGerantDe, new CancelEventArgs(false));

            try
            {
                frmFactures.Text = "Montants facturés à la date du " + DateTime.Now.ToString("dd") + "/" + DateTime.Now.ToString("MM");

                JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));

                if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                {
                    Label356.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year - 1));
                    Label357.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year));
                    DateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Now.Year);
                    DateMaxi = DateTime.Now.ToString("dd") + "/" + DateTime.Now.ToString("MM") + "/" + DateTime.Now.ToString("yyyy");
                }
                else
                {
                    if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= Convert.ToInt16(General.MoisFinExercice))
                    {
                        Label356.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year - 2)) + " / " + Convert.ToString(Convert.ToDouble(DateTime.Now.Year - 1));
                        Label357.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year - 1)) + " / " + Convert.ToString(Convert.ToDouble(DateTime.Now.Year));
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year - 1);
                        //MoisFinExercice + 1
                        DateMaxi = DateTime.Now.Day.ToString("00") + "/" + DateTime.Now.Month.ToString("00") + "/" + DateTime.Now.Year.ToString();
                        //MoisFinExercice
                    }
                    else
                    {
                        Label356.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year - 1)) + " / " + Convert.ToString(Convert.ToDouble(DateTime.Now.Year));
                        Label357.Text = Convert.ToString(Convert.ToDouble(DateTime.Now.Year)) + " / " + Convert.ToString(Convert.ToDouble(DateTime.Now.Year + 1));
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice - 1
                        DateMaxi = DateTime.Now.Day.ToString("00") + "/" + DateTime.Now.Month.ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice
                    }
                }


                sWhereCpt = "SELECT       NCompte  From Immeuble WHERE Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                var adoTemp = new ModAdo();
                rsCptTiers = adoTemp.fc_OpenRecordSet(sWhereCpt);

                sWhereCpt = "";

                if (rsCptTiers.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsCptTiers.Rows)
                    {
                        if (sWhereCpt == "")
                            sWhereCpt = " ( CT_NUM ='" + StdSQLchaine.gFr_DoublerQuote(dr["Ncompte"] + "") + "'";
                        else
                            sWhereCpt = sWhereCpt + " OR  CT_NUM ='" + StdSQLchaine.gFr_DoublerQuote(dr["Ncompte"] + "") + "'";
                    }
                    sWhereCpt = sWhereCpt + ")";
                }
                else
                    sWhereCpt = " CT_NUM  ='@@@@@@@@@' ";

                adoTemp.Close();
                rsCptTiers = null;

                SAGE.fc_OpenConnSage();

                rsFactures = new DataTable();

                //Fioul
                //Année n
                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN FROM F_ECRITUREC " + " WHERE " + sWhereCpt + " AND (LEFT(EC_Piece, 2) = 'FM')";
                sqlSelect = sqlSelect + " AND JM_DATE<='" + DateMaxi + "' AND JM_DATE>='" + DateMini + "' AND EC_SENS='0'";

                if (General.sJournalVente != "")
                    sqlSelect = sqlSelect + " AND JO_NUM='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";


                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);

                if (rsFactures.Rows.Count > 0)
                {
                    lblNFioul.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblNFioul.Text = "00.00";
                }
                rsFactures.Dispose();
                rsFactures = null;

                //Année n-1
                dtDateNmoins1 = Convert.ToDateTime(DateMini).AddYears(-1);
                dtDateNmoins1 = Convert.ToDateTime(DateMini).AddDays(-1);

                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN1 FROM F_ECRITUREC" + " WHERE " + sWhereCpt + " AND  (LEFT(EC_Piece, 2) = 'FM')";
                //===> Mondir le 22.09.2020, pour intégrer les modfis de la version V21.09.2020 de VB6
                //sqlSelect = sqlSelect + " AND JM_DATE<='" + Convert.ToDateTime(DateMaxi).AddYears(-1) + "' AND JM_DATE>='" + Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //=== modif du 21 09 2020
                sqlSelect = sqlSelect + " AND JM_DATE<='" + dtDateNmoins1 + "' AND JM_DATE>='" + Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //===> Fin Modif Mondir

                if (General.sJournalVente != "")
                    sqlSelect = sqlSelect + " AND JO_NUM ='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";

                rsFactures = new DataTable();
                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);

                if (rsFactures.Rows.Count > 0)
                {
                    lblN1Fioul.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN1"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblN1Fioul.Text = "00.00";
                }
                rsFactures.Dispose();


                if (Convert.ToDouble(lblNFioul.Text.Replace(" ", "")) >= Convert.ToDouble(lblN1Fioul.Text.Replace(" ", "")))
                {
                    lblEcFioul.ForeColor = ColorTranslator.FromOle(0xc000);
                }
                else
                {
                    lblEcFioul.ForeColor = ColorTranslator.FromOle(0xc0);
                }

                if (Convert.ToDouble(lblNFioul.Text.Replace(" ", "")) != 0 || Convert.ToDouble(lblN1Fioul.Text.Replace(" ", "")) != 0)
                {
                    FrameFioul.Visible = true;
                }
                else
                {
                    FrameFioul.Visible = false;
                }

                //if (Convert.ToDouble(lblNFioul.Text) != 0 || Convert.ToDouble(lblN1Fioul.Text) != 0)
                //{
                //    //FrameFioul.Visible = true;
                //    label62.Visible = true;
                //    lblN1Fioul.Visible = true;
                //    lblNFioul.Visible = true;
                //    lblEcFioul.Visible = true;
                //}
                //else
                //{
                //    label62.Visible = false;
                //    lblN1Fioul.Visible = false;
                //    lblNFioul.Visible = false;
                //    lblEcFioul.Visible = false;
                //}
                lblEcFioul.Text = (Convert.ToDouble(lblNFioul.Text.Replace(" ", "")) - Convert.ToDouble(lblN1Fioul.Text.Replace(" ", ""))).ToString("### ### #00.00") + " €";
                lblNFioul.Text = lblNFioul.Text + " €";
                lblN1Fioul.Text = lblN1Fioul.Text + " €";


                //Travaux
                //Année n
                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN FROM F_ECRITUREC" + " WHERE " + sWhereCpt + " AND  (LEFT(EC_Piece, 2) = 'TA' OR  LEFT(EC_Piece, 2) = 'TM')";
                sqlSelect = sqlSelect + " AND JM_DATE<='" + DateMaxi + "' AND JM_DATE>='" + DateMini + "' AND EC_SENS='0'";
                if (General.sJournalVente != "")
                    sqlSelect = sqlSelect + "AND JO_NUM ='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";

                rsFactures = new DataTable();
                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);
                if (rsFactures.Rows.Count > 0)
                {
                    lblNTrav.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblNTrav.Text = "00.00";
                }
                rsFactures.Dispose();

                //Année n-1
                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN1 FROM F_ECRITUREC" + " WHERE " + sWhereCpt + "  AND  (LEFT(EC_Piece, 2) = 'TA' OR LEFT(EC_Piece, 2) = 'TM')";
                //===> Mondir le 22.09.2020, pour intégrer les modfis de la version V21.09.2020 de VB6
                //sqlSelect = sqlSelect + " AND JM_DATE<='" + Convert.ToDateTime(DateMaxi).AddYears(-1) + "' AND JM_DATE>='" + Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //=== modif du 21 09 2020
                sqlSelect = sqlSelect + " AND JM_DATE<='" + dtDateNmoins1 + "' AND JM_DATE>='" + Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //=== Fin Modif Mondir


                if (General.sJournalVente != "")
                {
                    sqlSelect = sqlSelect + " AND JO_NUM='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";
                }


                rsFactures = new DataTable();
                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);
                if (rsFactures.Rows.Count > 0)
                {
                    lblN1Trav.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN1"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblN1Trav.Text = "00.00";
                }
                rsFactures.Dispose();

                if (Convert.ToDouble(lblNTrav.Text.Replace(" ", "")) >= Convert.ToDouble(lblN1Trav.Text.Replace(" ", "")))
                {
                    lblEcTrav.ForeColor = ColorTranslator.FromOle(0xc000);
                }
                else
                {
                    lblEcTrav.ForeColor = ColorTranslator.FromOle(0xc0);
                }

                lblEcTrav.Text = (Convert.ToDouble(lblNTrav.Text.Replace(" ", "")) - Convert.ToDouble(lblN1Trav.Text.Replace(" ", ""))).ToString("### ### #00.00") + " €";
                lblNTrav.Text = lblNTrav.Text + " €";
                lblN1Trav.Text = lblN1Trav.Text + " €";

                //Contrat
                //Année n
                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN FROM F_ECRITUREC " + " WHERE " + sWhereCpt + " AND  (LEFT(EC_Piece, 2) = 'CA' OR LEFT(EC_Piece, 2) = 'CM'  OR LEFT(EC_RefPiece, 2) = 'CM' OR LEFT(EC_RefPiece, 2) = 'CA')";
                sqlSelect = sqlSelect + " AND (JM_DATE<='" + DateMaxi + "') AND (JM_DATE>='" + DateMini + "') AND EC_SENS='0'";
                if (General.sJournalVente != "")
                    sqlSelect = sqlSelect + " AND JO_NUM='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";

                rsFactures = new DataTable();
                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);

                if (rsFactures.Rows.Count > 0)
                {
                    lblNCont.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblNCont.Text = "00.00";
                }
                rsFactures.Dispose();

                //Année n-1
                //===> Mondir le 22.09.2020, pour intégrer les modfis de la version V21.09.2020 de VB6
                sqlSelect = "SELECT SUM(EC_Montant) AS TotalN1 FROM F_ECRITUREC WHERE " + sWhereCpt + " AND  (LEFT(EC_Piece, 2) = 'CA' OR LEFT(EC_Piece, 2) = 'CM' OR LEFT(EC_RefPiece, 2) = 'CM' OR LEFT(EC_RefPiece, 2) = 'CA')";
                //sqlSelect = sqlSelect + " AND JM_DATE<='" + Convert.ToDateTime(DateMaxi).AddYears(-1) + "' AND JM_DATE>='" + Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //== modif du 21 09 2020.
                sqlSelect = sqlSelect + " AND JM_DATE<='" + dtDateNmoins1 + "' AND JM_DATE>='" +
                            Convert.ToDateTime(DateMini).AddYears(-1) + "' AND EC_SENS='0'";
                //===> Fin Modif Mondir

                if (General.sJournalVente != "")
                    sqlSelect = sqlSelect + " AND JO_NUM='" + StdSQLchaine.gFr_DoublerQuote(General.sJournalVente) + "'";
                rsFactures = new DataTable();
                SDArsFactures = new SqlDataAdapter(sqlSelect, SAGE.adoSage);
                SDArsFactures.Fill(rsFactures);

                if (rsFactures.Rows.Count > 0)
                {
                    lblN1Cont.Text = Convert.ToDouble(General.nz(rsFactures.Rows[0]["TotalN1"], "0")).ToString("### ### #00.00");
                }
                else
                {
                    lblN1Cont.Text = "00.00";
                }
                rsFactures.Dispose();
                if (Convert.ToDouble(lblNCont.Text.Replace(" ", "")) >= Convert.ToDouble(lblN1Cont.Text.Replace(" ", "")))
                {
                    lblEcCont.ForeColor = ColorTranslator.FromOle(0xc000);
                }
                else
                {
                    lblEcCont.ForeColor = ColorTranslator.FromOle(0xc0);
                }

                lblEcCont.Text = (Convert.ToDouble(lblNCont.Text.Replace(" ", "")) - Convert.ToDouble(lblN1Cont.Text.Replace(" ", ""))).ToString("### ### #00.00") + " €";
                lblNCont.Text = lblNCont.Text + " €";
                lblN1Cont.Text = lblN1Cont.Text + " €";


                rsFactures = null;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " fc_OngletCommercial ");
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fC_OngletIntervention()
        {
            //initialise les listes déroulantes en fonction du client
            General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse,"
                + " Immeuble.CodePostal, Immeuble.Ville"
                + " From Immeuble"
                + " WHERE Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";

            sheridan.InitialiseCombo(cmbCodeimmeuble, General.sSQL, "codeImmeuble");

            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat"
                + " FROM TypeCodeEtat";

            if (cmbStatus.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbStatus, General.sSQL, "CodeEtat");
            }
            //sSQL = "SELECT Intervenant_INT.Matricule_PER, Personnel.Nom" _
            //& "  FROM Intervenant_INT INNER JOIN Personnel ON" _
            //& " Intervenant_INT.Matricule_PER = Personnel.Matricule" _
            //& " WHERE Intervenant_INT.code1_tab ='" & gFr_DoublerQuote(txtCode1) & "'"
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom"
                + "  FROM Personnel INNER JOIN SpecifQualif ON"
                + " Personnel.CodeQualif = SpecifQualif.CodeQualif "
                + " order by nom ";

            if (cmbIntervenant.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
            }

            General.sSQL = "SELECT CodeArticle , Designation1 " + " From FacArticle";
            if (cmbArticle.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbArticle, General.sSQL, "CodeArticle");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void FC_ChargeOnglet()
        {
            string sCode = null;
            sCode = txtCode1.Text;
            if (General.adocnn == null || SSTab1.SelectedTab == null)
            {
                return;
            }

            if (SSTab1.SelectedTab.Index == 1)
            {
                fc_ChargeGrilleGestionnaire(sCode);
                fc_ChargeGrilleIntervenant(sCode);
            }
            else if (SSTab1.SelectedTab.Index == 4)
            {
                fc_OngletCommercial();
            }
            else if (SSTab1.SelectedTab.Index == 5)
            {
                fc_OngletDocumention();
            }
            else if (SSTab1.SelectedTab.Index == 6)
            {
                fC_OngletIntervention();
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());


            }
            else if (SSTab1.SelectedTab.Index == 7)
            {
                fC_Ongletdevis();
                cmbDevis_Click(cmbDevis, new System.EventArgs());

                // onglet finance
            }
            else if (SSTab1.SelectedTab.Index == 3)
            {
                txtArretDate.Text = DateTime.Now.ToString("dd/MM/yyyy");
                txtEcheance2.Text = DateTime.Now.ToString("dd/MM/yyyy");
                OptSyndic_CheckedChanged(OptSyndic, new System.EventArgs());
                OptSyndic.Checked = true;
                txtSyndic1.Value = txtCode1.Text;
                txtSyndic2.Value = txtCode1.Text;
                fc_ChargeFinance();
            }
            else if (SSTab1.SelectedTab.Index == 8)
            {
                fc_CLRE_ClientRelance(txtCode1.Text);

            }
            fc_DropMaticule();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNameGrille"></param>
        private void fc_DropMaticule(object sNameGrille = null)
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Qualification.CodeQualif," + " Qualification.Qualification, Personnel.NumRadio, Personnel.Note_NOT" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

            if (ssDropMatricule.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropMatricule, General.sSQL, "Matricule", true);
            }

            if (SSINTERVENANT.DataSource != null)
                this.SSINTERVENANT.DisplayLayout.Bands[0].Columns["Matricule_per"].ValueList = this.ssDropMatricule;
            if (ssIntervention.DataSource != null)
                this.ssIntervention.DisplayLayout.Bands[0].Columns["Intervenant"].ValueList = this.ssDropMatricule;
            if (GridCLRE.DataSource != null)
                GridCLRE.DisplayLayout.Bands[0].Columns["matricule"].ValueList = this.ssDropMatricule;
            if (ssDropMatricule.DataSource != null)
            {
                ssDropMatricule.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
                ssDropMatricule.DisplayLayout.Bands[0].Columns["NumRadio"].Hidden = true;
                ssDropMatricule.DisplayLayout.Bands[0].Columns["Note_NOT"].Hidden = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode1"></param>
        private void fc_CLRE_ClientRelance(string sCode1)
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT   CLRE_Noauto, Code1, CLRE_Date, Matricule, CLRE_Contact," + " CLRE_Tel, CLRE_Fax, CLRE_Mail, CLRE_DateVisite, CLRE_Commentaire" + " From CLRE_ClientRelance" + " WHERE  Code1 = '" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";

                modAdorsCLRE = new ModAdo();
                rsCLRE = modAdorsCLRE.fc_OpenRecordSet(sSQL, GridCLRE, "CLRE_Noauto");

                GridCLRE.DataSource = rsCLRE;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_CLRE_ClientRelance");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object fc_ChargeFinance()
        {
            object functionReturnValue = null;
            DataTable rsAdoFinance = default(DataTable);
            SqlDataAdapter SDArsAdoFinance;
            double MontantCredit = 0;
            double MontantDebit = 0;
            string compteTiers = null;

            string DateMini = null;
            string DateMaxi = null;

            int JourMaxiMois = 0;
            string sDateFacture = null;
            string sCritere = null;
            string sDateEch = null;

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                    return functionReturnValue;
                }

                if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                {
                    sCritere = General.cCritereFinance;
                }
                else
                {
                    sCritere = "";
                }

                /* j'ai commenté cette ligne de code et le remplacer lors de son utilisation 
                pour ajouter la date dans le 2 eme parametre afin d'eviter un beug
                par exmple si DateTime.Now.Year =2020 et la date DateTime.Now.Year + 1=2021 et General.MoisFinExercice=2
                donc DateMaxi=29/02/2021 et le dernier jour du mois 2 en 2021 est 28 et non 29 alors dans ce cas un bug se déclenche  */
                //JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));

                if (General.gfr_liaison("AfficheExercices", "1") == "1")
                {
                    Frame70.Visible = true;
                }
                else
                {
                    Frame70.Visible = false;
                }

                if (General.gfr_liaison("AfficheExercices", "1") == "1")
                {
                    if (cmbExercices.ActiveRow != null)
                    {
                        DateMini = cmbExercices.ActiveRow.Cells["Du"].Value.ToString();
                        DateMaxi = cmbExercices.ActiveRow.Cells["Au"].Value.ToString();
                    }
                }
                else
                {
                    if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                    {
                        DateMini = "01/" + "01" + "/" + DateTime.Now.Year;
                        JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));
                        DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + DateTime.Now.Year;
                    }
                    else
                    {
                        if (DateTime.Now.Month >= 1 && DateTime.Now.Month <=
                            Convert.ToInt16(General.MoisFinExercice))
                        {
                            DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year - 1);
                            //MoisFinExercice + 1
                            JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));
                            DateMaxi = JourMaxiMois + "/" +
                                       Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                            //MoisFinExercice
                        }
                        else
                        {
                            DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" +
                                       Convert.ToString(DateTime.Now.Year);
                            //MoisFinExercice - 1
                            JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")), DateTime.Now.Year + 1);
                            DateMaxi = JourMaxiMois + "/" +
                                       Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year + 1);
                            //MoisFinExercice
                        }
                    }
                }


                //    If MoisFinExercice = "12" Or Not IsNumeric(MoisFinExercice) Then
                //            DateMini = "01/" & "01" & "/" & CStr(Year(Date))
                //            DateMaxi = JourMaxiMois & "/" & MoisFinExercice & "/" & CStr(Year(Date))
                //    Else
                //        If Month(Date) >= 1 And Month(Date) <= CInt(MoisFinExercice) Then
                //            DateMini = "01/" & Format(CStr(CInt(MoisFinExercice) + 1), "00") & "/" & CStr(Year(Date) - 1) 'MoisFinExercice + 1
                //            DateMaxi = JourMaxiMois & "/" & Format(MoisFinExercice, "00") & "/" & CStr(Year(Date)) 'MoisFinExercice
                //        Else
                //            DateMini = "01/" & Format(CStr(CInt(MoisFinExercice) + 1), "00") & "/" & CStr(Year(Date)) 'MoisFinExercice - 1
                //            DateMaxi = JourMaxiMois & "/" & Format(MoisFinExercice, "00") & "/" & CStr((Year(Date) + 1)) 'MoisFinExercice
                //        End If
                //    End If


                rsAdoFinance = new DataTable();
                SAGE.fc_OpenConnSage();

                var SourceDT = new DataTable();
                SourceDT.Columns.Add("Compte Immeuble");
                SourceDT.Columns.Add("Journal");
                SourceDT.Columns.Add("Date Journal");
                SourceDT.Columns.Add("Date Facture");
                SourceDT.Columns.Add("Référence facture");
                SourceDT.Columns.Add("Echéance");
                SourceDT.Columns.Add("Débit");
                SourceDT.Columns.Add("Crédit");
                SSGridSituation.DataSource = SourceDT;

                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    General.fc_MajCompteTiersGdp("", txtCode1.Text);
                }

                if (optCptImmeuble.Checked == true)
                {
                    compteTiers = ComboComptes.Text;
                    SSGridSituation.DisplayLayout.Bands[0].Columns["Compte Immeuble"].Hidden = true;


                }
                else if (optCptGerant.Checked == true)
                {
                    compteTiers = General.Left(txtCode1.Text, 17);
                    SSGridSituation.DisplayLayout.Bands[0].Columns["Compte Immeuble"].Hidden = false;
                }

                if (optEcrituresNonLetrees.Checked == true)
                {

                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT F_ECRITUREC.CT_NUM,EC_PIECE,EC_SENS,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date," +
                        " EC_Jour FROM F_ECRITUREC INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                    General.sSQL = General.sSQL + " WHERE ";
                    General.sSQL = General.sSQL + " JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }

                    General.sSQL = General.sSQL +
                                   " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere +
                                   ") " + " or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + "))";
                    General.sSQL = General.sSQL + " ORDER BY F_ECRITUREC.CT_NUM, EC_DATE ASC";

                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT F_ECRITUREC.CT_NUM,EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date, EC_Jour " +
                        " FROM F_ECRITUREC INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                    General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi +
                                   "' AND ";

                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") or " +
                                   " (F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                    General.sSQL = General.sSQL + " ORDER BY F_ECRITUREC.CT_NUM, EC_DATE ASC";

                }
                //rsAdoFinance.Close
                rsAdoFinance = new DataTable();
                SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                SDArsAdoFinance.Fill(rsAdoFinance);



                if (rsAdoFinance.Rows.Count > 0)
                {

                    foreach (DataRow rsAdoFinanceRow in rsAdoFinance.Rows)
                    {
                        if (rsAdoFinanceRow["JO_Num"].ToString().ToUpper() == General.cAN.ToUpper())
                        {
                            sDateFacture = rsAdoFinanceRow["EC_Echeance"] + "";
                        }
                        else
                        {
                            sDateFacture =
                                 Convert.ToInt32(rsAdoFinanceRow["EC_Jour"]).ToString("00") +
                                General.Mid(rsAdoFinanceRow["JM_Date"].ToString(), 3,
                                    rsAdoFinanceRow["JM_Date"].ToString().Length - 2);
                        }

                        if (!string.IsNullOrEmpty(sDateFacture) && General.IsDate(sDateFacture))
                            sDateFacture = Convert.ToDateTime(sDateFacture).ToString("dd/MM/yyyy");
                        else
                            sDateFacture = "";

                        string dateJournal = rsAdoFinanceRow["JM_Date"].ToString();
                        if (!string.IsNullOrEmpty(dateJournal) && General.IsDate(dateJournal))
                            dateJournal = Convert.ToDateTime(dateJournal).ToString("dd/MM/yyyy");
                        else
                            dateJournal = "";

                        string ec_Echeance = rsAdoFinanceRow["EC_Echeance"].ToString();
                        if (!string.IsNullOrEmpty(ec_Echeance) && General.IsDate(ec_Echeance))
                            ec_Echeance = Convert.ToDateTime(ec_Echeance).ToString("dd/MM/yyyy");
                        else
                            ec_Echeance = "";

                        //Colonne du montant dans les crédits
                        if (rsAdoFinanceRow["EC_Sens"].ToString() == "1")
                        {
                            var Row = SourceDT.NewRow();
                            Row["Compte Immeuble"] = rsAdoFinanceRow["CT_NUM"];
                            Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                            Row["Date Journal"] = dateJournal;
                            Row["Date Facture"] = sDateFacture;
                            Row["Référence facture"] = rsAdoFinanceRow["EC_Piece"];
                            Row["Echéance"] = ec_Echeance;
                            Row["Débit"] = null;
                            string credit = rsAdoFinanceRow["EC_Montant"].ToString();
                            if (!string.IsNullOrEmpty(credit) && General.IsNumeric(credit))
                                credit = Convert.ToDouble(credit).ToString("#.##");
                            else
                                credit = "";
                            Row["Crédit"] = credit;
                            SourceDT.Rows.Add(Row);
                            SSGridSituation.DataSource = SourceDT;
                        }
                        else
                        {
                            var Row = SourceDT.NewRow();
                            Row["Compte Immeuble"] = rsAdoFinanceRow["CT_NUM"];
                            Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                            Row["Date Journal"] = dateJournal;
                            Row["Date Facture"] = sDateFacture;
                            Row["Référence facture"] = rsAdoFinanceRow["EC_Piece"];
                            Row["Echéance"] = ec_Echeance;
                            string debit = rsAdoFinanceRow["EC_Montant"].ToString();
                            if (!string.IsNullOrEmpty(debit) && General.IsNumeric(debit))
                                debit = Convert.ToDouble(debit).ToString("#.##");
                            else
                                debit = "";
                            Row["Débit"] = debit;
                            Row["Crédit"] = null;
                            SourceDT.Rows.Add(Row);
                            SSGridSituation.DataSource = SourceDT;
                        }
                    }
                }
                rsAdoFinance.Dispose();
                if (optEcrituresNonLetrees.Checked == true)
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM ";
                    General.sSQL = General.sSQL + " WHERE JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi +
                                   "' AND ";
                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }
                    General.sSQL = General.sSQL +
                                   " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + " )" +
                                   " or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + "))";

                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + " )" + " or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + "))";

                }
                rsAdoFinance = new DataTable();
                SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                SDArsAdoFinance.Fill(rsAdoFinance);
                if (rsAdoFinance.Rows.Count > 0)
                {
                    MontantCredit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                }
                rsAdoFinance.Dispose();

                if (optEcrituresNonLetrees.Checked == true)
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }
                    General.sSQL = General.sSQL +
                                   " AND ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + ")" +
                                   " or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + "))";
                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL =
                        "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC  INNER JOIN F_COMPTET ON F_ECRITUREC.CT_NUM=F_COMPTET.CT_NUM WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    if (optCptGerant.Checked == true)
                    {
                        General.sSQL = General.sSQL + " F_COMPTET.CT_Classement='" + compteTiers + "'";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " F_ECRITUREC.CT_NUM='" + compteTiers + "'";
                    }
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + ") " + " or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" +
                                   sCritere + "))";

                }
                rsAdoFinance = new DataTable();
                SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                SDArsAdoFinance.Fill(rsAdoFinance);

                if (rsAdoFinance.Rows.Count > 0)
                {
                    MontantDebit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                }
                rsAdoFinance.Dispose();

                txtSoldeCompte.Text = Convert.ToString(MontantDebit - MontantCredit);
                txtTotalFacture.Text = Convert.ToString(MontantDebit);
                txtTotalRegle.Text = Convert.ToString(MontantCredit);

                SAGE.fc_CloseConnSage();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " Erreur Liaison Sage : ");
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fC_Ongletdevis()
        {
            string sSQL = null;
            //        sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse," _
            //'        & " Immeuble.CodePostal, Immeuble.Ville" _
            //'        & " From Immeuble" _
            //'        & " WHERE Immeuble.Code1 ='" & gFr_DoublerQuote(txtCode1) & "'"
            //        InitialiseCombo .cmbDevisImmeuble, Adodc4, sSQL, "codeImmeuble"

            //        sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle" _
            //'            & " FROM DevisCodeEtat"
            //        InitialiseCombo .cmbDevisStatus, Adodc12, sSQL, "Code"

            //        sSQL = "SELECT Personnel.Matricule, Personnel.Nom" _
            //'                & "  FROM Personnel INNER JOIN SpecifQualif ON" _
            //'                & " Personnel.CodeQualif = SpecifQualif.CodeQualif " _
            //'                & " order by nom "
            //
            //        InitialiseCombo .cmbDevisIntervenant, Adodc6, sSQL, "Matricule"

            //          sSQL = "SELECT CodeArticle , Designation1 AS Designation" _
            //'            & " From FacArticle" _
            //'            & " WHERE CodeCategorieArticle ='I'"
            //        If cmdDevisArticle.Rows = 0 Then
            //            InitialiseCombo cmdDevisArticle, Adodc5, sSQL, "CodeArticle"
            //        End If

            // ferme le recordset faisant référence au interventions liée au client
            ModAdo.fc_CloseRecordset(rsDevis);
            this.ssDevis.DataSource = null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNameGrille"></param>
        private void fc_DropCodeetat(string sNameGrille = null)
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat";

            if (ssDropCodeEtat.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropCodeEtat, General.sSQL, "CodeEtat", true);


            }
            this.ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].ValueList = this.ssDropCodeEtat;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        private void ListDirectory(TreeView treeView, string path)
        {
            try
            {
                treeView.Nodes.Clear();
                treeView.SuspendLayout();

                //var stack = new Stack<TreeNode>();
                //var rootDirectory = new DirectoryInfo(path);
                //var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
                //stack.Push(node);

                //while (stack.Count > 0)
                //{
                //    var currentNode = stack.Pop();
                //    currentNode.EnsureVisible();
                //    var directoryInfo = (DirectoryInfo)currentNode.Tag;
                //    if(directoryInfo.Exists)
                //    foreach (var directory in directoryInfo.GetDirectories())
                //    {
                //        var childDirectoryNode = new TreeNode(directory.Name) { Tag = directory.FullName };
                //        childDirectoryNode.ImageIndex = 1;
                //        childDirectoryNode.SelectedImageIndex = 0;
                //        currentNode.Nodes.Add(childDirectoryNode);
                //        // stack.Push(childDirectoryNode);
                //    }

                //}

                //treeView.Nodes.Add(node);

                //treeView.ExpandAll();
                //treeView.ResumeLayout();
                //foreach (TreeNode TN in treeView.Nodes)
                //    ListSubFolders(TN);


                if (Directory.Exists(path))
                {
                    var rootDirectory = new DirectoryInfo(path);
                    if (rootDirectory.GetDirectories().Length > 0)
                        foreach (var Dir in rootDirectory.GetDirectories())
                        {
                            var node = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            node.ImageIndex = 1;
                            node.SelectedImageIndex = 0;
                            treeView.Nodes.Add(node);
                        }

                    treeView.ExpandAll();
                    treeView.ResumeLayout();
                    foreach (TreeNode TN in treeView.Nodes)
                        ListSubFolders(TN);
                }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Cursor = Cursors.Default;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="TN"></param>
        private void ListSubFolders(TreeNode TN)
        {
            DirectoryInfo DI = new DirectoryInfo(TN.Tag.ToString());
            if (DI.Exists)
                if (DI.GetDirectories().Length > 0)
                {
                    foreach (var Dir in DI.GetDirectories())
                    {
                        var childDirectoryNode = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                        childDirectoryNode.ImageIndex = 1;
                        childDirectoryNode.SelectedImageIndex = 0;
                        TN.Nodes.Add(childDirectoryNode);
                        ListSubFolders(childDirectoryNode);
                    }
                }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_OngletDocumention()
        {
            try
            {

                Cursor = Cursors.WaitCursor;

                DirDocClient.Tag = General.sCheminDossier + "\\" + txtCode1.Text;
                ListDirectory(DirDocClient, DirDocClient.Tag.ToString());

                FilDocClient.Tag = DirDocClient.Tag;
                FilesInDirectory(FilDocClient, new DirectoryInfo(FilDocClient.Tag.ToString()));

                Cursor = Cursors.Default;

            }
            catch (Exception Ex)
            {
                Program.SaveException(Ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(Ex.Message + " Cet immeuble n'existe pas. Appuyer sur le bouton valider pour créer un dosiier pour cet immeuble.",
                    "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cursor = Cursors.Default;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="directoryInfo"></param>
        private void FilesInDirectory(ListBox listBox, DirectoryInfo directoryInfo)
        {
            try
            {
                listBox.Items.Clear();
                if (directoryInfo.Exists)
                    foreach (var file in directoryInfo.GetFiles())
                        listBox.Items.Add(file.Name);
            }
            catch (Exception e) { Program.SaveException(e); }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGerantDe_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtGerantDe.Text))
            {
                txtReqComGerant.Text = "{Table1.Code1}>='" + txtGerantDe.Text + "'" + (!string.IsNullOrEmpty(txtGerantA.Text) ? " AND {Table1.Code1}<='" + StdSQLchaine.gFr_DoublerQuote(txtGerantA.Text) + "'" : "");
            }
            else
            {
                txtReqComGerant.Text = (!string.IsNullOrEmpty(txtGerantA.Text) ? "{Table1.Code1}='" + StdSQLchaine.gFr_DoublerQuote(txtGerantA.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptSyndic_CheckedChanged(object sender, EventArgs e)
        {
            if (OptSyndic.Checked)
            {
                txtSyndic1.Text = "";
                txtSyndic2.Text = "";
                txtCommercial.Text = "";
                ssCombo14.Text = "";

                txtSyndic1.Visible = true;
                txtSyndic2.Visible = true;
                ssCombo14.Visible = true;
                Label4198.Visible = true;
                Label5726.Visible = true;
                Label5729.Visible = true;
                Label5758.Visible = true;
                txtCommercial.Visible = true;

                cmdSsyndic.Visible = true;

                txtImmeuble1.Visible = false;
                ssCombo19.Visible = false;
                Label4199.Visible = false;
                txtImmeuble2.Visible = false;
                Label5727.Visible = false;
                Label5730.Visible = false;
                Label5725.Visible = false;
                txtCommercialImm.Visible = false;

                txtParticulier1.Visible = false;
                ssCombo20.Visible = false;
                Label57200.Visible = false;
                ssCombo20.Visible = false;
                Label57200.Visible = false;
                txtParticulier2.Visible = false;
                Label5754.Visible = false;
                Label5756.Visible = false;
                cmdSPart.Visible = false;
                txtSyndic1.Focus();

                OptImmeuble.Checked = false;
                OptParticulier.Checked = false;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdComCommercial0_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            int Index = Convert.ToInt32(btn.Tag);
            string sCode = null;
            try
            {
                string requete = "SELECT Personnel.Matricule AS \"Matricule\", Personnel.Nom AS \"Nom\", Personnel.Prenom AS \"Prenom\", " +
                                 "SpecifQualif.LibelleQualif AS \"Qualification\" FROM Personnel LEFT JOIN SpecifQualif ON Personnel.CodeQualif=SpecifQualif.CodeQualif";
                string where_order = " (SpecifQualif.QualifCom=1 OR SpecifQualif.QualifCom=-1) ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un commercial" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (Index == 0)
                    {
                        txtComDe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (txtComA.Text == "XXXXXX")
                        {
                            txtComA.Text = "";
                            cmdVisu1.Visible = false;
                        }
                        txtComDe_Validating(txtComDe, new CancelEventArgs(false));
                    }
                    else if (Index == 1)
                    {
                        txtComA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (txtComDe.Text == "XXXXXX")
                        {
                            txtComDe.Text = "";
                            cmdVisu1.Visible = false;
                        }
                        txtComA_Validating(txtComA, new CancelEventArgs(false));
                    }
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        if (Index == 0)
                        {
                            txtComDe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            if (txtComA.Text == "XXXXXX")
                            {
                                txtComA.Text = "";
                                cmdVisu1.Visible = false;
                            }
                            txtComDe_Validating(txtComDe, new CancelEventArgs(false));
                        }
                        else if (Index == 1)
                        {
                            txtComA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            if (txtComDe.Text == "XXXXXX")
                            {
                                txtComDe.Text = "";
                                cmdVisu1.Visible = false;
                            }
                            txtComA_Validating(txtComA, new CancelEventArgs(false));
                        }
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + "; cmdComCommercial_Click ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtComDe_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtComDe.Text))
            {
                txtReqComCommercial.Text = "{Table1.Commercial}>='" + txtComDe.Text + "'" + (!string.IsNullOrEmpty(txtComA.Text) ? " AND {Table1.Commercial}<='" + StdSQLchaine.gFr_DoublerQuote(txtComA.Text) + "'" : "");
            }
            else
            {
                txtReqComCommercial.Text = (!string.IsNullOrEmpty(txtComA.Text) ? "{Table1.Commercial}='" + StdSQLchaine.gFr_DoublerQuote(txtComA.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtComA_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtComA.Text))
            {
                txtReqComCommercial.Text = "{Table1.Commercial}<='" + txtComA.Text + "'" + (!string.IsNullOrEmpty(txtComDe.Text) ? " AND {Table1.Commercial}>='" + StdSQLchaine.gFr_DoublerQuote(txtComDe.Text) + "'" : "");
            }
            else
            {
                txtReqComCommercial.Text = (!string.IsNullOrEmpty(txtComDe.Text) ? "{Table1.Commercial}='" + StdSQLchaine.gFr_DoublerQuote(txtComDe.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdComGerant0_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            int Index = Convert.ToInt32(btn.Tag);
            string sCode = null;
            try
            {
                string requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\", Ville as \"Ville\"  FROM Table1";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un gérant" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (Index == 0)
                    {
                        txtGerantDe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (txtGerantA.Text == "XXXXXX")
                        {
                            txtGerantA.Text = "";
                            cmdVisu0.Visible = false;
                        }
                        txtGerantDe_Validating(txtGerantDe, new CancelEventArgs(false));
                    }
                    else if (Index == 1)
                    {
                        txtGerantA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (txtGerantDe.Text == "XXXXXX")
                        {
                            txtGerantDe.Text = "";
                            cmdVisu0.Visible = false;
                        }
                        txtGerantA_Validating(txtGerantA, new CancelEventArgs(false));
                    }
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        if (Index == 0)
                        {
                            txtGerantDe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            if (txtGerantA.Text == "XXXXXX")
                            {
                                txtGerantA.Text = "";
                                cmdVisu0.Visible = false;
                            }
                            txtGerantDe_Validating(txtGerantDe, new CancelEventArgs(false));
                        }
                        else if (Index == 1)
                        {
                            txtGerantA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            if (txtGerantDe.Text == "XXXXXX")
                            {
                                txtGerantDe.Text = "";
                                cmdVisu0.Visible = false;
                            }
                            txtGerantA_Validating(txtGerantA, new CancelEventArgs(false));
                        }
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + "; cmdComGerant0_Click ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGerantA_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtGerantA.Text))
            {
                txtReqComGerant.Text = "{Table1.Code1}<='" + txtGerantA.Text + "'" + (!string.IsNullOrEmpty(txtGerantDe.Text) ? " AND {Table1.Code1}>='" + StdSQLchaine.gFr_DoublerQuote(txtGerantDe.Text) + "'" : "");
            }
            else
            {
                txtReqComGerant.Text = (!string.IsNullOrEmpty(txtGerantDe.Text) ? "{Table1.Code1}='" + StdSQLchaine.gFr_DoublerQuote(txtGerantDe.Text) + "'" : "");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdComPrint_Click(object sender, EventArgs e)
        {
            try
            {
                //    If txtCode1.Text = "" Then Exit Sub
                if (Dossier.fc_ControleFichier(General.gsRpt + General.EtatImmeublesClient) == false)
                    return;
                ReportDocument Report = new ReportDocument();
                Report.Load(General.gsRpt + General.EtatImmeublesClient);
                var SelectionFormula = "";
                //Gérant
                if (optSelectCom0.Checked == true)
                {
                    if (!string.IsNullOrEmpty(txtReqComGerant.Text))
                    {
                        SelectionFormula = "(" + txtReqComGerant.Text + ")";
                    }
                    if (!string.IsNullOrEmpty(txtReqEnergie.Text))
                    {
                        if (!string.IsNullOrEmpty(SelectionFormula))
                        {
                            SelectionFormula = SelectionFormula + " AND (" + txtReqEnergie.Text + ")";
                        }
                        else
                        {
                            SelectionFormula = txtReqEnergie.Text;
                        }
                    }
                    if (string.IsNullOrEmpty(SelectionFormula))
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez fait aucune sélection." + "\n" + "Etes-vous sûr de vouloir imprimer tous les gérants et tous leurs immeubles ?", "Aperçu des gérants et des immeubles", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }
                    //Commercial
                }
                else if (optSelectCom1.Checked == true)
                {
                    if (!string.IsNullOrEmpty(txtReqComCommercial.Text))
                    {
                        SelectionFormula = "(" + txtReqComCommercial.Text + ")";
                    }
                    if (!string.IsNullOrEmpty(txtReqEnergie.Text))
                    {
                        if (!string.IsNullOrEmpty(SelectionFormula))
                        {
                            SelectionFormula = SelectionFormula + " AND (" + txtReqEnergie.Text + ")";
                        }
                        else
                        {
                            SelectionFormula = txtReqEnergie.Text;
                        }
                    }
                    if (string.IsNullOrEmpty(SelectionFormula))
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez fait aucune sélection." + "\n" + "Etes-vous sûr de vouloir imprimer tous les gérants et tous leurs immeubles ?", "Aperçu des gérants et des immeubles par commercial", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }
                }
                CrystalReportFormView Form = new CrystalReportFormView(Report, SelectionFormula);
                Form.Show();
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
                General.gfr_liaison(this.Name + " cmdComPrint_Click ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDevisArticle_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeArticle , Designation1 AS Designation" + " From FacArticle" + " WHERE CodeCategorieArticle ='I'";
            //If cmdDevisArticle.Rows = 0 Then
            sheridan.InitialiseCombo(cmdDevisArticle, General.sSQL, "CodeArticle");
            //End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEcritures_Click(object sender, EventArgs e)
        {
            fc_ChargeFinance();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            switch (SSTab1.SelectedTab.Index)
            {
                case 6:
                    //Interventions
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                    ImpressionInterventions();
                    break;
                case 7:
                    //Devis
                    cmbDevis_Click(cmbDevis, new System.EventArgs());
                    ImpressionDevis();
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void ImpressionDevis()
        {

            string sSelect = null;
            string sWhere = null;
            string sOrder = null;
            int nbHeures = 0;

            ReportDocument Report = new ReportDocument();

            try
            {
                if (string.IsNullOrEmpty(General.strEtatReportDevis))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est prévu pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Dossier.fc_ControleFichier(General.gsRpt + General.strEtatReportDevis, false) == true)
                {
                    Report.Load(General.gsRpt + General.strEtatReportDevis);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est présent pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sWhere = "";
                if (!string.IsNullOrEmpty(cmbDevisImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.codeimmeuble} =\"" + cmbDevisImmeuble.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " And {DevisEntete.codeimmeuble}=\"" + cmbDevisImmeuble.Text + "\"";
                    }
                }

                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = "{Immeuble.code1}=\"" + txtCode1.Text + "\"";
                }
                else
                {
                    sWhere = sWhere + " And {Immeuble.code1}=\"" + txtCode1.Text + "\"";
                }

                if (!string.IsNullOrEmpty(cmbDevisIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeDeviseur}=\"" + cmbDevisIntervenant.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeDeviseur}=\"" + cmbDevisIntervenant.Text + "\"";
                    }
                }


                if (!string.IsNullOrEmpty(cmbDevisStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeEtat}=\"" + cmbDevisStatus.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeEtat}=\"" + cmbDevisStatus.Text + "\"";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(txtDevisDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(txtDevisDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(txtDevisAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(txtDevisAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }
                if (!string.IsNullOrEmpty(txt1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.TitreDevis}=like \"*" + txt1.Text + "*" + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.TitreDevis} LIKE \"*" + txt1.Text + "*" + "\"";
                    }
                }
                //    If txtNoIntervention <> "" Then
                //        If sWhere = "" Then
                //            sWhere = "{DevisEntete.NumeroDevis}=""" & .txtNoIntervention & """"
                //        Else
                //            sWhere = sWhere & " AND {DevisEntete.NumeroDevis}=""" & .txtNoIntervention & """"
                //        End If
                //    End If
                if (!string.IsNullOrEmpty(cmdDevisArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeTitre}=\"" + cmdDevisArticle.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeTitre}=\"" + cmdDevisArticle.Text + "\"";
                    }
                }

                //    If .txtDateSaisieDe <> "" Then
                //         If sWhere = "" Then
                //
                //             sWhere = "{DevisEntete.DateAcceptation}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //            sWhere = sWhere & " AND {DevisEntete.DateAcceptation}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //
                //        End If
                //    End If
                //    If .txtDateSaisieFin <> "" Then
                //        If sWhere = "" Then
                //
                //            sWhere = sWhere & "{DevisEntete.DateAcceptation}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //
                //             sWhere = sWhere & " AND {DevisEntete.DateAcceptation}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If

                CrystalReportFormView Form = new CrystalReportFormView(Report, sWhere);

                //CR.SortFields(0) = "+{Intervention.CodeImmeuble}"
                //CR.SortFields(1) = "+{Intervention.DateSaisie}"
                //CR.SortFields(2) = "+{Intervention.CodeEtat}"

                Form.Show();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Impression");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void ImpressionInterventions()
        {

            string sSelect = null;
            string sWhere = null;
            string sOrder = null;
            int nbHeures = 0;

            ReportDocument Report = new ReportDocument();

            try
            {
                Report.Load(General.gsRpt + General.CHEMINETAT);


                sWhere = "";

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.codeimmeuble}='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " {Intervention.codeimmeuble}='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                    }
                }

                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " {Immeuble.Code1}='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " AND {Immeuble.Code1}='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                }

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.Intervenant}='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.Intervenant}='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.CodeEtat}='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.CodeEtat}='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtinterDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }
                if (!string.IsNullOrEmpty(txtIntereAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }
                //    If .txtIntercom <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.Commentaire like '%" & .txtIntercom & "%'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.Commentaire LIKE '%" & .txtIntercom & "%'"
                //        End If
                //    End If
                //    If txtNoIntervention <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.NoIntervention = " & .txtNoIntervention & ""
                //        Else
                //            sWhere = sWhere & " and  Intervention.NoIntervention = " & .txtNoIntervention & ""
                //        End If
                //    End If
                //    If txtINT_AnaCode <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.INT_AnaCode LIKE '%" & .txtINT_AnaCode & "'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.INT_AnaCode LIKE '%" & .txtINT_AnaCode & "'"
                //        End If
                //    End If
                //     If ssComboActivite <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.INT_AnaCode LIKE '" & .ssComboActivite & "%'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.INT_AnaCode LIKE '" & .ssComboActivite & "%'"
                //        End If
                //    End If
                if (!string.IsNullOrEmpty(cmbArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " {Intervention.Article}= '" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.Article}= '" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'";
                    }
                }

                //    If .txtDateSaisieDe <> "" Then
                //         If sWhere = "" Then
                //
                //             sWhere = " {Intervention.DateSaisie}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //            sWhere = sWhere & " AND {Intervention.DateSaisie}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //
                //        End If
                //    End If
                //    If .txtDateSaisieFin <> "" Then
                //        If sWhere = "" Then
                //
                //            sWhere = sWhere & " {Intervention.dateSaisie}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //
                //             sWhere = sWhere & " AND {Intervention.dateSaisie}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If
                //    If .txtDatePrevue1 <> "" Then
                //        If sWhere = "" Then
                //            sWhere = sWhere & " {Intervention.DatePrevue}>=dateTime(" & Format(.txtDatePrevue1, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //             sWhere = sWhere & " AND {Intervention.DatePrevue}>=dateTime(" & Format(.txtDatePrevue1, "yyyy,mm,dd") & ",00,00,00)"
                //        End If
                //    End If
                //    If .txtDatePrevue2 <> "" Then
                //        If sWhere = "" Then
                //            sWhere = sWhere & " {Intervention.DatePrevue}<=dateTime(" & Format(.txtDatePrevue2, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //             sWhere = sWhere & " AND {Intervention.DatePrevue}<=dateTime(" & Format(.txtDatePrevue2, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If


                if (!string.IsNullOrEmpty(General.sSQL))
                {
                    //Report.SetParameterValue("Total2", CalculDesHeures());
                    Report.DataDefinition.FormulaFields[0].Text = "'" + CalculDesHeures() + "'";
                }
                else
                {
                    //Report.SetParameterValue("Total2", "");
                    Report.DataDefinition.FormulaFields[0].Text = "";
                }

                //+{Intervention.CodeImmeuble}
                //var FieldDef1 = Report.Database.Tables["Intervention"].Fields["CodeImmeuble"];
                //Report.DataDefinition.SortFields[0].Field = FieldDef1;
                //Report.DataDefinition.SortFields[0].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;

                ////+{Intervention.DateSaisie}
                //var FieldDef2 = Report.Database.Tables[1].Fields["DateSaisie"];
                //Report.DataDefinition.SortFields[1].Field = FieldDef2;
                //Report.DataDefinition.SortFields[1].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;

                ////+{Intervention.CodeEtat}
                //var FieldDef3 = Report.Database.Tables[1].Fields["CodeEtat"];
                //Report.DataDefinition.SortFields[2].Field = FieldDef3;
                //Report.DataDefinition.SortFields[2].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;

                var ReportForm = new CrystalReportFormView(Report, sWhere);
                ReportForm.Show();

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " ImpressionInterventions ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public string CalculDesHeures()
        {
            string functionReturnValue = null;

            int BisLongTemp = 0;
            int LongTemp = 0;
            float curtemp = default(float);
            string str_Renamed = null;
            DataTable rs = default(DataTable);
            SqlDataAdapter SDArs;
            string strDate = null;
            try
            {
                rs = new DataTable();
                SDArs = new SqlDataAdapter(BisStrRequete, General.adocnn);
                SDArs.Fill(rs);
                LongTemp = 0;
                if (rs.Rows.Count == 0)
                {
                    strDate = "00:00:00";
                    rs = null;
                    return functionReturnValue;
                }
                //----Calcul des heures en secondes
                foreach (DataRow rsRow in rs.Rows)
                {
                    if (rsRow["Duree"] != DBNull.Value)
                    {
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Hour * 3600;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Minute * 60;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Second;
                    }
                }
                rs = null;

                //----Calcul du temps en heures
                curtemp = 0;
                curtemp = (float)LongTemp / (float)3600;
                str_Renamed = Convert.ToString(curtemp);
                if (str_Renamed.Contains("."))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                    str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                    strDate = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains("."))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                        str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                        strDate = strDate + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt32(LongTemp).ToString("00");
                        strDate = strDate + str_Renamed;
                    }
                    else
                    {
                        strDate = strDate + "00:00";
                    }
                }
                else if (str_Renamed.Contains(','))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                    str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                    strDate = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains(','))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                        str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                        strDate = strDate + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt32(LongTemp).ToString("00");
                        strDate = strDate + str_Renamed;
                    }
                    else
                    {
                        strDate = strDate + "00:00";
                    }
                }
                else
                {
                    strDate = str_Renamed + ":00:00";
                }
                functionReturnValue = strDate;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";CalculDesHeures");
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            int nbRecAff = 0;
            ReportDocument CR = new ReportDocument();
            DataTable rsSage = default(DataTable);
            string strSqlSage = null;
            SqlConnection adoConnect = default(SqlConnection);
            string BaseClientSQL = null;
            string BaseClientSage = null;
            string ServeurClient = null;
            string stemp = null;

            string DateMini = null;
            string DateMaxi = null;

            int JourMaxiMois = 0;
            string sqlrequete = null;
            string sCritere = null;

            string sDateFacture = null;
            string sDateEch = null;


            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                    return;
                }

                if (OptParticulier.Checked && Check6.CheckState == CheckState.Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas lancer de relevés pour les particuliers si vous avez coché " + "la case intitulée 'Uniquement les contrats résiliés'.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                ///// ==================> Tested
                if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                {
                    sCritere = General.cCrietereFinanceBis;
                }
                else
                {
                    sCritere = "";
                }

                /// ===============> Tested
                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    General.fc_MajCompteTiersGdp("", "");
                }

                JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12")));

                /// =================> Tested All
                if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                {
                    DateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Now.Year);
                    DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + Convert.ToString(DateTime.Now.Year);
                }
                else
                {
                    if (DateTime.Now.Month >= 1 && DateTime.Now.Month <= Convert.ToInt16(General.MoisFinExercice))
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year - 1);
                        //MoisFinExercice + 1
                        DateMaxi = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice
                    }
                    else
                    {
                        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year);
                        //MoisFinExercice - 1
                        DateMaxi = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Now.Year + 1);
                        //MoisFinExercice
                    }
                }


                //// ================> Tested
                if (!(General.IsDate(txtArretDate.Text)))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtArretDate.Focus();
                    return;
                }
                //// ====================> Tested
                else if (!(General.IsDate(txtEcheance2.Text)))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEcheance2.Focus();
                    return;
                }
                adors = new DataTable();
                SablierOnOff(true);
                int xx = General.Execute("DELETE FROM TempFECRITURE");

                SAGE.fc_OpenConnSage();
                /// ================> Tested All
                if ((SAGE.adoSage != null))
                {
                    if (SAGE.adoSage.State == ConnectionState.Closed)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La connection avec la base Sage est fermée :" + "\n" + "Veuillez contacter votre administrateur réseau.", "Connection Sage impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        BaseClientSage = "[" + SAGE.adoSage.Database + "]";
                        BaseClientSQL = "[" + General.adocnn.Database + "]";
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La connection avec la base Sage n'a pas pu s'établir :" + "\n" + "Veuillez contacter votre administrateur réseau.", "Connection Sage impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                //    Dim I As Long
                //    For I = 0 To adocnn.Properties.Count - 1
                //        Debug.Print I & " : " & adocnn.Properties.Item(I).Name & " : " & adocnn.Properties.Item(I).value
                //    Next I
                //L'indice 70 des propriétés correspond à la valeur "Data Source" sous SQL Server
                ServeurClient = General.adocnn.DataSource;

                adoConnect = new SqlConnection($"SERVER={ServeurClient};DATABASE=master;INTEGRATED SECURITY=TRUE;");

                adoConnect.Open();

                if (OptSyndic.Checked == true)
                {
                    // la condition çi-dessous verifie si un nom de syndic a été saisie
                    // si aucun nom n'est saisie alors la requete renvoie tous les comptes
                    // joint entre la table immeuble et la table F_ECRITUREC
                    // pourvu d'un nom de syndic ou pas.
                    //// ============================> Tested
                    if (string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text) && string.IsNullOrEmpty(txtCommercial.Text))
                    {
                        cmdSsyndic.Enabled = false;
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null)", nbRecAff
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, " + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 )" + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                    " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                    Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                    " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " ) " + " or " + " (" +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" +
                                    " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" +
                                    " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                                    " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //+ " and " + BaseClientSage + General.cCrietereFinanceBis;

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null" + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        else
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," +
                                         " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse," +
                                         " ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1," +
                                         " ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal," +
                                         " GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink ,GerantAdresse2)" +
                                         " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " +
                                         BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " +
                                         BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " +
                                         BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " +
                                         BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " +
                                         BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " +
                                         BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " +
                                         BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " +
                                         BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " +
                                         BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " +
                                         BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " +
                                         BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021 ===> ajouter sqlrequete = sqlrequete
                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                         Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                         " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " ) " + " or " +
                                         " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021 ===> ajouter sqlrequete = sqlrequete
                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null" + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " )" +
                                         " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        Application.DoEvents();

                    }
                    ///=============> Tested
                    else if (string.IsNullOrEmpty(txtSyndic1.Text) && !string.IsNullOrEmpty(txtSyndic2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtSyndic1.Focus();
                        return;
                    }
                    ///=============> Tested
                    else if (!string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtSyndic2.Focus();
                        return;

                    }
                    else if (string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text) && !string.IsNullOrEmpty(txtCommercial.Text))
                    {
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "')", nbRecAff
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE" + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, " + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1," + " GerantComment2, EC_SENS, Temp ,EC_No,EC_NoLink,GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";
                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" + " AND " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'";
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND (SansReleveCompte is null or SansReleveCompte = 0)" +
                                         " ) " + " or " +
                                         " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" +
                                         " AND " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir


                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null";
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        else
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal," + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink ,GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " + BaseClientSage +
                                    ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" + BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'";
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //stemp = stemp & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }

                            stemp = stemp + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " ) " + " or " +
                                    " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" +
                                    BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }
                            stemp = stemp + ")";
                            //===> Fin Modif Mondir

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021 ===> ajouter stemp = stemp
                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis +
                            " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null";
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //stemp = stemp & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }
                            stemp = stemp + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";


                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }

                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        System.Windows.Forms.Application.DoEvents();

                        //            adocnn.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom,ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, Table1.Commercial, Personnel.Nom, Personnel.Prenom, Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Table1.Code1, Table1.Nom, Table1.Adresse1, Table1.CodePostal, Table1.Ville, Table1.ComGerant1, Table1.ComGerant2, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_PIECE " _
                        //'            & "FROM Table1 RIGHT JOIN ((F_ECRITUREC INNER JOIN Immeuble ON F_ECRITUREC.CT_NUM = Immeuble.NCompte) LEFT JOIN Personnel ON Immeuble.CodeCommercial = Personnel.Matricule) ON Table1.Code1 = Immeuble.Code1 " _
                        //'            & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "')"

                    }
                    else
                    {
                        cmdSsyndic.Enabled = false;
                        /// ==============================> Tested
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "')", nbRecAff
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE" + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE," + " EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble," + " ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale," + " Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse," + " GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp " + " ,EC_No,EC_NoLink, GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.Adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" +
                                         " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021 ===> line above is commented
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                         Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                         " And " + BaseClientSQL +
                                         ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" + " And " + BaseClientSQL +
                                         ".dbo.Table1.Code1 <='" +
                                         txtSyndic2.Text + "'" +
                                         " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " ) " +
                                         " or " + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini +
                                         "'" + " AND " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " +
                                         BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_LETTRE = 0 " + " and (" + BaseClientSage +
                                         ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir


                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo14.Text) + "'";
                            }
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercial.Text) + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null " + " And " + BaseClientSQL +
                                         ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" + " And " + BaseClientSQL + ".dbo.Table1.Code1 <='" + txtSyndic2.Text +
                                         "'" + " AND (SansReleveCompte is null or SansReleveCompte = 0)" + " )" +
                                         " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }
                        else
                        /// ======================================> Tested
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal," + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink,GerantAdresse2 ) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.Adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" +
                                    BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";
                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir
                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                    Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                    " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" +
                                    " And " + BaseClientSQL +
                                    ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'" +
                                    " AND (SansReleveCompte is null or SansReleveCompte = 0)" +
                                    " ) " + " or " + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini +
                                    "'" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" +
                                    BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null" +
                                    " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" + " And " + BaseClientSQL +
                                    ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'" + " AND (SansReleveCompte is null or SansReleveCompte = 0)" +
                                    " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }

                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        System.Windows.Forms.Application.DoEvents();

                        //            adocnn.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom,ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, Table1.Commercial, Personnel.Nom, Personnel.Prenom, Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Table1.Code1, Table1.Nom, Table1.Adresse1, Table1.CodePostal, Table1.Ville, Table1.ComGerant1, Table1.ComGerant2, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_PIECE " _
                        //'            & "FROM Table1 RIGHT JOIN ((F_ECRITUREC INNER JOIN Immeuble ON F_ECRITUREC.CT_NUM = Immeuble.NCompte) LEFT JOIN Personnel ON Immeuble.CodeCommercial = Personnel.Matricule) ON Table1.Code1 = Immeuble.Code1 " _
                        //'            & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "')"
                    }

                    fc_ContratResile();

                    adors = new DataTable();
                    // ===================> The Column NoAuto is Added By Mondir, To Force The Update, Must Add It To The Database !
                    SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        //if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString()) 
                        //    && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1900")
                        //{
                        //    adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        //}
                        //else
                        //{
                        //    strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                        //    dateTemp = Convert.ToDateTime(strdatetemp);
                        //    adorsRow["EC_Date"] = dateTemp;
                        //}

                        //'===08 05 2019 mise en commentaire du bloc ci dessus.

                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                           && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1900"
                           && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);//Format(CDate(strdatetemp), FormatDateSansHeureSQL)
                            adorsRow["EC_Date"] = dateTemp;

                            if (Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1900"
                               || Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }
                        }
                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);

                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    boolAutorise = false;
                    for (intIndex = 0; intIndex < tabstrCompte.Length; intIndex++)
                    {
                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);
                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }

                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");
                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;

                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");
                    //Affiche les résultats récapitulatifs

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveDeSyndic);
                    cmdSsyndic.Enabled = true;
                    if (cmdSPart.Enabled == true)
                    {
                        cmdSPart.Enabled = false;
                    }
                }
                else if (OptImmeuble.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtImmeuble1.Text) && string.IsNullOrEmpty(txtImmeuble2.Text) && string.IsNullOrEmpty(txtCommercialImm.Text))
                    {

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) " _
                        //'            & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                        //'            & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL & ".dbo.Table1.Nom, " _
                        //'            & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE " _
                        //'            & " FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM " _
                        //'            & " Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null )"


                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE"
                      + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, "
                      + " EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, "
                      + " ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, "
                      + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1,"
                      + " GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) "
                     + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, "
                     + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                     + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2";


                        if (General.sReleveCoproPoprietaire == "1")//tested
                        {
                            //'=== propriétaire.
                            SQL = SQL + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC INNER JOIN"
                          + " " + BaseClientSQL + ".dbo.Immeuble ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num ="
                          + " " + BaseClientSQL + ".dbo.Immeuble.NCompte INNER JOIN "
                          + " " + BaseClientSQL + ".dbo.Gestionnaire ON " + BaseClientSQL + ".dbo.Immeuble.Code1 "
                          + " = " + BaseClientSQL + ".dbo.Gestionnaire.Code1 AND "
                          + " " + BaseClientSQL + ".dbo.Immeuble.CodeGestionnaire = " + BaseClientSQL + ".dbo.Gestionnaire.CodeGestinnaire INNER JOIN "
                          + " " + BaseClientSQL + ".dbo.Qualification "
                          + " ON " + BaseClientSQL + ".dbo.Gestionnaire.CodeQualif_Qua = " + BaseClientSQL + ".dbo.Qualification.CodeQualif LEFT OUTER JOIN "
                          + " " + BaseClientSQL + ".dbo.Personnel RIGHT OUTER JOIN "
                          + " " + BaseClientSQL + ".dbo.Table1 "
                          + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                          + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1 ";


                        }
                        else
                        {


                            SQL = SQL + " FROM " + BaseClientSage + ".dbo.F_ECRITUREC"
                          + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble "
                          + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num =" + BaseClientSQL + ".dbo.Immeuble.NCompte "
                          + " LEFT OUTER JOIN "
                          + " " + BaseClientSQL + ".dbo.Personnel "
                          + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 "
                          + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                          + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                        }


                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%' ";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }


                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL)
                           + "'" + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            else
                            {
                                SQL = SQL + " ) ";
                            }
                        }

                        SQL = SQL + " or "
                                  + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'"
                                  + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                                  + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                                  + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }

                        SQL = SQL + "AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null "
                            + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            else
                            {
                                SQL = SQL + " ) ";
                            }
                        }


                        SQL = SQL + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                        //Debug.Print SQL

                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        Application.DoEvents();

                    }
                    else if (string.IsNullOrEmpty(txtImmeuble1.Text) && !string.IsNullOrEmpty(txtImmeuble2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fouchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtImmeuble1.Focus();
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtImmeuble1.Text) && string.IsNullOrEmpty(txtImmeuble2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fouchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtImmeuble2.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtImmeuble1.Text) && string.IsNullOrEmpty(txtImmeuble1.Text) && !string.IsNullOrEmpty(txtCommercialImm.Text))
                    {
                        // TODO : Mondir - Controle Not Found On VB6 Label12[0]
                        //Label12[0].Text = "Nombre d'écritures non lettrées";

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE "
                            + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE,"
                            + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal,"
                            + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2,"
                            + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1,"
                            + " GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) "
                            + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                            + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2";

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE "
                               + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE,"
                               + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal,"
                               + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2,"
                               + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1,"
                               + " GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) "
                               + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, "
                               + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                               + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2";


                        if (General.sReleveCoproPoprietaire == "1")//tested
                        {
                            // '=== propriétaire.

                            SQL = SQL + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC INNER JOIN"
                                   + " " + BaseClientSQL + ".dbo.Immeuble ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num ="
                                   + " " + BaseClientSQL + ".dbo.Immeuble.NCompte INNER JOIN "
                                   + " " + BaseClientSQL + ".dbo.Gestionnaire ON " + BaseClientSQL + ".dbo.Immeuble.Code1 "
                                   + " = " + BaseClientSQL + ".dbo.Gestionnaire.Code1 AND "
                                   + " " + BaseClientSQL + ".dbo.Immeuble.CodeGestionnaire = " + BaseClientSQL + ".dbo.Gestionnaire.CodeGestinnaire INNER JOIN "
                                   + " " + BaseClientSQL + ".dbo.Qualification "
                                   + " ON " + BaseClientSQL + ".dbo.Gestionnaire.CodeQualif_Qua = " + BaseClientSQL + ".dbo.Qualification.CodeQualif LEFT OUTER JOIN "
                                   + " " + BaseClientSQL + ".dbo.Personnel RIGHT OUTER JOIN "
                                   + " " + BaseClientSQL + ".dbo.Table1 "
                                   + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                                   + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1 ";


                        }
                        else
                        {
                            SQL = SQL + " FROM " + BaseClientSage + ".dbo.F_ECRITUREC"
                                    + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble "
                                    + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num =" + BaseClientSQL + ".dbo.Immeuble.NCompte "
                                    + " LEFT OUTER JOIN "
                                    + " " + BaseClientSQL + ".dbo.Personnel "
                                    + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 "
                                    + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                                    + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                        }

                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0"
                              + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%' ";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }

                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'";

                        if (!string.IsNullOrEmpty(txtCommercialImm.Text))
                        {
                            //SQL = SQL & " And " & BaseClientSQL & ".dbo.table1.Commercial ='" & txtCommercialImm.Text & "'"
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercialImm.Text) + "'";
                        }

                        SQL = SQL + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            //===> Mondir le 15.01.2021, moved to down, not the right place
                            //else
                            //{
                            //    SQL = SQL + " ) ";
                            //}
                        }
                        //===> Mondir le 15.01.021, moved from top
                        else
                        {
                            SQL = SQL + " ) ";
                        }

                        SQL = SQL + " or "
                                  + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                                  + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                                  + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                                  + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }

                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null ";

                        if (!string.IsNullOrEmpty(txtCommercialImm.Text))
                        {
                            //SQL = SQL & " And " & BaseClientSQL & ".dbo.table1.Commercial  ='" & txtCommercialImm.Text & "'"
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercialImm.Text) + "'";
                        }
                        SQL = SQL + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            //===> Mondir le 15.01.2021, moved to bot
                            //else
                            //{
                            //    SQL = SQL + " ) ";
                            //}
                        }
                        //===> Mondir le 15.01.2021, moved from top
                        else
                        {
                            SQL = SQL + " ) ";
                        }


                        SQL = SQL + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";
                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                        Application.DoEvents();

                    }
                    else
                    //// ===================> Tested
                    {

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) " _
                        //'            & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                        //'            & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL & ".dbo.Table1.Nom, " _
                        //'            & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE " _
                        //'            & " FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble >='" & txtImmeuble1 & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble <='" & txtImmeuble2 & "') " _
                        //'            & " or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM " _
                        //'            & " Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble >='" & txtImmeuble1 & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble <='" & txtImmeuble2 & "')"

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE "
                        + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE,"
                        + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal,"
                        + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2,"
                        + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1,"
                        + " GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) ";


                        SQL = SQL + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, "
                        + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                        + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2";



                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            // '=== propriétaire.
                            SQL = SQL + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC INNER JOIN"
                            + " " + BaseClientSQL + ".dbo.Immeuble ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num ="
                            + " " + BaseClientSQL + ".dbo.Immeuble.NCompte INNER JOIN "
                            + " " + BaseClientSQL + ".dbo.Gestionnaire ON " + BaseClientSQL + ".dbo.Immeuble.Code1 "
                            + " = " + BaseClientSQL + ".dbo.Gestionnaire.Code1 AND "
                            + " " + BaseClientSQL + ".dbo.Immeuble.CodeGestionnaire = " + BaseClientSQL + ".dbo.Gestionnaire.CodeGestinnaire INNER JOIN "
                            + " " + BaseClientSQL + ".dbo.Qualification "
                            + " ON " + BaseClientSQL + ".dbo.Gestionnaire.CodeQualif_Qua = " + BaseClientSQL + ".dbo.Qualification.CodeQualif LEFT OUTER JOIN "
                            + " " + BaseClientSQL + ".dbo.Personnel RIGHT OUTER JOIN "
                            + " " + BaseClientSQL + ".dbo.Table1 "
                            + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                            + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1 ";

                        }
                        else
                        {
                            SQL = SQL + " FROM " + BaseClientSage + ".dbo.F_ECRITUREC"
                               + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble "
                               + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num =" + BaseClientSQL + ".dbo.Immeuble.NCompte "
                               + " LEFT OUTER JOIN "
                               + " " + BaseClientSQL + ".dbo.Personnel "
                               + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 "
                               + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                               + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                        }


                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                        + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                        + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0"
                        + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";
                        //===> Fin Modif Mondir

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;


                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }
                        if (!string.IsNullOrEmpty(txtCommercialImm.Text))
                        {
                            //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercialImm.Text) + "'";
                        }


                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'"
                          + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble >='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble1.Value.ToString()) + "'"
                          + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble <='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble2.Value.ToString()) + "' ";
                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            else
                            {
                                SQL = SQL + " ) ";
                            }
                        }


                        SQL = SQL + " or (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                              + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%' ";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo19.Text) + "'";
                        }
                        if (!string.IsNullOrEmpty(txtCommercialImm.Text))
                        {
                            //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + StdSQLchaine.gFr_DoublerQuote(txtCommercialImm.Text) + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null "
                            + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble >='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble1.Text) + "'"
                            + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble <='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble2.Text) + "'";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }
                            //===> Mondir le 15.01.2021, moved down
                            //else
                            //{
                            //    SQL = SQL + " ) ";
                            //}
                        }
                        //===> Mondir le 15.01.2021, moved from top
                        else
                        {
                            SQL = SQL + " ) ";
                        }

                        SQL = SQL + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                        Application.DoEvents();

                    }

                    fc_ContratResile();


                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    SDAadors.Fill(adors);

                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        //if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString()) && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900")
                        //{
                        //    adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        //}
                        //else
                        //{
                        //    strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                        //    dateTemp = Convert.ToDateTime(strdatetemp);
                        //    adorsRow["EC_Date"] = dateTemp;
                        //}

                        //'===08 05 2019 mise en commentaire du bloc ci dessus.

                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);// Format(CDate(strdatetemp), FormatDateSansHeureSQL)
                            adorsRow["EC_Date"] = dateTemp;
                            if (Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                           || Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }
                        }

                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);

                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    boolAutorise = false;
                    for (intIndex = 0; intIndex <= tabstrCompte.Length - 1; intIndex++)
                    {
                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);

                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }
                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");

                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;
                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveImmeuble);
                }
                else if (OptParticulier.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtParticulier1.Text) && string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        cmdSPart.Enabled = false;

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //             SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " _
                        //'                & " " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier, " _
                        //'                & " " & BaseClientSQL & ".dbo.ImmeublePart.Nom, " & BaseClientSQL & ".dbo.ImmeublePart.Adresse, " & BaseClientSQL & ".dbo.ImmeublePart.CodePostal, " & BaseClientSQL & ".dbo.ImmeublePart.Ville, " & BaseClientSQL & ".dbo.ImmeublePart.Batiment, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodePostal" _
                        //'                & " FROM (" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.ImmeublePart ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.ImmeublePart.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSQL & ".dbo.ImmeublePart.CodeImmeuble = " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble" _
                        //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' ) or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null )"

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse," + " PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, " + " ImmCodePostal, EC_No, EC_NoLink,ImmCommerciale, NomCommerciale, Prenom  )" + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + " " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier, " + " " + BaseClientSQL + ".dbo.ImmeublePart.Nom, " + BaseClientSQL + ".dbo.ImmeublePart.Adresse, " + BaseClientSQL + ".dbo.ImmeublePart.CodePostal, " + BaseClientSQL + ".dbo.ImmeublePart.Ville, " + BaseClientSQL + ".dbo.ImmeublePart.Batiment, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink," + " " + BaseClientSQL + ".dbo.Immeuble.CodeCommercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom" + " FROM (" + BaseClientSage + ".dbo.F_ECRITUREC " + " INNER JOIN " + BaseClientSQL + ".dbo.ImmeublePart " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM = " + BaseClientSQL + ".dbo.ImmeublePart.NCompte)" + " LEFT JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSQL + ".dbo.ImmeublePart.CodeImmeuble = " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble" + " LEFT JOIN " + BaseClientSQL + ".dbo.table1" + " ON " + BaseClientSQL + ".dbo.Immeuble.code1 = " + BaseClientSQL + ".dbo.Table1.Code1" + " LEFT JOIN " + BaseClientSQL + ".dbo.Personnel" + " ON " + BaseClientSQL + ".dbo.table1.commercial =" + BaseClientSQL + ".dbo.Personnel.matricule";
                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" +
                              " AND " + BaseClientSage +
                              ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }

                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo20.Text) + "'";
                        }

                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " + " AND " +
                              BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                              Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "' )" +
                              " or (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" + " AND " +
                              BaseClientSage +
                              ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir


                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo20.Text) + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";


                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                        Application.DoEvents();

                    }
                    else if (string.IsNullOrEmpty(txtParticulier1.Text) && !string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtParticulier1.Focus();
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtParticulier1.Text) && string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtParticulier2.Focus();
                        return;
                    }
                    else
                    {
                        cmdSPart.Enabled = false;

                        //            adoConnect.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, ImmeublePart.CodeParticulier, ImmeublePart.Nom, ImmeublePart.Adresse, ImmeublePart.CodePostal, ImmeublePart.Ville, ImmeublePart.Batiment, F_ECRITUREC.EC_PIECE, Immeuble.Adresse, Immeuble.Ville, Immeuble.CodePostal" _
                        //'                & " FROM (F_ECRITUREC INNER JOIN ImmeublePart ON F_ECRITUREC.CT_NUM = ImmeublePart.NCompte) LEFT JOIN Immeuble ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble" _
                        //'                & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' and ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and ImmeublePart.CodeParticulier <='" & txtParticulier2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null and ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and ImmeublePart.CodeParticulier <='" & txtParticulier2 & "')"


                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)

                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT " & BaseClientSage & ".F_ECRITUREC.dbo.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " _
                        //'                & " " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier, " _
                        //'                & " " & BaseClientSQL & ".dbo.ImmeublePart.Nom, " & BaseClientSQL & ".dbo.ImmeublePart.Adresse, " & BaseClientSQL & ".dbo.ImmeublePart.CodePostal, " & BaseClientSQL & ".dbo.ImmeublePart.Ville, " & BaseClientSQL & ".dbo.ImmeublePart.Batiment, " & BaseClientSage & ".F_ECRITUREC.dbo.EC_PIECE, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodePostal" _
                        //'                & " FROM (" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.ImmeublePart ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.ImmeublePart.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSQL & ".dbo.ImmeublePart.CodeImmeuble = " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble" _
                        //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.dbo.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier <='" & txtParticulier2 & "') " _
                        //'                & " OR (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE IS NULL AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier <='" & txtParticulier2 & "')"

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, " + " EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse," + " PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, " + " ImmCodePostal ,EC_No,EC_Nolink ,ImmCommerciale, NomCommerciale, Prenom )" + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + " " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier, " + " " + BaseClientSQL + ".dbo.ImmeublePart.Nom, " + BaseClientSQL + ".dbo.ImmeublePart.Adresse, " + BaseClientSQL + ".dbo.ImmeublePart.CodePostal, " + BaseClientSQL + ".dbo.ImmeublePart.Ville, " + BaseClientSQL + ".dbo.ImmeublePart.Batiment, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink," + " " + BaseClientSQL + ".dbo.Immeuble.CodeCommercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom" + " FROM (" + BaseClientSage + ".dbo.F_ECRITUREC " + " INNER JOIN " + BaseClientSQL + ".dbo.ImmeublePart " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM = " + BaseClientSQL + ".dbo.ImmeublePart.NCompte)" + " LEFT JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSQL + ".dbo.ImmeublePart.CodeImmeuble = " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble" + " LEFT JOIN " + BaseClientSQL + ".dbo.table1" + " ON " + BaseClientSQL + ".dbo.Immeuble.code1 = " + BaseClientSQL + ".dbo.Table1.Code1" + " LEFT JOIN " + BaseClientSQL + ".dbo.Personnel" + " ON " + BaseClientSQL + ".dbo.table1.commercial =" + BaseClientSQL + ".dbo.Personnel.matricule";
                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" +
                              " AND " + BaseClientSage +
                              ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo20.Text) + "'";
                        }

                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                              Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                              " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier >='" + txtParticulier1.Text +
                              "'" + " and " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier <='" +
                              txtParticulier2.Text + "') " +
                              " OR (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" + " AND " +
                              BaseClientSage +
                              ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo20.Text) + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE IS NULL " + " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier >='" + txtParticulier1.Text + "'" + " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier <='" + txtParticulier2.Text + "')" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";


                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                    }

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    SDAadors.Fill(adors);

                    foreach (DataRow adorsRow in adors.Rows)
                    {

                        //'===08 05 2019 mise en commentaire du bloc ci dessus.
                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                             && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);
                            adorsRow["EC_Date"] = dateTemp;

                            if (Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                             && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }
                        }
                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();


                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);


                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    boolAutorise = false;
                    for (intIndex = 0; intIndex <= tabstrCompte.Length - 1; intIndex++)
                    {

                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);

                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }
                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");
                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;
                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveDeviseur);
                    cmdSPart.Enabled = true;
                    if (cmdSsyndic.Enabled == true)
                    {
                        cmdSsyndic.Enabled = false;
                    }
                }


                //Procédure de remplacement de la date de facture anouveau par la date initiale
                //de celle -ci
                ChangeDate();


                if (General.IsNumeric(txtEcrituresNonLettrees.Text))
                {
                    if (Convert.ToInt32(txtEcrituresNonLettrees.Text) > 0)
                    {
                        CrystalReportFormView Form = new CrystalReportFormView(CR, "");
                        Form.Show();
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Compte(s) soldé(s) sur cette période", "Relevé de compte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    CrystalReportFormView Form = new CrystalReportFormView(CR, "");
                    Form.Show();
                }

                SablierOnOff(false);

                SAGE.fc_CloseConnSage();

                if ((adoConnect != null))
                {
                    if (adoConnect.State != ConnectionState.Closed)
                        adoConnect.Close();
                    adoConnect?.Dispose();
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "Impression des relevés (cmdImprimer) :", false);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Sablier"></param>
        public void SablierOnOff(bool Sablier)
        {
            /// transforme le pointeur en sabier si fleche et inversement
            Application.DoEvents();
            if (Sablier)
            {
                //FrmMain.MousePointer = 11
                this.Cursor = Cursors.WaitCursor;
            }
            else
            {
                //FrmMain.MousePointer = 1
                this.Cursor = Cursors.Arrow;
            }
            Application.DoEvents();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ContratResile()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdoRs = new ModAdo();
            string[] sTabImm = null;
            string[] sTabImmNonResilie = null;
            int i = 0;
            int X = 0;

            try
            {
                if (Check6.CheckState != CheckState.Checked)
                {
                    return;
                }

                //==== procedure pour ne garder uniquement les contrats résilés.
                sSQL = "SELECT DISTINCT ImmCodeImmeuble" + " FROM         TempFECRITURE ";

                rs = modAdoRs.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    modAdoRs.Dispose();
                    return;
                }

                sTabImm = null;
                i = 0;

                //=== stocke les immeubles dans un tableau.
                foreach (DataRow rsRow in rs.Rows)
                {
                    Array.Resize(ref sTabImm, i + 1);
                    sTabImm[i] = rsRow["ImmCodeImmeuble"].ToString();
                    i = i + 1;
                }

                //=== conrtole si l'immeuble a au moins un contrat, si oui on le stocke
                //=== dans le tableau sTabImmNonResilie pour pouvoir le supprimer.
                X = 0;
                for (i = 0; i < sTabImm.Length; i++)
                {
                    sSQL = "SELECT TOP 1    CodeImmeuble " + " From Contrat" + " WHERE     (Resiliee = 0) AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sTabImm[i]) + "')";
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        Array.Resize(ref sTabImmNonResilie, X + 1);
                        sTabImmNonResilie[X] = sSQL;
                        X = X + 1;
                    }
                    else
                    {
                        //=== modif du 14 11 2011, controle si l'immeuble a au moins un contrat
                        //=== sinon on le supprime.
                        sSQL = "SELECT TOP 1    CodeImmeuble " + " From Contrat" + " WHERE  (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sTabImm[i]) + "')";

                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        if (string.IsNullOrEmpty(sSQL))
                        {
                            Array.Resize(ref sTabImmNonResilie, X + 1);
                            sTabImmNonResilie[X] = sTabImm[i];
                            X = X + 1;
                        }
                    }

                }

                modAdoRs.Dispose();

                //=== supprime les immeubles non résiliés.
                for (X = 0; X < sTabImmNonResilie.Length; X++)
                {

                    sSQL = "DELETE  FROM TempFECRITURE where ImmCodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sTabImmNonResilie[X]) + "'";
                    General.Execute(sSQL);
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ContratResile");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public bool fc_Result()
        {

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("Select Distinct CT_NUM FROM TempFECRITURE ", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtNbComptes.Text = General.nz(adors.Rows.Count, 0).ToString();
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();


            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT SUM(TempFECRITURE.EC_MONTANT) AS [MontantTotal] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantTotal.Text = Convert.ToString(Math.Round(Convert.ToDouble(General.nz(adors.Rows[0]["MontantTotal"], 0)), 2));
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT MIN(TempFECRITURE.EC_MONTANT) AS [MontantMin] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantMoins.Text = General.nz(adors.Rows[0]["MontantMin"], 0).ToString();
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT MAX(TempFECRITURE.EC_MONTANT) AS [MontantMax] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantPlus.Text = General.nz(adors.Rows[0]["MontantMax"], 0).ToString();

            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT COUNT(TempFECRITURE.EC_PIECE) AS [TotalPiece] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);


            if (adors.Rows.Count > 0)
            {
                txtEcrituresNonLettrees.Text = Convert.ToString(Math.Round(Convert.ToDouble(General.nz(adors.Rows[0]["TotalPiece"], 0)), 2));

            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            return true;

        }
        /// <summary>
        /// TODO : Mondir - Must Test This Fonction
        /// </summary>
        /// <returns></returns>
        private object ChangeDate()
        {
            object functionReturnValue = null;

            string sqlChange = null;
            string sqlchangeSage = null;
            DataTable rsChange = default(DataTable);
            SqlDataAdapter SDArsChange;
            SqlCommandBuilder SCBrsChange = null;
            DataTable rsChangeSage = default(DataTable);
            SqlDataAdapter SDArsChangeSage = null;
            SqlCommandBuilder SCBrsChangeSage = null;
            bool Match = false;
            int intEC_NoLink = 0;
            int intEC_No = 0;
            int i = 0;

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    return functionReturnValue;
                    //MsgBox "Liaison comptabilité inexistante."
                }

                SAGE.fc_OpenConnSage();

                rsChange = new DataTable();
                rsChangeSage = new DataTable();

                sqlChange = "SELECT EC_No,EC_NoLink,EC_Date FROM TempFEcriture WHERE Ec_NoLink <> 0";

                rsChange = new DataTable();
                SDArsChange = new SqlDataAdapter(sqlChange, General.adocnn);
                SDArsChange.Fill(rsChange);
                Match = false;
                i = 0;
                if (rsChange.Rows.Count > 0)
                {
                    sqlchangeSage = "SELECT EC_No,EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + rsChange.Rows[0]["EC_NoLink"];
                    foreach (DataRow rsChangeRow in rsChange.Rows)
                    {
                        rsChangeSage = new DataTable();
                        SDArsChangeSage = new SqlDataAdapter(sqlchangeSage, SAGE.adoSage);
                        SDArsChangeSage.Fill(rsChangeSage);
                        if (rsChangeSage.Rows.Count > 0)
                        {
                            intEC_NoLink = Convert.ToInt32(rsChangeRow["EC_NoLink"] + "");
                            intEC_No = Convert.ToInt32(rsChangeRow["EC_No"] + "");
                            while (!(Match == true))
                            {
                                i = i + 1;
                                using (var tmpModAdo = new ModAdo())
                                {
                                    if (!string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT EC_No,EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_NoLink, false, SAGE.adoSage)))
                                    {
                                        intEC_No = intEC_NoLink;
                                        using (var tmpModAdo2 = new ModAdo())
                                            intEC_NoLink = Convert.ToInt32(tmpModAdo2.fc_ADOlibelle("SELECT EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_NoLink, false, SAGE.adoSage));
                                        Match = false;
                                    }
                                    else
                                    {
                                        if (i > 1)
                                        {
                                            rsChangeRow["EC_Date"] = tmpModAdo.fc_ADOlibelle("SELECT EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_No, false, SAGE.adoSage);
                                            SCBrsChange = new SqlCommandBuilder(SDArsChange);
                                            SDArsChange.Update(rsChange);
                                            Match = true;
                                        }
                                        else
                                        {
                                            Match = true;
                                        }
                                    }
                                }
                            }
                            Match = false;
                            i = 0;
                        }
                        rsChangeSage?.Dispose();
                        SDArsChangeSage?.Dispose();
                        SCBrsChangeSage?.Dispose();
                        if (rsChange.Rows.Count > 0)
                        {
                            sqlchangeSage = "SELECT EC_No,EC_NoLink,EC_Date  FROM F_ECRITUREC WHERE EC_No=" + rsChangeRow["EC_NoLink"];
                        }
                    }

                    rsChange?.Dispose();
                    SDArsChange?.Dispose();
                    SCBrsChange?.Dispose();

                    rsChangeSage?.Dispose();
                    SDArsChangeSage?.Dispose();
                    SCBrsChangeSage?.Dispose();
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " ImpressionRelevés : Fonction ChangeDate ", false);
            }

            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLogimatique_Click(object sender, EventArgs e)
        {
            frmLogimatique frmLogimatique = new frmLogimatique();
            frmLogimatique.txtTypeExport.Text = "1";
            frmLogimatique.ShowDialog();
            frmLogimatique?.Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheClient_Click(object sender, EventArgs e)
        {
            if (blnAjout == false)
            {

                try
                {
                    string requete = "";
                    string where_order = "";

                    if (General.sAfficheCodeLong == "1")
                    {
                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," + " codePostal as \"CodePostal\", " + " Ville as \"Ville \", commercial as \"commercial \"," +
                                  " CodeLong2 as \"Référence Long\"  FROM Table1 ";
                    }
                    else
                    {
                        //===> Mondir le 04.11.2020, ajout du Adresse , ticket https://groupe-dt.mantishub.io/view.php?id=2044
                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," + "Adresse1 AS \"Adresse\"," + " codePostal as \"CodePostal\", " +
                            " Ville as \"Ville \", commercial as \"commercial \"  FROM Table1 ";
                    }


                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                    fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        fg.Dispose(); fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fc_ChargeEnregistrement(sCode);
                            fg.Dispose(); fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                catch (Exception ex) { Program.SaveException(ex); }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Rechercher()
        {
            if (blnAjout == false)
            {

                try
                {
                    string requete = "";
                    string where_order = "";

                    if (General.sAfficheCodeLong == "1")
                    {
                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                  " codePostal as \"CodePostal\", " +
                                  " Ville as \"Ville \", commercial as \"commercial \"," +
                                  " CodeLong2 as \"Référence Long\"  FROM Table1 ";

                    }
                    else
                    {

                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                  " codePostal as \"CodePostal\", " +
                                  " Ville as \"Ville \", commercial as \"commercial \"  FROM Table1 ";
                    }


                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                    fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fc_ChargeEnregistrement(sCode);
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                }
                catch (Exception e)
                {
                    Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public object fc_ComboCom()
        {
            short i = 0;
            DataTable rstmp = default(DataTable);
            string req = null;

            rstmp = new DataTable();
            ModAdo modAdorstmp;

            req = "SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif,"
                + " Qualification.CodeQualif,Qualification.Qualification "
                + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

            if (General.nbCodeCommercial > 0)
            {
                req = req + " WHERE ";
                for (i = 1; i <= (General.nbCodeCommercial); i++)
                {
                    if (i < (General.nbCodeCommercial))
                    {
                        req = req + "Personnel.CodeQualif='" + StdSQLchaine.gFr_DoublerQuote(General.CodeQualifCommercial[i]) + "' OR ";
                    }
                    else
                    {
                        req = req + "Personnel.CodeQualif='" + StdSQLchaine.gFr_DoublerQuote(General.CodeQualifCommercial[i]) + "'";
                    }
                }
                req = req + " ORDER BY Personnel.Matricule ";
            }

            modAdorstmp = new ModAdo();
            rstmp = modAdorstmp.fc_OpenRecordSet(req);

            if (rstmp.Rows.Count > 0)
            {
                var SSOleDBCmbCommercialSource = new DataTable();
                SSOleDBCmbCommercialSource.Columns.Add("Matricule");
                SSOleDBCmbCommercialSource.Columns.Add("Nom");
                SSOleDBCmbCommercialSource.Columns.Add("Prenom");
                SSOleDBCmbCommercialSource.Columns.Add("Qualification");
                foreach (DataRow rstmpRow in rstmp.Rows)
                {
                    SSOleDBCmbCommercialSource.Rows.Add(rstmpRow["Matricule"], rstmpRow["Nom"], rstmpRow["Prenom"],
                        rstmpRow["Qualification"]);
                    SSOleDBCmbCommercial.DataSource = SSOleDBCmbCommercialSource;
                }
            }
            modAdorstmp.Dispose();
            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheRaisonSociale_Click(object sender, EventArgs e)
        {
            if (blnAjout == false)
            {

                try
                {
                    string requete = "";
                    string where_order = "";

                    if (General.sAfficheCodeLong == "1")
                    {
                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                  " codePostal as \"CodePostal\", " +
                                  " Ville as \"Ville \", commercial as \"commercial \"," +
                                  " CodeLong2 as \"Référence Long\"  FROM Table1 ";

                    }
                    else
                    {

                        requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                  " codePostal as \"CodePostal\", " +
                                  " Ville as \"Ville \", commercial as \"commercial \"  FROM Table1 ";
                    }


                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                    fg.SetValues(new Dictionary<string, string> { { "Nom", txtNom.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fc_ChargeEnregistrement(sCode);
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                }
                catch (Exception ee)
                {
                    Erreurs.gFr_debug(ee, this.Name + ";Command1_Click");
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercheReglement_Click(object sender, EventArgs e)
        {
            try
            {
                string requete =
                    "SELECT CodeReglement.Code as \"code\", CodeReglement.Libelle as \"Libelle\", CodeReglement.ModeReglt as \"Mode de Reglement\" FROM CodeReglement";
                string where_order = "";
                SearchTemplate fg =
                    new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche de factures" };
                fg.SetValues(new Dictionary<string, string> { { "column", txtCodeReglement.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeReglement.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_AfficheReglement(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeReglement.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_AfficheReglement(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                txtCodeReglement.Focus();
            }
            catch (Exception ee)
            {
                Erreurs.gFr_debug(ee, this.Name + ";cmdRechercheReg_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_sauver();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSelectCommerciaux_Click(object sender, EventArgs e)
        {

            int i = 0;
            string sWhere = null;

            frmSelect frmSelect = new frmSelect();
            frmSelect.txtSelect.Text = "SELECT '' As Sélection, Personnel.Matricule AS [Matricule],Personnel.Nom,Personnel.Prenom,SpecifQualif.LibelleQualif AS Qualification FROM Personnel LEFT JOIN SpecifQualif ON Personnel.CodeQualif=SpecifQualif.CodeQualif";
            frmSelect.txtWhere.Text = " WHERE (SpecifQualif.QualifCom=1 OR SpecifQualif.QualifCom=-1)";
            frmSelect.txtOrder.Text = " ORDER BY SpecifQualif.LibelleQualif,Personnel.Nom";
            frmSelect.ShowDialog();
            if (frmSelect.chkAnnuler.CheckState == CheckState.Checked)
            {
                General.tabCodeSelect = new General.tpCodeSelect[1];
            }
            frmSelect.Close();
            sWhere = "";
            if (General.tabCodeSelect.Length != 0)
            {
                for (i = 0; i < General.tabCodeSelect.Length; i++)
                {
                    if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = "{" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR {" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(sWhere))
            {
                txtComDe.Text = "XXXXXX";
                txtComA.Text = "XXXXXX";
                txtReqComCommercial.Text = sWhere;
                cmdVisu1.Visible = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSelectEnergie_Click(object sender, EventArgs e)
        {

            int i = 0;
            string sWhere = null;

            frmSelect frmSelect = new frmSelect();
            frmSelect.txtSelect.Text = "SELECT '' AS Sélection, Energie.Code,Energie.Libelle FROM Energie ";
            frmSelect.txtOrder.Text = " ORDER BY Energie.Code";
            frmSelect.GridSelect.InitializeLayout += (se, ev) =>
            {
                ev.Layout.Bands[0].Columns["Code"].Header.Caption = "Code énergie";
            };
            frmSelect.ShowDialog();
            if (frmSelect.chkAnnuler.CheckState == CheckState.Checked)
            {
                General.tabCodeSelect = new General.tpCodeSelect[1];
            }
            frmSelect.Close();
            if (General.tabCodeSelect.Length != 0)
            {
                for (i = 0; i <= General.tabCodeSelect.Length - 1; i++)
                {
                    if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = "{" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR {" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(sWhere))
            {
                cmbEnergieDe.Text = "XXXXXX";
                cmbEnergieA.Text = "XXXXXX";
                txtReqEnergie.Text = sWhere;
                cmdVisu2.Visible = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSelectGerant_Click(object sender, EventArgs e)
        {
            int i = 0;
            string sWhere = null;
            frmSelect frmSelect = new frmSelect();
            frmSelect.txtSelect.Text = "SELECT '' AS Sélection,Table1.Code1,Table1.Nom FROM Table1 ";
            frmSelect.txtOrder.Text = " ORDER BY Table1.Code1";
            frmSelect.GridSelect.InitializeLayout += (se, ev) =>
            {
                if (frmSelect.GridSelect.DataSource != null)
                    ev.Layout.Bands[0].Columns["Code1"].Header.Caption = "Code Gérant";
            };
            frmSelect.ShowDialog();
            if (frmSelect.chkAnnuler.CheckState == CheckState.Checked)
            {
                General.tabCodeSelect = new General.tpCodeSelect[1];
            }

            frmSelect.Close();
            sWhere = "";
            if (General.tabCodeSelect.Length != 0)
            {
                for (i = 0; i < General.tabCodeSelect.Length; i++)
                {
                    if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = "{" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR {" + General.tabCodeSelect[i].sChamp + "}='" + General.tabCodeSelect[i].sCode + "'";
                        }
                    }
                }
            }
            if (!string.IsNullOrEmpty(sWhere))
            {
                txtGerantDe.Text = "XXXXXX";
                txtGerantA.Text = "XXXXXX";
                txtReqComGerant.Text = sWhere;
                cmdVisu0.Visible = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSPart_Click(object sender, EventArgs e)
        {
            ReportDocument CR = new ReportDocument();
            CR.Load(General.strSyntheseReleveDeviseur);
            CrystalReportFormView ReportForm = new CrystalReportFormView(CR);
            ReportForm.Show();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSsyndic_Click(object sender, EventArgs e)
        {
            ReportDocument CR = new ReportDocument();
            CR.Load(General.strSyntheseReleveDeComerc);
            CrystalReportFormView ReportForm = new CrystalReportFormView(CR);
            ReportForm.Show();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypologie_Click(object sender, EventArgs e)
        {
            if (cmbTypologie.ActiveRow != null)
                lblTypologie.Text = cmbTypologie.ActiveRow.Cells["Designation"].Value.ToString();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisu0_Click(object sender, EventArgs e)
        {
            var Button = sender as Button;
            int Index = Convert.ToInt32(Button.Tag);
            int i = 0;
            General.tabCodeSelect = new General.tpCodeSelect[1];
            string sWhere = null;

            switch (Index)
            {
                case 0:
                    if (string.IsNullOrEmpty(txtReqComGerant.Text))
                        return;

                    if (txtGerantDe.Text == "XXXXXX" && txtGerantA.Text == "XXXXXX")
                    {
                        RetourneTableau(txtReqComGerant.Text);
                    }
                    break;
                case 1:
                    if (string.IsNullOrEmpty(txtReqComCommercial.Text))
                        return;

                    if (txtComDe.Text == "XXXXXX" && txtComA.Text == "XXXXXX")
                    {
                        RetourneTableau(txtReqComCommercial.Text);
                    }
                    break;
                case 2:
                    if (string.IsNullOrEmpty(txtReqEnergie.Text))
                        return;

                    if (cmbEnergieDe.Text == "XXXXXX" && cmbEnergieA.Text == "XXXXXX")
                    {
                        RetourneTableau(txtReqEnergie.Text);
                    }
                    break;
            }

            frmVisu frmVisu = new frmVisu();
            if (Index == 0)
            {
                frmVisu.txtSelect.Text = "SELECT '' AS Sélection,Table1.Code1 AS [Code Gérant],Table1.Nom FROM Table1 ";
                frmVisu.txtOrder.Text = " ORDER BY Table1.Code1";
            }
            else if (Index == 1)
            {
                frmVisu.txtSelect.Text = "SELECT '' AS Sélection,Personnel.Matricule AS [Matricule],Personnel.Nom,Personnel.Prenom,SpecifQualif.LibelleQualif AS Qualification FROM Personnel LEFT JOIN SpecifQualif ON Personnel.CodeQualif=SpecifQualif.CodeQualif";
                frmVisu.txtWhere.Text = " WHERE (SpecifQualif.QualifCom=1 OR SpecifQualif.QualifCom=-1)";
                frmVisu.txtOrder.Text = " ORDER BY SpecifQualif.LibelleQualif,Personnel.Nom";
            }
            else if (Index == 2)
            {
                frmVisu.txtSelect.Text = "SELECT '' AS Sélection,Energie.Code AS [Code énergie],Energie.Libelle FROM Energie ";
                frmVisu.txtOrder.Text = " ORDER BY Energie.Code";
            }
            for (i = 0; i < General.tabCodeSelect.Length; i++)
            {
                if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                {
                    if (string.IsNullOrEmpty(frmVisu.txtWhere.Text))
                    {
                        frmVisu.txtWhere.Text = " WHERE " + General.tabCodeSelect[i].sChamp + "='" + StdSQLchaine.gFr_DoublerQuote(General.tabCodeSelect[i].sCode) + "'";
                    }
                    else
                    {
                        frmVisu.txtWhere.Text = frmVisu.txtWhere.Text + " OR " + General.tabCodeSelect[i].sChamp + "='" + StdSQLchaine.gFr_DoublerQuote(General.tabCodeSelect[i].sCode) + "'";
                    }
                }
            }

            if (string.IsNullOrEmpty(frmVisu.txtWhere.Text))
            {
                if (Index == 0)
                {
                    frmVisu.txtWhere.Text = " WHERE Table1.Code1=''";
                }
                else if (Index == 1)
                {
                    frmVisu.txtWhere.Text = " WHERE Personnel.Matricule=''";
                }
                else if (Index == 2)
                {
                    frmVisu.txtWhere.Text = " WHERE Energie.Code=''";
                }
            }

            frmVisu.ShowDialog();
            frmVisu.Close();

            sWhere = "";
            if (General.tabCodeSelect.Length != 0)
            {
                for (i = 0; i < General.tabCodeSelect.Length; i++)
                {
                    if (!string.IsNullOrEmpty(General.tabCodeSelect[i].sChamp) && General.tabCodeSelect[i].sSelect != 0)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = "{" + General.tabCodeSelect[i].sChamp + "}='" + StdSQLchaine.gFr_DoublerQuote(General.tabCodeSelect[i].sCode) + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " OR {" + General.tabCodeSelect[i].sChamp + "}='" + StdSQLchaine.gFr_DoublerQuote(General.tabCodeSelect[i].sCode) + "'";
                        }
                    }
                }
            }

            switch (Index)
            {
                case 0:
                    if (!string.IsNullOrEmpty(sWhere))
                    {
                        txtGerantDe.Text = "XXXXXX";
                        txtGerantA.Text = "XXXXXX";
                        txtReqComGerant.Text = sWhere;
                    }
                    else
                    {
                        txtGerantDe.Text = "";
                        txtGerantA.Text = "";
                        txtReqComGerant.Text = "";
                    }
                    break;
                case 1:
                    if (!string.IsNullOrEmpty(sWhere))
                    {
                        txtComDe.Text = "XXXXXX";
                        txtComA.Text = "XXXXXX";
                        txtReqComCommercial.Text = sWhere;
                    }
                    else
                    {
                        txtComDe.Text = "";
                        txtComA.Text = "";
                        txtReqComCommercial.Text = "";
                    }
                    break;
                case 2:
                    if (!string.IsNullOrEmpty(sWhere))
                    {
                        cmbEnergieDe.Text = "XXXXXX";
                        cmbEnergieA.Text = "XXXXXX";
                        txtReqEnergie.Text = sWhere;
                    }
                    else
                    {
                        cmbEnergieDe.Text = "";
                        cmbEnergieA.Text = "";
                        txtReqEnergie.Text = "";
                    }
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strChaine"></param>
        private void RetourneTableau(string strChaine)
        {
            string[] tabChamps = null;
            int intIndex = 0;
            int intdeb = 0;
            int intFin = 0;
            string[] tabCol = null;
            string[] tabValeur = null;
            int i = 0;

            //{Code Gérant}='108SEBASTOPOL' OR {Code Gérant}='10TRESOR'

            var R = new Regex("{[0-9a-zA-ZÀ-ÿ-. ]*}");
            foreach (Match M in R.Matches(strChaine))
            {
                if (i > General.tabCodeSelect.Length - 1)
                {
                    Array.Resize(ref General.tabCodeSelect, i + 1);
                }
                General.tabCodeSelect[i].sChamp = M.Value.Replace("{", "").Replace("}", "");
                General.tabCodeSelect[i].sSelect = 1;
                i++;
            }
            R = new Regex("'[0-9a-zA-ZÀ-ÿ-. ]*'");
            i = 0;
            foreach (Match M in R.Matches(strChaine))
            {
                General.tabCodeSelect[i].sCode = M.Value.Replace("'", "");
                i++;
            }

            //for (i = 0; i < tabCol.Length; i++)
            //{
            //    if (General.tabCodeSelect.Length < i)
            //    {
            //        Array.Resize(ref General.tabCodeSelect, i + 1);
            //    }
            //    General.tabCodeSelect[i].sChamp = tabCol[i];
            //    General.tabCodeSelect[i].sCode = tabValeur[i];
            //    General.tabCodeSelect[i].sSelect = 1;
            //}


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            if (DATA1.Rows.Count == 0)
                DATA1.Rows.Add(DATA1.NewRow());
            DATA1.Rows[0]["Commentaire1"] = Text3.Text;
            DATA1.Rows[0]["Commentaire2"] = Text1.Text;
            SCBDATA1 = new SqlCommandBuilder(SDADATA1);
            int xx = SDADATA1.Update(DATA1);
        }
        /// <summary>
        /// Tested ===========> Button Is Always Desabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            fc_supprimer();
        }
        /// <summary>
        /// Tested ===========> Button Is Always Desabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fc_supprimer()
        {
            string sSQL = null;
            try
            {
                if (!string.IsNullOrEmpty(General.nz(txtCode1.Text, "").ToString()))
                {

                    switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("êtes-vous sûr de vouloir supprimer ce client ?", "", MessageBoxButtons.OKCancel))
                    {
                        case DialogResult.OK:
                            break;

                        default:
                            return;

                            //Suppression annulée
                            break;
                    }
                }
                else
                {
                    return;
                }

                // supprime le client
                sSQL = "delete  from table1 where code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                General.Execute(sSQL);
                // supprime les gestionnaires liée au client
                //sSql = ""
                //adocnn.Execute sSql
                // supprime les intervenants liées au client
                //sSql = ""
                //adocnn.Execute sSql
                // initialise ablancs les controles
                fc_clear();
                // bloque les onglets
                fc_BloqueForm();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_supprimer");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirDocClient_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FilDocClient.Tag = DirDocClient.Tag;
            FilesInDirectory(FilDocClient, new DirectoryInfo(e.Node.Tag.ToString()));

            //var p = PointToScreen(new Point(e.Node.Bounds.X, e.Node.Bounds.Y));
            //label2s.Text = p.X + " " + p.Y;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilDocClient_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string sChemin = "";

                if (FilDocClient.SelectedItem != null)
                {
                    if (DirDocClient.SelectedNode != null && DirDocClient.SelectedNode.Tag != null)
                    {
                        sChemin = DirDocClient.SelectedNode.Tag + "\\" + FilDocClient.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }
                    else
                    {
                        sChemin = DirDocClient.Tag + "\\" + FilDocClient.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }

                }

            }
            catch (Exception ee) { Erreurs.gFr_debug(ee, this.Name + ";File1_DblClick"); }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridCLRE.ActiveRow.Cells["code1"].Text == "")
                GridCLRE.ActiveRow.Cells["code1"].Value = txtCode1.Text;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption);
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optCptGerant_CheckedChanged(object sender, EventArgs e)
        {
            if (optCptGerant.Checked)
            {
                ComboComptes.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optEcrituresNonLetrees_CheckedChanged(object sender, EventArgs e)
        {
            if (optEcrituresNonLetrees.Checked)
            {
                fc_ChargeFinance();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptImmeuble_CheckedChanged(object sender, EventArgs e)
        {
            if (OptImmeuble.Checked)
            {

                txtImmeuble1.Text = "";
                txtImmeuble2.Text = "";
                ssCombo19.Text = "";
                // Label4(199) = ""
                txtSyndic1.Visible = false;
                txtSyndic2.Visible = false;
                ssCombo14.Visible = false;
                Label4198.Visible = false;
                Label5726.Visible = false;
                Label5729.Visible = false;
                Label5758.Visible = false;
                txtCommercial.Visible = false;

                cmdSsyndic.Visible = false;

                txtImmeuble1.Visible = true;
                ssCombo19.Visible = true;
                Label4199.Visible = true;
                txtImmeuble2.Visible = true;
                Label5727.Visible = true;
                Label5730.Visible = true;
                Label5725.Visible = true;
                txtCommercialImm.Visible = true;
                txtImmeuble1.Focus();

                txtParticulier1.Visible = false;
                ssCombo20.Visible = false;
                //Label4200.Visible = false;
                txtParticulier2.Visible = false;
                //Label5728.Visible = false;
                //Label5731.Visible = false;
                cmdSPart.Visible = false;

                Label5754.Visible = false;
                Label57200.Visible = false;
                Label5756.Visible = false;

                OptSyndic.Checked = false;
                OptParticulier.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptParticulier_CheckedChanged(object sender, EventArgs e)
        {
            if (OptParticulier.Checked)
            {
                txtSyndic1.Text = "";
                txtSyndic2.Text = "";
                ssCombo14.Text = "";

                txtSyndic1.Visible = false;
                txtSyndic2.Visible = false;
                Label5726.Visible = false;
                Label5729.Visible = false;
                Label5758.Visible = false;
                txtCommercial.Visible = false;

                cmdSsyndic.Visible = false;

                txtImmeuble1.Visible = false;
                ssCombo19.Visible = false;
                Label4199.Visible = false;
                txtImmeuble2.Visible = false;
                Label5727.Visible = false;
                Label5730.Visible = false;
                Label5725.Visible = false;
                txtCommercialImm.Visible = false;

                txtParticulier1.Visible = true;
                ssCombo20.Visible = true;
                Label57200.Visible = true;
                ssCombo20.Visible = true;
                Label57200.Visible = true;
                txtParticulier2.Visible = true;
                Label5754.Visible = true;
                Label5756.Visible = true;
                cmdSPart.Visible = true;
                txtParticulier1.Focus();

                OptSyndic.Checked = false;
                OptImmeuble.Checked = false;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optSelectCom0_CheckedChanged(object sender, EventArgs e)
        {
            var Radio = sender as RadioButton;
            int Index = Convert.ToInt32(Radio.Tag);
            if (Radio.Checked)
            {
                txtGerantDe.AccReadOnly = true;
                txtGerantA.AccReadOnly = true;
                cmdComGerant0.Enabled = false;
                cmdComGerant1.Enabled = false;
                cmdSelectGerant.Enabled = false;
                cmdVisu0.Enabled = false;

                txtComDe.AccReadOnly = true;
                txtComA.AccReadOnly = true;
                cmdComCommercial0.Enabled = false;
                cmdComCommercial1.Enabled = false;
                cmdSelectCommerciaux.Enabled = false;
                cmdVisu1.Enabled = false;

                if (Index == 0)
                {
                    optSelectCom1.Checked = false;
                    txtGerantDe.Text = txtCode1.Text;
                    txtGerantA.Text = txtCode1.Text;
                    txtGerantDe_Validating(txtGerantDe, new CancelEventArgs(false));
                    txtGerantDe.AccReadOnly = false;
                    txtGerantDe.AccReadOnly = false;
                    txtGerantA.AccReadOnly = true;
                    cmdComGerant0.Enabled = true;
                    cmdComGerant1.Enabled = true;
                    cmdSelectGerant.Enabled = true;
                    cmdVisu0.Enabled = true;
                }
                else if (Index == 1)
                {
                    optSelectCom0.Checked = false;
                    txtComDe.Text = SSOleDBCmbCommercial.Text;
                    txtComA.Text = SSOleDBCmbCommercial.Text;
                    txtComDe.AccReadOnly = false;
                    txtComA.AccReadOnly = false;
                    cmdComCommercial0.Enabled = true;
                    cmdComCommercial1.Enabled = true;
                    cmdSelectCommerciaux.Enabled = true;
                    cmdVisu1.Enabled = true;
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optToutesEcritures_CheckedChanged(object sender, EventArgs e)
        {
            if (optToutesEcritures.Checked)
            {
                fc_ChargeFinance();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo14_BeforeDropDown(object sender, CancelEventArgs e)
        {
            var Combo = sender as UltraCombo;
            int Index = Convert.ToInt32(Combo.Tag);
            modAdorsGdpCombo = new ModAdo();

            string sSQL = null;

            switch (Index)
            {
                case 14:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";

                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo14.DataSource = rsGdpCombo;
                    break;
                case 19:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";

                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo19.DataSource = rsGdpCombo;
                    break;
                case 20:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";

                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo20.DataSource = rsGdpCombo;
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCode1.Text) && !string.IsNullOrEmpty(ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString()))
            {
                // stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString());
            }
            View.Theme.Theme.Navigate(typeof(UserDocDevis));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_RechercheQualite(string sCode)
        {
            // récupére dans le controle adodc1 les informations liées au matricule.
            if (ComboQualifGest.DataSource != null)
            {
                if (ComboQualifGest.Rows.Count > 0)
                {
                    for (int i = 0; i < ComboQualifGest.Rows.Count; i++)
                    {
                        if (ComboQualifGest.Rows[i].Cells["CodeQualif"].Value.ToString().ToUpper() == sCode.ToUpper())
                        {
                            if (ssGridGestionnaire.ActiveRow.Cells["Qualification"].Value.ToString() != ComboQualifGest.Rows[i].Cells["Qualification"].Value.ToString())
                                ssGridGestionnaire.ActiveRow.Cells["Qualification"].Value = ComboQualifGest.Rows[i].Cells["Qualification"].Value;
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssGridGestionnaire.ActiveRow == null)
                return;
            string sCode = null;
            if (string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["code1"].Value.ToString()) || string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["code1"].Text))
            {
                ssGridGestionnaire.ActiveRow.Cells["code1"].Value = txtCode1.Text;
            }

            if (string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["CodeQualif_Qua"].Value.ToString()))
            {
                ssGridGestionnaire.ActiveRow.Cells["CodeQualif_Qua"].Value = General.cGestionnaire;
                General.sSQL = "";
                General.sSQL = "SELECT Qualification.Qualification" + " From Qualification" + " WHERE Qualification.CodeQualif ='" + ssGridGestionnaire.ActiveRow.Cells["CodeQualif_QUA"].Value + "'";
                using (var tmpModAdo = new ModAdo())
                {
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        ssGridGestionnaire.ActiveRow.Cells["Qualification"].Value = General.nz(General.rstmp.Rows[0]["Qualification"], "");
                    }
                }
            }

            if (!string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString()))
            {
                fc_RechercheQualite(ssGridGestionnaire.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString());
            }

            if (ssGridGestionnaire.ActiveCell.Column.Key.ToUpper() == "CodeGestinnaire".ToUpper() && !string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["CodeGestinnaire"].Text))
            {
                sCode = General.fc_FormatCode(ssGridGestionnaire.ActiveRow.Cells["CodeGestinnaire"].Text);
                if (string.IsNullOrEmpty(sCode))
                {
                    e.Cancel = true;

                }
                else
                {
                    if (ssGridGestionnaire.ActiveRow.Cells["CodeGestinnaire"].Value.ToString() != sCode)
                        ssGridGestionnaire.ActiveRow.Cells["CodeGestinnaire"].Value = sCode;
                }

            }

            //==== Creation du compte extranet.
            if (ssGridGestionnaire.ActiveCell.Column.Key.ToUpper() == "MotdePasse_GES".ToUpper())
            {
                if (ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text.ToUpper() == txtMotPasse.Text.ToUpper())
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le mot de passe du gestionnaire doit être différent " + " du mot de passe du gérant.", "Mot de passe", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    fc_LogGestExtranet();
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LogGestExtranet()
        {
            DataTable rslogin = default(DataTable);
            ModAdo modAdoDataTable;
            string sSQL = null;

            try
            {
                modAdoDataTable = new ModAdo();
                sSQL = "SELECT Login,PassWord,Gerant, ID_LOgin FROM Login  ";

                if (!string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["ID_LOgin"].Text))
                {

                    rslogin = modAdoDataTable.fc_OpenRecordSet(sSQL + " WHERE ID_LOgin=" + StdSQLchaine.gFr_DoublerQuote(ssGridGestionnaire.ActiveRow.Cells["ID_LOgin"].Text));

                    if (rslogin.Rows.Count > 0)
                    {
                        //== si log existant et mot de passe vide,on supprime
                        if (string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text))
                        {
                            rslogin.Rows[0].Delete();
                            int xx = modAdoDataTable.Update();
                            //== le compte
                            ssGridGestionnaire.ActiveRow.Cells["ID_LOgin"].Value = DBNull.Value;
                        }
                        else
                        {
                            rslogin.Rows[0]["Password"] = ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text;
                            int xx = modAdoDataTable.Update();
                        }

                        //== si log inexsistant et presence
                    }
                    else if (!string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text))
                    {
                        var NewRow = rslogin.NewRow();
                        //== du mot de passe, on crée le compte.
                        NewRow["Login"] = txtCode1.Text;
                        NewRow["Gerant"] = 0;
                        NewRow["Password"] = ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text;
                        rslogin.Rows.Add(NewRow);
                        modAdoDataTable.Update();
                        ssGridGestionnaire.ActiveRow.Cells["ID_LOgin"].Value = rslogin.Rows[rslogin.Rows.Count - 1]["ID_LOgin"].ToString();
                    }



                }
                else if (!string.IsNullOrEmpty(ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text))
                {

                    //            rslogin.Open "SELECT Login.Login,Login.PassWord,Login.Gerant" _
                    //'            & " FROM Login WHERE Login.Login='" & gFr_DoublerQuote(txtCode1) _
                    //'            & "' AND Gerant=0 and password='" & .Columns("MotdePasse_GES").Text & "'", adocnn, adOpenKeyset, adLockOptimistic
                    rslogin = modAdoDataTable.fc_OpenRecordSet(sSQL + " WHERE ID_Login = 0");

                    //            If Not rslogin.EOF And Not rslogin.BOF Then
                    //                 rslogin!Password = .Columns("MotdePasse_GES").Text
                    //                 rslogin.Update
                    //            Else
                    var NewRow = rslogin.NewRow();
                    NewRow["Login"] = txtCode1.Text;
                    NewRow["Gerant"] = 0;
                    NewRow["Password"] = ssGridGestionnaire.ActiveRow.Cells["MotdePasse_GES"].Text;
                    rslogin.Rows.Add(NewRow);
                    int xx = modAdoDataTable.Update();
                    using (var tmpModAdo = new ModAdo())
                        ssGridGestionnaire.ActiveRow.Cells["ID_LOgin"].Value = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(Id_Login) FROM Login"));
                    // .Update
                    //            End If

                }
                modAdoDataTable.Dispose();
                //rslogin.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_LogExtranet");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            General.sSQL = "";
            General.sSQL = "SELECT Qualification.Qualification" + " From Qualification" + " WHERE Qualification.CodeQualif ='" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeQualif_QUA"].Value.ToString()) + "'";
            using (var tmpModAdo = new ModAdo())
            {
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    e.Row.Cells["Qualification"].Value =
                        General.nz(General.rstmp.Rows[0]["Qualification"], "");
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSGridSituation_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            try
            {
                ReportDocument crEcritures = new ReportDocument();
                var SelectionFormula = "";
                CrystalReportFormView Form;
                Cursor.Current = Cursors.AppStarting;
                //// =========================> Tested
                if (General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "TA" ||
                    General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "CM" ||
                    General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "FA" ||
                    General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "TM")
                {
                    crEcritures.Load(General.ETATOLDFACTUREMANUEL);
                    SelectionFormula = "{FactureManuelleEntete.NoFacture}='" + SSGridSituation.ActiveRow.Cells["Référence facture"].Value + "'";
                    Form = new CrystalReportFormView(crEcritures, SelectionFormula);
                    Form.Show();
                }
                else if (General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "CA")
                {
                    crEcritures.Load(General.CHEMINEUROONLY);
                    SelectionFormula = "{FactEnTete.Nofacture}='" + SSGridSituation.ActiveRow.Cells["Référence facture"].Value + "'";
                    Form = new CrystalReportFormView(crEcritures, SelectionFormula);
                    Form.Show();
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception exception)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur d'affichage de la facture : " + SSGridSituation.ActiveRow.Cells["Référence facture"].Value, "Erreur affichage facture", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Erreurs.gFr_debug(exception, this.Name + " SSGridSituation_DblClick ");
            }

        }
        /// <summary>
        /// Tested : This Controle is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSINTERVENANT_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (string.IsNullOrEmpty(SSINTERVENANT.ActiveRow.Cells["code1"].Value.ToString()))
            {
                SSINTERVENANT.ActiveRow.Cells["code1"].Value = txtCode1.Text;
            }
            if (SSINTERVENANT.ActiveCell.Column.Key.ToUpper() == "matricule".ToUpper())
            {
                fc_RechercheMatricule(SSINTERVENANT.ActiveRow, SSINTERVENANT.ActiveRow.Cells["Matricule"].Value.ToString());
            }
        }
        /// <summary>
        /// Tested : The Controle That Call This Function Is Hidden 
        /// </summary>
        /// <param name="Row"></param>
        /// <param name="sMatricule"></param>
        private void fc_RechercheMatricule(UltraGridRow Row, string sMatricule)
        {
            // récupére dans le controle adodc1 les informations liées au matricule.
            if (ssDropMatricule.DataSource != null)
            {
                if (ssDropMatricule.Rows.Count > 0)
                {
                    for (int i = 0; i < ssDropMatricule.Rows.Count; i++)
                    {
                        if (ssDropMatricule.Rows[i].Cells["matricule"].Value.ToString().ToUpper() == sMatricule.ToUpper())
                        {
                            Row.Cells["Nom"].Value = ssDropMatricule.Rows[i].Cells["Nom"].Value;
                            Row.Cells["CodeQualif"].Value = ssDropMatricule.Rows[i].Cells["CodeQualif"].Value;
                            Row.Cells["Qualification"].Value = ssDropMatricule.Rows[i].Cells["Qualification"].Value;
                            Row.Cells["NumRadio"].Value = ssDropMatricule.Rows[i].Cells["NumRadio"].Value;
                            Row.Cells["Note_NOT"].Value = ssDropMatricule.Rows[i].Cells["Note_NOT"].Value;
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested : This Controle is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSINTERVENANT_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            fc_RechercheMatricule(e.Row, e.Row.Cells["Matricule"].Value.ToString());
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            if (ssIntervention.ActiveCell.Column.Key.ToUpper() == "CodeEtat".ToUpper())
            {
                if (e.Cell.OriginalValue.ToString().ToUpper() != e.NewValue.ToString().ToUpper())
                {
                    General.fc_HistoStatut(Convert.ToInt32(ssIntervention.ActiveRow.Cells["NoIntervention"].Text), ssIntervention.ActiveRow.Cells["codeimmeuble"].Text,
                        "Fiche client", e.Cell.OriginalValue.ToString(), ssIntervention.ActiveRow.Cells["CodeEtat"].Text);
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCode1.Text))
            {
                // stocke la position de la fiche client
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);

                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, e.Row.Cells["NoIntervention"].Value.ToString());
            }
            View.Theme.Theme.Navigate(typeof(UserIntervention));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleGridImmeuble_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCode1.Text) && !string.IsNullOrEmpty(SSOleGridImmeuble.ActiveRow.Cells["CodeImmeuble"].Text))
            {
                // stocke la position de la fiche client
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, SSOleGridImmeuble.ActiveRow.Cells["CodeImmeuble"].Text);

                View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleGridImmeuble_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir le 11.05.2021, reported by mail Jules
            if (Convert.ToDouble(e.Row.Cells["Résilie"].Value) == 2)
            {
                e.Row.CellAppearance.BackColor = Color.FromArgb(192, 255, 192);
                e.Row.CellAppearance.ForeColor = Color.Black;
            }
            else if (Convert.ToDouble(e.Row.Cells["Résilie"].Value) == 1)
            {
                e.Row.CellAppearance.BackColor = Color.FromArgb(255, 128, 128);
                e.Row.CellAppearance.ForeColor = Color.Black;
            }
            else
            {
                e.Row.CellAppearance.BackColor = Color.White;
                e.Row.Cells["CodeImmeuble"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
                e.Row.Cells["CodeGestionnaire"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
                e.Row.Cells["EnergieLibelle"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
                e.Row.Cells["Commercial"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
            }

            //===>  Mondir le 11.05.2021,lines bellow commented by Mondir
            //if (Convert.ToBoolean(e.Row.Cells["Resilie"].Value) == true && Convert.ToBoolean(e.Row.Cells["Contrat"].Value) == true)
            //{
            //    var colorFromReg = "0x" + General.gfr_liaison("CouleurContratResil", "0x8080ff").ToLower();
            //    e.Row.CellAppearance.BackColor = ColorTranslator.FromOle(Convert.ToInt32(colorFromReg, 16));
            //    e.Row.CellAppearance.ForeColor = Color.Black;
            //}
            //else if (Convert.ToBoolean(e.Row.Cells["Resilie"].Value) == false && Convert.ToBoolean(e.Row.Cells["Contrat"].Value))
            //{
            //    var colorFromReg = "0x" + General.gfr_liaison("CouleurContratOK", "11186720");
            //    e.Row.CellAppearance.BackColor = ColorTranslator.FromOle(Convert.ToInt32(colorFromReg, 16));
            //    e.Row.CellAppearance.ForeColor = Color.Black;
            //}
            //else
            //{
            //    e.Row.CellAppearance.BackColor = ColorTranslator.FromOle(0xffffff);
            //    e.Row.Cells["Code Immeuble"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
            //    e.Row.Cells["Gestionnaire"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
            //    e.Row.Cells["Energie"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
            //    e.Row.Cells["Commercial"].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
            //}

            //e.Row.Cells[0].Appearance.ForeColor = Color.FromArgb(30, 136, 229);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_Validating(object sender, CancelEventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                General.sSQL = "SELECT TypeCodeEtat.LibelleCodeEtat" + " From TypeCodeEtat" +
                               " WHERE TypeCodeEtat.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'";
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    lblLibStatus.Text = General.nz(General.rstmp.Rows[0]["LibelleCodeEtat"], "").ToString();
                }
                else
                {
                    lblLibStatus.Text = "";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresse1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
            cmdChangeAdresse.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
            chkAdresseModifiee.CheckState = System.Windows.Forms.CheckState.Checked;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresse2_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
            cmdChangeAdresse.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
            chkAdresseModifiee.CheckState = System.Windows.Forms.CheckState.Checked;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode1_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            DataTable rsRechCode1 = default(DataTable);
            ModAdo modAdorsRechCode1;
            string sSqlCode1 = null;

            if (KeyAscii == 13 && blnAjout == false)
            {
                sSqlCode1 = "SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 like '%" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "%'";
                modAdorsRechCode1 = new ModAdo();
                rsRechCode1 = modAdorsRechCode1.fc_OpenRecordSet(sSqlCode1);
                if (rsRechCode1.Rows.Count > 1 || rsRechCode1.Rows.Count == 0)
                {
                    General.blControle = true;
                    fc_Rechercher();
                }
                else if (rsRechCode1.Rows.Count == 1)
                {
                    fc_ChargeEnregistrement(txtCode1.Text);
                }
                KeyAscii = 0;
                modAdorsRechCode1.Dispose();
            }

            if (KeyAscii == 0)
            {
                e.Handled = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode1_Validating(object sender, CancelEventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                string sCode = null;
                if (General.blControle == false)
                {
                    if (txtCode1.Text != txtSelCode1.Text)
                    {
                        // controle si codev client Existant
                        sCode = txtCode1.Text;
                        General.sSQL = "SELECT Table1.Code1" + " From Table1" + " WHERE Table1.Code1 ='" +
                                       StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                        General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                        if (General.rstmp.Rows.Count > 0)
                        {
                            // si enregistrement éxiste déja
                            if (blnAjout == true)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Client déjà existant", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                            sCode = General.rstmp.Rows[0]["Code1"].ToString();
                            fc_ChargeEnregistrement(sCode);
                            txtCode1.Text = sCode;
                        }
                        else if (blnAjout == false && !string.IsNullOrEmpty(txtCode1.Text))
                        {
                            fc_Rechercher();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCode1temp_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                if (bLoadChange == false)
                {
                    if (!string.IsNullOrEmpty(txtCode1temp.Text))
                    {
                        txtCode1.Text = txtCode1temp.Text;

                        General.saveInReg(Variable.cUserDocClient, "OptEt", Convert.ToString(OptEt.Checked));
                        General.saveInReg(Variable.cUserDocClient, "OptOu", Convert.ToString(OptOu.Checked));

                        txtCode1_KeyPress(txtCode1, new System.Windows.Forms.KeyPressEventArgs((char)13));
                    }
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";txtCode1temp_Change");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodePostal_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
            cmdChangeAdresse.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
            chkAdresseModifiee.CheckState = System.Windows.Forms.CheckState.Checked;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeReglement_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeReglement_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;

            if (KeyAscii == 13)
            {
                General.blControle = true;
                CmdRechercheReglement_Click(CmdRechercheReglement, new System.EventArgs());
                KeyAscii = 0;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtComA_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            if (txtComDe.Text == "XXXXXX")
            {
                txtComDe.Text = "";
                cmdVisu1.Visible = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtComDe_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            if (txtComA.Text == "XXXXXX")
            {
                txtComA.Text = "";
                cmdVisu1.Visible = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercial_Click(object sender, EventArgs e)
        {
            //    txtSyndic1.Text = ""
            //   txtSyndic2.Text = ""
            OptSyndic.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {

            if (txtCommercial.Rows.Count == 0)
            {

                General.sSQL = "SELECT DISTINCT Personnel.Matricule, Personnel.Nom " + " FROM Personnel INNER JOIN Table1 ON Personnel.Matricule=Table1.Commercial ORDER BY Personnel.Matricule";

                sheridan.InitialiseCombo(txtCommercial, General.sSQL, "Matricule", false);

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercialImm_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (txtCommercialImm.Rows.Count == 0)
            {

                General.sSQL = "SELECT DISTINCT Personnel.Matricule, Personnel.Nom " + " FROM Personnel INNER JOIN Immeuble ON Personnel.Matricule=Immeuble.CodeCommercial ORDER BY Personnel.Matricule";

                sheridan.InitialiseCombo(txtCommercialImm, General.sSQL, "Matricule", false);

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtEmail_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFax_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGerantA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtGerantDe.Text == "XXXXXX")
            {
                txtGerantDe.Text = "";
                cmdVisu0.Visible = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtGerantDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtGerantA.Text == "XXXXXX")
            {
                txtGerantA.Text = "";
                cmdVisu0.Visible = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtinterDe_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtinterDe.Text) && !General.IsDate(txtinterDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                e.Cancel = true;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntereAu_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIntereAu.Text) && !General.IsDate(txtIntereAu.Text))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                e.Cancel = true;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtNom_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                short KeyAscii = (short)e.KeyChar;
                string strCodeGerant = null;
                if (KeyAscii == 13 && blnAjout == false)
                {
                    strCodeGerant = General.nz(tmpModAdo.fc_ADOlibelle("SELECT Table1.Code1 FROM Table1 WHERE Table1.Nom='" + StdSQLchaine.gFr_DoublerQuote(txtNom.Text) + "'"), "").ToString();
                    if (!string.IsNullOrEmpty(strCodeGerant))
                    {
                        fc_ChargeEnregistrement(strCodeGerant);
                    }
                    else
                    {
                        General.blControle = true;
                        if (blnAjout == false)
                        {

                            try
                            {
                                string requete = "";
                                string where_order = "";

                                if (General.sAfficheCodeLong == "1")
                                {
                                    requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                              " codePostal as \"CodePostal\", " +
                                              " Ville as \"Ville \", commercial as \"commercial \"," +
                                              " CodeLong2 as \"Référence Long\"  FROM Table1 ";

                                }
                                else
                                {

                                    requete = "SELECT " + "Code1 as \"Ref. Gerant\", Nom AS \"Raison sociale\"," +
                                              " codePostal as \"CodePostal\", " +
                                              " Ville as \"Ville \", commercial as \"commercial \"  FROM Table1 ";
                                }


                                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                                fg.ugResultat.DoubleClickRow += (se, ev) =>
                                {
                                    string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fc_ChargeEnregistrement(sCode);
                                    fg.Dispose();
                                    fg.Close();
                                };

                                fg.ugResultat.KeyDown += (se, ev) =>
                                {

                                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                    {
                                        string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                        fc_ChargeEnregistrement(sCode);
                                        fg.Dispose();
                                        fg.Close();
                                    }
                                };
                                fg.StartPosition = FormStartPosition.CenterParent;
                                fg.ShowDialog();
                                SSTab1.SelectedTab = SSTab1.Tabs[0];
                            }
                            catch (Exception ee)
                            {
                                Erreurs.gFr_debug(ee, this.Name + ";Command1_Click");
                            }

                        }
                    }
                    KeyAscii = 0;
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtObservations_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtTelephone_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtVille_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
            cmdChangeAdresse.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
            chkAdresseModifiee.CheckState = System.Windows.Forms.CheckState.Checked;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //bool VisibleExecuted = false;
        private void UserDocClient_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");

                //VisibleExecuted = true;

                //ImmeubleMenu = new MainMenuSubItem("Immeuble", (se, ev) =>
                //{
                //    if (View.Theme.Theme.MenuGrid.Visible)
                //        View.Theme.Theme.MenuGrid.Visible = false;
                //    else
                //        View.Theme.Theme.MenuGrid.Visible = true;
                //    if (View.Theme.Theme.MenuGrid.Visible)
                //    {
                //        View.Theme.Theme.MenuGrid.DoubleClickRow -= MenuGrid_DoubleClickRow;
                //        View.Theme.Theme.MenuGrid.DoubleClickRow += MenuGrid_DoubleClickRow;
                //        fc_ChargeGridImmeuble();
                //        int W = View.Theme.Theme.MenuGrid.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>()
                //            .Where(ele => ele.Hidden == false)
                //            .Sum(ele => ele.Width);
                //        View.Theme.Theme.MenuGrid.Width = W + 40;
                //    }
                //});

                //lblGoFicheAppelante = new MainMenuSubItem("", (se, ev) =>
                //    {
                //        GoFicheAppelante(ModParametre.sFicheAppelante);
                //    })
                //{ Visible = false };
                //lblNavigation2 = new MainMenuSubItem("Météo", (se, ev) =>
                //{
                //    Process.Start(General.sLien2);
                //});
                //lblNavigation0 = new MainMenuSubItem("Gecet", (se, ev) =>
                //{
                //    Process.Start(General.sLien0);
                //});
                //lblNavigation1 = new MainMenuSubItem("Google", (se, ev) =>
                //{
                //    Process.Start(General.sLien1);
                //});
                //View.Theme.Theme.ShowSubMenu(lblNavigation2, lblNavigation0, lblNavigation1,
                //    new MainMenuSubItem("Fiche d'Appel", (se, ev) => { }),
                //    ImmeubleMenu,
                //    new MainMenuSubItem("Gérants", (se, ev) => { }),
                //    new MainMenuSubItem("Dispatching Intervention", (se, ev) => { }),
                //    new MainMenuSubItem("Dispatching Devis", (se, ev) => { })
                //    , lblGoFicheAppelante);


                int i = 0;

                bLoadChange = true;

                //=== version dédié à pz.
                if (General.sActiveVersionPz == "1")
                {
                    txt100.Visible = true;
                    Label4187.Visible = true;
                    txt101.Visible = true;
                    Label4188.Visible = true;
                    Label4190.Visible = true;
                    OptOu.Visible = true;
                    OptEt.Visible = true;
                    ssFindClient.Visible = true;

                    Frame1.Visible = false;

                }
                else
                {
                    txt100.Visible = false;
                    Label4187.Visible = false;
                    txt101.Visible = false;
                    Label4188.Visible = false;
                    Label4190.Visible = false;
                    OptOu.Visible = false;
                    OptEt.Visible = false;
                    ssFindClient.Visible = false;
                    Frame1.Visible = true;

                }

                SSTab1.SelectedTab = SSTab1.Tabs[0];
                if (General.gfr_liaison("Affiche Logimatique", "0") == "0")
                {
                    cmdLogimatique.Visible = false;
                }
                else
                {
                    cmdLogimatique.Visible = true;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                General.open_conn();

                //Charge dans la combo Commercial tous les commerciaux
                fc_ComboCom();

                cmbCoteAmour.DataSource = null;
                DataTable cmbCoteAmourSource = new DataTable();
                cmbCoteAmourSource.Columns.Add("");
                ///=====================> Tested
                for (i = 1; i <= 10; i++)
                {
                    cmbCoteAmourSource.Rows.Add(Convert.ToString(i));
                }
                cmbCoteAmour.DataSource = cmbCoteAmourSource;


                // Partie relevé de compte

                if (General.adocnn.State == ConnectionState.Open)
                {
                    DATA1 = new DataTable();
                    SDADATA1 = new SqlDataAdapter("SELECT * FROM Commentaire", General.adocnn.ConnectionString);
                    SDADATA1.Fill(DATA1);

                    if (DATA1.Rows.Count > 0)
                    {
                        Text3.Text = DATA1.Rows[DATA1.Rows.Count - 1]["Commentaire1"].ToString();
                        Text1.Text = DATA1.Rows[DATA1.Rows.Count - 1]["Commentaire2"].ToString();
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La Connection avec la base de données est infructieuse", "Connection non établie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Adodc17 = new DataTable();
                SDAAdodc17 = new SqlDataAdapter("SELECT CodeImmeuble,NCompte FROM IMMEUBLE order by CodeImmeuble", General.CHEMINBASE);
                SDAAdodc17.Fill(Adodc17);

                Adodc20 = new DataTable();
                SDAAdodc20 = new SqlDataAdapter("SELECT CodeImmeuble,NCompte FROM IMMEUBLE order by CodeImmeuble", General.CHEMINBASE);
                SDAAdodc20.Fill(Adodc20);

                Adodc16 = new DataTable();
                SDAAdodc16 = new SqlDataAdapter("SELECT Code1 FROM Table1 order by Code1", General.CHEMINBASE);
                SDAAdodc16.Fill(Adodc16);

                Adodc19 = new DataTable();
                SDAAdodc19 = new SqlDataAdapter("SELECT Code1 FROM Table1 order by Code1", General.CHEMINBASE);
                SDAAdodc19.Fill(Adodc19);

                Adodc18 = new DataTable();
                SDAAdodc18 = new SqlDataAdapter("SELECT CodeParticulier,CodeImmeuble,NCompte FROM ImmeublePart order by CodeParticulier", General.CHEMINBASE);
                SDAAdodc18.Fill(Adodc18);

                Adodc15 = new DataTable();
                SDAAdodc15 = new SqlDataAdapter("SELECT CodeParticulier,CodeImmeuble,NCompte FROM ImmeublePart order by CodeParticulier", General.CHEMINBASE);
                SDAAdodc15.Fill(Adodc15);

                txtSyndic1.DataSource = Adodc16;
                txtSyndic2.DataSource = Adodc19;
                txtImmeuble1.DataSource = Adodc17;
                txtImmeuble2.DataSource = Adodc20;
                txtParticulier1.DataSource = Adodc18;
                txtParticulier2.DataSource = Adodc15;

                fc_ChargeExercices(General.MoisFinExercice);

                //- positionne sur le dernier enregistrement.
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    // charge les enregistrements
                    fc_ChargeEnregistrement(ModParametre.sNewVar);
                }
                // fc_ChargeEnregistrement "BEDOS"
                // alimente la liste déroulante de la grille ssintervenant avec la table personnel


                bLoadChange = false;
                OptEt.Checked = General.getFrmReg(Variable.cUserDocClient, "OptEt") == "Vrai";
                OptOu.Checked = General.getFrmReg(Variable.cUserDocClient, "OptOu") == "Vrai";


                fc_VisibleLong();


                ModMain.bActivate = false;
                if (General.sReleveCoproPoprietaire == "1")
                {
                    Frame327.Visible = true;
                    //'Frame3(28).Visible = True;
                    //Frame328.Top = 2760;
                    //OptParticulier.Top = 2760;
                }

                else
                {

                    Frame327.Visible = false;
                    ////'Frame3(28).Visible = False
                    //OptParticulier.Top = 2400;
                    //Frame328.Top = 2400;
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMoisFin"></param>
        /// <returns></returns>
        private object fc_ChargeExercices(string sMoisFin)
        {
            object functionReturnValue = null;

            string sqlExercices = null;
            DataTable rsExercices = default(DataTable);
            SqlDataAdapter SDArsExercices;

            string strAddItem = null;
            string JourMaxiMois = null;
            string DateMini = null;
            string DateMaxi = null;
            int i = 0;
            bool blnMatch = false;

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {
                return functionReturnValue;
                // MsgBox "Liaison comptabilité inexistante."
            }

            blnMatch = false;
            SAGE.fc_OpenConnSage();
            rsExercices = new DataTable();

            sqlExercices = "SELECT DISTINCT YEAR(JM_Date) AS Annee";
            sqlExercices = sqlExercices + " FROM F_JMOUV";
            sqlExercices = sqlExercices + " ORDER BY YEAR(JM_Date) DESC";

            SDArsExercices = new SqlDataAdapter(sqlExercices, SAGE.adoSage);
            SDArsExercices.Fill(rsExercices);

            JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt16(sMoisFin)));

            if (sMoisFin == "12" || !General.IsNumeric(sMoisFin))
            {
                DateMini = "01/" + "01" + "/";
                DateMaxi = JourMaxiMois + "/12/";
            }
            else
            {
                if (sMoisFin == "12")
                {
                    DateMini = "01/" + "01" + "/";
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString("00") + "/";
                    //MoisFinExercice
                }
                else
                {
                    DateMini = "01/" + (Convert.ToInt16(sMoisFin) + 1).ToString("00") + "/";
                    //MoisFinExercice - 1
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString("00") + "/";
                    //MoisFinExercice
                }
            }

            var cmbExercicesSource = new DataTable();
            cmbExercicesSource.Columns.Add("Exercice");
            cmbExercicesSource.Columns.Add("Du");
            cmbExercicesSource.Columns.Add("Au");

            var NewRow = cmbExercicesSource.NewRow();

            if (rsExercices.Rows.Count > 0)
            {
                foreach (DataRow rsExercicesRow in rsExercices.Rows)
                {
                    if (Convert.ToInt32(rsExercicesRow["Annee"]) >= DateTime.Now.Year)
                    {
                        if (sMoisFin != "12")
                        {
                            if (blnMatch == false && Convert.ToInt16(General.nz(sMoisFin, "1")) < Convert.ToInt16(DateTime.Now.Month))
                            {
                                cmbExercicesSource.Rows.Add(
                                    rsExercicesRow["Annee"] + "/" + (Convert.ToInt32(rsExercicesRow["Annee"]) + 1),
                                    DateMini + rsExercicesRow["Annee"],
                                    DateMaxi + (Convert.ToInt32(rsExercicesRow["Annee"]) + 1));
                                cmbExercices.DataSource = cmbExercicesSource;
                                blnMatch = true;
                            }
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt32(rsExercicesRow["Annee"]) - 1) + "/" + rsExercicesRow["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt32(rsExercicesRow["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];
                        }
                        else if (sMoisFin == "12")
                        {
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = Convert.ToInt32(rsExercicesRow["Annee"]);
                            NewRow["Du"] = DateMini + (rsExercicesRow["Annee"]);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];
                        }
                    }
                    else
                    {
                        if (sMoisFin == "12")
                        {
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = Convert.ToInt32(rsExercicesRow["Annee"]);
                            NewRow["Du"] = DateMini + (rsExercicesRow["Annee"]);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];
                        }
                        else
                        {
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt32(rsExercicesRow["Annee"]) - 1) + "/" + rsExercicesRow["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt32(rsExercicesRow["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];
                        }
                    }
                    cmbExercicesSource.Rows.Add(NewRow);
                    cmbExercices.DataSource = cmbExercicesSource;
                }
            }

            for (i = 0; i <= cmbExercices.Rows.Count - 1; i++)
            {
                cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"];
                if (General.IsDate(cmbExercices.Rows[i].Cells["Du"].ToString()) && General.IsDate(cmbExercices.Rows[i].Cells["Au"].ToString()))
                {
                    if (Convert.ToDateTime(cmbExercices.Rows[i].Cells["Du"].Value) <= DateTime.Now && Convert.ToDateTime(cmbExercices.Rows[i].Cells["Au"].Value) >= DateTime.Today)
                    {
                        cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"].Value;
                        cmbExercices.ActiveRow.Cells[1].Value = cmbExercices.Rows[i].Cells[1].Value;
                        cmbExercices.ActiveRow.Cells[2].Value = cmbExercices.Rows[i].Cells[1].Value;
                        break;
                    }
                }
                if (i == cmbExercices.Rows.Count - 1)
                {
                    cmbExercices.Value = cmbExercices.Rows[0].Cells["Exercice"].Value;
                }
            }

            SAGE.fc_CloseConnSage();
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// Mondir : This Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSINTERVENANT_AfterRowUpdate(object sender, RowEventArgs e)
        {
            modAdorsIntervenant.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_AfterRowUpdate(object sender, RowEventArgs e)
        {
            modAdorsCorrespondants.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (e.Cell.DataChanged)
            {
                int xx = General.Execute($"UPDATE Intervention SET {e.Cell.Column.Key} = '{StdSQLchaine.gFr_DoublerQuote(e.Cell.Value.ToString())}' WHERE NoIntervention = '{ssIntervention.ActiveRow.Cells["NoIntervention"].Value}'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSINTERVENANT_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Matricule_per"].Header.Caption = "Matricule";
            e.Layout.Bands[0].Columns["Nom"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["Nom"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CodeQualif"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["CodeQualif"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Qualification"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["NumRadio"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["NumRadio"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Note_NOT"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["Note_NOT"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["NumRadio"].Header.Caption = "N° Radio";
            e.Layout.Bands[0].Columns["Note_NOT"].Header.Caption = "Note";

            fc_DropMaticule();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["NomGestion"].Header.Caption = "Nom";
            e.Layout.Bands[0].Columns["NomGestion"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CodeQualif_Qua"].Header.Caption = "CodeQualif";
            e.Layout.Bands[0].Columns["CodeQualif_Qua"].MaxLength = 50;
            e.Layout.Bands[0].Columns["TelGestion"].Header.Caption = "Tel.";
            e.Layout.Bands[0].Columns["TelGestion"].MaxLength = 50;
            e.Layout.Bands[0].Columns["TelPortable_GES"].Header.Caption = "Tel Portable";
            e.Layout.Bands[0].Columns["TelPortable_GES"].MaxLength = 50;
            e.Layout.Bands[0].Columns["TelBureau"].Header.Caption = "Tel. Bureau";
            e.Layout.Bands[0].Columns["TelBureau"].MaxLength = 50;
            e.Layout.Bands[0].Columns["FaxGestion"].Header.Caption = "Fax";
            e.Layout.Bands[0].Columns["FaxGestion"].MaxLength = 50;
            e.Layout.Bands[0].Columns["EMail_GES"].Header.Caption = "EMail";
            e.Layout.Bands[0].Columns["EMail_GES"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Horaire_GES"].Header.Caption = "Horaire";
            e.Layout.Bands[0].Columns["Horaire_GES"].MaxLength = 50;
            e.Layout.Bands[0].Columns["MotdePasse_GES"].Header.Caption = "Mot de Passe";
            e.Layout.Bands[0].Columns["MotdePasse_GES"].MaxLength = 50;
            e.Layout.Bands[0].Columns["AncienEmployeur"].Header.Caption = "Anscien Employeur";
            e.Layout.Bands[0].Columns["AncienEmployeur"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Qualification"].CellActivation = Activation.ActivateOnly;
            e.Layout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["code1"].Hidden = true;
            e.Layout.Bands[0].Columns["ID_LOGIN"].Hidden = true;

            fc_DropQualifGest();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CSecteur"].Header.Caption = "Chef Secteur";
            e.Layout.Bands[0].Columns["CodeDepanneur"].Header.Caption = "Depanneur";
            e.Layout.Bands[0].Columns["Wave"].CellAppearance.Image = Properties.Resources.volume_up_4_16;
            e.Layout.Bands[0].Columns["Wave"].Header.Caption = "Rapport Vocal";
            e.Layout.Bands[0].Columns["Commentaire"].Hidden = true;
            e.Layout.Bands[0].Columns["Article"].Hidden = true;
            e.Layout.Bands[0].Columns["Nom"].Hidden = true;
            e.Layout.Bands[0].Columns["CptREndu_INT"].Hidden = true;
            e.Layout.Bands[0].Columns["LibelleCodeEtat"].Hidden = true;
            e.Layout.Bands[0].Columns["NoIntervention"].Hidden = true;
            e.Layout.Bands[0].Columns["INT_Rapport1"].Hidden = true;
            e.Layout.Bands[0].Columns["Duree"].Hidden = true;
            e.Layout.Bands[0].Columns["INT_AnaActivite"].Hidden = true;
            e.Layout.Bands[0].Columns["Prioritaire"].Hidden = true;
            e.Layout.Bands[0].Columns["MontantHT"].Hidden = true;
            e.Layout.Bands[0].Columns["Prior"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeImmeuble"].CellActivation = Activation.NoEdit;

            fc_DropMaticule();
            fc_DropCodeetat();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "Immeuble";
            e.Layout.Bands[0].Columns["NumeroDevis"].Header.Caption = "N° Devis";
            e.Layout.Bands[0].Columns["CodeEtat"].Header.Caption = "CodeEtat";
            e.Layout.Bands[0].Columns["DateCreation"].Header.Caption = "Date";
            e.Layout.Bands[0].Columns["DateCreation"].Format = "dd/MM/yyyy hh:mm:ss";
            e.Layout.Bands[0].Columns["CodeDeviseur"].Header.Caption = "Deviseur";
            e.Layout.Bands[0].Columns["Code1"].Header.Caption = "Gerant";
            e.Layout.Bands[0].Columns["Observations"].Header.Caption = "Commentaire";
            e.Layout.Bands[0].Columns["TitreDevis"].Header.Caption = "Titre du Devis";
            e.Layout.Bands[0].Columns["CodeTitre"].Hidden = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CLRE_Date"].Header.Caption = "Date";
            e.Layout.Bands[0].Columns["CLRE_Contact"].Header.Caption = "Contact";
            e.Layout.Bands[0].Columns["CLRE_Contact"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CLRE_Tel"].Header.Caption = "Tel";
            e.Layout.Bands[0].Columns["CLRE_Tel"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CLRE_Fax"].Header.Caption = "Fax";
            e.Layout.Bands[0].Columns["CLRE_Fax"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CLRE_Mail"].Header.Caption = "Mail";
            e.Layout.Bands[0].Columns["CLRE_Mail"].MaxLength = 100;
            e.Layout.Bands[0].Columns["CLRE_DateVisite"].Header.Caption = "Date Visite";
            e.Layout.Bands[0].Columns["CLRE_Commentaire"].Header.Caption = "Commentaire";
            e.Layout.Bands[0].Columns["CLRE_Commentaire"].MaxLength = 200;
            e.Layout.Bands[0].Columns["CLRE_Noauto"].Hidden = true;
            e.Layout.Bands[0].Columns["Code1"].Hidden = true;

            fc_DropMaticule();
        }
        /// <summary>
        /// Tested - Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuGrid_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (txtCode1.Text != "")
            {
                //    ' stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocClient, txtCode1.Text);
                //    ' stocke les paramétres pour la feuille de destination.
                //ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocImmeuble, View.Theme.Theme.MenuGrid.ActiveRow.Cells["CodeImmeuble"].Value.ToString());
            }
            //View.Theme.Theme.Navigate(typeof(FicheImmeuble));
        }
        /// <summary>
        /// Tested - Controle Is Hidden
        /// </summary>
        private void fc_ChargeGridImmeuble()
        {
            //View.Theme.Theme.MenuGrid.DataSource = null;
            var tmpModAdo = new ModAdo();
            General.sSQL = "";
            General.sSQL = "SELECT Immeuble.CodeImmeuble, ISNULL(RaisonSocial_IMM, 0) AS RaisonSocial_IMM" + " From Immeuble" +
                           " WHERE Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
            General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
            var tmpDataTable = new DataTable();
            tmpDataTable.Columns.Add("CodeImmeuble");
            tmpDataTable.Columns.Add("RaisonSocial_IMM", typeof(bool));
            foreach (DataRow Row in General.rstmp.Rows)
            {
                var NewRow = tmpDataTable.NewRow();
                NewRow[0] = Row[0];
                NewRow[1] = Row[1].ToString() == "1";
                tmpDataTable.Rows.Add(NewRow);
            }
            if (General.rstmp.Rows.Count > 0)
            {
            }
            //View.Theme.Theme.PositionMenuGrid(ImmeubleMenu);
        }
        /// <summary>
        /// TODO : Mondir - Ask Rachid And Test This Funtion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFindClient_AfterCloseUp(object sender, EventArgs e)
        {
            if (ssFindClient.ActiveRow != null)
                txtCode1temp.Text = ssFindClient.ActiveRow.Cells["Code1"].Text;
        }
        /// <summary>
        /// TODO : Mondir - Ask Rachid And Test This Funtion
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFindClient_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string sSQL = null;
                string[] tTAb = null;
                string sWhere = null;
                string stemp = null;
                string sT = null;

                DataTable rs = default(DataTable);
                ModAdo modAdors;

                int i = 0;
                int j = 0;

                if (bLoadChange == false)
                {

                    sT = ssFindClient.Text.Trim();
                    stemp = "";
                    if (sT.Length > 0)
                    {


                        for (i = 1; i <= sT.Length; i++)
                        {
                            if (General.Mid(sT, i, 1) == " ")
                            {
                                if (j == 0)
                                {
                                    //== enléve les espaces doublées.
                                    stemp = stemp + General.Mid(sT, i, 1);
                                    j = 1;
                                }
                            }
                            else
                            {
                                j = 0;
                                stemp = stemp + General.Mid(sT, i, 1);
                            }
                        }

                        tTAb = stemp.Split(' ');

                        sWhere = "";

                        for (i = 0; i < tTAb.Length; i++)
                        {
                            if (i == 0)
                            {
                                sWhere = " WHERE Nom like '%" + tTAb[i] + "%'";
                            }
                            else
                            {
                                if (OptOu.Checked == true)
                                {
                                    sWhere = sWhere + " OR Nom like '%" + tTAb[i] + "%'";
                                }
                                else if (OptEt.Checked == true)
                                {
                                    sWhere = sWhere + " AND Nom like '%" + tTAb[i] + "%'";
                                }
                            }
                        }

                        //If Right(sWhere, 1) <> "'" Then
                        //    sWhere = sWhere & "'"
                        //End If
                        //sWhere = sWhere & ")"
                    }
                    else
                    {
                        ssFindClient.DataSource = null;
                        return;
                    }

                    sSQL = "SELECT TOP(10) Nom AS [Raison Social], Code1, Adresse1, Ville FROM Table1" + " " + sWhere + "";

                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    ssFindClient.DataSource = null;
                    ssFindClient.DataSource = rs;

                    modAdors?.Dispose();

                    //SendMessage ssFindClient.hWnd, BM_CLICK, 0, ""
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";ssFindClient_Change");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Fiche"></param>
        private void GoFicheAppelante(string Fiche)
        {
            // sauve les paramètres de la fiche gerant
            if (!string.IsNullOrEmpty(txtCode1.Text))
            {
                // stocke la position de la fiche gerant
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
            }


            switch (Fiche)
            {

                case Variable.cUserDocStandard:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(Appel.UserDocStandard));
                    break;

                case Variable.cUserIntervention:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(UserIntervention));
                    break;

                case Variable.cUserDocDevis:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(UserDocDevis));
                    break;

            }

        }
        /// <summary>
        /// Testé
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypologie_TextChanged(object sender, EventArgs e)
        {
            if (cmbTypologie.Rows.Count == 0)
            {
                cmbTypologie_BeforeDropDown(sender, null);
            }
            using (var tmpModAdo = new ModAdo())
                lblTypologie.Text = tmpModAdo.fc_ADOlibelle("SELECT Designation FROM TypeTypologie WHERE CodeTypologie=" + General.nz(cmbTypologie.Text, "0"));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = modAdorsCLRE.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCLRE_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsCLRE.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBCmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            if (SSOleDBCmbCommercial.ActiveRow == null)
                return;
            Label5742.Text = SSOleDBCmbCommercial.ActiveRow.Cells["Nom"].Value.ToString();
            Label5740.Text = SSOleDBCmbCommercial.ActiveRow.Cells["Prenom"].Value.ToString();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            FC_ChargeOnglet();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsCorrespondants.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeReglement_Validating(object sender, CancelEventArgs e)
        {
            if (!General.blControle)
            {
                e.Cancel = fc_AfficheReglement(txtCodeReglement.Text, true);
            }
        }

        private void cmdImpressionImm_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCode1.Text))
                    return;
                if (Dossier.fc_ControleFichier(General.gsRpt + General.EtatImmeublesClient) == false)
                    return;
                var ReportDocument = new ReportDocument();
                ReportDocument.Load(General.gsRpt + General.EtatImmeublesClient);
                var Form = new CrystalReportFormView(ReportDocument, "{Table1.Code1}='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");
                Form.Show();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                General.gfr_liaison(this.Name + " cmdImpressionImm_Click ");
            }
        }

        private void SSOleGridImmeuble_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //e.Layout.Bands[0].Columns["Code Immeuble"].CellAppearance.ForeColor = Color.FromArgb(30, 136, 229);
            //var Rows = SSOleGridImmeuble.Rows;
            e.Layout.Bands[0].Columns["Résilie"].Hidden = true;
            //===> Mondir le 12.10.2021, reported by juls via email
            e.Layout.Bands[0].Columns["P1"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["P2"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["P3"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["P4"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["P1"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["P2"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["P3"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["P4"].DefaultCellValue = false;
            //===> Fin Modif Mondir
        }

        private void ssGridGestionnaire_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CodeGestinnaire"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Code gestionnaire est obligatoire.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //e.Row.CancelUpdate();
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Code1"].DataChanged)
            {
                if (e.Row.Cells["Code1"].OriginalValue.ToString() != e.Row.Cells["Code1"].Text
               || e.Row.Cells["CodeGestinnaire"].OriginalValue.ToString() != e.Row.Cells["CodeGestinnaire"].Text)
                    using (var tmpModAdo = new ModAdo())
                    {
                        var exists = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Gestionnaire WHERE Code1 = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Code1"].Text.ToString())}' AND CodeGestinnaire = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeGestinnaire"].Text.ToString())}'"));
                        if (exists > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
            }

        }

        private void UserDocClient_SizeChanged(object sender, EventArgs e)
        {
            //var f = new Font("Microsoft Sans Serif", 8.25f, FontStyle.Regular);

            //float widthRatio = Width / 778f;
            //float heightRatio = Height / 632f;

            //SizeF scale = new SizeF(widthRatio, heightRatio);
            //this.Scale(scale);
            //ugResultat.Font = new Font("Microsoft Sans Serif", f.SizeInPoints * heightRatio * widthRatio);

            //foreach (Control control in this.Controls)
            //{

            //}

            //req(this, f, widthRatio, heightRatio);

            //MessageBox.Show(Width + " " + Height);
        }

        private void req(Control C, Font f, float widthRatio, float heightRatio)
        {
            foreach (Control c in C.Controls)
            {
                c.Font = new Font(C.Font.FontFamily.Name, f.SizeInPoints * heightRatio * widthRatio);

                if (c.HasChildren)
                    req(c, f, widthRatio, heightRatio);
            }
        }

        private void ultraDateTimeEditor1_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;
            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;
            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;
            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;
            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null) return;
            txtDevisDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_VisibleChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null) return;
            txtDevisDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null) return;
            txtDevisAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_ValueChanged(object sender, EventArgs e)
        {

            if (ultraDateTimeEditor4.Value == null) return;
            txtDevisAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ssIntervention_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow == null) return;
            var noInter = ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString();
            General.saveInReg(Variable.cUserDocClient, "NoIntervention", noInter);
        }

        private void ssDevis_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssDevis.ActiveRow == null) return;
            var noDevis = ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString();
            General.saveInReg(Variable.cUserDocClient, "NumeroDevis", noDevis);
        }

        //===> Mondir le 13.10.2020 et 14.10.2020, demande de Rachid, Drag & Drop
        //===> Mondir le 20.10.2020 added drag & drop from outlook demande de Rachid
        private TreeNode CopyNode = null;
        private void DirDocClient_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (CopyNode == null || e.Data == null)
                    return;

                new frmCopie(CopyNode.Tag.ToString(), e.Data).ShowDialog();

                fc_OngletDocumention();
            }
            catch
            {

            }
        }

        protected TreeNode FindTreeNode(int x, int y)
        {

            var node = DirDocClient.GetNodeAt(PointToClient(new Point(x - 13, y - 216)));
            return node;

        }

        private void DirDocClient_DragOver(object sender, DragEventArgs e)
        {
            try
            {
                e.Effect = DragDropEffects.Copy;
                TreeNode aNode = FindTreeNode(e.X, e.Y);
                //label3s.Text = e.X + " " + e.Y;
                if (aNode != null)
                {
                    CopyNode = aNode;
                }
            }
            catch
            {

            }
        }

        private void DirDocClient_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                DirDocClient.SelectedNode = e.Node;
        }

        private void collerToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            try
            {
                var node = DirDocClient.SelectedNode;
                if (node == null)
                {
                    CustomMessageBox.Show("Veuillez sélectionner un dossier de destination", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                IDataObject data_object = Clipboard.GetDataObject();

                if (data_object == null)
                    return;

                new frmCopie(node.Tag.ToString(), data_object).ShowDialog();

                fc_OngletDocumention();
            }
            catch
            {

            }
        }

        private void buttonPastFiles_Click(object sender, EventArgs e)
        {
            collerToolStripMenuItem_Click_1(null, null);
        }
        //===> Fin Modif Mondir
    }
}
