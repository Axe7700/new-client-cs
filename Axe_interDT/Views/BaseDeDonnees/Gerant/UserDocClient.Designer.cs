using iTalk;

namespace Axe_interDT.Views.BaseDeDonnees.Gerant
{
    partial class UserDocClient
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance247 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance248 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance249 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance250 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance251 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance252 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance253 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance254 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance255 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance256 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance257 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance258 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance259 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance260 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance261 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocClient));
            Infragistics.Win.Appearance appearance262 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance263 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance264 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance265 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance266 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance267 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance268 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance269 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance270 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance271 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance272 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance273 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance274 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance275 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance276 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance277 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance278 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance279 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance280 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance281 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance282 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance283 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance284 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance285 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance286 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance287 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance288 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance289 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance290 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance291 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance292 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance293 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance294 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance295 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance296 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance297 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance298 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance299 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance300 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance301 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance302 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance303 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance304 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance305 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance306 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance307 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance308 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance309 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance310 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance311 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance312 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance313 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance314 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance315 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance316 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance317 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance318 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance319 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance320 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance321 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance322 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance323 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance324 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance325 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance326 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance327 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance328 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance329 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance330 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance331 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance332 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance333 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance334 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance335 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance336 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance337 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance338 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance339 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance340 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance341 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance342 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance343 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance344 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance345 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance346 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance347 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance348 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance349 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance350 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance351 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance352 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance353 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance354 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance355 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance356 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance357 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance358 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance359 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance360 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance361 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance362 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance363 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance364 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance365 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance366 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance367 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance368 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance369 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance370 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance371 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance372 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance373 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance374 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance375 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance376 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance377 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance378 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance379 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance380 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance381 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance382 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance383 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance384 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance385 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance386 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance387 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance388 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance389 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance390 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance391 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance392 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance393 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance394 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance395 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance396 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance397 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance398 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance399 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance400 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance401 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance402 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance403 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance404 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance405 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab12 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab14 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab15 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance406 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance407 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance408 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance409 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance410 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance411 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance412 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance413 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance414 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance415 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance416 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance417 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl14 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.panel11 = new System.Windows.Forms.Panel();
            this.panel12 = new System.Windows.Forms.Panel();
            this.Label1 = new System.Windows.Forms.Label();
            this.panel13 = new System.Windows.Forms.Panel();
            this.txtSoldeCompte = new System.Windows.Forms.Label();
            this.panel14 = new System.Windows.Forms.Panel();
            this.txtTotalRegle = new System.Windows.Forms.Label();
            this.panel15 = new System.Windows.Forms.Panel();
            this.Label0 = new System.Windows.Forms.Label();
            this.panel16 = new System.Windows.Forms.Panel();
            this.Label2 = new System.Windows.Forms.Label();
            this.panel17 = new System.Windows.Forms.Panel();
            this.txtTotalFacture = new System.Windows.Forms.Label();
            this.tableLayoutPanel41 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.ComboComptes = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.optCptImmeuble = new System.Windows.Forms.RadioButton();
            this.optCptGerant = new System.Windows.Forms.RadioButton();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.optToutesEcritures = new System.Windows.Forms.RadioButton();
            this.optEcrituresNonLetrees = new System.Windows.Forms.RadioButton();
            this.Frame719 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbExercices = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdEcritures = new System.Windows.Forms.Button();
            this.SSGridSituation = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel52 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame327 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel34 = new System.Windows.Forms.TableLayoutPanel();
            this.opt1 = new System.Windows.Forms.RadioButton();
            this.Opt2 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel55 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel56 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.cmdSPart = new System.Windows.Forms.Button();
            this.OptParticulier = new System.Windows.Forms.RadioButton();
            this.Label5754 = new System.Windows.Forms.Label();
            this.ssCombo20 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtParticulier1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label57200 = new System.Windows.Forms.Label();
            this.Label5756 = new System.Windows.Forms.Label();
            this.txtParticulier2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel54 = new System.Windows.Forms.TableLayoutPanel();
            this.OptImmeuble = new System.Windows.Forms.RadioButton();
            this.Label5727 = new System.Windows.Forms.Label();
            this.ssCombo19 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtImmeuble1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label5730 = new System.Windows.Forms.Label();
            this.Label4199 = new System.Windows.Forms.Label();
            this.txtImmeuble2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label5725 = new System.Windows.Forms.Label();
            this.txtCommercialImm = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel53 = new System.Windows.Forms.TableLayoutPanel();
            this.OptSyndic = new System.Windows.Forms.RadioButton();
            this.Label5726 = new System.Windows.Forms.Label();
            this.txtSyndic1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label5729 = new System.Windows.Forms.Label();
            this.txtSyndic2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssCombo14 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label5758 = new System.Windows.Forms.Label();
            this.txtCommercial = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Label4198 = new System.Windows.Forms.Label();
            this.cmdSsyndic = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.Text1 = new iTalk.iTalk_TextBox_Small2();
            this.Command1 = new System.Windows.Forms.Button();
            this.Text3 = new iTalk.iTalk_TextBox_Small2();
            this.frmResultat = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMontantMoins = new iTalk.iTalk_TextBox_Small2();
            this.txtMontantTotal = new iTalk.iTalk_TextBox_Small2();
            this.label52 = new System.Windows.Forms.Label();
            this.txtNbComptes = new iTalk.iTalk_TextBox_Small2();
            this.txtMontantPlus = new iTalk.iTalk_TextBox_Small2();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txtEcrituresNonLettrees = new iTalk.iTalk_TextBox_Small2();
            this.Label19 = new System.Windows.Forms.Label();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.optRelEcritureLettree = new System.Windows.Forms.RadioButton();
            this.optRelEcritureNonLettree = new System.Windows.Forms.RadioButton();
            this.label18 = new System.Windows.Forms.Label();
            this.txtArretDate = new iTalk.iTalk_TextBox_Small2();
            this.txtEcheance2 = new iTalk.iTalk_TextBox_Small2();
            this.Check6 = new System.Windows.Forms.CheckBox();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel35 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.chkSyndic3 = new System.Windows.Forms.CheckBox();
            this.chkSyndic0 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.txtTotalComm = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.SSOleGridImmeuble = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.label16s = new System.Windows.Forms.Label();
            this.txtObservations = new iTalk.iTalk_TextBox_Small2();
            this.cmdImpressionImm = new System.Windows.Forms.Button();
            this.Frame70 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel9 = new System.Windows.Forms.Panel();
            this.txtAdresse1 = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresse2 = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtVille = new iTalk.iTalk_TextBox_Small2();
            this.txtCodePostal = new iTalk.iTalk_TextBox_Small2();
            this.label11 = new System.Windows.Forms.Label();
            this.panel10 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdLogimatique = new System.Windows.Forms.Button();
            this.cmdChangeAdresse = new System.Windows.Forms.Button();
            this.chkAdresseModifiee = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame70s = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtEmail = new iTalk.iTalk_TextBox_Small2();
            this.label13s = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTelephone = new iTalk.iTalk_TextBox_Small2();
            this.txtFax = new iTalk.iTalk_TextBox_Small2();
            this.label12s = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.chkSyndic1 = new System.Windows.Forms.CheckBox();
            this.label25 = new System.Windows.Forms.Label();
            this.chkSyndic2 = new System.Windows.Forms.CheckBox();
            this.label43s = new System.Windows.Forms.Label();
            this.txtMotPasse = new iTalk.iTalk_TextBox_Small2();
            this.txtLogin = new iTalk.iTalk_TextBox_Small2();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdChangeCommercial = new System.Windows.Forms.Button();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.Label5740 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.Label5742 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.SSOleDBCmbCommercial = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ssDropMatricule = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSINTERVENANT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ComboQualifGest = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssGridGestionnaire = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label12 = new System.Windows.Forms.Label();
            this.Text2 = new iTalk.iTalk_TextBox_Small2();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeReglement = new iTalk.iTalk_TextBox_Small2();
            this.lblLibCodeReglement = new iTalk.iTalk_TextBox_Small2();
            this.CmdRechercheReglement = new System.Windows.Forms.Button();
            this.txt5 = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.chkEditRel_TAB = new System.Windows.Forms.CheckBox();
            this.chkTransmissionBI_TAB = new System.Windows.Forms.CheckBox();
            this.chkOSOblig_TAB = new System.Windows.Forms.CheckBox();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbModeTrans_TAB = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label16 = new System.Windows.Forms.Label();
            this.lblLibModeTrans_TAB = new System.Windows.Forms.Label();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.SSTab2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl10 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel40 = new System.Windows.Forms.TableLayoutPanel();
            this.label36 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.label75 = new System.Windows.Forms.Label();
            this.txtReqEnergie = new iTalk.iTalk_TextBox_Small2();
            this.cmbEnergieA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdVisu2 = new System.Windows.Forms.Button();
            this.label74 = new System.Windows.Forms.Label();
            this.cmbEnergieDe = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdSelectEnergie = new System.Windows.Forms.Button();
            this.tableLayoutPanel48 = new System.Windows.Forms.TableLayoutPanel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.optSelectCom1 = new System.Windows.Forms.RadioButton();
            this.cmdComCommercial1 = new System.Windows.Forms.Button();
            this.txtComA = new iTalk.iTalk_TextBox_Small2();
            this.txtReqComCommercial = new iTalk.iTalk_TextBox_Small2();
            this.cmdComCommercial0 = new System.Windows.Forms.Button();
            this.label72 = new System.Windows.Forms.Label();
            this.txtComDe = new iTalk.iTalk_TextBox_Small2();
            this.label73 = new System.Windows.Forms.Label();
            this.cmdSelectCommerciaux = new System.Windows.Forms.Button();
            this.cmdVisu1 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.tableLayoutPanel33 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.cmdComPrint = new System.Windows.Forms.Button();
            this.tableLayoutPanel47 = new System.Windows.Forms.TableLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.optSelectCom0 = new System.Windows.Forms.RadioButton();
            this.cmdComGerant1 = new System.Windows.Forms.Button();
            this.txtGerantA = new iTalk.iTalk_TextBox_Small2();
            this.label70 = new System.Windows.Forms.Label();
            this.cmdComGerant0 = new System.Windows.Forms.Button();
            this.txtReqComGerant = new iTalk.iTalk_TextBox_Small2();
            this.txtGerantDe = new iTalk.iTalk_TextBox_Small2();
            this.label71 = new System.Windows.Forms.Label();
            this.cmdSelectGerant = new System.Windows.Forms.Button();
            this.cmdVisu0 = new System.Windows.Forms.Button();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel32 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTypologie = new System.Windows.Forms.Label();
            this.cmbTypologie = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdTypologie = new System.Windows.Forms.Button();
            this.chkListeRouge = new System.Windows.Forms.CheckBox();
            this.chkPrioritaire = new System.Windows.Forms.CheckBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.cmbCoteAmour = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.frmFactures = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.lblEcTrav = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.FrameFioul = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel37 = new System.Windows.Forms.TableLayoutPanel();
            this.label62 = new System.Windows.Forms.Label();
            this.lblN1Fioul = new System.Windows.Forms.Label();
            this.lblEcFioul = new System.Windows.Forms.Label();
            this.lblNFioul = new System.Windows.Forms.Label();
            this.lblNTrav = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.lblN1Trav = new System.Windows.Forms.Label();
            this.Label356 = new System.Windows.Forms.Label();
            this.Label357 = new System.Windows.Forms.Label();
            this.Label3516 = new System.Windows.Forms.Label();
            this.lblEcCont = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.lblNCont = new System.Windows.Forms.Label();
            this.lblN1Cont = new System.Windows.Forms.Label();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel49 = new System.Windows.Forms.TableLayoutPanel();
            this.FilDocClient = new System.Windows.Forms.ListBox();
            this.DirDocClient = new System.Windows.Forms.TreeView();
            this.contextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.collerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.tableLayoutPanel38 = new System.Windows.Forms.TableLayoutPanel();
            this.info1 = new Axe_interDT.Views.Theme.Info();
            this.buttonPastFiles = new System.Windows.Forms.Button();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Frame1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel42 = new System.Windows.Forms.TableLayoutPanel();
            this.Label4187 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txt100 = new iTalk.iTalk_TextBox_Small2();
            this.cmbArticle = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label28 = new System.Windows.Forms.Label();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.txt4 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel43 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel44 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbStatus = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblLibStatus = new System.Windows.Forms.Label();
            this.tableLayoutPanel45 = new System.Windows.Forms.TableLayoutPanel();
            this.lbllibIntervenant = new System.Windows.Forms.Label();
            this.cmbIntervenant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel46 = new System.Windows.Forms.TableLayoutPanel();
            this.txtinterDe = new iTalk.iTalk_TextBox_Small2();
            this.label24 = new System.Windows.Forms.Label();
            this.txtIntereAu = new iTalk.iTalk_TextBox_Small2();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label22 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.cmbCodeimmeuble = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.label25s = new System.Windows.Forms.Label();
            this.ssIntervention = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl9 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel50 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbDevis = new System.Windows.Forms.Button();
            this.txt101 = new iTalk.iTalk_TextBox_Small2();
            this.cmbDevisIntervenant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label31 = new System.Windows.Forms.Label();
            this.Label4188 = new System.Windows.Forms.Label();
            this.cmbDevisImmeuble = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel51 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDevisDe = new iTalk.iTalk_TextBox_Small2();
            this.txtDevisAu = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.ultraDateTimeEditor3 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.ultraDateTimeEditor4 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txt1 = new iTalk.iTalk_TextBox_Small2();
            this.label55 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.cmdDevisArticle = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label34 = new System.Windows.Forms.Label();
            this.cmbDevisStatus = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssDevis = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.GridCLRE = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssDropCodeEtat = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.label2s = new System.Windows.Forms.Label();
            this.label3s = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.cmd0 = new System.Windows.Forms.Button();
            this.cmdRechercheClient = new System.Windows.Forms.Button();
            this.cmdRechercheRaisonSociale = new System.Windows.Forms.Button();
            this.Label5744 = new System.Windows.Forms.Label();
            this.Label43 = new System.Windows.Forms.Label();
            this.OptEt = new System.Windows.Forms.RadioButton();
            this.OptOu = new System.Windows.Forms.RadioButton();
            this.Label4190 = new System.Windows.Forms.Label();
            this.ssFindClient = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txt0 = new iTalk.iTalk_TextBox_Small2();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.txtNom = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel36 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCode1temp = new iTalk.iTalk_TextBox_Small2();
            this.txt3 = new iTalk.iTalk_TextBox_Small2();
            this.txtSelCode1 = new iTalk.iTalk_TextBox_Small2();
            this.ultraTabPageControl14.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.panel13.SuspendLayout();
            this.panel14.SuspendLayout();
            this.panel15.SuspendLayout();
            this.panel16.SuspendLayout();
            this.panel17.SuspendLayout();
            this.tableLayoutPanel41.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboComptes)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.Frame719.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExercices)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSGridSituation)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.tableLayoutPanel52.SuspendLayout();
            this.Frame327.SuspendLayout();
            this.tableLayoutPanel34.SuspendLayout();
            this.tableLayoutPanel55.SuspendLayout();
            this.tableLayoutPanel56.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParticulier1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParticulier2)).BeginInit();
            this.tableLayoutPanel54.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialImm)).BeginInit();
            this.tableLayoutPanel53.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercial)).BeginInit();
            this.tableLayoutPanel29.SuspendLayout();
            this.frmResultat.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.panel1.SuspendLayout();
            this.ultraTabPageControl1.SuspendLayout();
            this.tableLayoutPanel35.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleGridImmeuble)).BeginInit();
            this.tableLayoutPanel11.SuspendLayout();
            this.Frame70.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.Frame70s.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCmbCommercial)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSINTERVENANT)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboQualifGest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridGestionnaire)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbModeTrans_TAB)).BeginInit();
            this.ultraTabPageControl5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab2)).BeginInit();
            this.SSTab2.SuspendLayout();
            this.ultraTabPageControl10.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.tableLayoutPanel40.SuspendLayout();
            this.panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergieA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergieDe)).BeginInit();
            this.tableLayoutPanel48.SuspendLayout();
            this.panel5.SuspendLayout();
            this.tableLayoutPanel33.SuspendLayout();
            this.tableLayoutPanel47.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.tableLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypologie)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCoteAmour)).BeginInit();
            this.frmFactures.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrameFioul)).BeginInit();
            this.FrameFioul.SuspendLayout();
            this.tableLayoutPanel37.SuspendLayout();
            this.ultraTabPageControl7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel49.SuspendLayout();
            this.contextMenu.SuspendLayout();
            this.tableLayoutPanel38.SuspendLayout();
            this.ultraTabPageControl8.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel42.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbArticle)).BeginInit();
            this.tableLayoutPanel43.SuspendLayout();
            this.tableLayoutPanel44.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).BeginInit();
            this.tableLayoutPanel45.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).BeginInit();
            this.tableLayoutPanel46.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssIntervention)).BeginInit();
            this.ultraTabPageControl9.SuspendLayout();
            this.tableLayoutPanel50.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisIntervenant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisImmeuble)).BeginInit();
            this.tableLayoutPanel51.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDevisArticle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDevis)).BeginInit();
            this.ultraTabPageControl6.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCLRE)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropCodeEtat)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssFindClient)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel36.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl14
            // 
            this.ultraTabPageControl14.Controls.Add(this.tableLayoutPanel21);
            this.ultraTabPageControl14.Location = new System.Drawing.Point(1, 21);
            this.ultraTabPageControl14.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl14.Name = "ultraTabPageControl14";
            this.ultraTabPageControl14.Size = new System.Drawing.Size(1842, 774);
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Controls.Add(this.panel11, 0, 2);
            this.tableLayoutPanel21.Controls.Add(this.tableLayoutPanel41, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.SSGridSituation, 0, 1);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 3;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1842, 774);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.panel12);
            this.panel11.Controls.Add(this.panel13);
            this.panel11.Controls.Add(this.panel14);
            this.panel11.Controls.Add(this.panel15);
            this.panel11.Controls.Add(this.panel16);
            this.panel11.Controls.Add(this.panel17);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel11.Location = new System.Drawing.Point(1452, 692);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(387, 79);
            this.panel11.TabIndex = 431;
            // 
            // panel12
            // 
            this.panel12.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel12.BackColor = System.Drawing.Color.Transparent;
            this.panel12.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel12.Controls.Add(this.Label1);
            this.panel12.Location = new System.Drawing.Point(86, 4);
            this.panel12.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(100, 36);
            this.panel12.TabIndex = 424;
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label1.Location = new System.Drawing.Point(4, 8);
            this.Label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(90, 19);
            this.Label1.TabIndex = 15;
            this.Label1.Text = "Solde";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel13
            // 
            this.panel13.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel13.BackColor = System.Drawing.Color.Transparent;
            this.panel13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel13.Controls.Add(this.txtSoldeCompte);
            this.panel13.Location = new System.Drawing.Point(86, 39);
            this.panel13.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(100, 36);
            this.panel13.TabIndex = 427;
            // 
            // txtSoldeCompte
            // 
            this.txtSoldeCompte.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSoldeCompte.Location = new System.Drawing.Point(4, 8);
            this.txtSoldeCompte.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtSoldeCompte.Name = "txtSoldeCompte";
            this.txtSoldeCompte.Size = new System.Drawing.Size(90, 19);
            this.txtSoldeCompte.TabIndex = 15;
            this.txtSoldeCompte.Text = "Solde";
            this.txtSoldeCompte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel14
            // 
            this.panel14.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel14.BackColor = System.Drawing.Color.Transparent;
            this.panel14.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel14.Controls.Add(this.txtTotalRegle);
            this.panel14.Location = new System.Drawing.Point(284, 39);
            this.panel14.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(100, 36);
            this.panel14.TabIndex = 429;
            // 
            // txtTotalRegle
            // 
            this.txtTotalRegle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalRegle.Location = new System.Drawing.Point(8, 8);
            this.txtTotalRegle.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtTotalRegle.Name = "txtTotalRegle";
            this.txtTotalRegle.Size = new System.Drawing.Size(88, 19);
            this.txtTotalRegle.TabIndex = 15;
            this.txtTotalRegle.Text = "Solde";
            this.txtTotalRegle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel15
            // 
            this.panel15.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel15.BackColor = System.Drawing.Color.Transparent;
            this.panel15.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel15.Controls.Add(this.Label0);
            this.panel15.Location = new System.Drawing.Point(185, 4);
            this.panel15.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(100, 36);
            this.panel15.TabIndex = 426;
            // 
            // Label0
            // 
            this.Label0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label0.Location = new System.Drawing.Point(6, 8);
            this.Label0.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label0.Name = "Label0";
            this.Label0.Size = new System.Drawing.Size(86, 19);
            this.Label0.TabIndex = 15;
            this.Label0.Text = "Débit";
            this.Label0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel16
            // 
            this.panel16.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel16.BackColor = System.Drawing.Color.Transparent;
            this.panel16.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel16.Controls.Add(this.Label2);
            this.panel16.Location = new System.Drawing.Point(284, 4);
            this.panel16.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(100, 36);
            this.panel16.TabIndex = 425;
            // 
            // Label2
            // 
            this.Label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label2.Location = new System.Drawing.Point(9, 8);
            this.Label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(80, 19);
            this.Label2.TabIndex = 15;
            this.Label2.Text = "Crédit";
            this.Label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel17
            // 
            this.panel17.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel17.BackColor = System.Drawing.Color.Transparent;
            this.panel17.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel17.Controls.Add(this.txtTotalFacture);
            this.panel17.Location = new System.Drawing.Point(185, 39);
            this.panel17.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(100, 36);
            this.panel17.TabIndex = 428;
            // 
            // txtTotalFacture
            // 
            this.txtTotalFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalFacture.Location = new System.Drawing.Point(6, 8);
            this.txtTotalFacture.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.txtTotalFacture.Name = "txtTotalFacture";
            this.txtTotalFacture.Size = new System.Drawing.Size(86, 19);
            this.txtTotalFacture.TabIndex = 15;
            this.txtTotalFacture.Text = "Solde";
            this.txtTotalFacture.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel41
            // 
            this.tableLayoutPanel41.ColumnCount = 4;
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel41.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 186F));
            this.tableLayoutPanel41.Controls.Add(this.groupBox8, 0, 0);
            this.tableLayoutPanel41.Controls.Add(this.groupBox7, 1, 0);
            this.tableLayoutPanel41.Controls.Add(this.Frame719, 2, 0);
            this.tableLayoutPanel41.Controls.Add(this.cmdEcritures, 3, 0);
            this.tableLayoutPanel41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel41.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel41.Name = "tableLayoutPanel41";
            this.tableLayoutPanel41.RowCount = 1;
            this.tableLayoutPanel41.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel41.Size = new System.Drawing.Size(1836, 94);
            this.tableLayoutPanel41.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.tableLayoutPanel24);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox8.Location = new System.Drawing.Point(6, 6);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox8.Size = new System.Drawing.Size(537, 82);
            this.groupBox8.TabIndex = 419;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Sélection du compte";
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 2;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 162F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Controls.Add(this.ComboComptes, 1, 1);
            this.tableLayoutPanel24.Controls.Add(this.optCptImmeuble, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.optCptGerant, 0, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 2;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(533, 61);
            this.tableLayoutPanel24.TabIndex = 0;
            // 
            // ComboComptes
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ComboComptes.DisplayLayout.Appearance = appearance1;
            this.ComboComptes.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ComboComptes.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboComptes.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboComptes.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ComboComptes.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboComptes.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ComboComptes.DisplayLayout.MaxColScrollRegions = 1;
            this.ComboComptes.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ComboComptes.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ComboComptes.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ComboComptes.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ComboComptes.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ComboComptes.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ComboComptes.DisplayLayout.Override.CellAppearance = appearance8;
            this.ComboComptes.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ComboComptes.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboComptes.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ComboComptes.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ComboComptes.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ComboComptes.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ComboComptes.DisplayLayout.Override.RowAppearance = appearance11;
            this.ComboComptes.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ComboComptes.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ComboComptes.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ComboComptes.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ComboComptes.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ComboComptes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ComboComptes.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ComboComptes.Location = new System.Drawing.Point(164, 33);
            this.ComboComptes.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ComboComptes.Name = "ComboComptes";
            this.ComboComptes.Size = new System.Drawing.Size(367, 27);
            this.ComboComptes.TabIndex = 3;
            this.ComboComptes.Tag = "0";
            // 
            // optCptImmeuble
            // 
            this.optCptImmeuble.AutoSize = true;
            this.optCptImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optCptImmeuble.Location = new System.Drawing.Point(2, 33);
            this.optCptImmeuble.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optCptImmeuble.Name = "optCptImmeuble";
            this.optCptImmeuble.Size = new System.Drawing.Size(155, 23);
            this.optCptImmeuble.TabIndex = 1;
            this.optCptImmeuble.Tag = "0";
            this.optCptImmeuble.Text = "Compte Immeuble";
            this.optCptImmeuble.UseVisualStyleBackColor = true;
            // 
            // optCptGerant
            // 
            this.optCptGerant.AutoSize = true;
            this.optCptGerant.Checked = true;
            this.optCptGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optCptGerant.Location = new System.Drawing.Point(2, 3);
            this.optCptGerant.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optCptGerant.Name = "optCptGerant";
            this.optCptGerant.Size = new System.Drawing.Size(133, 23);
            this.optCptGerant.TabIndex = 0;
            this.optCptGerant.TabStop = true;
            this.optCptGerant.Tag = "1";
            this.optCptGerant.Text = "Compte gérant";
            this.optCptGerant.UseVisualStyleBackColor = true;
            this.optCptGerant.CheckedChanged += new System.EventHandler(this.optCptGerant_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.tableLayoutPanel23);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox7.Location = new System.Drawing.Point(555, 6);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox7.Size = new System.Drawing.Size(538, 82);
            this.groupBox7.TabIndex = 420;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Choix du type d\'écriture";
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 1;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Controls.Add(this.optToutesEcritures, 0, 1);
            this.tableLayoutPanel23.Controls.Add(this.optEcrituresNonLetrees, 0, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 2;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(534, 61);
            this.tableLayoutPanel23.TabIndex = 0;
            // 
            // optToutesEcritures
            // 
            this.optToutesEcritures.AutoSize = true;
            this.optToutesEcritures.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optToutesEcritures.Location = new System.Drawing.Point(2, 33);
            this.optToutesEcritures.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optToutesEcritures.Name = "optToutesEcritures";
            this.optToutesEcritures.Size = new System.Drawing.Size(164, 23);
            this.optToutesEcritures.TabIndex = 1;
            this.optToutesEcritures.Text = "Toutes les écritures";
            this.optToutesEcritures.UseVisualStyleBackColor = true;
            this.optToutesEcritures.CheckedChanged += new System.EventHandler(this.optToutesEcritures_CheckedChanged);
            // 
            // optEcrituresNonLetrees
            // 
            this.optEcrituresNonLetrees.AutoSize = true;
            this.optEcrituresNonLetrees.Checked = true;
            this.optEcrituresNonLetrees.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optEcrituresNonLetrees.Location = new System.Drawing.Point(2, 3);
            this.optEcrituresNonLetrees.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optEcrituresNonLetrees.Name = "optEcrituresNonLetrees";
            this.optEcrituresNonLetrees.Size = new System.Drawing.Size(179, 23);
            this.optEcrituresNonLetrees.TabIndex = 0;
            this.optEcrituresNonLetrees.TabStop = true;
            this.optEcrituresNonLetrees.Text = "Ecritures non lettrées";
            this.optEcrituresNonLetrees.UseVisualStyleBackColor = true;
            this.optEcrituresNonLetrees.CheckedChanged += new System.EventHandler(this.optEcrituresNonLetrees_CheckedChanged);
            // 
            // Frame719
            // 
            this.Frame719.Controls.Add(this.tableLayoutPanel22);
            this.Frame719.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame719.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame719.Location = new System.Drawing.Point(1105, 6);
            this.Frame719.Margin = new System.Windows.Forms.Padding(6);
            this.Frame719.Name = "Frame719";
            this.Frame719.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Frame719.Size = new System.Drawing.Size(538, 82);
            this.Frame719.TabIndex = 421;
            this.Frame719.TabStop = false;
            this.Frame719.Text = "Choix de l\'exercice";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.cmbExercices, 0, 0);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 1;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(534, 61);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // cmbExercices
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbExercices.DisplayLayout.Appearance = appearance13;
            this.cmbExercices.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbExercices.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbExercices.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbExercices.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbExercices.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbExercices.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbExercices.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbExercices.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbExercices.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbExercices.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbExercices.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbExercices.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbExercices.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbExercices.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbExercices.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbExercices.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbExercices.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbExercices.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbExercices.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbExercices.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbExercices.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbExercices.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbExercices.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbExercices.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbExercices.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbExercices.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbExercices.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbExercices.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbExercices.Location = new System.Drawing.Point(2, 3);
            this.cmbExercices.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbExercices.Name = "cmbExercices";
            this.cmbExercices.Size = new System.Drawing.Size(530, 27);
            this.cmbExercices.TabIndex = 4;
            this.cmbExercices.Tag = "2";
            // 
            // cmdEcritures
            // 
            this.cmdEcritures.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEcritures.FlatAppearance.BorderSize = 0;
            this.cmdEcritures.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEcritures.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEcritures.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdEcritures.Location = new System.Drawing.Point(1651, 14);
            this.cmdEcritures.Margin = new System.Windows.Forms.Padding(2, 14, 2, 2);
            this.cmdEcritures.Name = "cmdEcritures";
            this.cmdEcritures.Size = new System.Drawing.Size(58, 35);
            this.cmdEcritures.TabIndex = 352;
            this.cmdEcritures.Tag = "2";
            this.cmdEcritures.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdEcritures, "Lancer la recherche des écritures");
            this.cmdEcritures.UseVisualStyleBackColor = false;
            this.cmdEcritures.Click += new System.EventHandler(this.cmdEcritures_Click);
            // 
            // SSGridSituation
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSGridSituation.DisplayLayout.Appearance = appearance25;
            this.SSGridSituation.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSGridSituation.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSGridSituation.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSGridSituation.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.SSGridSituation.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSGridSituation.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.SSGridSituation.DisplayLayout.MaxColScrollRegions = 1;
            this.SSGridSituation.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSGridSituation.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSGridSituation.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.SSGridSituation.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSGridSituation.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSGridSituation.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSGridSituation.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSGridSituation.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSGridSituation.DisplayLayout.Override.CellAppearance = appearance32;
            this.SSGridSituation.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSGridSituation.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.SSGridSituation.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.SSGridSituation.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSGridSituation.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.SSGridSituation.DisplayLayout.Override.RowAppearance = appearance35;
            this.SSGridSituation.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSGridSituation.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSGridSituation.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.SSGridSituation.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSGridSituation.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSGridSituation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSGridSituation.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSGridSituation.Location = new System.Drawing.Point(2, 103);
            this.SSGridSituation.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.SSGridSituation.Name = "SSGridSituation";
            this.SSGridSituation.Size = new System.Drawing.Size(1838, 583);
            this.SSGridSituation.TabIndex = 412;
            this.SSGridSituation.Text = "ultraGrid1";
            this.SSGridSituation.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSGridSituation_DoubleClickRow);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel52);
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel29);
            this.ultraTabPageControl3.Controls.Add(this.frmResultat);
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel25);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1842, 774);
            // 
            // tableLayoutPanel52
            // 
            this.tableLayoutPanel52.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel52.ColumnCount = 1;
            this.tableLayoutPanel52.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel52.Controls.Add(this.Frame327, 0, 3);
            this.tableLayoutPanel52.Controls.Add(this.tableLayoutPanel55, 0, 4);
            this.tableLayoutPanel52.Controls.Add(this.tableLayoutPanel54, 0, 2);
            this.tableLayoutPanel52.Controls.Add(this.tableLayoutPanel53, 0, 1);
            this.tableLayoutPanel52.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel52.Location = new System.Drawing.Point(5, 69);
            this.tableLayoutPanel52.Name = "tableLayoutPanel52";
            this.tableLayoutPanel52.RowCount = 5;
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel52.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel52.Size = new System.Drawing.Size(1502, 165);
            this.tableLayoutPanel52.TabIndex = 491;
            // 
            // Frame327
            // 
            this.Frame327.BackColor = System.Drawing.Color.Transparent;
            this.Frame327.Controls.Add(this.tableLayoutPanel34);
            this.Frame327.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame327.Location = new System.Drawing.Point(3, 88);
            this.Frame327.Name = "Frame327";
            this.Frame327.Size = new System.Drawing.Size(328, 42);
            this.Frame327.TabIndex = 443;
            this.Frame327.TabStop = false;
            // 
            // tableLayoutPanel34
            // 
            this.tableLayoutPanel34.ColumnCount = 2;
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel34.Controls.Add(this.opt1, 1, 0);
            this.tableLayoutPanel34.Controls.Add(this.Opt2, 0, 0);
            this.tableLayoutPanel34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel34.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel34.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel34.Name = "tableLayoutPanel34";
            this.tableLayoutPanel34.RowCount = 1;
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel34.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel34.Size = new System.Drawing.Size(322, 21);
            this.tableLayoutPanel34.TabIndex = 582;
            // 
            // opt1
            // 
            this.opt1.AutoSize = true;
            this.opt1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.opt1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.opt1.Location = new System.Drawing.Point(161, 0);
            this.opt1.Margin = new System.Windows.Forms.Padding(0);
            this.opt1.Name = "opt1";
            this.opt1.Size = new System.Drawing.Size(161, 21);
            this.opt1.TabIndex = 539;
            this.opt1.Text = "propriétaire";
            this.opt1.UseVisualStyleBackColor = true;
            // 
            // Opt2
            // 
            this.Opt2.AutoSize = true;
            this.Opt2.Checked = true;
            this.Opt2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Opt2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Opt2.Location = new System.Drawing.Point(0, 0);
            this.Opt2.Margin = new System.Windows.Forms.Padding(0);
            this.Opt2.Name = "Opt2";
            this.Opt2.Size = new System.Drawing.Size(161, 21);
            this.Opt2.TabIndex = 538;
            this.Opt2.TabStop = true;
            this.Opt2.Text = "Syndics, Autre";
            this.Opt2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel55
            // 
            this.tableLayoutPanel55.ColumnCount = 10;
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel55.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 88F));
            this.tableLayoutPanel55.Controls.Add(this.tableLayoutPanel56, 7, 0);
            this.tableLayoutPanel55.Controls.Add(this.OptParticulier, 0, 0);
            this.tableLayoutPanel55.Controls.Add(this.Label5754, 1, 0);
            this.tableLayoutPanel55.Controls.Add(this.ssCombo20, 6, 0);
            this.tableLayoutPanel55.Controls.Add(this.txtParticulier1, 2, 0);
            this.tableLayoutPanel55.Controls.Add(this.Label57200, 5, 0);
            this.tableLayoutPanel55.Controls.Add(this.Label5756, 3, 0);
            this.tableLayoutPanel55.Controls.Add(this.txtParticulier2, 4, 0);
            this.tableLayoutPanel55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel55.Location = new System.Drawing.Point(0, 133);
            this.tableLayoutPanel55.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel55.Name = "tableLayoutPanel55";
            this.tableLayoutPanel55.RowCount = 1;
            this.tableLayoutPanel55.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel55.Size = new System.Drawing.Size(1502, 32);
            this.tableLayoutPanel55.TabIndex = 494;
            // 
            // tableLayoutPanel56
            // 
            this.tableLayoutPanel56.ColumnCount = 2;
            this.tableLayoutPanel55.SetColumnSpan(this.tableLayoutPanel56, 3);
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.Controls.Add(this.cmdImprimer, 1, 0);
            this.tableLayoutPanel56.Controls.Add(this.cmdSPart, 0, 0);
            this.tableLayoutPanel56.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel56.Location = new System.Drawing.Point(1040, 0);
            this.tableLayoutPanel56.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel56.Name = "tableLayoutPanel56";
            this.tableLayoutPanel56.RowCount = 1;
            this.tableLayoutPanel56.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel56.Size = new System.Drawing.Size(462, 32);
            this.tableLayoutPanel56.TabIndex = 491;
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImprimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdImprimer.ForeColor = System.Drawing.Color.White;
            this.cmdImprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImprimer.Location = new System.Drawing.Point(233, 2);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(227, 28);
            this.cmdImprimer.TabIndex = 444;
            this.cmdImprimer.Tag = "6";
            this.cmdImprimer.Text = "Imprimer";
            this.cmdImprimer.UseVisualStyleBackColor = false;
            this.cmdImprimer.Click += new System.EventHandler(this.cmdImprimer_Click);
            // 
            // cmdSPart
            // 
            this.cmdSPart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSPart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSPart.Enabled = false;
            this.cmdSPart.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdSPart.FlatAppearance.BorderSize = 0;
            this.cmdSPart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSPart.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSPart.ForeColor = System.Drawing.Color.White;
            this.cmdSPart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSPart.Location = new System.Drawing.Point(2, 2);
            this.cmdSPart.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSPart.Name = "cmdSPart";
            this.cmdSPart.Size = new System.Drawing.Size(227, 28);
            this.cmdSPart.TabIndex = 443;
            this.cmdSPart.Tag = "3";
            this.cmdSPart.Text = "Synthèse";
            this.cmdSPart.UseVisualStyleBackColor = false;
            this.cmdSPart.Visible = false;
            this.cmdSPart.Click += new System.EventHandler(this.cmdSPart_Click);
            // 
            // OptParticulier
            // 
            this.OptParticulier.AutoSize = true;
            this.OptParticulier.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptParticulier.Location = new System.Drawing.Point(2, 3);
            this.OptParticulier.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptParticulier.Name = "OptParticulier";
            this.OptParticulier.Size = new System.Drawing.Size(98, 23);
            this.OptParticulier.TabIndex = 415;
            this.OptParticulier.Tag = "8";
            this.OptParticulier.Text = "Particulier";
            this.OptParticulier.UseVisualStyleBackColor = true;
            this.OptParticulier.CheckedChanged += new System.EventHandler(this.OptParticulier_CheckedChanged);
            // 
            // Label5754
            // 
            this.Label5754.AutoSize = true;
            this.Label5754.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5754.Location = new System.Drawing.Point(110, 3);
            this.Label5754.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5754.Name = "Label5754";
            this.Label5754.Size = new System.Drawing.Size(29, 19);
            this.Label5754.TabIndex = 0;
            this.Label5754.Text = "Du";
            this.Label5754.Visible = false;
            // 
            // ssCombo20
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssCombo20.DisplayLayout.Appearance = appearance37;
            this.ssCombo20.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo20.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo20.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ssCombo20.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo20.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssCombo20.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            this.ssCombo20.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssCombo20.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance42.BorderColor = System.Drawing.Color.Silver;
            appearance42.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssCombo20.DisplayLayout.Override.CellAppearance = appearance42;
            this.ssCombo20.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssCombo20.DisplayLayout.Override.CellPadding = 0;
            appearance43.BackColor = System.Drawing.SystemColors.Control;
            appearance43.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance43.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance43.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance43.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo20.DisplayLayout.Override.GroupByRowAppearance = appearance43;
            appearance44.TextHAlignAsString = "Left";
            this.ssCombo20.DisplayLayout.Override.HeaderAppearance = appearance44;
            this.ssCombo20.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssCombo20.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.ssCombo20.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssCombo20.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.ssCombo20.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssCombo20.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssCombo20.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssCombo20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssCombo20.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssCombo20.Location = new System.Drawing.Point(808, 3);
            this.ssCombo20.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssCombo20.Name = "ssCombo20";
            this.ssCombo20.Size = new System.Drawing.Size(230, 27);
            this.ssCombo20.TabIndex = 3;
            this.ssCombo20.Tag = "20";
            this.ssCombo20.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ssCombo14_BeforeDropDown);
            // 
            // txtParticulier1
            // 
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtParticulier1.DisplayLayout.Appearance = appearance46;
            this.txtParticulier1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance47.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.txtParticulier1.DisplayLayout.GroupByBox.Appearance = appearance47;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtParticulier1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance48;
            this.txtParticulier1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance49.BackColor2 = System.Drawing.SystemColors.Control;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtParticulier1.DisplayLayout.GroupByBox.PromptAppearance = appearance49;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtParticulier1.DisplayLayout.Override.ActiveCellAppearance = appearance50;
            this.txtParticulier1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtParticulier1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance51.BorderColor = System.Drawing.Color.Silver;
            appearance51.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtParticulier1.DisplayLayout.Override.CellAppearance = appearance51;
            this.txtParticulier1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtParticulier1.DisplayLayout.Override.CellPadding = 0;
            appearance52.BackColor = System.Drawing.SystemColors.Control;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.txtParticulier1.DisplayLayout.Override.GroupByRowAppearance = appearance52;
            appearance53.TextHAlignAsString = "Left";
            this.txtParticulier1.DisplayLayout.Override.HeaderAppearance = appearance53;
            this.txtParticulier1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtParticulier1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtParticulier1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtParticulier1.DisplayLayout.Override.TemplateAddRowAppearance = appearance54;
            this.txtParticulier1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtParticulier1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtParticulier1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtParticulier1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtParticulier1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtParticulier1.Location = new System.Drawing.Point(145, 3);
            this.txtParticulier1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtParticulier1.Name = "txtParticulier1";
            this.txtParticulier1.Size = new System.Drawing.Size(230, 27);
            this.txtParticulier1.TabIndex = 1;
            this.txtParticulier1.Tag = "4";
            this.txtParticulier1.Visible = false;
            // 
            // Label57200
            // 
            this.Label57200.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label57200.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label57200.Location = new System.Drawing.Point(648, 3);
            this.Label57200.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label57200.Name = "Label57200";
            this.Label57200.Size = new System.Drawing.Size(156, 29);
            this.Label57200.TabIndex = 437;
            this.Label57200.Text = "Responsable exploit.";
            // 
            // Label5756
            // 
            this.Label5756.AutoSize = true;
            this.Label5756.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5756.Location = new System.Drawing.Point(379, 3);
            this.Label5756.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5756.Name = "Label5756";
            this.Label5756.Size = new System.Drawing.Size(27, 19);
            this.Label5756.TabIndex = 419;
            this.Label5756.Text = "Au";
            this.Label5756.Visible = false;
            // 
            // txtParticulier2
            // 
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtParticulier2.DisplayLayout.Appearance = appearance55;
            this.txtParticulier2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance56.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance56.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance56.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance56.BorderColor = System.Drawing.SystemColors.Window;
            this.txtParticulier2.DisplayLayout.GroupByBox.Appearance = appearance56;
            appearance57.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtParticulier2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance57;
            this.txtParticulier2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance58.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance58.BackColor2 = System.Drawing.SystemColors.Control;
            appearance58.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance58.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtParticulier2.DisplayLayout.GroupByBox.PromptAppearance = appearance58;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtParticulier2.DisplayLayout.Override.ActiveCellAppearance = appearance59;
            this.txtParticulier2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtParticulier2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance60.BorderColor = System.Drawing.Color.Silver;
            appearance60.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtParticulier2.DisplayLayout.Override.CellAppearance = appearance60;
            this.txtParticulier2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtParticulier2.DisplayLayout.Override.CellPadding = 0;
            appearance61.BackColor = System.Drawing.SystemColors.Control;
            appearance61.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance61.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.BorderColor = System.Drawing.SystemColors.Window;
            this.txtParticulier2.DisplayLayout.Override.GroupByRowAppearance = appearance61;
            appearance62.TextHAlignAsString = "Left";
            this.txtParticulier2.DisplayLayout.Override.HeaderAppearance = appearance62;
            this.txtParticulier2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtParticulier2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtParticulier2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance63.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtParticulier2.DisplayLayout.Override.TemplateAddRowAppearance = appearance63;
            this.txtParticulier2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtParticulier2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtParticulier2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtParticulier2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtParticulier2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtParticulier2.Location = new System.Drawing.Point(414, 3);
            this.txtParticulier2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtParticulier2.Name = "txtParticulier2";
            this.txtParticulier2.Size = new System.Drawing.Size(230, 27);
            this.txtParticulier2.TabIndex = 2;
            this.txtParticulier2.Tag = "7";
            this.txtParticulier2.Visible = false;
            // 
            // tableLayoutPanel54
            // 
            this.tableLayoutPanel54.ColumnCount = 10;
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel54.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel54.Controls.Add(this.OptImmeuble, 0, 0);
            this.tableLayoutPanel54.Controls.Add(this.Label5727, 1, 0);
            this.tableLayoutPanel54.Controls.Add(this.ssCombo19, 8, 0);
            this.tableLayoutPanel54.Controls.Add(this.txtImmeuble1, 2, 0);
            this.tableLayoutPanel54.Controls.Add(this.Label5730, 3, 0);
            this.tableLayoutPanel54.Controls.Add(this.Label4199, 7, 0);
            this.tableLayoutPanel54.Controls.Add(this.txtImmeuble2, 4, 0);
            this.tableLayoutPanel54.Controls.Add(this.Label5725, 5, 0);
            this.tableLayoutPanel54.Controls.Add(this.txtCommercialImm, 6, 0);
            this.tableLayoutPanel54.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel54.Location = new System.Drawing.Point(0, 55);
            this.tableLayoutPanel54.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel54.Name = "tableLayoutPanel54";
            this.tableLayoutPanel54.RowCount = 1;
            this.tableLayoutPanel54.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel54.Size = new System.Drawing.Size(1502, 30);
            this.tableLayoutPanel54.TabIndex = 493;
            // 
            // OptImmeuble
            // 
            this.OptImmeuble.AutoSize = true;
            this.OptImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptImmeuble.Location = new System.Drawing.Point(2, 3);
            this.OptImmeuble.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptImmeuble.Name = "OptImmeuble";
            this.OptImmeuble.Size = new System.Drawing.Size(86, 23);
            this.OptImmeuble.TabIndex = 0;
            this.OptImmeuble.Tag = "7";
            this.OptImmeuble.Text = "Immeuble";
            this.OptImmeuble.UseVisualStyleBackColor = true;
            this.OptImmeuble.CheckedChanged += new System.EventHandler(this.OptImmeuble_CheckedChanged);
            // 
            // Label5727
            // 
            this.Label5727.AutoSize = true;
            this.Label5727.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5727.Location = new System.Drawing.Point(92, 3);
            this.Label5727.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5727.Name = "Label5727";
            this.Label5727.Size = new System.Drawing.Size(29, 19);
            this.Label5727.TabIndex = 0;
            this.Label5727.Text = "Du";
            this.Label5727.Visible = false;
            // 
            // ssCombo19
            // 
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            appearance64.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssCombo19.DisplayLayout.Appearance = appearance64;
            this.ssCombo19.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance65.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance65.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance65.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance65.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo19.DisplayLayout.GroupByBox.Appearance = appearance65;
            appearance66.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo19.DisplayLayout.GroupByBox.BandLabelAppearance = appearance66;
            this.ssCombo19.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance67.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance67.BackColor2 = System.Drawing.SystemColors.Control;
            appearance67.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance67.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo19.DisplayLayout.GroupByBox.PromptAppearance = appearance67;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssCombo19.DisplayLayout.Override.ActiveCellAppearance = appearance68;
            this.ssCombo19.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssCombo19.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance69.BorderColor = System.Drawing.Color.Silver;
            appearance69.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssCombo19.DisplayLayout.Override.CellAppearance = appearance69;
            this.ssCombo19.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssCombo19.DisplayLayout.Override.CellPadding = 0;
            appearance70.BackColor = System.Drawing.SystemColors.Control;
            appearance70.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance70.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance70.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance70.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo19.DisplayLayout.Override.GroupByRowAppearance = appearance70;
            appearance71.TextHAlignAsString = "Left";
            this.ssCombo19.DisplayLayout.Override.HeaderAppearance = appearance71;
            this.ssCombo19.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssCombo19.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.ssCombo19.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssCombo19.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.ssCombo19.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssCombo19.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssCombo19.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssCombo19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssCombo19.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssCombo19.Location = new System.Drawing.Point(1178, 3);
            this.ssCombo19.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssCombo19.Name = "ssCombo19";
            this.ssCombo19.Size = new System.Drawing.Size(229, 27);
            this.ssCombo19.TabIndex = 4;
            this.ssCombo19.Tag = "19";
            this.ssCombo19.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ssCombo14_BeforeDropDown);
            // 
            // txtImmeuble1
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtImmeuble1.DisplayLayout.Appearance = appearance73;
            this.txtImmeuble1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance74.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance74.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance74.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance74.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble1.DisplayLayout.GroupByBox.Appearance = appearance74;
            appearance75.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance75;
            this.txtImmeuble1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance76.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance76.BackColor2 = System.Drawing.SystemColors.Control;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance76.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble1.DisplayLayout.GroupByBox.PromptAppearance = appearance76;
            appearance77.BackColor = System.Drawing.SystemColors.Window;
            appearance77.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtImmeuble1.DisplayLayout.Override.ActiveCellAppearance = appearance77;
            this.txtImmeuble1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtImmeuble1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance78.BorderColor = System.Drawing.Color.Silver;
            appearance78.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtImmeuble1.DisplayLayout.Override.CellAppearance = appearance78;
            this.txtImmeuble1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtImmeuble1.DisplayLayout.Override.CellPadding = 0;
            appearance79.BackColor = System.Drawing.SystemColors.Control;
            appearance79.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance79.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance79.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance79.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble1.DisplayLayout.Override.GroupByRowAppearance = appearance79;
            appearance80.TextHAlignAsString = "Left";
            this.txtImmeuble1.DisplayLayout.Override.HeaderAppearance = appearance80;
            this.txtImmeuble1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtImmeuble1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtImmeuble1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtImmeuble1.DisplayLayout.Override.TemplateAddRowAppearance = appearance81;
            this.txtImmeuble1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtImmeuble1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtImmeuble1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtImmeuble1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmeuble1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtImmeuble1.Location = new System.Drawing.Point(134, 3);
            this.txtImmeuble1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtImmeuble1.Name = "txtImmeuble1";
            this.txtImmeuble1.Size = new System.Drawing.Size(229, 27);
            this.txtImmeuble1.TabIndex = 1;
            this.txtImmeuble1.Tag = "3";
            this.txtImmeuble1.Visible = false;
            // 
            // Label5730
            // 
            this.Label5730.AutoSize = true;
            this.Label5730.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5730.Location = new System.Drawing.Point(367, 3);
            this.Label5730.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5730.Name = "Label5730";
            this.Label5730.Size = new System.Drawing.Size(27, 19);
            this.Label5730.TabIndex = 0;
            this.Label5730.Text = "Au";
            this.Label5730.Visible = false;
            // 
            // Label4199
            // 
            this.Label4199.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label4199.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label4199.Location = new System.Drawing.Point(1015, 3);
            this.Label4199.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label4199.Name = "Label4199";
            this.Label4199.Size = new System.Drawing.Size(159, 27);
            this.Label4199.TabIndex = 0;
            this.Label4199.Text = "Responsable exploit.";
            // 
            // txtImmeuble2
            // 
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtImmeuble2.DisplayLayout.Appearance = appearance82;
            this.txtImmeuble2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance83.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble2.DisplayLayout.GroupByBox.Appearance = appearance83;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance84;
            this.txtImmeuble2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance85.BackColor2 = System.Drawing.SystemColors.Control;
            appearance85.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble2.DisplayLayout.GroupByBox.PromptAppearance = appearance85;
            appearance86.BackColor = System.Drawing.SystemColors.Window;
            appearance86.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtImmeuble2.DisplayLayout.Override.ActiveCellAppearance = appearance86;
            this.txtImmeuble2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtImmeuble2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance87.BorderColor = System.Drawing.Color.Silver;
            appearance87.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtImmeuble2.DisplayLayout.Override.CellAppearance = appearance87;
            this.txtImmeuble2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtImmeuble2.DisplayLayout.Override.CellPadding = 0;
            appearance88.BackColor = System.Drawing.SystemColors.Control;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble2.DisplayLayout.Override.GroupByRowAppearance = appearance88;
            appearance89.TextHAlignAsString = "Left";
            this.txtImmeuble2.DisplayLayout.Override.HeaderAppearance = appearance89;
            this.txtImmeuble2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtImmeuble2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtImmeuble2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtImmeuble2.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.txtImmeuble2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtImmeuble2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtImmeuble2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtImmeuble2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmeuble2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtImmeuble2.Location = new System.Drawing.Point(409, 3);
            this.txtImmeuble2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtImmeuble2.Name = "txtImmeuble2";
            this.txtImmeuble2.Size = new System.Drawing.Size(229, 27);
            this.txtImmeuble2.TabIndex = 2;
            this.txtImmeuble2.Tag = "6";
            this.txtImmeuble2.Visible = false;
            // 
            // Label5725
            // 
            this.Label5725.AutoSize = true;
            this.Label5725.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5725.Location = new System.Drawing.Point(642, 3);
            this.Label5725.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5725.Name = "Label5725";
            this.Label5725.Size = new System.Drawing.Size(92, 19);
            this.Label5725.TabIndex = 0;
            this.Label5725.Text = "Commercial";
            this.Label5725.Visible = false;
            // 
            // txtCommercialImm
            // 
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCommercialImm.DisplayLayout.Appearance = appearance91;
            this.txtCommercialImm.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance92.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance92.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance92.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance92.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercialImm.DisplayLayout.GroupByBox.Appearance = appearance92;
            appearance93.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercialImm.DisplayLayout.GroupByBox.BandLabelAppearance = appearance93;
            this.txtCommercialImm.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance94.BackColor2 = System.Drawing.SystemColors.Control;
            appearance94.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercialImm.DisplayLayout.GroupByBox.PromptAppearance = appearance94;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCommercialImm.DisplayLayout.Override.ActiveCellAppearance = appearance95;
            this.txtCommercialImm.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCommercialImm.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance96.BorderColor = System.Drawing.Color.Silver;
            appearance96.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCommercialImm.DisplayLayout.Override.CellAppearance = appearance96;
            this.txtCommercialImm.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCommercialImm.DisplayLayout.Override.CellPadding = 0;
            appearance97.BackColor = System.Drawing.SystemColors.Control;
            appearance97.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance97.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance97.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance97.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercialImm.DisplayLayout.Override.GroupByRowAppearance = appearance97;
            appearance98.TextHAlignAsString = "Left";
            this.txtCommercialImm.DisplayLayout.Override.HeaderAppearance = appearance98;
            this.txtCommercialImm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCommercialImm.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtCommercialImm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance99.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCommercialImm.DisplayLayout.Override.TemplateAddRowAppearance = appearance99;
            this.txtCommercialImm.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCommercialImm.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCommercialImm.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCommercialImm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommercialImm.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCommercialImm.Location = new System.Drawing.Point(782, 3);
            this.txtCommercialImm.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCommercialImm.Name = "txtCommercialImm";
            this.txtCommercialImm.Size = new System.Drawing.Size(229, 27);
            this.txtCommercialImm.TabIndex = 3;
            this.txtCommercialImm.Tag = "8";
            this.txtCommercialImm.Visible = false;
            this.txtCommercialImm.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCommercialImm_BeforeDropDown);
            // 
            // tableLayoutPanel53
            // 
            this.tableLayoutPanel53.ColumnCount = 10;
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 163F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel53.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 93F));
            this.tableLayoutPanel53.Controls.Add(this.OptSyndic, 0, 0);
            this.tableLayoutPanel53.Controls.Add(this.Label5726, 1, 0);
            this.tableLayoutPanel53.Controls.Add(this.txtSyndic1, 2, 0);
            this.tableLayoutPanel53.Controls.Add(this.Label5729, 3, 0);
            this.tableLayoutPanel53.Controls.Add(this.txtSyndic2, 4, 0);
            this.tableLayoutPanel53.Controls.Add(this.ssCombo14, 8, 0);
            this.tableLayoutPanel53.Controls.Add(this.Label5758, 5, 0);
            this.tableLayoutPanel53.Controls.Add(this.txtCommercial, 6, 0);
            this.tableLayoutPanel53.Controls.Add(this.Label4198, 7, 0);
            this.tableLayoutPanel53.Controls.Add(this.cmdSsyndic, 9, 0);
            this.tableLayoutPanel53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel53.Location = new System.Drawing.Point(0, 25);
            this.tableLayoutPanel53.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel53.Name = "tableLayoutPanel53";
            this.tableLayoutPanel53.RowCount = 1;
            this.tableLayoutPanel53.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel53.Size = new System.Drawing.Size(1502, 30);
            this.tableLayoutPanel53.TabIndex = 492;
            // 
            // OptSyndic
            // 
            this.OptSyndic.AutoSize = true;
            this.OptSyndic.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptSyndic.Location = new System.Drawing.Point(2, 3);
            this.OptSyndic.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptSyndic.Name = "OptSyndic";
            this.OptSyndic.Size = new System.Drawing.Size(75, 23);
            this.OptSyndic.TabIndex = 0;
            this.OptSyndic.Tag = "6";
            this.OptSyndic.Text = "Gérant";
            this.OptSyndic.UseVisualStyleBackColor = true;
            this.OptSyndic.CheckedChanged += new System.EventHandler(this.OptSyndic_CheckedChanged);
            // 
            // Label5726
            // 
            this.Label5726.AutoSize = true;
            this.Label5726.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5726.Location = new System.Drawing.Point(92, 3);
            this.Label5726.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5726.Name = "Label5726";
            this.Label5726.Size = new System.Drawing.Size(29, 19);
            this.Label5726.TabIndex = 0;
            this.Label5726.Text = "Du";
            // 
            // txtSyndic1
            // 
            appearance100.BackColor = System.Drawing.SystemColors.Window;
            appearance100.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSyndic1.DisplayLayout.Appearance = appearance100;
            this.txtSyndic1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance101.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance101.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance101.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance101.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic1.DisplayLayout.GroupByBox.Appearance = appearance101;
            appearance102.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance102;
            this.txtSyndic1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance103.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance103.BackColor2 = System.Drawing.SystemColors.Control;
            appearance103.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance103.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic1.DisplayLayout.GroupByBox.PromptAppearance = appearance103;
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            appearance104.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSyndic1.DisplayLayout.Override.ActiveCellAppearance = appearance104;
            this.txtSyndic1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtSyndic1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance105.BorderColor = System.Drawing.Color.Silver;
            appearance105.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtSyndic1.DisplayLayout.Override.CellAppearance = appearance105;
            this.txtSyndic1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtSyndic1.DisplayLayout.Override.CellPadding = 0;
            appearance106.BackColor = System.Drawing.SystemColors.Control;
            appearance106.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance106.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance106.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance106.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic1.DisplayLayout.Override.GroupByRowAppearance = appearance106;
            appearance107.TextHAlignAsString = "Left";
            this.txtSyndic1.DisplayLayout.Override.HeaderAppearance = appearance107;
            this.txtSyndic1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtSyndic1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtSyndic1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance108.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtSyndic1.DisplayLayout.Override.TemplateAddRowAppearance = appearance108;
            this.txtSyndic1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtSyndic1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtSyndic1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtSyndic1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSyndic1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSyndic1.Location = new System.Drawing.Point(134, 3);
            this.txtSyndic1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtSyndic1.Name = "txtSyndic1";
            this.txtSyndic1.Size = new System.Drawing.Size(229, 27);
            this.txtSyndic1.TabIndex = 1;
            this.txtSyndic1.Tag = "1";
            // 
            // Label5729
            // 
            this.Label5729.AutoSize = true;
            this.Label5729.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5729.Location = new System.Drawing.Point(367, 3);
            this.Label5729.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5729.Name = "Label5729";
            this.Label5729.Size = new System.Drawing.Size(27, 19);
            this.Label5729.TabIndex = 0;
            this.Label5729.Text = "Au";
            // 
            // txtSyndic2
            // 
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            appearance109.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSyndic2.DisplayLayout.Appearance = appearance109;
            this.txtSyndic2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance110.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance110.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance110.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance110.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic2.DisplayLayout.GroupByBox.Appearance = appearance110;
            appearance111.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance111;
            this.txtSyndic2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance112.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance112.BackColor2 = System.Drawing.SystemColors.Control;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance112.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic2.DisplayLayout.GroupByBox.PromptAppearance = appearance112;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            appearance113.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSyndic2.DisplayLayout.Override.ActiveCellAppearance = appearance113;
            this.txtSyndic2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtSyndic2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance114.BorderColor = System.Drawing.Color.Silver;
            appearance114.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtSyndic2.DisplayLayout.Override.CellAppearance = appearance114;
            this.txtSyndic2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtSyndic2.DisplayLayout.Override.CellPadding = 0;
            appearance115.BackColor = System.Drawing.SystemColors.Control;
            appearance115.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance115.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance115.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance115.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic2.DisplayLayout.Override.GroupByRowAppearance = appearance115;
            appearance116.TextHAlignAsString = "Left";
            this.txtSyndic2.DisplayLayout.Override.HeaderAppearance = appearance116;
            this.txtSyndic2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtSyndic2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtSyndic2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance117.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtSyndic2.DisplayLayout.Override.TemplateAddRowAppearance = appearance117;
            this.txtSyndic2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtSyndic2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtSyndic2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtSyndic2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSyndic2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSyndic2.Location = new System.Drawing.Point(409, 3);
            this.txtSyndic2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtSyndic2.Name = "txtSyndic2";
            this.txtSyndic2.Size = new System.Drawing.Size(229, 27);
            this.txtSyndic2.TabIndex = 2;
            this.txtSyndic2.Tag = "5";
            // 
            // ssCombo14
            // 
            appearance118.BackColor = System.Drawing.SystemColors.Window;
            appearance118.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssCombo14.DisplayLayout.Appearance = appearance118;
            this.ssCombo14.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance119.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance119.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance119.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo14.DisplayLayout.GroupByBox.Appearance = appearance119;
            appearance120.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo14.DisplayLayout.GroupByBox.BandLabelAppearance = appearance120;
            this.ssCombo14.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance121.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance121.BackColor2 = System.Drawing.SystemColors.Control;
            appearance121.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance121.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCombo14.DisplayLayout.GroupByBox.PromptAppearance = appearance121;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            appearance122.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssCombo14.DisplayLayout.Override.ActiveCellAppearance = appearance122;
            this.ssCombo14.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssCombo14.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance123.BorderColor = System.Drawing.Color.Silver;
            appearance123.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssCombo14.DisplayLayout.Override.CellAppearance = appearance123;
            this.ssCombo14.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssCombo14.DisplayLayout.Override.CellPadding = 0;
            appearance124.BackColor = System.Drawing.SystemColors.Control;
            appearance124.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance124.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance124.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance124.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCombo14.DisplayLayout.Override.GroupByRowAppearance = appearance124;
            appearance125.TextHAlignAsString = "Left";
            this.ssCombo14.DisplayLayout.Override.HeaderAppearance = appearance125;
            this.ssCombo14.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssCombo14.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.ssCombo14.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance126.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssCombo14.DisplayLayout.Override.TemplateAddRowAppearance = appearance126;
            this.ssCombo14.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssCombo14.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssCombo14.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssCombo14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssCombo14.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssCombo14.Location = new System.Drawing.Point(1178, 3);
            this.ssCombo14.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssCombo14.Name = "ssCombo14";
            this.ssCombo14.Size = new System.Drawing.Size(229, 27);
            this.ssCombo14.TabIndex = 4;
            this.ssCombo14.Tag = "14";
            this.ssCombo14.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ssCombo14_BeforeDropDown);
            // 
            // Label5758
            // 
            this.Label5758.AutoSize = true;
            this.Label5758.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5758.Location = new System.Drawing.Point(642, 3);
            this.Label5758.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label5758.Name = "Label5758";
            this.Label5758.Size = new System.Drawing.Size(92, 19);
            this.Label5758.TabIndex = 0;
            this.Label5758.Text = "Commercial";
            // 
            // txtCommercial
            // 
            appearance127.BackColor = System.Drawing.SystemColors.Window;
            appearance127.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCommercial.DisplayLayout.Appearance = appearance127;
            this.txtCommercial.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance128.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance128.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance128.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance128.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercial.DisplayLayout.GroupByBox.Appearance = appearance128;
            appearance129.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercial.DisplayLayout.GroupByBox.BandLabelAppearance = appearance129;
            this.txtCommercial.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance130.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance130.BackColor2 = System.Drawing.SystemColors.Control;
            appearance130.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance130.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercial.DisplayLayout.GroupByBox.PromptAppearance = appearance130;
            appearance131.BackColor = System.Drawing.SystemColors.Window;
            appearance131.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCommercial.DisplayLayout.Override.ActiveCellAppearance = appearance131;
            this.txtCommercial.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCommercial.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance132.BorderColor = System.Drawing.Color.Silver;
            appearance132.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCommercial.DisplayLayout.Override.CellAppearance = appearance132;
            this.txtCommercial.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCommercial.DisplayLayout.Override.CellPadding = 0;
            appearance133.BackColor = System.Drawing.SystemColors.Control;
            appearance133.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance133.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance133.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance133.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercial.DisplayLayout.Override.GroupByRowAppearance = appearance133;
            appearance134.TextHAlignAsString = "Left";
            this.txtCommercial.DisplayLayout.Override.HeaderAppearance = appearance134;
            this.txtCommercial.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCommercial.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.txtCommercial.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance135.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCommercial.DisplayLayout.Override.TemplateAddRowAppearance = appearance135;
            this.txtCommercial.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCommercial.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCommercial.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommercial.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCommercial.Location = new System.Drawing.Point(782, 3);
            this.txtCommercial.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.txtCommercial.Name = "txtCommercial";
            this.txtCommercial.Size = new System.Drawing.Size(229, 27);
            this.txtCommercial.TabIndex = 3;
            this.txtCommercial.Tag = "8";
            this.txtCommercial.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCommercial_BeforeDropDown);
            this.txtCommercial.Click += new System.EventHandler(this.txtCommercial_Click);
            // 
            // Label4198
            // 
            this.Label4198.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label4198.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label4198.Location = new System.Drawing.Point(1015, 3);
            this.Label4198.Margin = new System.Windows.Forms.Padding(2, 3, 2, 0);
            this.Label4198.Name = "Label4198";
            this.Label4198.Size = new System.Drawing.Size(159, 27);
            this.Label4198.TabIndex = 0;
            this.Label4198.Text = "Responsable exploit.";
            // 
            // cmdSsyndic
            // 
            this.cmdSsyndic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSsyndic.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdSsyndic.FlatAppearance.BorderSize = 0;
            this.cmdSsyndic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSsyndic.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdSsyndic.ForeColor = System.Drawing.Color.White;
            this.cmdSsyndic.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSsyndic.Location = new System.Drawing.Point(1411, 2);
            this.cmdSsyndic.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSsyndic.Name = "cmdSsyndic";
            this.cmdSsyndic.Size = new System.Drawing.Size(75, 25);
            this.cmdSsyndic.TabIndex = 0;
            this.cmdSsyndic.Tag = "3";
            this.cmdSsyndic.Text = "Synthèse";
            this.cmdSsyndic.UseVisualStyleBackColor = false;
            this.cmdSsyndic.Click += new System.EventHandler(this.cmdSsyndic_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label20.Location = new System.Drawing.Point(2, 0);
            this.label20.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(145, 19);
            this.label20.TabIndex = 452;
            this.label20.Text = "Selection d\'édition :";
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel29.ColumnCount = 1;
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Controls.Add(this.Text1, 0, 1);
            this.tableLayoutPanel29.Controls.Add(this.Command1, 0, 2);
            this.tableLayoutPanel29.Controls.Add(this.Text3, 0, 0);
            this.tableLayoutPanel29.Location = new System.Drawing.Point(3, 362);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 3;
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 49.99999F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(1506, 306);
            this.tableLayoutPanel29.TabIndex = 492;
            // 
            // Text1
            // 
            this.Text1.AccAcceptNumbersOnly = false;
            this.Text1.AccAllowComma = false;
            this.Text1.AccBackgroundColor = System.Drawing.Color.White;
            this.Text1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text1.AccHidenValue = "";
            this.Text1.AccNotAllowedChars = null;
            this.Text1.AccReadOnly = false;
            this.Text1.AccReadOnlyAllowDelete = false;
            this.Text1.AccRequired = false;
            this.Text1.BackColor = System.Drawing.Color.White;
            this.Text1.CustomBackColor = System.Drawing.Color.White;
            this.Text1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Text1.Location = new System.Drawing.Point(2, 135);
            this.Text1.Margin = new System.Windows.Forms.Padding(2);
            this.Text1.MaxLength = 32767;
            this.Text1.Multiline = true;
            this.Text1.Name = "Text1";
            this.Text1.ReadOnly = false;
            this.Text1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text1.Size = new System.Drawing.Size(1502, 130);
            this.Text1.TabIndex = 429;
            this.Text1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text1.UseSystemPasswordChar = false;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Dock = System.Windows.Forms.DockStyle.Right;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Ok_24x24;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(1346, 269);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(158, 35);
            this.Command1.TabIndex = 427;
            this.Command1.Tag = "5";
            this.Command1.Text = "Valider";
            this.toolTip1.SetToolTip(this.Command1, "Enregistrer l\'intitulé");
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // Text3
            // 
            this.Text3.AccAcceptNumbersOnly = false;
            this.Text3.AccAllowComma = false;
            this.Text3.AccBackgroundColor = System.Drawing.Color.White;
            this.Text3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text3.AccHidenValue = "";
            this.Text3.AccNotAllowedChars = null;
            this.Text3.AccReadOnly = false;
            this.Text3.AccReadOnlyAllowDelete = false;
            this.Text3.AccRequired = false;
            this.Text3.BackColor = System.Drawing.Color.White;
            this.Text3.CustomBackColor = System.Drawing.Color.White;
            this.Text3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text3.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Text3.Location = new System.Drawing.Point(2, 2);
            this.Text3.Margin = new System.Windows.Forms.Padding(2);
            this.Text3.MaxLength = 32767;
            this.Text3.Multiline = true;
            this.Text3.Name = "Text3";
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text3.Size = new System.Drawing.Size(1502, 129);
            this.Text3.TabIndex = 428;
            this.Text3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text3.UseSystemPasswordChar = false;
            // 
            // frmResultat
            // 
            this.frmResultat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.frmResultat.Controls.Add(this.tableLayoutPanel26);
            this.frmResultat.Location = new System.Drawing.Point(4, 238);
            this.frmResultat.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.frmResultat.Name = "frmResultat";
            this.frmResultat.Padding = new System.Windows.Forms.Padding(4);
            this.frmResultat.Size = new System.Drawing.Size(1502, 120);
            this.frmResultat.TabIndex = 491;
            this.frmResultat.TabStop = false;
            this.frmResultat.Text = "Résultat";
            this.frmResultat.Visible = false;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 4;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 249F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 289F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.txtMontantMoins, 1, 2);
            this.tableLayoutPanel26.Controls.Add(this.txtMontantTotal, 1, 1);
            this.tableLayoutPanel26.Controls.Add(this.label52, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.txtNbComptes, 3, 0);
            this.tableLayoutPanel26.Controls.Add(this.txtMontantPlus, 3, 1);
            this.tableLayoutPanel26.Controls.Add(this.label51, 2, 1);
            this.tableLayoutPanel26.Controls.Add(this.label50, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.label49, 2, 0);
            this.tableLayoutPanel26.Controls.Add(this.txtEcrituresNonLettrees, 1, 0);
            this.tableLayoutPanel26.Controls.Add(this.Label19, 0, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(4, 19);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(1494, 97);
            this.tableLayoutPanel26.TabIndex = 492;
            // 
            // txtMontantMoins
            // 
            this.txtMontantMoins.AccAcceptNumbersOnly = false;
            this.txtMontantMoins.AccAllowComma = false;
            this.txtMontantMoins.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMontantMoins.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMontantMoins.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMontantMoins.AccHidenValue = "";
            this.txtMontantMoins.AccNotAllowedChars = null;
            this.txtMontantMoins.AccReadOnly = false;
            this.txtMontantMoins.AccReadOnlyAllowDelete = false;
            this.txtMontantMoins.AccRequired = false;
            this.txtMontantMoins.BackColor = System.Drawing.Color.White;
            this.txtMontantMoins.CustomBackColor = System.Drawing.Color.White;
            this.txtMontantMoins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMontantMoins.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMontantMoins.ForeColor = System.Drawing.Color.Black;
            this.txtMontantMoins.Location = new System.Drawing.Point(251, 62);
            this.txtMontantMoins.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantMoins.MaxLength = 32767;
            this.txtMontantMoins.Multiline = false;
            this.txtMontantMoins.Name = "txtMontantMoins";
            this.txtMontantMoins.ReadOnly = false;
            this.txtMontantMoins.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMontantMoins.Size = new System.Drawing.Size(474, 27);
            this.txtMontantMoins.TabIndex = 2;
            this.txtMontantMoins.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMontantMoins.UseSystemPasswordChar = false;
            // 
            // txtMontantTotal
            // 
            this.txtMontantTotal.AccAcceptNumbersOnly = false;
            this.txtMontantTotal.AccAllowComma = false;
            this.txtMontantTotal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMontantTotal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMontantTotal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMontantTotal.AccHidenValue = "";
            this.txtMontantTotal.AccNotAllowedChars = null;
            this.txtMontantTotal.AccReadOnly = false;
            this.txtMontantTotal.AccReadOnlyAllowDelete = false;
            this.txtMontantTotal.AccRequired = false;
            this.txtMontantTotal.BackColor = System.Drawing.Color.White;
            this.txtMontantTotal.CustomBackColor = System.Drawing.Color.White;
            this.txtMontantTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMontantTotal.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMontantTotal.ForeColor = System.Drawing.Color.Black;
            this.txtMontantTotal.Location = new System.Drawing.Point(251, 32);
            this.txtMontantTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantTotal.MaxLength = 32767;
            this.txtMontantTotal.Multiline = false;
            this.txtMontantTotal.Name = "txtMontantTotal";
            this.txtMontantTotal.ReadOnly = false;
            this.txtMontantTotal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMontantTotal.Size = new System.Drawing.Size(474, 27);
            this.txtMontantTotal.TabIndex = 1;
            this.txtMontantTotal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMontantTotal.UseSystemPasswordChar = false;
            // 
            // label52
            // 
            this.label52.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label52.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label52.Location = new System.Drawing.Point(2, 60);
            this.label52.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(245, 37);
            this.label52.TabIndex = 21;
            this.label52.Text = "Montant de la plus petite écriture";
            // 
            // txtNbComptes
            // 
            this.txtNbComptes.AccAcceptNumbersOnly = false;
            this.txtNbComptes.AccAllowComma = false;
            this.txtNbComptes.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNbComptes.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNbComptes.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNbComptes.AccHidenValue = "";
            this.txtNbComptes.AccNotAllowedChars = null;
            this.txtNbComptes.AccReadOnly = false;
            this.txtNbComptes.AccReadOnlyAllowDelete = false;
            this.txtNbComptes.AccRequired = false;
            this.txtNbComptes.BackColor = System.Drawing.Color.White;
            this.txtNbComptes.CustomBackColor = System.Drawing.Color.White;
            this.txtNbComptes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNbComptes.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNbComptes.ForeColor = System.Drawing.Color.Black;
            this.txtNbComptes.Location = new System.Drawing.Point(1018, 2);
            this.txtNbComptes.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbComptes.MaxLength = 32767;
            this.txtNbComptes.Multiline = false;
            this.txtNbComptes.Name = "txtNbComptes";
            this.txtNbComptes.ReadOnly = false;
            this.txtNbComptes.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNbComptes.Size = new System.Drawing.Size(474, 27);
            this.txtNbComptes.TabIndex = 3;
            this.txtNbComptes.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNbComptes.UseSystemPasswordChar = false;
            // 
            // txtMontantPlus
            // 
            this.txtMontantPlus.AccAcceptNumbersOnly = false;
            this.txtMontantPlus.AccAllowComma = false;
            this.txtMontantPlus.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMontantPlus.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMontantPlus.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMontantPlus.AccHidenValue = "";
            this.txtMontantPlus.AccNotAllowedChars = null;
            this.txtMontantPlus.AccReadOnly = false;
            this.txtMontantPlus.AccReadOnlyAllowDelete = false;
            this.txtMontantPlus.AccRequired = false;
            this.txtMontantPlus.BackColor = System.Drawing.Color.White;
            this.txtMontantPlus.CustomBackColor = System.Drawing.Color.White;
            this.txtMontantPlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMontantPlus.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMontantPlus.ForeColor = System.Drawing.Color.Black;
            this.txtMontantPlus.Location = new System.Drawing.Point(1018, 32);
            this.txtMontantPlus.Margin = new System.Windows.Forms.Padding(2);
            this.txtMontantPlus.MaxLength = 32767;
            this.txtMontantPlus.Multiline = false;
            this.txtMontantPlus.Name = "txtMontantPlus";
            this.txtMontantPlus.ReadOnly = false;
            this.txtMontantPlus.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMontantPlus.Size = new System.Drawing.Size(474, 27);
            this.txtMontantPlus.TabIndex = 4;
            this.txtMontantPlus.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMontantPlus.UseSystemPasswordChar = false;
            // 
            // label51
            // 
            this.label51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label51.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label51.Location = new System.Drawing.Point(729, 30);
            this.label51.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(285, 30);
            this.label51.TabIndex = 19;
            this.label51.Text = "Montant de la plus grosse écriture";
            // 
            // label50
            // 
            this.label50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label50.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label50.Location = new System.Drawing.Point(2, 30);
            this.label50.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(245, 30);
            this.label50.TabIndex = 17;
            this.label50.Text = "Montant total des créances";
            // 
            // label49
            // 
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label49.Location = new System.Drawing.Point(729, 0);
            this.label49.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(285, 30);
            this.label49.TabIndex = 15;
            this.label49.Text = "Nombre de comptes client non soldés";
            // 
            // txtEcrituresNonLettrees
            // 
            this.txtEcrituresNonLettrees.AccAcceptNumbersOnly = false;
            this.txtEcrituresNonLettrees.AccAllowComma = false;
            this.txtEcrituresNonLettrees.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEcrituresNonLettrees.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEcrituresNonLettrees.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEcrituresNonLettrees.AccHidenValue = "";
            this.txtEcrituresNonLettrees.AccNotAllowedChars = null;
            this.txtEcrituresNonLettrees.AccReadOnly = false;
            this.txtEcrituresNonLettrees.AccReadOnlyAllowDelete = false;
            this.txtEcrituresNonLettrees.AccRequired = false;
            this.txtEcrituresNonLettrees.BackColor = System.Drawing.Color.White;
            this.txtEcrituresNonLettrees.CustomBackColor = System.Drawing.Color.White;
            this.txtEcrituresNonLettrees.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEcrituresNonLettrees.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEcrituresNonLettrees.ForeColor = System.Drawing.Color.Black;
            this.txtEcrituresNonLettrees.Location = new System.Drawing.Point(251, 2);
            this.txtEcrituresNonLettrees.Margin = new System.Windows.Forms.Padding(2);
            this.txtEcrituresNonLettrees.MaxLength = 32767;
            this.txtEcrituresNonLettrees.Multiline = false;
            this.txtEcrituresNonLettrees.Name = "txtEcrituresNonLettrees";
            this.txtEcrituresNonLettrees.ReadOnly = false;
            this.txtEcrituresNonLettrees.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEcrituresNonLettrees.Size = new System.Drawing.Size(474, 27);
            this.txtEcrituresNonLettrees.TabIndex = 0;
            this.txtEcrituresNonLettrees.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEcrituresNonLettrees.UseSystemPasswordChar = false;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label19.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label19.Location = new System.Drawing.Point(2, 0);
            this.Label19.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(245, 30);
            this.Label19.TabIndex = 13;
            this.Label19.Text = "Nombre d\'écritures non lettrées";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.Controls.Add(this.label17, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.txtArretDate, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.txtEcheance2, 1, 1);
            this.tableLayoutPanel25.Controls.Add(this.Check6, 2, 1);
            this.tableLayoutPanel25.Location = new System.Drawing.Point(5, 4);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 2;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(1502, 60);
            this.tableLayoutPanel25.TabIndex = 489;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label17.Location = new System.Drawing.Point(2, 2);
            this.label17.Margin = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(224, 19);
            this.label17.TabIndex = 449;
            this.label17.Text = "Ecritures arrêtées à la date du :";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.optRelEcritureLettree);
            this.panel1.Controls.Add(this.optRelEcritureNonLettree);
            this.panel1.Location = new System.Drawing.Point(1003, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 19);
            this.panel1.TabIndex = 47;
            this.panel1.Visible = false;
            // 
            // optRelEcritureLettree
            // 
            this.optRelEcritureLettree.AutoSize = true;
            this.optRelEcritureLettree.Location = new System.Drawing.Point(26, 50);
            this.optRelEcritureLettree.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optRelEcritureLettree.Name = "optRelEcritureLettree";
            this.optRelEcritureLettree.Size = new System.Drawing.Size(143, 20);
            this.optRelEcritureLettree.TabIndex = 445;
            this.optRelEcritureLettree.Tag = "6";
            this.optRelEcritureLettree.Text = "Toutes les écritures";
            this.optRelEcritureLettree.UseVisualStyleBackColor = true;
            this.optRelEcritureLettree.Visible = false;
            // 
            // optRelEcritureNonLettree
            // 
            this.optRelEcritureNonLettree.AutoSize = true;
            this.optRelEcritureNonLettree.Checked = true;
            this.optRelEcritureNonLettree.Location = new System.Drawing.Point(26, 21);
            this.optRelEcritureNonLettree.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optRelEcritureNonLettree.Name = "optRelEcritureNonLettree";
            this.optRelEcritureNonLettree.Size = new System.Drawing.Size(150, 20);
            this.optRelEcritureNonLettree.TabIndex = 446;
            this.optRelEcritureNonLettree.TabStop = true;
            this.optRelEcritureNonLettree.Tag = "6";
            this.optRelEcritureNonLettree.Text = "Ecritures non lettrées";
            this.optRelEcritureNonLettree.UseVisualStyleBackColor = true;
            this.optRelEcritureNonLettree.Visible = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label18.Location = new System.Drawing.Point(2, 32);
            this.label18.Margin = new System.Windows.Forms.Padding(2, 2, 2, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(237, 19);
            this.label18.TabIndex = 451;
            this.label18.Text = "Echéances arrêtées à la date du :";
            // 
            // txtArretDate
            // 
            this.txtArretDate.AccAcceptNumbersOnly = false;
            this.txtArretDate.AccAllowComma = false;
            this.txtArretDate.AccBackgroundColor = System.Drawing.Color.White;
            this.txtArretDate.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtArretDate.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtArretDate.AccHidenValue = "";
            this.txtArretDate.AccNotAllowedChars = null;
            this.txtArretDate.AccReadOnly = false;
            this.txtArretDate.AccReadOnlyAllowDelete = false;
            this.txtArretDate.AccRequired = false;
            this.txtArretDate.BackColor = System.Drawing.Color.White;
            this.txtArretDate.CustomBackColor = System.Drawing.Color.White;
            this.txtArretDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArretDate.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtArretDate.ForeColor = System.Drawing.Color.Black;
            this.txtArretDate.Location = new System.Drawing.Point(502, 2);
            this.txtArretDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtArretDate.MaxLength = 32767;
            this.txtArretDate.Multiline = false;
            this.txtArretDate.Name = "txtArretDate";
            this.txtArretDate.ReadOnly = false;
            this.txtArretDate.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtArretDate.Size = new System.Drawing.Size(496, 27);
            this.txtArretDate.TabIndex = 100;
            this.txtArretDate.Tag = "5";
            this.txtArretDate.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtArretDate.UseSystemPasswordChar = false;
            // 
            // txtEcheance2
            // 
            this.txtEcheance2.AccAcceptNumbersOnly = false;
            this.txtEcheance2.AccAllowComma = false;
            this.txtEcheance2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEcheance2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEcheance2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEcheance2.AccHidenValue = "";
            this.txtEcheance2.AccNotAllowedChars = null;
            this.txtEcheance2.AccReadOnly = false;
            this.txtEcheance2.AccReadOnlyAllowDelete = false;
            this.txtEcheance2.AccRequired = false;
            this.txtEcheance2.BackColor = System.Drawing.Color.White;
            this.txtEcheance2.CustomBackColor = System.Drawing.Color.White;
            this.txtEcheance2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEcheance2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEcheance2.ForeColor = System.Drawing.Color.Black;
            this.txtEcheance2.Location = new System.Drawing.Point(502, 32);
            this.txtEcheance2.Margin = new System.Windows.Forms.Padding(2);
            this.txtEcheance2.MaxLength = 32767;
            this.txtEcheance2.Multiline = false;
            this.txtEcheance2.Name = "txtEcheance2";
            this.txtEcheance2.ReadOnly = false;
            this.txtEcheance2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEcheance2.Size = new System.Drawing.Size(496, 27);
            this.txtEcheance2.TabIndex = 101;
            this.txtEcheance2.Tag = "6";
            this.txtEcheance2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEcheance2.UseSystemPasswordChar = false;
            // 
            // Check6
            // 
            this.Check6.AutoSize = true;
            this.Check6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Check6.Location = new System.Drawing.Point(1002, 33);
            this.Check6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Check6.Name = "Check6";
            this.Check6.Size = new System.Drawing.Size(334, 23);
            this.Check6.TabIndex = 434;
            this.Check6.Text = "Uniquement immeuble avec contrats résiliés";
            this.Check6.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel35);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 21);
            this.ultraTabPageControl1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1844, 796);
            // 
            // tableLayoutPanel35
            // 
            this.tableLayoutPanel35.ColumnCount = 1;
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel35.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel35.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel35.Controls.Add(this.Frame70, 0, 0);
            this.tableLayoutPanel35.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel35.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel35.Name = "tableLayoutPanel35";
            this.tableLayoutPanel35.RowCount = 3;
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.78168F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 26.12086F));
            this.tableLayoutPanel35.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel35.Size = new System.Drawing.Size(1844, 796);
            this.tableLayoutPanel35.TabIndex = 0;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel8.ColumnCount = 3;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel9, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel10, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.SSOleGridImmeuble, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel11, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.cmdImpressionImm, 2, 2);
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 400);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 3;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1838, 393);
            this.tableLayoutPanel8.TabIndex = 417;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.chkSyndic3, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.chkSyndic0, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(830, 3);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(177, 171);
            this.tableLayoutPanel9.TabIndex = 26;
            // 
            // chkSyndic3
            // 
            this.chkSyndic3.AutoSize = true;
            this.chkSyndic3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkSyndic3.Location = new System.Drawing.Point(2, 98);
            this.chkSyndic3.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSyndic3.Name = "chkSyndic3";
            this.chkSyndic3.Size = new System.Drawing.Size(60, 23);
            this.chkSyndic3.TabIndex = 26;
            this.chkSyndic3.Text = "UNIS";
            this.chkSyndic3.UseVisualStyleBackColor = true;
            // 
            // chkSyndic0
            // 
            this.chkSyndic0.AutoSize = true;
            this.chkSyndic0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkSyndic0.Location = new System.Drawing.Point(2, 23);
            this.chkSyndic0.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSyndic0.Name = "chkSyndic0";
            this.chkSyndic0.Size = new System.Drawing.Size(72, 23);
            this.chkSyndic0.TabIndex = 25;
            this.chkSyndic0.Text = "FNAIM";
            this.chkSyndic0.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Controls.Add(this.txtTotalComm, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(1010, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 2;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(828, 177);
            this.tableLayoutPanel10.TabIndex = 27;
            // 
            // txtTotalComm
            // 
            this.txtTotalComm.AccAcceptNumbersOnly = false;
            this.txtTotalComm.AccAllowComma = false;
            this.txtTotalComm.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotalComm.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotalComm.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotalComm.AccHidenValue = "";
            this.txtTotalComm.AccNotAllowedChars = null;
            this.txtTotalComm.AccReadOnly = false;
            this.txtTotalComm.AccReadOnlyAllowDelete = false;
            this.txtTotalComm.AccRequired = false;
            this.txtTotalComm.BackColor = System.Drawing.Color.White;
            this.txtTotalComm.CustomBackColor = System.Drawing.Color.White;
            this.txtTotalComm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalComm.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalComm.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(136)))), ((int)(((byte)(229)))));
            this.txtTotalComm.Location = new System.Drawing.Point(2, 22);
            this.txtTotalComm.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalComm.MaxLength = 500;
            this.txtTotalComm.Multiline = true;
            this.txtTotalComm.Name = "txtTotalComm";
            this.txtTotalComm.ReadOnly = false;
            this.txtTotalComm.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotalComm.Size = new System.Drawing.Size(824, 153);
            this.txtTotalComm.TabIndex = 17;
            this.txtTotalComm.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotalComm.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.label7.Location = new System.Drawing.Point(2, 0);
            this.label7.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 16);
            this.label7.TabIndex = 15;
            this.label7.Text = "Commerciaux";
            // 
            // SSOleGridImmeuble
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.SSOleGridImmeuble, 3);
            appearance136.BackColor = System.Drawing.SystemColors.Window;
            appearance136.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleGridImmeuble.DisplayLayout.Appearance = appearance136;
            this.SSOleGridImmeuble.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSOleGridImmeuble.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleGridImmeuble.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance137.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance137.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance137.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance137.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleGridImmeuble.DisplayLayout.GroupByBox.Appearance = appearance137;
            appearance138.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleGridImmeuble.DisplayLayout.GroupByBox.BandLabelAppearance = appearance138;
            this.SSOleGridImmeuble.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance139.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance139.BackColor2 = System.Drawing.SystemColors.Control;
            appearance139.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance139.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleGridImmeuble.DisplayLayout.GroupByBox.PromptAppearance = appearance139;
            this.SSOleGridImmeuble.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleGridImmeuble.DisplayLayout.MaxRowScrollRegions = 1;
            appearance140.BackColor = System.Drawing.SystemColors.Window;
            appearance140.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleGridImmeuble.DisplayLayout.Override.ActiveCellAppearance = appearance140;
            appearance141.BackColor = System.Drawing.Color.Transparent;
            appearance141.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleGridImmeuble.DisplayLayout.Override.ActiveRowAppearance = appearance141;
            this.SSOleGridImmeuble.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSOleGridImmeuble.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleGridImmeuble.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleGridImmeuble.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleGridImmeuble.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance142.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleGridImmeuble.DisplayLayout.Override.CardAreaAppearance = appearance142;
            appearance143.BorderColor = System.Drawing.Color.Silver;
            appearance143.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleGridImmeuble.DisplayLayout.Override.CellAppearance = appearance143;
            this.SSOleGridImmeuble.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleGridImmeuble.DisplayLayout.Override.CellPadding = 0;
            appearance144.BackColor = System.Drawing.SystemColors.Control;
            appearance144.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance144.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance144.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance144.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleGridImmeuble.DisplayLayout.Override.GroupByRowAppearance = appearance144;
            appearance145.TextHAlignAsString = "Left";
            this.SSOleGridImmeuble.DisplayLayout.Override.HeaderAppearance = appearance145;
            this.SSOleGridImmeuble.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleGridImmeuble.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance146.BackColor = System.Drawing.SystemColors.Window;
            appearance146.BorderColor = System.Drawing.Color.Silver;
            this.SSOleGridImmeuble.DisplayLayout.Override.RowAppearance = appearance146;
            this.SSOleGridImmeuble.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleGridImmeuble.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance147.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleGridImmeuble.DisplayLayout.Override.TemplateAddRowAppearance = appearance147;
            this.SSOleGridImmeuble.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleGridImmeuble.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleGridImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleGridImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleGridImmeuble.Location = new System.Drawing.Point(2, 180);
            this.SSOleGridImmeuble.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.SSOleGridImmeuble.Name = "SSOleGridImmeuble";
            this.SSOleGridImmeuble.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.SSOleGridImmeuble.Size = new System.Drawing.Size(1834, 171);
            this.SSOleGridImmeuble.TabIndex = 22;
            this.SSOleGridImmeuble.Text = "ultraGrid1";
            this.SSOleGridImmeuble.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleGridImmeuble_InitializeLayout);
            this.SSOleGridImmeuble.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSOleGridImmeuble_InitializeRow);
            this.SSOleGridImmeuble.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleGridImmeuble_DoubleClickRow);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.label16s, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtObservations, 0, 1);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(827, 177);
            this.tableLayoutPanel11.TabIndex = 26;
            // 
            // label16s
            // 
            this.label16s.AutoSize = true;
            this.label16s.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.label16s.Location = new System.Drawing.Point(2, 0);
            this.label16s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16s.Name = "label16s";
            this.label16s.Size = new System.Drawing.Size(144, 16);
            this.label16s.TabIndex = 10;
            this.label16s.Text = "Informations générales";
            // 
            // txtObservations
            // 
            this.txtObservations.AccAcceptNumbersOnly = false;
            this.txtObservations.AccAllowComma = false;
            this.txtObservations.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObservations.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObservations.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObservations.AccHidenValue = "";
            this.txtObservations.AccNotAllowedChars = null;
            this.txtObservations.AccReadOnly = false;
            this.txtObservations.AccReadOnlyAllowDelete = false;
            this.txtObservations.AccRequired = false;
            this.txtObservations.BackColor = System.Drawing.Color.White;
            this.txtObservations.CustomBackColor = System.Drawing.Color.White;
            this.txtObservations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservations.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtObservations.ForeColor = System.Drawing.Color.Black;
            this.txtObservations.Location = new System.Drawing.Point(2, 22);
            this.txtObservations.Margin = new System.Windows.Forms.Padding(2);
            this.txtObservations.MaxLength = 500;
            this.txtObservations.Multiline = true;
            this.txtObservations.Name = "txtObservations";
            this.txtObservations.ReadOnly = false;
            this.txtObservations.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObservations.Size = new System.Drawing.Size(823, 153);
            this.txtObservations.TabIndex = 16;
            this.txtObservations.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObservations.UseSystemPasswordChar = false;
            this.txtObservations.TextChanged += new System.EventHandler(this.txtObservations_TextChanged);
            // 
            // cmdImpressionImm
            // 
            this.cmdImpressionImm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImpressionImm.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdImpressionImm.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdImpressionImm.FlatAppearance.BorderSize = 0;
            this.cmdImpressionImm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImpressionImm.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImpressionImm.Location = new System.Drawing.Point(1776, 356);
            this.cmdImpressionImm.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImpressionImm.Name = "cmdImpressionImm";
            this.cmdImpressionImm.Size = new System.Drawing.Size(60, 35);
            this.cmdImpressionImm.TabIndex = 369;
            this.toolTip1.SetToolTip(this.cmdImpressionImm, "Imprime les immeubles,les gérants et les contrats du client en cours");
            this.cmdImpressionImm.UseVisualStyleBackColor = false;
            this.cmdImpressionImm.Click += new System.EventHandler(this.cmdImpressionImm_Click);
            // 
            // Frame70
            // 
            this.Frame70.Controls.Add(this.tableLayoutPanel4);
            this.Frame70.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame70.Location = new System.Drawing.Point(2, 3);
            this.Frame70.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Frame70.Name = "Frame70";
            this.Frame70.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Frame70.Size = new System.Drawing.Size(1840, 183);
            this.Frame70.TabIndex = 417;
            this.Frame70.TabStop = false;
            this.Frame70.Text = "Informations Adresse";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel10, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1836, 162);
            this.tableLayoutPanel4.TabIndex = 406;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.tableLayoutPanel2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(918, 162);
            this.panel2.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.36318F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.63682F));
            this.tableLayoutPanel2.Controls.Add(this.panel9, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtVille, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtCodePostal, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(918, 162);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.txtAdresse1);
            this.panel9.Controls.Add(this.txtAdresse2);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel9.Location = new System.Drawing.Point(251, 0);
            this.panel9.Margin = new System.Windows.Forms.Padding(0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(667, 32);
            this.panel9.TabIndex = 406;
            // 
            // txtAdresse1
            // 
            this.txtAdresse1.AccAcceptNumbersOnly = false;
            this.txtAdresse1.AccAllowComma = false;
            this.txtAdresse1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresse1.AccHidenValue = "";
            this.txtAdresse1.AccNotAllowedChars = null;
            this.txtAdresse1.AccReadOnly = false;
            this.txtAdresse1.AccReadOnlyAllowDelete = false;
            this.txtAdresse1.AccRequired = false;
            this.txtAdresse1.BackColor = System.Drawing.Color.White;
            this.txtAdresse1.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdresse1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAdresse1.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse1.Location = new System.Drawing.Point(0, 0);
            this.txtAdresse1.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse1.MaxLength = 50;
            this.txtAdresse1.Multiline = false;
            this.txtAdresse1.Name = "txtAdresse1";
            this.txtAdresse1.ReadOnly = false;
            this.txtAdresse1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse1.Size = new System.Drawing.Size(667, 27);
            this.txtAdresse1.TabIndex = 11;
            this.txtAdresse1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse1.UseSystemPasswordChar = false;
            this.txtAdresse1.TextChanged += new System.EventHandler(this.txtAdresse1_TextChanged);
            // 
            // txtAdresse2
            // 
            this.txtAdresse2.AccAcceptNumbersOnly = false;
            this.txtAdresse2.AccAllowComma = false;
            this.txtAdresse2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresse2.AccHidenValue = "";
            this.txtAdresse2.AccNotAllowedChars = null;
            this.txtAdresse2.AccReadOnly = false;
            this.txtAdresse2.AccReadOnlyAllowDelete = false;
            this.txtAdresse2.AccRequired = false;
            this.txtAdresse2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAdresse2.BackColor = System.Drawing.Color.White;
            this.txtAdresse2.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse2.Font = new System.Drawing.Font("Tahoma", 11F);
            this.txtAdresse2.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse2.Location = new System.Drawing.Point(2, 32);
            this.txtAdresse2.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse2.MaxLength = 50;
            this.txtAdresse2.Multiline = false;
            this.txtAdresse2.Name = "txtAdresse2";
            this.txtAdresse2.ReadOnly = false;
            this.txtAdresse2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse2.Size = new System.Drawing.Size(663, 27);
            this.txtAdresse2.TabIndex = 12;
            this.txtAdresse2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse2.UseSystemPasswordChar = false;
            this.txtAdresse2.TextChanged += new System.EventHandler(this.txtAdresse2_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(2, 0);
            this.label9.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 19);
            this.label9.TabIndex = 10;
            this.label9.Text = "Adresse";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label10.Location = new System.Drawing.Point(2, 66);
            this.label10.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 19);
            this.label10.TabIndex = 16;
            this.label10.Text = "Ville";
            // 
            // txtVille
            // 
            this.txtVille.AccAcceptNumbersOnly = false;
            this.txtVille.AccAllowComma = false;
            this.txtVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVille.AccHidenValue = "";
            this.txtVille.AccNotAllowedChars = null;
            this.txtVille.AccReadOnly = false;
            this.txtVille.AccReadOnlyAllowDelete = false;
            this.txtVille.AccRequired = false;
            this.txtVille.BackColor = System.Drawing.Color.White;
            this.txtVille.CustomBackColor = System.Drawing.Color.White;
            this.txtVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVille.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtVille.ForeColor = System.Drawing.Color.Black;
            this.txtVille.Location = new System.Drawing.Point(253, 68);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.MaxLength = 50;
            this.txtVille.Multiline = false;
            this.txtVille.Name = "txtVille";
            this.txtVille.ReadOnly = false;
            this.txtVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVille.Size = new System.Drawing.Size(663, 27);
            this.txtVille.TabIndex = 407;
            this.txtVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVille.UseSystemPasswordChar = false;
            this.txtVille.TextChanged += new System.EventHandler(this.txtVille_TextChanged);
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.AccAcceptNumbersOnly = false;
            this.txtCodePostal.AccAllowComma = false;
            this.txtCodePostal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodePostal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodePostal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodePostal.AccHidenValue = "";
            this.txtCodePostal.AccNotAllowedChars = null;
            this.txtCodePostal.AccReadOnly = false;
            this.txtCodePostal.AccReadOnlyAllowDelete = false;
            this.txtCodePostal.AccRequired = false;
            this.txtCodePostal.BackColor = System.Drawing.Color.White;
            this.txtCodePostal.CustomBackColor = System.Drawing.Color.White;
            this.txtCodePostal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodePostal.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodePostal.ForeColor = System.Drawing.Color.Black;
            this.txtCodePostal.Location = new System.Drawing.Point(253, 34);
            this.txtCodePostal.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodePostal.MaxLength = 5;
            this.txtCodePostal.Multiline = false;
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.ReadOnly = false;
            this.txtCodePostal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodePostal.Size = new System.Drawing.Size(663, 27);
            this.txtCodePostal.TabIndex = 406;
            this.txtCodePostal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodePostal.UseSystemPasswordChar = false;
            this.txtCodePostal.TextChanged += new System.EventHandler(this.txtCodePostal_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label11.Location = new System.Drawing.Point(2, 32);
            this.label11.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(92, 19);
            this.label11.TabIndex = 14;
            this.label11.Text = "Code postal";
            // 
            // panel10
            // 
            this.panel10.Controls.Add(this.tableLayoutPanel3);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel10.Location = new System.Drawing.Point(918, 0);
            this.panel10.Margin = new System.Windows.Forms.Padding(0);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(918, 162);
            this.panel10.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.70529F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.29471F));
            this.tableLayoutPanel3.Controls.Add(this.cmdLogimatique, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdChangeAdresse, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.chkAdresseModifiee, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(918, 162);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // cmdLogimatique
            // 
            this.cmdLogimatique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdLogimatique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdLogimatique.FlatAppearance.BorderSize = 0;
            this.cmdLogimatique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLogimatique.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdLogimatique.ForeColor = System.Drawing.Color.White;
            this.cmdLogimatique.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.cmdLogimatique.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdLogimatique.Location = new System.Drawing.Point(559, 83);
            this.cmdLogimatique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdLogimatique.Name = "cmdLogimatique";
            this.cmdLogimatique.Size = new System.Drawing.Size(357, 77);
            this.cmdLogimatique.TabIndex = 402;
            this.cmdLogimatique.Text = "   &Export Logimatique";
            this.cmdLogimatique.UseVisualStyleBackColor = false;
            this.cmdLogimatique.Click += new System.EventHandler(this.cmdLogimatique_Click);
            // 
            // cmdChangeAdresse
            // 
            this.cmdChangeAdresse.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdChangeAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdChangeAdresse.FlatAppearance.BorderSize = 0;
            this.cmdChangeAdresse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdChangeAdresse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdChangeAdresse.ForeColor = System.Drawing.Color.White;
            this.cmdChangeAdresse.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.cmdChangeAdresse.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdChangeAdresse.Location = new System.Drawing.Point(2, 83);
            this.cmdChangeAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.cmdChangeAdresse.Name = "cmdChangeAdresse";
            this.cmdChangeAdresse.Size = new System.Drawing.Size(553, 77);
            this.cmdChangeAdresse.TabIndex = 401;
            this.cmdChangeAdresse.Text = "           Appliquer cette adresse à tous les immeubles de ce gérant";
            this.cmdChangeAdresse.UseVisualStyleBackColor = false;
            this.cmdChangeAdresse.Click += new System.EventHandler(this.cmdChangeAdresse_Click);
            // 
            // chkAdresseModifiee
            // 
            this.chkAdresseModifiee.AutoSize = true;
            this.chkAdresseModifiee.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkAdresseModifiee.Location = new System.Drawing.Point(2, 3);
            this.chkAdresseModifiee.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkAdresseModifiee.Name = "chkAdresseModifiee";
            this.chkAdresseModifiee.Size = new System.Drawing.Size(148, 23);
            this.chkAdresseModifiee.TabIndex = 23;
            this.chkAdresseModifiee.Text = "Adresse Modifiée";
            this.chkAdresseModifiee.UseVisualStyleBackColor = true;
            this.chkAdresseModifiee.Visible = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.Frame70s, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.groupBox2, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 192);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1838, 202);
            this.tableLayoutPanel5.TabIndex = 417;
            // 
            // Frame70s
            // 
            this.Frame70s.Controls.Add(this.tableLayoutPanel6);
            this.Frame70s.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame70s.Location = new System.Drawing.Point(2, 3);
            this.Frame70s.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Frame70s.Name = "Frame70s";
            this.Frame70s.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Frame70s.Size = new System.Drawing.Size(915, 196);
            this.Frame70s.TabIndex = 405;
            this.Frame70s.TabStop = false;
            this.Frame70s.Text = "Informations d\'accés";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.47059F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.52941F));
            this.tableLayoutPanel6.Controls.Add(this.txtEmail, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.label13s, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.txtTelephone, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtFax, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.label12s, 0, 1);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(911, 175);
            this.tableLayoutPanel6.TabIndex = 408;
            // 
            // txtEmail
            // 
            this.txtEmail.AccAcceptNumbersOnly = false;
            this.txtEmail.AccAllowComma = false;
            this.txtEmail.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEmail.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEmail.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEmail.AccHidenValue = "";
            this.txtEmail.AccNotAllowedChars = null;
            this.txtEmail.AccReadOnly = false;
            this.txtEmail.AccReadOnlyAllowDelete = false;
            this.txtEmail.AccRequired = false;
            this.txtEmail.BackColor = System.Drawing.Color.White;
            this.txtEmail.CustomBackColor = System.Drawing.Color.White;
            this.txtEmail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEmail.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEmail.ForeColor = System.Drawing.Color.Black;
            this.txtEmail.Location = new System.Drawing.Point(243, 62);
            this.txtEmail.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail.MaxLength = 50;
            this.txtEmail.Multiline = false;
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.ReadOnly = false;
            this.txtEmail.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEmail.Size = new System.Drawing.Size(666, 27);
            this.txtEmail.TabIndex = 13;
            this.txtEmail.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEmail.UseSystemPasswordChar = false;
            this.txtEmail.TextChanged += new System.EventHandler(this.txtEmail_TextChanged);
            // 
            // label13s
            // 
            this.label13s.AutoSize = true;
            this.label13s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label13s.Location = new System.Drawing.Point(2, 0);
            this.label13s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label13s.Name = "label13s";
            this.label13s.Size = new System.Drawing.Size(84, 19);
            this.label13s.TabIndex = 3;
            this.label13s.Text = "Téléphone";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(2, 60);
            this.label8.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(52, 19);
            this.label8.TabIndex = 1;
            this.label8.Text = "E-mail";
            // 
            // txtTelephone
            // 
            this.txtTelephone.AccAcceptNumbersOnly = false;
            this.txtTelephone.AccAllowComma = false;
            this.txtTelephone.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTelephone.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTelephone.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTelephone.AccHidenValue = "";
            this.txtTelephone.AccNotAllowedChars = null;
            this.txtTelephone.AccReadOnly = false;
            this.txtTelephone.AccReadOnlyAllowDelete = false;
            this.txtTelephone.AccRequired = false;
            this.txtTelephone.BackColor = System.Drawing.Color.White;
            this.txtTelephone.CustomBackColor = System.Drawing.Color.White;
            this.txtTelephone.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelephone.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTelephone.ForeColor = System.Drawing.Color.Black;
            this.txtTelephone.Location = new System.Drawing.Point(243, 2);
            this.txtTelephone.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelephone.MaxLength = 50;
            this.txtTelephone.Multiline = false;
            this.txtTelephone.Name = "txtTelephone";
            this.txtTelephone.ReadOnly = false;
            this.txtTelephone.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTelephone.Size = new System.Drawing.Size(666, 27);
            this.txtTelephone.TabIndex = 11;
            this.txtTelephone.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTelephone.UseSystemPasswordChar = false;
            this.txtTelephone.TextChanged += new System.EventHandler(this.txtTelephone_TextChanged);
            // 
            // txtFax
            // 
            this.txtFax.AccAcceptNumbersOnly = false;
            this.txtFax.AccAllowComma = false;
            this.txtFax.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFax.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFax.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFax.AccHidenValue = "";
            this.txtFax.AccNotAllowedChars = null;
            this.txtFax.AccReadOnly = false;
            this.txtFax.AccReadOnlyAllowDelete = false;
            this.txtFax.AccRequired = false;
            this.txtFax.BackColor = System.Drawing.Color.White;
            this.txtFax.CustomBackColor = System.Drawing.Color.White;
            this.txtFax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFax.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFax.ForeColor = System.Drawing.Color.Black;
            this.txtFax.Location = new System.Drawing.Point(243, 32);
            this.txtFax.Margin = new System.Windows.Forms.Padding(2);
            this.txtFax.MaxLength = 50;
            this.txtFax.Multiline = false;
            this.txtFax.Name = "txtFax";
            this.txtFax.ReadOnly = false;
            this.txtFax.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFax.Size = new System.Drawing.Size(666, 27);
            this.txtFax.TabIndex = 12;
            this.txtFax.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFax.UseSystemPasswordChar = false;
            this.txtFax.TextChanged += new System.EventHandler(this.txtFax_TextChanged);
            // 
            // label12s
            // 
            this.label12s.AutoSize = true;
            this.label12s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label12s.Location = new System.Drawing.Point(2, 30);
            this.label12s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label12s.Name = "label12s";
            this.label12s.Size = new System.Drawing.Size(32, 19);
            this.label12s.TabIndex = 2;
            this.label12s.Text = "Fax";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.tableLayoutPanel7);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Location = new System.Drawing.Point(921, 3);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox2.Size = new System.Drawing.Size(915, 196);
            this.groupBox2.TabIndex = 406;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Acces au site Extranet";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.61488F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.38512F));
            this.tableLayoutPanel7.Controls.Add(this.chkSyndic1, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.label25, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.chkSyndic2, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.label43s, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtMotPasse, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtLogin, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(911, 175);
            this.tableLayoutPanel7.TabIndex = 408;
            // 
            // chkSyndic1
            // 
            this.chkSyndic1.AutoSize = true;
            this.chkSyndic1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkSyndic1.Location = new System.Drawing.Point(317, 119);
            this.chkSyndic1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSyndic1.Name = "chkSyndic1";
            this.chkSyndic1.Size = new System.Drawing.Size(68, 23);
            this.chkSyndic1.TabIndex = 25;
            this.chkSyndic1.Text = "CNAB";
            this.chkSyndic1.UseVisualStyleBackColor = true;
            this.chkSyndic1.Visible = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label25.Location = new System.Drawing.Point(2, 58);
            this.label25.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(101, 19);
            this.label25.TabIndex = 114;
            this.label25.Text = "Mot de passe";
            // 
            // chkSyndic2
            // 
            this.chkSyndic2.AutoSize = true;
            this.chkSyndic2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkSyndic2.Location = new System.Drawing.Point(2, 119);
            this.chkSyndic2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkSyndic2.Name = "chkSyndic2";
            this.chkSyndic2.Size = new System.Drawing.Size(65, 23);
            this.chkSyndic2.TabIndex = 24;
            this.chkSyndic2.Text = "CSAB";
            this.chkSyndic2.UseVisualStyleBackColor = true;
            this.chkSyndic2.Visible = false;
            // 
            // label43s
            // 
            this.label43s.AutoSize = true;
            this.label43s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label43s.Location = new System.Drawing.Point(2, 0);
            this.label43s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label43s.Name = "label43s";
            this.label43s.Size = new System.Drawing.Size(139, 19);
            this.label43s.TabIndex = 100;
            this.label43s.Text = "Login site Extranet";
            // 
            // txtMotPasse
            // 
            this.txtMotPasse.AccAcceptNumbersOnly = false;
            this.txtMotPasse.AccAllowComma = false;
            this.txtMotPasse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMotPasse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMotPasse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMotPasse.AccHidenValue = "";
            this.txtMotPasse.AccNotAllowedChars = null;
            this.txtMotPasse.AccReadOnly = false;
            this.txtMotPasse.AccReadOnlyAllowDelete = false;
            this.txtMotPasse.AccRequired = false;
            this.txtMotPasse.BackColor = System.Drawing.Color.White;
            this.txtMotPasse.CustomBackColor = System.Drawing.Color.White;
            this.txtMotPasse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMotPasse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMotPasse.ForeColor = System.Drawing.Color.Black;
            this.txtMotPasse.Location = new System.Drawing.Point(317, 60);
            this.txtMotPasse.Margin = new System.Windows.Forms.Padding(2);
            this.txtMotPasse.MaxLength = 50;
            this.txtMotPasse.Multiline = false;
            this.txtMotPasse.Name = "txtMotPasse";
            this.txtMotPasse.ReadOnly = false;
            this.txtMotPasse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMotPasse.Size = new System.Drawing.Size(592, 27);
            this.txtMotPasse.TabIndex = 15;
            this.txtMotPasse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMotPasse.UseSystemPasswordChar = false;
            // 
            // txtLogin
            // 
            this.txtLogin.AccAcceptNumbersOnly = false;
            this.txtLogin.AccAllowComma = false;
            this.txtLogin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLogin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLogin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLogin.AccHidenValue = "";
            this.txtLogin.AccNotAllowedChars = null;
            this.txtLogin.AccReadOnly = true;
            this.txtLogin.AccReadOnlyAllowDelete = false;
            this.txtLogin.AccRequired = false;
            this.txtLogin.BackColor = System.Drawing.Color.White;
            this.txtLogin.CustomBackColor = System.Drawing.Color.White;
            this.txtLogin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLogin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLogin.ForeColor = System.Drawing.Color.Black;
            this.txtLogin.Location = new System.Drawing.Point(317, 2);
            this.txtLogin.Margin = new System.Windows.Forms.Padding(2);
            this.txtLogin.MaxLength = 50;
            this.txtLogin.Multiline = false;
            this.txtLogin.Name = "txtLogin";
            this.txtLogin.ReadOnly = false;
            this.txtLogin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLogin.Size = new System.Drawing.Size(592, 27);
            this.txtLogin.TabIndex = 14;
            this.txtLogin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLogin.UseSystemPasswordChar = false;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel12);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1844, 796);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 5;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.Controls.Add(this.cmdChangeCommercial, 4, 1);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel13, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel14, 3, 1);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel15, 2, 1);
            this.tableLayoutPanel12.Controls.Add(this.tableLayoutPanel16, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.groupBox1, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.Padding = new System.Windows.Forms.Padding(3);
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1844, 796);
            this.tableLayoutPanel12.TabIndex = 520;
            // 
            // cmdChangeCommercial
            // 
            this.cmdChangeCommercial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdChangeCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdChangeCommercial.FlatAppearance.BorderSize = 0;
            this.cmdChangeCommercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdChangeCommercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdChangeCommercial.ForeColor = System.Drawing.Color.White;
            this.cmdChangeCommercial.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.cmdChangeCommercial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdChangeCommercial.Location = new System.Drawing.Point(1412, 735);
            this.cmdChangeCommercial.Margin = new System.Windows.Forms.Padding(2);
            this.cmdChangeCommercial.Name = "cmdChangeCommercial";
            this.cmdChangeCommercial.Size = new System.Drawing.Size(427, 56);
            this.cmdChangeCommercial.TabIndex = 508;
            this.cmdChangeCommercial.Text = "        Appliquer ce commercail à tous les immeubles de ce gérant";
            this.cmdChangeCommercial.UseVisualStyleBackColor = false;
            this.cmdChangeCommercial.Click += new System.EventHandler(this.cmdChangeCommercial_Click);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(6, 736);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 2;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(114, 54);
            this.tableLayoutPanel13.TabIndex = 522;
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(2, 24);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 30);
            this.label4.TabIndex = 512;
            this.label4.Text = "Commercial";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.Label5740, 0, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(984, 736);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 2;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(423, 54);
            this.tableLayoutPanel14.TabIndex = 521;
            // 
            // label5
            // 
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 0);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(419, 20);
            this.label5.TabIndex = 511;
            this.label5.Text = "Prenom";
            this.label5.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5740
            // 
            this.Label5740.AccAcceptNumbersOnly = false;
            this.Label5740.AccAllowComma = false;
            this.Label5740.AccBackgroundColor = System.Drawing.Color.White;
            this.Label5740.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label5740.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label5740.AccHidenValue = "";
            this.Label5740.AccNotAllowedChars = null;
            this.Label5740.AccReadOnly = true;
            this.Label5740.AccReadOnlyAllowDelete = false;
            this.Label5740.AccRequired = false;
            this.Label5740.BackColor = System.Drawing.Color.White;
            this.Label5740.CustomBackColor = System.Drawing.Color.White;
            this.Label5740.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label5740.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5740.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(136)))), ((int)(((byte)(229)))));
            this.Label5740.Location = new System.Drawing.Point(2, 25);
            this.Label5740.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.Label5740.MaxLength = 32767;
            this.Label5740.Multiline = false;
            this.Label5740.Name = "Label5740";
            this.Label5740.ReadOnly = false;
            this.Label5740.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label5740.Size = new System.Drawing.Size(419, 27);
            this.Label5740.TabIndex = 505;
            this.Label5740.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Label5740.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.label6, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.Label5742, 0, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(555, 736);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(423, 54);
            this.tableLayoutPanel15.TabIndex = 520;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(2, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(419, 20);
            this.label6.TabIndex = 511;
            this.label6.Text = "Nom";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Label5742
            // 
            this.Label5742.AccAcceptNumbersOnly = false;
            this.Label5742.AccAllowComma = false;
            this.Label5742.AccBackgroundColor = System.Drawing.Color.White;
            this.Label5742.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label5742.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label5742.AccHidenValue = "";
            this.Label5742.AccNotAllowedChars = null;
            this.Label5742.AccReadOnly = true;
            this.Label5742.AccReadOnlyAllowDelete = false;
            this.Label5742.AccRequired = false;
            this.Label5742.BackColor = System.Drawing.Color.White;
            this.Label5742.CustomBackColor = System.Drawing.Color.White;
            this.Label5742.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label5742.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label5742.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(136)))), ((int)(((byte)(229)))));
            this.Label5742.Location = new System.Drawing.Point(2, 25);
            this.Label5742.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.Label5742.MaxLength = 32767;
            this.Label5742.Multiline = false;
            this.Label5742.Name = "Label5742";
            this.Label5742.ReadOnly = false;
            this.Label5742.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label5742.Size = new System.Drawing.Size(419, 27);
            this.Label5742.TabIndex = 504;
            this.Label5742.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Label5742.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 1;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.SSOleDBCmbCommercial, 0, 1);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(126, 736);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(423, 54);
            this.tableLayoutPanel16.TabIndex = 519;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(2, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(419, 20);
            this.label3.TabIndex = 511;
            this.label3.Text = "Code Gerant";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // SSOleDBCmbCommercial
            // 
            appearance148.BackColor = System.Drawing.SystemColors.Window;
            appearance148.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBCmbCommercial.DisplayLayout.Appearance = appearance148;
            this.SSOleDBCmbCommercial.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBCmbCommercial.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance149.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance149.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance149.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance149.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCommercial.DisplayLayout.GroupByBox.Appearance = appearance149;
            appearance150.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCmbCommercial.DisplayLayout.GroupByBox.BandLabelAppearance = appearance150;
            this.SSOleDBCmbCommercial.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance151.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance151.BackColor2 = System.Drawing.SystemColors.Control;
            appearance151.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance151.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCmbCommercial.DisplayLayout.GroupByBox.PromptAppearance = appearance151;
            this.SSOleDBCmbCommercial.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBCmbCommercial.DisplayLayout.MaxRowScrollRegions = 1;
            appearance152.BackColor = System.Drawing.SystemColors.Window;
            appearance152.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.ActiveCellAppearance = appearance152;
            appearance153.BackColor = System.Drawing.SystemColors.Highlight;
            appearance153.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.ActiveRowAppearance = appearance153;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance154.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.CardAreaAppearance = appearance154;
            appearance155.BorderColor = System.Drawing.Color.Silver;
            appearance155.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.CellAppearance = appearance155;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.CellPadding = 0;
            appearance156.BackColor = System.Drawing.SystemColors.Control;
            appearance156.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance156.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance156.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance156.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.GroupByRowAppearance = appearance156;
            appearance157.TextHAlignAsString = "Left";
            this.SSOleDBCmbCommercial.DisplayLayout.Override.HeaderAppearance = appearance157;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance158.BackColor = System.Drawing.SystemColors.Window;
            appearance158.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.RowAppearance = appearance158;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance159.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBCmbCommercial.DisplayLayout.Override.TemplateAddRowAppearance = appearance159;
            this.SSOleDBCmbCommercial.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBCmbCommercial.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBCmbCommercial.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBCmbCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBCmbCommercial.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleDBCmbCommercial.Location = new System.Drawing.Point(3, 23);
            this.SSOleDBCmbCommercial.Name = "SSOleDBCmbCommercial";
            this.SSOleDBCmbCommercial.Size = new System.Drawing.Size(417, 27);
            this.SSOleDBCmbCommercial.TabIndex = 503;
            this.SSOleDBCmbCommercial.AfterCloseUp += new System.EventHandler(this.SSOleDBCmbCommercial_AfterCloseUp);
            // 
            // groupBox1
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.groupBox1, 5);
            this.groupBox1.Controls.Add(this.ssDropMatricule);
            this.groupBox1.Controls.Add(this.SSINTERVENANT);
            this.groupBox1.Controls.Add(this.ComboQualifGest);
            this.groupBox1.Controls.Add(this.ssGridGestionnaire);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(5, 6);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(6);
            this.groupBox1.Size = new System.Drawing.Size(1834, 724);
            this.groupBox1.TabIndex = 509;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gestionnaire";
            // 
            // ssDropMatricule
            // 
            appearance160.BackColor = System.Drawing.SystemColors.Window;
            appearance160.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropMatricule.DisplayLayout.Appearance = appearance160;
            this.ssDropMatricule.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropMatricule.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance161.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance161.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance161.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance161.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.GroupByBox.Appearance = appearance161;
            appearance162.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BandLabelAppearance = appearance162;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance163.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance163.BackColor2 = System.Drawing.SystemColors.Control;
            appearance163.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance163.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.PromptAppearance = appearance163;
            this.ssDropMatricule.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropMatricule.DisplayLayout.MaxRowScrollRegions = 1;
            appearance164.BackColor = System.Drawing.SystemColors.Window;
            appearance164.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveCellAppearance = appearance164;
            appearance165.BackColor = System.Drawing.SystemColors.Highlight;
            appearance165.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveRowAppearance = appearance165;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance166.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.CardAreaAppearance = appearance166;
            appearance167.BorderColor = System.Drawing.Color.Silver;
            appearance167.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropMatricule.DisplayLayout.Override.CellAppearance = appearance167;
            this.ssDropMatricule.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropMatricule.DisplayLayout.Override.CellPadding = 0;
            appearance168.BackColor = System.Drawing.SystemColors.Control;
            appearance168.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance168.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance168.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance168.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.GroupByRowAppearance = appearance168;
            appearance169.TextHAlignAsString = "Left";
            this.ssDropMatricule.DisplayLayout.Override.HeaderAppearance = appearance169;
            this.ssDropMatricule.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropMatricule.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance170.BackColor = System.Drawing.SystemColors.Window;
            appearance170.BorderColor = System.Drawing.Color.Silver;
            this.ssDropMatricule.DisplayLayout.Override.RowAppearance = appearance170;
            this.ssDropMatricule.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance171.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropMatricule.DisplayLayout.Override.TemplateAddRowAppearance = appearance171;
            this.ssDropMatricule.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropMatricule.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropMatricule.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropMatricule.Location = new System.Drawing.Point(197, 257);
            this.ssDropMatricule.Name = "ssDropMatricule";
            this.ssDropMatricule.Size = new System.Drawing.Size(186, 25);
            this.ssDropMatricule.TabIndex = 504;
            this.ssDropMatricule.Visible = false;
            // 
            // SSINTERVENANT
            // 
            appearance172.BackColor = System.Drawing.SystemColors.Window;
            appearance172.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSINTERVENANT.DisplayLayout.Appearance = appearance172;
            this.SSINTERVENANT.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSINTERVENANT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSINTERVENANT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance173.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance173.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance173.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance173.BorderColor = System.Drawing.SystemColors.Window;
            this.SSINTERVENANT.DisplayLayout.GroupByBox.Appearance = appearance173;
            appearance174.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSINTERVENANT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance174;
            this.SSINTERVENANT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance175.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance175.BackColor2 = System.Drawing.SystemColors.Control;
            appearance175.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance175.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSINTERVENANT.DisplayLayout.GroupByBox.PromptAppearance = appearance175;
            this.SSINTERVENANT.DisplayLayout.MaxColScrollRegions = 1;
            this.SSINTERVENANT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance176.BackColor = System.Drawing.SystemColors.Window;
            appearance176.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSINTERVENANT.DisplayLayout.Override.ActiveCellAppearance = appearance176;
            appearance177.BackColor = System.Drawing.SystemColors.Highlight;
            appearance177.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSINTERVENANT.DisplayLayout.Override.ActiveRowAppearance = appearance177;
            this.SSINTERVENANT.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSINTERVENANT.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSINTERVENANT.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSINTERVENANT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSINTERVENANT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance178.BackColor = System.Drawing.SystemColors.Window;
            this.SSINTERVENANT.DisplayLayout.Override.CardAreaAppearance = appearance178;
            appearance179.BorderColor = System.Drawing.Color.Silver;
            appearance179.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSINTERVENANT.DisplayLayout.Override.CellAppearance = appearance179;
            this.SSINTERVENANT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSINTERVENANT.DisplayLayout.Override.CellPadding = 0;
            appearance180.BackColor = System.Drawing.SystemColors.Control;
            appearance180.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance180.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance180.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance180.BorderColor = System.Drawing.SystemColors.Window;
            this.SSINTERVENANT.DisplayLayout.Override.GroupByRowAppearance = appearance180;
            appearance181.TextHAlignAsString = "Left";
            this.SSINTERVENANT.DisplayLayout.Override.HeaderAppearance = appearance181;
            this.SSINTERVENANT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSINTERVENANT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance182.BackColor = System.Drawing.SystemColors.Window;
            appearance182.BorderColor = System.Drawing.Color.Silver;
            this.SSINTERVENANT.DisplayLayout.Override.RowAppearance = appearance182;
            this.SSINTERVENANT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSINTERVENANT.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance183.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSINTERVENANT.DisplayLayout.Override.TemplateAddRowAppearance = appearance183;
            this.SSINTERVENANT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSINTERVENANT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSINTERVENANT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSINTERVENANT.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSINTERVENANT.Location = new System.Drawing.Point(6, 21);
            this.SSINTERVENANT.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.SSINTERVENANT.Name = "SSINTERVENANT";
            this.SSINTERVENANT.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.SSINTERVENANT.Size = new System.Drawing.Size(1822, 697);
            this.SSINTERVENANT.TabIndex = 1;
            this.SSINTERVENANT.Text = "ultraGrid1";
            this.SSINTERVENANT.Visible = false;
            this.SSINTERVENANT.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSINTERVENANT_InitializeLayout);
            this.SSINTERVENANT.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSINTERVENANT_InitializeRow);
            this.SSINTERVENANT.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSINTERVENANT_AfterRowUpdate);
            this.SSINTERVENANT.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.SSINTERVENANT_BeforeExitEditMode);
            // 
            // ComboQualifGest
            // 
            appearance184.BackColor = System.Drawing.SystemColors.Window;
            appearance184.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ComboQualifGest.DisplayLayout.Appearance = appearance184;
            this.ComboQualifGest.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ComboQualifGest.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance185.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance185.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance185.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance185.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.GroupByBox.Appearance = appearance185;
            appearance186.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboQualifGest.DisplayLayout.GroupByBox.BandLabelAppearance = appearance186;
            this.ComboQualifGest.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance187.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance187.BackColor2 = System.Drawing.SystemColors.Control;
            appearance187.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance187.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboQualifGest.DisplayLayout.GroupByBox.PromptAppearance = appearance187;
            this.ComboQualifGest.DisplayLayout.MaxColScrollRegions = 1;
            this.ComboQualifGest.DisplayLayout.MaxRowScrollRegions = 1;
            appearance188.BackColor = System.Drawing.SystemColors.Window;
            appearance188.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ComboQualifGest.DisplayLayout.Override.ActiveCellAppearance = appearance188;
            appearance189.BackColor = System.Drawing.SystemColors.Highlight;
            appearance189.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ComboQualifGest.DisplayLayout.Override.ActiveRowAppearance = appearance189;
            this.ComboQualifGest.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ComboQualifGest.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance190.BackColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.Override.CardAreaAppearance = appearance190;
            appearance191.BorderColor = System.Drawing.Color.Silver;
            appearance191.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ComboQualifGest.DisplayLayout.Override.CellAppearance = appearance191;
            this.ComboQualifGest.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ComboQualifGest.DisplayLayout.Override.CellPadding = 0;
            appearance192.BackColor = System.Drawing.SystemColors.Control;
            appearance192.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance192.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance192.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance192.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.Override.GroupByRowAppearance = appearance192;
            appearance193.TextHAlignAsString = "Left";
            this.ComboQualifGest.DisplayLayout.Override.HeaderAppearance = appearance193;
            this.ComboQualifGest.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ComboQualifGest.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance194.BackColor = System.Drawing.SystemColors.Window;
            appearance194.BorderColor = System.Drawing.Color.Silver;
            this.ComboQualifGest.DisplayLayout.Override.RowAppearance = appearance194;
            this.ComboQualifGest.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance195.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ComboQualifGest.DisplayLayout.Override.TemplateAddRowAppearance = appearance195;
            this.ComboQualifGest.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ComboQualifGest.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ComboQualifGest.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ComboQualifGest.Location = new System.Drawing.Point(206, 169);
            this.ComboQualifGest.Name = "ComboQualifGest";
            this.ComboQualifGest.Size = new System.Drawing.Size(186, 25);
            this.ComboQualifGest.TabIndex = 503;
            this.ComboQualifGest.Visible = false;
            // 
            // ssGridGestionnaire
            // 
            appearance196.BackColor = System.Drawing.SystemColors.Window;
            appearance196.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridGestionnaire.DisplayLayout.Appearance = appearance196;
            this.ssGridGestionnaire.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridGestionnaire.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance197.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance197.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance197.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance197.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.Appearance = appearance197;
            appearance198.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.BandLabelAppearance = appearance198;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance199.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance199.BackColor2 = System.Drawing.SystemColors.Control;
            appearance199.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance199.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.PromptAppearance = appearance199;
            this.ssGridGestionnaire.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridGestionnaire.DisplayLayout.MaxRowScrollRegions = 1;
            appearance200.BackColor = System.Drawing.SystemColors.Window;
            appearance200.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridGestionnaire.DisplayLayout.Override.ActiveCellAppearance = appearance200;
            appearance201.BackColor = System.Drawing.SystemColors.Highlight;
            appearance201.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridGestionnaire.DisplayLayout.Override.ActiveRowAppearance = appearance201;
            this.ssGridGestionnaire.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.ssGridGestionnaire.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridGestionnaire.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridGestionnaire.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance202.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.Override.CardAreaAppearance = appearance202;
            appearance203.BorderColor = System.Drawing.Color.Silver;
            appearance203.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridGestionnaire.DisplayLayout.Override.CellAppearance = appearance203;
            this.ssGridGestionnaire.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridGestionnaire.DisplayLayout.Override.CellPadding = 0;
            appearance204.BackColor = System.Drawing.SystemColors.Control;
            appearance204.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance204.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance204.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance204.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.Override.GroupByRowAppearance = appearance204;
            appearance205.TextHAlignAsString = "Left";
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderAppearance = appearance205;
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance206.BackColor = System.Drawing.SystemColors.Window;
            appearance206.BorderColor = System.Drawing.Color.Silver;
            this.ssGridGestionnaire.DisplayLayout.Override.RowAppearance = appearance206;
            this.ssGridGestionnaire.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridGestionnaire.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance207.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridGestionnaire.DisplayLayout.Override.TemplateAddRowAppearance = appearance207;
            this.ssGridGestionnaire.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridGestionnaire.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridGestionnaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ssGridGestionnaire.Location = new System.Drawing.Point(6, 21);
            this.ssGridGestionnaire.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssGridGestionnaire.Name = "ssGridGestionnaire";
            this.ssGridGestionnaire.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.ssGridGestionnaire.Size = new System.Drawing.Size(1822, 697);
            this.ssGridGestionnaire.TabIndex = 0;
            this.ssGridGestionnaire.Text = "ultraGrid1";
            this.ssGridGestionnaire.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridGestionnaire_InitializeLayout);
            this.ssGridGestionnaire.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridGestionnaire_InitializeRow);
            this.ssGridGestionnaire.AfterRowsDeleted += new System.EventHandler(this.ssGridGestionnaire_AfterRowsDeleted);
            this.ssGridGestionnaire.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridGestionnaire_AfterRowUpdate);
            this.ssGridGestionnaire.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.ssGridGestionnaire_BeforeRowUpdate);
            this.ssGridGestionnaire.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssGridGestionnaire_BeforeExitEditMode);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel17);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1844, 796);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.45161F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.54839F));
            this.tableLayoutPanel17.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.groupBox5, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.lblLibModeTrans_TAB, 1, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1844, 796);
            this.tableLayoutPanel17.TabIndex = 417;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.tableLayoutPanel18);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox4.Location = new System.Drawing.Point(2, 203);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox4.Size = new System.Drawing.Size(1442, 590);
            this.groupBox4.TabIndex = 25;
            this.groupBox4.TabStop = false;
            this.groupBox4.Visible = false;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.label12, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.Text2, 0, 1);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1438, 569);
            this.tableLayoutPanel18.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(3, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(144, 16);
            this.label12.TabIndex = 14;
            this.label12.Text = "Informations générales";
            // 
            // Text2
            // 
            this.Text2.AccAcceptNumbersOnly = false;
            this.Text2.AccAllowComma = false;
            this.Text2.AccBackgroundColor = System.Drawing.Color.White;
            this.Text2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text2.AccHidenValue = "";
            this.Text2.AccNotAllowedChars = null;
            this.Text2.AccReadOnly = false;
            this.Text2.AccReadOnlyAllowDelete = false;
            this.Text2.AccRequired = false;
            this.Text2.BackColor = System.Drawing.Color.White;
            this.Text2.CustomBackColor = System.Drawing.Color.White;
            this.Text2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text2.ForeColor = System.Drawing.Color.Black;
            this.Text2.Location = new System.Drawing.Point(2, 22);
            this.Text2.Margin = new System.Windows.Forms.Padding(2);
            this.Text2.MaxLength = 32767;
            this.Text2.Multiline = true;
            this.Text2.Name = "Text2";
            this.Text2.ReadOnly = false;
            this.Text2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text2.Size = new System.Drawing.Size(1434, 545);
            this.Text2.TabIndex = 0;
            this.Text2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text2.UseSystemPasswordChar = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.tableLayoutPanel19);
            this.groupBox5.Controls.Add(this.label16);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Location = new System.Drawing.Point(2, 3);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox5.Size = new System.Drawing.Size(1442, 194);
            this.groupBox5.TabIndex = 23;
            this.groupBox5.TabStop = false;
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel20, 1, 4);
            this.tableLayoutPanel19.Controls.Add(this.txt5, 1, 5);
            this.tableLayoutPanel19.Controls.Add(this.label13, 0, 5);
            this.tableLayoutPanel19.Controls.Add(this.label14, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.chkEditRel_TAB, 0, 3);
            this.tableLayoutPanel19.Controls.Add(this.chkTransmissionBI_TAB, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.chkOSOblig_TAB, 0, 2);
            this.tableLayoutPanel19.Controls.Add(this.label15, 0, 4);
            this.tableLayoutPanel19.Controls.Add(this.cmbModeTrans_TAB, 1, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 6;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1438, 173);
            this.tableLayoutPanel19.TabIndex = 25;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 3;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel20.Controls.Add(this.txtCodeReglement, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.lblLibCodeReglement, 1, 0);
            this.tableLayoutPanel20.Controls.Add(this.CmdRechercheReglement, 2, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(236, 108);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(6, 0, 0, 0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1202, 30);
            this.tableLayoutPanel20.TabIndex = 1;
            // 
            // txtCodeReglement
            // 
            this.txtCodeReglement.AccAcceptNumbersOnly = false;
            this.txtCodeReglement.AccAllowComma = false;
            this.txtCodeReglement.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeReglement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeReglement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeReglement.AccHidenValue = "";
            this.txtCodeReglement.AccNotAllowedChars = null;
            this.txtCodeReglement.AccReadOnly = false;
            this.txtCodeReglement.AccReadOnlyAllowDelete = false;
            this.txtCodeReglement.AccRequired = false;
            this.txtCodeReglement.BackColor = System.Drawing.Color.White;
            this.txtCodeReglement.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeReglement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeReglement.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeReglement.ForeColor = System.Drawing.Color.Black;
            this.txtCodeReglement.Location = new System.Drawing.Point(2, 2);
            this.txtCodeReglement.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeReglement.MaxLength = 32767;
            this.txtCodeReglement.Multiline = false;
            this.txtCodeReglement.Name = "txtCodeReglement";
            this.txtCodeReglement.ReadOnly = false;
            this.txtCodeReglement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeReglement.Size = new System.Drawing.Size(227, 27);
            this.txtCodeReglement.TabIndex = 0;
            this.txtCodeReglement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeReglement.UseSystemPasswordChar = false;
            this.txtCodeReglement.TextChanged += new System.EventHandler(this.txtCodeReglement_TextChanged);
            this.txtCodeReglement.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeReglement_KeyPress);
            this.txtCodeReglement.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeReglement_Validating);
            // 
            // lblLibCodeReglement
            // 
            this.lblLibCodeReglement.AccAcceptNumbersOnly = false;
            this.lblLibCodeReglement.AccAllowComma = false;
            this.lblLibCodeReglement.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibCodeReglement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibCodeReglement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibCodeReglement.AccHidenValue = "";
            this.lblLibCodeReglement.AccNotAllowedChars = null;
            this.lblLibCodeReglement.AccReadOnly = false;
            this.lblLibCodeReglement.AccReadOnlyAllowDelete = false;
            this.lblLibCodeReglement.AccRequired = false;
            this.lblLibCodeReglement.BackColor = System.Drawing.Color.White;
            this.lblLibCodeReglement.CustomBackColor = System.Drawing.Color.White;
            this.lblLibCodeReglement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibCodeReglement.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLibCodeReglement.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(30)))), ((int)(((byte)(136)))), ((int)(((byte)(229)))));
            this.lblLibCodeReglement.Location = new System.Drawing.Point(233, 2);
            this.lblLibCodeReglement.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibCodeReglement.MaxLength = 50;
            this.lblLibCodeReglement.Multiline = false;
            this.lblLibCodeReglement.Name = "lblLibCodeReglement";
            this.lblLibCodeReglement.ReadOnly = false;
            this.lblLibCodeReglement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibCodeReglement.Size = new System.Drawing.Size(920, 27);
            this.lblLibCodeReglement.TabIndex = 1;
            this.lblLibCodeReglement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibCodeReglement.UseSystemPasswordChar = false;
            // 
            // CmdRechercheReglement
            // 
            this.CmdRechercheReglement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercheReglement.FlatAppearance.BorderSize = 0;
            this.CmdRechercheReglement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercheReglement.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.CmdRechercheReglement.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.CmdRechercheReglement.Location = new System.Drawing.Point(1158, 2);
            this.CmdRechercheReglement.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CmdRechercheReglement.Name = "CmdRechercheReglement";
            this.CmdRechercheReglement.Size = new System.Drawing.Size(25, 20);
            this.CmdRechercheReglement.TabIndex = 366;
            this.toolTip1.SetToolTip(this.CmdRechercheReglement, "Recherche de l\'immeuble");
            this.CmdRechercheReglement.UseVisualStyleBackColor = false;
            this.CmdRechercheReglement.Click += new System.EventHandler(this.CmdRechercheReglement_Click);
            // 
            // txt5
            // 
            this.txt5.AccAcceptNumbersOnly = false;
            this.txt5.AccAllowComma = false;
            this.txt5.AccBackgroundColor = System.Drawing.Color.White;
            this.txt5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt5.AccHidenValue = "";
            this.txt5.AccNotAllowedChars = null;
            this.txt5.AccReadOnly = false;
            this.txt5.AccReadOnlyAllowDelete = false;
            this.txt5.AccRequired = false;
            this.txt5.BackColor = System.Drawing.Color.White;
            this.txt5.CustomBackColor = System.Drawing.Color.White;
            this.txt5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt5.ForeColor = System.Drawing.Color.Black;
            this.txt5.Location = new System.Drawing.Point(236, 140);
            this.txt5.Margin = new System.Windows.Forms.Padding(6, 2, 2, 2);
            this.txt5.MaxLength = 50;
            this.txt5.Multiline = false;
            this.txt5.Name = "txt5";
            this.txt5.ReadOnly = false;
            this.txt5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt5.Size = new System.Drawing.Size(1200, 27);
            this.txt5.TabIndex = 1;
            this.txt5.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt5.UseSystemPasswordChar = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label13.Location = new System.Drawing.Point(2, 142);
            this.label13.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(132, 19);
            this.label13.TabIndex = 368;
            this.label13.Text = "Remise applicable";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label14.Location = new System.Drawing.Point(2, 30);
            this.label14.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(160, 19);
            this.label14.TabIndex = 3;
            this.label14.Text = "Mode de transmission";
            // 
            // chkEditRel_TAB
            // 
            this.chkEditRel_TAB.AutoSize = true;
            this.chkEditRel_TAB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEditRel_TAB.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkEditRel_TAB.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkEditRel_TAB.Location = new System.Drawing.Point(2, 85);
            this.chkEditRel_TAB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkEditRel_TAB.Name = "chkEditRel_TAB";
            this.chkEditRel_TAB.Size = new System.Drawing.Size(226, 20);
            this.chkEditRel_TAB.TabIndex = 26;
            this.chkEditRel_TAB.Text = "Edition d\'un relevé de compte ";
            this.chkEditRel_TAB.UseVisualStyleBackColor = true;
            // 
            // chkTransmissionBI_TAB
            // 
            this.chkTransmissionBI_TAB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTransmissionBI_TAB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkTransmissionBI_TAB.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkTransmissionBI_TAB.Location = new System.Drawing.Point(2, 3);
            this.chkTransmissionBI_TAB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkTransmissionBI_TAB.Name = "chkTransmissionBI_TAB";
            this.chkTransmissionBI_TAB.Size = new System.Drawing.Size(226, 20);
            this.chkTransmissionBI_TAB.TabIndex = 24;
            this.chkTransmissionBI_TAB.Text = "Transmission des B.I";
            this.chkTransmissionBI_TAB.UseVisualStyleBackColor = true;
            // 
            // chkOSOblig_TAB
            // 
            this.chkOSOblig_TAB.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkOSOblig_TAB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkOSOblig_TAB.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkOSOblig_TAB.Location = new System.Drawing.Point(2, 59);
            this.chkOSOblig_TAB.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkOSOblig_TAB.Name = "chkOSOblig_TAB";
            this.chkOSOblig_TAB.Size = new System.Drawing.Size(226, 20);
            this.chkOSOblig_TAB.TabIndex = 25;
            this.chkOSOblig_TAB.Text = "O/S obligatoire                              ";
            this.chkOSOblig_TAB.UseVisualStyleBackColor = true;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label15.Location = new System.Drawing.Point(2, 112);
            this.label15.Margin = new System.Windows.Forms.Padding(2, 4, 2, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(147, 19);
            this.label15.TabIndex = 9;
            this.label15.Text = "Mode de règlement";
            // 
            // cmbModeTrans_TAB
            // 
            appearance208.BackColor = System.Drawing.SystemColors.Window;
            appearance208.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbModeTrans_TAB.DisplayLayout.Appearance = appearance208;
            this.cmbModeTrans_TAB.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbModeTrans_TAB.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance209.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance209.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance209.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance209.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbModeTrans_TAB.DisplayLayout.GroupByBox.Appearance = appearance209;
            appearance210.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbModeTrans_TAB.DisplayLayout.GroupByBox.BandLabelAppearance = appearance210;
            this.cmbModeTrans_TAB.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance211.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance211.BackColor2 = System.Drawing.SystemColors.Control;
            appearance211.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance211.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbModeTrans_TAB.DisplayLayout.GroupByBox.PromptAppearance = appearance211;
            this.cmbModeTrans_TAB.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbModeTrans_TAB.DisplayLayout.MaxRowScrollRegions = 1;
            appearance212.BackColor = System.Drawing.SystemColors.Window;
            appearance212.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbModeTrans_TAB.DisplayLayout.Override.ActiveCellAppearance = appearance212;
            appearance213.BackColor = System.Drawing.SystemColors.Highlight;
            appearance213.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbModeTrans_TAB.DisplayLayout.Override.ActiveRowAppearance = appearance213;
            this.cmbModeTrans_TAB.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbModeTrans_TAB.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance214.BackColor = System.Drawing.SystemColors.Window;
            this.cmbModeTrans_TAB.DisplayLayout.Override.CardAreaAppearance = appearance214;
            appearance215.BorderColor = System.Drawing.Color.Silver;
            appearance215.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbModeTrans_TAB.DisplayLayout.Override.CellAppearance = appearance215;
            this.cmbModeTrans_TAB.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbModeTrans_TAB.DisplayLayout.Override.CellPadding = 0;
            appearance216.BackColor = System.Drawing.SystemColors.Control;
            appearance216.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance216.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance216.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance216.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbModeTrans_TAB.DisplayLayout.Override.GroupByRowAppearance = appearance216;
            appearance217.TextHAlignAsString = "Left";
            this.cmbModeTrans_TAB.DisplayLayout.Override.HeaderAppearance = appearance217;
            this.cmbModeTrans_TAB.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbModeTrans_TAB.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance218.BackColor = System.Drawing.SystemColors.Window;
            appearance218.BorderColor = System.Drawing.Color.Silver;
            this.cmbModeTrans_TAB.DisplayLayout.Override.RowAppearance = appearance218;
            this.cmbModeTrans_TAB.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance219.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbModeTrans_TAB.DisplayLayout.Override.TemplateAddRowAppearance = appearance219;
            this.cmbModeTrans_TAB.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbModeTrans_TAB.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbModeTrans_TAB.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbModeTrans_TAB.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbModeTrans_TAB.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbModeTrans_TAB.Location = new System.Drawing.Point(236, 29);
            this.cmbModeTrans_TAB.Margin = new System.Windows.Forms.Padding(6, 3, 2, 3);
            this.cmbModeTrans_TAB.MaxLength = 50;
            this.cmbModeTrans_TAB.Name = "cmbModeTrans_TAB";
            this.cmbModeTrans_TAB.Size = new System.Drawing.Size(1200, 27);
            this.cmbModeTrans_TAB.TabIndex = 0;
            this.cmbModeTrans_TAB.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbModeTrans_TAB_BeforeDropDown);
            this.cmbModeTrans_TAB.Validating += new System.ComponentModel.CancelEventHandler(this.cmbModeTrans_TAB_Validating);
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.label16.Location = new System.Drawing.Point(473, 52);
            this.label16.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(288, 19);
            this.label16.TabIndex = 11;
            // 
            // lblLibModeTrans_TAB
            // 
            this.lblLibModeTrans_TAB.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblLibModeTrans_TAB.Location = new System.Drawing.Point(1456, 50);
            this.lblLibModeTrans_TAB.Margin = new System.Windows.Forms.Padding(10, 50, 2, 0);
            this.lblLibModeTrans_TAB.Name = "lblLibModeTrans_TAB";
            this.lblLibModeTrans_TAB.Size = new System.Drawing.Size(155, 19);
            this.lblLibModeTrans_TAB.TabIndex = 26;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.SSTab2);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1844, 796);
            // 
            // SSTab2
            // 
            this.SSTab2.CloseButtonLocation = Infragistics.Win.UltraWinTabs.TabCloseButtonLocation.None;
            this.SSTab2.Controls.Add(this.ultraTabSharedControlsPage2);
            this.SSTab2.Controls.Add(this.ultraTabPageControl14);
            this.SSTab2.Controls.Add(this.ultraTabPageControl3);
            this.SSTab2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab2.Location = new System.Drawing.Point(0, 0);
            this.SSTab2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.SSTab2.Name = "SSTab2";
            this.SSTab2.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.SSTab2.Size = new System.Drawing.Size(1844, 796);
            this.SSTab2.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Flat;
            this.SSTab2.TabIndex = 1;
            ultraTab6.TabPage = this.ultraTabPageControl14;
            ultraTab6.Text = "Situation du compte";
            ultraTab1.TabPage = this.ultraTabPageControl3;
            ultraTab1.Text = "Relevés de compte";
            this.SSTab2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab6,
            ultraTab1});
            this.SSTab2.TextOrientation = Infragistics.Win.UltraWinTabs.TextOrientation.Horizontal;
            this.SSTab2.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(1842, 774);
            // 
            // ultraTabPageControl10
            // 
            this.ultraTabPageControl10.Controls.Add(this.tableLayoutPanel27);
            this.ultraTabPageControl10.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl10.Name = "ultraTabPageControl10";
            this.ultraTabPageControl10.Size = new System.Drawing.Size(1844, 796);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 1;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Controls.Add(this.tableLayoutPanel31, 0, 2);
            this.tableLayoutPanel27.Controls.Add(this.tableLayoutPanel28, 0, 1);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 4;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(1844, 796);
            this.tableLayoutPanel27.TabIndex = 0;
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 1;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel40, 0, 4);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel48, 0, 3);
            this.tableLayoutPanel31.Controls.Add(this.label30, 0, 1);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel33, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.tableLayoutPanel47, 0, 2);
            this.tableLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel31.Location = new System.Drawing.Point(3, 398);
            this.tableLayoutPanel31.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 5;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(1838, 190);
            this.tableLayoutPanel31.TabIndex = 582;
            // 
            // tableLayoutPanel40
            // 
            this.tableLayoutPanel40.ColumnCount = 10;
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel40.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel40.Controls.Add(this.label36, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.panel6, 0, 0);
            this.tableLayoutPanel40.Controls.Add(this.txtReqEnergie, 9, 0);
            this.tableLayoutPanel40.Controls.Add(this.cmbEnergieA, 5, 0);
            this.tableLayoutPanel40.Controls.Add(this.cmdVisu2, 8, 0);
            this.tableLayoutPanel40.Controls.Add(this.label74, 4, 0);
            this.tableLayoutPanel40.Controls.Add(this.cmbEnergieDe, 2, 0);
            this.tableLayoutPanel40.Controls.Add(this.cmdSelectEnergie, 7, 0);
            this.tableLayoutPanel40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel40.Location = new System.Drawing.Point(0, 152);
            this.tableLayoutPanel40.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel40.Name = "tableLayoutPanel40";
            this.tableLayoutPanel40.RowCount = 1;
            this.tableLayoutPanel40.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel40.Size = new System.Drawing.Size(1838, 38);
            this.tableLayoutPanel40.TabIndex = 582;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label36.Location = new System.Drawing.Point(127, 8);
            this.label36.Margin = new System.Windows.Forms.Padding(7, 8, 2, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(27, 19);
            this.label36.TabIndex = 541;
            this.label36.Text = "du";
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.label75);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(0, 0);
            this.panel6.Margin = new System.Windows.Forms.Padding(0);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(120, 38);
            this.panel6.TabIndex = 418;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label75.Location = new System.Drawing.Point(2, 8);
            this.label75.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(84, 19);
            this.label75.TabIndex = 533;
            this.label75.Text = "Energie de";
            // 
            // txtReqEnergie
            // 
            this.txtReqEnergie.AccAcceptNumbersOnly = false;
            this.txtReqEnergie.AccAllowComma = false;
            this.txtReqEnergie.AccBackgroundColor = System.Drawing.Color.White;
            this.txtReqEnergie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtReqEnergie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtReqEnergie.AccHidenValue = "";
            this.txtReqEnergie.AccNotAllowedChars = null;
            this.txtReqEnergie.AccReadOnly = false;
            this.txtReqEnergie.AccReadOnlyAllowDelete = false;
            this.txtReqEnergie.AccRequired = false;
            this.txtReqEnergie.BackColor = System.Drawing.Color.White;
            this.txtReqEnergie.CustomBackColor = System.Drawing.Color.White;
            this.txtReqEnergie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReqEnergie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtReqEnergie.ForeColor = System.Drawing.Color.Black;
            this.txtReqEnergie.Location = new System.Drawing.Point(1758, 8);
            this.txtReqEnergie.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtReqEnergie.MaxLength = 32767;
            this.txtReqEnergie.Multiline = false;
            this.txtReqEnergie.Name = "txtReqEnergie";
            this.txtReqEnergie.ReadOnly = false;
            this.txtReqEnergie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtReqEnergie.Size = new System.Drawing.Size(78, 27);
            this.txtReqEnergie.TabIndex = 540;
            this.txtReqEnergie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtReqEnergie.UseSystemPasswordChar = false;
            this.txtReqEnergie.Visible = false;
            // 
            // cmbEnergieA
            // 
            this.tableLayoutPanel40.SetColumnSpan(this.cmbEnergieA, 2);
            appearance220.BackColor = System.Drawing.SystemColors.Window;
            appearance220.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbEnergieA.DisplayLayout.Appearance = appearance220;
            this.cmbEnergieA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance221.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance221.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance221.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance221.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergieA.DisplayLayout.GroupByBox.Appearance = appearance221;
            appearance222.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergieA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance222;
            this.cmbEnergieA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance223.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance223.BackColor2 = System.Drawing.SystemColors.Control;
            appearance223.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance223.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergieA.DisplayLayout.GroupByBox.PromptAppearance = appearance223;
            appearance224.BackColor = System.Drawing.SystemColors.Window;
            appearance224.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbEnergieA.DisplayLayout.Override.ActiveCellAppearance = appearance224;
            this.cmbEnergieA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbEnergieA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance225.BorderColor = System.Drawing.Color.Silver;
            appearance225.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbEnergieA.DisplayLayout.Override.CellAppearance = appearance225;
            this.cmbEnergieA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbEnergieA.DisplayLayout.Override.CellPadding = 0;
            appearance226.BackColor = System.Drawing.SystemColors.Control;
            appearance226.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance226.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance226.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance226.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergieA.DisplayLayout.Override.GroupByRowAppearance = appearance226;
            appearance227.TextHAlignAsString = "Left";
            this.cmbEnergieA.DisplayLayout.Override.HeaderAppearance = appearance227;
            this.cmbEnergieA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbEnergieA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.cmbEnergieA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance228.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbEnergieA.DisplayLayout.Override.TemplateAddRowAppearance = appearance228;
            this.cmbEnergieA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbEnergieA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbEnergieA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbEnergieA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEnergieA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbEnergieA.Location = new System.Drawing.Point(831, 3);
            this.cmbEnergieA.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbEnergieA.Name = "cmbEnergieA";
            this.cmbEnergieA.Size = new System.Drawing.Size(637, 27);
            this.cmbEnergieA.TabIndex = 533;
            this.cmbEnergieA.Tag = "5";
            this.cmbEnergieA.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbEnergieA_BeforeDropDown);
            this.cmbEnergieA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEnergieA_KeyPress);
            this.cmbEnergieA.Validating += new System.ComponentModel.CancelEventHandler(this.cmbEnergieA_Validating);
            // 
            // cmdVisu2
            // 
            this.cmdVisu2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisu2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu2.FlatAppearance.BorderSize = 0;
            this.cmdVisu2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu2.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdVisu2.Location = new System.Drawing.Point(1692, 2);
            this.cmdVisu2.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu2.Name = "cmdVisu2";
            this.cmdVisu2.Size = new System.Drawing.Size(61, 34);
            this.cmdVisu2.TabIndex = 539;
            this.cmdVisu2.Tag = "2";
            this.cmdVisu2.UseVisualStyleBackColor = false;
            this.cmdVisu2.Visible = false;
            this.cmdVisu2.Click += new System.EventHandler(this.cmdVisu0_Click);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label74.Location = new System.Drawing.Point(811, 0);
            this.label74.Margin = new System.Windows.Forms.Padding(8, 0, 8, 0);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(10, 19);
            this.label74.TabIndex = 530;
            this.label74.Text = "à";
            // 
            // cmbEnergieDe
            // 
            this.tableLayoutPanel40.SetColumnSpan(this.cmbEnergieDe, 2);
            appearance229.BackColor = System.Drawing.SystemColors.Window;
            appearance229.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbEnergieDe.DisplayLayout.Appearance = appearance229;
            this.cmbEnergieDe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance230.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance230.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance230.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance230.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergieDe.DisplayLayout.GroupByBox.Appearance = appearance230;
            appearance231.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergieDe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance231;
            this.cmbEnergieDe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance232.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance232.BackColor2 = System.Drawing.SystemColors.Control;
            appearance232.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance232.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergieDe.DisplayLayout.GroupByBox.PromptAppearance = appearance232;
            appearance233.BackColor = System.Drawing.SystemColors.Window;
            appearance233.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbEnergieDe.DisplayLayout.Override.ActiveCellAppearance = appearance233;
            this.cmbEnergieDe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbEnergieDe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance234.BorderColor = System.Drawing.Color.Silver;
            appearance234.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbEnergieDe.DisplayLayout.Override.CellAppearance = appearance234;
            this.cmbEnergieDe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbEnergieDe.DisplayLayout.Override.CellPadding = 0;
            appearance235.BackColor = System.Drawing.SystemColors.Control;
            appearance235.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance235.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance235.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance235.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergieDe.DisplayLayout.Override.GroupByRowAppearance = appearance235;
            appearance236.TextHAlignAsString = "Left";
            this.cmbEnergieDe.DisplayLayout.Override.HeaderAppearance = appearance236;
            this.cmbEnergieDe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbEnergieDe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            this.cmbEnergieDe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance237.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbEnergieDe.DisplayLayout.Override.TemplateAddRowAppearance = appearance237;
            this.cmbEnergieDe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbEnergieDe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbEnergieDe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbEnergieDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEnergieDe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbEnergieDe.Location = new System.Drawing.Point(162, 3);
            this.cmbEnergieDe.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbEnergieDe.Name = "cmbEnergieDe";
            this.cmbEnergieDe.Size = new System.Drawing.Size(639, 27);
            this.cmbEnergieDe.TabIndex = 532;
            this.cmbEnergieDe.Tag = "1";
            this.cmbEnergieDe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbEnergieDe_BeforeDropDown);
            this.cmbEnergieDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEnergieDe_KeyPress);
            this.cmbEnergieDe.Validating += new System.ComponentModel.CancelEventHandler(this.cmbEnergieDe_Validating);
            // 
            // cmdSelectEnergie
            // 
            this.cmdSelectEnergie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSelectEnergie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSelectEnergie.FlatAppearance.BorderSize = 0;
            this.cmdSelectEnergie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelectEnergie.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F);
            this.cmdSelectEnergie.ForeColor = System.Drawing.Color.White;
            this.cmdSelectEnergie.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSelectEnergie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSelectEnergie.Location = new System.Drawing.Point(1472, 2);
            this.cmdSelectEnergie.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSelectEnergie.Name = "cmdSelectEnergie";
            this.cmdSelectEnergie.Size = new System.Drawing.Size(216, 34);
            this.cmdSelectEnergie.TabIndex = 538;
            this.cmdSelectEnergie.Text = "     &Sélectionner les énergies";
            this.toolTip1.SetToolTip(this.cmdSelectEnergie, "ouvre une fenêtre afin d\'appliquer les formules  du révision aux chantiers séléct" +
        "ionnés de ce client");
            this.cmdSelectEnergie.UseVisualStyleBackColor = false;
            this.cmdSelectEnergie.Click += new System.EventHandler(this.cmdSelectEnergie_Click);
            // 
            // tableLayoutPanel48
            // 
            this.tableLayoutPanel48.ColumnCount = 10;
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00001F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99999F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel48.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 190F));
            this.tableLayoutPanel48.Controls.Add(this.panel5, 0, 0);
            this.tableLayoutPanel48.Controls.Add(this.cmdComCommercial1, 6, 0);
            this.tableLayoutPanel48.Controls.Add(this.txtComA, 5, 0);
            this.tableLayoutPanel48.Controls.Add(this.txtReqComCommercial, 9, 0);
            this.tableLayoutPanel48.Controls.Add(this.cmdComCommercial0, 3, 0);
            this.tableLayoutPanel48.Controls.Add(this.label72, 4, 0);
            this.tableLayoutPanel48.Controls.Add(this.txtComDe, 2, 0);
            this.tableLayoutPanel48.Controls.Add(this.label73, 1, 0);
            this.tableLayoutPanel48.Controls.Add(this.cmdSelectCommerciaux, 7, 0);
            this.tableLayoutPanel48.Controls.Add(this.cmdVisu1, 8, 0);
            this.tableLayoutPanel48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel48.Location = new System.Drawing.Point(0, 114);
            this.tableLayoutPanel48.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel48.Name = "tableLayoutPanel48";
            this.tableLayoutPanel48.RowCount = 1;
            this.tableLayoutPanel48.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel48.Size = new System.Drawing.Size(1838, 38);
            this.tableLayoutPanel48.TabIndex = 584;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.optSelectCom1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(0, 0);
            this.panel5.Margin = new System.Windows.Forms.Padding(0);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(120, 38);
            this.panel5.TabIndex = 418;
            // 
            // optSelectCom1
            // 
            this.optSelectCom1.AutoSize = true;
            this.optSelectCom1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optSelectCom1.Location = new System.Drawing.Point(2, 9);
            this.optSelectCom1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.optSelectCom1.Name = "optSelectCom1";
            this.optSelectCom1.Size = new System.Drawing.Size(110, 23);
            this.optSelectCom1.TabIndex = 521;
            this.optSelectCom1.Tag = "1";
            this.optSelectCom1.Text = "Commercial";
            this.optSelectCom1.UseVisualStyleBackColor = true;
            this.optSelectCom1.CheckedChanged += new System.EventHandler(this.optSelectCom0_CheckedChanged);
            // 
            // cmdComCommercial1
            // 
            this.cmdComCommercial1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdComCommercial1.Enabled = false;
            this.cmdComCommercial1.FlatAppearance.BorderSize = 0;
            this.cmdComCommercial1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdComCommercial1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdComCommercial1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdComCommercial1.Location = new System.Drawing.Point(1332, 8);
            this.cmdComCommercial1.Margin = new System.Windows.Forms.Padding(3, 8, 0, 0);
            this.cmdComCommercial1.Name = "cmdComCommercial1";
            this.cmdComCommercial1.Size = new System.Drawing.Size(24, 20);
            this.cmdComCommercial1.TabIndex = 527;
            this.cmdComCommercial1.Tag = "1";
            this.toolTip1.SetToolTip(this.cmdComCommercial1, "Recherche du gérant par son code");
            this.cmdComCommercial1.UseVisualStyleBackColor = false;
            this.cmdComCommercial1.Click += new System.EventHandler(this.cmdComCommercial0_Click);
            // 
            // txtComA
            // 
            this.txtComA.AccAcceptNumbersOnly = false;
            this.txtComA.AccAllowComma = false;
            this.txtComA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtComA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtComA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtComA.AccHidenValue = "";
            this.txtComA.AccNotAllowedChars = null;
            this.txtComA.AccReadOnly = true;
            this.txtComA.AccReadOnlyAllowDelete = false;
            this.txtComA.AccRequired = false;
            this.txtComA.BackColor = System.Drawing.Color.White;
            this.txtComA.CustomBackColor = System.Drawing.Color.White;
            this.txtComA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtComA.ForeColor = System.Drawing.Color.Black;
            this.txtComA.Location = new System.Drawing.Point(775, 8);
            this.txtComA.Margin = new System.Windows.Forms.Padding(0, 8, 0, 0);
            this.txtComA.MaxLength = 32767;
            this.txtComA.Multiline = false;
            this.txtComA.Name = "txtComA";
            this.txtComA.ReadOnly = false;
            this.txtComA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtComA.Size = new System.Drawing.Size(554, 27);
            this.txtComA.TabIndex = 501;
            this.txtComA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtComA.UseSystemPasswordChar = false;
            // 
            // txtReqComCommercial
            // 
            this.txtReqComCommercial.AccAcceptNumbersOnly = false;
            this.txtReqComCommercial.AccAllowComma = false;
            this.txtReqComCommercial.AccBackgroundColor = System.Drawing.Color.White;
            this.txtReqComCommercial.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtReqComCommercial.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtReqComCommercial.AccHidenValue = "";
            this.txtReqComCommercial.AccNotAllowedChars = null;
            this.txtReqComCommercial.AccReadOnly = false;
            this.txtReqComCommercial.AccReadOnlyAllowDelete = false;
            this.txtReqComCommercial.AccRequired = false;
            this.txtReqComCommercial.BackColor = System.Drawing.Color.White;
            this.txtReqComCommercial.CustomBackColor = System.Drawing.Color.White;
            this.txtReqComCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReqComCommercial.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtReqComCommercial.ForeColor = System.Drawing.Color.Black;
            this.txtReqComCommercial.Location = new System.Drawing.Point(1649, 8);
            this.txtReqComCommercial.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtReqComCommercial.MaxLength = 32767;
            this.txtReqComCommercial.Multiline = false;
            this.txtReqComCommercial.Name = "txtReqComCommercial";
            this.txtReqComCommercial.ReadOnly = false;
            this.txtReqComCommercial.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtReqComCommercial.Size = new System.Drawing.Size(187, 27);
            this.txtReqComCommercial.TabIndex = 530;
            this.txtReqComCommercial.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtReqComCommercial.UseSystemPasswordChar = false;
            this.txtReqComCommercial.Visible = false;
            // 
            // cmdComCommercial0
            // 
            this.cmdComCommercial0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdComCommercial0.Enabled = false;
            this.cmdComCommercial0.FlatAppearance.BorderSize = 0;
            this.cmdComCommercial0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdComCommercial0.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdComCommercial0.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdComCommercial0.Location = new System.Drawing.Point(718, 8);
            this.cmdComCommercial0.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.cmdComCommercial0.Name = "cmdComCommercial0";
            this.cmdComCommercial0.Size = new System.Drawing.Size(24, 20);
            this.cmdComCommercial0.TabIndex = 526;
            this.cmdComCommercial0.Tag = "0";
            this.toolTip1.SetToolTip(this.cmdComCommercial0, "Recherche du gérant par son code");
            this.cmdComCommercial0.UseVisualStyleBackColor = false;
            this.cmdComCommercial0.Click += new System.EventHandler(this.cmdComCommercial0_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label72.Location = new System.Drawing.Point(757, 8);
            this.label72.Margin = new System.Windows.Forms.Padding(8, 8, 2, 0);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(16, 19);
            this.label72.TabIndex = 524;
            this.label72.Text = "à";
            // 
            // txtComDe
            // 
            this.txtComDe.AccAcceptNumbersOnly = false;
            this.txtComDe.AccAllowComma = false;
            this.txtComDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtComDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtComDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtComDe.AccHidenValue = "";
            this.txtComDe.AccNotAllowedChars = null;
            this.txtComDe.AccReadOnly = true;
            this.txtComDe.AccReadOnlyAllowDelete = false;
            this.txtComDe.AccRequired = false;
            this.txtComDe.BackColor = System.Drawing.Color.White;
            this.txtComDe.CustomBackColor = System.Drawing.Color.White;
            this.txtComDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtComDe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtComDe.ForeColor = System.Drawing.Color.Black;
            this.txtComDe.Location = new System.Drawing.Point(162, 8);
            this.txtComDe.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtComDe.MaxLength = 32767;
            this.txtComDe.Multiline = false;
            this.txtComDe.Name = "txtComDe";
            this.txtComDe.ReadOnly = false;
            this.txtComDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtComDe.Size = new System.Drawing.Size(551, 27);
            this.txtComDe.TabIndex = 500;
            this.txtComDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtComDe.UseSystemPasswordChar = false;
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label73.Location = new System.Drawing.Point(127, 8);
            this.label73.Margin = new System.Windows.Forms.Padding(7, 8, 2, 0);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(27, 19);
            this.label73.TabIndex = 523;
            this.label73.Text = "du";
            // 
            // cmdSelectCommerciaux
            // 
            this.cmdSelectCommerciaux.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSelectCommerciaux.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSelectCommerciaux.Enabled = false;
            this.cmdSelectCommerciaux.FlatAppearance.BorderSize = 0;
            this.cmdSelectCommerciaux.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelectCommerciaux.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F);
            this.cmdSelectCommerciaux.ForeColor = System.Drawing.Color.White;
            this.cmdSelectCommerciaux.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSelectCommerciaux.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSelectCommerciaux.Location = new System.Drawing.Point(1363, 2);
            this.cmdSelectCommerciaux.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSelectCommerciaux.Name = "cmdSelectCommerciaux";
            this.cmdSelectCommerciaux.Size = new System.Drawing.Size(216, 34);
            this.cmdSelectCommerciaux.TabIndex = 528;
            this.cmdSelectCommerciaux.Text = "     &Sélectionner les commerciaux";
            this.toolTip1.SetToolTip(this.cmdSelectCommerciaux, "ouvre une fenêtre afin d\'appliquer les formules  du révision aux chantiers séléct" +
        "ionnés de ce client");
            this.cmdSelectCommerciaux.UseVisualStyleBackColor = false;
            this.cmdSelectCommerciaux.Click += new System.EventHandler(this.cmdSelectCommerciaux_Click);
            // 
            // cmdVisu1
            // 
            this.cmdVisu1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisu1.Enabled = false;
            this.cmdVisu1.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu1.FlatAppearance.BorderSize = 0;
            this.cmdVisu1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu1.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdVisu1.Location = new System.Drawing.Point(1583, 2);
            this.cmdVisu1.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu1.Name = "cmdVisu1";
            this.cmdVisu1.Size = new System.Drawing.Size(61, 34);
            this.cmdVisu1.TabIndex = 529;
            this.cmdVisu1.Tag = "1";
            this.cmdVisu1.UseVisualStyleBackColor = false;
            this.cmdVisu1.Visible = false;
            this.cmdVisu1.Click += new System.EventHandler(this.cmdVisu0_Click);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label30.Location = new System.Drawing.Point(2, 38);
            this.label30.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(145, 19);
            this.label30.TabIndex = 554;
            this.label30.Text = "Selection d\'édition :";
            // 
            // tableLayoutPanel33
            // 
            this.tableLayoutPanel33.ColumnCount = 2;
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel33.Controls.Add(this.cmdComPrint, 1, 0);
            this.tableLayoutPanel33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel33.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel33.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel33.Name = "tableLayoutPanel33";
            this.tableLayoutPanel33.RowCount = 1;
            this.tableLayoutPanel33.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel33.Size = new System.Drawing.Size(1838, 38);
            this.tableLayoutPanel33.TabIndex = 582;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label21.Location = new System.Drawing.Point(2, 0);
            this.label21.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(315, 19);
            this.label21.TabIndex = 553;
            this.label21.Text = "Sélection des gérants et de leurs immeubles";
            // 
            // cmdComPrint
            // 
            this.cmdComPrint.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdComPrint.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdComPrint.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdComPrint.FlatAppearance.BorderSize = 0;
            this.cmdComPrint.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdComPrint.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdComPrint.Location = new System.Drawing.Point(1776, 1);
            this.cmdComPrint.Margin = new System.Windows.Forms.Padding(2, 1, 2, 2);
            this.cmdComPrint.Name = "cmdComPrint";
            this.cmdComPrint.Size = new System.Drawing.Size(60, 35);
            this.cmdComPrint.TabIndex = 510;
            this.toolTip1.SetToolTip(this.cmdComPrint, "Imprime les immeubles,les gérants et les contrats du client en cours");
            this.cmdComPrint.UseVisualStyleBackColor = false;
            this.cmdComPrint.Click += new System.EventHandler(this.cmdComPrint_Click);
            // 
            // tableLayoutPanel47
            // 
            this.tableLayoutPanel47.ColumnCount = 10;
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 66F));
            this.tableLayoutPanel47.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 82F));
            this.tableLayoutPanel47.Controls.Add(this.panel4, 0, 0);
            this.tableLayoutPanel47.Controls.Add(this.cmdComGerant1, 6, 0);
            this.tableLayoutPanel47.Controls.Add(this.txtGerantA, 5, 0);
            this.tableLayoutPanel47.Controls.Add(this.label70, 4, 0);
            this.tableLayoutPanel47.Controls.Add(this.cmdComGerant0, 3, 0);
            this.tableLayoutPanel47.Controls.Add(this.txtReqComGerant, 9, 0);
            this.tableLayoutPanel47.Controls.Add(this.txtGerantDe, 2, 0);
            this.tableLayoutPanel47.Controls.Add(this.label71, 1, 0);
            this.tableLayoutPanel47.Controls.Add(this.cmdSelectGerant, 7, 0);
            this.tableLayoutPanel47.Controls.Add(this.cmdVisu0, 8, 0);
            this.tableLayoutPanel47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel47.Location = new System.Drawing.Point(0, 76);
            this.tableLayoutPanel47.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel47.Name = "tableLayoutPanel47";
            this.tableLayoutPanel47.RowCount = 1;
            this.tableLayoutPanel47.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel47.Size = new System.Drawing.Size(1838, 38);
            this.tableLayoutPanel47.TabIndex = 583;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.optSelectCom0);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(120, 38);
            this.panel4.TabIndex = 418;
            // 
            // optSelectCom0
            // 
            this.optSelectCom0.AutoSize = true;
            this.optSelectCom0.Checked = true;
            this.optSelectCom0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optSelectCom0.Location = new System.Drawing.Point(3, 8);
            this.optSelectCom0.Margin = new System.Windows.Forms.Padding(2, 6, 2, 3);
            this.optSelectCom0.Name = "optSelectCom0";
            this.optSelectCom0.Size = new System.Drawing.Size(75, 23);
            this.optSelectCom0.TabIndex = 511;
            this.optSelectCom0.TabStop = true;
            this.optSelectCom0.Tag = "0";
            this.optSelectCom0.Text = "Gérant";
            this.optSelectCom0.UseVisualStyleBackColor = true;
            this.optSelectCom0.CheckedChanged += new System.EventHandler(this.optSelectCom0_CheckedChanged);
            // 
            // cmdComGerant1
            // 
            this.cmdComGerant1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdComGerant1.FlatAppearance.BorderSize = 0;
            this.cmdComGerant1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdComGerant1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdComGerant1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdComGerant1.Location = new System.Drawing.Point(1441, 8);
            this.cmdComGerant1.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.cmdComGerant1.Name = "cmdComGerant1";
            this.cmdComGerant1.Size = new System.Drawing.Size(24, 20);
            this.cmdComGerant1.TabIndex = 517;
            this.cmdComGerant1.Tag = "1";
            this.toolTip1.SetToolTip(this.cmdComGerant1, "Recherche du gérant par son code");
            this.cmdComGerant1.UseVisualStyleBackColor = false;
            this.cmdComGerant1.Click += new System.EventHandler(this.cmdComGerant0_Click);
            // 
            // txtGerantA
            // 
            this.txtGerantA.AccAcceptNumbersOnly = false;
            this.txtGerantA.AccAllowComma = false;
            this.txtGerantA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerantA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerantA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerantA.AccHidenValue = "";
            this.txtGerantA.AccNotAllowedChars = null;
            this.txtGerantA.AccReadOnly = false;
            this.txtGerantA.AccReadOnlyAllowDelete = false;
            this.txtGerantA.AccRequired = false;
            this.txtGerantA.BackColor = System.Drawing.Color.White;
            this.txtGerantA.CustomBackColor = System.Drawing.Color.White;
            this.txtGerantA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerantA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGerantA.ForeColor = System.Drawing.Color.Black;
            this.txtGerantA.Location = new System.Drawing.Point(831, 8);
            this.txtGerantA.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtGerantA.MaxLength = 32767;
            this.txtGerantA.Multiline = false;
            this.txtGerantA.Name = "txtGerantA";
            this.txtGerantA.ReadOnly = false;
            this.txtGerantA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerantA.Size = new System.Drawing.Size(605, 27);
            this.txtGerantA.TabIndex = 543;
            this.txtGerantA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerantA.UseSystemPasswordChar = false;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label70.Location = new System.Drawing.Point(811, 8);
            this.label70.Margin = new System.Windows.Forms.Padding(8, 8, 2, 0);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(16, 19);
            this.label70.TabIndex = 514;
            this.label70.Text = "à";
            // 
            // cmdComGerant0
            // 
            this.cmdComGerant0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdComGerant0.FlatAppearance.BorderSize = 0;
            this.cmdComGerant0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdComGerant0.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdComGerant0.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdComGerant0.Location = new System.Drawing.Point(772, 8);
            this.cmdComGerant0.Margin = new System.Windows.Forms.Padding(3, 8, 3, 3);
            this.cmdComGerant0.Name = "cmdComGerant0";
            this.cmdComGerant0.Size = new System.Drawing.Size(25, 20);
            this.cmdComGerant0.TabIndex = 516;
            this.cmdComGerant0.Tag = "0";
            this.toolTip1.SetToolTip(this.cmdComGerant0, "Recherche du gérant par son code");
            this.cmdComGerant0.UseVisualStyleBackColor = false;
            this.cmdComGerant0.Click += new System.EventHandler(this.cmdComGerant0_Click);
            // 
            // txtReqComGerant
            // 
            this.txtReqComGerant.AccAcceptNumbersOnly = false;
            this.txtReqComGerant.AccAllowComma = false;
            this.txtReqComGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtReqComGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtReqComGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtReqComGerant.AccHidenValue = "";
            this.txtReqComGerant.AccNotAllowedChars = null;
            this.txtReqComGerant.AccReadOnly = false;
            this.txtReqComGerant.AccReadOnlyAllowDelete = false;
            this.txtReqComGerant.AccRequired = false;
            this.txtReqComGerant.BackColor = System.Drawing.Color.White;
            this.txtReqComGerant.CustomBackColor = System.Drawing.Color.White;
            this.txtReqComGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtReqComGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtReqComGerant.ForeColor = System.Drawing.Color.Black;
            this.txtReqComGerant.Location = new System.Drawing.Point(1758, 8);
            this.txtReqComGerant.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtReqComGerant.MaxLength = 32767;
            this.txtReqComGerant.Multiline = false;
            this.txtReqComGerant.Name = "txtReqComGerant";
            this.txtReqComGerant.ReadOnly = false;
            this.txtReqComGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtReqComGerant.Size = new System.Drawing.Size(78, 27);
            this.txtReqComGerant.TabIndex = 520;
            this.txtReqComGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtReqComGerant.UseSystemPasswordChar = false;
            this.txtReqComGerant.Visible = false;
            // 
            // txtGerantDe
            // 
            this.txtGerantDe.AccAcceptNumbersOnly = false;
            this.txtGerantDe.AccAllowComma = false;
            this.txtGerantDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerantDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerantDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerantDe.AccHidenValue = "";
            this.txtGerantDe.AccNotAllowedChars = null;
            this.txtGerantDe.AccReadOnly = false;
            this.txtGerantDe.AccReadOnlyAllowDelete = false;
            this.txtGerantDe.AccRequired = false;
            this.txtGerantDe.BackColor = System.Drawing.Color.White;
            this.txtGerantDe.CustomBackColor = System.Drawing.Color.White;
            this.txtGerantDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerantDe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGerantDe.ForeColor = System.Drawing.Color.Black;
            this.txtGerantDe.Location = new System.Drawing.Point(162, 8);
            this.txtGerantDe.Margin = new System.Windows.Forms.Padding(2, 8, 2, 2);
            this.txtGerantDe.MaxLength = 32767;
            this.txtGerantDe.Multiline = false;
            this.txtGerantDe.Name = "txtGerantDe";
            this.txtGerantDe.ReadOnly = false;
            this.txtGerantDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerantDe.Size = new System.Drawing.Size(605, 27);
            this.txtGerantDe.TabIndex = 541;
            this.txtGerantDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerantDe.UseSystemPasswordChar = false;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label71.Location = new System.Drawing.Point(127, 8);
            this.label71.Margin = new System.Windows.Forms.Padding(7, 8, 2, 0);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(27, 19);
            this.label71.TabIndex = 513;
            this.label71.Text = "du";
            // 
            // cmdSelectGerant
            // 
            this.cmdSelectGerant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSelectGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSelectGerant.FlatAppearance.BorderSize = 0;
            this.cmdSelectGerant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelectGerant.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F);
            this.cmdSelectGerant.ForeColor = System.Drawing.Color.White;
            this.cmdSelectGerant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSelectGerant.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSelectGerant.Location = new System.Drawing.Point(1472, 2);
            this.cmdSelectGerant.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSelectGerant.Name = "cmdSelectGerant";
            this.cmdSelectGerant.Size = new System.Drawing.Size(216, 34);
            this.cmdSelectGerant.TabIndex = 518;
            this.cmdSelectGerant.Text = "     &Sélectionner les gérants";
            this.toolTip1.SetToolTip(this.cmdSelectGerant, "ouvre une fenêtre afin d\'appliquer les formules  du révision aux chantiers séléct" +
        "ionnés de ce client");
            this.cmdSelectGerant.UseVisualStyleBackColor = false;
            this.cmdSelectGerant.Click += new System.EventHandler(this.cmdSelectGerant_Click);
            // 
            // cmdVisu0
            // 
            this.cmdVisu0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisu0.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu0.FlatAppearance.BorderSize = 0;
            this.cmdVisu0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu0.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdVisu0.Location = new System.Drawing.Point(1692, 2);
            this.cmdVisu0.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu0.Name = "cmdVisu0";
            this.cmdVisu0.Size = new System.Drawing.Size(61, 34);
            this.cmdVisu0.TabIndex = 519;
            this.cmdVisu0.Tag = "0";
            this.cmdVisu0.UseVisualStyleBackColor = false;
            this.cmdVisu0.Visible = false;
            this.cmdVisu0.Click += new System.EventHandler(this.cmdVisu0_Click);
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.tableLayoutPanel32, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.frmFactures, 1, 0);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(3, 208);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(1838, 190);
            this.tableLayoutPanel28.TabIndex = 582;
            // 
            // tableLayoutPanel32
            // 
            this.tableLayoutPanel32.ColumnCount = 4;
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel32.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel32.Controls.Add(this.lblTypologie, 3, 2);
            this.tableLayoutPanel32.Controls.Add(this.cmbTypologie, 2, 2);
            this.tableLayoutPanel32.Controls.Add(this.cmdTypologie, 1, 2);
            this.tableLayoutPanel32.Controls.Add(this.chkListeRouge, 2, 0);
            this.tableLayoutPanel32.Controls.Add(this.chkPrioritaire, 0, 0);
            this.tableLayoutPanel32.Controls.Add(this.label67, 0, 2);
            this.tableLayoutPanel32.Controls.Add(this.label66, 0, 1);
            this.tableLayoutPanel32.Controls.Add(this.cmbCoteAmour, 2, 1);
            this.tableLayoutPanel32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel32.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel32.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel32.Name = "tableLayoutPanel32";
            this.tableLayoutPanel32.RowCount = 3;
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel32.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel32.Size = new System.Drawing.Size(919, 190);
            this.tableLayoutPanel32.TabIndex = 552;
            // 
            // lblTypologie
            // 
            this.lblTypologie.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblTypologie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblTypologie.Location = new System.Drawing.Point(553, 65);
            this.lblTypologie.Margin = new System.Windows.Forms.Padding(2, 5, 2, 0);
            this.lblTypologie.Name = "lblTypologie";
            this.lblTypologie.Size = new System.Drawing.Size(364, 19);
            this.lblTypologie.TabIndex = 541;
            // 
            // cmbTypologie
            // 
            appearance238.BackColor = System.Drawing.SystemColors.Window;
            appearance238.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbTypologie.DisplayLayout.Appearance = appearance238;
            this.cmbTypologie.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbTypologie.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance239.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance239.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance239.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance239.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbTypologie.DisplayLayout.GroupByBox.Appearance = appearance239;
            appearance240.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbTypologie.DisplayLayout.GroupByBox.BandLabelAppearance = appearance240;
            this.cmbTypologie.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance241.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance241.BackColor2 = System.Drawing.SystemColors.Control;
            appearance241.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance241.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbTypologie.DisplayLayout.GroupByBox.PromptAppearance = appearance241;
            this.cmbTypologie.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbTypologie.DisplayLayout.MaxRowScrollRegions = 1;
            appearance242.BackColor = System.Drawing.SystemColors.Window;
            appearance242.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbTypologie.DisplayLayout.Override.ActiveCellAppearance = appearance242;
            appearance243.BackColor = System.Drawing.SystemColors.Highlight;
            appearance243.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbTypologie.DisplayLayout.Override.ActiveRowAppearance = appearance243;
            this.cmbTypologie.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbTypologie.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance244.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTypologie.DisplayLayout.Override.CardAreaAppearance = appearance244;
            appearance245.BorderColor = System.Drawing.Color.Silver;
            appearance245.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbTypologie.DisplayLayout.Override.CellAppearance = appearance245;
            this.cmbTypologie.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbTypologie.DisplayLayout.Override.CellPadding = 0;
            appearance246.BackColor = System.Drawing.SystemColors.Control;
            appearance246.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance246.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance246.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance246.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbTypologie.DisplayLayout.Override.GroupByRowAppearance = appearance246;
            appearance247.TextHAlignAsString = "Left";
            this.cmbTypologie.DisplayLayout.Override.HeaderAppearance = appearance247;
            this.cmbTypologie.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbTypologie.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance248.BackColor = System.Drawing.SystemColors.Window;
            appearance248.BorderColor = System.Drawing.Color.Silver;
            this.cmbTypologie.DisplayLayout.Override.RowAppearance = appearance248;
            this.cmbTypologie.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance249.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbTypologie.DisplayLayout.Override.TemplateAddRowAppearance = appearance249;
            this.cmbTypologie.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbTypologie.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbTypologie.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbTypologie.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmbTypologie.Location = new System.Drawing.Point(186, 63);
            this.cmbTypologie.Name = "cmbTypologie";
            this.cmbTypologie.Size = new System.Drawing.Size(362, 25);
            this.cmbTypologie.TabIndex = 504;
            this.cmbTypologie.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbTypologie_BeforeDropDown);
            this.cmbTypologie.TextChanged += new System.EventHandler(this.cmbTypologie_TextChanged);
            this.cmbTypologie.Click += new System.EventHandler(this.cmbTypologie_Click);
            // 
            // cmdTypologie
            // 
            this.cmdTypologie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdTypologie.FlatAppearance.BorderSize = 0;
            this.cmdTypologie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTypologie.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdTypologie.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdTypologie.Location = new System.Drawing.Point(153, 63);
            this.cmdTypologie.Name = "cmdTypologie";
            this.cmdTypologie.Size = new System.Drawing.Size(25, 20);
            this.cmdTypologie.TabIndex = 507;
            this.cmdTypologie.UseVisualStyleBackColor = false;
            this.cmdTypologie.Click += new System.EventHandler(this.cmdTypologie_Click);
            // 
            // chkListeRouge
            // 
            this.chkListeRouge.AutoSize = true;
            this.chkListeRouge.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkListeRouge.Location = new System.Drawing.Point(185, 3);
            this.chkListeRouge.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkListeRouge.Name = "chkListeRouge";
            this.chkListeRouge.Size = new System.Drawing.Size(106, 23);
            this.chkListeRouge.TabIndex = 413;
            this.chkListeRouge.Text = "Liste rouge";
            this.chkListeRouge.UseVisualStyleBackColor = true;
            // 
            // chkPrioritaire
            // 
            this.chkPrioritaire.AutoSize = true;
            this.chkPrioritaire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkPrioritaire.Location = new System.Drawing.Point(2, 3);
            this.chkPrioritaire.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.chkPrioritaire.Name = "chkPrioritaire";
            this.chkPrioritaire.Size = new System.Drawing.Size(146, 23);
            this.chkPrioritaire.TabIndex = 412;
            this.chkPrioritaire.Text = "Gérant prioritaire";
            this.chkPrioritaire.UseVisualStyleBackColor = true;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label67.Location = new System.Drawing.Point(2, 60);
            this.label67.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(77, 19);
            this.label67.TabIndex = 505;
            this.label67.Text = "Typologie";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label66.Location = new System.Drawing.Point(2, 30);
            this.label66.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(104, 19);
            this.label66.TabIndex = 504;
            this.label66.Text = "Côte d\'amour";
            // 
            // cmbCoteAmour
            // 
            appearance250.BackColor = System.Drawing.SystemColors.Window;
            appearance250.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCoteAmour.DisplayLayout.Appearance = appearance250;
            this.cmbCoteAmour.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCoteAmour.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance251.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance251.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance251.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance251.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCoteAmour.DisplayLayout.GroupByBox.Appearance = appearance251;
            appearance252.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCoteAmour.DisplayLayout.GroupByBox.BandLabelAppearance = appearance252;
            this.cmbCoteAmour.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance253.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance253.BackColor2 = System.Drawing.SystemColors.Control;
            appearance253.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance253.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCoteAmour.DisplayLayout.GroupByBox.PromptAppearance = appearance253;
            this.cmbCoteAmour.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCoteAmour.DisplayLayout.MaxRowScrollRegions = 1;
            appearance254.BackColor = System.Drawing.SystemColors.Window;
            appearance254.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCoteAmour.DisplayLayout.Override.ActiveCellAppearance = appearance254;
            appearance255.BackColor = System.Drawing.SystemColors.Highlight;
            appearance255.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCoteAmour.DisplayLayout.Override.ActiveRowAppearance = appearance255;
            this.cmbCoteAmour.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCoteAmour.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance256.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCoteAmour.DisplayLayout.Override.CardAreaAppearance = appearance256;
            appearance257.BorderColor = System.Drawing.Color.Silver;
            appearance257.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCoteAmour.DisplayLayout.Override.CellAppearance = appearance257;
            this.cmbCoteAmour.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCoteAmour.DisplayLayout.Override.CellPadding = 0;
            appearance258.BackColor = System.Drawing.SystemColors.Control;
            appearance258.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance258.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance258.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance258.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCoteAmour.DisplayLayout.Override.GroupByRowAppearance = appearance258;
            appearance259.TextHAlignAsString = "Left";
            this.cmbCoteAmour.DisplayLayout.Override.HeaderAppearance = appearance259;
            this.cmbCoteAmour.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCoteAmour.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance260.BackColor = System.Drawing.SystemColors.Window;
            appearance260.BorderColor = System.Drawing.Color.Silver;
            this.cmbCoteAmour.DisplayLayout.Override.RowAppearance = appearance260;
            this.cmbCoteAmour.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance261.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCoteAmour.DisplayLayout.Override.TemplateAddRowAppearance = appearance261;
            this.cmbCoteAmour.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCoteAmour.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCoteAmour.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCoteAmour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCoteAmour.Location = new System.Drawing.Point(186, 33);
            this.cmbCoteAmour.Name = "cmbCoteAmour";
            this.cmbCoteAmour.Size = new System.Drawing.Size(362, 25);
            this.cmbCoteAmour.TabIndex = 503;
            // 
            // frmFactures
            // 
            this.frmFactures.BackColor = System.Drawing.Color.Transparent;
            this.frmFactures.Controls.Add(this.tableLayoutPanel30);
            this.frmFactures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frmFactures.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.frmFactures.Location = new System.Drawing.Point(922, 3);
            this.frmFactures.Name = "frmFactures";
            this.frmFactures.Size = new System.Drawing.Size(913, 184);
            this.frmFactures.TabIndex = 542;
            this.frmFactures.TabStop = false;
            this.frmFactures.Text = "Montants facturés";
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 4;
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel30.Controls.Add(this.lblEcTrav, 3, 2);
            this.tableLayoutPanel30.Controls.Add(this.label38, 0, 0);
            this.tableLayoutPanel30.Controls.Add(this.FrameFioul, 0, 3);
            this.tableLayoutPanel30.Controls.Add(this.lblNTrav, 2, 2);
            this.tableLayoutPanel30.Controls.Add(this.label58, 0, 2);
            this.tableLayoutPanel30.Controls.Add(this.lblN1Trav, 1, 2);
            this.tableLayoutPanel30.Controls.Add(this.Label356, 1, 0);
            this.tableLayoutPanel30.Controls.Add(this.Label357, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.Label3516, 3, 0);
            this.tableLayoutPanel30.Controls.Add(this.lblEcCont, 3, 1);
            this.tableLayoutPanel30.Controls.Add(this.label57, 0, 1);
            this.tableLayoutPanel30.Controls.Add(this.lblNCont, 2, 1);
            this.tableLayoutPanel30.Controls.Add(this.lblN1Cont, 1, 1);
            this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 4;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(907, 163);
            this.tableLayoutPanel30.TabIndex = 582;
            // 
            // lblEcTrav
            // 
            this.lblEcTrav.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEcTrav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEcTrav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEcTrav.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblEcTrav.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblEcTrav.Location = new System.Drawing.Point(678, 80);
            this.lblEcTrav.Margin = new System.Windows.Forms.Padding(0);
            this.lblEcTrav.Name = "lblEcTrav";
            this.lblEcTrav.Size = new System.Drawing.Size(229, 40);
            this.lblEcTrav.TabIndex = 34;
            this.lblEcTrav.Text = "lblEcTrav";
            this.lblEcTrav.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.BackColor = System.Drawing.Color.Transparent;
            this.label38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label38.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.label38.Location = new System.Drawing.Point(2, 0);
            this.label38.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(222, 40);
            this.label38.TabIndex = 24;
            this.label38.Text = "Année";
            this.label38.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FrameFioul
            // 
            this.FrameFioul.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.tableLayoutPanel30.SetColumnSpan(this.FrameFioul, 4);
            this.FrameFioul.Controls.Add(this.tableLayoutPanel37);
            this.FrameFioul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrameFioul.Location = new System.Drawing.Point(0, 120);
            this.FrameFioul.Margin = new System.Windows.Forms.Padding(0);
            this.FrameFioul.Name = "FrameFioul";
            this.FrameFioul.Size = new System.Drawing.Size(907, 43);
            this.FrameFioul.TabIndex = 583;
            // 
            // tableLayoutPanel37
            // 
            this.tableLayoutPanel37.ColumnCount = 4;
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel37.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel37.Controls.Add(this.label62, 0, 0);
            this.tableLayoutPanel37.Controls.Add(this.lblN1Fioul, 1, 0);
            this.tableLayoutPanel37.Controls.Add(this.lblEcFioul, 3, 0);
            this.tableLayoutPanel37.Controls.Add(this.lblNFioul, 2, 0);
            this.tableLayoutPanel37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel37.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel37.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel37.Name = "tableLayoutPanel37";
            this.tableLayoutPanel37.RowCount = 1;
            this.tableLayoutPanel37.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel37.Size = new System.Drawing.Size(907, 43);
            this.tableLayoutPanel37.TabIndex = 0;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.BackColor = System.Drawing.Color.Transparent;
            this.label62.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label62.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.label62.Location = new System.Drawing.Point(2, 0);
            this.label62.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(222, 43);
            this.label62.TabIndex = 43;
            this.label62.Text = "Fioul";
            this.label62.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN1Fioul
            // 
            this.lblN1Fioul.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblN1Fioul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblN1Fioul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblN1Fioul.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblN1Fioul.Location = new System.Drawing.Point(226, 0);
            this.lblN1Fioul.Margin = new System.Windows.Forms.Padding(0);
            this.lblN1Fioul.Name = "lblN1Fioul";
            this.lblN1Fioul.Size = new System.Drawing.Size(226, 43);
            this.lblN1Fioul.TabIndex = 40;
            this.lblN1Fioul.Text = "lblN1Fioul";
            this.lblN1Fioul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEcFioul
            // 
            this.lblEcFioul.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEcFioul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEcFioul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEcFioul.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.lblEcFioul.Location = new System.Drawing.Point(678, 0);
            this.lblEcFioul.Margin = new System.Windows.Forms.Padding(0);
            this.lblEcFioul.Name = "lblEcFioul";
            this.lblEcFioul.Size = new System.Drawing.Size(229, 43);
            this.lblEcFioul.TabIndex = 42;
            this.lblEcFioul.Text = "lblEcFioul";
            this.lblEcFioul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNFioul
            // 
            this.lblNFioul.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNFioul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNFioul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNFioul.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNFioul.Location = new System.Drawing.Point(452, 0);
            this.lblNFioul.Margin = new System.Windows.Forms.Padding(0);
            this.lblNFioul.Name = "lblNFioul";
            this.lblNFioul.Size = new System.Drawing.Size(226, 43);
            this.lblNFioul.TabIndex = 41;
            this.lblNFioul.Text = "lblNFioul";
            this.lblNFioul.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNTrav
            // 
            this.lblNTrav.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNTrav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNTrav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNTrav.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNTrav.Location = new System.Drawing.Point(452, 80);
            this.lblNTrav.Margin = new System.Windows.Forms.Padding(0);
            this.lblNTrav.Name = "lblNTrav";
            this.lblNTrav.Size = new System.Drawing.Size(226, 40);
            this.lblNTrav.TabIndex = 33;
            this.lblNTrav.Text = "lblNTrav";
            this.lblNTrav.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.BackColor = System.Drawing.Color.Transparent;
            this.label58.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label58.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.label58.Location = new System.Drawing.Point(2, 80);
            this.label58.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(222, 40);
            this.label58.TabIndex = 35;
            this.label58.Text = "Travaux";
            this.label58.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN1Trav
            // 
            this.lblN1Trav.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblN1Trav.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblN1Trav.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblN1Trav.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblN1Trav.Location = new System.Drawing.Point(226, 80);
            this.lblN1Trav.Margin = new System.Windows.Forms.Padding(0);
            this.lblN1Trav.Name = "lblN1Trav";
            this.lblN1Trav.Size = new System.Drawing.Size(226, 40);
            this.lblN1Trav.TabIndex = 32;
            this.lblN1Trav.Text = "lblN1Trav";
            this.lblN1Trav.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label356
            // 
            this.Label356.BackColor = System.Drawing.Color.Transparent;
            this.Label356.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label356.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.Label356.Location = new System.Drawing.Point(228, 0);
            this.Label356.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label356.Name = "Label356";
            this.Label356.Size = new System.Drawing.Size(222, 40);
            this.Label356.TabIndex = 25;
            this.Label356.Text = "n - 1";
            this.Label356.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label357
            // 
            this.Label357.BackColor = System.Drawing.Color.Transparent;
            this.Label357.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label357.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.Label357.Location = new System.Drawing.Point(454, 0);
            this.Label357.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label357.Name = "Label357";
            this.Label357.Size = new System.Drawing.Size(222, 40);
            this.Label357.TabIndex = 26;
            this.Label357.Text = "n";
            this.Label357.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Label3516
            // 
            this.Label3516.BackColor = System.Drawing.Color.Transparent;
            this.Label3516.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label3516.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.Label3516.Location = new System.Drawing.Point(680, 0);
            this.Label3516.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label3516.Name = "Label3516";
            this.Label3516.Size = new System.Drawing.Size(225, 40);
            this.Label3516.TabIndex = 27;
            this.Label3516.Text = "écart";
            this.Label3516.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblEcCont
            // 
            this.lblEcCont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblEcCont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEcCont.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblEcCont.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblEcCont.ForeColor = System.Drawing.Color.SpringGreen;
            this.lblEcCont.Location = new System.Drawing.Point(678, 40);
            this.lblEcCont.Margin = new System.Windows.Forms.Padding(0);
            this.lblEcCont.Name = "lblEcCont";
            this.lblEcCont.Size = new System.Drawing.Size(229, 40);
            this.lblEcCont.TabIndex = 30;
            this.lblEcCont.Text = "lblEcCont";
            this.lblEcCont.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.BackColor = System.Drawing.Color.Transparent;
            this.label57.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label57.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Bold);
            this.label57.Location = new System.Drawing.Point(2, 40);
            this.label57.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(222, 40);
            this.label57.TabIndex = 31;
            this.label57.Text = "Contrat";
            this.label57.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblNCont
            // 
            this.lblNCont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblNCont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNCont.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblNCont.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNCont.Location = new System.Drawing.Point(452, 40);
            this.lblNCont.Margin = new System.Windows.Forms.Padding(0);
            this.lblNCont.Name = "lblNCont";
            this.lblNCont.Size = new System.Drawing.Size(226, 40);
            this.lblNCont.TabIndex = 29;
            this.lblNCont.Text = "lblNCont";
            this.lblNCont.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblN1Cont
            // 
            this.lblN1Cont.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblN1Cont.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblN1Cont.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lblN1Cont.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblN1Cont.Location = new System.Drawing.Point(226, 40);
            this.lblN1Cont.Margin = new System.Windows.Forms.Padding(0);
            this.lblN1Cont.Name = "lblN1Cont";
            this.lblN1Cont.Size = new System.Drawing.Size(226, 40);
            this.lblN1Cont.TabIndex = 28;
            this.lblN1Cont.Text = "lblNCont";
            this.lblN1Cont.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.groupBox6);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(1844, 796);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.tableLayoutPanel49);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Location = new System.Drawing.Point(0, 0);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox6.Size = new System.Drawing.Size(1844, 796);
            this.groupBox6.TabIndex = 417;
            this.groupBox6.TabStop = false;
            // 
            // tableLayoutPanel49
            // 
            this.tableLayoutPanel49.ColumnCount = 2;
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel49.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel49.Controls.Add(this.FilDocClient, 1, 1);
            this.tableLayoutPanel49.Controls.Add(this.DirDocClient, 0, 1);
            this.tableLayoutPanel49.Controls.Add(this.tableLayoutPanel38, 0, 0);
            this.tableLayoutPanel49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel49.Location = new System.Drawing.Point(2, 18);
            this.tableLayoutPanel49.Name = "tableLayoutPanel49";
            this.tableLayoutPanel49.RowCount = 2;
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel49.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel49.Size = new System.Drawing.Size(1840, 775);
            this.tableLayoutPanel49.TabIndex = 2;
            // 
            // FilDocClient
            // 
            this.FilDocClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FilDocClient.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.FilDocClient.FormattingEnabled = true;
            this.FilDocClient.ItemHeight = 19;
            this.FilDocClient.Location = new System.Drawing.Point(922, 41);
            this.FilDocClient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.FilDocClient.Name = "FilDocClient";
            this.FilDocClient.Size = new System.Drawing.Size(916, 731);
            this.FilDocClient.TabIndex = 1;
            this.FilDocClient.DoubleClick += new System.EventHandler(this.FilDocClient_DoubleClick);
            // 
            // DirDocClient
            // 
            this.DirDocClient.AllowDrop = true;
            this.DirDocClient.ContextMenuStrip = this.contextMenu;
            this.DirDocClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DirDocClient.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.DirDocClient.ImageIndex = 0;
            this.DirDocClient.ImageList = this.imageList1;
            this.DirDocClient.Location = new System.Drawing.Point(2, 41);
            this.DirDocClient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.DirDocClient.Name = "DirDocClient";
            this.DirDocClient.SelectedImageIndex = 0;
            this.DirDocClient.Size = new System.Drawing.Size(916, 731);
            this.DirDocClient.TabIndex = 0;
            this.DirDocClient.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.DirDocClient_AfterSelect);
            this.DirDocClient.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.DirDocClient_NodeMouseClick);
            this.DirDocClient.DragDrop += new System.Windows.Forms.DragEventHandler(this.DirDocClient_DragDrop);
            this.DirDocClient.DragOver += new System.Windows.Forms.DragEventHandler(this.DirDocClient_DragOver);
            // 
            // contextMenu
            // 
            this.contextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.collerToolStripMenuItem});
            this.contextMenu.Name = "contextColler";
            this.contextMenu.Size = new System.Drawing.Size(106, 26);
            this.contextMenu.Text = "Coller";
            // 
            // collerToolStripMenuItem
            // 
            this.collerToolStripMenuItem.Name = "collerToolStripMenuItem";
            this.collerToolStripMenuItem.Size = new System.Drawing.Size(105, 22);
            this.collerToolStripMenuItem.Text = "Coller";
            this.collerToolStripMenuItem.Click += new System.EventHandler(this.collerToolStripMenuItem_Click_1);
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // tableLayoutPanel38
            // 
            this.tableLayoutPanel38.ColumnCount = 2;
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 218F));
            this.tableLayoutPanel38.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel38.Controls.Add(this.info1, 1, 0);
            this.tableLayoutPanel38.Controls.Add(this.buttonPastFiles, 0, 0);
            this.tableLayoutPanel38.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel38.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel38.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel38.Name = "tableLayoutPanel38";
            this.tableLayoutPanel38.RowCount = 1;
            this.tableLayoutPanel38.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel38.Size = new System.Drawing.Size(920, 38);
            this.tableLayoutPanel38.TabIndex = 351;
            // 
            // info1
            // 
            this.info1.BackColor = System.Drawing.Color.Transparent;
            this.info1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("info1.BackgroundImage")));
            this.info1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.info1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.info1.Dock = System.Windows.Forms.DockStyle.Left;
            this.info1.Format = Axe_interDT.Views.Theme.Info.InfoFormat.Squar;
            this.info1.Link = "http://azdt-iis-01:5000/Post/2";
            this.info1.Location = new System.Drawing.Point(218, 2);
            this.info1.Margin = new System.Windows.Forms.Padding(0, 2, 0, 2);
            this.info1.Name = "info1";
            this.info1.Size = new System.Drawing.Size(32, 34);
            this.info1.TabIndex = 353;
            // 
            // buttonPastFiles
            // 
            this.buttonPastFiles.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.buttonPastFiles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.buttonPastFiles.FlatAppearance.BorderSize = 0;
            this.buttonPastFiles.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPastFiles.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.buttonPastFiles.ForeColor = System.Drawing.Color.White;
            this.buttonPastFiles.Image = global::Axe_interDT.Properties.Resources.Copy_24x24;
            this.buttonPastFiles.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.buttonPastFiles.Location = new System.Drawing.Point(2, 2);
            this.buttonPastFiles.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.buttonPastFiles.Name = "buttonPastFiles";
            this.buttonPastFiles.Size = new System.Drawing.Size(216, 34);
            this.buttonPastFiles.TabIndex = 350;
            this.buttonPastFiles.Text = "Coller Fichier(s) / Dossier(s)";
            this.buttonPastFiles.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.buttonPastFiles, "Annuler");
            this.buttonPastFiles.UseVisualStyleBackColor = false;
            this.buttonPastFiles.Click += new System.EventHandler(this.buttonPastFiles_Click);
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.Frame1);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(1844, 796);
            // 
            // Frame1
            // 
            this.Frame1.ColumnCount = 2;
            this.Frame1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Frame1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.Frame1.Controls.Add(this.tableLayoutPanel42, 1, 0);
            this.Frame1.Controls.Add(this.tableLayoutPanel43, 0, 0);
            this.Frame1.Controls.Add(this.ssIntervention, 0, 1);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Margin = new System.Windows.Forms.Padding(0);
            this.Frame1.Name = "Frame1";
            this.Frame1.RowCount = 2;
            this.Frame1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.Frame1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.Frame1.Size = new System.Drawing.Size(1844, 796);
            this.Frame1.TabIndex = 448;
            // 
            // tableLayoutPanel42
            // 
            this.tableLayoutPanel42.ColumnCount = 2;
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel42.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel42.Controls.Add(this.Label4187, 0, 2);
            this.tableLayoutPanel42.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel42.Controls.Add(this.txt100, 1, 2);
            this.tableLayoutPanel42.Controls.Add(this.cmbArticle, 1, 0);
            this.tableLayoutPanel42.Controls.Add(this.label28, 0, 1);
            this.tableLayoutPanel42.Controls.Add(this.cmbIntervention, 1, 3);
            this.tableLayoutPanel42.Controls.Add(this.txt4, 1, 1);
            this.tableLayoutPanel42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel42.Location = new System.Drawing.Point(922, 0);
            this.tableLayoutPanel42.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel42.Name = "tableLayoutPanel42";
            this.tableLayoutPanel42.RowCount = 4;
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel42.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel42.Size = new System.Drawing.Size(922, 131);
            this.tableLayoutPanel42.TabIndex = 2;
            // 
            // Label4187
            // 
            this.Label4187.AutoSize = true;
            this.Label4187.Dock = System.Windows.Forms.DockStyle.Left;
            this.Label4187.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label4187.Location = new System.Drawing.Point(3, 60);
            this.Label4187.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.Label4187.Name = "Label4187";
            this.Label4187.Size = new System.Drawing.Size(135, 30);
            this.Label4187.TabIndex = 357;
            this.Label4187.Text = "N°Enregistrement";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Left;
            this.label27.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label27.Location = new System.Drawing.Point(3, 0);
            this.label27.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(144, 30);
            this.label27.TabIndex = 17;
            this.label27.Text = "Type d\'intervention";
            // 
            // txt100
            // 
            this.txt100.AccAcceptNumbersOnly = false;
            this.txt100.AccAllowComma = false;
            this.txt100.AccBackgroundColor = System.Drawing.Color.White;
            this.txt100.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt100.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt100.AccHidenValue = "";
            this.txt100.AccNotAllowedChars = null;
            this.txt100.AccReadOnly = true;
            this.txt100.AccReadOnlyAllowDelete = false;
            this.txt100.AccRequired = false;
            this.txt100.BackColor = System.Drawing.Color.White;
            this.txt100.CustomBackColor = System.Drawing.Color.White;
            this.txt100.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt100.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt100.ForeColor = System.Drawing.Color.Black;
            this.txt100.Location = new System.Drawing.Point(278, 62);
            this.txt100.Margin = new System.Windows.Forms.Padding(2);
            this.txt100.MaxLength = 32767;
            this.txt100.Multiline = false;
            this.txt100.Name = "txt100";
            this.txt100.ReadOnly = false;
            this.txt100.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt100.Size = new System.Drawing.Size(642, 27);
            this.txt100.TabIndex = 12;
            this.txt100.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt100.UseSystemPasswordChar = false;
            // 
            // cmbArticle
            // 
            appearance262.BackColor = System.Drawing.SystemColors.Window;
            appearance262.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbArticle.DisplayLayout.Appearance = appearance262;
            this.cmbArticle.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbArticle.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance263.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance263.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance263.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance263.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.GroupByBox.Appearance = appearance263;
            appearance264.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbArticle.DisplayLayout.GroupByBox.BandLabelAppearance = appearance264;
            this.cmbArticle.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance265.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance265.BackColor2 = System.Drawing.SystemColors.Control;
            appearance265.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance265.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbArticle.DisplayLayout.GroupByBox.PromptAppearance = appearance265;
            this.cmbArticle.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbArticle.DisplayLayout.MaxRowScrollRegions = 1;
            appearance266.BackColor = System.Drawing.SystemColors.Window;
            appearance266.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbArticle.DisplayLayout.Override.ActiveCellAppearance = appearance266;
            appearance267.BackColor = System.Drawing.SystemColors.Highlight;
            appearance267.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbArticle.DisplayLayout.Override.ActiveRowAppearance = appearance267;
            this.cmbArticle.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbArticle.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance268.BackColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.Override.CardAreaAppearance = appearance268;
            appearance269.BorderColor = System.Drawing.Color.Silver;
            appearance269.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbArticle.DisplayLayout.Override.CellAppearance = appearance269;
            this.cmbArticle.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbArticle.DisplayLayout.Override.CellPadding = 0;
            appearance270.BackColor = System.Drawing.SystemColors.Control;
            appearance270.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance270.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance270.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance270.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.Override.GroupByRowAppearance = appearance270;
            appearance271.TextHAlignAsString = "Left";
            this.cmbArticle.DisplayLayout.Override.HeaderAppearance = appearance271;
            this.cmbArticle.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbArticle.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance272.BackColor = System.Drawing.SystemColors.Window;
            appearance272.BorderColor = System.Drawing.Color.Silver;
            this.cmbArticle.DisplayLayout.Override.RowAppearance = appearance272;
            this.cmbArticle.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance273.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbArticle.DisplayLayout.Override.TemplateAddRowAppearance = appearance273;
            this.cmbArticle.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbArticle.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbArticle.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbArticle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbArticle.Location = new System.Drawing.Point(278, 3);
            this.cmbArticle.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbArticle.Name = "cmbArticle";
            this.cmbArticle.Size = new System.Drawing.Size(642, 27);
            this.cmbArticle.TabIndex = 10;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Left;
            this.label28.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label28.Location = new System.Drawing.Point(3, 30);
            this.label28.Margin = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(104, 30);
            this.label28.TabIndex = 13;
            this.label28.Text = "Commentaire";
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmbIntervention.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervention.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbIntervention.Location = new System.Drawing.Point(860, 93);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(60, 35);
            this.cmbIntervention.TabIndex = 352;
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbIntervention, "Rechercher");
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // txt4
            // 
            this.txt4.AccAcceptNumbersOnly = false;
            this.txt4.AccAllowComma = false;
            this.txt4.AccBackgroundColor = System.Drawing.Color.White;
            this.txt4.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt4.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt4.AccHidenValue = "";
            this.txt4.AccNotAllowedChars = null;
            this.txt4.AccReadOnly = false;
            this.txt4.AccReadOnlyAllowDelete = false;
            this.txt4.AccRequired = false;
            this.txt4.BackColor = System.Drawing.Color.White;
            this.txt4.CustomBackColor = System.Drawing.Color.White;
            this.txt4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt4.ForeColor = System.Drawing.Color.Black;
            this.txt4.Location = new System.Drawing.Point(278, 32);
            this.txt4.Margin = new System.Windows.Forms.Padding(2);
            this.txt4.MaxLength = 32767;
            this.txt4.Multiline = false;
            this.txt4.Name = "txt4";
            this.txt4.ReadOnly = false;
            this.txt4.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt4.Size = new System.Drawing.Size(642, 27);
            this.txt4.TabIndex = 11;
            this.txt4.Tag = "4";
            this.txt4.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt4.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel43
            // 
            this.tableLayoutPanel43.ColumnCount = 2;
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel43.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel44, 1, 2);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel45, 1, 3);
            this.tableLayoutPanel43.Controls.Add(this.tableLayoutPanel46, 1, 1);
            this.tableLayoutPanel43.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel43.Controls.Add(this.label26, 0, 3);
            this.tableLayoutPanel43.Controls.Add(this.cmbCodeimmeuble, 1, 0);
            this.tableLayoutPanel43.Controls.Add(this.label23, 0, 1);
            this.tableLayoutPanel43.Controls.Add(this.label25s, 0, 2);
            this.tableLayoutPanel43.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel43.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel43.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel43.Name = "tableLayoutPanel43";
            this.tableLayoutPanel43.RowCount = 4;
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel43.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel43.Size = new System.Drawing.Size(922, 131);
            this.tableLayoutPanel43.TabIndex = 0;
            // 
            // tableLayoutPanel44
            // 
            this.tableLayoutPanel44.ColumnCount = 2;
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel44.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel44.Controls.Add(this.cmbStatus, 0, 0);
            this.tableLayoutPanel44.Controls.Add(this.lblLibStatus, 1, 0);
            this.tableLayoutPanel44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel44.Location = new System.Drawing.Point(276, 64);
            this.tableLayoutPanel44.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel44.Name = "tableLayoutPanel44";
            this.tableLayoutPanel44.RowCount = 1;
            this.tableLayoutPanel44.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel44.Size = new System.Drawing.Size(646, 32);
            this.tableLayoutPanel44.TabIndex = 12;
            // 
            // cmbStatus
            // 
            appearance274.BackColor = System.Drawing.SystemColors.Window;
            appearance274.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbStatus.DisplayLayout.Appearance = appearance274;
            this.cmbStatus.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbStatus.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance275.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance275.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance275.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance275.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.GroupByBox.Appearance = appearance275;
            appearance276.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatus.DisplayLayout.GroupByBox.BandLabelAppearance = appearance276;
            this.cmbStatus.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance277.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance277.BackColor2 = System.Drawing.SystemColors.Control;
            appearance277.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance277.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatus.DisplayLayout.GroupByBox.PromptAppearance = appearance277;
            this.cmbStatus.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbStatus.DisplayLayout.MaxRowScrollRegions = 1;
            appearance278.BackColor = System.Drawing.SystemColors.Window;
            appearance278.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbStatus.DisplayLayout.Override.ActiveCellAppearance = appearance278;
            appearance279.BackColor = System.Drawing.SystemColors.Highlight;
            appearance279.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbStatus.DisplayLayout.Override.ActiveRowAppearance = appearance279;
            this.cmbStatus.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbStatus.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance280.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.Override.CardAreaAppearance = appearance280;
            appearance281.BorderColor = System.Drawing.Color.Silver;
            appearance281.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbStatus.DisplayLayout.Override.CellAppearance = appearance281;
            this.cmbStatus.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbStatus.DisplayLayout.Override.CellPadding = 0;
            appearance282.BackColor = System.Drawing.SystemColors.Control;
            appearance282.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance282.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance282.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance282.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.Override.GroupByRowAppearance = appearance282;
            appearance283.TextHAlignAsString = "Left";
            this.cmbStatus.DisplayLayout.Override.HeaderAppearance = appearance283;
            this.cmbStatus.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbStatus.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance284.BackColor = System.Drawing.SystemColors.Window;
            appearance284.BorderColor = System.Drawing.Color.Silver;
            this.cmbStatus.DisplayLayout.Override.RowAppearance = appearance284;
            this.cmbStatus.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance285.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbStatus.DisplayLayout.Override.TemplateAddRowAppearance = appearance285;
            this.cmbStatus.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbStatus.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbStatus.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbStatus.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbStatus.Location = new System.Drawing.Point(2, 3);
            this.cmbStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(383, 27);
            this.cmbStatus.TabIndex = 6;
            this.cmbStatus.Validating += new System.ComponentModel.CancelEventHandler(this.cmbStatus_Validating);
            // 
            // lblLibStatus
            // 
            this.lblLibStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblLibStatus.Location = new System.Drawing.Point(389, 0);
            this.lblLibStatus.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblLibStatus.Name = "lblLibStatus";
            this.lblLibStatus.Size = new System.Drawing.Size(255, 32);
            this.lblLibStatus.TabIndex = 354;
            // 
            // tableLayoutPanel45
            // 
            this.tableLayoutPanel45.ColumnCount = 2;
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel45.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel45.Controls.Add(this.lbllibIntervenant, 0, 0);
            this.tableLayoutPanel45.Controls.Add(this.cmbIntervenant, 0, 0);
            this.tableLayoutPanel45.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel45.Location = new System.Drawing.Point(276, 96);
            this.tableLayoutPanel45.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel45.Name = "tableLayoutPanel45";
            this.tableLayoutPanel45.RowCount = 1;
            this.tableLayoutPanel45.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel45.Size = new System.Drawing.Size(646, 35);
            this.tableLayoutPanel45.TabIndex = 13;
            // 
            // lbllibIntervenant
            // 
            this.lbllibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenant.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lbllibIntervenant.Location = new System.Drawing.Point(389, 0);
            this.lbllibIntervenant.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lbllibIntervenant.Name = "lbllibIntervenant";
            this.lbllibIntervenant.Size = new System.Drawing.Size(255, 35);
            this.lbllibIntervenant.TabIndex = 355;
            // 
            // cmbIntervenant
            // 
            appearance286.BackColor = System.Drawing.SystemColors.Window;
            appearance286.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenant.DisplayLayout.Appearance = appearance286;
            this.cmbIntervenant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance287.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance287.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance287.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance287.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.GroupByBox.Appearance = appearance287;
            appearance288.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance288;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance289.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance289.BackColor2 = System.Drawing.SystemColors.Control;
            appearance289.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance289.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.PromptAppearance = appearance289;
            this.cmbIntervenant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance290.BackColor = System.Drawing.SystemColors.Window;
            appearance290.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveCellAppearance = appearance290;
            appearance291.BackColor = System.Drawing.SystemColors.Highlight;
            appearance291.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveRowAppearance = appearance291;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance292.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.CardAreaAppearance = appearance292;
            appearance293.BorderColor = System.Drawing.Color.Silver;
            appearance293.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenant.DisplayLayout.Override.CellAppearance = appearance293;
            this.cmbIntervenant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenant.DisplayLayout.Override.CellPadding = 0;
            appearance294.BackColor = System.Drawing.SystemColors.Control;
            appearance294.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance294.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance294.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance294.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.GroupByRowAppearance = appearance294;
            appearance295.TextHAlignAsString = "Left";
            this.cmbIntervenant.DisplayLayout.Override.HeaderAppearance = appearance295;
            this.cmbIntervenant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance296.BackColor = System.Drawing.SystemColors.Window;
            appearance296.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenant.DisplayLayout.Override.RowAppearance = appearance296;
            this.cmbIntervenant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance297.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenant.DisplayLayout.Override.TemplateAddRowAppearance = appearance297;
            this.cmbIntervenant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbIntervenant.Location = new System.Drawing.Point(2, 3);
            this.cmbIntervenant.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbIntervenant.Name = "cmbIntervenant";
            this.cmbIntervenant.Size = new System.Drawing.Size(383, 27);
            this.cmbIntervenant.TabIndex = 8;
            this.cmbIntervenant.Validating += new System.ComponentModel.CancelEventHandler(this.cmbIntervenant_Validating);
            // 
            // tableLayoutPanel46
            // 
            this.tableLayoutPanel46.ColumnCount = 5;
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel46.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel46.Controls.Add(this.txtinterDe, 0, 0);
            this.tableLayoutPanel46.Controls.Add(this.label24, 2, 0);
            this.tableLayoutPanel46.Controls.Add(this.txtIntereAu, 3, 0);
            this.tableLayoutPanel46.Controls.Add(this.ultraDateTimeEditor1, 1, 0);
            this.tableLayoutPanel46.Controls.Add(this.ultraDateTimeEditor2, 4, 0);
            this.tableLayoutPanel46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel46.Location = new System.Drawing.Point(276, 32);
            this.tableLayoutPanel46.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel46.Name = "tableLayoutPanel46";
            this.tableLayoutPanel46.RowCount = 1;
            this.tableLayoutPanel46.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel46.Size = new System.Drawing.Size(646, 32);
            this.tableLayoutPanel46.TabIndex = 11;
            // 
            // txtinterDe
            // 
            this.txtinterDe.AccAcceptNumbersOnly = false;
            this.txtinterDe.AccAllowComma = false;
            this.txtinterDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtinterDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtinterDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtinterDe.AccHidenValue = "";
            this.txtinterDe.AccNotAllowedChars = null;
            this.txtinterDe.AccReadOnly = false;
            this.txtinterDe.AccReadOnlyAllowDelete = false;
            this.txtinterDe.AccRequired = false;
            this.txtinterDe.BackColor = System.Drawing.Color.White;
            this.txtinterDe.CustomBackColor = System.Drawing.Color.White;
            this.txtinterDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtinterDe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtinterDe.ForeColor = System.Drawing.Color.Black;
            this.txtinterDe.Location = new System.Drawing.Point(2, 2);
            this.txtinterDe.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtinterDe.MaxLength = 32767;
            this.txtinterDe.Multiline = false;
            this.txtinterDe.Name = "txtinterDe";
            this.txtinterDe.ReadOnly = false;
            this.txtinterDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtinterDe.Size = new System.Drawing.Size(285, 27);
            this.txtinterDe.TabIndex = 2;
            this.txtinterDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtinterDe.UseSystemPasswordChar = false;
            this.txtinterDe.Validating += new System.ComponentModel.CancelEventHandler(this.txtinterDe_Validating);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label24.Location = new System.Drawing.Point(309, 0);
            this.label24.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(26, 32);
            this.label24.TabIndex = 5;
            this.label24.Text = "au";
            // 
            // txtIntereAu
            // 
            this.txtIntereAu.AccAcceptNumbersOnly = false;
            this.txtIntereAu.AccAllowComma = false;
            this.txtIntereAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntereAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntereAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntereAu.AccHidenValue = "";
            this.txtIntereAu.AccNotAllowedChars = null;
            this.txtIntereAu.AccReadOnly = false;
            this.txtIntereAu.AccReadOnlyAllowDelete = false;
            this.txtIntereAu.AccRequired = false;
            this.txtIntereAu.BackColor = System.Drawing.Color.White;
            this.txtIntereAu.CustomBackColor = System.Drawing.Color.White;
            this.txtIntereAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntereAu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIntereAu.ForeColor = System.Drawing.Color.Black;
            this.txtIntereAu.Location = new System.Drawing.Point(339, 2);
            this.txtIntereAu.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtIntereAu.MaxLength = 32767;
            this.txtIntereAu.Multiline = false;
            this.txtIntereAu.Name = "txtIntereAu";
            this.txtIntereAu.ReadOnly = false;
            this.txtIntereAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntereAu.Size = new System.Drawing.Size(285, 27);
            this.txtIntereAu.TabIndex = 3;
            this.txtIntereAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntereAu.UseSystemPasswordChar = false;
            this.txtIntereAu.Validating += new System.ComponentModel.CancelEventHandler(this.txtIntereAu_Validating);
            // 
            // ultraDateTimeEditor1
            // 
            this.ultraDateTimeEditor1.DateTime = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(287, 2);
            this.ultraDateTimeEditor1.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor1.MinimumSize = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor1.TabIndex = 6;
            this.ultraDateTimeEditor1.Value = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor1.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor1_ValueChanged);
            this.ultraDateTimeEditor1.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor1_AfterCloseUp);
            // 
            // ultraDateTimeEditor2
            // 
            this.ultraDateTimeEditor2.DateTime = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(624, 2);
            this.ultraDateTimeEditor2.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor2.MinimumSize = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(20, 24);
            this.ultraDateTimeEditor2.TabIndex = 7;
            this.ultraDateTimeEditor2.Value = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor2.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor2_ValueChanged);
            this.ultraDateTimeEditor2.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor2_AfterCloseUp);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label22.Location = new System.Drawing.Point(2, 0);
            this.label22.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(146, 19);
            this.label22.TabIndex = 1;
            this.label22.Text = "Sélection immeuble";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label26.Location = new System.Drawing.Point(2, 96);
            this.label26.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(97, 19);
            this.label26.TabIndex = 1;
            this.label26.Text = "Intervenants";
            // 
            // cmbCodeimmeuble
            // 
            appearance298.BackColor = System.Drawing.SystemColors.Window;
            appearance298.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCodeimmeuble.DisplayLayout.Appearance = appearance298;
            this.cmbCodeimmeuble.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCodeimmeuble.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance299.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance299.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance299.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance299.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.Appearance = appearance299;
            appearance300.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BandLabelAppearance = appearance300;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance301.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance301.BackColor2 = System.Drawing.SystemColors.Control;
            appearance301.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance301.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.PromptAppearance = appearance301;
            this.cmbCodeimmeuble.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCodeimmeuble.DisplayLayout.MaxRowScrollRegions = 1;
            appearance302.BackColor = System.Drawing.SystemColors.Window;
            appearance302.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveCellAppearance = appearance302;
            appearance303.BackColor = System.Drawing.SystemColors.Highlight;
            appearance303.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveRowAppearance = appearance303;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance304.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.CardAreaAppearance = appearance304;
            appearance305.BorderColor = System.Drawing.Color.Silver;
            appearance305.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellAppearance = appearance305;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellPadding = 0;
            appearance306.BackColor = System.Drawing.SystemColors.Control;
            appearance306.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance306.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance306.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance306.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.GroupByRowAppearance = appearance306;
            appearance307.TextHAlignAsString = "Left";
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderAppearance = appearance307;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance308.BackColor = System.Drawing.SystemColors.Window;
            appearance308.BorderColor = System.Drawing.Color.Silver;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowAppearance = appearance308;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance309.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCodeimmeuble.DisplayLayout.Override.TemplateAddRowAppearance = appearance309;
            this.cmbCodeimmeuble.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCodeimmeuble.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCodeimmeuble.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbCodeimmeuble.Location = new System.Drawing.Point(278, 3);
            this.cmbCodeimmeuble.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbCodeimmeuble.Name = "cmbCodeimmeuble";
            this.cmbCodeimmeuble.Size = new System.Drawing.Size(642, 27);
            this.cmbCodeimmeuble.TabIndex = 10;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label23.Location = new System.Drawing.Point(2, 32);
            this.label23.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(73, 19);
            this.label23.TabIndex = 1;
            this.label23.Text = "Depuis le";
            // 
            // label25s
            // 
            this.label25s.AutoSize = true;
            this.label25s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label25s.Location = new System.Drawing.Point(2, 64);
            this.label25s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label25s.Name = "label25s";
            this.label25s.Size = new System.Drawing.Size(53, 19);
            this.label25s.TabIndex = 1;
            this.label25s.Text = "Status";
            // 
            // ssIntervention
            // 
            this.Frame1.SetColumnSpan(this.ssIntervention, 2);
            appearance310.BackColor = System.Drawing.SystemColors.Window;
            appearance310.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssIntervention.DisplayLayout.Appearance = appearance310;
            this.ssIntervention.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssIntervention.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssIntervention.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance311.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance311.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance311.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance311.BorderColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.GroupByBox.Appearance = appearance311;
            appearance312.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssIntervention.DisplayLayout.GroupByBox.BandLabelAppearance = appearance312;
            this.ssIntervention.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance313.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance313.BackColor2 = System.Drawing.SystemColors.Control;
            appearance313.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance313.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssIntervention.DisplayLayout.GroupByBox.PromptAppearance = appearance313;
            this.ssIntervention.DisplayLayout.MaxColScrollRegions = 1;
            this.ssIntervention.DisplayLayout.MaxRowScrollRegions = 1;
            appearance314.BackColor = System.Drawing.SystemColors.Window;
            appearance314.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssIntervention.DisplayLayout.Override.ActiveCellAppearance = appearance314;
            appearance315.BackColor = System.Drawing.SystemColors.Highlight;
            appearance315.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssIntervention.DisplayLayout.Override.ActiveRowAppearance = appearance315;
            this.ssIntervention.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssIntervention.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssIntervention.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ssIntervention.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssIntervention.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance316.BackColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.Override.CardAreaAppearance = appearance316;
            appearance317.BorderColor = System.Drawing.Color.Silver;
            appearance317.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssIntervention.DisplayLayout.Override.CellAppearance = appearance317;
            this.ssIntervention.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssIntervention.DisplayLayout.Override.CellPadding = 0;
            appearance318.BackColor = System.Drawing.SystemColors.Control;
            appearance318.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance318.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance318.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance318.BorderColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.Override.GroupByRowAppearance = appearance318;
            appearance319.TextHAlignAsString = "Left";
            this.ssIntervention.DisplayLayout.Override.HeaderAppearance = appearance319;
            this.ssIntervention.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssIntervention.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance320.BackColor = System.Drawing.SystemColors.Window;
            appearance320.BorderColor = System.Drawing.Color.Silver;
            this.ssIntervention.DisplayLayout.Override.RowAppearance = appearance320;
            this.ssIntervention.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssIntervention.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance321.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssIntervention.DisplayLayout.Override.TemplateAddRowAppearance = appearance321;
            this.ssIntervention.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssIntervention.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssIntervention.Location = new System.Drawing.Point(2, 134);
            this.ssIntervention.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssIntervention.Name = "ssIntervention";
            this.ssIntervention.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.ssIntervention.Size = new System.Drawing.Size(1840, 659);
            this.ssIntervention.TabIndex = 111;
            this.ssIntervention.Text = "ultraGrid1";
            this.ssIntervention.AfterCellUpdate += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ssIntervention_AfterCellUpdate);
            this.ssIntervention.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssIntervention_InitializeLayout);
            this.ssIntervention.AfterRowActivate += new System.EventHandler(this.ssIntervention_AfterRowActivate);
            this.ssIntervention.BeforeCellUpdate += new Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventHandler(this.ssIntervention_BeforeCellUpdate);
            this.ssIntervention.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssIntervention_DoubleClickRow);
            // 
            // ultraTabPageControl9
            // 
            this.ultraTabPageControl9.Controls.Add(this.tableLayoutPanel50);
            this.ultraTabPageControl9.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl9.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl9.Name = "ultraTabPageControl9";
            this.ultraTabPageControl9.Size = new System.Drawing.Size(1844, 796);
            // 
            // tableLayoutPanel50
            // 
            this.tableLayoutPanel50.ColumnCount = 4;
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.51613F));
            this.tableLayoutPanel50.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.54839F));
            this.tableLayoutPanel50.Controls.Add(this.cmbDevis, 3, 3);
            this.tableLayoutPanel50.Controls.Add(this.txt101, 3, 2);
            this.tableLayoutPanel50.Controls.Add(this.cmbDevisIntervenant, 1, 3);
            this.tableLayoutPanel50.Controls.Add(this.label31, 0, 3);
            this.tableLayoutPanel50.Controls.Add(this.Label4188, 2, 2);
            this.tableLayoutPanel50.Controls.Add(this.cmbDevisImmeuble, 1, 0);
            this.tableLayoutPanel50.Controls.Add(this.tableLayoutPanel51, 1, 1);
            this.tableLayoutPanel50.Controls.Add(this.txt1, 3, 1);
            this.tableLayoutPanel50.Controls.Add(this.label55, 2, 1);
            this.tableLayoutPanel50.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel50.Controls.Add(this.label32, 0, 2);
            this.tableLayoutPanel50.Controls.Add(this.label29, 2, 0);
            this.tableLayoutPanel50.Controls.Add(this.cmdDevisArticle, 3, 0);
            this.tableLayoutPanel50.Controls.Add(this.label34, 0, 1);
            this.tableLayoutPanel50.Controls.Add(this.cmbDevisStatus, 1, 2);
            this.tableLayoutPanel50.Controls.Add(this.ssDevis, 0, 4);
            this.tableLayoutPanel50.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel50.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel50.Name = "tableLayoutPanel50";
            this.tableLayoutPanel50.RowCount = 5;
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel50.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel50.Size = new System.Drawing.Size(1844, 796);
            this.tableLayoutPanel50.TabIndex = 417;
            // 
            // cmbDevis
            // 
            this.cmbDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmbDevis.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmbDevis.FlatAppearance.BorderSize = 0;
            this.cmbDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbDevis.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbDevis.Location = new System.Drawing.Point(1782, 93);
            this.cmbDevis.Margin = new System.Windows.Forms.Padding(2, 3, 2, 2);
            this.cmbDevis.Name = "cmbDevis";
            this.cmbDevis.Size = new System.Drawing.Size(60, 35);
            this.cmbDevis.TabIndex = 354;
            this.cmbDevis.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbDevis, "Rechercher");
            this.cmbDevis.UseVisualStyleBackColor = false;
            this.cmbDevis.Click += new System.EventHandler(this.cmbDevis_Click);
            // 
            // txt101
            // 
            this.txt101.AccAcceptNumbersOnly = false;
            this.txt101.AccAllowComma = false;
            this.txt101.AccBackgroundColor = System.Drawing.Color.White;
            this.txt101.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt101.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt101.AccHidenValue = "";
            this.txt101.AccNotAllowedChars = null;
            this.txt101.AccReadOnly = true;
            this.txt101.AccReadOnlyAllowDelete = false;
            this.txt101.AccRequired = false;
            this.txt101.BackColor = System.Drawing.Color.White;
            this.txt101.CustomBackColor = System.Drawing.Color.White;
            this.txt101.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt101.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt101.ForeColor = System.Drawing.Color.Black;
            this.txt101.Location = new System.Drawing.Point(1300, 62);
            this.txt101.Margin = new System.Windows.Forms.Padding(2);
            this.txt101.MaxLength = 32767;
            this.txt101.Multiline = false;
            this.txt101.Name = "txt101";
            this.txt101.ReadOnly = false;
            this.txt101.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt101.Size = new System.Drawing.Size(542, 27);
            this.txt101.TabIndex = 6;
            this.txt101.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt101.UseSystemPasswordChar = false;
            // 
            // cmbDevisIntervenant
            // 
            appearance322.BackColor = System.Drawing.SystemColors.Window;
            appearance322.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbDevisIntervenant.DisplayLayout.Appearance = appearance322;
            this.cmbDevisIntervenant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbDevisIntervenant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance323.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance323.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance323.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance323.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisIntervenant.DisplayLayout.GroupByBox.Appearance = appearance323;
            appearance324.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisIntervenant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance324;
            this.cmbDevisIntervenant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance325.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance325.BackColor2 = System.Drawing.SystemColors.Control;
            appearance325.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance325.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisIntervenant.DisplayLayout.GroupByBox.PromptAppearance = appearance325;
            this.cmbDevisIntervenant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbDevisIntervenant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance326.BackColor = System.Drawing.SystemColors.Window;
            appearance326.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbDevisIntervenant.DisplayLayout.Override.ActiveCellAppearance = appearance326;
            appearance327.BackColor = System.Drawing.SystemColors.Highlight;
            appearance327.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbDevisIntervenant.DisplayLayout.Override.ActiveRowAppearance = appearance327;
            this.cmbDevisIntervenant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbDevisIntervenant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance328.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDevisIntervenant.DisplayLayout.Override.CardAreaAppearance = appearance328;
            appearance329.BorderColor = System.Drawing.Color.Silver;
            appearance329.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbDevisIntervenant.DisplayLayout.Override.CellAppearance = appearance329;
            this.cmbDevisIntervenant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbDevisIntervenant.DisplayLayout.Override.CellPadding = 0;
            appearance330.BackColor = System.Drawing.SystemColors.Control;
            appearance330.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance330.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance330.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance330.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisIntervenant.DisplayLayout.Override.GroupByRowAppearance = appearance330;
            appearance331.TextHAlignAsString = "Left";
            this.cmbDevisIntervenant.DisplayLayout.Override.HeaderAppearance = appearance331;
            this.cmbDevisIntervenant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbDevisIntervenant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance332.BackColor = System.Drawing.SystemColors.Window;
            appearance332.BorderColor = System.Drawing.Color.Silver;
            this.cmbDevisIntervenant.DisplayLayout.Override.RowAppearance = appearance332;
            this.cmbDevisIntervenant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance333.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbDevisIntervenant.DisplayLayout.Override.TemplateAddRowAppearance = appearance333;
            this.cmbDevisIntervenant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbDevisIntervenant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbDevisIntervenant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbDevisIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDevisIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbDevisIntervenant.Location = new System.Drawing.Point(278, 93);
            this.cmbDevisIntervenant.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDevisIntervenant.Name = "cmbDevisIntervenant";
            this.cmbDevisIntervenant.Size = new System.Drawing.Size(640, 27);
            this.cmbDevisIntervenant.TabIndex = 3;
            this.cmbDevisIntervenant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbDevisIntervenant_BeforeDropDown);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label31.Location = new System.Drawing.Point(2, 90);
            this.label31.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(70, 19);
            this.label31.TabIndex = 9;
            this.label31.Text = "Deviseur";
            // 
            // Label4188
            // 
            this.Label4188.AutoSize = true;
            this.Label4188.Dock = System.Windows.Forms.DockStyle.Left;
            this.Label4188.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label4188.Location = new System.Drawing.Point(922, 60);
            this.Label4188.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4188.Name = "Label4188";
            this.Label4188.Size = new System.Drawing.Size(135, 30);
            this.Label4188.TabIndex = 361;
            this.Label4188.Text = "N°Enregistrement";
            this.Label4188.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmbDevisImmeuble
            // 
            appearance334.BackColor = System.Drawing.SystemColors.Window;
            appearance334.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbDevisImmeuble.DisplayLayout.Appearance = appearance334;
            this.cmbDevisImmeuble.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbDevisImmeuble.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance335.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance335.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance335.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance335.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisImmeuble.DisplayLayout.GroupByBox.Appearance = appearance335;
            appearance336.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisImmeuble.DisplayLayout.GroupByBox.BandLabelAppearance = appearance336;
            this.cmbDevisImmeuble.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance337.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance337.BackColor2 = System.Drawing.SystemColors.Control;
            appearance337.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance337.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisImmeuble.DisplayLayout.GroupByBox.PromptAppearance = appearance337;
            this.cmbDevisImmeuble.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbDevisImmeuble.DisplayLayout.MaxRowScrollRegions = 1;
            appearance338.BackColor = System.Drawing.SystemColors.Window;
            appearance338.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbDevisImmeuble.DisplayLayout.Override.ActiveCellAppearance = appearance338;
            appearance339.BackColor = System.Drawing.SystemColors.Highlight;
            appearance339.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbDevisImmeuble.DisplayLayout.Override.ActiveRowAppearance = appearance339;
            this.cmbDevisImmeuble.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbDevisImmeuble.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance340.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDevisImmeuble.DisplayLayout.Override.CardAreaAppearance = appearance340;
            appearance341.BorderColor = System.Drawing.Color.Silver;
            appearance341.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbDevisImmeuble.DisplayLayout.Override.CellAppearance = appearance341;
            this.cmbDevisImmeuble.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbDevisImmeuble.DisplayLayout.Override.CellPadding = 0;
            appearance342.BackColor = System.Drawing.SystemColors.Control;
            appearance342.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance342.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance342.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance342.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisImmeuble.DisplayLayout.Override.GroupByRowAppearance = appearance342;
            appearance343.TextHAlignAsString = "Left";
            this.cmbDevisImmeuble.DisplayLayout.Override.HeaderAppearance = appearance343;
            this.cmbDevisImmeuble.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbDevisImmeuble.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance344.BackColor = System.Drawing.SystemColors.Window;
            appearance344.BorderColor = System.Drawing.Color.Silver;
            this.cmbDevisImmeuble.DisplayLayout.Override.RowAppearance = appearance344;
            this.cmbDevisImmeuble.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance345.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbDevisImmeuble.DisplayLayout.Override.TemplateAddRowAppearance = appearance345;
            this.cmbDevisImmeuble.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbDevisImmeuble.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbDevisImmeuble.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbDevisImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDevisImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbDevisImmeuble.Location = new System.Drawing.Point(278, 3);
            this.cmbDevisImmeuble.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDevisImmeuble.Name = "cmbDevisImmeuble";
            this.cmbDevisImmeuble.Size = new System.Drawing.Size(640, 27);
            this.cmbDevisImmeuble.TabIndex = 0;
            this.cmbDevisImmeuble.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbDevisImmeuble_BeforeDropDown);
            // 
            // tableLayoutPanel51
            // 
            this.tableLayoutPanel51.ColumnCount = 5;
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel51.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 22F));
            this.tableLayoutPanel51.Controls.Add(this.txtDevisDe, 0, 0);
            this.tableLayoutPanel51.Controls.Add(this.txtDevisAu, 3, 0);
            this.tableLayoutPanel51.Controls.Add(this.label33, 2, 0);
            this.tableLayoutPanel51.Controls.Add(this.ultraDateTimeEditor3, 1, 0);
            this.tableLayoutPanel51.Controls.Add(this.ultraDateTimeEditor4, 4, 0);
            this.tableLayoutPanel51.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel51.Location = new System.Drawing.Point(276, 30);
            this.tableLayoutPanel51.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel51.Name = "tableLayoutPanel51";
            this.tableLayoutPanel51.RowCount = 1;
            this.tableLayoutPanel51.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel51.Size = new System.Drawing.Size(644, 30);
            this.tableLayoutPanel51.TabIndex = 1;
            // 
            // txtDevisDe
            // 
            this.txtDevisDe.AccAcceptNumbersOnly = false;
            this.txtDevisDe.AccAllowComma = false;
            this.txtDevisDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDevisDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDevisDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevisDe.AccHidenValue = "";
            this.txtDevisDe.AccNotAllowedChars = null;
            this.txtDevisDe.AccReadOnly = false;
            this.txtDevisDe.AccReadOnlyAllowDelete = false;
            this.txtDevisDe.AccRequired = false;
            this.txtDevisDe.BackColor = System.Drawing.Color.White;
            this.txtDevisDe.CustomBackColor = System.Drawing.Color.White;
            this.txtDevisDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDevisDe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDevisDe.ForeColor = System.Drawing.Color.Black;
            this.txtDevisDe.Location = new System.Drawing.Point(2, 2);
            this.txtDevisDe.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDevisDe.MaxLength = 32767;
            this.txtDevisDe.Multiline = false;
            this.txtDevisDe.Name = "txtDevisDe";
            this.txtDevisDe.ReadOnly = false;
            this.txtDevisDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDevisDe.Size = new System.Drawing.Size(284, 27);
            this.txtDevisDe.TabIndex = 2;
            this.txtDevisDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDevisDe.UseSystemPasswordChar = false;
            // 
            // txtDevisAu
            // 
            this.txtDevisAu.AccAcceptNumbersOnly = false;
            this.txtDevisAu.AccAllowComma = false;
            this.txtDevisAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDevisAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDevisAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevisAu.AccHidenValue = "";
            this.txtDevisAu.AccNotAllowedChars = null;
            this.txtDevisAu.AccReadOnly = false;
            this.txtDevisAu.AccReadOnlyAllowDelete = false;
            this.txtDevisAu.AccRequired = false;
            this.txtDevisAu.BackColor = System.Drawing.Color.White;
            this.txtDevisAu.CustomBackColor = System.Drawing.Color.White;
            this.txtDevisAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDevisAu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDevisAu.ForeColor = System.Drawing.Color.Black;
            this.txtDevisAu.Location = new System.Drawing.Point(338, 2);
            this.txtDevisAu.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDevisAu.MaxLength = 32767;
            this.txtDevisAu.Multiline = false;
            this.txtDevisAu.Name = "txtDevisAu";
            this.txtDevisAu.ReadOnly = false;
            this.txtDevisAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDevisAu.Size = new System.Drawing.Size(284, 27);
            this.txtDevisAu.TabIndex = 3;
            this.txtDevisAu.TabStop = false;
            this.txtDevisAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDevisAu.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(308, 0);
            this.label33.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(26, 30);
            this.label33.TabIndex = 100;
            this.label33.Text = "au";
            // 
            // ultraDateTimeEditor3
            // 
            this.ultraDateTimeEditor3.DateTime = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraDateTimeEditor3.Location = new System.Drawing.Point(286, 2);
            this.ultraDateTimeEditor3.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor3.MinimumSize = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor3.Name = "ultraDateTimeEditor3";
            this.ultraDateTimeEditor3.Size = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor3.TabIndex = 101;
            this.ultraDateTimeEditor3.Value = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor3.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor3_AfterCloseUp);
            this.ultraDateTimeEditor3.VisibleChanged += new System.EventHandler(this.ultraDateTimeEditor3_VisibleChanged);
            // 
            // ultraDateTimeEditor4
            // 
            this.ultraDateTimeEditor4.DateTime = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor4.Location = new System.Drawing.Point(622, 2);
            this.ultraDateTimeEditor4.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor4.MinimumSize = new System.Drawing.Size(20, 27);
            this.ultraDateTimeEditor4.Name = "ultraDateTimeEditor4";
            this.ultraDateTimeEditor4.Size = new System.Drawing.Size(20, 24);
            this.ultraDateTimeEditor4.TabIndex = 102;
            this.ultraDateTimeEditor4.Value = new System.DateTime(2020, 7, 23, 0, 0, 0, 0);
            this.ultraDateTimeEditor4.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor4_ValueChanged);
            this.ultraDateTimeEditor4.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor4_AfterCloseUp);
            // 
            // txt1
            // 
            this.txt1.AccAcceptNumbersOnly = false;
            this.txt1.AccAllowComma = false;
            this.txt1.AccBackgroundColor = System.Drawing.Color.White;
            this.txt1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt1.AccHidenValue = "";
            this.txt1.AccNotAllowedChars = null;
            this.txt1.AccReadOnly = false;
            this.txt1.AccReadOnlyAllowDelete = false;
            this.txt1.AccRequired = false;
            this.txt1.BackColor = System.Drawing.Color.White;
            this.txt1.CustomBackColor = System.Drawing.Color.White;
            this.txt1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt1.ForeColor = System.Drawing.Color.Black;
            this.txt1.Location = new System.Drawing.Point(1300, 32);
            this.txt1.Margin = new System.Windows.Forms.Padding(2);
            this.txt1.MaxLength = 32767;
            this.txt1.Multiline = false;
            this.txt1.Name = "txt1";
            this.txt1.ReadOnly = false;
            this.txt1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt1.Size = new System.Drawing.Size(542, 27);
            this.txt1.TabIndex = 5;
            this.txt1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt1.UseSystemPasswordChar = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Dock = System.Windows.Forms.DockStyle.Left;
            this.label55.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label55.Location = new System.Drawing.Point(922, 30);
            this.label55.Margin = new System.Windows.Forms.Padding(2, 0, 22, 0);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(101, 30);
            this.label55.TabIndex = 359;
            this.label55.Text = "Titre du devis";
            this.label55.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label35.Location = new System.Drawing.Point(2, 0);
            this.label35.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(272, 30);
            this.label35.TabIndex = 100;
            this.label35.Text = "Sélection immeuble";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label32.Location = new System.Drawing.Point(2, 60);
            this.label32.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(53, 19);
            this.label32.TabIndex = 7;
            this.label32.Text = "Status";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label29.Location = new System.Drawing.Point(922, 0);
            this.label29.Margin = new System.Windows.Forms.Padding(2, 0, 22, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(354, 30);
            this.label29.TabIndex = 13;
            this.label29.Text = "Type du devis";
            // 
            // cmdDevisArticle
            // 
            appearance346.BackColor = System.Drawing.SystemColors.Window;
            appearance346.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmdDevisArticle.DisplayLayout.Appearance = appearance346;
            this.cmdDevisArticle.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmdDevisArticle.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance347.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance347.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance347.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance347.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdDevisArticle.DisplayLayout.GroupByBox.Appearance = appearance347;
            appearance348.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdDevisArticle.DisplayLayout.GroupByBox.BandLabelAppearance = appearance348;
            this.cmdDevisArticle.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance349.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance349.BackColor2 = System.Drawing.SystemColors.Control;
            appearance349.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance349.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdDevisArticle.DisplayLayout.GroupByBox.PromptAppearance = appearance349;
            this.cmdDevisArticle.DisplayLayout.MaxColScrollRegions = 1;
            this.cmdDevisArticle.DisplayLayout.MaxRowScrollRegions = 1;
            appearance350.BackColor = System.Drawing.SystemColors.Window;
            appearance350.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdDevisArticle.DisplayLayout.Override.ActiveCellAppearance = appearance350;
            appearance351.BackColor = System.Drawing.SystemColors.Highlight;
            appearance351.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmdDevisArticle.DisplayLayout.Override.ActiveRowAppearance = appearance351;
            this.cmdDevisArticle.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmdDevisArticle.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance352.BackColor = System.Drawing.SystemColors.Window;
            this.cmdDevisArticle.DisplayLayout.Override.CardAreaAppearance = appearance352;
            appearance353.BorderColor = System.Drawing.Color.Silver;
            appearance353.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmdDevisArticle.DisplayLayout.Override.CellAppearance = appearance353;
            this.cmdDevisArticle.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmdDevisArticle.DisplayLayout.Override.CellPadding = 0;
            appearance354.BackColor = System.Drawing.SystemColors.Control;
            appearance354.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance354.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance354.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance354.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdDevisArticle.DisplayLayout.Override.GroupByRowAppearance = appearance354;
            appearance355.TextHAlignAsString = "Left";
            this.cmdDevisArticle.DisplayLayout.Override.HeaderAppearance = appearance355;
            this.cmdDevisArticle.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmdDevisArticle.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance356.BackColor = System.Drawing.SystemColors.Window;
            appearance356.BorderColor = System.Drawing.Color.Silver;
            this.cmdDevisArticle.DisplayLayout.Override.RowAppearance = appearance356;
            this.cmdDevisArticle.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance357.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmdDevisArticle.DisplayLayout.Override.TemplateAddRowAppearance = appearance357;
            this.cmdDevisArticle.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmdDevisArticle.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmdDevisArticle.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmdDevisArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDevisArticle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdDevisArticle.Location = new System.Drawing.Point(1300, 3);
            this.cmdDevisArticle.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdDevisArticle.Name = "cmdDevisArticle";
            this.cmdDevisArticle.Size = new System.Drawing.Size(542, 27);
            this.cmdDevisArticle.TabIndex = 4;
            this.cmdDevisArticle.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmdDevisArticle_BeforeDropDown);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label34.Location = new System.Drawing.Point(2, 30);
            this.label34.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(73, 19);
            this.label34.TabIndex = 0;
            this.label34.Text = "Depuis le";
            // 
            // cmbDevisStatus
            // 
            appearance358.BackColor = System.Drawing.SystemColors.Window;
            appearance358.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbDevisStatus.DisplayLayout.Appearance = appearance358;
            this.cmbDevisStatus.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbDevisStatus.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance359.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance359.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance359.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance359.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisStatus.DisplayLayout.GroupByBox.Appearance = appearance359;
            appearance360.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisStatus.DisplayLayout.GroupByBox.BandLabelAppearance = appearance360;
            this.cmbDevisStatus.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance361.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance361.BackColor2 = System.Drawing.SystemColors.Control;
            appearance361.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance361.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDevisStatus.DisplayLayout.GroupByBox.PromptAppearance = appearance361;
            this.cmbDevisStatus.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbDevisStatus.DisplayLayout.MaxRowScrollRegions = 1;
            appearance362.BackColor = System.Drawing.SystemColors.Window;
            appearance362.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbDevisStatus.DisplayLayout.Override.ActiveCellAppearance = appearance362;
            appearance363.BackColor = System.Drawing.SystemColors.Highlight;
            appearance363.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbDevisStatus.DisplayLayout.Override.ActiveRowAppearance = appearance363;
            this.cmbDevisStatus.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbDevisStatus.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance364.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDevisStatus.DisplayLayout.Override.CardAreaAppearance = appearance364;
            appearance365.BorderColor = System.Drawing.Color.Silver;
            appearance365.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbDevisStatus.DisplayLayout.Override.CellAppearance = appearance365;
            this.cmbDevisStatus.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbDevisStatus.DisplayLayout.Override.CellPadding = 0;
            appearance366.BackColor = System.Drawing.SystemColors.Control;
            appearance366.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance366.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance366.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance366.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDevisStatus.DisplayLayout.Override.GroupByRowAppearance = appearance366;
            appearance367.TextHAlignAsString = "Left";
            this.cmbDevisStatus.DisplayLayout.Override.HeaderAppearance = appearance367;
            this.cmbDevisStatus.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbDevisStatus.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance368.BackColor = System.Drawing.SystemColors.Window;
            appearance368.BorderColor = System.Drawing.Color.Silver;
            this.cmbDevisStatus.DisplayLayout.Override.RowAppearance = appearance368;
            this.cmbDevisStatus.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance369.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbDevisStatus.DisplayLayout.Override.TemplateAddRowAppearance = appearance369;
            this.cmbDevisStatus.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbDevisStatus.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbDevisStatus.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbDevisStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDevisStatus.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbDevisStatus.Location = new System.Drawing.Point(278, 63);
            this.cmbDevisStatus.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmbDevisStatus.Name = "cmbDevisStatus";
            this.cmbDevisStatus.Size = new System.Drawing.Size(640, 27);
            this.cmbDevisStatus.TabIndex = 2;
            this.cmbDevisStatus.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbDevisStatus_BeforeDropDown);
            // 
            // ssDevis
            // 
            this.tableLayoutPanel50.SetColumnSpan(this.ssDevis, 4);
            appearance370.BackColor = System.Drawing.SystemColors.Window;
            appearance370.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDevis.DisplayLayout.Appearance = appearance370;
            this.ssDevis.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssDevis.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDevis.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance371.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance371.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance371.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance371.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDevis.DisplayLayout.GroupByBox.Appearance = appearance371;
            appearance372.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDevis.DisplayLayout.GroupByBox.BandLabelAppearance = appearance372;
            this.ssDevis.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance373.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance373.BackColor2 = System.Drawing.SystemColors.Control;
            appearance373.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance373.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDevis.DisplayLayout.GroupByBox.PromptAppearance = appearance373;
            this.ssDevis.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDevis.DisplayLayout.MaxRowScrollRegions = 1;
            appearance374.BackColor = System.Drawing.SystemColors.Window;
            appearance374.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDevis.DisplayLayout.Override.ActiveCellAppearance = appearance374;
            appearance375.BackColor = System.Drawing.SystemColors.Highlight;
            appearance375.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDevis.DisplayLayout.Override.ActiveRowAppearance = appearance375;
            this.ssDevis.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssDevis.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDevis.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance376.BackColor = System.Drawing.SystemColors.Window;
            this.ssDevis.DisplayLayout.Override.CardAreaAppearance = appearance376;
            appearance377.BorderColor = System.Drawing.Color.Silver;
            appearance377.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDevis.DisplayLayout.Override.CellAppearance = appearance377;
            this.ssDevis.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDevis.DisplayLayout.Override.CellPadding = 0;
            appearance378.BackColor = System.Drawing.SystemColors.Control;
            appearance378.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance378.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance378.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance378.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDevis.DisplayLayout.Override.GroupByRowAppearance = appearance378;
            appearance379.TextHAlignAsString = "Left";
            this.ssDevis.DisplayLayout.Override.HeaderAppearance = appearance379;
            this.ssDevis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDevis.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance380.BackColor = System.Drawing.SystemColors.Window;
            appearance380.BorderColor = System.Drawing.Color.Silver;
            this.ssDevis.DisplayLayout.Override.RowAppearance = appearance380;
            this.ssDevis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssDevis.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance381.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDevis.DisplayLayout.Override.TemplateAddRowAppearance = appearance381;
            this.ssDevis.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDevis.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssDevis.Location = new System.Drawing.Point(2, 133);
            this.ssDevis.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssDevis.Name = "ssDevis";
            this.ssDevis.Size = new System.Drawing.Size(1840, 660);
            this.ssDevis.TabIndex = 110;
            this.ssDevis.Text = "ultraGrid1";
            this.ssDevis.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssDevis_InitializeLayout);
            this.ssDevis.AfterRowActivate += new System.EventHandler(this.ssDevis_AfterRowActivate);
            this.ssDevis.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssDevis_DoubleClickRow);
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.groupBox12);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1844, 796);
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.GridCLRE);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.Location = new System.Drawing.Point(0, 0);
            this.groupBox12.Margin = new System.Windows.Forms.Padding(6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Padding = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.groupBox12.Size = new System.Drawing.Size(1844, 796);
            this.groupBox12.TabIndex = 21;
            this.groupBox12.TabStop = false;
            // 
            // GridCLRE
            // 
            appearance382.BackColor = System.Drawing.SystemColors.Window;
            appearance382.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridCLRE.DisplayLayout.Appearance = appearance382;
            this.GridCLRE.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridCLRE.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridCLRE.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance383.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance383.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance383.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance383.BorderColor = System.Drawing.SystemColors.Window;
            this.GridCLRE.DisplayLayout.GroupByBox.Appearance = appearance383;
            appearance384.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridCLRE.DisplayLayout.GroupByBox.BandLabelAppearance = appearance384;
            this.GridCLRE.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance385.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance385.BackColor2 = System.Drawing.SystemColors.Control;
            appearance385.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance385.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridCLRE.DisplayLayout.GroupByBox.PromptAppearance = appearance385;
            this.GridCLRE.DisplayLayout.MaxColScrollRegions = 1;
            this.GridCLRE.DisplayLayout.MaxRowScrollRegions = 1;
            appearance386.BackColor = System.Drawing.SystemColors.Window;
            appearance386.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridCLRE.DisplayLayout.Override.ActiveCellAppearance = appearance386;
            appearance387.BackColor = System.Drawing.SystemColors.Highlight;
            appearance387.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridCLRE.DisplayLayout.Override.ActiveRowAppearance = appearance387;
            this.GridCLRE.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridCLRE.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridCLRE.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridCLRE.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridCLRE.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance388.BackColor = System.Drawing.SystemColors.Window;
            this.GridCLRE.DisplayLayout.Override.CardAreaAppearance = appearance388;
            appearance389.BorderColor = System.Drawing.Color.Silver;
            appearance389.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridCLRE.DisplayLayout.Override.CellAppearance = appearance389;
            this.GridCLRE.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridCLRE.DisplayLayout.Override.CellPadding = 0;
            appearance390.BackColor = System.Drawing.SystemColors.Control;
            appearance390.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance390.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance390.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance390.BorderColor = System.Drawing.SystemColors.Window;
            this.GridCLRE.DisplayLayout.Override.GroupByRowAppearance = appearance390;
            appearance391.TextHAlignAsString = "Left";
            this.GridCLRE.DisplayLayout.Override.HeaderAppearance = appearance391;
            this.GridCLRE.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridCLRE.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance392.BackColor = System.Drawing.SystemColors.Window;
            appearance392.BorderColor = System.Drawing.Color.Silver;
            this.GridCLRE.DisplayLayout.Override.RowAppearance = appearance392;
            this.GridCLRE.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridCLRE.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance393.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridCLRE.DisplayLayout.Override.TemplateAddRowAppearance = appearance393;
            this.GridCLRE.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridCLRE.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridCLRE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCLRE.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridCLRE.Location = new System.Drawing.Point(2, 18);
            this.GridCLRE.Margin = new System.Windows.Forms.Padding(6);
            this.GridCLRE.Name = "GridCLRE";
            this.GridCLRE.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.GridCLRE.Size = new System.Drawing.Size(1840, 775);
            this.GridCLRE.TabIndex = 1;
            this.GridCLRE.Text = "ultraGrid1";
            this.GridCLRE.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridCLRE_InitializeLayout);
            this.GridCLRE.AfterRowsDeleted += new System.EventHandler(this.GridCLRE_AfterRowsDeleted);
            this.GridCLRE.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridCLRE_AfterRowUpdate);
            this.GridCLRE.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridCLRE_BeforeExitEditMode);
            this.GridCLRE.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridCLRE_BeforeRowsDeleted);
            this.GridCLRE.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.GridCLRE_Error);
            // 
            // ssDropCodeEtat
            // 
            appearance394.BackColor = System.Drawing.SystemColors.Window;
            appearance394.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropCodeEtat.DisplayLayout.Appearance = appearance394;
            this.ssDropCodeEtat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropCodeEtat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance395.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance395.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance395.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance395.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.Appearance = appearance395;
            appearance396.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance396;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance397.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance397.BackColor2 = System.Drawing.SystemColors.Control;
            appearance397.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance397.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.PromptAppearance = appearance397;
            this.ssDropCodeEtat.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropCodeEtat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance398.BackColor = System.Drawing.SystemColors.Window;
            appearance398.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropCodeEtat.DisplayLayout.Override.ActiveCellAppearance = appearance398;
            appearance399.BackColor = System.Drawing.SystemColors.Highlight;
            appearance399.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropCodeEtat.DisplayLayout.Override.ActiveRowAppearance = appearance399;
            this.ssDropCodeEtat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropCodeEtat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance400.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.Override.CardAreaAppearance = appearance400;
            appearance401.BorderColor = System.Drawing.Color.Silver;
            appearance401.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropCodeEtat.DisplayLayout.Override.CellAppearance = appearance401;
            this.ssDropCodeEtat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropCodeEtat.DisplayLayout.Override.CellPadding = 0;
            appearance402.BackColor = System.Drawing.SystemColors.Control;
            appearance402.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance402.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance402.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance402.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.Override.GroupByRowAppearance = appearance402;
            appearance403.TextHAlignAsString = "Left";
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderAppearance = appearance403;
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance404.BackColor = System.Drawing.SystemColors.Window;
            appearance404.BorderColor = System.Drawing.Color.Silver;
            this.ssDropCodeEtat.DisplayLayout.Override.RowAppearance = appearance404;
            this.ssDropCodeEtat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance405.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropCodeEtat.DisplayLayout.Override.TemplateAddRowAppearance = appearance405;
            this.ssDropCodeEtat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropCodeEtat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropCodeEtat.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropCodeEtat.Location = new System.Drawing.Point(1423, 62);
            this.ssDropCodeEtat.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssDropCodeEtat.Name = "ssDropCodeEtat";
            this.ssDropCodeEtat.Size = new System.Drawing.Size(100, 22);
            this.ssDropCodeEtat.TabIndex = 22;
            this.ssDropCodeEtat.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdEditer);
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdSupprimer);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1465, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(385, 43);
            this.flowLayoutPanel1.TabIndex = 13;
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdEditer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.CmdEditer.Location = new System.Drawing.Point(323, 2);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(60, 35);
            this.CmdEditer.TabIndex = 368;
            this.toolTip1.SetToolTip(this.CmdEditer, "Imprimer");
            this.CmdEditer.UseVisualStyleBackColor = false;
            this.CmdEditer.Click += new System.EventHandler(this.CmdEditer_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(259, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 1;
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(195, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 349;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(131, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 350;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(67, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 351;
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(3, 2);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 364;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSupprimer, "Supprimer");
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            this.cmdSupprimer.Click += new System.EventHandler(this.cmdSupprimer_Click);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Controls.Add(this.ultraTabPageControl4);
            this.SSTab1.Controls.Add(this.ultraTabPageControl5);
            this.SSTab1.Controls.Add(this.ultraTabPageControl6);
            this.SSTab1.Controls.Add(this.ultraTabPageControl7);
            this.SSTab1.Controls.Add(this.ultraTabPageControl8);
            this.SSTab1.Controls.Add(this.ultraTabPageControl9);
            this.SSTab1.Controls.Add(this.ultraTabPageControl10);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.SSTab1.Location = new System.Drawing.Point(2, 136);
            this.SSTab1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1846, 818);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 348;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "Identification";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Correspondants";
            ultraTab3.TabPage = this.ultraTabPageControl4;
            ultraTab3.Text = "Administratif";
            ultraTab10.TabPage = this.ultraTabPageControl5;
            ultraTab10.Text = "Finances";
            ultraTab4.TabPage = this.ultraTabPageControl10;
            ultraTab4.Text = "Commercial";
            ultraTab12.TabPage = this.ultraTabPageControl7;
            ultraTab12.Text = "Documentation";
            ultraTab14.TabPage = this.ultraTabPageControl8;
            ultraTab14.Text = "Intervention";
            ultraTab15.TabPage = this.ultraTabPageControl9;
            ultraTab15.Text = "Devis";
            ultraTab11.TabPage = this.ultraTabPageControl6;
            ultraTab11.Text = "Suivi Client";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13,
            ultraTab2,
            ultraTab3,
            ultraTab10,
            ultraTab4,
            ultraTab12,
            ultraTab14,
            ultraTab15,
            ultraTab11});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.SSTab1.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SSTab1_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1844, 796);
            // 
            // label2s
            // 
            this.label2s.AutoSize = true;
            this.label2s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2s.Location = new System.Drawing.Point(2, 30);
            this.label2s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2s.Name = "label2s";
            this.label2s.Size = new System.Drawing.Size(97, 19);
            this.label2s.TabIndex = 0;
            this.label2s.Text = "Code Gerant";
            // 
            // label3s
            // 
            this.label3s.AutoSize = true;
            this.label3s.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3s.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3s.Location = new System.Drawing.Point(2, 60);
            this.label3s.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3s.Name = "label3s";
            this.label3s.Size = new System.Drawing.Size(121, 30);
            this.label3s.TabIndex = 2;
            this.label3s.Text = "Raison sociale";
            // 
            // cmd0
            // 
            this.cmd0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd0.FlatAppearance.BorderSize = 0;
            this.cmd0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd0.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmd0.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmd0.Location = new System.Drawing.Point(697, 3);
            this.cmd0.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmd0.Name = "cmd0";
            this.cmd0.Size = new System.Drawing.Size(25, 19);
            this.cmd0.TabIndex = 53;
            this.cmd0.Tag = "0";
            this.toolTip1.SetToolTip(this.cmd0, "Recherche du gérant par son code");
            this.cmd0.UseVisualStyleBackColor = false;
            this.cmd0.Click += new System.EventHandler(this.cmd0_Click);
            // 
            // cmdRechercheClient
            // 
            this.cmdRechercheClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheClient.FlatAppearance.BorderSize = 0;
            this.cmdRechercheClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheClient.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheClient.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheClient.Location = new System.Drawing.Point(697, 33);
            this.cmdRechercheClient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdRechercheClient.Name = "cmdRechercheClient";
            this.cmdRechercheClient.Size = new System.Drawing.Size(25, 19);
            this.cmdRechercheClient.TabIndex = 8;
            this.toolTip1.SetToolTip(this.cmdRechercheClient, "Recherche du gérant par son code");
            this.cmdRechercheClient.UseVisualStyleBackColor = false;
            this.cmdRechercheClient.Click += new System.EventHandler(this.cmdRechercheClient_Click);
            // 
            // cmdRechercheRaisonSociale
            // 
            this.cmdRechercheRaisonSociale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheRaisonSociale.FlatAppearance.BorderSize = 0;
            this.cmdRechercheRaisonSociale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheRaisonSociale.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheRaisonSociale.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheRaisonSociale.Location = new System.Drawing.Point(697, 63);
            this.cmdRechercheRaisonSociale.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.cmdRechercheRaisonSociale.Name = "cmdRechercheRaisonSociale";
            this.cmdRechercheRaisonSociale.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheRaisonSociale.TabIndex = 9;
            this.toolTip1.SetToolTip(this.cmdRechercheRaisonSociale, "Recherche du gérant par sa raison social");
            this.cmdRechercheRaisonSociale.UseVisualStyleBackColor = false;
            this.cmdRechercheRaisonSociale.Click += new System.EventHandler(this.cmdRechercheRaisonSociale_Click);
            // 
            // Label5744
            // 
            this.Label5744.AutoSize = true;
            this.Label5744.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Underline);
            this.Label5744.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.Label5744.Location = new System.Drawing.Point(2, 0);
            this.Label5744.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label5744.Name = "Label5744";
            this.Label5744.Size = new System.Drawing.Size(61, 19);
            this.Label5744.TabIndex = 351;
            this.Label5744.Tag = "44";
            this.Label5744.Text = "Groupe";
            this.Label5744.Click += new System.EventHandler(this.Label5744_Click);
            // 
            // Label43
            // 
            this.Label43.AutoSize = true;
            this.Label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Label43.Location = new System.Drawing.Point(509, 58);
            this.Label43.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(104, 16);
            this.Label43.TabIndex = 356;
            this.Label43.Text = "Référence Long";
            this.Label43.Visible = false;
            // 
            // OptEt
            // 
            this.OptEt.AutoSize = true;
            this.OptEt.Location = new System.Drawing.Point(267, 13);
            this.OptEt.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptEt.Name = "OptEt";
            this.OptEt.Size = new System.Drawing.Size(35, 17);
            this.OptEt.TabIndex = 358;
            this.OptEt.Text = "Et";
            this.OptEt.UseVisualStyleBackColor = true;
            this.OptEt.Visible = false;
            // 
            // OptOu
            // 
            this.OptOu.AutoSize = true;
            this.OptOu.Location = new System.Drawing.Point(320, 10);
            this.OptOu.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.OptOu.Name = "OptOu";
            this.OptOu.Size = new System.Drawing.Size(39, 17);
            this.OptOu.TabIndex = 359;
            this.OptOu.Text = "Ou";
            this.OptOu.UseVisualStyleBackColor = true;
            this.OptOu.Visible = false;
            // 
            // Label4190
            // 
            this.Label4190.AutoSize = true;
            this.Label4190.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Label4190.Location = new System.Drawing.Point(8, 0);
            this.Label4190.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label4190.Name = "Label4190";
            this.Label4190.Size = new System.Drawing.Size(116, 16);
            this.Label4190.TabIndex = 360;
            this.Label4190.Text = "Recherche rapide";
            this.Label4190.Visible = false;
            // 
            // ssFindClient
            // 
            appearance406.BackColor = System.Drawing.SystemColors.Window;
            appearance406.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssFindClient.DisplayLayout.Appearance = appearance406;
            this.ssFindClient.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssFindClient.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance407.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance407.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance407.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance407.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFindClient.DisplayLayout.GroupByBox.Appearance = appearance407;
            appearance408.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFindClient.DisplayLayout.GroupByBox.BandLabelAppearance = appearance408;
            this.ssFindClient.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance409.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance409.BackColor2 = System.Drawing.SystemColors.Control;
            appearance409.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance409.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFindClient.DisplayLayout.GroupByBox.PromptAppearance = appearance409;
            this.ssFindClient.DisplayLayout.MaxColScrollRegions = 1;
            this.ssFindClient.DisplayLayout.MaxRowScrollRegions = 1;
            appearance410.BackColor = System.Drawing.SystemColors.Window;
            appearance410.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssFindClient.DisplayLayout.Override.ActiveCellAppearance = appearance410;
            appearance411.BackColor = System.Drawing.SystemColors.Highlight;
            appearance411.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssFindClient.DisplayLayout.Override.ActiveRowAppearance = appearance411;
            this.ssFindClient.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssFindClient.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance412.BackColor = System.Drawing.SystemColors.Window;
            this.ssFindClient.DisplayLayout.Override.CardAreaAppearance = appearance412;
            appearance413.BorderColor = System.Drawing.Color.Silver;
            appearance413.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssFindClient.DisplayLayout.Override.CellAppearance = appearance413;
            this.ssFindClient.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssFindClient.DisplayLayout.Override.CellPadding = 0;
            appearance414.BackColor = System.Drawing.SystemColors.Control;
            appearance414.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance414.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance414.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance414.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFindClient.DisplayLayout.Override.GroupByRowAppearance = appearance414;
            appearance415.TextHAlignAsString = "Left";
            this.ssFindClient.DisplayLayout.Override.HeaderAppearance = appearance415;
            this.ssFindClient.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssFindClient.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance416.BackColor = System.Drawing.SystemColors.Window;
            appearance416.BorderColor = System.Drawing.Color.Silver;
            this.ssFindClient.DisplayLayout.Override.RowAppearance = appearance416;
            this.ssFindClient.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance417.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssFindClient.DisplayLayout.Override.TemplateAddRowAppearance = appearance417;
            this.ssFindClient.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssFindClient.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssFindClient.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssFindClient.Location = new System.Drawing.Point(184, -3);
            this.ssFindClient.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.ssFindClient.Name = "ssFindClient";
            this.ssFindClient.Size = new System.Drawing.Size(45, 22);
            this.ssFindClient.TabIndex = 361;
            this.ssFindClient.Visible = false;
            this.ssFindClient.AfterCloseUp += new System.EventHandler(this.ssFindClient_AfterCloseUp);
            this.ssFindClient.TextChanged += new System.EventHandler(this.ssFindClient_TextChanged);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 125F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.09455F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.90544F));
            this.tableLayoutPanel1.Controls.Add(this.Label5744, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txt0, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmd0, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2s, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCode1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdRechercheClient, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmdRechercheRaisonSociale, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNom, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label3s, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 43);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 90);
            this.tableLayoutPanel1.TabIndex = 416;
            // 
            // txt0
            // 
            this.txt0.AccAcceptNumbersOnly = false;
            this.txt0.AccAllowComma = false;
            this.txt0.AccBackgroundColor = System.Drawing.Color.White;
            this.txt0.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt0.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt0.AccHidenValue = "";
            this.txt0.AccNotAllowedChars = null;
            this.txt0.AccReadOnly = false;
            this.txt0.AccReadOnlyAllowDelete = false;
            this.txt0.AccRequired = false;
            this.txt0.BackColor = System.Drawing.Color.White;
            this.txt0.CustomBackColor = System.Drawing.Color.White;
            this.txt0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt0.ForeColor = System.Drawing.Color.Black;
            this.txt0.Location = new System.Drawing.Point(127, 2);
            this.txt0.Margin = new System.Windows.Forms.Padding(2);
            this.txt0.MaxLength = 50;
            this.txt0.Multiline = false;
            this.txt0.Name = "txt0";
            this.txt0.ReadOnly = false;
            this.txt0.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt0.Size = new System.Drawing.Size(566, 27);
            this.txt0.TabIndex = 352;
            this.txt0.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt0.UseSystemPasswordChar = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(127, 32);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 50;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = false;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(566, 27);
            this.txtCode1.TabIndex = 353;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            this.txtCode1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCode1_KeyPress);
            this.txtCode1.Validating += new System.ComponentModel.CancelEventHandler(this.txtCode1_Validating);
            // 
            // txtNom
            // 
            this.txtNom.AccAcceptNumbersOnly = false;
            this.txtNom.AccAllowComma = false;
            this.txtNom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNom.AccHidenValue = "";
            this.txtNom.AccNotAllowedChars = null;
            this.txtNom.AccReadOnly = false;
            this.txtNom.AccReadOnlyAllowDelete = false;
            this.txtNom.AccRequired = false;
            this.txtNom.BackColor = System.Drawing.Color.White;
            this.txtNom.CustomBackColor = System.Drawing.Color.White;
            this.txtNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNom.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNom.ForeColor = System.Drawing.Color.Black;
            this.txtNom.Location = new System.Drawing.Point(127, 62);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.MaxLength = 50;
            this.txtNom.Multiline = false;
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = false;
            this.txtNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNom.Size = new System.Drawing.Size(566, 27);
            this.txtNom.TabIndex = 354;
            this.txtNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNom.UseSystemPasswordChar = false;
            this.txtNom.TextChanged += new System.EventHandler(this.txtNom_TextChanged);
            this.txtNom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNom_KeyPress);
            // 
            // tableLayoutPanel36
            // 
            this.tableLayoutPanel36.ColumnCount = 1;
            this.tableLayoutPanel36.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel36.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel36.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel36.Controls.Add(this.SSTab1, 0, 2);
            this.tableLayoutPanel36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel36.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel36.Name = "tableLayoutPanel36";
            this.tableLayoutPanel36.RowCount = 3;
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel36.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel36.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel36.TabIndex = 417;
            // 
            // txtCode1temp
            // 
            this.txtCode1temp.AccAcceptNumbersOnly = false;
            this.txtCode1temp.AccAllowComma = false;
            this.txtCode1temp.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1temp.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1temp.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1temp.AccHidenValue = "";
            this.txtCode1temp.AccNotAllowedChars = null;
            this.txtCode1temp.AccReadOnly = false;
            this.txtCode1temp.AccReadOnlyAllowDelete = false;
            this.txtCode1temp.AccRequired = false;
            this.txtCode1temp.BackColor = System.Drawing.Color.White;
            this.txtCode1temp.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1temp.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCode1temp.ForeColor = System.Drawing.Color.Black;
            this.txtCode1temp.Location = new System.Drawing.Point(381, 46);
            this.txtCode1temp.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1temp.MaxLength = 32767;
            this.txtCode1temp.Multiline = false;
            this.txtCode1temp.Name = "txtCode1temp";
            this.txtCode1temp.ReadOnly = false;
            this.txtCode1temp.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1temp.Size = new System.Drawing.Size(150, 27);
            this.txtCode1temp.TabIndex = 357;
            this.txtCode1temp.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1temp.UseSystemPasswordChar = false;
            this.txtCode1temp.Visible = false;
            this.txtCode1temp.TextChanged += new System.EventHandler(this.txtCode1temp_TextChanged);
            // 
            // txt3
            // 
            this.txt3.AccAcceptNumbersOnly = false;
            this.txt3.AccAllowComma = false;
            this.txt3.AccBackgroundColor = System.Drawing.Color.White;
            this.txt3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt3.AccHidenValue = "";
            this.txt3.AccNotAllowedChars = null;
            this.txt3.AccReadOnly = false;
            this.txt3.AccReadOnlyAllowDelete = false;
            this.txt3.AccRequired = false;
            this.txt3.BackColor = System.Drawing.Color.White;
            this.txt3.CustomBackColor = System.Drawing.Color.White;
            this.txt3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txt3.ForeColor = System.Drawing.Color.Black;
            this.txt3.Location = new System.Drawing.Point(614, 60);
            this.txt3.Margin = new System.Windows.Forms.Padding(2);
            this.txt3.MaxLength = 32767;
            this.txt3.Multiline = false;
            this.txt3.Name = "txt3";
            this.txt3.ReadOnly = false;
            this.txt3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt3.Size = new System.Drawing.Size(162, 27);
            this.txt3.TabIndex = 355;
            this.txt3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt3.UseSystemPasswordChar = false;
            this.txt3.Visible = false;
            // 
            // txtSelCode1
            // 
            this.txtSelCode1.AccAcceptNumbersOnly = false;
            this.txtSelCode1.AccAllowComma = false;
            this.txtSelCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSelCode1.AccHidenValue = "";
            this.txtSelCode1.AccNotAllowedChars = null;
            this.txtSelCode1.AccReadOnly = false;
            this.txtSelCode1.AccReadOnlyAllowDelete = false;
            this.txtSelCode1.AccRequired = false;
            this.txtSelCode1.BackColor = System.Drawing.Color.White;
            this.txtSelCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtSelCode1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSelCode1.ForeColor = System.Drawing.Color.Black;
            this.txtSelCode1.Location = new System.Drawing.Point(381, 62);
            this.txtSelCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelCode1.MaxLength = 32767;
            this.txtSelCode1.Multiline = false;
            this.txtSelCode1.Name = "txtSelCode1";
            this.txtSelCode1.ReadOnly = false;
            this.txtSelCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelCode1.Size = new System.Drawing.Size(71, 27);
            this.txtSelCode1.TabIndex = 354;
            this.txtSelCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelCode1.UseSystemPasswordChar = false;
            this.txtSelCode1.Visible = false;
            // 
            // UserDocClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel36);
            this.Controls.Add(this.ssDropCodeEtat);
            this.Controls.Add(this.ssFindClient);
            this.Controls.Add(this.Label4190);
            this.Controls.Add(this.OptOu);
            this.Controls.Add(this.OptEt);
            this.Controls.Add(this.txtCode1temp);
            this.Controls.Add(this.Label43);
            this.Controls.Add(this.txt3);
            this.Controls.Add(this.txtSelCode1);
            this.Margin = new System.Windows.Forms.Padding(2, 3, 2, 3);
            this.Name = "UserDocClient";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche Gerant";
            this.Load += new System.EventHandler(this.UserDocClient_Load);
            this.SizeChanged += new System.EventHandler(this.UserDocClient_SizeChanged);
            this.VisibleChanged += new System.EventHandler(this.UserDocClient_VisibleChanged);
            this.ultraTabPageControl14.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.panel14.ResumeLayout(false);
            this.panel15.ResumeLayout(false);
            this.panel16.ResumeLayout(false);
            this.panel17.ResumeLayout(false);
            this.tableLayoutPanel41.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboComptes)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.Frame719.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbExercices)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSGridSituation)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tableLayoutPanel52.ResumeLayout(false);
            this.tableLayoutPanel52.PerformLayout();
            this.Frame327.ResumeLayout(false);
            this.tableLayoutPanel34.ResumeLayout(false);
            this.tableLayoutPanel34.PerformLayout();
            this.tableLayoutPanel55.ResumeLayout(false);
            this.tableLayoutPanel55.PerformLayout();
            this.tableLayoutPanel56.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParticulier1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtParticulier2)).EndInit();
            this.tableLayoutPanel54.ResumeLayout(false);
            this.tableLayoutPanel54.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercialImm)).EndInit();
            this.tableLayoutPanel53.ResumeLayout(false);
            this.tableLayoutPanel53.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCombo14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercial)).EndInit();
            this.tableLayoutPanel29.ResumeLayout(false);
            this.frmResultat.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tableLayoutPanel35.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleGridImmeuble)).EndInit();
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.Frame70.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.Frame70s.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCmbCommercial)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSINTERVENANT)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ComboQualifGest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridGestionnaire)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbModeTrans_TAB)).EndInit();
            this.ultraTabPageControl5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSTab2)).EndInit();
            this.SSTab2.ResumeLayout(false);
            this.ultraTabPageControl10.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel31.PerformLayout();
            this.tableLayoutPanel40.ResumeLayout(false);
            this.tableLayoutPanel40.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergieA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergieDe)).EndInit();
            this.tableLayoutPanel48.ResumeLayout(false);
            this.tableLayoutPanel48.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.tableLayoutPanel33.ResumeLayout(false);
            this.tableLayoutPanel33.PerformLayout();
            this.tableLayoutPanel47.ResumeLayout(false);
            this.tableLayoutPanel47.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.tableLayoutPanel32.ResumeLayout(false);
            this.tableLayoutPanel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypologie)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCoteAmour)).EndInit();
            this.frmFactures.ResumeLayout(false);
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FrameFioul)).EndInit();
            this.FrameFioul.ResumeLayout(false);
            this.tableLayoutPanel37.ResumeLayout(false);
            this.tableLayoutPanel37.PerformLayout();
            this.ultraTabPageControl7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel49.ResumeLayout(false);
            this.contextMenu.ResumeLayout(false);
            this.tableLayoutPanel38.ResumeLayout(false);
            this.ultraTabPageControl8.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel42.ResumeLayout(false);
            this.tableLayoutPanel42.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbArticle)).EndInit();
            this.tableLayoutPanel43.ResumeLayout(false);
            this.tableLayoutPanel43.PerformLayout();
            this.tableLayoutPanel44.ResumeLayout(false);
            this.tableLayoutPanel44.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).EndInit();
            this.tableLayoutPanel45.ResumeLayout(false);
            this.tableLayoutPanel45.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).EndInit();
            this.tableLayoutPanel46.ResumeLayout(false);
            this.tableLayoutPanel46.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssIntervention)).EndInit();
            this.ultraTabPageControl9.ResumeLayout(false);
            this.tableLayoutPanel50.ResumeLayout(false);
            this.tableLayoutPanel50.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisIntervenant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisImmeuble)).EndInit();
            this.tableLayoutPanel51.ResumeLayout(false);
            this.tableLayoutPanel51.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDevisArticle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDevisStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDevis)).EndInit();
            this.ultraTabPageControl6.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCLRE)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropCodeEtat)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssFindClient)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel36.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label16s;
        private iTalk_TextBox_Small2 txtEmail;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12s;
        private iTalk_TextBox_Small2 txtFax;
        private System.Windows.Forms.Label label13s;
        private iTalk_TextBox_Small2 txtTelephone;
        private iTalk_TextBox_Small2 txtVille;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private iTalk_TextBox_Small2 txtCodePostal;
        private iTalk_TextBox_Small2 txtAdresse2;
        private System.Windows.Forms.Label label9;
        private iTalk_TextBox_Small2 txtAdresse1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridGestionnaire;
        private iTalk_TextBox_Small2 txtCodeReglement;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbModeTrans_TAB;
        private iTalk_TextBox_Small2 Text2;
        private System.Windows.Forms.GroupBox groupBox12;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridCLRE;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssIntervention;
        private System.Windows.Forms.Label label22;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbCodeimmeuble;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private iTalk_TextBox_Small2 txtIntereAu;
        private iTalk_TextBox_Small2 txtinterDe;
        private System.Windows.Forms.Label label27;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbArticle;
        private System.Windows.Forms.Label label26;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenant;
        private System.Windows.Forms.Label label25s;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbStatus;
        private System.Windows.Forms.Label label28;
        private iTalk_TextBox_Small2 txt4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssDevis;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label31;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbDevisIntervenant;
        private System.Windows.Forms.Label label32;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbDevisStatus;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private iTalk_TextBox_Small2 txtDevisAu;
        private iTalk_TextBox_Small2 txtDevisDe;
        private System.Windows.Forms.Label label35;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbDevisImmeuble;
        private System.Windows.Forms.TreeView DirDocClient;
        private System.Windows.Forms.ListBox FilDocClient;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl9;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button CmdSauver;
        private System.Windows.Forms.Label label2s;
        private iTalk_TextBox_Small2 txtCode1;
        private System.Windows.Forms.Label label3s;
        private iTalk_TextBox_Small2 txtNom;
        public System.Windows.Forms.Button cmdRechercheClient;
        public System.Windows.Forms.Button cmdRechercheRaisonSociale;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Button cmbDevis;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button CmdEditer;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab2;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl14;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        public System.Windows.Forms.Button cmdEcritures;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbExercices;
        private System.Windows.Forms.RadioButton optToutesEcritures;
        private System.Windows.Forms.RadioButton optEcrituresNonLetrees;
        private Infragistics.Win.UltraWinGrid.UltraCombo ComboComptes;
        private System.Windows.Forms.RadioButton optCptImmeuble;
        private System.Windows.Forms.RadioButton optCptGerant;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSGridSituation;
        private System.Windows.Forms.Label txtTotalRegle;
        private System.Windows.Forms.Label Label2;
        private System.Windows.Forms.Label txtTotalFacture;
        private System.Windows.Forms.Label Label0;
        private System.Windows.Forms.Label txtSoldeCompte;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.Label Label5726;
        private System.Windows.Forms.RadioButton OptSyndic;
        private iTalk_TextBox_Small2 txtEcheance2;
        private iTalk_TextBox_Small2 txtArretDate;
        private System.Windows.Forms.Label Label5756;
        private System.Windows.Forms.Label Label5754;
        private System.Windows.Forms.RadioButton OptParticulier;
        private System.Windows.Forms.Label Label5730;
        private System.Windows.Forms.Label Label5727;
        private System.Windows.Forms.RadioButton OptImmeuble;
        public System.Windows.Forms.Button cmdSsyndic;
        private System.Windows.Forms.Label Label5758;
        private System.Windows.Forms.Label Label5729;
        private System.Windows.Forms.Label label52;
        private iTalk_TextBox_Small2 txtMontantMoins;
        private System.Windows.Forms.Label label51;
        private iTalk_TextBox_Small2 txtMontantPlus;
        private System.Windows.Forms.Label label50;
        private iTalk_TextBox_Small2 txtMontantTotal;
        private System.Windows.Forms.Label label49;
        private iTalk_TextBox_Small2 txtNbComptes;
        private System.Windows.Forms.Label Label19;
        private iTalk_TextBox_Small2 txtEcrituresNonLettrees;
        public System.Windows.Forms.Button Command1;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssDropCodeEtat;
        private System.Windows.Forms.Label lblLibStatus;
        private iTalk_TextBox_Small2 txt100;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtSyndic1;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtImmeuble1;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtParticulier1;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtSyndic2;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtImmeuble2;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtParticulier2;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtCommercial;
        private System.Windows.Forms.CheckBox chkAdresseModifiee;
        private System.Windows.Forms.Label Label4187;
        private System.Windows.Forms.ImageList imageList1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox chkEditRel_TAB;
        private System.Windows.Forms.CheckBox chkOSOblig_TAB;
        private System.Windows.Forms.CheckBox chkTransmissionBI_TAB;
        public System.Windows.Forms.Button cmd0;
        private System.Windows.Forms.Label Label5744;
        private iTalk_TextBox_Small2 txt0;
        public System.Windows.Forms.Button cmdChangeAdresse;
        public System.Windows.Forms.Button cmdLogimatique;
        private System.Windows.Forms.Label label25;
        private iTalk_TextBox_Small2 txtMotPasse;
        private System.Windows.Forms.Label label43s;
        private iTalk_TextBox_Small2 txtLogin;
        private System.Windows.Forms.CheckBox chkSyndic1;
        private System.Windows.Forms.CheckBox chkSyndic2;
        private System.Windows.Forms.Label label7;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSOleGridImmeuble;
        private System.Windows.Forms.CheckBox chkSyndic3;
        private System.Windows.Forms.CheckBox chkSyndic0;
        private iTalk_TextBox_Small2 txtObservations;
        private iTalk_TextBox_Small2 Label5740;
        private iTalk_TextBox_Small2 Label5742;
        private Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBCmbCommercial;
        public System.Windows.Forms.Button cmdChangeCommercial;
        private iTalk_TextBox_Small2 lblLibCodeReglement;
        private iTalk_TextBox_Small2 txt5;
        public System.Windows.Forms.Button CmdRechercheReglement;
        private System.Windows.Forms.CheckBox Check6;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssCombo20;
        private System.Windows.Forms.Label Label57200;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtCommercialImm;
        private System.Windows.Forms.Label Label5725;
        public System.Windows.Forms.Button cmdImprimer;
        public System.Windows.Forms.Button cmdSPart;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssCombo19;
        private System.Windows.Forms.Label Label4199;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssCombo14;
        private System.Windows.Forms.Label Label4198;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl10;
        private System.Windows.Forms.Label Label3516;
        private System.Windows.Forms.Label Label357;
        private System.Windows.Forms.Label Label356;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label lblEcTrav;
        private System.Windows.Forms.Label lblNTrav;
        private System.Windows.Forms.Label lblN1Trav;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.Label lblEcCont;
        private System.Windows.Forms.Label lblNCont;
        private System.Windows.Forms.Label lblN1Cont;
        private System.Windows.Forms.CheckBox chkListeRouge;
        private System.Windows.Forms.CheckBox chkPrioritaire;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbTypologie;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbCoteAmour;
        public System.Windows.Forms.Button cmdTypologie;
        public System.Windows.Forms.Button cmdSelectGerant;
        public System.Windows.Forms.Button cmdComGerant1;
        public System.Windows.Forms.Button cmdComGerant0;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.RadioButton optSelectCom0;
        public System.Windows.Forms.Button cmdComPrint;
        private iTalk_TextBox_Small2 txtReqEnergie;
        public System.Windows.Forms.Button cmdVisu2;
        public System.Windows.Forms.Button cmdSelectEnergie;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbEnergieA;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbEnergieDe;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label75;
        private iTalk_TextBox_Small2 txtReqComCommercial;
        public System.Windows.Forms.Button cmdVisu1;
        public System.Windows.Forms.Button cmdSelectCommerciaux;
        public System.Windows.Forms.Button cmdComCommercial1;
        public System.Windows.Forms.Button cmdComCommercial0;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.RadioButton optSelectCom1;
        private iTalk_TextBox_Small2 txtReqComGerant;
        public System.Windows.Forms.Button cmdVisu0;
        private System.Windows.Forms.Label Label4188;
        private iTalk_TextBox_Small2 txt101;
        private System.Windows.Forms.Label label55;
        private iTalk_TextBox_Small2 txt1;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmdDevisArticle;
        private System.Windows.Forms.Label lblTypologie;
        private iTalk_TextBox_Small2 txtSelCode1;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSINTERVENANT;
        private iTalk_TextBox_Small2 txt3;
        private System.Windows.Forms.Label Label43;
        private Infragistics.Win.UltraWinGrid.UltraCombo ComboQualifGest;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssDropMatricule;
        private System.Windows.Forms.RadioButton optRelEcritureNonLettree;
        private System.Windows.Forms.RadioButton optRelEcritureLettree;
        private iTalk_TextBox_Small2 txtCode1temp;
        private System.Windows.Forms.RadioButton OptEt;
        private System.Windows.Forms.RadioButton OptOu;
        private System.Windows.Forms.Label Label4190;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssFindClient;
        private iTalk_TextBox_Small2 txtComA;
        private iTalk_TextBox_Small2 txtGerantA;
        private iTalk_TextBox_Small2 txtComDe;
        private iTalk_TextBox_Small2 txtGerantDe;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox Frame70s;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.GroupBox Frame70;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.GroupBox Frame719;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox frmResultat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        private System.Windows.Forms.TableLayoutPanel Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel42;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel43;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel44;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel45;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel46;
        private System.Windows.Forms.Label lbllibIntervenant;
        private System.Windows.Forms.Label lblLibModeTrans_TAB;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel32;
        private System.Windows.Forms.GroupBox frmFactures;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel49;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel50;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel51;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel41;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel52;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel53;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel54;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel55;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel56;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel33;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel40;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel47;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel48;
        public System.Windows.Forms.Button cmdImpressionImm;
        private iTalk_TextBox_Small2 txtTotalComm;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label36;
        private iTalk_TextBox_Small2 Text1;
        private iTalk_TextBox_Small2 Text3;
        public System.Windows.Forms.GroupBox Frame327;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel34;
        public System.Windows.Forms.RadioButton opt1;
        public System.Windows.Forms.RadioButton Opt2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel35;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor3;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor ultraDateTimeEditor4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel36;
        private Infragistics.Win.Misc.UltraGroupBox FrameFioul;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel37;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label lblN1Fioul;
        private System.Windows.Forms.Label lblEcFioul;
        private System.Windows.Forms.Label lblNFioul;
        private System.Windows.Forms.ContextMenuStrip contextMenu;
        private System.Windows.Forms.ToolStripMenuItem collerToolStripMenuItem;
        public System.Windows.Forms.Button buttonPastFiles;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel38;
        private Theme.Info info1;
    }
}
