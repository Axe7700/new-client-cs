﻿namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Intervenant_Par_Immeuble
{
    partial class UserDocIntervenantParImmeuble
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocIntervenantParImmeuble));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGridInterv = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblIntervanant = new iTalk.iTalk_TextBox_Small2();
            this.label2s = new System.Windows.Forms.Label();
            this.txtMatricule = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblTotal = new System.Windows.Forms.Label();
            this.cmbImmParTech = new System.Windows.Forms.Button();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridInterv)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ssGridInterv, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ssGridInterv
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridInterv.DisplayLayout.Appearance = appearance1;
            this.ssGridInterv.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridInterv.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridInterv.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridInterv.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridInterv.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridInterv.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridInterv.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridInterv.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridInterv.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridInterv.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridInterv.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridInterv.DisplayLayout.Override.AllowRowFiltering = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridInterv.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridInterv.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridInterv.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridInterv.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridInterv.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridInterv.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridInterv.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridInterv.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridInterv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridInterv.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridInterv.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridInterv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridInterv.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridInterv.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridInterv.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridInterv.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridInterv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ssGridInterv.Location = new System.Drawing.Point(3, 43);
            this.ssGridInterv.Name = "ssGridInterv";
            this.ssGridInterv.Size = new System.Drawing.Size(1844, 911);
            this.ssGridInterv.TabIndex = 23;
            this.ssGridInterv.Text = "ultraGrid1";
            this.ssGridInterv.AfterRowsDeleted += new System.EventHandler(this.ssGridInterv_AfterRowsDeleted);
            this.ssGridInterv.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridInterv_AfterRowUpdate);
            this.ssGridInterv.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridInterv_BeforeRowsDeleted);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1850, 40);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.lblIntervanant);
            this.panel2.Controls.Add(this.label2s);
            this.panel2.Controls.Add(this.txtMatricule);
            this.panel2.Controls.Add(this.cmdRechercheIntervenant);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(925, 40);
            this.panel2.TabIndex = 1;
            // 
            // lblIntervanant
            // 
            this.lblIntervanant.AccAcceptNumbersOnly = false;
            this.lblIntervanant.AccAllowComma = false;
            this.lblIntervanant.AccBackgroundColor = System.Drawing.Color.White;
            this.lblIntervanant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblIntervanant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblIntervanant.AccHidenValue = "";
            this.lblIntervanant.AccNotAllowedChars = null;
            this.lblIntervanant.AccReadOnly = false;
            this.lblIntervanant.AccReadOnlyAllowDelete = false;
            this.lblIntervanant.AccRequired = false;
            this.lblIntervanant.BackColor = System.Drawing.Color.White;
            this.lblIntervanant.CustomBackColor = System.Drawing.Color.White;
            this.lblIntervanant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblIntervanant.ForeColor = System.Drawing.Color.Black;
            this.lblIntervanant.Location = new System.Drawing.Point(395, 7);
            this.lblIntervanant.Margin = new System.Windows.Forms.Padding(2);
            this.lblIntervanant.MaxLength = 35;
            this.lblIntervanant.Multiline = false;
            this.lblIntervanant.Name = "lblIntervanant";
            this.lblIntervanant.ReadOnly = true;
            this.lblIntervanant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblIntervanant.Size = new System.Drawing.Size(253, 27);
            this.lblIntervanant.TabIndex = 370;
            this.lblIntervanant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblIntervanant.UseSystemPasswordChar = false;
            // 
            // label2s
            // 
            this.label2s.AutoSize = true;
            this.label2s.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2s.Location = new System.Drawing.Point(3, 10);
            this.label2s.Name = "label2s";
            this.label2s.Size = new System.Drawing.Size(97, 19);
            this.label2s.TabIndex = 367;
            this.label2s.Text = "Intervenants";
            // 
            // txtMatricule
            // 
            this.txtMatricule.AccAcceptNumbersOnly = false;
            this.txtMatricule.AccAllowComma = false;
            this.txtMatricule.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMatricule.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMatricule.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMatricule.AccHidenValue = "";
            this.txtMatricule.AccNotAllowedChars = null;
            this.txtMatricule.AccReadOnly = false;
            this.txtMatricule.AccReadOnlyAllowDelete = false;
            this.txtMatricule.AccRequired = false;
            this.txtMatricule.BackColor = System.Drawing.Color.White;
            this.txtMatricule.CustomBackColor = System.Drawing.Color.White;
            this.txtMatricule.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtMatricule.ForeColor = System.Drawing.Color.Black;
            this.txtMatricule.Location = new System.Drawing.Point(107, 7);
            this.txtMatricule.Margin = new System.Windows.Forms.Padding(2);
            this.txtMatricule.MaxLength = 35;
            this.txtMatricule.Multiline = false;
            this.txtMatricule.Name = "txtMatricule";
            this.txtMatricule.ReadOnly = false;
            this.txtMatricule.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMatricule.Size = new System.Drawing.Size(253, 27);
            this.txtMatricule.TabIndex = 368;
            this.txtMatricule.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMatricule.UseSystemPasswordChar = false;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheIntervenant.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheIntervenant.Image")));
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(365, 10);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheIntervenant.TabIndex = 369;
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lblTotal);
            this.panel1.Controls.Add(this.cmbImmParTech);
            this.panel1.Controls.Add(this.cmdExportExcel);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(925, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(925, 40);
            this.panel1.TabIndex = 2;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(3, 11);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(370, 19);
            this.lblTotal.TabIndex = 368;
            // 
            // cmbImmParTech
            // 
            this.cmbImmParTech.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmbImmParTech.FlatAppearance.BorderSize = 0;
            this.cmbImmParTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbImmParTech.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbImmParTech.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbImmParTech.Location = new System.Drawing.Point(682, 2);
            this.cmbImmParTech.Margin = new System.Windows.Forms.Padding(2);
            this.cmbImmParTech.Name = "cmbImmParTech";
            this.cmbImmParTech.Size = new System.Drawing.Size(60, 35);
            this.cmbImmParTech.TabIndex = 360;
            this.cmbImmParTech.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmbImmParTech.UseVisualStyleBackColor = false;
            this.cmbImmParTech.Click += new System.EventHandler(this.cmbImmParTech_Click);
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.excel_3_24;
            this.cmdExportExcel.Location = new System.Drawing.Point(746, 2);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(60, 35);
            this.cmdExportExcel.TabIndex = 366;
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // UserDocIntervenantParImmeuble
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocIntervenantParImmeuble";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Intervenant Par Immeuble";
            this.VisibleChanged += new System.EventHandler(this.UserDocIntervenantParImmeuble_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridInterv)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button cmbImmParTech;
        public System.Windows.Forms.Button cmdExportExcel;
        private System.Windows.Forms.Panel panel2;
        private iTalk.iTalk_TextBox_Small2 lblIntervanant;
        private System.Windows.Forms.Label label2s;
        private iTalk.iTalk_TextBox_Small2 txtMatricule;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridInterv;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Panel panel1;
    }
}
