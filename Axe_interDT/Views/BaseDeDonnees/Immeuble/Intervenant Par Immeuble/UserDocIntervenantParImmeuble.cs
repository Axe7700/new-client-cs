﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Intervenant_Par_Immeuble
{
    public partial class UserDocIntervenantParImmeuble : UserControl
    {
        private DataTable rsInterv;
        private ModAdo rsIntervModAdo;
        const int cCol = 0;
        public UserDocIntervenantParImmeuble()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbImmParTech_Click(object sender, System.EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            lblTotal.Text = "";
            fc_Execute();
            Cursor = Cursors.Default;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, System.EventArgs e)
        {
            if (rsIntervModAdo == null)
                cmbImmParTech_Click(sender, e);

            Microsoft.Office.Interop.Excel.Application oXL;
            Microsoft.Office.Interop.Excel.Workbook oWB;
            Microsoft.Office.Interop.Excel.Worksheet oSheet;
            Microsoft.Office.Interop.Excel.Range oRng;
            Microsoft.Office.Interop.Excel.Range oResizeRange;
            DataTable rsExport = null;
            SqlDataAdapter SDArsExport = null;
            SqlCommandBuilder SCBrsExport = null;
            //DataColumn oField;
            int i;
            int j;
            int lLigne;
            string sMatricule = "";
            bool bok;
            try
            {
                Cursor = Cursors.WaitCursor;
                //' Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;
                //' Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                rsExport = new DataTable();
                rsIntervModAdo.SDArsAdo.Fill(rsExport);
                if (rsExport.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                //  '== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i++;
                }

                Cursor = Cursors.WaitCursor;

                // ' Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                // 'formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                oResizeRange.Borders.Weight = 1;
                lLigne = 3;

                Cursor = Cursors.WaitCursor;
                foreach (DataRow rsExportRow in rsExport.Rows)
                {
                    j = 1;
                    foreach (DataColumn rsExportColumn in rsExport.Columns)
                    {
                        oSheet.Cells[lLigne, j + cCol].value = rsExportRow[rsExportColumn.ColumnName];
                        j++;
                    }

                    lLigne++;
                }

                // 'formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                Cursor = Cursors.Default;
                rsExport = null;
                CustomMessageBox.Show("Exportation terminée", "Opération réussie", MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
            }
            catch (System.Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, System.EventArgs e)
        {
            string requete = "SELECT Personnel.Matricule as \"Matricule\","
                + " Personnel.Nom as \"Nom\","
                + " Personnel.prenom as \"Prenom\","
                + " Qualification.Qualification as \"Qualification\","
                + " Personnel.Memoguard as \"MemoGuard\","
                + " Personnel.NumRadio as \"NumRadio\""
                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            string where_order = "";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des intervenants" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtMatricule.Text = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                lblIntervanant.Text = fg.ugResultat.ActiveRow.Cells["Nom"].Value.ToString();
                fg.Dispose(); fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtMatricule.Text = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                    lblIntervanant.Text = fg.ugResultat.ActiveRow.Cells["Nom"].Value.ToString();
                    fg.Dispose(); fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Execute()
        {
            string sSQL = "";
            string sWhere = "";

            try
            {
                sSQL = " DELETE FROM TempTechParImm  where CreePar ='" + General.fncUserName() + "'";
                General.Execute(sSQL);

                sSQL = "INSERT INTO TempTechParImm"
                    + " ( CodeImmeuble, CodeDepanneur, Nom, Prenom, [Adresse 1 immeuble],"
                    + " [Adresse 2 immeuble], [CP immeuble], [Ville immeuble], CodeAcces1,"
                    + " CodeAcces2, Gardien, TelGardien, FaxGardien, SpecifAcces, HorairesLoge, "
                    + " Client, Adresse1, CodePostal, Ville,CreePar)"
                    + " SELECT DISTINCT"
                    + "  Immeuble.CodeImmeuble, Immeuble.CodeDepanneur, Personnel.Nom,"
                    + " Personnel.Prenom,"
                    + " Immeuble.Adresse AS [Adresse 1 immeuble], Immeuble.Adresse2_Imm AS [Adresse 2 immeuble],"
                    + " Immeuble.CodePostal AS [CP immeuble],"
                    + " Immeuble.Ville AS [Ville immeuble], Immeuble.CodeAcces1, Immeuble.CodeAcces2, "
                    + " Immeuble.Gardien, Immeuble.TelGardien, Immeuble.FaxGardien,"
                    + " Immeuble.SpecifAcces, Immeuble.HorairesLoge, Table1.Nom AS Client, Table1.Adresse1,"
                    + " Table1.CodePostal, Table1.Ville, "
                    + " '" + General.fncUserName() + "' AS CreePar "
                    + " FROM         Immeuble INNER JOIN"
                    + " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule INNER JOIN"
                    + " Table1 ON Immeuble.Code1 = Table1.Code1 INNER JOIN"
                    + " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble"
                    + " WHERE     (Contrat.Resiliee = 0)";

                if (!string.IsNullOrEmpty(txtMatricule.Text))
                {
                    sWhere = " AND CodeDepanneur ='" + StdSQLchaine.gFr_DoublerQuote(txtMatricule.Text) + "'";
                    sSQL = sSQL + sWhere;
                }

                General.Execute(sSQL);

                sSQL = "SELECT Id, CodeImmeuble, CodeDepanneur, Nom, Prenom, [Adresse 1 immeuble],"
                       + " [Adresse 2 immeuble], [CP immeuble], [Ville immeuble], "
                       + " CodeAcces1, CodeAcces2, Gardien, TelGardien, FaxGardien, SpecifAcces,"
                       + " HorairesLoge, Client, Adresse1, CodePostal, Ville, LibelleContrat, CreePar,"
                       + " PCS"
                       + " FROM         TempTechParImm"
                       + " WHERE CreePar ='" + General.fncUserName() + "'";

                rsIntervModAdo = new ModAdo();
                rsInterv = rsIntervModAdo.fc_OpenRecordSet(sSQL);

                fc_maj();

                ssGridInterv.DataSource = rsInterv;

                lblTotal.Text = "Total :" + rsInterv.Rows.Count;

                if (rsInterv.Rows.Count == 0)
                {
                    CustomMessageBox.Show("Aucun enregistrement.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_maj()
        {
            DataTable rs;
            ModAdo rsModAdo;
            string sSQL = "";

            if (rsInterv == null || rsInterv.Rows.Count == 0)
            {
                return;
            }

            try
            {
                foreach (DataRow rsIntervRow in rsInterv.Rows)
                {
                    sSQL = "SELECT Nom_IMC FROM IMC_ImmCorrespondant WHERE CodeImmeuble_IMM='" + StdSQLchaine.gFr_DoublerQuote(rsIntervRow["CodeImmeuble"]?.ToString()) + "' AND PresidentCS_IMC='-1'";

                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                    rsIntervRow["PCS"] = sSQL;

                    sSQL =
                        "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, "
                        + " Contrat.CodeArticle, FacArticle.Designation1 "
                        + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle  "
                        + " WHERE Contrat.CodeImmeuble like '" +
                        StdSQLchaine.gFr_DoublerQuote(rsIntervRow["CodeImmeuble"]?.ToString())
                        + "%' "
                        + " AND Avenant=0 GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, "
                        + " FacArticle.Designation1 "
                        //===> Mondir le 12.01.2021, ajout du order by pour corriger : #2166
                        + " ORDER BY MAX(Contrat.DateEffet) DESC";
                    //===> Fin Modif Mondir

                    rsModAdo = new ModAdo();
                    rs = rsModAdo.fc_OpenRecordSet(sSQL);

                    if (rs?.Rows.Count > 0)
                    {
                        rsIntervRow["LibelleContrat"] = rs.Rows[0]["Designation1"];
                    }

                    rsModAdo.Close();
                }

                rsIntervModAdo.Update();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void UserDocIntervenantParImmeuble_VisibleChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridInterv_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            rsIntervModAdo.Update();
        }

        private void ssGridInterv_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsIntervModAdo.Update();
        }

        private void ssGridInterv_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
