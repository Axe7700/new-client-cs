﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Fiche_Mode_Reglement
{
    public partial class UserDocReglement : UserControl
    {
        bool blModif;
        bool blnAjout;
        public UserDocReglement()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbModeReglt_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[6];
            tabJour[0] = "BAO";
            tabJour[1] = "CCP";
            tabJour[2] = "CHEQUE";
            tabJour[3] = "PRELEVEMENT";
            tabJour[4] = "TRAITE";
            tabJour[5] = "VIREMENT";

            sheridan.AlimenteCmbAdditemTableau(cmbModeReglt, tabJour, "");
            //Me.ssCHP_ChargePers.Columns("CHP_MOIS").DropDownHwnd = Me.ssDropMois.hWnd
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypeEcheance_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //== affiche ou non les titres des colonnes
            DataTable dt = new DataTable();
            dt.Columns.Add("Column 0");
            dt.Columns.Add("Column 1");

            dt.Rows.Add("F", "Fin de mois Le");
            dt.Rows.Add("L", "Jours dans mois Le");
            dt.Rows.Add("N", "Jour Comptant");

            cmbTypeEcheance.DataSource = dt;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            if (blModif == true)
            {
                DialogResult d = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                switch (d)
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_clear();
            fc_BloqueForm("Ajouter");

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm(string sCommande = "")
        {
            try {
                var _with3 = this;

                _with3.cmdAjouter.Enabled = true;
                _with3.CmdRechercher.Enabled = true;
                _with3.cmdSupprimer.Enabled = true;
                _with3.CmdSauver.Enabled = true;
                CmdRechercher.Enabled = true;
                //=== desactive le mode Ajout
                blnAjout = false;

                return;
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_DeBloqueForm");
            }

        }
        /// <summary>
        /// Testd
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {
            try
            {

                var _with4 = this;

                _with4.cmdSupprimer.Enabled = false;
                //=== mode ajout
                if (sCommande.ToUpper() == "Ajouter".ToUpper())//Tested
                {
                    _with4.cmdAjouter.Enabled = true;
                    _with4.CmdRechercher.Enabled = false;
                    _with4.CmdSauver.Enabled = true;
                    CmdRechercher.Enabled = false;
                    // Active le mode Ajout
                    blnAjout = true;
                }
                else//Tested
                {
                    //=== desactive le mode Ajout
                    blnAjout = false;
                    CmdRechercher.Enabled = true;
                    _with4.CmdRechercher.Enabled = true;
                    _with4.CmdSauver.Enabled = false;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BloqueForm");
            }
        }
        private void fc_annuler()
        {
            var _with5 = this;
            if (blModif == true)
            {
                DialogResult d = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question);
                switch (d)
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_clear();
            fc_BloqueForm();

        }
        private bool FC_insert()
        {
            bool functionReturnValue = false;
            // creation d' un nouveau client
            DataTable rsExtranet = default(DataTable);
            functionReturnValue = true;

            var _with6 = this;

            if (txtCode.Text.Contains("'"))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'apostrophe n'est pas un caractère autorisé." + "\n", "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCode.Focus();
                return functionReturnValue;
            }

            if (txtCode.Text.Contains(" "))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'espace n'est pas un caractère autorisé." + "\n", "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCode.Focus();
                return functionReturnValue;
            }

            //         SQL = "INSERT INTO MARQ_Marque ( MARQ_Code, MARQ_RaisonSocial)"
            //         SQL = SQL & " VALUES ("
            //         SQL = SQL & "'" & Trim(gFr_DoublerQuote(.txtMARQ_Code)) & "',"
            //         SQL = SQL & "'" & Trim(gFr_DoublerQuote(.txtMARQ_RaisonSocial)) & "'"
            //         SQL = SQL & ")"
            //         adocnn.Execute SQL, , adExecuteNoRecords
            //         txtSelMARQ_Code = txtMARQ_Code

            // debloque les onglets.
            fc_DeBloqueForm();
            return functionReturnValue;



        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_sauver()
        {
            string sCode = null;
            string sSQL = null;
            int lMsgbox = 0;
            DataTable rs = default(DataTable);

            sCode = txtCode.Text;

            //=== controle si code client saisie
            if (string.IsNullOrEmpty(txtCode.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            sSQL = "SELECT     Code, Libelle, ModeReglt, NombreJour, TypeEcheance, Echeance" + " From CodeReglement"
                + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(txtCode.Text) + "'";
            var tmpAdors = new ModAdo();
            rs = tmpAdors.fc_OpenRecordSet(sSQL);

            if (rs.Rows.Count == 0)//Tested
            {
                rs.Rows.Add();
            }
            else if (string.IsNullOrEmpty(txtSelCode.Text))//Tested
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement existe déjà.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                fc_load(txtCode.Text);
                return;
            }

            rs.Rows[0]["Code"] = txtCode.Text;
            rs.Rows[0]["Libelle"] = txtLibelle.Text;
            rs.Rows[0]["ModeReglt"] = cmbModeReglt.Text;

            if (string.IsNullOrEmpty(txtNombreJour.Text))
            {
                txtNombreJour.Text = "0";
            }

            rs.Rows[0]["NombreJour"] = General.nz(txtNombreJour.Text, 0);

            rs.Rows[0]["TypeEcheance"] = cmbTypeEcheance.Text;

            if (string.IsNullOrEmpty(txtEcheance.Text))
            {
                txtEcheance.Text = "0";
            }

            rs.Rows[0]["Echeance"] = General.nz(txtEcheance.Text, 0);

            int xx=tmpAdors.Update();
            txtSelCode.Text = txtCode.Text;
            rs.Dispose();
            rs = null;

            //If txtSelCode = "" Then
            //    If FC_Insert = False Then
            //        Exit Sub
            //    Else
            //        blnAjout = False
            //    End If
            //Else
            //    fc_maj scode
            //End If

            //=== debloque les controles.
            fc_DeBloqueForm();

            blModif = false;

        }
        /// <summary>
        /// Tested
        /// </summary>

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
        }
        /// <summary>
        /// TODO 
        /// searchTemplate ==> Lorsque je fais la recherche du NombreJour et echéance en mm temps 
        /// ==> une erreur se déclenche dans le where "and"
        /// </summary>
        private void cmdfindModeReglement_Click(object sender, EventArgs e)
        {
            string sCode = null;
            string req = "";
            string where = "";

            try
            {
                req = "SELECT     Code as \"Code\", "
                    + " Libelle as \"Libelle\","
                    + " NombreJour as \"Nombre Jour\", "
                    + " TypeEcheance as \"Type Echeance\", "
                    + " Echeance as \"Echeance\"FROM         CodeReglement";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un mode de reglement" };
                fg.ugResultat.DoubleClickRow += (se, ev) => {
                    //====charge les enregistrements de l'immeuble
                    txtModeRglt.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblReglemenent.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) => {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    { //====charge les enregistrements de l'immeuble
                        txtModeRglt.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lblReglemenent.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

            }
           catch(Exception ex){
            Erreurs.gFr_debug(ex,this.Name + ";cmdfindModeReglement_Click");
    }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
        }
        /// <summary>
        ///Tested
        /// 
        /// </summary>
        private void fc_Rechercher()
        {
            string sCode = null;
            string req = "";
            string where = "";
            try
            {
              
                    req = "SELECT     Code as \"Code\", "
                        + " Libelle as \"Libelle\","
                        + " NombreJour as \"Nombre Jour\", "
                        + " TypeEcheance as \"Type Echeance\", "
                        + " Echeance as \"Echeance\"  FROM    CodeReglement";
                    SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un mode de reglement" };
                    fg.ugResultat.DoubleClickRow += (se, ev) => {
                        //====charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_load(sCode);
                        fg.Dispose(); fg.Close();
                    };
                    fg.ugResultat.KeyDown += (se, ev) => {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        { //====charge les enregistrements de l'immeuble
                            sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fc_load(sCode);
                            fg.Dispose(); fg.Close();
                        }

                    };
                    fg.StartPosition = FormStartPosition.CenterScreen;
                    fg.ShowDialog();


                    return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_Rechercher");
                return;
            }
           
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            if (fc_CtrSaisie() == true)//tested
            {
                return;
            }

            fc_sauver();

            fc_DeBloqueForm();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrSaisie()
        {
            bool functionReturnValue = false;
            //=== retourne true en cas d'erreur.
            try
            {
                functionReturnValue = false;

                if (string.IsNullOrEmpty(txtCode.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code est obligatoire.",  "validation annulée",MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                if (string.IsNullOrEmpty(cmbModeReglt.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le moyen de réglement est obligatoire.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (!General.IsNumeric(txtNombreJour.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le nombre de jour doit être de type numérique.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(cmbTypeEcheance.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type d'échéance est obligatoire.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }


                if (!General.IsNumeric(txtEcheance.Text))//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'écheance doit être de type numérique.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                return functionReturnValue;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_CtrSaisie");
                return functionReturnValue;
            }

           
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTest_Click(object sender, EventArgs e)
        {
            try
            {
               
                if (string.IsNullOrEmpty(txtDate.Text))//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtDate.Focus();
                    return;
                }

                if (!General.IsDate(txtDate.Text))//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date saisie est incorrecte", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtDate.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(txtModeRglt.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un mode de réglement", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    txtModeRglt.Text = "";
                    txtModeRglt.Focus();
                    return;
                }
                else
                {//Tested
                    using (var tmpAdo = new ModAdo())
                        lblReglemenent.Text = tmpAdo.fc_ADOlibelle("SELECT LIBELLE FROM CodeReglement WHERE code ='"
                            + StdSQLchaine.gFr_DoublerQuote(txtModeRglt.Text) + "'");
                    if (string.IsNullOrEmpty(lblReglemenent.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce mode de réglement n'existe pas.", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }

             txtDateResultat.Text = Convert.ToString(General.CalcEch( Convert.ToDateTime(txtDate.Text),txtModeRglt.Text));//Tested

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";cmdTest_Click");
            }
            }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_load(string sCode)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            try
            {
                sSQL = "SELECT    Code, Libelle, ModeReglt, NombreJour," + " TypeEcheance, Echeance" + " From CodeReglement "
                    + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                var tmpADors = new ModAdo();
                rs = tmpADors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count>0)
                {

                    txtCode.Text = rs.Rows[0]["Code"] + "";
                    txtLibelle.Text = rs.Rows[0]["Libelle"] + "";
                    cmbModeReglt.Text = rs.Rows[0]["ModeReglt"] + "";
                    txtNombreJour.Text = rs.Rows[0]["NombreJour"] + "";
                    cmbTypeEcheance.Text = rs.Rows[0]["TypeEcheance"] + "";
                    txtEcheance.Text = rs.Rows[0]["Echeance"] + "";
                    txtSelCode.Text = txtCode.Text;
                }
                else
                {
                    fc_clear();
                }

                rs.Dispose();
                rs = null;
                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_Load");
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_clear()
        {

            txtCode.Text = "";
            txtLibelle.Text = "";
            cmbModeReglt.Text = "";
            txtNombreJour.Text = "";
            cmbTypeEcheance.Text = "";
            txtEcheance.Text = "";
            txtSelCode.Text = "";

        }

        private void UserDocReglement_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
                View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");
        }
    }
}
