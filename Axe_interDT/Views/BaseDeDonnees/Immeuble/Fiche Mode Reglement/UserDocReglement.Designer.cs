namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Fiche_Mode_Reglement
{
    partial class UserDocReglement
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSelCode = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCode = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelle = new iTalk.iTalk_TextBox_Small2();
            this.txtNombreJour = new iTalk.iTalk_TextBox_Small2();
            this.txtEcheance = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbModeReglt = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmbTypeEcheance = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtModeRglt = new iTalk.iTalk_TextBox_Small2();
            this.txtDate = new iTalk.iTalk_TextBox_Small2();
            this.txtDateResultat = new iTalk.iTalk_TextBox_Small2();
            this.lblReglemenent = new iTalk.iTalk_TextBox_Small2();
            this.cmdTest = new System.Windows.Forms.Button();
            this.cmdfindModeReglement = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbModeReglt)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypeEcheance)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdSupprimer);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1200, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(321, 40);
            this.flowLayoutPanel1.TabIndex = 352;
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(259, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 374;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(195, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 373;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(131, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 365;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(67, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 359;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(3, 2);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 364;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtSelCode, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtCode, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtLibelle, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtNombreJour, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtEcheance, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.cmbModeReglt, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmbTypeEcheance, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 7;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1509, 210);
            this.tableLayoutPanel1.TabIndex = 353;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(295, 30);
            this.label5.TabIndex = 389;
            this.label5.Text = "Echéance";
            // 
            // txtSelCode
            // 
            this.txtSelCode.AccAcceptNumbersOnly = false;
            this.txtSelCode.AccAllowComma = false;
            this.txtSelCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSelCode.AccHidenValue = "";
            this.txtSelCode.AccNotAllowedChars = null;
            this.txtSelCode.AccReadOnly = false;
            this.txtSelCode.AccReadOnlyAllowDelete = false;
            this.txtSelCode.AccRequired = false;
            this.txtSelCode.BackColor = System.Drawing.Color.White;
            this.txtSelCode.CustomBackColor = System.Drawing.Color.White;
            this.txtSelCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSelCode.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSelCode.ForeColor = System.Drawing.Color.Black;
            this.txtSelCode.Location = new System.Drawing.Point(905, 2);
            this.txtSelCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelCode.MaxLength = 32767;
            this.txtSelCode.Multiline = false;
            this.txtSelCode.Name = "txtSelCode";
            this.txtSelCode.ReadOnly = false;
            this.txtSelCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelCode.Size = new System.Drawing.Size(297, 27);
            this.txtSelCode.TabIndex = 502;
            this.txtSelCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelCode.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(295, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Code";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(295, 30);
            this.label1.TabIndex = 385;
            this.label1.Text = "Libelle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(295, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Moyen Réglement";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 120);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(295, 30);
            this.label3.TabIndex = 387;
            this.label3.Text = "Nombre Jour";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 150);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(295, 30);
            this.label4.TabIndex = 388;
            this.label4.Text = "Type Echéance";
            // 
            // txtCode
            // 
            this.txtCode.AccAcceptNumbersOnly = false;
            this.txtCode.AccAllowComma = false;
            this.txtCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode.AccHidenValue = "";
            this.txtCode.AccNotAllowedChars = null;
            this.txtCode.AccReadOnly = false;
            this.txtCode.AccReadOnlyAllowDelete = false;
            this.txtCode.AccRequired = false;
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.CustomBackColor = System.Drawing.Color.White;
            this.txtCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCode.ForeColor = System.Drawing.Color.Black;
            this.txtCode.Location = new System.Drawing.Point(303, 32);
            this.txtCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode.MaxLength = 32767;
            this.txtCode.Multiline = false;
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = false;
            this.txtCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode.Size = new System.Drawing.Size(297, 27);
            this.txtCode.TabIndex = 0;
            this.txtCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode.UseSystemPasswordChar = false;
            // 
            // txtLibelle
            // 
            this.txtLibelle.AccAcceptNumbersOnly = false;
            this.txtLibelle.AccAllowComma = false;
            this.txtLibelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelle.AccHidenValue = "";
            this.txtLibelle.AccNotAllowedChars = null;
            this.txtLibelle.AccReadOnly = false;
            this.txtLibelle.AccReadOnlyAllowDelete = false;
            this.txtLibelle.AccRequired = false;
            this.txtLibelle.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.txtLibelle, 2);
            this.txtLibelle.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibelle.ForeColor = System.Drawing.Color.Black;
            this.txtLibelle.Location = new System.Drawing.Point(303, 62);
            this.txtLibelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelle.MaxLength = 32767;
            this.txtLibelle.Multiline = false;
            this.txtLibelle.Name = "txtLibelle";
            this.txtLibelle.ReadOnly = false;
            this.txtLibelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelle.Size = new System.Drawing.Size(598, 27);
            this.txtLibelle.TabIndex = 1;
            this.txtLibelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelle.UseSystemPasswordChar = false;
            // 
            // txtNombreJour
            // 
            this.txtNombreJour.AccAcceptNumbersOnly = false;
            this.txtNombreJour.AccAllowComma = false;
            this.txtNombreJour.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNombreJour.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNombreJour.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNombreJour.AccHidenValue = "";
            this.txtNombreJour.AccNotAllowedChars = null;
            this.txtNombreJour.AccReadOnly = false;
            this.txtNombreJour.AccReadOnlyAllowDelete = false;
            this.txtNombreJour.AccRequired = false;
            this.txtNombreJour.BackColor = System.Drawing.Color.White;
            this.txtNombreJour.CustomBackColor = System.Drawing.Color.White;
            this.txtNombreJour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNombreJour.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNombreJour.ForeColor = System.Drawing.Color.Black;
            this.txtNombreJour.Location = new System.Drawing.Point(303, 122);
            this.txtNombreJour.Margin = new System.Windows.Forms.Padding(2);
            this.txtNombreJour.MaxLength = 32767;
            this.txtNombreJour.Multiline = false;
            this.txtNombreJour.Name = "txtNombreJour";
            this.txtNombreJour.ReadOnly = false;
            this.txtNombreJour.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNombreJour.Size = new System.Drawing.Size(297, 27);
            this.txtNombreJour.TabIndex = 3;
            this.txtNombreJour.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNombreJour.UseSystemPasswordChar = false;
            // 
            // txtEcheance
            // 
            this.txtEcheance.AccAcceptNumbersOnly = false;
            this.txtEcheance.AccAllowComma = false;
            this.txtEcheance.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEcheance.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEcheance.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEcheance.AccHidenValue = "";
            this.txtEcheance.AccNotAllowedChars = null;
            this.txtEcheance.AccReadOnly = false;
            this.txtEcheance.AccReadOnlyAllowDelete = false;
            this.txtEcheance.AccRequired = false;
            this.txtEcheance.BackColor = System.Drawing.Color.White;
            this.txtEcheance.CustomBackColor = System.Drawing.Color.White;
            this.txtEcheance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEcheance.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEcheance.ForeColor = System.Drawing.Color.Black;
            this.txtEcheance.Location = new System.Drawing.Point(303, 182);
            this.txtEcheance.Margin = new System.Windows.Forms.Padding(2);
            this.txtEcheance.MaxLength = 32767;
            this.txtEcheance.Multiline = false;
            this.txtEcheance.Name = "txtEcheance";
            this.txtEcheance.ReadOnly = false;
            this.txtEcheance.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEcheance.Size = new System.Drawing.Size(297, 27);
            this.txtEcheance.TabIndex = 5;
            this.txtEcheance.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEcheance.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label6, 2);
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(605, 30);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(596, 30);
            this.label6.TabIndex = 506;
            this.label6.Text = "(Code sur 5 caractères)";
            // 
            // cmbModeReglt
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbModeReglt.DisplayLayout.Appearance = appearance1;
            this.cmbModeReglt.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbModeReglt.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbModeReglt.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbModeReglt.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbModeReglt.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbModeReglt.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbModeReglt.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbModeReglt.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbModeReglt.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbModeReglt.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbModeReglt.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbModeReglt.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbModeReglt.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbModeReglt.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbModeReglt.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbModeReglt.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbModeReglt.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbModeReglt.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbModeReglt.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbModeReglt.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbModeReglt.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbModeReglt.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbModeReglt.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbModeReglt.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbModeReglt.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbModeReglt.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbModeReglt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbModeReglt.Location = new System.Drawing.Point(304, 93);
            this.cmbModeReglt.Name = "cmbModeReglt";
            this.cmbModeReglt.Size = new System.Drawing.Size(295, 22);
            this.cmbModeReglt.TabIndex = 2;
            this.cmbModeReglt.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbModeReglt_BeforeDropDown);
            // 
            // cmbTypeEcheance
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbTypeEcheance.DisplayLayout.Appearance = appearance13;
            this.cmbTypeEcheance.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbTypeEcheance.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbTypeEcheance.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbTypeEcheance.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbTypeEcheance.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbTypeEcheance.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbTypeEcheance.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbTypeEcheance.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbTypeEcheance.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbTypeEcheance.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbTypeEcheance.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbTypeEcheance.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbTypeEcheance.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbTypeEcheance.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbTypeEcheance.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbTypeEcheance.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbTypeEcheance.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbTypeEcheance.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbTypeEcheance.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbTypeEcheance.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbTypeEcheance.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbTypeEcheance.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbTypeEcheance.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbTypeEcheance.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbTypeEcheance.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbTypeEcheance.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbTypeEcheance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbTypeEcheance.Location = new System.Drawing.Point(304, 153);
            this.cmbTypeEcheance.Name = "cmbTypeEcheance";
            this.cmbTypeEcheance.Size = new System.Drawing.Size(295, 22);
            this.cmbTypeEcheance.TabIndex = 4;
            this.cmbTypeEcheance.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbTypeEcheance_BeforeDropDown);
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.tableLayoutPanel3);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.groupBox6.Location = new System.Drawing.Point(1, 217);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(1);
            this.groupBox6.Size = new System.Drawing.Size(1513, 87);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Test";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 7;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.84082F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.60987F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48613F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48613F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.09091F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.48613F));
            this.tableLayoutPanel3.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label9, 6, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtModeRglt, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtDate, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtDateResultat, 6, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblReglemenent, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmdTest, 4, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmdfindModeReglement, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label10, 5, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1, 19);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.Padding = new System.Windows.Forms.Padding(2);
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1511, 67);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(5, 2);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(302, 33);
            this.label7.TabIndex = 384;
            this.label7.Text = "Code reglement";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(5, 35);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(302, 37);
            this.label8.TabIndex = 385;
            this.label8.Text = "Date";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1265, 2);
            this.label9.Name = "label9";
            this.label9.Padding = new System.Windows.Forms.Padding(1);
            this.label9.Size = new System.Drawing.Size(241, 33);
            this.label9.TabIndex = 386;
            this.label9.Text = "Résultat";
            this.label9.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // txtModeRglt
            // 
            this.txtModeRglt.AccAcceptNumbersOnly = false;
            this.txtModeRglt.AccAllowComma = false;
            this.txtModeRglt.AccBackgroundColor = System.Drawing.Color.White;
            this.txtModeRglt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtModeRglt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtModeRglt.AccHidenValue = "";
            this.txtModeRglt.AccNotAllowedChars = null;
            this.txtModeRglt.AccReadOnly = false;
            this.txtModeRglt.AccReadOnlyAllowDelete = false;
            this.txtModeRglt.AccRequired = false;
            this.txtModeRglt.BackColor = System.Drawing.Color.White;
            this.txtModeRglt.CustomBackColor = System.Drawing.Color.White;
            this.txtModeRglt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModeRglt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtModeRglt.ForeColor = System.Drawing.Color.Black;
            this.txtModeRglt.Location = new System.Drawing.Point(312, 4);
            this.txtModeRglt.Margin = new System.Windows.Forms.Padding(2);
            this.txtModeRglt.MaxLength = 32767;
            this.txtModeRglt.Multiline = false;
            this.txtModeRglt.Name = "txtModeRglt";
            this.txtModeRglt.ReadOnly = false;
            this.txtModeRglt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtModeRglt.Size = new System.Drawing.Size(301, 27);
            this.txtModeRglt.TabIndex = 0;
            this.txtModeRglt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtModeRglt.UseSystemPasswordChar = false;
            // 
            // txtDate
            // 
            this.txtDate.AccAcceptNumbersOnly = false;
            this.txtDate.AccAllowComma = false;
            this.txtDate.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDate.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDate.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDate.AccHidenValue = "";
            this.txtDate.AccNotAllowedChars = null;
            this.txtDate.AccReadOnly = false;
            this.txtDate.AccReadOnlyAllowDelete = false;
            this.txtDate.AccRequired = false;
            this.txtDate.BackColor = System.Drawing.Color.White;
            this.txtDate.CustomBackColor = System.Drawing.Color.White;
            this.txtDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDate.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDate.ForeColor = System.Drawing.Color.Black;
            this.txtDate.Location = new System.Drawing.Point(312, 37);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.MaxLength = 32767;
            this.txtDate.Multiline = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = false;
            this.txtDate.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDate.Size = new System.Drawing.Size(301, 27);
            this.txtDate.TabIndex = 2;
            this.txtDate.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDate.UseSystemPasswordChar = false;
            // 
            // txtDateResultat
            // 
            this.txtDateResultat.AccAcceptNumbersOnly = false;
            this.txtDateResultat.AccAllowComma = false;
            this.txtDateResultat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateResultat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateResultat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateResultat.AccHidenValue = "";
            this.txtDateResultat.AccNotAllowedChars = null;
            this.txtDateResultat.AccReadOnly = false;
            this.txtDateResultat.AccReadOnlyAllowDelete = false;
            this.txtDateResultat.AccRequired = false;
            this.txtDateResultat.BackColor = System.Drawing.Color.White;
            this.txtDateResultat.CustomBackColor = System.Drawing.Color.White;
            this.txtDateResultat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateResultat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateResultat.ForeColor = System.Drawing.Color.Black;
            this.txtDateResultat.Location = new System.Drawing.Point(1264, 37);
            this.txtDateResultat.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateResultat.MaxLength = 32767;
            this.txtDateResultat.Multiline = false;
            this.txtDateResultat.Name = "txtDateResultat";
            this.txtDateResultat.ReadOnly = false;
            this.txtDateResultat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateResultat.Size = new System.Drawing.Size(243, 27);
            this.txtDateResultat.TabIndex = 504;
            this.txtDateResultat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateResultat.UseSystemPasswordChar = false;
            // 
            // lblReglemenent
            // 
            this.lblReglemenent.AccAcceptNumbersOnly = false;
            this.lblReglemenent.AccAllowComma = false;
            this.lblReglemenent.AccBackgroundColor = System.Drawing.Color.White;
            this.lblReglemenent.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblReglemenent.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblReglemenent.AccHidenValue = "";
            this.lblReglemenent.AccNotAllowedChars = null;
            this.lblReglemenent.AccReadOnly = false;
            this.lblReglemenent.AccReadOnlyAllowDelete = false;
            this.lblReglemenent.AccRequired = false;
            this.lblReglemenent.BackColor = System.Drawing.Color.White;
            this.lblReglemenent.CustomBackColor = System.Drawing.Color.White;
            this.lblReglemenent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblReglemenent.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblReglemenent.ForeColor = System.Drawing.Color.Black;
            this.lblReglemenent.Location = new System.Drawing.Point(644, 4);
            this.lblReglemenent.Margin = new System.Windows.Forms.Padding(2);
            this.lblReglemenent.MaxLength = 32767;
            this.lblReglemenent.Multiline = false;
            this.lblReglemenent.Name = "lblReglemenent";
            this.lblReglemenent.ReadOnly = false;
            this.lblReglemenent.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblReglemenent.Size = new System.Drawing.Size(239, 27);
            this.lblReglemenent.TabIndex = 1;
            this.lblReglemenent.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblReglemenent.UseSystemPasswordChar = false;
            // 
            // cmdTest
            // 
            this.cmdTest.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdTest.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdTest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdTest.FlatAppearance.BorderSize = 0;
            this.cmdTest.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTest.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.cmdTest.ForeColor = System.Drawing.Color.White;
            this.cmdTest.Image = global::Axe_interDT.Properties.Resources.Calculator_16x16;
            this.cmdTest.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdTest.Location = new System.Drawing.Point(895, 12);
            this.cmdTest.Margin = new System.Windows.Forms.Padding(10);
            this.cmdTest.Name = "cmdTest";
            this.tableLayoutPanel3.SetRowSpan(this.cmdTest, 2);
            this.cmdTest.Size = new System.Drawing.Size(223, 50);
            this.cmdTest.TabIndex = 575;
            this.cmdTest.Text = "&Calcul échéance";
            this.cmdTest.UseVisualStyleBackColor = false;
            this.cmdTest.Click += new System.EventHandler(this.cmdTest_Click);
            // 
            // cmdfindModeReglement
            // 
            this.cmdfindModeReglement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdfindModeReglement.FlatAppearance.BorderSize = 0;
            this.cmdfindModeReglement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdfindModeReglement.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdfindModeReglement.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdfindModeReglement.Location = new System.Drawing.Point(615, 5);
            this.cmdfindModeReglement.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.cmdfindModeReglement.Name = "cmdfindModeReglement";
            this.cmdfindModeReglement.Size = new System.Drawing.Size(20, 20);
            this.cmdfindModeReglement.TabIndex = 576;
            this.cmdfindModeReglement.Tag = "";
            this.toolTip1.SetToolTip(this.cmdfindModeReglement, "Recherche de l\'intervenant");
            this.cmdfindModeReglement.UseVisualStyleBackColor = false;
            this.cmdfindModeReglement.Click += new System.EventHandler(this.cmdfindModeReglement_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1131, 2);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(128, 33);
            this.label10.TabIndex = 577;
            this.label10.Text = "===>";
            this.label10.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox6, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 216F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 89F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1515, 813);
            this.tableLayoutPanel2.TabIndex = 412;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1521, 859);
            this.tableLayoutPanel4.TabIndex = 413;
            // 
            // UserDocReglement
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "UserDocReglement";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche Mode Reglement";
            this.VisibleChanged += new System.EventHandler(this.UserDocReglement_VisibleChanged);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbModeReglt)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbTypeEcheance)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdSupprimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCode;
        public iTalk.iTalk_TextBox_Small2 txtLibelle;
        public iTalk.iTalk_TextBox_Small2 txtNombreJour;
        public iTalk.iTalk_TextBox_Small2 txtEcheance;
        public System.Windows.Forms.Label label6;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbModeReglt;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbTypeEcheance;
        public iTalk.iTalk_TextBox_Small2 txtSelCode;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtModeRglt;
        public iTalk.iTalk_TextBox_Small2 txtDate;
        public iTalk.iTalk_TextBox_Small2 txtDateResultat;
        public iTalk.iTalk_TextBox_Small2 lblReglemenent;
        public System.Windows.Forms.Button cmdTest;
        public System.Windows.Forms.Button cmdfindModeReglement;
        public System.Windows.Forms.Label label10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}
