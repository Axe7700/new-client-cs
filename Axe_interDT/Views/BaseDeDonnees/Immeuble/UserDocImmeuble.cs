﻿using Axe_interDT.Shared;
using Axe_interDT.Shared.FilterOfGrids;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Gerant.Forms;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Fiche_Mode_Reglement;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.FicheLocalisation;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Contrat.FicheGMAO;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinGrid;
using iTalk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble
{
    public partial class UserDocImmeuble : UserControl
    {
        DataTable rsCorrespondants;
        private ModAdo modAdorsCorrespondants;
        DataTable rsCorr;
        private ModAdo modAdorsCorr;
        DataTable rsIMRE;
        private ModAdo modAdorsIMRE;
        DataTable rsCopro;
        private ModAdo modAdorsCopro;
        DataTable rsIntervenant;
        private ModAdo modAdorsIntervenant;
        DataTable rsintervention;
        private ModAdo modAdorsintervention;
        DataTable rsDevis;
        private ModAdo modAdorsDevis;
        DataTable rsICA_ImmCategorieInterv;
        private ModAdo modAdorsICA_ImmCategorieInterv;
        DataTable rsIMA_ImmArticle;
        private ModAdo modAdorsIMA_ImmArticle;
        DataTable rsBE;
        private ModAdo modAdorsBE;
        DataTable rsStatut;
        private ModAdo modAdorsStatut;
        DataTable rsFAC;
        private ModAdo modAdorsFAC;
        DataTable rsMatPers;
        private ModAdo modAdorsMatPers;
        DataTable rsMail;
        private ModAdo modAdorsMail;
        DataTable rsCompteurs;
        private ModAdo modAdorsCompteurs;

        DataTable rsGdpCombo;
        private ModAdo modAdorsGdpCombo;
        DataTable rsCompetence;
        private ModAdo modAdorsCompetence;

        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        private DataTable rsIDA;
        private ModAdo rsIDAModAdo;
        //===> Fin Modif Mondir

        private DataTable DATA1;
        private SqlDataAdapter SDADATA1;
        private SqlCommandBuilder SCBDATA1;

        private DataTable Adodc17;
        private SqlDataAdapter SDAAdodc17;
        private SqlCommandBuilder SCBAdodc17;

        private DataTable Adodc20;
        private SqlDataAdapter SDAAdodc20;
        private SqlCommandBuilder SCBAdodc20;

        private DataTable Adodc16;
        private SqlDataAdapter SDAAdodc16;
        private SqlCommandBuilder SCBAdodc16;

        private DataTable Adodc19;
        private SqlDataAdapter SDAAdodc19;
        private SqlCommandBuilder SCBAdodc19;

        private DataTable Adodc18;
        private SqlDataAdapter SDAAdodc18;
        private SqlCommandBuilder SCBAdodc18;

        private DataTable Adodc15;
        private SqlDataAdapter SDAAdodc15;
        private SqlCommandBuilder SCBAdodc15;

        ModAdo modAdorsPrest;

        bool blModif;
        bool blnAjout;
        string sQualiteCopro;
        string CodeFicheAppelante;
        string BisStrRequete;


        DataTable adors;
        private SqlDataAdapter SDAadors;
        private SqlCommandBuilder SCBadors;

        private ModAdo modAdoadors;
        string[] tabstrCompte;
        int intIndex;
        string ok;
        bool boolAutorise;
        DateTime dateTemp;
        string strdatetemp;
        string SQL;

        bool blnKeyPress;
        bool bLoadChange;
        bool boolErreurValidate;
        bool bCloseUp;

        //===> Mondir le 03.12.2020, https://groupe-dt.mantishub.io/view.php?id=2108
        bool activateResposableAndCommercial = false;
        //===> Fin Modif Mondir

        //===> Mondir le 16.04.2021, https://groupe-dt.mantishub.io/view.php?id=2189#c5817
        public static bool ActivateFinanceTab { get; set; }
        //===> Fin Modif Mondir

        private struct Personnel
        {
            public string sCommercial;
            public string sDevisRadiateur;
            public string sDevisExploit;
            public string sRenovChauff1;
            public string sRenovChauff2;
            public string sChauffagiste1;
            public string sChauffagiste2;
            public string sSecteur;
        }

        Personnel tpPers;

        public UserDocImmeuble()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_sauver()
        {
            bool functionReturnValue = false;
            string sCode = null;
            int lMsgbox = 0;
            string sAdresseClient = null;
            DataTable rsCopro = default(DataTable);
            ModAdo modAdorsCopro;
            string sCompteCopro = null;
            string sSQLTemp = null;

            try
            {
                sCode = txtCodeImmeuble.Text;
                // controle si code client saisie
                if (string.IsNullOrEmpty(sCode))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un nom d'immeuble.", "Validation annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Error);

                    //UltraMessageBoxInfo messageinfo = new UltraMessageBoxInfo();
                    //messageinfo.Buttons = MessageBoxButtons.OK;
                    //messageinfo.Icon = MessageBoxIcon.Error;
                    //messageinfo.Caption = "Validation annulée";
                    //messageinfo.TextFormatted = "Vous devez saisir un nom d'immeuble.";
                    //messageinfo.HeaderAppearance.BorderColor = Color.Red;
                    //this.ultraMessageBoxManager1.ButtonAlignment = Infragistics.Win.HAlign.Right;
                    //this.ultraMessageBoxManager1.ShowMessageBox(messageinfo);


                    return functionReturnValue;
                }


                if (!string.IsNullOrEmpty(txtCodeGestionnaire.Text))
                {

                    General.sSQL = "SELECT CodeGestinnaire" + " FROM Gestionnaire WHERE CodeGestinnaire   = '" +
                                   StdSQLchaine.gFr_DoublerQuote(txtCodeGestionnaire.Text) + "'" + " and Code1='" +
                                   StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                    using (var tmpModAdo = new ModAdo())
                        General.sSQL = tmpModAdo.fc_ADOlibelle(General.sSQL);

                    if (string.IsNullOrEmpty(General.sSQL))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Attention Ce gestionnaire n'existe pas, validation annulée." +
                            "Veuillez selectionner un gestionnaire dans la liste déroulante.", "Validation annulée",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        return functionReturnValue;
                        //txtCodeGestionnaire.SetFocus
                    }

                }

                //=== recherche de l'adresse client pour la mise à jour de la compta.
                using (var tmpModAdo = new ModAdo())
                    sAdresseClient =
                        tmpModAdo.fc_ADOlibelle("select adresse1 from table1 where code1='" +
                                                StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");
                Cursor.Current = Cursors.WaitCursor;

                if (string.IsNullOrEmpty(txtSelCodeImmeuble.Text))
                {
                    if (FC_insert() == false)
                    {
                        return functionReturnValue;
                    }
                    //=== ajout du compte immeuble.
                    var codeImmeuble = txtCodeImmeuble.Text;
                    var txtCode = this.txtCode1.Text;
                    var codeImm = txtCodeImmeuble.Text;
                    txtNCompte.Text = SAGE.fc_prcAjoutTiers(0, ref codeImmeuble, ref codeImm,
                         ref txtCode, txtAdresse.Text, "", txtCodePostal.Text, txtVille.Text,
                        sAdresseClient, "",
                        "", "", "", "", "", txtCodeImmeuble.Text, codeImmeubleCurrent: txtCodeImmeuble.Text);
                    //Commented by Mondir ===> No Needs to change the text of the controls, and this caused a bug ===> https://groupe-dt.mantishub.io/view.php?id=1652
                    //Passing by reference a text of a TextBox will not change the text of this control in VB6 !
                    //txtCodeImmeuble.Text = codeImm;
                    //txtCode1.Text = txtCode;
                    txtAnalytiqueImmeuble.Text = General.Left(txtNCompte.Text, 13);
                    txtCompteDivers.Text = General.Left(txtCode1.Text, 17);
                }
                else
                {
                    functionReturnValue = true;

                    if (string.IsNullOrEmpty(txtCode1.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code Client est obligatoire", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        Cursor.Current = Cursors.Arrow;
                        txtCode1.Focus();
                        return functionReturnValue;
                    }

                    //===> Mondir le 23.11.2020, demandé par Xavier et Jules, ticket 2086 et 2088
                    using (var tmpModAdo = new ModAdo())
                    {
                        var clientInDb = tmpModAdo.fc_ADOlibelle($"SELECT Code1 FROM Immeuble WHERE CodeImmeuble = '{txtSelCodeImmeuble.Text}'");
                        if (!string.IsNullOrEmpty(clientInDb) && clientInDb.ToUpper() != txtCode1.Text.ToUpper())
                        {
                            var qualification = tmpModAdo.fc_ADOlibelle($"SELECT CodeQualif FROM Personnel WHERE Login = '{General.fncUserName()}'");
                            if (qualification.ToUpper() != "DIR")
                            {
                                var dr = CustomMessageBox.Show(
                                    "Vous n'avez pas le droit de modifier le code client.\nSouhaitez-vous réinitialiser l'ancien code client et continuer?",
                                    "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                if (dr == DialogResult.Yes)
                                {
                                    txtCode1.Text = clientInDb;
                                }
                                else
                                {
                                    return false;
                                }
                            }
                        }
                    }
                    //===> Fin Modif

                    //If UCase(adocnn.DefaultDatabase) = UCase(cDelostal) Then
                    if (General.sVersionImmParComm == "1")
                    {
                        //=== modif du 23 09 2011, le responsable d'exploitation n'est plus
                        //=== obligatoire.
                        //=== modif du 04 07 2013, a partir d'une date le responsable d'exploit est oblifgatoire
                        //=== ainsi que le code commercial.

                        //===  nouvelle modif du 05 11 2013, le commercial et le resp exploit n'est plus obligatoire,
                        //=== seul les personnes habilitées peuvent modifier ces éléments.
                        if (General.IsDate(General.sDateComExploitOblitoire))
                        {
                            //                                sSQLTemp = fc_ADOlibelle("SELECT DateCreation From Immeuble WHERE CodeImmeuble = '" && gFr_DoublerQuote(txtCodeImmeuble) && "'")
                            //
                            //                                If IsDate(sSQLTemp) Then
                            //                                    If CDate(sSQLTemp) >= CDate(sDateComExploitOblitoire) Then
                            //                                            If ssCombo(13).Text = "" Then
                            //                                                MsgBox "Le responsable d'exploitation est obligatoire.", _
                            //'                                                            vbInformation, "Opération annulée"
                            //                                                SSTab1.Tab = 1
                            //                                                Screen.MousePointer = 1
                            //                                                ssCombo(13).SetFocus
                            //                                                Exit Function
                            //                                            End If
                            //                                            If ssCombo(15).Text = "" Then
                            //                                                MsgBox "Le commercial est obligatoire.", _
                            //'                                                            vbInformation, "Opération annulée"
                            //                                                SSTab1.Tab = 1
                            //                                                Screen.MousePointer = 1
                            //                                                ssCombo(15).SetFocus
                            //                                                Exit Function
                            //                                            End If
                            //
                            //
                            //                                    End If
                            //                                End If
                        }
                    }

                    //            If cmbEnergie.Text = "" Then
                    //                MsgBox "Vous devez saisir l'énergie de l'immeuble"
                    //                SSTab1.Tab = 0
                    //                Screen.MousePointer = 0
                    //                cmbEnergie.SetFocus
                    //                Exit Function
                    //            End If


                    if (General.sImmGeolocObligatoire == "1")
                    {
                        if (string.IsNullOrEmpty(txt115.Text) && string.IsNullOrEmpty(txt112.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Avant de valider, vous devez obligatoirement géo-localiser cet immeuble.",
                                "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SSTab1.SelectedTab = SSTab1.Tabs[0];
                            return functionReturnValue;
                        }

                        if (!General.IsNumeric(txt112.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La longitude doit être de type numérique.", "Erreur de saisie",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SSTab1.SelectedTab = SSTab1.Tabs[0];
                            return functionReturnValue;
                        }
                        else
                        {
                            if (!(Convert.ToDouble(txt112.Text) >= 1 && Convert.ToDouble(txt112.Text) <= 4))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La longitude doit être comprise entre 1 et 4.", "Erreur",
                               MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return functionReturnValue;
                            }

                        }
                        if (!General.IsNumeric(txt115.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La latitude doit être de type numérique.", "Erreur de saisie",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SSTab1.SelectedTab = SSTab1.Tabs[0];
                            return functionReturnValue;
                        }

                        if (!(Convert.ToDouble(txt115.Text) >= 47 && Convert.ToDouble(txt115.Text) <= 50))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La longitude doit être comprise entre 47 et 50.", "Erreur",
                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }

                        if (string.IsNullOrEmpty(txtCodePostal.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code postal est obligatoire.", "",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }
                        if (string.IsNullOrEmpty(txtVille.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La ville est obligatoire.", "",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }
                        if (string.IsNullOrEmpty(txtAdresse.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse est obligatoire.", "",
                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }
                    }

                    if (string.IsNullOrEmpty(txtCodeTVA.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir le taux de TVA applicable.", "Validation annulée",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[2];
                        Cursor.Current = Cursors.Arrow;
                        txtCodeTVA.Focus();
                        return functionReturnValue;
                    }

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    if (Check_23.Checked && txt_173.Text == "")
                    {
                        CustomMessageBox.Show("La case à cocher 'Adresse mail obligatoire' est cochée, vous devez saisir une adresse mail.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return false;
                    }
                    //===> Fin Modif Mondir

                    //===  Modif des infos du compte de l 'immeuble
                    //===  Si le compte existe déjà, alors, mise à jour
                    string codeImmeuble = txtCodeImmeuble.Text;
                    string code1 = txtCode1.Text;
                    string codeImm = txtCodeImmeuble.Text;
                    if (!string.IsNullOrEmpty(General.nz(txtNCompte.Text, "").ToString()))
                    {
                        //=== Si le compte tiers existe : on enlève le C du départ pour
                        //=== envoyer le bon compte en paramètre sans modifier la
                        //=== fonction  fc_prcAjoutTiers()
                        string scompte = General.Right(txtNCompte.Text, txtNCompte.Text.Length - 1);
                        txtNCompte.Text = SAGE.fc_prcAjoutTiers(0,
                           ref scompte, ref codeImmeuble,
                             ref code1, txtAdresse.Text, txtAdresse2_IMM.Text, txtCodePostal.Text, txtVille.Text,
                            sAdresseClient, codeImmeubleCurrent: txtCodeImmeuble.Text);
                        //Commented by Mondir ===> No Needs to change the text of the controls, and this caused a bug ===> https://groupe-dt.mantishub.io/view.php?id=1652
                        //Passing by reference a text of a TextBox will not change the text of this control in VB6 !
                        //txtCode1.Text = code1;
                    }
                    else
                    {
                        txtNCompte.Text = SAGE.fc_prcAjoutTiers(0, ref codeImmeuble, ref codeImm,
                             ref code1, txtAdresse.Text, txtAdresse2_IMM.Text, txtCodePostal.Text, txtVille.Text,
                            sAdresseClient, "",
                            "", "", "", "", "", txtCodeImmeuble.Text, codeImmeubleCurrent: txtCodeImmeuble.Text);
                        //Commented by Mondir ===> No Needs to change the text of the controls, and this caused a bug ===> https://groupe-dt.mantishub.io/view.php?id=1652
                        //Passing by reference a text of a TextBox will not change the text of this control in VB6 !
                        //txtCode1.Text = code1;

                    }

                    //=============Ajout ou modif des infos des comptes des copros
                    ssGridCopro.UpdateData();
                    rsCopro = new DataTable();
                    modAdorsCopro = new ModAdo();
                    rsCopro = modAdorsCopro.fc_OpenRecordSet(
                        "SELECT ImmeublePart.CodeImmeuble,ImmeublePart.Nom,ImmeublePart.CodeParticulier," +
                        "ImmeublePart.Adresse,ImmeublePart.CodePostal,ImmeublePart.NCompte," +
                        "ImmeublePart.Tel,ImmeublePart.Fax,ImmeublePart.eMail, " +
                        "ImmeublePart.Ville FROM ImmeublePart WHERE ImmeublePart.CodeImmeuble='" +
                        txtCodeImmeuble.Text + "' AND CompteCree='1'");

                    if (rsCopro.Rows.Count > 0)
                    {
                        foreach (DataRow rsCoproRow in rsCopro.Rows)
                        {
                            if (!string.IsNullOrEmpty(rsCoproRow["Ncompte"] + ""))
                            {
                                sCompteCopro = General.Right(rsCoproRow["Ncompte"].ToString(),
                                    rsCoproRow["Ncompte"].ToString().Length - 1);
                            }
                            else
                            {
                                sCompteCopro = rsCoproRow["CodeParticulier"] + rsCoproRow["CodeImmeuble"].ToString();
                            }
                            string sCompteCopro1 = sCompteCopro;
                            string sCompteCopro2 = sCompteCopro;
                            string sCompteCopro3 = sCompteCopro;

                            if (string.IsNullOrEmpty(rsCoproRow["Ncompte"] + ""))
                            {

                                SAGE.fc_prcAjoutTiers(1, ref sCompteCopro, ref sCompteCopro, ref sCompteCopro,
                                    rsCoproRow["Adresse"] + "", "", rsCoproRow["CodePostal"] + "",
                                    rsCoproRow["Ville"] + "", rsCoproRow["Nom"] + "", rsCoproRow["Tel"] + "",
                                    rsCoproRow["fax"] + "", "", rsCoproRow["Email"] + "",
                                    rsCoproRow["CodeParticulier"] + "", rsCoproRow["CodeImmeuble"] + "",
                                    rsCoproRow["CodeParticulier"] + "", codeImmeubleCurrent: txtCodeImmeuble.Text);
                            }
                            else
                            {
                                SAGE.fc_prcAjoutTiers(1, ref sCompteCopro, ref sCompteCopro, ref sCompteCopro,
                                    rsCoproRow["Adresse"] + "", "", rsCoproRow["CodePostal"] + "",
                                    rsCoproRow["Ville"] + "", rsCoproRow["Nom"] + "", rsCoproRow["Tel"] + "",
                                    rsCoproRow["fax"] + "", "", rsCoproRow["Email"] + "",
                                    rsCoproRow["CodeParticulier"] + "", rsCoproRow["CodeImmeuble"] + "", codeImmeubleCurrent: txtCodeImmeuble.Text);
                            }

                        }
                    }
                    rsCopro?.Dispose();
                    rsCopro = null;
                    //=============Fin d'ajout des copro.

                    // Me.ssGridCopro.ReBind
                    fc_ssGridCopro(txtCodeImmeuble.Text);

                    txtAnalytiqueImmeuble.Text = General.Left(txtNCompte.Text, 13);
                    txtCompteDivers.Text = General.Left(txtCode1.Text, 17);

                    //==== mise à jour des infos immeubles.
                    fc_maj(sCode);

                    if (fc_SavePrestations() == false)
                    {
                        functionReturnValue = false;
                    }
                }

                Cursor.Current = Cursors.Arrow;

                //=== valide les gilles.
                ssGridImmCorrespondant.UpdateData();
                GridIMRE.UpdateData();
                ssGridCopro.UpdateData();
                GridCompteurs.UpdateData();
                //===.ssGridGestionnaire.Update
                ssIntervention.UpdateData();
                ssDevis.UpdateData();
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                GridIDA.UpdateData();
                //===> Fin Modif Mondir
                //=== debloque les controles du formulaires.
                fc_DeBloqueForm();

                //=== creation des dossiers.
                Video.fc_CreeDossierImm(txtCodeImmeuble.Text);

                if (General.sTomtom == "1")
                {

                    Label2510.Text = modTt.fc_IdTomtom(txtCodeImmeuble.Text, Label2510.Text);

                }


                //=== deflage le controle de modifs.
                blModif = false;
                ssGridBE.UpdateData();

                fc_CtrlImmatriculation();

                //===26 06 2018, historise la mise à jour.
                fc_HistoListeRouge();

                //=== stocke la position de la fiche immeuble.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);

                View.Theme.Theme.AfficheAlertSucces();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "");
                return false;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool FC_insert()
        {
            bool functionReturnValue = false;


            //=== creation d'un nouveau client.
            functionReturnValue = true;

            ///===========> Tested
            if (txtCodeImmeuble.Text.Contains(" "))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne devez pas saisir d'espace dans le code immeuble. Veuillez le modifier.",
                    "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodeImmeuble.Focus();
                return functionReturnValue;
            }

            if (txtCodeImmeuble.Text.Contains("'"))
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne devez pas saisir d'apostrophe dans le code immeuble. Veuillez le modifier.",
                    "Création annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtCodeImmeuble.Focus();
                return functionReturnValue;
            }

            SQL = "INSERT INTO IMMEUBLE ( codeimmeuble,DateCreation, CreePar )";
            SQL = SQL + " VALUES (";
            SQL = SQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',";
            SQL = SQL + "'" + DateTime.Now.ToString(General.FormatDateSansHeureSQL) + "',";
            SQL = SQL + "'" + General.Left(General.fncUserName(), 50) + "'";
            //SQL = SQL && "'" && gFr_DoublerQuote(.txtRaisonSocial_IMM) && "'"
            SQL = SQL + ")";
            int x = General.Execute(SQL);

            //=== debloque les onglets.
            fc_DeBloqueForm();
            txtSelCodeImmeuble.Text = txtCodeImmeuble.Text;

            if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
            {

                txt97.Text = ModP1Gaz.cUoGDP;
                //=== code UO.
                txt96.Text = ModP1Gaz.fc_InsertAxeP1(txtCodeImmeuble.Text);
                //===Code Affaire.
                txt95.Text = ModP1Gaz.cCodeNumOrdre;
                //=== CodeNumordre.
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm(string sCommande = "")
        {
            try
            {
                //.SSTab1.Tab = 0
                SSTab1.Enabled = true;
                cmdAjouter.Enabled = true;
                CmdRechercher.Enabled = true;
                cmdSupprimer.Enabled = true;
                CmdSauver.Enabled = true;
                cmdRechercheClient.Enabled = true;
                cmdRechercheImmeuble.Enabled = true;
                // .cmdRechercheRaisonSocial.Enabled = True
                cmdRechercheImmeuble.Enabled = true;
                //desactive le mode modeAjout
                blnAjout = false;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_DeBloqueForm");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="lPosition"></param>
        /// <param name="blnRemove"></param>

        bool ssGridCoproIsLoading = false;
        private void fc_ssGridCopro(string sCode, int lPosition = 0, bool blnRemove = true)
        {
            using (var tmpModAdo = new ModAdo())
            {
                int i = 0;
                //- recherche du libelle qualification pour le code Copro. pour alimenter
                //le champ qualite de la grille ssGridCopro.

                General.sSQL = "";
                General.sSQL = "Select LibelleQualif from SpecifQualif where QualifPart='1'";
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    sQualiteCopro = General.rstmp.Rows[0]["LibelleQualif"].ToString();
                }
                General.sSQL = "";
                General.sSQL =
                    "SELECT ImmeublePart.CodeImmeuble,ImmeublePart.CompteCree,ImmeublePart.NCompte, ImmeublePart.CodeParticulier," +
                    " ImmeublePart.Nom, ImmeublePart.CodeQualif_QUA,'' as Qualification  " +
                    " ,Adresse, CodePostal,Ville ," + " ImmeublePart.Tel, ImmeublePart.TelPortable_IMP," +
                    " ImmeublePart.Fax ," + " ImmeublePart.eMail," + " ImmeublePart.Note_IMP" + " FROM ImmeublePart  " +
                    " where ImmeublePart.CodeImmeuble= '" + sCode + "'" + " order by ImmeublePart.CodeParticulier";

                modAdorsCopro = new ModAdo();
                rsCopro = modAdorsCopro.fc_OpenRecordSet(General.sSQL);
                this.ssGridCopro.DataSource = rsCopro;
                ssGridCoproIsLoading = true;
                ssGridCopro.UpdateData();
                ssGridCoproIsLoading = false;

                fc_DropQualiteFact();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropQualiteFact()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Qualification.CodeQualif, Qualification.Qualification" +
                           " FROM Qualification where Facturable_QUA = 1";

            if (this.ssDropFacturation.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropFacturation, General.sSQL, "CodeQualif", true);


            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_maj(string sCode)
        {

            SQL = " Update immeuble set ";
            SQL = SQL + "Adresse='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse.Text) + "',";
            SQL = SQL + "Adresse2_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresse2_IMM.Text) + "',";
            SQL = SQL + "AdresseFact1_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresseFact1_IMM.Text) + "',";
            SQL = SQL + "AdresseFact2_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresseFact2_IMM.Text) + "',";
            SQL = SQL + "AdresseFact3_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresseFact3_IMM.Text) + "',";
            SQL = SQL + "AdresseFact4_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtAdresseFact4_IMM.Text) + "',";
            SQL = SQL + "AngleRue='" + StdSQLchaine.gFr_DoublerQuote(txtAngleRue.Text) + "',";
            SQL = SQL + "Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "',";
            SQL = SQL + "CodeAcces1='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces1.Text) + "',";

            SQL = SQL + "Immatriculation='" + StdSQLchaine.gFr_DoublerQuote(txt159.Text) + "',";

            SQL = SQL + "CodeAcces2='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces2.Text) + "',";
            SQL = SQL + "CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',";
            SQL = SQL + "CodePostal='" + StdSQLchaine.gFr_DoublerQuote(txtCodePostal.Text) + "',";
            SQL = SQL + "CodePostal_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodePostal_IMM.Text) + "',";

            //===> Mondir le 29.11.2020 pour ajouter les modfis de la verion V29.11.2020
            SQL = SQL + "AdresseEnvoi1='" + StdSQLchaine.gFr_DoublerQuote(txt165.Text) + "',";
            SQL = SQL + "AdresseEnvoi2='" + StdSQLchaine.gFr_DoublerQuote(txt166.Text) + "',";
            SQL = SQL + "AdresseEnvoi3='" + StdSQLchaine.gFr_DoublerQuote(txt167.Text) + "',";
            SQL = SQL + "AdresseEnvoi4='" + StdSQLchaine.gFr_DoublerQuote(txt168.Text) + "',";
            SQL = SQL + "AdresseEnvoi5='" + StdSQLchaine.gFr_DoublerQuote(txt169.Text) + "',";

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Commented
            //SQL = SQL + "AdresseEnvoi6='" + StdSQLchaine.gFr_DoublerQuote(txt170.Text) + "',";

            SQL = SQL + "AdresseEnvoiCP='" + StdSQLchaine.gFr_DoublerQuote(txt171.Text) + "',";
            SQL = SQL + "AdresseEnvoiVille='" + StdSQLchaine.gFr_DoublerQuote(txt172.Text) + "',";

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            SQL = SQL + "SIREN='" + StdSQLchaine.gFr_DoublerQuote(txt_163.Text).Trim() + "',";
            SQL = SQL + "TiersPayeur='" + StdSQLchaine.gFr_DoublerQuote(txt_164.Text).Trim() + "',";
            //SQL = SQL & "SIRET='" & Trim(gFr_DoublerQuote(txt(164).Text)) & "',"
            SQL = SQL + "Mail='" + StdSQLchaine.gFr_DoublerQuote(txt_173.Text).Trim() + "',";
            SQL = SQL + "CAT_Code='" + StdSQLchaine.gFr_DoublerQuote(ssCombo_23.Text).Trim() + "',";
            if (Check_23.Checked)
            {
                SQL = SQL + "MailObligatoire=1,";
            }
            else
            {
                SQL = SQL + "MailObligatoire=0,";
            }

            if (Check22.Checked)
                SQL = SQL + "UtiliseAdresseEnvoi=1,";
            else
                SQL = SQL + "UtiliseAdresseEnvoi=0,";
            //===> Fin Modif Mondir

            SQL = SQL + "CodeReglement_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodeReglement_IMM.Text) + "',";

            SQL = SQL + "CommentaireFacture1='" + StdSQLchaine.gFr_DoublerQuote(txtCommentaireFacture1.Text) + "',";
            SQL = SQL + "CommentaireFacture2='" + StdSQLchaine.gFr_DoublerQuote(txtCommentaireFacture2.Text) + "',";

            SQL = SQL + "CodeTVA='" + StdSQLchaine.gFr_DoublerQuote(txtCodeTVA.Text) + "',";
            SQL = SQL + "CptAppelFond='" + StdSQLchaine.gFr_DoublerQuote(txtCptAppelFond.Text) + "',";
            SQL = SQL + "HorairesLoge='" + StdSQLchaine.gFr_DoublerQuote(txtHorairesLoge.Text) + "',";
            SQL = SQL + "NCompte='" + StdSQLchaine.gFr_DoublerQuote(txtNCompte.Text) + "',";
            SQL = SQL + "CompteDivers='" + StdSQLchaine.gFr_DoublerQuote(txtCompteDivers.Text) + "',";
            SQL = SQL + "Observations='" + StdSQLchaine.gFr_DoublerQuote(txtObservations.Text) + "',";
            SQL = SQL + "Consigne='" + StdSQLchaine.gFr_DoublerQuote(txt161.Text) + "',";

            //        SQL = SQL & "ObservationComm_IMM='" & nz(gFr_DoublerQuote(txtObservationComm_IMM.Text), "") & "',"
            // SQL = SQL & "RaisonSocial_IMM='" & nz(gFr_DoublerQuote(txtRaisonSocial_IMM), "") & "',"
            SQL = SQL + "RefClient='" + StdSQLchaine.gFr_DoublerQuote(txtRefClient.Text) + "',";
            SQL = SQL + "P1='" + (Check41.CheckState == CheckState.Checked ? 1 : 0) + "',";
            SQL = SQL + "P2='" + (Check42.CheckState == CheckState.Checked ? 1 : 0) + "',";
            SQL = SQL + "P3='" + (Check43.CheckState == CheckState.Checked ? 1 : 0) + "',";
            SQL = SQL + "P4='" + (Check44.CheckState == CheckState.Checked ? 1 : 0) + "',";
            SQL = SQL + "SansReleveCompte='" +
                  General.nz(ChkSansReleveCompte.CheckState == CheckState.Checked ? 1 : 0, 0) + "',";
            SQL = SQL + "ListeRouge='" + General.nz(ChkListeRouge.CheckState == CheckState.Checked ? 1 : 0, 0) + "',";



            SQL = SQL + "Prioritaire=" + (chkPrioritaire.CheckState == CheckState.Checked ? 1 : 0) + ",";
            SQL = SQL + "DTAPositif=" + (chkDTAPositif.CheckState == CheckState.Checked ? 1 : 0) + ",";
            // SQL = SQL & "ReceptionDTA=" & chkReceptionDTA.value & ","
            SQL = SQL + "DTANegatif=" + (chkDTANégatif.CheckState == CheckState.Checked ? 1 : 0) + ",";
            SQL = SQL + "ChaudCondensation=" + (Check12.CheckState == CheckState.Checked ? 1 : 0) + ",";

            SQL = SQL + "DTAPositifSo=" + (chkDTAPositfSO.CheckState == CheckState.Checked ? 1 : 0) + ",";

            SQL = SQL + "Avocat=" + (Check16.CheckState == CheckState.Checked ? 1 : 0) + ",";
            SQL = SQL + "MiseEnDemeure=" + (Check20.CheckState == CheckState.Checked ? 1 : 0) + ",";
            SQL = SQL + "Resilie=" + (Check21.CheckState == CheckState.Checked ? 1 : 0) + ",";

            SQL = SQL + "situation='" + StdSQLchaine.gFr_DoublerQuote(txtSituation.Text) + "',";

            //===> Mondir le 07.05.2021, https://groupe-dt.mantishub.io/view.php?id=1381#c5964
            //SQL = SQL + "SitBoiteAClef='" + StdSQLchaine.gFr_DoublerQuote(txtSituation.Text) + "', ";
            //===> Mondir le 14.05.2021, demande de modification par Xavier, https://groupe-dt.mantishub.io/view.php?id=1381#c6039
            SQL = SQL + "SitBoiteAClef='" + StdSQLchaine.gFr_DoublerQuote(txtSitBoiteAClef.Text) + "', ";
            //===> Fin Modif Mondir

            SQL = SQL + "CodeDepanneur='" + StdSQLchaine.gFr_DoublerQuote(this.SSOleDBCmbDep1.Text) + "',";
            SQL = SQL + "CodeDepanneur1='" + StdSQLchaine.gFr_DoublerQuote(this.lblMatriculeDep2.Text) + "',";
            SQL = SQL + "ville='" + StdSQLchaine.gFr_DoublerQuote(txtVille.Text) + "',";
            //SQL = SQL & "CodeDispatcheur='" & gFr_DoublerQuote(Me.SSOleDBCmbDispatch.value) & "',"

            //If UCase(adocnn.DefaultDatabase) = UCase(cDelostal) Then
            if (General.sVersionImmParComm == "1")
            {
                SQL = SQL + "CodeRespExploit='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ssCombo13.Text), "") + "',";
                SQL = SQL + "CodeCommercial='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ssCombo15.Text), "") + "',";
                if (General.sChefSecteurLibre == "1")
                {
                    SQL = SQL + "CodeChefSecteur='" + StdSQLchaine.gFr_DoublerQuote(ssCombo22.Text) + "',";
                }
                else
                {
                    SQL = SQL + "CodeChefSecteur='" + StdSQLchaine.gFr_DoublerQuote(txt104.Text) + "',";
                }
            }
            else
            {
                SQL = SQL + "CodeCommercial='" + General.nz(StdSQLchaine.gFr_DoublerQuote(this.lblCodeCommercial.Text), "") + "',";
                SQL = SQL + "CodeChefSecteur='" + StdSQLchaine.gFr_DoublerQuote(txtCodeChefSecteur.Text) + "',";
            }

            SQL = SQL + "CodeRamoneur ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo21.Text) + "',";

            //If sTomtom = "1" Then
            SQL = SQL + "Latitude='" + txt115.Text + "',";
            SQL = SQL + "Longitude='" + txt112.Text + "',";
            SQL = SQL + "AdresseGeocode ='" + StdSQLchaine.gFr_DoublerQuote(txt116.Text) + "',";
            //SQL = SQL & "LogTomtom='" & Label25(10) & "',"
            //End If

            SQL = SQL + "ModifierLe='" + DateTime.Now.ToString(General.FormatDateSQL) + "',";
            SQL = SQL + "ModifierPar='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "',";

            //SQL = SQL & "Gardien='" & gFr_DoublerQuote(txtGardien.Text) & "',"
            SQL = SQL + "Gardien='" + StdSQLchaine.gFr_DoublerQuote(txt154.Text) + "',";
            //SQL = SQL & "TelGardien='" & gFr_DoublerQuote(txtTelGardien.Text) & "',"
            SQL = SQL + "TelGardien='" + StdSQLchaine.gFr_DoublerQuote(txt155.Text) + "',";
            //SQL = SQL & "FaxGardien='" & gFr_DoublerQuote(txtFaxGardien.Text) & "',"
            SQL = SQL + "FaxGardien='" + StdSQLchaine.gFr_DoublerQuote(txt157.Text) + "',";
            SQL = SQL + "VilleFact_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtVilleFact_IMM.Text) + "',";
            SQL = SQL + "SEC_Code='" + StdSQLchaine.gFr_DoublerQuote(txtSEC_Code.Text) + "',";
            SQL = SQL + "CodeGestionnaire='" + StdSQLchaine.gFr_DoublerQuote(txtCodeGestionnaire.Text) + "',";
            //        SQL = SQL & "Energie='" & gFr_DoublerQuote(cmbEnergie.value) & "',"
            SQL = SQL + "SpecifAcces='" + StdSQLchaine.gFr_DoublerQuote(SpecifAcces.Text) + "'";
            SQL = SQL + " where CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

            if (!string.IsNullOrEmpty(txtCode1.Text))
            {
                txtCode1.ReadOnly = true;
            }
            else
            {
                txtCode1.ReadOnly = false;
            }

            int x = General.Execute(SQL);

            //=== modif du 1 mars 2010, ajout des onglets gaz de paris.
            if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
            {
                // fc_MajOngletGaz

            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_SavePrestations()
        {
            bool functionReturnValue = false;
            DataTable adotemp = default(DataTable);
            ModAdo modAdoadotemp;
            try
            {
                functionReturnValue = true;
                SSOleDBGrid1.UpdateData();
                if (boolErreurValidate == true)
                {
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                if (!string.IsNullOrEmpty(txtValorisation.Text) && !General.IsNumeric(txtValorisation.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Validation annulée, Vous devez inserer une valeur numerique dans le champs valorisation",
                        "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtValorisation.Focus();
                    SSTab1.SelectedTab = SSTab1.Tabs[5];
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                else if (!string.IsNullOrEmpty(txtDuree.Text) && !General.IsNumeric(txtDuree.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation annulée, Vous devez inserer une valeur numerique dans le champs duree",
                        "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDuree.Focus();
                    SSTab1.SelectedTab = SSTab1.Tabs[5];
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                else if (SSOleDBGrid1.Rows.Count == 0)
                {
                    return functionReturnValue;
                }
                else
                {
                    //----recherche si éxiste un enregistrement avec ce code immeuble dans PreparationIntervention.
                    modAdoadotemp = new ModAdo();
                    adotemp = modAdoadotemp.fc_OpenRecordSet(
                        "SELECT * from preparationintervention where codeimmeuble ='" + txtCodeImmeuble.Text + "'");
                    if (adotemp.Rows.Count > 0)
                    {
                        //----Si nous sommes en mode ajout et que ce code immeuble éxiste déja
                        //----nous annulons la validation
                        //----si ce codeimmeuble éxiste alors on les supprime.
                        //foreach (DataRow adotempRow in adotemp.Rows)
                        //{
                        //    adotemp.Rows.Remove(adotempRow);
                        //}
                        for (int i = 0; i < adotemp.Rows.Count; i++)
                            adotemp.Rows[i].Delete();
                        int xx = modAdoadotemp.Update();
                    }
                    FournisTablePrepInterv(adotemp, modAdoadotemp);
                    modAdoadotemp.Dispose();
                    //----si erreur dans la transaction redige vers err: et annule celle-çi.

                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="adotemp"></param>
        /// <param name="modAdo"></param>
        private void FournisTablePrepInterv(DataTable adotemp, ModAdo modAdo)
        {

            bool @bool = false;
            int i = 0;
            object bm = null;
            for (i = 0; i <= SSOleDBGrid1.Rows.Count - 1; i++)
            {
                int No = 0;
                var NewRow = adotemp.NewRow();
                SqlCommandBuilder cb = new SqlCommandBuilder(modAdo.SDArsAdo);
                modAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                modAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                modAdo.SDArsAdo.InsertCommand = insertCmd;

                modAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {
                    if (e.StatementType == StatementType.Insert)
                    {
                        if (No == 0)
                        {
                            var cc = e.Command.Parameters["@ID"].Value;
                            No = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                            NewRow["No"] = No;
                            SSOleDBGrid1.Rows[i].Cells["No"].Value = No;
                        }
                    }
                });

                NewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                NewRow["Nompersonne"] = General.gsUtilisateur;
                NewRow["Intervenant"] = SSOleDBGrid1.Rows[i].Cells["Intervenant"].Text;
                NewRow["CodeArticle"] = SSOleDBGrid1.Rows[i].Cells["CodeArticle"].Text;

                if (SSOleDBGrid1.Rows[i].Cells["Janvier"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Janvier"].Value) == true)
                {
                    NewRow["janvier"] = 1;
                }
                else
                {
                    NewRow["janvier"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Fevrier"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Fevrier"].Value) == true)
                {
                    NewRow["Fevrier"] = 1;
                }
                else
                {
                    NewRow["Fevrier"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Mars"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Mars"].Value) == true)
                {
                    NewRow["Mars"] = 1;
                }
                else
                {
                    NewRow["Mars"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Avril"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Avril"].Value) == true)
                {
                    NewRow["Avril"] = 1;
                }
                else
                {
                    NewRow["Avril"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Mai"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Mai"].Value))
                {
                    NewRow["Mai"] = 1;
                }
                else
                {
                    NewRow["Mai"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Juin"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Juin"].Value))
                {
                    NewRow["Juin"] = 1;
                }
                else
                {
                    NewRow["Juin"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Juillet"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Juillet"].Value))
                {
                    NewRow["Juillet"] = 1;
                }
                else
                {
                    NewRow["Juillet"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Aout"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Aout"].Value))
                {
                    NewRow["Aout"] = 1;
                }
                else
                {
                    NewRow["Aout"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Septembre"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Septembre"].Value))
                {
                    NewRow["Septembre"] = 1;
                }
                else
                {
                    NewRow["Septembre"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Octobre"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Octobre"].Value))
                {
                    NewRow["Octobre"] = 1;
                }
                else
                {
                    NewRow["Octobre"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Novembre"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Novembre"].Value))
                {
                    NewRow["Novembre"] = 1;
                }
                else
                {
                    NewRow["Novembre"] = 0;
                }

                if (SSOleDBGrid1.Rows[i].Cells["Decembre"].Value != null && Convert.ToBoolean(SSOleDBGrid1.Rows[i].Cells["Decembre"].Value))
                {
                    NewRow["Decembre"] = 1;
                }
                else
                {
                    NewRow["Decembre"] = 0;
                }

                NewRow["Commentaire"] = SSOleDBGrid1.Rows[i].Cells["Commentaire"].Text;

                //==== modif du 10 Janvier, la valorisation et la durée se calculent
                //=== pour chaque intervenant.
                //If txtValorisation <> "" And IsNumeric(txtValorisation) Then
                //    txtValorisation = FncArrondir(txtValorisation)
                //    adotemp!valorisation = txtValorisation

                //End If

                //If txtDuree <> "" And IsNumeric(txtDuree) Then
                //    adotemp!Duree = txtDuree
                //End If

                NewRow["Valorisation"] = General.nz(SSOleDBGrid1.Rows[i].Cells["valorisation"].Text, 0);
                NewRow["Duree"] = General.nz(SSOleDBGrid1.Rows[i].Cells["Duree"].Text, 0);

                adotemp.Rows.Add(NewRow.ItemArray);

                int xx = modAdo.Update();

            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object fc_CtrlImmatriculation()
        {
            object functionReturnValue = null;
            int i = 0;
            string stemp = null;

            try
            {
                if (!string.IsNullOrEmpty(txt159.Text))
                {
                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Commented
                    //if (txt159.Text.Length != 9)
                    //{
                    //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    //        "L'Immatriculation doit être composée de 9 caractères(2 lettres suivi de 7 chiffres).",
                    //        "Erreur sur l'Immatriculation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //    return functionReturnValue;
                    //}

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    //=== modif du 05 01 2021, immatriculation remplacé par le numéro de TVA intra.
                    if (txt159.Text.Length != 13)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Le N° de TVA Intracommunautaire doit être composée de 13 caractères(2 lettres suivi de 11 chiffres).",
                            "Erreur sur N°TVA intra.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return functionReturnValue;
                    }
                    //===> Fin Modif Mondir

                    var Regex = new Regex("\\w{2}\\d{7}");
                    if (!Regex.IsMatch(txt159.Text))
                    {
                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        //    "L'Immatriculation doit être composée de 9 caractères(2 lettres suivi de 7 chiffres).",
                        //    "Erreur sur l'Immatriculation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Le N° de TVA Intracommunautaire doit être composée de 13 caractères(2 lettres suivi de 11 chiffres).",
                            "Erreur sur N°TVA intra.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        //===> Fin Modif Mondir
                        return functionReturnValue;
                    }

                    //for (i = 1; i < txt159.Text.Length; i++)
                    //{

                    //    stemp = General.Mid(txt159.Text, i, 1);
                    //    if (i == 1 || i == 2)
                    //    {
                    //        if (General.IsNumeric(stemp))
                    //        {
                    //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    //                "L'Immatriculation doit être composée de 9 caractères(2 lettres suivi de 7 chiffres).",
                    //                "Erreur sur l'Immatriculation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //            break;
                    //        }
                    //    }

                    //    if (i > 2)
                    //    {
                    //        if (!General.IsNumeric(stemp))
                    //        {
                    //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    //                "L'Immatriculation doit être composée de 9 caractères(2 lettres suivi de 7 chiffres).",
                    //                "Erreur sur l'Immatriculation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //            break;
                    //        }
                    //    }
                    //}
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_CtrlImmatriculation");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_HistoListeRouge()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors;

            try
            {
                if (Check13.CheckState != ChkListeRouge.CheckState)
                {

                    sSQL = "SELECT     Codeimmeule, DateMaj, MiseAjourPar, ListeRouge, ID" +
                           " From HISTO_ListeRouge WHERE     (ID = 0)";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    var NewRow = rs.NewRow();

                    NewRow["Codeimmeule"] = txtCodeImmeuble.Text;
                    NewRow["DateMaj"] = DateTime.Now;
                    NewRow["MiseAjourPar"] = General.fncUserName();
                    NewRow["ListeRouge"] = ChkListeRouge.CheckState == CheckState.Checked ? 1 : 0;
                    rs.Rows.Add(NewRow);
                    int xx = modAdors.Update();
                    modAdors.Dispose();
                }

                Check13.CheckState = ChkListeRouge.CheckState;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_HistoListeRouge");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DroitListeRouge()
        {
            string sSQL = null;
            try
            {
                if (General.sDroitListeRouge == "1")
                {

                    sSQL = "SELECT     AUT_Objet" + " From AUT_Autorisation " + " WHERE     (AUT_Formulaire = '" +
                           Variable.cUserDocImmeuble + "') AND (AUT_Objet = 'ListeRouge')" + " AND (AUT_Nom = '" +
                           StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')";
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        ChkListeRouge.Enabled = true;
                    }
                    else
                    {
                        ChkListeRouge.Enabled = false;
                    }
                }
                else
                {
                    ChkListeRouge.Enabled = true;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_DroitListeRouge");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ClearControleGaz()
        {

            txt97.Text = "";
            //txtCode1 = ""
            //==== Voir ou sont créer les codes clients.
            ///' sReq = sReq & ", CodeGestionnaire = '" & gFr_DoublerQuote(txtCodeGestionnaire.Text) & "'"
            //ModifieLe = '" & dtModifieLe & "'"
            //sReq = sReq & ", ModifiePar = '" & fncUserName & "'"
            txt0.Text = "";
            txt98.Text = "";
            txt15.Text = "";
            txt13.Text = "";
            txt14.Text = "";
            txt11.Text = "";
            txt10.Text = "";
            txt9.Text = "";
            txt22.Text = "";
            txt12.Text = "";
            txt21.Text = "";
            txt19.Text = "";
            txt18.Text = "";
            txt17.Text = "";
            txt23.Text = "";
            txt31.Text = "";
            txt67.Text = "";
            txt68.Text = "";
            txt20.Text = "";
            txt24.Text = "";
            txt7.Text = "";
            txt6.Text = "";
            txt8.Text = "";
            txt16.Text = "";
            txt25.Text = "";
            txt26.Text = "";
            txt5.Text = "";
            txt1.Text = "";
            txt3.Text = "";
            txt2.Text = "";
            txt4.Text = "";
            txt27.Text = "";
            txt28.Text = "";
            txt30.Text = "";
            txt29.Text = "";
            txt34.Text = "";
            txt99.Text = "";
            txt46.Text = "";
            txt40.Text = "";
            txt42.Text = "";
            txt43.Text = "";
            txt44.Text = "";
            txt39.Text = "";
            txt36.Text = "";
            txt33.Text = "";
            txt35.Text = "";
            txt45.Text = "";
            txt60.Text = "";
            txt32.Text = "";
            txt58.Text = "";
            txt57.Text = "";
            txt56.Text = "";
            txt48.Text = "";
            txt55.Text = "";
            txt47.Text = "";
            txt54.Text = "";
            txt41.Text = "";
            txt53.Text = "";
            txt38.Text = "";
            txt52.Text = "";
            txt37.Text = "";
            txt51.Text = "";
            txt49.Text = "";
            txt50.Text = "";
            txt63.Text = "";

            Label4114.Text = "";

            ssCombo10.Text = "";
            ssCombo11.Text = "";
            ssCombo0.Text = "";
            ssCombo1.Text = "";

            ssCombo6.Text = "";
            ssCombo7.Text = "";
            ssCombo4.Text = "";
            ssCombo5.Text = "";
            ssCombo3.Text = "";
            ssCombo2.Text = "";
            ssCombo9.Text = "";
            ssCombo8.Text = "";


            Check1.CheckState = CheckState.Unchecked;

            Check2.CheckState = CheckState.Unchecked;
            Check3.CheckState = CheckState.Unchecked;
            Check4.CheckState = CheckState.Unchecked;
            Check5.CheckState = CheckState.Unchecked;
            Check9.CheckState = CheckState.Unchecked;
            Check10.CheckState = CheckState.Unchecked;
            Check11.CheckState = CheckState.Unchecked;



        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_annuler()
        {
            //    With Me
            //        If blModif = True Then
            //             Select Case MsgBox("Voulez-vous enregistrer vos modifications ?", vbYesNoCancel)
            //                     Case vbYes
            //                         fc_sauver
            //
            //                     Case vbNo
            //                         ' Vider devis en cours
            //                     Case Else
            //                         Exit Sub 'Reprendre saisie en cours
            //                 End Select
            //         End If
            //         fc_InitialiseGrille
            //         fc_clear
            //         fc_BloqueForm
            //
            //    End With
            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                // charge les enregistremants
                fc_ChargeEnregistrement(ModParametre.sNewVar);
            }
            else
            {
                fc_InitialiseGrille();
                fc_Clear();
                fc_BloqueForm();
            }
            frmResultat.Visible = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {
            try
            {
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                SSTab1.Enabled = false;
                cmdSupprimer.Enabled = false;
                // mode ajout
                if (sCommande.ToUpper() == "Ajouter".ToUpper())
                {
                    cmdRechercheClient.Enabled = false;
                    cmdRechercheImmeuble.Enabled = false;
                    //.cmdRechercheRaisonSocial.Enabled = False
                    cmdAjouter.Enabled = true;
                    CmdRechercher.Enabled = false;
                    CmdSauver.Enabled = true;
                    //active le mode modeAjout
                    blnAjout = true;
                }
                else
                {
                    cmdRechercheClient.Enabled = true;
                    cmdRechercheImmeuble.Enabled = true;
                    //.cmdRechercheRaisonSocial.Enabled = True
                    CmdRechercher.Enabled = true;
                    CmdSauver.Enabled = false;
                    //desactive le mode modeAjout
                    blnAjout = false;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_BloqueForm");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Groupe()
        {
            //=== modif du 13 12 2015.
            string sSQL = null;
            try
            {
                if (!string.IsNullOrEmpty(Label2511.Text))
                {
                    return;
                }

                sSQL = "SELECT     CRCL_GroupeClient.CRCL_RaisonSocial" + " FROM         Table1 INNER JOIN" +
                       " CRCL_GroupeClient ON Table1.CRCL_Code = CRCL_GroupeClient.CRCL_Code " +
                       " WHERE  Table1.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                using (var tmpModAdo = new ModAdo())
                    Label2511.Text = tmpModAdo.fc_ADOlibelle(sSQL);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_Groupe");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeEnregistrement(string sCode)
        {
            DataTable rsSecteur = default(DataTable);
            ModAdo rsSecteurAdo = new ModAdo();
            DataTable rsDep2 = default(DataTable);
            string sSQLgmao = null;
            string sSQLPart = null;

            //SSTab2.Tab = 0
            //SSTab1.Tab = 0
            fc_InitialiseGrille();
            //  fc_ComboTRTP
            General.sSQL = "";
            General.sSQL =
                "SELECT Immeuble.*,Table1.Prioritaire AS Prior,TechReleve.CodeEnergie1, TechReleve.CodeImmeuble AS CodeImmReleve FROM IMMEUBLE ";
            General.sSQL = General.sSQL + " LEFT JOIN Table1 ON Immeuble.Code1=Table1.Code1";
            General.sSQL = General.sSQL +
                           " LEFT JOIN TechReleve ON Immeuble.CodeImmeuble=TechReleve.CodeImmeuble WHERE Immeuble.CodeImmeuble='" +
                           StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            //Set rstmp = fc_OpenRecordSet(sSQl)
            var tmpModAdo = new ModAdo();
            General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
            if (General.rstmp.Rows.Count > 0)
            {
                this.txtCodeImmeuble.Text = General.rstmp.Rows[0]["CodeImmeuble"] + "";

                //===> Mondir le 02.04.2021, code bellow has been  commented, we need this for both DT and LONG, no need for General.sAfficheLibParticulier
                //'=== modif du 05 09 2018.
                //if (General.sAfficheLibParticulier == "1")
                //{
                //    sSQLPart = "SELECT     Immeuble.CodeImmeuble"
                //   + " FROM         Immeuble INNER JOIN "
                //   + " Gestionnaire ON Immeuble.Code1 = Gestionnaire.Code1 AND Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire "
                //   + " WHERE     (Gestionnaire.CodeQualif_Qua = 'part') AND (Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "')";

                //    using (var tmpsSQLPart = new ModAdo())
                //        sSQLPart = tmpsSQLPart.fc_ADOlibelle(sSQLPart);

                //    if (!string.IsNullOrEmpty(sSQLPart))
                //    {
                //        fc_ChargeFinance();

                //        //===> Mondir le 02.04.2021, line above commented by Mondir to fix https://groupe-dt.mantishub.io/view.php?id=2189
                //        //lblParticulier.Text = "Particulier - solde " + txtSoldeCompte.Text + "";
                //        lblParticulier.Text = txtSoldeCompte.Text;
                //        //===> Fin Modif Mondir

                //        lblParticulier.Visible = true;
                //    }
                //    else
                //    {
                //        lblParticulier.Text = "";
                //        lblParticulier.Visible = false;

                //    }
                //}
                //else
                //{
                //    lblParticulier.Text = "";
                //    lblParticulier.Visible = false;
                //}


                fc_ChargeFinance();

                //===> Mondir le 16.04.2021, commented line is old https://groupe-dt.mantishub.io/view.php?id=2189#c5817
                //lblParticulier.Text = txtSoldeCompte.Text;
                lblParticulier.Text = txtSoldeCompte.Text.ToDouble().ToString("#,##0.00");

                lblParticulier.Visible = true;

                //===> Fin Modir Mondir



                this.txtSelCodeImmeuble.Text = General.rstmp.Rows[0]["CodeImmeuble"] + "";
                this.txtAdresse.Text = General.rstmp.Rows[0]["Adresse"] + "";
                this.txtAdresse2_IMM.Text = General.rstmp.Rows[0]["Adresse2_Imm"] + "";
                this.txtAdresseFact1_IMM.Text = General.rstmp.Rows[0]["AdresseFact1_IMM"] + "";
                this.txtAdresseFact2_IMM.Text = General.rstmp.Rows[0]["AdresseFact2_IMM"] + "";
                this.txtAdresseFact3_IMM.Text = General.rstmp.Rows[0]["AdresseFact3_IMM"] + "";
                this.txtAdresseFact4_IMM.Text = General.rstmp.Rows[0]["AdresseFact4_IMM"] + "";
                //Me.txtAdresseGardien = nz(RSTMP!AdresseGardien, "")
                //Me.txtAllumageRalenti = nz(RSTMP!AllumageRalenti, "0")
                this.txtAngleRue.Text = General.rstmp.Rows[0]["AngleRue"] + "";
                //Me.txtAuteur = nz(rsTmp!Auteur, "")
                //Me.txtBoiteAClef = nz(rsTmp!BoiteAClef, "0")
                //Me.txtClefAcces = nz(rsTmp!ClefAcces, "")
                this.txtCode1.Text = General.rstmp.Rows[0]["Code1"] + "";
                //=== modif du 13 12 2015.
                Label2511.Text = "";
                fc_Groupe();


                this.SpecifAcces.Text = General.rstmp.Rows[0]["SpecifAcces"] + "";
                //===> Mondir le 17.05.2021, https://groupe-dt.mantishub.io/view.php?id=1381#c5964
                //this.txtSituation.Text = General.rstmp.Rows[0]["situation"] + "";
                //===> Mondir le 14.05.2021, cmmented coz a mdification asked by Xavier https://groupe-dt.mantishub.io/view.php?id=1381#c6039
                //this.txtSituation.Text = General.rstmp.Rows[0]["SitBoiteAClef"] + "";
                this.txtSituation.Text = General.rstmp.Rows[0]["situation"] + "";
                //===> Fin Modif Mondir

                //===> Mondir le 14.05.2021, demande de Modification par Xavier, https://groupe-dt.mantishub.io/view.php?id=1381#c6039
                this.txtSitBoiteAClef.Text = General.rstmp.Rows[0]["SitBoiteAClef"] + "";
                //===> Fin Modif Mondir

                this.txtCodeAcces1.Text = General.rstmp.Rows[0]["codeAcces1"] + "";

                this.txt159.Text = General.rstmp.Rows[0]["Immatriculation"] + "";

                this.txtCodeAcces2.Text = General.rstmp.Rows[0]["CodeAcces2"] + "";
                this.chkPrioritaire.Checked = General.nz(General.rstmp.Rows[0]["Prioritaire"], "0").ToString() == "1";
                this.chkDTAPositif.Checked = General.nz(General.rstmp.Rows[0]["DTAPositif"], "0").ToString() == "1";
                // Me.chkReceptionDTA.value = nz(rstmp!Receptiondta, "0")
                this.chkDTANégatif.Checked = General.nz(General.rstmp.Rows[0]["DTANegatif"], "0").ToString() == "1";
                Check12.Checked = General.nz(General.rstmp.Rows[0]["ChaudCondensation"], 0).ToString() == "1";

                this.chkDTAPositfSO.Checked = General.nz(General.rstmp.Rows[0]["DTAPositifSo"], "0").ToString() == "1";

                if (General.nz(General.rstmp.Rows[0]["Prior"], "0").ToString() == "0")
                {
                    lblGerantPrioritaire.Text = "Le gérant n'est pas prioritaire";
                }
                else
                {
                    lblGerantPrioritaire.Text = "Le gérant est prioritaire";
                }
                this.lblCodeEnergie.Text = General.rstmp.Rows[0]["CodeEnergie1"] + "";
                using (var tmpModAdo2 = new ModAdo())
                    this.lblEnergie.Text =
                        tmpModAdo2.fc_ADOlibelle("SELECT Libelle FROM Energie WHERE Code='" +
                                                 StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeEnergie1"].ToString()) + "" + "'");

                // Me.txtCodeChefSecteur = nz(rsTmp!CodeChefSecteur, "")
                // Me.txtCodeCommercial = nz(rsTmp!CodeCommercial, "")
                // Me.txtCodeContrat = nz(rsTmp!CodeContrat, "")
                // Me.txtCodeDepanneur = nz(rsTmp!CodeDepanneur, "")
                // Me.txtCodeDepanneur1 = nz(rsTmp!CodeDepanneur1, "")
                this.txtCodeGestionnaire.Text = General.rstmp.Rows[0]["CodeGestionnaire"] + "";
                if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["CodeGestionnaire"], "").ToString()))
                {
                    fc_InfoGestion(General.rstmp.Rows[0]["CodeGestionnaire"].ToString(),
                        General.rstmp.Rows[0]["Code1"] + "");
                }
                else
                {
                    Label255.Text = "";
                    Label256.Text = "";
                    Label257.Text = "";
                    Label251.Text = "";
                    Label258.Text = "";
                }
                this.txtCodePostal.Text = General.rstmp.Rows[0]["CodePostal"] + "";
                this.txtCodePostal_IMM.Text = General.rstmp.Rows[0]["CodePostal_IMM"] + "";
                this.txtCodeReglement_IMM.Text = General.rstmp.Rows[0]["CodeReglement_IMM"] + "";

                //===> Mondir le 29.11.2020 pour ajouter les modfis de la version V29.11.2020 ===> Tese
                txt165.Text = General.rstmp.Rows[0]["AdresseEnvoi1"] + "";
                txt166.Text = General.rstmp.Rows[0]["AdresseEnvoi2"] + "";
                txt167.Text = General.rstmp.Rows[0]["AdresseEnvoi3"] + "";
                txt168.Text = General.rstmp.Rows[0]["AdresseEnvoi4"] + "";
                txt169.Text = General.rstmp.Rows[0]["AdresseEnvoi5"] + "";
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                //txt170.Text = General.rstmp.Rows[0]["AdresseEnvoi6"] + "";
                //===> Fin Modif Mondir
                txt171.Text = General.rstmp.Rows[0]["AdresseEnvoiCP"] + "";
                txt172.Text = General.rstmp.Rows[0]["AdresseEnvoiVille"] + "";

                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                txt_163.Text = General.rstmp.Rows[0]["SIREN"] + "";
                txt_164.Text = General.rstmp.Rows[0]["TiersPayeur"] + "";
                //txt(164) = rstmp!SIRET & ""
                Check_23.Checked = General.nz(General.rstmp.Rows[0]["MailObligatoire"], 0).ToInt() == 1;
                txt_173.Text = General.rstmp.Rows[0]["Mail"] + "";
                ssCombo_23.Text = General.rstmp.Rows[0]["CAT_Code"] + "";
                fc_libCAT_Code();
                //===> Fin Modif Mondir

                if (Convert.ToInt32(General.nz(General.rstmp.Rows[0]["UtiliseAdresseEnvoi"], 0)) == 1)
                {
                    Check22.Checked = true;
                }
                else
                {
                    Check22.Checked = false;
                }
                fc_AdresseEnvoi();
                //===> Fin Modif Mondir

                label4208.Text = General.rstmp.Rows[0]["DateCreation"] + "";
                label4210.Text = General.rstmp.Rows[0]["CreePar"] + "";

                //txtoldCodeImmeuble = nz(rstmp!oldCodeImmeuble, "")
                this.txtCodeTVA.Text = General.rstmp.Rows[0]["CodeTVA"] + "";
                // Me.txtCodeTypeContrat = nz(rsTmp!CodeTypeContrat, "")
                // Me.txtCom1 = nz(rsTmp!Com1, "")
                // Me.txtCom2 = nz(rsTmp!Com2, "")
                this.txtCommentaireFacture1.Text = General.rstmp.Rows[0]["CommentaireFacture1"] + "";
                this.txtCommentaireFacture2.Text = General.rstmp.Rows[0]["CommentaireFacture2"] + "";
                this.txtCptAppelFond.Text = General.rstmp.Rows[0]["CptAppelFond"] + "";

                //If sTomtom = "1" Then

                txt115.Text = General.rstmp.Rows[0]["latitude"] + "";
                txt112.Text = General.rstmp.Rows[0]["longitude"] + "";
                txt116.Text = General.rstmp.Rows[0]["AdresseGeocode"] + "";
                //Label25(10) = rstmp!LogTomTom & ""

                //End If

                // Me.txtCptClientGerant = nz(rsTmp!CptClientGerant, "")
                // Me.txtCS1 = nz(rsTmp!CS1, "")
                // Me.txtCS2 = nz(rsTmp!CS2, "")
                // Me.txtCS3 = nz(rsTmp!CS3, "")
                // Me.txtDate1 = nz(rsTmp!Date1, "")
                // Me.txtDate2 = nz(rsTmp!Date2, "")
                // Me.txtDateAssemblee = nz(rsTmp!DateAssemblee, "")
                // Me.txtDateCréation = nz(rsTmp!DateCréation, "")
                // Me.txtDateRevision = nz(rsTmp!DateRevision, "")
                // Me.txtDateVidange = nz(rsTmp!DateVidange, "")
                // Me.txtDeleguation = nz(rsTmp!Deleguation, "")
                // Me.txtDeleguationCS1 = nz(rsTmp!DeleguationCS1, "")
                // Me.txtFaxGardien = rstmp!FaxGardien & ""
                txt157.Text = General.rstmp.Rows[0]["FaxGardien"] + "";
                //Me.txtGardien = rstmp!Gardien & ""
                txt154.Text = General.rstmp.Rows[0]["Gardien"] + "";
                this.txtRefClient.Text = General.rstmp.Rows[0]["RefClient"] + "";
                // Me.txthoraires = nz(rsTmp!horaires, "")
                this.txtHorairesLoge.Text = General.rstmp.Rows[0]["HorairesLoge"] + "";
                // Me.txtMAJ = nz(rsTmp!MAJ, "")
                this.txtNCompte.Text = General.rstmp.Rows[0]["Ncompte"] + "";
                this.txtAnalytiqueImmeuble.Text = General.Left(txtNCompte.Text, 13);
                txtCompteDivers.Text = General.rstmp.Rows[0]["CompteDivers"] + "";
                // Me.txtNettoyagePreparateur = nz(rsTmp!NettoyagePreparateur, "0")
                //            txtObservationComm_IMM = nz(rstmp!ObservationComm_IMM, "")
                this.txtObservations.Text = General.rstmp.Rows[0]["Observations"] + "";
                this.txt161.Text = General.rstmp.Rows[0]["Consigne"] + "";
                this.Check41.Checked = General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "True";
                this.Check42.Checked = General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "True";
                this.Check43.Checked = General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "True";
                this.Check44.Checked = General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "True";
                this.ChkSansReleveCompte.Checked = General.nz(General.rstmp.Rows[0]["SansReleveCompte"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["SansReleveCompte"], "0").ToString() == "True";
                this.ChkListeRouge.Checked = General.nz(General.rstmp.Rows[0]["ListeRouge"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["ListeRouge"], "0").ToString() == "True";
                this.Check13.Checked = General.nz(General.rstmp.Rows[0]["ListeRouge"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["ListeRouge"], "0").ToString() == "True";


                fc_DroitListeRouge();

                //Me.txtRaisonSocial_IMM = nz(rsTmp!RaisonSocial_IMM, "")
                // Me.txtRepartPrivatif = nz(rsTmp!RepartPrivatif, "")
                // Me.txtRepartProf = nz(rsTmp!RepartProf, "")
                // Me.txtRepartTotal = nz(rsTmp!RepartTotal, "")
                // Me.txtRevisionRobinet = nz(rsTmp!RevisionRobinet, "0")
                // Me.txtSitBoiteAClef = nz(rsTmp!SitBoiteAClef, "")
                // Me.txtSpecifAcces = nz(rsTmp!SpecifAcces, "")
                // Me.txtSuivi = nz(rsTmp!Suivi, "")
                // Me.txtTelCS1 = nz(rsTmp!TelCS1, "")
                // Me.txtTelCS2 = nz(rsTmp!TelCS2, "")
                // Me.txtTelCS3 = nz(rsTmp!TelCS3, "")
                //Me.txtTelGardien = rstmp!TelGardien & ""
                txt155.Text = General.rstmp.Rows[0]["TelGardien"] + "";
                this.txtVille.Text = General.rstmp.Rows[0]["Ville"] + "";
                this.txtVilleFact_IMM.Text = General.rstmp.Rows[0]["VilleFact_IMM"] + "";
                this.txtCodeChefSecteur.Text = General.rstmp.Rows[0]["CodeChefSecteur"] + "";
                if (!string.IsNullOrEmpty(txtCodeChefSecteur.Text))
                {
                    using (var tmpModAdo2 = new ModAdo())
                        this.lbllibCodeChefSecteur.Text = tmpModAdo2.fc_ADOlibelle("select nom from personnel where matricule ='" + txtCodeChefSecteur.Text + "'");
                }
                this.SSOleDBCmbDep1.Value = General.rstmp.Rows[0]["CodeDepanneur"] + "";

                ssCombo21.Text = General.rstmp.Rows[0]["CodeRamoneur"] + "";
                fc_LibRamoneur();
                if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["CodeDepanneur"].ToString()))
                {
                    using (var tmpModAdo2 = new ModAdo())
                    {
                        this.lblDep1Nom.Text =
                            tmpModAdo2.fc_ADOlibelle(
                                "SELECT Nom, Prenom FROM Personnel WHERE Matricule ='" + SSOleDBCmbDep1.Value + "'",
                                true);
                        //Me.lblDep1Prenom.Caption = fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" & SSOleDBCmbDep1.value & "'")
                        this.lblDep1Qualif.Text =
                            tmpModAdo2.fc_ADOlibelle(
                                "SELECT Qualification.Qualification,Qualification.CodeQualif,Personnel.Matricule,Personnel.CodeQualif FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif  WHERE Personnel.Matricule ='" +
                                SSOleDBCmbDep1.Value + "'");
                    }
                }
                else
                {
                    this.lblDep1Nom.Text = "";
                    //Me.lblDep1Prenom.Caption = ""
                    this.lblDep1Qualif.Text = "";
                    lblMatriculeDep2.Text = "";
                    lblDep2Nom.Text = "";
                    // lblDep2Prenom.Caption = ""
                    lblDep2Qualif.Text = "";
                    txtCodeChefSecteur.Text = "";
                    lbllibCodeChefSecteur.Text = "";
                    // lblPrenomChefSecteur.Caption = ""
                    lblQualifChefSecteur.Text = "";
                    lblMatriculeREspExpl.Text = "";
                    lblLibRespExpl.Text = "";
                    // lblPrenomRespExpl.Caption = ""
                    lblQualifRespExpl.Text = "";
                    //==modif rachid du 27/12/2004..
                    lblMatriculeRondier.Text = "";
                    lblRondierNom.Text = "";
                    // lblRondierPrenom = ""
                    lblRondierQualif.Text = "";
                    //===============================
                }

                if (!string.IsNullOrEmpty(this.SSOleDBCmbDep1.Text))
                {
                    var modAdorsDep2 = new ModAdo();
                    using (var tmpModAdo2 = new ModAdo())
                        rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                            "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Initiales='" +
                            tmpModAdo2.fc_ADOlibelle("SELECT Dep2 FROM Personnel WHERE Personnel.Matricule='" +
                                                     General.nz(General.rstmp.Rows[0]["CodeDepanneur"].ToString(), "") +
                                                     "'") + "'");
                    if (rsDep2.Rows.Count > 0)
                    {
                        lblMatriculeDep2.Text = rsDep2.Rows[0]["Matricule"] + "";
                        lblDep2Nom.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];
                        // lblDep2Prenom.Caption = rsDep2.Fields("Prenom") & ""
                        lblDep2Qualif.Text = rsDep2.Rows[0]["Qualification"] + "";
                    }
                    modAdorsDep2.Dispose();
                    modAdorsDep2 = new ModAdo();
                    using (var tmpModAdo2 = new ModAdo())
                        rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                            "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Initiales='" +
                            (tmpModAdo2.fc_ADOlibelle("SELECT CSecteur FROM Personnel WHERE Personnel.Matricule='" +
                                                      General.rstmp.Rows[0]["CodeDepanneur"] + "'")) + "'");
                    if (rsDep2.Rows.Count > 0)
                    {
                        txtCodeChefSecteur.Text = rsDep2.Rows[0]["Matricule"] + "";
                        lbllibCodeChefSecteur.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];
                        // lblPrenomChefSecteur.Caption = rsDep2.Fields("Prenom") & ""
                        lblQualifChefSecteur.Text = rsDep2.Rows[0]["Qualification"] + "";
                    }
                    modAdorsDep2.Dispose();
                    //' rsDep2.Open "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Initiales='" & (fc_ADOlibelle("SELECT CRespExploit FROM Personnel WHERE Personnel.Matricule='" & txtCodeChefSecteur.Caption & "'")) & "'", adocnn


                    //=== modif rachid du 6 12 2010, la gestion du personnel a changé pour la
                    //=== société delostal, le responsable d'exploitation ne depends plus
                    //=== de l'intervenant.
                    //If UCase(adocnn.DefaultDatabase) <> UCase(cDelostal) Then
                    if (General.sVersionImmParComm != "1")
                    {
                        modAdorsDep2 = new ModAdo();
                        using (var tmpModAdo2 = new ModAdo())
                        {
                            rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                                "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Initiales='" +
                                (tmpModAdo2.fc_ADOlibelle(
                                    "SELECT CRespExploit FROM Personnel WHERE Personnel.Matricule='" +
                                    General.rstmp.Rows[0]["CodeDepanneur"] + "'")) + "'");
                            if (rsDep2.Rows.Count > 0)
                            {
                                lblMatriculeREspExpl.Text = rsDep2.Rows[0]["Matricule"] + "";
                                lblLibRespExpl.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];
                                // lblPrenomRespExpl.Caption = rsDep2.Fields("Prenom") & ""
                                lblQualifRespExpl.Text = rsDep2.Rows[0]["Qualification"] + "";
                            }
                        }
                        modAdorsDep2.Dispose();
                    }

                    //==== modif rachid
                    modAdorsDep2 = new ModAdo();
                    using (var tmpModAdo2 = new ModAdo())
                        rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                            "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                            " Qualification.Qualification FROM Personnel INNER JOIN Qualification ON " +
                            " Personnel.CodeQualif = Qualification.CodeQualif " + " WHERE Initiales='" +
                            (tmpModAdo2.fc_ADOlibelle("SELECT Rondier FROM Personnel WHERE Personnel.Matricule='" +
                                                      General.nz(General.rstmp.Rows[0]["CodeDepanneur"], "") + "'")) +
                            "'");

                    if (rsDep2.Rows.Count > 0)
                    {
                        lblMatriculeRondier.Text = rsDep2.Rows[0]["Matricule"] + " " + rsDep2.Rows[0]["Prenom"];
                        lblRondierNom.Text = rsDep2.Rows[0]["Nom"] + "";
                        // lblRondierPrenom.Caption = rsDep2.Fields("Prenom") & ""

                        lblRondierQualif.Text = rsDep2.Rows[0]["Qualification"] + "";
                    }
                    modAdorsDep2.Dispose();
                    //====================================
                }



                //=== modif (suite) rachid du 6 12 2010, la gestion du personnel a changé pour la
                //=== société delostal, le responsable d'exploitation ne depends plus
                //=== de l'intervenant.
                //If UCase(adocnn.DefaultDatabase) = UCase(cDelostal) Then
                if (General.sVersionImmParComm == "1")
                {
                    ssCombo13.Text = General.rstmp.Rows[0]["CodeRespExploit"] + "";
                    fc_FindPersonnnel(ssCombo13.Text);

                    if (General.sChefSecteurLibre == "1")
                    {
                        ssCombo22.Text = General.rstmp.Rows[0]["CodeChefSecteur"] + "";

                        if (string.IsNullOrEmpty(ssCombo22.Text))
                        {
                            txt105.Text = "";
                            txt156.Text = "";
                        }
                        else
                        {
                            rsSecteur = rsSecteurAdo.fc_OpenRecordSet("SELECT Nom, Prenom , "
                                                                        + " Qualification.Qualification"
                                                                        + " FROM Personnel left JOIN Qualification ON Personnel.CodeQualif"
                                                                        + " = Qualification.CodeQualif "
                                                                        + " WHERE Personnel.Matricule = '" + StdSQLchaine.gFr_DoublerQuote(ssCombo22.Text) + "'");
                            if (rsSecteur.Rows.Count > 0)
                            {
                                txt105.Text = rsSecteur.Rows[0]["Nom"] + " " + rsSecteur.Rows[0]["Prenom"] + "";
                                txt156.Text = rsSecteur.Rows[0]["Qualification"] + "";
                            }
                            else
                            {
                                txt105.Text = "";
                                txt156.Text = "";
                            }
                            rsSecteur.Dispose();
                            rsSecteur = null;
                        }
                    }

                    if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["CodeCommercial"] + ""))
                    {
                        ssCombo15.Text = General.rstmp.Rows[0]["CodeCommercial"] + "";
                    }
                    else
                    {
                        using (var tmpModAdo2 = new ModAdo())
                            ssCombo15.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Commercial FROM Table1 WHERE Table1.Code1='" +
                                                         StdSQLchaine.gFr_DoublerQuote(General
                                                             .nz(General.rstmp.Rows[0]["Code1"], "").ToString()) + "'");
                    }
                    if (!string.IsNullOrEmpty(ssCombo15.Text))
                    {
                        using (var tmpModAdo2 = new ModAdo())
                            txt113.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Nom, Prenom FROM Personnel WHERE Matricule ='" +
                                                         ssCombo15.Text + "'");
                    }
                    else
                    {
                        txt113.Text = "";
                    }
                }
                else
                {

                    //            Me.SSOleDBCmbDep2.value = nz(rstmp!CodeDepanneur1, "")
                    //            If Not IsNull(rstmp!CodeDepanneur1) And Not (rstmp!CodeDepanneur1 = "") Then
                    //                Me.lblDep2Nom.Caption = fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" & SSOleDBCmbDep2.value & "'")
                    //                Me.lblDep2Prenom.Caption = fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" & SSOleDBCmbDep2.value & "'")
                    //                Me.lblDep2Qualif.Caption = fc_ADOlibelle("SELECT Qualification.Qualification,Qualification.CodeQualif,Personnel.Matricule,Personnel.CodeQualif FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif  WHERE Personnel.Matricule ='" & SSOleDBCmbDep2.value & "'")
                    //            End If


                    if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["CodeCommercial"] + ""))
                    {
                        this.lblCodeCommercial.Text = General.rstmp.Rows[0]["CodeCommercial"] + "";
                    }
                    else
                    {
                        using (var tmoModAdo2 = new ModAdo())
                            this.lblCodeCommercial.Text = tmoModAdo2.fc_ADOlibelle("SELECT Commercial FROM Table1 WHERE Table1.Code1='" +
                                StdSQLchaine.gFr_DoublerQuote(General.nz(General.rstmp.Rows[0]["Code1"], "").ToString()) + "'");
                    }
                    if (!string.IsNullOrEmpty(this.lblCodeCommercial.Text))
                    {
                        using (var tmoModAdo2 = new ModAdo())
                        {
                            this.lblCommercNom.Text =
                                tmoModAdo2.fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" +
                                                     this.lblCodeCommercial.Text + "'");
                            this.lblCommercPrenom.Text =
                                tmoModAdo2.fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" +
                                                     this.lblCodeCommercial.Text + "'");
                        }
                    }
                    else
                    {
                        this.lblCommercNom.Text = "";
                        this.lblCommercPrenom.Text = "";
                    }
                }
                //            Me.SSOleDBCmbDispatch = nz(rstmp!CodeDispatcheur, "")
                //            If Not IsNull(rstmp!CodeDispatcheur) And Not (rstmp!CodeDispatcheur = "") Then
                //                Me.lblDispatchNom.Caption = fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" & Me.SSOleDBCmbDispatch.value & "'")
                //                Me.lblDispatchPrenom.Caption = fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" & Me.SSOleDBCmbDispatch.value & "'")
                //            End If

                this.txtSEC_Code.Text = General.rstmp.Rows[0]["SEC_Code"] + "";
                // If txtCodeChefSecteur <> "" Then
                //     Me.txtSEC_Code = fc_ADOlibelle("select sec_code from personnel where matricule ='" & txtCodeChefSecteur & "'")
                // End If
                //recherche du libelle du réglement.

                ///===============> Tested
                if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["CodeImmReleve"], "").ToString()))
                {
                    btnCpteRendu.BackColor = Color.FromArgb(34, 135, 134);
                }
                else
                {
                    btnCpteRendu.BackColor = Color.FromArgb(55, 84, 96);
                }

                //=== modif du 05 07 2017, ajout du bouton GMAO.
                sSQLgmao = "SELECT     COP_NoAuto From COP_ContratP2 WHERE  codeimmeuble = '" +
                           StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' ORDER BY COP_NoAuto DESC";
                using (var tmpModAdo2 = new ModAdo())
                    sSQLgmao = tmpModAdo2.fc_ADOlibelle(sSQLgmao);
                if (!string.IsNullOrEmpty(sSQLgmao))
                {
                    Cmd8.BackColor = Color.FromArgb(34, 135, 134);
                }
                else
                {
                    Cmd8.BackColor = Color.FromArgb(85, 115, 128);
                }

                tmpModAdo.Dispose();

                fc_AfficheReglement(txtCodeReglement_IMM.Text);
                fc_DeBloqueForm();
                //FC_ChargeOnglet

                fc_RechercheContrat(txtCodeImmeuble.Text);
                //txtAdresse.SetFocus
                fc_ChargePrestations(txtCodeImmeuble.Text);

                fc_LibLimiteContrat();
                fc_Competence();


                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    // fc_LoadP1Gaz txtCodeImmeuble
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                fc_LoadIDA();
                //===> Fin Modif Mondirs

                //===> Mondir le 15.02.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2256
                checkContrat();
                //===> Fin Modif Mondir

                //===> Mondir le 16.04.2021, https://groupe-dt.mantishub.io/view.php?id=2189#c5817
                if (ActivateFinanceTab)
                {
                    lblParticulier_Click(null, null);
                }
                //===> Fin Modif Mondir
            }
            else
            {
                fc_Clear();
                fc_ClearControleGaz();
            }
            FC_ChargeOnglet();
            blModif = false;

            fc_LoadMailSite(txtCodeImmeuble.Text);

            if (General.sActiveP1 == "1")
            {
                fc_ChargeAppareils();
            }

            ///
            ///
            ///============================> 
            ///
            ///
            //var BackButton = View.Theme.Theme.MainMenu.Controls.Cast<Control>().First(c => c.Name == "PanelItems")
            //    .Controls.Cast<MainMenuItem>().First(c => c.Type == MainMenuItem.MainMenuItemtype.BackButton);

            //switch (ModParametre.sFicheAppelante)
            //{
            //    case Variable.cUserIntervention:
            //        BackButton.LabelItemName.Text = "INTERVENTION";
            //        CodeFicheAppelante =
            //            General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
            //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
            //        BackButton.Visible = true;
            //        break;
            //    case Variable.cUserDocStandard:
            //        BackButton.LabelItemName.Text = "FICHE D'APPEL";
            //        CodeFicheAppelante =
            //            General.getFrmReg(Variable.cUserDocStandard, "NewVar", "");
            //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
            //        BackButton.Visible = true;
            //        break;
            //    case Variable.cUserDocDevis:
            //        BackButton.LabelItemName.Text = "Devis";
            //        CodeFicheAppelante =
            //            General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
            //        BackButton.ClickEvent = (se, ev) => GoFicheAppelante(ModParametre.sFicheAppelante);
            //        BackButton.Visible = true;
            //        break;

            //    default:
            //        BackButton.Visible = false;
            //        break;
            //}

        }

        /// <summary>
        /// Mondir le 15.02.2021 to fix https://groupe-dt.mantishub.io/view.php?id=2256
        /// </summary>
        private void checkContrat()
        {
            //===> Get Active contrats
            var dt = ModContrat.fc_GetActiveContract(txtCodeImmeuble.Text);
            //===> If there is no active contrats
            if (dt.Rows.Count == 0)
            {
                //===> CheckBoxes P1 to P4
                Check41.Checked = false;
                Check42.Checked = false;
                Check43.Checked = false;
                Check44.Checked = false;

                var query = $"UPDATE IMMEUBLE SET P1 = 0, P2 = 0, P3 = 0, P4 = 0 WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
                General.Execute(query);

                //===> Mondir le 31.05.2021, ony if there is only at least one contrat but ended ! https://groupe-dt.mantishub.io/view.php?id=2445
                if (ModContrat.AllContractsCount(txtCodeImmeuble.Text) != 0)
                {
                    //===> Mondir le 29.04.2021, https://groupe-dt.mantishub.io/view.php?id=2420
                    Check21.Checked = true;
                    //===> Fin Modif Mondir

                    query = $"UPDATE IMMEUBLE SET Resilie = 1 WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
                    General.Execute(query);

                    //===> Mondir le 02.06.2021 https://groupe-dt.mantishub.io/view.php?id=2445#c6088
                    var dateMax = "";
                    using (var tmpModAdo = new ModAdo())
                    {
                        dateMax = tmpModAdo.fc_ADOlibelle($"SELECT MAX(DateFin) FROM Contrat WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'");
                    }
                    //===> Fin Modif Mondir

                    //===> Mondir le 01.06.2021, moved this line down https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                    //lblContratResilie.Visible = true;
                    //===> Mondir le 02.06.2021, added this codition below https://groupe-dt.mantishub.io/view.php?id=2445#c6088
                    if (!dateMax.IsDate())
                    {
                        lblContratResilie.Text = "Attention : Contrat résilié";
                    }
                    else
                    {
                        lblContratResilie.Text = $"Attention : Contrat résilié le {dateMax.ToDate().ToString("dd/MM/yyyy")}";
                    }
                    //===> Fin Modif Mondir
                }
                //===> Mondir le 01.06.2021, added this else if https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                else if (ModContrat.AllContractsCount(txtCodeImmeuble.Text) == 0)
                {
                    lblContratResilie.Text = "Immeuble sans contrat";
                }

                //===> Mondir le 01.06.2021 https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                lblContratResilie.Visible = true;
            }
            else
            {
                lblContratResilie.Visible = false;

                Check21.Checked = false;

                Check41.Checked = dt.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString().ToUpper() == "P1");
                Check42.Checked = dt.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString().ToUpper() == "P2");
                Check43.Checked = dt.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString().ToUpper() == "P3");
                Check44.Checked = dt.Rows.Cast<DataRow>().Any(r => r["COT_Code"].ToString().ToUpper() == "P4");

                var query = $"UPDATE IMMEUBLE SET P1 = {(Check41.Checked ? "1" : "0")}, P2 = {(Check42.Checked ? "1" : "0")}, " +
                    $"P3 = {(Check43.Checked ? "1" : "0")}, P4 = {(Check44.Checked ? "1" : "0")}, Resilie = {(Check21.Checked ? "1" : "0")}" +
                    $" WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
                General.Execute(query);
            }

            //if (Check41.Checked || Check42.Checked || Check43.Checked || Check44.Checked || Check21.Checked)
            //{


            //===> Mondir le 11.10.2021, commented the line below
            //using (var tmpModAdo = new ModAdo())
            //{
            //    var query = $@"SELECT COUNT(*) AS 'T',SUM(CASE WHEN Resiliee = 1 THEN 1 ELSE 0 END) AS 'R', CodeImmeuble FROM Contrat
            //                WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'
            //                GROUP BY CodeImmeuble";
            //    var dt = tmpModAdo.fc_OpenRecordSet(query);
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        if (dt.Rows[0]["T"].ToInt() == dt.Rows[0]["R"].ToInt())
            //        {
            //            Check41.Checked = false;
            //            Check42.Checked = false;
            //            Check43.Checked = false;
            //            Check44.Checked = false;

            //            //===> Mondir le 29.04.2021, https://groupe-dt.mantishub.io/view.php?id=2420
            //            Check21.Checked = true;
            //            //===> Fin Modif Mondir

            //            query =
            //                $"UPDATE IMMEUBLE SET P1 = 0, P2 = 0, P3 = 0, P4 = 0 WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
            //            General.Execute(query);
            //        }
            //    }
            //}
            //}

            //===> Mondir le 02.04.2021, afficher le label contrat reselie
            //var dt2 = ModContrat.fc_GetActiveContract(txtCodeImmeuble.Text);
            //if (dt2 == null || dt2.Rows.Count == 0)
            //{
            //    lblContratResilie.Visible = true;
            //}
            //else
            //{
            //    lblContratResilie.Visible = false;

            //    //===> Mondir le 23.04.2021, https://groupe-dt.mantishub.io/view.php?id=2419
            //    //===> Mondir le 10.05.2021, moved this code here
            //    using (var tmpModAdo = new ModAdo())
            //    {
            //        //===> Check P1
            //        var query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P1'";
            //        var dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check41.Checked = dt.Rows.Count > 0;

            //        //===> Check P2
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P2'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check42.Checked = dt.Rows.Count > 0;

            //        //===> Check P3
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P3'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check43.Checked = dt.Rows.Count > 0;

            //        //===> Check P4
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P4'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check44.Checked = dt.Rows.Count > 0;

            //        query = $"UPDATE IMMEUBLE SET P1 = {(Check41.Checked ? "1" : "0")}, P2 = {(Check42.Checked ? "1" : "0")}, P3 = {(Check43.Checked ? "1" : "0")}, P4 = {(Check44.Checked ? "1" : "0")} WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
            //        General.Execute(query);
            //    }
            //    //===> Fin Modif Mondir
            //}
            //===> Fin Modif Monsdir



        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        private void fc_libCAT_Code()
        {
            string sSQl = "";

            try
            {
                sSQl = "SELECT   CAT_Libelle From CAT_CategorieImmeuble WHERE  CAT_Code = '" +
                       StdSQLchaine.gFr_DoublerQuote(ssCombo_23.Text) + "'";

                using (var tmpModAdo = new ModAdo())
                {
                    Label4_221.Text = tmpModAdo.fc_ADOlibelle(sSQl);
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Fiche"></param>
        private void GoFicheAppelante(string Fiche)
        {
            // sauve les paramètres de la fiche immeuble
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                // stocke la position de la fiche gerant
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
            }


            switch (Fiche)
            {

                case Variable.cUserDocStandard:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(Appel.UserDocStandard));
                    break;

                case Variable.cUserIntervention:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);
                    //Mondir - Must Navigate To : UserIntervention
                    //View.Theme.Theme.Navigate(typeof(UserIntervention));
                    break;

                case Variable.cUserDocDevis:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(UserDocDevis));
                    break;

                case Variable.cUserDocClient:
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, CodeFicheAppelante);
                    View.Theme.Theme.Navigate(typeof(UserDocClient));
                    break;

            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_InitialiseGrille()
        {
            ModAdo.fc_CloseRecordset(rsCorrespondants);
            ModAdo.fc_CloseRecordset(rsCorr);
            ModAdo.fc_CloseRecordset(rsCopro);
            ModAdo.fc_CloseRecordset(rsIntervenant);
            //ssGridGestionnaire.ReBind
            ssGridImmCorrespondant.DataSource = null;
            GridIMRE.DataSource = null;
            ssGridCopro.DataSource = null;
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            GridIDA.DataSource = null;
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeGestionnaire"></param>
        /// <param name="sCode1"></param>
        private void fc_InfoGestion(string sCodeGestionnaire, string sCode1)
        {

            DataTable rs = default(DataTable);
            ModAdo modAdors;
            General.sSQL = "SELECT NomGestion, TelGestion, TelPortable_GES, FaxGestion , EMail_GES " +
                           " FROM Gestionnaire WHERE CodeGestinnaire = '" +
                           StdSQLchaine.gFr_DoublerQuote(sCodeGestionnaire) + "'" + " and Code1='" +
                           StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";
            modAdors = new ModAdo();
            rs = modAdors.fc_OpenRecordSet(General.sSQL);
            if (rs.Rows.Count > 0)
            {
                Label255.Text = General.nz(rs.Rows[0]["NomGestion"], "").ToString();
                Label256.Text = General.nz(rs.Rows[0]["TelGestion"], "").ToString();
                Label257.Text = General.nz(rs.Rows[0]["TelPortable_GES"], "").ToString();
                Label251.Text = General.nz(rs.Rows[0]["FaxGestion"], "").ToString();
                Label258.Text = General.nz(rs.Rows[0]["EMail_GES"], "").ToString();
            }
            else
            {
                Label255.Text = "";
                Label256.Text = "";
                Label257.Text = "";
                Label251.Text = "";
                Label258.Text = "";

            }

            modAdors.Dispose();

            //            sSQl = "SELECT TelGestion FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
            //            Label25(6).Caption = fc_ADOlibelle(sSQl)
            //
            //            sSQl = ""
            //            sSQl = "SELECT TelPortable_GES FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
            //            Label25(7).Caption = fc_ADOlibelle(sSQl)

            //            sSQl = ""
            //            sSQl = "SELECT FaxGestion FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
            //            Label25(1).Caption = fc_ADOlibelle(sSQl)

            //            sSQl = ""
            //            sSQl = "SELECT EMail_GES FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
            //            Label25(8).Caption = fc_ADOlibelle(sSQl)

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LibRamoneur()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT nom, prenom From personnel where matricule ='" + ssCombo21.Text + "'";
                using (var tmpModAdo = new ModAdo())
                    txt160.Text = tmpModAdo.fc_ADOlibelle(sSQL, true);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_LibRamoneur");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sResponsable"></param>
        private void fc_FindPersonnnel(string sResponsable)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            var modAdors = new ModAdo();
            bool bComm = false;

            try
            {
                //sSQL = "SELECT     Initiales, Nom, Prenom, Matricule, CodeQualif," _
                //& " Commercial, DevRadiateur, DevExploit, RenovChauffage1, RenovChauffage2" _
                //& " From Personnel" _
                //& " WHERE MATRICULE = '" & gFr_DoublerQuote(sResponsable) & "'"

                bComm = false;

                sSQL = "SELECT     Initiales, Nom, Prenom, Matricule, Personnel.CodeQualif," +
                       " Commercial, DevRadiateur, DevExploit, RenovChauffage1, RenovChauffage2, " +
                       " Qualification.Qualification, Chauffagiste1, Chauffagiste2, CSecteur " +
                       " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                       " = Qualification.CodeQualif " +
                       " WHERE MATRICULE = '" + StdSQLchaine.gFr_DoublerQuote(sResponsable) + "'";

                rs = modAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    //    ssCombo(15) = rs!Commercial & ""
                    //    txt(117) = rs!DevExploit & ""
                    //    txt(121) = rs!DevRadiateur & ""
                    //    txt(114) = rs!RenovChauffage1 & ""
                    //    txt(108) = rs!RenovChauffage2 & ""
                    txt107.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                    txt123.Text = rs.Rows[0]["Qualification"] + "";
                    tpPers.sCommercial = rs.Rows[0]["Commercial"] + "";
                    tpPers.sDevisExploit = rs.Rows[0]["DevExploit"] + "";
                    tpPers.sDevisRadiateur = rs.Rows[0]["DevRadiateur"] + "";
                    tpPers.sRenovChauff1 = rs.Rows[0]["RenovChauffage1"] + "";
                    tpPers.sRenovChauff2 = rs.Rows[0]["RenovChauffage2"] + "";
                    tpPers.sChauffagiste1 = rs.Rows[0]["Chauffagiste1"] + "";
                    tpPers.sChauffagiste2 = rs.Rows[0]["Chauffagiste2"] + "";
                    tpPers.sSecteur = rs.Rows[0]["CSecteur"] + "";

                    modAdors.Dispose();

                    //===> Mondir le 03.12.2020, ajout de cette variable pour supprimer la relation entre le responsable d'exploi et le commercial : https://groupe-dt.mantishub.io/view.php?id=2108
                    if (bCloseUp == true && activateResposableAndCommercial)
                    {
                        sSQL = "select matricule FROM personnel where Initiales ='" +
                               StdSQLchaine.gFr_DoublerQuote(tpPers.sCommercial) + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        if (!string.IsNullOrEmpty(ssCombo15.Text) && sSQL != ssCombo15.Text &&
                            !string.IsNullOrEmpty(tpPers.sCommercial))
                        {
                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez changer le commercial, voulez vous continuer ?",
                                    "Commercial", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                bComm = true;
                            }
                        }
                        else if (string.IsNullOrEmpty(txt113.Text))
                        {
                            bComm = true;

                        }

                        //=== si aucun commercial, mettre le commercial de la fiche gérant par défaut.
                        if (string.IsNullOrEmpty(tpPers.sCommercial))
                        {
                            sSQL = "SELECT Commercial FROM TABLE1 WHERE code1 ='" +
                                   StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
                            using (var tmpModAdo = new ModAdo())
                                sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                            if (!string.IsNullOrEmpty(sSQL))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                    "Aucun commercial n'est attaché à ce responsable d'exploitation," +
                                    " le commercial par défaut sera celui attaché à la fiche gérant.",
                                    "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                sSQL = "select Initiales FROM personnel where matricule ='" +
                                       StdSQLchaine.gFr_DoublerQuote(sSQL) + "'";
                                using (var tmpModAdo = new ModAdo())
                                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                                tpPers.sCommercial = sSQL;
                                bComm = true;
                            }

                        }

                        if (bComm == true)
                        {
                            //=== commercial.
                            sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                                   " Qualification.Qualification" +
                                   " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                                   " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sCommercial) + "'";
                            modAdors = new ModAdo();
                            rs = modAdors.fc_OpenRecordSet(sSQL);
                            if (rs.Rows.Count > 0)
                            {
                                ssCombo15.Text = rs.Rows[0]["Matricule"] + "";
                                txt113.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];

                            }
                            else
                            {
                                ssCombo15.Text = "";
                                txt113.Text = "";

                            }
                            modAdors.Dispose();
                        }


                        bCloseUp = false;
                    }

                    //=== devis exploitation.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sDevisExploit) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count > 0)
                    {
                        txt117.Text = rs.Rows[0]["Matricule"] + "";
                        txt118.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt128.Text = rs.Rows[0]["Qualification"] + "";
                    }
                    else
                    {
                        txt118.Text = "";
                        txt128.Text = "";
                        txt117.Text = "";
                    }

                    modAdors.Dispose();

                    //=== devis radiateur.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sDevisRadiateur) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        txt121.Text = rs.Rows[0]["Matricule"] + "";
                        txt122.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt125.Text = rs.Rows[0]["Qualification"] + "";
                    }
                    else
                    {
                        txt122.Text = "";
                        txt125.Text = "";
                        txt121.Text = "";
                    }


                    modAdors.Dispose();

                    //=== renovation chaufferie 1.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sRenovChauff1) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        txt114.Text = rs.Rows[0]["Matricule"] + "";
                        txt120.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt124.Text = rs.Rows[0]["Qualification"] + "";

                    }
                    else
                    {
                        txt120.Text = "";
                        txt124.Text = "";
                        txt114.Text = "";
                    }


                    modAdors.Dispose();

                    //=== renovation chaufferie 2.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sRenovChauff2) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        txt108.Text = rs.Rows[0]["Matricule"] + "";
                        txt119.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt128.Text = rs.Rows[0]["Qualification"] + "";
                    }
                    else
                    {
                        txt119.Text = "";
                        txt128.Text = "";
                        txt108.Text = "";
                    }

                    modAdors.Dispose();

                    //=== chauffagiste 1.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sChauffagiste1) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        txt103.Text = rs.Rows[0]["Matricule"] + "";
                        txt102.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt106.Text = rs.Rows[0]["Qualification"] + "";
                    }
                    else
                    {
                        txt103.Text = "";
                        txt102.Text = "";
                        txt106.Text = "";
                    }

                    modAdors.Dispose();

                    //=== chauffagiste 2.
                    sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                           " Qualification.Qualification" +
                           " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                           " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sChauffagiste2) + "'";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        txt110.Text = rs.Rows[0]["Matricule"] + "";
                        txt109.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                        txt111.Text = rs.Rows[0]["Qualification"] + "";
                    }
                    else
                    {
                        txt110.Text = "";
                        txt109.Text = "";
                        txt111.Text = "";
                    }

                    modAdors.Dispose();

                    //=== Chef de secteur.
                    if (General.sChefSecteurLibre != "1")
                    {
                        sSQL = "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," +
                              " Qualification.Qualification" +
                              " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" +
                              " = Qualification.CodeQualif WHERE Initiales='" + StdSQLchaine.gFr_DoublerQuote(tpPers.sSecteur) + "'";
                        modAdors = new ModAdo();
                        rs = modAdors.fc_OpenRecordSet(sSQL);
                        if (rs.Rows.Count > 0)
                        {
                            txt104.Text = rs.Rows[0]["Matricule"] + "";
                            txt105.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];
                            txt156.Text = rs.Rows[0]["Qualification"] + "";
                        }
                        else
                        {
                            txt104.Text = "";
                            txt105.Text = "";
                            txt156.Text = "";
                        }

                    }



                }
                else
                {
                    txt107.Text = "";
                    txt123.Text = "";
                    ssCombo15.Text = "";
                    //ssCombo(21) = ""

                    txt113.Text = "";
                    txt118.Text = "";
                    txt128.Text = "";
                    txt117.Text = "";
                    txt122.Text = "";
                    txt125.Text = "";
                    txt121.Text = "";
                    txt120.Text = "";
                    txt124.Text = "";
                    txt114.Text = "";
                    txt119.Text = "";
                    txt128.Text = "";
                    txt108.Text = "";
                    txt103.Text = "";
                    txt102.Text = "";
                    txt106.Text = "";
                    txt110.Text = "";
                    txt109.Text = "";
                    txt111.Text = "";
                }

                modAdors.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_FindPersonnnel");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blAfficheMessage"></param>
        /// <returns></returns>
        private bool fc_AfficheReglement(string sCode, bool blAfficheMessage = false)
        {
            bool functionReturnValue = false;
            // recherche du libelle du code de reglement. renvoie true si cekui ci existe.
            //si blAfficheMessage est à true affichage du message d'erreur.
            DataTable rsAffReglement = default(DataTable);
            var modAdorsAffReglement = new ModAdo();
            if (!string.IsNullOrEmpty(sCode))
            {
                General.sSQL = "";
                General.sSQL = "SELECT CodeReglement.Libelle  FROM CodeReglement" + " where Code='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                rsAffReglement = modAdorsAffReglement.fc_OpenRecordSet(General.sSQL);

                if (rsAffReglement.Rows.Count > 0)
                {
                    lblLibCodeReglement_IMM.Text = General.nz(rsAffReglement.Rows[0]["Libelle"], "").ToString();
                    Label467.Text = lblLibCodeReglement_IMM.Text.ToUpper();
                    functionReturnValue = false;
                }
                else
                {
                    lblLibCodeReglement_IMM.Text = "";
                    Label467.Text = "";
                    if (blAfficheMessage == true)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce type de réglement n'éxiste pas", "Validation annulée", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[2];
                        functionReturnValue = true;
                        txtCodeReglement_IMM.Focus();
                    }
                }
            }
            else
            {
                lblLibCodeReglement_IMM.Text = "";
                Label467.Text = "";
            }

            if ((rsAffReglement != null))
            {
                modAdorsAffReglement.Dispose();
            }
            return functionReturnValue;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_RechercheContrat(string sCodeImmeuble)
        {
            try
            {
                string LenNumContrat = null;

                //        LenNumContrat = fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" & sCodeImmeuble & "'")

                //        sSQl = ""
                //        sSQl = "SELECT MAX(Contrat.NumContrat) AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " _
                //'                & " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle "
                //            sSQl = sSQl & " WHERE Contrat.CodeImmeuble like '" & sCodeImmeuble & "%' AND Avenant=0"
                //            sSQl = sSQl & " AND LEN(NumContrat)=" & nz(LenNumContrat, "''")
                //            sSQl = sSQl & " GROUP BY Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1"

                General.sSQL = "";
                General.sSQL =
                    "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1," +
                    " Contrat.CodeArticle, FacArticle.Designation1 " +
                    " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                //sSQL = sSQL & " WHERE Contrat.CodeImmeuble like '" & sCodeimmeuble & "%' AND Avenant=0"
                //=== modif du 05 10 2014, remplace dans la requete le like par le =.
                General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '" +
                               StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' AND Avenant=0";
                General.sSQL = General.sSQL +
                               " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";

                var tmpModAdo = new ModAdo();
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                ///===========> Tested
                if (General.rstmp.Rows.Count > 0)
                {
                    txtContrat.Text = General.rstmp.Rows[0]["Designation1"].ToString();
                    txtCodeContrat.Text = General.rstmp.Rows[0]["MaxiNum"].ToString();
                }
                else
                {
                    txtContrat.Text = "";
                    txtCodeContrat.Text = "";
                }
                tmpModAdo.Dispose();

                //Si j'ai un contrat, je vérifie qu'il n'est pas résilié
                if (!string.IsNullOrEmpty(txtCodeContrat.Text))
                {

                    General.sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat " +
                                   " LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                                   " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" +
                                   DateTime.Today.ToString(General.FormatDateSQL) + "' OR Contrat.DateFin IS NULL)))" + " AND Contrat.CodeImmeuble='" +
                                   sCodeImmeuble + "'";
                    tmpModAdo = new ModAdo();
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtContrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                        txtCodeContrat.Text = General.rstmp.Rows[0]["NumContrat"] + "";
                        //TODO : Mondir - Not Found On VB6
                        //lblContrat.Visible = false;
                    }
                    else
                    {
                        //TODO : Mondir - Not Found On VB6
                        //lblContrat.Visible = true;
                    }
                }

                tmpModAdo?.Dispose();

                if (string.IsNullOrEmpty(txtContrat.Text))
                {
                    SSOleDBGrid1.Enabled = false;
                }
                else
                {
                    SSOleDBGrid1.Enabled = true;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "RechercheContrat");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="codeImmeublePrestations"></param>
        /// <returns></returns>
        public object fc_ChargePrestations(string codeImmeublePrestations)
        {
            DataTable rsPrest = default(DataTable);
            modAdorsPrest = new ModAdo();
            string sSQL = null;

            sSQL = "";

            //rsPrest.Open "Select * from preparationintervention where codeimmeuble ='" & codeImmeublePrestations & "'", adocnn, adOpenForwardOnly, adLockReadOnly
            //===  modif du 19 août 2011, ajout du champs code article.
            sSQL = "SELECT	PreparationIntervention.*, '' AS Designation FROM PreparationIntervention " +
                   " where codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(codeImmeublePrestations) + "'   order by No";

            rsPrest = modAdorsPrest.fc_OpenRecordSet(sSQL, SSOleDBGrid1, "No");

            if (rsPrest.Rows.Count > 0)
            {
                txtValorisation.Text = rsPrest.Rows[0]["Valorisation"] + "";
                txtDuree.Text = rsPrest.Rows[0]["Duree"] + "";
            }

            else
            {
                txtValorisation.Text = "0";
                txtDuree.Text = "0";
            }

            RemplitGrille(rsPrest);
            ContratResiliee();

            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="adotemp"></param>
        private void RemplitGrille(DataTable adotemp)
        {
            //----procédure pour remplir un tableau de type AddItem
            SSOleDBGrid1.DataSource = adotemp;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void ContratResiliee()
        {
            DataTable adotemp = default(DataTable);
            var modAdoadotemp = new ModAdo();
            string LenNumContrat = null;
            DataTable rsContratResilie = default(DataTable);
            var modAdorsContratResilie = new ModAdo();
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                Text3.Text = "";
                //        LenNumContrat = fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" & txtCodeImmeuble.Text & "'")


                //            sSQl = ""
                //            sSQl = "SELECT MAX(Contrat.DateEffet) AS MaxiNum, Contrat.Resiliee,Contrat.NumContrat " _
                //'                & " From Contrat "
                //            sSQl = sSQl & " WHERE Contrat.CodeImmeuble like '" & txtCodeImmeuble.Text & "%' AND Avenant=0"
                //            sSQl = sSQl & " GROUP BY Contrat.NumContrat ,Contrat.Resiliee"

                adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT Contrat.Avenant,contrat.NumContrat," +
                                                         " FacArticle.CodeArticle ,Facarticle.Designation1," +
                                                         " contrat.resiliee" +
                                                         " FROM FacArticle INNER JOIN Contrat ON FacArticle.CodeArticle = Contrat.CodeArticle" +
                                                         " Where  Contrat.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) +
                                                         "'" +
                                                         " ORDER BY Contrat.Resiliee DESC , Contrat.NumContrat DESC ," +
                                                         " Contrat.Avenant ");

                if (adotemp.Rows.Count > 0)
                {
                    General.sSQL =
                        "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                        "WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" + DateTime.Today.ToString(General.FormatDateSQL) +
                        "' OR Contrat.DateFin IS NULL))) AND Contrat.CodeImmeuble='" +
                        StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                    rsContratResilie = modAdorsContratResilie.fc_OpenRecordSet(General.sSQL);
                    if (rsContratResilie.Rows.Count > 0)
                    {
                        //TODO : Mondir - Controle Not Found ON VB6
                        //lblContrat.Visible = false;
                    }
                    else
                    {
                        //TODO : Mondir - Controle Not Found ON VB6
                        //lblContrat.Visible = true;
                    }
                    modAdorsContratResilie.Dispose();
                    rsContratResilie = null;
                    foreach (DataRow adotempRow in adotemp.Rows)
                    {
                        Text3.Text = Text3.Text + adotempRow["NumContrat"] + "  " + adotempRow["avenant"] + "  " +
                                     adotempRow["Designation1"] + "\r\n";
                    }
                }
                else
                {
                    //TODO : Mondir - Controle Not Found ON VB6
                    //lblContrat.Visible = false;
                }
                modAdoadotemp.Dispose();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Competence()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT  PreparationIntervention.Intervenant, Personnel.Nom, Personnel.Prenom, " +
                       " Qualification.Qualification" + " FROM         Qualification INNER JOIN" +
                       " Personnel ON Qualification.CodeQualif = Personnel.CodeQualif INNER JOIN" +
                       " PreparationIntervention " + " ON Personnel.Matricule = PreparationIntervention.Intervenant" +
                       " WHERE PreparationIntervention.Codeimmeuble = '" +
                       StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                var modAdorsCompetence = new ModAdo();
                rsCompetence = modAdorsCompetence.fc_OpenRecordSet(sSQL);

                ssGridComptence.DataSource = rsCompetence;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_Competence");

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Clear()
        {
            this.txtSEC_Code.Text = "";
            this.lbllibCodeChefSecteur.Text = "";
            this.txtCodeChefSecteur.Text = "";
            this.txtSelCodeImmeuble.Text = "";
            this.txtAdresse.Text = "";
            this.txtAdresse2_IMM.Text = "";
            this.txtAdresseFact1_IMM.Text = "";
            this.txtAdresseFact2_IMM.Text = "";
            this.txtAdresseFact3_IMM.Text = "";
            this.txtAdresseFact4_IMM.Text = "";
            //Me.txtAdresseGardien = ""
            //Me.txtAllumageRalenti = ""
            this.txtAngleRue.Text = "";
            //Me.txtAuteur = ""
            //Me.txtBoiteAClef =""
            //Me.txtClefAcces = ""
            this.txtCode1.Text = "";
            this.txtCode1.ReadOnly = false;
            //Me.txtCode2 =""
            this.txtCodeAcces1.Text = "";
            this.txtCodeAcces2.Text = "";
            this.txt159.Text = "";
            // Me.txtCodeCommercial = ""
            // Me.txtCodeContrat = ""
            // Me.txtCodeDepanneur = ""
            // Me.txtCodeDepanneur1 = ""
            // Me.txtCodeDispatcheur =""

            this.txtCodeImmeuble.Text = "";
            this.txtCodePostal.Text = "";
            this.txtCodePostal_IMM.Text = "";

            //===> Mondir le 29.11.2020 pour ajouter les modfis de la version V29.11.2020
            txt165.Text = "";
            txt166.Text = "";
            txt167.Text = "";
            txt168.Text = "";
            txt169.Text = "";
            txt170.Text = "";
            txt171.Text = "";
            txt172.Text = "";
            Check22.Checked = false;
            //===> Fin Modif Mondir

            //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
            txt_163.Text = "";
            txt_164.Text = "";
            //txt(164) = ""
            Check_23.Checked = false;
            txt_173.Text = "";
            ssCombo_23.Text = "";
            Label4_221.Text = "";
            //===> Fin Modif Mondir

            this.txtCodeReglement_IMM.Text = "";
            this.txtCodeTVA.Text = "";
            // Me.txtCodeTypeContrat = ""
            // Me.txtCom1 = ""
            // Me.txtCom2 = ""
            this.txtCommentaireFacture1.Text = "";
            this.txtCommentaireFacture2.Text = "";
            this.txtCptAppelFond.Text = "";
            // Me.txtCptClientGerant = ""
            // Me.txtCS1 = ""
            // Me.txtCS2 = ""
            // Me.txtCS3 = ""
            // Me.txtDate1 =""
            // Me.txtDate2 = ""
            // Me.txtDateAssemblee = ""
            // Me.txtDateCréation = ""
            // Me.txtDateRevision = ""
            // Me.txtDateVidange = ""
            // Me.txtDeleguation = ""
            // Me.txtDeleguationCS1 = ""
            //Me.txtFaxGardien = ""
            txt157.Text = "";
            //Me.txtGardien = ""
            txt154.Text = "";
            // Me.txthoraires = ""
            this.txtHorairesLoge.Text = "";
            // Me.txtMAJ = ""
            this.txtNCompte.Text = "";
            // Me.txtNettoyagePreparateur = ""
            this.txtObservations.Text = "";
            this.Check41.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.Check42.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.Check43.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.Check44.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.ChkSansReleveCompte.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.ChkListeRouge.CheckState = System.Windows.Forms.CheckState.Unchecked;
            Check13.CheckState = System.Windows.Forms.CheckState.Unchecked;

            this.txtoldCodeImmeuble.Text = "";
            // Me.txtRepartPrivatif = ""
            // Me.txtRepartProf = ""
            // Me.txtRepartTotal = ""
            // Me.txtRevisionRobinet = ""
            // Me.txtSitBoiteAClef = ""
            this.SpecifAcces.Text = "";
            // Me.txtSuivi = ""
            // Me.txtTelCS1 = ""
            // Me.txtTelCS2 = ""
            // Me.txtTelCS3 = ""
            //Me.txtTelGardien = ""
            txt155.Text = "";

            this.txtVille.Text = "";
            this.txtVilleFact_IMM.Text = "";
            this.txtSituation.Text = "";
            this.lblCodeEnergie.Text = "";
            this.lblEnergie.Text = "";
            this.txtCodeGestionnaire.Text = "";
            Label255.Text = "";
            Label256.Text = "";
            Label257.Text = "";
            Label251.Text = "";
            Label258.Text = "";

            SSOleDBCmbDep1.Text = "000";
            lblDep1Nom.Text = "";
            //lblDep1Prenom.Caption = ""
            lblDep1Qualif.Text = "";
            SSOleDBCmbDep1.Text = "";


            //            SSOleDBCmbDep2.Text = ""
            lblMatriculeDep2.Text = "";
            lblDep2Nom.Text = "";
            //lblDep2Prenom.Caption = ""
            lblDep2Qualif.Text = "";


            txtCodeChefSecteur.Text = "";
            lbllibCodeChefSecteur.Text = "";
            // lblPrenomChefSecteur.Caption = ""
            lblQualifChefSecteur.Text = "";

            lblMatriculeREspExpl.Text = "";
            lblLibRespExpl.Text = "";
            // lblPrenomRespExpl.Caption = ""
            lblQualifRespExpl.Text = "";

            //            SSOleDBCmbDispatch.Text = ""
            //            lblDispatchNom.Caption = ""
            //            lblDispatchPrenom.Caption = ""

            lblCodeCommercial.Text = "";
            lblCommercNom.Text = "";
            lblCommercPrenom.Text = "";
            txt107.Text = "";
            txt123.Text = "";
            ssCombo15.Text = "";
            ssCombo21.Text = "";
            txt160.Text = "";

            txt113.Text = "";

            txt118.Text = "";
            txt128.Text = "";
            txt117.Text = "";
            txt122.Text = "";
            txt125.Text = "";
            txt121.Text = "";
            txt120.Text = "";
            txt124.Text = "";
            txt114.Text = "";
            txt119.Text = "";
            txt128.Text = "";
            txt108.Text = "";
            txt103.Text = "";
            txt102.Text = "";
            txt106.Text = "";
            txt110.Text = "";
            txt109.Text = "";
            txt111.Text = "";
            lblMatriculeRondier.Text = "";
            lblRondierNom.Text = "";
            lblRondierQualif.Text = "";
            ssCombo13.Text = "";
            txt104.Text = "";
            txt105.Text = "";
            txt156.Text = "";
            txt161.Text = "";
            txtRefClient.Text = "";

            Label2510.Text = "";
            txt115.Text = "";
            txt112.Text = "";
            txt116.Text = "";

            ssCombo22.Text = "";
            ssCombo22.Value = "";
            Check12.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkDTANégatif.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkDTAPositif.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkDTAPositfSO.CheckState = System.Windows.Forms.CheckState.Unchecked;

            Check16.Checked = false;
            Check20.Checked = false;
            Check21.Checked = false;


            lblLibCodeReglement_IMM.Text = "";
            fc_InitialiseGrille();
            blModif = false;
            //=== modof du 13 12 2015.
            Label2511.Text = "";

            txtCompteDivers.Text = "";
            txtAnalytiqueImmeuble.Text = "";

            Opt3.Checked = false;
            Opt4.Checked = false;
            Opt5.Checked = false;
            Opt6.Checked = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void FC_ChargeOnglet()
        {
            DataTable rsEcritureCopro = default(DataTable);
            var modAdorsEcritureCopro = new ModAdo();
            int i = 0;
            string sCode = null;

            //if ((General.adocnn != null))
            //{
            //    if (!(General.adocnn.State == ConnectionState.Open))
            //    {
            //        return;
            //    }
            //}
            //else
            //{
            //    return;
            //}


            // cherche les grilles

            sCode = txtCodeImmeuble.Text;
            if (SSTab1.SelectedTab.Index == 0)
            {
                fc_ChargeGrilleGestionnaire(txtCode1.Text);

                // correspondant
            }
            else if (SSTab1.SelectedTab.Index == 1)
            {
                fc_ChargeGrilleimmCorrespondant(sCode);
                fc_ssGridCopro(sCode);
                fc_ssGridBE(sCode);


                fc_Competence();

            }
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            //===> Add this function
            else if (SSTab1.SelectedTab.Index == 2)
            {
                fc_LoadIDA();
            }
            else if (SSTab1.SelectedTab.Index == 4)
            {
                fc_IMRE_ImmRelance(sCode);

            }
            else if (SSTab1.SelectedTab.Index == 7)
            {
                //fC_OngletIntervention
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());



                // onglet technique.
            }
            else if (SSTab1.SelectedTab.Index == 4)
            {
                //        fc_GrilleArticle sCode
                //  fc_ChargeGrilleIntervenant sCode

            }
            else if (SSTab1.SelectedTab.Index == 6)
            {
                fc_OngletDocumention();

            }
            else if (SSTab1.SelectedTab.Index == 8)
            {
                fC_Ongletdevis();
                cmbDevis_Click(cmbDevis, new System.EventArgs());

            }
            else if (SSTab1.SelectedTab.Index == 3)
            {
                fc_ChargeFinance();
                txtArretDate.Text = DateTime.Today.ToString("dd/MM/yyyy");
                txtEcheance2.Text = DateTime.Today.ToString("dd/MM/yyyy");
                OptImmeuble_CheckedChanged(OptImmeuble, new System.EventArgs());
                OptImmeuble.Checked = true;
                txtImmeuble1.Value = txtCodeImmeuble.Text;
                txtImmeuble2.Value = txtCodeImmeuble.Text;
                General.sSQL = "SELECT ImmeublePart.NCompte ," + " ImmeublePart.Nom " + " FROM ImmeublePart  " +
                               " WHERE ImmeublePart.CodeImmeuble= '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' AND CompteCree=1 " +
                               " order by ImmeublePart.Nom";
                modAdorsEcritureCopro = new ModAdo();
                rsEcritureCopro = modAdorsEcritureCopro.fc_OpenRecordSet(General.sSQL);
                ComboComptes.DataSource = rsEcritureCopro;
                ComboComptes.DisplayLayout.Bands[0].Columns[0].Width = 300;
                ComboComptes.DisplayLayout.Bands[0].Columns[1].Width = 300;
            }
            // alimente la liste déroulante de la grille ssintervenant avec la table personnel
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropBE()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT BE.CodeBE AS [Code], BE.Libelle, BE.Contact, BE.Tel, BE.Fax, BE.eMail " +
                           " FROM BE ORDER BY BE.CodeBE";

            if (cmbBE.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.cmbBE), General.sSQL, "Code", true);

            }

            this.ssGridBE.DisplayLayout.Bands[0].Columns["CodeBE"].ValueList = this.cmbBE;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeGrilleGestionnaire(string sCode)
        {

            //    Dim i As Long

            //    sSQl = ""
            //    sSQl = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion," _
            //'        & " Gestionnaire.CodeQualif_Qua, '' as Qualification," _
            //'        & " Gestionnaire.TelGestion, Gestionnaire.TelPortable_GES," _
            //'        & " Gestionnaire.FaxGestion,  Gestionnaire.EMail_GES," _
            //'        & " Gestionnaire.Horaire_GES, Gestionnaire.MotdePasse_GES, Gestionnaire.code1" _
            //'        & " FROM Gestionnaire  "
            //    sSQl = sSQl & " where Gestionnaire.Code1 ='" & gFr_DoublerQuote(sCode) & "'"
            //    sSQl = sSQl & " and Gestionnaire.codeQualif_qua = '" & cGestionnaire & "'"
            //
            //    Set rsCorrespondants = New ADODB.Recordset
            //    rsCorrespondants.Open sSQl, adocnn, adOpenKeyset, adLockOptimistic
            //    Me.ssGridGestionnaire.ReBind
            //    ssGridGestionnaire.StyleSets("Selected").BackColor = &HFF8080
            //    ssGridGestionnaire.StyleSets("NoSelected").BackColor = &HFFFFFF
            //
            //    If txtCodeGestionnaire.Text <> "" And ssGridGestionnaire.Rows > 0 Then
            //        ssGridGestionnaire.MoveFirst
            //        For i = 0 To ssGridGestionnaire.Rows - 1
            //            If ssGridGestionnaire.Columns("CodeGestinnaire").Text = txtCodeGestionnaire.Text Then
            //                ssGridGestionnaire.Columns("CodeGestinnaire").CellStyleSet "Selected", ssGridGestionnaire.Row
            //                Exit For
            //            Else
            //                ssGridGestionnaire.Columns("CodeGestinnaire").CellStyleSet "NoSelected", ssGridGestionnaire.Row
            //            End If
            //            ssGridGestionnaire.MoveNext
            //        Next i
            //    End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="lPosition"></param>
        /// <param name="blnRemove"></param>
        private void fc_ChargeGrilleimmCorrespondant(string sCode, int lPosition = 0, bool blnRemove = true)
        {
            int i = 0;

            General.sSQL = "";
            //General.sSQL = "SELECT IMC_ImmCorrespondant.CodeImmeuble_IMM, " + " IMC_ImmCorrespondant.CodeCorresp_IMC," +
            //               " IMC_ImmCorrespondant.nom_imc, " + " IMC_ImmCorrespondant.CodeQualif_QUA " +
            //               " , '' as Qualification, " + " IMC_ImmCorrespondant.tel_imc, " +
            //               " IMC_ImmCorrespondant.telportable_imc, " + " IMC_ImmCorrespondant.Fax_IMC," +
            //               " IMC_ImmCorrespondant.email_imc, " + " IMC_ImmCorrespondant.Note_imc, PresidentCS_IMC " +
            //               " FROM  IMC_ImmCorrespondant" + " where CodeImmeuble_IMM= '" + sCode + "'";

            General.sSQL = "SELECT IMC_ImmCorrespondant.CodeImmeuble_IMM, " + " IMC_ImmCorrespondant.CodeCorresp_IMC," +
                           " IMC_ImmCorrespondant.nom_imc, " + " IMC_ImmCorrespondant.CodeQualif_QUA " +
                           " ,  " + " IMC_ImmCorrespondant.tel_imc, " +
                           " IMC_ImmCorrespondant.telportable_imc, " + " IMC_ImmCorrespondant.Fax_IMC," +
                           " IMC_ImmCorrespondant.email_imc, " + " IMC_ImmCorrespondant.Note_imc, PresidentCS_IMC ,'' as 'Président du conseil syndical' " +
                           " FROM  IMC_ImmCorrespondant" + " where CodeImmeuble_IMM= '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            General.sSQL = General.sSQL + " order by IMC_ImmCorrespondant.CodeCorresp_IMC ";

            modAdorsCorr = new ModAdo();
            rsCorr = modAdorsCorr.fc_OpenRecordSet(General.sSQL, ssGridImmCorrespondant, "CodeCorresp_IMC");
            this.ssGridImmCorrespondant.DataSource = rsCorr;
            ssGridImmCorrespondant.UpdateData();
            fc_DropQualite();

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropQualite()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Qualification.CodeQualif, Qualification.Qualification" +
                           " FROM Qualification where Correspondant_COR = 1";

            if (this.ssDropQualite.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropQualite, General.sSQL, "CodeQualif", true);

            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ssGridBE(string sCode)
        {
            try
            {
                //    sSQl = ""
                //    sSQl = "SELECT Imm_BE.CodeBE AS CodeBE_Imm,Imm_BE.CodeImmeuble,Imm_BE.Libelle, " _
                //'            & " Imm_BE.Contact, Imm_BE.Tel, Imm_BE.Fax, " _
                //'            & " Imm_BE.eMail " _
                //'            & " FROM Imm_BE " _
                //'            & " WHERE Imm_BE.CodeImmeuble='" & sCode & "'" _
                //'            & " ORDER BY Imm_BE.CodeBE"

                //== modif rachid du 18 janvier 2005

                General.sSQL = "SELECT Imm_BE.CodeBE ,Imm_BE.CodeImmeuble, '' as libelle," +
                               " '' as adresse , '' as CP , '' as ville, '' as contact, " +
                               " '' as Tel, '' as Fax ,'' as eMail " +
                               " FROM Imm_BE " + " WHERE Imm_BE.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'" +
                               " ORDER BY Imm_BE.CodeBE";
                RsBeInit:


                //            If rsBE Is Nothing Then
                //                Set rsBE = New ADODB.Recordset
                //            ElseIf rsBE.State = adStateOpen Then
                //                rsBE.Close
                //            End If
                modAdorsBE = new ModAdo();
                rsBE = modAdorsBE.fc_OpenRecordSet(General.sSQL);
                // rsBE.Open sSQl, adocnn, adOpenKeyset, adLockOptimistic

                this.ssGridBE.DataSource = rsBE;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                //ErrGridBE:
                //if (Err().Number == 3219)
                //{
                //    rsBE.CancelUpdate();
                //    // ERROR: Not supported in C#: ResumeStatement

                //}
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeimmeible"></param>
        private void fc_IMRE_ImmRelance(string sCodeimmeible)
        {
            string sSQL = null;

            try
            {

                //tva

                sSQL = "SELECT     IMRE_Noauto, Codeimmeuble, IMRE_Date, Matricule,IMRE_Contact, " +
                       " IMRE_Tel, IMRE_Fax, IMRE_Mail, IMRE_NoFacture, IMRE_Montant, IMRE_Commentaire" +
                       " From IMRE_ImmRelance" + " WHERE Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeible) + "'";

                modAdorsIMRE = new ModAdo();
                rsIMRE = modAdorsIMRE.fc_OpenRecordSet(sSQL, GridIMRE, "IMRE_Noauto");

                GridIMRE.DataSource = rsIMRE;



            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_IMRE_ImmRelance");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNameGrille"></param>
        private void fc_DropCodeetat(object sNameGrille = null)
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat";

            if (ssDropCodeEtat.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropCodeEtat, General.sSQL, "CodeEtat", true);


            }

            this.ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].ValueList = this.ssDropCodeEtat;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            string sWhere = null;
            string sComentaire;
            //=== version dédié à pz.
            if (General.sActiveVersionPz == "1")
            {
                //=== ajout de la table devis et de la table gestion standard.
                General.sSQL =
                    "SELECT Intervention.DateRealise, Intervention.NoIntervention,Intervention.Intervenant," +
                    " Intervention.CodeImmeuble," + " Intervention.CodeEtat, TypeCodeEtat.LibelleCodeEtat, " +
                    " Intervention.Commentaire,Immeuble.CodeDepanneur,Intervention.Designation,Personnel.CSecteur," +
                    " Intervention.Wave,Intervention.Duree" + " FROM         GestionStandard INNER JOIN" +
                    " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard " +
                    " LEFT OUTER JOIN DevisEnTete " + " ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis " +
                    " LEFT OUTER JOIN TypeCodeEtat " + " ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat " +
                    " LEFT OUTER JOIN Immeuble " + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble" +
                    " LEFT OUTER JOIN Personnel " + " ON Immeuble.CodeDepanneur = Personnel.Matricule";

            }
            else
            {
                General.sSQL =
                    "SELECT Intervention.DateRealise, Intervention.NoIntervention,Intervention.Intervenant, Intervention.CodeImmeuble," +
                    " Intervention.CodeEtat, TypeCodeEtat.LibelleCodeEtat, " +
                    " Intervention.Commentaire,Immeuble.CodeDepanneur,Intervention.Designation,Personnel.CSecteur,Intervention.Wave,Intervention.Duree" +
                    " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat =" +
                    " Intervention.CodeEtat) LEFT JOIN Immeuble ON Intervention.CodeImmeuble =" +
                    " Immeuble.CodeImmeuble LEFT JOIN Personnel ON Immeuble.CodeDepanneur=Personnel.Matricule";

            }

            sWhere = " WHERE Intervention.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            if (!string.IsNullOrEmpty(cmbIntervenant.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " and Intervention.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'";
                }
            }
            if (!string.IsNullOrEmpty(cmbStatus.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.CodeEtat='" + cmbStatus.Text + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.CodeEtat='" + cmbStatus.Text + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtinterDe.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.daterealise>='" +
                             Convert.ToDateTime(txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.daterealise>='" +
                             Convert.ToDateTime(txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtIntereAu.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.daterealise<='" +
                             Convert.ToDateTime(txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.daterealise<='" +
                             Convert.ToDateTime(txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txt158.Text))
            {
                sComentaire = txt158.Text.Replace(" ", "%");
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Commentaire like '%" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.Commentaire LIKE '%" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                }
            }
            if (!string.IsNullOrEmpty(txt162.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    //sWhere = " where Intervention.daterealise>='" & Format(.txtinterDe, "mm/dd/yyyy") & "'"
                    sWhere = " where Intervention.dateSaisie>='" + txt162.Text + " 00:00:00" + "'";
                }
                else
                {
                    sWhere = sWhere + " and Intervention.dateSaisie>='" + txt162.Text + " 00:00:00" + "'";
                    //sWhere = sWhere & " and  Intervention.daterealise>='" & Format(.txtinterDe, "mm/dd/yyyy") & "'"
                }
            }
            if (!string.IsNullOrEmpty(txtDateSaisieFin.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    //sWhere = " where Intervention.daterealise<='" & Format(.txtIntereAu, "mm/dd/yyyy") & "'"
                    sWhere = sWhere + " and  Intervention.dateSaisie<='" + txtDateSaisieFin.Text + " 23:59:59" + "'";
                }
                else
                {
                    //sWhere = sWhere & " and  Intervention.daterealise<='" & Format(.txtIntereAu, "mm/dd/yyyy") & "'"
                    sWhere = sWhere + " and  Intervention.dateSaisie<='" + txtDateSaisieFin.Text + " 23:59:59" + "'";
                }
            }
            if (!string.IsNullOrEmpty(cmbArticle.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where Intervention.Article ='" + cmbArticle.Text + "'";
                }
                else
                {
                    sWhere = sWhere + " and  Intervention.Article ='" + cmbArticle.Text + "'";
                }
            }

            if (!string.IsNullOrEmpty(txt100.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " WHERE DevisEntete.NoEnregistrement='" + StdSQLchaine.gFr_DoublerQuote(txt100.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + StdSQLchaine.gFr_DoublerQuote(txt100.Text) + "'";
                }
            }


            if (!string.IsNullOrEmpty(sWhere))
            {
                General.sSQL = General.sSQL + sWhere;
            }
            BisStrRequete = General.sSQL;
            General.sSQL = General.sSQL + " order by DateRealise desc";

            var modAdorsintervention = new ModAdo();
            rsintervention = modAdorsintervention.fc_OpenRecordSet(General.sSQL);

            Label4201.Text = "Total : " + rsintervention.Rows.Count;

            if (rsintervention.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention pour ces paramétres.", "Historique Intervention",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (SSTab1.SelectedTab.Index == 7)
                {
                    // .txtDateSaisieDe.SetFocus
                }
            }
            this.ssIntervention.DataSource = rsintervention;
            string sNoIntervention = General.getFrmReg(Variable.cUserDocImmeuble, "NoIntervention", "");
            var row = ssIntervention.Rows.Where(l => l.Cells["NoIntervention"].Value.ToString() == sNoIntervention).FirstOrDefault();
            if (row != null)
            {
                row.Activate();
                row.Selected = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_OngletDocumention()
        {
            try
            {
                string sCode = null;
                sCode = txtCodeImmeuble.Text;
                if (Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode) == 1)
                {
                    Video.fc_CreeDossierImm(sCode);
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\correspondance"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\correspondance\in"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\correspondance\out"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\photos"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\devis"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\devis\Internee"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\devis\Externe"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\contrats"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\contrats\Interne"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\contrats\Externe\"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\factures"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\factures\Interne"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\factures\Externe"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\fax"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\fax\in"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\fax\out"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\email"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\email\in"
                    //        fc_CreateDossier sCheminDossier & "\" & sCode & "\email\out"
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                DirDocClient.Tag = General.sCheminDossier + "\\" + txtCodeImmeuble.Text;
                ListDirectory(DirDocClient, DirDocClient.Tag.ToString());
                FilDocClient.Tag = DirDocClient.Tag;
                FilesInDirectory(FilDocClient, new DirectoryInfo(FilDocClient.Tag.ToString()));
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;


            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_OngletDocumention");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        private void ListDirectory(TreeView treeView, string path)
        {
            try
            {
                treeView.Nodes.Clear();
                treeView.SuspendLayout();

                //var stack = new Stack<TreeNode>();
                //var rootDirectory = new DirectoryInfo(path);
                //var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
                //stack.Push(node);

                //while (stack.Count > 0)
                //{
                //    var currentNode = stack.Pop();
                //    currentNode.EnsureVisible();
                //    var directoryInfo = (DirectoryInfo)currentNode.Tag;
                //    if(directoryInfo.Exists)
                //    foreach (var directory in directoryInfo.GetDirectories())
                //    {
                //        var childDirectoryNode = new TreeNode(directory.Name) { Tag = directory.FullName };
                //        childDirectoryNode.ImageIndex = 1;
                //        childDirectoryNode.SelectedImageIndex = 0;
                //        currentNode.Nodes.Add(childDirectoryNode);
                //        // stack.Push(childDirectoryNode);
                //    }

                //}

                //treeView.Nodes.Add(node);

                //treeView.ExpandAll();
                //treeView.ResumeLayout();
                //foreach (TreeNode TN in treeView.Nodes)
                //    ListSubFolders(TN);


                if (Directory.Exists(path))
                {
                    var rootDirectory = new DirectoryInfo(path);
                    if (rootDirectory.GetDirectories().Length > 0)
                        foreach (var Dir in rootDirectory.GetDirectories())
                        {
                            var node = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            var random = Guid.NewGuid().ToString();
                            node.Name = random;
                            node.ImageIndex = 1;
                            node.SelectedImageIndex = 0;
                            if (node.Text.Length < 248)
                            {
                                treeView.Nodes.Add(node);
                            }
                        }

                    treeView.ExpandAll();
                    treeView.ResumeLayout();
                    foreach (TreeNode TN in treeView.Nodes)
                        ListSubFolders(TN);
                }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="TN"></param>
        private void ListSubFolders(TreeNode TN)
        {
            try
            {

                DirectoryInfo DI = new DirectoryInfo(TN.Tag.ToString());
                if (DI.Exists)
                    if (DI.GetDirectories().Length > 0)
                    {
                        foreach (var Dir in DI.GetDirectories())
                        {
                            var childDirectoryNode = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            childDirectoryNode.ImageIndex = 1;
                            childDirectoryNode.SelectedImageIndex = 0;
                            var random = Guid.NewGuid().ToString();
                            childDirectoryNode.Name = random;
                            TN.Nodes.Add(childDirectoryNode);
                            ListSubFolders(childDirectoryNode);
                        }
                    }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="directoryInfo"></param>
        private void FilesInDirectory(ListBox listBox, DirectoryInfo directoryInfo)
        {
            try
            {
                listBox.Items.Clear();
                if (directoryInfo.Exists)
                    foreach (var file in directoryInfo.GetFiles())
                    {
                        listBox.Items.Add(file.Name);
                    }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fC_Ongletdevis()
        {
            string sSQL = null;

            sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle" + " FROM DevisCodeEtat";
            //InitialiseCombo .cmbDevisStatus, Adodc10, sSQL, "Code"
            modAdorsStatut = new ModAdo();
            rsStatut = modAdorsStatut.fc_OpenRecordSet(sSQL);
            sheridan.style("ECRAN BLEU", cmbDevisStatus);

            cmbDevisStatus.DataSource = rsStatut;

            // sSQL = "SELECT Intervenant_INT.Matricule_PER, Personnel.Nom" _
            //& "  FROM Intervenant_INT INNER JOIN Personnel ON" _
            //& " Intervenant_INT.Matricule_PER = Personnel.Matricule" _
            //& " WHERE Intervenant_INT.code1_tab ='" & gFr_DoublerQuote(txtCode1) & "'"
            sSQL = "SELECT Personnel.Matricule, Personnel.Nom" + "  FROM Personnel INNER JOIN SpecifQualif ON"
                + " Personnel.CodeQualif = SpecifQualif.CodeQualif " + " order by nom ";

            //InitialiseCombo .cmbDevisIntervenant, Adodc6, sSQL, "Matricule"
            modAdorsMatPers = new ModAdo();
            rsMatPers = modAdorsMatPers.fc_OpenRecordSet(sSQL);
            sheridan.style("ECRAN BLEU", cmbDevisIntervenant);
            cmbDevisIntervenant.DataSource = rsMatPers;

            sSQL = "SELECT CodeArticle , Designation1 " + " From FacArticle" + " WHERE CodeCategorieArticle ='I'";
            if (cmdDevisArticle.Rows.Count == 0)
            {
                // InitialiseCombo cmdDevisArticle, Adodc5, sSQL, "CodeArticle"
                modAdorsFAC = new ModAdo();
                rsFAC = modAdorsFAC.fc_OpenRecordSet(sSQL);
                sheridan.style("ECRAN BLEU", cmdDevisArticle);
                cmdDevisArticle.DataSource = rsFAC;
            }

            // ferme le recordset faisant référence au interventions liée au client
            modAdorsDevis?.Dispose();
            this.ssDevis.DataSource = null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDevis_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            string sWhere = null;

            sSQL = "SELECT DevisEntete.DateCreation,DevisEnTete.DateAcceptation, DevisEnTete.CodeImmeuble,"
                + " DevisEnTete.NumeroDevis, DevisEnTete.TitreDevis, DevisEnTete.CodeEtat,"
                + " TypeCodeEtat.LibelleCodeEtat, DevisEnTete.CodeDeviseur, Personnel.Nom"
                + " FROM (DevisEnTete LEFT JOIN Personnel ON DevisEnTete.CodeDeviseur ="
                + " Personnel.Matricule) LEFT JOIN TypeCodeEtat ON DevisEnTete.CodeEtat"
                + " = TypeCodeEtat.CodeEtat";

            sWhere = "";

            sWhere = " where DevisEnTete.codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

            if (!string.IsNullOrEmpty(cmbDevisIntervenant.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where DevisEnTete.CodeDeviseur='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisIntervenant.Text) + "'";
                }
                else
                {
                    sWhere = sWhere + " and DevisEnTete.CodeDeviseur='" + StdSQLchaine.gFr_DoublerQuote(cmbDevisIntervenant.Text) + "'";
                }
            }

            if (!string.IsNullOrEmpty(cmbDevisStatus.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where DevisEnTete.CodeEtat='" + cmbDevisStatus.Text + "'";
                }
                else
                {
                    sWhere = sWhere + " and  DevisEnTete.CodeEtat='" + cmbDevisStatus.Text + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtDevisDe.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where DevisEnTete.DateAcceptation>='" + Convert.ToDateTime(txtDevisDe.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  DevisEnTete.DateAcceptation>='" + Convert.ToDateTime(txtDevisDe.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtDevisAu.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where DevisEnTete.DateAcceptation<='" + Convert.ToDateTime(txtDevisAu.Text).ToString(General.FormatDateSQL) + "'";
                }
                else
                {
                    sWhere = sWhere + " and  DevisEnTete.DateAcceptation<='" + Convert.ToDateTime(txtDevisAu.Text).ToString(General.FormatDateSQL) + "'";
                }
            }
            if (!string.IsNullOrEmpty(txtDevisCommentaire.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " where DevisEnTete.TitreDevis like '%" + StdSQLchaine.gFr_DoublerQuote(txtDevisCommentaire.Text) + "%'";
                }
                else
                {
                    sWhere = sWhere + " and  DevisEnTete.TitreDevis like '%" + StdSQLchaine.gFr_DoublerQuote(txtDevisCommentaire.Text) + "%'";
                }
            }

            if (!string.IsNullOrEmpty(txt101.Text))
            {
                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = " WHERE DevisEntete.NoEnregistrement='" + txt101.Text + "'";
                }
                else
                {
                    sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + txt101.Text + "'";
                }
            }

            if (!string.IsNullOrEmpty(sWhere))
            {
                sSQL = sSQL + sWhere + " ORDER BY DateCreation DESC";
            }

            modAdorsDevis = new ModAdo();
            rsDevis = modAdorsDevis.fc_OpenRecordSet(sSQL);


            if (rsDevis.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis trouvé pour ces paramétres.", "Historique Intervention", MessageBoxButtons.OK, MessageBoxIcon.Information);
                if (SSTab1.SelectedTab.Index == 7)
                {
                    // .txt(162).SetFocus
                }
            }

            General.saveInReg(Variable.cUserDocImmeuble, "DevisDe", txtDevisDe.Text);
            General.saveInReg(Variable.cUserDocImmeuble, "DevisAu", txtDevisAu.Text);
            General.saveInReg(Variable.cUserDocImmeuble, "DevisStatus", cmbDevisStatus.Text);
            General.saveInReg(Variable.cUserDocImmeuble, "DevisIntervenant", cmbDevisIntervenant.Text);
            General.saveInReg(Variable.cUserDocImmeuble, "DevisArticle", cmdDevisArticle.Text);
            General.saveInReg(Variable.cUserDocImmeuble, "DevisCommentaire", txtDevisCommentaire.Text);

            this.ssDevis.DataSource = rsDevis;
            string noDevis = General.getFrmReg(Variable.cUserDocImmeuble, "NumeroDevis", "");
            var row = ssDevis.Rows.Where(l => l.Cells["NumeroDevis"].Value.ToString() == noDevis).FirstOrDefault();
            if (row != null)
            {
                row.Activate();
                row.Selected = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object fc_ChargeFinance()
        {
            object functionReturnValue = null;
            DataTable rsAdoFinance = default(DataTable);
            SqlDataAdapter SDArsAdoFinance = null;
            double MontantCredit = 0;
            double MontantDebit = 0;
            string compteTiers = null;

            string DateMini = null;
            string DateMaxi = null;
            int JourMaxiMois = 0;
            string sDateFacture = null;
            string sCritere = null;
            DataTable rsCptGdp = default(DataTable);
            var modAdorsCptGdp = new ModAdo();
            string sDateEch = null;

            try
            {
                //===> Mondir le 02.04.2021, code commented by Mondir https://groupe-dt.mantishub.io/view.php?id=2189
                //if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                //{
                //    sCritere = General.cCritereFinance;
                //}
                //else
                //{
                //    sCritere = "";
                //}
                sCritere = SAGE.GetCritere();
                //===> Fin Modif Mondir

                //===@@@ modif du 29 05 2017, desactive Sage.
                ///=============> Tested
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                    return functionReturnValue;
                }

                //==================> Tested
                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    General.fc_MajCompteTiersGdp(txtCodeImmeuble.Text, "");
                }

                //JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice));

                if (General.gfr_liaison("AfficheExercices", "1") == "1")
                {
                    Frame326.Visible = true;
                }
                else
                {
                    Frame326.Visible = false;
                }

                if (Frame326.Visible == true)
                {
                    if (cmbExercices.ActiveRow == null)
                        return null;
                    DateMini = cmbExercices.ActiveRow.Cells["Du"].Value.ToString();
                    DateMaxi = cmbExercices.ActiveRow.Cells["Au"].Value.ToString();
                }
                else
                {
                    //===> Mondir le 02.04.2021, code moved to SAGE.GetSoldeClientSearchParams https://groupe-dt.mantishub.io/view.php?id=2189
                    //if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
                    //{
                    //    DateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Today.Year);
                    //    JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year);
                    //    DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + Convert.ToString(DateTime.Today.Year);
                    //}
                    //else
                    //{
                    //    if (DateTime.Today.Month >= 1 && DateTime.Today.Month <= Convert.ToInt16(General.MoisFinExercice))
                    //    {
                    //        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year - 1);
                    //        //MoisFinExercice + 1
                    //        JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year);
                    //        DateMaxi = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //        //MoisFinExercice
                    //    }
                    //    else
                    //    {
                    //        DateMini = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //        //MoisFinExercice - 1
                    //        JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt16(General.MoisFinExercice), DateTime.Today.Year + 1);
                    //        DateMaxi = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year + 1);
                    //        //MoisFinExercice
                    //    }
                    //}

                    var soldeClientSearchParams = SAGE.GetSoldeClientSearchParams();
                    DateMini = soldeClientSearchParams.DateMini;
                    JourMaxiMois = soldeClientSearchParams.JourMaxiMois;
                    DateMaxi = soldeClientSearchParams.DateMaxi;

                    //====> Fin Modif Mondir
                }
                var SourceDT = new DataTable();
                SourceDT.Columns.Add("Journal");
                SourceDT.Columns.Add("Date Journal", typeof(DateTime));
                SourceDT.Columns.Add("Date Facture", typeof(DateTime));
                SourceDT.Columns.Add("Référence facture");
                SourceDT.Columns.Add("Echéance", typeof(DateTime));
                SourceDT.Columns.Add("Débit", typeof(double));
                SourceDT.Columns.Add("Crédit", typeof(double));
                SSGridSituation.DataSource = SourceDT;

                SAGE.fc_OpenConnSage();

                if (optCptCopro.Checked == true)
                {
                    compteTiers = ComboComptes.Text;
                }
                else if (optCptImmeuble.Checked == true)
                {
                    using (var tmpModAdo = new ModAdo())
                        compteTiers = tmpModAdo.fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
                    //        '=== modif temporaire 11 decembre 2009 , pour gaz de paris les comptes tiers sont crées dans
                    //        '=== dans la base delostal.
                    //        If UCase(adocnn.DefaultDatabase) = UCase(cNameGDP) Then
                    //
                    //                Set rsCptGdp = New adodb.Recordset
                    //                With rsCptGdp
                    //                    .Open "SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'", _
                    //'                            "Provider=SQLOLEDB.1;" _
                    //'                            & "Integrated Security=SSPI;" _
                    //'                            & "Persist Security Info=False;" _
                    //'                            & "User ID=sa;Initial Catalog=DELOSTAL;" _
                    //'                            & "Data Source=sql01"
                    //
                    //                    If Not (.EOF And .bof) Then
                    //                        compteTiers = nz(.Fields("NCompte").value, "")
                    //                    End If
                    //                    .Close
                    //                End With
                    //                Set rsCptGdp = Nothing
                    //        End If

                }

                if (optEcrituresNonLetrees.Checked == true)
                {

                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT EC_PIECE,EC_SENS,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date,EC_JOUR FROM F_ECRITUREC ";
                    General.sSQL = General.sSQL + "WHERE ";
                    General.sSQL = General.sSQL + " JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + " CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%' " + sCritere + " ) ";
                    General.sSQL = General.sSQL + "  or ";
                    General.sSQL = General.sSQL + " (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%' " + sCritere + " ))";
                    General.sSQL = General.sSQL + " ORDER BY EC_DATE ASC";

                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date,EC_JOUR  FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + " JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + " CT_NUM='" + compteTiers + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.CG_NUM  Like '411%' " + sCritere + "   ) or (F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "  ))";
                    General.sSQL = General.sSQL + " ORDER BY EC_DATE ASC";

                }
                rsAdoFinance = new DataTable();
                SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                SDArsAdoFinance.Fill(rsAdoFinance);

                //rsAdoFinance.Close

                if (rsAdoFinance.Rows.Count > 0)
                {
                    foreach (DataRow rsAdoFinanceRow in rsAdoFinance.Rows)
                    {
                        if (rsAdoFinanceRow["JO_Num"].ToString().ToUpper() == General.cAN.ToUpper())
                        {
                            sDateFacture = rsAdoFinanceRow["EC_Echeance"] + "";

                            if (sDateFacture.Contains("01/01/1900") || sDateFacture.Contains("01/01/1753"))
                            {
                                sDateFacture =
                                Convert.ToInt32(rsAdoFinanceRow["EC_Jour"]).ToString("00") +
                                General.Mid(rsAdoFinanceRow["JM_Date"].ToString(), 3,
                                    rsAdoFinanceRow["JM_Date"].ToString().Length - 2);
                            }
                        }
                        else
                        {
                            sDateFacture =
                                Convert.ToInt32(rsAdoFinanceRow["EC_Jour"]).ToString("00") +
                                General.Mid(rsAdoFinanceRow["JM_Date"].ToString(), 3,
                                    rsAdoFinanceRow["JM_Date"].ToString().Length - 2);
                        }


                        sDateEch = rsAdoFinanceRow["EC_Echeance"] + "";
                        if (sDateEch.Contains("01/01/1900") || sDateEch.Contains("01/01/1753"))
                        {
                            sDateEch =
                            Convert.ToInt32(rsAdoFinanceRow["EC_Jour"]).ToString("00") +
                            General.Mid(rsAdoFinanceRow["JM_Date"].ToString(), 3,
                                rsAdoFinanceRow["JM_Date"].ToString().Length - 2);
                        }

                        if (rsAdoFinanceRow["EC_Sens"].ToString() == "1") //Colonne du montant dans les crédits
                        {
                            var Row = SourceDT.NewRow();
                            Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                            Row["Date Journal"] = rsAdoFinanceRow["JM_Date"];
                            Row["Date Facture"] = sDateFacture;
                            Row["Référence facture"] = rsAdoFinanceRow["EC_Piece"];
                            Row["Echéance"] = sDateEch;//rsAdoFinanceRow["EC_Echeance"]
                            Row["Débit"] = DBNull.Value;
                            Row["Crédit"] = rsAdoFinanceRow["EC_Montant"];
                            SourceDT.Rows.Add(Row);
                            SSGridSituation.DataSource = SourceDT;
                        }
                        else
                        {
                            var Row = SourceDT.NewRow();
                            Row["Journal"] = rsAdoFinanceRow["JO_Num"];
                            Row["Date Journal"] = rsAdoFinanceRow["JM_Date"];
                            Row["Date Facture"] = sDateFacture;
                            Row["Référence facture"] = rsAdoFinanceRow["EC_Piece"];
                            Row["Echéance"] = sDateEch;//rsAdoFinanceRow["EC_Echeance"]
                            Row["Débit"] = rsAdoFinanceRow["EC_Montant"];
                            Row["Crédit"] = DBNull.Value;
                            SourceDT.Rows.Add(Row);
                            SSGridSituation.DataSource = SourceDT;
                        }
                    }
                }
                rsAdoFinance?.Dispose();
                SDArsAdoFinance?.Dispose();

                //====> Mondir le 02.04.2021, commented by Mondir to fix https://groupe-dt.mantishub.io/view.php?id=2189
                //====> If you want to add a modificaiion, to this code, please look to this fonction first SAGE.GetSoldeClient
                //if (optEcrituresNonLetrees.Checked == true)
                //{
                //    //        sSQl = ""
                //    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                //    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                //    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                //    General.sSQL = "";
                //    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                //    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                //    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                //    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "  )" + " or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

                //}
                //else
                //{
                //    //        sSQl = ""
                //    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                //    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                //    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                //    General.sSQL = "";
                //    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                //    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                //    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                //    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

                //}
                //rsAdoFinance = new DataTable();
                //SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                //SDArsAdoFinance.Fill(rsAdoFinance);
                //if (rsAdoFinance.Rows.Count > 0)
                //{
                //    MontantCredit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                //}
                //rsAdoFinance?.Dispose();
                //SDArsAdoFinance?.Dispose();

                //if (optEcrituresNonLetrees.Checked == true)
                //{
                //    //        sSQl = ""
                //    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                //    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                //    //        sSQl = sSQl & " AND ((F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                //    General.sSQL = "";
                //    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                //    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                //    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                //    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + " ) " + " or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";
                //}
                //else
                //{
                //    //        sSQl = ""
                //    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                //    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                //    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                //    General.sSQL = "";
                //    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                //    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                //    General.sSQL = General.sSQL + "CT_NUM='" + compteTiers + "'";
                //    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + ") " + " or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'" + sCritere + "))";

                //}
                //rsAdoFinance = new DataTable();
                //SDArsAdoFinance = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                //SDArsAdoFinance.Fill(rsAdoFinance);
                //if (rsAdoFinance.Rows.Count > 0)
                //{
                //    MontantDebit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                //}
                //rsAdoFinance?.Dispose();
                //SDArsAdoFinance?.Dispose();
                //rsAdoFinance = null;

                //txtSoldeCompte.Text = Convert.ToString(MontantDebit - MontantCredit);
                //txtTotalFacture.Text = Convert.ToString(MontantDebit);
                //txtTotalRegle.Text = Convert.ToString(MontantCredit);

                var soldeClient = SAGE.GetSoldeClient(optEcrituresNonLetrees.Checked, DateMini, DateMaxi, compteTiers, sCritere);
                txtSoldeCompte.Text = soldeClient.Solde.ToString();
                txtTotalFacture.Text = soldeClient.Debit.ToString();
                txtTotalRegle.Text = soldeClient.Credit.ToString();
                //===> Fin Modif Mondir

                SAGE.fc_CloseConnSage();
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " fc_ChargeFinance ");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptImmeuble_CheckedChanged(object sender, EventArgs e)
        {
            if (OptImmeuble.Checked)
            {
                txtImmeuble1.Text = "";
                txtImmeuble2.Text = "";
                ssCombo19.Text = "";
                txtSyndic1.Visible = false;
                txtSyndic2.Visible = false;
                ssCombo14.Visible = false;
                Label4198.Visible = false;
                txtCommercial.Visible = false;
                Label449.Visible = false;
                Label455.Visible = false;
                Label458.Visible = false;
                txtImmeuble1.Visible = true;
                txtImmeuble2.Visible = true;
                ssCombo19.Visible = true;
                Label4199.Visible = true;
                Label453.Visible = true;
                Label457.Visible = true;
                txtParticulier1.Visible = false;
                ssCombo20.Visible = false;
                Label4200.Visible = false;
                txtParticulier2.Visible = false;
                Label454.Visible = false;
                Label456.Visible = false;
                cmdSsyndic.Visible = false;
                cmdSPart.Visible = false;
                txtImmeuble1.Focus();

                OptParticulier.Checked = false;
                OptSyndic.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_LoadMailSite(string sCodeImmeuble)
        {

            string sSQL = null;


            try
            {
                sSQL = "SELECT     Codeimmeuble, adresseMail " +
                       "From ImmeubleMails " +
                       "WHERE     Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(General.nz(sCodeImmeuble, "@@@@@@@@@").ToString()) + "'";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                modAdorsMail = new ModAdo();
                rsMail = modAdorsMail.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridMailSiteWeb.DataSource = rsMail;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_LoadMailSite");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ChargeAppareils()
        {

            string SQL = null;

            SQL = "SELECT PDAB_Noauto, '' as Localisation,  CodeUO, CodeImmeuble, CodeChaufferie, CodeNumOrdre, ";
            SQL = SQL + " NumAppareil, Matricule, Type_Compteur, '' as TypeLibelle ,Libelle, Energie, ";
            SQL = SQL + " Niveau_Mini, Niveau_Maxi, DiametreCuve, OrigineP1 ";
            SQL = SQL + " FROM Imm_Appareils ";
            SQL = SQL + " WHERE ";
            SQL = SQL + "  CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            //SQL = SQL & " AND CodeNumOrdre = '" & txtNumOrdre.Text & "'"
            //If sUChaufferie = "1" Then
            //    SQL = SQL & " AND CodeChaufferie = '" & txtCodeChaufferie.Text & "'"
            //End If
            SQL = SQL + " ORDER BY Type_Compteur, Libelle ";

            if ((modAdorsCompteurs == null))
            {
                modAdorsCompteurs = new ModAdo();
            }

            rsCompteurs = modAdorsCompteurs.fc_OpenRecordSet(SQL);

            GridCompteurs.DataSource = rsCompteurs;

            SQL = "SELECT CODE, LIBELLE ";
            SQL = SQL + " FROM P1_TypeCpt ";
            SQL = SQL + " ORDER BY CODE";

            sheridan.InitialiseCombo(Drop_TypeCpt, SQL, "CODE", true, ModP1.adoP1);

            // Drop_TypeCpt.DisplayLayout.Bands[0].Columns[1].Width = (int)(Math.Abs(Drop_TypeCpt.DisplayLayout.Bands[0].Columns[1].Width) * 1.5);
            GridCompteurs.DisplayLayout.Bands[0].Columns["Type_Compteur"].ValueList = Drop_TypeCpt;

            //=== localisation.
            SQL = "SELECT     PDAB_Libelle AS Localisation, PDAB_Noauto AS ID" + " From PDAB_BadgePDA " + " WHERE  Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' " + " ORDER BY Localisation";

            sheridan.InitialiseCombo(DropLocalisation, SQL, "Localisation", true);
            GridCompteurs.DisplayLayout.Bands[0].Columns["Localisation"].ValueList = DropLocalisation;

            SQL = "SELECT NumAppareil AS [Appareil], Libelle, Energie, NumCompteur AS [Matricule], Type_Compteur AS [Type compteur], DiametreCuve AS [Diametre cuve] ";
            SQL = SQL + " FROM P1_Appareil ";
            SQL = SQL + " INNER JOIN P12000 ON ";
            SQL = SQL + " P1_Appareil.NoFichierGraphe = P12000.NoFichierGraphe ";
            SQL = SQL + " WHERE  ";
            SQL = SQL + " P12000.CodeAffaire ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + " '";
            //SQL = SQL & " AND P12000.CodeNumOrdre = '" & txtNumOrdre.Text & "' "

            sheridan.InitialiseCombo(cmbDrpDwnAppareils, SQL, "Appareil", true, ModP1.adoP1);
            GridCompteurs.DisplayLayout.Bands[0].Columns["NumAppareil"].ValueList = cmbDrpDwnAppareils;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCpteRendu_Click(object sender, EventArgs e)
        {
            General.saveInReg(Variable.cUserDocRelTech, "NewVar", txtCodeImmeuble.Text);
            General.saveInReg(Variable.cUserDocRelTech, "Origine", this.Name);
            View.Theme.Theme.Navigate(typeof(UserDocRelTech));
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocRelTech);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeArticle , Designation1 " + " From FacArticle";
            //If cmbArticle.Rows = 0 Then
            sheridan.InitialiseCombo(cmbArticle, General.sSQL, "CodeArticle");
            //End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeBe"></param>
        public void fc_InfoBe(string sCodeBe, UltraGridRow Row)
        {

            DataTable rs = default(DataTable);
            var modAdors = new ModAdo();
            try
            {
                //_btnCpteRendu

                General.sSQL = "SELECT libelle , adresse, cp, Ville, Contact, Tel, Fax, Email " + " FROM BE" + " WHERE CodeBe=" + StdSQLchaine.gFr_DoublerQuote(sCodeBe) + "";

                rs = modAdors.fc_OpenRecordSet(General.sSQL);

                if (rs.Rows.Count > 0)
                {
                    Row.Cells["Libelle"].Value = rs.Rows[0]["Libelle"] + "";
                    Row.Cells["adresse"].Value = rs.Rows[0]["Adresse"] + "";
                    Row.Cells["CP"].Value = rs.Rows[0]["CP"] + "";
                    Row.Cells["Ville"].Value = rs.Rows[0]["Ville"] + "";
                    Row.Cells["Contact"].Value = rs.Rows[0]["contact"] + "";
                    Row.Cells["Tel"].Value = rs.Rows[0]["Tel"] + "";
                    Row.Cells["Fax"].Value = rs.Rows[0]["fax"] + "";
                    Row.Cells["EMail"].Value = rs.Rows[0]["Email"] + "";
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_InfoBe");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_Validated(object sender, EventArgs e)
        {

            General.sSQL = "SELECT Personnel.Nom" + " From Personnel" + " WHERE (((Personnel.Matricule)='" + cmbIntervenant.Text + "'))";
            using (var tmpModAdo = new ModAdo())
            {
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    lblLibIntervenant.Text = General.nz(General.rstmp.Rows[0]["Nom"], "").ToString();
                }
                else
                {
                    lblLibIntervenant.Text = "";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom, "
                + " Qualification.Qualification" + " FROM Qualification INNER JOIN Personnel" +
                " ON (Qualification.CodeQualif = Personnel.CodeQualif)" + " ORDER BY nom ";
            //Personnel.Nom, Personnel.Prenom, Qualification.Qualification ASC"
            //& " WHERE (((IntervenantImm_INM.Codeimmeuble_IMM)='" & txtCodeImmeuble & "'))"

            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_Click(object sender, EventArgs e)
        {
            if (cmbIntervenant.ActiveRow != null)
                lblLibIntervenant.Text = cmbIntervenant.ActiveRow.Cells["Nom"].Value.ToString();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat";

            //If cmbStatus.Rows = 0 Then
            sheridan.InitialiseCombo(cmbStatus, General.sSQL, "CodeEtat");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_Validated(object sender, EventArgs e)
        {
            General.sSQL = "SELECT TypeCodeEtat.LibelleCodeEtat" + " From TypeCodeEtat" + " WHERE TypeCodeEtat.CodeEtat ='" + cmbStatus.Text + "'";
            using (var tmpModAdo = new ModAdo())
            {
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    lblLibStatus.Text = General.nz(General.rstmp.Rows[0]["LibelleCodeEtat"], "").ToString();
                }
                else
                {
                    lblLibStatus.Text = "";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmd4_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            var Index = Convert.ToInt32(btn.Tag);
            string sSQLgmao = null;
            string sSelectionFormula = null;
            string sReturn = null;
            try
            {
                switch (Index)
                {

                    case 4:

                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Contacter Axeciel");

                        //=== geocode une adresse
                        //            With frmGeocoder
                        //                sTomtomLat = ""
                        //                stomtomLong = ""
                        //                sTomtomAdresse = ""
                        //                .TxtAdresse = TxtAdresse
                        //                .txtCP = TxtCodePostal
                        //                .TxtVille = TxtVille
                        //                .txtCodeImmeuble = txtCodeImmeuble
                        //                If sTomtom = "1" Then
                        //                    Label25(10) = fc_IdTomtom(txtCodeImmeuble, Label25(10))
                        //
                        //                End If
                        //                .Show vbModal
                        //                If sTomtomLat <> "" Then
                        //                    txt(115).Text = sTomtomLat
                        //                    txt(112).Text = stomtomLong
                        //                    txt(116).Text = sTomtomAdresse
                        //                End If
                        //            End With
                        //            Unload frmGeocoder
                        //=== geocodage.
                        //break;
                        var frmGeocoderV2 = new frmGeocoderV2();

                        General.sLattitudeImmeuble = "";
                        General.sLongitudeimmeuble = "";
                        General.sAdresseGPS = "";

                        frmGeocoderV2.TxtAdresse.Text = txtAdresse.Text;
                        frmGeocoderV2.TxtCodePostal.Text = txtCodePostal.Text;
                        frmGeocoderV2.TxtVille.Text = txtVille.Text;
                        frmGeocoderV2.ShowDialog();
                        if (!string.IsNullOrEmpty(General.sLongitudeimmeuble))
                        {

                            txt115.Text = General.sLattitudeImmeuble;
                            txt112.Text = General.sLongitudeimmeuble;
                            txt116.Text = General.sAdresseGPS;
                        }
                        break;


                    case 7:

                        //ModMain.fc_Navigue(this, General.gsCheminPackage + General.ProgUserDocReglement);
                        View.Theme.Theme.Navigate(typeof(UserDocReglement));
                        break;
                    case 8:
                        sSQLgmao = "SELECT     COP_NoAuto From COP_ContratP2 WHERE  codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' ORDER BY COP_NoAuto DESC";
                        using (var tmpModAdo = new ModAdo())
                            sSQLgmao = tmpModAdo.fc_ADOlibelle(sSQLgmao);
                        if (!string.IsNullOrEmpty(sSQLgmao))
                        {
                            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                            ModParametre.fc_SaveParamPosition("", Variable.cUserDocP2, sSQLgmao);
                            View.Theme.Theme.Navigate(typeof(UserDocP2V2));
                            //,  , Me.Left
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas de fiche GMAO associée à cet immeuble.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;


                    case 9:
                        //=== formulaire historique des listes rouges.
                        frmHistoListeRouge frmHistoListeRouge = new frmHistoListeRouge();
                        frmHistoListeRouge.txtCodeimmeuble.Text = txtCodeImmeuble.Text;
                        frmHistoListeRouge.ShowDialog();
                        break;

                    case 10:
                        General.fc_CreateFact(true, false, ssIntervention.ActiveRow.Cells["Codeetat"].Text, "", txtCodeImmeuble.Text,
                        this, Convert.ToInt32(General.nz(ssIntervention.ActiveRow.Cells["nointervention"].Text, 0)), General.fc_GetNumStandardFromInter(Convert.ToInt32(General.nz(ssIntervention.ActiveRow.Cells["nointervention"].Text, 0))));
                        break;

                    case 11:
                        General.fc_CreateFact(true, true, "", ssDevis.ActiveRow.Cells["NumeroDevis"].Text, txtCodeImmeuble.Text, this, 0, General.fc_GetNumStandardFromDevis(General.nz(ssDevis.ActiveRow.Cells["NumeroDevis"].Text, 0) + ""));
                        break;
                    case 12:
                        if (Opt3.Checked || Opt4.Checked)
                        {
                            //===> Mondir le 05.10.2020, ajout du Cursors.WaitCursor
                            Cursor = Cursors.WaitCursor;
                            sSelectionFormula = "{immeuble.codeimmeuble} = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                            //===> Mondir le 05.10.2020, exception cuz the table was null
                            ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                            //===> Fin Modif Mondir
                            ModCrystalPDF.tabFormulas[0].sNom = "";
                            sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.sEtatExportImmAstreinteV9, "", "", "", "", CheckState.Checked);
                            Cursor = Cursors.Default;
                            ModuleAPI.Ouvrir(sReturn);
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Affichage 24/24 non disponible pour cette immeuble", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        break;

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "Cmd4_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_Ajouter();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Ajouter()
        {
            if (blModif == true)
            {
                switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_InitialiseGrille();
            fc_Clear();
            fc_BloqueForm("Ajouter");
            frmResultat.Visible = false;
            fc_ClearPrestations();
            fc_LoadMailSite("");
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ClearPrestations()
        {
            SSOleDBGrid1.DataSource = null;

        }
        /// <summary>
        /// Tested
        /// Modifs de la version 29.09.2020 ajoutée par Mondir le 30.09.2020
        /// Modifs de la version 29.09.2020 testée par Mondir le 30.09.2020
        /// </summary>
        private void fc_Rechercher()
        {
            string sCode = null;

            try
            {
                if (string.IsNullOrEmpty(txtCode1.Text))
                {
                    General.Execute("DELETE FROM Immeuble WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "'");
                }
                frmResultat.Visible = false;

                //Mondir : Changed the query to point Immeuble.CodeCommercial
                //https://groupe-dt.mantishub.io/view.php?id=1704
                string requete = "SELECT     Immeuble.CodeImmeuble AS \"Code Immeuble\", " + "Immeuble.Adresse AS \"adresse\", Immeuble.Ville AS \"Ville\", " +
                                 " Immeuble.AngleRue AS \"angle de rue\", " + " Immeuble.Code1 AS \"Gerant\", Immeuble.CodeCommercial as \"Mat. commercial\"" +
                                 " FROM         Immeuble";
                //====> Mondir le 30.06.2020, supprimé cla jointure INNER JOIN" + Table1 ON Immeuble.Code1 = Table1.Code1
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_ChargeEnregistrement(sCode);
                    //fc_ChargeGrilleGestionnaire scode
                    //fc_ChargeGrilleInterve
                    General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        //fc_ChargeGrilleGestionnaire scode
                        //fc_ChargeGrilleInterve
                        General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                txtAdresse.Focus();
                SSTab1.SelectedTab = SSTab1.Tabs[0];


            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheClient_Click(object sender, EventArgs e)
        {
            fc_RechercheClient();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdRechercheReglement_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete =
                    "SELECT CodeReglement.Code as \"code\", CodeReglement.Libelle as \"Libelle\", CodeReglement.ModeReglt as \"Mode de Reglement\" FROM CodeReglement";
                string where_order = "";
                SearchTemplate fg =
                    new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche de factures" };
                fg.SetValues(new Dictionary<string, string> { { "Code", txtCodeReglement_IMM.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeReglement_IMM.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_AfficheReglement(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                    txtCodeReglement_IMM.Focus();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeReglement_IMM.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_AfficheReglement(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                        txtCodeReglement_IMM.Focus();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheReg_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_sauver();

            switch (SSTab1.SelectedTab.Index)
            {
                case 1:
                    ssGridCopro.DataSource = rsCopro;
                    if (rsCorrespondants != null)
                        ssGridImmCorrespondant.DataSource = rsCorrespondants;
                    break;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSPart_Click(object sender, EventArgs e)
        {
            ReportDocument CR = new ReportDocument();
            CR.Load(General.strSyntheseReleveDeviseur);
            CrystalReportFormView Form = new CrystalReportFormView(CR);
            Form.Show();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSsyndic_Click(object sender, EventArgs e)
        {
            ReportDocument CR = new ReportDocument();
            CR.Load(General.strSyntheseReleveDeComerc);
            CrystalReportFormView Form = new CrystalReportFormView(CR);
            Form.Show();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSuivImmGridGestionnaire_Click(object sender, EventArgs e)
        {
            fc_ChargeGrilleimmCorrespondant(txtCodeImmeuble.Text, ssGridImmCorrespondant.Rows.Count, false);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            fc_supprimer();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_supprimer()
        {
            string sSQL = null;

            try
            {
                if (!string.IsNullOrEmpty(General.nz(txtCodeImmeuble, "").ToString()))
                {

                    switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("êtes-vous sûr de vouloir supprimer ce immeuble ?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.OK:
                            break;

                        default:
                            return;

                            //Suppression annulée
                            break;
                    }
                }
                else
                {
                    return;
                }

                // supprime le client
                sSQL = "delete  from immeuble where codeimmeuble='" + txtCodeImmeuble.Text + "'";
                General.Execute(sSQL);
                // supprime les gestionnaires liée au client
                //sSql = ""
                //adocnn.Execute sSql
                // supprime les intervenants liées au client
                //sSql = ""
                //adocnn.Execute sSql
                // initialise ablancs les controles
                fc_Clear();
                // bloque les onglets
                fc_BloqueForm();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_supprimer");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVersCodeDepanneur_Click(object sender, EventArgs e)
        {
            int i = 0;
            try
            {
                frmCodeDepanneur frmCodeDepanneur = new frmCodeDepanneur();
                frmCodeDepanneur.ShowDialog();
                frmCodeDepanneur?.Dispose();

                foreach (SearchTemplate Form in Application.OpenForms.Cast<Form>().Where(ele => ele is SearchTemplate))
                    Form.Close();

                fc_ChargeEnregistrement(txtCodeImmeuble.Text);
                SSTab1.SelectedTab = SSTab1.Tabs[1];
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdVersCodeDepanneur_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboComptes_ValueChanged(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(ComboComptes.Text))
            {
                optCptCopro.Checked = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboComptes_Click(object sender, EventArgs e)
        {
            optCptCopro.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            if (DATA1.Rows.Count == 0)
            {
                DATA1.Rows.Add(DATA1.NewRow());
            }
            DATA1.Rows[0]["Commentaire1"] = Text1.Text;
            DATA1.Rows[0]["Commentaire2"] = Text2.Text;
            SCBDATA1 = new SqlCommandBuilder(SDADATA1);
            int xx = SDADATA1.Update(DATA1);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirDocClient_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FilDocClient.Tag = DirDocClient.Tag;
            FilesInDirectory(FilDocClient, new DirectoryInfo(e.Node.Tag.ToString()));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FilDocClient_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string sChemin = "";
                if (FilDocClient.SelectedItem != null)
                {
                    if (DirDocClient.SelectedNode != null && DirDocClient.SelectedNode.Tag != null)
                    {
                        sChemin = DirDocClient.SelectedNode.Tag + "\\" + FilDocClient.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }
                    else
                    {
                        sChemin = DirDocClient.Tag + "\\" + FilDocClient.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }

                }

            }
            catch (Exception ee) { Erreurs.gFr_debug(ee, this.Name + ";File1_DblClick"); }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Droit()
        {
            string sSQL = null;
            string sDroitcompt = null;
            string codeQualif = "";

            try
            {
                //===> Mondir le 10.03.2021, https://groupe-dt.mantishub.io/view.php?id=2286
                ssCombo13.Enabled = false;
                ssCombo15.Enabled = false;
                //===> Fin Modif Mondir

                //===> Mondir le 23.03.2021, https://groupe-dt.mantishub.io/view.php?id=2286#c5703 for LONG also
                //if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper() || General.adocnn.Database.ToUpper() == General.cNameDelostalTest.ToUpper())
                //{

                //===  controlesi l'utilisateur a droit de saisir le responsable d'exploitation.
                using (var tmpModAdo = new ModAdo())
                {

                    //===> Mondir le 02.12.2020, https://groupe-dt.mantishub.io/view.php?id=2108
                    codeQualif = tmpModAdo.fc_ADOlibelle($"SELECT CodeQualif FROM Personnel WHERE Login = '{StdSQLchaine.gFr_DoublerQuote(General.fncUserName())}'");
                    //===> Mondir le 09.03.2021, ajout de la condition codeQualif == "14" https://groupe-dt.mantishub.io/view.php?id=2286
                    if (codeQualif?.ToUpper() == "DIR")
                    {
                        ssCombo13.Enabled = true;
                        ssCombo15.Enabled = true;
                        //===> Mondir le 10.30.2021, commented line bellow pour corriger pour corriger https://groupe-dt.mantishub.io/view.php?id=2286#c5565
                        //ssCombo15.Enabled = true;
                        //===> Fin Modif Mondir
                        //return;
                    }
                    else if (codeQualif?.ToUpper() == "14")
                    {
                        ssCombo13.Enabled = true;
                    }
                    //===> Fin Modif Mondir

                    //====> Mondir le 10.03.2021, https://groupe-dt.mantishub.io/view.php?id=2286
                    //sDroitcompt = tmpModAdo.fc_ADOlibelle("SELECT AUT_Nom From AUT_Autorisation " + " WHERE  (AUT_Objet = '" +
                    //                                      StdSQLchaine.gFr_DoublerQuote(Variable
                    //                                          .cSaisieReponsable) + "') AND (AUT_Nom = '" +
                    //                                      StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) +
                    //                                      "')" + " AND (AUT_Formulaire = '" +
                    //                                      StdSQLchaine.gFr_DoublerQuote(Variable.cUserDocImmeuble) +
                    //                                      "') ");
                    //===> Fin Modif Mondir


                }


                //====> Mondir le 10.03.2021, https://groupe-dt.mantishub.io/view.php?id=2286
                //if (!string.IsNullOrEmpty(sDroitcompt))
                //{
                //    ssCombo13.Enabled = true;
                //}
                //else
                //{

                //    ssCombo13.Enabled = false;
                //}
                //===> Fin Modif Mondir

                //===  controlesi l'utilisateur a droit de saisir le commercial..
                //===> Mondir le 15.03.2021 https://groupe-dt.mantishub.io/view.php?id=2286#c5609
                //using (var tmpModAdo = new ModAdo())
                //    sDroitcompt = tmpModAdo.fc_ADOlibelle("SELECT AUT_Nom From AUT_Autorisation " + " WHERE  (AUT_Objet = '" + StdSQLchaine.gFr_DoublerQuote(Variable.cSaisieCommercial) + "') AND (AUT_Nom = '" +
                //        StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')" + " AND (AUT_Formulaire = '" + StdSQLchaine.gFr_DoublerQuote(Variable.cUserDocImmeuble) + "') ");

                //if (!string.IsNullOrEmpty(sDroitcompt))
                //{
                //    ssCombo15.Enabled = true;
                //}
                //else
                //{

                //    ssCombo15.Enabled = false;
                //}

                //}
                //else
                //{
                //    ssCombo13.Enabled = true;
                //    ssCombo15.Enabled = true;
                //}
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_Droit");
            }
        }

        /// <summary>
        /// Tsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocImmeuble_Load(object sender, EventArgs e)
        {
            string sSQlbloque = "";

            try
            {
                //lblCodeCommercial.iTalkTB.DoubleClick += lblCodeCommercial_DoubleClick;
                fc_Droit();
                View.Theme.Theme.MainForm.FormClosing += (se, ev) =>
                {
                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        // stocke la position de la fiche immeuble
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                    }

                    using (var tmpModAdo = new ModAdo())
                        if (string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT Code1 FROM Immeuble WHERE CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'")))
                        {
                            if (string.IsNullOrEmpty(txtCode1.Text))
                            {
                                int x = General.Execute("DELETE FROM Immeuble WHERE CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
                            }
                            else
                            {
                                fc_sauver();
                            }
                        }

                    SAGE.fc_CloseConnSage();

                    if ((modAdorsCorrespondants != null))
                    {
                        modAdorsCorrespondants?.Dispose();
                    }

                    if ((modAdorsCorr != null))
                    {
                        modAdorsCorr?.Dispose();
                    }

                    if ((modAdorsCopro != null))
                    {
                        modAdorsCopro?.Dispose();
                    }

                    if ((modAdorsIntervenant != null))
                    {
                        modAdorsIntervenant?.Dispose();
                    }

                    if ((modAdorsintervention != null))
                    {
                        modAdorsintervention?.Dispose();
                    }

                    if ((modAdorsDevis != null))
                    {
                        modAdorsDevis?.Dispose();
                    }

                    if ((modAdorsICA_ImmCategorieInterv != null))
                    {
                        modAdorsICA_ImmCategorieInterv?.Dispose();
                    }

                    if ((modAdorsIMA_ImmArticle != null))
                    {
                        modAdorsIMA_ImmArticle?.Dispose();
                    }
                    // ferme la connection SAGE.
                    SAGE.fc_CloseConnSage();
                };
                //=== modif du 02 10 2019.
                ssGridCopro.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                Label4_206.Text = "N°TVA Intracommunautaire";
                txt_159.MaxLength = 13;

                sSQlbloque = "SELECT     AUT_Formulaire"
                    + " From AUT_Autorisation "
                    + " WHERE        (AUT_Formulaire = N'" + Name + "') "
                    + " AND (AUT_Objet = N'TiersPayeur')"
                    + " AND  AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";

                using (var tmpModAdo = new ModAdo())
                    sSQlbloque = tmpModAdo.fc_ADOlibelle(sSQlbloque);

                if (!sSQlbloque.IsNullOrEmpty())
                {
                    Label4_223.Visible = true;
                    txt_164.Visible = true;
                }
                else
                {
                    Label4_223.Visible = false;
                    txt_164.Visible = false;
                }
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridIMRE.ActiveRow != null)
                if (string.IsNullOrEmpty(GridIMRE.ActiveRow.Cells["CodeImmeuble"].Text))
                {
                    GridIMRE.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (GridIMRE.ActiveCell == null)
                return;
            if (e.ErrorType == ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + GridIMRE.ActiveCell.Column.Header.Caption);
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (GridMailSiteWeb.ActiveRow == null)
                return;
            if (GridMailSiteWeb.ActiveRow.IsAddRow)
                return;
            if (string.IsNullOrEmpty(GridMailSiteWeb.ActiveRow.Cells["Codeimmeuble"].Text))
            {
                GridMailSiteWeb.ActiveRow.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;
            }

            if (GridMailSiteWeb.ActiveCell != null)
                if (GridMailSiteWeb.ActiveCell.Column.Key.ToUpper() == "adresseMail".ToUpper())
                {
                    GridMailSiteWeb.ActiveRow.Cells["adresseMail"].Value = GridMailSiteWeb.ActiveRow.Cells["adresseMail"].Text.Trim();
                }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optCptCopro_CheckedChanged(object sender, EventArgs e)
        {
            if (optCptCopro.Checked)
            {
                ComboComptes.Enabled = true;
                optCptImmeuble.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optCptImmeuble_CheckedChanged(object sender, EventArgs e)
        {
            if (optCptImmeuble.Checked)
            {
                ComboComptes.Text = "";
                optCptCopro.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optEcrituresNonLetrees_CheckedChanged(object sender, EventArgs e)
        {
            if (optEcrituresNonLetrees.Checked)
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                fc_ChargeFinance();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptSyndic_CheckedChanged(object sender, EventArgs e)
        {
            if (OptSyndic.Checked)
            {
                txtSyndic1.Text = "";
                txtSyndic2.Text = "";
                ssCombo14.Text = "";
                txtCommercial.Text = "";
                txtSyndic1.Visible = true;
                txtSyndic2.Visible = true;
                ssCombo14.Visible = true;
                Label4198.Visible = true;
                txtCommercial.Visible = true;
                Label449.Visible = true;
                Label455.Visible = true;
                Label458.Visible = true;
                txtImmeuble1.Visible = false;
                txtImmeuble2.Visible = false;
                ssCombo19.Visible = false;
                Label4199.Visible = false;
                Label453.Visible = false;
                Label457.Visible = false;
                txtParticulier1.Visible = false;
                ssCombo20.Visible = false;
                Label4200.Visible = false;
                txtParticulier2.Visible = false;
                Label454.Visible = false;
                Label456.Visible = false;
                cmdSsyndic.Visible = true;
                cmdSPart.Visible = false;
                txtSyndic1.Focus();
                OptParticulier.Checked = false;
                OptImmeuble.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptParticulier_CheckedChanged(object sender, EventArgs e)
        {
            if (OptParticulier.Checked)
            {

                txtParticulier1.Text = "";
                txtParticulier2.Text = "";
                ssCombo20.Text = "";

                txtSyndic1.Visible = false;
                txtSyndic2.Visible = false;
                ssCombo14.Visible = false;
                Label4198.Visible = false;
                txtCommercial.Visible = false;
                Label449.Visible = false;
                Label455.Visible = false;
                Label458.Visible = false;
                txtImmeuble1.Visible = false;
                txtImmeuble2.Visible = false;
                ssCombo19.Visible = false;
                Label4199.Visible = false;
                Label453.Visible = false;
                Label457.Visible = false;
                txtParticulier1.Visible = true;
                ssCombo20.Visible = true;
                Label4200.Visible = true;
                txtParticulier2.Visible = true;
                Label454.Visible = true;
                Label456.Visible = true;
                cmdSPart.Visible = true;
                cmdSsyndic.Visible = false;

                txtParticulier1.Focus();

                OptImmeuble.Checked = false;
                OptSyndic.Checked = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optToutesEcritures_CheckedChanged(object sender, EventArgs e)
        {
            if (optToutesEcritures.Checked)
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                fc_ChargeFinance();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void SpecifAcces_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo13_AfterCloseUp(object sender, EventArgs e)
        {
            DataTable rsSecteur = new DataTable();
            ModAdo rsSecteurAdo = new ModAdo();
            var ctr = sender as UltraCombo;
            var Index = Convert.ToInt32(ctr.Tag);
            switch (Index)
            {

                case 13:
                    //=== exploitation.
                    bCloseUp = true;
                    fc_FindPersonnnel(ssCombo13.Text);
                    break;

                case 21:
                    //=== ramoneur.
                    fc_LibRamoneur();
                    break;

                case 22:
                    rsSecteur = rsSecteurAdo.fc_OpenRecordSet("SELECT Nom, Prenom , "
                             + " Qualification.Qualification"
                             + " FROM Personnel left JOIN Qualification ON Personnel.CodeQualif"
                             + " = Qualification.CodeQualif "
                             + " WHERE Personnel.Matricule = '" + StdSQLchaine.gFr_DoublerQuote(ssCombo22.Text) + "'");
                    if (rsSecteur.Rows.Count > 0)
                    {
                        txt105.Text = rsSecteur.Rows[0]["Nom"] + " " + rsSecteur.Rows[0]["Prenom"];
                        txt106.Text = rsSecteur.Rows[0]["Qualification"] + " ";
                    }
                    else
                    {
                        txt105.Text = "";
                        txt106.Text = "";
                    }
                    rsSecteur.Dispose();
                    rsSecteur = null;
                    break;
                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                case 23:
                    fc_libCAT_Code();
                    break;
                    //===> Fin Modif Mondir

            }
        }
        /// <summary>
        /// Tested
        /// Modif de la version V29.06.2020 ajoutée par Mondir le 30.06.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo0_BeforeDropDown(object sender, CancelEventArgs e)
        {
            DataTable rsGdpCombo = new DataTable();
            ModAdo rsGdpComboAdo = new ModAdo();
            string sSQL = null;
            int i = 0;
            var ctr = sender as UltraCombo;
            var Index = Convert.ToInt32(ctr.Tag);
            switch (Index)
            {

                case 0:
                    sSQL = " SELECT Designation, CleAuto " + " FROM p_UsageLocaux ORDER BY Designation, CleAuto";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo0);
                    ssCombo0.DataSource = rsGdpCombo;
                    break;

                case 1:

                    sSQL = "SELECT Designation, CleAuto  FROM p_UsageGaz ORDER BY Designation, CleAuto";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo1);
                    ssCombo1.DataSource = rsGdpCombo;
                    break;

                case 5:
                    //==type de compteur.

                    sSQL = "SELECT Code, Libelle FROM TypeCompteur ORDER BY Code";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo5);
                    ssCombo5.DataSource = rsGdpCombo;
                    break;

                case 6:
                    //=== pression aval.
                    sSQL = "SELECT CleAuto, Pression, Unite FROM p_PressionAval ORDER BY Pression";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo6);
                    ssCombo6.DataSource = rsGdpCombo;
                    break;

                case 7:
                    //=== Correcteur.
                    sSQL = "SELECT Adresse AS Correcteur " + " FROM T_Param WHERE Code = 'Compteur' AND SousCode = 'Correcteur' ORDER BY Adresse";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo7);
                    ssCombo7.DataSource = rsGdpCombo;
                    break;

                case 8:
                    sSQL = "SELECT CodeProfil, Designation " + " FROM P1_TableProfils ORDER BY CodeProfil, Designation";
                    rsGdpCombo = ModP1Gaz.fc_OpenRecordSetP1Gaz(sSQL);
                    sheridan.style("ECRAN BLEU", ssCombo8);
                    ssCombo8.DataSource = rsGdpCombo;
                    break;

                case 13:
                    //=== responsable d'exploitation.
                    //sSQL = ""
                    //sSQL = "SELECT Matricule, Nom, Prenom, LibelleQualif "
                    //sSQL = sSQL & " FROM Personnel INNER JOIN SpecifQualif ON "
                    //sSQL = sSQL & " Personnel.CodeQualif=SpecifQualif.CodeQualif "
                    //sSQL = sSQL & " WHERE SpecifQualif.QualifRExp=1 OR SpecifQualif.QualifRExp=16 OR SpecifQualif.QualifRExp=13 OR SpecifQualif.QualifRExp=14 OR SpecifQualif.QualifRExp=17"
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";

                    //.DataFieldList = strChampSel
                    modAdorsGdpCombo = new ModAdo();
                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    //ssCombo(13).DataFieldList = "Matricule"
                    //ssCombo(13).DataFieldToDisplay = "Matricule"
                    //style "ECRAN BLEU", ssCombo(13)
                    ssCombo13.DataSource = rsGdpCombo;
                    break;
                case 14:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";
                    modAdorsGdpCombo = new ModAdo();
                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo14.DataSource = rsGdpCombo;
                    break;

                case 15:
                    //==== commercial.
                    sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
                    if (General.CodeQualifCommercial.Length > 0)
                    {
                        sSQL = sSQL + " WHERE ";
                        for (i = 1; i < General.CodeQualifCommercial.Length; i++)
                        {
                            if (i < General.CodeQualifCommercial.Length - 1)
                            {
                                sSQL = sSQL + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                            }
                            else
                            {
                                sSQL = sSQL + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                            }
                        }
                    }

                    sSQL = sSQL + " ORDER BY Personnel.Nom";
                    modAdorsGdpCombo = new ModAdo();
                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo15.DataSource = rsGdpCombo;
                    break;

                case 19:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";
                    modAdorsGdpCombo = new ModAdo();
                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo19.DataSource = rsGdpCombo;
                    break;
                case 20:
                    //=== responsable d'exploitation.
                    sSQL = "SELECT     Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification" + " FROM         Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE     (Personnel.CodeQualif = '12') OR" + " (Personnel.CodeQualif = '13') OR" + " (Personnel.CodeQualif = '16') OR" + " (Personnel.CodeQualif = '17') OR" + " (Personnel.CodeQualif = '14')";
                    modAdorsGdpCombo = new ModAdo();
                    rsGdpCombo = modAdorsGdpCombo.fc_OpenRecordSet(sSQL);
                    ssCombo20.DataSource = rsGdpCombo;
                    break;


                case 21:
                    break;
                //==== code ramoneur.
                case 22:
                    //=== chef de secteur.
                    if (General.UCase(General.sClimMat) == General.UCase(ssCombo13.Text))
                    {
                        sSQL = "SELECT        Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification"
                         + " FROM            Personnel INNER JOIN "
                         + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif "
                         //====> Mondir le 30.06.2020 pour ajouter la condition ' AND (Personnel.CRespExploit = ', version V29.06.2020
                         + " WHERE        (Personnel.CodeQualif = N'13') AND (Personnel.CRespExploit = '" + General.sClimInitiale + "')";
                        //====> Fin modif Mondir
                    }
                    else
                    {
                        sSQL = "SELECT        Personnel.Matricule,Personnel.Nom, Personnel.Prenom,  Qualification.Qualification"
                          + " FROM            Personnel INNER JOIN "
                          + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif "
                          + " WHERE      Personnel.Initiales IN(SELECT        CSecteur "
                          + " From Personnel "
                          + " WHERE    Matricule = '" + ssCombo13.Text + "' )"
                          + " OR "
                          + " Personnel.Initiales IN(SELECT        CSecteur2 "
                          + " From Personnel "
                         + " WHERE    Matricule = '" + ssCombo13.Text + "' )";
                    }
                    rsGdpCombo = rsGdpComboAdo.fc_OpenRecordSet(sSQL);
                    ssCombo22.DataSource = rsGdpCombo;
                    break;
                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                case 23:
                    sSQL = " SELECT        CAT_Code, CAT_Libelle"
                        + " From CAT_CategorieImmeuble "
                        + " ORDER BY CAT_Code";
                    rsGdpCombo = rsGdpComboAdo.fc_OpenRecordSet(sSQL);
                    ssCombo_23.DataSource = rsGdpCombo;
                    break;
                    //===> Fin Modif Mondir
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_AfterRowUpdate(object sender, RowEventArgs e)
        {
            blnKeyPress = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text) && !string.IsNullOrEmpty(ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString()))
            {
                // stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString());
                General.saveInReg(Variable.cUserDocImmeuble, "Historique", "2");
            }
            View.Theme.Theme.Navigate(typeof(UserDocDevis));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }
        /// <summary>
        /// Tested - Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFindImmeuble_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string sSQL = null;
                string[] tTAb = null;
                string sWhere = null;
                string stemp = null;
                string sT = null;

                DataTable rs = default(DataTable);
                var modAdors = new ModAdo();
                int i = 0;
                int j = 0;
                string sWhereBIS = null;
                if (bLoadChange == false)
                {

                    sT = ssFindImmeuble.Text.Trim();
                    stemp = "";
                    if (sT.Length > 0)
                    {


                        for (i = 1; i < sT.Length; i++)
                        {
                            if (General.Mid(sT, i, 1) == " ")
                            {
                                if (j == 0)
                                {
                                    //== enléve les espaces doublées.
                                    stemp = stemp + General.Mid(sT, i, 1);
                                    j = 1;
                                }
                            }
                            else
                            {
                                j = 0;
                                stemp = stemp + General.Mid(sT, i, 1);
                            }
                        }

                        tTAb = stemp.Split(' ');

                        sWhere = "";

                        for (i = 0; i < tTAb.Length; i++)
                        {
                            if (i == 0)
                            {
                                sWhere = " WHERE Immeuble.Adresse like '%" + tTAb[i] + "%'";
                            }
                            else
                            {
                                if (OptOu.Checked == true)
                                {
                                    // sWhere = sWhere + " OR Immeuble.Adresse like '%" + tTAb[i] + "%'";
                                    sWhere = sWhere + " and  Immeuble.Adresse like '%" + tTAb[i] + "%'";
                                }
                                else if (OptEt.Checked == true)
                                {
                                    sWhere = sWhere + " AND Immeuble.Adresse like '%" + tTAb[i] + "%'";
                                }
                            }
                        }
                        sWhere = sWhere + ")";

                        sWhereBIS = "";
                        for (i = 0; i < tTAb.Length; i++)
                        {
                            if (i == 0)
                            {
                                sWhereBIS = " WHERE Immeuble.codeimmeuble like '%" + tTAb[i] + "%'";
                            }
                            else
                            {
                                if (OptOu.Checked == true)
                                {
                                    // sWhere = sWhere + " OR Immeuble.Adresse like '%" + tTAb[i] + "%'";
                                    sWhereBIS = sWhereBIS + " and  Immeuble.codeimmeuble like '%" + tTAb[i] + "%'";
                                }
                                else if (OptEt.Checked == true)
                                {
                                    sWhereBIS = sWhereBIS + " AND Immeuble.codeimmeuble like '%" + tTAb[i] + "%'";
                                }
                            }
                        }
                        //If Right(sWhere, 1) <> "'" Then
                        //    sWhere = sWhere & "'"
                        //End If
                        //sWhere = sWhere & ")"
                    }
                    else
                    {
                        ssFindImmeuble.DataSource = null;
                        return;
                    }

                    sSQL = "SELECT TOP(10) Immeuble.Codeimmeuble, Immeuble.adresse, Immeuble.ville, Table1.Nom "
                        + " FROM Immeuble INNER JOIN"
                        + " Table1 ON Immeuble.Code1 = Table1.Code1" + " " + sWhere + " or (" + sWhereBIS + ") ";


                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    ssFindImmeuble.DataSource = null;
                    ssFindImmeuble.DataSource = rs;


                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";ssFindImmeuble_Change");
            }
        }
        /// <summary>
        /// Tested - Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFindImmeuble_AfterCloseUp(object sender, EventArgs e)
        {
            if (ssFindImmeuble.ActiveRow != null)
                txtCodeimmeubleTemp.Text = ssFindImmeuble.ActiveRow.Cells["CodeImmeuble"].Text;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssGridBE.ActiveRow == null)
                return;
            if (string.IsNullOrEmpty(ssGridBE.ActiveRow.Cells["CodeImmeuble"].Text))
            {
                ssGridBE.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {

                try
                {
                    string requete = "SELECT BE.CodeBE AS \"Code\", BE.Libelle AS \"Libelle\", " +
                                     "BE.Contact AS \"Contact\", BE.Tel AS \"Tel\", BE.Fax AS \"Fax\", " +
                                     " BE.eMail AS \"E-Mail\"" + " FROM BE  ";
                    string where_order = "";
                    SearchTemplate fg =
                        new SearchTemplate(this, null, requete, where_order, "")
                        {
                            Text = "Recherche des bureaux d'étude"
                        };
                    fg.SetValues(new Dictionary<string, string> { { "CodeBE", ssGridBE.ActiveRow.Cells["CodeBE"].Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        ssGridBE.ActiveRow.Cells["CodeBE"].Value = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            ssGridBE.ActiveRow.Cells["CodeBE"].Value = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                catch (Exception ex)
                {
                    Erreurs.gFr_debug(ex, this.Name + ";ssGridBE_KeyPress");
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row != null && e.Row.Cells["CodeBE"].Text != "")
                fc_InfoBe(e.Row.Cells["CodeBE"].Text, e.Row);
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (!e.ReInitialize)
                e.Row.Cells["Qualification"].Value = sQualiteCopro;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssGridCopro.ActiveRow == null)
                return;

            if (string.IsNullOrEmpty(ssGridCopro.ActiveRow.Cells["CodeImmeuble"].Text))
            {
                ssGridCopro.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
            }

            if (!string.IsNullOrEmpty(ssGridCopro.ActiveRow.Cells["CodeQualif_QUA"].Text))
            {
                using (var tmpModAdo = new ModAdo())
                    ssGridCopro.ActiveRow.Cells["Qualification"].Value = tmpModAdo.fc_ADOlibelle("SELECT LibelleQualif FROM SpecifQualif WHERE CodeQualif='" +
                        StdSQLchaine.gFr_DoublerQuote(ssGridCopro.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString()) + "'");
            }

            if (ssGridCopro.ActiveCell != null && ssGridCopro.ActiveCell.Column.Index == 1)
            {
                if (ssGridCopro.ActiveCell.OriginalValue != DBNull.Value && Convert.ToBoolean(ssGridCopro.ActiveCell.OriginalValue))
                {
                    ssGridCopro.ActiveRow.Cells["CompteCree"].Value = true;
                }
            }

            if (string.IsNullOrEmpty(ssGridCopro.ActiveRow.Cells["Adresse"].Value.ToString())
                && string.IsNullOrEmpty(ssGridCopro.ActiveRow.Cells["CodePostal"].Value.ToString())
                && string.IsNullOrEmpty(ssGridCopro.ActiveRow.Cells["Ville"].Value.ToString()))
            {
                if (txtAdresse.Text.Length > 50)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La longueur de cet adresse dépasse la valeur maximale qui peut être acceptée par cette cellule.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    e.Cancel = true;
                }
                else
                {
                    ssGridCopro.ActiveRow.Cells["Adresse"].Value = txtAdresse.Text;
                }

                ssGridCopro.ActiveRow.Cells["CodePostal"].Value = txtCodePostal.Text;
                ssGridCopro.ActiveRow.Cells["Ville"].Value = txtVille.Text;
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_AfterRowUpdate(object sender, RowEventArgs e)
        {
            blnKeyPress = false;
            int xx = modAdorsCopro.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (ssGridCoproIsLoading)
                return;

            if (e.Row == null)
                return;
            if (string.IsNullOrEmpty(e.Row.Cells["CodeParticulier"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code du particulier est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
                return;
            }


            //General.sSQL = "";
            //General.sSQL =
            //    "SELECT ImmeublePart.CodeImmeuble,ImmeublePart.CompteCree,ImmeublePart.NCompte, ImmeublePart.CodeParticulier," +
            //    " ImmeublePart.Nom, ImmeublePart.CodeQualif_QUA,'' as Qualification  " +
            //    " ,Adresse, CodePostal,Ville ," + " ImmeublePart.Tel, ImmeublePart.TelPortable_IMP," +
            //    " ImmeublePart.Fax ," + " ImmeublePart.eMail," + " ImmeublePart.Note_IMP" + " FROM ImmeublePart  " +
            //    " where ImmeublePart.CodeImmeuble= '" + sCode + "'" + " order by ImmeublePart.CodeParticulier";


            if (General.UtiliseSage == "1")
            {
                if (string.IsNullOrEmpty(e.Row.Cells["NCompte"].Text) && (e.Row.Cells["CompteCree"].Text != "" && Convert.ToBoolean(e.Row.Cells["CompteCree"].Text) == false))
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous créer un compte à ce copropriétaire ?", "Création de compte", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        e.Row.Cells["CompteCree"].Value = true;
                    }
                    else
                    {
                        e.Row.Cells["CompteCree"].Value = false;
                    }
                }
            }
            else
            {
                e.Row.Cells["CompteCree"].Value = false;
            }
            if (e.Row.Cells["CodeParticulier"].DataChanged)
            {
                if (e.Row.Cells["CodeParticulier"].OriginalValue.ToString() != e.Row.Cells["CodeParticulier"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ImmeublePart WHERE CodeParticulier = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeParticulier"].Text)}' AND CodeImmeuble = '{e.Row.Cells["CodeImmeuble"].Text}'"));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxists déjà.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                        }
                    }
                }
            }
            e.Row.Cells["Nom"].Value = e.Row.Cells["Nom"].Value.ToString().Trim();


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_AfterRowUpdate(object sender, RowEventArgs e)
        {
            blnKeyPress = false;

            if (ssGridImmCorrespondant.ActiveRow == null) return;
            if (string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Text))
            {

                string insert = "insert into IMC_ImmCorrespondant " +
                     "(CodeImmeuble_IMM,nom_imc,CodeQualif_QUA, tel_imc,  telportable_imc, Fax_IMC, email_imc,  Note_imc,PresidentCS_IMC )" +
                     " values('" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["nom_imc"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif_QUA"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["telportable_imc"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["telportable_imc"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["Fax_IMC"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["email_imc"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["Note_imc"].Text) + "','" +
                       StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Text) + "')";
                int aa = General.Execute(insert);

                using (var tmp = new ModAdo())
                {
                    int cle = Convert.ToInt32(tmp.fc_ADOlibelle("select max(CodeCorresp_IMC) from IMC_ImmCorrespondant"));
                    ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Value = cle.ToString();
                }
            }
            if (General.nz(ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value, "0").ToString() != "0")
            {
                int xx = General.Execute("UPDATE IMC_ImmCorrespondant SET PresidentCS_IMC='0' WHERE CodeImmeuble_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
            }

            string update = "UPDATE IMC_ImmCorrespondant SET PresidentCS_IMC='" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Text) + "'";
            update = update + ",  nom_imc = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["nom_imc"].Text) + "'";
            update = update + ", CodeQualif_QUA = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif_QUA"].Text) + "'";
            update = update + " , tel_imc = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["tel_imc"].Text) + "'";
            update = update + ",  telportable_imc = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["telportable_imc"].Text) + "'";
            update = update + ", Fax_IMC= '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["Fax_IMC"].Text) + "'";
            update = update + ", email_imc = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["email_imc"].Text) + "'";
            update = update + ",  Note_imc = '" + StdSQLchaine.gFr_DoublerQuote(ssGridImmCorrespondant.ActiveRow.Cells["Note_imc"].Text) + "'";
            update = update + " WHERE CodeCorresp_IMC =" + ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Value + " and  CodeImmeuble_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            int yy = General.Execute(update);
            //int xx = modAdorsCorr.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            if (ssGridImmCorrespondant.ActiveRow == null)
                return;
            //ecris le code code immeuble
            if (string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeImmeuble_IMM"].Value?.ToString()))
            {
                ssGridImmCorrespondant.ActiveRow.Cells["CodeImmeuble_IMM"].Value = txtCodeImmeuble.Text;
            }
            if (!string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif_QUA"].Value?.ToString()))
            {
                fc_RechercheQualite(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString(), ssGridImmCorrespondant.ActiveRow);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="Row"></param>
        private void fc_RechercheQualite(string sCode, UltraGridRow Row)
        {
            // récupére dans le controle adodc1 les informations liées au matricule.
            if (ssDropQualite.DataSource != null)
            {
                if (ssDropQualite.Rows.Count > 0)
                {
                    for (int i = 0; i < ssDropQualite.Rows.Count; i++)
                    {
                        if (ssDropQualite.Rows[i].Cells["CodeQualif"].Value.ToString().ToUpper() == sCode.ToUpper())
                        {

                            Row.Cells["Qualification"].Value = ssDropQualite.Rows[i].Cells["Qualification"].Value;
                            break;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (ssGridImmCorrespondant.ActiveRow == null)
                return;
            if (string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif_QUA"].Text.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code qualification est obligatoire", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_Leave(object sender, EventArgs e)
        {
            //====> Mondir le 08.02.2021, commented this line to solve https://groupe-dt.mantishub.io/view.php?id=2183
            //if (ssGridImmCorrespondant.ActiveRow != null)
            //    ssGridImmCorrespondant.ActiveRow.CancelUpdate();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;
            if (e.Row.Cells["PresidentCS_IMC"].Value.ToString() == "-1")
            {
                e.Row.Cells["Président du conseil syndical"].Value = true;
            }
            if (e.Row.Cells["PresidentCS_IMC"].Value.ToString() == "0" || e.Row.Cells["PresidentCS_IMC"].Value == DBNull.Value)
            {
                e.Row.Cells["Président du conseil syndical"].Value = false;
            }
            if (!string.IsNullOrEmpty(e.Row.Cells["CodeQualif_QUA"].Value?.ToString()))
            {
                fc_RechercheQualite(e.Row.Cells["CodeQualif_QUA"].Value.ToString(), e.Row);
            }
        }
        /// <summary>
        /// TODO : Mondir - Must Test This Fonction - Probleme Of Computer Arch 64bit or 32bit With Crystal Version
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSGridSituation_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (SSGridSituation.ActiveRow == null)
                return;
            ReportDocument crEcritures = new ReportDocument();
            string SelectionFormula = "";
            CrystalReportFormView Form = null;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            if (General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "TA" ||
                General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "CM" ||
                General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "FA" ||
                General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "TM")
            {
                crEcritures.Load(General.ETATOLDFACTUREMANUEL);
                SelectionFormula = "{FactureManuelleEntete.NoFacture}='" + SSGridSituation.ActiveRow.Cells["Référence facture"].Value + "'";
                Form = new CrystalReportFormView(crEcritures, SelectionFormula);
                Form.Show();
            }
            else if (General.Left(SSGridSituation.ActiveRow.Cells["Référence facture"].Value.ToString(), 2) == "CA")
            {
                crEcritures.Load(General.CHEMINEUROONLY);
                SelectionFormula = "{FactEnTete.Nofacture}='" + SSGridSituation.ActiveRow.Cells["Référence facture"].Value + "'";
                Form = new CrystalReportFormView(crEcritures, SelectionFormula);
                Form.Show();
            }
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            // Form?.Dispose();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterRowUpdate(object sender, RowEventArgs e)
        {
            blnKeyPress = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            blnKeyPress = false;
            if (ssIntervention.ActiveRow == null)
                return;
            if (ssIntervention.ActiveCell != null && ssIntervention.ActiveCell.Column.Key.ToUpper() == "CodeEtat".ToUpper())
            {
                if (ssIntervention.ActiveCell.OriginalValue.ToString().ToUpper() != ssIntervention.ActiveCell.Text.ToUpper())
                {
                    General.fc_HistoStatut(Convert.ToInt32(ssIntervention.ActiveRow.Cells["NoIntervention"].Text), ssIntervention.ActiveRow.Cells["codeimmeuble"].Text,
                        "Fiche immeuble", ssIntervention.ActiveCell.OriginalValue.ToString(), ssIntervention.ActiveCell.Text);
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_ClickCellButton(object sender, CellEventArgs e)
        {
            bool boolFolder = false;
            bool boolMp3 = false;
            string sMp3 = null;
            if (ssIntervention.ActiveRow == null)
                return;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                if (ssIntervention.ActiveRow.Cells["Wave"].Value != null && !string.IsNullOrEmpty(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString()))
                {
                    //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                    sMp3 = General.Mid(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString(), 1, ssIntervention.ActiveRow.Cells["Wave"].Value.ToString().Length - 3) + "mp3";
                    // sMp3 = txtWave
                    boolMp3 = File.Exists(sMp3);
                    General.fso = null;
                    if (boolMp3 == true)
                    {
                        ModuleAPI.Ouvrir(sMp3);
                        return;
                    }
                    else
                    {

                        boolFolder = File.Exists(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString());
                        if (boolFolder == true)
                        {
                            ModuleAPI.Ouvrir(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString());
                            return;
                        }
                    }
                    if (boolFolder == false && boolMp3 == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //Ouvrir Me.hWnd, sMp3
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdWave_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (ssIntervention.ActiveRow == null)
                return;
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                // stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text, Convert.ToString(1), txtinterDe.Text, txtIntereAu.Text,
                   cmbStatus.Text, cmbIntervenant.Text, cmbArticle.Text, txt158.Text);


                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString());
                General.saveInReg(Variable.cUserDocImmeuble, "Historique", "1");
            }

            View.Theme.Theme.Navigate(typeof(UserIntervention));

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }

        /// <summary>
        /// Tested
        /// ===> Mondir le 09.03.2021, please checl this function LoadDep1()
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBCmbDep1_ValueChanged(object sender, EventArgs e)
        {

            if (SSOleDBCmbDep1.ActiveRow == null)
                return;

            DataTable rsDep2 = default(DataTable);
            var modAdorsDep2 = new ModAdo();

            var rsDep = new DataTable();
            var modAdorsDep = new ModAdo();

            rsDep = modAdorsDep.fc_OpenRecordSet("SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif,Personnel.Dep2, Personnel.CSecteur , " +
                                                 "Qualification.CodeQualif, Qualification.Qualification, Personnel.Rondier" +
                                                   $" FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Matricule='{SSOleDBCmbDep1.Text}'");

            if (rsDep?.Rows.Count == 0)
                return;

            lblDep1Nom.Text = rsDep.Rows[0]["Nom"] + " " + rsDep.Rows[0]["Prenom"];
            //lblDep1Prenom.Caption = SSOleDBCmbDep1.Columns("Prenom").value
            lblDep1Qualif.Text = rsDep.Rows[0]["Qualification"]?.ToString();

            rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification" +
                " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" + " = Qualification.CodeQualif WHERE Initiales='" +
            StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["Dep2"]?.ToString()) + "'");

            if (rsDep2.Rows.Count > 0)
            {
                lblMatriculeDep2.Text = rsDep2.Rows[0]["Matricule"] + "";
                lblDep2Nom.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"] + "";
                //lblDep2Prenom.Caption = rsDep2.Rows[0]["Prenom") & ""
                lblDep2Qualif.Text = rsDep2.Rows[0]["Qualification"] + "";
            }
            else
            {
                lblMatriculeDep2.Text = "";
                lblDep2Nom.Text = "";
                // lblDep2Prenom.Caption = ""
                lblDep2Qualif.Text = "";
            }
            modAdorsDep2?.Dispose();

            //== Modif Rachid du 27 decembre 2004.
            modAdorsDep2 = new ModAdo();
            rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification " + " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE Initiales='" +
                StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["Rondier"]?.ToString()) + "'");

            if (rsDep2.Rows.Count > 0)
            {
                lblMatriculeRondier.Text = rsDep2.Rows[0]["Matricule"] + "";
                lblRondierNom.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];
                // lblRondierPrenom.Caption = rsDep2.Fields("Prenom") & ""


                lblRondierQualif.Text = rsDep2.Rows[0]["Qualification"] + "";
            }
            else
            {
                lblMatriculeRondier.Text = "";
                lblRondierNom.Text = "";

                // lblRondierPrenom.Caption = ""
                lblRondierQualif.Text = "";
            }
            modAdorsDep2?.Dispose();
            //======================================

            if (SSOleDBCmbDep1.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().Any(ele => ele.Key.ToUpper() == "CSecteur".ToUpper()))
            {
                modAdorsDep2 = new ModAdo();
                rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                    "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification" +
                    " FROM Personnel INNER JOIN Qualification ON" +
                    " Personnel.CodeQualif = " + " Qualification.CodeQualif WHERE Initiales='" +
                   StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["CSecteur"]?.ToString()) + "'");

                if (rsDep2.Rows.Count > 0)
                {
                    txtCodeChefSecteur.Text = rsDep2.Rows[0]["Matricule"] + "";
                    lbllibCodeChefSecteur.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];

                    //lblPrenomChefSecteur.Caption = rsDep2.Fields("Prenom") & ""

                    lblQualifChefSecteur.Text = rsDep2.Rows[0]["Qualification"] + "";
                }
                else
                {
                    txtCodeChefSecteur.Text = "";
                    lbllibCodeChefSecteur.Text = "";

                    // lblPrenomChefSecteur.Caption = ""


                    lblQualifChefSecteur.Text = "";
                }
                modAdorsDep2.Dispose();
            }

            ///   rsDep2.Open "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," _
            //& " Qualification.Qualification FROM Personnel INNER JOIN Qualification " _
            //& " ON Personnel.CodeQualif = Qualification.CodeQualif " _
            //& " WHERE Initiales='" & (fc_ADOlibelle("SELECT CRespExploit FROM Personnel WHERE Personnel.Matricule='" & txtCodeChefSecteur.Caption & "'")) & "'", adocnn

            //If UCase(adocnn.DefaultDatabase) <> UCase(cDelostal) Then
            if (General.sVersionImmParComm != "1")
            {
                modAdorsDep2 = new ModAdo();
                using (var tmpModAdo = new ModAdo())
                    rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification FROM Personnel INNER JOIN Qualification " +
                        " ON Personnel.CodeQualif = Qualification.CodeQualif " + " WHERE Initiales='" +
                        (tmpModAdo.fc_ADOlibelle("SELECT CRespExploit FROM Personnel" + " WHERE Personnel.Matricule='" + SSOleDBCmbDep1.Value + "'")) + "'");

                if (rsDep2.Rows.Count > 0)
                {
                    lblMatriculeREspExpl.Text = rsDep2.Rows[0]["Matricule"] + "";
                    lblLibRespExpl.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];

                    // lblPrenomRespExpl.Caption = rsDep2.Fields("Prenom") & ""
                    lblQualifRespExpl.Text = rsDep2.Rows[0]["Qualification"] + "";
                }
                else
                {
                    lblMatriculeREspExpl.Text = "";
                    lblLibRespExpl.Text = "";

                    //  lblPrenomRespExpl.Caption = ""

                    lblQualifRespExpl.Text = "";
                }
                modAdorsDep2.Dispose();
            }

            modAdorsDep2?.Dispose();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            boolErreurValidate = false;
            if (string.IsNullOrWhiteSpace(e.Row.Cells["Codeimmeuble"].Text))
                e.Row.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;

            if (SSOleDBGrid1.ActiveRow != null && string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["intervenant"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez saisir un intervenant.", "Donnée manquante", MessageBoxButtons.OK, MessageBoxIcon.Information);

                e.Cancel = true;
                boolErreurValidate = true;
                return;
            }

            if (SSOleDBGrid1.ActiveRow != null && string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["CodeArticle"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez saisir un code article.", "Donnée manquante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                SSOleDBGrid1.ActiveRow.Cells["CodeArticle"].Activate();
                e.Cancel = true;
                boolErreurValidate = true;
                return;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns[1].ValueList = SSOleDBDropDown1;
            //personnel.
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["codearticle"].ValueList = DropDownArticle;

            e.Layout.Bands[0].Columns["Codeimmeuble"].Hidden = true;
            e.Layout.Bands[0].Columns["Duree"].Header.Caption = "Durée";
            e.Layout.Bands[0].Columns["Valorisation"].Header.Caption = "Tarif";
            e.Layout.Bands[0].Columns["codearticle"].Header.Caption = "Code Article";
            e.Layout.Bands[0].Columns["Janvier"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Janvier"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Fevrier"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Fevrier"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Mars"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Mars"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Avril"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Avril"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Mai"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Mai"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Juin"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Juin"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Juillet"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Juillet"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Aout"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Aout"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Septembre"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Septembre"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Octobre"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Octobre"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Novembre"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Novembre"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Decembre"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Decembre"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["Designation"].Header.Caption = "Libellé";
            e.Layout.Bands[0].Columns["Designation"].CellActivation = Activation.NoEdit;

            e.Layout.Bands[0].Columns["No"].Hidden = true;
            e.Layout.Bands[0].Columns["NomPersonne"].Hidden = true;
            e.Layout.Bands[0].Columns["Resiliee"].Hidden = true;

            e.Layout.Bands[0].Columns["Intervenant"].Header.VisiblePosition = 0;
            e.Layout.Bands[0].Columns["Duree"].Header.VisiblePosition = 1;
            e.Layout.Bands[0].Columns["Valorisation"].Header.VisiblePosition = 2;
            e.Layout.Bands[0].Columns["Janvier"].Header.VisiblePosition = 3;
            e.Layout.Bands[0].Columns["Fevrier"].Header.VisiblePosition = 4;
            e.Layout.Bands[0].Columns["Mars"].Header.VisiblePosition = 5;
            e.Layout.Bands[0].Columns["Avril"].Header.VisiblePosition = 6;
            e.Layout.Bands[0].Columns["Mai"].Header.VisiblePosition = 7;
            e.Layout.Bands[0].Columns["Juin"].Header.VisiblePosition = 8;
            e.Layout.Bands[0].Columns["Juillet"].Header.VisiblePosition = 9;
            e.Layout.Bands[0].Columns["Aout"].Header.VisiblePosition = 10;
            e.Layout.Bands[0].Columns["Septembre"].Header.VisiblePosition = 11;
            e.Layout.Bands[0].Columns["Octobre"].Header.VisiblePosition = 12;
            e.Layout.Bands[0].Columns["Novembre"].Header.VisiblePosition = 13;
            e.Layout.Bands[0].Columns["Decembre"].Header.VisiblePosition = 14;
            e.Layout.Bands[0].Columns["codearticle"].Header.VisiblePosition = 15;
            e.Layout.Bands[0].Columns["Designation"].Header.VisiblePosition = 16;
            e.Layout.Bands[0].Columns["Commentaire"].Header.VisiblePosition = 17;
            e.Layout.Bands[0].Columns["Commentaire"].MaxLength = 100;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            Type lDatatype = null;

            if (SSOleDBGrid1.ActiveCell == null)
                return;

            lDatatype = SSOleDBGrid1.ActiveCell.Column.DataType;

            if ((lDatatype == typeof(short) || lDatatype == typeof(int) || lDatatype == typeof(double) || lDatatype == typeof(DateTime)) && e.ErrorType == ErrorType.Data)
            {
                SSOleDBGrid1.ActiveCell.Value = null;
            }

            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de saisie dans la colonne " + SSOleDBGrid1.ActiveCell.Column.Header.Caption);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            FC_ChargeOnglet();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab2_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            if (SSTab2.SelectedTab.Index == 0)
            {
                optCptImmeuble.Checked = true;
                optCptImmeuble_CheckedChanged(sender, e);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt159_TextChanged(object sender, EventArgs e)
        {
            var txt = sender as iTalk_TextBox_Small2;
            int Index = Convert.ToInt32(txt.Tag);
            switch (Index)
            {

                case 159:
                    //=== immatriculation
                    blModif = true;
                    break;
                case 161:
                    blModif = true;
                    break;
                //===> Mondir le 29.11.2020 pour ajouter les modifs de la version V29.11.2020
                case 165:
                case 166:
                case 167:
                case 168:
                case 169:
                case 170:
                case 171:
                case 172:
                    blModif = true;
                    break;
                    //===> Fin Modif Mondir

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresse_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresse2_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresseFact1_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresseFact2_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresseFact3_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAdresseFact4_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtAngleRue_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCode1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCode1_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;
            if (KeyAscii == 13)
            {
                General.blControle = true;
                fc_RechercheClient();
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void FC_recupAdresseFact(string sCode)
        {
            General.sSQL = "";
            General.sSQL = "SELECT Nom, Adresse1, Adresse2, Adresse3, CodePostal, Ville" + " From Table1" + " WHERE Code1 ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            using (var tmpModAdo = new ModAdo())
            {
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                txtAdresseFact1_IMM.Text = General.nz(General.rstmp.Rows[0]["Nom"], "").ToString();
                txtAdresseFact2_IMM.Text = General.nz(General.rstmp.Rows[0]["Adresse1"], "").ToString();
                txtAdresseFact3_IMM.Text = General.nz(General.rstmp.Rows[0]["Adresse2"], "").ToString();
                txtCodePostal_IMM.Text = General.nz(General.rstmp.Rows[0]["CodePostal"], "").ToString();
                txtVilleFact_IMM.Text = General.nz(General.rstmp.Rows[0]["Ville"], "").ToString();
                txtCompteDivers.Text = sCode;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_RechercheClient()
        {
            string sCode = null;
            try
            {

                string requete = "SELECT " + "Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1 ";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCode1.Text = sCode;
                    fc_ChargeGrilleGestionnaire(txtCode1.Text);
                    FC_recupAdresseFact(txtCode1.Text);

                    if (General.sVersionImmParComm == "1")
                    {
                        using (var tmpModAdo = new ModAdo())
                        {
                            ssCombo15.Text =
                                tmpModAdo.fc_ADOlibelle("SELECT Commercial FROM Table1 " + " WHERE Table1.Code1='" +
                                                     StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");

                            txt113.Text =
                                tmpModAdo.fc_ADOlibelle(
                                    "SELECT Nom, Prenom FROM Personnel WHERE Matricule ='" + ssCombo15.Text + "'",
                                    true);
                            //Me.lblCommercPrenom.Caption = fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" & Me.lblCodeCommercial.Caption & "'")
                        }

                    }

                    using (var tmpModAdo2 = new ModAdo())
                        this.lblCodeCommercial.Text = tmpModAdo2.fc_ADOlibelle("SELECT Commercial FROM Table1 WHERE Table1.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");

                    if (!(string.IsNullOrEmpty(txtCode1.Text)))
                    {
                        using (var tmpModAdo2 = new ModAdo())
                        {
                            this.lblCommercNom.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" +
                                                     this.lblCodeCommercial.Text + "'");
                            this.lblCommercPrenom.Text =
                                tmpModAdo2.fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" +
                                                     this.lblCodeCommercial.Text + "'");
                        }
                    }

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCode1.Text = sCode;
                        fc_ChargeGrilleGestionnaire(txtCode1.Text);
                        FC_recupAdresseFact(txtCode1.Text);

                        if (General.sVersionImmParComm == "1")
                        {
                            using (var tmpModAdo = new ModAdo())
                            {
                                ssCombo15.Text =
                                    tmpModAdo.fc_ADOlibelle("SELECT Commercial FROM Table1 " + " WHERE Table1.Code1='" +
                                                            StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");

                                txt113.Text =
                                    tmpModAdo.fc_ADOlibelle(
                                        "SELECT Nom, Prenom FROM Personnel WHERE Matricule ='" + ssCombo15.Text + "'",
                                        true);
                                //Me.lblCommercPrenom.Caption = fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" & Me.lblCodeCommercial.Caption & "'")
                            }

                        }

                        using (var tmpModAdo2 = new ModAdo())
                            this.lblCodeCommercial.Text = tmpModAdo2.fc_ADOlibelle("SELECT Commercial FROM Table1 WHERE Table1.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");

                        if (!(string.IsNullOrEmpty(txtCode1.Text)))
                        {
                            using (var tmpModAdo2 = new ModAdo())
                            {
                                this.lblCommercNom.Text =
                                    tmpModAdo2.fc_ADOlibelle("SELECT Nom FROM Personnel WHERE Matricule ='" +
                                                             this.lblCodeCommercial.Text + "'");
                                this.lblCommercPrenom.Text =
                                    tmpModAdo2.fc_ADOlibelle("SELECT Prenom FROM Personnel WHERE Matricule ='" +
                                                             this.lblCodeCommercial.Text + "'");
                            }
                        }

                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


                txtCode1.Focus();
                fc_Groupe();

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCode1_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            bool Cancel = eventArgs.Cancel;
            if (General.blControle == false)
            {
                Cancel = fc_AfficheClient(txtCode1.Text, true);
            }
            eventArgs.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blAfficheMessage"></param>
        /// <returns></returns>
        private bool fc_AfficheClient(string sCode, bool blAfficheMessage = false)
        {
            bool functionReturnValue = false;
            // recherche du libelle du code de reglement. renvoie true si cekui ci existe.
            //si blAfficheMessage est à true affichage du message d'erreur.
            if (!string.IsNullOrEmpty(sCode))
            {
                General.sSQL = "";
                General.sSQL = "SELECT " + "Code1 as [Ref gerant], Nom, Ville FROM Table1 " + " where Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                using (var tmpModAdo = new ModAdo())
                {
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        fc_ChargeGrilleGestionnaire(txtCode1.Text);
                        functionReturnValue = false;
                    }
                    else
                    {

                        if (blAfficheMessage == true)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce client n'éxiste pas", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SSTab1.SelectedTab = SSTab1.Tabs[0];
                            functionReturnValue = true;
                            txtCode1.Focus();
                        }
                    }
                }
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtArretDate_Validating(object sender, CancelEventArgs e)
        {
            if (!(General.IsDate(txtArretDate.Text)) && !string.IsNullOrEmpty(txtArretDate.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "Données éronnéés", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtArretDate.Focus();
                return;
                //ElseIf txtArretDate = "" Then
                //   MsgBox "Vous devez saisir une date", vbCritical, "Données éronnéés"
                //  txtArretDate.SetFocus
                // Exit Sub
            }
            else if (!string.IsNullOrEmpty(txtArretDate.Text))
            {
                txtArretDate.Text = Convert.ToDateTime(txtArretDate.Text).ToString("dd/MM/yyyy");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeAcces1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeAcces2_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeGestionnaire_ValueChanged(object sender, EventArgs e)
        {
            if (txtCodeGestionnaire.IsItemInList())
            {
                //            sSQl = ""
                //            sSQl = "SELECT NomGestion FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
                //            Label25(5).Caption = fc_ADOlibelle(sSQl)
                //
                //            sSQl = ""
                //            sSQl = "SELECT TelGestion FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
                //            Label25(6).Caption = fc_ADOlibelle(sSQl)
                //
                //            sSQl = ""
                //            sSQl = "SELECT TelPortable_GES FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
                //            Label25(7).Caption = fc_ADOlibelle(sSQl)
                //
                //            sSQl = ""
                //            sSQl = "SELECT FaxGestion FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
                //            Label25(1).Caption = fc_ADOlibelle(sSQl)
                //
                //            sSQl = ""
                //            sSQl = "SELECT EMail_GES FROM Gestionnaire WHERE CodeGestinnaire='" & txtCodeGestionnaire.Text & "'"
                //            Label25(8).Caption = fc_ADOlibelle(sSQl)

                //== Modif rachid du 19 fevrier 2005
                fc_InfoGestion((txtCodeGestionnaire.Text), txtCode1.Text);
            }
            else
            {
                Label255.Text = "";
                Label256.Text = "";
                Label257.Text = "";
                Label251.Text = "";
                Label258.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeGestionnaire_AfterCloseUp(object sender, EventArgs e)
        {
            if (txtCodeGestionnaire.ActiveRow == null)
                return;
            Label255.Text = txtCodeGestionnaire.ActiveRow.Cells["NomGestion"].Text;

            Label256.Text = txtCodeGestionnaire.ActiveRow.Cells["TelGestion"].Text;

            Label257.Text = txtCodeGestionnaire.ActiveRow.Cells["TelPortable_GES"].Text;

            Label251.Text = txtCodeGestionnaire.ActiveRow.Cells["FaxGestion"].Text;

            Label258.Text = txtCodeGestionnaire.ActiveRow.Cells["EMail_GES"].Text;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeGestionnaire_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "";
            General.sSQL = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion, Qualification.Qualification," + " Gestionnaire.TelGestion, Gestionnaire.TelPortable_GES," + " Gestionnaire.FaxGestion,  Gestionnaire.EMail_GES";
            //sSQL = sSQL & " FROM Gestionnaire  "
            General.sSQL = General.sSQL + " FROM         Gestionnaire INNER JOIN" + " Qualification ON Gestionnaire.CodeQualif_Qua = Qualification.CodeQualif  ";

            General.sSQL = General.sSQL + " where Gestionnaire.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
            //sSQL = sSQL & " and Gestionnaire.codeQualif_qua = '" & cGestionnaire & "'"

            sheridan.InitialiseCombo(txtCodeGestionnaire, General.sSQL, "CodeGestinnaire");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeImmeuble_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)

        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            // apostrophe
            if (KeyAscii == 39)
            {
                KeyAscii = 0;
            }
            // /
            if (KeyAscii == 47)
            {
                KeyAscii = 0;
            }
            // \
            if (KeyAscii == 92)
            {
                KeyAscii = 0;
            }
            if (KeyAscii == 13 && blnAjout == false)
            {
                using (var tmpModAdo = new ModAdo())
                {
                    //===> Mondir le 17.06.2021 https://groupe-dt.mantishub.io/view.php?id=2216
                    var count = tmpModAdo.fc_ADOlibelle("SELECT COUNT(*) FROM Immeuble WHERE Immeuble.CodeImmeuble LIKE '%" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "%'").ToInt();
                    //===> Fin Modif Mondir

                    if (count == 1 && !string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'"), "").ToString()))
                    {
                        ////=======================> Added By Mondir : I Had to force the control 'txtCodeImmeuble' to list its Focus, coz if not, his event will be executed later and will coz some probleme
                        ////=======================> For example : The user enter the 'Code Immeuble' the  he will click on ENTER Key on the keyboard,(txtCodeImmeuble has the focus allways !),
                        ////=======================> Then the user will click a double click on a row in the grid to navigate to UserDocIntervention, in this case the event 'txtCodeImmeuble_Leave' will be executed before
                        ////=======================> the double click event on the grid, so in this event 'txtCodeImmeuble_Leave' we have a code that will a row selected !
                        ////=======================> after the douvle click event will be executed but in the time we already lost the 'Intervention' we want !
                        ssIntervention.Focus();
                        fc_ChargeEnregistrement(txtCodeImmeuble.Text);
                    }
                    else
                    {
                        General.blControle = true;
                        string sCode = null;

                        try
                        {
                            if (string.IsNullOrEmpty(txtCode1.Text))
                            {
                                int xx = General.Execute("DELETE FROM Immeuble WHERE CodeImmeuble='" +
                                                         txtCodeImmeuble.Text + "'");
                            }

                            frmResultat.Visible = false;


                            //string requete = "SELECT     Immeuble.CodeImmeuble AS \"Code Immeuble\", " + "Immeuble.Adresse AS \"adresse\", Immeuble.Ville AS \"Ville\", " +
                            //                 " Immeuble.AngleRue AS \"angle de rue\", " + " Immeuble.Code1 AS \"Gerant\", Table1.commercial as \"Mat. commercial\"" + " FROM         Immeuble INNER JOIN" +
                            //                 " Table1 ON Immeuble.Code1 = Table1.Code1";
                            //====> Mondir le 06.07.2020, Afficher le commercial de l'immeuble et non commercial du client
                            string requete = "SELECT     Immeuble.CodeImmeuble AS \"Code Immeuble\", " +
                                             "Immeuble.Adresse AS \"adresse\", Immeuble.Ville AS \"Ville\", " +
                                             " Immeuble.AngleRue AS \"angle de rue\", " +
                                             " Immeuble.Code1 AS \"Gerant\", Immeuble.CodeCommercial as \"Mat. commercial\"" +
                                             " FROM         Immeuble";
                            //====> Fin Modif Mondir
                            string where_order = "";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "")
                            { Text = "Recherche des immeuble" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                // charge les enregistrements de l'immeuble
                                sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fc_ChargeEnregistrement(sCode);
                                //fc_ChargeGrilleGestionnaire scode
                                //fc_ChargeGrilleInterve
                                General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                                fg.Dispose();
                                fg.Close();
                            };

                            fg.ugResultat.KeyDown += (se, ev) =>
                            {

                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    // charge les enregistrements de l'immeuble
                                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fc_ChargeEnregistrement(sCode);
                                    //fc_ChargeGrilleGestionnaire scode
                                    //fc_ChargeGrilleInterve
                                    General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                                    fg.Dispose();
                                    fg.Close();
                                }
                            };
                            fg.StartPosition = FormStartPosition.CenterParent;
                            fg.ShowDialog();
                            txtAdresse.Focus();
                            SSTab1.SelectedTab = SSTab1.Tabs[0];


                        }
                        catch (Exception e)
                        {
                            Erreurs.gFr_debug(e, this.Name + ";Command1_Click");
                        }

                        txtCodeImmeuble.iTalkTB.Focus();
                    }
                }

                KeyAscii = 0;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeuble_Validated(object sender, EventArgs e)
        {
            //string sCode = null;
            //string sCodeImmeubleA = null;

            //if (General.blControle == false)
            //{
            //    // controle si code client Existant
            //    sCode = txtCodeImmeuble.Text;
            //    General.sSQL = "SELECT Immeuble.codeImmeuble" + " From Immeuble" + " WHERE Immeuble.codeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            //    using (var tmpModAdo = new ModAdo())
            //    {
            //        General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);

            //        if (General.rstmp.Rows.Count > 0)
            //        {
            //            // si enregistrement éxiste déja
            //            if (blnAjout == true)
            //            {
            //                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Immeuble déjà existant", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

            //            }
            //            sCode = General.rstmp.Rows[0]["CodeImmeuble"].ToString();
            //            fc_ChargeEnregistrement(sCode);
            //            txtCodeImmeuble.Text = sCode;
            //        }
            //        else if (blnAjout == false && !string.IsNullOrEmpty(txtCodeImmeuble.Text))
            //        {
            //            //MsgBox "Immeuble inéxistant", vbCritical, ""
            //            sCodeImmeubleA = txtCodeImmeuble.Text;
            //            fc_Clear();
            //            txtCodeImmeuble.Text = sCodeImmeubleA;
            //        }
            //    }
            //}
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodePostal_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodePostal_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeReglement_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
            fc_AfficheReglement((txtCodeReglement_IMM.Text));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeReglement_IMM_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                General.blControle = true;
                CmdRechercheReglement_Click(CmdRechercheReglement, new System.EventArgs());
                KeyAscii = 0;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeReglement_IMM_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            if (General.blControle == false)
            {
                eventArgs.Cancel = fc_AfficheReglement(txtCodeReglement_IMM.Text, true);

            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>

        private void txtCodeTVA_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(txtCodeTVA.Text))
            {
                txtCodeTVA.Text = txtCodeTVA.Text.ToUpper();
                if (txtCodeTVA.Text != "TR" && txtCodeTVA.Text != "TP")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Seuls les valeurs TR ou TP sont admises.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    eventArgs.Cancel = true;
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeTVA_TextChanged(object sender, EventArgs e)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercial_Click(object sender, EventArgs e)
        {
            OptSyndic.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT DISTINCT Personnel.Matricule, Personnel.Nom " + " FROM Personnel INNER JOIN Table1 ON Personnel.Matricule=Table1.Commercial ORDER BY" +
                           " Personnel.Matricule";

            sheridan.InitialiseCombo(txtCommercial, General.sSQL, "Matricule", false);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCptAppelFond_TextChanged(object sender, EventArgs e)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtEcheance2_Validated(object sender, EventArgs e)
        {
            if (!(General.IsDate(txtEcheance2.Text)) && !string.IsNullOrEmpty(txtEcheance2.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "Données éronnées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtEcheance2.Focus();
                return;
                //ElseIf txtEcheance2 = "" Then
                //   MsgBox "Vous devez saisir une date", vbCritical, "Données éronnéés"
                //  txtEcheance2.SetFocus
                // Exit Sub
            }
            else if (!string.IsNullOrEmpty(txtEcheance2.Text))
            {
                txtEcheance2.Text = Convert.ToDateTime(txtEcheance2.Text).ToString("dd/MM/yyyy");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtHorairesLoge_TextChanged(object sender, EventArgs e)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmeuble1_Click(object sender, EventArgs e)
        {
            OptImmeuble.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmeuble2_Click(object sender, EventArgs e)
        {
            OptImmeuble.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtinterDe_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            bool Cancel = eventArgs.Cancel;
            if (!string.IsNullOrEmpty(txtinterDe.Text) && !General.IsDate(txtinterDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txtinterDe.Focus();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtIntereAu_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            bool Cancel = eventArgs.Cancel;
            if (!string.IsNullOrEmpty(txtIntereAu.Text) && !General.IsDate(txtIntereAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txtIntereAu.Focus();
            }
            eventArgs.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtNCompte_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtObservations_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtParticulier1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            OptParticulier.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtParticulier2_Click(object sender, EventArgs e)
        {
            OptParticulier.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtSyndic1_Click(object sender, EventArgs e)
        {
            OptSyndic.Checked = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtSyndic2_ClickEvent(System.Object eventSender, System.EventArgs eventArgs)
        {
            //    txtCommercial.Text = ""
            OptSyndic.Checked = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtVille_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtVilleFact_IMM_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocImmeuble_VisibleChanged(object sender, EventArgs e)
        {
            if (Visible)
            {
                View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocImmeuble");
                txt161.MaxLength = 100;
                //Frame330.Enabled = false;Todo

                fc_Droit();
                bLoadChange = true;
                bLoadChange = true;

                //=== version dédié à pz.
                if (General.sActiveVersionPz == "1")
                {
                    txt100.Visible = true;
                    Label4187.Visible = true;
                    txt101.Visible = true;
                    Label4188.Visible = true;
                    Label4190.Visible = true;
                    OptOu.Visible = true;
                    OptEt.Visible = true;
                    ssFindImmeuble.Visible = true;

                    Label413.Visible = false;
                    lblCodeEnergie.Visible = false;
                    lblEnergie.Visible = false;
                    Check41.Visible = false;
                    Check42.Visible = false;
                    Check43.Visible = false;
                    Check44.Visible = false;
                    Label458.Visible = false;
                    txtCommercial.Visible = false;


                }
                else
                {
                    txt100.Visible = false;
                    Label4187.Visible = false;
                    txt101.Visible = false;
                    Label4188.Visible = false;
                    Label4190.Visible = false;
                    OptOu.Visible = false;
                    OptEt.Visible = false;
                    ssFindImmeuble.Visible = false;

                    Label413.Visible = true;
                    lblCodeEnergie.Visible = true;
                    lblEnergie.Visible = true;
                    Check41.Visible = true;
                    Check42.Visible = true;
                    Check43.Visible = true;
                    Check44.Visible = true;
                    Label458.Visible = true;
                    txtCommercial.Visible = true;

                }

                //If sTomtom = "1" Then
                //    Frame3(23).Visible = True
                //Else
                //    Frame3(23).Visible = False
                //End If

                //If UCase(adocnn.DefaultDatabase) = UCase(cDelostal) Then
                if (General.sVersionImmParComm == "1")
                {
                    Label459.Visible = false;
                    lblMatriculeREspExpl.Visible = false;
                    lblLibRespExpl.Visible = false;
                    lblQualifRespExpl.Visible = false;
                    Label15.Visible = false;
                    lblCodeCommercial.Visible = false;
                    lblCommercNom.Visible = false;
                    lblCommercPrenom.Visible = false;
                    Frame322.Visible = true;

                    Label18.Visible = true;

                    txt103.Visible = true;
                    txt102.Visible = true;
                    txt106.Visible = true;

                    Label115.Visible = true;

                    txt110.Visible = true;
                    txt109.Visible = true;
                    txt111.Visible = true;
                    ssGridComptence.Visible = true;
                }
                else
                {

                    Label459.Visible = true;
                    lblMatriculeREspExpl.Visible = true;
                    lblLibRespExpl.Visible = true;
                    lblQualifRespExpl.Visible = true;
                    Label15.Visible = true;
                    lblCodeCommercial.Visible = true;
                    lblCommercNom.Visible = true;
                    lblCommercPrenom.Visible = true;
                    Frame322.Visible = false;
                    Label18.Visible = false;

                    txt103.Visible = false;
                    txt102.Visible = false;
                    txt106.Visible = false;
                    Label115.Visible = false;
                    txt110.Visible = false;
                    txt109.Visible = false;
                    txt111.Visible = false;
                    ssGridComptence.Visible = false;

                }

                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    ModP1Gaz.fc_OpConnectP1Gaz();

                }

                if (General.adocnn.Database.ToUpper() == General.cNameGDP.ToUpper())
                {
                    SSTab1.TabsPerRow = 15;
                    SSTab1.Tabs[9].Visible = true;
                    SSTab1.Tabs[10].Visible = true;
                    SSTab1.Tabs[11].Visible = true;
                    SSTab1.Tabs[12].Visible = true;
                    SSTab1.Tabs[13].Visible = true;
                    Frame319.Visible = true;
                }
                else
                {
                    SSTab1.Tabs[9].Visible = false;
                    SSTab1.Tabs[10].Visible = false;
                    SSTab1.Tabs[11].Visible = false;
                    SSTab1.Tabs[12].Visible = false;
                    SSTab1.Tabs[13].Visible = false;
                    SSTab1.TabsPerRow = 10;
                    Frame319.Visible = false;
                }


                SSTab1.SelectedTab = SSTab1.Tabs[0];
                SSTab2.SelectedTab = SSTab2.Tabs[0];
                //sstab1.TabVisible(4) = False

                if (General.gfr_liaison("Affiche Logimatique", "0") == "0")
                {
                    cmdLogimatique.Visible = false;
                }
                else
                {
                    cmdLogimatique.Visible = true;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                General.open_conn();


                //Partie Impression des relevés de compte de l'immeuble

                if (General.adocnn.State == ConnectionState.Open)
                {
                    SDADATA1 = new SqlDataAdapter("SELECT * FROM Commentaire;", General.CHEMINBASE);
                    DATA1 = new DataTable();
                    SDADATA1.Fill(DATA1);
                    if (DATA1.Rows.Count > 0)
                    {
                        Text1.Text = DATA1.Rows[0]["Commentaire1"].ToString();
                        Text2.Text = DATA1.Rows[0]["Commentaire2"].ToString();
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La Connection avec la base de données est infructieuse", "Connection non établie", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                Adodc17 = new DataTable();
                SDAAdodc17 = new SqlDataAdapter("SELECT CodeImmeuble,NCompte FROM IMMEUBLE order by CodeImmeuble", General.CHEMINBASE);
                SDAAdodc17.Fill(Adodc17);

                Adodc20 = new DataTable();
                SDAAdodc20 = new SqlDataAdapter("SELECT CodeImmeuble,NCompte FROM IMMEUBLE order by CodeImmeuble", General.CHEMINBASE);
                SDAAdodc20.Fill(Adodc20);

                Adodc16 = new DataTable();
                SDAAdodc16 = new SqlDataAdapter("SELECT Code1 FROM Table1 order by Code1", General.CHEMINBASE);
                SDAAdodc16.Fill(Adodc16);

                Adodc19 = new DataTable();
                SDAAdodc19 = new SqlDataAdapter("SELECT Code1 FROM Table1 order by Code1", General.CHEMINBASE);
                SDAAdodc19.Fill(Adodc19);

                Adodc18 = new DataTable();
                SDAAdodc18 = new SqlDataAdapter("SELECT CodeParticulier,CodeImmeuble,NCompte FROM ImmeublePart order by CodeParticulier", General.CHEMINBASE);
                SDAAdodc18.Fill(Adodc18);

                Adodc15 = new DataTable();
                SDAAdodc15 = new SqlDataAdapter("SELECT CodeParticulier,CodeImmeuble,NCompte FROM ImmeublePart order by CodeParticulier", General.CHEMINBASE);
                SDAAdodc15.Fill(Adodc15);

                txtParticulier1.DataSource = Adodc18;
                txtSyndic1.DataSource = Adodc16;
                txtImmeuble1.DataSource = Adodc17;
                txtImmeuble1.DisplayLayout.Bands[0].Columns["NCompte"].Hidden = true;
                txtSyndic2.DataSource = Adodc19;
                txtImmeuble2.DataSource = Adodc20;
                txtImmeuble2.DisplayLayout.Bands[0].Columns["NCompte"].Hidden = true;
                txtParticulier2.DataSource = Adodc15;

                //    With Adodc11
                //        .ConnectionString = CHEMINBASE
                //        .RecordSource = "SELECT DISTINCT Table1.Commercial AS Matricule, Personnel.Nom " _
                //'                & " FROM Personnel INNER JOIN Table1 ON Personnel.Matricule=Table1.Commercial ORDER BY Table1.Commercial"
                //        .Refresh
                //        txtCommercial.Refresh
                //    End With

                fc_ChargeComboPersonnel();
                fc_LoadArticle();

                //If UCase(adocnn.DefaultDatabase) = UCase(cNameAmmann) And UCase(fncUserName) = UCase("rachid abbouchi") Then
                //fc_ChargeExercicesAmmann MoisFinExercice
                //Else

                fc_ChargeExercices(General.MoisFinExercice);
                //End If
                fc_ChargeDateExercice();

                SSTab1.SelectedTab = SSTab1.Tabs[0];

                fc_ComboTRTP();
                fc_ComboDep();
                fc_ComboDisp();
                //- positionne sur le dernier enregistrement.
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    // charge les enregistremants
                    fc_ChargeEnregistrement(ModParametre.sNewVar);
                }
                else
                {
                    fc_InitialiseGrille();
                    fc_Clear();
                    fc_BloqueForm();
                }
                // positionne sur l'historique intervention.
                if (General.getFrmReg(Variable.cUserDocImmeuble, "Historique", "") == "1")
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[7];
                    txtinterDe.Text = General.getFrmReg(Variable.cUserDocImmeuble, "De", "");
                    txtIntereAu.Text = General.getFrmReg(Variable.cUserDocImmeuble, "Au", "");
                    cmbStatus.Text = General.getFrmReg(Variable.cUserDocImmeuble, "Status", "");
                    cmbIntervenant.Text = General.getFrmReg(Variable.cUserDocImmeuble, "Intervenant", "");
                    cmbArticle.Text = General.getFrmReg(Variable.cUserDocImmeuble, "Article", "");
                    txt158.Text = General.getFrmReg(Variable.cUserDocImmeuble, "Commentaire", "");
                    txt162.Text = General.getFrmReg(Variable.cUserDocImmeuble, "SaisieDe", "");
                    txtDateSaisieFin.Text = General.getFrmReg(Variable.cUserDocImmeuble, "SaisieFin", "");
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                    General.saveInReg(Variable.cUserDocImmeuble, "Historique", "0");
                }
                if (General.getFrmReg(Variable.cUserDocImmeuble, "Historique", "") == "2")
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[8];
                    txtDevisDe.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisDe", "");
                    txtDevisAu.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisAu", "");
                    cmbDevisStatus.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisStatus", "");
                    cmbDevisIntervenant.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisIntervenant", "");
                    cmdDevisArticle.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisArticle", "");
                    txtDevisCommentaire.Text = General.getFrmReg(Variable.cUserDocImmeuble, "DevisCommentaire", "");
                    cmbDevis_Click(cmbDevis, new System.EventArgs());
                    General.saveInReg(Variable.cUserDocImmeuble, "Historique", "0");
                }


                bLoadChange = false;
                OptEt.Checked = General.getFrmReg(Variable.cUserDocImmeuble, "OptEt", Convert.ToString(true)) == "Vrai";
                OptOu.Checked = General.getFrmReg(Variable.cUserDocImmeuble, "optOu", Convert.ToString(true)) == "Vrai";

                if (General.sActiveP1 == "1")
                {
                    SSTab1.Tabs[14].Visible = true;
                }
                else
                {
                    SSTab1.Tabs[14].Visible = false;
                }

                //=== modif du 047 07 2018, on affiche ou pas le ramoneur.
                if (General.sRamoneurParImmeuble == "1")
                {
                    ssCombo21.Visible = true;
                    txt160.Visible = true;
                    Label116.Visible = true;
                }
                else
                {
                    ssCombo21.Visible = false;
                    txt160.Visible = false;
                    Label116.Visible = false;
                }


                if (General.sReleveCoproPoprietaire == "1")
                {
                    Frame327.Visible = true;
                    //'Frame3(28).Visible = True;
                    //Frame328.Top = 2760;
                    //OptParticulier.Top = 2760;
                }

                else
                {

                    Frame327.Visible = false;
                    ////'Frame3(28).Visible = False
                    //OptParticulier.Top = 2400;
                    //Frame328.Top = 2400;
                }

                /// Mohammed 24.06.2020 added rachid's modifs
                if (General.sEvolution == "1")
                {
                    Cmd10.Visible = true;
                    Cmd11.Visible = true;
                }
                else
                {
                    Cmd10.Visible = false;
                    Cmd11.Visible = false;
                }
                if (General.sChefSecteurLibre == "1")
                {
                    ssCombo22.Visible = true;
                    txt104.Visible = false;
                }
                else
                {
                    ssCombo22.Visible = false;
                    txt104.Visible = true;
                }

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                if (General.sAdresseEnvoie == "1")
                {
                    txt165.Visible = true;
                    txt166.Visible = true;
                    txt167.Visible = true;
                    txt168.Visible = true;
                    txt169.Visible = true;
                    txt170.Visible = false;
                    Label4_217.Visible = false;
                    txt171.Visible = true;
                    txt172.Visible = true;
                    Check22.Visible = true;


                    txt165.MaxLength = 50;
                    txt166.MaxLength = 50;
                    txt167.MaxLength = 50;
                    txt168.MaxLength = 50;
                    txt169.MaxLength = 50;
                    txt170.MaxLength = 50;
                    txt171.MaxLength = 50;
                    txt172.MaxLength = 50;
                }
                else
                {
                    txt165.Visible = false;
                    txt166.Visible = false;
                    txt167.Visible = false;
                    txt168.Visible = false;
                    txt169.Visible = false;
                    txt170.Visible = false;
                    Label4_217.Visible = false;
                    txt171.Visible = false;
                    txt172.Visible = false;
                    Check22.Visible = false;
                }
                //===> Fin Modif Mondir
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        public void fc_ComboDisp()
        {
            //    Dim i As Integer
            //    Dim rstmp As ADODB.Recordset
            //    Dim req As String

            //    Set rstmp = New ADODB.Recordset
            //
            //    req = "SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif," _
            //'        & " Qualification.CodeQualif,Qualification.Qualification " _
            //'        & "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif "
            //
            //    If nbCodeDispatch > 0 Then
            //        req = req & " WHERE "
            //        For i = 1 To (nbCodeDispatch)
            //            If i < (nbCodeDispatch) Then
            //                req = req & "Personnel.CodeQualif='" & CodeQualifDispatch(i) & "' OR "
            //            Else
            //                req = req & "Personnel.CodeQualif='" & CodeQualifDispatch(i) & "'"
            //            End If
            //        Next i
            //        req = req & " ORDER BY Personnel.Matricule "
            //    End If
            //
            //    rstmp.Open req, adocnn
            //
            //    If Not rstmp.EOF And Not rstmp.BOF Then
            //        rstmp.MoveFirst
            //        While Not rstmp.EOF
            //            SSOleDBCmbDispatch.AddItem rstmp!matricule & vbTab & rstmp!Nom & vbTab & rstmp!Prenom & vbTab & rstmp!Qualification
            //            rstmp.MoveNext
            //        Wend
            //    End If
            //    rstmp.Close
            //    Set rstmp = Nothing
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ChargeComboPersonnel()
        {
            DataTable rsPersonnel = default(DataTable);
            var modAdoDataTable = new ModAdo();

            rsPersonnel = modAdoDataTable.fc_OpenRecordSet("SELECT  Matricule,Nom FROM Personnel WHERE MATRICULE is not null order by Nom");

            if (rsPersonnel.Rows.Count > 0)
            {
                SSOleDBDropDown1.DataSource = rsPersonnel;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadArticle()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            var modAdors = new ModAdo();

            try
            {
                DropDownArticle.DataSource = null;

                sSQL = "SELECT     CodeArticle, Designation1 " + " FROM         FacArticle " + " order by CodeArticle";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                DropDownArticle.DataSource = rs;


            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_LoadArticle");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMoisFin"></param>
        /// <returns></returns>
        private object fc_ChargeExercices(string sMoisFin)
        {
            object functionReturnValue = null;

            string sqlExercices = null;
            DataTable rsExercices = default(DataTable);
            SqlDataAdapter SDArsExercices = null;
            SqlCommandBuilder SCBrsExercices = null;
            string strAddItem = null;
            string JourMaxiMois = null;
            string DateMini = null;
            string DateMaxi = null;
            int i = 0;
            bool blnMatch = false;

            ///========> Tested
            if (General.sDesActiveSage == "1")
            {
                return functionReturnValue;
            }

            blnMatch = false;
            SAGE.fc_OpenConnSage();

            sqlExercices = "SELECT DISTINCT YEAR(JM_Date) AS Annee";
            sqlExercices = sqlExercices + " FROM F_JMOUV";
            sqlExercices = sqlExercices + " ORDER BY YEAR(JM_Date) DESC";

            rsExercices = new DataTable();
            SDArsExercices = new SqlDataAdapter(sqlExercices, SAGE.adoSage);
            SDArsExercices.Fill(rsExercices);

            JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt16(sMoisFin)));

            ////=========> Tested
            if (sMoisFin == "12" || !General.IsNumeric(sMoisFin))
            {
                DateMini = "01/" + "01" + "/";
                DateMaxi = JourMaxiMois + "/12/";
            }
            else
            {
                if (sMoisFin == "12")
                {
                    DateMini = "01/" + "01" + "/";
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString("00") + "/";
                    //MoisFinExercice
                }
                else
                {
                    DateMini = "01/" + (Convert.ToInt16(sMoisFin) + 1).ToString("00") + "/";
                    //MoisFinExercice - 1
                    DateMaxi = JourMaxiMois + "/" + Convert.ToInt16(sMoisFin).ToString("00") + "/";
                    //MoisFinExercice
                }
            }

            var cmbExercicesSource = new DataTable();
            cmbExercicesSource.Columns.Add("Exercice");
            cmbExercicesSource.Columns.Add("Du");
            cmbExercicesSource.Columns.Add("Au");

            var NewRow = cmbExercicesSource.NewRow();

            if (rsExercices.Rows.Count > 0)
            {
                foreach (DataRow rsExercicesRow in rsExercices.Rows)
                {
                    ///==============> Tested
                    if (Convert.ToInt32(rsExercicesRow["Annee"]) >= DateTime.Today.Year)
                    {
                        if (sMoisFin != "12")
                        {
                            if (blnMatch == false && Convert.ToInt16(General.nz(sMoisFin, "1")) < Convert.ToInt16(DateTime.Today.Month))
                            {
                                cmbExercicesSource.Rows.Add(
                                    rsExercicesRow["Annee"] + "/" + (Convert.ToInt32(rsExercicesRow["Annee"]) + 1),
                                    DateMini + rsExercicesRow["Annee"],
                                    DateMaxi + (Convert.ToInt32(rsExercicesRow["Annee"]) + 1));
                                cmbExercices.DataSource = cmbExercicesSource;
                                blnMatch = true;
                            }

                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt32(rsExercicesRow["Annee"]) - 1) + "/" + rsExercicesRow["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt32(rsExercicesRow["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];

                        }
                        else if (sMoisFin == "12")
                        {
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = Convert.ToInt32(rsExercicesRow["Annee"]);
                            NewRow["Du"] = DateMini + (rsExercicesRow["Annee"]);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];

                        }
                    }
                    else
                    {
                        if (sMoisFin == "12")
                        {
                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = Convert.ToInt32(rsExercicesRow["Annee"]);
                            NewRow["Du"] = DateMini + (rsExercicesRow["Annee"]);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];

                        }
                        ///===========> Tested
                        else
                        {

                            NewRow = cmbExercicesSource.NewRow();
                            NewRow["Exercice"] = (Convert.ToInt32(rsExercicesRow["Annee"]) - 1) + "/" + rsExercicesRow["Annee"];
                            NewRow["Du"] = DateMini + (Convert.ToInt32(rsExercicesRow["Annee"]) - 1);
                            NewRow["Au"] = DateMaxi + rsExercicesRow["Annee"];
                        }
                    }
                    //  Debug.Print strAddItem
                    cmbExercicesSource.Rows.Add(NewRow);
                    cmbExercices.DataSource = cmbExercicesSource;
                }
            }

            //=== afiiche un exercice par defaut dans la liste deroulante.


            for (i = 0; i <= cmbExercices.Rows.Count - 1; i++)
            {
                cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"];
                if (General.IsDate(cmbExercices.Rows[i].Cells["Du"].ToString()) && General.IsDate(cmbExercices.Rows[i].Cells["Au"].ToString()))
                {
                    if (Convert.ToDateTime(cmbExercices.Rows[i].Cells["Du"].Value) <= DateTime.Today && Convert.ToDateTime(cmbExercices.Rows[i].Cells["Au"].Value) >= DateTime.Today)
                    {
                        cmbExercices.Value = cmbExercices.Rows[i].Cells["Exercice"].Value;
                        if (cmbExercices.ActiveRow != null)
                        {
                            cmbExercices.ActiveRow.Cells[1].Value = cmbExercices.Rows[i].Cells[1].Value;
                            cmbExercices.ActiveRow.Cells[2].Value = cmbExercices.Rows[i].Cells[1].Value;
                        }
                        break;
                    }
                }
                if (i == cmbExercices.Rows.Count - 1)
                {
                    cmbExercices.Value = cmbExercices.Rows[0].Cells["Exercice"].Value;
                }
            }



            SAGE.fc_CloseConnSage();
            return functionReturnValue;

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ChargeDateExercice()
        {

            string JourMaxiMois = null;

            JourMaxiMois = Convert.ToString(General.CalculMaxiJourMois(Convert.ToInt16(General.nz(General.MoisFinExercice, "12"))));

            if (General.MoisFinExercice == "12" || !General.IsNumeric(General.MoisFinExercice))
            {
                txtExercice0.Text = "01/" + "01" + "/" + Convert.ToString(DateTime.Today.Year);
                txtExercice1.Text = JourMaxiMois + "/" + General.MoisFinExercice + "/" + Convert.ToString(DateTime.Today.Year);
            }
            else
            {
                ////===========> Tested
                if (DateTime.Today.Month >= 1 && DateTime.Today.Month <= Convert.ToInt16(General.MoisFinExercice))
                {
                    txtExercice0.Text = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year - 1);
                    //MoisFinExercice + 1
                    txtExercice1.Text = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //MoisFinExercice
                }
                else
                {
                    txtExercice0.Text = "01/" + (Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                    //MoisFinExercice - 1
                    txtExercice1.Text = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year + 1);
                    //MoisFinExercice
                }
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ComboTRTP()
        {
            string sAdd = null;

            DataTable txtCodeTVADataSource = new DataTable();
            txtCodeTVADataSource.Columns.Add("Column 0");
            txtCodeTVADataSource.Rows.Add("TR");
            txtCodeTVADataSource.Rows.Add("TP");
            txtCodeTVA.DataSource = txtCodeTVADataSource;
            txtCodeTVA.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Taux de tva";
            txtCodeTVA.ValueMember = "Column 0";
        }

        /// <summary>
        /// Tested
        /// </summary>
        public void fc_ComboDep()
        {
            short i = 0;
            DataTable rstmp = default(DataTable);
            var modAdoDataTable = new ModAdo();
            string req = null;


            req = "SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif,Personnel.Dep2," + " Personnel.CSecteur , " + " Qualification.CodeQualif,Qualification.Qualification, Personnel.Rondier " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

            if (General.nbCodeTechnicien > 0)
            {
                req = req + " WHERE ";
                for (i = 1; i <= (General.nbCodeTechnicien); i++)
                {
                    if (i < (General.nbCodeTechnicien))
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifTechnicien[i] + "' OR ";
                    }
                    else
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifTechnicien[i] + "'";
                    }
                }
                req = req + " ORDER BY Personnel.Matricule ";
            }

            rstmp = modAdoDataTable.fc_OpenRecordSet(req);

            SSOleDBCmbDep1.DataSource = null;
            ssCombo21.DataSource = null;

            if (rstmp.Rows.Count > 0)
            {
                SSOleDBCmbDep1.DataSource = rstmp;
                ssCombo21.DataSource = rstmp;

            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridComptence_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            try
            {
                string sSQL = null;

                if (GridCompteurs.ActiveRow == null)
                    return;

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["CodeUO"].Text))
                {
                    GridCompteurs.ActiveRow.Cells["CodeUO"].Value = "01";
                }

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["CodeImmeuble"].Text))
                {
                    GridCompteurs.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["CodeNumOrdre"].Text))
                {
                    GridCompteurs.ActiveRow.Cells["CodeNumOrdre"].Value = "00";
                }

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["CodeChaufferie"].Text))
                {
                    GridCompteurs.ActiveRow.Cells["CodeChaufferie"].Value = "0000";
                }

                if (GridCompteurs.ActiveCell != null && GridCompteurs.ActiveCell.Column.Key.ToUpper() == "NumAppareil".ToUpper())
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(GridCompteurs.ActiveCell.OriginalValue)) && Convert.ToString(GridCompteurs.ActiveCell.OriginalValue) !=
                        GridCompteurs.ActiveRow.Cells["NumAppareil"].Text)
                    {
                        e.Cancel = true;
                        return;
                    }
                }

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["NumAppareil"].Text))
                {
                    GridCompteurs.ActiveRow.Cells["NumAppareil"].Value = Convert.ToString(ModP1.fc_GetNumAppereils(1));
                    GridCompteurs.ActiveRow.Cells["OrigineP1"].Value = "0";
                }


                if (GridCompteurs.ActiveCell != null && GridCompteurs.ActiveCell.Column.Key.ToUpper() == "Localisation".ToUpper())
                {
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle("SELECT PDAB_Noauto From PDAB_BadgePDA " + " WHERE PDAB_Libelle = '" +
                            StdSQLchaine.gFr_DoublerQuote(GridCompteurs.ActiveCell.Text) + "'" + " AND Codeimmeuble = '"
                            + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        GridCompteurs.ActiveRow.Cells["PDAB_Noauto"].Value = Convert.ToInt32(sSQL);
                    }
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";GridCompteurs_BeforeColUpdate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeimmeubleTemp_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                if (bLoadChange == false)
                {
                    if (!string.IsNullOrEmpty(txtCodeimmeubleTemp.Text))
                    {
                        txtCodeImmeuble.Text = txtCodeimmeubleTemp.Text;

                        General.saveInReg(Variable.cUserDocImmeuble, "OptEt", Convert.ToString(OptEt.Checked));
                        General.saveInReg(Variable.cUserDocImmeuble, "OptOu", Convert.ToString(OptOu.Checked));

                        txtCodeImmeuble_KeyPress(txtCodeImmeuble, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
                    }
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";txtCodeimmeubleTemp_Change");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeChefSecteur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                General.blControle = true;
                //cmdRechercheChefSecteur_Click(sender, e);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo5_ValueChanged(object sender, EventArgs e)
        {
            var ctl = sender as UltraCombo;
            int Index = Convert.ToInt32(ctl.Tag);
            switch (Index)
            {


                case 5:
                    //==type de compteur.

                    Label4116.Text = ModP1Gaz.fc_ADOlibelleP1Gaz("SELECT Libelle FROM TypeCompteur WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(ssCombo5.Text) + "'");
                    break;


                //// ===============> Tested
                case 6:
                    //=== pression aval.

                    Label4117.Text = ModP1Gaz.fc_ADOlibelleP1Gaz("SELECT Pression, Unite AS Designation  " + " FROM p_PressionAval WHERE CleAuto =" + General.nz(ssCombo6.Text, 0), true);
                    break;
                //Label4(117) = nz(gFr_ADO_dLookup("", "p_PressionAval", "CleAuto = " & nz(cmbPressionAval.Text, "0")), "")
                case 8:
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo5_Click(object sender, EventArgs e)
        {
            var ctl = sender as UltraCombo;
            int Index = Convert.ToInt32(ctl.Tag);
            switch (Index)
            {


                case 5:
                    //==type de compteur.

                    Label4116.Text = ModP1Gaz.fc_ADOlibelleP1Gaz("SELECT Libelle FROM TypeCompteur WHERE Code ='" + StdSQLchaine.gFr_DoublerQuote(ssCombo5.Text) + "'");
                    break;

                case 6:
                    //=== pression aval.

                    Label4117.Text = ModP1Gaz.fc_ADOlibelleP1Gaz("SELECT Pression, Unite AS Designation  " + " FROM p_PressionAval WHERE CleAuto =" + General.nz(ssCombo6.Text, 0), true);
                    break;

                case 8:
                    //=== Correcteur.
                    break;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblCodeCommercial_DoubleClick(object sender, EventArgs e)
        {
            cmbCodeCommercial.Visible = true;
            cmbCodeCommercial.Text = lblCodeCommercial.Text;
            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, le commercial ne sera attaché qu'à cet immeuble (le gérant n'en est pas affecté)", "Modification du commercial de l'immeuble", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblGoContrat_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //    MsgBox "Fonctionnalité non installée", vbInformation, "Contrat"
            //    Exit Sub

            try
            {
                fc_sauver();
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    if (!string.IsNullOrEmpty(txtCodeContrat.Text))
                    {
                        // stocke la position de la fiche immeuble
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtCodeContrat.Text);
                        View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
                    }
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";lblGoContrat_Click");
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>

        private void Label4_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            var ctl = eventSender as Label;
            int Index = Convert.ToInt32(ctl.Tag);
            if (Index == 17)
            {
                if (!string.IsNullOrEmpty(txtCode1.Text))
                {
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);

                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                    View.Theme.Theme.Navigate(typeof(UserDocClient));
                }
                else
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas affecté de client à l'immeuble." + "\n" + "Voulez-vous en affecter un maintenant ?", "Navigation vers le client annulée", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cmdRechercheClient_Click(cmdRechercheClient, new System.EventArgs());
                    }
                }
            }
            else if (Index == 202)
            {



                if (!string.IsNullOrEmpty(Label2511.Text))
                {
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);

                    General.sSQL = "SELECT     CRCL_GroupeClient.CRCL_Code" + " FROM         Table1 INNER JOIN " + " CRCL_GroupeClient ON Table1.CRCL_Code = CRCL_GroupeClient.CRCL_Code" +
                        " WHERE     (Table1.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "')";

                    using (var tmpModAdo = new ModAdo())
                        General.sSQL = tmpModAdo.fc_ADOlibelle(General.sSQL);

                    General.saveInReg(Variable.cUserDocGroupe, "txtCRCL_Code", General.sSQL);

                    View.Theme.Theme.Navigate(typeof(Groupe.UserDocGroupe));

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, il n'y a pas de groupe associé à ce client.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            else if (Index == 205)
            {
                //=== fiche localisation.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);

                General.saveInReg(ModConstantes.cUserDocLocalisation, "txtCodeImmeuble", txtCodeImmeuble.Text);

                View.Theme.Theme.Navigate(typeof(UserDocLocalisation));
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            try
            {
                string sSQL = null;

                if (!string.IsNullOrEmpty(e.Row.Cells["PDAB_Noauto"].Text))
                {

                    sSQL = "SELECT     PDAB_Libelle From PDAB_BadgePDA " + " WHERE  Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'" + " AND PDAB_Noauto = " + e.Row.Cells["PDAB_Noauto"].Value;

                    using (var tmpModAdo = new ModAdo())
                        e.Row.Cells["Localisation"].Value = tmpModAdo.fc_ADOlibelle(sSQL);

                }

                if (!string.IsNullOrEmpty(e.Row.Cells["Type_Compteur"].Text))
                {
                    sSQL = "SELECT LIBELLE  FROM P1_TypeCpt WHERE CODE ='" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Type_Compteur"].Text) + "'";

                    using (var tmpModAdo = new ModAdo())
                        e.Row.Cells["TypeLibelle"].Value = tmpModAdo.fc_ADOlibelle(sSQL);


                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";GridCompteurs_RowLoaded");
            }
        }
        /// <summary>
        /// TODO : Mondir - Must Test This - Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBE_AfterCloseUp(object sender, EventArgs e)
        {
            if (ssGridBE.ActiveRow != null && !string.IsNullOrEmpty(ssGridBE.ActiveRow.Cells["codebe"].Text))
                fc_InfoBe(ssGridBE.ActiveRow.Cells["codebe"].Text, ssGridBE.ActiveRow);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeCommercial_ValueChanged(object sender, EventArgs e)
        {
            if (cmbCodeCommercial.ActiveRow == null)
                return;
            lblCommercNom.Text = cmbCodeCommercial.ActiveRow.Cells["Nom"].Text;
            lblCommercPrenom.Text = cmbCodeCommercial.ActiveRow.Cells["Prenom"].Text;
            lblCodeCommercial.Text = cmbCodeCommercial.ActiveRow.Cells["Matricule"].Text;
            cmbCodeCommercial.Visible = false;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                short i = 0;
                string req = null;

                req = "SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom," + "Qualification.Qualification " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

                if (General.CodeQualifCommercial.Length > 0)
                {
                    req = req + " WHERE ";
                    for (i = 1; i < General.CodeQualifCommercial.Length; i++)
                    {
                        if (i < General.CodeQualifCommercial.Length - 1)
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                        }
                        else
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                        }
                    }
                }

                req = req + " ORDER BY Personnel.Matricule ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (cmbCodeCommercial.Rows.Count == 0)
                {
                    sheridan.InitialiseCombo(cmbCodeCommercial, req, "Matricule");
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmbCodeCommercial_DropDown;");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CodeImmeuble_IMM"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeCorresp_IMC"].Hidden = true;
            if (!e.Layout.Bands[0].Columns.Cast<UltraGridColumn>().Any(c => c.Key.ToUpper() == "Qualification".ToUpper()))
                e.Layout.Bands[0].Columns.Add("Qualification", "Qualification");
            e.Layout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Qualification"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Qualification"].Header.VisiblePosition = 4;
            e.Layout.Bands[0].Columns["nom_imc"].Header.Caption = "Nom";
            e.Layout.Bands[0].Columns["nom_imc"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CodeQualif_QUA"].Header.Caption = "Code Qualif";
            e.Layout.Bands[0].Columns["CodeQualif_QUA"].MaxLength = 50;
            e.Layout.Bands[0].Columns["tel_imc"].Header.Caption = "Tel";
            e.Layout.Bands[0].Columns["tel_imc"].MaxLength = 50;
            e.Layout.Bands[0].Columns["telportable_imc"].Header.Caption = "Tel Portable";
            e.Layout.Bands[0].Columns["telportable_imc"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Fax_IMC"].Header.Caption = "Fax";
            e.Layout.Bands[0].Columns["Fax_IMC"].MaxLength = 50;
            e.Layout.Bands[0].Columns["email_imc"].Header.Caption = "Email";
            e.Layout.Bands[0].Columns["email_imc"].MaxLength = 100;
            e.Layout.Bands[0].Columns["Note_imc"].Header.Caption = "Note";
            e.Layout.Bands[0].Columns["Note_imc"].MaxLength = 50;
            //e.Layout.Bands[0].Columns["PresidentCS_IMC"].Header.Caption = "Président du conseil syndical";
            e.Layout.Bands[0].Columns["PresidentCS_IMC"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeQualif_QUA"].ValueList = this.ssDropQualite;
            e.Layout.Bands[0].Columns["Président du conseil syndical"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Président du conseil syndical"].DefaultCellValue = 0;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CompteCree"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["CompteCree"].Editor.DataFilter = new BooleanColumnDataFilter();

            e.Layout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            e.Layout.Bands[0].Columns["CompteCree"].Header.Caption = "Créer compte";
            e.Layout.Bands[0].Columns["NCompte"].Header.Caption = "Compte";
            e.Layout.Bands[0].Columns["NCompte"].MaxLength = 50;
            e.Layout.Bands[0].Columns["NCompte"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["NCompte"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["CodeParticulier"].Header.Caption = "Nom";
            e.Layout.Bands[0].Columns["CodeParticulier"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Nom"].Header.Caption = "Nom pour Adresse";
            e.Layout.Bands[0].Columns["Nom"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CodeQualif_QUA"].Header.Caption = "CodeQualif";
            e.Layout.Bands[0].Columns["CodeQualif_QUA"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["TelPortable_IMP"].Header.Caption = "TelPortable";
            e.Layout.Bands[0].Columns["TelPortable_IMP"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Note_IMP"].Header.Caption = "Note";
            e.Layout.Bands[0].Columns["Note_IMP"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Qualification"].CellActivation = Activation.NoEdit;
            this.ssGridCopro.DisplayLayout.Bands[0].Columns["CodeQualif_QUA"].ValueList = this.ssDropFacturation;
            e.Layout.Bands[0].Columns["Adresse"].MaxLength = 50;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CodeBE"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            e.Layout.Bands[0].Columns["libelle"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["libelle"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["adresse"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["adresse"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["CP"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CP"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["ville"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["ville"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["contact"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["contact"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Tel"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Tel"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["Fax"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["Fax"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["eMail"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["eMail"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["eMail"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["eMail"].CellActivation = Activation.NoEdit;

            fc_DropBE();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["IMRE_Noauto"].Hidden = true;
            e.Layout.Bands[0].Columns["Codeimmeuble"].Hidden = true;
            e.Layout.Bands[0].Columns["IMRE_Date"].Header.Caption = "Date";
            e.Layout.Bands[0].Columns["IMRE_Contact"].Header.Caption = "Contact";
            e.Layout.Bands[0].Columns["IMRE_Contact"].MaxLength = 50;
            e.Layout.Bands[0].Columns["IMRE_Tel"].Header.Caption = "Tel";
            e.Layout.Bands[0].Columns["IMRE_Tel"].MaxLength = 50;
            e.Layout.Bands[0].Columns["IMRE_Fax"].Header.Caption = "Fax";
            e.Layout.Bands[0].Columns["IMRE_Fax"].MaxLength = 50;
            e.Layout.Bands[0].Columns["IMRE_Mail"].Header.Caption = "Email";
            e.Layout.Bands[0].Columns["IMRE_Mail"].MaxLength = 100;
            e.Layout.Bands[0].Columns["IMRE_NoFacture"].Header.Caption = "NoFacture";
            e.Layout.Bands[0].Columns["IMRE_NoFacture"].MaxLength = 50;
            e.Layout.Bands[0].Columns["IMRE_Montant"].Header.Caption = "Montant";
            e.Layout.Bands[0].Columns["IMRE_Commentaire"].Header.Caption = "Commentaire";
            e.Layout.Bands[0].Columns["IMRE_Commentaire"].MaxLength = 200;
            e.Layout.Bands[0].Columns["Matricule"].ValueList = SSOleDBDropDown1;
            e.Layout.Bands[0].Columns["Matricule"].MaxLength = 50;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["NoIntervention"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeImmeuble"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CodeImmeuble"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["LibelleCodeEtat"].Hidden = true;
            //e.Layout.Bands[0].Columns["Commentaire"].Hidden = true;
            e.Layout.Bands[0].Columns["Duree"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeDepanneur"].Header.Caption = "Depanneur";
            e.Layout.Bands[0].Columns["CSecteur"].Header.Caption = "Chef de Secteur";
            e.Layout.Bands[0].Columns["Wave"].Header.Caption = "Rapport";
            e.Layout.Bands[0].Columns["Wave"].Style = ColumnStyle.Button;
            e.Layout.Bands[0].Columns["Wave"].CellButtonAppearance.Image = Properties.Resources.volume_up_4_16;
            e.Layout.Bands[0].Columns["Wave"].CellAppearance.Image = Properties.Resources.volume_up_4_16;

            //===> Mondir le 07.05.2021 https://groupe-dt.mantishub.io/view.php?id=2284#c5970
            e.Layout.Bands[0].Columns["CodeEtat"].CellActivation = Activation.NoEdit;
            //===> Fin Modif Mondir

            fc_DropCodeetat();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDevis_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["LibelleCodeEtat"].Hidden = true;
            e.Layout.Bands[0].Columns["Nom"].Hidden = true;
            e.Layout.Bands[0].Columns["DateCreation"].Header.Caption = "Date de Création";
            e.Layout.Bands[0].Columns["CodeImmeuble"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CodeImmeuble"].CellActivation = Activation.NoEdit;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["PDAB_Noauto"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeUO"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeNumOrdre"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeChaufferie"].Hidden = true;
            e.Layout.Bands[0].Columns["Niveau_Mini"].Hidden = true;
            e.Layout.Bands[0].Columns["OrigineP1"].Hidden = true;
            e.Layout.Bands[0].Columns["typeLibelle"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["typeLibelle"].CellActivation = Activation.NoEdit;
            e.Layout.Bands[0].Columns["typeLibelle"].CellAppearance.ForeColor = Color.Blue;
            e.Layout.Bands[0].Columns["Localisation"].Header.Caption = "Nom locatlisation";
            e.Layout.Bands[0].Columns["NumAppareil"].Header.Caption = "N° Appareil";
            e.Layout.Bands[0].Columns["Type_Compteur"].Header.Caption = "Type";
            e.Layout.Bands[0].Columns["Niveau_Maxi"].Header.Caption = "Capacité Maxi";
            e.Layout.Bands[0].Columns["DiametreCuve"].Header.Caption = "Diametre Cuve";
            e.Layout.Bands[0].Columns["TypeLibelle"].Header.Caption = "Libelle du type";
        }
        /// <summary>
        /// Tested
        /// Modifs de la version 29.09.2020 ajoutée par Mondir le 30.09.2020
        /// Modifs de la version 29.09.2020 testée par Mondir le 30.09.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            if (blnAjout == false)
            {
                string sCode = null;

                try
                {
                    if (string.IsNullOrEmpty(txtCode1.Text))
                    {
                        General.Execute("DELETE FROM Immeuble WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "'");
                    }
                    frmResultat.Visible = false;

                    //Mondir : Changed the query to point Immeuble.CodeCommercial
                    //https://groupe-dt.mantishub.io/view.php?id=1704
                    string requete = "SELECT     Immeuble.CodeImmeuble AS \"Code Immeuble\", " + "Immeuble.Adresse AS \"adresse\", Immeuble.Ville AS \"Ville\", " +
                                     " Immeuble.AngleRue AS \"angle de rue\", " + " Immeuble.Code1 AS \"Gerant\", Immeuble.CodeCommercial as \"Mat. commercial\"" +
                                     " FROM         Immeuble";
                    //===> Mondir 30.06.2020 : supprimé la jointure INNER JOIN Table1 ON Immeuble.Code1 = Table1.Code1, modif de la version V29.06.2020
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeuble" };
                    fg.SetValues(new Dictionary<string, string>() { { "CodeImmeuble", txtCodeImmeuble.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        // charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        //fc_ChargeGrilleGestionnaire scode
                        //fc_ChargeGrilleInterve
                        General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                        fg.Dispose(); fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            // charge les enregistrements de l'immeuble
                            sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fc_ChargeEnregistrement(sCode);
                            //fc_ChargeGrilleGestionnaire scode
                            //fc_ChargeGrilleInterve
                            General.saveInReg(Variable.cUserDocImmeuble, "NewVar", sCode);
                            fg.Dispose(); fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                    txtAdresse.Focus();
                    SSTab1.SelectedTab = SSTab1.Tabs[0];


                }
                catch (Exception ee)
                {
                    Erreurs.gFr_debug(ee, this.Name + ";Command1_Click");
                }
                txtCodeImmeuble.Focus();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEcritures_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_ChargeFinance();
            //  fc_ChargeFinance01032006
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            switch (SSTab1.SelectedTab.Index)
            {
                case 7:
                    //Interventions
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                    ImpressionInterventions();
                    break;
                case 8:
                    //Devis
                    cmbDevis_Click(cmbDevis, new System.EventArgs());
                    ImpressionDevis();
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void ImpressionInterventions()
        {

            string sSelect = null;
            string sWhere = null;
            string sOrder = null;
            int nbHeures = 0;

            try
            {
                ReportDocument CR1 = new ReportDocument();
                CR1.Load(General.gsRpt + General.CHEMINETAT);

                sWhere = "";

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.codeimmeuble}='" + txtCodeImmeuble.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " {Intervention.codeimmeuble}='" + txtCodeImmeuble.Text + "'";
                    }
                }

                //    If sWhere = "" Then
                //        sWhere = " {Immeuble.Code1}='" & .txtCode1 & "'"
                //    Else
                //        sWhere = sWhere & " AND {Immeuble.Code1}='" & .txtCode1 & "'"
                //    End If

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.Intervenant}='" + cmbIntervenant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.Intervenant}='" + cmbIntervenant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.CodeEtat}='" + cmbStatus.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.CodeEtat}='" + cmbStatus.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtinterDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }
                if (!string.IsNullOrEmpty(txtIntereAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }
                //    If .txtIntercom <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.Commentaire like '%" & .txtIntercom & "%'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.Commentaire LIKE '%" & .txt(158) & "%'"
                //        End If
                //    End If
                //    If txtNoIntervention <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.NoIntervention = " & .txtNoIntervention & ""
                //        Else
                //            sWhere = sWhere & " and  Intervention.NoIntervention = " & .txtNoIntervention & ""
                //        End If
                //    End If
                //    If txtINT_AnaCode <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.INT_AnaCode LIKE '%" & .txtINT_AnaCode & "'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.INT_AnaCode LIKE '%" & .txtINT_AnaCode & "'"
                //        End If
                //    End If
                //     If ssComboActivite <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " where Intervention.INT_AnaCode LIKE '" & .ssComboActivite & "%'"
                //        Else
                //            sWhere = sWhere & " and  Intervention.INT_AnaCode LIKE '" & .ssComboActivite & "%'"
                //        End If
                //    End If
                if (!string.IsNullOrEmpty(cmbArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " {Intervention.Article}= '" + cmbArticle.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {Intervention.Article}= '" + cmbArticle.Text + "'";
                    }
                }

                //    If .txtDateSaisieDe <> "" Then
                //         If sWhere = "" Then
                //
                //             sWhere = " {Intervention.DateSaisie}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //            sWhere = sWhere & " AND {Intervention.DateSaisie}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //
                //        End If
                //    End If
                //    If .txtDateSaisieFin <> "" Then
                //        If sWhere = "" Then
                //
                //            sWhere = sWhere & " {Intervention.dateSaisie}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //
                //             sWhere = sWhere & " AND {Intervention.dateSaisie}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If
                //    If .txtDatePrevue1 <> "" Then
                //        If sWhere = "" Then
                //            sWhere = sWhere & " {Intervention.DatePrevue}>=dateTime(" & Format(.txtDatePrevue1, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //             sWhere = sWhere & " AND {Intervention.DatePrevue}>=dateTime(" & Format(.txtDatePrevue1, "yyyy,mm,dd") & ",00,00,00)"
                //        End If
                //    End If
                //    If .txtDatePrevue2 <> "" Then
                //        If sWhere = "" Then
                //            sWhere = sWhere & " {Intervention.DatePrevue}<=dateTime(" & Format(.txtDatePrevue2, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //             sWhere = sWhere & " AND {Intervention.DatePrevue}<=dateTime(" & Format(.txtDatePrevue2, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If

                if (!string.IsNullOrEmpty(General.sSQL))
                {
                    //Report.DataDefinition.FormulaFields["Total2"].Text = "187:10:10";
                    //CR1.SetParameterValue("Total2", CalculDesHeures());
                    CR1.DataDefinition.FormulaFields["Total"].Text = "'" + CalculDesHeures() + "'";
                }
                else
                {
                    CR1.DataDefinition.FormulaFields["Total"].Text = "";
                }
                var cc = CR1.DataDefinition.SortFields.Count;
                //+{Intervention.CodeImmeuble}

                //var FieldDef1 = CR1.Database.Tables["Intervention"].Fields["CodeImmeuble"];                            
                //CR1.DataDefinition.SortFields[0].Field = FieldDef1 ;
                //CR1.DataDefinition.SortFields[0].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;

                ////+{Intervention.DateSaisie}
                //var FieldDef2 = CR1.Database.Tables["Intervention"].Fields["DateSaisie"];
                //CR1.DataDefinition.SortFields[1].Field = FieldDef2;
                //CR1.DataDefinition.SortFields[1].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;

                ////CR1.set_SortFields(2, "+{Intervention.CodeEtat}");
                //var FieldDef3 = CR1.Database.Tables["Intervention"].Fields["CodeEtat"];
                //CR1.DataDefinition.SortFields[2].Field = FieldDef3;
                //CR1.DataDefinition.SortFields[2].SortDirection = CrystalDecisions.Shared.SortDirection.AscendingOrder;


                var ReportForm = new CrystalReportFormView(CR1, sWhere);
                ReportForm.Show();

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " ImpressionInterventions ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void ImpressionDevis()
        {

            string sSelect = null;
            string sWhere = null;
            string sOrder = null;
            int nbHeures = 0;

            ReportDocument Report = new ReportDocument();

            try
            {
                if (string.IsNullOrEmpty(General.strEtatReportDevis))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est prévu pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Dossier.fc_ControleFichier(General.gsRpt + General.strEtatReportDevis, false) == true)
                {
                    Report.Load(General.gsRpt + General.strEtatReportDevis);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est présent pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sWhere = "";
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.codeimmeuble} =\"" + txtCodeImmeuble.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " And {DevisEntete.codeimmeuble}=\"" + txtCodeImmeuble.Text + "\"";
                    }
                }

                if (string.IsNullOrEmpty(sWhere))
                {
                    sWhere = "{Immeuble.code1}=\"" + txtCode1.Text + "\"";
                }
                else
                {
                    sWhere = sWhere + " And {Immeuble.code1}=\"" + txtCode1.Text + "\"";
                }

                if (!string.IsNullOrEmpty(cmbDevisIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeDeviseur}=\"" + cmbDevisIntervenant.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeDeviseur}=\"" + cmbDevisIntervenant.Text + "\"";
                    }
                }


                if (!string.IsNullOrEmpty(cmbDevisStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeEtat}=\"" + cmbDevisStatus.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeEtat}=\"" + cmbDevisStatus.Text + "\"";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(txtDevisDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(txtDevisDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }
                if (!string.IsNullOrEmpty(txtDevisAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(txtDevisAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(txtDevisAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }
                if (!string.IsNullOrEmpty(txt1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.TitreDevis}=like \"*" + txt1.Text + "*" + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.TitreDevis} LIKE \"*" + txt1.Text + "*" + "\"";
                    }
                }
                //    If txtNoIntervention <> "" Then
                //        If sWhere = "" Then
                //            sWhere = "{DevisEntete.NumeroDevis}=""" & .txtNoIntervention & """"
                //        Else
                //            sWhere = sWhere & " AND {DevisEntete.NumeroDevis}=""" & .txtNoIntervention & """"
                //        End If
                //    End If
                if (!string.IsNullOrEmpty(cmdDevisArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeTitre}=\"" + cmdDevisArticle.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeTitre}=\"" + cmdDevisArticle.Text + "\"";
                    }
                }

                //    If .txtDateSaisieDe <> "" Then
                //         If sWhere = "" Then
                //
                //             sWhere = "{DevisEntete.DateAcceptation}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //        Else
                //            sWhere = sWhere & " AND {DevisEntete.DateAcceptation}>=dateTime(" & Format(.txtDateSaisieDe, "yyyy,mm,dd") & ",00,00,00)"
                //
                //        End If
                //    End If
                //    If .txtDateSaisieFin <> "" Then
                //        If sWhere = "" Then
                //
                //            sWhere = sWhere & "{DevisEntete.DateAcceptation}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        Else
                //
                //             sWhere = sWhere & " AND {DevisEntete.DateAcceptation}<=dateTime(" & Format(.txtDateSaisieFin, "yyyy,mm,dd") & ",23,59,59)"
                //        End If
                //    End If

                CrystalReportFormView Form = new CrystalReportFormView(Report, sWhere);

                //CR.SortFields(0) = "+{Intervention.CodeImmeuble}"
                //CR.SortFields(1) = "+{Intervention.DateSaisie}"
                //CR.SortFields(2) = "+{Intervention.CodeEtat}"

                Form.Show();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Impression");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public string CalculDesHeures()
        {
            string functionReturnValue = null;

            int BisLongTemp = 0;
            int LongTemp = 0;
            float curtemp = default(float);
            string str_Renamed = null;
            DataTable rs = default(DataTable);
            SqlDataAdapter SDArs;
            string strDate = null;
            try
            {
                rs = new DataTable();
                SDArs = new SqlDataAdapter(BisStrRequete, General.adocnn);
                SDArs.Fill(rs);
                LongTemp = 0;
                if (rs.Rows.Count == 0)
                {
                    strDate = "00:00:00";
                    rs = null;
                    return functionReturnValue;
                }
                //----Calcul des heures en secondes
                foreach (DataRow rsRow in rs.Rows)
                {
                    if (rsRow["Duree"] != DBNull.Value)
                    {
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Hour * 3600;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Minute * 60;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Second;
                    }
                }
                rs = null;

                //----Calcul du temps en heures
                curtemp = 0;
                curtemp = (float)LongTemp / (float)3600;
                str_Renamed = Convert.ToString(curtemp);
                if (str_Renamed.Contains("."))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                    str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                    strDate = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains("."))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                        str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                        strDate = strDate + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt32(LongTemp).ToString("00");
                        strDate = strDate + str_Renamed;
                    }
                    else
                    {
                        strDate = strDate + "00:00";
                    }
                }
                else if (str_Renamed.Contains(','))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                    str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                    strDate = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains(','))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                        str_Renamed = Convert.ToInt32(str_Renamed).ToString("00");
                        strDate = strDate + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt32(LongTemp).ToString("00");
                        strDate = strDate + str_Renamed;
                    }
                    else
                    {
                        strDate = strDate + "00:00";
                    }
                }
                else
                {
                    strDate = str_Renamed + ":00:00";
                }
                functionReturnValue = strDate;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";CalculDesHeures");
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            int nbRecAff = 0;
            ReportDocument CR = new ReportDocument();
            DataTable rsSage = default(DataTable);
            string strSqlSage = null;
            SqlConnection adoConnect = default(SqlConnection);
            string BaseClientSQL = null;
            string BaseClientSage = null;
            string ServeurClient = null;
            string stemp = null;

            string DateMini = null;
            string DateMaxi = null;

            short JourMaxiMois = 0;
            string sqlrequete = null;
            string sCritere = null;

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Liaison comptabilité inexistante.");
                    return;
                }

                if (OptParticulier.Checked && Check6.CheckState == CheckState.Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas lancer de relevés pour les particuliers si vous avez coché " + "la case intitulée 'Uniquement les contrats résiliés'.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                ///// ==================> Tested
                if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                {
                    sCritere = General.cCrietereFinanceBis;
                }
                else
                {
                    sCritere = "";
                }

                DateMini = txtExercice0.Text;
                DateMaxi = txtExercice1.Text;


                //// ================> Tested
                if (!(General.IsDate(txtArretDate.Text)))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtArretDate.Focus();
                    return;
                }
                //// ====================> Tested
                else if (!(General.IsDate(txtEcheance2.Text)))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtEcheance2.Focus();
                    return;
                }
                adors = new DataTable();
                SablierOnOff(true);
                int xx = General.Execute("DELETE FROM TempFECRITURE");

                SAGE.fc_OpenConnSage();
                /// ================> Tested All
                if ((SAGE.adoSage != null))
                {
                    if (SAGE.adoSage.State == ConnectionState.Closed)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La connection avec la base Sage est fermée :" + "\n" + "Veuillez contacter votre administrateur réseau.", "Connection Sage impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else
                    {
                        BaseClientSage = "[" + SAGE.adoSage.Database + "]";
                        BaseClientSQL = "[" + General.adocnn.Database + "]";
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La connection avec la base Sage n'a pas pu s'établir :" + "\n" + "Veuillez contacter votre administrateur réseau.", "Connection Sage impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                //    Dim I As Long
                //    For I = 0 To adocnn.Properties.Count - 1
                //        Debug.Print I & " : " & adocnn.Properties.Item(I).Name & " : " & adocnn.Properties.Item(I).value
                //    Next I
                //L'indice 70 des propriétés correspond à la valeur "Data Source" sous SQL Server
                ServeurClient = General.adocnn.DataSource;

                adoConnect = new SqlConnection($"SERVER={ServeurClient};DATABASE=master;INTEGRATED SECURITY=TRUE;");

                adoConnect.Open();

                if (OptSyndic.Checked == true)
                {
                    // la condition çi-dessous verifie si un nom de syndic a été saisie
                    // si aucun nom n'est saisie alors la requete renvoie tous les comptes
                    // joint entre la table immeuble et la table F_ECRITUREC
                    // pourvu d'un nom de syndic ou pas.
                    //// ============================> Tested
                    if (string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text) && string.IsNullOrEmpty(txtCommercial.Text))
                    {
                        cmdSsyndic.Enabled = false;
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null)", nbRecAff
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, " + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 )" + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";
                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " + BaseClientSage +
                                    ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                                    ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                    " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }
                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                    Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'"
                                    + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                    + " ) "
                                    + " or "
                                    + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" +
                                    DateMaxi + "'" + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                                    " and (" + BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }
                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir


                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null"
                                + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                + " )"
                                + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        else
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," +
                                         " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse," +
                                         " ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1," +
                                         " ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal," +
                                         " GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink ,GerantAdresse2)" +
                                         " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " +
                                         BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " +
                                         BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " +
                                         BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " +
                                         BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL +
                                         ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL +
                                         ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL +
                                         ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL +
                                         ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL +
                                         ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" +
                                         " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL +
                                         ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL +
                                         ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " +
                                         BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" + " AND " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" + BaseClientSage +
                                         ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " +
                                         " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                         Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'"
                                         + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                         + " ) "
                                         + " or "
                                         + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" +
                                         " AND " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" +
                                         BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis +
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir
                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " +
                                " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null"
                                + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                + " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        Application.DoEvents();

                    }
                    ///=============> Tested
                    else if (string.IsNullOrEmpty(txtSyndic1.Text) && !string.IsNullOrEmpty(txtSyndic2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtSyndic1.Focus();
                        return;
                    }
                    ///=============> Tested
                    else if (!string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtSyndic2.Focus();
                        return;

                    }
                    ///=============> Tested
                    else if (string.IsNullOrEmpty(txtSyndic1.Text) && string.IsNullOrEmpty(txtSyndic2.Text) && (!string.IsNullOrEmpty(txtCommercial.Text) || !string.IsNullOrEmpty(ssCombo14.Text)))
                    {
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "')", nbRecAff
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE" + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, " + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1," + " GerantComment2, EC_SENS, Temp ,EC_No,EC_NoLink,GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" + " AND " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) +
                                         "'";

                            sqlrequete = sqlrequete + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                                    + " ) "
                                                    + " or "
                                                    + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini +
                                                    "'" + " AND " + BaseClientSage +
                                                    ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " +
                                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                                    " and " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null";

                            sqlrequete = sqlrequete + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                //===> Mondir le 02.03.2021, line bellow is commented to fix : https://groupe-dt.mantishub.io/view.php?id=2320
                                //+ " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";
                                //===> Fin Modif Mondir
                                + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();

                        }
                        else
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal," + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink ,GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " + BaseClientSage +
                                    ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" + BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis 
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'";

                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //stemp = stemp & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }

                            if (ssCombo14.Text != "")
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" +
                                        ssCombo14.Text + "'";
                            }

                            stemp = stemp + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                          + " ) " + " or " + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" +
                                          DateMini + "'" + " AND " + BaseClientSage +
                                          ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" + BaseClientSage +
                                          ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }
                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis
                            //===> Fin Modif Mondir

                            + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " +
                            " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null";


                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //stemp = stemp & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }
                            stemp = stemp + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                + " )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            if (ssCombo14.Text != "")
                            {
                                stemp = stemp + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" +
                                        ssCombo14.Text + "'";
                            }


                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }

                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        System.Windows.Forms.Application.DoEvents();

                        //            adocnn.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom,ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, Table1.Commercial, Personnel.Nom, Personnel.Prenom, Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Table1.Code1, Table1.Nom, Table1.Adresse1, Table1.CodePostal, Table1.Ville, Table1.ComGerant1, Table1.ComGerant2, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_PIECE " _
                        //'            & "FROM Table1 RIGHT JOIN ((F_ECRITUREC INNER JOIN Immeuble ON F_ECRITUREC.CT_NUM = Immeuble.NCompte) LEFT JOIN Personnel ON Immeuble.CodeCommercial = Personnel.Matricule) ON Table1.Code1 = Immeuble.Code1 " _
                        //'            & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "')"

                    }
                    else
                    {
                        cmdSsyndic.Enabled = false;
                        /// ==============================> Tested
                        if (optRelEcritureNonLettree.Checked == true)
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre d'écritures non lettrées";

                            //Différence entre les deux : absence du test sur le champ
                            //EC_NoLink (celui-ci devait être égal à 0)

                            //                adoConnect.Execute "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                            //'                & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                            //'                & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                            //'                & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                            //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Table1.Code1 >='" & txtSyndic1 & "' And " & BaseClientSQL & ".dbo.Table1.Code1 <='" & txtSyndic2 & "')", nbRecAff
                            sqlrequete = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE" + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE," + " EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble," + " ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale," + " Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse," + " GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp " + " ,EC_No,EC_NoLink, GerantAdresse2) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.Adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";
                            sqlrequete = sqlrequete + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" +
                                         DateMini + "'" + " AND " +
                                         BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " +
                                         BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_LETTRE = 0 " + " and (" + BaseClientSage +
                                         ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }
                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }

                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }

                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                         Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                         " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" +
                                         " And " + BaseClientSQL + ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'"
                                         + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                         + " ) "
                                         + " or " + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini +
                                         "'" + " AND " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                                         ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                                         " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis;
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                sqlrequete = sqlrequete + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                             General.sCptCLientDouteux + "'";
                            }

                            sqlrequete = sqlrequete + ")";

                            sqlrequete = sqlrequete + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            if (!string.IsNullOrEmpty(ssCombo14.Text))
                            {
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo14.Text + "'";
                            }
                            if (!string.IsNullOrEmpty(txtCommercial.Text))
                            {
                                //sqlrequete = sqlrequete & " And " & BaseClientSQL & ".dbo.Table1.Commercial ='" & txtCommercial & "'"
                                sqlrequete = sqlrequete + " And " + BaseClientSQL + ".dbo.immeuble.CodeCommercial ='" + txtCommercial.Text + "'";
                            }
                            sqlrequete = sqlrequete + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null " + " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" + " And " + BaseClientSQL + ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'"
                                + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                + " )"
                                + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(sqlrequete, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }
                        else
                        /// ======================================> Tested
                        {
                            // TODO : Mondir - This Controle Label12[0] Not Found On VB6 
                            //Label12[0].Text = "Nombre total d'écritures";
                            stemp = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal," + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2," + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, " + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink,GerantAdresse2 ) " + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR," + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, " + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.Adresse2" + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC" + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num = " + BaseClientSQL + ".dbo.Immeuble.NCompte " + " LEFT OUTER JOIN " + BaseClientSQL + ".dbo.Personnel " + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 " + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial " + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";

                            stemp = stemp + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini +
                                    "'" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" +
                                    DateMaxi + "'" + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis 
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }

                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " + " AND " +
                                    BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                                    Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'" +
                                    " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" +
                                    " And " + BaseClientSQL + ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'"
                                    + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                    + " ) "
                                    + " or " + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'" +
                                    " AND " + BaseClientSage +
                                    ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and (" + BaseClientSage +
                                    ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                            //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                            //" and " + BaseClientSage + General.cCrietereFinanceBis 
                            if (!General.sCptCLientDouteux.IsNullOrEmpty())
                            {
                                stemp = stemp + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                        General.sCptCLientDouteux + "'";
                            }
                            stemp = stemp + ")";

                            stemp = stemp + " and " + BaseClientSage + General.cCrietereFinanceBis;
                            //===> Fin Modif Mondir

                            stemp = stemp + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null" + " And " + BaseClientSQL + ".dbo.Table1.Code1 >='" + txtSyndic1.Text + "'" + " And " + BaseClientSQL + ".dbo.Table1.Code1 <='" + txtSyndic2.Text + "'"
                                     + " AND (SansReleveCompte is null or SansReleveCompte = 0)"
                                     + " )"
                                     + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";

                            SqlCommand CMD = new SqlCommand(stemp, adoConnect);
                            if (adoConnect.State != ConnectionState.Open)
                                adoConnect.Open();
                            nbRecAff = CMD.ExecuteNonQuery();
                        }

                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        System.Windows.Forms.Application.DoEvents();

                        //            adocnn.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom,ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, Table1.Commercial, Personnel.Nom, Personnel.Prenom, Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Table1.Code1, Table1.Nom, Table1.Adresse1, Table1.CodePostal, Table1.Ville, Table1.ComGerant1, Table1.ComGerant2, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_PIECE " _
                        //'            & "FROM Table1 RIGHT JOIN ((F_ECRITUREC INNER JOIN Immeuble ON F_ECRITUREC.CT_NUM = Immeuble.NCompte) LEFT JOIN Personnel ON Immeuble.CodeCommercial = Personnel.Matricule) ON Table1.Code1 = Immeuble.Code1 " _
                        //'            & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "'  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null  And Table1.Code1 >='" & txtSyndic1 & "' And Table1.Code1 <='" & txtSyndic2 & "')"
                    }

                    fc_ContratResile();

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    //===> Mondir le 09.10.2020, added this line to fix : https://groupe-dt.mantishub.io/view.php?id=2025
                    SDAadors.Fill(adors);
                    //===> Fin Modif Mondir

                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        //if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString()) && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1900")
                        //{
                        //    adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        //}
                        //else
                        //{
                        //    strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                        //    dateTemp = Convert.ToDateTime(strdatetemp);
                        //    adorsRow["EC_Date"] = dateTemp;
                        //}
                        //=== 08 05 2019 mise en commentaire du bloc ci dessus.


                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                            && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1900"
                            && Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);//Format(CDate(strdatetemp), FormatDateSansHeureSQL)
                            adorsRow["EC_Date"] = dateTemp;

                            if (Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") == "01/01/1900"
                              || Convert.ToDateTime(adorsRow["EC_Echeance"].ToString()).ToString("dd/MM/yyyy") == "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }

                        }


                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);

                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    boolAutorise = false;
                    for (intIndex = 0; intIndex < tabstrCompte.Length; intIndex++)
                    {
                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);
                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }

                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");
                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;

                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");
                    //Affiche les résultats récapitulatifs

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveDeSyndic);
                    cmdSsyndic.Enabled = true;
                    if (cmdSPart.Enabled == true)
                    {
                        cmdSPart.Enabled = false;
                    }
                }
                else if (OptImmeuble.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtImmeuble1.Text) &&
                        string.IsNullOrEmpty(txtImmeuble2.Text))
                    {

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) " _
                        //'            & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                        //'            & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL & ".dbo.Table1.Nom, " _
                        //'            & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE " _
                        //'            & " FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM " _
                        //'            & " Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage  & ".dbo.F_ECRITUREC.EC_ECHEANCE is null )"
                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE "
                               + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT,"
                               + " EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille,"
                               + " ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2,"
                               + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, "
                               + " GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) ";

                        SQL = SQL + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, "
                            + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                            + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink , " + BaseClientSQL + ".dbo.Table1.adresse2";


                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            //'=== propriétaire.
                            SQL = SQL + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC INNER JOIN"
                            + " " + BaseClientSQL + ".dbo.Immeuble ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num ="
                            + " " + BaseClientSQL + ".dbo.Immeuble.NCompte INNER JOIN "
                            + " " + BaseClientSQL + ".dbo.Gestionnaire ON " + BaseClientSQL + ".dbo.Immeuble.Code1 "
                            + " = " + BaseClientSQL + ".dbo.Gestionnaire.Code1 AND "
                            + " " + BaseClientSQL + ".dbo.Immeuble.CodeGestionnaire = " + BaseClientSQL + ".dbo.Gestionnaire.CodeGestinnaire INNER JOIN "
                            + " " + BaseClientSQL + ".dbo.Qualification "
                            + " ON " + BaseClientSQL + ".dbo.Gestionnaire.CodeQualif_Qua = " + BaseClientSQL + ".dbo.Qualification.CodeQualif LEFT OUTER JOIN "
                            + " " + BaseClientSQL + ".dbo.Personnel RIGHT OUTER JOIN "
                            + " " + BaseClientSQL + ".dbo.Table1 "
                            + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                            + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1 ";


                        }
                        else
                        {

                            SQL = SQL + " FROM " + BaseClientSage + ".dbo.F_ECRITUREC"
                               + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble "
                               + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num =" + BaseClientSQL + ".dbo.Immeuble.NCompte "
                               + " LEFT OUTER JOIN "
                               + " " + BaseClientSQL + ".dbo.Personnel "
                               + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 "
                               + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                               + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";
                        }

                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                              + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }

                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo19.Text + "'";
                        }

                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'"
                            + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }

                        }
                        else
                        {
                            SQL = SQL + " ) ";
                        }

                        SQL = SQL + " or "
                                  + " (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >'" + DateMini + "'"
                                  + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                                  + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                                  + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%' ";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo19.Text + "'";
                        }

                        SQL = SQL + "AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null "
                            + " AND (SansReleveCompte is null or SansReleveCompte = 0)";

                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }

                        }
                        else
                        {
                            SQL = SQL + " ) ";
                        }

                        SQL = SQL + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";
                        //'===================================================================
                        //'                            Label12(0).Caption = "Nombre total d'écritures"
                        //'            sqlrequete = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink ) SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                        //'            & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR," & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.table1.Commercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL _
                        //'            & ".dbo.Table1.Nom, " & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE , " & BaseClientSage & ".dbo.F_ECRITUREC.EC_No, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NoLink" _
                        //'            & " FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) " _
                        //'            & " LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 " _
                        //'            & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.JM_Date >='" & DateMini & "' AND " & BaseClientSage & ".dbo.F_ECRITUREC.JM_Date <='" & DateMaxi & "' and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411 % '  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "') or (" & BaseClientSage & ".dbo.F_ECRITUREC.JM_Date >'" & DateMini & "' AND " & BaseClientSage & ".dbo.F_ECRITUREC.JM_Date <='" & DateMaxi & "' and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411 % '  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null)" _
                        //'            & " ORDER BY " & BaseClientSage & ".dbo.F_ECRITUREC.EC_Date "

                        //'===================================================================


                        //Debug.Print SQL

                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);
                        Application.DoEvents();

                    }
                    else if (string.IsNullOrEmpty(txtImmeuble1.Text) && !string.IsNullOrEmpty(txtImmeuble2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fouchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtImmeuble1.Focus();
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtImmeuble1.Text) && string.IsNullOrEmpty(txtImmeuble2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fouchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtImmeuble2.Focus();
                        return;
                    }
                    else
                    //// ===================> Tested
                    {

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal, ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2, GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1, GerantComment2, EC_SENS, Temp ) " _
                        //'            & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " _
                        //'            & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.CodePostal, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial, " & BaseClientSQL & ".dbo.Personnel.Nom, " & BaseClientSQL & ".dbo.Personnel.Prenom, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture1, " & BaseClientSQL & ".dbo.Immeuble.CommentaireFacture2, " & BaseClientSQL & ".dbo.Table1.Code1, " & BaseClientSQL & ".dbo.Table1.Nom, " _
                        //'            & BaseClientSQL & ".dbo.Table1.Adresse1, " & BaseClientSQL & ".dbo.Table1.CodePostal, " & BaseClientSQL & ".dbo.Table1.Ville, " & BaseClientSQL & ".dbo.Table1.ComGerant1, " & BaseClientSQL & ".dbo.Table1.ComGerant2, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE " _
                        //'            & " FROM " & BaseClientSQL & ".dbo.Table1 RIGHT JOIN ((" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.Immeuble.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Personnel ON " & BaseClientSQL & ".dbo.Immeuble.CodeCommercial = " & BaseClientSQL & ".dbo.Personnel.Matricule) ON " & BaseClientSQL & ".dbo.Table1.Code1 = " & BaseClientSQL & ".dbo.Immeuble.Code1 WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble >='" & txtImmeuble1 & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble <='" & txtImmeuble2 & "') " _
                        //'            & " or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM " _
                        //'            & " Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble >='" & txtImmeuble1 & "' And " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble <='" & txtImmeuble2 & "')"


                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE "
                           + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, "
                           + " EC_MONTANT, EC_JOUR, JM_DATE, JO_NUM, ImmCodeImmeuble, ImmAdresse, ImmCodePostal,"
                           + " ImmVille, ImmCommerciale, NomCommerciale, Prenom, ImmCommentaire1, ImmCommentaire2,"
                           + " GerantCode1, GerantNom, GerantAdresse, GerantCodePostal, GerantVille, GerantComment1,"
                           + " GerantComment2, EC_SENS, Temp,EC_No,EC_NoLink, GerantAdresse2 ) ";


                        SQL = SQL + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, "
                        + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Table1.Commercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture1, " + BaseClientSQL + ".dbo.Immeuble.CommentaireFacture2, " + BaseClientSQL + ".dbo.Table1.Code1, " + BaseClientSQL + ".dbo.Table1.Nom, "
                        + BaseClientSQL + ".dbo.Table1.Adresse1, " + BaseClientSQL + ".dbo.Table1.CodePostal, " + BaseClientSQL + ".dbo.Table1.Ville, " + BaseClientSQL + ".dbo.Table1.ComGerant1, " + BaseClientSQL + ".dbo.Table1.ComGerant2, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE , " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink, " + BaseClientSQL + ".dbo.Table1.adresse2";


                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            //'=== propriétaire.
                            SQL = SQL + " FROM  " + BaseClientSage + ".dbo.F_ECRITUREC INNER JOIN"
                              + " " + BaseClientSQL + ".dbo.Immeuble ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num ="
                              + " " + BaseClientSQL + ".dbo.Immeuble.NCompte INNER JOIN "
                              + " " + BaseClientSQL + ".dbo.Gestionnaire ON " + BaseClientSQL + ".dbo.Immeuble.Code1 "
                              + " = " + BaseClientSQL + ".dbo.Gestionnaire.Code1 AND "
                              + " " + BaseClientSQL + ".dbo.Immeuble.CodeGestionnaire = " + BaseClientSQL + ".dbo.Gestionnaire.CodeGestinnaire INNER JOIN "
                              + " " + BaseClientSQL + ".dbo.Qualification "
                              + " ON " + BaseClientSQL + ".dbo.Gestionnaire.CodeQualif_Qua = " + BaseClientSQL + ".dbo.Qualification.CodeQualif LEFT OUTER JOIN "
                              + " " + BaseClientSQL + ".dbo.Personnel RIGHT OUTER JOIN "
                              + " " + BaseClientSQL + ".dbo.Table1 "
                              + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                              + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1 ";



                        }
                        else
                        {
                            SQL = SQL + " FROM " + BaseClientSage + ".dbo.F_ECRITUREC"
                                    + " INNER JOIN " + BaseClientSQL + ".dbo.Immeuble "
                                    + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_Num =" + BaseClientSQL + ".dbo.Immeuble.NCompte "
                                    + " LEFT OUTER JOIN "
                                    + " " + BaseClientSQL + ".dbo.Personnel "
                                    + " RIGHT OUTER JOIN " + BaseClientSQL + ".dbo.Table1 "
                                    + " ON " + BaseClientSQL + ".dbo.Personnel.Matricule = " + BaseClientSQL + ".dbo.Table1.commercial "
                                    + " ON " + BaseClientSQL + ".dbo.Immeuble.Code1 = " + BaseClientSQL + ".dbo.Table1.Code1";
                        }


                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                              + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo19.Text + "'";
                        }



                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" + Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "'"
                                    + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble >='" + txtImmeuble1.Text + "'"
                                    + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble <='" + txtImmeuble2.Text + "'"
                                    + " AND (SansReleveCompte is null or SansReleveCompte = 0)";



                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }

                        }
                        else
                        {
                            SQL = SQL + " ) ";
                        }


                        SQL = SQL + " or (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'"
                              + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 "
                              + " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%' ";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //+ " and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo19.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo19.Text + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null "
                            + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble >='" + txtImmeuble1.Text + "'"
                            + " And " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble <='" + txtImmeuble2.Text + "'"
                            + " AND (SansReleveCompte is null or SansReleveCompte = 0)";


                        if (General.sReleveCoproPoprietaire == "1")
                        {
                            if (opt1.Checked == false)
                            {
                                //'=== syndic, autres.
                                SQL = SQL + " AND (Qualification.Qualification <> 'PART' or Qualification.Qualification is null))";
                            }
                            else if (Opt2.Checked == false)
                            {
                                //'=== Proprietaire.
                                SQL = SQL + " AND Qualification.Qualification = 'PART' )";
                            }

                        }
                        else
                        {
                            SQL = SQL + " ) ";
                        }
                        SQL = SQL + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";
                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                        Application.DoEvents();

                    }

                    fc_ContratResile();


                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    SDAadors.Fill(adors);

                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        //if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString()) && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900")
                        //{
                        //    adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        //}
                        //else
                        //{
                        //    strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                        //    dateTemp = Convert.ToDateTime(strdatetemp);
                        //    adorsRow["EC_Date"] = dateTemp;
                        //}

                        //===08 05 2019 mise en commentaire du bloc ci dessus.
                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);// Format(CDate(strdatetemp), FormatDateSansHeureSQL)
                            adorsRow["EC_Date"] = dateTemp;

                            if (Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") == "01/01/1900"
                             || Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") == "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }
                        }
                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);

                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();

                    boolAutorise = false;
                    for (intIndex = 0; intIndex <= tabstrCompte.Length - 1; intIndex++)
                    {
                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);

                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }
                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");

                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;
                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveImmeuble);
                }
                else if (OptParticulier.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtParticulier1.Text) && string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        cmdSPart.Enabled = false;

                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)


                        //             SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " _
                        //'                & " " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier, " _
                        //'                & " " & BaseClientSQL & ".dbo.ImmeublePart.Nom, " & BaseClientSQL & ".dbo.ImmeublePart.Adresse, " & BaseClientSQL & ".dbo.ImmeublePart.CodePostal, " & BaseClientSQL & ".dbo.ImmeublePart.Ville, " & BaseClientSQL & ".dbo.ImmeublePart.Batiment, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodePostal" _
                        //'                & " FROM (" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.ImmeublePart ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.ImmeublePart.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSQL & ".dbo.ImmeublePart.CodeImmeuble = " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble" _
                        //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' ) or (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE is null )"

                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE," +
                              " EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse," +
                              " PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, " + " ImmCodePostal, EC_No, EC_NoLink,ImmCommerciale, NomCommerciale, Prenom  )" +
                              " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " +
                              BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + " " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " +
                              BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL +
                              ".dbo.ImmeublePart.CodeParticulier, " + " " + BaseClientSQL + ".dbo.ImmeublePart.Nom, " + BaseClientSQL + ".dbo.ImmeublePart.Adresse, " +
                              BaseClientSQL + ".dbo.ImmeublePart.CodePostal, " + BaseClientSQL + ".dbo.ImmeublePart.Ville, " + BaseClientSQL + ".dbo.ImmeublePart.Batiment, " +
                              BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.Ville, " +
                              BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink," +
                              " " + BaseClientSQL + ".dbo.Immeuble.CodeCommercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom" +
                              " FROM (" + BaseClientSage + ".dbo.F_ECRITUREC " + " INNER JOIN " + BaseClientSQL + ".dbo.ImmeublePart " + " ON " + BaseClientSage +
                              ".dbo.F_ECRITUREC.CT_NUM = " + BaseClientSQL + ".dbo.ImmeublePart.NCompte)" + " LEFT JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " +
                              BaseClientSQL + ".dbo.ImmeublePart.CodeImmeuble = " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble" + " LEFT JOIN " + BaseClientSQL +
                              ".dbo.table1" + " ON " + BaseClientSQL + ".dbo.Immeuble.code1 = " + BaseClientSQL + ".dbo.Table1.Code1" + " LEFT JOIN " + BaseClientSQL +
                              ".dbo.Personnel" + " ON " + BaseClientSQL + ".dbo.table1.commercial =" + BaseClientSQL + ".dbo.Personnel.matricule";

                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" +
                              " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" +
                              DateMaxi + "'" + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo20.Text + "'";
                        }

                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0 " + " AND " +
                              BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                              Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) + "' )" +
                              " or (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" + " AND " +
                              BaseClientSage +
                              ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" + " and " + BaseClientSage +
                              ".dbo.F_ECRITUREC.EC_LETTRE = 0" +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo20.Text + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NOLINK = 0" + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE is null )" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";


                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                        Application.DoEvents();

                    }
                    else if (string.IsNullOrEmpty(txtParticulier1.Text) && !string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtParticulier1.Focus();
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtParticulier1.Text) && string.IsNullOrEmpty(txtParticulier2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour une selection avec fourchette vous devez saisir ces deux champs.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtParticulier2.Focus();
                        return;
                    }
                    else
                    {
                        cmdSPart.Enabled = false;

                        //            adoConnect.Execute "INSERT INTO TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT F_ECRITUREC.CG_NUM, F_ECRITUREC.CT_NUM, F_ECRITUREC.EC_PIECE, F_ECRITUREC.EC_REFPIECE, F_ECRITUREC.EC_INTITULE, F_ECRITUREC.EC_DATE, F_ECRITUREC.EC_ECHEANCE, F_ECRITUREC.EC_MONTANT, F_ECRITUREC.EC_SENS, F_ECRITUREC.EC_JOUR, F_ECRITUREC.JM_DATE, F_ECRITUREC.JO_NUM, ImmeublePart.CodeParticulier, ImmeublePart.Nom, ImmeublePart.Adresse, ImmeublePart.CodePostal, ImmeublePart.Ville, ImmeublePart.Batiment, F_ECRITUREC.EC_PIECE, Immeuble.Adresse, Immeuble.Ville, Immeuble.CodePostal" _
                        //'                & " FROM (F_ECRITUREC INNER JOIN ImmeublePart ON F_ECRITUREC.CT_NUM = ImmeublePart.NCompte) LEFT JOIN Immeuble ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble" _
                        //'                & " WHERE (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' and ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and ImmeublePart.CodeParticulier <='" & txtParticulier2 & "') or (EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 AND F_ECRITUREC.EC_ECHEANCE is null and ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and ImmeublePart.CodeParticulier <='" & txtParticulier2 & "')"


                        //Différence entre les deux : absence du test sur le champ
                        //EC_NoLink (celui-ci devait être égal à 0)

                        //            SQL = "INSERT INTO " & BaseClientSQL & ".dbo.TempFECRITURE ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse, PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, ImmCodePostal )" _
                        //'                & " SELECT " & BaseClientSage & ".F_ECRITUREC.dbo.CG_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_PIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_REFPIECE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_INTITULE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE, " _
                        //'                & " " & BaseClientSage & ".dbo.F_ECRITUREC.EC_MONTANT, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_SENS, " & BaseClientSage & ".dbo.F_ECRITUREC.EC_JOUR, " & BaseClientSage & ".dbo.F_ECRITUREC.JM_DATE, " & BaseClientSage & ".dbo.F_ECRITUREC.JO_NUM, " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier, " _
                        //'                & " " & BaseClientSQL & ".dbo.ImmeublePart.Nom, " & BaseClientSQL & ".dbo.ImmeublePart.Adresse, " & BaseClientSQL & ".dbo.ImmeublePart.CodePostal, " & BaseClientSQL & ".dbo.ImmeublePart.Ville, " & BaseClientSQL & ".dbo.ImmeublePart.Batiment, " & BaseClientSage & ".F_ECRITUREC.dbo.EC_PIECE, " & BaseClientSQL & ".dbo.Immeuble.Adresse, " & BaseClientSQL & ".dbo.Immeuble.Ville, " & BaseClientSQL & ".dbo.Immeuble.CodePostal" _
                        //'                & " FROM (" & BaseClientSage & ".dbo.F_ECRITUREC INNER JOIN " & BaseClientSQL & ".dbo.ImmeublePart ON " & BaseClientSage & ".dbo.F_ECRITUREC.CT_NUM = " & BaseClientSQL & ".dbo.ImmeublePart.NCompte) LEFT JOIN " & BaseClientSQL & ".dbo.Immeuble ON " & BaseClientSQL & ".dbo.ImmeublePart.CodeImmeuble = " & BaseClientSQL & ".dbo.Immeuble.CodeImmeuble" _
                        //'                & " WHERE (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.dbo.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE <='" & Format(txtEcheance2, FormatDateSQL) & "' AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' and " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier <='" & txtParticulier2 & "') " _
                        //'                & " OR (" & BaseClientSage & ".dbo.F_ECRITUREC.EC_LETTRE = 0 and " & BaseClientSage & ".dbo.F_ECRITUREC.CG_NUM  Like '411%'  AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_NOLINK = 0 AND " & BaseClientSage & ".dbo.F_ECRITUREC.EC_ECHEANCE IS NULL AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier >='" & txtParticulier1 & "' AND " & BaseClientSQL & ".dbo.ImmeublePart.CodeParticulier <='" & txtParticulier2 & "')"

                        //== modif rachid du 25 juillet 2005, remplace la requete ci dessus,
                        //== ajout du commercial de l'immeuble.
                        SQL = "INSERT INTO " + BaseClientSQL + ".dbo.TempFECRITURE " + " ( CG_NUM, CT_NUM, EC_PIECE, EC_REFPIECE, EC_INTITULE, EC_DATE, EC_ECHEANCE, " + " EC_MONTANT, EC_SENS, EC_JOUR, JM_DATE, JO_NUM, PartiCode, PartiNom, PartiAdresse," + " PartiCodePostal, PartiVille, PartiBatiment, Temp, ImmAdresse, ImmVille, " + " ImmCodePostal ,EC_No,EC_Nolink ,ImmCommerciale, NomCommerciale, Prenom )" + " SELECT " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_REFPIECE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_INTITULE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE, " + " " + BaseClientSage + ".dbo.F_ECRITUREC.EC_MONTANT, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_SENS, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_JOUR, " + BaseClientSage + ".dbo.F_ECRITUREC.JM_DATE, " + BaseClientSage + ".dbo.F_ECRITUREC.JO_NUM, " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier, " + " " + BaseClientSQL + ".dbo.ImmeublePart.Nom, " + BaseClientSQL + ".dbo.ImmeublePart.Adresse, " + BaseClientSQL + ".dbo.ImmeublePart.CodePostal, " + BaseClientSQL + ".dbo.ImmeublePart.Ville, " + BaseClientSQL + ".dbo.ImmeublePart.Batiment, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_PIECE, " + BaseClientSQL + ".dbo.Immeuble.Adresse, " + BaseClientSQL + ".dbo.Immeuble.Ville, " + BaseClientSQL + ".dbo.Immeuble.CodePostal, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_No, " + BaseClientSage + ".dbo.F_ECRITUREC.EC_NoLink," + " " + BaseClientSQL + ".dbo.Immeuble.CodeCommercial, " + BaseClientSQL + ".dbo.Personnel.Nom, " + BaseClientSQL + ".dbo.Personnel.Prenom" + " FROM (" + BaseClientSage + ".dbo.F_ECRITUREC " + " INNER JOIN " + BaseClientSQL + ".dbo.ImmeublePart " + " ON " + BaseClientSage + ".dbo.F_ECRITUREC.CT_NUM = " + BaseClientSQL + ".dbo.ImmeublePart.NCompte)" + " LEFT JOIN " + BaseClientSQL + ".dbo.Immeuble " + " ON " + BaseClientSQL + ".dbo.ImmeublePart.CodeImmeuble = " + BaseClientSQL + ".dbo.Immeuble.CodeImmeuble" + " LEFT JOIN " + BaseClientSQL + ".dbo.table1" + " ON " + BaseClientSQL + ".dbo.Immeuble.code1 = " + BaseClientSQL + ".dbo.Table1.Code1" + " LEFT JOIN " + BaseClientSQL + ".dbo.Personnel" + " ON " + BaseClientSQL + ".dbo.table1.commercial =" + BaseClientSQL + ".dbo.Personnel.matricule";
                        SQL = SQL + " WHERE (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" +
                              " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" +
                              DateMaxi + "'" + " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }
                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir


                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo20.Text + "'";
                        }


                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE <='" +
                              Convert.ToDateTime(txtEcheance2.Text).ToString(General.FormatDateSQL) +
                              "'" + " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier >='" +
                              txtParticulier1.Text + "'" +
                              " and " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier <='" + txtParticulier2.Text +
                              "') " +
                              " OR (" + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date >='" + DateMini + "'" +
                              " AND " + BaseClientSage + ".dbo.F_ECRITUREC.JM_Date <='" + DateMaxi + "'" +
                              " and " + BaseClientSage + ".dbo.F_ECRITUREC.EC_LETTRE = 0 " +
                              " and (" + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  Like '411%'";

                        //===> Mondir le 18.01.2021 pour ajouter les modifs de la version V12.01.2021
                        //" and " + BaseClientSage + General.cCrietereFinanceBis;
                        if (!General.sCptCLientDouteux.IsNullOrEmpty())
                        {
                            SQL = SQL + " OR " + BaseClientSage + ".dbo.F_ECRITUREC.CG_NUM  = '" +
                                  General.sCptCLientDouteux + "'";
                        }

                        SQL = SQL + ")";

                        SQL = SQL + " and " + BaseClientSage + General.cCrietereFinanceBis;
                        //===> Fin Modif Mondir

                        if (!string.IsNullOrEmpty(ssCombo20.Text))
                        {
                            SQL = SQL + " And " + BaseClientSQL + ".dbo.immeuble.CodeRespExploit ='" + ssCombo20.Text + "'";
                        }
                        SQL = SQL + " AND " + BaseClientSage + ".dbo.F_ECRITUREC.EC_ECHEANCE IS NULL " + " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier >='" + txtParticulier1.Text + "'" + " AND " + BaseClientSQL + ".dbo.ImmeublePart.CodeParticulier <='" + txtParticulier2.Text + "')" + " ORDER BY " + BaseClientSage + ".dbo.F_ECRITUREC.EC_Date ";


                        SqlCommand CMD = new SqlCommand(SQL, adoConnect);
                        if (adoConnect.State != ConnectionState.Open)
                            adoConnect.Open();
                        nbRecAff = CMD.ExecuteNonQuery();
                        txtEcrituresNonLettrees.Text = Convert.ToString(nbRecAff);

                    }

                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("SELECT NoAuto,TempFECRITURE.EC_DATE, TempFECRITURE.EC_ECHEANCE, TempFECRITURE.EC_JOUR, TempFECRITURE.JM_DATE, TempFECRITURE.JO_NUM FROM TempFECRITURE", General.adocnn);
                    SDAadors.Fill(adors);

                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        //if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString()) && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900")
                        //{
                        //    adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        //}
                        //else
                        //{
                        //    strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                        //    dateTemp = Convert.ToDateTime(strdatetemp);
                        //    adorsRow["EC_Date"] = dateTemp;
                        //}
                        //===08 05 2019 mise en commentaire du bloc ci dessus.
                        if (adorsRow["JO_Num"].ToString() == "AN" && General.IsDate(adorsRow["EC_Echeance"].ToString())
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1900"
                            && Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") != "01/01/1753")
                        {
                            adorsRow["EC_Date"] = adorsRow["EC_Echeance"];
                        }
                        else
                        {
                            strdatetemp = adorsRow["EC_Jour"] + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Month + "/" + Convert.ToDateTime(adorsRow["JM_Date"]).Year;
                            dateTemp = Convert.ToDateTime(strdatetemp);//Format(CDate(strdatetemp), FormatDateSansHeureSQL)
                            adorsRow["EC_Date"] = dateTemp;

                            if (Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") == "01/01/1900"
                               || Convert.ToDateTime(adorsRow["EC_Echeance"]).ToString("dd/MM/yyyy") == "01/01/1753")
                            {
                                adorsRow["EC_Echeance"] = dateTemp;
                            }
                        }

                        SCBadors = new SqlCommandBuilder(SDAadors);
                        SDAadors.Update(adors);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();


                    adors = new DataTable();
                    SDAadors = new SqlDataAdapter("Select Distinct CT_NUM from TempFECRITURE ", General.adocnn);
                    SDAadors.Fill(adors);


                    tabstrCompte = new string[intIndex + 1];
                    //cette boucle permet de stocker le champ CT_NUM dans un tableau
                    //afin de fermer la connection pour des questions de
                    //performance.
                    foreach (DataRow adorsRow in adors.Rows)
                    {
                        tabstrCompte[intIndex] = adorsRow["CT_NUM"] + "";
                        intIndex = intIndex + 1;
                        Array.Resize(ref tabstrCompte, intIndex + 1);
                    }
                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    boolAutorise = false;
                    for (intIndex = 0; intIndex <= tabstrCompte.Length - 1; intIndex++)
                    {

                        adors = new DataTable();
                        SDAadors = new SqlDataAdapter("SELECT TempFECRITURE.EC_DATE, TempFECRITURE.CT_NUM FROM TempFECRITURE where CT_NUM ='" + tabstrCompte[intIndex] + "'", General.adocnn);
                        SDAadors.Fill(adors);

                        foreach (DataRow adorsRow in adors.Rows)
                        {
                            if (Convert.ToDateTime(txtArretDate.Text) >= Convert.ToDateTime(adorsRow["EC_Date"]))
                            {
                                boolAutorise = true;
                                break;
                            }
                        }
                        if (boolAutorise == false)
                        {
                            General.Execute("DELETE FROM tempFECRITURE WHERE CT_NUM ='" + tabstrCompte[intIndex] + "'");
                        }
                        boolAutorise = false;
                        adors?.Dispose();
                        SDAadors?.Dispose();
                        SCBadors?.Dispose();
                    }

                    adors?.Dispose();
                    SDAadors?.Dispose();
                    SCBadors?.Dispose();
                    tabstrCompte = null;
                    // convertie les montants positifs en négatif quand EC_Sens = 1
                    General.Execute("UPDATE TempFECRITURE SET TempFECRITURE.EC_MONTANT = TempFECRITURE.EC_MONTANT*(-1) WHERE TempFECRITURE.EC_SENS=1");

                    fc_MAJ_Date();

                    fc_Result();
                    CR.Load(General.strReleveDeviseur);
                    cmdSPart.Enabled = true;
                    if (cmdSsyndic.Enabled == true)
                    {
                        cmdSsyndic.Enabled = false;
                    }
                }


                //Procédure de remplacement de la date de facture anouveau par la date initiale
                //de celle -ci
                ChangeDate();


                if (General.IsNumeric(txtEcrituresNonLettrees.Text))
                {
                    if (Convert.ToInt32(txtEcrituresNonLettrees.Text) > 0)
                    {
                        CrystalReportFormView Form = new CrystalReportFormView(CR, "");
                        Form.Show();
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Compte(s) soldé(s) sur cette période", "Relevé de compte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {
                    CrystalReportFormView Form = new CrystalReportFormView(CR, "");
                    Form.Show();
                }

                SablierOnOff(false);

                SAGE.fc_CloseConnSage();

                if ((adoConnect != null))
                {
                    if (adoConnect.State != ConnectionState.Closed)
                        adoConnect.Close();
                    adoConnect?.Dispose();
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "Impression des relevés (cmdImprimer) :", false);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object ChangeDate()
        {
            object functionReturnValue = null;

            string sqlChange = null;
            string sqlchangeSage = null;
            DataTable rsChange = default(DataTable);
            SqlDataAdapter SDArsChange;
            SqlCommandBuilder SCBrsChange = null;
            DataTable rsChangeSage = default(DataTable);
            SqlDataAdapter SDArsChangeSage = null;
            SqlCommandBuilder SCBrsChangeSage = null;
            bool Match = false;
            int intEC_NoLink = 0;
            int intEC_No = 0;
            int i = 0;

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    return functionReturnValue;
                    //MsgBox "Liaison comptabilité inexistante."
                }

                SAGE.fc_OpenConnSage();

                rsChange = new DataTable();
                rsChangeSage = new DataTable();

                sqlChange = "SELECT NoAuto,EC_No,EC_NoLink,EC_Date FROM TempFEcriture WHERE Ec_NoLink <> 0";

                rsChange = new DataTable();
                SDArsChange = new SqlDataAdapter(sqlChange, General.adocnn);
                SDArsChange.Fill(rsChange);
                Match = false;
                i = 0;
                if (rsChange.Rows.Count > 0)
                {
                    sqlchangeSage = "SELECT EC_No,EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + rsChange.Rows[0]["EC_NoLink"];
                    foreach (DataRow rsChangeRow in rsChange.Rows)
                    {
                        rsChangeSage = new DataTable();
                        SDArsChangeSage = new SqlDataAdapter(sqlchangeSage, SAGE.adoSage);
                        SDArsChangeSage.Fill(rsChangeSage);
                        if (rsChangeSage.Rows.Count > 0)
                        {
                            intEC_NoLink = Convert.ToInt32(rsChangeRow["EC_NoLink"] + "");
                            intEC_No = Convert.ToInt32(rsChangeRow["EC_No"] + "");
                            while (!(Match == true))
                            {
                                i = i + 1;
                                using (var tmpModAdo = new ModAdo())
                                {
                                    if (!string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT EC_No,EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_NoLink, true, SAGE.adoSage)))
                                    {
                                        intEC_No = intEC_NoLink;
                                        using (var tmpModAdo2 = new ModAdo())
                                            intEC_NoLink = Convert.ToInt32(tmpModAdo2.fc_ADOlibelle("SELECT EC_NoLink,EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_NoLink, false, SAGE.adoSage));
                                        Match = false;
                                    }
                                    else
                                    {
                                        if (i > 1)
                                        {
                                            rsChangeRow["EC_Date"] = tmpModAdo.fc_ADOlibelle("SELECT EC_Date FROM F_ECRITUREC WHERE EC_No=" + intEC_No, false, SAGE.adoSage);
                                            SCBrsChange = new SqlCommandBuilder(SDArsChange);
                                            int x = SDArsChange.Update(rsChange);
                                            Match = true;
                                        }
                                        else
                                        {
                                            Match = true;
                                        }
                                    }
                                }
                            }
                            Match = false;
                            i = 0;
                        }
                        rsChangeSage?.Dispose();
                        SDArsChangeSage?.Dispose();
                        SCBrsChangeSage?.Dispose();
                        if (rsChange.Rows.Count > 0)
                        {
                            sqlchangeSage = "SELECT EC_No,EC_NoLink,EC_Date  FROM F_ECRITUREC WHERE EC_No=" + rsChangeRow["EC_NoLink"];
                        }
                    }

                    rsChange?.Dispose();
                    SDArsChange?.Dispose();
                    SCBrsChange?.Dispose();

                    rsChangeSage?.Dispose();
                    SDArsChangeSage?.Dispose();
                    SCBrsChangeSage?.Dispose();
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " ImpressionRelevés : Fonction ChangeDate ", false);
            }

            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Sablier"></param>
        public void SablierOnOff(bool Sablier)
        {
            /// transforme le pointeur en sabier si fleche et inversement
            Application.DoEvents();
            if (Sablier)
            {
                //FrmMain.MousePointer = 11
                this.Cursor = Cursors.WaitCursor;
            }
            else
            {
                //FrmMain.MousePointer = 1
                this.Cursor = Cursors.Arrow;
            }
            Application.DoEvents();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ContratResile()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdoRs = new ModAdo();
            string[] sTabImm = null;
            string[] sTabImmNonResilie = null;
            int i = 0;
            int X = 0;

            try
            {
                if (Check6.CheckState != CheckState.Checked)
                {
                    return;
                }

                //==== procedure pour ne garder uniquement les contrats résilés.
                sSQL = "SELECT DISTINCT ImmCodeImmeuble" + " FROM         TempFECRITURE ";

                rs = modAdoRs.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    modAdoRs.Dispose();
                    return;
                }

                sTabImm = null;
                i = 0;

                //=== stocke les immeubles dans un tableau.
                foreach (DataRow rsRow in rs.Rows)
                {
                    Array.Resize(ref sTabImm, i + 1);
                    sTabImm[i] = rsRow["ImmCodeImmeuble"].ToString();
                    i = i + 1;
                }

                //=== conrtole si l'immeuble a au moins un contrat, si oui on le stocke
                //=== dans le tableau sTabImmNonResilie pour pouvoir le supprimer.
                X = 0;
                for (i = 0; i < sTabImm.Length; i++)
                {
                    sSQL = "SELECT TOP 1    CodeImmeuble " + " From Contrat" + " WHERE     (Resiliee = 0) AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sTabImm[i]) + "')";
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        Array.Resize(ref sTabImmNonResilie, X + 1);
                        sTabImmNonResilie[X] = sSQL;
                        X = X + 1;
                    }
                    else
                    {
                        //=== modif du 14 11 2011, controle si l'immeuble a au moins un contrat
                        //=== sinon on le supprime.
                        sSQL = "SELECT TOP 1    CodeImmeuble " + " From Contrat" + " WHERE  (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sTabImm[i]) + "')";

                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        if (string.IsNullOrEmpty(sSQL))
                        {
                            Array.Resize(ref sTabImmNonResilie, X + 1);
                            sTabImmNonResilie[X] = sTabImm[i];
                            X = X + 1;
                        }
                    }

                }

                modAdoRs.Dispose();

                //=== supprime les immeubles non résiliés.
                for (X = 0; X < sTabImmNonResilie.Length; X++)
                {

                    sSQL = "DELETE  FROM TempFECRITURE where ImmCodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sTabImmNonResilie[X]) + "'";
                    General.Execute(sSQL);
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ContratResile");
            }
        }
        /// <summary>
        /// Version VB V15.09.2020 ajoutées le 15.09.2020 par Mondir
        /// Tested
        /// </summary>
        private void fc_MAJ_Date()
        {
            string sSQL = null;

            try
            {
                sSQL = "UPDATE    TempFECRITURE" + " Set EC_DATE = FactureManuelleEnTete.DateFacture" + " FROM         FactureManuelleEnTete INNER JOIN" + " TempFECRITURE ON FactureManuelleEnTete.NoFacture = TempFECRITURE.EC_PIECE" + " WHERE     (TempFECRITURE.JO_NUM = N'AN')";

                int xx = General.Execute(sSQL);

                sSQL = "UPDATE    TempFECRITURE" + " Set EC_DATE = FactEnTete.DateFacture" + " FROM         TempFECRITURE INNER JOIN" + " FactEnTete ON TempFECRITURE.EC_PIECE = FactEnTete.NoFacture" + " WHERE     (TempFECRITURE.JO_NUM = N'AN')";

                xx = General.Execute(sSQL);

                sSQL = "UPDATE    TempFECRITURE" + " SET              EC_DATE = FODE_FactFodEnTete.FODE_DatePiece " +
                       " FROM         FODE_FactFodEnTete INNER JOIN " +
                       " TempFECRITURE ON FODE_FactFodEnTete.FODE_Nofacture = TempFECRITURE.EC_PIECE" +
                       //===> Mondir le 17.09.2020 pour ajouter les modifs de la version V15.09.2020
                       " WHERE     (TempFECRITURE.JO_NUM = N'AN') AND FODE_FactFodEnTete.FODE_DatePiece >='" + DateTime.Now.Date.AddYears(-8) + "'";
                //===> Fin Modif Mondir

                xx = General.Execute(sSQL);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_MAJ_Date");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public bool fc_Result()
        {

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("Select Distinct CT_NUM FROM TempFECRITURE ", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtNbComptes.Text = General.nz(adors.Rows.Count, 0).ToString();
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();


            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT SUM(TempFECRITURE.EC_MONTANT) AS [MontantTotal] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantTotal.Text = Convert.ToString(Math.Round(Convert.ToDouble(General.nz(adors.Rows[0]["MontantTotal"], 0)), 2));
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT MIN(TempFECRITURE.EC_MONTANT) AS [MontantMin] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantMoins.Text = General.nz(adors.Rows[0]["MontantMin"], 0).ToString();
                frmResultat.Visible = true;
            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT MAX(TempFECRITURE.EC_MONTANT) AS [MontantMax] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);

            if (adors.Rows.Count > 0)
            {
                txtMontantPlus.Text = General.nz(adors.Rows[0]["MontantMax"], 0).ToString();

            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            adors = new DataTable();
            SDAadors = new SqlDataAdapter("SELECT COUNT(TempFECRITURE.EC_PIECE) AS [TotalPiece] FROM TempFECRITURE", General.adocnn);
            SDAadors.Fill(adors);


            if (adors.Rows.Count > 0)
            {
                txtEcrituresNonLettrees.Text = Convert.ToString(Math.Round(Convert.ToDouble(General.nz(adors.Rows[0]["TotalPiece"], 0)), 2));

            }
            adors?.Dispose();
            SDAadors?.Dispose();
            SCBadors?.Dispose();
            Application.DoEvents();

            return true;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLogimatique_Click(object sender, EventArgs e)
        {
            frmLogimatique frmLogimatique = new frmLogimatique();
            frmLogimatique.txtTypeExport.Text = "0";
            frmLogimatique.ShowDialog();
            frmLogimatique?.Dispose();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            if (txtCodeImmeuble.Text != "")
                fc_sauver();

            fc_Rechercher();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeChefSecteur_Validating(object sender, CancelEventArgs e)
        {
            string sSEC_Code = null;
            DialogResult lMsg;
            if (General.blControle == false && !string.IsNullOrEmpty(txtCodeChefSecteur.Text))
            {
                General.sSQL = "";
                General.sSQL = "SELECT " + " Personnel.Nom ," + " Personnel.prenom , sec_code" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                General.sSQL = General.sSQL + " where Matricule ='" + txtCodeChefSecteur.Text + "'";
                var tmpModAdo = new ModAdo();
                General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    lbllibCodeChefSecteur.Text = General.nz(General.rstmp.Rows[0]["Nom"], "").ToString();
                    sSEC_Code = General.nz(General.rstmp.Rows[0]["SEC_Code"], "").ToString();
                    if (!string.IsNullOrEmpty(txtSEC_Code.Text))
                    {
                        if (txtSEC_Code.Text != sSEC_Code)
                        {
                            lMsg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez changer le secteur, " +
                                                   "voulez vous continuer.", "", MessageBoxButtons.YesNo);
                            if (lMsg == DialogResult.Yes)
                            {
                                txtSEC_Code.Text = sSEC_Code;
                            }

                        }
                    }
                    else
                    {
                        txtSEC_Code.Text = sSEC_Code;
                    }





                }
                else
                {
                    //Cancel = True
                    lbllibCodeChefSecteur.Text = "";

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce chef de secteur n'existe pas");
                }
            }
            return;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSGridSituation_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Date Journal"].Format = "dd/MM/yyyy";
            e.Layout.Bands[0].Columns["Date Facture"].Format = "dd/MM/yyyy";
            e.Layout.Bands[0].Columns["Echéance"].Format = "dd/MM/yyyy";
            e.Layout.Bands[0].Columns["Débit"].Format = "#,##0.00";
            e.Layout.Bands[0].Columns["Crédit"].Format = "#,##0.00";

            foreach (UltraGridColumn col in e.Layout.Bands[0].Columns)
                col.CellActivation = Activation.ActivateOnly;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_AfterRowsDeleted(object sender, EventArgs e)
        {
            // int xx = modAdorsCorr.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCopro_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsCopro.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = modAdorsBE.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsBE.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsIMRE.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIMRE_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = modAdorsIMRE.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowUpdate(object sender, RowEventArgs e)
        {
            // int xx = modAdorsPrest.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowsDeleted(object sender, EventArgs e)
        {
            //int xx = modAdorsPrest.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = modAdorsCompteurs.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsCompteurs.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Codeimmeuble"].Hidden = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = modAdorsMail.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = modAdorsMail.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMailSiteWeb_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (!e.Row.IsAddRow)
                return;
            if (string.IsNullOrEmpty(e.Row.Cells["Codeimmeuble"].Text))
            {
                e.Row.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterCellUpdate(object sender, CellEventArgs e)
        {
            if (ssIntervention.ActiveRow == null)
                return;
            foreach (UltraGridCell Cell in ssIntervention.ActiveRow.Cells)
                if (Cell.DataChanged)
                {
                    //===> Mondir le 16.04.2021, https://groupe-dt.mantishub.io/view.php?id=2389#c5880
                    if (Cell.Column.Key.ToUpper() == "Intervenant".ToUpper())
                    {
                        using (var tmpModAdo = new ModAdo())
                        {
                            var req = $"SELECT Nom FROM Personnel WHERE NonActif = 0 AND Matricule = '{ssIntervention.ActiveRow.Cells["Intervenant"].Text}'";
                            req = tmpModAdo.fc_ADOlibelle(req);
                            if (string.IsNullOrEmpty(req))
                            {
                                //CustomMessageBox.Show("Veuillez vérifier que cet Intervenant existe et qu'il est actif", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                CustomMessageBox.Show("Cet intervenant n'existe pas ou est inactif\nModification annulée", "Modification annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                var interventant = tmpModAdo.fc_ADOlibelle($"SELECT Intervenant FROM Intervention WHERE NoIntervention = '{ssIntervention.ActiveRow.Cells["NoIntervention"].Text}'");
                                ssIntervention.ActiveRow.Cells["Intervenant"].Value = interventant;
                                return;
                            }
                        }
                    }
                    int xx = General.Execute($"UPDATE Intervention SET {Cell.Column.Key} = '{StdSQLchaine.gFr_DoublerQuote(Cell.Value.ToString())}' WHERE NoIntervention = '{ssIntervention.ActiveRow.Cells["NoIntervention"].Value}'");
                }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            string sSQL = null;
            if (SSOleDBGrid1.ActiveRow == null || SSOleDBGrid1.ActiveCell == null)
                return;
            try
            {
                if (SSOleDBGrid1.ActiveCell.Column.Key.ToUpper() == "CodeArticle".ToUpper())
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveCell.Text))
                    {
                        sSQL = "SELECT      Designation1 From FacArticle WHERE CodeArticle = '" + StdSQLchaine.gFr_DoublerQuote(SSOleDBGrid1.ActiveCell.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                        SSOleDBGrid1.ActiveRow.Cells["Designation"].Value = sSQL;
                        //=== modif du 30 09 2015, on bloque le code "PO1"
                        if (string.IsNullOrEmpty(sSQL))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce code article n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;

                        }
                        else if (SSOleDBGrid1.ActiveRow.Cells["Designation"].Text.ToUpper() == "PO1".ToUpper())
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le code article 'PO1'  n'est pas autorisé, veuillez utilser la code 'P01'", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                    }

                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";SSOleDBGrid1_BeforeColUpdate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCompteurs_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            try
            {
                if (GridCompteurs.ActiveRow == null)
                    return;

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["Type_Compteur"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type est obligatoire.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                if (string.IsNullOrEmpty(GridCompteurs.ActiveRow.Cells["PDAB_Noauto"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La localisation est obligatoire.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";GridCompteurs_BeforeUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(e.Row.Cells["CodeArticle"].Value.ToString()))
            {
                string sSQL = "SELECT      Designation1 From FacArticle WHERE CodeArticle = '" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeArticle"].Value.ToString()) + "'";
                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                e.Row.Cells["Designation"].Value = sSQL;
                e.Row.Update();
            }

        }

        private void GridMailSiteWeb_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["adresseMail"].OriginalValue.ToString() != e.Row.Cells["adresseMail"].Text)
            {
                using (var tmpModAdo = new ModAdo())
                {
                    var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ImmeubleMails WHERE Codeimmeuble = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Codeimmeuble"].Text)}' AND adresseMail = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["adresseMail"].Text)}'"));
                    if (inDb > 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Row.CancelUpdate();
                        e.Cancel = true;
                    }
                }
            }
        }

        private void ssGridBE_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["CodeBE"].OriginalValue.ToString() != e.Row.Cells["CodeBE"].Text)
            {
                using (var tmpModAdo = new ModAdo())
                {
                    var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Imm_BE WHERE CodeImmeuble = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeImmeuble"].Text)}' " +
                        $"AND CodeBE = '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeBE"].Text)}'"));
                    if (inDb > 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //e.Row.CancelUpdate();
                        e.Cancel = true;
                        return;
                    }
                }
            }

        }

        private void ssGridComptence_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Intervenant"].Header.Caption = "Matricule";
        }

        private void Label19_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {

            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'"), "").ToString()))
                        {

                            requete = "SELECT Personnel.Matricule as \"Matricule\","
                                + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                                + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\","
                                + " Personnel.NumRadio as \"NumRadio\" "
                                + " FROM Qualification INNER JOIN Personnel"
                                + " ON (Qualification.CodeQualif = Personnel.CodeQualif)";
                            where = "";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, " ORDER BY nom ") { Text = "Recherche d'une intervenant" };

                            if (General.IsNumeric(cmbIntervenant.Text))
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Personnel.Matricule", cmbIntervenant.Text } });
                            }
                            else
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Personnel.Nom", cmbIntervenant.Text } });
                            }

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {

                                    cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_TextChanged(object sender, EventArgs e)
        {
            cmbIntervenant_Validated(cmbIntervenant, new System.EventArgs());
        }

        private void ultraDateTimeEditor3_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null) return;
            txt162.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();

        }

        private void ultraDateTimeEditor3_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null) return;
            txt162.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();

        }

        private void ultraDateTimeEditor4_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null) return;
            txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null) return;
            txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;
            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor1_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;
            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;
            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;
            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor5_ValueChanged(object sender, EventArgs e)
        {

            if (ultraDateTimeEditor5.Value == null) return;
            txtDevisDe.Text = Convert.ToDateTime(ultraDateTimeEditor5.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor5_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor5.Value == null) return;
            txtDevisDe.Text = Convert.ToDateTime(ultraDateTimeEditor5.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor6_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor6.Value == null) return;
            txtDevisAu.Text = Convert.ToDateTime(ultraDateTimeEditor6.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor6_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor6.Value == null) return;
            txtDevisAu.Text = Convert.ToDateTime(ultraDateTimeEditor6.Value).ToShortDateString();
        }

        private void txtDevisDe_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (!string.IsNullOrEmpty(txtDevisDe.Text) && !General.IsDate(txtDevisDe.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txtDevisDe.Focus();
            }
        }

        private void txtDevisAu_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (!string.IsNullOrEmpty(txtDevisAu.Text) && !General.IsDate(txtDevisAu.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txtDevisAu.Focus();
            }
        }

        private void txtDateSaisieFin_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (!string.IsNullOrEmpty(txtDateSaisieFin.Text) && !General.IsDate(txtDateSaisieFin.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txtDateSaisieFin.Focus();
            }

        }

        private void txtDateSaisieDe_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (!string.IsNullOrEmpty(txt162.Text) && !General.IsDate(txt162.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Cancel = true;
                txt162.Focus();
            }
        }

        private void ssGridImmCorrespondant_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            string delete = "delete from IMC_ImmCorrespondant  where CodeCorresp_IMC ="
                + ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Text + " and  CodeImmeuble_IMM='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            int xx = General.Execute(delete);
        }

        private void ssGridImmCorrespondant_AfterExitEditMode(object sender, EventArgs e)
        {
            if (ssGridImmCorrespondant.ActiveRow == null || ssGridImmCorrespondant.ActiveCell == null) return;
            if (ssGridImmCorrespondant.ActiveRow.Cells["Président du conseil syndical"].Value != null && ssGridImmCorrespondant.ActiveRow.Cells["Président du conseil syndical"].Value != DBNull.Value && ssGridImmCorrespondant.ActiveRow.Cells["Président du conseil syndical"].Value.ToString() == "True")
            {
                ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value = "-1";
            }
            else
            {
                ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value = "0";
            }
        }

        private void ssIntervention_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow == null) return;
            var noInter = ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString();
            General.saveInReg(Variable.cUserDocImmeuble, "NoIntervention", noInter);
        }

        private void ssDevis_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssDevis.ActiveRow == null) return;
            var noDevis = ssDevis.ActiveRow.Cells["NumeroDevis"].Value.ToString();
            General.saveInReg(Variable.cUserDocImmeuble, "NumeroDevis", noDevis);
        }

        private void txtCodeImmeuble_Leave(object sender, EventArgs e)
        {
            string sCode = null;
            string sCodeImmeubleA = null;

            if (General.blControle == false)
            {
                // controle si code client Existant
                sCode = txtCodeImmeuble.Text;
                General.sSQL = "SELECT Immeuble.codeImmeuble" + " From Immeuble" + " WHERE Immeuble.codeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                using (var tmpModAdo = new ModAdo())
                {
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        // si enregistrement éxiste déja
                        if (blnAjout == true)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Immeuble déjà existant", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

                        }
                        sCode = General.rstmp.Rows[0]["CodeImmeuble"].ToString();
                        fc_ChargeEnregistrement(sCode);
                        txtCodeImmeuble.Text = sCode;
                    }
                    else if (blnAjout == false && !string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        //MsgBox "Immeuble inéxistant", vbCritical, ""
                        sCodeImmeubleA = txtCodeImmeuble.Text;
                        fc_Clear();
                        txtCodeImmeuble.Text = sCodeImmeubleA;
                    }
                }
            }
        }

        private void UserDocImmeuble_SizeChanged(object sender, EventArgs e)
        {
            //View.Theme.Theme.MainForm.LabelHeader.Text = Height.ToString() + " " + Width.ToString(); ;
        }
        private void ssGridCopro_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {

            string[] notFillableColumns = { "CodeParticulier", "CodeImmeuble", "Qualification" };

            if (!e.Cell.Row.IsAddRow && e.Cell.DataChanged && notFillableColumns.Contains(e.Cell.Column.Key))
                e.Cancel = true;
        }

        private void fc_LibLimiteContrat()
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            bool b24;

            try
            {
                sSQL = "SELECT        Contrat.CodeImmeuble, FacArticle.[24sur24] "
                        + " FROM            Contrat INNER JOIN "
                        + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle "
                        + " WHERE    (Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "')"
                        + " AND  (Contrat.Resiliee = 0) AND (Contrat.nonFacturable = 0)";
                rs = rsAdo.fc_OpenRecordSet(sSQL);
                b24 = false;

                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow r in rs.Rows)
                    {
                        if (Convert.ToInt32(General.nz(r["24sur24"], 0)) == 1)
                        {
                            b24 = true;
                            break;
                        }
                    }
                    if (b24 == true)
                    {
                        if (General.UCase(ssCombo13.Text) == General.UCase(General.sClimMat))
                        {
                            Opt4.Checked = true;
                        }
                        else
                        {
                            Opt3.Checked = true;
                        }
                    }
                    else
                    {
                        if (General.UCase(ssCombo13.Text) == General.UCase(General.sClimMat))
                        {
                            Opt6.Checked = true;
                        }
                        else
                        {
                            //===> Mondir le 30.10.2020, next commented line is a bug
                            //Opt3.Checked = true;
                            Opt5.Checked = true;
                            //===> Fin Modif Mondir
                        }

                    }
                }
                else
                {
                    Opt3.Checked = false;
                    Opt4.Checked = false;
                    Opt5.Checked = false;
                    Opt6.Checked = false;
                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_LibLimiteContrat");
            }
        }

        /// <summary>
        /// Mondir le 02.10.2020, supprimé regarder ssCombo0_BeforeDropDown
        /// Après confirmation de Rachid, le Tag ssCombo22 sert à rien, remttre la vateur 22 dans le Tag de ssCombo22
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCombo22_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                string sSQL;
                if (General.UCase(General.sClimMat) == General.UCase(ssCombo13.Text))
                {
                    sSQL = "SELECT        Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.Qualification"
                     + " FROM            Personnel INNER JOIN "
                     + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif "
                     + " WHERE        (Personnel.CodeQualif = N'13')";
                }
                else
                {
                    sSQL = "SELECT        Personnel.Matricule,Personnel.Nom, Personnel.Prenom,  Qualification.Qualification"
                      + " FROM            Personnel INNER JOIN "
                      + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif "
                      + " WHERE      Personnel.Initiales IN(SELECT        CSecteur "
                      + " From Personnel "
                      + " WHERE    Matricule = '" + ssCombo13.Text + "' )"
                      + " OR "
                      + " Personnel.Initiales IN(SELECT        CSecteur2 "
                      + " From Personnel "
                     + " WHERE    Matricule = '" + ssCombo13.Text + "' )";
                }
                var rsGdpComboAdo = new ModAdo();
                rsGdpCombo = rsGdpComboAdo.fc_OpenRecordSet(sSQL);
                ssCombo22.DataSource = rsGdpCombo;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        //===> Mondir le 13.10.2020 et 14.10.2020, demande de Rachid, Drag & Drop
        //===> Mondir le 20.10.2020 added drag & drop from outlook demande de Rachid
        private TreeNode CopyNode = null;
        private void DirDocClient_DragDrop(object sender, DragEventArgs e)
        {
            try
            {
                if (CopyNode == null || e.Data == null)
                    return;

                new frmCopie(CopyNode.Tag.ToString(), e.Data).ShowDialog();

                fc_OngletDocumention();
            }
            catch
            {

            }
        }

        protected TreeNode FindTreeNode(int x, int y)
        {

            var node = DirDocClient.GetNodeAt(PointToClient(new Point(x, y - 128)));
            return node;

        }

        private void DirDocClient_DragOver(object sender, DragEventArgs e)
        {
            e.Effect = DragDropEffects.Copy;
            TreeNode aNode = FindTreeNode(e.X, e.Y);
            if (aNode != null)
            {
                CopyNode = aNode;
            }
        }

        private void DirDocClient_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                DirDocClient.SelectedNode = e.Node;
        }

        private void collerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                var node = DirDocClient.SelectedNode;
                if (node == null)
                {
                    CustomMessageBox.Show("Veuillez sélectionner un dossier de destination", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                DataObject data_object = (DataObject)Clipboard.GetDataObject();

                //var f = data_object.GetFormats();

                if (data_object == null)
                    return;

                new frmCopie(node.Tag.ToString(), data_object).ShowDialog();

                fc_OngletDocumention();
            }
            catch
            {

            }
        }

        private void buttonPastFiles_Click(object sender, EventArgs e)
        {
            collerToolStripMenuItem_Click(null, null);
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIDA_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (GridIDA.ActiveRow == null || !GridIDA.ActiveRow.DataChanged)
                    return;

                if (string.IsNullOrEmpty(GridIDA.ActiveRow.Cells["CodeImmeuble"].Text))
                {
                    GridIDA.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }

                if (string.IsNullOrEmpty(GridIDA.ActiveRow.Cells["IDA_CreeLe"].Text))
                {
                    GridIDA.ActiveRow.Cells["IDA_CreeLe"].Value = DateTime.Now;
                }

                if (string.IsNullOrEmpty(GridIDA.ActiveRow.Cells["IDA_CreePar"].Text))
                {
                    GridIDA.ActiveRow.Cells["IDA_CreePar"].Value = General.fncUserName();
                }

                GridIDA.ActiveRow.Cells["IDA_MajLe"].Value = DateTime.Now;
                GridIDA.ActiveRow.Cells["IDA_MajPar"].Value = General.fncUserName();
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        private void fc_LoadIDA()
        {
            string sSQL = "";

            try
            {
                sSQL =
                    "SELECT      IDA_ID, Codeimmeuble, IDA_DateAchevement,IDA_CreeLe, IDA_CreePar, IDA_MajLe, IDA_MajPar"
                    + " FROM IDA_ImmDateAchevement "
                    + " WHERE Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'"
                    //===> Mondir le 29.11.2020 pour ajouter les modifs de la version V29.11.2020 ===> Tested
                    //===> Added DESC to the ORDER BY
                    //+ " ORDER BY IDA_DateAchevement ";
                    + " ORDER BY IDA_DateAchevement DESC";
                //===> Fin Modif 29.11.2020

                Cursor = Cursors.WaitCursor;

                rsIDAModAdo = new ModAdo();
                rsIDA = rsIDAModAdo.fc_OpenRecordSet(sSQL, GridIDA, "IDA_ID");

                Cursor = Cursors.Default;

                GridIDA.DataSource = rsIDA;

                GridIDA.DisplayLayout.Bands[0].Columns["IDA_MajLe"].CellActivation = Activation.NoEdit;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_MajPar"].CellActivation = Activation.NoEdit;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_CreeLe"].CellActivation = Activation.NoEdit;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_CreePar"].CellActivation = Activation.NoEdit;

                GridIDA.DisplayLayout.Bands[0].Columns["IDA_MajLe"].CellAppearance.BackColor = Color.LightGray;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_MajPar"].CellAppearance.BackColor = Color.LightGray;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_CreeLe"].CellAppearance.BackColor = Color.LightGray;
                GridIDA.DisplayLayout.Bands[0].Columns["IDA_CreePar"].CellAppearance.BackColor = Color.LightGray;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIDA_AfterRowUpdate(object sender, RowEventArgs e)
        {
            try
            {
                rsIDAModAdo.Update();
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIDA_AfterRowsDeleted(object sender, EventArgs e)
        {
            try
            {
                rsIDAModAdo.Update();
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIDA_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIDA_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["IDA_ID"].Hidden = true;
            e.Layout.Bands[0].Columns["Codeimmeuble"].Hidden = true;

            e.Layout.Bands[0].Columns["IDA_DateAchevement"].Header.Caption = "Date Achevement";
            e.Layout.Bands[0].Columns["IDA_CreeLe"].Header.Caption = "Crée-le";
            e.Layout.Bands[0].Columns["IDA_CreePar"].Header.Caption = "Crée Par";
            e.Layout.Bands[0].Columns["IDA_MajLe"].Header.Caption = "Maj Le";
            e.Layout.Bands[0].Columns["IDA_MajPar"].Header.Caption = "Maj Par";
        }

        /// <summary>
        /// Mondir le 29.11.2020 pour les modifs de la version V29.11.2020 ===> Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check22_CheckedChanged(object sender, EventArgs e)
        {
            var Index = Convert.ToInt32((sender as CheckBox).Tag);
            switch (Index)
            {
                case 22:
                    fc_AdresseEnvoi();
                    break;
            }
        }

        /// <summary>
        /// Mondir le 29.11.2020 pour les modifs de la version V29.11.2020 ===> Tested
        /// </summary>
        private void fc_AdresseEnvoi()
        {
            try
            {
                if (Check22.Checked)
                {
                    txt165.Enabled = true;
                    txt165.BackColor = Color.White;
                    txt166.Enabled = true;
                    txt166.BackColor = Color.White;
                    txt167.Enabled = true;
                    txt167.BackColor = Color.White;
                    txt168.Enabled = true;
                    txt168.BackColor = Color.White;
                    txt169.Enabled = true;
                    txt169.BackColor = Color.White;
                    txt170.Enabled = true;
                    txt170.BackColor = Color.White;
                    txt171.Enabled = true;
                    txt171.BackColor = Color.White;
                    txt172.Enabled = true;
                    txt172.BackColor = Color.White;
                }
                else
                {
                    txt165.Enabled = false;
                    txt165.BackColor = Color.Gray;
                    txt166.Enabled = false;
                    txt166.BackColor = Color.Gray;
                    txt167.Enabled = false;
                    txt167.BackColor = Color.Gray;
                    txt168.Enabled = false;
                    txt168.BackColor = Color.Gray;
                    txt169.Enabled = false;
                    txt169.BackColor = Color.Gray;
                    txt170.Enabled = false;
                    txt170.BackColor = Color.Gray;
                    txt171.Enabled = false;
                    txt171.BackColor = Color.Gray;
                    txt172.Enabled = false;
                    txt172.BackColor = Color.Gray;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void ssCombo_23_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CAT_Code"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["CAT_Libelle"].Header.Caption = "Libelle";
        }

        /// <summary>
        /// Mondir le 09.03.2021, https://groupe-dt.mantishub.io/view.php?id=2344
        /// </summary>
        private void LoadDep1()
        {
            DataTable rsDep2 = default(DataTable);
            var modAdorsDep2 = new ModAdo();

            var rsDep = new DataTable();
            var modAdorsDep = new ModAdo();

            rsDep = modAdorsDep.fc_OpenRecordSet("SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif,Personnel.Dep2, Personnel.CSecteur , " +
                                                 "Qualification.CodeQualif, Qualification.Qualification, Personnel.Rondier" +
                                                   $" FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif WHERE Matricule='{SSOleDBCmbDep1.Text}'");

            if (rsDep?.Rows.Count == 0)
                return;

            lblDep1Nom.Text = rsDep.Rows[0]["Nom"] + " " + rsDep.Rows[0]["Prenom"];
            //lblDep1Prenom.Caption = SSOleDBCmbDep1.Columns("Prenom").value
            lblDep1Qualif.Text = rsDep.Rows[0]["Qualification"]?.ToString();

            rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification" +
                " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif" + " = Qualification.CodeQualif WHERE Initiales='" +
            StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["Dep2"]?.ToString()) + "'");

            if (rsDep2.Rows.Count > 0)
            {
                lblMatriculeDep2.Text = rsDep2.Rows[0]["Matricule"] + "";
                lblDep2Nom.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"] + "";
                //lblDep2Prenom.Caption = rsDep2.Rows[0]["Prenom") & ""
                lblDep2Qualif.Text = rsDep2.Rows[0]["Qualification"] + "";
            }
            else
            {
                lblMatriculeDep2.Text = "";
                lblDep2Nom.Text = "";
                // lblDep2Prenom.Caption = ""
                lblDep2Qualif.Text = "";
            }
            modAdorsDep2?.Dispose();

            //== Modif Rachid du 27 decembre 2004.
            modAdorsDep2 = new ModAdo();
            rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification " + " FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " WHERE Initiales='" +
                StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["Rondier"]?.ToString()) + "'");

            if (rsDep2.Rows.Count > 0)
            {
                lblMatriculeRondier.Text = rsDep2.Rows[0]["Matricule"] + "";
                lblRondierNom.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];
                // lblRondierPrenom.Caption = rsDep2.Fields("Prenom") & ""


                lblRondierQualif.Text = rsDep2.Rows[0]["Qualification"] + "";
            }
            else
            {
                lblMatriculeRondier.Text = "";
                lblRondierNom.Text = "";

                // lblRondierPrenom.Caption = ""
                lblRondierQualif.Text = "";
            }
            modAdorsDep2?.Dispose();
            //======================================

            if (SSOleDBCmbDep1.DisplayLayout.Bands[0].Columns.Cast<UltraGridColumn>().Any(ele => ele.Key.ToUpper() == "CSecteur".ToUpper()))
            {
                modAdorsDep2 = new ModAdo();
                rsDep2 = modAdorsDep2.fc_OpenRecordSet(
                    "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule,Qualification.Qualification" +
                    " FROM Personnel INNER JOIN Qualification ON" +
                    " Personnel.CodeQualif = " + " Qualification.CodeQualif WHERE Initiales='" +
                   StdSQLchaine.gFr_DoublerQuote(rsDep.Rows[0]["CSecteur"]?.ToString()) + "'");

                if (rsDep2.Rows.Count > 0)
                {
                    txtCodeChefSecteur.Text = rsDep2.Rows[0]["Matricule"] + "";
                    lbllibCodeChefSecteur.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];

                    //lblPrenomChefSecteur.Caption = rsDep2.Fields("Prenom") & ""

                    lblQualifChefSecteur.Text = rsDep2.Rows[0]["Qualification"] + "";
                }
                else
                {
                    txtCodeChefSecteur.Text = "";
                    lbllibCodeChefSecteur.Text = "";

                    // lblPrenomChefSecteur.Caption = ""


                    lblQualifChefSecteur.Text = "";
                }
                modAdorsDep2.Dispose();
            }

            ///   rsDep2.Open "SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," _
            //& " Qualification.Qualification FROM Personnel INNER JOIN Qualification " _
            //& " ON Personnel.CodeQualif = Qualification.CodeQualif " _
            //& " WHERE Initiales='" & (fc_ADOlibelle("SELECT CRespExploit FROM Personnel WHERE Personnel.Matricule='" & txtCodeChefSecteur.Caption & "'")) & "'", adocnn

            //If UCase(adocnn.DefaultDatabase) <> UCase(cDelostal) Then
            if (General.sVersionImmParComm != "1")
            {
                modAdorsDep2 = new ModAdo();
                using (var tmpModAdo = new ModAdo())
                    rsDep2 = modAdorsDep2.fc_OpenRecordSet("SELECT Personnel.Nom,Personnel.Prenom,Personnel.Matricule," + " Qualification.Qualification FROM Personnel INNER JOIN Qualification " +
                        " ON Personnel.CodeQualif = Qualification.CodeQualif " + " WHERE Initiales='" +
                        (tmpModAdo.fc_ADOlibelle("SELECT CRespExploit FROM Personnel" + " WHERE Personnel.Matricule='" + SSOleDBCmbDep1.Value + "'")) + "'");

                if (rsDep2.Rows.Count > 0)
                {
                    lblMatriculeREspExpl.Text = rsDep2.Rows[0]["Matricule"] + "";
                    lblLibRespExpl.Text = rsDep2.Rows[0]["Nom"] + " " + rsDep2.Rows[0]["Prenom"];

                    // lblPrenomRespExpl.Caption = rsDep2.Fields("Prenom") & ""
                    lblQualifRespExpl.Text = rsDep2.Rows[0]["Qualification"] + "";
                }
                else
                {
                    lblMatriculeREspExpl.Text = "";
                    lblLibRespExpl.Text = "";

                    //  lblPrenomRespExpl.Caption = ""

                    lblQualifRespExpl.Text = "";
                }
                modAdorsDep2.Dispose();
            }

            modAdorsDep2?.Dispose();
        }

        /// <summary>
        /// Mondir le 09.03.2021, https://groupe-dt.mantishub.io/view.php?id=2344
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBCmbDep1_Validated(object sender, EventArgs e)
        {
            LoadDep1();
        }

        /// <summary>
        /// Mondir le 09.03.2021, https://groupe-dt.mantishub.io/view.php?id=2344
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBCmbDep1_TextChanged(object sender, EventArgs e)
        {
            LoadDep1();
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Mondir le 02.04.2021 https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblParticulier_TextChanged(object sender, EventArgs e)
        {
            if (lblParticulier.Text.ToDouble() < 0)
            {
                lblParticulier.BackColor = Color.Red;
            }
            else
            {
                lblParticulier.BackColor = Color.FromArgb(55, 84, 96);
            }
        }

        /// <summary>
        /// Mondir le 16.04.2021 https://groupe-dt.mantishub.io/view.php?id=2189#c5817
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblParticulier_Click(object sender, EventArgs e)
        {
            SSTab1.SelectedTab = SSTab1.Tabs[3];
        }

        /// <summary>
        /// Mondir le 30.04.2021 https://groupe-dt.mantishub.io/view.php?id=2423
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            //===> Mondir le 12.03.2021, demande de Rachid
            using (var tmpModAdo = new ModAdo())
            {
                var sSQL = "SELECT     AUT_Formulaire, AUT_Nom" +
                           " From AUT_Autorisation" +
                           " WHERE  AUT_Nom = '" + General.fncUserName() +
                           "' and    AUT_Objet = 'ExportExcelRecherche'";
                var dt = tmpModAdo.fc_OpenRecordSet(sSQL);
                if (General._ExecutionMode == General.ExecutionMode.Prod && dt.Rows.Count == 0)
                {
                    CustomMessageBox.Show("Navigation annulée, Vous n'avez pas les autorisations nécessaires pour exporter ces données sous excel.", "Export annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            //===> Fin Modif Mondir

            var frm = new frmExportExcel();
            frm.ShowDialog();
            frm?.Dispose();
        }
    }
}
