﻿using Axe_interDT.Shared;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    public partial class frmExportExcel : Form
    {
        DataTable dataTableResult = null;

        public frmExportExcel()
        {
            InitializeComponent();
        }

        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            var requete = @"SELECT DISTINCT Immeuble.CodeImmeuble AS 'Code Immeuble', Immeuble.Adresse AS 'Adresse', Immeuble.Ville AS 'Ville', Immeuble.AngleRue AS 'Angle de rue',
                            Immeuble.Code1 AS 'Gerant', Immeuble.CodeCommercial as 'Mat. commercial', DTANegatif AS 'DTA Négatif', DTAPositif AS 'DTA Positif', DTAPositifSo AS 'DTA Positif SO' FROM Immeuble
                            LEFT JOIN Contrat ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble";

            var where = "";

            if (chkContratActif.Checked)
            {
                if (where.IsNullOrEmpty())
                {
                    where += $" WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'{DateTime.Today.ToString(General.FormatDateSQL)}')))";
                }
                else
                {
                    where += $" AND (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'{DateTime.Today.ToString(General.FormatDateSQL)}')))";
                }
            }

            if (chkDTANégatif.Checked)
            {
                if (where.IsNullOrEmpty())
                {
                    where += " WHERE Immeuble.DTANegatif = '1'";
                }
                else
                {
                    where += " AND Immeuble.DTANegatif = '1'";
                }
            }

            if (chkDTAPositif.Checked)
            {
                if (where.IsNullOrEmpty())
                {
                    where += " WHERE Immeuble.DTAPositif = '1'";
                }
                else
                {
                    where += " AND Immeuble.DTAPositif = '1'";
                }
            }

            if (chkDTAPositfSO.Checked)
            {
                if (where.IsNullOrEmpty())
                {
                    where += " WHERE Immeuble.DTAPositifSo = '1'";
                }
                else
                {
                    where += " AND Immeuble.DTAPositifSo = '1'";
                }
            }

            var modAdo = new ModAdo();
            dataTableResult = modAdo.fc_OpenRecordSet(requete + where);
            ugResultat.DataSource = dataTableResult;
            ugResultat.Text = $"Résultats - {dataTableResult.Rows.Count} Immeubles trouvés";
        }

        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oResizeRange;
            int i = 0;
            int lLigne = 0;
            int j = 0;

            oXL = new Excel.Application();
            oXL.Visible = false;

            // Get a new workbook.
            //oWB = oXL.Workbooks.Add();
            //oSheet = oWB.ActiveSheet;
            var rsExport = dataTableResult;

            if (rsExport == null)
            {
                CmdRechercher_Click(null, null);
                return;
            }

            if (rsExport.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            Cursor = Cursors.WaitCursor;

            progressBar1.Visible = true;
            progressBar1.Maximum = rsExport.Rows.Count;

            //Copier le ficher excel dans le dossier des fichiers excel
            var fileName = "ExportImmeuble.xlsx";
            i = 2;
            while (File.Exists($"{Axe_interDT.View.Theme.Theme.ExcelFolder}\\{fileName}"))
            {
                fileName = $"ExportImmeuble({i}).xlsx";
                i++;
            }

            var fullPath = $"{Axe_interDT.View.Theme.Theme.ExcelFolder}\\{fileName}";

            File.WriteAllBytes(fullPath, Properties.Resources.ExportImmeuble);

            oXL.Workbooks.Open(fullPath);
            oSheet = oXL.ActiveSheet;

            lLigne = 7;

            foreach (DataRow rsExportRows in rsExport.Rows)
            {
                oSheet.Range[$"A{lLigne}", $"F{lLigne}"].Interior.Color = lLigne % 2 != 0 ? System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.White) : System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.FromArgb(242, 242, 242));

                j = 1;
                foreach (DataColumn rsExportCol in rsExport.Columns)
                {
                    var stemp = "";

                    if (rsExportCol.ColumnName.Contains("DTA"))
                    {
                        if (rsExportRows[rsExportCol.ColumnName].ToString() == "1")
                        {
                            stemp = "X";
                        }
                        else
                        {
                            stemp = "";
                        }
                    }
                    else
                    {
                        stemp = rsExportRows[rsExportCol.ColumnName] + "";
                    }

                    oSheet.Cells[lLigne, j + 0].value = stemp;

                    j = j + 1;
                }

                progressBar1.Increment(1);

                lLigne = lLigne + 1;
            }

            progressBar1.Visible = false;
            progressBar1.Value = 0;

            oXL.Visible = true;
            oXL.UserControl = true;
            oRng = null;
            oSheet = null;
            oWB = null;
            oXL = null;

            Cursor = Cursors.Default;
        }

        private void ugResultat_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DTA Négatif"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["DTA Positif"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["DTA Positif SO"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }
    }
}
