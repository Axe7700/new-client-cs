﻿namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    partial class frmCopie
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.labelTitle = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonOui = new System.Windows.Forms.Button();
            this.buttonNon = new System.Windows.Forms.Button();
            this.textBoxTo = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.labelEnCours = new System.Windows.Forms.Label();
            this.textBoxPathEnCours = new System.Windows.Forms.TextBox();
            this.gridPaths = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.gridPaths)).BeginInit();
            this.SuspendLayout();
            // 
            // labelTitle
            // 
            this.labelTitle.AutoSize = true;
            this.labelTitle.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitle.Location = new System.Drawing.Point(12, 9);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(445, 26);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "Voulez-vous copier le(s) fichier(s)/dossier(s) : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 353);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(166, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "Dans le dossier :";
            // 
            // buttonOui
            // 
            this.buttonOui.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonOui.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.buttonOui.FlatAppearance.BorderSize = 0;
            this.buttonOui.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonOui.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.buttonOui.ForeColor = System.Drawing.Color.White;
            this.buttonOui.Location = new System.Drawing.Point(1132, 467);
            this.buttonOui.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.buttonOui.Name = "buttonOui";
            this.buttonOui.Size = new System.Drawing.Size(85, 40);
            this.buttonOui.TabIndex = 6;
            this.buttonOui.Text = "Oui";
            this.buttonOui.UseVisualStyleBackColor = false;
            this.buttonOui.Click += new System.EventHandler(this.buttonOui_Click);
            // 
            // buttonNon
            // 
            this.buttonNon.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonNon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.buttonNon.FlatAppearance.BorderSize = 0;
            this.buttonNon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonNon.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.buttonNon.ForeColor = System.Drawing.Color.White;
            this.buttonNon.Location = new System.Drawing.Point(1223, 467);
            this.buttonNon.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.buttonNon.Name = "buttonNon";
            this.buttonNon.Size = new System.Drawing.Size(85, 40);
            this.buttonNon.TabIndex = 7;
            this.buttonNon.Text = "Non";
            this.buttonNon.UseVisualStyleBackColor = false;
            this.buttonNon.Click += new System.EventHandler(this.buttonNon_Click);
            // 
            // textBoxTo
            // 
            this.textBoxTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxTo.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.textBoxTo.Location = new System.Drawing.Point(17, 382);
            this.textBoxTo.Name = "textBoxTo";
            this.textBoxTo.ReadOnly = true;
            this.textBoxTo.Size = new System.Drawing.Size(1291, 32);
            this.textBoxTo.TabIndex = 9;
            // 
            // progressBar
            // 
            this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.progressBar.Location = new System.Drawing.Point(17, 467);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(1112, 40);
            this.progressBar.TabIndex = 10;
            this.progressBar.Visible = false;
            // 
            // labelEnCours
            // 
            this.labelEnCours.AutoSize = true;
            this.labelEnCours.BackColor = System.Drawing.Color.Transparent;
            this.labelEnCours.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEnCours.Location = new System.Drawing.Point(11, 430);
            this.labelEnCours.Name = "labelEnCours";
            this.labelEnCours.Size = new System.Drawing.Size(263, 26);
            this.labelEnCours.TabIndex = 11;
            this.labelEnCours.Text = "Fichier / Dossier en cours :";
            this.labelEnCours.Visible = false;
            // 
            // textBoxPathEnCours
            // 
            this.textBoxPathEnCours.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxPathEnCours.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.textBoxPathEnCours.Location = new System.Drawing.Point(281, 430);
            this.textBoxPathEnCours.Name = "textBoxPathEnCours";
            this.textBoxPathEnCours.ReadOnly = true;
            this.textBoxPathEnCours.Size = new System.Drawing.Size(848, 32);
            this.textBoxPathEnCours.TabIndex = 12;
            this.textBoxPathEnCours.Visible = false;
            // 
            // gridPaths
            // 
            this.gridPaths.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.gridPaths.DisplayLayout.Appearance = appearance1;
            this.gridPaths.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.gridPaths.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.gridPaths.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridPaths.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.gridPaths.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.gridPaths.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.gridPaths.DisplayLayout.MaxColScrollRegions = 1;
            this.gridPaths.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.gridPaths.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.gridPaths.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.gridPaths.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.gridPaths.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.gridPaths.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.gridPaths.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.gridPaths.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.gridPaths.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.gridPaths.DisplayLayout.Override.CellAppearance = appearance8;
            this.gridPaths.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.gridPaths.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.gridPaths.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.gridPaths.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.gridPaths.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.gridPaths.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.gridPaths.DisplayLayout.Override.RowAppearance = appearance11;
            this.gridPaths.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.gridPaths.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.gridPaths.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.gridPaths.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.gridPaths.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.gridPaths.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.gridPaths.Location = new System.Drawing.Point(16, 38);
            this.gridPaths.Name = "gridPaths";
            this.gridPaths.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.gridPaths.Size = new System.Drawing.Size(1292, 312);
            this.gridPaths.TabIndex = 14;
            this.gridPaths.Text = "ultraGrid1";
            this.gridPaths.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.gridPaths_InitializeLayout);
            this.gridPaths.BeforeCellUpdate += new Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventHandler(this.gridPaths_BeforeCellUpdate);
            // 
            // frmCopie
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1320, 511);
            this.Controls.Add(this.gridPaths);
            this.Controls.Add(this.textBoxPathEnCours);
            this.Controls.Add(this.labelEnCours);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.textBoxTo);
            this.Controls.Add(this.buttonNon);
            this.Controls.Add(this.buttonOui);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.labelTitle);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1336, 550);
            this.Name = "frmCopie";
            this.Text = "frmCopie";
            ((System.ComponentModel.ISupportInitialize)(this.gridPaths)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button buttonOui;
        public System.Windows.Forms.Button buttonNon;
        private System.Windows.Forms.TextBox textBoxTo;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label labelEnCours;
        private System.Windows.Forms.TextBox textBoxPathEnCours;
        private Infragistics.Win.UltraWinGrid.UltraGrid gridPaths;
    }
}