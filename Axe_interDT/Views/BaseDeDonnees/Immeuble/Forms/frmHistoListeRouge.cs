﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    public partial class frmHistoListeRouge : Form
    {
        private DataTable rsListe;
        private ModAdo modAdorsListe;
        /// <summary>
        /// Tested
        /// </summary>
        public frmHistoListeRouge()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadHisto()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     Codeimmeule, DateMaj, MiseAjourPar, ListeRouge, ID" + " From HISTO_ListeRouge " + " WHERE     Codeimmeule = '" +
                    StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'  ORDER BY DateMaj DESC";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsListe = new ModAdo();
                rsListe = modAdorsListe.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridListe.DataSource = rsListe;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_LoadHisto");
            }
        }

        private void frmHistoListeRouge_Activated(object sender, EventArgs e)
        {
           
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridListe_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridListe_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsListe.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridListe_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsListe.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmHistoListeRouge_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadHisto();
        }

        private void GridListe_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["DateMaj"].Header.Caption = "Date de Maj";
            e.Layout.Bands[0].Columns["MiseAjourPar"].Header.Caption = "Mise à jour par";
            e.Layout.Bands[0].Columns["ListeRouge"].Header.Caption = "Liste Rouge";
            e.Layout.Bands[0].Columns["Codeimmeule"].Hidden = true;
            e.Layout.Bands[0].Columns["ID"].Hidden = true;
        }
    }
}
