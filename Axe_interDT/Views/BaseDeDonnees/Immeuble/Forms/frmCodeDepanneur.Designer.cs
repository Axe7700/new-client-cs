﻿namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    partial class frmCodeDepanneur
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.txtAncienDepanneur = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNouveauDepanneur = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdRechDepann = new System.Windows.Forms.Button();
            this.cmdNouveauDepanneur = new System.Windows.Forms.Button();
            this.SSDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.SSDBGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdRemplace = new System.Windows.Forms.Button();
            this.cmdannuler = new System.Windows.Forms.Button();
            this.cmdValider = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.lblTotal2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.txtAncienDepanneur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNouveauDepanneur)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid2)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(17, 48);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(143, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Ancien dépanneur :";
            // 
            // txtAncienDepanneur
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtAncienDepanneur.DisplayLayout.Appearance = appearance1;
            this.txtAncienDepanneur.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtAncienDepanneur.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.txtAncienDepanneur.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtAncienDepanneur.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.txtAncienDepanneur.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtAncienDepanneur.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.txtAncienDepanneur.DisplayLayout.MaxColScrollRegions = 1;
            this.txtAncienDepanneur.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtAncienDepanneur.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtAncienDepanneur.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.txtAncienDepanneur.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtAncienDepanneur.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.txtAncienDepanneur.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtAncienDepanneur.DisplayLayout.Override.CellAppearance = appearance8;
            this.txtAncienDepanneur.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtAncienDepanneur.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.txtAncienDepanneur.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.txtAncienDepanneur.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.txtAncienDepanneur.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtAncienDepanneur.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.txtAncienDepanneur.DisplayLayout.Override.RowAppearance = appearance11;
            this.txtAncienDepanneur.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtAncienDepanneur.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.txtAncienDepanneur.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtAncienDepanneur.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtAncienDepanneur.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtAncienDepanneur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAncienDepanneur.Location = new System.Drawing.Point(169, 48);
            this.txtAncienDepanneur.Name = "txtAncienDepanneur";
            this.txtAncienDepanneur.Size = new System.Drawing.Size(186, 27);
            this.txtAncienDepanneur.TabIndex = 0;
            this.txtAncienDepanneur.AfterCloseUp += new System.EventHandler(this.txtAncienDepanneur_AfterCloseUp);
            this.txtAncienDepanneur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAncienDepanneur_KeyPress);
            // 
            // txtNouveauDepanneur
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtNouveauDepanneur.DisplayLayout.Appearance = appearance13;
            this.txtNouveauDepanneur.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtNouveauDepanneur.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.txtNouveauDepanneur.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtNouveauDepanneur.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.txtNouveauDepanneur.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtNouveauDepanneur.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.txtNouveauDepanneur.DisplayLayout.MaxColScrollRegions = 1;
            this.txtNouveauDepanneur.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNouveauDepanneur.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtNouveauDepanneur.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.txtNouveauDepanneur.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtNouveauDepanneur.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.txtNouveauDepanneur.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtNouveauDepanneur.DisplayLayout.Override.CellAppearance = appearance20;
            this.txtNouveauDepanneur.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtNouveauDepanneur.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.txtNouveauDepanneur.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.txtNouveauDepanneur.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.txtNouveauDepanneur.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtNouveauDepanneur.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.txtNouveauDepanneur.DisplayLayout.Override.RowAppearance = appearance23;
            this.txtNouveauDepanneur.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtNouveauDepanneur.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.txtNouveauDepanneur.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtNouveauDepanneur.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtNouveauDepanneur.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtNouveauDepanneur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNouveauDepanneur.Location = new System.Drawing.Point(169, 76);
            this.txtNouveauDepanneur.Name = "txtNouveauDepanneur";
            this.txtNouveauDepanneur.Size = new System.Drawing.Size(186, 27);
            this.txtNouveauDepanneur.TabIndex = 1;
            this.txtNouveauDepanneur.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNouveauDepanneur_KeyPress);
            this.txtNouveauDepanneur.Leave += new System.EventHandler(this.txtNouveauDepanneur_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(17, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 19);
            this.label1.TabIndex = 504;
            this.label1.Text = "Nouveau dépanneur :";
            // 
            // cmdRechDepann
            // 
            this.cmdRechDepann.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechDepann.FlatAppearance.BorderSize = 0;
            this.cmdRechDepann.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechDepann.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechDepann.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechDepann.Location = new System.Drawing.Point(361, 49);
            this.cmdRechDepann.Name = "cmdRechDepann";
            this.cmdRechDepann.Size = new System.Drawing.Size(25, 20);
            this.cmdRechDepann.TabIndex = 506;
            this.cmdRechDepann.UseVisualStyleBackColor = false;
            this.cmdRechDepann.Visible = false;
            this.cmdRechDepann.Click += new System.EventHandler(this.cmdRechDepann_Click);
            // 
            // cmdNouveauDepanneur
            // 
            this.cmdNouveauDepanneur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdNouveauDepanneur.FlatAppearance.BorderSize = 0;
            this.cmdNouveauDepanneur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdNouveauDepanneur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdNouveauDepanneur.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdNouveauDepanneur.Location = new System.Drawing.Point(361, 77);
            this.cmdNouveauDepanneur.Name = "cmdNouveauDepanneur";
            this.cmdNouveauDepanneur.Size = new System.Drawing.Size(25, 20);
            this.cmdNouveauDepanneur.TabIndex = 507;
            this.cmdNouveauDepanneur.UseVisualStyleBackColor = false;
            this.cmdNouveauDepanneur.Visible = false;
            this.cmdNouveauDepanneur.Click += new System.EventHandler(this.cmdNouveauDepanneur_Click);
            // 
            // SSDBGrid1
            // 
            this.SSDBGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SSDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.SSDBGrid1.DisplayLayout.UseFixedHeaders = true;
            this.SSDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSDBGrid1.Location = new System.Drawing.Point(15, 125);
            this.SSDBGrid1.Name = "SSDBGrid1";
            this.SSDBGrid1.Size = new System.Drawing.Size(237, 439);
            this.SSDBGrid1.TabIndex = 572;
            this.SSDBGrid1.Text = "Immeuble";
            this.SSDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSDBGrid1_InitializeLayout);
            this.SSDBGrid1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSDBGrid1_InitializeRow);
            this.SSDBGrid1.Click += new System.EventHandler(this.SSDBGrid1_Click);
            this.SSDBGrid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSDBGrid1_KeyPress);
            // 
            // SSDBGrid2
            // 
            this.SSDBGrid2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SSDBGrid2.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSDBGrid2.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSDBGrid2.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSDBGrid2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSDBGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSDBGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSDBGrid2.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.SSDBGrid2.DisplayLayout.UseFixedHeaders = true;
            this.SSDBGrid2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSDBGrid2.Location = new System.Drawing.Point(266, 125);
            this.SSDBGrid2.Name = "SSDBGrid2";
            this.SSDBGrid2.Size = new System.Drawing.Size(237, 439);
            this.SSDBGrid2.TabIndex = 573;
            this.SSDBGrid2.Text = "Prestation";
            this.SSDBGrid2.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSDBGrid2_InitializeRow);
            // 
            // cmdRemplace
            // 
            this.cmdRemplace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRemplace.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRemplace.FlatAppearance.BorderSize = 0;
            this.cmdRemplace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRemplace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdRemplace.ForeColor = System.Drawing.Color.White;
            this.cmdRemplace.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdRemplace.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRemplace.Location = new System.Drawing.Point(19, 9);
            this.cmdRemplace.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRemplace.Name = "cmdRemplace";
            this.cmdRemplace.Size = new System.Drawing.Size(144, 35);
            this.cmdRemplace.TabIndex = 574;
            this.cmdRemplace.Text = "     &Remplacer tout";
            this.cmdRemplace.UseVisualStyleBackColor = false;
            this.cmdRemplace.Click += new System.EventHandler(this.cmdRemplace_Click);
            // 
            // cmdannuler
            // 
            this.cmdannuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.cmdannuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdannuler.FlatAppearance.BorderSize = 0;
            this.cmdannuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdannuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdannuler.ForeColor = System.Drawing.Color.White;
            this.cmdannuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdannuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdannuler.Location = new System.Drawing.Point(402, 11);
            this.cmdannuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdannuler.Name = "cmdannuler";
            this.cmdannuler.Size = new System.Drawing.Size(76, 35);
            this.cmdannuler.TabIndex = 575;
            this.cmdannuler.Text = "&Annuler";
            this.cmdannuler.UseVisualStyleBackColor = false;
            this.cmdannuler.Visible = false;
            // 
            // cmdValider
            // 
            this.cmdValider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.cmdValider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValider.FlatAppearance.BorderSize = 0;
            this.cmdValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdValider.ForeColor = System.Drawing.Color.White;
            this.cmdValider.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdValider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdValider.Location = new System.Drawing.Point(402, 50);
            this.cmdValider.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(76, 35);
            this.cmdValider.TabIndex = 576;
            this.cmdValider.Text = "&Valider";
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Visible = false;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(16, 105);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(236, 17);
            this.lblTotal.TabIndex = 577;
            // 
            // lblTotal2
            // 
            this.lblTotal2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal2.Location = new System.Drawing.Point(267, 105);
            this.lblTotal2.Name = "lblTotal2";
            this.lblTotal2.Size = new System.Drawing.Size(236, 17);
            this.lblTotal2.TabIndex = 578;
            // 
            // frmCodeDepanneur
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(515, 576);
            this.Controls.Add(this.lblTotal2);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.cmdValider);
            this.Controls.Add(this.cmdannuler);
            this.Controls.Add(this.cmdRemplace);
            this.Controls.Add(this.SSDBGrid2);
            this.Controls.Add(this.SSDBGrid1);
            this.Controls.Add(this.cmdNouveauDepanneur);
            this.Controls.Add(this.cmdRechDepann);
            this.Controls.Add(this.txtNouveauDepanneur);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAncienDepanneur);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(531, 615);
            this.MinimumSize = new System.Drawing.Size(531, 615);
            this.Name = "frmCodeDepanneur";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Load += new System.EventHandler(this.frmCodeDepanneur_Load);
            ((System.ComponentModel.ISupportInitialize)(this.txtAncienDepanneur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtNouveauDepanneur)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtAncienDepanneur;
        private Infragistics.Win.UltraWinGrid.UltraCombo txtNouveauDepanneur;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdRechDepann;
        public System.Windows.Forms.Button cmdNouveauDepanneur;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSDBGrid1;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSDBGrid2;
        public System.Windows.Forms.Button cmdRemplace;
        public System.Windows.Forms.Button cmdannuler;
        public System.Windows.Forms.Button cmdValider;
        private System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Label lblTotal2;
    }
}