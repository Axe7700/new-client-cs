﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Microsoft.VisualBasic.FileIO;
using iwantedue.Windows.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    public partial class frmCopie : Form
    {
        private string[] pathsFrom;
        private string pathTo;
        private DataTable dataSource = new DataTable();

        public frmCopie()
        {
            InitializeComponent();
        }

        //===> Mondir le 20.10.2020, ajout de copier coller depuis outlook delandé par Rachid
        public frmCopie(string pathTo, IDataObject data) : this()
        {
            string[] pathsFrom = null;
            var tempPath = Path.GetTempPath() + "Axe_interDT\\";

            //If not exists, create
            if (!Directory.Exists(tempPath))
                Directory.CreateDirectory(tempPath);

            //Clear temp folder
            DirectoryInfo di = new DirectoryInfo(tempPath);
            foreach (FileInfo file in di.GetFiles())
            {
                file.Delete();
            }
            foreach (DirectoryInfo dir in di.GetDirectories())
            {
                dir.Delete(true);
            }

            if (data.GetDataPresent(DataFormats.FileDrop, false) == true)
            {
                //if file are copy from a norma path
                pathsFrom = (string[])data.GetData(DataFormats.FileDrop, false);
            }
            else if (data.GetFormats().Contains("FileGroupDescriptor"))
            {
                //If file are comming from outlook, we copy files to local temp file
                //wrap standard IDataObject in OutlookDataObject
                OutlookDataObject dataObject = new OutlookDataObject(data);
                //get the names and data streams of the files dropped
                string[] filenames = (string[])dataObject.GetData("FileGroupDescriptor");
                MemoryStream[] filestreams = (MemoryStream[])dataObject.GetData("FileContents");

                var pathsAfterProcess = new List<string>();

                for (int fileIndex = 0; fileIndex < filenames.Length; fileIndex++)
                {
                    //use the fileindex to get the name and data stream
                    string filename = filenames[fileIndex];
                    //write to temp file
                    filename = tempPath + DateTime.Now.ToString("yyyyMMdd") + "–" + filename;

                    while (File.Exists(filename))
                    {
                        filename = DateTime.Now.ToString("yyyyMMdd") + "–" + (new Random()).Next(1, 100) + "–" + filenames[fileIndex];
                        filename = tempPath + filename;
                    }

                    MemoryStream filestream = filestreams[fileIndex];

                    //save the file stream using its name to the application path
                    FileStream outputStream = File.Create(filename);
                    filestream.WriteTo(outputStream);
                    outputStream.Close();
                    pathsAfterProcess.Add(filename);
                }

                pathsFrom = pathsAfterProcess.ToArray();
            }

            if (pathsFrom == null || pathsFrom.Length == 0)
            {
                CustomMessageBox.Show("Aucun fichier / dossier sélectionné");
                Close();
                return;
            }

            this.pathsFrom = pathsFrom;
            this.pathTo = pathTo;
            progressBar.Maximum = this.pathsFrom.Length;

            //pathsFrom.ToList().ForEach(ele =>
            //{
            //    var type = FileOrFolder(ele) == 1 ? " (Fichier)" : " (Dossier)";
            //    listBoxPaths.Items.Add(ele + type);
            //});


            dataSource.Columns.Add("path");
            dataSource.Columns.Add("name");
            dataSource.Columns.Add("type");

            pathsFrom.ToList().ForEach(ele =>
            {
                var row = dataSource.NewRow();
                row["path"] = ele;
                row["type"] = FileOrFolder(ele) == 1 ? "Fichier" : "Dossier";
                if (row["type"].ToString() == "Fichier")
                    row["name"] = Path.GetFileNameWithoutExtension(ele);
                else
                    row["name"] = new DirectoryInfo(ele).Name;
                dataSource.Rows.Add(row);
            });

            gridPaths.DataSource = dataSource;

            textBoxTo.Text = pathTo;
        }

        private void buttonOui_Click(object sender, EventArgs e)
        {
            gridPaths.UpdateData();

            var index = 0;
            labelEnCours.Visible = true;
            progressBar.Visible = true;
            textBoxPathEnCours.Visible = true;
            dataSource.Rows.Cast<DataRow>().ToList().ForEach(ele =>
            {
                var pathFrom = ele["path"].ToString();
                var newFileName = ele["name"].ToString().TrimStart().TrimEnd();

                if (FileOrFolder(pathFrom) == 1)
                    newFileName += Path.GetExtension(ele["path"].ToString());

                labelEnCours.Text = "Fichier / Dossier en cours : ";
                textBoxPathEnCours.Text = pathFrom;
                progressBar.Value = ++index;

                if (FileOrFolder(pathFrom) == 1)
                {
                    try
                    {
                        FileSystem.CopyFile(pathFrom, pathTo + "\\" + newFileName, UIOption.AllDialogs);
                    }
                    catch
                    {

                    }
                }
                else if (FileOrFolder(pathFrom) == 2)
                {
                    try
                    {
                        FileSystem.CopyDirectory(pathFrom, pathTo + "\\" + newFileName, UIOption.AllDialogs);
                    }
                    catch
                    {

                    }
                }
            });
            Clipboard.Clear();
            Close();
        }

        private int FileOrFolder(string path)
        {
            if (File.Exists(path))
                return 1;
            else if (Directory.Exists(path))
                return 2;
            return 0;
        }

        private void buttonNon_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void gridPaths_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["path"].Header.Caption = "Chemin";
            e.Layout.Bands[0].Columns["type"].Header.Caption = "Type";
            e.Layout.Bands[0].Columns["name"].Header.Caption = "Nom";

            e.Layout.Bands[0].Columns["path"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            e.Layout.Bands[0].Columns["type"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

            e.Layout.Bands[0].Columns["path"].Width = 700;
            e.Layout.Bands[0].Columns["type"].Width = 100;
            e.Layout.Bands[0].Columns["name"].Width = 300;
        }

        private void gridPaths_BeforeCellUpdate(object sender, Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventArgs e)
        {
            if(e.Cell.Column.Key == "name")
            {
                if(string.IsNullOrWhiteSpace(e.Cell.Text))
                {
                    CustomMessageBox.Show("Le nom du fichier / dossier ne peut pas être vide");
                    e.Cancel = true;
                }
            }
        }
    }
}
