﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    public partial class frmGeocoderV2 : Form
    {
        //TODO : Mondir - Map API Require $$$
        string[] tabRepose6 = new string[11];
        string[] tabRepose7 = new string[11];
        public frmGeocoderV2()
        {
            InitializeComponent();
        }

        private void bntEnregistrement_Click(object sender, EventArgs e)
        {
            try
            {
                if (ListAdresse.SelectedIndex == -1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez d'abord cliquer sur une ligne d'adresse pour la sélectionner" + " et appuyer de nouveau sur le bouton appliquer.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                General.sAdresseGPS = ListAdresse.SelectedItems[0].ToString().Trim();

                if (!string.IsNullOrEmpty(General.sAdresseGPS))
                {

                    General.sLongitudeimmeuble = txtLong.Text.Trim();
                    General.sLattitudeImmeuble = txtLat.Text.Trim();

                }

                this.Close();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";bntEnregistrement_Click");
            }
        }

        private void btnQuiter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ChercheCordonnees_Click(object sender, EventArgs e)
        {
            string Adresse = null;
            string Ville = null;
            string CodePostal = null;
            string Pays = null;

            string AdresseComplete = null;

            Adresse = "";
            Ville = "";
            CodePostal = "";
            Pays = "";

            AdresseComplete = "";

            if ((!string.IsNullOrEmpty(TxtAdresse.Text)))
            {
                Adresse = TxtAdresse.Text;
                AdresseComplete = Adresse + ",";
            }

            if ((!string.IsNullOrEmpty(TxtVille.Text)))
            {
                Ville = TxtVille.Text;
                AdresseComplete = AdresseComplete + Ville + ", ";
            }

            if ((!string.IsNullOrEmpty(TxtPays.Text)))
            {
                Pays = TxtPays.Text;
                AdresseComplete = AdresseComplete + Pays + " ";
            }

            if ((!string.IsNullOrEmpty(TxtCodePostal.Text)))
            {
                CodePostal = TxtCodePostal.Text;
                AdresseComplete = AdresseComplete + CodePostal + "";
            }

            getGoogleMapsGeocode(AdresseComplete);

            //fgGetLatAndLongUsingAddress AdresseComplete

            //GoogleMap AdresseComplete  'Adresse, Ville, CodePostal, Pays

            //search Adresse, CodePostal, Ville
        }
        private string getGoogleMapsGeocode(string sAddr)
        {
            //XMLHTTP60 xhrRequest = default(XMLHTTP60);
            string sQuery = "";
            //DOMDocument60 domResponse = default(DOMDocument60);
            //IXMLDOMNode ixnStatus = default(IXMLDOMNode);
            //IXMLDOMNode ixnLng = default(IXMLDOMNode);
            //IXMLDOMNode ixnLat = default(IXMLDOMNode);
            List<string> tabRepose = new List<string>();
            List<string> tabRepose1 = new List<string>();
            List<string> tabRepose2 = new List<string>();
            List<string> tabRepose3 = new List<string>();
            List<string> tabRepose4 = new List<string>();
            List<string> tabRepose5 = new List<string>();
            string Result = "";
            int i = 0;
            int j = 0;
            int T = 0;
            int l = 0;
            int m = 0;
            int n = 0;
            int k = 0;
            int v = 0;
            string ixnLat = "";
            string ixnLng = "";
            try
            {
                sQuery = "https://maps.googleapis.com/maps/api/geocode/xml?sensor=false&key=" + General.sKeyGeocodage + "&address=" +
                         sAddr.Replace(" ", "+");
                WebClient WC = new WebClient();
                string domResponse = WC.DownloadString(sQuery);
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Axe_interDT\xml.xml",
                    domResponse);
                XmlDocument XD = new XmlDocument();
                XD.Load(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\Axe_interDT\xml.xml");
                string Status = XD.GetElementsByTagName("status")[0].InnerText;
                if (Status != "OK")
                {
                    for (i = 0; i < ListAdresse.Items.Count; i++)
                    {
                        ListAdresse.Items.RemoveAt(0);
                        tabRepose6[i] = "";
                        tabRepose7[i] = "";
                    }
                    LabelResultat.Text = "Adresse non disponible ou incomplète, pas de Coordonnées géographiques !";
                    return Result;
                }
                ixnLat =
                    XD.GetElementsByTagName("geometry")[0].ChildNodes.Cast<XmlNode>()
                        .First(ele => ele.Name == "location")
                        .FirstChild.InnerText;
                ixnLng =
                    XD.GetElementsByTagName("geometry")[0].ChildNodes.Cast<XmlNode>()
                        .First(ele => ele.Name == "location")
                        .LastChild.InnerText;
                var Adresse =
                    XD.GetElementsByTagName("formatted_address")[0].InnerText;
                ListAdresse.Items.Clear();
                ListAdresse.Items.Add(Adresse);
                //tabRepose = domResponse.Split(new string[] { ", France" }, StringSplitOptions.None).ToList();
                //for (i = 0; i < ListAdresse.Items.Count; i++)
                //{
                //    ListAdresse.Items.RemoveAt(0);
                //    tabRepose6[i] = "";
                //    tabRepose7[i] = "";
                //}
                //n = 0;
                //for (i = 0; i < tabRepose.Count; i++)
                //{
                //    tabRepose1 = tabRepose[i].Split(new string[] { "country political" }, StringSplitOptions.None).ToList();
                //    for (k = 0; k < tabRepose1.Count; k++)
                //    {
                //        tabRepose2 = tabRepose1[k].Split(new string[] { "street_address" }, StringSplitOptions.None).ToList();
                //        for (l = 0; l < tabRepose2.Count; l++)
                //        {
                //            n++;
                //            tabRepose4.Add("");
                //            tabRepose4[n - 1] = tabRepose2[l];
                //        }
                //    }
                //}
                ////'    ' Adresse
                //if (TxtAdresse.Text != "" || TxtCodePostal.Text != "")
                //{
                //    if (tabRepose4[i].Contains("locality political"))
                //    {
                //        tabRepose3 = tabRepose4[1].Split(new string[] { "locality political" }, StringSplitOptions.None).ToList();
                //        tabRepose3 = tabRepose3[1].Split(new string[] { "administrative_area_level_2" }, StringSplitOptions.None).ToList();
                //        ListAdresse.Items.Add(tabRepose3[0]);
                //    }
                //    else
                //        ListAdresse.Items.Add(tabRepose4[1]);
                //}
                //for (v = 0; v <= tabRepose4.Count - 1 / 2; v++)
                //{
                //    if (v < tabRepose4.Count - 1 / 2)
                //    {
                //        tabRepose3 = tabRepose4[2 * v + 1].Split(new string[] { "locality political" }, StringSplitOptions.None).ToList();
                //        tabRepose3 = tabRepose3[1].Split(new string[] { "administrative_area_level_2" }, StringSplitOptions.None).ToList();
                //        tabRepose3 = tabRepose3[0].Split(new string[] { "administrative_area_level_1" }, StringSplitOptions.None).ToList();
                //        ListAdresse.Items.Add(tabRepose3[0]);
                //    }
                //}
                ////' Coordonnees
                //if ((TxtAdresse.Text != "" && TxtCodePostal.Text != "") ||
                //    (TxtAdresse.Text != "" && TxtCodePostal.Text == ""))
                //{
                //    if (TxtAdresse.Text == "")
                //    {

                //        if (tabRepose4[3].Contains("postal_code"))
                //        {
                //            tabRepose5 = tabRepose4[3].Split(new string[] { "postal_code" }, StringSplitOptions.None).ToList();
                //            tabRepose6[0] = tabRepose5[1].Substring(1, 11);
                //            tabRepose7[0] = tabRepose5[1].Substring(12, 11);
                //        }
                //        else
                //        {
                //            tabRepose6[0] = tabRepose4[2].Substring(1, 11);
                //            tabRepose7[0] = tabRepose4[2].Substring(12, 11);
                //        }

                //    }
                //    else if (TxtAdresse.Text != "")
                //    {
                //        if (tabRepose4[3].Contains("postal_code"))
                //        {
                //            tabRepose5 = tabRepose4[3].Split(new string[] { "postal_code" }, StringSplitOptions.None).ToList();
                //            tabRepose6[0] = tabRepose5[1].Substring(1, 11);
                //            tabRepose7[0] = tabRepose5[1].Substring(12, 11);
                //        }
                //        else
                //        {
                //            tabRepose6[0] = tabRepose4[2].Substring(1, 11);
                //            tabRepose7[0] = tabRepose4[2].Substring(12, 11);
                //        }
                //    }
                //    else
                //    {
                //        if (tabRepose4[2].Contains("postal_code"))
                //        {
                //            tabRepose5 = tabRepose4[2].Split(new string[] { "postal_code" }, StringSplitOptions.None).ToList();
                //            tabRepose6[0] = tabRepose5[1].Substring(1, 11);
                //            tabRepose7[0] = tabRepose5[1].Substring(12, 11);
                //        }
                //        else
                //        {
                //            tabRepose6[0] = tabRepose4[3].Substring(1, 11);
                //            tabRepose7[0] = tabRepose4[3].Substring(12, 11);
                //        }
                //    }
                //}
                //for (v = 1; v <= tabRepose4.Count / 2; v++)
                //{
                //    if (v < tabRepose4.Count / 2)
                //    {
                //        if (tabRepose4[2 * v].Contains("postal_code"))
                //        {
                //            tabRepose5 =
                //                tabRepose4[2 * v].Split(new string[] { "postal_code" }, StringSplitOptions.None).ToList();
                //            tabRepose6[v - 1] = tabRepose5[1].Substring(1, 11);
                //            tabRepose7[v - 1] = tabRepose5[1].Substring(12, 11);
                //        }
                //        else
                //        {
                //            tabRepose6[v - 1] = tabRepose4[2 * v].Substring(1, 11);
                //            tabRepose7[v - 1] = tabRepose4[2 * v].Substring(12, 11);
                //        }
                //    }
                //}
                Result = $"{ixnLng}, {ixnLat}";
                LabelResultat.Text = "ok...";
                txtLat.Text = ixnLat;
                txtLong.Text = ixnLng;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
            return Result;
        }

    }
}
