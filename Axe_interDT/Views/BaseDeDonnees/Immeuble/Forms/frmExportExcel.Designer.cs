﻿
namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    partial class frmExportExcel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkDTANégatif = new System.Windows.Forms.CheckBox();
            this.chkDTAPositif = new System.Windows.Forms.CheckBox();
            this.chkDTAPositfSO = new System.Windows.Forms.CheckBox();
            this.chkContratActif = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.ugResultat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).BeginInit();
            this.SuspendLayout();
            // 
            // chkDTANégatif
            // 
            this.chkDTANégatif.AutoSize = true;
            this.chkDTANégatif.BackColor = System.Drawing.Color.Transparent;
            this.chkDTANégatif.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDTANégatif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkDTANégatif.Location = new System.Drawing.Point(124, 27);
            this.chkDTANégatif.Margin = new System.Windows.Forms.Padding(4, 14, 4, 5);
            this.chkDTANégatif.Name = "chkDTANégatif";
            this.chkDTANégatif.Size = new System.Drawing.Size(106, 22);
            this.chkDTANégatif.TabIndex = 573;
            this.chkDTANégatif.Text = "DTA Négatif";
            this.chkDTANégatif.UseVisualStyleBackColor = false;
            // 
            // chkDTAPositif
            // 
            this.chkDTAPositif.AutoSize = true;
            this.chkDTAPositif.BackColor = System.Drawing.Color.Transparent;
            this.chkDTAPositif.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDTAPositif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkDTAPositif.ForeColor = System.Drawing.Color.Red;
            this.chkDTAPositif.Location = new System.Drawing.Point(238, 27);
            this.chkDTAPositif.Margin = new System.Windows.Forms.Padding(4, 14, 4, 5);
            this.chkDTAPositif.Name = "chkDTAPositif";
            this.chkDTAPositif.Size = new System.Drawing.Size(101, 22);
            this.chkDTAPositif.TabIndex = 574;
            this.chkDTAPositif.Text = "DTA Positif";
            this.chkDTAPositif.UseVisualStyleBackColor = false;
            // 
            // chkDTAPositfSO
            // 
            this.chkDTAPositfSO.AutoSize = true;
            this.chkDTAPositfSO.BackColor = System.Drawing.Color.Transparent;
            this.chkDTAPositfSO.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDTAPositfSO.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkDTAPositfSO.Location = new System.Drawing.Point(347, 27);
            this.chkDTAPositfSO.Margin = new System.Windows.Forms.Padding(4, 14, 4, 5);
            this.chkDTAPositfSO.Name = "chkDTAPositfSO";
            this.chkDTAPositfSO.Size = new System.Drawing.Size(127, 22);
            this.chkDTAPositfSO.TabIndex = 575;
            this.chkDTAPositfSO.Text = "DTA Positif SO";
            this.chkDTAPositfSO.UseVisualStyleBackColor = false;
            // 
            // chkContratActif
            // 
            this.chkContratActif.AutoSize = true;
            this.chkContratActif.BackColor = System.Drawing.Color.Transparent;
            this.chkContratActif.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkContratActif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.chkContratActif.Location = new System.Drawing.Point(8, 27);
            this.chkContratActif.Margin = new System.Windows.Forms.Padding(4, 14, 4, 5);
            this.chkContratActif.Name = "chkContratActif";
            this.chkContratActif.Size = new System.Drawing.Size(108, 22);
            this.chkContratActif.TabIndex = 576;
            this.chkContratActif.Text = "Contrat Actif";
            this.chkContratActif.UseVisualStyleBackColor = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkContratActif);
            this.groupBox1.Controls.Add(this.chkDTAPositfSO);
            this.groupBox1.Controls.Add(this.chkDTANégatif);
            this.groupBox1.Controls.Add(this.chkDTAPositif);
            this.groupBox1.Location = new System.Drawing.Point(13, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(517, 69);
            this.groupBox1.TabIndex = 577;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Paramètre de recherche :";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CmdRechercher);
            this.groupBox2.Controls.Add(this.cmdExportExcel);
            this.groupBox2.Location = new System.Drawing.Point(538, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(251, 69);
            this.groupBox2.TabIndex = 578;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Actions :";
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.CmdRechercher.ForeColor = System.Drawing.Color.White;
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.CmdRechercher.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CmdRechercher.Location = new System.Drawing.Point(6, 26);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(114, 37);
            this.CmdRechercher.TabIndex = 580;
            this.CmdRechercher.Text = "Rechercher";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdExportExcel.ForeColor = System.Drawing.Color.White;
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.Excel_16x16;
            this.cmdExportExcel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExportExcel.Location = new System.Drawing.Point(124, 26);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(114, 37);
            this.cmdExportExcel.TabIndex = 579;
            this.cmdExportExcel.Text = " Export";
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // ugResultat
            // 
            this.ugResultat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ugResultat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugResultat.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ugResultat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugResultat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ugResultat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ugResultat.DisplayLayout.UseFixedHeaders = true;
            this.ugResultat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ugResultat.Location = new System.Drawing.Point(13, 91);
            this.ugResultat.Name = "ugResultat";
            this.ugResultat.Size = new System.Drawing.Size(1159, 588);
            this.ugResultat.TabIndex = 579;
            this.ugResultat.Text = "Résultats ";
            this.ugResultat.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ugResultat_InitializeLayout);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(796, 44);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(376, 23);
            this.progressBar1.TabIndex = 580;
            this.progressBar1.Visible = false;
            // 
            // frmExportExcel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1184, 691);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.ugResultat);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmExportExcel";
            this.Text = "Export Excel";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox chkDTANégatif;
        private System.Windows.Forms.CheckBox chkDTAPositif;
        private System.Windows.Forms.CheckBox chkDTAPositfSO;
        private System.Windows.Forms.CheckBox chkContratActif;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        public System.Windows.Forms.Button cmdExportExcel;
        public System.Windows.Forms.Button CmdRechercher;
        public Infragistics.Win.UltraWinGrid.UltraGrid ugResultat;
        private iTalk.iTalk_ProgressBar progressBar;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}