﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    public partial class frmCodeDepanneur : Form
    {
        int i;
        int j;
        string SQL;
        DataTable rs;
        private ModAdo modAdors;
        DataTable rs2;
        private ModAdo modAdors2;
        object VAR;
        private DataTable Data1;
        private ModAdo modAdoData1;

        public frmCodeDepanneur()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdNouveauDepanneur_Click(object sender, EventArgs e)
        {
            //----lancement fiche de recherche

            try
            {
                string requete = "SELECT  matricule AS \"matricule\",Nom AS \"Nom\",Prenom AS \"Prenom\" from personnel";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles" };
                fg.SetValues(new Dictionary<string, string> { { "matricule", txtNouveauDepanneur.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtNouveauDepanneur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    cmdRemplace.Enabled = true;
                    // cmdannuler.Enabled = True
                    cmdValider.Enabled = true;
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtNouveauDepanneur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        cmdRemplace.Enabled = true;
                        // cmdannuler.Enabled = True
                        cmdValider.Enabled = true;
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex) { Program.SaveException(ex); }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechDepann_Click(object sender, EventArgs e)
        {
            //---Si un changement a été effectué demandé d'abord si il veux valider
            //If boolChangement = True Then
            //    i = MsgBox("Vous avez éffectué des modifications, voulez-vous valider?", vbExclamation + vbYesNo, "Attention")
            //    If i = 6 Then
            //        cmdValider_Click
            //    End If
            //    Exit Sub
            //End If

            //----lancement fiche de recherche

            try
            {
                string requete = "SELECT  matricule AS \"matricule\",Nom AS \"Nom\",Prenom AS \"Prenom\" from personnel";
                string where_order = "";
                SearchTemplate fg =
                    new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles" };
                fg.SetValues(new Dictionary<string, string> { { "matricule", txtAncienDepanneur.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    //.stxt = "SELECT  Immeuble.CodeDepanneur AS [Code Depanneur], Personnel.Nom AS Nom, Personnel.Prenom AS Prenom, Immeuble.CodeImmeuble AS [Code Immeuble] FROM Immeuble LEFT JOIN Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule"
                    txtAncienDepanneur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fournisTable();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        //.stxt = "SELECT  Immeuble.CodeDepanneur AS [Code Depanneur], Personnel.Nom AS Nom, Personnel.Prenom AS Prenom, Immeuble.CodeImmeuble AS [Code Immeuble] FROM Immeuble LEFT JOIN Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule"
                        txtAncienDepanneur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fournisTable();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fournisTable()
        {

            if (!string.IsNullOrEmpty(txtAncienDepanneur.Text))
            {
                //SQL = "SELECT  matricule,Nom,Prenom from personnel where matricule ='" & txtAncienDepanneur & "'"

                // Set rs = gdb.OpenRecordset(SQL, dbOpenSnapshot)
                if (!txtAncienDepanneur.IsItemInList())
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce dépanneur n'éxiste pas", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //If rs.EOF = True Then
                //cmdRechDepann_Click

                //     Exit Sub
                // End If
                SQL = "Select CodeImmeuble,CodeDepanneur, '' AS Change from Immeuble where codedepanneur ='" + txtAncienDepanneur.Text + "' order by CodeImmeuble";
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(SQL);
                //Set rs = gdb.OpenRecordset(SQL, dbOpenSnapshot)

            }
            else if (string.IsNullOrEmpty(txtAncienDepanneur.Text))
            {
                SSDBGrid1.DataSource = null;
                SSDBGrid2.DataSource = null;
                cmdRemplace.Enabled = false;
                //cmdRechDepann_Click
                return;
            }

            if (rs.Rows.Count > 0)
            {
                //SSDBGrid1.DataSource = rs;
                //var TmpDT = rs.Copy();
                //TmpDT.Rows.Clear();
                //SSDBGrid2.DataSource = TmpDT;
                j = rs.Rows.Count;

                fournisGrille(1);
                

                //----Ajoute une colonne temporaire.
                i = SSDBGrid1.DisplayLayout.Bands[0].Columns.Count;
                SSDBGrid1.DisplayLayout.Bands[0].Columns[i - 1].Hidden = true;

                for (i = 0; i <= SSDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                }

                FournisChamps(1);
                if (j == 1)
                {
                    lblTotal.Text = j + " enregistrement";
                }
                else
                {
                    lblTotal.Text = j + " enregistrements";
                }


                SQL = "SELECT  immeuble.codeimmeuble, immeuble.codedepanneur, '' AS Change" + " FROM Immeuble INNER JOIN PreparationIntervention ON " + " (Immeuble.CodeDepanneur = PreparationIntervention.Intervenant)" + " AND (Immeuble.CodeImmeuble = PreparationIntervention.Codeimmeuble)" + " WHERE PreparationIntervention.Intervenant ='" + txtAncienDepanneur.Text + "' order by immeuble.codeimmeuble";
                if (modAdors == null)
                {
                    modAdors = new ModAdo();
                }
                rs = modAdors.fc_OpenRecordSet(SQL);
                j = rs.Rows.Count;
                //Set rs = gdb.OpenRecordset(SQL, dbOpenSnapshot)
                fournisGrille(0);
                //----Ajoute une colonne temporaire.
                i = SSDBGrid2.DisplayLayout.Bands[0].Columns.Count;
                SSDBGrid2.DisplayLayout.Bands[0].Columns[i - 1].Hidden = true;

                for (i = 0; i <= SSDBGrid2.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSDBGrid2.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                }
                FournisChamps(0);
                if (j == 1)
                {
                    lblTotal2.Text = j + " enregistrement";
                }
                else
                {
                    lblTotal2.Text = j + " enregistrements";
                }

                if (!string.IsNullOrEmpty(txtNouveauDepanneur.Text))
                {
                    cmdRemplace.Enabled = true;
                }
                else
                {
                    cmdRemplace.Enabled = false;
                }
            }
            else
            {
                SSDBGrid1.DataSource = null;
                SSDBGrid2.DataSource = null;
                cmdRemplace.Enabled = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas d'immeuble associé à ce depanneur", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lngGrille"></param>
        private void FournisChamps(int lngGrille)
        {
            if (lngGrille == 1)
            {
                if (rs.Columns.Count > 0)
                    for (i = 0; i <= rs.Columns.Count - 1; i++)
                    {
                        SSDBGrid1.DisplayLayout.Bands[0].Columns[i].Header.Caption = rs.Columns[i].ColumnName;
                    }
            }
            else
            {
                if (rs.Columns.Count > 0)
                    for (i = 0; i <= rs.Columns.Count - 1; i++)
                    {
                        SSDBGrid2.DisplayLayout.Bands[0].Columns[i].Header.Caption = rs.Columns[i].ColumnName;
                    }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lngGrille"></param>
        private void fournisGrille(int lngGrille)
        {
            if (lngGrille == 1)
            {
                SSDBGrid1.DataSource = rs;
            }
            else
            {
                SSDBGrid2.DataSource = rs;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRemplace_Click(object sender, EventArgs e)
        {
            DialogResult DR = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous tous remplacer?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (DR == DialogResult.Yes)
            {
                SQL = "Select matricule from personnel where matricule ='" + txtNouveauDepanneur.Text + "'";
                if (modAdors == null)
                {
                    modAdors = new ModAdo();
                }
                rs = modAdors.fc_OpenRecordSet(SQL);
                //Set rs = gdb.OpenRecordset(SQL, dbOpenSnapshot)

                if (rs.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce nouveau depanneur n'existe pas.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                else
                {
                    SQL = "UPDATE PreparationIntervention " + " SET PreparationIntervention.Intervenant = '" + txtNouveauDepanneur.Text + "' where intervenant ='" + txtAncienDepanneur.Text + "'";

                    int x = General.Execute(SQL);
                    RemplaceTout();
                    //boolChangement = False
                    cmdRemplace.Enabled = false;
                    SSDBGrid2.Visible = false;
                    for (j = 0; j <= SSDBGrid2.Rows.Count - 1; j++)
                    {
                        //VAR = .GetBookmark(j)
                        //----récupére seulement les enregistrements séléctionnés.
                        //.MoveRecords j
                        SSDBGrid2.Rows[j].Cells["CodeDepanneur"].Value = txtNouveauDepanneur.Text;
                        SSDBGrid2.Rows[j].Cells["Change"].Value = "1";
                        
                        // For j = 0 To .Columns.Count - 1
                        //     .Columns(j).CellStyleSet "yesBisStyle", VAR
                        // Next j

                    }
                    SSDBGrid2.UpdateData();
                    SSDBGrid2.Visible = true;
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void RemplaceTout()
        {

            if (modAdors == null)
            {
                modAdors = new ModAdo();
            }
            rs = modAdors.fc_OpenRecordSet("Select CodeImmeuble,CodeDepanneur, '' change from Immeuble where codedepanneur ='" + txtAncienDepanneur.Text + "'");
            //Set rs = gdb.OpenRecordset("Select CodeImmeuble,CodeDepanneur from Immeuble where codedepanneur ='" & txtAncienDepanneur & "'")
            foreach (DataRow rsRow in rs.Rows)
            {

                //.EditMode
                rsRow["codedepanneur"] = txtNouveauDepanneur.Text;
            }
            modAdors.Update();

            foreach (DataRow rsRow in rs.Rows)
            {

                //.EditMode
                rsRow["change"] = 1;
            }
            SSDBGrid1.DataSource = rs;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCodeDepanneur_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            modAdoData1 = new ModAdo();
            Data1 = modAdoData1.fc_OpenRecordSet("SELECT  matricule,Nom,Prenom from personnel order by matricule");
            txtAncienDepanneur.DataSource = Data1;
            txtNouveauDepanneur.DataSource = Data1;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_Click(object sender, EventArgs e)
        {
            Modifie();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void Modifie()
        {
            if (SSDBGrid1.ActiveRow == null)
                return;

            if (string.IsNullOrEmpty(txtNouveauDepanneur.Text))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un nouveau depanneur pour pouvoir modifier", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }
            else
            {
                //----Verifie si ce depanneur éxiste.
                SQL = "select matricule from personnel where matricule ='" + txtNouveauDepanneur.Text + "'";
                if (modAdors2 == null)
                {
                    modAdors2 = new ModAdo();
                }
                rs2 = modAdors2.fc_OpenRecordSet(SQL);
                //Set rs2 = gdb.OpenRecordset(SQL, dbOpenSnapshot)

                if (rs2.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce matricule n'existe pas.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtNouveauDepanneur.Focus();
                    return;
                }
                else
                {
                    SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value = txtNouveauDepanneur.Text;

                }

                if (string.IsNullOrEmpty(SSDBGrid1.ActiveRow.Cells["change"].Value.ToString()))
                {
                    for (i = 0; i <= SSDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    {
                        SSDBGrid1.ActiveRow.Cells[i].Appearance.BackColor = ColorTranslator.FromOle(8454143);

                    }
                    SSDBGrid1.ActiveRow.Cells["change"].Value = "1";
                    SSDBGrid1.UpdateData();

                    SQL = "UPDATE PreparationIntervention " + " SET PreparationIntervention.Intervenant = '" + SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value + "'" +
                        " WHERE PreparationIntervention.Codeimmeuble='" + SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value + "'";
                    General.Execute(SQL);



                    SQL = "UPDATE immeuble SET CodeDepanneur ='" + SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value + "'" +
                        " WHERE codeimmeuble = '" + SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value + "'";
                    General.Execute(SQL);
                    change(SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value.ToString(), SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value.ToString(), true);
                }
                else
                {

                    for (i = 0; i <= SSDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    {
                        SSDBGrid1.ActiveRow.Cells[i].Appearance.BackColor = ColorTranslator.FromOle(0xffffff);
                    }
                    SSDBGrid1.ActiveRow.Cells["change"].Value = "";
                    SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value = txtAncienDepanneur.Text;
                    SSDBGrid1.UpdateData();
                    SQL = "UPDATE PreparationIntervention " + " SET PreparationIntervention.Intervenant = '" + SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value +
                        "'" + " WHERE PreparationIntervention.Codeimmeuble='" + SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value + "'";
                    int x = General.Execute(SQL);

                    SQL = "UPDATE immeuble SET CodeDepanneur ='" + SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value + "'" + " WHERE codeimmeuble = '" +
                        SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value + "'";
                    x = General.Execute(SQL);
                    change(SSDBGrid1.ActiveRow.Cells["CodeImmeuble"].Value.ToString(), SSDBGrid1.ActiveRow.Cells["CodeDepanneur"].Value, false);
                }
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeImmeuble"></param>
        /// <param name="strCodeDepanneur"></param>
        /// <param name="boolOk"></param>
        private void change(string strCodeImmeuble, object strCodeDepanneur, bool boolOk)
        {

            int j = 0;


            if (boolOk == true)
            {
                for (i = 0; i <= SSDBGrid2.Rows.Count - 1; i++)
                {
                    //----récupére seulement les enregistrements séléctionnés.
                    if (SSDBGrid2.Rows[i].Cells["Codeimmeuble"].Value.ToString() == strCodeImmeuble)
                    {
                        SSDBGrid2.Rows[i].Cells["CodeDepanneur"].Value = strCodeDepanneur;
                        SSDBGrid2.Rows[i].Cells["Change"].Value = "1";
                        SSDBGrid2.UpdateData();
                        SSDBGrid2.Rows[i].Appearance.BackColor = ColorTranslator.FromOle(8454143);
                        break;
                    }
                }
            }
            ///=============> Tested
            else
            {
                for (i = 0; i <= SSDBGrid2.Rows.Count - 1; i++)
                {
                    if (SSDBGrid2.Rows[i].Cells["Codeimmeuble"].Value.ToString() == strCodeImmeuble)
                    {
                        SSDBGrid2.Rows[i].Cells["CodeDepanneur"].Value = strCodeDepanneur;
                        SSDBGrid2.Rows[i].Cells["Change"].Value = "";
                        SSDBGrid2.UpdateData();
                        SSDBGrid2.Rows[i].Appearance.BackColor = ColorTranslator.FromOle(0xffffff);
                        break;
                    }
                }
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                Modifie();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["change"].Hidden = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["change"].Value.ToString() == "1")
            {
                // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                e.Row.Appearance.BackColor = ColorTranslator.FromOle(8454143);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid2_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["change"].Value.ToString() == "1")
            {
                // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                e.Row.Appearance.BackColor = ColorTranslator.FromOle(8454143);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAncienDepanneur_AfterCloseUp(object sender, EventArgs e)
        {
            fournisTable();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAncienDepanneur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                fournisTable();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNouveauDepanneur_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //-----recherche si ce code depanneur existe
                //SQL = "Select matricule from personnel where matricule ='" & txtNouveauDepanneur & "'"
                //Set rs2 = gdb.OpenRecordset(SQL, dbOpenSnapshot)
                //If rs2.EOF = True Then
                if (!txtNouveauDepanneur.IsItemInList())
                {

                    cmdRemplace.Enabled = false;
                    cmdValider.Enabled = false;
                    // cmdannuler.Enabled = False
                    cmdNouveauDepanneur_Click(cmdNouveauDepanneur, new System.EventArgs());
                }
                else
                {
                    cmdRemplace.Enabled = true;
                    cmdValider.Enabled = true;
                    // cmdannuler.Enabled = True
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNouveauDepanneur_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtNouveauDepanneur.Text))
            {
                cmdRemplace.Enabled = false;
                // cmdannuler.Enabled = False
                // cmdValider.Enabled = False
            }
            else
            {
                cmdRemplace.Enabled = true;
            }
        }
    }
}
