﻿namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms
{
    partial class frmGeocoderV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmGeocoderV2));
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.LabelResultat = new System.Windows.Forms.Label();
            this.btnQuiter = new System.Windows.Forms.Button();
            this.bntEnregistrement = new System.Windows.Forms.Button();
            this.txtLat = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.txtLong = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.ListAdresse = new System.Windows.Forms.ListBox();
            this.label6 = new System.Windows.Forms.Label();
            this.ChercheCordonnees = new System.Windows.Forms.Button();
            this.TxtPays = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.TxtVille = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.TxtCodePostal = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.TxtAdresse = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(615, 64);
            this.label1.TabIndex = 0;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.LabelResultat);
            this.groupBox1.Controls.Add(this.btnQuiter);
            this.groupBox1.Controls.Add(this.bntEnregistrement);
            this.groupBox1.Controls.Add(this.txtLat);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.txtLong);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.ListAdresse);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.ChercheCordonnees);
            this.groupBox1.Controls.Add(this.TxtPays);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.TxtVille);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.TxtCodePostal);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.TxtAdresse);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(16, 76);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(611, 474);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Saisissez votre adresse :";
            // 
            // LabelResultat
            // 
            this.LabelResultat.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(201)))), ((int)(((byte)(175)))));
            this.LabelResultat.Location = new System.Drawing.Point(6, 410);
            this.LabelResultat.Name = "LabelResultat";
            this.LabelResultat.Size = new System.Drawing.Size(599, 48);
            this.LabelResultat.TabIndex = 568;
            // 
            // btnQuiter
            // 
            this.btnQuiter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.btnQuiter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnQuiter.FlatAppearance.BorderSize = 0;
            this.btnQuiter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnQuiter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.btnQuiter.ForeColor = System.Drawing.Color.White;
            this.btnQuiter.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.btnQuiter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnQuiter.Location = new System.Drawing.Point(395, 369);
            this.btnQuiter.Margin = new System.Windows.Forms.Padding(2);
            this.btnQuiter.Name = "btnQuiter";
            this.btnQuiter.Size = new System.Drawing.Size(210, 35);
            this.btnQuiter.TabIndex = 567;
            this.btnQuiter.Text = "Quiter";
            this.btnQuiter.UseVisualStyleBackColor = false;
            this.btnQuiter.Click += new System.EventHandler(this.btnQuiter_Click);
            // 
            // bntEnregistrement
            // 
            this.bntEnregistrement.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.bntEnregistrement.Cursor = System.Windows.Forms.Cursors.Hand;
            this.bntEnregistrement.FlatAppearance.BorderSize = 0;
            this.bntEnregistrement.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.bntEnregistrement.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.bntEnregistrement.ForeColor = System.Drawing.Color.White;
            this.bntEnregistrement.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.bntEnregistrement.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.bntEnregistrement.Location = new System.Drawing.Point(181, 369);
            this.bntEnregistrement.Margin = new System.Windows.Forms.Padding(2);
            this.bntEnregistrement.Name = "bntEnregistrement";
            this.bntEnregistrement.Size = new System.Drawing.Size(210, 35);
            this.bntEnregistrement.TabIndex = 516;
            this.bntEnregistrement.Text = "     Appliquer à l\'immeuble";
            this.bntEnregistrement.UseVisualStyleBackColor = false;
            this.bntEnregistrement.Click += new System.EventHandler(this.bntEnregistrement_Click);
            // 
            // txtLat
            // 
            this.txtLat.AccAcceptNumbersOnly = false;
            this.txtLat.AccAllowComma = false;
            this.txtLat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLat.AccHidenValue = "";
            this.txtLat.AccNotAllowedChars = null;
            this.txtLat.AccReadOnly = false;
            this.txtLat.AccReadOnlyAllowDelete = false;
            this.txtLat.AccRequired = false;
            this.txtLat.BackColor = System.Drawing.Color.White;
            this.txtLat.CustomBackColor = System.Drawing.Color.White;
            this.txtLat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLat.ForeColor = System.Drawing.Color.Black;
            this.txtLat.Location = new System.Drawing.Point(99, 337);
            this.txtLat.Margin = new System.Windows.Forms.Padding(2);
            this.txtLat.MaxLength = 32767;
            this.txtLat.Multiline = false;
            this.txtLat.Name = "txtLat";
            this.txtLat.ReadOnly = false;
            this.txtLat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLat.Size = new System.Drawing.Size(278, 27);
            this.txtLat.TabIndex = 6;
            this.txtLat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLat.UseSystemPasswordChar = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(6, 337);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(67, 19);
            this.label8.TabIndex = 514;
            this.label8.Text = "Latitude";
            // 
            // txtLong
            // 
            this.txtLong.AccAcceptNumbersOnly = false;
            this.txtLong.AccAllowComma = false;
            this.txtLong.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLong.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLong.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLong.AccHidenValue = "";
            this.txtLong.AccNotAllowedChars = null;
            this.txtLong.AccReadOnly = false;
            this.txtLong.AccReadOnlyAllowDelete = false;
            this.txtLong.AccRequired = false;
            this.txtLong.BackColor = System.Drawing.Color.White;
            this.txtLong.CustomBackColor = System.Drawing.Color.White;
            this.txtLong.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLong.ForeColor = System.Drawing.Color.Black;
            this.txtLong.Location = new System.Drawing.Point(99, 306);
            this.txtLong.Margin = new System.Windows.Forms.Padding(2);
            this.txtLong.MaxLength = 32767;
            this.txtLong.Multiline = false;
            this.txtLong.Name = "txtLong";
            this.txtLong.ReadOnly = false;
            this.txtLong.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLong.Size = new System.Drawing.Size(278, 27);
            this.txtLong.TabIndex = 5;
            this.txtLong.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLong.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(6, 306);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 19);
            this.label7.TabIndex = 512;
            this.label7.Text = "Longitude";
            // 
            // ListAdresse
            // 
            this.ListAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ListAdresse.FormattingEnabled = true;
            this.ListAdresse.ItemHeight = 19;
            this.ListAdresse.Location = new System.Drawing.Point(10, 137);
            this.ListAdresse.Name = "ListAdresse";
            this.ListAdresse.Size = new System.Drawing.Size(595, 137);
            this.ListAdresse.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(6, 115);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(337, 19);
            this.label6.TabIndex = 510;
            this.label6.Text = "Adresse(s)  associée(s), choisissez parmi la liste :";
            // 
            // ChercheCordonnees
            // 
            this.ChercheCordonnees.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.ChercheCordonnees.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.ChercheCordonnees.FlatAppearance.BorderSize = 0;
            this.ChercheCordonnees.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ChercheCordonnees.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.ChercheCordonnees.ForeColor = System.Drawing.Color.White;
            this.ChercheCordonnees.Image = global::Axe_interDT.Properties.Resources.Copy_16x16;
            this.ChercheCordonnees.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ChercheCordonnees.Location = new System.Drawing.Point(426, 51);
            this.ChercheCordonnees.Margin = new System.Windows.Forms.Padding(2);
            this.ChercheCordonnees.Name = "ChercheCordonnees";
            this.ChercheCordonnees.Size = new System.Drawing.Size(180, 45);
            this.ChercheCordonnees.TabIndex = 509;
            this.ChercheCordonnees.Text = "Recherche des Coordonnées";
            this.ChercheCordonnees.UseVisualStyleBackColor = false;
            this.ChercheCordonnees.Click += new System.EventHandler(this.ChercheCordonnees_Click);
            // 
            // TxtPays
            // 
            this.TxtPays.AccAcceptNumbersOnly = false;
            this.TxtPays.AccAllowComma = false;
            this.TxtPays.AccBackgroundColor = System.Drawing.Color.White;
            this.TxtPays.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TxtPays.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TxtPays.AccHidenValue = "";
            this.TxtPays.AccNotAllowedChars = null;
            this.TxtPays.AccReadOnly = false;
            this.TxtPays.AccReadOnlyAllowDelete = false;
            this.TxtPays.AccRequired = false;
            this.TxtPays.BackColor = System.Drawing.Color.White;
            this.TxtPays.CustomBackColor = System.Drawing.Color.White;
            this.TxtPays.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TxtPays.ForeColor = System.Drawing.Color.Black;
            this.TxtPays.Location = new System.Drawing.Point(426, 20);
            this.TxtPays.Margin = new System.Windows.Forms.Padding(2);
            this.TxtPays.MaxLength = 32767;
            this.TxtPays.Multiline = false;
            this.TxtPays.Name = "TxtPays";
            this.TxtPays.ReadOnly = false;
            this.TxtPays.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TxtPays.Size = new System.Drawing.Size(180, 27);
            this.TxtPays.TabIndex = 1;
            this.TxtPays.Text = "France";
            this.TxtPays.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TxtPays.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(356, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(39, 16);
            this.label5.TabIndex = 507;
            this.label5.Text = "Pays";
            // 
            // TxtVille
            // 
            this.TxtVille.AccAcceptNumbersOnly = false;
            this.TxtVille.AccAllowComma = false;
            this.TxtVille.AccBackgroundColor = System.Drawing.Color.White;
            this.TxtVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TxtVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TxtVille.AccHidenValue = "";
            this.TxtVille.AccNotAllowedChars = null;
            this.TxtVille.AccReadOnly = false;
            this.TxtVille.AccReadOnlyAllowDelete = false;
            this.TxtVille.AccRequired = false;
            this.TxtVille.BackColor = System.Drawing.Color.White;
            this.TxtVille.CustomBackColor = System.Drawing.Color.White;
            this.TxtVille.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TxtVille.ForeColor = System.Drawing.Color.Black;
            this.TxtVille.Location = new System.Drawing.Point(99, 82);
            this.TxtVille.Margin = new System.Windows.Forms.Padding(2);
            this.TxtVille.MaxLength = 32767;
            this.TxtVille.Multiline = false;
            this.TxtVille.Name = "TxtVille";
            this.TxtVille.ReadOnly = false;
            this.TxtVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TxtVille.Size = new System.Drawing.Size(323, 27);
            this.TxtVille.TabIndex = 3;
            this.TxtVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TxtVille.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(6, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 19);
            this.label4.TabIndex = 505;
            this.label4.Text = "Ville";
            // 
            // TxtCodePostal
            // 
            this.TxtCodePostal.AccAcceptNumbersOnly = false;
            this.TxtCodePostal.AccAllowComma = false;
            this.TxtCodePostal.AccBackgroundColor = System.Drawing.Color.White;
            this.TxtCodePostal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TxtCodePostal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TxtCodePostal.AccHidenValue = "";
            this.TxtCodePostal.AccNotAllowedChars = null;
            this.TxtCodePostal.AccReadOnly = false;
            this.TxtCodePostal.AccReadOnlyAllowDelete = false;
            this.TxtCodePostal.AccRequired = false;
            this.TxtCodePostal.BackColor = System.Drawing.Color.White;
            this.TxtCodePostal.CustomBackColor = System.Drawing.Color.White;
            this.TxtCodePostal.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TxtCodePostal.ForeColor = System.Drawing.Color.Black;
            this.TxtCodePostal.Location = new System.Drawing.Point(99, 51);
            this.TxtCodePostal.Margin = new System.Windows.Forms.Padding(2);
            this.TxtCodePostal.MaxLength = 32767;
            this.TxtCodePostal.Multiline = false;
            this.TxtCodePostal.Name = "TxtCodePostal";
            this.TxtCodePostal.ReadOnly = false;
            this.TxtCodePostal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TxtCodePostal.Size = new System.Drawing.Size(323, 27);
            this.TxtCodePostal.TabIndex = 2;
            this.TxtCodePostal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TxtCodePostal.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(6, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 19);
            this.label3.TabIndex = 503;
            this.label3.Text = "Code Postal";
            // 
            // TxtAdresse
            // 
            this.TxtAdresse.AccAcceptNumbersOnly = false;
            this.TxtAdresse.AccAllowComma = false;
            this.TxtAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.TxtAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TxtAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.TxtAdresse.AccHidenValue = "";
            this.TxtAdresse.AccNotAllowedChars = null;
            this.TxtAdresse.AccReadOnly = false;
            this.TxtAdresse.AccReadOnlyAllowDelete = false;
            this.TxtAdresse.AccRequired = false;
            this.TxtAdresse.BackColor = System.Drawing.Color.White;
            this.TxtAdresse.CustomBackColor = System.Drawing.Color.White;
            this.TxtAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TxtAdresse.ForeColor = System.Drawing.Color.Black;
            this.TxtAdresse.Location = new System.Drawing.Point(99, 20);
            this.TxtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.TxtAdresse.MaxLength = 32767;
            this.TxtAdresse.Multiline = false;
            this.TxtAdresse.Name = "TxtAdresse";
            this.TxtAdresse.ReadOnly = false;
            this.TxtAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TxtAdresse.Size = new System.Drawing.Size(180, 27);
            this.TxtAdresse.TabIndex = 1;
            this.TxtAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TxtAdresse.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(6, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "Adresse";
            // 
            // frmGeocoderV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(639, 558);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(655, 538);
            this.Name = "frmGeocoderV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmGeocoderV2";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 TxtPays;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 TxtVille;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 TxtCodePostal;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 TxtAdresse;
        public System.Windows.Forms.Button ChercheCordonnees;
        public iTalk.iTalk_TextBox_Small2 txtLat;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtLong;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ListBox ListAdresse;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.Button bntEnregistrement;
        public System.Windows.Forms.Button btnQuiter;
        public System.Windows.Forms.Label LabelResultat;
    }
}