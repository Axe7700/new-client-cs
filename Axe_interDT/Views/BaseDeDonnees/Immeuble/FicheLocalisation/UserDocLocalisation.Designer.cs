namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.FicheLocalisation
{
    partial class UserDocLocalisation
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame725 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label3 = new System.Windows.Forms.Label();
            this.Cmd17 = new System.Windows.Forms.Button();
            this.Cmd18 = new System.Windows.Forms.Button();
            this.Cmd16 = new System.Windows.Forms.Button();
            this.chk4 = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chk2 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Cmd15 = new System.Windows.Forms.Button();
            this.TreeView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.ContextMenuSupp = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.MenuSupp = new System.Windows.Forms.ToolStripMenuItem();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txt16 = new iTalk.iTalk_TextBox_Small2();
            this.txt15 = new iTalk.iTalk_TextBox_Small2();
            this.txt17 = new iTalk.iTalk_RichTextBox();
            this.tableLayoutPanel3.SuspendLayout();
            this.Frame725.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).BeginInit();
            this.ContextMenuSupp.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(546, 33);
            this.label33.TabIndex = 502;
            this.label33.Text = "Code Immeuble";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 37.5F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 62.5F));
            this.tableLayoutPanel3.Controls.Add(this.Frame725, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 73);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.78195F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.84211F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 16.16541F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.58647F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1850, 884);
            this.tableLayoutPanel3.TabIndex = 413;
            // 
            // Frame725
            // 
            this.Frame725.BackColor = System.Drawing.Color.Transparent;
            this.Frame725.Controls.Add(this.tableLayoutPanel1);
            this.Frame725.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame725.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Frame725.Location = new System.Drawing.Point(696, 115);
            this.Frame725.Name = "Frame725";
            this.tableLayoutPanel3.SetRowSpan(this.Frame725, 2);
            this.Frame725.Size = new System.Drawing.Size(1151, 460);
            this.Frame725.TabIndex = 582;
            this.Frame725.TabStop = false;
            this.Frame725.Text = "BADGE";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.16631F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.30238F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.30238F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.00648F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.87041F));
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Cmd17, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.Cmd18, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Cmd16, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.chk4, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.chk2, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.txt16, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txt15, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txt17, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 9;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1145, 436);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 136);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(234, 32);
            this.label3.TabIndex = 574;
            this.label3.Text = "Commentaire";
            // 
            // Cmd17
            // 
            this.Cmd17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Cmd17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cmd17.FlatAppearance.BorderSize = 0;
            this.Cmd17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd17.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.Cmd17.ForeColor = System.Drawing.Color.White;
            this.Cmd17.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.Cmd17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd17.Location = new System.Drawing.Point(702, 2);
            this.Cmd17.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd17.Name = "Cmd17";
            this.Cmd17.Size = new System.Drawing.Size(212, 31);
            this.Cmd17.TabIndex = 391;
            this.Cmd17.Tag = "17";
            this.Cmd17.Text = "     Valider";
            this.Cmd17.UseVisualStyleBackColor = false;
            this.Cmd17.Click += new System.EventHandler(this.Cmd15_Click);
            // 
            // Cmd18
            // 
            this.Cmd18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Cmd18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cmd18.FlatAppearance.BorderSize = 0;
            this.Cmd18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd18.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.Cmd18.ForeColor = System.Drawing.Color.White;
            this.Cmd18.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.Cmd18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd18.Location = new System.Drawing.Point(472, 2);
            this.Cmd18.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd18.Name = "Cmd18";
            this.Cmd18.Size = new System.Drawing.Size(226, 31);
            this.Cmd18.TabIndex = 390;
            this.Cmd18.Tag = "18";
            this.Cmd18.Text = "  Annuler";
            this.Cmd18.UseVisualStyleBackColor = false;
            this.Cmd18.Click += new System.EventHandler(this.Cmd15_Click);
            // 
            // Cmd16
            // 
            this.Cmd16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Cmd16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cmd16.FlatAppearance.BorderSize = 0;
            this.Cmd16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd16.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.Cmd16.ForeColor = System.Drawing.Color.White;
            this.Cmd16.Image = global::Axe_interDT.Properties.Resources.Edit_16x16;
            this.Cmd16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd16.Location = new System.Drawing.Point(242, 2);
            this.Cmd16.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd16.Name = "Cmd16";
            this.Cmd16.Size = new System.Drawing.Size(226, 31);
            this.Cmd16.TabIndex = 392;
            this.Cmd16.Tag = "16";
            this.Cmd16.Text = "      Modifier";
            this.Cmd16.UseVisualStyleBackColor = false;
            this.Cmd16.Click += new System.EventHandler(this.Cmd15_Click);
            // 
            // chk4
            // 
            this.chk4.AutoSize = true;
            this.chk4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chk4.Location = new System.Drawing.Point(919, 38);
            this.chk4.Name = "chk4";
            this.chk4.Size = new System.Drawing.Size(78, 21);
            this.chk4.TabIndex = 570;
            this.chk4.Tag = "4";
            this.chk4.Text = "Installé";
            this.chk4.UseVisualStyleBackColor = true;
            this.chk4.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 62);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(234, 30);
            this.label4.TabIndex = 571;
            this.label4.Text = "N°localisation";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(234, 32);
            this.label5.TabIndex = 572;
            this.label5.Text = "Localisation";
            // 
            // chk2
            // 
            this.chk2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chk2, 3);
            this.chk2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chk2.Location = new System.Drawing.Point(473, 216);
            this.chk2.Name = "chk2";
            this.chk2.Size = new System.Drawing.Size(669, 217);
            this.chk2.TabIndex = 573;
            this.chk2.Tag = "2";
            this.chk2.Text = "Ne pas prendre en compte";
            this.chk2.UseVisualStyleBackColor = true;
            this.chk2.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.Cmd15, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.TreeView1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel3.SetRowSpan(this.tableLayoutPanel2, 4);
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(687, 878);
            this.tableLayoutPanel2.TabIndex = 583;
            // 
            // Cmd15
            // 
            this.Cmd15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Cmd15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cmd15.FlatAppearance.BorderSize = 0;
            this.Cmd15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Cmd15.ForeColor = System.Drawing.Color.White;
            this.Cmd15.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.Cmd15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd15.Location = new System.Drawing.Point(7, 7);
            this.Cmd15.Margin = new System.Windows.Forms.Padding(7);
            this.Cmd15.Name = "Cmd15";
            this.Cmd15.Size = new System.Drawing.Size(673, 36);
            this.Cmd15.TabIndex = 387;
            this.Cmd15.Tag = "15";
            this.Cmd15.Text = "Ajout Localisation";
            this.Cmd15.UseVisualStyleBackColor = false;
            this.Cmd15.Click += new System.EventHandler(this.Cmd15_Click);
            // 
            // TreeView1
            // 
            appearance1.BorderColor = System.Drawing.Color.Black;
            this.TreeView1.Appearance = appearance1;
            this.TreeView1.ContextMenuStrip = this.ContextMenuSupp;
            this.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeView1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeView1.Location = new System.Drawing.Point(3, 53);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.Size = new System.Drawing.Size(681, 822);
            this.TreeView1.TabIndex = 388;
            this.TreeView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeView1_AfterSelect);
            // 
            // ContextMenuSupp
            // 
            this.ContextMenuSupp.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuSupp});
            this.ContextMenuSupp.Name = "ContextMenuSupp";
            this.ContextMenuSupp.Size = new System.Drawing.Size(130, 26);
            // 
            // MenuSupp
            // 
            this.MenuSupp.Name = "MenuSupp";
            this.MenuSupp.Size = new System.Drawing.Size(129, 22);
            this.MenuSupp.Text = "Supprimer";
            this.MenuSupp.Click += new System.EventHandler(this.MenuSupp_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.88764F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70.11236F));
            this.tableLayoutPanel4.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtCodeImmeuble, 1, 0);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 33);
            this.tableLayoutPanel4.TabIndex = 504;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel2.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1784, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(66, 40);
            this.flowLayoutPanel2.TabIndex = 505;
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(4, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 359;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel3, 0, 2);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 3;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel5.TabIndex = 506;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(554, 2);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(1294, 27);
            this.txtCodeImmeuble.TabIndex = 503;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // txt16
            // 
            this.txt16.AccAcceptNumbersOnly = false;
            this.txt16.AccAllowComma = false;
            this.txt16.AccBackgroundColor = System.Drawing.Color.White;
            this.txt16.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt16.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt16.AccHidenValue = "";
            this.txt16.AccNotAllowedChars = null;
            this.txt16.AccReadOnly = false;
            this.txt16.AccReadOnlyAllowDelete = false;
            this.txt16.AccRequired = false;
            this.txt16.BackColor = System.Drawing.Color.White;
            this.txt16.CustomBackColor = System.Drawing.Color.White;
            this.txt16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt16.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt16.ForeColor = System.Drawing.Color.Black;
            this.txt16.Location = new System.Drawing.Point(242, 64);
            this.txt16.Margin = new System.Windows.Forms.Padding(2);
            this.txt16.MaxLength = 32767;
            this.txt16.Multiline = false;
            this.txt16.Name = "txt16";
            this.txt16.ReadOnly = true;
            this.txt16.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt16.Size = new System.Drawing.Size(226, 27);
            this.txt16.TabIndex = 0;
            this.txt16.Tag = "16";
            this.txt16.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt16.UseSystemPasswordChar = false;
            // 
            // txt15
            // 
            this.txt15.AccAcceptNumbersOnly = false;
            this.txt15.AccAllowComma = false;
            this.txt15.AccBackgroundColor = System.Drawing.Color.White;
            this.txt15.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt15.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt15.AccHidenValue = "";
            this.txt15.AccNotAllowedChars = null;
            this.txt15.AccReadOnly = false;
            this.txt15.AccReadOnlyAllowDelete = false;
            this.txt15.AccRequired = false;
            this.txt15.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.txt15, 3);
            this.txt15.CustomBackColor = System.Drawing.Color.White;
            this.txt15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt15.ForeColor = System.Drawing.Color.Black;
            this.txt15.Location = new System.Drawing.Point(242, 94);
            this.txt15.Margin = new System.Windows.Forms.Padding(2);
            this.txt15.MaxLength = 32767;
            this.txt15.Multiline = false;
            this.txt15.Name = "txt15";
            this.txt15.ReadOnly = false;
            this.txt15.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt15.Size = new System.Drawing.Size(672, 27);
            this.txt15.TabIndex = 1;
            this.txt15.Tag = "15";
            this.txt15.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txt15.UseSystemPasswordChar = false;
            // 
            // txt17
            // 
            this.txt17.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt17.AutoWordSelection = false;
            this.txt17.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txt17, 3);
            this.txt17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt17.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt17.ForeColor = System.Drawing.Color.Black;
            this.txt17.Location = new System.Drawing.Point(243, 139);
            this.txt17.Name = "txt17";
            this.txt17.ReadOnly = false;
            this.tableLayoutPanel1.SetRowSpan(this.txt17, 3);
            this.txt17.Size = new System.Drawing.Size(670, 71);
            this.txt17.TabIndex = 2;
            this.txt17.Tag = "17";
            this.txt17.WordWrap = true;
            // 
            // UserDocLocalisation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel5);
            this.Name = "UserDocLocalisation";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche Localisation";
            this.VisibleChanged += new System.EventHandler(this.UserDocLocalisation_VisibleChanged);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.Frame725.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).EndInit();
            this.ContextMenuSupp.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.GroupBox Frame725;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button Cmd17;
        public System.Windows.Forms.Button Cmd18;
        public System.Windows.Forms.Button Cmd16;
        public System.Windows.Forms.CheckBox chk4;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.CheckBox chk2;
        public iTalk.iTalk_TextBox_Small2 txt16;
        public iTalk.iTalk_TextBox_Small2 txt15;
        public iTalk.iTalk_RichTextBox txt17;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Button Cmd15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public System.Windows.Forms.Button CmdRechercher;
        private Infragistics.Win.UltraWinTree.UltraTree TreeView1;
        private System.Windows.Forms.ContextMenuStrip ContextMenuSupp;
        private System.Windows.Forms.ToolStripMenuItem MenuSupp;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
    }
}
