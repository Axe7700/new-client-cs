﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win.UltraWinTree;

namespace Axe_interDT.Views.BaseDeDonnees.Immeuble.FicheLocalisation
{
    public partial class UserDocLocalisation : UserControl
    {
        const string cLibelleNivBadge1 = "Localisation Badge";
        const string cLIbelleNiv1Vide = "Vide";
        const string cCodeLocalDefaut = "CHAUF";
        const string cLibelleLocalDefaut = "Chaufferie";
        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cNiv3 = "C";
        public UserDocLocalisation()
        {
            InitializeComponent();
        }

        private void Cmd15_Click(object sender, EventArgs e)
        {
            var cmd = sender as Button;
            int Index = Convert.ToInt32(cmd.Tag);

            string sLibelleLoc = null;

            switch (Index)
            {

                case 15:
                    //==== bouton d'ajout de localisation de badge.
                    sLibelleLoc = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le nom de la localisation.", "Badge");
                    sLibelleLoc = General.Trim(sLibelleLoc);
                    if (!string.IsNullOrEmpty(sLibelleLoc))
                    {
                       ModPDA.fc_AddPDABimm(txtCodeImmeuble.Text, true, sLibelleLoc);
                       fc_TreeBadge(txtCodeImmeuble.Text);
                    }
                    break;
                case 16:
                    //==== bouton permattant la modification des badges.
                    fc_LockBadge( false);
                    break;

                case 17:
                    //==== bouton permattant la validation des badges.
                    fc_LockBadge( true);
                    fc_UpdPDAB();
                    break;

                case 18:
                    //==== bouton permattant l'annulation de la modification des badges.
                    fc_LockBadge( true);
                    break;



            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            fc_Rechercher();
            fc_SavePos();
        }

        private void UserDocLocalisation_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
           
            txtCodeImmeuble.Text = General.getFrmReg( ModConstantes.cUserDocLocalisation, "txtCodeImmeuble", "");

            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))//tested
            {
                fc_ChargeEnregistrement( txtCodeImmeuble.Text);//Tested
            }
            else
            {
                fc_clear();
                fc_LockBadge( false);
                Frame725.Visible = false;
            }
        }

        private void fc_clear()
        {
            try
            {
                //=== libellé du site.
                txt15.Text = "";
                //=== Ne pas prendre en compte.
                chk2.CheckState = System.Windows.Forms.CheckState.Unchecked;
                //=== numero du site.
                txt16.Text = "";
                //=== emplacemenrt du badge
                txt17.Text = "";

                TreeView1.Nodes.Clear();

                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_clear");
            }
            
            
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeEnregistrement(string sCode)
        {
            try
            {
                fc_TreeBadge(sCode);//Tested
                Frame725.Visible = false;
                  fc_LockBadge(true);//Tested

                return;
            }catch(Exception ex) {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ChargeEnregistrement");
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Rechercher()
        {
            string sCode = null;
            string req = "";
            string where = "";
           try { 
            req = "SELECT     Immeuble.CodeImmeuble as \"Code immeuble\"," 
                + " Immeuble.Adresse as \"Adresse\"," 
                + " Immeuble.AngleRue as \"Angle Rue\"," + " Immeuble.Ville as \"Ville\"  " 
                + " FROM         PDAB_BadgePDA INNER JOIN" 
                + " Immeuble ON PDAB_BadgePDA.Codeimmeuble = Immeuble.CodeImmeuble";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeuble" };
                fg.ugResultat.DoubleClickRow += (se, ev) => {
                    //====charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeImmeuble.Text = sCode;
                    fc_ChargeEnregistrement(sCode);//Tested
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) => {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    { //====charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCodeImmeuble.Text = sCode;
                        fc_ChargeEnregistrement(sCode);
                        fg.Dispose(); fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();


                }
           catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";Command1_Click");
                
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bBloque"></param>
        private void fc_LockBadge(bool bBloque)
        {
            try { 

            Cmd17.Visible = false;
            Cmd18.Visible = false;
            Cmd16.Visible = false;
            txt15.ReadOnly = true;
            txt15.BackColor = System.Drawing.ColorTranslator.FromOle(0xe0e0e0);
            txt15.ForeColor = System.Drawing.Color.Blue;
            txt17.ReadOnly = true;
            txt17.BackColor = System.Drawing.ColorTranslator.FromOle(0xe0e0e0);
            txt17.ForeColor = System.Drawing.Color.Blue;
            TreeView1.Enabled = true;

            if (bBloque == true)//tested
            {
                Cmd16.Visible = true;

            }
            else//Tested
            {
                Cmd17.Visible = true;
                Cmd18.Visible = true;
                txt15.ReadOnly = false;
                txt15.BackColor = System.Drawing.Color.White;
                txt15.ForeColor = System.Drawing.Color.Black;
                txt17.ReadOnly = false;
                txt17.BackColor = System.Drawing.Color.White;
                txt17.ForeColor = System.Drawing.Color.Black;
                TreeView1.Enabled = false;
            }

            return;
        }catch(Exception ex) {

                Erreurs.gFr_debug(ex,this.Name + ";fc_LockBadge");
            }
          
        }
   
        private void fc_UpdPDAB()
        {
            string sSQL = null;
           
            int lPDAB_Noauto = 0;
            DataTable rs = default(DataTable);
            try
            {
                var tmpAdors = new ModAdo();
                if (TreeView1.Nodes.Count == 0)
                {

                    return;
                }

                foreach (UltraTreeNode oNode in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode iNode in oNode.Nodes)
                    {
                        if (iNode.Selected == true)
                        {
                            if (General.UCase(General.Left(iNode.Key, 1)) == General.UCase(cNiv2))
                            {
                                lPDAB_Noauto = Convert.ToInt32(General.Right(iNode.Key, General.Len(iNode.Key) - 1));
                                break; // TODO: might not be correct. Was : Exit For
                            }
                            else
                            {
                                return;
                            }
                        }
                    }
                }


                if (lPDAB_Noauto == 0)
                {
                    return;
                }

                sSQL = "SELECT PDAB_Noauto,PDAB_Libelle, PDAB_LieuBadge  " + " FROM PDAB_BadgePDA WHERE  PDAB_Noauto =" + lPDAB_Noauto;
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count==0)
                {
                    rs.Dispose();
                    rs = null;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune modification effectuée.", "Erreur",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return;
                }

                rs.Rows[0]["PDAB_Libelle"] = txt15.Text;
                rs.Rows[0]["PDAB_LieuBadge"] = txt17.Text;

                tmpAdors.Update();
                rs.Dispose();
                rs = null;
             
                foreach (UltraTreeNode oNode in TreeView1.Nodes)
                {
                   
                    if (oNode.Selected == true)
                    {
                        oNode.Text = txt15.Text;
                        if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv1))
                        {
                            Frame725.Visible = false;
                            //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0)
                            //         fc_ModeAddOrNo True
                            //          '== met à jour la localisation.
                            //        txtLCCI_NoAuto = Node.Text
                            //
                            //        txtLCCI_Code = Node.Text
                            //        txtLCCI_Libelle = Node.Text
                            //        txtLCCI_Libelle.Locked = True
                            //        txtLCCI_Libelle.FORECOLOR = vbBlue

                        }
                        else if (General.UCase(General.Left(oNode.Key, 1)) == General.UCase(cNiv2))
                        {
                            Frame725.Visible = true;

                            fc_PDAB(Convert.ToInt16(General.nz(General.Right(oNode.Key, General.Len(oNode.Key) - 1), 0)));
                            //         '== met à jour la localisation.
                            //        txtLCCI_NoAuto = Right(Node.Key, Len(Node.Key) - 1)
                            //
                            //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0) _
                            //'         & " and LCCI_NoAuto = " & nz(txtLCCI_NoAuto, 0)
                            //        fc_ModeAddOrNo False
                            //        txtLCCI_Code = fc_ADOlibelle("SELECT LCCI_Code FROM  LCCI_LocalChaufferieImm " _
                            //'                    & " WHERE LCCI_NoAuto =" & txtLCCI_NoAuto)
                            //        txtLCCI_Libelle = Node.Text
                            //        txtLCCI_Libelle.Locked = False
                            //        txtLCCI_Libelle.FORECOLOR = vbBlack


                        }

                        fc_BoldNode(TreeView1);

                        break; // TODO: might not be correct. Was : Exit For
                    }
                    foreach (UltraTreeNode iNode in oNode.Nodes)
                    {
                        if (iNode.Selected == true)
                        {
                            iNode.Text = txt15.Text;
                            if (General.UCase(General.Left(iNode.Key, 1)) == General.UCase(cNiv1))
                            {
                                Frame725.Visible = false;
                                //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0)
                                //         fc_ModeAddOrNo True
                                //          '== met à jour la localisation.
                                //        txtLCCI_NoAuto = Node.Text
                                //
                                //        txtLCCI_Code = Node.Text
                                //        txtLCCI_Libelle = Node.Text
                                //        txtLCCI_Libelle.Locked = True
                                //        txtLCCI_Libelle.FORECOLOR = vbBlue

                            }
                            else if (General.UCase(General.Left(iNode.Key, 1)) == General.UCase(cNiv2))
                            {
                                Frame725.Visible = true;

                                fc_PDAB(Convert.ToInt16(General.nz(General.Right(iNode.Key, General.Len(iNode.Key) - 1), 0)));
                                //         '== met à jour la localisation.
                                //        txtLCCI_NoAuto = Right(Node.Key, Len(Node.Key) - 1)
                                //
                                //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0) _
                                //'         & " and LCCI_NoAuto = " & nz(txtLCCI_NoAuto, 0)
                                //        fc_ModeAddOrNo False
                                //        txtLCCI_Code = fc_ADOlibelle("SELECT LCCI_Code FROM  LCCI_LocalChaufferieImm " _
                                //'                    & " WHERE LCCI_NoAuto =" & txtLCCI_NoAuto)
                                //        txtLCCI_Libelle = Node.Text
                                //        txtLCCI_Libelle.Locked = False
                                //        txtLCCI_Libelle.FORECOLOR = vbBlack


                            }

                            fc_BoldNode(TreeView1);

                            break; // TODO: might not be correct. Was : Exit For
                        }
                    }
                }

                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_UpdPDAB");
            }
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_SavePos()
        {
            General.saveInReg( ModConstantes.cUserDocLocalisation, "txtCodeImmeuble", txtCodeImmeuble.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_TreeBadge(string sCodeImmeuble)
        {
            //=== charge le treevieuw appliquée au badge.
            string sSQL = null;
            DataTable rs = default(DataTable);
            string stemp = null;
            try
            {

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //==reinitialise le treview.
                TreeView1.Nodes.Clear();
                var tmpAdors = new ModAdo();
                sSQL = "SELECT     PDAB_Noauto, PDAB_Libelle, PDAB_NonActif, PDAB_LieuBadge,"
                        + " Codeimmeuble, PDAB_ImageCB, Images, PDAB_IDBarre" + " From PDAB_BadgePDA"
                        + " WHERE Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    Frame725.Visible = false;
                    return;
                }

                //==Ajout du 1er niveau de hiérarchie.
                TreeView1.Nodes.Add(cNiv1 + sCodeImmeuble, cLibelleNivBadge1);
                TreeView1.Nodes.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                //TreeView1.Nodes[cNiv1 + sCodeImmeuble].Selected = true;
                TreeView1.Nodes[cNiv1 + sCodeImmeuble].Expanded=true;
                int i = 0;
                var _with2 = rs;

                if (_with2.Rows.Count > 0)
                {
                    foreach (DataRow rsRow in rs.Rows)
                    {
                        //===CONTROLE SI L'IDBARRE EST PRESENT

                        if (string.IsNullOrEmpty(General.nz(rsRow["PDAB_IDBarre"], "").ToString()))//Tested
                        {
                            stemp = ModPDA.fc_NumeroBadge(rsRow["CodeImmeuble"].ToString());
                            rsRow["PDAB_IDBarre"] = stemp;
                            int xx=tmpAdors.Update();
                        }
                        //=== ajout du second niveau de hiearchie.
                        for (i = 0; i < TreeView1.Nodes.Count; i++)
                        {
                            TreeView1.Nodes[i].Nodes.Add(cNiv2 + General.UCase(rsRow["PDAB_Noauto"]), General.nz(rsRow["PDAB_Libelle"], "").ToString());
                            TreeView1.Nodes[i].Nodes.Override.NodeAppearance.Image= Properties.Resources.close_folder;
                        }
                       // TreeView1.Nodes.Add(cNiv1 + sCodeImmeuble);
                            
                      //      cNiv2 + General.UCase(rsRow["PDAB_Noauto"]), General.nz(rsRow["PDAB_Libelle"], ""), 2);
                        // _with2.MoveNext();
                    }

                }

                _with2.Dispose();

                rs = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_TreeBadge");
            }
        }
          private void fc_PDAB(int lPDAB_Noauto)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            try
            {

                sSQL = "SELECT     PDAB_Noauto, PDAB_Libelle, PDAB_NonActif, PDAB_LieuBadge,"
                        + " Codeimmeuble, PDAB_ImageCB, Images, PDAB_IDBarre"
                        + " From PDAB_BadgePDA" + " WHERE     (PDAB_Noauto = '" + lPDAB_Noauto + "')";

                var tmpAdo = new ModAdo();
                rs = tmpAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    //=== nom du site.
                    txt15.Text = rs.Rows[0]["PDAB_Libelle"] + "";
                    //=== Ne pas prendre en compte.

                    chk2.Checked = General.nz(rs.Rows[0]["PDAB_NonActif"], 0).ToString() == "1";
                    //=== numero du site.
                    txt16.Text = rs.Rows[0]["PDAB_IDBarre"] + "";
                    //=== emplacemenrt du badge
                    txt17.Text = rs.Rows[0]["PDAB_LieuBadge"] + "";

                }
                else
                {

                    //=== libellé du site.
                    txt15.Text = "";
                    //=== Ne pas prendre en compte.
                    chk2.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    //=== numero du site.
                    txt16.Text = "";
                    //=== emplacemenrt du badge
                    txt17.Text = "";

                }

                rs.Dispose();
                rs = null;
                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + ";fc_PDAB");
            }
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treev"></param>
        private void fc_BoldNode(UltraTree treev)
        {
           
             foreach (UltraTreeNode oNode in treev.Nodes)
            {           
                if (oNode.Selected == true)
                {
                    oNode.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;



                }
                else
                {
                    oNode.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.False;
                }

            }

        }

      

        private void TreeView1_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            UltraTreeNode  Node = TreeView1.ActiveNode;
            Node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv1))
            {
                Frame725.Visible = false;
                //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0)
                //         fc_ModeAddOrNo True
                //          '== met à jour la localisation.
                //        txtLCCI_NoAuto = Node.Text
                //
                //        txtLCCI_Code = Node.Text
                //        txtLCCI_Libelle = Node.Text
                //        txtLCCI_Libelle.Locked = True
                //        txtLCCI_Libelle.FORECOLOR = vbBlue

            }
            else if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv2))
            {
                Frame725.Visible = true;

                fc_PDAB(Convert.ToInt16(General.nz(General.Right(Node.Key, General.Len(Node.Key) - 1), 0)));
                //         '== met à jour la localisation.
                //        txtLCCI_NoAuto = Right(Node.Key, Len(Node.Key) - 1)
                //
                //        fc_DEPI_DescriptifChaufferie " WHERE CHAI_NoAuto = " & nz(txtCHAI_NoAuto, 0) _
                //'         & " and LCCI_NoAuto = " & nz(txtLCCI_NoAuto, 0)
                //        fc_ModeAddOrNo False
                //        txtLCCI_Code = fc_ADOlibelle("SELECT LCCI_Code FROM  LCCI_LocalChaufferieImm " _
                //'                    & " WHERE LCCI_NoAuto =" & txtLCCI_NoAuto)
                //        txtLCCI_Libelle = Node.Text
                //        txtLCCI_Libelle.Locked = False
                //        txtLCCI_Libelle.FORECOLOR = vbBlack


            }

            fc_BoldNode(TreeView1);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuSupp_Click(object sender, EventArgs e)
        {
            try { 
            DialogResult dg=DialogResult.No;
            int lPDAB_Noauto = 0;
            int lindex = 0;
            bool bNodeSelected = false;

            foreach (UltraTreeNode oNode in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode oNode_N2 in oNode.Nodes)
                    {
                        if (oNode_N2.Selected)
                        {
                            if (General.UCase(General.Left(oNode_N2.Key, 1)) == General.UCase(cNiv2))
                            {
                                //   '== récupére la clé du noeud pour la requete de suppression
                                lPDAB_Noauto = Convert.ToInt32(General.Right(oNode_N2.Key, General.Len(oNode_N2.Key) - 1));

                                // '== récupére l'index pour la cle de suppression.
                                lindex = oNode.Index;
                                bNodeSelected = true;
                                dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous supprimer la localisation " + oNode_N2.Text + "", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                                break;
                            }
                        }
                    }
            }
            if (bNodeSelected == false)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord sélectionner une localisation  pour pouvoir la supprimer", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (dg == DialogResult.Yes)
            {
                    General.Execute($"DELETE  FROM PDAB_BadgePDA WHERE PDAB_Noauto ={lPDAB_Noauto}");
                    TreeView1.ActiveNode.Remove();
                    foreach (UltraTreeNode oNode in TreeView1.Nodes)
                        foreach (UltraTreeNode oNode_N2 in oNode.Nodes)
                            if (General.Left(oNode_N2.Key, 1).ToUpper() == cNiv2.ToUpper())
                            {
                                bNodeSelected = true;
                                oNode_N2.Selected = true;                              
                                TreeView1.ActiveNode = oNode_N2;
                               
                                break;
                            }
                    if (!bNodeSelected)
                        fc_TreeBadge(txtCodeImmeuble.Text);
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source);
            }
        }
    }
    }

