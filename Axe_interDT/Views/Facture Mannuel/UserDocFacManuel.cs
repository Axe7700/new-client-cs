﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Appel;
using Axe_interDT.Views.Appel.Forms;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Facture_Mannuel.Forms;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Word = Microsoft.Office.Interop.Word;

namespace Axe_interDT.Views.Facture_Mannuel
{
    public partial class UserDocFacManuel : UserControl
    {
        System.Windows.Forms.Control ControleSurFeuille;
        int activeRow = -1;
        int activeCol = -1;
        bool leftOrTab = false;
        DataTable SSOleDBGrid1DataTable;
        ModAdo SSOleDBGrid1ModAdo;
        DataTable adoManuel;
        DataTable adotemp;
        DataTable adoPied;
        DataTable rsDate;
        object dbEuroCr;
        System.DateTime dbEURO;
        //boolean indiquant si le programme est en mode ajout
        bool boolAddnew;
        //===> boolLoaded means that the control is loaded
        bool boolLoaded;
        bool boolSuppression;
        bool booltemp;
        bool boolVide;
        bool boolZero;
        private bool boolIntervention { get; set; }
        bool boolinsertInterv;
        bool boolFermeture;
        bool boolCorps;
        bool boolPied;
        bool boolErreur;
        bool boolErreurGeneral;
        bool boolFlag;
        bool boolMontantligne;
        bool boolNom;
        bool boolOnlyArticle;
        bool boolAddrow;
        bool BoolAffichemessageErrur;
        bool boolErrorCorps;
        bool boolInsertion;
        bool boolrechmulti;
        bool boolRechercheMULTI;
        bool boolInsertionArticle;
        bool boolArticleDevisCtl;

        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        const string cFacture = "FACTURE";
        const string cAvoir = "AVOIR";
        //===> Fin Modif Mondir

        bool bButtonAdd;
        //===> Modif de la version V31.08.2020 de VB6 ajouté par Mondir le 31.08.2020
        private DataTable rsInter = null;
        private ModAdo rsInterADO = null;
        //===> Fin Modif Mondir

        const string cArticleCee = "CEEP";
        const string cMOTPS = "MOTPS";
        const string cMOTPNUIT = "MOTPNUIT";
        const string cMODS = "MODS";
        const string cMODNuit = "MODNuit";
        const string cArticleAcc = "IACPT10";
        string srSQL;
        //variable stockant la cle de la table FactureManuellleEntete
        int longCle;
        //modif intervention
        int i;
        int j;
        int l;
        ///à verifier
        int v;

        int k;
        int longNumfacture;
        int Y;
        int LongBaseNoFacture;
        int i2;
        int longBoucle;

        string SQL;
        string sCodeFamille;
        string sIntitulé;
        string[] strtabPoste;
        string strPoste;
        string strErrorCorps;
        string[] strTabCompte7;
        string strCompte7;
        private string TypeFacture { get; set; }
        string SQLTypeFacture;
        string strMiseAJour;
        string strComptabilisee;
        string strAnaActivite;
        string[] strTabAnaActivite;
        string strNomPersonne;
        //variable stockant le type de taux à affecter pour chaque immeuble (taux réduit ou taux plein).
        string strTRouTP;
        string SQLIntervention;
        string strCodeArticle;
        string strIntervention;
        int lNoIntervention;

        double[] doubtabTVA;
        double[] doubTabMontantHT;
        double doubtempTVA;
        double doubMontant;
        double doubMontantHT;
        double doubMontantTVA;
        double doubMontantTTC;
        double[] doubTempTVACompte7;
        double doubMontantHTPied;


        string sCompte7AchatTR;
        string sCompteTvaAchatTR;
        string sTauxTvaAchatTR;


        string sCompte7AchatTP;
        string sCompteTvaAchatTP;
        string sTauxTvaAchatTP;

        string sCompte7DevisTR;
        string sCompteTvaDevisTR;
        string sTauxTvaDevisTR;


        string sCompte7DevisTP;
        string sCompteTvaDevisTP;
        string sTauxTvaDevisTP;

        string sLibelleArtDevis;

        string sAnalTrav;
        string sAnalCont;
        string sAnalDevis;

        object dbdate1;
        object dbdate2;

        string[] tabNoFacture;
        string[] tabMontantFact;

        SqlConnection cnnTrans;

        int londMsgb;

        //Const cRAPPORT = "C:\RapportFactManuel.txt"
        //Const cFACTURENONMISEAJOUR = "C:\FactureEnAttente.txt"
        //Const cRAPPORT = "W:\PACKAGES\INTRANET\RapportFactManuel.txt"
        //Const cFACTURENONMISEAJOUR = "W:\PACKAGES\INTRANET\FactureEnAttente.txt"

        //Const cCheminFichier = "W:\PACKAGES\INTRANET\erreurcontrat.txt"
        //ReportDocument CR = new ReportDocument();
        //ReportDocument CR2 = new ReportDocument();
        public struct CumulDuree
        {
            public string sDuree;
            public double dbMtMO;
            public string sdate;
            public string sCodeMO;
            public string sCodeDEP;
            public string sLibelleArtMO;
            public string sLibelleArtDEP;
            public double dbMOdep;
            public string sCpteVente;
            public string sCpteTVA;
            public int dbTaux;
            public string sAnalytique;
            public string sNoInter;
            public double dbPrixUnitaireMo;
            public double dbPrixUnitaireDep;
            public string sCpteVenteDep;
            public string sCpteTVADep;
            public int dbTauxDep;
        }
        public struct SituationDevis
        {
            public double dbTaux;
            public double dbHT;
            public string sCodeArticle;
            //Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            public string sLibelleArticle;
            //Fin Modif Mondir
            public string sCptVenteTR;
            public string sCptVenteTP;
            public string sCptTvaTR;
            public string sCptTvaTP;
        }
        const string cServClim = "CLIM";
        const string cServTrx = "TRAVAUX";
        const string cServExp = "EXPLOIT";
        const string cDefautActivite = "TSP";
        public struct Deduction
        {
            public double dbQte;
            public double dbPxUni;
            public double dbTot;
            public double dbTaux;
            public string sCpt7;
            public string sCptTva;
            public string sNofacture;
            public string sArticle;
        }

        bool blnChangeManu;
        public UserDocFacManuel()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cdmDevisPDA_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cdmDevisPDA_Click - Enter In The Function");
            //===> Fin Modif Mondir

            string[] strNoIntervention = null;


            DataTable rs = default(DataTable);
            ModAdo modAdors = new ModAdo();
            string sSQL = null;
            int i = 0;
            bool bMajInt = false;
            bool bIndex = false;


            bMajInt = true;
            try
            {

                if (!string.IsNullOrEmpty(SQLIntervention) && General.Left(txtEnCours.Text, 2) == "XX")
                {
                    //== le programme attribu un numero de facture à une intervention
                    //==  seulement si l'utilisateur à validé au moins une fois cette facture.
                    sSQL = "SELECT noFacture FROM intervention " + SQLIntervention;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    rs = modAdors.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow rsRow in rs.Rows)
                        {

                            if (string.IsNullOrEmpty(General.nz(rsRow["NoFacture"], "").ToString()))
                            {
                                bMajInt = false;
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider cette facture au moins une fois avant de consulter les factures scannées.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                            else
                            {
                                bMajInt = true;
                            }
                            //rs.MoveNext();
                        }
                    }

                }

                if (bMajInt == false)
                {

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }
                //===================================================>Tested
                sSQL = "SELECT     InterventionDoc.ID, InterventionDoc.Nointervention, InterventionDoc.NomFichier, "
                        + " InterventionDoc.Chemin, InterventionDoc.TypeFichier, " + " InterventionDoc.CreeLe , InterventionDoc.CreePar "
                        + " FROM         InterventionDoc INNER JOIN " + " Intervention ON InterventionDoc.Nointervention = Intervention.NoIntervention INNER JOIN "
                        + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture"
                        + " WHERE     (FactureManuelleEnTete.NoFacture = '" + txtEnCours.Text + "') and  TypeFichier = 'Devis travaux' ";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                i = 0;
                bIndex = false;
                if (rs.Rows.Count > 0)//tested
                {
                    foreach (DataRow rsRow in rs.Rows)//tested
                    {
                        {
                            bIndex = true;
                            Array.Resize(ref strNoIntervention, i + 1);

                            strNoIntervention[i] = General.nz(rsRow["NoIntervention"], "999999999").ToString();
                            i = i + 1;
                            // rs.MoveNext();
                        }
                    }

                }

                rs = null;


                //=== modif du 29 11 2007,ajout module readsoft (facture scannée et interprétée).

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                if (bIndex == true)//tested
                {
                    General.fc_FindDevisPDA(strNoIntervention);
                    //fc_FindFactFourn stNobonDeCommande, Me.hwnd
                }
                else//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a aucun devis liée à ce(s) interventions.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cdmDevisPDA_Click");
            }

        }

        private void Check1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Check1_KeyPress - Enter In The Function ");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                if (Check1.CheckState == CheckState.Checked)
                {
                    Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }
                else if (Check1.CheckState == 0)
                {
                    Check1.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                KeyAscii = 0;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmbService_AfterCloseUp - Enter In The Function ");
            //===> Fin Modif Mondir

            fc_LibService();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_Validated(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmbService_Validated - Enter In The Function ");
            //===> Fin Modif Mondir

            fc_LibService();

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LibService()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LibService() - Enter In The Function ");
            //===> Fin Modif Mondir

            string sSQL = null;
            DataTable rs = default(DataTable);

            try
            {
                if (string.IsNullOrEmpty(cmbService.Text))
                {
                    lblLibService.Text = "";
                    return;
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    lblLibService.Text = "";
                    return;
                }

                SAGE.fc_OpenConnSage();
                sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";

                rs = new DataTable();
                ModAdo modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                if (rs.Rows.Count > 0)
                {
                    lblLibService.Text = rs.Rows[0]["CA_Intitule"].ToString() + "";
                }

                // rs.Close();

                rs = null;

                SAGE.fc_CloseConnSage();
                modAdors.Dispose();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LibService");
            }

        }
        /// <summary>
        /// testetd
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlService()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlService() - Enter In The Function ");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            //===  retourne true si tout est correct.
            DataTable rs = default(DataTable);

            functionReturnValue = true;
            try
            {
                if (General.sServiceAnalytique != "1")
                {
                    return functionReturnValue;
                }


                if (!General.IsDate(txtDateDeFacture.Text))
                {
                    return functionReturnValue;
                }

                //=== controle si le bon de commande a été crée avant la date contenue dans dtServiceAnalytique
                //===  si vrai on n'oblige pas la saisie du champs service.
                if (Convert.ToDateTime(txtDateDeFacture.Text) < General.dtServiceAnalytique)
                {
                    return functionReturnValue;
                }


                if (string.IsNullOrEmpty(cmbService.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code service est désormais obligatoire, veuillez sélectionner un code service dans la liste déroulante.", "Saisie incomplète.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    cmbService.Focus();
                    return functionReturnValue;
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    if (!string.IsNullOrEmpty(cmbService.Text))
                    {
                        //===  controle si le code analytique service est correct.
                        SAGE.fc_OpenConnSage();
                        General.sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";
                        rs = new DataTable();
                        ModAdo modAdors = new ModAdo();
                        rs = modAdors.fc_OpenRecordSet(General.sSQL, null, null, SAGE.adoSage);

                        if (rs.Rows.Count == 0)
                        {


                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code service " + cmbService.Text + " n'existe pas, veuillez en sélectionner un autre code service dans la liste déroulante.");

                            functionReturnValue = false;
                        }

                        //rs.Close();

                        rs = null;

                        SAGE.fc_CloseConnSage();
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlService");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmbService_BeforeDropDown() - Enter In The Function ");
            //===> Fin Modif Mondir

            string sSQL = null;
            sSQL = "SELECT     CA_Num as Code, CA_Intitule AS Intitulé " + " From F_COMPTEA" + " ";
            sSQL = sSQL + ModAnalytique.fc_WhereServiceAnalytique(" Where N_Analytique = 6");
            sSQL = sSQL + " ORDER BY CA_Num";
            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    SAGE.fc_OpenConnSage();

                    sheridan.InitialiseCombo(cmbService, sSQL, "Code", true, SAGE.adoSage);

                    SAGE.fc_CloseConnSage();
                    cmbService.DisplayLayout.Bands[0].Columns[0].Width = 250;

                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbService_DropDown");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAchat_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAchat_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            try
            {
                ReportDocument CR = new ReportDocument();
                string SelectionFormula = "";

                if (SSOleDBGrid3.Rows.Count == 0)
                {
                    return;
                }

                if (File.Exists(General.gsRpt + General.ETATBONCOMMANDE))
                {
                    CR.Load(General.gsRpt + General.ETATBONCOMMANDE);
                    SelectionFormula = "{BonDeCommande.NoIntervention}= " + SSOleDBGrid3.Rows[0].Cells["NoIntervention"].Value.ToString();
                    CrystalReportFormView ReportForm = new CrystalReportFormView(CR, SelectionFormula);
                    ReportForm.Show();

                    SelectionFormula = "";
                    CR.FileName = "";
                    CR.FileName = General.ETATOLDFACTUREMANUEL;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAchat_Click");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAction_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAction_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            int Index = Convert.ToInt32((sender as Button).Tag);
            string sTypeAffichage = null;
            string sSQL = null;
            int i = 0;
            Microsoft.Office.Interop.Word.Application oWord = null;
            int lOrigTop = 0;
            bool bInstanceWord = false;

            try
            {

                if (Index == 4)
                {


                    //=== Use a running word instance if possible.
                    oWord = new Word.Application();


                    //=== Otherwise use a new instance.
                    if (oWord == null)
                    {
                        bInstanceWord = false;
                        oWord = new Word.Application();


                        if (oWord == null)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulé, Word n'est pas installé dans votre systéme.", "Word inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                    else
                    {
                        bInstanceWord = true;
                    }


                    lOrigTop = oWord.Top;
                    //oWord.Visible = False
                    var _with2 = SSOleDBGrid1;
                    if (_with2.Rows.Count > 0)//tested
                    {

                        // _with2.CtlUpdate();
                        // _with2.MoveFirst();
                        _with2.UpdateData();

                        for (i = 0; i <= _with2.Rows.Count - 1; i++)//tested
                        {
                            _with2.Rows[i].Cells["Designation"].Value = Correcteur.WSpellCheck(_with2.Rows[i].Cells["Designation"].Text, true, ref oWord);
                            _with2.UpdateData();
                            //_with2.MoveNext();
                        }

                    }

                    // Debug.Print(bInstanceWord);

                    //if (bInstanceWord == false)
                    if (oWord != null)
                    {
                        oWord.Top = lOrigTop;
                        oWord.Quit();
                        oWord = null;
                    }


                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAction_Click");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdActualiser_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdActualiser_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            try
            {
                //----Verifie si une facture est en cours


                if (string.IsNullOrEmpty(txtEnCours.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas de facture en cours", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //----Vérifie si il éxiste au moins un montant en ligne.

                var _with3 = SSOleDBGrid1;

                // _with3.CtlUpdate();
                // _with3.MoveFirst();

                boolMontantligne = false;
                for (i = 0; i <= _with3.Rows.Count - 1; i++)
                {
                    //----içi dbdate1 ne contient pas de date

                    UltraGridRow row = SSOleDBGrid1.Rows[i];

                    if (!string.IsNullOrEmpty(row.Cells["MontantLigne"].Text))//tested
                    {
                        boolMontantligne = true;
                        break;
                    }
                    if (!string.IsNullOrEmpty(row.Cells["AnaActivite"].Text))
                    {
                    }
                }
                if (boolMontantligne == false)//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour créer un pied de facture, il faut au moins un montant en ligne dans le corps de facture.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //----Dans le cas ou l'utilisateur a déja crée un corps et un pied de facture
                    //----et que celui-ci décide de supprimer le corps de facture on supprime
                    //----également le pied de facture.
                    General.Execute("DELETE FactureManuellePied From FactureManuellePied Where FactureManuellePied.CleFactureManuelle=" + longCle);
                    General.Execute("DELETE FactureManuellePiedbis From FactureManuellePiedbis Where FactureManuellePiedbis.CleFactureManuelle=" + longCle);
                    SSOleDBGrid2.DataSource = null;

                    return;
                }
                j = 0;
                strtabPoste = new string[j + 1];
                strtabPoste[j] = "";
                i = 0;
                doubtabTVA = new double[i + 1];

                k = 0;
                strTabCompte7 = new string[k + 1];
                strTabCompte7[k] = "";
                //'  ReDim doubTempTVACompte7(k)
                Y = 0;
                strTabAnaActivite = new string[Y + 1];
                strTabAnaActivite[Y] = "";

                strErrorCorps = "";
                booltemp = false;
                boolVide = false;
                boolZero = false;



                Frame3.Text = "Actualisation";
                Frame3.ForeColor = System.Drawing.ColorTranslator.FromOle(0x80ff80);

                //----verifie si il éxiste déja des champs avec la cle clefacturemanuelle, si oui
                //----une demande de suppresion est demandée.
                // Set adotemp = New ADODB.Recordset
                //     adotemp.Open "SELECT FactureManuellePiedBis.* From FactureManuellePiedBis Where FactureManuellePiedBis.CleFactureManuelle =" & longCle, adocnn, adOpenKeyset, adLockOptimistic
                // If adotemp.EOF = False Then
                //     londMsgb = MsgBox("Désirez vous supprimer et réactualiser le pied de facture", vbDefaultButton2 + vbYesNo + vbQuestion, "Réactualisation")
                //     If londMsgb = 7 Then
                //         adotemp.Close
                //         Set adotemp = Nothing
                //         Frame3.Caption = "Arrete"
                //         Frame3.ForeColor = &H8000000E
                //         Exit Sub
                //     Else
                //         adocnn.Execute "Delete * from FactureManuellePiedBis where ClefactureManuelle= " & longCle
                //     End If
                // End If
                //adotemp.Close

                SablierOnOff(true);

                // adotemp.Open "SELECT FactureManuelleDetail.Poste, FactureManuelleDetail.NumeroLigne, FactureManuelleDetail.MontantLigne, FactureManuelleDetail.TVA, FactureManuelleDetail.Compte7, FactureManuelleDetail.AnaActivite FROM FactureManuelleDetail Where FactureManuelleDetail.CleFactureManuelle =" & longCle, adocnn, adOpenForwardOnly, adLockReadOnly

                //----Routine comptant le nombre de poste, de taux de tva, de compte de classe 7
                //----dans le corps de facture.


                ///===================> Tell here everything is good !

                var _with4 = SSOleDBGrid1;
                for (longBoucle = 0; longBoucle <= _with4.Rows.Count - 1; longBoucle++)//tested
                {

                    UltraGridRow rows = SSOleDBGrid1.Rows[longBoucle];

                    if (!string.IsNullOrEmpty(rows.Cells["Montantligne"].Text))
                    {

                        if (!string.IsNullOrEmpty(rows.Cells["TVA"].Text) && General.IsNumeric(rows.Cells["TVA"].Text))
                        {
                            //cette routine stocke le nombre de TVA différente avec les montants
                            doubtempTVA = Convert.ToDouble(rows.Cells["TVA"].Text);
                            if (doubtempTVA == 0 && boolZero == false)
                            {
                                i = i + 1;
                                Array.Resize(ref doubtabTVA, i + 1);
                                boolZero = true;
                            }
                            for (v = 0; v <= doubtabTVA.Length - 1; v++)
                            {
                                if (doubtabTVA[v] == doubtempTVA)
                                {
                                    booltemp = true;
                                    break;
                                }
                            }
                            if (booltemp == false)
                            {
                                doubtabTVA[v - 1] = doubtempTVA;
                                i = i + 1;
                                Array.Resize(ref doubtabTVA, i + 1);
                            }
                            booltemp = false;
                            //cette routine compte le nombre de compte de classe7 différents.
                            strCompte7 = rows.Cells["Compte7"].Text;

                            for (v = 0; v <= strTabCompte7.Length - 1; v++)
                            {
                                if (strTabCompte7[v] == strCompte7)
                                {
                                    booltemp = true;
                                    break;
                                }
                            }
                            if (booltemp == false)
                            {
                                strTabCompte7[v - 1] = strCompte7;
                                k = k + 1;
                                Array.Resize(ref strTabCompte7, k + 1);
                                strTabCompte7[k] = "";
                            }
                            booltemp = false;
                            //cette routine stocke la nombre d'analytique activitée différentes.

                            strAnaActivite = rows.Cells["AnaActivite"].Text;

                            for (v = 0; v <= strTabAnaActivite.Length - 1; v++)
                            {
                                if (strTabAnaActivite[v] == strAnaActivite)
                                {
                                    booltemp = true;
                                    break;
                                }
                            }
                            if (booltemp == false)
                            {
                                strTabAnaActivite[v - 1] = strAnaActivite;
                                Y = Y + 1;
                                Array.Resize(ref strTabAnaActivite, Y + 1);
                                strTabAnaActivite[Y] = "";
                            }

                            booltemp = false;
                        }
                        else if (string.IsNullOrEmpty(rows.Cells["TVA"].Text))
                        {
                            strErrorCorps = "- il y a un montant en ligne sans taux de tva dans le corps de facture" + "\n";
                        }
                    }
                    else if (!string.IsNullOrEmpty(rows.Cells["TVA"].Text))
                    {
                        strErrorCorps = strErrorCorps + "- il y a un taux de TVA sans montant en ligne dans le corps de facture";
                    }
                    //cette routine stocke le nombre de poste différent sachant qu'un poste peut étre
                    //null ou contenir une chaine vide(qui est la valeur par défaut des variables
                    //strings).
                    strPoste = rows.Cells["Poste"].Text;
                    if (string.IsNullOrEmpty(strPoste) && boolVide == false)
                    {
                        j = j + 1;
                        Array.Resize(ref strtabPoste, j + 1);
                        strtabPoste[j] = "";
                        boolVide = true;
                    }
                    for (v = 0; v <= strtabPoste.Length - 1; v++)
                    {
                        if (strtabPoste[v] == strPoste)
                        {
                            booltemp = true;
                            break;
                        }
                    }
                    if (booltemp == false)
                    {
                        strtabPoste[v - 1] = strPoste;
                        j = j + 1;
                        Array.Resize(ref strtabPoste, j + 1);
                        strtabPoste[j] = "";
                    }
                    booltemp = false;
                    //    adotemp.MoveNext
                    // Loop
                    //Else
                    //    MsgBox "Vous devez fournir la table du corps avant de lancer l'actualisation", vbCritical, "opération annulée"
                    //    Frame3.Caption = "Arrete"
                    //    Frame3.ForeColor = &H8000000E
                    //    SablierOnOff False
                    //    Exit Sub
                    //End If
                    //adotemp.Close

                }


                //--------------------------------------------------
                adotemp = new DataTable();
                var tmp = new ModAdo();
                //cette routine fournis la table facturemanuellepiedbis Pour chaque poste un calcul
                //de montant HT, TTC etc... est effectué pour chaque compte de classe 7 différent.
                if (string.IsNullOrEmpty(strErrorCorps))
                {
                    SSOleDBGrid2.DataSource = null;
                    adoPied = new DataTable();
                    doubtempTVA = 0;
                    var ModadoPied = new ModAdo();
                    //'ReDim doubTabMontantHT(UBound(doubtabTVA) - 1)
                    adoPied = ModadoPied.fc_OpenRecordSet("SELECT FactureManuellePiedBis.* From FactureManuellePiedBis Where FactureManuellePiedBis.CleFactureManuelle =" + longCle);
                    //----1 ere boucle sur chaque numéro de poste.
                    if (adoPied.Rows.Count > 0)
                    {
                        foreach (DataRow r in adoPied.Rows)
                        {
                            r.Delete();
                            //adoPied.MoveNext();
                        }
                    }
                    ///==================> Mondir : Change -1 to -2 coz the last element in table is null or ""
                    for (v = 0; v <= strtabPoste.Length - 2; v++)
                    {
                        doubMontant = 0;
                        //----2 éme boucle sur chaque analytique activité.
                        for (Y = 0; Y <= strTabAnaActivite.Length - 2; Y++)
                        {
                            //----3 éme boucle sur chaque compte de la classe 7.
                            for (i = 0; i <= strTabCompte7.Length - 2; i++)

                            {
                                //----4 éme boucle sur chaque taux de TVA.
                                for (j = 0; j <= doubtabTVA.Length - 2; j++)
                                {

                                    if (string.IsNullOrEmpty(strtabPoste[v]))
                                    {


                                        SQL = "SELECT FactureManuelleDetail.Poste, FactureManuelleDetail.NumeroLigne, FactureManuelleDetail.Designation," +
                                        " FactureManuelleDetail.MontantLigne, FactureManuelleDetail.TVA, FactureManuelleDetail.Compte7 "
                                        + "From FactureManuelleDetail Where (FactureManuelleDetail.CleFactureManuelle =" + longCle
                                        + " and Poste ='' and Compte7 ='" + StdSQLchaine.gFr_NombreAng(strTabCompte7[i])
                                        + "' AND TVA = " + StdSQLchaine.gFr_NombreAng(doubtabTVA[j]) + " and AnaActivite ='"
                                        + StdSQLchaine.gFr_DoublerQuote(strTabAnaActivite[Y]) + "')or(FactureManuelleDetail.CleFactureManuelle =" + longCle
                                        + " and Poste is null and Compte7 ='" + strTabCompte7[i] + "' AND TVA = " + StdSQLchaine.gFr_NombreAng(doubtabTVA[j])
                                        + " and AnaActivite ='" + StdSQLchaine.gFr_DoublerQuote(strTabAnaActivite[Y]) + "')";

                                    }
                                    else
                                    {

                                        SQL = "SELECT FactureManuelleDetail.Poste, FactureManuelleDetail.NumeroLigne, FactureManuelleDetail.Designation,"
                                        + " FactureManuelleDetail.MontantLigne, FactureManuelleDetail.TVA, FactureManuelleDetail.Compte7 From FactureManuelleDetail" +
                                        " Where FactureManuelleDetail.CleFactureManuelle =" + longCle + " and Poste ='" + StdSQLchaine.gFr_DoublerQuote(strtabPoste[v])
                                        + "' and compte7 ='" + strTabCompte7[i] + "' AND TVA = " + StdSQLchaine.gFr_NombreAng(doubtabTVA[j])
                                        + " and AnaActivite ='" + StdSQLchaine.gFr_DoublerQuote(strTabAnaActivite[Y]) + "'";
                                    }

                                    adotemp = tmp.fc_OpenRecordSet(SQL);
                                    doubMontant = 0;
                                    if (adotemp.Rows.Count > 0)
                                    {
                                        foreach (DataRow dr in adotemp.Rows)
                                        {
                                            doubMontant = General.FncArrondir(doubMontant + Convert.ToDouble(General.nz(dr["MontantLigne"].ToString(), 0)), 2);
                                            // adotemp.MoveNext();
                                        }
                                        // adoPied.AddNew();


                                        var newRow = adoPied.NewRow();
                                        newRow["CleFactureManuelle"] = longCle;

                                        newRow["Poste"] = strtabPoste[v];

                                        newRow["MontantHT"] = doubMontant;
                                        newRow["TVA"] = doubtabTVA[j];
                                        newRow["MontantTVA"] = General.FncArrondir(Convert.ToDouble(doubMontant * (doubtabTVA[j] / 100)), 2);
                                        newRow["MontantTTC"] = General.FncArrondir(Convert.ToDouble(doubMontant + (doubMontant * (doubtabTVA[j] / 100))), 2);
                                        newRow["Compte7"] = strTabCompte7[i];
                                        newRow["AnaActivite"] = strTabAnaActivite[Y];

                                        adoPied.Rows.Add(newRow);
                                        SSOleDBGrid2.DataSource = adoPied;
                                        var xx = ModadoPied.Update();

                                    }

                                    adotemp.Dispose();
                                }
                            }
                        }
                    }
                    // If UBound(doubtabTVA) = 0 Then GoTo RECOM
                    doubTabMontantHT = new double[doubtabTVA.Length - 1];
                    doubMontant = 0;
                    for (i = 0; i <= doubtabTVA.Length - 2; i++)
                    {
                        SQL = "SELECT FactureManuelleDetail.MontantLigne From FactureManuelleDetail Where FactureManuelleDetail.CleFactureManuelle ="
                            + longCle + " and TVA = " + StdSQLchaine.gFr_NombreAng(doubtabTVA[i]);
                        adotemp = tmp.fc_OpenRecordSet(SQL);
                        foreach (DataRow dr in adotemp.Rows)
                        {
                            doubMontant = General.FncArrondir(Convert.ToDouble(General.nz(dr["MontantLigne"], 0)), 2);
                            //=== modif du 22 janvier 2008, ajout de la fonction arrondir
                            doubTabMontantHT[i] = General.FncArrondir(doubTabMontantHT[i] + doubMontant, 2);
                            //  adotemp.MoveNext();
                        }
                        adotemp.Dispose();
                    }
                }
                else//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(strErrorCorps, "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    Frame3.Text = "Arrete";
                    Frame3.ForeColor = System.Drawing.ColorTranslator.FromOle(0x8000000);
                    strtabPoste = null;

                    adotemp = null;
                    doubtabTVA = null;
                    strTabCompte7 = null;
                    doubTempTVACompte7 = null;
                    SablierOnOff(false);
                    return;
                }

                //  adoTemp.Close
                strtabPoste = null;

                //cette routine stocke les montants HT, TTC etc... totals ainsi que pour chaque taux
                //de TVA différents dans la table Facturemanuellepied.
                adotemp = tmp.fc_OpenRecordSet("SELECT FactureManuellePied.* From FactureManuellePied Where FactureManuellePied.CleFactureManuelle =" + longCle);
                doubMontantHT = 0;
                doubMontantTVA = 0;
                doubMontantTTC = 0;
                if (adotemp.Rows.Count > 0)
                {
                    foreach (DataRow dr in adotemp.Rows)
                    {
                        dr.Delete();

                    }
                }
                var _with5 = adotemp;
                var adotempRow = _with5.NewRow();

                /// Probeleme etat de crystal report, il faut initialiser les champs avec 0 sinon on aura le symbole % vide
                adotempRow["TVA"] = 0;
                for (i = 1; i <= 5; i++)
                {
                    adotempRow["STMontantHT" + i] = 0;
                    adotempRow["TVA" + i] = 0;
                    adotempRow["STMontantTVA" + i] = 0;
                    adotempRow["STMontantTTC" + i] = 0;
                }

                for (i = 0; i <= doubtabTVA.Length - 2; i++)
                {

                    //adocnn.Execute "INSERT INTO FactureManuellePied ( cleFactureManuelle, STMontantHT" & i + 1 & "," & " TVA" & i + 1 & "," & " STMontantTVA" & i + 1 & "," & " STMontantTTC" & i + 1 & ") Values (" & longCle & doubTabMontantHT(i) & "," & doubtabTVA(i) & "," & doubTabMontantHT(i) * (doubtabTVA(i) / 100) & "," & doubTabMontantHT(i) + doubTabMontantHT(i) * (doubtabTVA(i) / 100) & ")"


                    // if (_with5.Rows.Count == 0)
                    //  {
                    // if (i == 0)
                    //{
                    // adotemp.AddNew();

                    adotempRow["CleFactureManuelle"] = longCle;
                    //}
                    // }

                    //var STMontantTVA = General.FncArrondir(doubTabMontantHT[i] * (doubtabTVA[i] / 100), 2);
                    //var STMontantTTC = General.FncArrondir(doubTabMontantHT[i] + STMontantTVA, 2);


                    if (_with5.Columns.Cast<DataColumn>().Any(col => col.ColumnName.ToUpper() == ("STMontantHT" + Convert.ToInt32(i + 1)).ToUpper()))
                        adotempRow["STMontantHT" + Convert.ToInt32(i + 1)] = doubTabMontantHT[i];
                    if (_with5.Columns.Cast<DataColumn>().Any(col => col.ColumnName.ToUpper() == ("TVA" + Convert.ToInt32(i + 1)).ToUpper()))
                        adotempRow["TVA" + Convert.ToInt32(i + 1)] = doubtabTVA[i];
                    if (_with5.Columns.Cast<DataColumn>().Any(col => col.ColumnName.ToUpper() == ("STMontantTVA" + Convert.ToInt32(i + 1)).ToUpper()))
                        adotempRow["STMontantTVA" + Convert.ToInt32(i + 1)] = General.FncArrondir(doubTabMontantHT[i] * (doubtabTVA[i] / 100), 2);
                    if (_with5.Columns.Cast<DataColumn>().Any(col => col.ColumnName.ToUpper() == ("STMontantTTC" + Convert.ToInt32(i + 1)).ToUpper()))
                        adotempRow["STMontantTTC" + Convert.ToInt32(i + 1)] = General.FncArrondir(doubTabMontantHT[i] + doubTabMontantHT[i] * (doubtabTVA[i] / 100), 2);
                    doubMontantHT = doubMontantHT + doubTabMontantHT[i];
                    doubMontantTVA = doubMontantTVA + General.FncArrondir(doubTabMontantHT[i] * (doubtabTVA[i] / 100), 2);
                    doubMontantTTC = doubMontantHT + doubMontantTVA;
                }
                adotempRow["MontantHT"] = doubMontantHT;

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (doubMontantHT >= 0)
                {
                    adotempRow["FactureAvoir"] = cFacture;
                }
                else
                {
                    adotempRow["FactureAvoir"] = cAvoir;
                }
                //===> Fin Modif Mondir

                if (optDevis.Checked == true)
                {

                    lblTotalSituations.Text = Convert.ToString(Convert.ToDouble(General.nz(txtSituation.Text, "0")) + doubMontantHT) + " €";
                }
                else if (optIntervention.Checked)
                {
                    lblMontantDevis.Text = Convert.ToString(doubMontantHT) + " €";
                    string achat = lblTotalAchats.Text.Remove(lblTotalAchats.Text.Length - 1);
                    if ((Convert.ToDouble(General.nz(achat, "0")) + Convert.ToDouble(General.nz(txtCharges.Text, "0"))) != 0)
                    {

                        lblTotalSituations.Text = Convert.ToString(General.FncArrondir(doubMontantHT / (Convert.ToDouble(General.nz(lblTotalAchats.Text.Replace("€", ""), "0")) + Convert.ToDouble(General.nz(txtCharges.Text, "0")))));
                    }
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblVentesReelles.Text = Convert.ToDouble(Convert.ToDouble(General.nz(txtVenteMem.Text, "0")) + doubMontantHT).ToString() + " €";
                if (Convert.ToDouble(General.nz(lblChargesReelles.Text, "0")) != 0)
                {
                    lblCoefficientReel.Text = (Convert.ToDouble(General.nz(lblVentesReelles.Text.Replace("€", ""), 0)) /
                                               Convert.ToDouble(General.nz(lblChargesReelles.Text, 0))).ToString();
                }
                lblCoefficientReel.Text = General
                    .FncArrondir(Convert.ToDouble(General.nz(lblCoefficientReel.Text, 0)), 3).ToString("##.000");
                //===> Fin Modif Mondir



                adotempRow["MontantTVA"] = doubMontantTVA;
                //adotempRow["MontantTTC"] = doubMontantTTC;
                adotempRow["MontantTTC"] = doubMontantTTC;
                //la doubtempTVA sert içi à stocker le total HT
                _with5.Rows.Add(adotempRow.ItemArray);
                var xxx = tmp.Update();
                adotemp.Dispose();

                adotemp = null;
                doubtabTVA = null;
                strTabCompte7 = null;
                doubTempTVACompte7 = null;
                //Timer1.Enabled = True
                SablierOnOff(false);
                Frame3.Text = "Arrete";
                Frame3.ForeColor = System.Drawing.ColorTranslator.FromOle(0x8000000);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdActualiser_Click");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAjouter_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            //Dans la procédure cmdAjouter_Click est effectuer une recherche
            //du maximum du champs CleFactureManuelle puis un ajout et une validation
            //à l'insu de l'utilisateur afin d'initialiser les tableaux des onglets
            //CORPS DE FACTURE et ARRETE. Si l'utilisateur décide de ne pas
            //valider cette facture alors celle-çi sera supprimer (d'ou l'utilisation
            //de la variable boolAddnew).
            //la stucture çi-dessous vérifie l'état de adomanuel si celui-çi est
            //ouvert donc un recordset éxiste déja sinon un nouveau sera crée.

            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, "cmdAjouter_Click - L'ajout d'une nouvelle facture");
            //===> Fin Modif Mondir

            i = 0;
            try
            {
                if (General.sEvolution == "1")
                {
                    //Modif 17-07-2020 ajouté par salma 
                    if (bButtonAdd == false && (optIntervention.Checked || optDevis.Checked))
                    {
                        CustomMessageBox.Show("Vous ne pouvez pas facturer une intervention ou un devis directement depuis la fiche de facturation manuelle," + "\t" + "rendez-vous sur une intervention et cliquez sur le bouton 'facture'.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //Fin modif 
                Recom:

                SablierOnOff1(true);
                lblP3.Visible = false;
                lblChaudCondensation.Visible = false;

                frmSynthese.Visible = false;
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                fraSynthese.Visible = false;
                //===> Fin modif Mondir


                //----boolinsertInterv correspond aux interventions insérées(mode intervention)
                boolinsertInterv = false;
                if (boolIntervention == true)//tested
                {
                    SSTab1.Tabs[4].Visible = true;
                    SSOleDBGrid3.DataSource = null;
                    cmdRechImmeuble.Enabled = true;
                    cmdInserer.Enabled = true;
                }

                //----Recherche le numero de cle de facture le plus grand.
                adoManuel = new DataTable();
                ModAdo ModadoManuel = new ModAdo();

                adoManuel = ModadoManuel.fc_OpenRecordSet("SELECT Max(FactureManuelleEntete.CleFactureManuelle) AS MaxDeCleFactureManuelle FROM FactureManuelleEntete");

                if (adoManuel.Rows.Count > 0)//tested
                {
                    if (string.IsNullOrEmpty(adoManuel.Rows[0]["MaxDeCleFactureManuelle"].ToString()) && !General.IsNumeric(adoManuel.Rows[0]["MaxDeCleFactureManuelle"].ToString()))
                    {
                        longCle = 1;
                    }
                    else
                    {
                        longCle = Convert.ToInt32(adoManuel.Rows[0]["MaxDeCleFactureManuelle"]);
                    }
                }

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdAjouter_Click - longCle = {longCle}");
                //===> Fin Modif Mondir

                adoManuel = null;

                //----Ajoute + 1 à ce numéro puis l'insére dans la table factureManuelleEnTete.
                //adoManuel.Open "FactureManuelleEntete", adocnn, adOpenDynamic, adLockOptimistic
                srSQL = "";

                adoManuel = ModadoManuel.fc_OpenRecordSet("Select * from FactureManuelleEntete where CleFactureManuelle = 0");
                boolAddnew = true;

                //adoManuel.AddNew();
                var NewRow = adoManuel.NewRow();
                //adoManuel.CancelUpdate
                longCle = longCle + 1;
                NewRow["CleFactureManuelle"] = longCle;
                NewRow["NoFacture"] = "XX" + longCle;
                NewRow["TypeFacture"] = TypeFacture;
                NewRow["Nompersonne"] = strNomPersonne;
                NewRow["ModeReglement"] = "CH000";
                NewRow["LibelleReglement"] = "Chèque à réception";
                NewRow["DateFacture"] = DateTime.Today.ToShortDateString();
                NewRow["CodeUO"] = "01";
                NewRow["comptabilisee"] = "0";
                NewRow["ExportPDF"] = "0";
                //adoManuel!dateFacture = Date
                adoManuel.Rows.Add(NewRow);

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdAjouter_Click - NoFacture = XX{longCle}");
                //===> Fin Modif Mondir

                //---- si ce numero exixte déja provoquant ainsi une erreur dans la base de donéees.
                //'on error Resume Next
                var up = ModadoManuel.Update();

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdAjouter_Click - Table FactureManuelleEntete Updated = {up}");
                //===> Fin Modif Mondir

                /*  while (Err().Number != 0)
                  {
                      Erreurs.gFr_debug(this.Name + ";cmdAjouter_Click_boucle_sur_No");
                      Err().Clear();
                      adoManuel.CancelUpdate();
                      adoManuel.AddNew();
                      longCle = longCle + 1;
                      adoManuel.Fields("CleFactureManuelle").Value = longCle;
                      adoManuel.Fields("NoFacture").Value = "XX" + longCle;
                      adoManuel.Fields("TypeFacture").Value = TypeFacture;
                      adoManuel.Fields("Nompersonne").Value = strNomPersonne;
                      adoManuel.Fields("ModeReglement").Value = "CH000";
                      adoManuel.Fields("LibelleReglement").Value = "Chèque à réception";
                      adoManuel.Fields("DateFacture").Value = Microsoft.VisualBasic.Compatibility.VB6.Support.Format(DateAndTime.Today, General.FormatDateSansHeureSQL);
                      adoManuel.Update();
                  }*/


                adoManuel.Dispose();

                adoManuel = null;

                fc_LockCpt();

                //----Affiche le numero Temporaire.
                txtEnCours.Text = "XX" + longCle;
                //----initialisation des tableaux
                //'on error GoTo 0
                //Adodc1.RecordSource = "SELECT FactureManuelleDetail.* From FactureManuelleDetail Where FactureManuelleDetail.CleFactureManuelle =" & longCle & " order by NumeroLigne"
                //Adodc1.Refresh
                //'on error GoTo Erreur
                SSOleDBGrid1.DataSource = null;
                SSOleDBGrid2.DataSource = null;
                //Adodc2.RecordSource = "SELECT FactureManuellePiedBis.* From FactureManuellePiedBis Where FactureManuellePiedBIS.CleFactureManuelle =" & longCle
                //Adodc2.Refresh

                FirstRemplitGrille();
                SecondRemplitGrille();
                DataTable dt = new DataTable();
                RemplitGrille(dt);
                fc_ChargeContrat("0");
                InitialiseEcran(this);

                //---Boque les boutons inutiles.
                cmdSupprimer.Enabled = false;
                cmdAnnuler.Enabled = true;
                cmdValider.Enabled = true;
                cmdAjouter.Enabled = false;
                CmdRechercher.Enabled = false;
                cmdRechercheImmeuble.Enabled = true;
                cmdRechercheReg.Enabled = true;

                //---Valeur par defaut dans l'Entete de facture.

                retabliEcran(this);
                txtDateDeFacture.Text = DateTime.Today.ToShortDateString();
                //== modif du 07 septembre 2007
                //txtMode = "CH000"
                //txtModeLibelle = "Chèque à réception"

                Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;

                //'on error Resume Next
                if (boolIntervention == false)
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    txtCodeimmeuble.Focus();
                }
                else
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[4];
                    txtImmeuble.Enabled = true;
                    txtImmeuble.Focus();
                }
                SablierOnOff1(false);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAjouter_Click");
            }

            return;
            //err:
            //  If err.Number = -2147217887 Then
            //-----numéro d'erreur sur les doublons.
            //        If i < 2 Then
            //            adoManuel.CancelUpdate
            //            adoManuel.Close
            //            Set adoManuel = Nothing
            //            err.Clear
            //            err.Number = 0
            //            GoTo RECOM
            //        Else
            //            MsgBox "Une erreur est survenue dans un champs clé primaire," & vbCrLf & "Veuillez fermer cette application et recommencer.", vbCritical, "Erreur dans index"
            //           adoManuel.CancelUpdate
            //            adoManuel.Close
            //            Set adoManuel = Nothing
            //            Exit Sub
            //        End If
            //   End If
        }
        private void faux()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"faux() - Enter In The Function ");
            //===> Fin Modif Mondir
            try
            {
                General.Execute("INSERT INTO FactureManuelleEntete ( CleFactureManuelle) values (" + longCle + ") ");

                // adoManuel.Update
                // adoManuel.CancelUpdate
                //      adoManuel.AddNew
                //      adoManuel!CleFactureManuelle = longCle + 1
                //      adoManuel.Update

                //adoManuel.Close
                //Set adoManuel = Nothing
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";faux");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnalytique_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAnalytique_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            string sSQL = null;
            DataTable rs = default(DataTable);
            string sCRWhere = null;
            int[] tAffaire = null;
            int i = 0;
            int lnumfichestandard = 0;
            string SelectionFormula = "";
            ReportDocument CRAnALY = new ReportDocument();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

            sSQL = "SELECT   Intervention.Numfichestandard"
                + " FROM         FactureManuelleEnTete INNER JOIN"
                + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture"
                + " WHERE     FactureManuelleEnTete.Facture = 1 AND "
                + " FactureManuelleEnTete.Imprimer = 1  " + " AND FactureManuelleEnTete.TypeFacture = '" + TypeFacture + "' ";
            try
            {
                if (optPersonne.Checked == true)//tested
                {
                    sSQL = sSQL + " AND FactureManuelleEnTete.NomPersonne ='" + strNomPersonne + "'";

                }
                else if (OptNomChoisis.Checked == true)
                {
                    sSQL = sSQL + " AND FactureManuelleEnTete.NomPersonne ='" + Combo1.Text + "'";

                }
                else if (OptTous.Checked == true)
                {
                    //== pas de critere.
                }


                //== par date.
                if (Option2.Checked == true)//tested
                {
                    sSQL = sSQL + " AND FactureManuelleEnTete.DateFacture >='" + txtMiseDate1.Text + "'" + " AND FactureManuelleEnTete.DateFacture <= '" + txtMiseDate2.Text + "'";

                    //== par numero (clefacturemanuelle).
                }
                else if (Option7.Checked == true)//tested
                {
                    sSQL = sSQL + " AND FactureManuelleEntete.CleFactureManuelle >= " + General.nz(txtMiseAjour1.Text, 0)
                    + " And FactureManuelleEntete.CleFactureManuelle<= " + General.nz(txtMiseAjour2.Text, 0);

                    //== facture en cours.
                }
                else if (Option8.Checked == true)
                {
                    sSQL = sSQL + " AND FactureManuelleEntete.CleFactureManuelle = " + longCle + "";

                }

                ModAdo ModAdors = new ModAdo();
                rs = ModAdors.fc_OpenRecordSet(sSQL + "order by Numfichestandard");

                if (rs.Rows.Count == 0)//tested
                {
                    rs = null;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement trouvé.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sCRWhere = "";
                i = 0;
                lnumfichestandard = 0;

                if (rs.Rows.Count > 0)//tested
                {
                    foreach (DataRow r in rs.Rows)
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                        if (lnumfichestandard != Convert.ToInt32(General.nz(r["Numfichestandard"], 0)))//tested
                        {
                            Array.Resize(ref tAffaire, i + 1);

                            tAffaire[i] = Convert.ToInt32(General.nz(r["Numfichestandard"], 0));
                            i = i + 1;

                            lnumfichestandard = Convert.ToInt32(General.nz(r["Numfichestandard"], 0));
                            if (string.IsNullOrEmpty(sCRWhere))//tested
                            {

                                sCRWhere = "( {ETAE_AffaireEntete.Numfichestandard} = " + General.nz(r["Numfichestandard"], 0);
                            }
                            else//tested
                            {

                                sCRWhere = sCRWhere + " OR {ETAE_AffaireEntete.Numfichestandard} = " + General.nz(r["Numfichestandard"], 0);
                            }

                        }
                    }

                }

                rs = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                if (string.IsNullOrEmpty(sCRWhere))
                    return;
                sCRWhere = sCRWhere + " ) and {ETAE_AffaireEntete.Utilisateur} = '" + General.fncUserName() + "'";



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModAnalytique.fc_AnalyAffaire(tAffaire, "", "", 1, true);


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //== requete pour l'état.
                if (File.Exists(General.ETATANALYTIQUE2FACTURE))
                {
                    CRAnALY.Load(General.ETATANALYTIQUE2FACTURE);
                    SelectionFormula = sCRWhere;
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    CrystalReportFormView ReportForm = new CrystalReportFormView(CRAnALY, SelectionFormula);
                    ReportForm.Show();

                }

                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";Cmdanalytique_Click");
                if (ex.InnerException != null)
                    Program.SaveException(null, "error in crystal report function Cmdanalytique_Click ===> details : " + ex.InnerException.Message);
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAnnuler_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            int londMsgb = 0;
            try
            {
                if (boolFermeture == false)
                {

                    DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous annuler cette facture?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    londMsgb = Convert.ToInt32(dg);
                }

                if (londMsgb != 7 || boolFermeture == true)
                {
                    if (boolAddnew == true)
                    {
                        SablierOnOff1(true);
                        General.Execute("Delete  from FactureManuelleEnTete where CleFactureManuelle = " + longCle);
                        cmdAjouter.Enabled = true;
                        CmdRechercher.Enabled = true;
                        cmdAnnuler.Enabled = false;
                        cmdValider.Enabled = false;
                        cmdSupprimer.Enabled = false;
                        lblP3.Visible = false;
                        lblChaudCondensation.Visible = false;
                        //Timer1.Enabled = True
                        SSOleDBGrid3.DataSource = null;
                        DataTable dt = new DataTable();
                        RemplitGrille(dt);
                        boolinsertInterv = true;
                        cmdInserer.Enabled = true;
                        cmdRechercheImmeuble.Enabled = false;
                        cmdRechercheReg.Enabled = false;
                        InitialiseEcran(this);
                        strTRouTP = "";
                        txtEnCours.Text = "";
                        txtTVA.Text = "";
                        longCle = 0;
                        frmSynthese.Visible = false;
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        fraSynthese.Visible = false;
                        //===> Fin Modif Mondir
                        if (boolIntervention == true)
                        {
                            SQLIntervention = "";
                            SSTab1.SelectedTab = SSTab1.Tabs[4];
                        }
                        else
                        {
                            SSTab1.SelectedTab = SSTab1.Tabs[0];
                        }
                        boolAddnew = false;
                        SablierOnOff1(false);
                        //'on error Resume Next
                        // cmdAjouter.SetFocus
                    }
                }

                //'on error Resume Next
                //cmdAnnuler.SetFocus
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAnnuler_Click");
                return;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdArticleGecet_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdArticleGecet_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            SSOleDBGrid1.UpdateData();
            if (General.fncUserName() == "mohammed")
                General.sGecetV3 = "1";
            if (General.sGecetV3 == "1")//tested
            {
                frmGecetV3 frmGecet3 = new frmGecetV3();
                var _with6 = frmGecet3;
                General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.fncUserName() + "'");
                _with6.txtCle.Text = "1";
                _with6.txtOrigine.Text = Variable.cUserDocFacManuel;
                _with6.txtType.Text = "1";
                _with6.txtKFO.Text = "1";
                _with6.txtKMO.Text = "1";
                _with6.txtKST.Text = "1";
                _with6.txtPxHeure.Text = "1";
                _with6.txtTVA.Text = "1";
                _with6.txtType.Text = "0";
                //_with6.CmdPanier.Visible = false;
                //_with6.cmdAppliquer.Visible = false;
                //_with6.cmdAppliqueClose.Visible = false;
                General.gsUtilisateur = General.fncUserName();
                _with6.ShowDialog();
                frmGecet3.Close();
                if ((SSOleDBGrid1.ActiveRow != null && SSOleDBGrid1.ActiveRow.IsAddRow))
                    boolAddrow = true;
                else
                    boolAddrow = false;
                BisRechercheGecet();

            }

            else if (General.sGecetV2 == "1")//tested
            {
                frmGecet2 frmGecet2 = new frmGecet2();
                var _with6 = frmGecet2;
                General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                _with6.txtCle.Text = "1";
                _with6.txtOrigine.Text = Variable.cUserDocFacManuel;
                _with6.txtType.Text = "1";
                _with6.txtKFO.Text = "1";
                _with6.txtKMO.Text = "1";
                _with6.txtKST.Text = "1";
                _with6.txtPxHeure.Text = "1";
                _with6.txtTVA.Text = "1";
                _with6.txtType.Text = "0";
                _with6.CmdPanier.Visible = false;
                _with6.cmdAppliquer.Visible = false;
                _with6.cmdAppliqueClose.Visible = false;
                _with6.ShowDialog();

                frmGecet2.Close();
            }
            else//tested
            {
                frmGecet frmGecet = new frmGecet();
                var _with7 = frmGecet;
                General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                _with7.txtCle.Text = "1";
                _with7.txtOrigine.Text = Variable.cUserDocFacManuel;
                _with7.txtType.Text = "1";
                _with7.txtKFO.Text = "1";
                _with7.txtKMO.Text = "1";
                _with7.txtKST.Text = "1";
                _with7.txtPxHeure.Text = "1";
                _with7.txtTVA.Text = "1";
                _with7.txtType.Text = "0";
                _with7.CmdPanier.Visible = false;
                _with7.cmdAppliquer.Visible = false;
                _with7.cmdAppliqueClose.Visible = false;
                _with7.ShowDialog();

                frmGecet.Close();
            }
        }
        /// <summary>
        /// tested
        /// Modif de la version 02.07.2020(V2) ajoutées par Mondir
        /// Modif de la version 02.07.2020(V2) testées par Mondir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExport_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdExport_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            //===> Mondir, Modifs de la version 02.07.2020(V2)
            if (!OptFactEnCours.Checked)
            {
                CustomMessageBox.Show("Vous ne pouvez exporter que la facture en cours.", "",
                    MessageBoxIcon.Information);
                return;
            }
            //===> Fin Modif Mondir
            fc_Export(false);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdfactScann_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdfactScann_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            string[] stNobonDeCommande = null;


            DataTable rs = default(DataTable);
            ModAdo modAdoRs = new ModAdo();
            string sSQL = null;
            int i = 0;
            bool bMajInt = false;
            bool bIndex = false;



            bMajInt = true;
            try
            {
                if (!string.IsNullOrEmpty(SQLIntervention) && General.Left(txtEnCours.Text, 2) == "XX")
                {
                    //== le programme attribu un numero de facture à une intervention
                    //==  seulement si l'utilisateur à validé au moins une fois cette facture.
                    sSQL = "SELECT noFacture FROM intervention " + SQLIntervention;
                    rs = modAdoRs.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow rowRs in rs.Rows)
                        {
                            if (string.IsNullOrEmpty(General.nz(rowRs["NoFacture"], "").ToString()))
                            {
                                bMajInt = false;
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider cette facture au moins une fois avant de consulter les factures scannées.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                            else
                            {
                                bMajInt = true;
                            }
                        }
                    }

                }

                if (bMajInt == false)
                    return;
                ////======================>tested
                sSQL = "SELECT     BonDeCommande.NoBonDeCommande, Intervention.NoIntervention," + " FactureManuelleEnTete.NoFacture"
                    + " FROM         Intervention INNER JOIN" + " BonDeCommande " + " ON Intervention.NoIntervention = BonDeCommande.NoIntervention "
                    + " INNER JOIN" + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture"
                    + " WHERE     (FactureManuelleEnTete.NoFacture = '" + txtEnCours.Text + "')";

                rs = modAdoRs.fc_OpenRecordSet(sSQL);
                i = 0;
                bIndex = false;
                if (rs.Rows.Count > 0)//tested
                {
                    foreach (DataRow rowRs in rs.Rows)
                    {
                        bIndex = true;
                        Array.Resize(ref stNobonDeCommande, i + 1);

                        stNobonDeCommande[i] = General.nz(rowRs["NoBonDeCommande"].ToString(), "999999999").ToString();
                        i = i + 1;
                        // rs.MoveNext();
                    }
                }
                rs.Dispose();

                rs = null;


                //=== modif du 29 11 2007,ajout module readsoft (facture scannée et interprétée).

                if (bIndex == true)//tested
                {
                    General.fc_FindFactFournRD(stNobonDeCommande);
                    //fc_FindFactFourn stNobonDeCommande, Me.hwnd
                }
                else//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a aucune facture fournisseur liée à ce(s) interventions.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdfactScann_Click");
            }


        }
        /// <summary>
        /// tested mais il faut vérifier cette frmAfficheObjet
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdFax_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"CmdFax_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            try
            {
                Forms.frmAfficheObjet frmAfficheObjet = new Forms.frmAfficheObjet();

                if (SSOleDBGrid3.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n' y a pas d'intervention en cours");
                    //'on error Resume Next
                    CmdFax.Focus();
                    return;
                }
                //----Verifie si une facture est en cours

                adotemp = new DataTable();
                ModAdo tmpAdotemp = new ModAdo();

                adotemp = tmpAdotemp.fc_OpenRecordSet("SELECT GestionStandard.CodeOrigine1, GestionStandard.Document,"
                    + " GestionStandard.NumFichestandard" + " FROM GestionStandard INNER JOIN Intervention ON"
                    + " GestionStandard.NumFicheStandard = " + " Intervention.NumFicheStandard"
                    + " WHERE Intervention.NoIntervention=" + SSOleDBGrid3.Rows[0].Cells["NoIntervention"].Value.ToString());

                if (adotemp.Rows.Count > 0)
                {
                    if (adotemp.Rows[0]["CodeOrigine1"].ToString() == "1" || adotemp.Rows[0]["CodeOrigine1"].ToString() == "4")
                    {

                        if (!string.IsNullOrEmpty(adotemp.Rows[0]["Document"].ToString()))
                        {
                            frmAfficheObjet.OLE1.Text = adotemp.Rows[0]["Document"].ToString();
                            //TODO
                            //cette frm affiche le contenu du document 
                            //  frmAfficheObjet.OLE1.CreateLink(adotemp.Rows[0]["Document"].ToString());todo
                            //  frmAfficheObjet.OLE1.DoVerb();todo
                            frmAfficheObjet.ShowDialog();
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("l'option fax ou courrier a été sélectionnée dans la fiche standard n° " + adotemp.Rows[0]["Numfichestandard"].ToString() + "\n" + "mais aucun document n'est présent", "Document manquant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }

                    }
                    adotemp.Dispose();
                    tmpAdotemp.Dispose();
                    adotemp = null;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdFax_Click");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHistorique_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdHistorique_Click() - Enter In The Function ");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(txtImmeuble.Text))
            {
                General.NomImmeuble = txtImmeuble.Text;
                frmHistorique frmHistorique = new frmHistorique();
                frmHistorique.ShowDialog();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="CleFac"></param>
        /// <returns></returns>
        private short RechDate(int CleFac)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"RechDate() - Enter in Function - CleFac = {CleFac}");
            //===> Fin Modif Mondir

            short functionReturnValue = 0;
            //=== si rechdate1 = 0 ancien état
            //=== si rechdate1 = 1 en V2
            //=== si rechdate1 = 2 en V3

            try
            {
                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> ajut du : !General.IsDate(General.dtdateFactManuV3
                if (!General.IsDate(General.sDateFactureManuelleV2) && !General.IsDate(General.dtdateFactManuV3))
                {
                    functionReturnValue = 0;
                    return functionReturnValue;
                }

                rsDate = new DataTable();
                ModAdo rsDateModAdo = new ModAdo();
                rsDate = rsDateModAdo.fc_OpenRecordSet("SELECT FactureManuelleEntete.DateFacture" + " From FactureManuelleEntete"
                    + " WHERE FactureManuelleEntete.CleFactureManuelle=" + CleFac);

                if (rsDate.Rows.Count > 0)
                {
                    if (General.IsDate(rsDate.Rows[0]["DateFacture"].ToString()))
                    {
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                        if (Convert.ToDateTime(rsDate.Rows[0]["DateFacture"]) >= Convert.ToDateTime(General.dtdateFactManuV3))
                        {
                            //=== nouvel état au 16 / 12 / 2020.
                            functionReturnValue = 2;
                        }
                        //Fin Modif Mondir
                        else if (Convert.ToDateTime(rsDate.Rows[0]["DateFacture"]) >= Convert.ToDateTime(General.sDateFactureManuelleV2))
                        {
                            functionReturnValue = 1;
                        }
                        else
                        {
                            functionReturnValue = 0;
                        }
                    }
                    else
                    {
                        functionReturnValue = 0;
                    }
                }
                rsDate.Dispose();
                rsDateModAdo.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RechDate");
                return functionReturnValue;
            }


        }
        /// <summary>
        /// Tested mais il faut  verifier dans le cas du chargement du rapport  CR2 ,
        /// pour le moment j'ai pas ce rapport dans le dossier des rapports et osi dans la  version VB6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdImprimer_Click() - Enter in Function");
            //===> Fin Modif Mondir

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            string sSQLTVA = "";
            DataTable rsTVA300 = null;
            ModAdo rsTVA300ModAdo = new ModAdo();
            bool bLoadGrille = false;
            //===> Fin Modif Mondir

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            int lVersionDate;
            string sFileReport = "";
            //===> Fin Modif Mondir

            string SelectionFormula = "";
            ReportDocument CR = new ReportDocument();
            ReportDocument CR2 = new ReportDocument();

            try
            {
                if (CR.FileName == "")//tested
                {
                    if (File.Exists(General.ETATOLDFACTUREMANUEL))
                    {
                        CR.Load(General.ETATOLDFACTUREMANUEL);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'exsite pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }

                if (CR2.FileName == "")
                {
                    if (File.Exists(General.ETATFacturemanuelleSQLv2))
                        CR2.Load(General.ETATFacturemanuelleSQLv2);//todo J'ai besoin de ce rapport car il n'existe pas dans le chemin spécifié dans DATA...
                }


                SablierOnOff1(true);
                SQLTypeFacture = "";
                SQL = "";
                boolErreurGeneral = false;
                //----stocke la requete contenant les champs à verifier
                sCodeFamille = "SELECT FactureManuelleEntete.CleFactureManuelle," + " FactureManuelleEntete.NoFacture, FactureManuelleEntete.NumFacture,"
                    + " FactureManuelleEntete.NCompte, FactureManuelleEntete.Facture," + " FactureManuelleEntete.ModeReglement,FactureManuelleEntete.Verifiee,"
                    + " FactureManuelleEntete.NomIntervention, FactureManuelleDetail.MontantLigne," + " FactureManuelleDetail.TVA, FactureManuelleDetail.Compte,"
                    + " FactureManuelleDetail.Compte7, FactureManuelleDetail.AnaActivite," + " FactureManuelleDetail.numeroligne, FactureManuellePied.MontantHT, FactureManuelleEntete.DateFacture, FactureManuellePied.MontantTTC "
                    + " FROM (FactureManuelleEntete LEFT JOIN FactureManuelleDetail" + " ON FactureManuelleEntete.CleFactureManuelle ="
                    + " FactureManuelleDetail.CleFactureManuelle) LEFT JOIN FactureManuellePied"
                    + " ON FactureManuelleEntete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle";
                //----Rajoute un critére pour la requete çi-dessus( personne qui a crée la facture)
                if (optPersonne.Checked == true)//tested
                {
                    SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne ='" + strNomPersonne + "'";
                    SQL = "{FactureManuelleEntete.NomPersonne} ='" + strNomPersonne + "' and ";
                }
                else if (OptNomChoisis.Checked == true)
                {
                    SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne ='" + Combo1.Text + "'";
                    SQL = "{FactureManuelleEntete.NomPersonne} ='" + Combo1.Text + "' and ";
                }
                else if (OptTous.Checked == true)
                {
                    SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne is not null";
                    //---Ne fait rien.
                }

                //----Option par numéro de facture.
                if (Option5.Checked == true)//tested
                {

                    if (string.IsNullOrEmpty(txtDebut.Text) || !(General.IsNumeric(txtDebut.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtDebut.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtFin.Text) || !(General.IsNumeric(txtFin.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtFin.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    //If txtFin <> txtDebut Then
                    //        MsgBox "Vous ne pouvez saisir qu'une seule facture à la fois.", vbCritical, "Données eronnées"
                    //        txtFin.SetFocus
                    //        SablierOnOff False
                    //        Exit Sub
                    //End If
                    //----stocke la clause where des champs à vérifier.
                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.CleFactureManuelle >= " + txtDebut.Text
                        + " AND FactureManuelleEntete.CleFactureManuelle<= " + txtFin.Text + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'"
                        + " AND FactureManuelleEntete.Facture= 0 and " + " FactureManuelleEntete.Imprimer = 0";
                    sCodeFamille = sCodeFamille + SQLTypeFacture;

                    //----stocke la requete pour crystalReport
                    //rIf txtDateDeFacture < CDate(dbEURO) Then
                    //r   SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtDebut & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtFin & " and {FactureManuelleEntete.Facture} = false  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = false"
                    //rElse
                    //r    SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtDebut & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtFin & " and {FactureManuelleEntete.Facture} = false  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = 0"
                    //rEnd If
                    //----option facture en cours.
                }
                else if (Option6.Checked == true)
                {


                    if (string.IsNullOrEmpty(txtEnCours.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas de facture en cours à imprimer", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        SablierOnOff(false);
                        return;
                    }
                    else if (cmdAjouter.Enabled == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        SablierOnOff(false);
                        return;
                    }
                    else
                    {

                        //----stocke la clause where des champs à vérifier.
                        SQLTypeFacture = SQLTypeFacture + " and FactureManuelleEntete.CleFactureManuelle = " + longCle
                            + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'"
                            + " AND FactureManuelleEntete.Facture = 0 and FactureManuelleEntete.Imprimer = 0";
                        sCodeFamille = sCodeFamille + SQLTypeFacture;

                        //----stocke la requete pour crystalReport
                        //rIf txtDateDeFacture < CDate(dbEURO) Then
                        //r     SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} = " & longCle & "  and {FactureManuelleEntete.Facture} = false AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = false"
                        //rElse
                        //r     SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} = " & longCle & "  and {FactureManuelleEntete.Facture} = 0 AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = 0"
                        //r End If
                    }

                    //----Option par periode
                }
                else if (Option3.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtImpDate1.Text) || !(General.IsDate(txtImpDate1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtImpDate1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtImpDate2.Text) || !(General.IsDate(txtImpDate2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtImpDate2.Focus();
                        return;
                    }
                    else
                    {
                        txtImpDate1.Text = Convert.ToDateTime(txtImpDate1.Text).ToString("dd/MM/yyyy");
                        txtImpDate2.Text = Convert.ToDateTime(txtImpDate2.Text).ToString("dd/MM/yyyy");
                    }
                    //----stocke la clause where des champs à vérifier.
                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.DateFacture >='" + Convert.ToDateTime(txtImpDate1.Text).ToString("dd/MM/yyyy")
                        + "' And FactureManuelleEntete.DateFacture <='" + Convert.ToDateTime(txtImpDate2.Text).ToString("dd/MM/yyyy")
                        + "'  AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'" + " AND FactureManuelleEntete.Facture = 0 and FactureManuelleEntete.Imprimer = 0";
                    sCodeFamille = sCodeFamille + SQLTypeFacture;

                    //----stocke la requete pour crystalReport

                    dbdate1 = Convert.ToDateTime(txtImpDate1.Text).ToString("yyyy,MM,dd");

                    dbdate2 = Convert.ToDateTime(txtImpDate2.Text).ToString("yyyy,MM,dd");


                    //rIf txtDateDeFacture < CDate(dbEURO) Then
                    //r    SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "'and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false"
                    //rElse
                    //r    SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "'and {FactureManuelleEntete.Facture} = 0 and {FactureManuelleEntete.Imprimer} = 0"
                    //rEnd If
                }

                //----vérifie les factures.
                adotemp = new DataTable();
                ModAdo tmpAdo = new ModAdo();
                adotemp = tmpAdo.fc_OpenRecordSet(sCodeFamille);

                //  Debug.Print(adotemp.RecordCount);
                // Debug.Print(sCodeFamille);

                //If Not adotemp.EOF Then
                VerifieFacture(ref adotemp);
                //End If

                if ((adotemp != null))
                {
                    adotemp = new DataTable();
                }
                //rCR.WindowShowPrintSetupBtn = True
                //rCR.SelectionFormula = SQL
                //rCR.Action = 1

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //=== Modif du 02/11/2020, mise à jour du code TVA300
                sSQLTVA = "SELECT CleFactureManuelle FROM FactureManuelleEnTete " + SQLTypeFacture;
                rsTVA300 = rsTVA300ModAdo.fc_OpenRecordSet(sSQLTVA);

                foreach (DataRow rsTVA300Row in rsTVA300.Rows)
                {
                    // === attention bLoadGrille est un pointeur.
                    fc_ADDTVA300(Convert.ToInt32(General.nz(rsTVA300Row["CleFactureManuelle"], 0)), ref bLoadGrille);
                }
                //===> Fin Modif Mondir


                //=== PREPARE LA REQUETE POUR CRYSTAL REPORT.
                //=== Option par numéro de facture.
                if (Option5.Checked == true)
                {
                    //If RechDate(txtDebut) = 0 Then
                    SQL = "{FactureManuelleEntete.CleFactureManuelle} >=" + txtDebut.Text + " and {FactureManuelleEntete.CleFactureManuelle} <=" +
                        txtFin.Text + "   AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                    lVersionDate = RechDate(txtDebut.Text.ToInt());

                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(SQL, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                    else if (lVersionDate == 1) //===> Replace RechDate() by lVersionDate
                    {
                        //  CR2.WindowShowPrintSetupBtn = true;
                        //SelectionFormula = SQL;
                        //CrystalReportFormView CrystalForm = new CrystalReportFormView(CR2, SelectionFormula);
                        //CrystalForm.Show();

                        //===> Mondir le 05.03.2021 pour corriger #2247
                        SelectionFormula = SQL;
                        CR.RecordSelectionFormula = SelectionFormula;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        //===> Fin Modif Mondir

                    }
                    else
                    {
                        //CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = SQL;
                        CR.RecordSelectionFormula = SelectionFormula;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                    }
                    //Else
                    //    SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtDebut & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtFin & " and {FactureManuelleEntete.Facture} = false  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = false"
                    //    CR2.WindowShowPrintSetupBtn = True
                    //    CR2.SelectionFormula = SQL
                    //    CR2.Action = 1
                    //End If

                    //=== option facture en cours.
                }
                else if (Option6.Checked == true)
                {
                    //    If RechDate(longCle) = 0 Then
                    SQL = SQL + "{FactureManuelleEntete.CleFactureManuelle} = " + longCle + " "
                        + " AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Tested
                    lVersionDate = RechDate(longCle);

                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        //===> Mondir le 02.04.2021, changed strMiseAJour by SQL, coz strMiseAJour is allways empty https://groupe-dt.mantishub.io/view.php?id=2372
                        //====> was evModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                        sFileReport = devModeCrystal.fc_ExportCrystal(SQL, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);

                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    else if (lVersionDate == 1) //===> Replace RechDate() by lVersionDate
                    {
                        // CR2.WindowShowPrintSetupBtn = true;
                        //SelectionFormula = SQL;
                        //CrystalReportFormView CrystalForm = new CrystalReportFormView(CR2, SelectionFormula);
                        //CrystalForm.Show();
                        //===> Mondir le 22.01.2021, old code, found while looking at https://groupe-dt.mantishub.io/view.php?id=2220
                        //if (General.fncUserName() == "mohammed")
                        //{
                        //    SelectionFormula = SQL;
                        //    CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        //    CrystalForm.Show();
                        //    Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                        //}
                        //===> New Code
                        SelectionFormula = SQL;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                        //===> Fin Modif Mondir
                    }
                    else
                    {
                        //CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = SQL;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                    }

                    //   Else
                    //       SQL = SQL & "{FactureManuelleEntete.CleFactureManuelle} = " & longCle & "  and {FactureManuelleEntete.Facture} = false AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Imprimer} = false"
                    //       CR2.WindowShowPrintSetupBtn = True
                    //       CR2.SelectionFormula = SQL
                    //       CR2.Action = 1
                    //   End If
                    //=== Option par periode
                }
                else if (Option3.Checked == true)
                {
                    //If txtImpDate1 >= CDate(dbEURO) Then
                    //    SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "'and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false"

                    //    CR2.WindowShowPrintSetupBtn = True
                    //    CR2.SelectionFormula = SQL
                    //    CR2.Action = 1
                    //    CR2.SelectionFormula = ""
                    //ElseIf txtImpDate1 < dbEURO And txtImpDate2 < dbEURO Then
                    //SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = 0 and {FactureManuelleEntete.Imprimer} = 0"

                    SQL = SQL + "{FactureManuelleEntete.datefacture}>= Date (" + dbdate1 + ") " + " and {FactureManuelleEntete.dateFacture}<= Date (" + dbdate2 + ")" +
                          " AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false";

                    //===> Mondir le 05.03.2021 pour corriger #2247
                    lVersionDate = RechDate(longCle);

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    //===> Line bellow is commented by Mondir to fix #2247
                    //if (General.dtdateFactManuV3 >= txtImpDate1.Text.ToDate())
                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        //===> Mondir le 02.04.2021, changed strMiseAJour by SQL, coz strMiseAJour is allways empty https://groupe-dt.mantishub.io/view.php?id=2372
                        //====> was evModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                        sFileReport = devModeCrystal.fc_ExportCrystal(SQL, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    //===> Line bellow is commented by Mondir to fix #2247
                    //else if (General.IsDate(General.sDateFactureManuelleV2))
                    else if (lVersionDate == 1)
                    {
                        //===> Mondir le 05.03.2021 Line bellow is commented by Mondir to fix #2247
                        //if (Convert.ToDateTime(txtImpDate1.Text) >= Convert.ToDateTime(General.sDateFactureManuelleV2))
                        //{
                        //    // CR2.WindowShowPrintSetupBtn = true;
                        //    //todo rapport inexistant CR2
                        //    //SelectionFormula = SQL;
                        //    //CrystalReportFormView CrystalForm = new CrystalReportFormView(CR2, SelectionFormula);
                        //    //CrystalForm.Show();
                        //    //SelectionFormula = "";
                        //}
                        //else
                        //{
                        // CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = SQL;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                        SelectionFormula = "";
                        //}
                    }
                    else
                    {
                        //  CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = SQL;
                        CrystalReportFormView CrystalForm = new CrystalReportFormView(CR, SelectionFormula);
                        CrystalForm.Show();
                        Program.SaveException(null, "Rapport Clause Where :==> " + SelectionFormula);
                        SelectionFormula = "";
                    }
                    //ElseIf txtImpDate1 < dbEURO Then
                    // dbdate2 = #6/15/2001#
                    //    SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}< Date (" & dbEuroCr & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "'and {FactureManuelleEntete.Facture} = false and {FactureManuelleEntete.Imprimer} = false"
                    //    CR.WindowShowPrintSetupBtn = True
                    //    CR.SelectionFormula = SQL
                    //    CR.Action = 1
                    //    CR.SelectionFormula = ""
                    //CR.Reset

                    //    SQL = SQL & "{FactureManuelleEntete.datefacture}>= Date (" & dbEuroCr & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "'and {FactureManuelleEntete.Facture} = 0 and {FactureManuelleEntete.Imprimer} = 0"

                    //    CR2.WindowShowPrintSetupBtn = True
                    //    CR2.SelectionFormula = SQL
                    //    CR2.Action = 1
                    //    CR2.SelectionFormula = ""
                    //     Exit Sub
                    // End If

                }

                //-----Si une erreur dans les factures est trouvée, on affiche le message çi-dessus.
                if (boolErreurGeneral == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des erreurs ont été décelées, cliquez sur synthése pour visualiser les erreurs possible.", "Factures non valides", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                //  SelectionFormula = "";

                SablierOnOff1(false);

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                if (bLoadGrille)
                {
                    SSOleDBGrid1.DataSource = null;
                    FirstRemplitGrille();
                }
                //===> Fin Modif Mondir
                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmdImprimer_Click");
                if (ex.InnerException != null)
                    Program.SaveException(null, "error in crystal report function cmdImrpimer_click ===> details : " + ex.InnerException.Message);
            }

        }


        /// <summary>
        /// Tested mais tjr on va avoir le mm msg de la dernière 'else if' 
        /// car  la colonne "Choix" dans la gride SSOleDBGrid3 se remplit tjr par la valeur 'Non' donc la variable 'SQLIntervention' est tjr vide
        /// Modifs de la version V02.07.2020 ajoutées par Mondir
        /// Modifs de la version V02.07.2020 testées par Mondir
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdInserer_Click(object sender, EventArgs e)
        {
            try
            {
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                int lNumficheStandard = 0;
                //===> Fin Modif Mondir

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                string sSQLWhereStandard = "";
                //===> Fin Modif Mondir

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdInserer_Click() - Enter in Function");
                //===> Fin Modif Mondir

                //----Verifie si une facture est en cours
                if (string.IsNullOrEmpty(txtEnCours.Text))
                {
                    //===> Mondir 03.08.2020, Ajout du LOG
                    Program.SaveException(null, $"cmdInserer_Click() -  if (string.IsNullOrEmpty(txtEnCours.Text)) - txtEnCours.Text Empty");
                    //===> Fin Modif Mondir

                    //===> Mondir le 02.09.2020, Ajout au message d'erreur "Veuillez annuler la facture actuelle et en créer une autre" ===> Demandé par Fabiola
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas de facture en cours, Veuillez annuler la facture actuelle et en créer une autre", "Opération annulée",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //'on error Resume Next
                    cmdInserer.Focus();
                    return;
                }

                //====> Mondir le 02.07.2020, modif de la version 02.07.2020(V2), this condition commencted
                //'=== selectionner un service.
                //=== on déplace cette portion de code.
                //if (General.sEvolution == "1")
                //{
                //    ModMain.sServiceFact = "";
                //    frmServiceFact frm = new frmServiceFact();
                //    frm.optTVAimm.Checked = false;
                //    frm.optTP.Checked = false;
                //    frm.OptTR.Checked = false;
                //    //====> Mondir le 02.07.2020, ajouter les modfis de la version V02.07.2020
                //    //frm.optTVAimm.Checked = false;
                //    if (optDevis1.Checked)
                //    {
                //        frm.txtDevis.Text = "1";
                //        frm.optTVAimm.Enabled = false;
                //        frm.optTP.Enabled = false;
                //        frm.OptTR.Enabled = false;
                //        frm.Label2.Enabled = false;
                //    }
                //    else
                //    {
                //        frm.txtDevis.Text = "0";
                //        frm.optTVAimm.Enabled = true;
                //        frm.optTP.Enabled = true;
                //        frm.OptTR.Enabled = true;
                //        frm.Label2.Enabled = true;
                //    }
                //    //====> Fin modif Mondir
                //    ModMain.sTVAfactXX = "";
                //    frm.ShowDialog();

                //    if (ModMain.sServiceFact == "")
                //        return;
                //    else
                //    {
                //        cmbService.Text = ModMain.sServiceFact;
                //        if (ModMain.sTVAfactXX != "")
                //            txtTVA.Text = ModMain.sTVAfactXX;
                //    }
                //}
                //====> Fin Modif Mondir

                //----récupere les interventions selectionnées pour les insérer dans
                //----le corps de facture.
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                SablierOnOff(true);
                //===> Fin Modif Mondir

                lNumficheStandard = 0;

                var _with10 = SSOleDBGrid3;
                //_with10.MoveFirst();

                SQLIntervention = "";
                SQL = "";

                for (i = 0; i <= _with10.Rows.Count - 1; i++)
                {
                    //----ici dbdate1 ne contient pas de date

                    UltraGridRow row = SSOleDBGrid3.Rows[i];
                    if (row.Cells["Choix"].Value.ToString() == "Oui")
                    {
                        if (string.IsNullOrEmpty(SQLIntervention))
                        {
                            lNoIntervention = Convert.ToInt32(General.nz(row.Cells["NoIntervention"].Value, 0));
                            SQLIntervention = "WHERE (intervention.NoIntervention = " + row.Cells["NoIntervention"].Value;
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            sSQLWhereStandard = " WHERE intervention.NumFicheStandard = " +
                                                row.Cells["NumFicheStandard"].Value;
                            //===> Fin Modif Mondir
                        }
                        else
                        {
                            SQLIntervention = SQLIntervention + " OR intervention.NoIntervention = " + row.Cells["NoIntervention"].Value;
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            sSQLWhereStandard = sSQLWhereStandard + " OR intervention.NumFicheStandard = " +
                                                row.Cells["NumFicheStandard"].Value;
                            //===> Fin Modif Mondir

                        }

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        //TODO .Cell Text
                        lNumficheStandard = Convert.ToInt32(General.nz(row.Cells["NumFicheStandard"].Text, 0));
                        //===> Fin Modif Mondir
                    }
                }

                //=== modif du 30 03 2012, ajout des parentheses.
                if (!string.IsNullOrEmpty(SQLIntervention))
                {
                    SQLIntervention = SQLIntervention + ")";
                }

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdInserer_Click - Select Query = {SQLIntervention}");
                //===> Fin Modif Mondir

                if (!string.IsNullOrEmpty(SQLIntervention))
                {
                    //====> Mondir : Modif de la version 02.07.2020(V2)
                    //=== code déplacé le 02 07 2020.
                    if (General.sEvolution == "1")
                    {
                        ModMain.sServiceFact = "";
                        frmServiceFact frm = new frmServiceFact();
                        frm.optTVAimm.Checked = false;
                        frm.OptTR.Checked = false;
                        frm.optTP.Checked = false;
                        ModMain.sTVAfactXX = "";
                        //====> Mondir le 02.07.2020, ajouter les modfis de la version V02.07.2020
                        //frm.optTVAimm.Checked = false;
                        if (optDevis1.Checked)
                        {
                            frm.txtDevis.Text = "1";
                            frm.optTVAimm.Enabled = false;
                            frm.optTP.Enabled = false;
                            frm.OptTR.Enabled = false;
                            frm.Label2.Enabled = false;
                        }
                        else
                        {
                            frm.txtDevis.Text = "0";
                            frm.optTVAimm.Enabled = true;
                            frm.optTP.Enabled = true;
                            frm.OptTR.Enabled = true;
                            frm.Label2.Enabled = true;

                            if (General.fc_CtrlPariculier(SQLIntervention) == "")
                                frm.optTVAimm.Checked = true;
                        }

                        //====> Fin modif Mondir
                        frm.ShowDialog();

                        if (ModMain.sServiceFact == "")
                            return;
                        else
                        {
                            cmbService.Text = ModMain.sServiceFact;
                            if (ModMain.sTVAfactXX != "")
                                txtTVA.Text = ModMain.sTVAfactXX;
                        }
                        //====> Fin modif Mondir 02.07.2020(V2)
                    }
                    //=== fin du code déplacé.

                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> ajout du param sSQLWhereStandard
                    fc_Synthese(lNumficheStandard, sSQLWhereStandard);
                    //===> Fin Modif Mondir

                    if (optDevis.Checked == true)
                    {
                        if (fc_VerifDevis() == false)
                        {
                            return;
                        }
                        //frmSynthese.Visible = true;
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        fraSynthese.Visible = true;
                        //===> Fin Modif Mondir
                    }
                    else if (optIntervention.Checked)
                    {
                        fc_VerifInterv();
                        //frmSynthese.Visible = true;

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        fraSynthese.Visible = true;
                        //===> Fin Modif Mondir
                    }
                    else
                    {
                        frmSynthese.Visible = false;
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        fraSynthese.Visible = false;
                        //===> Fin Modif Mondir
                    }

                    //----indique que des interventions ont été selectionnées.

                    boolinsertInterv = true;
                    cmdInserer.Enabled = false;
                    txtImmeuble.Enabled = false;
                    txtCodeimmeuble.Text = txtImmeuble.Text;
                    cmdRechImmeuble.Enabled = false;


                    //---fournis l'entéte de facture.
                    fournisEntete(lNoIntervention, ModMain.sTVAfactXX);//tested

                    //--- récupére le code analytique de l intervention

                    fc_FindCodeAnal(SQLIntervention);

                    FirstRemplitGrille();
                    //---fournis le corps de facture.
                    insertIntervInCorps();

                    txtNoIntervention.Text = Convert.ToString(lNoIntervention);
                    SSTab1.SelectedTab = SSTab1.Tabs[1];
                    SablierOnOff(false);


                    if (SSOleDBGrid1.DisplayLayout.Bands[0].Columns["SSFamille"].CellActivation == Activation.NoEdit)
                    {
                        SSOleDBGrid1.Focus();
                    }

                    fc_LoadInter();

                }
                else if (string.IsNullOrEmpty(SQLIntervention))//tested
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention n'a été sélectionnée." + "\n" + "Vous devez sélectionner des interventions pour créer une facture", "Insertion nulle", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    SablierOnOff(false);
                    cmdInserer.Focus();
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdInserer_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNofacture"></param>
        private void fc_ChargeInfosFacture(string sNofacture)
        {

            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChargeInfosFacture() - Enter in Function - sNofacture = {sNofacture}");
            //===> Fin Modif Mondir

            DataTable rsDevis = default(DataTable);
            string strSelect = null;
            string sNodevis = null;
            string sNumFicheStandard = null;
            bool Match = false;
            int lngHeure = 0;
            int lngMinute = 0;
            int lngSeconde = 0;
            System.DateTime sDuree = default(System.DateTime);
            string sDureeTemp = null;
            double sDureeDiff = 0;
            int i = 0;

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            string sSQLxyz = "";
            string sWhereMultiAppel = "";
            //===> Fin Modif Mondir

            //Récupère le ou les devis liés aux interventions sélectionnées   =================>Tested
            rsDevis = new DataTable();
            ModAdo rsDevisModAdo = new ModAdo();
            strSelect = "SELECT DISTINCT GestionStandard.NoDevis,GestionStandard.NumFicheStandard FROM GestionStandard INNER JOIN Intervention ";
            strSelect = strSelect + " ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard ";
            strSelect = strSelect + " WHERE Intervention.NoFacture='" + sNofacture + "'";

            rsDevis = rsDevisModAdo.fc_OpenRecordSet(strSelect);
            if (rsDevis.Rows.Count > 0)
            {
                // rsDevis.MoveFirst();
                //Stock le numéro de devis et le numéro de la fiche d'appel
                sNodevis = rsDevis.Rows[0]["NoDevis"].ToString() + "";
                sNumFicheStandard = rsDevis.Rows[0]["Numfichestandard"].ToString() + "";

                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (sNodevis != rsDevisRow["NoDevis"].ToString() + "")
                    {
                        Match = true;

                        break;
                    }
                    // rsDevis.MoveNext();
                }
            }
            rsDevis.Dispose();


            //Calcul du montant total du devis  ===========================>TEsted
            rsDevis = rsDevisModAdo.fc_OpenRecordSet("SELECT SUM(DevisDetail.TotalVenteApresCoef) AS TotalVente FROM DevisDetail WHERE DevisDetail.NumeroDevis='"
                + sNodevis + "' GROUP BY DevisDetail.NumeroDevis");

            if (rsDevis.Rows.Count > 0)
            {
                //  rsDevis.MoveFirst();

                lblMontantDevis.Text = General.nz(rsDevis.Rows[0]["TotalVente"], "0").ToString() + " €";

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblhtDevis.Text = General.nz(rsDevis.Rows[0]["TotalVente"], "0").ToString() + " €";
                //===> Fin Modif Mondir
            }
            else
            {
                lblMontantDevis.Text = "0 €";

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblhtDevis.Text = "0 €";
                //===> Fin Modif Mondir
            }
            rsDevis.Dispose();

            //Calcul du montant total des achats réalisés sur ce devis========================>Tested


            //rsDevis.Open "SELECT SUM(BCD_Detail.BCD_PrixHT * BCD_Detail.BCD_Quantite) AS TotalAchats FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande WHERE BonDeCommande.NumFicheStandard=" & nz(sNumFicheStandard, "0") & " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL GROUP BY BonDeCommande.NumFicheStandard,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite", adocnn
            //== modif rachid du 8 sept 2005 le champs quantité n etant pas numeric cette requete pose certains pb.
            //  rsDevis.Open "SELECT BCD_Detail.BCD_PrixHT , BCD_Detail.BCD_Quantite FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande WHERE BonDeCommande.NumFicheStandard=" & nz(sNumFicheStandard, "0") & " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL GROUP BY BonDeCommande.NumFicheStandard,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite", adocnn

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            sSQLxyz = "SELECT     BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite "
                      + " FROM         BCD_Detail INNER JOIN BonDeCommande "
                      + " ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande ";
            if (string.IsNullOrEmpty(sNodevis))
            {
                sWhereMultiAppel = ModAnalytique.fc_MultiAppel(Convert.ToInt32(General.nz(sNumFicheStandard, 0)));
            }

            if (!string.IsNullOrEmpty(sWhereMultiAppel))
            {
                sSQLxyz = sSQLxyz + " " + sWhereMultiAppel.Replace("intervention.", "BonDeCommande.");
            }
            else
            {
                sSQLxyz = sSQLxyz + " WHERE  BonDeCommande.NumFicheStandard =" + General.nz(sNumFicheStandard, "0");
            }
            //===> Fin Modif Mondir

            rsDevis = rsDevisModAdo.fc_OpenRecordSet(sSQLxyz);

            // If Not (rsDevis.EOF And rsDevis.BOF) Then
            if (rsDevis.Rows.Count > 0)//tested
            {
                // rsDevis.MoveFirst();
                lblTotalAchats.Text = "0";
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (General.IsNumeric(rsDevisRow["BCD_PrixHT"].ToString()) && General.IsNumeric(rsDevisRow["BCD_Quantite"].ToString()))
                    {
                        //lblTotalAchats.Caption = CStr(CDbl(lblTotalAchats.Caption) + CDbl(nz(rsDevis!TotalAchats, "0")))
                        lblTotalAchats.Text = Convert.ToString(Convert.ToDouble(lblTotalAchats.Text) + Convert.ToDouble(General.nz(rsDevisRow["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(rsDevisRow["BCD_Quantite"], "0")));
                    }
                    else
                    {
                        //    Stop
                    }
                    // rsDevis.MoveNext();
                }
                lblTotalAchats.Text = lblTotalAchats.Text + " €";
            }
            else
            {
                lblTotalAchats.Text = "0 €";
            }
            rsDevis.Dispose();


            //Calcul du montant total des factures sur situation de ce devis ==========================>Tested
            tabNoFacture = new string[1];
            //Sert à afficher une info bulle
            tabMontantFact = new string[1];
            //Sert à afficher une info bulle

            strSelect = "";
            strSelect = "SELECT FactureManuellePied.MontantHT AS TotalSituations,FactureManuelleEntete.NoFacture FROM FactureManuellePied";
            strSelect = strSelect + " INNER JOIN FactureManuelleEntete ON ";
            strSelect = strSelect + " FactureManuellePied.CleFactureManuelle=FactureManuelleEntete.CleFactureManuelle ";
            strSelect = strSelect + " INNER JOIN Intervention ON ";
            strSelect = strSelect + " FactureManuelleEntete.NoFacture=Intervention.NoFacture ";

            strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + General.nz(sNumFicheStandard, "0") + "";
            strSelect = strSelect + " AND NOT FactureManuellePied.MontantHT IS NULL ";
            strSelect = strSelect + " GROUP BY FactureManuellePied.MontantHT,FactureManuelleEntete.NoFacture,Intervention.NumFicheStandard ";
            rsDevisModAdo = new ModAdo();
            rsDevis = rsDevisModAdo.fc_OpenRecordSet(strSelect);

            lblTotalSituations.Text = "0";
            i = 0;

            if (rsDevis.Rows.Count > 0)
            {
                //  rsDevis.MoveFirst();

                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (tabNoFacture.Length <= i)
                    {
                        Array.Resize(ref tabNoFacture, i + 1);
                    }
                    if (tabMontantFact.Length <= i)
                    {
                        Array.Resize(ref tabMontantFact, i + 1);
                    }
                    tabNoFacture[i] = rsDevisRow["NoFacture"] + "";
                    tabMontantFact[i] = Convert.ToString(rsDevisRow["TotalSituations"] + "");

                    lblTotalSituations.Text = Convert.ToString(Convert.ToDouble(General.nz(lblTotalSituations.Text, "0")) + Convert.ToDouble(General.nz(rsDevisRow["TotalSituations"], "0")));
                    i = i + 1;
                    // rsDevis.MoveNext();
                }
                txtSituation.Text = lblTotalSituations.Text;
                lblTotalSituations.Text = lblTotalSituations.Text + " €";

            }
            else
            {
                lblTotalSituations.Text = "0 €";
                // ToolTip1.SetToolTip(lblTotalSituations, "Aucune facture enregistrée");
            }
            rsDevis.Dispose();

            //=== calcul de la main d'oeuvre.
            strSelect = "";
            strSelect = strSelect + "SELECT Intervention.HeureDebut,Intervention.HeureFin,";
            strSelect = strSelect + " Intervention.Duree FROM Intervention ";

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020, ajout du cette contion
            if (!string.IsNullOrEmpty(sWhereMultiAppel))
            {
                strSelect = strSelect + sWhereMultiAppel;
            }
            else
            {
                strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + General.nz(sNumFicheStandard, "0");
            }

            rsDevis = rsDevisModAdo.fc_OpenRecordSet(strSelect);

            if (rsDevis.Rows.Count > 0)
            {
                //  rsDevis.MoveFirst();
                sDuree = Convert.ToDateTime("01/01/1900 00:00:00");
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {

                    if (!string.IsNullOrEmpty(rsDevisRow["Duree"].ToString()) && General.IsDate(rsDevisRow["Duree"].ToString()))
                    {
                        if (Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay > Convert.ToDateTime("00:00:00").TimeOfDay)//tested
                        {
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Hours);

                        }
                        else if (!string.IsNullOrEmpty(rsDevisRow["HeureDebut"].ToString()) &&
                            General.IsDate(rsDevisRow["HeureDebut"].ToString()) &&
                            !string.IsNullOrEmpty(rsDevisRow["HeureFin"].ToString()) &&
                            General.IsDate(rsDevisRow["HeureFin"].ToString()))
                        {
                            sDureeTemp = General.fc_calcDuree(Convert.ToDateTime(rsDevisRow["HeureDebut"]), Convert.ToDateTime(rsDevisRow["HeureFin"]));

                            sDureeTemp = Convert.ToDateTime(sDureeTemp).ToString("HH:mm:ss");
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(sDureeTemp).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(sDureeTemp).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(sDureeTemp).TimeOfDay.Hours);

                        }
                    }
                    //   rsDevis.MoveNext();
                }
                rsDevis.Dispose();
                //================================>TEsted

                sDureeDiff = (sDuree - Convert.ToDateTime("01/01/1900 00:00:00")).TotalSeconds;


                if (sDureeDiff < 0)
                {
                    sDureeDiff = sDureeDiff * (-1);
                }
                lngHeure = Convert.ToInt32(sDureeDiff / 3600);

                if (lngHeure * 3600 > sDureeDiff)//tested
                {
                    lngHeure = lngHeure - 1;
                }

                lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

                if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
                {
                    lngMinute = lngMinute - 1;
                }

                lngSeconde = Convert.ToInt32(sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

                if (General.Len(Convert.ToString(lngHeure)) == 1)
                {
                    lblTotalHeures.Text = "0" + lngHeure + ":";
                }
                else
                {
                    lblTotalHeures.Text = lngHeure + ":";
                }

                if (General.Len(Convert.ToString(lngMinute)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngMinute + ":";
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngMinute + ":";
                }

                if (General.Len(Convert.ToString(lngSeconde)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngSeconde;
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngSeconde;
                }
            }
            else
            {
                lblTotalHeures.Text = "00:00:00";
            }


            rsDevis = null;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNofacture"></param>
        private void fc_ChargeInfoFactInterv(string sNofacture)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChargeInfoFactInterv() - Enter in Function - sNofacture = {sNofacture}");
            //===> Fin Modif Mondir

            DataTable rsDevis = default(DataTable);
            string strSelect = null;
            string sNodevis = null;
            string sNumFicheStandard = null;
            bool Match = false;
            int lngHeure = 0;
            int lngMinute = 0;
            int lngSeconde = 0;
            System.DateTime sDuree = default(System.DateTime);
            string sDureeTemp = null;
            double sDureeDiff = 0;
            int i = 0;

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            string sTempSQL = "";
            string sWhereMultiAppel = "";
            string sSQlNumFiche = "";
            //===> Fin Modif Mondir

            try
            {
                rsDevis = new DataTable();
                ModAdo rsDevisModAdo = new ModAdo();

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //=== recupére le n° de fiche d'appel.
                sSQlNumFiche = "";
                //===> Fin Modif MOndir

                //Pas de calcul du montant total du devis : intervention hors contrat et hors
                //Devis
                lblMontantDevis.Text = "0 €";
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblhtDevis.Text = "0 €";
                //===> Fin Modif Mondir

                //Calcul du montant total des achats réalisés sur cette (ces) intervention(s)
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                sTempSQL = "SELECT SUM(BCD_Detail.BCD_PrixHT * BCD_Detail.BCD_Quantite) AS TotalAchats FROM BCD_Detail "
                           + " INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande "
                           + " INNER JOIN Intervention ON BonDeCommande.NoIntervention=Intervention.NoIntervention ";

                sTempSQL = sTempSQL + " WHERE Intervention.NoFacture='" + sNofacture + "' AND NOT BCD_PrixHT IS NULL "
                           + " AND NOT BCD_Quantite IS NULL ";

                sTempSQL = sTempSQL + " GROUP BY Intervention.NoFacture,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite";

                rsDevis = rsDevisModAdo.fc_OpenRecordSet(sTempSQL);
                //===> Fin Modif Mondir

                if (rsDevis != null && rsDevis.Rows.Count > 0)//tested
                {
                    // rsDevis.MoveFirst();
                    lblTotalAchats.Text = "0";

                    foreach (DataRow rsDevisRow in rsDevis.Rows)
                    {

                        lblTotalAchats.Text = Convert.ToString(Convert.ToDouble(General.nz(lblTotalAchats.Text, "0")) + Convert.ToDouble(General.nz(rsDevisRow["TotalAchats"], "0")));
                        // rsDevis.MoveNext();
                    }
                    lblTotalAchats.Text = lblTotalAchats.Text + " €";
                }
                else
                {
                    lblTotalAchats.Text = "0 €";
                }
                rsDevis?.Dispose();

                //Pas de calcul du montant total des factures sur situation car pas
                //de situation sur intervention hors contrat et hors devis
                tabNoFacture = new string[1];
                //Sert à afficher une info bulle
                tabMontantFact = new string[1];
                //Sert à afficher une info bulle
                lblTotalSituations.Text = "0 €";

                strSelect = "";
                strSelect = strSelect + "SELECT Intervention.HeureDebut,Intervention.HeureFin,";
                strSelect = strSelect + " Intervention.Duree FROM Intervention ";
                strSelect = strSelect + " WHERE Intervention.NoFacture='" + sNofacture + "'";

                rsDevis = rsDevisModAdo.fc_OpenRecordSet(strSelect);
                if (rsDevis.Rows.Count > 0)
                {
                    //rsDevis.MoveFirst();
                    sDuree = Convert.ToDateTime("01/01/1900 00:00:00");
                    foreach (DataRow rsDevisRow in rsDevis.Rows)
                    {

                        if (!string.IsNullOrEmpty(rsDevisRow["Duree"].ToString()) && General.IsDate(rsDevisRow["Duree"]))
                        {
                            if (Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay > Convert.ToDateTime("00:00:00").TimeOfDay)//tested
                            {
                                sDuree = sDuree.AddSeconds(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Seconds);
                                sDuree = sDuree.AddMinutes(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Minutes);
                                sDuree = sDuree.AddHours(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Hours);


                            }
                            else if (!string.IsNullOrEmpty(rsDevisRow["HeureDebut"].ToString()) &&
                                General.IsDate(rsDevisRow["HeureDebut"].ToString()) &&
                                !string.IsNullOrEmpty(rsDevisRow["HeureFin"].ToString()) &&
                                General.IsDate(rsDevisRow["HeureFin"].ToString()))
                            {
                                sDureeTemp = General.fc_calcDuree(Convert.ToDateTime(rsDevisRow["HeureDebut"]), Convert.ToDateTime(rsDevisRow["HeureFin"]));

                                sDureeTemp = Convert.ToDateTime(sDureeTemp).ToString("HH:mm:ss");
                                sDuree = sDuree.AddSeconds(Convert.ToDateTime(sDureeTemp).TimeOfDay.Seconds);
                                sDuree = sDuree.AddMinutes(Convert.ToDateTime(sDureeTemp).TimeOfDay.Minutes);
                                sDuree = sDuree.AddHours(Convert.ToDateTime(sDureeTemp).TimeOfDay.Hours);

                            }
                        }
                        // rsDevis.MoveNext();
                    }
                    rsDevis.Dispose();

                    sDureeDiff = (sDuree - Convert.ToDateTime("01/01/1900 00:00:00")).TotalSeconds;

                    if (sDureeDiff < 0)
                    {
                        sDureeDiff = sDureeDiff * (-1);
                    }
                    lngHeure = Convert.ToInt32(sDureeDiff / 3600);

                    if (lngHeure * 3600 > sDureeDiff)//tested
                    {
                        lngHeure = lngHeure - 1;
                    }

                    lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

                    if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
                    {
                        lngMinute = lngMinute - 1;
                    }

                    lngSeconde = Convert.ToInt32(sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

                    if (General.Len(Convert.ToString(lngHeure)) == 1)//tested
                    {
                        lblTotalHeures.Text = "0" + lngHeure + ":";
                    }
                    else
                    {
                        lblTotalHeures.Text = lngHeure + ":";
                    }

                    if (General.Len(Convert.ToString(lngMinute)) == 1)
                    {
                        lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngMinute + ":";
                    }
                    else
                    {
                        lblTotalHeures.Text = lblTotalHeures.Text + lngMinute + ":";
                    }

                    if (General.Len(Convert.ToString(lngSeconde)) == 1)//tested
                    {
                        lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngSeconde;
                    }
                    else
                    {
                        lblTotalHeures.Text = lblTotalHeures.Text + lngSeconde;
                    }
                }
                else
                {
                    lblTotalHeures.Text = "00:00:00";
                }

                rsDevis = null;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

        }

        private void fc_VerifInterv()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_VerifInterv() - Enter in Function");
            //===> Fin Modif Mondir

            DataTable rsDevis = default(DataTable);
            string strSelect = null;
            string sNodevis = null;
            string sNumFicheStandard = null;
            bool Match = false;
            int lngHeure = 0;
            int lngMinute = 0;
            int lngSeconde = 0;
            System.DateTime sDuree = default(System.DateTime);
            string sDureeTemp = null;
            double sDureeDiff = 0;
            int i = 0;
            string sSQLxx = null;
            string sSqlrequete = null;
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            string sWhereMultiAppel = "";
            //===> Fin Modif Mondir

            rsDevis = new DataTable();
            ModAdo rsDevisModAdo = new ModAdo();

            //Pas de calcul du montant total du devis : intervention hors contrat et hors
            //Devis
            lblMontantDevis.Text = "0 €";

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            lblhtDevis.Text = "0 €";

            // === 02 / 10 / 2020 recherche le no de fiche d'appel.
            sSQLxx = "SELECT NumFicheStandard FROM  Intervention " + SQLIntervention;
            using (var tmpModAdo = new ModAdo())
                sNumFicheStandard = General.nz(tmpModAdo.fc_ADOlibelle(sSQLxx), 0).ToString();


            sWhereMultiAppel = ModAnalytique.fc_MultiAppel(Convert.ToInt32(sNumFicheStandard));
            //===> Fin Modif Mondir

            //=== modif du 6 octobre 2011, controle si il existe au moins un bon d commande, sinon la requete ci dessous
            //=== plante.
            sSQLxx = "SELECT     BCD_Detail.BCD_PrixHT"
                     + " FROM         BCD_Detail INNER JOIN"
                     + " BonDeCommande ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande INNER JOIN"
                     + " Intervention ON BonDeCommande.NoIntervention = Intervention.NoIntervention ";

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            if (!string.IsNullOrEmpty(sWhereMultiAppel))
            {
                sSQLxx = sSQLxx + sWhereMultiAppel;
            }
            else
            {
                sSQLxx = sSQLxx + SQLIntervention;
            }

            sSQLxx = sSQLxx + " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL ";

            //===> Fin Modif Mondir

            ModAdo tmpAdo = new ModAdo();
            sSQLxx = tmpAdo.fc_ADOlibelle(sSQLxx);

            if (!string.IsNullOrEmpty(sSQLxx))
            {

                //Calcul du montant total des achats réalisés sur cette (ces) intervention(s)
                sSqlrequete = "SELECT SUM(BCD_Detail.BCD_PrixHT *  BCD_Detail.BCD_Quantite) AS TotalAchats "
                              + " FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande "
                              + " INNER JOIN Intervention ON BonDeCommande.NoIntervention=Intervention.NoIntervention "
                              + SQLIntervention + " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL "
                              + " GROUP BY BonDeCommande.NoIntervention,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite";


                sSQLxx = "SELECT SUM(BCD_Detail.BCD_PrixHT *  BCD_Detail.BCD_Quantite) AS TotalAchats "
                         + " FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande "
                         + " INNER JOIN Intervention ON BonDeCommande.NoIntervention=Intervention.NoIntervention ";

                if (!string.IsNullOrEmpty(sWhereMultiAppel))
                {
                    sSQLxx = sSQLxx + sWhereMultiAppel;
                }
                else
                {
                    sSQLxx = sSQLxx + SQLIntervention;
                }

                sSQLxx = sSQLxx + " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL "
                                + " GROUP BY BonDeCommande.NoIntervention,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite";

                rsDevis = rsDevisModAdo.fc_OpenRecordSet(sSQLxx);

                if (rsDevis != null && rsDevis.Rows.Count > 0)
                {
                    lblTotalAchats.Text = "0";
                    foreach (DataRow rsDevisRow in rsDevis.Rows)
                    {
                        lblTotalAchats.Text = (Convert.ToDouble(lblTotalAchats.Text) +
                                                   Convert.ToDouble(General.nz(rsDevis.Rows[0]["TotalAchats"], "0"))).ToString();
                    }
                    lblTotalAchats.Text = lblTotalAchats.Text + " €";
                }
                else
                {
                    lblTotalAchats.Text = "0 €";
                }
                rsDevis.Dispose();
            }
            else
            {
                lblTotalAchats.Text = "0 €";
            }
            //Pas de calcul du montant total des factures sur situation car pas
            //de situation sur intervention hors contrat et hors devis
            tabNoFacture = new string[1];
            //Sert à afficher une fenêtre
            tabMontantFact = new string[1];
            //Sert à afficher une fenêtre
            lblTotalSituations.Text = "0.0";

            strSelect = "";
            strSelect = strSelect + "SELECT Intervention.HeureDebut,Intervention.HeureFin,";
            strSelect = strSelect + " Intervention.Duree FROM Intervention ";
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            if (!string.IsNullOrEmpty(sWhereMultiAppel))
            {
                strSelect = strSelect + sWhereMultiAppel;
            }
            else
            {
                strSelect = strSelect + SQLIntervention;
            }


            rsDevis = tmpAdo.fc_OpenRecordSet(strSelect);

            if (rsDevis.Rows.Count > 0)
            {
                // rsDevis.MoveFirst();
                sDuree = Convert.ToDateTime("01/01/1900 00:00:00");
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {

                    if (!string.IsNullOrEmpty(rsDevisRow["Duree"].ToString()) && General.IsDate(rsDevisRow["Duree"]))
                    {
                        if (Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay > Convert.ToDateTime("00:00:00").TimeOfDay)
                        {
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Hours);


                        }
                        else if (!string.IsNullOrEmpty(rsDevisRow["HeureDebut"].ToString()) &&
                            General.IsDate(rsDevisRow["HeureDebut"].ToString()) &&
                            !string.IsNullOrEmpty(rsDevisRow["HeureFin"].ToString()) &&
                             General.IsDate(rsDevisRow["HeureFin"].ToString()))
                        {
                            sDureeTemp = General.fc_calcDuree(Convert.ToDateTime(rsDevisRow["HeureDebut"]), Convert.ToDateTime(rsDevisRow["HeureFin"]));

                            sDureeTemp = Convert.ToDateTime(sDureeTemp).ToString("HH:mm:ss");
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(sDureeTemp).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(sDureeTemp).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(sDureeTemp).TimeOfDay.Hours);

                        }
                    }


                    // rsDevis.MoveNext();
                }
                rsDevis.Dispose();
                sDureeDiff = (sDuree - Convert.ToDateTime("01/01/1900 00:00:00")).TotalSeconds;

                if (sDureeDiff < 0)
                {
                    sDureeDiff = sDureeDiff * (-1);
                }
                lngHeure = Convert.ToInt32(sDureeDiff / 3600);

                if (lngHeure * 3600 > sDureeDiff)
                {
                    lngHeure = lngHeure - 1;
                }

                lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

                if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
                {
                    lngMinute = lngMinute - 1;
                }

                lngSeconde = Convert.ToInt32(sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

                if (General.Len(Convert.ToString(lngHeure)) == 1)
                {
                    lblTotalHeures.Text = "0" + lngHeure + ":";
                }
                else
                {
                    lblTotalHeures.Text = lngHeure + ":";
                }

                if (General.Len(Convert.ToString(lngMinute)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngMinute + ":";
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngMinute + ":";
                }

                if (General.Len(Convert.ToString(lngSeconde)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngSeconde;
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngSeconde;
                }
            }
            else
            {
                lblTotalHeures.Text = "00:00:00";
            }


            rsDevis = null;

        }

        private bool fc_VerifDevis()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_VerifDevis() - Enter in Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            DataTable rsDevis = default(DataTable);
            string strSelect = null;
            string sNodevis = null;
            string sNumFicheStandard = null;
            bool Match = false;
            bool bQte = false;
            int lngHeure = 0;
            int lngMinute = 0;
            int lngSeconde = 0;
            System.DateTime sDuree = default(System.DateTime);
            string sDureeTemp = null;
            double sDureeDiff = 0;
            int i = 0;
            double dbTotHT = 0;
            double dbQte = 0;

            //Récupère le ou les devis liés aux interventions sélectionnées
            rsDevis = new DataTable();
            ModAdo tmpAdo = new ModAdo();
            strSelect = "SELECT DISTINCT GestionStandard.NoDevis,GestionStandard.NumFicheStandard FROM GestionStandard INNER JOIN Intervention ";
            strSelect = strSelect + " ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard ";
            strSelect = strSelect + SQLIntervention;
            rsDevis = tmpAdo.fc_OpenRecordSet(strSelect);

            if (rsDevis.Rows.Count > 0)
            {
                // rsDevis.MoveFirst();      

                //Stock le numéro de devis et le numéro de la fiche d'appel
                sNodevis = rsDevis.Rows[0]["NoDevis"].ToString() + "";
                sNumFicheStandard = rsDevis.Rows[0]["Numfichestandard"].ToString() + "";
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (sNodevis != rsDevisRow["NoDevis"].ToString() + "")
                    {
                        Match = true;
                        break;
                    }
                    // rsDevis.MoveNext();
                }
            }
            rsDevis.Dispose();

            //Si il y a 2 devis différents, alors recommencer la sélection.
            if (Match == true)
            {
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez sélectionné des interventions sur plusieurs appels (donc plusieurs devis)." + "\n" + "Vous ne pouvez pas avoir plusieurs fiches d'appel liées."
                    + "\n" + "Veuillez modifier votre sélection d'interventions", "Sélection des interventions devis", MessageBoxButtons.OK, MessageBoxIcon.Question);

                rsDevis = null;
                return functionReturnValue;
            }
            else
            {
                functionReturnValue = true;
            }

            //Calcul du montant total du devis
            rsDevis = tmpAdo.fc_OpenRecordSet("SELECT SUM(DevisDetail.TotalVenteApresCoef) AS TotalVente FROM DevisDetail WHERE DevisDetail.NumeroDevis='" + sNodevis + "' GROUP BY DevisDetail.NumeroDevis");
            if (rsDevis.Rows.Count > 0)
            {
                // rsDevis.MoveFirst();
                lblMontantDevis.Text = General.nz(rsDevis.Rows[0]["TotalVente"], "0") + " €";
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblhtDevis.Text = General.nz(rsDevis.Rows[0]["TotalVente"], "0") + " €";
                //===> Fin Modif Mondir
            }
            else
            {
                lblMontantDevis.Text = "0 €";
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lblhtDevis.Text = "0 €";
                //===> Fin Modif Mondir
            }
            rsDevis.Dispose();

            //== modif rachid du 29 12 2004, la requete ci dessous tombe en erreur quand
            //== le champs quantité à une valeur avec un signe ex: -1

            //sSQL = "SELECT SUM(BCD_Detail.BCD_PrixHT *  BCD_Detail.BCD_Quantite  ) AS TotalAchats FROM BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande WHERE " _
            //& "BonDeCommande.NumFicheStandard=" & sNumFicheStandard & " AND " _
            //& "NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL " _
            //& "AND (ISNUMERIC(BCD_Detail.BCD_PrixHT) = 1) AND (ISNUMERIC(BCD_Detail.BCD_Quantite) = 1) " _
            //& "GROUP BY BonDeCommande.NumFicheStandard,BCD_Detail.BCD_PrixHT, BCD_Detail.BCD_Quantite"

            General.sSQL = "SELECT BCD_Detail.BCD_PrixHT ,  BCD_Detail.BCD_Quantite, NoBonDeCommande" + " FROM BCD_Detail INNER JOIN BonDeCommande "
                + " ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande" + " WHERE " + "BonDeCommande.NumFicheStandard=" + sNumFicheStandard;

            //Calcul du montant total des achats réalisés sur ce devis
            rsDevis = tmpAdo.fc_OpenRecordSet(General.sSQL);
            if (rsDevis.Rows.Count > 0)
            {

                //rsDevis.MoveFirst();
                lblTotalAchats.Text = "0";

                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {

                    dbTotHT = 0;
                    bQte = true;
                    if (!string.IsNullOrEmpty(rsDevisRow["BCD_PrixHT"].ToString()) && General.nz(rsDevisRow["BCD_PrixHT"], 0).ToString() != "0")
                    {

                        if (string.IsNullOrEmpty(General.nz(rsDevisRow["BCD_Quantite"], "").ToString()))
                        {

                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention une ligne du bon de commande n°" + rsDevisRow["NoBonDeCommande"]
                                + " a un montant HT mais pas de quantite, la quantité 1 sera prise par défaut.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dbQte = 1;

                        }
                        else if (!General.IsNumeric(rsDevisRow["BCD_Quantite"].ToString()))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention une ligne du bon de commande n°" + rsDevisRow["NoBonDeCommande"]
                                + " a une quantité non numérique, cette ligne ne sera pas prise en compte dans" + " le total HT.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            bQte = false;

                        }
                        else
                        {
                            dbQte = Convert.ToDouble(rsDevisRow["BCD_Quantite"].ToString());
                        }

                        if (bQte == true)
                        {
                            dbTotHT = Convert.ToDouble(rsDevisRow["BCD_PrixHT"]) * dbQte;

                            lblTotalAchats.Text = ((Convert.ToDouble(General.nz(lblTotalAchats.Text, "0")) + (Convert.ToDouble(General.nz(dbTotHT, "0")))).ToString());
                        }
                        else
                        {

                        }
                    }

                    // rsDevis.MoveNext();

                }
                lblTotalAchats.Text = lblTotalAchats.Text + " €";
            }
            else
            {
                lblTotalAchats.Text = "0 €";
            }

            rsDevis.Dispose();
            //== fin modif 29 12 2004

            //Calcul du montant total des factures sur situation de ce devis
            tabNoFacture = new string[1];
            //Sert à afficher une info bulle
            tabMontantFact = new string[1];
            //Sert à afficher une info bulle

            strSelect = "";
            strSelect = "SELECT FactureManuellePied.MontantHT AS TotalSituations,FactureManuelleEntete.NoFacture FROM FactureManuellePied";
            strSelect = strSelect + " INNER JOIN FactureManuelleEntete ON ";
            strSelect = strSelect + " FactureManuellePied.CleFactureManuelle=FactureManuelleEntete.CleFactureManuelle ";
            strSelect = strSelect + " INNER JOIN Intervention ON ";
            strSelect = strSelect + " FactureManuelleEntete.NoFacture=Intervention.NoFacture ";

            strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + General.nz(sNumFicheStandard, "0") + "";
            strSelect = strSelect + " AND NOT FactureManuellePied.MontantHT IS NULL ";
            strSelect = strSelect + " GROUP BY FactureManuellePied.MontantHT,FactureManuelleEntete.NoFacture,Intervention.NumFicheStandard ";

            rsDevis = tmpAdo.fc_OpenRecordSet(strSelect);
            lblTotalSituations.Text = "0";
            i = 0;

            if (rsDevis.Rows.Count > 0)
            {
                //rsDevis.MoveFirst();
                Array.Resize(ref tabNoFacture, rsDevis.Rows.Count);
                Array.Resize(ref tabMontantFact, rsDevis.Rows.Count);
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    //if (tabNoFacture.Length < i)
                    //{
                    //    Array.Resize(ref tabNoFacture, i + 1);
                    //}
                    //if (tabMontantFact.Length < i)
                    //{
                    //    Array.Resize(ref tabMontantFact, i + 1);
                    //}
                    tabNoFacture[i] = rsDevisRow["NoFacture"] + "";
                    tabMontantFact[i] = Convert.ToString(rsDevisRow["TotalSituations"] + "");

                    lblTotalSituations.Text = Convert.ToString(Convert.ToDouble(General.nz(lblTotalSituations.Text, "0")) + Convert.ToDouble(General.nz(rsDevisRow["TotalSituations"], "0")));
                    i = i + 1;
                    //  rsDevis.MoveNext();
                }
                txtSituation.Text = lblTotalSituations.Text;
                lblTotalSituations.Text = lblTotalSituations.Text + " €";

            }
            else
            {
                lblTotalSituations.Text = "0 €";
                //ToolTip1.SetToolTip(lblTotalSituations, "Aucune facture enregistrée");todo
            }
            rsDevis.Dispose();

            strSelect = "";
            strSelect = strSelect + "SELECT Intervention.HeureDebut,Intervention.HeureFin,";
            strSelect = strSelect + " Intervention.Duree FROM Intervention ";
            strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + sNumFicheStandard;

            rsDevis = tmpAdo.fc_OpenRecordSet(strSelect);

            if (rsDevis.Rows.Count > 0)
            {
                //rsDevis.MoveFirst();
                sDuree = Convert.ToDateTime("01/01/1900 00:00:00");
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (!string.IsNullOrEmpty(rsDevisRow["Duree"].ToString()))
                    {

                        if (Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay > Convert.ToDateTime("00:00:00").TimeOfDay)
                        {
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(rsDevisRow["Duree"]).TimeOfDay.Hours);


                        }
                        else if (!string.IsNullOrEmpty(rsDevisRow["HeureDebut"].ToString()) &&
                            General.IsDate(rsDevisRow["HeureDebut"].ToString()) &&
                            !string.IsNullOrEmpty(rsDevisRow["HeureFin"].ToString()) &&
                            General.IsDate(rsDevisRow["HeureFin"].ToString()))
                        {
                            sDureeTemp = General.fc_calcDuree(Convert.ToDateTime(rsDevisRow["HeureDebut"]), Convert.ToDateTime(rsDevisRow["HeureFin"]));

                            sDureeTemp = Convert.ToDateTime(sDureeTemp).ToString("HH:mm:ss");
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(sDureeTemp).TimeOfDay.Seconds);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(sDureeTemp).TimeOfDay.Minutes);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(sDureeTemp).TimeOfDay.Hours);

                        }
                    }


                    // rsDevis.MoveNext();
                }
                rsDevis.Dispose();
                sDureeDiff = sDuree.Second;

                sDureeDiff = (sDuree - Convert.ToDateTime("01/01/1900 00:00:00")).TotalSeconds;

                if (sDureeDiff < 0)
                {
                    sDureeDiff = sDureeDiff * (-1);
                }
                lngHeure = Convert.ToInt32(sDureeDiff / 3600);

                if (lngHeure * 3600 > sDureeDiff)
                {
                    lngHeure = lngHeure - 1;
                }

                lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

                if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
                {
                    lngMinute = lngMinute - 1;
                }

                lngSeconde = Convert.ToInt32(sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

                if (General.Len(Convert.ToString(lngHeure)) == 1)
                {
                    lblTotalHeures.Text = "0" + lngHeure + ":";
                }
                else
                {
                    lblTotalHeures.Text = lngHeure + ":";
                }

                if (General.Len(Convert.ToString(lngMinute)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngMinute + ":";
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngMinute + ":";
                }

                if (General.Len(Convert.ToString(lngSeconde)) == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngSeconde;
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngSeconde;
                }
            }
            else
            {
                lblTotalHeures.Text = "00:00:00";
            }


            rsDevis = null;
            return functionReturnValue;

        }

        private void fc_FindCodeAnal(string sWhere)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_FindCodeAnal() - Enter in Function");
            //===> Fin Modif Mondir

            string sSQL = null;
            sSQL = "select INT_AnaCode from intervention ";
            sSQL = sSQL + sWhere;
            var tmpAdo = new ModAdo();
            General.rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
            if (General.rstmp.Rows.Count > 0)
            {
                txtINT_AnaCode.Text = General.nz(General.rstmp.Rows[0]["INT_AnaCode"].ToString(), "").ToString();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMenu_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMenu_Click - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                int londMsgb = 0;
                if (cmdAjouter.Enabled == true)
                {
                    londMsgb = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous changer de type de facture", "Menu", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                    if (londMsgb == 7)
                    {

                        cmdMenu.Focus();
                        return;
                    }
                    longCle = 0;
                    frmSynthese.Visible = false;
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    fraSynthese.Visible = false;
                    //===> Fin Modif Mondir
                    SablierOnOff1(true);
                    //vide les recordset
                    SSOleDBGrid1.DataSource = null;
                    SSOleDBGrid2.DataSource = null;
                    //Adodc2.RecordSource = "SELECT FactureManuellePiedBis.* From FactureManuellePiedBis Where FactureManuellePiedBIS.CleFactureManuelle =0"
                    SSOleDBGrid3.DataSource = null;
                    DataTable dt = new DataTable();
                    RemplitGrille(dt);
                    cmdRechercheImmeuble.Enabled = false;
                    cmdRechercheReg.Enabled = false;
                    InitialiseEcran(this);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    SSTab1.Enabled = false;
                    // ce code ne marche pas
                    //===================> Mondir le 15.06.2020 : If we use the visible false, so we cant use the .checked anymore, it will be always false !
                    ///==================> So the better is to hide it
                    //Frame9.Location = new Point(Frame9.Location.X, 30);

                    // Salma le 17.06.2020==========>j'ai remplacé la visbilité par la taille  pour la frame9 et le boutton cmdValidOpt
                    Frame9.Size = new Size(1374, 69);
                    cmdAjouter.Visible = false;
                    cmdSupprimer.Visible = false;
                    cmdValider.Visible = false;
                    cmdAnnuler.Visible = false;
                    CmdRechercher.Visible = false;
                    // CmdIntegration.Visible = False;
                    //======> modif Salma le 17.06.2020
                    //cmdValidOpt.Visible = true;
                    cmdValidOpt.Size = new Size(60, 35);
                    cmdMenu.Visible = false;
                    //Timer1.Enabled = True
                    SablierOnOff1(false);

                    if (OptConMan.Checked == true)
                    {
                        OptConMan.Focus();
                    }
                    else if (OptFioulMan.Checked == true)
                    {
                        OptFioulMan.Focus();
                    }
                    else if (OptTravMan.Checked == true)
                    {
                        OptTravMan.Focus();
                    }
                    else if (optIntervention.Checked)
                    {
                        optIntervention.Focus();
                    }

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous étes en mode ajout" + "\n" + "vous ne pouvez pas retouner dans le menu" + "\n" + "valider ou bien annuler", "action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    cmdMenu.Focus();
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdMenu_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdMiseAjour_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"CmdMiseAjour_Click - Updating facture");
            //===> Fin Modif Mondir

            string SelectionFormula = "";
            int nbRecAff = 0;
            string sqlExportPDF = null;
            int lnumfichestandard = 0;
            System.DateTime dbDateFacture = default(System.DateTime);
            string sNameExport = null;
            ReportDocument CR = new ReportDocument();
            ReportDocument CR2 = new ReportDocument();
            string sSQlEtatInt;
            DataTable rsEtatX;
            ModAdo rsEtatXADO = new ModAdo();
            string sTempxx;
            string sTempyy;
            string sOldCodeX;
            //===> Ahmed: Modification 08 09 2020 V 07 09 2020
            string sTYD_Code = "";
            //===> Fin

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            int lVersionDate;
            string sFileReport;
            //===> Fin Modif Mondir

            //Dim lNoLigneTva            As Long
            //Dim sSQlTva             As String
            //Dim sLibArticleTva      As String
            //Dim rsTVA300            As ADODB.Recordset
            //Dim dbTTCtva300         As Double
            //Dim bTVA300             As Boolean
            //Dim dtTva               As Date

            try
            {
                //fc_ConnectImprimante
                if (string.IsNullOrEmpty(CR.FileName))
                {
                    if (File.Exists(General.ETATOLDFACTUREMANUEL))
                    {
                        CR.Load(General.ETATOLDFACTUREMANUEL);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas !", "",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }


                }

                if (string.IsNullOrEmpty(CR2.FileName))
                {
                    if (File.Exists(General.ETATFacturemanuelleSQLv2))
                        CR2.Load(General.ETATFacturemanuelleSQLv2);

                }



                boolErreurGeneral = false;
                SQLTypeFacture = "";
                strMiseAJour = "";
                sNameExport = "";
                SablierOnOff(true);
                //----stocke la requete contenant les champs à verifier avant la mise à jour
                sCodeFamille =
                    "SELECT FactureManuelleEntete.CleFactureManuelle," + " FactureManuelleEntete.NoFacture,FactureManuelleEntete.DateFacture, FactureManuelleEntete.NumFacture,"
                                                                       + " FactureManuelleEntete.NCompte, FactureManuelleEntete.Facture," +
                                                                       " FactureManuelleEntete.ModeReglement,FactureManuelleEntete.Verifiee,"
                                                                       + " FactureManuelleEntete.NomIntervention, FactureManuelleDetail.MontantLigne," +
                                                                       " FactureManuelleDetail.TVA, FactureManuelleDetail.Compte,"
                                                                       + " FactureManuelleDetail.Compte7, FactureManuelleDetail.AnaActivite," +
                                                                       " FactureManuelleDetail.numeroligne, FactureManuellePied.MontantHT, FactureManuellePied.MontantTTC "
                                                                       + " FROM (FactureManuelleEntete LEFT JOIN FactureManuelleDetail" +
                                                                       " ON FactureManuelleEntete.CleFactureManuelle ="
                                                                       + " FactureManuelleDetail.CleFactureManuelle) LEFT JOIN FactureManuellePied"
                                                                       + " ON FactureManuelleEntete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle";

                //----Rajoute un critére pour la requete çi-dessus( personne qui a crée la facture)
                strMiseAJour = "";
                if (optPersonne.Checked == true)
                {
                    SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne ='" + strNomPersonne + "'";
                    strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + strNomPersonne + "' and ";
                }
                else if (OptNomChoisis.Checked == true)
                {
                    boolNom = false;

                    for (i = 0; i <= Combo1.Rows.Count - 1; i++)
                    {
                        string ff = Combo1.Rows[i].Cells[0].Value.ToString();
                        if (Combo1.Rows[i].Cells[0].Value.ToString() == Combo1.Text)
                        {
                            boolNom = true;
                            break;
                        }
                    }

                    if (boolNom == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Ce nom n'existe pas, choisisez un nom valide dans la liste.", "", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        SablierOnOff(false);
                        return;
                    }


                    londMsgb = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Attention vous allez mettre à jours des factures des factures crées par " + Combo1.Text, "",
                        MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information));
                    if (londMsgb != 7)
                    {
                        SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne ='" + Combo1.Text + "'  ";
                        strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + Combo1.Text + "' and";
                    }
                    else
                    {
                        SablierOnOff(false);
                        return;
                    }
                }
                else if (OptTous.Checked == true) //tested
                {
                    londMsgb = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox
                        .Show(
                            "Attention vous avez choisis l'option Tous, vous allez mettre à jours toutes les factures" +
                            Combo1.Text, "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information));
                    //===> Modif Mondir le 01.09.2020 pour corriger : https://groupe-dt.mantishub.io/view.php?id=1927
                    //if (londMsgb != 7)//tested
                    if (londMsgb == (int)DialogResult.Yes) //tested
                    {
                        SQLTypeFacture = " Where FactureManuelleEntete.NomPersonne is not null";
                    }
                    else
                    {

                        SablierOnOff(false);
                        return;
                    }
                }

                LongBaseNoFacture = 0;
                longNumfacture = 0;

                //----option par Numero de facture
                if (Option7.Checked == true)
                {

                    //----vérifie si une valeur numerique est saisie dans txtMiseAjour1
                    if (string.IsNullOrEmpty(txtMiseAjour1.Text) || !(General.IsNumeric(txtMiseAjour1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtMiseAjour1.Focus();
                        return;

                        //----vérifie si une valeur numerique est saisie dans txtMiseAjour1.
                    }
                    else if (string.IsNullOrEmpty(txtMiseAjour2.Text) || !(General.IsNumeric(txtMiseAjour2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK,
                            MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtMiseAjour2.Focus();
                        return;
                    }

                    //--- Le numero de facture doit etre identique
                    //If txtMiseAjour1 <> txtMiseAjour2 Then
                    //    MsgBox "Vous ne pouvez saisir qu'une seule facture à la fois.", vbCritical, "Données eronnées"
                    //    SablierOnOff False
                    //    txtMiseAjour2.SetFocus
                    //    Exit Sub
                    //End If
                    //----verifie si la facture en cours est validée.
                    if (!string.IsNullOrEmpty(txtEnCours.Text))
                    {
                        if (General.Left(txtEnCours.Text, 2) == "XX")
                        {
                            if (longCle >= Convert.ToDouble(txtMiseAjour1.Text) &&
                                longCle <= Convert.ToDouble(txtMiseAjour2.Text) && cmdAjouter.Enabled == false)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                    "Vous devez d'abord valider cette facture avant de la mettre à jour.",
                                    "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SablierOnOff(false);
                                return;
                            }
                        }
                    }

                    //----stocke la clause where des champs à vérifier.
                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.CleFactureManuelle >= " +
                                     txtMiseAjour1.Text
                                     + " And FactureManuelleEntete.CleFactureManuelle<= " + txtMiseAjour2.Text +
                                     " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";

                    sqlExportPDF = SQLTypeFacture;
                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.Facture= 0 and " +
                                     " FactureManuelleEntete.Imprimer = 0";
                    sCodeFamille = sCodeFamille + SQLTypeFacture;

                    //---Requete pour crystal Report
                    //rIf txtDateDeFacture < CDate(dbEURO) Then
                    //r   strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtMiseAjour1 & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtMiseAjour2 & " AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //rElse
                    //r    strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtMiseAjour1 & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtMiseAjour2 & " AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = 1 and {FactureManuelleEntete.Imprimer} = 1"
                    //rEnd If
                    //----facture en cours
                }
                else if (Option8.Checked == true)
                {
                    if (!string.IsNullOrEmpty(txtEnCours.Text))
                    {
                        if (General.Left(txtEnCours.Text, 2) == "XX")
                        {
                            if (cmdAjouter.Enabled == false)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                    "Vous devez d'abord valider cette facture avant de la mettre à jour.",
                                    "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SablierOnOff(false);
                                return;
                            }
                        }
                    }
                    else if (string.IsNullOrEmpty(txtEnCours.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "il n'y a pas de facture en cours", "Opération annulée", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        SablierOnOff(false);
                        return;
                    }

                    if (General.Left(txtEnCours.Text, 2) == "XX")
                    {

                        //----stocke la clause where des champs à vérifier.
                        SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.CleFactureManuelle = " + longCle +
                                         "" + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";
                        sqlExportPDF = SQLTypeFacture;
                        SQLTypeFacture = SQLTypeFacture +
                                         " AND FactureManuelleEntete.Facture = 0 and FactureManuelleEntete.Imprimer = 0";
                        sCodeFamille = sCodeFamille + SQLTypeFacture;
                        //---Requete pour crystal Report
                        //rIf txtDateDeFacture < CDate(dbEURO) Then
                        //r    strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle}= " & longCle & " AND {FactureManuelleEntete.TypeFacture}= '" & TypeFacture & "' and {FactureManuelleEntete.Facture}= true and {FactureManuelleEntete.Imprimer}= true"
                        //rElse
                        //r   strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle}= " & longCle & " AND {FactureManuelleEntete.TypeFacture}= '" & TypeFacture & "' and {FactureManuelleEntete.Facture}= 1 and {FactureManuelleEntete.Imprimer}= 1"
                        //rEnd If
                    }
                    else //tested
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Cette facture est déja mise à jour" + "\n" +
                            "Pour l'imprimer de nouveau, allez en visualisation", "Action annulées",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SablierOnOff(false);
                        return;
                    }

                    //----Option par période
                }
                else if (Option2.Checked == true) //tested
                {
                    if (string.IsNullOrEmpty(txtMiseDate1.Text) || !(General.IsDate(txtMiseDate1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date",
                            "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SablierOnOff(false);
                        txtMiseDate1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtMiseDate2.Text) || !(General.IsDate(txtMiseDate2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date",
                            "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SablierOnOff(false);
                        txtMiseDate2.Focus();
                        return;
                    }
                    else
                    {
                        txtMiseDate1.Text = Convert.ToDateTime(txtMiseDate1.Text).ToString("dd/MM/yyyy");
                        txtMiseDate2.Text = Convert.ToDateTime(txtMiseDate2.Text).ToString("dd/MM/yyyy");
                    }

                    if (!string.IsNullOrEmpty(txtEnCours.Text))
                    {
                        if (General.Left(txtEnCours.Text, 2) == "XX")
                        {
                            if (string.IsNullOrEmpty(txtDateDeFacture.Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                    "Vous n'avez pas saisie de date sur la facture en cours", "Opération annulé",
                                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SablierOnOff(false);
                                return;
                            }
                            else
                            {
                                if (Convert.ToDateTime(txtDateDeFacture.Text) >= Convert.ToDateTime(txtMiseDate1.Text)
                                    && Convert.ToDateTime(txtDateDeFacture.Text) <=
                                    Convert.ToDateTime(txtMiseDate2.Text)
                                    && cmdAjouter.Enabled == false)
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                        "Vous devez d'abord valider cette facture avant de la mettre à jour.",
                                        "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    SablierOnOff(false);
                                    return;
                                }
                            }
                        }
                    }

                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.DateFacture >='" +
                                     Convert.ToDateTime(txtMiseDate1.Text).ToString("dd/MM/yyyy")
                                     + "'" + " And  FactureManuelleEntete.DateFacture <='" +
                                     Convert.ToDateTime(txtMiseDate2.Text).ToString("dd/MM/yyyy")
                                     + "'" + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";
                    sqlExportPDF = SQLTypeFacture;
                    SQLTypeFacture = SQLTypeFacture + " AND FactureManuelleEntete.Facture = 0" +
                                     " and FactureManuelleEntete.Imprimer = 0";

                    sCodeFamille = sCodeFamille + SQLTypeFacture;

                    //---Requete pour crystal Report

                    dbdate1 = Convert.ToDateTime(txtMiseDate1.Text).ToString("yyyy,MM,dd");
                    dbdate2 = Convert.ToDateTime(txtMiseDate2.Text).ToString("yyyy,MM,dd");
                    //r strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture} >= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture} <= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"

                }

                //----verifie les factures
                adotemp = new DataTable();
                ModAdo tmpAdo = new ModAdo();
                adotemp = tmpAdo.fc_OpenRecordSet(sCodeFamille);

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"CmdMiseAjour_Click - Seleting Qury = {sCodeFamille}");
                Program.SaveException(null, $"CmdMiseAjour_Click - adotemp Found = {adotemp?.Rows.Count}");
                //===> Fin Modif Mondir

                VerifieFacture(ref adotemp);



                //if ((adotemp != null))
                //{
                //    adotemp = new DataTable();
                //}
                //----si aucune facture n'est trouvée
                if (i == 0) //tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Il n'y a aucune facture à mettre à jour", "Opération annulées", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    adotemp = new DataTable();
                    SablierOnOff(false);
                    return;
                    //----si des factures sont verifiée
                }
                else if (j > 0)
                {
                    //===> Mondir le 02.12.2020, s'il y a des factures
                    //===> Mondir le 02.12.2020, ticket 2102
                    if (txtNoDevis.Text != "")
                    {
                        if (General.fc_CtrResponsableTravaux(txtNoDevis.Text, 0) == false)
                            return;
                    }
                    //===> Fin Modif Mondir

                    adoPied = new DataTable();
                    ModAdo tmpAdoPied = new ModAdo();
                    //---- Recherche le plus grand numero de facture dans la table NoFacture.
                    if (boolIntervention == true) //tested
                    {
                        adoPied = tmpAdoPied.fc_OpenRecordSet(
                            "SELECT NoFactureIntervention.NumFacture, NoFactureIntervention.Autorise FROM NoFactureIntervention ORDER BY NoFactureIntervention.NumFacture ASC");
                    }
                    else
                    {
                        adoPied = tmpAdoPied.fc_OpenRecordSet(
                            "SELECT NoFacture.NumFacture, NoFacture.Autorise FROM NoFacture ORDER BY NoFacture.NumFacture ASC");
                    }

                    if (adoPied.Rows.Count > 0) //tested
                    {

                        // adoPied.MoveLast();
                        if (adoPied.Rows[adoPied.Rows.Count - 1]["Autorise"].ToString() == "False")
                        {
                            adoPied.Rows[adoPied.Rows.Count - 1]["Autorise"] = true;
                            LongBaseNoFacture =
                                Convert.ToInt32(General.nz(adoPied.Rows[adoPied.Rows.Count - 1]["NumFacture"], 0));
                            longNumfacture = LongBaseNoFacture + j;
                            var xxx = tmpAdoPied.Update();

                            var NewRow = adoPied.NewRow();
                            NewRow["NumFacture"] = longNumfacture;
                            adoPied.Rows.Add(NewRow);
                            xxx = tmpAdoPied.Update();
                            //                adoPied.CancelUpdate
                            //                adoPied.Close
                        }
                        else
                        {
                            adoPied.Dispose();

                            adoPied = null;
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                "La table des numeros de facture est en session, patientez et recommencez dans quelques instants.");
                            SablierOnOff(false);
                            return;
                        }
                    }

                    adoPied.Dispose();

                    adoPied = null;

                    //---- Numeroter les factures
                    //adocnn.BeginTrans
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    //===> Ajout du FactureManuelleEntete.CleFactureManuelle
                    adotemp = tmpAdo.fc_OpenRecordSet(
                        "SELECT FactureManuelleEntete.NoFacture, FactureManuelleEntete.Facture, " + " FactureManuelleEntete.NumFacture, FactureManuelleEntete.verifiee,"
                                                                                                  +
                                                                                                  " FactureManuelleEntete.Imprimer, FactureManuelleEntete.datefacture, " +
                                                                                                  " FactureManuelleEntete.dateecheance, FactureManuelleEntete.ModeReglement, FactureManuelleEntete.Definitive  "
                                                                                                  + " ,FactureManuelleEntete.CleFactureManuelle"
                                                                                                  + " From FactureManuelleEntete " +
                                                                                                  SQLTypeFacture +
                                                                                                  " and verifiee = 1");
                    LongBaseNoFacture = LongBaseNoFacture + 1;
                    i = 0;
                    foreach (DataRow rs in adotemp.Rows) //tested
                    {
                        if (boolIntervention == true)
                        {
                            if (General.sEvolution == "1")
                            {
                                sSQlEtatInt =
                                    "SELECT       Intervention.NoFacture, Intervention.NoDevis, Intervention.CodeEtat, "
                                    + " Intervention.CodeImmeuble, Intervention.NoIntervention, "
                                    + " DevisEnTete.CodeEtat AS CodeEtatDevis, DevisEnTete.TYD_Code, GestionStandard.NumFicheStandard "
                                    + " FROM            Intervention INNER JOIN "
                                    + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard LEFT OUTER JOIN "
                                    + " DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis "
                                    + " WHERE Intervention.NoFacture = '" + rs["NoFacture"] + "'";
                                rsEtatXADO = new ModAdo();
                                rsEtatX = rsEtatXADO.fc_OpenRecordSet(sSQlEtatInt);

                                //===> Mondir 03.08.2020, Ajout du LOG
                                Program.SaveException(null,
                                    $"CmdMiseAjour_Click - Working on facture = {rs["NoFacture"]}");
                                Program.SaveException(null, $"CmdMiseAjour_Click - Seleting Qury = {sSQlEtatInt}");
                                Program.SaveException(null,
                                    $"CmdMiseAjour_Click - Intervention Found = {rsEtatX?.Rows.Count}");
                                //===> Fin Modif Mondir

                                if (rsEtatX.Rows.Count > 0)
                                {
                                    //===> Mondir 05.08.2020, Ajout du LOG
                                    Program.SaveException(null, $"CmdMiseAjour_Click - if rsEtatX.Rows.Count");
                                    //===> Fin Modif Mondir

                                    sOldCodeX = rsEtatX.Rows[0]["CodeEtat"] + "";
                                    lnumfichestandard =
                                        Convert.ToInt32(General.nz(rsEtatX.Rows[0]["NumFicheStandard"], 0));

                                    //===> Mondir le 29.11.2020 pour ajouter la version V29.11.2020 ===> Tested
                                    //===> this line is commaned and changed by the line below
                                    //if (optIntervention.Checked)
                                    if (string.IsNullOrEmpty(General.nz(rsEtatX.Rows[0]["NoDevis"], "")?.ToString()))
                                    {
                                        //===> Mondir 06.08.2020, Ajout du LOG
                                        Program.SaveException(null,
                                            $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optIntervention.Checked");
                                        //===> Fin Modif Mondir
                                        if (!chkSolderDevis.Checked && rs["Definitive"] + "" != "1")
                                        {
                                            //===> Mondir 06.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optIntervention.Checked > if !chkSolderDevis.Checked");
                                            //===> Fin Modif Mondir
                                            sSQlEtatInt = "UPDATE Intervention SET Intervention.CodeEtat = 'TS',"
                                                          + " Intervention.NoFacture = '" + TypeFacture +
                                                          LongBaseNoFacture + "'"
                                                          + " Where Intervention.NoFacture = '" + rs["NoFacture"] + "'";

                                            var up = General.Execute(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if optIntervention.Checked > if");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - optIntervention.checked = true, chkSolderDevis.checked = false, rs[\"Definitive\"] = {rs["Definitive"]}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Update Intervention = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Intervention Updated = {up}");
                                            //===> Fin Modif Mondir

                                            foreach (DataRow row in rsEtatX.Rows)
                                                General.fc_HistoStatut(Convert.ToInt32(row["NoIntervention"]),
                                                    row["CodeImmeuble"] + "", "Fiche facture", row["CodeEtat"] + "",
                                                    "TS");
                                        }
                                        else
                                        {
                                            //===> Mondir 06.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optIntervention.Checked > else !chkSolderDevis.Checked");
                                            //===> Fin Modif Mondir
                                            //'=== Vu avec Fabiola le 14 05 2020 pour une facture définitive, on passe en T uniquement les interventions
                                            //'=== selectionnées contrairement au devis ou toutes les interventions liés à une fiche d'appel passent en

                                            sSQlEtatInt = "UPDATE Intervention SET Intervention.CodeEtat = 'T',"
                                                          + " Intervention.NoFacture = '" + TypeFacture +
                                                          LongBaseNoFacture + "'"
                                                          + " Where Intervention.NoFacture = '" + rs["NoFacture"] + "'";

                                            var up = General.Execute(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if optIntervention.Checked > else");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - optIntervention.checked = true, chkSolderDevis.checked = false, rs[\"Definitive\"] = {rs["Definitive"]}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Update Intervention = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Intervention Updated = {up}");
                                            //===> Fin Modif Mondir

                                            foreach (DataRow row in rsEtatX.Rows)
                                                General.fc_HistoStatut(Convert.ToInt32(row["NoIntervention"]),
                                                    row["CodeImmeuble"] + "", "Fiche facture", row["CodeEtat"] + "",
                                                    "T");

                                        }
                                    }
                                    //===> Mondir le 29.11.2020, commented in V29.11.2020 ===> Tested
                                    //else if (optDevis.Checked)
                                    else
                                    {
                                        //===> Mondir 06.08.2020, Ajout du LOG
                                        Program.SaveException(null,
                                            $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optDevis.Checked");
                                        //===> Fin Modif Mondir
                                        if (chkSolderDevis.Checked == false && rs["Definitive"] + "" != "1")
                                        {
                                            //===> Mondir 06.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optDevis.Checked > if !chkSolderDevis.Checked");
                                            //===> Fin Modif Mondir
                                            sSQlEtatInt = "UPDATE Intervention SET Intervention.CodeEtat = 'TS', " +
                                                          " Intervention.NoFacture = '" + TypeFacture +
                                                          LongBaseNoFacture +
                                                          "' Where                            Intervention.NoFacture = '" +
                                                          rs["NoFacture"] + "'";

                                            var up = General.Execute(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - else if optDevis.Checked > if");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - chkSolderDevis.checked = false, rs[\"Definitive\"] = {rs["Definitive"]}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Update Intervention = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Intervention Updated = {up}");
                                            //===> Fin Modif Mondir

                                            foreach (DataRow row in rsEtatX.Rows)
                                                General.fc_HistoStatut(Convert.ToInt32(row["NoIntervention"]),
                                                    row["CodeImmeuble"] + "", "Fiche         facture",
                                                    row["CodeEtat"] + "", "TS");

                                            sSQlEtatInt = "UPDATE       DevisEnTete" + " Set CodeEtat =  'TS' " +
                                                          " FROM            DevisEnTete             INNER JOIN" +
                                                          " GestionStandard ON DevisEnTete.NumeroDevis = GestionStandard.NoDevis " +
                                                          " WHERE             GestionStandard.NumFicheStandard =" +
                                                          lnumfichestandard;

                                            up = General.Execute(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Update DevisEnTete = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - DevisEnTete Updated = {up}");
                                            //===> Fin Modif Mondir
                                        }
                                        else
                                        {
                                            //===> Mondir 06.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - if rsEtatX.Rows.Count > if optDevis.Checked > else !chkSolderDevis.Checked");
                                            //===> Fin Modif Mondir

                                            //'=== facture défnitive.
                                            // Ahmed : modification le 08 09 2020 V 07 08 2020 : Suppression de la condition suivante par Rachid
                                            //if (General.nz(rsEtatX.Rows[0]["TYD_Code"], "") + "" != "")
                                            //{
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - TYD_Code = {rsEtatX.Rows[0]["TYD_Code"]} ");

                                            if (rsEtatX.Rows[0]["TYD_Code"] != null &&
                                                rsEtatX.Rows[0]["TYD_Code"].ToString() != "")
                                                sTYD_Code = rsEtatX.Rows[0]["TYD_Code"].ToString();
                                            else sTYD_Code = "";

                                            if (General.UCase(sTYD_Code.Trim()) == General.UCase("P3"))
                                            {
                                                sTempxx = "P3";
                                                sTempyy = "P3";
                                            }
                                            else if (General.UCase(sTYD_Code.Trim()) == General.UCase("P4"))
                                            {
                                                sTempxx = "F";
                                                sTempyy = "P4";
                                            }
                                            else
                                            {
                                                sTempxx = "T";
                                                sTempyy = "T";
                                            }

                                            rsEtatX.Dispose();
                                            rsEtatX = null;

                                            sSQlEtatInt =
                                                "SELECT        NoFacture, CodeEtat, CodeImmeuble, NoIntervention,"
                                                + " NumFicheStandard, Nofacture"
                                                + " From Intervention "
                                                + " WHERE   NumFicheStandard =" + lnumfichestandard;

                                            //=====> Modifier 22-06-2020 salma 
                                            //changer tmpAdo par rsEtatXADO (la source du tmpAdo se change et cela cause la non modification sur FactureManuelleEntete
                                            rsEtatX = rsEtatXADO.fc_OpenRecordSet(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - else if optDevis.Checked > else");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Select Qurey = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - Intervention Found = {rsEtatX?.Rows.Count}");
                                            //===> Fin Modif Mondir

                                            foreach (DataRow row in rsEtatX.Rows)
                                            {
                                                //===> Mondir 03.08.2020, Ajout du LOG
                                                Program.SaveException(null,
                                                    $"CmdMiseAjour_Click - Update Intervention, row[\"NoIntervention\"] = {row["NoIntervention"]}");
                                                //===> Fin Modif Mondir

                                                if (General.UCase(General.nz(row["NoFacture"], "")) ==
                                                    General.UCase(rs["NoFacture"].ToString()))
                                                {
                                                    sSQlEtatInt =
                                                        "UPDATE Intervention SET Intervention.CodeEtat = '" + sTempxx +
                                                        "', "
                                                        + " Intervention.NoFacture = '" + TypeFacture +
                                                        LongBaseNoFacture + "'"
                                                        + " WHERE   NoIntervention =" +
                                                        General.nz(row["NoIntervention"], 0);

                                                    var up = General.Execute(sSQlEtatInt);

                                                    //===> Mondir 03.08.2020, Ajout du LOG
                                                    Program.SaveException(null,
                                                        $"CmdMiseAjour_Click - else if optDevis.Checked > else > foreach > if, Update Qury = {sSQlEtatInt}");
                                                    Program.SaveException(null,
                                                        $"CmdMiseAjour_Click - Intervention Updated = {up}");
                                                    //===> Fin Modif Mondir
                                                }
                                                else
                                                {
                                                    sSQlEtatInt =
                                                        "UPDATE Intervention SET Intervention.CodeEtat = '" + sTempxx +
                                                        "'  WHERE                     NoIntervention =" +
                                                        General.nz(row["NoIntervention"], 0);

                                                    var up = General.Execute(sSQlEtatInt);

                                                    //===> Mondir 03.08.2020, Ajout du LOG
                                                    Program.SaveException(null,
                                                        $"CmdMiseAjour_Click - else if optDevis.Checked > else > foreach > else, Update Qury = {sSQlEtatInt}");
                                                    Program.SaveException(null,
                                                        $"CmdMiseAjour_Click - Intervention Updated = {up}");
                                                    //===> Fin Modif Mondir
                                                }

                                                General.fc_HistoStatut(Convert.ToInt32(row["NoIntervention"]),
                                                    row["CodeImmeuble"] + "", "Fiche             facture",
                                                    row["CodeEtat"] + "", sTempxx);
                                            }

                                            sSQlEtatInt = "UPDATE       DevisEnTete"
                                                          + " Set CodeEtat ='" + sTempyy + "'"
                                                          + " FROM            DevisEnTete INNER JOIN"
                                                          + " GestionStandard ON DevisEnTete.NumeroDevis = GestionStandard.NoDevis "
                                                          + " WHERE   GestionStandard.NumFicheStandard =" +
                                                          lnumfichestandard;

                                            var upp = General.Execute(sSQlEtatInt);

                                            //===> Mondir 03.08.2020, Ajout du LOG
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - else if optDevis.Checked > if, Update Qury = {sSQlEtatInt}");
                                            Program.SaveException(null,
                                                $"CmdMiseAjour_Click - DevisEnTete Updated = {upp}");
                                            //===> Fin Modif Mondir

                                            // Modif Ahmed: 08 09 2020 V 07 09 2020 : surppresion de la condition globale 'If nz(rsEtatX!TYD_Code, "") <> "" Then
                                            //}
                                        }
                                    }
                                }

                                rsEtatX?.Dispose();
                                rsEtatX = null;
                            }
                            else
                            {
                                if (optIntervention.Checked) //tested
                                {
                                    var xxx = General.Execute(
                                        "UPDATE Intervention SET Intervention.CodeEtat = 'T', Intervention.NoFacture = '" + TypeFacture
                                                                                                                          + LongBaseNoFacture +
                                                                                                                          "' Where Intervention.NoFacture = '" +
                                                                                                                          rs
                                                                                                                              ["NoFacture"] +
                                                                                                                          "'");

                                    //====> Mondir le 30.06.2020
                                    //====> Commenté le 03.08.2020, No Need for this line
                                    //Program.SaveException(null, "UPDATE Intervention SET Intervention.CodeEtat = 'T', Intervention.NoFacture = '" + TypeFacture + LongBaseNoFacture + "' Where Intervention.NoFacture = '" + rs["NoFacture"] + "'");
                                }
                                else if (optDevis.Checked == true) //tested
                                {
                                    if (chkSolderDevis.Checked == false)
                                    {
                                        General.Execute(
                                            "UPDATE Intervention SET Intervention.CodeEtat = 'TS', Intervention.NoFacture = '"
                                            + TypeFacture + LongBaseNoFacture + "' Where Intervention.NoFacture = '" +
                                            rs["NoFacture"] + "'");

                                        //====> Mondir le 30.06.2020, save qury to log
                                        //====> Commenté le 03.08.2020, No Need for this line
                                        //Program.SaveException(null, "UPDATE Intervention SET Intervention.CodeEtat = 'TS', Intervention.NoFacture = '" + TypeFacture +
                                        //                            LongBaseNoFacture + "' Where Intervention.NoFacture = '" + rs["NoFacture"] + "'");
                                        //====> Fin Modif Mondir
                                    }
                                    else
                                    {
                                        //== modif rachid du 10 janvier 2005, mettre toutes les interventions de la fiche d'appel à T.
                                        using (ModAdo tmp = new ModAdo())
                                            lnumfichestandard = Convert.ToInt32(
                                                tmp.fc_ADOlibelle(
                                                    "SELECT NumFicheStandard FROM INTERVENTION Where Intervention.NoFacture = '" +
                                                    rs["NoFacture"] + "'"));
                                        General.Execute(
                                            "UPDATE Intervention SET  CodeEtat = 'T' WHERE numFicheStandard =" +
                                            lnumfichestandard);

                                        General.Execute(
                                            "UPDATE Intervention SET Intervention.CodeEtat = 'T', Intervention.NoFacture = '" +
                                            TypeFacture + LongBaseNoFacture + "' Where Intervention.NoFacture = '" +
                                            rs["NoFacture"] + "'");

                                        //====> Mondir le 30.06.2020, save qury to log
                                        //====> Commenté le 03.08.2020, No Need for this line
                                        //Program.SaveException(null, "UPDATE Intervention SET  CodeEtat = 'T' WHERE numFicheStandard =" + lnumfichestandard);
                                        //Program.SaveException(null, "UPDATE Intervention SET Intervention.CodeEtat = 'T', Intervention.NoFacture = '" + TypeFacture + LongBaseNoFacture + "' Where Intervention.NoFacture = '" + rs["NoFacture"] + "'");
                                        //====> Fin Modif Mondir
                                    }
                                }
                            }
                        }

                        //===> Mondir 03.08.2020, Ajout du LOG
                        Program.SaveException(null,
                            $"CmdMiseAjour_Click - rs[\"NoFacture\"] = {rs["NoFacture"]} ===> NoFacture = {TypeFacture + LongBaseNoFacture}");
                        //===> Fin Modif Mondir

                        rs["NoFacture"] = TypeFacture + LongBaseNoFacture;
                        rs["NumFacture"] = LongBaseNoFacture;
                        rs["Facture"] = true;
                        rs["Imprimer"] = true;
                        if (General.sNoFactureManuelAuto == "1") //tested
                        {
                            //== Modif du 16 03 2006. date de facture automatisée.
                            rs["DateFacture"] = DateTime.Today;
                            rs["DateEcheance"] = General.CalcEch(Convert.ToDateTime(rs["DateFacture"]),
                                rs["ModeReglement"] + "");
                        }

                        LongBaseNoFacture = LongBaseNoFacture + 1;
                        i = i + 1;
                        //  adotemp.MoveNext();

                        //=== modif du 05/10/2020, ajout de l'article PDA.

                        //'''            If sTVA300 <> "" Then
                        //'''                    ' === Controle si il existe une date d'achévement.
                        //'''                    sSQlTva = "SELECT   TOP 1     IDA_DateAchevement From IDA_ImmDateAchevement " _
                        //'''                        & " WHERE        Codeimmeuble = '" & gFr_DoublerQuote(txtCodeimmeuble) & "' ORDER BY IDA_DateAchevement DESC"
                        //'''                    sSQlTva = fc_ADOlibelle(sSQlTva)
                        //'''
                        //'''                    bTVA300 = False
                        //'''
                        //'''                    If sSQlTva <> "" Then
                        //'''                        If IsDate(sSQlTva) Then
                        //'''                                dtTva = DateAdd("yyyy", -2, Date)
                        //'''
                        //'''                                If CDate(DateValue(sSQlTva)) >= dtTva Then
                        //'''                                    bTVA300 = True
                        //'''                                End If
                        //'''                        End If
                        //'''                    End If
                        //'''
                        //'''                    If bTVA300 = True Then
                        //'''                            ' === controle si le code TVA existe dans la facture manuelle.
                        //'''                            sSQlTva = "SELECT   FactureManuelleDetail.article,  FactureManuelleDetail.NumeroLigne," _
                        //'''                                    & " FactureManuellePied.MontantTTC " _
                        //'''                                    & " FROM FactureManuelleDetail INNER JOIN" _
                        //'''                                    & " FactureManuellePied ON FactureManuelleDetail.CleFactureManuelle = FactureManuellePied.CleFactureManuelle " _
                        //'''                                    & " WHERE        FactureManuellePied.CleFactureManuelle = " & nz(adotemp!CleFactureManuelle, 0) _
                        //'''                                    & " ORDER BY NumeroLigne DESC "
                        //'''
                        //'''                            Set rsTVA300 = fc_OpenRecordSet(sSQlTva, 1)
                        //'''                            bTVA300 = False
                        //'''                            lNoLigneTva = 0
                        //'''
                        //'''                            Do While rsTVA300.EOF = False
                        //'''
                        //'''                                    If lNoLigneTva = 0 Then
                        //'''                                        lNoLigneTva = nz(rsTVA300!NumeroLigne, 0)
                        //'''                                        lNoLigneTva = lNoLigneTva + 1
                        //'''                                        dbTTCtva300 = nz(rsTVA300!MontantTTc, 0)
                        //'''                                    End If
                        //'''
                        //'''                                    If UCase(nz(rsTVA300!Article, "")) = UCase(sTVA300) Then
                        //'''                                            bTVA300 = True
                        //'''                                            Exit Do
                        //'''                                    End If
                        //'''                                    rsTVA300.MoveNext
                        //'''                            Loop
                        //'''                            If bTVA300 = False Then
                        //'''
                        //'''                                   If dbTTCtva300 < dbSeuilTTCtva300 Then
                        //'''
                        //'''                                        sLibArticleTva = "SELECT Designation1 From FacArticle " _
                        //'''                                            & " WHERE  CodeArticle = '" & sTVA300 & "'"
                        //'''                                        sLibArticleTva = fc_ADOlibelle(sLibArticleTva)
                        //'''
                        //'''                                        If sLibArticleTva <> "" Then
                        //'''                                                ' === insére une ligne vide.
                        //'''                                                sSQlTva = "INSERT INTO FactureManuelleDetail" _
                        //'''                                                    & " ( NumeroLigne, Designation, CleFactureManuelle) " _
                        //'''                                                    & " VALUES        (" & lNoLigneTva & ", ''," & nz(adotemp!CleFactureManuelle, 0) & ")"
                        //'''
                        //'''                                                adocnn.Execute sSQlTva
                        //'''
                        //'''                                                lNoLigneTva = lNoLigneTva + 1
                        //'''
                        //'''                                                sSQlTva = "INSERT INTO FactureManuelleDetail" _
                        //'''                                                    & " (Article, NumeroLigne, Designation, CleFactureManuelle) " _
                        //'''                                                    & " VALUES        ('" & sTVA300 & "'," _
                        //'''                                                        & lNoLigneTva & ", '" & gFr_DoublerQuote(sLibArticleTva) & "'," & nz(adotemp!CleFactureManuelle, 0) & ")"
                        //'''
                        //'''                                                adocnn.Execute sSQlTva
                        //'''                                        End If
                        //'''                                    End If
                        //'''                            End If
                        //'''
                        //'''                            rsTVA300.Close
                        //'''                            Set rsTVA300 = Nothing
                        //'''                    End If
                        //'''            End If
                    }

                    //----si le nombre de facture vérifiée n'est pas égale au total des factures récupéres
                    //----dans la requete çi-dessus alors on annule la transaction.
                    if (j == i)
                    {
                        //adocnn.CommitTrans
                    }
                    else
                    {
                        //adocnn.RollbackTrans
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Des erreurs se sont produites, recommencez l'opération" + "\n" +
                            "et cliquez sur synthese pour connaitre l'état d'avancement de la mise à jour",
                            "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SablierOnOff(false);
                        return;
                    }

                    var xxxx = tmpAdo.Update();
                    adotemp.Dispose();

                    //===> Mondir 03.08.2020, Ajout du LOG
                    Program.SaveException(null, $"CmdMiseAjour_Click - FactureEntet Updated = {xxxx}");
                    //===> Fin Modif Mondir

                    //--- fournis le nouveau numero de facture en cours dans txtEnCours si celle-çi est facturée.
                    adotemp = tmpAdo.fc_OpenRecordSet(
                        "SELECT FactureManuelleEntete.NoFacture FROM FactureManuelleEntete WHERE FactureManuelleEntete.CleFactureManuelle = " +
                        longCle + "  AND FactureManuelleEntete.TypeFacture='" + TypeFacture +
                        "' AND FactureManuelleEntete.Facture= 1 and FactureManuelleEntete.Imprimer = 1");

                    if (adotemp.Rows.Count > 0) //tested
                    {
                        txtEnCours.Text = adotemp.Rows[0]["NoFacture"].ToString() + "";
                        //EcranLecture
                    }

                    adotemp.Dispose();

                    adotemp = null;


                }

                //=== requete pour CR.
                //=== Par date.
                // ==par date
                if (Option2.Checked == true)
                {
                    //If txtMiseDate1 >= CDate(dbEURO) Then
                    //   strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture} >= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture} <= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"

                    //   CR2.WindowShowPrintSetupBtn = True
                    //   CR2.SelectionFormula = strMiseAJour
                    //   CR2.Action = 1
                    //   CR2.SelectionFormula = ""
                    //ElseIf txtMiseDate1 < dbEURO And txtMiseDate2 < dbEURO Then

                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.datefacture} >= Date (" + dbdate1 + ") and "
                                   + " {FactureManuelleEntete.dateFacture} <= Date (" + dbdate2 +
                                   ") AND {FactureManuelleEntete.TypeFacture} ='"
                                   + TypeFacture +
                                   "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    if (General.dtdateFactManuV3 >= txtMiseDate1.Text.ToDate())
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    else if (General.IsDate(General.sDateFactureManuelleV2))
                    {
                        if (Convert.ToDateTime(txtMiseDate1.Text) >= Convert.ToDateTime(General.sDateFactureManuelleV2))
                        {
                            if (chk3bacs.CheckState == CheckState.Checked)
                            {
                                // fc_ImprimeBacs(General.ETATFacturemanuelleSQLv2, strMiseAJour);TODO General.ETATFacturemanuelleSQLv2 n'existe pas
                            }

                            //     CR2.WindowShowPrintSetupBtn = true;
                            //Todo this cr2 n'existe pas
                            //SelectionFormula = strMiseAJour;
                            //CrystalReportFormView crform = new CrystalReportFormView(CR2, SelectionFormula);
                            //crform.Show();                       
                            //  // CR2.Action = 1;
                            //  SelectionFormula = "";
                            //sNameExport = CR2.Name;

                            //===> Mondir le 22.01.2021, CR was not found by Selma, https://groupe-dt.mantishub.io/view.php?id=2222 , i replaced it with CR
                            SelectionFormula = strMiseAJour;
                            CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                            crform.Show();
                            SelectionFormula = "";
                        }
                        else
                        {
                            if (chk3bacs.CheckState == CheckState.Checked)
                            {
                                fc_ImprimeBacs(General.ETATOLDFACTUREMANUEL, strMiseAJour);
                            }

                            // CR.WindowShowPrintSetupBtn = true;
                            SelectionFormula = strMiseAJour;
                            CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                            crform.Show();
                            SelectionFormula = "";
                        }
                    }
                    else
                    {
                        if (chk3bacs.CheckState == CheckState.Checked)
                        {
                            fc_ImprimeBacs(General.ETATOLDFACTUREMANUEL, strMiseAJour);
                        }

                        //CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                        crform.Show();
                        SelectionFormula = "";
                    }


                    //                    If chk3bacs.value = 1 Then
                    //                        fc_ImprimeBacs ETATOLDFACTUREMANUEL, strMiseAJour
                    //                    End If
                    //
                    //                    CR.WindowShowPrintSetupBtn = True
                    //                    CR.SelectionFormula = strMiseAJour
                    //                    CR.Action = 1
                    //                    CR.SelectionFormula = ""
                    //ElseIf txtMiseDate1 < dbEURO Then

                    //    strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture} >= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture} < Date (" & dbEuroCr & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //    CR.WindowShowPrintSetupBtn = True
                    //    CR.SelectionFormula = strMiseAJour
                    //    CR.Action = 1
                    //    CR.SelectionFormula = ""
                    //CR.Reset

                    //     strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture} >= Date (" & dbEuroCr & ") and {FactureManuelleEntete.dateFacture} <= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //     CR2.WindowShowPrintSetupBtn = True
                    //     CR2.SelectionFormula = strMiseAJour
                    //     CR2.Action = 1
                    //     CR2.SelectionFormula = ""

                    //  End If
                    // par Numero de facture
                }
                else if (Option7.Checked == true)
                {
                    // If RechDate(txtMiseAjour1) = 0 Then
                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.CleFactureManuelle} >=" + txtMiseAjour1.Text +
                                   " and {FactureManuelleEntete.CleFactureManuelle} <="
                                   + txtMiseAjour2.Text + " AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture +
                                   "' and {FactureManuelleEntete.Facture} = true " +
                                   " and {FactureManuelleEntete.Imprimer} = true";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ====> Tested
                    lVersionDate = RechDate(txtMiseAjour1.Text.ToInt());
                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir 
                    else if (lVersionDate == 1) //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> remplacer RechDate(txtMiseAjour1) par lVersionDate ===> Tested
                    {
                        if (chk3bacs.CheckState == CheckState.Checked)
                        {
                            // fc_ImprimeBacs(General.ETATFacturemanuelleSQLv2, strMiseAJour);TODO General.ETATFacturemanuelleSQLv2 n'existe pas
                        }

                        // CR2.WindowShowPrintSetupBtn = true;
                        //Todo this cr2 n'existe pas
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                        crform.Show();
                        SelectionFormula = "";
                        //sNameExport = CR2.Name;
                    }
                    else
                    {
                        if (chk3bacs.CheckState == CheckState.Checked)
                        {
                            fc_ImprimeBacs(General.ETATOLDFACTUREMANUEL, strMiseAJour);
                        }

                        //==============>Tested
                        //CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                        crform.Show();
                        SelectionFormula = "";
                    }

                    //Else
                    //        strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle} >=" & txtMiseAjour1 & " and {FactureManuelleEntete.CleFactureManuelle} <=" & txtMiseAjour2 & " AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //        CR2.WindowShowPrintSetupBtn = True
                    //        CR2.SelectionFormula = strMiseAJour
                    //        CR2.Action = 1
                    //        CR2.SelectionFormula = ""
                    //End If
                    // facture en cours.
                }
                else if (Option8.Checked == true)
                {
                    //If RechDate(longCle) = 0 Then
                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.CleFactureManuelle}= " + longCle +
                                   " AND {FactureManuelleEntete.TypeFacture}= '"
                                   + TypeFacture +
                                   "' and {FactureManuelleEntete.Facture}= true and {FactureManuelleEntete.Imprimer}= true";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    lVersionDate = RechDate(longCle);
                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir 
                    else if (lVersionDate == 1) //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> remplacer RechDate(txtMiseAjour1) par lVersionDate
                    {

                        if (chk3bacs.CheckState == CheckState.Checked)
                        {
                            // fc_ImprimeBacs(General.ETATFacturemanuelleSQLv2, strMiseAJour);TODO General.ETATFacturemanuelleSQLv2 n'existe pas
                        }

                        //    CR2.WindowShowPrintSetupBtn = true;
                        //Todo this cr2 n'existe pas
                        SelectionFormula = strMiseAJour;
                        //===> Mondir le 22.01.2021, this code was commented coz CR2 was not found by Salma, https://groupe-dt.mantishub.io/view.php?id=2222
                        CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                        crform.Show();
                        SelectionFormula = "";
                        //===> Fin Modif Mondir
                        //CrystalReportFormView crform = new CrystalReportFormView(CR2, SelectionFormula);
                        //crform.Show();
                        //SelectionFormula = "";
                        //sNameExport = CR2. Name;
                    }
                    else
                    {
                        if (chk3bacs.CheckState == CheckState.Checked)
                        {
                            //  fc_ImprimeBacs(ref General.ETATOLDFACTUREMANUEL, ref strMiseAJour);
                        }

                        //CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView crform = new CrystalReportFormView(CR, SelectionFormula);
                        crform.Show();
                        SelectionFormula = "";

                        //Else
                        //        strMiseAJour = strMiseAJour & "{FactureManuelleEntete.CleFactureManuelle}= " & longCle & " AND {FactureManuelleEntete.TypeFacture}= '" & TypeFacture & "' and {FactureManuelleEntete.Facture}= true and {FactureManuelleEntete.Imprimer}= true"
                        //        CR2.WindowShowPrintSetupBtn = True
                        //        CR2.SelectionFormula = strMiseAJour
                        //        CR2.Action = 1
                        //        CR2.SelectionFormula = ""
                        //End If
                        //End If

                    }
                }

                //----Si çelle-çi sont imprimées, mettre le champs imlprimer a true
                //----adocnn.Execute "UPDATE FactureManuelleEntete SET FactureManuelleEntete.Imprimer = True " & SQLTypeFacture & " and verifiee = true"

                //-----Si une erreur dans les factures est trouvée, on affiche le message çi-dessus.
                fc_ExportFactMan(sqlExportPDF, sNameExport);
                if (General.UCase(General.Left(txtEnCours.Text, 2)) != "XX")
                    EcranLecture(this);
                if (boolErreurGeneral == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Toutes les factures n'ont pas été mise à jour, aprés l'impression des factures valides." +
                        "\n" + "Cliquez sur synthèse pour visualiser les erreurs détectées.",
                        "Factures non valides", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                SelectionFormula = "";
                if (General.sNoFactureManuelAuto == "1")
                {
                    //== si la date de facture est automatisée, on rafraichi la date
                    //== de la facture en cours. Modif du 16 03 2006.
                    using (var tmpado = new ModAdo())
                    {
                        txtDateDeFacture.Text = tmpado.fc_ADOlibelle(
                            "SELECT DateFacture FROM FactureManuelleEntete " + " WHERE NoFacture ='" +
                            txtEnCours.Text + "'");
                    }
                }

                SablierOnOff(false);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                if (ex.InnerException != null)
                    Program.SaveException(null,
                        "error in crystal report function cmdImrpimer_click ===> details : " +
                        ex.InnerException.Message);
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sWhere"></param>
        /// <param name="sCheminExport"></param>
        /// <returns></returns>
        public double fc_ExportFactMan(string sWhere, string sCheminExport = "")
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ExportFactMan() - Enter In The Function - sCheminExport = {sCheminExport}");
            //===> Fin Modif Mondir

            double functionReturnValue = 0;


            string sCodeImmeuble = null;
            string sNumeroFact = null;
            string sTitre = null;
            int i = 0;
            double nbRec = 0;
            object X = null;

            string sFiltreDevis1 = null;
            string sRep = null;
            DataTable rs = default(DataTable);

            ModAdo modAdoRs = new ModAdo();

            ReportDocument report = new ReportDocument();
            rs = new DataTable();
            try
            {

                if (!(sWhere.ToUpper()).Contains("WHERE"))
                {
                    sWhere = " WHERE " + sWhere;
                }

                sWhere = sWhere + " AND FactureManuelleEntete.Facture=1 and FactureManuelleEntete.Imprimer=1";
                SQL = "SELECT FactureManuelleEntete.* FROM FactureManuelleEntete ";
                SQL = SQL + sWhere;

                rs = modAdoRs.fc_OpenRecordSet(SQL);

                ReportDocument crAppl = new ReportDocument();

                if (string.IsNullOrEmpty(sCheminExport))
                {
                    //Set Report = crAppl.OpenReport(gsRpt & "FacturemanuelleSQL-V9.rpt")
                    //=== modif du 11 frv 2015

                    if (rs.Rows.Count > 0)
                    {
                        if (General.IsDate(rs.Rows[0]["DateFacture"].ToString()))
                        {
                            if (Convert.ToDateTime(rs.Rows[0]["DateFacture"].ToString()) >= General.dtdateFactManuV2)
                            {
                                if (File.Exists(General.ETATFACTUREMANUELExportV2))
                                {
                                    report.Load(General.ETATFACTUREMANUELExportV2);
                                }
                                else
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                        "Ce rapport n'existe pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return functionReturnValue;
                                }
                            }
                            else //tested
                            {
                                if (File.Exists(General.ETATFACTUREMANUELExport))
                                {
                                    report.Load(General.ETATFACTUREMANUELExport);
                                }
                                else
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                        "Ce rapport n'existe pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return functionReturnValue;
                                }
                            }
                        }
                        else
                        {
                            if (File.Exists(General.ETATFACTUREMANUELExport))
                            {
                                report.Load(General.ETATFACTUREMANUELExport);
                            }
                            else
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                    "Ce rapport n'existe pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return functionReturnValue;
                            }
                        }
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez prévenir immédiatement votre éditeur, l'export n'a pas fonctionné.");
                    return functionReturnValue;
                    //Set Report = crAppl.OpenReport(sCheminExport)
                }

                i = 0;
                nbRec = rs.Rows.Count;
                System.Windows.Forms.Application.DoEvents();
                if (rs.Rows.Count > 0)
                {

                    //rs.MoveFirst();
                    foreach (DataRow row in rs.Rows)
                    {
                        i = i + 1;
                        System.Windows.Forms.Application.DoEvents();
                        sCodeImmeuble = row["Affaire"].ToString().Trim() + "";
                        sNumeroFact = row["NoFacture"].ToString().Replace("/", "-") + "";
                        sTitre = "Facture semi-automatique pour " + sCodeImmeuble + " du " + row["DateFacture"];
                        sRep = "\\Factures\\out";
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble);
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Factures ");
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + sRep);

                        if (Dossier.fc_ControleFichier(General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact + ".pdf") == false)
                        {

                            report.RecordSelectionFormula = "{FactureManuelleEnTete.NoFacture}='" + row["NoFacture"] + "'";

                            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf");

                            General.Execute("UPDATE FactureManuelleEntete SET ExportPDF=1 WHERE NoFacture='" + sNumeroFact + "'");
                            SQL = "INSERT INTO DOC_Documents (";
                            SQL = SQL + "DOC_Nat,";
                            SQL = SQL + "DOC_PhyPath,";
                            SQL = SQL + "DOC_VirPath,";
                            SQL = SQL + "DOC_DTPath,";
                            SQL = SQL + "DOC_Name,";
                            SQL = SQL + "DOC_DCreat,";
                            SQL = SQL + "DOC_DModif,";
                            SQL = SQL + "DOC_FType,";
                            SQL = SQL + "DOC_Imm,";
                            SQL = SQL + "DOC_InOut,";
                            SQL = SQL + "DOC_Sujet,";
                            SQL = SQL + "DOC_NoPiece";
                            SQL = SQL + ") VALUES (";
                            SQL = SQL + "'Factures',";
                            //SQL = SQL & "'\\DELOSTAL-DC-01\DataDT\" & sCodeImmeuble & sRep & "\Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                            SQL = SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            SQL = SQL + "'../Data/" + sCodeImmeuble + sRep.Replace("\\", "/") + "/Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            SQL = SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf" + "',";
                            SQL = SQL + "'Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            SQL = SQL + "'" + DateTime.Now + "',";
                            SQL = SQL + "'" + DateTime.Now + "',";
                            SQL = SQL + "'PDF File',";
                            SQL = SQL + "'" + sCodeImmeuble + "',";
                            SQL = SQL + "'out',";

                            SQL = SQL + "'" + StdSQLchaine.gFr_DoublerQuote(sTitre) + "',";
                            SQL = SQL + "'" + sNumeroFact.Replace("/", "-") + ".pdf'";
                            SQL = SQL + ")";
                            General.Execute(SQL);
                        }
                        General.Execute("UPDATE FactureManuelleEntete SET ExportPDF=1 WHERE NoFacture='" + sNumeroFact + "'");
                        // rs.MoveNext();
                    }
                }

                //  Report = null;

                crAppl = null;
                functionReturnValue = nbRec;
                return functionReturnValue;
            }
            catch (Exception ex)
            {

                functionReturnValue = 0;
                Erreurs.gFr_debug(ex, this.Name + ";fc_exportFactMan");
                return functionReturnValue;

            }

        }

        //TODO faut verifier cette fct
        private void fc_ImprimeBacs(string sFile, string sSelectionFormula)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ImprimeBacs() - Enter In The Function - sFile = {sFile}, sSelectionFormula = {sSelectionFormula}");
            //===> Fin Modif Mondir

            ReportDocument CR = new ReportDocument();
            CR.Load(sFile);
            /*todo
             * 
             * short NameLength = 0;
            short formulacount = 0;
            short result = 0;
            short jobnum = 0;
            short FormulaN = 0;
            short textLength = 0;
            int NameHandle = 0;
            int textHandle = 0;
            string formulaName = null;
            string FormulaText = null;
            string SelectionText = null;
            short dialogflag = 0;
            DevMode crDEVMODE = default(DevMode);
            string Affi_dupli = null;
            int Cpt = 0;
            object Cpt_Ax = null;
            short sectionCode = 0;
            short sectionN = 0;
            string TempText = null;
            PESectionOptions SectionOptions = default(PESectionOptions);
            string Formule_Anfac = null;
            //Formule Etat FacturecontratA_Annex1_DI
            string Formule_AnDef = null;
            //Formule Etat FacturecontratA_Annex1_MP
            short mainjob1 = 0;
            short mainjob2 = 0;
            int subreportHandle = 0;
            short Npages = 0;
            Printer X = default(Printer);
            bool fok = false;
            string sWhere = null;
            string sport = null;


            //== ouvre le moteur Crystal.
            result = ApiCrystal.PEOpenEngine();
            if (result == 0)
            {
                Interaction.MsgBox("Could not start the print engine. Execution must halt.", MsgBoxStyle.OkOnly + MsgBoxStyle.Critical, "Serious Error");
            }

            //== PEOpenPrintJob : pointe sur l'etat.
            jobnum = ApiCrystal.PEOpenPrintJob(sFile);
            // Name from label on sample form
            //== PEOpenSubreport: pointe le sous état pour permettre d'utiliser
            //== ses formules.
            //mainjob1% = PEOpenSubreport(jobnum%, "FactureContratA_An_fac.rpt")
            ErrorTrap("", jobnum);
            // mainjob2% = PEOpenSubreport(jobnum%, "FactureContratA_AnDef.rpt")
            // Define the size of the SectionOptions structure
            SectionOptions.StructSize = ApiCrystal.PE_SIZEOF_SECTION_OPTIONS;
            SelectionText = sSelectionFormula;
            ///Selection des enregistrements
            result = ApiCrystal.PESetSelectionFormula(jobnum, SelectionText);
            ErrorTrap("", jobnum);
            result = ApiCrystal.PEEnableProgressDialog(jobnum, 1);
            // Set whether or not to display progress dialog
            ErrorTrap("", jobnum);
            result = ApiCrystal.PESetDialogParentWindow(jobnum, this.Handle.ToInt32());
            // Set dialog parent to main form
            ErrorTrap("", jobnum);

            ///Sort directement en impression
            result = ApiCrystal.PEOutputToPrinter(jobnum, 1);
            // Second value is number of copies
            ErrorTrap("", jobnum);
            crDEVMODE.dmOrientation = devModeCrystal.DMORIENT_PORTRAIT;
            crDEVMODE.dmDeviceName = "\\\\SVR-GESTEN\\Lexmark T620";
            // Name of the printer
            crDEVMODE.dmSpecVersion = 0x320;
            // Version of the DEVMODE structure - the structure used in this code is for Win32
            crDEVMODE.dmSize = 68;
            // Size of the DevMode structure
            crDEVMODE.dmFields = devModeCrystal.DM_ORIENTATION | devModeCrystal.DM_DEFAULTSOURCE;
            // Do the SelectPrinter call

            for (Cpt = 1; Cpt <= 3; Cpt++)
            {

                ErrorTrap("", jobnum);
                switch (Cpt)
                {
                    case 1:
                        // Bac 1
                        crDEVMODE.dmDefaultSource = 1;
                        break;
                    case 2:
                        //Bac 2
                        crDEVMODE.dmDefaultSource = 2;
                        break;
                    case 3:
                        //Bac3
                        crDEVMODE.dmDefaultSource = 3;
                        break;

                }

                ErrorTrap("GetNPages in PrintPreview", jobnum);
                foreach (X in Printers)
                {
                    if (X.DeviceName == "\\\\SVR-GESTEN\\Lexmark T620")
                    {
                       
                        sport = X.Port;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
             
                result = ApiCrystal.PESelectPrinter(jobnum, "winspool", "\\\\SVR-GESTEN\\Lexmark T620", sport, ref crDEVMODE);
                ErrorTrap("", jobnum);
                result = ApiCrystal.PEStartPrintJob(jobnum, true);
                // Note that flag must ALWAYS be true - setting to false will yield unpredictable results
                ErrorTrap("", jobnum);
            }
            ApiCrystal.PEClosePrintJob(jobnum);
            ErrorTrap("", jobnum);
            ApiCrystal.PECloseEngine();
            return;*/



        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="CleFac"></param>
        /// <returns></returns>
        private short RechDateOnNoFac(int CleFac)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"RechDateOnNoFac() - Enter In The Function - CleFac = {CleFac}");
            //===> Fin Modif Mondir

            short functionReturnValue = 0;

            try
            {
                if (!General.IsDate(General.sDateFactureManuelleV2))
                {
                    functionReturnValue = 0;
                    return functionReturnValue;
                }

                //=== si rechdate1 = 0 alors facture en franc
                //=== si rechdate1 = 1 alors facture en franc et en euro
                rsDate = new DataTable();
                ModAdo ModAdorsDate = new ModAdo();

                rsDate = ModAdorsDate.fc_OpenRecordSet("SELECT FactureManuelleEntete.DateFacture"
                    + " From FactureManuelleEntete"
                    + " WHERE FactureManuelleEntete.NumFacture=" + CleFac);

                if (rsDate.Rows.Count > 0)
                {
                    if (General.IsDate(rsDate.Rows[0]["DateFacture"].ToString()))
                    {
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        if (Convert.ToDateTime(rsDate.Rows[0]["DateFacture"].ToString()) >=
                            Convert.ToDateTime(General.dtdateFactManuV3))
                        {
                            functionReturnValue = 2;
                        }
                        //===> Fin Modif Mondir
                        else if (Convert.ToDateTime(rsDate.Rows[0]["DateFacture"].ToString()) >= Convert.ToDateTime(General.sDateFactureManuelleV2))
                        {
                            functionReturnValue = 1;
                        }
                        else
                        {
                            functionReturnValue = 0;
                        }
                    }
                    else
                    {
                        functionReturnValue = 0;
                    }
                }
                rsDate.Dispose();

                rsDate = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RechDateOnNoFac");
                return functionReturnValue;
            }


        }
        /// <summary>
        /// Mondir integrer les modif de la version V17.09.2020 
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheImmeuble_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sReq = null;

            lblP3.Visible = false;
            lblChaudCondensation.Visible = false;
            General.boolautre = false;
            string where_order = "";
            string requete = "";
            try
            {
                if (optGerant.Checked == true)//tested
                {

                    requete = "SELECT Table1.Code1 AS \"Code gérant\", Table1.Nom AS \"nom\" , Immeuble.CodeImmeuble AS \"Code immeuble\"," +
                       " Immeuble.Adresse as \"ADRESSE\" FROM Table1 INNER JOIN Immeuble ON Table1.Code1 = Immeuble.Code1";

                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles par gérant" };
                    if (!string.IsNullOrEmpty(txtCodeimmeuble.Text))
                    {
                        fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeimmeuble.Text } });
                    }
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fournisEntete();//tested
                        fg.Dispose(); fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                            fournisEntete();
                            fg.Dispose(); fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();

                    adotemp = null;
                }
                else if (optParticulier.Checked == true)
                {

                    requete = "SELECT ImmeublePart.CodeParticulier AS \"Code Particulier\", ImmeublePart.Nom AS \"Nom Particulier\"," +
                        " ImmeublePart.CodeImmeuble AS \"Code Immeuble\" FROM ImmeublePart" +
                        " LEFT JOIN (Table1 RIGHT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1) ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble";
                    where_order = " ImmeublePart.CodeImmeuble='" + txtCodeimmeuble.Text + "'";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles par Particuliers" };

                    if (!string.IsNullOrEmpty(txtCodeimmeuble.Text))
                    {
                        fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeimmeuble.Text } });
                    }
                    adotemp = new DataTable();
                    var tmpAdo = new ModAdo();


                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        adotemp = tmpAdo.fc_OpenRecordSet("SELECT ImmeublePart.CodeParticulier, ImmeublePart.Nom,"
                       + " ImmeublePart.CodeImmeuble, immeuble.comptedivers, Immeuble.Adresse as [IMMAdresse],"
                       + " Immeuble.CodePostal AS CP, Immeuble.Ville AS Ville1, Immeuble.CommentaireFacture1,"
                       + " Immeuble.CommentaireFacture2, Immeuble.CodeTVA,Immeuble.P3, ImmeublePart.Adresse as [PARTAdresse],"
                       + " ImmeublePart.Ville, ImmeublePart.CodePostal, ImmeublePart.NCompte as NC, immeuble.ChaudCondensation "
                       + " FROM ImmeublePart LEFT JOIN Immeuble ON ImmeublePart.CodeImmeuble ="
                       + " Immeuble.CodeImmeuble WHERE ImmeublePart.CodeParticulier='"
                       //============> Mondir : fg.ugResultat.ActiveRow.Cells[4] why ? god knows !
                       + StdSQLchaine.gFr_DoublerQuote(fg.ugResultat.ActiveRow.Cells[1].Value.ToString())
                       + "' AND ImmeublePart.CodeImmeuble='" + txtCodeimmeuble.Text + "'");
                        if (adotemp.Rows.Count > 0)
                        {
                            if (adotemp.Rows[0]["P3"].ToString() == "True")
                            {
                                lblP3.Visible = true;
                            }
                            //====> Mondir : Pour integrer les modifs de la version V15.09.2020
                            else
                            {
                                //===> Mondir le 02.12.2020, == false et non == true, https://groupe-dt.mantishub.io/view.php?id=2106
                                //lblP3.Visible = true;
                                //===> Fin modif Mondir
                                lblP3.Visible = false;
                            }
                            //====> Fin Modif Mondir
                            if (General.nz(adotemp.Rows[0]["ChaudCondensation"], 0).ToString() == "1")
                            {
                                lblChaudCondensation.Visible = true;
                            }
                            else
                            {
                                lblChaudCondensation.Visible = false;
                            }
                            txtGerNom.Text = adotemp.Rows[0]["Nom"].ToString() + "";
                            txtGerAdresse1.Text = adotemp.Rows[0]["PARTAdresse"].ToString() + "";
                            // txtGerAdresse2 = !adresse2 & ""
                            //' txtGerAdresse3 = !adresse3 & ""

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txtGerAdresse2.Text = "";
                            txtGerAdresse3.Text = "";
                            //===> Fin Modif Mondir

                            txtGerCP.Text = adotemp.Rows[0]["CodePostal"].ToString() + "";
                            txtGerVille.Text = adotemp.Rows[0]["Ville"].ToString() + "";
                            txtCodeimmeuble.Text = adotemp.Rows[0]["CodeImmeuble"].ToString() + "";
                            txtImmAdresse1.Text = adotemp.Rows[0]["IMMAdresse"].ToString() + "";
                            txtImmCP.Text = adotemp.Rows[0]["CP"].ToString() + "";
                            txtImmVille.Text = adotemp.Rows[0]["Ville1"].ToString() + "";
                            txtNCompte.Text = adotemp.Rows[0]["NC"].ToString() + "";
                            strTRouTP = adotemp.Rows[0]["CodeTVA"].ToString() + "";
                            if (string.IsNullOrEmpty(strTRouTP))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                            }
                            // txtCommentaire1 = !CommentaireFacture1 & ""
                            // txtCommentaire2 = !CommentaireFacture2 & ""
                            txtCommentaire1.Text = "";
                            txtCommentaire2.Text = "";

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txt_0.Text = adotemp.Rows[0]["Nom"] + "";
                            txt_1.Text = adotemp.Rows[0]["PARTAdresse"] + "";
                            txt_2.Text = "";
                            txt_3.Text = "";
                            txt_4.Text = "";
                            txt_5.Text = adotemp.Rows[0]["CodePostal"] + "";
                            txt_6.Text = adotemp.Rows[0]["Ville"] + "";
                            txt_7.Text = "";
                            txt_8.Text = "";
                            txt_9.Text = "1";
                            //===> Fin Modif Mondir

                            txtTVA.Text = strTRouTP;
                            txtMode.Text = General.sCodeReglement;

                            fc_ModeRegl(true);
                            using (tmpAdo = new ModAdo())
                            {
                                txtModeLibelle.Text = tmpAdo.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement"
                                    + " where Code='" + txtMode.Text + "'");
                            }

                        }
                        else
                        {
                            txtCommentaire1.Text = "";
                            txtCommentaire2.Text = "";
                            txtGerNom.Text = "";
                            txtGerAdresse1.Text = "";
                            txtGerAdresse2.Text = "";
                            txtGerAdresse3.Text = "";
                            txtGerCP.Text = "";
                            txtGerVille.Text = "";
                            // txtCodeimmeuble = ""
                            txtImmAdresse1.Text = "";
                            txtImmAdresse2.Text = "";
                            txtImmCP.Text = "";
                            txtImmVille.Text = "";
                            txtNCompte.Text = "";
                            txtRefClient.Text = "";
                            strTRouTP = "";
                            //voir déclaration variable dans général
                            txtTVA.Text = "";
                            txtMode.Text = "";
                            txtModeLibelle.Text = "";

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txt_0.Text = "";
                            txt_1.Text = "";
                            txt_2.Text = "";
                            txt_3.Text = "";
                            txt_4.Text = "";
                            txt_5.Text = "";
                            txt_6.Text = "";
                            txt_7.Text = "";
                            txt_8.Text = "";
                            txt_9.Text = "";
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        }
                        fg.Dispose(); fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            adotemp = tmpAdo.fc_OpenRecordSet("SELECT ImmeublePart.CodeParticulier, ImmeublePart.Nom,"
                      + " ImmeublePart.CodeImmeuble, immeuble.comptedivers, Immeuble.Adresse as [IMMAdresse],"
                      + " Immeuble.CodePostal AS CP, Immeuble.Ville AS Ville1, Immeuble.CommentaireFacture1,"
                      + " Immeuble.CommentaireFacture2, Immeuble.CodeTVA,Immeuble.P3, ImmeublePart.Adresse as [PARTAdresse],"
                      + " ImmeublePart.Ville, ImmeublePart.CodePostal, ImmeublePart.NCompte as NC, immeuble.ChaudCondensation "
                      + " FROM ImmeublePart LEFT JOIN Immeuble ON ImmeublePart.CodeImmeuble ="
                      + " Immeuble.CodeImmeuble WHERE ImmeublePart.CodeParticulier='"
                      + StdSQLchaine.gFr_DoublerQuote(fg.ugResultat.ActiveRow.Cells[4].Value.ToString())
                      + "' AND ImmeublePart.CodeImmeuble='" + txtCodeimmeuble.Text + "'");
                            if (adotemp.Rows.Count > 0)
                            {
                                if (adotemp.Rows[0]["P3"].ToString() == "True")
                                {
                                    lblP3.Visible = true;
                                }
                                //====> Mondir : Pour integrer les modifs de la version V15.09.2020
                                else
                                {
                                    //===> Mondir le 02.12.2020, == false et non == true, https://groupe-dt.mantishub.io/view.php?id=2106
                                    //lblP3.Visible = true;
                                    //===> Fin modif Mondir
                                    lblP3.Visible = false;
                                }
                                //====> FIn Modif Mondir
                                if (General.nz(adotemp.Rows[0]["ChaudCondensation"], 0).ToString() == "1")
                                {
                                    lblChaudCondensation.Visible = true;
                                }
                                else
                                {
                                    lblChaudCondensation.Visible = false;
                                }
                                txtGerNom.Text = adotemp.Rows[0]["Nom"].ToString() + "";
                                txtGerAdresse1.Text = adotemp.Rows[0]["PARTAdresse"].ToString() + "";
                                // txtGerAdresse2 = !adresse2 & ""
                                //' txtGerAdresse3 = !adresse3 & ""
                                txtGerCP.Text = adotemp.Rows[0]["CodePostal"].ToString() + "";
                                txtGerVille.Text = adotemp.Rows[0]["Ville"].ToString() + "";
                                txtCodeimmeuble.Text = adotemp.Rows[0]["CodeImmeuble"].ToString() + "";
                                txtImmAdresse1.Text = adotemp.Rows[0]["IMMAdresse"].ToString() + "";
                                txtImmCP.Text = adotemp.Rows[0]["CP"].ToString() + "";
                                txtImmVille.Text = adotemp.Rows[0]["Ville1"].ToString() + "";
                                txtNCompte.Text = adotemp.Rows[0]["NC"].ToString() + "";
                                strTRouTP = adotemp.Rows[0]["CodeTVA"].ToString() + "";
                                if (string.IsNullOrEmpty(strTRouTP))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                                }
                                // txtCommentaire1 = !CommentaireFacture1 & ""
                                // txtCommentaire2 = !CommentaireFacture2 & ""
                                txtCommentaire1.Text = "";
                                txtCommentaire2.Text = "";
                                txtTVA.Text = strTRouTP;
                                txtMode.Text = General.sCodeReglement;

                                fc_ModeRegl(true);
                                using (tmpAdo = new ModAdo())
                                {
                                    txtModeLibelle.Text = tmpAdo.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement"
                                        + " where Code='" + txtMode.Text + "'");
                                }

                            }
                            else
                            {
                                txtCommentaire1.Text = "";
                                txtCommentaire2.Text = "";
                                txtGerNom.Text = "";
                                txtGerAdresse1.Text = "";
                                txtGerAdresse2.Text = "";
                                txtGerAdresse3.Text = "";
                                txtGerCP.Text = "";
                                txtGerVille.Text = "";
                                // txtCodeimmeuble = ""
                                txtImmAdresse1.Text = "";
                                txtImmAdresse2.Text = "";
                                txtImmCP.Text = "";
                                txtImmVille.Text = "";
                                txtNCompte.Text = "";
                                txtRefClient.Text = "";
                                strTRouTP = "";
                                //voir déclaration variable dans général
                                txtTVA.Text = "";
                                txtMode.Text = "";
                                txtModeLibelle.Text = "";
                            }
                            fg.Dispose(); fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            string sSQL = "";
            //===> Fin Modif Mondir

            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"CmdRechercher_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                SSOleDBGrid1.UpdateData();

                General.boolautre = true;
                adoManuel = new DataTable();
                var adoManuelMod = new ModAdo();

                //Modif 17-07-2020 ajouté par salma
                string req = "SELECT NomPersonne as \"Cree Par\", FactureManuelleEntete.NoFacture as \"NoFacture\","
                    + "FactureManuelleEntete.NomIntervention as \"CodeImmeuble\", FactureManuelleEntete.DateFacture as \"DateFacture\","
                    + " FactureManuelleEntete.Client as \"Gérant\" ,FactureManuelleEntete.RefNoFacture as \"Ref NoFacture\"  From FactureManuelleEntete";
                //Fin modif
                string where = "FactureManuelleEntete.TypeFacture ='" + TypeFacture + "'";

                SablierOnOff(true);
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de factures" };
                fg.SetValues(new Dictionary<string, string> { { "NomPersonne", strNomPersonne } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    //Modif 17-07-2020 ajouté par salma
                    SQLIntervention = "";
                    //Fin modif
                    fc_load(ev.Row.Cells["NoFacture"].Value + "");

                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    sSQL = "SELECT  TOP (1) NumFicheStandard From Intervention "
                           + " WHERE  NoFacture = '" + General.nz(ev.Row.Cells["NoFacture"].Text, 0) + "'";
                    using (var tmpModAdo = new ModAdo())
                        sSQL = General.nz(tmpModAdo.fc_ADOlibelle(sSQL), "0").ToString();
                    fc_Synthese(Convert.ToInt32(sSQL));
                    //===> Fin Modif Mondir

                    fg.Dispose();
                    fg.Close();

                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        //Modif 17-07-2020 ajouté par salma
                        SQLIntervention = "";
                        //Fin modif
                        fc_load(fg.ugResultat.ActiveRow.Cells["NoFacture"].Value + "");

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        sSQL = "SELECT  TOP (1) NumFicheStandard From Intervention "
                               + " WHERE  NoFacture = '" + General.nz(fg.ugResultat.ActiveRow.Cells["NoFacture"].Text, 0) + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQL = General.nz(tmpModAdo.fc_ADOlibelle(sSQL), "0").ToString();
                        fc_Synthese(Convert.ToInt32(sSQL));
                        //===> Fin Modif Mondir

                        fg.Dispose();
                        fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdRechercher_Click");
                return;
            }

        }
        /// <summary>
        /// /tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheReg_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheReg_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                string req = "SELECT CodeReglement.Code as \"code\", CodeReglement.Libelle as \"Libelle\", CodeReglement.ModeReglt as \"Mode de Reglement\" FROM CodeReglement";
                string where_order = "";

                SearchTemplate fg = new SearchTemplate(this, null, req, where_order, "") { Text = "Recherche de factures" };
                SablierOnOff(true);
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtMode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtModeLibelle.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    txtTypeReglement.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtMode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtModeLibelle.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        txtTypeReglement.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                        fg.Dispose(); fg.Close();

                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();


                SablierOnOff(false);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheReg_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechImmeuble_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechImmeuble_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                General.boolautre = false;
                if (string.IsNullOrEmpty(txtEnCours.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour créer une facture appuyer d'abord sur le bouton Ajouter.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                string req = "SELECT Immeuble.CodeImmeuble as \"Code immeuble\", Immeuble.Adresse as \"Adresse\",Immeuble.ville as \"Ville\" From Immeuble";
                string where_order = "";

                SearchTemplate fg = new SearchTemplate(this, null, req, where_order, "") { Text = "Recherche de factures" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtImmeuble.Text = fg.ugResultat.ActiveRow.Cells["Code immeuble"].Value + "";
                    txtImmeuble_KeyPress(txtImmeuble, new KeyPressEventArgs((char)13));
                    fg.Dispose(); fg.Close();
                };
                fg.ShowDialog();



                //'=== exit sub tres important...
                return;
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblCodeImmeuble.Text = txtImmeuble.Text;

                    fc_ChargeContrat(txtImmeuble.Text);
                    SablierOnOff(true);
                    SSOleDBGrid3.DataSource = null;
                    adotemp = new DataTable();
                    DataTable dt = new DataTable();
                    RemplitGrille(dt);

                    var _with18 = adotemp;
                    var tmp = new ModAdo();

                    SQL = "SELECT Intervention.CodeEtat, Intervention.Deplacement, Intervention.CodeImmeuble,"
                    + " Intervention.Designation, Intervention.Commentaire, Intervention.Commentaire2,"
                    + " Intervention.Commentaire3, Intervention.DateRealise, Intervention.Intervenant,"
                    + " Intervention.NumFicheStandard, Intervention.NoIntervention,"
                    + " GestionStandard.Document, Intervention.Article, Intervention.Achat,"
                    + " Intervention.Acompte, INT_Rapport1, INT_Rapport2,INT_Rapport3, Intervention.Wave, "
                    + " Intervention.HeureDebut,Intervention.HeureFin,Intervention.Duree, nofacture"
                    + " FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard"
                    + " WHERE (((Intervention.CodeImmeuble)='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble.Text) + "')) ";

                    if (optDevis.Checked == true)
                    {
                        //== modif du 07 janvier 2007.
                        // SQL = SQL & " AND Intervention.CodeEtat = 'D' ORDER BY Intervention.NumFicheStandard DESC,Intervention.DateRealise "
                        //== récupération de toutes les interventions liées aux devis.
                        SQL = SQL + " AND (GestionStandard.NumFicheStandard IN" + " (SELECT     NumFicheStandard From intervention" + " WHERE      codeetat = 'D'))";

                    }
                    else
                    {
                        SQL = SQL + " AND Intervention.CodeEtat = 'E' ORDER BY Intervention.DateRealise ";
                    }
                    if (General.sEvolution == "1")
                        SQL = SQL + " AND (Intervention.nofacture is null or Intervention.nofacture = '') ";

                    SQL = SQL + " ORDER BY Intervention.NumFicheStandard ";
                    _with18 = tmp.fc_OpenRecordSet(SQL);

                    if (_with18.Rows.Count > 0)
                    {

                        RemplitGrille(_with18);
                    }
                    _with18.Dispose();


                    adotemp = null;

                    if (SSOleDBGrid3.Rows.Count > 0 && !string.IsNullOrEmpty(SSOleDBGrid3.Rows[0].Cells["Document"].Value.ToString()))
                    {
                        CmdFax.Enabled = true;
                    }
                    else
                    {
                        CmdFax.Enabled = false;
                    }


                    if (SSOleDBGrid3.Rows.Count > 0 && SSOleDBGrid3.Rows[0].Cells["Achat"].Value.ToString() == "1")
                    {
                        cmdAchat.Enabled = true;
                    }
                    else
                    {
                        cmdAchat.Enabled = false;
                    }
                    SablierOnOff(false);

                    //----Renvoie le focus
                    if (SSOleDBGrid3.Rows.Count > 0)
                    {
                        SSOleDBGrid3.Focus();
                    }
                    else
                    {
                        txtImmeuble.Enabled = true;
                        txtImmeuble.Focus();
                    }

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {


                        txtImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lblCodeImmeuble.Text = txtImmeuble.Text;

                        fc_ChargeContrat(txtImmeuble.Text);
                        SablierOnOff(true);
                        SSOleDBGrid3.DataSource = null;
                        DataTable dt = new DataTable();
                        RemplitGrille(dt);
                        adotemp = new DataTable();

                        var _with18 = adotemp;
                        var tmp = new ModAdo();

                        SQL = "SELECT Intervention.CodeEtat, Intervention.Deplacement, Intervention.CodeImmeuble,"
                        + " Intervention.Designation, Intervention.Commentaire, Intervention.Commentaire2,"
                        + " Intervention.Commentaire3, Intervention.DateRealise, Intervention.Intervenant,"
                        + " Intervention.NumFicheStandard, Intervention.NoIntervention,"
                        + " GestionStandard.Document, Intervention.Article, Intervention.Achat,"
                        + " Intervention.Acompte, INT_Rapport1, INT_Rapport2,INT_Rapport3, Intervention.Wave, "
                        + " Intervention.HeureDebut,Intervention.HeureFin,Intervention.Duree, nofacture"
                        + " FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard"
                        + " WHERE (((Intervention.CodeImmeuble)='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble.Text) + "')) ";

                        if (optDevis.Checked == true)
                        {
                            //== modif du 07 janvier 2007.
                            // SQL = SQL & " AND Intervention.CodeEtat = 'D' ORDER BY Intervention.NumFicheStandard DESC,Intervention.DateRealise "
                            //== récupération de toutes les interventions liées aux devis.
                            SQL = SQL + " AND (GestionStandard.NumFicheStandard IN" + " (SELECT     NumFicheStandard From intervention" + " WHERE      codeetat = 'D'))";

                        }
                        else
                        {
                            SQL = SQL + " AND Intervention.CodeEtat = 'E' ORDER BY Intervention.DateRealise ";
                        }

                        _with18 = tmp.fc_OpenRecordSet(SQL);

                        if (_with18.Rows.Count > 0)
                        {

                            RemplitGrille(_with18);
                        }
                        _with18.Dispose();


                        adotemp = null;

                        if (SSOleDBGrid3.Rows.Count > 0 && !string.IsNullOrEmpty(SSOleDBGrid3.Rows[0].Cells["Document"].Value.ToString()))
                        {
                            CmdFax.Enabled = true;
                        }
                        else
                        {
                            CmdFax.Enabled = false;
                        }


                        if (SSOleDBGrid3.Rows.Count > 0 && SSOleDBGrid3.Rows[0].Cells["Achat"].Value.ToString() == "True")
                        {
                            cmdAchat.Enabled = true;
                        }
                        else
                        {
                            cmdAchat.Enabled = false;
                        }
                        SablierOnOff(false);

                        //----Renvoie le focus
                        if (SSOleDBGrid3.Rows.Count > 0)
                        {
                            SSOleDBGrid3.Focus();
                        }
                        else
                        {
                            txtImmeuble.Enabled = true;
                            txtImmeuble.Focus();
                        }
                        fg.Dispose(); fg.Close();

                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechImmeuble_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_ChargeContrat(string sCodeImmeuble)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChargeContrat() - Enter In The Function - sCodeImmeuble = {sCodeImmeuble}");
            //===> Fin Modif Mondir

            int i = 0;
            DataTable rsContrat = default(DataTable);
            ModAdo rsContratModAdo = new ModAdo();
            string strSelect = null;

            try
            {
                if (GridContrat.Rows.Count > 0)
                {
                    GridContrat.DataSource = null;

                }

                rsContrat = new DataTable();
                strSelect = "SELECT Contrat.NumContrat,Contrat.Avenant,Contrat.LibelleCont1,";
                strSelect = strSelect + "Contrat.CodeArticle,FacArticle.Designation1,";
                strSelect = strSelect + "Contrat.DateFin,Contrat.Resiliee";
                strSelect = strSelect + " FROM Contrat ";
                strSelect = strSelect + " INNER JOIN FacArticle ON ";
                strSelect = strSelect + " Contrat.CodeArticle=FacArticle.CodeArticle ";
                strSelect = strSelect + " WHERE Contrat.CodeImmeuble='" + sCodeImmeuble + "'";
                strSelect = strSelect + " ORDER BY Contrat.NumContrat,Contrat.Avenant";
                if (sCodeImmeuble == "0")
                {
                    lblCodeImmeuble.Visible = false;
                }
                else
                {
                    lblCodeImmeuble.Visible = true;
                    lblCodeImmeuble.Text = sCodeImmeuble;
                }


                rsContrat = rsContratModAdo.fc_OpenRecordSet(strSelect);
                var dtGridContrat = new DataTable();
                dtGridContrat.Columns.Add("Code Contrat");
                dtGridContrat.Columns.Add("Avenant");
                dtGridContrat.Columns.Add("Intitulé");
                dtGridContrat.Columns.Add("Prestation");
                dtGridContrat.Columns.Add("datefin");
                dtGridContrat.Columns.Add("Resiliee");
                GridContrat.DataSource = dtGridContrat;

                var newRow = dtGridContrat.NewRow();

                if (rsContrat.Rows.Count > 0)//tested
                {
                    foreach (DataRow RowContrat in rsContrat.Rows)
                    {
                        var _with19 = rsContrat;
                        newRow["Code Contrat"] = RowContrat["NumContrat"];
                        newRow["Avenant"] = RowContrat["Avenant"];
                        newRow["Intitulé"] = RowContrat["LibelleCont1"];
                        newRow["Prestation"] = RowContrat["Designation1"];
                        newRow["datefin"] = RowContrat["DateFin"];
                        newRow["Resiliee"] = RowContrat["Resiliee"];

                        dtGridContrat.Rows.Add(newRow.ItemArray);
                        /*GridContrat.AddItem(_with19.Fields("NumContrat").Value + Constants.vbTab + _with19.Fields("avenant").Value 
                            + Constants.vbTab + _with19.Fields("LibelleCont1").Value + Constants.vbTab + _with19.Fields("Designation1").Value
                            + Constants.vbTab + _with19.Fields("datefin").Value + Constants.vbTab + _with19.Fields("Resiliee").Value);*/

                    }
                    GridContrat.DataSource = dtGridContrat;
                }

                rsContrat.Dispose();

                rsContrat = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_ChargeContrat ");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSupprimer_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                SablierOnOff1(true);
                if (General.Left(txtEnCours.Text, 2) != "XX")
                {
                    //londMsgb = MsgBox("Attention cette facture a été mise à jour. Désirez vous supprimer cette facture?", vbDefaultButton2 + vbYesNo + vbCritical, "Suppression")
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cette facture a éte mise à jour, vous ne pouvez pas la supprimer.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else
                {
                    londMsgb = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous supprimer cette facture?", "Suppression", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question));
                }
                if (londMsgb != 7)
                {
                    if (boolIntervention == true)
                    {
                        if (General.Left(txtEnCours.Text, 2) == "XX")
                        {
                            /// Mondir le 16.06.2020, Ajouter Historique de status pour les inter, demandé par Fabiola
                            /// ====================> This Is Not In VB6, 
                            if (General.sEvolution == "1")
                            {
                                var sSQL_2 = "SELECT  NoIntervention, CodeEtat  "
                                         + " From Intervention WHERE NoFacture = '" + txtEnCours.Text + "'";
                                var rs_2Ado = new ModAdo();
                                var rs_2 = rs_2Ado.fc_OpenRecordSet(sSQL_2);
                                if (rs_2.Rows.Count > 0)
                                {
                                    foreach (DataRow r2 in rs_2.Rows)
                                    {
                                        if (optIntervention.Checked)
                                            General.fc_HistoStatut(Convert.ToInt32(General.nz(r2["NoIntervention"], 0)), txtCodeimmeuble.Text, "Fiche facture", "S", "E");
                                        else if (optDevis.Checked)
                                            General.fc_HistoStatut(Convert.ToInt32(General.nz(r2["NoIntervention"], 0)), txtCodeimmeuble.Text, "Fiche facture", "S", "D");
                                    }

                                }
                                rs_2.Dispose();
                                rs_2 = null;
                            }

                            //modif du 20092002
                            if (optIntervention.Checked)
                            {
                                General.Execute("UPDATE Intervention SET  CodeEtat = 'E' where NoFacture ='" + txtEnCours.Text + "' and CodeEtat <> 'D'");
                                General.Execute("UPDATE Intervention SET Intervention.NoFacture ='' where NoFacture ='" + txtEnCours.Text + "'");

                                //====> Mondir le 30.06.2020, save qury to log
                                Program.SaveException(null, "UPDATE Intervention SET  CodeEtat = 'E' where NoFacture ='" + txtEnCours.Text + "' and CodeEtat <> 'D'");
                                Program.SaveException(null, "UPDATE Intervention SET Intervention.NoFacture ='' where NoFacture ='" + txtEnCours.Text + "'");
                                //====> Fin modif Mondir
                            }
                            else if (optDevis.Checked == true)
                            {
                                General.Execute("UPDATE Intervention SET  CodeEtat = 'D' where NoFacture ='" + txtEnCours.Text + "'");
                                General.Execute("UPDATE Intervention SET Intervention.NoFacture ='' where NoFacture ='" + txtEnCours.Text + "'");

                                //====> Mondir le 30.06.2020, save qury to log
                                Program.SaveException(null, "UPDATE Intervention SET  CodeEtat = 'D' where NoFacture ='" + txtEnCours.Text + "'");
                                Program.SaveException(null, "UPDATE Intervention SET Intervention.NoFacture ='' where NoFacture ='" + txtEnCours.Text + "'");
                                //====> Fin modif Mondir
                            }
                        }
                    }
                    General.Execute("DELETE  FROM FactureManuelleEnTete WHERE CleFactureManuelle=" + longCle);
                    InitialiseEcran(this);
                    cmdAjouter.Enabled = true;
                    cmdAnnuler.Enabled = false;
                    cmdSupprimer.Enabled = false;
                    cmdValider.Enabled = false;
                    cmdRechercheImmeuble.Enabled = false;
                    cmdRechercheReg.Enabled = false;
                    strTRouTP = "";
                    txtEnCours.Text = "";
                    txtTVA.Text = "";
                    boolAddnew = false;
                    frmSynthese.Visible = false;
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    fraSynthese.Visible = false;
                    //===> Fin Modif Mondir

                    SSOleDBGrid3.DataSource = null;
                    DataTable dt = new DataTable();
                    RemplitGrille(dt);
                    boolinsertInterv = true;
                    cmdInserer.Enabled = true;
                    if (boolIntervention == true)
                    {
                        SQLIntervention = "";
                        SSTab1.Tabs[4].Visible = true;
                        SSTab1.SelectedTab = SSTab1.Tabs[4];
                    }
                    else
                    {
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                    }
                }


                ///'on error Resume Next
                //cmdSupprimer.SetFocus
                SablierOnOff1(false);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSupprimer_Click");
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSyntheseMAJ_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSyntheseMAJ_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                ModuleAPI.Ouvrir(General.cRAPPORT);
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSyntheseMAJ_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdValider_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdValider_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string sSQL_2 = null;
                DataTable rs_2 = new DataTable();
                ModAdo rs_2Ado = new ModAdo();

                int lnumfichestandard = 0;
                SqlConnection adotrans = default(SqlConnection);

                //----si le mode intervention est activé et qu'aucune insertion
                //----n'est effectué(empêche la validation)
                if (boolinsertInterv == false && boolIntervention == true)//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Avant de valider cette facture, vous devez insérer des interventions", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    cmdValider.Focus();
                    return;
                }

                //----si il manque un codeImmeuble.
                if (string.IsNullOrEmpty(txtCodeimmeuble.Text))
                {
                    if (boolFermeture == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un code immeuble", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff1(false);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        txtCodeimmeuble.Focus();
                        ///'on error Resume Next
                        cmdValider.Focus();
                        return;
                    }
                    else
                    {
                        //===> Mondir le 11.01.2021, https://groupe-dt.mantishub.io/view.php?id=2154
                        //===> Commented lines are old
                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, la facture sera supprimée", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                        //===> new :
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, le code immeuble est obligatoire", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //===> Fin Modif Mondir
                        return;
                    }
                }
                //----Si il manque une date de facture.
                if (string.IsNullOrEmpty(txtDateDeFacture.Text))
                {
                    if (boolFermeture == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer une date de facture", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        txtDateDeFacture.Focus();
                        SablierOnOff1(false);
                        ///'on error Resume Next
                        cmdValider.Focus();
                        return;
                    }
                    else
                    {
                        //===> Mondir le 11.01.2021, https://groupe-dt.mantishub.io/view.php?id=2154
                        //===> Commented lines are old
                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, la facture sera supprimée", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                        //===> new :
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, la date de facture est obligatoire", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //===> Fin Modif Mondir
                        return;
                    }
                }

                if (txtNCompte.Text == "")
                {
                    if (boolFermeture == false)
                    {
                        Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un compte tiers", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        SablierOnOff1(false);
                        //SSTab1.ActiveTab = SSTab1.Tabs[0];
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        txtNCompte.Focus();
                        cmdValider.Focus();
                        return;
                    }
                    else
                    {
                        //===> Mondir le 11.01.2021, https://groupe-dt.mantishub.io/view.php?id=2154
                        //===> Commented lines are old
                        //Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, la facture sera supprimée", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        //cmdAnnuler_Click(cmdAnnuler, null);
                        //===> new :
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, le numéro de compte est obligatoire", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //===> Fin Modif Mondir
                        return;
                    }
                }

                //--- code analytique obligatoire.
                //    If txtINT_AnaCode = "" Then
                //        SSTab1.Tab = 0
                //        SablierOnOff1 False
                //        MsgBox "Le code analytique est obligatoire", vbCritical, "Données manquantes"
                //        Exit Sub
                //    End If
                //----si il manque un mode de réglement.
                if (string.IsNullOrEmpty(txtMode.Text))
                {
                    if (boolFermeture == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un mode de réglement", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        txtMode.Focus();
                        SablierOnOff1(false);
                        ///'on error Resume Next
                        cmdValider.Focus();
                        return;
                    }
                    else
                    {
                        //===> Mondir le 11.01.2021, https://groupe-dt.mantishub.io/view.php?id=2154
                        //===> Commented lines are old
                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, la facture sera supprimée", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                        //===> new :
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des données sont manquantes, le mode de réglement est obligatoire", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        //===> Fin Modif Mondir
                        return;
                    }
                }
                //--- controle si le numero d'intervention éxiste.
                if (!string.IsNullOrEmpty(txtNoIntervention.Text) && txtNoIntervention.Text != "0")
                {
                    if (fc_FindIntervention(Convert.ToInt32(txtNoIntervention.Text)) == false)
                    {
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce numéro d'intervention est inexistant.", "Données manquantes");
                        SablierOnOff1(false);
                        return;
                    }
                }

                if (string.IsNullOrEmpty(txtImmAdresse1.Text) && string.IsNullOrEmpty(txtImmAdresse2.Text) && string.IsNullOrEmpty(txtimmAdresse3.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse du lieu d'intervention est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtImmAdresse1.Focus();
                    SablierOnOff1(false);
                    return;
                }

                if (string.IsNullOrEmpty(txtImmCP.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code postal du lieu d'intervention est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtImmCP.Focus();
                    SablierOnOff1(false);
                    return;
                }

                if (string.IsNullOrEmpty(txtImmVille.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La ville du lieu d'intervention est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtImmVille.Focus();
                    SablierOnOff1(false);
                    return;
                }

                if (string.IsNullOrEmpty(txtGerNom.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse du gérant est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtGerNom.Focus();
                    SablierOnOff1(false);
                    return;
                }

                if (string.IsNullOrEmpty(txtGerAdresse1.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse du gérant ligne 1 est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtGerAdresse1.Focus();
                    SablierOnOff1(false);
                    return;
                }

                //if (string.IsNullOrEmpty(txtGerNom.Text) && string.IsNullOrEmpty(txtGerAdresse1.Text) && string.IsNullOrEmpty(txtGerAdresse2.Text))
                //{
                //    SSTab1.SelectedTab = SSTab1.Tabs[0];
                //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'adresse du gérant est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //    txtGerNom.Focus();
                //    SablierOnOff1(false);
                //    return;
                //}

                if (string.IsNullOrEmpty(txtGerCP.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code postal du gérant d'intervention est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtGerCP.Focus();
                    SablierOnOff1(false);
                    return;
                }

                if (string.IsNullOrEmpty(txtGerVille.Text))
                {
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La ville du gérant est obligatoire.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtGerVille.Focus();
                    SablierOnOff1(false);
                    return;
                }
                if (fc_CtrlService() == false)//tested
                {
                    return;
                }

                //----Si une erreur a été détécté dans le corps de facture
                if (boolErrorCorps == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("une erreur est survenue dans le corps de facture, la validation est annulée", "validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    ///'on error Resume Next
                    cmdValider.Focus();
                    return;
                }
                //----Debut de la validation.
                SablierOnOff1(true);

                //----Validation de l'entete de facture.
                ValidationEnTete();//tested

                //----Validation du corps de facture en mode transaction.


                //=== modif du 29 nov 2007

                //  adocnn.BeginTrans

                //  adocnn.Execute "BEGIN TRANSACTION Trans1" & Replace(fncUserName, " ", "")


                /* TODO à verifier ouvrir la connexion
                 * string chaineConnexion = General.adocnn.ConnectionString;

                 cnnTrans = new SqlConnection(chaineConnexion);
                 cnnTrans.Open();
                 cnnTrans.BeginTransaction();*/
                if (General.adocnn.State == ConnectionState.Closed)
                    General.adocnn.Open();

                SqlCommand cmdB = new SqlCommand("BEGIN TRANSACTION Trans1", General.adocnn);
                cmdB.ExecuteNonQuery();
                //On Error GoTo 0
                ValidationCorps();//tested
                ///'on error GoTo 0
                ///'on error GoTo err

                // cnnTrans.CommitTrans();todo
                // cnnTrans.Close();
                SqlCommand cmdC = new SqlCommand("COMMIT TRANSACTION Trans1", General.adocnn);
                cmdC.ExecuteNonQuery();
                ///adocnn.Execute "COMMIT TRANSACTION Trans1" & Replace(fncUserName, " ", "")

                ///'on error GoTo 0
                ///'on error GoTo Erreur
                //----Validation du pied de facture.
                cmdActualiser_Click(cmdActualiser, new System.EventArgs());

                if (optDevis.Checked == true)
                {
                    fc_ValidDevisSituation();
                }

                boolAddnew = false;
                //----Si c'est une facturation d'intervention Insertion du numero de facture dans la table
                //----intervention et mise du code état à 'S'.
                //----Si l'intervention découle d'un devis, mise du code etat à T de toutes
                //----les interventions issues du devis
                object sOldCodeEtat = null;

                //===> Mondir 03.08.2020, Ajout du LOG
                Program.SaveException(null, $"cmdValider_Click - boolIntervention = {boolIntervention}, txtEnCours.Text = {txtEnCours.Text} ");
                //===> Fin Modif Mondir

                if (boolIntervention == true)
                {
                    //===> Mondir 03.08.2020, Ajout du LOG
                    Program.SaveException(null, $"cmdValider_Click - boolIntervention = {boolIntervention} ");
                    //===> Fin Modif Mondir
                    if (!string.IsNullOrEmpty(SQLIntervention) && General.Left(txtEnCours.Text, 2) == "XX")
                    {
                        var tmpAdo = new ModAdo();

                        sOldCodeEtat = tmpAdo.fc_ADOlibelle("SELECT CodeEtat FROM  Intervention " + SQLIntervention);
                        if (General.sEvolution == "1")
                        {
                            sSQL_2 = "UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "' " + SQLIntervention;
                            var up = General.Execute(sSQL_2);

                            //====> Mondir le 30.06.2020, save qury to log
                            //===> Mondir 03.08.2020, Ajout du LOG
                            Program.SaveException(null, $"cmdValider_Click - if boolIntervention > if > if ");
                            Program.SaveException(null, $"cmdValider_Click - Update Intervention = {sSQL_2}");
                            Program.SaveException(null, $"cmdValider_Click - Intervention Updated = {up}");
                            //====> Fin Modif Mondir

                            sSQL_2 = "SELECT  NoIntervention, CodeEtat  "
                                        + " From Intervention WHERE NoFacture = '" + txtEnCours.Text + "'";
                            rs_2 = rs_2Ado.fc_OpenRecordSet(sSQL_2);

                            //===> Mondir 03.08.2020, Ajout du LOG
                            Program.SaveException(null, $"cmdValider_Click - Select Intervention = {sSQL_2}");
                            Program.SaveException(null, $"cmdValider_Click - Intervention Selected = {rs_2?.Rows.Count}");
                            //====> Fin Modif Mondir

                            if (rs_2.Rows.Count > 0)
                            {
                                foreach (DataRow r2 in rs_2.Rows)
                                {
                                    General.fc_HistoStatut(Convert.ToInt32(General.nz(r2["NoIntervention"], 0)), txtCodeimmeuble.Text, "Fiche facture", r2["CodeEtat"] + "" + "", "S");
                                    sSQL_2 = "UPDATE Intervention SET Intervention.CodeEtat ='S' WHERE NoIntervention =" + General.nz(r2["NoIntervention"], 0);
                                    up = General.Execute(sSQL_2);

                                    //===> Mondir 03.08.2020, Ajout du LOG
                                    Program.SaveException(null, $"cmdValider_Click - if boolIntervention > if > if > if > foreach - Update Intervention = {r2["NoIntervention"]}");
                                    Program.SaveException(null, $"cmdValider_Click - if boolIntervention > if > if > if > foreach - Update Query = {sSQL_2}");
                                    //====> Fin Modif Mondir
                                }

                            }
                            rs_2.Dispose();
                            rs_2 = null;
                            //Modif 17-07-2020 ajouté par salma
                            SQLIntervention = "";
                            //Fin modif
                        }
                        else
                        {
                            if (optAf.Checked == true)
                            {

                                General.Execute("UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'S' " + SQLIntervention);


                                //====> Mondir le 30.06.2020, save qury to log
                                Program.SaveException(null, "UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'S' " + SQLIntervention);
                                //====> Fin Modif Mondir

                                General.fc_HistoStatut(Convert.ToInt32(txtNoIntervention.Text), txtCodeimmeuble.Text, "Fiche facture", sOldCodeEtat + "", "S");

                                MAJCodeEtatDevis();

                                SSTab1.Tabs[4].Visible = false;
                                SQLIntervention = "";

                            }
                            else if (optDevis.Checked == true)
                            {

                                if (chkSolderDevis.CheckState == CheckState.Checked)
                                {
                                    //== modif rachid du 10 janvier 2005, mettre toutes les interventions de la fiche d'appel à T.
                                    lnumfichestandard = Convert.ToInt32(tmpAdo.fc_ADOlibelle("SELECT NumFicheStandard FROM INTERVENTION " + SQLIntervention));

                                    General.Execute("UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'T' " + SQLIntervention);

                                    General.Execute("UPDATE Intervention SET  CodeEtat = 'T' WHERE numFicheStandard =" + lnumfichestandard);


                                    General.fc_HistoStatut(Convert.ToInt32(txtNoIntervention.Text), txtCodeimmeuble.Text, "Fiche facture", sOldCodeEtat + "", "T");

                                    //====> Mondir le 30.06.2020, save qury to log
                                    Program.SaveException(null, "UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'T' " + SQLIntervention);
                                    Program.SaveException(null, "UPDATE Intervention SET  CodeEtat = 'T' WHERE numFicheStandard =" + lnumfichestandard);
                                    //====> Fin Modif Mondir
                                }
                                else
                                {
                                    General.Execute("UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'TS' " + SQLIntervention);


                                    General.fc_HistoStatut(Convert.ToInt32(txtNoIntervention.Text), txtCodeimmeuble.Text, "Fiche facture", sOldCodeEtat + "", "TS");

                                    //====> Mondir le 30.06.2020, save qury to log
                                    Program.SaveException(null, "UPDATE Intervention SET Intervention.NoFacture ='" + txtEnCours.Text + "', CodeEtat = 'TS' " + SQLIntervention);
                                    //====> Fin Modif Mondir
                                }
                                MAJCodeEtatDevis();
                                //                    Set adotemp = New ADODB.Recordset
                                //                    With adotemp
                                //                    SQL = "SELECT Intervention.NoIntervention,Intervention.NoDevis FROM Intervention " & SQLIntervention
                                //                        .Open SQL, adocnn
                                //                        If Not .EOF And Not .BOF Then
                                //                            .MoveFirst
                                //                            If Not IsNull(!NoDevis) And Not (!NoDevis = "") Then
                                //                                adocnn.Execute "UPDATE Intervention SET Intervention.CodeEtat ='TS' WHERE " _
                                //'                                & "(Intervention.NumFicheStandard IN(SELECT GestionStandard.NumFicheStandard " _
                                //'                                & " FROM GestionStandard WHERE GestionStandard.NoDevis='" & !NoDevis & "'))"
                                //                            End If
                                //                        End If
                                //                        .Close
                                //                    End With
                                //                    Set adotemp = Nothing
                                SSTab1.Tabs[4].Visible = false;
                                SQLIntervention = "";

                            }

                        }

                    }
                    else
                    {
                        //===> Mondir 03.08.2020, Ajout du LOG
                        Program.SaveException(null, $"cmdValider_Click - !string.IsNullOrEmpty(SQLIntervention) && General.Left(txtEnCours.Text, 2) == \"XX\" ===> to else !!! ");
                        //===> Fin Modif Mondir
                    }
                }

                //----------------------Pocédure GESTEN--------------------------
                //---------------------------------------------------------------
                //--- on cloture la facture.
                //    If txtNoIntervention <> "" And txtNoIntervention <> "0" Then
                //        sSQl = "SELECT NoIntervention, CodeEtat, INT_DateProduit, INT_TypeIntervention" _
                //'            & " FROM Intervention WHERE NoIntervention =" & txtNoIntervention
                //        Set rstmp = fc_OpenRecordSet(sSQl)
                //        If Not rstmp.EOF Then
                //                ' si intervention regie ou produit on cloture.
                //                If rstmp!INT_TypeIntervention = 1 Or rstmp!INT_TypeIntervention = 5 Then
                //                    rstmp!CodeEtat = "C"
                //                    'Modif xavier du 29/08/02
                //                    sub_LockBondecommande rstmp("NoIntervention")
                //                End If
                //                ' si devis.
                //                If rstmp!INT_TypeIntervention = 2 Then
                //                    rstmp!CodeEtat = "F"
                //                End If
                //                rstmp.Update
                //        Else
                //            MsgBox "Intervention introuvable", vbCritical, ""
                //        End If
                //        rstmp.Close
                //        'adocnn.Execute sSQl
                //    End If
                //---------------------------------------------------------------
                //----------------------Fin Pocédure GESTEN----------------------


                //----Bloque les boutons inutiles.
                cmdSupprimer.Enabled = true;
                cmdAnnuler.Enabled = false;
                cmdValider.Enabled = true;
                cmdAjouter.Enabled = true;
                CmdRechercher.Enabled = true;
                SablierOnOff1(false);

                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdValider_Click_Transac");
                SqlCommand cmdR = new SqlCommand("ROLLBACK TRANSACTION Trans1", General.adocnn);
                cmdR.ExecuteNonQuery();
            }
        }
        private void fc_ValidDevisSituation()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ValidDevisSituation() - Enter In The Function");
            //===> Fin Modif Mondir

            object functionReturnValue = null;
            int i = 0;
            DataTable rsDevis = default(DataTable);
            string strSelect = null;
            string[] tabNoDevis = null;
            string[] tabMontantDev = null;

            ModAdo rsDevisModAdo = new ModAdo();

            if (string.IsNullOrEmpty(SQLIntervention))
                return;

            tabNoDevis = new string[1];
            tabMontantDev = new string[1];

            rsDevis = new DataTable();
            strSelect = "SELECT DevisDetail.NumeroDevis,SUM(DevisDetail.TotalVenteApresCoef) AS TotalDev ";
            strSelect = strSelect + " FROM DevisDetail ";
            strSelect = strSelect + " INNER JOIN GestionStandard ON DevisDetail.NumeroDevis=GestionStandard.NoDevis ";
            strSelect = strSelect + " INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard ";
            strSelect = strSelect + SQLIntervention;
            strSelect = strSelect + " GROUP BY DevisDetail.NumeroDevis,Intervention.NoIntervention ";

            rsDevis = rsDevisModAdo.fc_OpenRecordSet(strSelect);
            if (rsDevis.Rows.Count > 0)
            {
                //  rsDevis.MoveFirst();
                i = 0;
                tabNoDevis = new string[rsDevis.Rows.Count];
                tabMontantDev = new string[rsDevis.Rows.Count];
                foreach (DataRow rsRow in rsDevis.Rows)
                {
                    //if (tabNoDevis.Length < i)
                    //{
                    //    Array.Resize(ref tabNoDevis, i + 1);
                    //}
                    //if (tabMontantDev.Length < i)
                    //{
                    //    Array.Resize(ref tabMontantDev, i + 1);
                    //}
                    tabNoDevis[i] = rsRow["numerodevis"] + "";
                    tabMontantDev[i] = rsRow["TotalDev"] + "";
                    i = i + 1;
                    //rsDevis.MoveNext();
                }
            }

            rsDevis.Dispose();

            rsDevis = null;

            for (i = 0; i <= tabNoDevis.Length - 1; i++)
            {
                strSelect = "";
                strSelect = "SELECT";
            }
            return;

        }

        private bool fc_FindIntervention(int lNoIntervention)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_FindIntervention() - Enter In The Function - lNoIntervention = {lNoIntervention}");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            string sSQL = null;
            sSQL = "select nointervention from intervention ";
            sSQL = sSQL + " where nointervention=" + lNoIntervention;

            ModAdo tmpAdo = new ModAdo();

            General.rstmp = tmpAdo.fc_OpenRecordSet(sSQL);

            if (General.rstmp.Rows.Count > 0)
            {
                functionReturnValue = true;
            }
            else
            {
                functionReturnValue = false;
            }
            tmpAdo.Dispose();
            return functionReturnValue;

        }
        private void MAJCodeEtatDevis()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"MAJCodeEtatDevis() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                //== Mise a jours du codeEtat dans la table devisEnTete si cette intervention
                //== est liee a un devis (Champs NoDevis alimenté).
                adotemp = new DataTable();
                var _with20 = adotemp;
                SQL = "SELECT GestionStandard.NoDevis FROM GestionStandard ";
                SQL = SQL + " INNER JOIN Intervention ON ";
                SQL = SQL + " GestionStandard.NumFicheStandard=Intervention.NumFicheStandard ";
                SQL = SQL + SQLIntervention;
                ModAdo tmpAdo = new ModAdo();

                _with20 = tmpAdo.fc_OpenRecordSet(SQL);
                if (_with20.Rows.Count > 0)
                {
                    foreach (DataRow rsRow in _with20.Rows)
                    {
                        if (!string.IsNullOrEmpty(rsRow["NoDevis"].ToString()))
                        {
                            if (chkSolderDevis.CheckState == CheckState.Checked)
                            {
                                General.Execute("UPDATE DevisEnTete SET CodeEtat ='T',Cloture='0' WHERE "
                                    + " NumeroDevis IN(SELECT GestionStandard.NoDevis FROM "
                                    + " GestionStandard WHERE GestionStandard.NoDevis='" + rsRow["NoDevis"] + "')");
                            }
                            else
                            {
                                General.Execute("UPDATE DevisEnTete SET CodeEtat ='TS',Cloture='0' WHERE "
                                    + " NumeroDevis IN(SELECT GestionStandard.NoDevis FROM "
                                    + " GestionStandard WHERE GestionStandard.NoDevis='" + rsRow["NoDevis"] + "')");
                            }
                            // _with20.MoveNext();
                        }
                    }
                }
                _with20.Dispose();

                adotemp = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";MAJCodeEtatDevis");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void cmdValidOpt_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdValidOpt_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                boolIntervention = false;
                SSTab1.Tabs[4].Visible = false;
                txtINT_AnaCode.ReadOnly = false;
                txtINT_AnaCode.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cNoir);
                txtNoIntervention.ReadOnly = false;
                txtNoIntervention.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cNoir);
                chkSolderDevis.Visible = false;

                PicDevis.Visible = false;

                if (OptConMan.Checked == true)//tested
                {
                    TypeFacture = "CM";
                    Frame1.Text = "CONTRAT MANUEL";
                    lblDebut.Text = "CM";
                    lblFin.Text = "à CM";
                    frame6.Text = "Visualisation et impression des factures mises à jour Contractuelles";
                    optGerant.Enabled = true;
                    optParticulier.Enabled = true;

                }
                else if (OptFioulMan.Checked == true)
                {
                    TypeFacture = "FM";
                    Frame1.Text = "FIOUL MANUEL";
                    lblDebut.Text = "FM";
                    lblFin.Text = "à FM";
                    frame6.Text = "Visualisation et impression des factures mises à jour FIOUL";
                    optGerant.Enabled = true;
                    optParticulier.Enabled = true;

                }
                else if (OptTravMan.Checked == true)
                {
                    TypeFacture = "TM";
                    Frame1.Text = "TRAVAUX MANUELS";
                    lblDebut.Text = "TM";
                    lblFin.Text = "à TM";
                    frame6.Text = "Visualisation et impression des factures mises à jour MANUELLES";
                    optGerant.Enabled = true;
                    optParticulier.Enabled = true;
                }
                else if (optIntervention.Checked || optDevis.Checked == true)//tested
                {
                    if (optDevis.Checked == true)
                    {
                        chkSolderDevis.Visible = true;
                        Label242.Text = "Total des situations";
                        Label243.Text = "Montant total du devis";
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        lblhtDevis.Visible = true;
                        lbl6.Visible = true;
                        //===> Fin Modif Mondir
                    }
                    else//tested
                    {
                        Label242.Text = "Coefficient réalisé";
                        Label243.Text = "Montant de la facture";
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        lblhtDevis.Visible = false;
                        lbl6.Visible = false;
                        //===> Fin Modif Mondir
                    }
                    txtINT_AnaCode.ReadOnly = true;
                    txtINT_AnaCode.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                    txtNoIntervention.ReadOnly = true;
                    txtNoIntervention.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                    TypeFacture = "TA";
                    Frame1.Text = "INTERVENTION";
                    lblDebut.Text = "TA";
                    lblFin.Text = "à TA";
                    SSTab1.Tabs[4].Visible = true;
                    SSTab1.SelectedTab = SSTab1.Tabs[4];
                    //----mode intervention.
                    boolIntervention = true;
                    optGerant.Enabled = true;
                    optParticulier.Enabled = true;

                    if (General.sCritreDevisFac == "1" && optDevis.Checked)
                    {
                        PicDevis.Visible = true;
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez choisir un type de facture.", "Type de facture manquant", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                SSTab1.Enabled = true;
                /// Ce code ne marche pas
                ///===================> Mondir le 15.06.2020 : If we use the visible false, so we cant use the .checked anymore, it will be always false !
                ///===================> So the better is to hide it
                //Frame9.Location = new Point(Frame9.Location.X, -50);
                // ===== >Salma le 17.06.2020==========>j'ai remplacé la visbilité par la taille  pour la frame9 et le boutton cmdValidOpt
                Frame9.Size = new Size(0, 0);
                cmdAjouter.Visible = true;
                cmdSupprimer.Visible = true;
                cmdValider.Visible = true;
                cmdAnnuler.Visible = true;
                CmdRechercher.Visible = true;
                //////================> Mondir le 16.06.2020 : After setting cmdValidOpt.Visible = false also optDevis is moved to check false, even when we check it,
                ///// so i need to save its value and check if changed to put it again
                //var tmpBool = optDevis.Checked;
                //cmdValidOpt.Visible = false;
                //if (tmpBool != optDevis.Checked)
                //    optDevis.Checked = tmpBool;
                /////=====> Fin Modif Mondir

                // ======> Salma le 17.06.2020 modif
                //cmdValidOpt.Visible = false;
                cmdValidOpt.Size = new Size(0, 0);
                cmdMenu.Visible = true;
                FirstRemplitGrille();
                SecondRemplitGrille();
                DataTable dt1 = new DataTable();
                RemplitGrille(dt1);
                fc_ChargeContrat("0");

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdValidOpt_Click");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisu_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVisu_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            int lVersionDate;
            string sFileReport = "";
            int lCleTemp;
            //===> Fin Modif Mondir

            try
            {
                ReportDocument CR = new ReportDocument();
                ReportDocument CR2 = new ReportDocument();
                string SelectionFormula = "";
                strMiseAJour = "";

                if (CR.FileName == "")
                {
                    if (File.Exists(General.ETATOLDFACTUREMANUEL))
                        CR.Load(General.ETATOLDFACTUREMANUEL);
                }
                if (CR2.FileName == "")
                {
                    if (File.Exists(General.ETATFacturemanuelleSQLv2))
                        CR2.Load(General.ETATFacturemanuelleSQLv2);

                }
                if (optPersonne.Checked == true)
                {

                    strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + strNomPersonne + "' and ";
                }
                else if (OptNomChoisis.Checked == true)
                {

                    strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + Combo1.Text + "' and ";
                }
                else if (OptTous.Checked == true)
                {

                    //---Ne fait rien.
                }


                if (Option1.Checked == true)
                {
                    if (string.IsNullOrEmpty(txtVisu1.Text) || !(General.IsNumeric(txtVisu1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisu1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtVisu2.Text) || !(General.IsNumeric(txtVisu2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisu2.Focus();
                        return;
                    }
                    //--- Le numero de facture doit etre identique
                    //If txtVisu1 <> txtVisu2 Then
                    //    MsgBox "Vous ne pouvez saisir qu'une seule facture à la fois.", vbCritical, "Données eronnées"
                    //    SablierOnOff False
                    //    txtVisu2.SetFocus
                    //    Exit Sub
                    //End If
                    // If RechDateOnNoFac(txtVisu1) = 0 Then
                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.NumFacture} >=" + txtVisu1.Text + " and {FactureManuelleEntete.NumFacture} <=" + txtVisu2.Text + "  AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true";
                    //            If chk3bacsVisu.value = 1 Then
                    //                fc_ImprimeBacs ETATOLDFACTUREMANUEL, strMiseAJour
                    //            End If

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    using (var tmpModAdo = new ModAdo())
                    {
                        lCleTemp = General.nz(tmpModAdo.fc_ADOlibelle("SELECT TOP (1) CleFactureManuelle From FactureManuelleEnTete "
                                                                      + "   WHERE NoFacture ='" + lblDebut.Text + txtVisu1.Text + "'"), 0).ToInt();
                        lVersionDate = RechDate(lCleTemp);
                    }

                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    else if (lVersionDate == 1) //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 RechDateOnNoFac() remplacé par lVersionDate == 1
                    {
                        SelectionFormula = strMiseAJour;
                        // CR2.WindowShowPrintSetupBtn = true;
                        //  CR2.Action = 1;
                        //Todo CR2==> ce rapport n'existe pas 
                        //CrystalReportFormView form = new CrystalReportFormView(CR2, SelectionFormula);
                        //form.ShowDialog();
                        //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date du facture " + lblDebut.Text + " " + txtVisu1.Text + " est supérieur à la date du FactureManuelleV2 enregistré ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //===> Mondir le 22.01.2021, https://groupe-dt.mantishub.io/view.php?id=2225 , Salma na pas trouver le rapport V2j j'utilise le rapport V1
                        CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                        form.Show();
                        Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                        SelectionFormula = "";
                        //===> Fin Modif Mondur

                        return;
                    }
                    else
                    {
                        SelectionFormula = strMiseAJour;
                        //CR.WindowShowPrintSetupBtn = true;
                        //CR.Action = 1;
                        CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                        form.Show();
                        Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                        SelectionFormula = "";
                    }
                    //Else
                    //   strMiseAJour = strMiseAJour & "{FactureManuelleEntete.NumFacture} >=" & txtVisu1 & " and {FactureManuelleEntete.NumFacture} <=" & txtVisu2 & "  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //   CR2.SelectionFormula = strMiseAJour
                    //   CR2.WindowShowPrintSetupBtn = True
                    //   CR2.Action = 1
                    //   CR2.SelectionFormula = ""
                    //End If
                    //rIf txtDateDeFacture < CDate(dbEURO) Then
                    //r    strMiseAJour = "{FactureManuelleEntete.NumFacture} >=" & txtVisu1 & " and {FactureManuelleEntete.NumFacture} <=" & txtVisu2 & "  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //rElse
                    //r    strMiseAJour = "{FactureManuelleEntete.NumFacture} >=" & txtVisu1 & " and {FactureManuelleEntete.NumFacture} <=" & txtVisu2 & "  AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = 1 and {FactureManuelleEntete.Imprimer} = 1"
                    //rEnd If


                    //Exit Sub
                }
                else if (Option4.Checked == true)//tested
                {
                    if (string.IsNullOrEmpty(txtVisuDate1.Text) || !(General.IsDate(txtVisuDate1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisuDate1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtVisuDate2.Text) || !(General.IsDate(txtVisuDate2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisuDate2.Focus();
                        return;
                    }

                    dbdate1 = DateTime.Parse(txtVisuDate1.Text).ToString("yyyy,MM,dd");

                    dbdate2 = Convert.ToDateTime(txtVisuDate2.Text).ToString("yyyy,MM,dd");


                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.datefacture}>= Date (" + dbdate1 + ") and {FactureManuelleEntete.dateFacture}<= Date (" + dbdate2 + ")  AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true";


                    //            If chk3bacsVisu.value = 1 Then
                    //                fc_ImprimeBacs ETATOLDFACTUREMANUEL, strMiseAJour
                    //            End If

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    //===> Mondir le 25.01.2021, ajout de la codition !General.gfr_liaison("FacturemanuelleSQLv3", "").IsNullOrEmpty() pour corriger https://groupe-dt.mantishub.io/view.php?id=2233
                    if (txtVisuDate1.Text.IsDate() && General.dtdateFactManuV3 >= txtVisuDate1.Text.ToDate() && !General.gfr_liaison("FacturemanuelleSQLv3", "").IsNullOrEmpty())
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    else if (General.IsDate(General.sDateFactureManuelleV2))
                    {
                        if (Convert.ToDateTime(txtVisuDate1.Text) >= Convert.ToDateTime(General.sDateFactureManuelleV2))
                        {
                            // CR2.WindowShowPrintSetupBtn = true;
                            SelectionFormula = strMiseAJour;
                            // Todo CR2 ce rapport n'existe pas 
                            //CrystalReportFormView form = new CrystalReportFormView(CR2, SelectionFormula);
                            //form.ShowDialog();

                            CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                            form.Show();
                            Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                            SelectionFormula = "";

                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date du facture " + txtVisuDate1.Text + " est supérieur à la date du FactureManuelleV2 enregistré", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            // CR2.Action = 1;
                            SelectionFormula = "";
                            return;
                        }
                        else//tested
                        {
                            //   CR.WindowShowPrintSetupBtn = true;
                            SelectionFormula = strMiseAJour;
                            // CR.Action = 1;
                            CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                            form.Show();
                            Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                            SelectionFormula = "";
                        }
                    }
                    else
                    {
                        // CR.WindowShowPrintSetupBtn = true;
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                        form.Show();
                        // CR.Action = 1;
                        SelectionFormula = "";
                    }
                    //ElseIf txtVisuDate1 < dbEURO Then
                    // dbdate2 = #6/15/2001#

                    //    strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture}>= Date (" & dbdate1 & ") and {FactureManuelleEntete.dateFacture}< Date (" & dbEuroCr & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true"
                    //    CR.WindowShowPrintSetupBtn = True
                    //    CR.SelectionFormula = strMiseAJour
                    //    CR.Action = 1
                    //    CR.SelectionFormula = ""
                    //CR.Reset


                    //    strMiseAJour = strMiseAJour & "{FactureManuelleEntete.datefacture}>= Date (" & dbEuroCr & ") and {FactureManuelleEntete.dateFacture}<= Date (" & dbdate2 & ") AND {FactureManuelleEntete.TypeFacture} ='" & TypeFacture & "' and {FactureManuelleEntete.Facture} = 1 and {FactureManuelleEntete.Imprimer} = 1"
                    //    CR2.WindowShowPrintSetupBtn = True
                    //    CR2.SelectionFormula = strMiseAJour
                    //    CR2.Action = 1
                    //    CR2.SelectionFormula = ""
                    //    Exit Sub
                    //End If

                    //CR.WindowShowPrintSetupBtn = True
                    //CR.SelectionFormula = strMiseAJour
                    //CR.Action = 1
                    //CR.SelectionFormula = ""
                }
                else if (OptFactEnCours.Checked)
                {
                    strMiseAJour = strMiseAJour + "{FactureManuelleEntete.nofacture} ='" + txtEnCours.Text + "' AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    lVersionDate = RechDateOnNoFac(General.Mid(txtEnCours.Text, 3, General.Len(txtEnCours.Text) - 2).ToInt()).ToInt();
                    if (lVersionDate == 2)
                    {
                        ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                        ModCrystalPDF.tabFormulas[0].sNom = "";
                        ModCrystalPDF.tabFormulas[0].sType = "";
                        ModCrystalPDF.tabFormulas[0].sCritere = "";
                        sFileReport = devModeCrystal.fc_ExportCrystal(strMiseAJour, General.ETATFacturemanuelleSQLv3,
                            intNoFieldSelection: CheckState.Checked);
                        Process.Start(sFileReport);
                    }
                    //===> Fin Modif Mondir
                    else if (lVersionDate == 1) // ===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> old : (RechDateOnNoFac(Convert.ToInt32(General.Mid(txtEnCours.Text, 3, (txtEnCours.Text.Length) - 2))) == 1)
                    {
                        SelectionFormula = strMiseAJour;
                        //Todo CR2==> ce rapport n'existe pas 
                        //CrystalReportFormView form = new CrystalReportFormView(CR2, SelectionFormula);
                        //form.Show();

                        //===> Mondir le 22.01.2021, https://groupe-dt.mantishub.io/view.php?id=2225 , Salma na pas trouver le rapport V2j j'utilise le rapport V1
                        CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                        form.Show();
                        Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                        SelectionFormula = "";
                        //===> Fin Modif Mondur

                    }
                    else
                    {
                        SelectionFormula = strMiseAJour;
                        CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                        form.Show();
                        Program.SaveException(null, "Report Clause Where ===>" + SelectionFormula);
                        SelectionFormula = "";
                    }
                }
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdVisu_Click");
                if (ex.InnerException != null)
                    Program.SaveException(null, "error in crystal report function CmdVisu_Click ===> details : " + ex.InnerException.Message);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Command1_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                adotemp = new DataTable();
                var tmpAdo = new ModAdo();
                var _with21 = adotemp;

                _with21 = tmpAdo.fc_OpenRecordSet("SELECT Count(FactureManuelleEntete.Facture) AS CompteDeFacture,"
                    + " FactureManuelleEntete.NomPersonne, FactureManuelleEntete.TypeFacture" + " From FactureManuelleEntete"
                    + " GROUP BY FactureManuelleEntete.NomPersonne, FactureManuelleEntete.Facture," + " FactureManuelleEntete.TypeFacture"
                    + " Having FactureManuelleEntete.NomPersonne <> '' And" + " FactureManuelleEntete.Facture = 0"
                    + " ORDER BY FactureManuelleEntete.NomPersonne");


                // supprimer le fichier si il exsite  
                if (File.Exists(General.cFACTURENONMISEAJOUR))
                    File.Delete(General.cFACTURENONMISEAJOUR);

                if (_with21.Rows.Count > 0)
                {
                    //----En tete deu fichier des erreurs
                    File.AppendAllText(General.cFACTURENONMISEAJOUR, "Fichier des factures non mise à jours -" + DateTime.Now + "\r\n");
                    File.AppendAllText(General.cFACTURENONMISEAJOUR, "\r\n");
                    File.AppendAllText(General.cFACTURENONMISEAJOUR, "\r\n");
                    //  FileSystem.PrintLine(2, "Fichier des factures non mise à jours -" + DateAndTime.Now);

                    // FileSystem.PrintLine(2, Constants.vbCrLf);
                    foreach (DataRow rs in _with21.Rows)
                    {
                        SQL = "";
                        switch (rs["TypeFacture"].ToString())
                        {
                            case "TM":
                                SQL = "Travaux manuels";
                                break;
                            case "CM":
                                SQL = "Contrat manuels";
                                break;
                            case "FM":
                                SQL = "Fioul manuels";
                                break;
                            case "TA":
                                SQL = "Interventions";
                                break;
                        }
                        File.AppendAllText(General.cFACTURENONMISEAJOUR, "Utilisateur : " + rs["Nompersonne"].ToString() + "" + " -> Type de facture: " + SQL + " -> Total: " + rs["CompteDeFacture"].ToString() + "" + "\r\n");
                        File.AppendAllText(General.cFACTURENONMISEAJOUR, "\r\n");
                        //  FileSystem.PrintLine(2, "Utilisateur : " + rs["Nompersonne"].ToString() + "" + " -> Type de facture: " + SQL + " -> Total: " + rs["CompteDeFacture"].ToString() + "");
                        // FileSystem.PrintLine(2, "");
                        //_with21.MoveNext();
                    }
                    _with21.Dispose();
                }
                // FileSystem.FileClose(2);

                ModuleAPI.Ouvrir(General.cFACTURENONMISEAJOUR);

                //dbdate1 = Shell("c:\program files\accessoires\wordpad.exe " & "C:\FacturesNonMiseAjour", 3)

                adotemp = null;
                return;


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridContrat_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"GridContrat_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;

            var _with22 = GridContrat;
            if (!string.IsNullOrEmpty(e.Row.Cells["DateFin"].Value.ToString()) && General.IsDate(e.Row.Cells["DateFin"].Value.ToString()))
            {
                if (e.Row.Cells["Resiliee"].Value.ToString() == "True" &&
                    !string.IsNullOrEmpty(e.Row.Cells["DateFin"].Value.ToString()) &&
                    Convert.ToDateTime(e.Row.Cells["DateFin"].Value.ToString()) <= DateTime.Today)
                {
                    for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                    {
                        e.Row.Cells[i].Appearance.BackColor = Color.IndianRed;
                        e.Row.Cells[i].Appearance.ForeColor = Color.Blue;


                    }
                }
                else
                {
                    for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                    {
                        e.Row.Cells[i].Appearance.BackColor = ColorTranslator.FromOle(0x80ff80);
                        e.Row.Cells[i].Appearance.ForeColor = ColorTranslator.FromOle(0x0);

                    }
                }
            }
            else
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = ColorTranslator.FromOle(0x80ff80);
                    e.Row.Cells[i].Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }
            }
        }
        private void lblTotalSituations_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"lblTotalSituations_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            string sPhrase = null;
            if (optDevis.Checked == true)
            {
                if (!string.IsNullOrEmpty(tabNoFacture[0]))
                {
                    sPhrase = "Factures enregistrées :";
                    for (i = 0; i <= tabNoFacture.Length; i++)
                    {
                        sPhrase = sPhrase + "\n";

                        sPhrase = sPhrase + "\n" + tabNoFacture[i] + " : " + General.nz(tabMontantFact[i], "0") + " €";
                    }
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sPhrase, "Factures sur situation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else if (optIntervention.Checked)
            {
                sPhrase = "Montant total des charges liées aux interventions sélectionnées :";
                sPhrase = sPhrase + "\n";

                sPhrase = sPhrase + "\n" + (Convert.ToDouble(General.nz(lblTotalAchats.Text, "0")) + Convert.ToDouble(General.nz(txtCharges.Text, "0"))).ToString() + " €";
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sPhrase, "Déboursé des interventions sélectionnées", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptConMan_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"OptConMan_KeyPress() - Enter In The Function");
            //===> Fin Modif 

            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdValidOpt_Click(cmdValidOpt, new System.EventArgs());
                KeyAscii = 0;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optDevis_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optDevis_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir 05.08.2020, Ajout du LOG
            Program.SaveException(null, $"optDevis_CheckedChanged() - optDevis.Checked = {optDevis.Checked}");
            //===> Fin Modif Mondir

            if (optDevis.Checked)
            {
                optDevis1.Checked = true;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optIntervention_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optIntervention_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir 05.08.2020, Ajout du LOG
            Program.SaveException(null, $"optIntervention_CheckedChanged() - optIntervention.Checked = {optIntervention.Checked}");
            //===> Fin Modif Mondir

            if (optIntervention.Checked)
            {
                optAf.Checked = true;
            }
            //optInterventionFlag = optIntervention.Checked;
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptFioulMan_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"OptFioulMan_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdValidOpt_Click(cmdValidOpt, new System.EventArgs());
                KeyAscii = 0;
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optIntervention_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optIntervention_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdValidOpt_Click(cmdValidOpt, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option1_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option1_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option1.Checked)
            {
                txtVisu1.Enabled = true;
                txtVisu2.Enabled = true;
                txtVisuDate1.Enabled = false;
                txtVisuDate1.Text = "";
                txtVisuDate2.Enabled = false;
                txtVisuDate2.Text = "";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option2_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option2_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option2.Checked)
            {
                txtMiseAjour1.Enabled = false;
                txtMiseAjour1.Text = "";
                txtMiseAjour2.Text = "";
                txtMiseAjour2.Enabled = false;
                txtMiseDate1.Enabled = true;
                txtMiseDate2.Enabled = true;
                txtMiseDate1.Focus();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option3_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option3_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option3.Checked)
            {
                txtDebut.Enabled = false;
                txtDebut.Text = "";
                txtFin.Text = "";
                txtFin.Enabled = false;
                txtImpDate1.Enabled = true;
                txtImpDate2.Enabled = true;
                //txtImpDate1.SetFocus
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option4_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option4_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option4.Checked)
            {
                txtVisu1.Enabled = false;
                txtVisu1.Text = "";
                txtVisu2.Text = "";
                txtVisu2.Enabled = false;
                txtVisuDate1.Enabled = true;
                txtVisuDate2.Enabled = true;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option6_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option6_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option6.Checked)
            {
                txtDebut.Enabled = false;
                txtDebut.Text = "";
                txtFin.Text = "";
                txtFin.Enabled = false;
                txtImpDate1.Enabled = false;
                txtImpDate1.Text = "";
                txtImpDate2.Text = "";
                txtImpDate2.Enabled = false;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option5_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option5_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option5.Checked)
            {
                txtDebut.Enabled = true;
                txtFin.Enabled = true;
                txtImpDate1.Enabled = false;
                txtImpDate1.Text = "";
                txtImpDate2.Enabled = false;
                txtImpDate2.Text = "";
                // txtDebut.SetFocus
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option6_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option6_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdImprimer_Click(cmdImprimer, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option7_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option7_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option7.Checked)
            {
                txtMiseAjour1.Enabled = true;
                txtMiseAjour2.Enabled = true;
                txtMiseDate1.Enabled = false;
                txtMiseDate1.Text = "";
                txtMiseDate2.Enabled = false;
                txtMiseDate2.Text = "";
                txtMiseAjour1.Focus();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option8_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"Option8_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (Option8.Checked)
            {
                txtMiseAjour1.Enabled = false;
                txtMiseAjour1.Text = "";
                txtMiseAjour2.Text = "";
                txtMiseAjour2.Enabled = false;
                txtMiseDate1.Enabled = false;
                txtMiseDate2.Enabled = false;
                txtMiseDate1.Text = "";
                txtMiseDate2.Text = "";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptNomChoisis_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"OptNomChoisis_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (OptNomChoisis.Checked)
            {
                Combo1.Enabled = true;
                label21.Enabled = false;
                lblNom.Enabled = false;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptTous_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"OptTous_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (OptTous.Checked)
            {
                Combo1.Enabled = false;
                label21.Enabled = true;
                lblNom.Enabled = false;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optPersonne_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optPersonne_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (optPersonne.Checked)
            {
                Combo1.Enabled = false;
                label21.Enabled = false;
                lblNom.Enabled = true;
            }

        }

        private void OptTravMan_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"OptTravMan_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdValidOpt_Click(cmdValidOpt, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_BeforeExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            var _with23 = SSOleDBGrid1;
            try
            {
                if (SSOleDBGrid1.ActiveRow == null)
                    return;

                activeRow = -1;
                activeCol = -1;

                //insertion d'un article
                if (_with23.ActiveCell.Column.Key.ToUpper() == "Article".ToUpper() && !string.IsNullOrEmpty(_with23.ActiveRow.Cells["Article"].Text))
                {
                    if (_with23.ActiveCell.DataChanged)
                        if (boolRechercheMULTI == false)
                        {
                            boolOnlyArticle = true;

                            strCodeArticle = _with23.ActiveRow.Cells["Article"].Text;
                            activeRow = _with23.ActiveRow.Index;
                            activeCol = _with23.ActiveCell.Column.Index;

                            if (leftOrTab)
                                activeCol++;

                            leftOrTab = false;

                            if (_with23.ActiveRow.IsAddRow)
                            {
                                boolAddrow = true;
                                timer2.Enabled = true;
                                return;
                            }
                            else
                            {
                                _with23.ActiveRow.Cells["Article"].Value = _with23.ActiveRow.Cells["Article"].OriginalValue;
                                timer2.Enabled = true;
                                return;
                            }
                        }
                }

                //champs Quantité
                if (_with23.ActiveCell.Column.Key.ToUpper() == "Quantite".ToUpper())
                {
                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["Quantite"].Text) && !General.IsNumeric(_with23.ActiveRow.Cells["Quantite"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Inserer une valeur numérique", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        return;
                    }

                    if (!(string.IsNullOrEmpty(_with23.ActiveRow.Cells["Quantite"].Text)) && !(string.IsNullOrEmpty(_with23.ActiveRow.Cells["PrixUnitaire"].Text)))
                    {
                        _with23.ActiveRow.Cells["MontantLigne"].Value = General.FncArrondir(Convert.ToDouble(General.nz(_with23.ActiveRow.Cells["Quantite"].Text, 0))
                            * Convert.ToDouble(General.nz(_with23.ActiveRow.Cells["PrixUnitaire"].Text, 0)));
                    }
                }

                //Prix unitaire
                if (_with23.ActiveCell.Column.Key.ToUpper() == "PrixUnitaire".ToUpper())//tested
                {
                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["PrixUnitaire"].Text) && !General.IsNumeric(_with23.ActiveRow.Cells["PrixUnitaire"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Inserer une valeur numérique", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = false;
                        return;
                    }

                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["Quantite"].Text) && !string.IsNullOrEmpty(_with23.ActiveRow.Cells["PrixUnitaire"].Text))
                    {
                        _with23.ActiveRow.Cells["MontantLigne"].Value = General.FncArrondir(Convert.ToDouble(_with23.ActiveRow.Cells["Quantite"].Text) * Convert.ToDouble(_with23.ActiveRow.Cells["PrixUnitaire"].Text));
                    }
                }

                //Montant ligne
                if (_with23.ActiveCell.Column.Key.ToUpper() == "MontantLigne".ToUpper())//tested
                {
                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["MontantLigne"].Text) && !General.IsNumeric(_with23.ActiveRow.Cells["MontantLigne"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Inserer une valeur numérique", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        e.Cancel = true;
                        return;
                    }

                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["MontantLigne"].Text))
                    {
                        _with23.ActiveRow.Cells["MontantLigne"].Value = General.FncArrondir(Convert.ToDouble(_with23.ActiveRow.Cells["MontantLigne"].Text));
                    }
                }


                //compte de vente
                if (_with23.ActiveCell.Column.Key.ToUpper() == "Compte7".ToUpper())//tested
                {
                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["Compte7"].Text))
                    {
                        //===@@@ modif du 29 05 2017, desactive Sage.
                        if (General.sDesActiveSage == "1")
                        {

                        }
                        else
                        {
                            SAGE.fc_OpenConnSage();
                            adotemp = new DataTable();
                            var tmpAdo = new ModAdo();
                            adotemp = tmpAdo.fc_OpenRecordSet("SELECT F_COMPTEG.CG_NUM AS [Code comptable], F_COMPTEG.CG_INTITULE, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM WHERE (F_COMPTEG.CG_NUM) ='" + _with23.ActiveRow.Cells["Compte7"].Text + "'", null, null, SAGE.adoSage);
                            if (adotemp.Rows.Count > 0)
                            {
                                SSOleDBGrid1.ActiveRow.Cells["Compte"].Value = adotemp.Rows[0]["CG_NUM"];
                                SSOleDBGrid1.ActiveRow.Cells["TVA"].Value = adotemp.Rows[0]["TA_Taux"];
                            }
                            else
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le numéro de compte de vente n'est pas valide, veuillez en saisir un autre", "Compte de vente érronné", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                SSOleDBGrid1.ActiveRow.Cells["Compte"].Value = "";
                                SSOleDBGrid1.ActiveRow.Cells["TVA"].Value = DBNull.Value;
                            }
                            adotemp.Dispose();

                            adotemp = null;
                        }
                    }
                }

                //Analytique activité
                //AnaActivite
                if (_with23.ActiveCell.Column.Key.ToUpper() == "AnaActivite".ToUpper())//tested
                {
                    if (!string.IsNullOrEmpty(_with23.ActiveRow.Cells["AnaActivite"].Text) && !General.IsNumeric(_with23.ActiveRow.Cells["AnaActivite"].Text))
                    {
                        //===@@@ modif du 29 05 2017, desactive Sage.
                        if (General.sDesActiveSage == "1")
                        {

                        }
                        else
                        {
                            //=== modif du 14 10 2014, ajout de la connection sage.
                            SAGE.fc_OpenConnSage();
                            adotemp = new DataTable();
                            var tmpAdo = new ModAdo();
                            adotemp = tmpAdo.fc_OpenRecordSet("SELECT F_COMPTEA.CA_NUM FROM F_COMPTEA WHERE (F_COMPTEA.CA_NUM) ='" + _with23.ActiveRow.Cells["AnaActivite"].Text + "' AND N_ANALYTIQUE=1", null, null, SAGE.adoSage);

                            if (adotemp.Rows.Count > 0)
                            {
                                SSOleDBGrid1.ActiveRow.Cells["AnaActivite"].Value = adotemp.Rows[0]["ca_num"];

                            }
                            else
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le compte analytique saisi est inconnu, veuillez en saisir un autre", "Compte analytique érronné", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                SSOleDBGrid1.ActiveRow.Cells["AnaActivite"].Value = "";
                                e.Cancel = true;
                                return;
                            }
                            adotemp.Dispose();

                            adotemp = null;
                        }
                    }
                }

                //=== controle si le code analytique existe.
                //'    If SSOleDBGrid1.Columns("AnaActivite").Position = ColIndex Then
                //    '
                //'            fc_CtrlAnalytique SSOleDBGrid1.Columns("AnaActivite").Text
                //'            Cancel = True
                //'            Exit Sub
                //'    End If
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_BeforeColUpdate");
            }

        }

        private void SSOleDBGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            ////---- Vérifie si un compte de vente est saisie.
            //if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[8].Text) && string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[11].Text) && boolArticleDevisCtl == false)
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un compte de vente", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            //    SSOleDBGrid1.ActiveCell = SSOleDBGrid1.ActiveRow.Cells["Compte de vente"];
            //    SSOleDBGrid1.PerformAction(UltraGridAction.EnterEditMode);//verifier avec Mondir
            //    SSOleDBGrid1.ActiveRow.Selected = true;
            //    // SSOleDBGrid1.ActiveCell.CancelUpdate();
            //    // SSOleDBGrid1.Col = 11;
            //    return;
            //}
            ////---- Vérifie si un compte de TVA est saisie.
            //if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[8].Text) && string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[10].Text) && boolArticleDevisCtl == false)
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un compte TVA", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);

            //    SSOleDBGrid1.ActiveCell = SSOleDBGrid1.ActiveRow.Cells[10];
            //    SSOleDBGrid1.PerformAction(UltraGridAction.EnterEditMode);//verifier avec Mondir
            //    SSOleDBGrid1.ActiveRow.Selected = true;
            //    return;
            //}
        }

        private void SSOleDBGrid1_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            e.DisplayPromptMsg = false;
        }
        private void SSOleDBGrid1_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_BeforeRowInsert() - Enter In The Function");
            //===> Fin Modif Mondir

            boolInsertion = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_BeforeRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                //if (stopBeforeRowUpdate)// this if added here to stop before row update event from firing when SSOleDBGrid1.DisplayLayout.Band[0].AddNew() is called
                //    return;code initule
                ////---- Vérifie si un compte de vente est saisie.
                if (!string.IsNullOrEmpty(e.Row.Cells["MontantLigne"].Text) && string.IsNullOrEmpty(e.Row.Cells["Compte7"].Text) && boolArticleDevisCtl == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un compte de vente", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Row.Cells["Compte7"].Activated = true;
                    e.Cancel = true;
                    return;
                }
                //---- Vérifie si un compte de TVA est saisie.
                if (!string.IsNullOrEmpty(e.Row.Cells["MontantLigne"].Text) && string.IsNullOrEmpty(e.Row.Cells["Compte"].Text) && boolArticleDevisCtl == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez inserer un compte TVA", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Row.Cells["Compte"].Activated = true;
                    e.Cancel = true;
                    return;
                }

                //=========> Mondir 09.06.2020 : Changed [14] to ["CleFactureManuelle"], Column 14 is not CleFactureManuelle
                e.Row.Cells["CleFactureManuelle"].Value = longCle;// 'CHAMPS CleFactureManuelle(non visible)

                if ((!string.IsNullOrEmpty(e.Row.Cells["PrixUnitaire"].Text) || !string.IsNullOrEmpty(e.Row.Cells["MontantLigne"].Text)) && string.IsNullOrEmpty(e.Row.Cells["Quantite"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez préciser la quantité", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                    e.Cancel = true;
                    return;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_DoubleClickCell() - Enter In The Function");
            //===> Fin Modif Mondir

            if (SSOleDBGrid1.ActiveCell == null)
                return;

            if (SSOleDBGrid1.ActiveCell.Column.Index == 0)
            {
                var DataSource = SSOleDBGrid1.DataSource as DataTable;

                if (SSOleDBGrid1.ActiveRow.IsAddRow)//tested
                {
                    // SSOleDBGrid1.AddItem("");
                    DataSource.Rows.Add(DataSource.NewRow());

                }
                else//tested
                {
                    //  SSOleDBGrid1.AddItem("", SSOleDBGrid1.AddItemRowIndex(SSOleDBGrid1.Bookmark));
                    DataSource.Rows.InsertAt(DataSource.NewRow(), SSOleDBGrid1.ActiveRow.Index);
                }
            }
        }

        private void SSOleDBGrid1_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_Error() - Enter In The Function");
            //===> Fin Modif Mondir

            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des erreurs lors de la validation des données se sont produites" + "\n" + "Vérifiez si vous n'avez pas inserer des valeurs alphanumériques dans des champs numériques"
                + "\n" + "Si le probleme persiste rafraichissez le tableau" + "\n" + "sinon valider et redemarer votre application", "Erreur dans la validation", MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            SSOleDBGrid1.EventManager.AllEventsEnabled = false;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Designation"].Width = 200;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["AnaActivite"].ValueList = SSOleDBDropDown1; //tva
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].ValueList = SSOleDBDropDown5;  //compte de vente     
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].Header.Caption = "Compte de TVA";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].Header.Caption = "Compte de vente";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TVA"].CellAppearance.BackColor = Color.Gray;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].CellAppearance.BackColor = Color.Gray;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumeroLigne"].CellAppearance.BackColor = Color.Gray;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CleFactureManuelle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Famille"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["SSFamille"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Unite"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TVA"].CellActivation = Activation.NoEdit;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].CellActivation = Activation.NoEdit;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumeroLigne"].CellActivation = Activation.NoEdit;

            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Poste"].Header.VisiblePosition = 0;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Article"].Header.VisiblePosition = 1;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Designation"].Header.VisiblePosition = 2;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Quantite"].Header.VisiblePosition = 3;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PrixUnitaire"].Header.VisiblePosition = 4;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MontantLigne"].Header.VisiblePosition = 5;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TVA"].Header.VisiblePosition = 6;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].Header.VisiblePosition = 7;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].Header.VisiblePosition = 8;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["AnaActivite"].Header.VisiblePosition = 9;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumeroLigne"].Header.VisiblePosition = 10;

            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeCategorie"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TarifArticle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TypePrestation"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["SousPrestation"].Hidden = true;

            //===> Mondir le 30.06.2020, Designation has no limits
            //SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Designation"].MaxLength = 40;
            //===> Fin modif Mondir
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Poste"].MaxLength = 5;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Famille"].MaxLength = 50;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["SSFamille"].MaxLength = 50;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Article"].MaxLength = 9;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Quantite"].MaxLength = 20;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Unite"].MaxLength = 20;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PrixUnitaire"].MaxLength = 20;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MontantLigne"].MaxLength = 20;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TVA"].MaxLength = 20;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].MaxLength = 50;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].MaxLength = 50;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["AnaActivite"].MaxLength = 256;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumeroLigne"].MaxLength = 256;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CleFactureManuelle"].MaxLength = 256;

            ///===> Mondir le 06.10.2020, demandé par Rachid
            SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = HeaderClickAction.Select;
            //===> fin modif Mondir

            //===> Mondir le 26.03.2021, https://groupe-dt.mantishub.io/view.php?id=2329
            //SSOleDBGrid1.DisplayLayout.Bands[0].Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            //===> Fin Modif Mondir

            SSOleDBGrid1.EventManager.AllEventsEnabled = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (SSOleDBGrid1.ActiveCell == null)
                    return;

                //MessageBox.Show(e.KeyChar.ToString());

                if (SSOleDBGrid1.ActiveCell.Column.Key.ToUpper() == "Poste".ToUpper())//tested
                {
                    var dt = SSOleDBGrid1.DataSource as DataTable;

                    if (e.KeyChar == 13)
                    {
                        if (SSOleDBGrid1.ActiveRow.IsAddRow)//tested
                        {
                            dt.Rows.Add(dt.NewRow());
                        }
                        else//tested
                        {
                            //   SSOleDBGrid1.AddItem("", SSOleDBGrid1.AddItemRowIndex(SSOleDBGrid1.Bookmark));
                            //===> Mondir le 08.03.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2327
                            //===> Line bellow is the old line, i added + 1 to the index
                            //dt.Rows.InsertAt(dt.NewRow(), SSOleDBGrid1.ActiveRow.Index);
                            //===> Mondir le 08.03.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2327#c5734
                            //===> Line bellow is the old line, i removed  + 1 again !!! to the index
                            dt.Rows.InsertAt(dt.NewRow(), SSOleDBGrid1.ActiveRow.Index);
                            //====> Fin Modif Mondir
                        }

                    }
                }
                else if (SSOleDBGrid1.ActiveCell.Column.Key.ToUpper() == "Article".ToUpper())//tested
                {
                    string where = "";
                    if (e.KeyChar == 13)
                    {
                        boolRechercheMULTI = true;

                        string req = "SELECT FacArticle.CodeArticle as \"Code Article\", FacArticle.Designation1 as \"Designation\" FROM FacArticle";
                        if (General.UCase(TypeFacture) == General.UCase("CM"))
                        {
                            where = " (FacArticle.CodeCategorieArticle='P'  OR FacArticle.CodeCategorieArticle='BT')";

                        }
                        else
                        {
                            where = " (FacArticle.CodeCategorieArticle='T' OR FacArticle.CodeCategorieArticle='BT')";

                        }
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des articles" };
                        fg.SetValues(new Dictionary<string, string> { { "CodeArticle", SSOleDBGrid1.ActiveRow.Cells["article"].Text.Trim() } });

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            boolOnlyArticle = true;

                            strCodeArticle = fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString();

                            if (SSOleDBGrid1.ActiveRow.IsAddRow)//tested
                            {
                                boolAddrow = true;
                                timer2.Enabled = true;
                                fg.Dispose();
                                fg.Close();
                                //  boolInsertion = True
                                //   eventArgs.keyAscii = 0;
                                return;
                            }
                            else//tested
                            {
                                timer2.Enabled = true;
                                fg.Dispose();
                                fg.Close();
                                return;
                            }

                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                boolOnlyArticle = true;

                                strCodeArticle = fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString();

                                if (SSOleDBGrid1.ActiveRow.IsAddRow)
                                {
                                    boolAddrow = true;
                                    timer2.Enabled = true;
                                    fg.Dispose();
                                    fg.Close();
                                    //  boolInsertion = True
                                    //   eventArgs.keyAscii = 0;
                                    return;
                                }
                                else
                                {
                                    timer2.Enabled = true;
                                    fg.Dispose();
                                    fg.Close();
                                    return;
                                }


                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();


                        //Mondir : Added this line to fix ==> https://groupe-dt.mantishub.io/view.php?id=1719
                        boolRechercheMULTI = false;

                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_KeyPress");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid3_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid3_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var _with25 = SSOleDBGrid3;
                //----enléve le message par défaut
                e.DisplayPromptMsg = false;
                //----affiche le message de confirmation de suppression.
                ModMain.londMsgb = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous supprimer ce(s) enregistrements?" + "\n" + "\n"
                    + "Si oui,le code état de l'intervention sera mis à ZZ.", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation));
                if (londMsgb != 7)
                {
                    //----boucle sur tous les enregist. séléctionnées pour la suppression
                    SQL = "";
                    for (i = 0; i <= _with25.Selected.Rows.Count - 1; i++)
                    {
                        //----dbdate1 n'est pas une date mais continet un signet

                        //  dbdate1 = _with25.SelBookmarks(i);
                        //----verifie si ces interventions sont sélétionnées.

                        if (_with25.Selected.Rows[i].Cells["Choix"].Value.ToString() == "Non")
                        {
                            //----Récupére tous les n° d'interventions.
                            if (string.IsNullOrEmpty(SQL))
                            {

                                SQL = " where intervention.NoIntervention = " + _with25.Selected.Rows[i].Cells["NoIntervention"].Value.ToString();
                            }
                            else
                            {

                                SQL = SQL + " and intervention.NoIntervention = " + _with25.Selected.Rows[i].Cells["NoIntervention"].Value.ToString();
                            }

                        }
                        else if (boolinsertInterv == true && _with25.Selected.Rows[i].Cells["Choix"].Value.ToString() == "Oui")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppresion annulée" + "Ce(s) interventions sont séléctionnées pour l'insertion" + "\n" + "Pour les suprimer annuler ou supprimer la facture", "Suppression annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            e.Cancel = false;
                            return;

                        }
                        else if (_with25.Selected.Rows[i].Cells["Choix"].Value.ToString() == "Oui")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppresion annulée" + "Ce(s) interventions sont séléctionnées pour l'insertion" + "\n" + "Pour les suprimer désélectionner l 'intervention", "Suppression annulée", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            e.Cancel = false;
                            return;
                        }
                    }
                    //----éxecute la requete qui changera le codeEtat en ZZ des interventions séléctionnées
                    if (!string.IsNullOrEmpty(SQL))
                    {
                        General.Execute("UPDATE Intervention SET CodeEtat = 'ZZ'" + SQL);
                    }
                }
                else
                {
                    //----annule la suppression.
                    e.Cancel = false;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid3_BeforeDelete");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid3_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid3_ClickCellButton() - Enter In The Function");
            //===> Fin Modif Mondir

            bool boolFolder = false;
            bool boolMp3 = false;
            string sMp3 = null;
            DataTable rsAchat = default(DataTable);

            ReportDocument CR = new ReportDocument();
            string SelectionFormula = "";
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            try
            {
                if (SSOleDBGrid3.ActiveCell.Column.Index == SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Column.Index)//tested
                {

                    if (!string.IsNullOrEmpty(SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Value.ToString()))
                    {
                        //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                        sMp3 = General.Mid(SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Value.ToString(), 1, General.Len(SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Value.ToString()) - 3) + "mp3";
                        // sMp3 = txtWave
                        //  General.fso = Interaction.CreateObject("Scripting.FileSystemObject");

                        // boolMp3 = General.fso.FileExists(sMp3);
                        boolMp3 = File.Exists(sMp3);
                        General.fso = null;
                        if (boolMp3 == true)
                        {
                            ModuleAPI.Ouvrir(sMp3);
                            return;
                        }
                        else
                        {
                            boolFolder = File.Exists(SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Value.ToString());

                            General.fso = null;
                            if (boolFolder == true)
                            {
                                ModuleAPI.Ouvrir(SSOleDBGrid3.ActiveRow.Cells["Rapport Vocal"].Value.ToString());
                                return;
                            }
                        }
                        if (boolFolder == false && boolMp3 == false)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        //Ouvrir Me.hWnd, sMp3
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }

                }
                else if (SSOleDBGrid3.ActiveCell.Column.Index == SSOleDBGrid3.ActiveRow.Cells["Achat"].Column.Index)//tested
                {
                    if (SSOleDBGrid3.ActiveRow.Cells["Achat"].Value.ToString() == "1")
                    {
                        rsAchat = new DataTable();
                        var tmpAdo = new ModAdo();
                        rsAchat = tmpAdo.fc_OpenRecordSet("SELECT BonDeCommande.NoBonDeCommande FROM BonDeCommande WHERE BonDeCommande.NoIntervention=" + General.nz(SSOleDBGrid3.ActiveRow.Cells["NoIntervention"].Value, "''"));
                        if (rsAchat.Rows.Count > 0)
                        {
                            //  rsAchat.MoveFirst();
                            CR.Load(General.gsRpt + General.ETATBONCOMMANDE);
                            //CR.Destination = Crystal.DestinationConstants.crptToWindow;
                            foreach (DataRow row in rsAchat.Rows)
                            {
                                var _with26 = CR;
                                SelectionFormula = "{BonDeCommande.NoBonDeCommande}= " + row["NoBonDeCommande"];
                                CrystalReportFormView form = new CrystalReportFormView(CR, SelectionFormula);
                                form.Show();
                                //_with26.Action = 1;
                                // rsAchat.MoveNext();
                            }
                        }
                        rsAchat.Dispose();
                        rsAchat = null;
                        SelectionFormula = "";
                        CR.FileName = "";
                        CR.FileName = General.ETATOLDFACTUREMANUEL;
                    }
                }
                if (SSOleDBGrid3.ActiveCell.Column.Index == SSOleDBGrid3.ActiveRow.Cells["Fichier"].Column.Index)//TEsted
                {
                    frmFichierInter frm = new frmFichierInter();
                    frm.txtNoIntervention.Text = SSOleDBGrid3.ActiveRow != null ? SSOleDBGrid3.ActiveRow.Cells["NoIntervention"].Value.ToString() : "";
                    frm.ShowDialog();
                }
                return;
            }
            catch (Exception ex)

            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdWave_Click");
                return;
            }

        }

        private void SSOleDBGrid3_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid3_DoubleClickRow() - Enter In The Function");
            //===> Fin Modif Mondir

            //----si la colonne choix est a "Oui" alors rétablir la couleur
            //----initiale puis remplacer "oui" par "non" et vice verça
            //----si aucune intervention n'a été inserer.
            //'on error GoTo Erreur
            try
            {
                if (boolinsertInterv == false)
                {
                    var _with27 = SSOleDBGrid3;

                    if (e.Row.Cells["Choix"].Value.ToString() == "Oui")
                    {
                        // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                        for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                        {
                            // e.Row.Cells["Choix"].Value.CellStyleSet("NoStyle", _with27.Row);TODO
                        }
                        e.Row.Cells["Choix"].Value = "Non";

                        //_with27.CtlUpdate();todo
                    }
                    else if (e.Row.Cells["Choix"].Value.ToString() == "Non")
                    {
                        if (!string.IsNullOrEmpty(e.Row.Cells["NoFacture"].Text))
                        {
                            DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cette intervention a déjà été facturée, Si vous décidez de l'insérer cette intervention sera affectée avec un nouveau numéro de facture." + "\n" + "Voulez-vous continuer ?", "Intervention facturée", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                            if (dg == DialogResult.No)
                            {
                                return;
                            }
                        }

                        // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                        for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                        {
                            // _with27.Groups(0).Columns(i).CellStyleSet("yesStyle", _with27.Row);
                        }
                        e.Row.Cells["Choix"].Value = "Oui";

                        //_with27.CtlUpdate();
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("les interventions ont déjà été insérer," + "\n" + "pour modifier celles-çi, annuler ou supprimer la facture", "Insertion annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid3_DblClick");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid3_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {

            try
            {
                var _with30 = SSOleDBGrid3;
                //----Donne une couleur bleu pour le champs Codeimmeuble
                e.Row.Cells["CodeImmeuble"].Appearance.BackColor = Color.Aquamarine;
                e.Row.Cells["NoFacture"].Appearance.BackColor = Color.DeepPink;
                e.Row.Cells["Fichier"].Appearance.BackColor = Color.Green;
                //  _with30.Groups(0).Columns(0).CellStyleSet("yesBisStyle", SSOleDBGrid1.Row);
                //----Color en jaune Les enregistrements avec le champs choix
                //----à oui.
                if (e.Row.Cells["Choix"].Value.ToString() == "Oui")
                {
                    for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                        e.Row.Cells[i].Appearance.BackColor = Color.LightGoldenrodYellow;
                }
                else
                {
                    for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                        e.Row.Cells[i].Appearance.BackColor = Color.White;
                }
                /* if (_with30.Columns("Choix").value == "Oui")
                 {
                     // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                     for (i = 1; i <= _with30.Groups(0).Columns.Count - 1; i++)
                     {
                         _with30.Groups(0).Columns(i).CellStyleSet("yesStyle", _with30.Row);
                     }
                 }*/
                SSOleDBGrid3.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid3_RowLoaded");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSTab1_SelectedTabChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                SSOleDBGrid1.UpdateData();
                if (SSTab1.SelectedTab == SSTab1.Tabs[3])
                {
                    if (Combo1.Rows.Count == 0)
                    {
                        //===remplit le ComboBox de l'onglet imprimer avec les noms des personnes ayant facturé.
                        adotemp = new DataTable();

                        var tmpAdo = new ModAdo();
                        var _with31 = adotemp;
                        _with31 = tmpAdo.fc_OpenRecordSet("SELECT distinct FactureManuelleEntete.NomPersonne From FactureManuelleEntete WHERE FactureManuelleEntete.NomPersonne <>'' order by NomPersonne");

                        DataTable dt = new DataTable();
                        dt.Columns.Add("Personnel");
                        var newRow = dt.NewRow();

                        foreach (DataRow r in _with31.Rows)
                        {
                            if (strNomPersonne != r["Nompersonne"].ToString())
                            {
                                newRow["Personnel"] = r["Nompersonne"];
                                dt.Rows.Add(newRow.ItemArray);
                                Combo1.DataSource = dt;
                            }
                            // _with31.MoveNext();
                        }
                        _with31.Dispose();

                        _with31 = null;
                    }
                    //===> Modif de la version V31.08.2020 ajouté par Mondir le 31.08.2020
                    fc_LoadInter();
                    //===> Fin Modif Mondir
                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    fc_MailObligatoire();
                    //===> Fin Modif Mondir
                    // Option5.Focus();
                }

                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";SSTab1_Click");
            }

        }

        /// <summary>
        /// Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        private void fc_MailObligatoire()
        {
            string sSQl = "";
            try
            {
                sSQl = "SELECT        TOP (1) MailObligatoire From Immeuble WHERE   CodeImmeuble = '" +
                       StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                using (var tmpModAdo = new ModAdo())
                    sSQl = tmpModAdo.fc_ADOlibelle(sSQl);

                if (sSQl == "1")
                {
                    lblMail.Visible = true;
                }
                else
                {
                    lblMail.Visible = false;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer2_Tick(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"timer2_Tick() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                if (boolAddrow == true)//tested
                {
                    SSOleDBGrid1.EventManager.AllEventsEnabled = false;

                    if (boolInsertion == true)
                    {
                        SSOleDBGrid1.UpdateData();

                        if (SSOleDBGrid1.Rows.Count == 1)
                            SSOleDBGrid1.Rows[0].Delete(false);
                        else if (SSOleDBGrid1.Rows.Count > 1)
                            SSOleDBGrid1.Rows[SSOleDBGrid1.Rows.Count - 1].Delete(false);
                    }
                    BisRechercheArticle();

                    SSOleDBGrid1.EventManager.AllEventsEnabled = true;

                    if (activeRow != -1 && activeCol != -1 && activeRow <= SSOleDBGrid1.Rows.Count - 1)
                    {
                        SSOleDBGrid1.Rows[activeRow].Cells[activeCol].Activate();
                        SSOleDBGrid1.PerformAction(UltraGridAction.EnterEditMode);
                    }

                    boolAddrow = false;
                }
                else//tested
                {
                    SSOleDBGrid1.UpdateData();

                    BisRechercheArticle();
                }
                boolInsertion = false;
                SSOleDBGrid1.UpdateData();
                boolOnlyArticle = false;
                timer2.Enabled = false;
                SSOleDBGrid1.UpdateData();
                boolRechercheMULTI = false;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Timer2_Timer");
            }

        }

        private void txtCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeimmeuble_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)e.KeyChar;

            var tmpado = new ModAdo();
            try
            {
                if (KeyAscii == 13)
                {
                    if (adotemp == null)
                    {
                        adotemp = new DataTable();
                    }
                    else
                    {

                        adotemp.Dispose();

                    }

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021, f (optGerant.Checked == true) changed a lot
                    if (optGerant.Checked == true)
                    {

                        adotemp = tmpado.fc_OpenRecordSet("SELECT Table1.Code1, Table1.Nom , Immeuble.CodeImmeuble ,"
                            + " Immeuble.Adresse , Immeuble.CodePostal, Immeuble.Ville AS Vil,Immeuble.NCompte,"
                            + " Table1.Adresse1, Table1.Adresse2, Table1.Adresse3, Table1.CodePostal AS CodePost, "
                            + " Table1.Ville,Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Immeuble.CodeTVA "
                            + " , Table1.codereglement, immeuble.RefClient "
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            + " , AdresseEnvoi1, AdresseEnvoi2, AdresseEnvoi3, AdresseEnvoi4, AdresseEnvoi5, AdresseEnvoiCP, AdresseEnvoiVille, UtiliseAdresseEnvoi, SIREN,  Immeuble.Immatriculation"
                            //===> Fin Modif Mondir
                            + " FROM Table1 LEFT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1 where Immeuble.CodeImmeuble='"
                            + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'", null, null, General.adocnn);


                        if (adotemp.Rows.Count > 0)
                        {
                            txtGerNom.Text = adotemp.Rows[0]["Nom"].ToString();

                            txtImmAdresse1.Text = adotemp.Rows[0]["Adresse"].ToString() + "";
                            txtCodeimmeuble.Text = adotemp.Rows[0]["CodeImmeuble"].ToString() + "";
                            txtImmCP.Text = adotemp.Rows[0]["CodePostal"].ToString() + "";
                            txtImmVille.Text = adotemp.Rows[0]["Vil"].ToString() + "";

                            if (General.sAdresseEnvoie == "1")
                            {
                                if (General.nz(adotemp.Rows[0]["UtiliseAdresseEnvoi"], 0).ToInt() == 1)
                                {
                                    txtCommentaire1.Text = General.Left(adotemp.Rows[0]["AdresseEnvoi1"] + "", 50);
                                    txtCommentaire2.Text = General.Left(adotemp.Rows[0]["AdresseEnvoi2"] + "", 50);
                                    txtGerNom.Text = General.Left(adotemp.Rows[0]["AdresseEnvoi3"] + "", 50);
                                    txtGerAdresse1.Text = General.Left(adotemp.Rows[0]["AdresseEnvoi4"] + "", 50);
                                    txtGerAdresse2.Text = General.Left(adotemp.Rows[0]["AdresseEnvoi5"] + "", 50);
                                    txtGerCP.Text = General.Left(adotemp.Rows[0]["AdresseEnvoiCP"] + "", 50);
                                    txtGerVille.Text = General.Left(adotemp.Rows[0]["AdresseEnvoiVille"] + "", 50);
                                }
                                else
                                {
                                    txtCommentaire1.Text = adotemp.Rows[0]["CommentaireFacture1"] + "";
                                    txtCommentaire2.Text = adotemp.Rows[0]["CommentaireFacture2"] + "";
                                    txtGerNom.Text = adotemp.Rows[0]["Adresse1"] + "";
                                    txtGerAdresse1.Text = adotemp.Rows[0]["Adresse2"] + "";
                                    txtGerAdresse2.Text = adotemp.Rows[0]["adresse3"] + "";
                                    txtGerCP.Text = adotemp.Rows[0]["CodePost"] + "";
                                    txtGerVille.Text = adotemp.Rows[0]["Ville"] + "";
                                }
                                txt_0.Text = adotemp.Rows[0]["CommentaireFacture1"] + "";
                                txt_1.Text = adotemp.Rows[0]["CommentaireFacture2"] + "";
                                txt_2.Text = adotemp.Rows[0]["Adresse1"] + "";
                                txt_3.Text = adotemp.Rows[0]["Adresse2"] + "";
                                txt_4.Text = adotemp.Rows[0]["adresse3"] + "";
                                txt_5.Text = adotemp.Rows[0]["CodePost"] + "";
                                txt_6.Text = adotemp.Rows[0]["Ville"] + "";
                            }
                            else
                            {
                                txtCommentaire1.Text = adotemp.Rows[0]["CommentaireFacture1"].ToString() + "";
                                txtCommentaire2.Text = adotemp.Rows[0]["CommentaireFacture1"].ToString() + "";
                                txtGerAdresse1.Text = adotemp.Rows[0]["Adresse1"].ToString() + "";
                                txtGerAdresse2.Text = adotemp.Rows[0]["Adresse2"].ToString() + "";
                                txtGerAdresse3.Text = adotemp.Rows[0]["adresse3"].ToString() + "";
                                txtGerCP.Text = adotemp.Rows[0]["CodePost"].ToString() + "";
                                txtGerVille.Text = adotemp.Rows[0]["Ville"].ToString() + "";
                            }
                            txt_7.Text = adotemp.Rows[0]["SIREN"].ToString() + "";
                            txt_8.Text = adotemp.Rows[0]["Immatriculation"].ToString() + "";
                            txt_9.Text = "0";


                            txtNCompte.Text = adotemp.Rows[0]["Ncompte"].ToString() + "";
                            txtRefClient.Text = adotemp.Rows[0]["RefClient"].ToString() + "";
                            strTRouTP = adotemp.Rows[0]["CodeTVA"].ToString() + "";
                            if (string.IsNullOrEmpty(strTRouTP))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                            }
                            txtTVA.Text = strTRouTP;
                            //== modif du 07 SEPT 2007 le code du reglement
                            //== se situe sur la fiche gérant.

                            if (!string.IsNullOrEmpty(adotemp.Rows[0]["CodeReglement"].ToString()))
                            {
                                txtMode.Text = adotemp.Rows[0]["CodeReglement"] + "";
                            }
                            else
                            {
                                txtMode.Text = General.sCodeReglement;
                            }

                            fc_ModeRegl(false);

                            txtModeLibelle.Text = tmpado.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement"
                                + " where Code='" + txtMode.Text + "'");

                        }
                        else
                        {
                            txtGerNom.Text = "";
                            txtGerAdresse1.Text = "";
                            txtGerAdresse2.Text = "";
                            txtGerAdresse3.Text = "";
                            txtGerCP.Text = "";
                            txtGerVille.Text = "";
                            txtImmAdresse1.Text = "";
                            // txtCodeimmeuble = ""
                            txtImmCP.Text = "";
                            txtImmVille.Text = "";
                            txtCommentaire1.Text = "";
                            txtCommentaire2.Text = "";
                            txtNCompte.Text = "";
                            strTRouTP = "";
                            txtTVA.Text = "";
                            txtRefClient.Text = "";

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txt_0.Text = "";
                            txt_1.Text = "";
                            txt_2.Text = "";
                            txt_3.Text = "";
                            txt_4.Text = "";
                            txt_5.Text = "";
                            txt_6.Text = "";
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021

                            adotemp?.Dispose();

                            adotemp = null;
                            cmdRechercheImmeuble_Click(cmdRechercheImmeuble, new System.EventArgs());
                            //goto EventExitSub;
                        }

                    }
                    else if (optParticulier.Checked == true)
                    {
                        //  adotemp.Open "SELECT ImmeublePart.CodeParticulier , ImmeublePart.Nom,  ImmeublePart.CodeImmeuble, ImmeublePart.NCompte, ImmeublePart.Adresse, ImmeublePart.CodePostal, ImmeublePart.Ville, Table1.Nom, Table1.Adresse1, Table1.Adresse2, Table1.Adresse3, Table1.CodePostal, Table1.Ville FROM ImmeublePart LEFT JOIN (Table1 RIGHT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1) ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble where ImmeublePart.CodeParticulier='" & gFr_DoublerQuote(txtCodeimmeuble) & "'", adocnn, adOpenForwardOnly, adLockReadOnly

                        adotemp = tmpado.fc_OpenRecordSet("SELECT ImmeublePart.CodeParticulier, ImmeublePart.Nom, "
                            + " ImmeublePart.CodeImmeuble, ImmeublePart.NCompte, "
                            + " Immeuble.Adresse, Immeuble.CodePostal AS CP, Immeuble.Ville AS Ville1, "
                            + " Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2, Immeuble.CodeTVA, "
                            + " ImmeublePart.Adresse AS Adr, ImmeublePart.Ville, ImmeublePart.CodePostal " + "  "
                            + " FROM ImmeublePart LEFT JOIN Immeuble " + " ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble "
                            + " where ImmeublePart.CodeParticulier='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'", null, null, General.adocnn);

                        if (adotemp.Rows.Count > 0)
                        {
                            var _with32 = adotemp;
                            txtGerNom.Text = _with32.Rows[0]["Nom"] + "";
                            txtGerAdresse1.Text = _with32.Rows[0]["Adr"] + "";
                            // txtGerAdresse2 = !adresse2 & ""
                            //' txtGerAdresse3 = !adresse3 & ""

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            if (General.sAdresseEnvoie == "1")
                            {
                                txt_0.Text = _with32.Rows[0]["Nom"] + "";
                                txt_1.Text = _with32.Rows[0]["Adr"] + "";
                                txt_5.Text = _with32.Rows[0]["CodePostal"] + "";
                                txt_6.Text = _with32.Rows[0]["Ville"] + "";
                            }
                            //===> Fin Modif Mondir

                            txtGerCP.Text = _with32.Rows[0]["CodePostal"] + "";
                            txtGerVille.Text = _with32.Rows[0]["Ville"] + "";

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txt_9.Text = "1";
                            //===> Fin Modif Mondir

                            txtCodeimmeuble.Text = _with32.Rows[0]["CodeImmeuble"] + "";
                            txtImmAdresse1.Text = _with32.Rows[0]["Adresse"] + "";
                            txtImmCP.Text = _with32.Rows[0]["CP"] + "";
                            txtImmVille.Text = _with32.Rows[0]["Ville1"] + "";
                            txtNCompte.Text = _with32.Rows[0]["Ncompte"] + "";
                            strTRouTP = _with32.Rows[0]["CodeTVA"] + "";
                            if (string.IsNullOrEmpty(strTRouTP))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                            }
                            txtCommentaire1.Text = _with32.Rows[0]["CommentaireFacture1"] + "";
                            txtCommentaire2.Text = _with32.Rows[0]["CommentaireFacture2"] + "";
                            txtTVA.Text = strTRouTP;


                        }
                        else
                        {
                            txtGerNom.Text = "";
                            txtGerAdresse1.Text = "";
                            txtGerAdresse2.Text = "";
                            txtGerAdresse3.Text = "";
                            txtGerCP.Text = "";
                            txtGerVille.Text = "";
                            txtImmAdresse1.Text = "";
                            txtCodeimmeuble.Text = "";
                            txtImmCP.Text = "";
                            txtImmVille.Text = "";
                            txtCommentaire1.Text = "";
                            txtCommentaire2.Text = "";
                            txtNCompte.Text = "";
                            txtRefClient.Text = "";

                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            txt_0.Text = "";
                            txt_1.Text = "";
                            txt_2.Text = "";
                            txt_3.Text = "";
                            txt_4.Text = "";
                            txt_5.Text = "";
                            txt_6.Text = "";
                            txt_7.Text = "";
                            txt_8.Text = "";
                            txt_9.Text = "";
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        }
                    }
                    //----enleve le bip
                    KeyAscii = 0;
                    adotemp?.Dispose();

                    adotemp = null;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeimmeuble_KeyPress");
            }

        }
        private void fc_ModeRegl(bool Bcopro)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ModeRegl() - Enter In The Function - Bcopro = {Bcopro}");
            //===> Fin Modif Mondir

            string sReq = null;

            var tmpAdo = new ModAdo();
            try
            {
                if (General.sModeRegClient == "1")
                {

                    sReq = "SELECT     Table1.CodeReglement" + " FROM   Immeuble INNER JOIN"
                        + " Table1 ON Immeuble.Code1 = Table1.Code1"
                        + "  WHERE     Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                }
                else if (General.sModeRegFacManu == "1")//tested
                {
                    sReq = "SELECT CodeReglement_IMM FROM immeuble " + " WHERE codeimmeuble='"
                        + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                }
                else
                {
                    return;
                }

                if (Bcopro == false)
                {
                    if (General.sModeRegFacManu == "1")//teseted
                    {

                        sReq = tmpAdo.fc_ADOlibelle(sReq);
                        if (!string.IsNullOrEmpty(sReq))
                        {
                            txtMode.Text = sReq;
                        }

                    }
                }

                if (Bcopro == true)
                {
                    if (General.sModeRegFacManuCopro == "1")
                    {

                        sReq = tmpAdo.fc_ADOlibelle(sReq);
                        if (!string.IsNullOrEmpty(sReq))
                        {
                            txtMode.Text = sReq;
                        }
                    }
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ModeRegl");
            }
        }

        private void txtDateDeFacture_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateDeFacture_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            blnChangeManu = true;
            //Indique que le chargement de txtDateDeFacture
            //s'éffectue manuellement

        }

        private void txtDateDeFacture_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateDeFacture_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtDateDeFacture.Text)) && !string.IsNullOrEmpty(txtDateDeFacture.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtDateDeFacture.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtDateDeFacture.Text))
            {
                txtDateDeFacture.Text = Convert.ToDateTime(txtDateDeFacture.Text).ToString();

                if (blnChangeManu == true)
                {
                    if (Convert.ToDateTime(txtDateDeFacture.Text) < General.GetDateDebutExercice())
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date de facture valable pour l'exercice précédent." + "\n" + "Veuillez en saisir une autre", "Date de facture invalide", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtDateDeFacture.Text = Convert.ToString(DateTime.Today);
                    }
                }
            }
        }

        private void txtDebut_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtDebut_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdImprimer_Click(cmdImprimer, new System.EventArgs());
                KeyAscii = 0;
            }
        }

        private void txtFin_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtFin_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdImprimer_Click(cmdImprimer, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// Modif de la version V28.06.2020 de VB6 Intégrée par Mondir le 28.06.2020
        /// Modif de la version V28.06.2020 de VB6 testée par Mondir le 28.06.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImmeuble_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            var tmpAdo = new ModAdo();
            short KeyAscii = (short)(e.KeyChar);
            try
            {
                //'on error GoTo Erreur
                if (KeyAscii == 13)
                {
                    SablierOnOff(true);
                    SSOleDBGrid3.DataSource = null;
                    adotemp = new DataTable();
                    var _with33 = adotemp;
                    RemplitGrille(adotemp);
                    // SQL = "SELECT Intervention.CodeEtat, Intervention.Deplacement, Intervention.CodeImmeuble, FacArticle.Designation1, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, Intervention.DateRealise, Intervention.Intervenant, Intervention.NumFicheStandard, Intervention.NoIntervention, GestionStandard.Document, Intervention.Article, Intervention.Achat" _
                    //& " FROM GestionStandard INNER JOIN (FacArticle RIGHT JOIN Intervention ON FacArticle.CodeArticle = Intervention.Article) ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard" _
                    //& " WHERE Intervention.CodeImmeuble ='" & txtImmeuble & "'"

                    //                 SQL = "SELECT Intervention.CodeEtat, Intervention.Deplacement, Intervention.CodeImmeuble, Intervention.Designation, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, Intervention.DateRealise, Intervention.Intervenant, Intervention.NumFicheStandard, Intervention.NoIntervention, GestionStandard.Document, Intervention.Article, Intervention.Achat, Intervention.Acompte" _
                    //'                     & " , INT_Rapport1, INT_Rapport2,INT_Rapport3,Wave " _
                    //'                    & " FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard" _
                    //'                    & " WHERE (((Intervention.CodeImmeuble)='" & gFr_DoublerQuote(txtImmeuble) & "')) "
                    //If optDevis.Value = True Then
                    // SQL = SQL & " and Intervention.CodeEtat = 'AF' ORDER BY Intervention.DateRealise"
                    //ElseIf optAf.Value = True Then
                    // SQL = SQL & " and Intervention.CodeEtat = 'E' ORDER BY Intervention.DateRealise"
                    //End If
                    //                 SQL = SQL & " and (Intervention.CodeEtat = 'AF'"
                    //                SQL = SQL & " or Intervention.CodeEtat = 'F') ORDER BY Intervention.DateRealise "

                    SQL = "SELECT Intervention.CodeEtat, Intervention.Deplacement, Intervention.CodeImmeuble,"
                        + " Intervention.Designation, Intervention.Commentaire, Intervention.Commentaire2,"
                        + " Intervention.Commentaire3, Intervention.DateRealise, Intervention.Intervenant,"
                        + " Intervention.NumFicheStandard, Intervention.NoIntervention," + " GestionStandard.Document, Intervention.Article, Intervention.Achat,"
                        + " Intervention.Acompte, INT_Rapport1, INT_Rapport2,INT_Rapport3, Wave,HeureDebut,HeureFin,Duree, NoFacture,'Fichier' as Fichier "
                        + " FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard"
                        + " WHERE (((Intervention.CodeImmeuble)='" + StdSQLchaine.gFr_DoublerQuote(txtImmeuble.Text) + "')) ";

                    if (General.sEvolution == "1")
                    {
                        SQL = SQL + " AND (Intervention.nofacture is null or Intervention.nofacture = '') ";

                        //====> Old Before V28.06.2020
                        if (Variable.lFactNumFicheStandard != 0)
                        {
                            //===> Ahmed: Modification 08 09 2020 V 07 09 2020
                            Program.SaveException(null, $"CmdMiseAjour_Click - General.sActiveFactMultiAppel  = {General.sActiveFactMultiAppel } optIntervention={optIntervention.Checked}");
                            if (General.sActiveFactMultiAppel == "1" && optIntervention.Checked)
                            {
                                //'=== ne fait rien.
                            }
                            else
                            {
                                SQL = SQL + " AND (Intervention.NumFicheStandard =" + Variable.lFactNumFicheStandard + ") ";
                            }

                        }
                        //Variable.lFactNumFicheStandard = 0;

                        if (optDevis.Checked)
                        {
                            //====> Mondir : Inntegrer les modif de la version V28.06.2020
                            //if (Variable.lFactNumFicheStandard != 0)
                            //{
                            //    SQL = SQL + " AND (Intervention.NumFicheStandard =" + Variable.lFactNumFicheStandard + ") ";
                            //}
                            SQL = SQL + "  AND (Intervention.CodeEtat <> '05' AND Intervention.CodeEtat <> 'ZZ')";
                            //====> Fin Modif Mondir
                        }
                        else
                        {
                            //'SQL = SQL & "  AND (Intervention.CodeEtat = 'E' OR Intervention.CodeEtat <> 'T' " _
                            //    & " )"
                            //'SQL = SQL & " AND (Intervention.CodeEtat <> '05' and Intervention.CodeEtat <> 'ZZ' and Intervention.CodeEtat <> 'P3')"
                            //'SQL = SQL & " AND (Intervention.NoDevis IS NULL OR Intervention.NoDevis = '')"

                            SQL = SQL + "  AND (Intervention.CodeEtat = 'E' )";
                        }
                        //====> Mondir : Inntegrer les modif de la version V28.06.2020
                        Variable.lFactNumFicheStandard = 0;
                        //====> Fin Modif Mondir
                    }
                    else
                    {
                        if (optDevis.Checked == true)
                        {
                            //== modif du 07 janvier 2007.
                            //SQL = SQL & " AND Intervention.CodeEtat = 'D' ORDER BY Intervention.NumFicheStandard DESC,Intervention.DateRealise "
                            //== récupération de toutes les interventions liées aux devis.
                            SQL = SQL + " AND (GestionStandard.NumFicheStandard IN" + " (SELECT     NumFicheStandard From intervention" + " WHERE      codeetat = 'D'))";
                        }
                        else
                        {
                            SQL = SQL + " AND Intervention.CodeEtat = 'E'";
                        }

                    }

                    if (General.sCritreDevisFac == "1")
                    {
                        if (optDevis.Checked == true)
                        {
                            if (string.IsNullOrEmpty(txtNoDevis.Text))
                            {
                                CustomMessageBox.Show("Le numéro de devis est désormais obligatoire.");
                                return;
                            }

                            SQL = SQL + " AND Intervention.nodevis = '" + txtNoDevis.Text + "'  ";
                        }

                    }

                    //SQL = SQL + " ORDER BY Intervention.DateRealise ";

                    //===> Modif de la version V31.08.2020 par Mondir le 31.08.2020
                    //SQL = SQL + " ORDER BY Intervention.NumFicheStandard, Intervention.DateRealise ";
                    //=== modif du 31 / 08 / 2020.
                    SQL = SQL + "ORDER BY  Intervention.DateRealise ";
                    //===> Fin Modif Mondir


                    adotemp = tmpAdo.fc_OpenRecordSet(SQL);
                    if (adotemp.Rows.Count > 0)
                    {
                        fc_ChargeContrat(txtImmeuble.Text);
                        RemplitGrille(adotemp);
                        // adotemp.MoveFirst();
                    }
                    else
                    {
                        CustomMessageBox.Show("Il n'y a aucune intervention non facturée disponible.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        adotemp.Dispose();
                        SablierOnOff(false);
                        cmdRechImmeuble_Click(cmdRechImmeuble, new System.EventArgs());
                        return;
                    }
                    adotemp.Dispose();

                    adotemp = null;
                    if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[0].Cells["Document"].ToString()))
                    {
                        CmdFax.Enabled = true;
                    }
                    else
                    {
                        CmdFax.Enabled = false;
                    }
                    if (SSOleDBGrid3.Rows[0].Cells["Achat"].ToString() == "True")
                    {
                        cmdAchat.Enabled = true;
                    }
                    else
                    {
                        cmdAchat.Enabled = false;
                    }
                    SablierOnOff(false);
                    //----enleve le bip.
                    KeyAscii = 0;
                    SSOleDBGrid3.Focus();
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtimmeuble_KeyPress");
            }


        }

        private void txtImpDate1_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate1_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option3.Checked = true;
        }

        private void txtImpDate1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdImprimer_Click(cmdImprimer, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImpDate1_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate1_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtImpDate1.Text)) && !string.IsNullOrEmpty(txtImpDate1.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImpDate1.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtImpDate1.Text))
            {
                txtImpDate1.Text = Convert.ToDateTime(txtImpDate1.Text).ToShortDateString();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImpDate2_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate2_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option3.Checked = true;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImpDate2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate2_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdImprimer_Click(cmdImprimer, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImpDate2_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtImpDate2_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtImpDate2.Text)) && !string.IsNullOrEmpty(txtImpDate2.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtImpDate2.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtImpDate2.Text))
            {
                txtImpDate2.Text = Convert.ToDateTime(txtImpDate2.Text).ToShortDateString();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseAjour1_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseAjour1_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option7.Checked = true;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseAjour1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseAjour1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                CmdMiseAjour_Click(CmdMiseAjour, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseAjour2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseAjour2_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                CmdMiseAjour_Click(CmdMiseAjour, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseAjour2_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseAjour2_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option7.Checked = true;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                CmdMiseAjour_Click(CmdMiseAjour, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate1_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate1_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option2.Checked = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate1_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate1_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtMiseDate1.Text)) && !string.IsNullOrEmpty(txtMiseDate1.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMiseDate1.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtMiseDate1.Text))
            {
                var dt = Convert.ToDateTime(txtMiseDate1.Text);
                txtMiseDate1.Text = Convert.ToDateTime(txtMiseDate1.Text).ToShortDateString();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate2_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate2_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtMiseDate2.Text)) && !string.IsNullOrEmpty(txtMiseDate2.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtMiseDate2.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtMiseDate2.Text))
            {
                txtMiseDate2.Text = Convert.ToDateTime(txtMiseDate2.Text).ToShortDateString();
            }
        }
        /// <summary>
        /// TESTSED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate2_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                CmdMiseAjour_Click(CmdMiseAjour, new System.EventArgs());
                KeyAscii = 0;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMiseDate2_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMiseDate2_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option2.Checked = true;
        }

        private void txtMode_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtMode_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            var tmpAdo = new ModAdo();
            try
            {
                if (KeyAscii == 13)
                {
                    adotemp = new DataTable();

                    adotemp = tmpAdo.fc_OpenRecordSet("SELECT CodeReglement.Code, CodeReglement.Libelle, CodeReglement.ModeReglt FROM CodeReglement where CodeReglement.Code ='"
                        + StdSQLchaine.gFr_DoublerQuote(txtMode.Text) + "'");

                    if (adotemp.Rows.Count > 0)
                    {
                        txtMode.Text = adotemp.Rows[0]["Code"] + "";
                        txtModeLibelle.Text = adotemp.Rows[0]["Libelle"] + "";
                        txtTypeReglement.Text = adotemp.Rows[0]["ModeReglt"] + "";
                    }
                    else
                    {
                        txtMode.Text = "";
                        txtModeLibelle.Text = "";
                        txtTypeReglement.Text = "";
                    }
                    KeyAscii = 0;
                    adotemp.Dispose();

                    adotemp = null;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtMode_KeyPress");
            }

        }

        private void txtTVA_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtTVA_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii != 8 && KeyAscii != 13)
            {
                if (General.Len(txtTVA.Text) == 0 && KeyAscii.ToString() != "T")
                {
                    KeyAscii = 0;
                }
                else if (General.Len(txtTVA.Text) == 1 && KeyAscii.ToString() != "R" && KeyAscii.ToString() != "P")
                {
                    KeyAscii = 0;
                }
                else if (General.Len(txtTVA.Text) == 2)
                {
                    KeyAscii = 0;
                }
                if (txtTVA.Text != "T")
                {
                    // KeyAscii = 0
                }

            }
        }

        private void txtTVA_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtTVA_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            if (txtTVA.Text != "TR" & txtTVA.Text != "TP" && !string.IsNullOrEmpty(txtTVA.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisissez TR OU TP", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtTVA.Focus();
            }
            else
            {
                strTRouTP = txtTVA.Text;
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisu1_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisu1_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option1.Checked = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisu1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisu1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdVisu_Click(cmdVisu, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisu2_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisu2_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option1.Checked = true;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisu2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisu2_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdVisu_Click(cmdVisu, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate1_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate1_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtVisuDate1.Text)) && !string.IsNullOrEmpty(txtVisuDate1.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtVisuDate1.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtVisuDate1.Text))
            {
                txtVisuDate1.Text = Convert.ToDateTime(txtVisuDate1.Text).ToShortDateString();

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate1_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdVisu_Click(cmdVisu, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate1_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate1_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option4.Checked = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate2_Enter(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate2_Enter() - Enter In The Function");
            //===> Fin Modif Mondir

            Option4.Checked = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate2_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate2_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdVisu_Click(cmdVisu, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVisuDate2_Leave(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtVisuDate2_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!(General.IsDate(txtVisuDate2.Text)) && !string.IsNullOrEmpty(txtVisuDate2.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                txtVisuDate2.Focus();
                return;
            }
            else if (!string.IsNullOrEmpty(txtVisuDate2.Text))
            {
                txtVisuDate2.Text = Convert.ToDateTime(txtVisuDate2.Text).ToShortDateString();
            }
        }
        /// <summary>
        /// tested*
        /// Modif de la version V29.06.2020 Ajoutée par Mondir le 30.06.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocFacManuel_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"UserDocFacManuel_VisibleChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            //== toutes les factures a partir de cette date
            //== auront un affichage en euro.
            //  ''on error GoTo Erreur

            string sSQlXX = "";

            if (!Visible)
            {
                fc_controle();
                SAGE.fc_CloseConnSage();
                boolLoaded = false;
                return;
            }


            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocFacManuel");

            boolLoaded = true;

            if (General.sServiceAnalytique == "1")//tested
            {
                cmbService.Visible = true;
                lblLibService.Visible = true;
                label5.Visible = true;
            }
            else
            {
                cmbService.Visible = false;
                lblLibService.Visible = false;
                label5.Visible = false;
            }





            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            dbEURO = Convert.ToDateTime("15 / 06 / 2001 00:00:00");

            dbEuroCr = Convert.ToDateTime(dbEURO).ToString("yyyy,MM,dd");
            SSTab1.SelectedTab = SSTab1.Tabs[0];
            SSTab1.Tabs[4].Visible = false;
            SablierOnOff1(true);//tested

            //Set adocnn = New ADODB.Connection
            //    adocnn.Open CHEMINBASE

            General.open_conn();


            if (General.adocnn.State == ConnectionState.Open)
            {


                //----creer des styles de couleur pour les enregistrements
                /*  SSOleDBGrid3.StyleSets["yesStyle"].BackColor = 8454143;
                  SSOleDBGrid3.StyleSets["yesBisStyle"].BackColor = 16776960;
                  SSOleDBGrid3.StyleSets["NoStyle"].BackColor = 0xffffff;

                  GridContrat.StyleSets["Resilie"].BackColor = 0x8080ff;
                  GridContrat.StyleSets["Resilie"].ForeColor = 0x0;

                  GridContrat.StyleSets["NoResilie"].BackColor = 0x80ff80;
                  GridContrat.StyleSets["NoResilie"].ForeColor = 0x0;

                  //-----Voir aide du grid Sheridan additem
                  SSOleDBGrid3.FieldDelimiter = "!";
                  SSOleDBGrid3.FieldSeparator = ";";*/

                cmdSupprimer.Enabled = false;
                cmdValider.Enabled = false;
                cmdAnnuler.Enabled = false;
                InitialiseEcran(this);//tested
                cmdRechercheImmeuble.Enabled = false;
                cmdRechercheReg.Enabled = false;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                ///===================> Mondir le 15.06.2020 : If we use the visible false, so we cant use the .checked anymore, it will be always false !
                ///==================> So the better is to hide it
                //Frame9.Location = new Point(Frame9.Location.X, 30);
                //Frame9.Visible = true;
                Frame9.Size = new Size(1374, 69);
                //=======> Salma 25-06-2020 :https://groupe-dt.mantishub.io/view.php?id=1870
                //lorsqu'on navigue vers d'autre fiche et on veut retourner vers factManuelle 
                //  'frmSynthese' reste sur son dérnier etat ,mais il doit etre invisible (sur design)
                frmSynthese.Visible = false;
                cmdAjouter.Visible = false;
                cmdSupprimer.Visible = false;
                cmdValider.Visible = false;
                cmdAnnuler.Visible = false;
                CmdRechercher.Visible = false;
                //CmdIntegration.Visible = False
                //cmdValidOpt.Visible = true;
                cmdValidOpt.Size = new Size(60, 35);
                cmdMenu.Visible = false;

                // Ouverture de la connection SAGE
                var tmpAdo = new ModAdo();

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    SAGE.fc_OpenConnSage();
                }

                if ((adotemp != null))
                {

                    adotemp.Dispose();

                }
                else
                {
                    adotemp = new DataTable();
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {//tested
                    var _with34 = adotemp;

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Analytique activité");
                    var newrow = dt.NewRow();

                    _with34 = tmpAdo.fc_OpenRecordSet("SELECT distinct F_COMPTEA.CA_NUM From F_COMPTEA WHERE F_COMPTEA.N_ANALYTIQUE=1", null, null, SAGE.adoSage);

                    if (_with34.Rows.Count > 0)
                    {
                        // _with34.MoveFirst();
                        foreach (DataRow r in _with34.Rows)
                        {
                            newrow["Analytique activité"] = r["CA_NUM"];
                            dt.Rows.Add(newrow.ItemArray);
                        }
                        SSOleDBDropDown1.DataSource = dt;
                    }
                    _with34.Dispose();
                }


                //=== compte par defaut pour les achats.
                var _with35 = adotemp;
                _with35 = tmpAdo.fc_OpenRecordSet("SELECT FacArticle.CG_Num,CG_Num2 From FacArticle WHERE FacArticle.CodeArticle='"
                    + General.sArticleAchat + "'");

                if (_with35.Rows.Count > 0)//tested
                {
                    // _with35.MoveFirst();
                    sCompte7AchatTP = _with35.Rows[0]["CG_Num2"] + "";
                    sCompte7AchatTR = _with35.Rows[0]["CG_NUM"] + "";
                }
                _with35.Dispose();

                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    SQL = "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                        + " WHERE (F_COMPTEG.CG_NUM ='" + sCompte7AchatTP + "') OR (F_COMPTEG.CG_NUM='" + sCompte7AchatTR + "')  " + "  ORDER BY F_COMPTEG.CG_NUM";

                    _with35 = tmpAdo.fc_OpenRecordSet(SQL, null, null, SAGE.adoSage);
                    if (_with35.Rows.Count > 0)//tested
                    {
                        // _with35.MoveFirst();
                        foreach (DataRow r in _with35.Rows)
                        {
                            if ((r["CG_Num1"] + "") == sCompte7AchatTP)//tested
                            {
                                sCompteTvaAchatTP = r["CG_NUM"] + "";
                                sTauxTvaAchatTP = r["TA_Taux"] + "";
                            }
                            else//tested
                            {
                                sCompteTvaAchatTR = r["CG_NUM"] + "";
                                sTauxTvaAchatTR = r["TA_Taux"] + "";
                            }
                            // _with35.MoveNext();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sCompteTvaAchatTP))
                {
                    sCompteTvaAchatTP = sCompteTvaAchatTR;
                    sTauxTvaAchatTP = sTauxTvaAchatTR;
                }

                if (string.IsNullOrEmpty(sCompteTvaAchatTR))
                {
                    sCompteTvaAchatTR = sCompteTvaAchatTP;
                    sTauxTvaAchatTR = sTauxTvaAchatTP;
                }
                if (General.adocnn.State == ConnectionState.Open)
                {
                    General.adocnn.Close();
                    _with35.Dispose();
                }
                //=== compte par defaut pour les devis.
                _with35 = tmpAdo.fc_OpenRecordSet("SELECT FacArticle.Designation1,FacArticle.CG_Num,CG_Num2 From FacArticle WHERE FacArticle.CodeArticle='" + General.sArticleDevis + "'");

                if (_with35.Rows.Count > 0)//tested
                {
                    //  _with35.MoveFirst();
                    sCompte7DevisTP = _with35.Rows[0]["CG_Num2"] + "";
                    sCompte7DevisTR = _with35.Rows[0]["CG_NUM"] + "";
                    sLibelleArtDevis = _with35.Rows[0]["Designation1"] + "";
                }
                _with35.Dispose();

                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    SQL = "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                        + " WHERE (F_COMPTEG.CG_NUM ='" + sCompte7DevisTP + "') OR (F_COMPTEG.CG_NUM='" + sCompte7DevisTR + "')  " + "  ORDER BY F_COMPTEG.CG_NUM";

                    _with35 = tmpAdo.fc_OpenRecordSet(SQL, null, null, SAGE.adoSage);

                    if (_with35.Rows.Count > 0)//tested
                    {
                        //_with35.MoveFirst();
                        foreach (DataRow r in _with35.Rows)
                        {
                            if ((r["CG_Num1"] + "") == sCompte7DevisTP)//tested
                            {
                                //== modif rachid 17 05 2005, erreur de variable.
                                //If (adotemp!CG_Num1 & "") = sCompte7AchatTP Then
                                sCompteTvaDevisTP = r["CG_NUM"] + "";
                                sTauxTvaDevisTP = r["TA_Taux"] + "";
                            }
                            else//tested
                            {
                                sCompteTvaDevisTR = r["CG_NUM"] + "";
                                sTauxTvaDevisTR = r["TA_Taux"] + "";
                            }
                            //_with35.MoveNext();
                        }
                    }
                    _with35.Dispose();

                }
                if (string.IsNullOrEmpty(sCompteTvaDevisTP))
                {
                    sCompteTvaDevisTP = sCompteTvaDevisTR;
                    sTauxTvaDevisTP = sTauxTvaDevisTR;
                }

                if (string.IsNullOrEmpty(sCompteTvaAchatTR))
                {
                    sCompteTvaAchatTR = sCompteTvaDevisTP;
                    sTauxTvaAchatTR = sTauxTvaDevisTP;
                }

                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    SQL = "SELECT CA_NUM,ActiviteDevis,ActiviteTravaux,ActiviteContrat FROM F_CompteA WHERE ActiviteDevis=1 OR ActiviteContrat=1 OR ActiviteTravaux=1";

                    _with35 = tmpAdo.fc_OpenRecordSet("SELECT CA_NUM,ActiviteDevis,ActiviteTravaux,ActiviteContrat FROM F_CompteA WHERE ActiviteDevis=1 OR ActiviteContrat=1 OR ActiviteTravaux=1", null, null, SAGE.adoSage);
                    if (_with35.Rows.Count > 0)//tested
                    {
                        //_with35.MoveFirst();
                        foreach (DataRow r in _with35.Rows)
                        {

                            if (General.nz(r["ActiviteDevis"], "0").ToString() == "True")//tested
                            {
                                sAnalDevis = r["CA_NUM"] + "";

                            }
                            else if (General.nz(r["ActiviteContrat"], "0").ToString() == "True")//tested
                            {
                                sAnalCont = r["CA_NUM"] + "";

                            }
                            else if (General.nz(r["ActiviteTravaux"], "0").ToString() == "True")//tested
                            {
                                sAnalTrav = r["CA_NUM"] + "";
                            }
                            // _with35.MoveNext();
                        }
                    }
                    _with35.Dispose();
                }

                //----Ci-dessous Pour une éventuelle modif.
                //Adodc4.ConnectionString = CHEMINBASE
                //Adodc4.RecordSource = "FamilleArticle"
                //Adodc4.Refresh
                //Adodc5.ConnectionString = CHEMINBASE
                //Adodc5.RecordSource = "SousFamilleArticle"
                //Adodc5.Refresh
                // With Adodc3
                //        .ConnectionString = CHEMINBASE
                //        .RecordSource = "SELECT CodeImmeuble FROM IMMEUBLE order by CodeImmeuble"
                //        .Refresh
                // End With
                //------------------------------------------

                //Adodc6.ConnectionString = CHEMINBASE
                //Adodc6.RecordSource = "Select CodeArticle, Designation1 from facArticle order by CodeArticle"
                //Adodc6.Refresh



                //    SQL = "SELECT F_COMPTEG.CG_NUM , F_COMPTEG.CG_INTITULE, " _
                //'    & " F_TAXE.CG_NUM, F_TAXE.TA_TAUX " _
                //'    & " FROM F_COMPTEG ,F_TAXE ,F_ETAXE " _
                //'    & " WHERE F_TAXE.TA_NO = F_ETAXE.TA_NO and F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                //'    & " and (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and length(F_COMPTEG.CG_NUM) = 8 )  "

                //    SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM , F_COMPTEG.CG_INTITULE, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN (F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO)) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                //'    & " WHERE (F_COMPTEG.CG_NUM LIKE '488%') OR (F_COMPTEG.CG_NUM Like '7%' AND (LEN(F_COMPTEG.CG_NUM) = 8 ))  " _
                //'    & "  ORDER BY F_COMPTEG.CG_NUM"


                //    SQL = "SELECT F_COMPTEG.CG_NUM, F_COMPTEG.CG_INTITULE, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                //'    & " WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 )  "


                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    //SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable], F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
                    //& " WHERE (F_COMPTEG.CG_NUM) LIKE '488%' OR (F_COMPTEG.CG_NUM Like '7%' AND LEN(F_COMPTEG.CG_NUM) = 8 )  " _
                    //& "  ORDER BY F_COMPTEG.CG_NUM"

                    SQL = "SELECT DISTINCT F_COMPTEG.CG_NUM AS [Code comptable], F_COMPTEG.CG_INTITULE AS [Intitulé], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" + " WHERE (F_COMPTEG.CG_NUM) LIKE '488%' OR (F_COMPTEG.CG_NUM Like '7%' AND LEN(F_COMPTEG.CG_NUM) >= 3 )  " + "  ORDER BY F_COMPTEG.CG_NUM";

                    adotemp = tmpAdo.fc_OpenRecordSet(SQL, null, null, SAGE.adoSage);

                    DataTable dt = new DataTable();
                    dt.Columns.Add("Code comptable");
                    dt.Columns.Add("Intitulé");
                    dt.Columns.Add("Compte de TVA");
                    dt.Columns.Add("Taux");

                    var newrow = dt.NewRow();
                    //    adotemp.Open SQL, adocnn
                    if (adotemp.Rows.Count > 0)//tested
                    {
                        //_with35.MoveFirst();
                        foreach (DataRow r in adotemp.Rows)
                        {


                            newrow["Code comptable"] = r["Code comptable"];
                            newrow["Intitulé"] = r["Intitulé"];
                            newrow["Compte de TVA"] = r["CG_NUM"];
                            newrow["Taux"] = r["TA_Taux"];
                            dt.Rows.Add(newrow.ItemArray);
                            // _with36.MoveNext();                            
                        }
                        SSOleDBDropDown5.DataSource = dt;
                        adotemp.Dispose();
                    }
                }


                strNomPersonne = General.fncUserName();
                lblNom.Text = strNomPersonne;
                SSTab1.Enabled = false;
            }
            if (General.sFACTURATIONTRAVAUX == "0")//tested
            {
                OptTravMan.Enabled = false;
            }

            //== recupere les

            if (General.sModeRegFacManu == "1")//tested
            {
                lblDateEcheance.Visible = true;
                txtDateEcheance.Visible = true;
            }
            else
            {
                lblDateEcheance.Visible = false;
                txtDateEcheance.Visible = false;
            }

            SablierOnOff(false);

            optIntervention.Focus();

            ////    ' Ferme la connection SAGE
            //    fc_CloseConnSage
            /* lblNavigation[0].Text = General.sNomLien0;
             lblNavigation[1].Text = General.sNomLien1;
             lblNavigation[2].Text = General.sNomLien2;*/

            FirstRemplitGrille();
            SecondRemplitGrille();
            DataTable dt1 = new DataTable();
            RemplitGrille(dt1);
            fc_ChargeContrat("0");

            //===> Mondir 05.08.2020, Ajout du LOG
            Program.SaveException(null, $"UserDocFacManuel_VisibleChanged() - General.sEvolution = {General.sEvolution}, " +
                                        $"General.sEvolution = {General.sEvolution}, " +
                                        $"Variable.lFactAVoir = {Variable.lFactAVoir}, " +
                                        $"Variable.lFactInterOuDevis = {Variable.lFactInterOuDevis}");
            //===> Fin Modif Mondir

            if (General.sEvolution == "1")
            {
                if (Variable.lFactAVoir == 1 || Variable.lFactAVoir == 2)
                {
                    if (Variable.lFactInterOuDevis == 1)
                    {
                        optIntervention.Checked = true;
                        optIntervention.Checked = true;
                    }
                    else if (Variable.lFactInterOuDevis == 2)
                    {
                        optDevis.Checked = true;
                    }
                    //=== controle si il existe une facture.
                    sSQlXX = "SELECT        Intervention.NoFacture"
                            + " FROM            Intervention INNER JOIN "
                            + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture "
                            + " WHERE        (Intervention.NumFicheStandard = " + Variable.lFactNumFicheStandard + ") AND (Intervention.NoFacture LIKE N'XX%')";
                    using (var tmpAdo = new ModAdo())
                        sSQlXX = tmpAdo.fc_ADOlibelle(sSQlXX);

                    if (!string.IsNullOrEmpty(sSQlXX))
                    {
                        CustomMessageBox.Show("Attention il existe déjà une facture en cours (n°" + sSQlXX + ") pour cette affaire", "Facture existante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }

                    cmdValidOpt_Click(cmdValidOpt, new System.EventArgs());

                    Variable.lFactAVoir = 0;
                    //Modif 17-07-2020 ajouté par salma
                    bButtonAdd = true;
                    //Fin modif
                    cmdAjouter_Click(cmdAjouter, new System.EventArgs());
                    //Modif 17-07-2020 ajouté par salma
                    bButtonAdd = false;
                    //Fin modif
                    txtImmeuble.Text = Variable.sImmFact;
                    txtNoDevis.Text = Variable.sDevisFact;
                    txtImmeuble_KeyPress(txtImmeuble, new KeyPressEventArgs((char)13));
                    //'=== facture definitive.
                    if (Variable.lDefOuAcompteOuGarantie == 2)
                    {
                        chkSolderDevis.Checked = true;
                        chkDefinitive.Checked = true;
                    }
                }
                //Frame10
                optDeselect.Visible = true;
                optSelect.Visible = true;

                Option6.Checked = true;
                Option8.Checked = true;
                OptFactEnCours.Checked = true;
                //====> Mondir le 30.06.2020, cette ligne étais dans la version 29.06.2020 de VB6, pas besoin dans C#
                //=== le champs désignation est illimité (ntext).
                //SSOleDBGrid1.Columns("Designation").FieldLen = 0
                //====> Fin Modif Mondir
            }
            else
            {
                //Frame10
                //====> Mondir le 30.06.2020 pour ajouter les modif de la version V29.06.2020 de VB6
                if (SSOleDBGrid1.DataSource != null)
                    SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Designation"].MaxLength = 200;
                //====> Fin modif Mondir
                optDeselect.Visible = false;
                optSelect.Visible = false;
            }
            Variable.sImmFact = "";
            Variable.lFactAVoir = 0;
            Variable.lFactInterOuDevis = 0;
            Variable.sDevisFact = "";

            return;

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        private void InitialiseEcran(Control C)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            //Program.SaveException(null, $"InitialiseEcran() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    InitialiseEcran(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {

                if (C.Tag != null && C.Tag.ToString() == "Valide")//tested
                {
                    C.Enabled = true;
                    C.Text = "";

                }
                else if (C.Tag != null && C.Tag.ToString() == "Fermer")//tested
                {
                    txtEnCours.Enabled = true;
                    txtEnCours.ReadOnly = true;

                }
                else
                {
                    C.Text = "";
                    C.Enabled = false;
                }

            }

            // SSOleDBGrid1.Enabled = False
            // SSOleDBGrid2.Enabled = False
            if (C is UltraGrid)
            {
                for (i = 0; i <= SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }
                for (i = 0; i <= SSOleDBGrid2.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSOleDBGrid2.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }
            }
            if (General.sServiceAnalytique == "1")//tested
            {
                cmbService.Text = "";
                lblLibService.Text = "";
            }
            chkDefinitive.Checked = false;
            //Mondir 24.06.2020 : Demande de Fabiola par mail => o	La coche « solder toutes les interventions » et « Tout sélectionner/désélectionner » ne se remet pas à jour lors de la génération d’une nouvelle facture
            optSelect.Checked = false;
            optDeselect.Checked = false;
            chkSolderDevis.Checked = false;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        private void retabliEcran(Control C)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            //Program.SaveException(null, $"retabliEcran() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;

            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    retabliEcran(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                C.Enabled = true;
            }
            //SSOleDBGrid1.Enabled = True
            //SSOleDBGrid2.Enabled = True

            txtEnCours.Enabled = true;
            txtEnCours.ReadOnly = true;

            if (C is UltraGrid)
            {
                for (i = 0; i <= SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    var key = SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].Key.ToUpper();

                    if (key != "TVA".ToUpper() && key != "Compte".ToUpper())
                    {
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    }
                }
                for (i = 0; i <= SSOleDBGrid2.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSOleDBGrid2.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                }
            }
            chkDefinitive.Enabled = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="Sablier"></param>
        public void SablierOnOff(bool Sablier)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SablierOnOff() - Enter In The Function - Sablier = {Sablier}");
            //===> Fin Modif Mondir

            //-----transforme le pointeur en sabier si fleche et inversement
            System.Windows.Forms.Application.DoEvents();
            if (Sablier)
            {

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.No;
            }
            else
            {

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
        }

        public void SablierOnOff1(bool Sablier)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SablierOnOff1() - Enter In The Function - Sablier = {Sablier}");
            //===> Fin Modif Mondir

            //-----transforme le pointeur en sabier si fleche et inversement
            if (Sablier)
            {

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            }
            else
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
        }
        //Private Sub Me_Terminate()
        // ''on error GoTo Erreur
        // fc_controle
        //     ' Ferme la connection SAGE
        //    fc_CloseConnSage
        //
        //  Exit Sub
        //erreur:
        //    gFr_debug Me.Name & ";Me_Terminate"
        //End Sub
        private void fc_controle()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_controle() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir le 31.05.2021 ajout du !txtEnCours.Text.Contains("XX" https://groupe-dt.mantishub.io/view.php?id=2449
            if (!boolLoaded || !txtEnCours.Text.Contains("XX"))
            {
                return;
            }

            frmLogin frmLogin = new frmLogin();
            frmLogin.StartPosition = FormStartPosition.CenterParent;

            var Message = "";
            if (boolAddnew == true)
            {
                Message = "Vous étes en mode ajout, Voulez vous enregistrer cette facture?";
                //===> Mondir le 26.03.2021 added ModMain. coz it was only londMsgb
                //===> Mondir le 15.04.2021, commented line bellow has nee, moved down
                //if (ModMain.londMsgb == 7)
                //{
                //    boolFermeture = true;
                //    cmdValider_Click(cmdValider, new System.EventArgs());
                //}
                //else
                //{
                //    boolFermeture = true;
                //    cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                //}
                //===> Fin Modif Mondir
            }
            else
            {
                Message = "Voulez vous enregistrer cette facture?";
            }
            frmLogin.labelMessage.Text = Message;
            frmLogin.ShowDialog();

            //===> Mondir le 26.03.2021 added ModMain. coz it was only londMsgb
            if (ModMain.londMsgb == 7)
            {
                boolFermeture = true;
                cmdValider_Click(cmdValider, new System.EventArgs());
            }
            else
            {
                boolFermeture = true;
                cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
            }

            if ((adoManuel != null))
            {
                adoManuel.Dispose();
                adoManuel = null;
            }

            // CR.SelectionFormula = "";
            // CR.ReportFileName = "";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        private void EcranLecture(Control C)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"EcranLecture() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    EcranLecture(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                if (C.Tag != null && C.Tag.ToString() == "Valide")
                {
                    C.Enabled = true;
                }
                else
                {
                    C.Enabled = false;
                }

            }
            if (C is UltraGrid)
            {
                //SSOleDBGrid1.Enabled = False
                for (i = 0; i <= SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }
                for (i = 0; i <= SSOleDBGrid2.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    SSOleDBGrid2.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                }
            }
            txtEnCours.Enabled = false;
            chkDefinitive.Enabled = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="adotemp"></param>
        private void RemplitGrille(DataTable adotemp)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"RemplitGrille() - Enter In The Function");
            //===> Fin Modif Mondir

            //----procédure pour remplir un tableau de type additem
            try
            {
                var _with37 = adotemp;
                DataTable dt = new DataTable();
                dt.Columns.Add("CodeImmeuble");
                dt.Columns.Add("NoIntervention");
                dt.Columns.Add("DateRealise");
                dt.Columns.Add("CodeEtat");
                dt.Columns.Add("Intervenant");
                dt.Columns.Add("Deplacement");
                dt.Columns.Add("Designation1");
                dt.Columns.Add("Commentaire");
                dt.Columns.Add("Commentaire2");
                dt.Columns.Add("Commentaire3");
                dt.Columns.Add("Numfichestandard");
                dt.Columns.Add("Document");
                dt.Columns.Add("Choix");
                dt.Columns.Add("CodeArticle");
                //dt.Columns.Add("BonDeCommande");
                dt.Columns.Add("Achat");
                dt.Columns.Add("acompte");
                dt.Columns.Add("INT_Rapport1");
                dt.Columns.Add("INT_Rapport2");
                dt.Columns.Add("INT_Rapport3");
                dt.Columns.Add("Rapport Vocal");
                dt.Columns.Add("HeureDebut");
                dt.Columns.Add("HeureFin");
                dt.Columns.Add("Duree");
                dt.Columns.Add("NoFacture");
                dt.Columns.Add("Fichier");
                dt.Columns.Add("N°Appel");
                SSOleDBGrid3.DataSource = dt;

                var NewR = dt.NewRow();
                // SSOleDBGrid3.Redraw = false;todo
                //.MoveFirst
                foreach (DataRow r in adotemp.Rows)
                {
                    NewR["CodeImmeuble"] = r["CodeImmeuble"];
                    NewR["NoIntervention"] = r["NoIntervention"];
                    if (r["DateRealise"] != DBNull.Value && General.IsDate(r["DateRealise"]))
                        NewR["DateRealise"] = Convert.ToDateTime(r["DateRealise"]).ToString(General.FormatDateSansHeureSQL);
                    NewR["CodeEtat"] = r["CodeEtat"];
                    NewR["Intervenant"] = r["Intervenant"];
                    NewR["Deplacement"] = r["Deplacement"];
                    NewR["Designation1"] = r["Designation"];
                    NewR["Commentaire"] = r["Commentaire"];
                    NewR["Commentaire2"] = r["Commentaire2"];
                    NewR["Commentaire3"] = r["Commentaire3"];
                    NewR["Numfichestandard"] = r["Numfichestandard"];
                    NewR["Document"] = r["Document"];
                    NewR["Choix"] = "Non";
                    NewR["CodeArticle"] = r["Article"];
                    if (string.IsNullOrEmpty(r["Achat"].ToString()))
                        NewR["Achat"] = 0;
                    else
                        NewR["Achat"] = 1;
                    NewR["acompte"] = r["acompte"];
                    NewR["INT_Rapport1"] = r["INT_Rapport1"];
                    NewR["INT_Rapport2"] = r["INT_Rapport2"];
                    NewR["INT_Rapport3"] = r["INT_Rapport3"];
                    NewR["Rapport Vocal"] = r["Wave"];
                    NewR["HeureDebut"] = r["HeureDebut"];
                    NewR["HeureFin"] = r["HeureFin"];
                    NewR["Duree"] = r["Duree"];
                    NewR["NoFacture"] = r["NoFacture"];

                    NewR["Fichier"] = "Fichier";
                    NewR["N°Appel"] = r["Numfichestandard"];

                    dt.Rows.Add(NewR.ItemArray);

                }
                SSOleDBGrid3.DataSource = dt;
                SSOleDBGrid3.Refresh();
                //===> Mondir le 06.07.2020, sometime the grid ot showing anything, only blank rows
                SSOleDBGrid3.UpdateData();
                SSOleDBGrid3.Update();
                SSOleDBGrid3.Rows.Refresh(RefreshRow.FireInitializeRow, true);
                //===> Fin Modif Mondir
                //  SSOleDBGrid3.Redraw = true;todo
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RemplitGrille");
            }

        }
        /// <summary>
        /// ttested
        /// </summary>
        private void FirstRemplitGrille()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"FirstRemplitGrille() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                SSOleDBGrid1ModAdo = new ModAdo();
                SSOleDBGrid1DataTable = SSOleDBGrid1ModAdo.fc_OpenRecordSet("SELECT * From FactureManuelleDetail Where FactureManuelleDetail.CleFactureManuelle =" + longCle + " order by NumeroLigne");
                //SSOleDBGrid1.Redraw = false;
                //DataTable dt = new DataTable();
                //dt.Columns.Add("Poste");
                //dt.Columns.Add("Famille");
                //dt.Columns.Add("SSFamille");

                //dt.Columns.Add("Article");
                //dt.Columns.Add("Designation");
                //dt.Columns.Add("Quantite");
                //dt.Columns.Add("Unite");
                //dt.Columns.Add("Prixunitaire");
                //dt.Columns.Add("MontantLigne");
                //dt.Columns.Add("TVA");
                //dt.Columns.Add("Compte");
                //dt.Columns.Add("Compte7");
                //dt.Columns.Add("AnaActivite");
                //dt.Columns.Add("NumeroLigne");
                //dt.Columns.Add("CleFactureManuelle");
                SSOleDBGrid1.DataSource = SSOleDBGrid1DataTable;

                //var NewR = dt.NewRow();
                //if (_with38.Rows.Count > 0)
                //{

                //    // SSOleDBGrid3.Redraw = false;
                //    //.MoveFirst
                //    foreach (DataRow r in _with38.Rows)
                //    {

                //        NewR["Poste"] = r["Poste"];
                //        NewR["Famille"] = r["Famille"];
                //        NewR["SSFamille"] = r["SSFamille"];
                //        NewR["Article"] = r["Article"];
                //        NewR["Designation"] = r["Designation"];
                //        NewR["Quantite"] = r["Quantite"];

                //        string prixunitaire = r["Prixunitaire"].ToString();

                //        if (!string.IsNullOrEmpty(prixunitaire) && General.IsNumeric(prixunitaire))
                //            prixunitaire = Convert.ToDouble(prixunitaire).ToString("#.##");
                //        else
                //            prixunitaire = "";

                //        NewR["Prixunitaire"] = prixunitaire;

                //        string montantLigne = r["MontantLigne"].ToString();

                //        if (!string.IsNullOrEmpty(montantLigne) && General.IsNumeric(montantLigne))
                //            montantLigne = Convert.ToDouble(montantLigne).ToString("#.##");
                //        else
                //            montantLigne = "";

                //        NewR["MontantLigne"] = montantLigne;

                //        string tva = r["TVA"].ToString();

                //        if (!string.IsNullOrEmpty(tva) && General.IsNumeric(tva))
                //            tva = Convert.ToDouble(tva).ToString("#.#");
                //        else
                //            tva = "";

                //        NewR["TVA"] = tva;
                //        NewR["Compte de TVA"] = r["Compte"];
                //        NewR["Compte de vente"] = r["Compte7"];
                //        NewR["AnaActivite"] = r["AnaActivite"];
                //        NewR["NumeroLigne"] = r["NumeroLigne"];
                //        NewR["CleFactureManuelle"] = r["CleFactureManuelle"];
                //        dt.Rows.Add(NewR.ItemArray);

                //    }
                //    SSOleDBGrid1.DataSource = dt;
                //}
                //_with38.Dispose();
                SSOleDBGrid1.Update();
                //  SSOleDBGrid1.Redraw = true;

                //adotemp = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FirstRemplitGrille");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void SecondRemplitGrille()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SecondRemplitGrille() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                adotemp = new DataTable();
                var tmpAdo = new ModAdo();
                var _with38 = adotemp;
                _with38 = tmpAdo.fc_OpenRecordSet("SELECT FactureManuellePiedBis.* From FactureManuellePiedBis Where FactureManuellePiedBIS.CleFactureManuelle =" + longCle);
                DataTable dt = new DataTable();

                dt.Columns.Add("CleFactureManuelle");
                dt.Columns.Add("Poste");
                dt.Columns.Add("MontantHT");
                dt.Columns.Add("TVA");
                dt.Columns.Add("MontantTVA");
                dt.Columns.Add("MontantTTC");
                dt.Columns.Add("Compte7");
                dt.Columns.Add("Analytique Activite");
                dt.Columns.Add("Designation");
                SSOleDBGrid2.DataSource = dt;
                var NewR = dt.NewRow();
                if (_with38.Rows.Count > 0)
                {

                    // SSOleDBGrid3.Redraw = false;
                    //.MoveFirst
                    foreach (DataRow r in _with38.Rows)
                    {

                        NewR["Poste"] = r["Poste"];

                        string montantHT = r["montantHT"].ToString();
                        if (!string.IsNullOrEmpty(montantHT) && General.IsNumeric(montantHT))
                            montantHT = Convert.ToDouble(montantHT).ToString("#.##");
                        else
                            montantHT = "";
                        NewR["MontantHT"] = montantHT;

                        string tva = r["TVA"].ToString();
                        if (!string.IsNullOrEmpty(tva) && General.IsNumeric(tva))
                            tva = Convert.ToDouble(tva).ToString("#.#");
                        else
                            tva = "";

                        NewR["TVA"] = tva;


                        string montantTVA = r["montantTVA"].ToString();
                        if (!string.IsNullOrEmpty(montantTVA) && General.IsNumeric(montantTVA))
                            montantTVA = Convert.ToDouble(montantTVA).ToString("#.##");
                        else
                            montantTVA = "";

                        NewR["MontantTVA"] = montantTVA;

                        string montantTTC = r["montantTTC"].ToString();
                        if (!string.IsNullOrEmpty(montantTTC) && General.IsNumeric(montantTTC))
                            montantTTC = Convert.ToDouble(montantTTC).ToString("#.##");
                        else
                            montantTTC = "";


                        NewR["MontantTTC"] = montantTTC;
                        NewR["Compte7"] = r["Compte7"];
                        NewR["Analytique Activite"] = r["AnaActivite"];

                        dt.Rows.Add(NewR.ItemArray);
                    }
                    SSOleDBGrid2.DataSource = dt;
                }
                _with38.Dispose();
                SSOleDBGrid3.Refresh();
                //  SSOleDBGrid2.Redraw = true;

                adotemp = null;
                return;


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SecondRemplitGrille");

            }

        }
        /// <summary>
        /// tetsted
        /// Modifs de la version V05.07.2020 Ajoutées Par Mondir le 05.07.2020 ===> Dfault value of sTypeTvaXX to empty insead of 33
        /// Modifs de la version V05.07.2020 Testées Par Mondir le 05.07.2020
        /// </summary>
        /// <param name="lintervention"></param>
        private void fournisEntete(int lintervention = 0, string sTypeTvaXX = "")
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fournisEntete() - Enter In The Function - lintervention = {lintervention}, sTypeTvaXX = {sTypeTvaXX}");
            //===> Fin Modif Mondir

            try
            {
                bool blnCopro = false;

                string sCodeParticulier = null;
                string sReq = null;

                adotemp = new DataTable();
                General.rstmp = new DataTable();
                var tmpAdo = new ModAdo();
                lblP3.Visible = false;
                lblChaudCondensation.Visible = false;
                //controle si la facture doit etre etablis au nom du gerant ou differente,un copro par exemple.
                if (lintervention != 0)
                {

                    General.sSQL = "SELECT GestionStandard.CodeOrigine2, GestionStandard.CodeParticulier"
                        + " FROM GestionStandard INNER JOIN" + " Intervention ON GestionStandard.NumFicheStandard"
                        + " = Intervention.NumFicheStandard" + " WHERE CodeOrigine2 = '10' and Nointervention ="
                        + lintervention + " and (GestionStandard.CodeParticulier is not null or GestionStandard.CodeParticulier <> '')";

                    General.rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        blnCopro = true;
                        sCodeParticulier = General.rstmp.Rows[0]["CodeParticulier"] + "";
                    }
                    else
                    {
                        blnCopro = false;
                    }
                    General.rstmp.Dispose();

                    General.rstmp = null;

                }
                else//tested
                {
                    blnCopro = false;
                }

                if (blnCopro == false)//tested
                {

                    adotemp = tmpAdo.fc_OpenRecordSet("SELECT Table1.Code1, Table1.Nom , Immeuble.CodeImmeuble , Immeuble.Adresse ,"
                        + " Immeuble.CodePostal as [IMMCodePostal], Immeuble.Ville as [IMMVille],Immeuble.NCompte,"
                        + " Table1.Adresse1, Table1.Adresse2,Table1.codereglement, Immeuble.AdresseFact1_IMM, Immeuble.AdresseFact2_IMM"
                        + " ,Immeuble.AdresseFact3_IMM, CodePostal_IMM, VilleFact_IMM," + " Table1.Adresse3, Table1.CodePostal as [TABCodePostal], Table1.Ville as [TABVille],"
                        + " Immeuble.CommentaireFacture1,  immeuble.RefClient, " + " Immeuble.CommentaireFacture2,Immeuble.CodeTVA,CodeReglement_IMM,VIR_Code, Immeuble.P3, immeuble.ChaudCondensation "
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        + " , AdresseEnvoi1, AdresseEnvoi2, AdresseEnvoi3, AdresseEnvoi4, AdresseEnvoi5, AdresseEnvoiCP, AdresseEnvoiVille, UtiliseAdresseEnvoi, SIREN,  Immeuble.Immatriculation "
                        //===> Fin Modif Mondir
                        + " FROM Table1 LEFT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1" + " where Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'");

                    if (adotemp.Rows.Count > 0)
                    {
                        var _with40 = adotemp;
                        //txtGerNom = !Nom & ""
                        // modif 06082002
                        //txtGerAdresse1 = !adresse1 & ""
                        //txtGerAdresse2 = !adresse2 & ""
                        //txtGerAdresse3 = !adresse3 & ""
                        if (_with40.Rows[0]["P3"].ToString() == "True")
                        {
                            lblP3.Visible = true;

                        }
                        else//tested
                        {
                            lblP3.Visible = false;
                        }

                        if (General.nz(_with40.Rows[0]["ChaudCondensation"], 0).ToString() == "1")
                        {
                            lblChaudCondensation.Visible = true;
                        }
                        else//tested
                        {
                            lblChaudCondensation.Visible = false;
                        }

                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        if (General.sAdresseEnvoie == "1")
                        {
                            if (General.nz(_with40.Rows[0]["UtiliseAdresseEnvoi"], 0).ToInt() == 1)
                            {
                                txtCommentaire1.Text = General.Left(_with40.Rows[0]["AdresseEnvoi1"] + "", 50);
                                txtCommentaire2.Text = General.Left(_with40.Rows[0]["AdresseEnvoi2"] + "", 50);
                                txtGerNom.Text = General.Left(_with40.Rows[0]["AdresseEnvoi3"] + "", 50);
                                txtGerAdresse1.Text = General.Left(_with40.Rows[0]["AdresseEnvoi4"] + "", 50);
                                txtGerAdresse2.Text = General.Left(_with40.Rows[0]["AdresseEnvoi5"] + "", 50);
                                txtGerCP.Text = General.Left(_with40.Rows[0]["AdresseEnvoiCP"] + "", 50);
                                txtGerVille.Text = General.Left(_with40.Rows[0]["AdresseEnvoiVille"] + "", 50);
                            }
                            else
                            {
                                txtCommentaire1.Text = _with40.Rows[0]["CommentaireFacture1"] + "";
                                txtCommentaire2.Text = _with40.Rows[0]["CommentaireFacture2"] + "";
                                txtGerNom.Text = _with40.Rows[0]["AdresseFact1_IMM"] + "";
                                txtGerAdresse1.Text = _with40.Rows[0]["AdresseFact2_IMM"] + "";
                                txtGerAdresse2.Text = _with40.Rows[0]["AdresseFact3_IMM"] + "";
                                txtGerCP.Text = _with40.Rows[0]["CodePostal_IMM"] + "";
                                txtGerVille.Text = _with40.Rows[0]["VilleFact_IMM"] + "";
                            }
                            txt_0.Text = _with40.Rows[0]["CommentaireFacture1"] + "";
                            txt_1.Text = _with40.Rows[0]["CommentaireFacture2"] + "";
                            txt_2.Text = _with40.Rows[0]["AdresseFact1_IMM"] + "";
                            txt_3.Text = _with40.Rows[0]["AdresseFact2_IMM"] + "";
                            txt_4.Text = _with40.Rows[0]["AdresseFact3_IMM"] + "";
                            txt_5.Text = _with40.Rows[0]["CodePostal_IMM"] + "";
                            txt_6.Text = _with40.Rows[0]["VilleFact_IMM"] + "";
                        }
                        else
                        {
                            txtGerNom.Text = _with40.Rows[0]["AdresseFact1_IMM"] + "";
                            txtGerAdresse1.Text = _with40.Rows[0]["AdresseFact2_IMM"] + "";
                            txtGerAdresse2.Text = _with40.Rows[0]["AdresseFact3_IMM"] + "";
                            txtGerCP.Text = _with40.Rows[0]["CodePostal_IMM"] + "";
                            txtGerVille.Text = _with40.Rows[0]["VilleFact_IMM"] + "";
                            txtCommentaire1.Text = _with40.Rows[0]["CommentaireFacture1"] + "";
                            txtCommentaire2.Text = _with40.Rows[0]["CommentaireFacture2"] + "";
                        }
                        txt_7.Text = _with40.Rows[0]["SIREN"] + "";
                        txt_8.Text = _with40.Rows[0]["Immatriculation"] + "";
                        txt_9.Text = "0";
                        //===> Fin Modif Mondir

                        //== rachid message apparait souvent derange plus que n'aide.25/09/2006.
                        ///                   If txtGerNom.Text <> !Nom & "" And txtGerAdresse1.Text <> !Nom & "" And txtGerAdresse2.Text <> !Nom & "" Then
                        ///                        MsgBox "Attention, le nom du gérant n'apparaît pas dans l'adresse de facturation.", vbInformation, "Adresse de facturation"
                        ///                   End If

                        //txtGerAdresse3 = !AdresseFact3_IMM & ""
                        //txtGerCP = ![TABCodePostal] & ""
                        //txtGerVille = ![TABVille] & ""

                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        txtImmAdresse1.Text = _with40.Rows[0]["Adresse"] + "";
                        txtCodeimmeuble.Text = _with40.Rows[0]["CodeImmeuble"] + "";
                        txtImmCP.Text = _with40.Rows[0]["IMMCodePostal"] + "";
                        txtImmVille.Text = _with40.Rows[0]["IMMVille"] + "";

                        txtNCompte.Text = _with40.Rows[0]["Ncompte"] + "";
                        //===> Fin Modif Mondir

                        if (General.Left(General.UCase(txtNCompte.Text), 1) == General.UCase("d"))
                        {
                            optParticulier.Checked = true;
                        }
                        else//tested
                        {
                            optGerant.Checked = true;
                        }

                        //strTRouTP = _with40.Rows[0]["CodeTVA"] + "";

                        if (!string.IsNullOrEmpty(sTypeTvaXX))
                        {
                            strTRouTP = sTypeTvaXX;
                        }
                        else
                        {
                            strTRouTP = _with40.Rows[0]["CodeTVA"] + "";
                        }

                        sTypeTvaXX = "";
                        //modif rachid 310702
                        // If Not IsNull(!CodeReglement_IMM) And !CodeReglement_IMM <> "" Then
                        //     txtMode = !CodeReglement_IMM & ""
                        // End If
                        //== modif du 07 SEPT 2007 le code du reglement
                        //== se situe sur la fiche gérant.

                        if (!string.IsNullOrEmpty(_with40.Rows[0]["CodeReglement"].ToString()))//tested
                        {
                            txtMode.Text = _with40.Rows[0]["CodeReglement"].ToString() + "";
                        }
                        else
                        {
                            txtMode.Text = General.sCodeReglement;
                        }
                        fc_ModeRegl(false);

                        txtRefClient.Text = _with40.Rows[0]["RefClient"] + "";
                        using (var tmp = new ModAdo())
                            txtModeLibelle.Text = tmp.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement" + " where Code='" + txtMode.Text + "'");

                        txtVIR_Code.Text = _with40.Rows[0]["VIR_Code"] + "";
                        if (string.IsNullOrEmpty(strTRouTP))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                        }
                        txtTVA.Text = strTRouTP;
                    }
                    else
                    {
                        fc_ClearEntete();
                    }


                }
                else
                {

                    General.sSQL = "SELECT   ImmeublePart.Nom,Immeuble.CodeImmeuble,ImmeublePart.NCompte as NC, CodeParticulier, ImmeublePart.Adresse as adresseTAB,"
                            + " ImmeublePart.CodePostal as [TABCodePostal], ImmeublePart.ville as [TABVille], Immeuble.Adresse ,"
                            + " Immeuble.CodePostal as [IMMCodePostal], Immeuble.Ville as [IMMVille],Immeuble.CompteDivers,"
                            + " Immeuble.CommentaireFacture1," + " Immeuble.CommentaireFacture2,Immeuble.CodeTVA " + " FROM ImmeublePart INNER JOIN"
                            + " Immeuble ON ImmeublePart.CodeImmeuble = Immeuble.CodeImmeuble" + " WHERE Immeuble.CodeImmeuble = '"
                            + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'" + " AND ImmeublePart.CodeParticulier = '" + StdSQLchaine.gFr_DoublerQuote(sCodeParticulier) + "'";
                    // adotemp.Close
                    var tmp = new ModAdo();

                    adotemp = tmp.fc_OpenRecordSet(General.sSQL);
                    var _with41 = adotemp;
                    if (adotemp.Rows.Count > 0)
                    {

                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        if (General.sAdresseEnvoie == "1")
                        {
                            txt_0.Text = _with41.Rows[0]["Nom"] + "";
                            txt_1.Text = _with41.Rows[0]["adresseTAB"] + "";
                            txt_2.Text = "";
                            txt_3.Text = "";
                            txt_4.Text = "";
                            txt_5.Text = _with41.Rows[0]["TABCodePostal"] + "";
                            txt_6.Text = _with41.Rows[0]["TABVille"] + "";
                        }
                        //===> Fin Modif Mondir

                        txtGerNom.Text = _with41.Rows[0]["Nom"] + "";
                        txtGerAdresse1.Text = _with41.Rows[0]["adresseTAB"] + "";
                        txtGerAdresse2.Text = "";
                        txtGerAdresse3.Text = "";
                        txtGerCP.Text = _with41.Rows[0]["TABCodePostal"] + "";
                        txtGerVille.Text = _with41.Rows[0]["TABVille"] + "";

                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        txt_7.Text = "";
                        txt_8.Text = "";
                        txt_9.Text = "1";
                        //===> Fin Modif Mondir

                        txtImmAdresse1.Text = _with41.Rows[0]["Adresse"] + "";
                        txtCodeimmeuble.Text = _with41.Rows[0]["CodeImmeuble"] + "";
                        txtImmCP.Text = _with41.Rows[0]["IMMCodePostal"] + "";
                        txtImmVille.Text = _with41.Rows[0]["IMMVille"] + "";
                        //txtCommentaire1 = !CommentaireFacture1 & ""
                        //txtCommentaire2 = !CommentaireFacture2 & ""
                        txtNCompte.Text = _with41.Rows[0]["NC"] + "";

                        if (General.Left(General.UCase(txtNCompte.Text), 1) == General.UCase("d"))
                        {
                            optParticulier.Checked = true;
                        }
                        else
                        {
                            optGerant.Checked = true;
                        }
                        strTRouTP = _with41.Rows[0]["CodeTVA"] + "";
                        //modif rachid 310702
                        if (string.IsNullOrEmpty(strTRouTP))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de code TVA." + "\n" + "Aller dans le programme immeuble pour résoudre ce probléme.");
                        }
                        txtTVA.Text = strTRouTP;
                        txtMode.Text = General.sCodeReglement;

                        fc_ModeRegl(true);


                        txtModeLibelle.Text = tmp.fc_ADOlibelle("SELECT CodeReglement.Libelle  FROM CodeReglement" + " where Code='" + txtMode.Text + "'");
                    }
                    else
                    {
                        fc_ClearEntete();
                    }




                }
                adotemp.Dispose();

                General.rstmp = null;

                return;
            }
            catch (Exception ex) { Erreurs.gFr_debug(ex, this.Name + ";fournisEntete"); }


        }
        private void fc_ClearEntete()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ClearEntete() - Enter In The Function");
            //===> Fin Modif Mondir

            txtGerNom.Text = "";
            txtGerAdresse1.Text = "";
            txtGerAdresse2.Text = "";
            txtGerAdresse3.Text = "";
            txtGerCP.Text = "";
            txtGerVille.Text = "";
            txtImmAdresse1.Text = "";
            txtCodeimmeuble.Text = "";
            txtImmCP.Text = "";
            txtImmVille.Text = "";
            txtCommentaire1.Text = "";
            txtCommentaire2.Text = "";
            txtNCompte.Text = "";
            strTRouTP = "";
            //voir déclaration variable dans général
            txtTVA.Text = "";
            //txtMode = ""
            //txtModeLibelle = ""
            cmbService.Text = "";
            lblLibService.Text = "";
            txtRefClient.Text = "";

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            txt_0.Text = "";
            txt_1.Text = "";
            txt_2.Text = "";
            txt_3.Text = "";
            txt_4.Text = "";
            txt_5.Text = "";
            txt_6.Text = "";
            txt_7.Text = "";
            txt_8.Text = "";
            txt_9.Text = "";
            //===> Fin Modif Mondir
        }
        private void insertIntervInCorps()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"insertIntervInCorps() - Enter In The Function");
            //===> Fin Modif Mondir

            double dblPrixVente = 0;
            DataTable rsPrixVente = default(DataTable);
            DataTable rsDevis = default(DataTable);
            string sNSFamille = null;
            string sTexteLigne = null;
            double dblPrixVenteTP = 0;
            double dblPrixVenteTR = 0;
            double dblPrixPoste = 0;
            double dblCharges = 0;
            var tmp = new ModAdo();
            try
            {
                if (General.UCase(General.fncUserName()) == General.UCase("rachid abbouchi") || General.sEvolution == "1")
                {
                    //'=== lDefOuAcompteOuGarantie est à 1 => Facture interv accompte
                    //'=== lDefOuAcompteOuGarantie est 2 => Facture interv definitive.
                    //'=== lDefOuAcompteOuGarantie est => Facture devis de garantie.

                    if (optIntervention.Checked)
                    {
                        fc_InsertCorpTravauxV2(txtTVA.Text, Variable.lDefOuAcompteOuGarantie);//tested
                    }
                    else if (optDevis.Checked)
                    {
                        fc_InsertCorpDevisV2(txtTVA.Text, Variable.lDefOuAcompteOuGarantie);//tested
                    }
                    Variable.lDefOuAcompteOuGarantie = 0;
                    return;
                }

                //   SSOleDBGrid1.Redraw = false;
                //----procedure pour inserer lesinterventions sélectionées dans le corps de l'état.

                SSTab1.SelectedTab = SSTab1.Tabs[0];
                txtCharges.Text = "0";
                var _with42 = SSOleDBGrid3;

                // _with42.MoveFirst();

                strIntervention = "";
                SQL = "";

                var dt = SSOleDBGrid1.DataSource as DataTable;
                var NewRow = dt.NewRow();


                SSOleDBGrid1.EventManager.AllEventsEnabled = false;

                for (k = 0; k <= _with42.Rows.Count - 1; k++)
                {

                    //----içi dbdate1 ne contient pas de date

                    // dbdate1 = _with42.GetBookmark(k);
                    //----récupére seulement les enregistrements séléctionnés.

                    if (_with42.Rows[k].Cells["Choix"].Value.ToString() == "Oui")
                    {
                        NewRow = dt.NewRow();

                        var _with43 = SSOleDBGrid1;
                        //Designation
                        NewRow["Designation"] = "Intervention du " + SSOleDBGrid3.Rows[k].Cells["DateRealise"].Value.ToString();
                        dt.Rows.Add(NewRow);

                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["CodeArticle"].Value.ToString()))
                        {
                            strIntervention = SSOleDBGrid3.Rows[k].Cells["Designation1"].Value.ToString();
                            boolOnlyArticle = true;
                            boolAddrow = true;
                            strCodeArticle = SSOleDBGrid3.Rows[k].Cells["CodeArticle"].Value.ToString();
                            boolInsertionArticle = true;
                            BisRechercheArticle();
                            boolInsertionArticle = false;
                            boolOnlyArticle = false;
                            boolAddrow = false;
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["commentaire"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["commentaire"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["commentaire2"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["commentaire2"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["commentaire3"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["commentaire3"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["INT_Rapport1"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["INT_Rapport1"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["INT_Rapport2"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["INT_Rapport2"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["INT_Rapport3"].Value.ToString()))
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = SSOleDBGrid3.Rows[k].Cells["INT_Rapport3"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }
                        ////-----si un le champs bondecommande dans SSOleDBGrid3 est a true on recherche tous les bons
                        ////-----de commandes associés a cette intervention.

                        if (optDevis.Checked == false && !optIntervention.Checked)
                        {
                            if (SSOleDBGrid3.Rows[k].Cells["Achat"].Value != DBNull.Value
                                && SSOleDBGrid3.Rows[k].Cells["Achat"].Value.ToString() == "1")
                            {
                                adotemp = new DataTable();

                                adotemp = tmp.fc_OpenRecordSet("SELECT BonDeCommande.ProduitTotalHT, BonDeCommande.NoBonDeCommande"
                                    + " From BonDeCommande " + " WHERE BonDeCommande.NoIntervention="
                                    + SSOleDBGrid3.Rows[k].Cells["NoIntervention"].Value.ToString());

                                if (adotemp.Rows.Count > 0)
                                {
                                    foreach (DataRow r in adotemp.Rows)
                                    {
                                        if (!string.IsNullOrEmpty(r["ProduitTotalHT"].ToString()) && r["ProduitTotalHT"].ToString() != "0")
                                        {
                                            NewRow = dt.NewRow();
                                            NewRow["Designation"] = "Pour fourniture au montant de: " + r["ProduitTotalHT"] + "";
                                            dt.Rows.Add(NewRow);
                                            SSOleDBGrid1.UpdateData();
                                            //var xx = SSOleDBGrid1ModAdo.Update();
                                        }
                                        else
                                        {
                                            NewRow = dt.NewRow();
                                            NewRow["Designation"] = "ERREUR: le bon n°" + r["NoBonDeCommande"] + " n'a pas de total HT";
                                            dt.Rows.Add(NewRow);
                                            SSOleDBGrid1.UpdateData();
                                            // var xx = SSOleDBGrid1ModAdo.Update();
                                        }
                                    }
                                }
                            }
                        }
                        else if (optIntervention.Checked)
                        {
                            if (SSOleDBGrid3.Rows[k].Cells["Achat"].Value != DBNull.Value
                                && (SSOleDBGrid3.Rows[k].Cells["Achat"].Value).ToString() == "1")
                            {

                                adotemp = new DataTable();

                                adotemp = tmp.fc_OpenRecordSet("SELECT BCD_Detail.BCD_Designation, BCD_Detail.BCD_Quantite, "
                                    + " BCD_Detail.BCD_PrixHT From BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande "
                                    + " WHERE BonDeCommande.NoIntervention=" + SSOleDBGrid3.Rows[k].Cells["NoIntervention"].Value
                                    + " AND BCD_Detail.BCD_Quantite<>'' AND NOT BCD_Quantite IS NULL AND ISNUMERIC(BCD_Quantite)=1");
                                if (adotemp.Rows.Count > 0)
                                {
                                    foreach (DataRow r in adotemp.Rows)
                                    {
                                        if (Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0")) > 0)
                                        {
                                            if (rsPrixVente == null)
                                            {
                                                rsPrixVente = new DataTable();

                                            }
                                            if (rsPrixVente != null)
                                            {
                                                rsPrixVente.Dispose();
                                            }

                                            rsPrixVente = tmp.fc_OpenRecordSet("SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente WHERE PalierAchat>="
                                                + Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0"))
                                                + " ORDER BY PalierAchat ASC");

                                            if (rsPrixVente.Rows.Count > 0)
                                            {
                                                //rsPrixVente.MoveFirst();
                                                if (rsPrixVente.Rows[0]["AfficherFormule"].ToString() != "0")
                                                {
                                                    NewRow = dt.NewRow();
                                                    NewRow["Designation"] = "Formule utilisée : (" + rsPrixVente.Rows[0]["a"].ToString()
                                                        + "*" + Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                         * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")) + ") + " + rsPrixVente.Rows[0]["b"].ToString();
                                                    dt.Rows.Add(NewRow);
                                                }

                                                dblPrixVente = ((Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                    * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")))
                                                    * Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], 0)) + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], 0)));
                                            }
                                            else
                                            {
                                                rsPrixVente.Dispose();

                                                rsPrixVente = tmp.fc_OpenRecordSet("SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente WHERE PalierAchat<="
                                                    + Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                    * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0"))
                                                    + " ORDER BY PalierAchat DESC");

                                                if (rsPrixVente.Rows.Count > 0)
                                                {
                                                    //rsPrixVente.MoveFirst();
                                                    if (rsPrixVente.Rows[0]["AfficherFormule"].ToString() != "0")
                                                    {
                                                        // _with43.AddNew();

                                                        NewRow["Designation"] = "Formule utilisée : (" + rsPrixVente.Rows[0]["a"].ToString() + "*"
                                                            + Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                          * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")) + ") + " + rsPrixVente.Rows[0]["b"].ToString();

                                                        //  _with43.CtlUpdate();
                                                    }


                                                    dblPrixVente = ((Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                        * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")))
                                                        * Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], 0)) + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], 0)));
                                                }
                                                else
                                                {

                                                    dblPrixVente = Convert.ToDouble(General.nz(r["BCD_PrixHT"], "0"))
                                                        * Convert.ToDouble(General.nz(r["BCD_Quantite"], "0"));
                                                }
                                            }
                                            rsPrixVente.Dispose();
                                        }
                                        else
                                        {
                                            dblPrixVente = 0;
                                        }

                                        NewRow = dt.NewRow();
                                        NewRow["Article"] = General.sArticleAchat;
                                        NewRow["Designation"] = r["BCD_Designation"] + "";

                                        NewRow["Quantite"] = Convert.ToDouble(General.nz(r["BCD_Quantite"], "0"));

                                        if (Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")) != Convert.ToDouble("0"))
                                        {

                                            NewRow["PrixUnitaire"] = Convert.ToString(dblPrixVente / Convert.ToDouble(General.nz(r["BCD_Quantite"], "0")));
                                            //                                        Else
                                            //                                            .Columns("PrixUnitaire").value = CStr(dblPrixVente)
                                        }
                                        NewRow["MontantLigne"] = Convert.ToString(dblPrixVente);

                                        if (txtTVA.Text == "TP")
                                        {
                                            NewRow["TVA"] = sTauxTvaAchatTP;
                                            NewRow["Compte"] = sCompteTvaAchatTP;
                                            NewRow["Compte7"] = sCompte7AchatTP;
                                        }
                                        else if (txtTVA.Text == "TR")
                                        {
                                            NewRow["TVA"] = sTauxTvaAchatTR;
                                            NewRow["Compte"] = sCompteTvaAchatTR;
                                            NewRow["Compte7"] = sCompte7AchatTR;
                                        }

                                        NewRow["AnaActivite"] = tmp.fc_ADOlibelle("SELECT INT_AnaActivite FROM Intervention WHERE Intervention.NoIntervention="
                                            + SSOleDBGrid3.Rows[k].Cells["NoIntervention"].Value);

                                        dt.Rows.Add(NewRow);
                                        SSOleDBGrid1.UpdateData();
                                        //var xxx = SSOleDBGrid1ModAdo.Update();

                                    }
                                    if ((rsPrixVente != null))
                                    {
                                        rsPrixVente.Dispose();
                                        rsPrixVente = null;
                                    }
                                }
                            }
                            fc_CalcDebourseInterv(SSOleDBGrid3.Rows[k].Cells["Duree"].Value.ToString(), SSOleDBGrid3.Rows[k].Cells["HeureDebut"].Value.ToString(),
                                 SSOleDBGrid3.Rows[k].Cells["HeureFin"].Value.ToString(), SSOleDBGrid3.Rows[k].Cells["Deplacement"].Value.ToString(),
                               SSOleDBGrid3.Rows[k].Cells["Intervenant"].Value.ToString(), SSOleDBGrid3.Rows[k].Cells["DateRealise"].Value.ToString());

                        }
                        else if (optDevis.Checked == true)
                        {
                            if (chkSolderDevis.CheckState == CheckState.Checked)
                            {
                                NewRow = dt.NewRow();
                                NewRow["Designation"] = "Montant restant à facturer : " + Convert.ToString(Convert.ToDouble(General.nz(lblMontantDevis.Text.Replace("€", ""), "0"))
                                    - Convert.ToDouble(General.nz((lblTotalSituations.Text.Replace("€", "")), "0"))) + " €";
                                dt.Rows.Add(NewRow);
                                //SSOleDBGrid1ModAdo.Update();
                            }

                            rsDevis = new DataTable();

                            rsDevis = tmp.fc_OpenRecordSet("SELECT DevisDetail.TexteLigne,DevisDetail.TotalVenteApresCoef,DevisDetail.NsFamille,DevisDetail.CodeTVA "
                                + "FROM DevisDetail INNER JOIN GestionStandard ON DevisDetail.NumeroDevis=GestionStandard.NoDevis "
                                + "INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard WHERE Intervention.NoIntervention="
                                + General.nz(SSOleDBGrid3.Rows[k].Cells["NoIntervention"].Value.ToString(), "0") + " ORDER BY DevisDetail.NumeroLigne ");

                            if (rsDevis.Rows.Count > 0)
                            {
                                sNSFamille = rsDevis.Rows[0]["NSFamille"] + "";
                                sTexteLigne = rsDevis.Rows[0]["TexteLigne"] + "";
                                dblPrixVenteTP = 0;
                                dblPrixVenteTR = 0;
                                dblPrixPoste = 0;

                                foreach (DataRow dr in rsDevis.Rows)
                                {
                                    if (sNSFamille != dr["NSFamille"] + "")
                                    {
                                        if (dblPrixPoste != 0)
                                        {
                                            NewRow = dt.NewRow();
                                            NewRow["Designation"] = "\t" + sTexteLigne;
                                            dt.Rows.Add(NewRow);
                                            //SSOleDBGrid1ModAdo.Update();
                                        }
                                        dblPrixPoste = 0;
                                        sNSFamille = dr["NSFamille"] + "";
                                        sTexteLigne = dr["TexteLigne"] + "";
                                    }

                                    dblPrixPoste = dblPrixPoste + Convert.ToDouble(General.nz(dr["TotalVenteApresCoef"], "0"));

                                    if (dr["CodeTVA"] + "" == "5.5" || dr["CodeTVA"] + "" == "7" || dr["CodeTVA"] + "" == "10")
                                    {

                                        dblPrixVenteTR = dblPrixVenteTR + Convert.ToDouble(General.nz(dr["TotalVenteApresCoef"], "0"));
                                    }
                                    else if (dr["CodeTVA"] + "" == "19.6" || dr["CodeTVA"] + "" == "20")
                                    {

                                        dblPrixVenteTP = dblPrixVenteTP + Convert.ToDouble(General.nz(dr["TotalVenteApresCoef"], "0"));

                                    }
                                }

                                if (string.IsNullOrEmpty(General.sArticleDevis))
                                {
                                    boolArticleDevisCtl = true;
                                }

                                if (dblPrixVenteTR != 0)
                                {
                                    NewRow = dt.NewRow();
                                    NewRow["Article"] = General.sArticleDevis;
                                    NewRow["Designation"] = sLibelleArtDevis;
                                    NewRow["Quantite"] = "1";
                                    NewRow["PrixUnitaire"] = dblPrixVenteTR;
                                    NewRow["MontantLigne"] = dblPrixVenteTR;
                                    NewRow["TVA"] = sTauxTvaDevisTR;
                                    NewRow["Compte"] = sCompteTvaDevisTR;
                                    NewRow["Compte7"] = sCompte7DevisTR;
                                    NewRow["AnaActivite"] = sAnalDevis;
                                    dt.Rows.Add(NewRow);
                                    //SSOleDBGrid1ModAdo.Update();
                                }
                                if (dblPrixVenteTP != 0)
                                {
                                    NewRow = dt.NewRow();
                                    NewRow["Article"] = General.sArticleDevis;
                                    NewRow["Designation"] = sLibelleArtDevis;
                                    NewRow["Quantite"] = "1";
                                    NewRow["PrixUnitaire"] = dblPrixVenteTP;
                                    NewRow["MontantLigne"] = dblPrixVenteTP;
                                    NewRow["TVA"] = sTauxTvaDevisTP;
                                    NewRow["Compte"] = sCompteTvaDevisTP;
                                    NewRow["Compte7"] = sCompte7DevisTP;
                                    NewRow["AnaActivite"] = sAnalDevis;
                                    dt.Rows.Add(NewRow);
                                    //SSOleDBGrid1ModAdo.Update();
                                }
                                boolArticleDevisCtl = false;
                            }
                            rsDevis.Dispose();

                            rsDevis = null;
                        }

                        if (!string.IsNullOrEmpty(SSOleDBGrid3.Rows[k].Cells["acompte"].Value.ToString()) && SSOleDBGrid3.Rows[k].Cells["acompte"].Value.ToString() != "0")
                        {
                            NewRow = dt.NewRow();
                            NewRow["Designation"] = "Acompte à déduire: " + SSOleDBGrid3.Rows[k].Cells["acompte"].Value.ToString();
                            dt.Rows.Add(NewRow);
                        }

                        if (optDevis.Checked == true)
                        {
                            break;
                        }
                    }
                }

                SSOleDBGrid1.EventManager.AllEventsEnabled = true;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";insertIntervInCorps");
            }

        }

        private void fc_CalcDebourseInterv(string sDuree, string sHeureDebut, string sHeureFin, string sDeplacement, string sMat, string sDateRealise)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CalcDebourseInterv() - Enter In The Function - sDuree = {sDuree}, sHeureDebut = {sHeureDebut}, sHeureFin = {sHeureFin}, sDeplacement = {sDeplacement}," +
                                        $"sMat = {sMat}, sDateRealise = {sDateRealise}");
            //===> Fin Modif Mondir

            string sDureeDiff = null;
            bool blnDeplace = false;

            if (!General.IsNumeric(sDeplacement))
            {
                blnDeplace = false;
            }
            else
            {
                if (sDeplacement == "1")
                {
                    blnDeplace = true;
                }
                else
                {
                    blnDeplace = false;
                }
            }

            if (General.IsDate(sHeureDebut) && General.IsDate(sHeureFin))
            {
                sDureeDiff = General.fc_calcDuree(Convert.ToDateTime(sHeureDebut), Convert.ToDateTime(sHeureFin));
                if (General.IsDate(sDateRealise))
                {

                    txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0"))
                        + Convert.ToDouble(General.nz(General.fc_CalcDebourse(Convert.ToDateTime(sDureeDiff), sMat, Convert.ToDateTime(sDateRealise), blnDeplace), "0")));
                }
                else
                {

                    txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0"))
                        + Convert.ToDouble(General.nz(General.fc_CalcDebourse(Convert.ToDateTime(sDureeDiff), sMat, General.cDefDateRealise, blnDeplace), "0")));
                }
            }
            else if (General.IsDate(sDuree))
            {
                if (General.IsDate(sDateRealise))
                {

                    txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0"))
                        + Convert.ToDouble(General.nz(General.fc_CalcDebourse(Convert.ToDateTime(sDuree), sMat, Convert.ToDateTime(sDateRealise), blnDeplace), "0")));
                }
                else
                {
                    txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0"))
                        + Convert.ToDouble(General.nz(General.fc_CalcDebourse(Convert.ToDateTime(sDuree), sMat, General.cDefDateRealise, blnDeplace), "0")));
                }
            }

        }

        private void recherchearticle()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"recherchearticle() - Enter In The Function");
            //===> Fin Modif Mondir

            var _with44 = SSOleDBGrid1;
            adotemp = new DataTable();
            var tmpAdo = new ModAdo();
            try
            {
                //  adoTemp.Open "SELECT FacArticle.CodeArticle, FacArticle.Designation1,FacArticle.cg_Num, Tva.Code, Tva.Taux, Tva.Compte, FacArticle.ca_Num FROM Tva INNER JOIN FacArticle ON Tva.Code = FacArticle.CodeTVA where CodeArticle ='" & gFr_DoublerQuote(.Columns(3).Value) & "'", adocnn, adOpenForwardOnly, adLockReadOnly

                adotemp = tmpAdo.fc_OpenRecordSet("SELECT FacArticle.CodeArticle, FacArticle.Designation1,FacArticle.cg_Num, FacArticle.cg_Num2,  FacArticle.ca_Num, FacArticle.PrixAchat FROM  FacArticle where CodeArticle ='"
                    + StdSQLchaine.gFr_DoublerQuote(_with44.ActiveRow.Cells["Article"].Value.ToString()) + "'");

                if (adotemp.Rows.Count > 0)
                {
                    //test les champs cgnum et cgnum2 de la table article
                    //si ils sont égaux alors on recherchera les informations associé à ce compte
                    //sinon nous récupérons le codetva de l'immeuble stocké dans strTrouTP
                    //qui nous indiquera quelle compte recupéré cgnum ou cgnum2
                    adoPied = new DataTable();
                    var tmpAdoPied = new ModAdo();
                    strTRouTP = "";

                    if (!string.IsNullOrEmpty(txtCodeimmeuble.Text))
                    {

                        adoPied = tmpAdoPied.fc_OpenRecordSet("SELECT Immeuble.CodeTVA From Immeuble WHERE Immeuble.Codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'");
                        if (adoPied.Rows.Count > 0)
                        {
                            strTRouTP = adoPied.Rows[0]["CodeTVA"] + "";
                        }
                        adoPied.Dispose();

                        adoPied = null;
                    }
                    strCompte7 = "";

                    if (adotemp.Rows[0]["CG_NUM"].ToString().Trim() == adotemp.Rows[0]["CG_Num2"].ToString().Trim())
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"] + "";
                    }
                    else if (strTRouTP == "TR")
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"] + "";
                    }
                    else if (strTRouTP == "TP")
                    {
                        strCompte7 = adotemp.Rows[0]["CG_Num2"] + "";
                    }
                    if (!string.IsNullOrEmpty(strTRouTP) || !string.IsNullOrEmpty(strCompte7))
                    {
                        if (!string.IsNullOrEmpty(strCompte7))
                        {
                            // SSOleDBDropDown5.MoveFirst();
                            for (i2 = 0; i2 <= SSOleDBDropDown5.Rows.Count - 1; i2++)
                            {
                                if (strCompte7 == SSOleDBDropDown5.ActiveRow.Cells["Code comptable"].Value.ToString())
                                {

                                    _with44.ActiveRow.Cells["TVA"].Value = SSOleDBDropDown5.ActiveRow.Cells["TA_TAUX"].Value.ToString();
                                    //(3) taux

                                    _with44.ActiveRow.Cells["Compte"].Value = SSOleDBDropDown5.ActiveRow.Cells["CG_NUM"].Value;
                                    //(2) compte tva
                                    _with44.ActiveRow.Cells["Compte7"].Value = strCompte7;
                                    //classse7
                                    break;
                                }
                                else
                                {
                                    _with44.ActiveRow.Cells["TVA"].Value = "";
                                    //(3) taux
                                    _with44.ActiveRow.Cells["Compte"].Value = "";
                                    //(2) compte tva
                                    _with44.ActiveRow.Cells["Compte7"].Value = "";
                                    // compte7
                                }
                                //  SSOleDBDropDown5.MoveNext();
                            }
                        }
                    }
                    _with44.ActiveRow.Cells["TVA"].Value = adotemp.Rows[0]["PrixAchat"] + "";
                    _with44.ActiveRow.Cells["Compte"].Value = adotemp.Rows[0]["ca_num"] + "";
                    _with44.ActiveRow.Cells["Compte7"].Value = adotemp.Rows[0]["Designation1"] + "";
                    //.Columns(9).Value = !taux & ""
                    //.Columns(10).Value = !compte & ""
                    //.Columns(11).Value = adoTemp!CG_NUM & ""
                }
                else
                {
                    _with44.ActiveRow.Cells["Designation"].Value = "";
                    _with44.ActiveRow.Cells["TVA"].Value = "";
                    _with44.ActiveRow.Cells["Compte"].Value = "";
                    _with44.ActiveRow.Cells["Compte7"].Value = "";
                    _with44.ActiveRow.Cells["AnaActivite"].Value = "";
                }
                adotemp.Dispose();

                adotemp = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";recherchearticle");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rs"></param>
        private void VerifieFacture(ref DataTable rs)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"VerifieFacture() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                //----Procédure pour vérification des factures.
                bool boolMontanta0 = false;

                var _with45 = rs;
                boolErreur = false;
                boolCorps = false;
                boolPied = false;
                boolMontanta0 = false;
                doubMontantHT = 0;
                doubMontant = 0;
                i = 0;
                boolFlag = false;
                // supprimer le fichier si il exsite  
                if (File.Exists(General.cRAPPORT))
                    File.Delete(General.cRAPPORT);

                if (rs.Rows.Count > 0)
                {
                    //----En tete deu fichier des erreurs

                    File.AppendAllText(General.cRAPPORT, "Fichier des rejets des factures Manuelles -" + "\r\n");
                    File.AppendAllText(General.cRAPPORT, "Transfert du " + DateTime.Now + "\r\n");
                    File.AppendAllText(General.cRAPPORT, "\r\n");

                    sIntitulé = _with45.Rows[0]["NoFacture"].ToString();

                    // foreach (DataRow r in rs.Rows)
                    while (i <= rs.Rows.Count - 1)
                    {
                        //----si la facture n'a pas été vérifiée.

                        if (Convert.ToBoolean(General.nz(_with45.Rows[i]["verifiee"], false)) == false)
                        {
                            //----évite de boucler plusieurs fois le même enregistrement.
                            if (boolFlag == false)
                            {
                                boolFlag = true;
                                //----Verifie si éxiste un mode de réglement.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["ModeReglement"].ToString()))
                                {

                                    File.AppendAllText(General.cRAPPORT, "Il n'y a pas de mode de réglement dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----Verifie si existe un Code Immeuble.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["NomIntervention"].ToString()))//tested
                                {

                                    File.AppendAllText(General.cRAPPORT, "Il n'y a pas de code immeuble dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }
                                //----Verifie si existe un Code Immeuble.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["MontantTTc"].ToString()))//tested
                                {

                                    File.AppendAllText(General.cRAPPORT, "montant TTC à zero interdit dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----Vérifie si éxiste un compte Tiers.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["Ncompte"].ToString()))//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "Il n'y a pas de n° de compte tiers dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }


                                //----Verifie si existe une date de facture.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["DateFacture"].ToString()))
                                {
                                    File.AppendAllText(General.cRAPPORT, "Il n'y a pas de date de facture dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----Verifie si existe une date de facture.

                                if (_with45.Rows[i]["DateFacture"] != DBNull.Value && !string.IsNullOrEmpty(_with45.Rows[i]["DateFacture"].ToString()))//tested
                                {
                                    if (General.IsDate(_with45.Rows[i]["DateFacture"].ToString()))//tested
                                    {
                                        if (Convert.ToDateTime(_with45.Rows[i]["DateFacture"].ToString()) < General.GetDateDebutExercice())//tested
                                        {
                                            File.AppendAllText(General.cRAPPORT, "La date de facture dans la facture n°" + sIntitulé + " est inferieure à la date du début de l'exercice en cours" + "\r\n");
                                            File.AppendAllText(General.cRAPPORT, "  ==> la date de facture est : " + _with45.Rows[i]["DateFacture"] + " et doit être inferieure à " + General.GetDateDebutExercice() + "\r\n");
                                            boolErreur = true;
                                            boolErreurGeneral = true;
                                        }
                                    }
                                }

                                //----Vérifie si il éxiste un montant HT donc un pied de facture

                                if (Convert.ToDouble(General.nz(_with45.Rows[i]["MontantHT"], 0)) == 0)//tested
                                {
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                    boolMontanta0 = true;
                                    doubMontant = General.FncArrondir(Convert.ToDouble(General.nz(_with45.Rows[i]["MontantHT"], 0)));
                                    //----indique qu'il éxiste un pied de facture
                                    boolPied = true;
                                }
                                else if (General.IsNumeric(_with45.Rows[i]["MontantHT"].ToString()))
                                {
                                    //----indique qu'il éxiste un pied de facture
                                    boolPied = true;
                                    doubMontant = General.FncArrondir(Convert.ToDouble(General.nz(_with45.Rows[i]["MontantHT"], "0")), 2);
                                }
                            }

                            //----Vérifie si il éxiste au moins un montant en ligne.
                            if (_with45.Rows[i]["MontantLigne"] != DBNull.Value && (General.IsNumeric(_with45.Rows[i]["MontantLigne"].ToString()) || _with45.Rows[i]["MontantLigne"].ToString() == ""))//tested
                            {

                                //----boolCorps = true indique qu'il éxiste au moins un montant en ligne donc un corps de facture.
                                boolCorps = true;

                                //----compte tous les montants pour chaque facture.
                                if (string.IsNullOrEmpty(_with45.Rows[i]["MontantLigne"].ToString()))//tested
                                {
                                    doubMontantHT = General.FncArrondir(Convert.ToDouble(doubMontantHT) + 0, 2);
                                }
                                else
                                {
                                    doubMontantHT = General.FncArrondir(Convert.ToDouble(doubMontantHT) + Convert.ToDouble(_with45.Rows[i]["MontantLigne"].ToString()), 2);
                                }
                                //----vérifie si il éxiste un montant de TVA.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["TVA"].ToString()))//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "Il n'y a pas de taux de TVA dans la facture n°" + sIntitulé + " ligne n°" + _with45.Rows[i]["NumeroLigne"].ToString() + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----Vérifie si il existe un Compte de classe 7.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["Compte7"].ToString()))//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "il n'y a pas de compte de classe 7 dans le corps de facture n°" + sIntitulé + " ligne n°" + _with45.Rows[i]["NumeroLigne"].ToString() + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----vérifie si éxiste un compte de TVA associé.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["Compte"].ToString()))//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "il n'y a pas de compte de tva dans le corps de facture n°" + sIntitulé + " ligne n°" + _with45.Rows[i]["NumeroLigne"].ToString() + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----vérifie si éxiste une analytique activité.

                                if (string.IsNullOrEmpty(_with45.Rows[i]["AnaActivite"].ToString()) || _with45.Rows[i]["AnaActivite"] == DBNull.Value)//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "il n'y a pas d'analytique activité dans le corps de facture n°" + sIntitulé + " ligne n°" + _with45.Rows[i]["NumeroLigne"].ToString() + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }
                            }


                            //_with45.MoveNext();
                            bool SUITE = false;

                            //if (i == 0)
                            //{
                            //    boolCorps = false;
                            //    //goto SUITE;
                            //}
                            i++;
                            if (i == _with45.Rows.Count)
                            {
                                SUITE = true;
                            }
                            if (SUITE || _with45.Rows[i]["NoFacture"].ToString() != sIntitulé)
                            {
                                // SUITE:
                                //----si boolCorps = False dons pas de corps.
                                if (boolCorps == false)
                                {
                                    File.AppendAllText(General.cRAPPORT, "il n'y a pas de corps de facture dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----si boolPied = False donc pas de pied de facture.
                                if (boolPied == false)
                                {
                                    File.AppendAllText(General.cRAPPORT, "il n'y a pas de pied de facture dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }
                                else
                                {
                                    //----si il éxiste un pied de facture, on verifie les montantHT.
                                    if (doubMontant != doubMontantHT)//tested
                                    {
                                        File.AppendAllText(General.cRAPPORT, "le montant HT du pied n'est pas égale à la somme du corps dans la facture n°" + sIntitulé + "\r\n");
                                        boolErreur = true;
                                        boolErreurGeneral = true;
                                    }
                                }

                                //----si boolmontant = true donc le montantHT = 0
                                if (boolMontanta0 == true)//tested
                                {
                                    File.AppendAllText(General.cRAPPORT, "Le montant est à 0 dans la facture n°" + sIntitulé + "\r\n");
                                    boolErreur = true;
                                    boolErreurGeneral = true;
                                }

                                //----si boolErreur = False donc la facture est bonne.
                                if (boolErreur == false)
                                {
                                    General.Execute("UPDATE FactureManuelleEntete SET Verifiee=1 WHERE NoFacture='" + sIntitulé + "'" + "\r\n");

                                    //===> Mondir le 11.02.2021, je ne sais pas d'ou ça vient ?!!!
                                    for (int xx = 0; xx < _with45.Rows.Count; xx++)
                                    {
                                        if (_with45.Rows[xx]["NoFacture"]?.ToString() == sIntitulé?.ToUpper())
                                        {
                                            _with45.Rows[xx]["verifiee"] = true;
                                        }
                                    }
                                    //===> Fin Modif Mondir


                                    //if(SUITE)
                                    //    _with45.Rows[i - 1]["verifiee"] = true;
                                    //else
                                    //    _with45.Rows[i]["verifiee"] = true;
                                }

                                //----stocke le nouveau numéro de facture du recordset.
                                if (_with45.Rows.Count > 0 && !SUITE)
                                {
                                    sIntitulé = rs.Rows[i]["NoFacture"].ToString() + "";
                                }
                                boolFlag = false;
                                boolCorps = false;
                                boolPied = false;
                                boolErreur = false;
                                boolMontanta0 = false;
                                doubMontant = 0;
                                doubMontantHT = 0;
                            }
                        }
                        else
                        {

                            //----stocke le nouveau numéro de facture du recordset.
                            sIntitulé = rs.Rows[i]["NoFacture"].ToString() + "";
                            i++;
                            // _with45.MoveNext();
                        }
                    }
                    //----Compte les factures bonnes ou mauvaises
                    //  _with45.MoveFirst();
                    int countFac = rs.Rows.Count - 1;
                    sIntitulé = _with45.Rows[0]["NoFacture"].ToString();
                    i = 1;
                    j = 0;

                    if (Convert.ToBoolean(General.nz(rs.Rows[j]["verifiee"], false)))
                    {
                        j = 1;
                    }
                    foreach (DataRow dr in rs.Rows)
                    {

                        if (dr["NoFacture"].ToString() != sIntitulé)
                        {
                            if (dr["verifiee"] != DBNull.Value && Convert.ToBoolean(dr["verifiee"]) == true)
                            {
                                j = j + 1;
                            }
                            i = i + 1;
                            sIntitulé = dr["NoFacture"].ToString();
                        }

                        // _with45.MoveNext();
                    }

                    File.AppendAllText(General.cRAPPORT, "\r\n");
                    File.AppendAllText(General.cRAPPORT, "Nombre total de facture : " + i + "\r\n");
                    File.AppendAllText(General.cRAPPORT, "Facture Valide: " + j + "\r\n");
                    File.AppendAllText(General.cRAPPORT, "facture invalide: " + Convert.ToInt32(i - j) + "\r\n");


                    //If blnFic = True Then
                    //    Var = Shell("c:\program files\accessoires\wordpad.exe " & gstrCheminAscii & "ErrTransContrac.txt", 3)
                    // End If
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";VerifieFacture");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void ValidationEnTete()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"ValidationEnTete() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                adoManuel = new DataTable();
                var _with46 = adoManuel;
                var tmpAdoManuel = new ModAdo();
                _with46 = tmpAdoManuel.fc_OpenRecordSet("SELECT FactureManuelleEntete.* From FactureManuelleEntete Where FactureManuelleEntete.CleFactureManuelle =" + longCle);
                if (_with46.Rows.Count > 0)
                {
                    _with46.Rows[0]["NomIntervention"] = txtCodeimmeuble.Text;
                    _with46.Rows[0]["Objet1"] = txtObjet1.Text;
                    _with46.Rows[0]["objet2"] = txtObjet2.Text;
                    _with46.Rows[0]["client"] = txtGerNom.Text;
                    _with46.Rows[0]["ClientAdresse1"] = txtGerAdresse1.Text;
                    _with46.Rows[0]["clientAdresse2"] = txtGerAdresse2.Text;
                    //!clientAdresse3 = txtGerAdresse2
                    _with46.Rows[0]["ClientCP"] = txtGerCP.Text;
                    _with46.Rows[0]["ClientVille"] = txtGerVille.Text;
                    _with46.Rows[0]["adrIntervention1"] = txtImmAdresse1.Text;
                    _with46.Rows[0]["adrIntervention2"] = txtImmAdresse2.Text;
                    _with46.Rows[0]["adrIntervention3"] = txtimmAdresse3.Text;
                    _with46.Rows[0]["CPIntervention"] = txtImmCP.Text;
                    _with46.Rows[0]["VilleIntervention"] = txtImmVille.Text;
                    _with46.Rows[0]["ModeReglement"] = txtMode.Text;
                    _with46.Rows[0]["LibelleReglement"] = txtModeLibelle.Text;
                    _with46.Rows[0]["TypeReglement"] = txtTypeReglement.Text;
                    _with46.Rows[0]["DateFacture"] = txtDateDeFacture.Text;
                    _with46.Rows[0]["ServiceAnalytique"] = cmbService.Text;
                    _with46.Rows[0]["CommentaireFacture1"] = txtCommentaire1.Text;
                    _with46.Rows[0]["CommentaireFacture2"] = txtCommentaire2.Text;

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    _with46.Rows[0]["Adresse1Fact"] = txt_0.Text;
                    _with46.Rows[0]["Adresse2Fact"] = txt_1.Text;
                    _with46.Rows[0]["Adresse3Fact"] = txt_2.Text;
                    _with46.Rows[0]["Adresse4Fact"] = txt_3.Text;
                    _with46.Rows[0]["Adresse5Fact"] = txt_4.Text;
                    _with46.Rows[0]["CPFact"] = txt_5.Text;
                    _with46.Rows[0]["VilleFact"] = txt_6.Text;
                    _with46.Rows[0]["SIREN"] = txt_7.Text;
                    _with46.Rows[0]["TVAintra"] = txt_8.Text;
                    _with46.Rows[0]["AdresseAutre"] = General.nz(txt_9.Text, 0);
                    //===> Fin Modif Mondir

                    _with46.Rows[0]["VIR_Code"] = txtVIR_Code.Text;
                    _with46.Rows[0]["INT_AnaCode"] = txtINT_AnaCode.Text;
                    if (Check1.CheckState == CheckState.Checked && optIntervention.Checked)
                    {
                        _with46.Rows[0]["Ncompte"] = "CPARTICULIERS";
                    }
                    else
                    {
                        _with46.Rows[0]["Ncompte"] = txtNCompte.Text;
                    }
                    if (string.IsNullOrEmpty(txtNoIntervention.Text))
                    {

                        _with46.Rows[0]["NoIntervention"] = 0;
                    }
                    else
                    {
                        _with46.Rows[0]["NoIntervention"] = txtNoIntervention.Text;
                    }

                    _with46.Rows[0]["CodeTVA"] = txtTVA.Text;
                    _with46.Rows[0]["ValiderPar"] = strNomPersonne + "-" + DateTime.Now;
                    //If Check1.Value = 1 Then
                    //    !affaire = "PARTICULIERS"
                    //Else
                    _with46.Rows[0]["Affaire"] = txtCodeimmeuble.Text;
                    //End If
                    if (txtDateDeFacture.Text != "" && General.IsDate(txtDateDeFacture.Text))
                    {
                        _with46.Rows[0]["DateEcheance"] = General.CalcEch(Convert.ToDateTime(txtDateDeFacture.Text), txtMode.Text);
                    }
                    txtDateEcheance.Text = _with46.Rows[0]["DateEcheance"] + "";

                    _with46.Rows[0]["FTM_Compta"] = chkFTM_Compta.CheckState;

                    _with46.Rows[0]["Definitive"] = General.nz(chkDefinitive.CheckState, 0);

                    _with46.Rows[0]["RefClient"] = txtRefClient.Text;
                }
                tmpAdoManuel.Update();

                _with46.Dispose();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ValidationEnTete");

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void ValidationCorps()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"ValidationCorps() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                SSOleDBGrid1.UpdateData();
                adoManuel = new DataTable();
                var tmpAdoManuel = new ModAdo();
                adoManuel = tmpAdoManuel.fc_OpenRecordSet("SELECT FactureManuelleDetail.* From FactureManuelleDetail" + " Where FactureManuelleDetail.CleFactureManuelle =" + longCle);

                //----si un corps éxiste déja alors on le suppime.
                var _with47 = adoManuel;
                if (_with47.Rows.Count > 0)
                {
                    foreach (DataRow rs in _with47.Rows)

                        rs.Delete();
                }

                //----Fournis la table du corps avec les nouveaux enregistrements.
                var _with48 = SSOleDBGrid1;
                // _with48.MoveFirst();

                for (i = 0; i <= SSOleDBGrid1.Rows.Count - 1; i++)
                {

                    // dbdate1 = SSOleDBGrid1.GetBookmark(i);
                    UltraGridRow row = _with48.Rows[i];

                    var r = _with47.NewRow();
                    r["CleFactureManuelle"] = longCle;
                    r["Poste"] = row.Cells["Poste"].Text;
                    r["Famille"] = row.Cells["Famille"].Text;
                    r["SSFamille"] = row.Cells["SSFamille"].Text;
                    r["Article"] = row.Cells["Article"].Text;
                    //r["Designation"] = General.Left(row.Cells["Designation"].Text, 60);
                    //'=== le champs designation est désormais limité(ntext)
                    r["Designation"] = row.Cells["Designation"].Text;

                    if (!string.IsNullOrEmpty(row.Cells["Quantite"].Text))
                    {
                        r["Quantite"] = row.Cells["Quantite"].Text;
                    }
                    else
                    {
                        r["Quantite"] = System.DBNull.Value;
                    }

                    r["Unite"] = row.Cells["Unite"].Text;

                    if (!string.IsNullOrEmpty(row.Cells["PrixUnitaire"].Text))
                    {
                        if (row.Cells["PrixUnitaire"].Text.Contains("€"))
                        {
                            string prixUnitaire = row.Cells["PrixUnitaire"].Text;
                            r["PrixUnitaire"] = prixUnitaire.Remove(prixUnitaire.Length - 1);
                        }
                        else
                        {
                            r["PrixUnitaire"] = row.Cells["PrixUnitaire"].Text;
                        }
                    }
                    else
                    {
                        r["PrixUnitaire"] = System.DBNull.Value;
                    }
                    if (!string.IsNullOrEmpty(row.Cells["MontantLigne"].Text))
                    {
                        if (row.Cells["MontantLigne"].Text.Contains("€"))
                        {
                            string montantLigne = row.Cells["MontantLigne"].Text;
                            r["MontantLigne"] = montantLigne.Remove(montantLigne.Length - 1);
                        }
                        else
                        {
                            r["MontantLigne"] = row.Cells["MontantLigne"].Text;
                        }

                    }
                    else
                    {
                        r["MontantLigne"] = System.DBNull.Value;
                    }
                    if (!string.IsNullOrEmpty(row.Cells["TVA"].Text))
                    {


                        r["TVA"] = row.Cells["TVA"].Text;

                    }
                    else
                    {
                        r["TVA"] = System.DBNull.Value;
                    }

                    r["Compte"] = row.Cells["Compte"].Text;
                    //Compte TVA
                    r["Compte7"] = row.Cells["Compte7"].Text;
                    //Compte de classe 7 (vente)
                    r["AnaActivite"] = row.Cells["AnaActivite"].Text;
                    r["NumeroLigne"] = i + 1;
                    adoManuel.Rows.Add(r.ItemArray);

                    var xxx = tmpAdoManuel.Update();
                }
                adoManuel.Dispose();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ValidationCorps");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void BisRechercheArticle()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"BisRechercheArticle() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                bool boolLastDesignation = false;
                bool bPrixAchat = false;
                bPrixAchat = false;

                adotemp = new DataTable();
                var tmp = new ModAdo();
                string sSQlArticle = null;

                sSQlArticle = "SELECT FacArticle.CodeArticle, FacArticle.Designation1,"
                    + " FacArticle.Designation2,FacArticle.Designation3,"
                    + " FacArticle.Designation4, FacArticle.Designation5,"
                    + " FacArticle.Designation6, FacArticle.Designation7,"
                    + " FacArticle.Designation8, FacArticle.Designation9,"
                    + " FacArticle.Designation10,FacArticle.cg_Num, FacArticle.cg_Num2,"
                    + " FacArticle.ca_Num, FacArticle.PrixAchat"
                    + " FROM  FacArticle where CodeArticle ='" + StdSQLchaine.gFr_DoublerQuote(strCodeArticle.Trim()) + "'";

                if (General.UCase(TypeFacture) == General.UCase("CM"))
                {
                    sSQlArticle = sSQlArticle + " AND (FacArticle.CodeCategorieArticle='P'  OR FacArticle.CodeCategorieArticle='BT')";
                }
                else
                {
                    sSQlArticle = sSQlArticle + " AND (FacArticle.CodeCategorieArticle='T' OR FacArticle.CodeCategorieArticle='BT')";
                }

                adotemp = tmp.fc_OpenRecordSet(sSQlArticle);
                if (adotemp.Rows.Count == 0)
                {
                    adotemp = null;
                    return;
                    //  boolBoucle = False
                }
                // adotemp.Close
                //----Chaque article peut posséder jusqu'a dix designations,on boucle
                //----jusqu'a trouver une désignation non Null puis on les insére en
                //----sautant une ligne a chaque fois.
                j = 0;

                var dt = SSOleDBGrid1.DataSource as DataTable;

                var index = 0;
                if (SSOleDBGrid1.ActiveRow != null)
                    index = SSOleDBGrid1.ActiveRow.Index;
                for (i = 1; i <= 10; i++)//tested
                {
                    if (!string.IsNullOrEmpty(adotemp.Rows[0]["Designation" + i.ToString()].ToString()))//tested
                    {
                        j = j + 1;
                    }
                }
                //SSOleDBGrid1.Redraw = False
                //----si une seule désignation est trouvée :ajout du code article, prix achat etc...
                if (j == 1)//tested
                {
                    var newRow = dt.NewRow();

                    i = 1;
                    if (boolIntervention == true && boolInsertionArticle == true)
                    {
                        //  SQL = "!!;!!;!!;" _
                        //& "!" & adotemp!CodeArticle & "!;" _
                        //& "!" & strIntervention & "!;"
                        // modif 070802
                        // SQL = "!!;!!;!!;" + "!!;" + "!" + strIntervention + "!;";
                        newRow["Designation"] = strIntervention;
                    }
                    else//tested
                    {

                        // SQL = "!!;!!;!!;" + "!" + adotemp.Rows[0]["CodeArticle"] + "!;" + "!" + adotemp.Rows[0]["Designation"] + "!;";
                        newRow["Article"] = adotemp.Rows[0]["CodeArticle"];
                        newRow["Designation"] = adotemp.Rows[0]["Designation" + j.ToString()];
                    }

                    if (boolOnlyArticle == true)//tested
                    {
                        // SQL = SQL + "!!;";

                    }
                    else
                    {

                        newRow["Quantite"] = Convert.ToDouble(General.nz(adotemp.Rows[0]["FamilleArticleDetail.Quantite"], "0")) * Convert.ToDouble(General.nz(adotemp.Rows[0]["SousFamilleArticleDetail.Quantite"], "0"));
                        // SQL = SQL + "!" + Convert.ToDouble(adotemp.Rows[0]["FamilleArticleDetail.Quantite"]) * Convert.ToDouble(adotemp.Rows[0]["SousFamilleArticleDetail.Quantite"]) + "!;";
                    }

                    SQL = SQL + "!!;";

                    if (General.IsNumeric(adotemp.Rows[0]["PrixAchat"].ToString()))
                    {
                        // SQL = SQL + "!" + adotemp.Rows[0]["PrixAchat"] + "!;";

                        newRow["PrixUnitaire"] = adotemp.Rows[0]["PrixAchat"];
                        bPrixAchat = true;
                    }
                    else//tested
                    {
                        bPrixAchat = false;
                        //SQL = SQL + "!!;";
                    }

                    //== modif rachid, n'affiche pas les comptes si aucun prix d'achat( remodifier)
                    // If bPrixAchat = True Then
                    if (TestTauxTVA(ref newRow) == true)
                    {
                        // SQL = SQL + "!" + fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString() + "") + "!;";
                        newRow["AnaActivite"] = adotemp.Rows[0]["ca_num"];
                    }
                    else
                    {
                        //SQL = SQL + "!!;!!;!!;!!;!" + fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString() + "") + "!;";
                        //Mondir 01.04.2020 : Added this condition to fix this bug https://groupe-dt.mantishub.io/view.php?id=1754
                        newRow["AnaActivite"] = fc_SelectCa_Num(adotemp.Rows[0]["ca_num"] != DBNull.Value ? adotemp.Rows[0]["ca_num"].ToString() : "");
                    }

                    //  ElseIf bPrixAchat = False Then
                    //       SQL = SQL & "!!;!!;!!;!!;!!;"

                    // End If

                    if (boolAddrow == true)
                    {
                        dt.Rows.Add(newRow);
                    }
                    else
                    {

                        dt.Rows.InsertAt(newRow, index);
                        index++;

                        //===> Mondir le 17.06.2021 https://groupe-dt.mantishub.io/view.php?id=2213#c5532
                        if (activeRow != -1 && activeCol != -1 && activeRow <= SSOleDBGrid1.Rows.Count - 1)
                        {
                            SSOleDBGrid1.Rows[activeRow].Cells[activeCol].Activate();
                            SSOleDBGrid1.PerformAction(UltraGridAction.EnterEditMode);
                        }
                        //===> Fin Modif Mondir
                    }
                }
                else
                {
                    //----si plusieurs désignation sont trouvées
                    for (i = 1; i <= 10; i++)//TESTED
                    {

                        //---ajout du code article et de la désignation.
                        if (i == 1)//TESTED
                        {
                            var newRow = dt.NewRow();
                            if (boolIntervention == true && boolInsertionArticle == true)
                            {
                                //  SQL = "!!;!!;!!;" _
                                //& "!" & adotemp!CodeArticle & "!;" _
                                //& "!" & strIntervention & "!;"

                                // modif 070802
                                // SQL = "!!;!!;!!;" + "!" + adotemp.Rows[i]["CodeArticle"].ToString() + "!;" + "!" + strIntervention + "!;";
                                newRow["Article"] = adotemp.Rows[0]["CodeArticle"];
                                newRow["Designation"] = strIntervention;
                            }
                            else//TESTED
                            {
                                //SQL = "!!;!!;!!;" + "!" + adotemp.Rows[i]["CodeArticle"].ToString() + "!;" + "!" + adotemp.Fields("Designation" + i).Value + "!;";
                                newRow["Article"] = adotemp.Rows[0]["CodeArticle"];
                                newRow["Designation"] = adotemp.Rows[0]["Designation" + i.ToString()];
                            }



                            if (boolAddrow == true)
                            {
                                dt.Rows.Add(newRow);
                                //dt.Rows.Add(SQL);
                            }
                            else//TESTED
                            {
                                // SSOleDBGrid1.AddItem(SQL, SSOleDBGrid1.AddItemRowIndex(SSOleDBGrid1.Bookmark) + i - 1);

                                dt.Rows.InsertAt(newRow, index);
                                index++;
                            }
                        }
                        else if (i == j)//tested
                        {
                            var newRow = dt.NewRow();
                            // SQL = "!!;!!;!!;!!;" + "!" + adotemp.Fields("Designation" + i).Value + "!;";
                            newRow["Designation"] = adotemp.Rows[0]["Designation" + i.ToString()];
                            if (boolOnlyArticle == true)
                            {
                                //SQL = SQL + "!!;";
                            }
                            else
                            {
                                // SQL = SQL + "!" + adotemp.Fields("FamilleArticleDetail.Quantite").Value * adotemp.Fields("SousFamilleArticleDetail.Quantite").Value + "!;";
                                newRow["Quantite"] = Convert.ToDouble(General.nz(adotemp.Rows[0]["FamilleArticleDetail.Quantite"], "0")) * Convert.ToDouble(General.nz(adotemp.Rows[0]["SousFamilleArticleDetail.Quantite"], "0"));
                            }
                            SQL = SQL + "!!;";
                            if (General.IsNumeric(adotemp.Rows[0]["PrixAchat"].ToString()))//tested
                            {
                                newRow["PrixUnitaire"] = adotemp.Rows[0]["PrixAchat"];
                                // SQL = SQL + "!" + adotemp.Fields("PrixAchat").Value + "!;";
                            }
                            else
                            {
                                // SQL = SQL + "!!;";
                            }
                            if (TestTauxTVA(ref newRow) == true)//tested
                            {
                                newRow["AnaActivite"] = fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString());
                                // SQL = SQL + "!" + fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString() + "") + "!;";
                            }
                            else
                            {
                                newRow["AnaActivite"] = fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString());
                                // SQL = SQL + "!!;!!;!!;!!;!" + fc_SelectCa_Num(adotemp.Rows[0]["ca_num"].ToString() + "") + "!;";
                            }
                            boolLastDesignation = true;
                            if (boolAddrow == true)
                            {
                                dt.Rows.Add(newRow);
                                // SSOleDBGrid1.AddItem(SQL);
                            }
                            else
                            {

                                dt.Rows.InsertAt(newRow, index);
                                index++;
                                //  SSOleDBGrid1.AddItem(SQL, SSOleDBGrid1.AddItemRowIndex(SSOleDBGrid1.Bookmark) + i - 1);
                            }
                            break;
                        }
                        else//TESTED
                        {
                            //----si plusieurs désignation sont touvées on ajoute d'abord les prix achats etc... sans code article
                            // SQL = "!!;!!;!!;!!;";
                            // SQL = SQL + "!" + adotemp.Fields("Designation" + i).Value + "!;";
                            var newRow = dt.NewRow();
                            newRow["Designation"] = adotemp.Rows[0]["Designation" + i.ToString()];
                            if (boolAddrow == true)
                            {
                                dt.Rows.Add(newRow);
                                // SSOleDBGrid1.AddItem(SQL);
                            }
                            else//TESTED
                            {

                                dt.Rows.InsertAt(newRow, index);
                                index++;
                                // SSOleDBGrid1.AddItem(SQL, SSOleDBGrid1.AddItemRowIndex(SSOleDBGrid1.Bookmark) + i - 1);
                            }
                        }
                    }
                }
                adotemp.Dispose();
                //SSOleDBGrid1.Redraw = true;
                SSOleDBGrid1.UpdateData();
                adotemp = null;
                return;
            }


            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";BisRechercheArticle");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="ca_num"></param>
        /// <returns></returns>
        private string fc_SelectCa_Num(string ca_num)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_SelectCa_Num() - Enter In The Function - ca_num = {ca_num}");
            //===> Fin Modif Mondir

            string functionReturnValue = null;

            if (General.sAnalyTravauxAuto == "1")
            {

                if (optDevis.Checked == true)
                {
                    functionReturnValue = sAnalDevis;
                }
                else if (OptConMan.Checked == true)
                {
                    functionReturnValue = sAnalCont;
                }
                else if (optIntervention.Checked)
                {
                    functionReturnValue = sAnalTrav;
                }
                else
                {
                    functionReturnValue = ca_num;

                }


            }
            else
            {

                functionReturnValue = ca_num;
            }
            return functionReturnValue;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="newRow"></param>
        /// <returns></returns>
        private bool TestTauxTVA(ref DataRow newRow)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"TestTauxTVA() - Enter In The Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            try
            {

                //---test les champs cgnum et cgnum2 de la table article
                //---si ils sont égaux alors on recherchera les informations associé à ce compte
                //---sinon nous récupérons le codetva de l'immeuble stocké dans strTrouTP
                //---qui nous indiquera quelle compte à recupérer(cgnum ou cgnum2)
                //'on error GoTo Erreur
                var _with49 = adotemp;
                functionReturnValue = false;
                strCompte7 = "";
                if (General.sEvolution == "1")
                {
                    if (adotemp.Rows[0]["CG_NUM"].ToString().Trim() == adotemp.Rows[0]["CG_Num2"].ToString().Trim())//tested
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"].ToString() + "";
                    }
                    else if (General.UCase(txtTVA.Text) == General.UCase("TR"))//tested
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"].ToString() + "";
                    }
                    else if (General.UCase(txtTVA.Text) == General.UCase("TP"))
                    {
                        strCompte7 = adotemp.Rows[0]["CG_Num2"].ToString() + "";
                    }

                }
                else
                {
                    if (adotemp.Rows[0]["CG_NUM"].ToString().Trim() == adotemp.Rows[0]["CG_Num2"].ToString().Trim())//tested
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"].ToString() + "";
                    }
                    else if (strTRouTP == "TR")//tested
                    {
                        strCompte7 = adotemp.Rows[0]["CG_NUM"].ToString() + "";
                    }
                    else if (strTRouTP == "TP")
                    {
                        strCompte7 = adotemp.Rows[0]["CG_Num2"].ToString() + "";
                    }

                }

                if (!string.IsNullOrEmpty(strCompte7))//tested
                {

                    // SSOleDBDropDown5.MoveFirst();
                    for (i2 = 0; i2 <= SSOleDBDropDown5.Rows.Count - 1; i2++)//tested
                    {
                        if (strCompte7 == SSOleDBDropDown5.Rows[i2].Cells["Code comptable"].Value.ToString())
                        {
                            // SQL = SQL + "!!;" + "!" + SSOleDBDropDown5.Rows[i2].Cells["TA_TAUX"].Value + "!;" + "!" + SSOleDBDropDown5.Rows[i2].Cells["CG_NUM"].Value + "!;" + "!" + strCompte7 + "!;";
                            string tva = SSOleDBDropDown5.Rows[i2].Cells["Taux"].Value.ToString();

                            //===> Mondir le 18.09.2020, changed "#.#" to "F" to fix this https://groupe-dt.mantishub.io/view.php?id=1990
                            if (!string.IsNullOrEmpty(tva) && General.IsNumeric(tva))
                                tva = Convert.ToDouble(tva).ToString("F");
                            else
                                tva = "";

                            //Mondir 01.04.2020 : Added this condition to fix this bug https://groupe-dt.mantishub.io/view.php?id=1754
                            if (!string.IsNullOrEmpty(tva))
                                newRow["TVA"] = tva;

                            newRow["Compte"] = SSOleDBDropDown5.Rows[i2].Cells["Compte de TVA"].Value;
                            newRow["Compte7"] = strCompte7;
                            functionReturnValue = true;
                            break;
                        }

                        //  SSOleDBDropDown5.MoveNext();
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";TestTauxTVA");
                return functionReturnValue;
            }
        }

        private void SSOleDBGrid1_AfterRowInsert(object sender, RowEventArgs e)
        {
            //TODO : Mondir Delete for a test
            //boolInsertion = false;
        }

        private void GridContrat_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"GridContrat_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            GridContrat.EventManager.AllEventsEnabled = false;
            GridContrat.DisplayLayout.Bands[0].Columns["datefin"].Hidden = true;
            GridContrat.DisplayLayout.Bands[0].Columns["Resiliee"].Hidden = true;
            GridContrat.DisplayLayout.Bands[0].Columns["Prestation"].Width = 200;
            GridContrat.DisplayLayout.Bands[0].Columns["Intitulé"].Width = 200;
            GridContrat.EventManager.AllEventsEnabled = true;
        }

        private void SSOleDBGrid3_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid3_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            SSOleDBGrid3.EventManager.AllEventsEnabled = false;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["acompte"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Numfichestandard"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Document"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Choix"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["HeureDebut"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["HeureFin"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Duree"].Hidden = true;

            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["INT_Rapport1"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["INT_Rapport2"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["INT_Rapport3"].Hidden = true;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Rapport vocal"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Rapport vocal"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Rapport vocal"].CellButtonAppearance.Image = Properties.Resources.volume_up_4_16;

            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Achat"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Achat"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Achat"].CellButtonAppearance.Image = Properties.Resources.square_outline_16;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Achat"].Width = 150;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Rapport vocal"].Width = 450;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Designation1"].Width = 450;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Commentaire"].Width = 450;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Commentaire2"].Width = 450;

            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Fichier"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Fichier"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            SSOleDBGrid3.DisplayLayout.Bands[0].Columns["Fichier"].CellButtonAppearance.Image = Properties.Resources.square_outline_16;
            SSOleDBGrid3.EventManager.AllEventsEnabled = true;
        }

        private void optPersonne_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optPersonne_VisibleChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (optPersonne.Checked)
            {
                Combo1.Enabled = false;
                label21.Enabled = false;
                lblNom.Enabled = true;
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {

        }

        private void cmdAvoir_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAvoir_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sNofacture = "";
            string[] sTab = null;
            try
            {
                DialogResult dr = CustomMessageBox.Show("Désirez-vous créer un avoir pour la facture " + txtEnCours.Text + ".", "Avoir", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dr == DialogResult.No)
                    return;
                sNofacture = modAvoir.fc_AJoutAvoir(txtEnCours.Text);

                if (sNofacture != "")
                {
                    //Modif 17-07-2020 ajouté par salma
                    sTab = sNofacture.Split(';');
                    SQLIntervention = "WHERE (intervention.NoIntervention = " + sTab[1] + ")";
                    //fc_load(sNofacture, false);
                    fc_load(sTab[0], false);
                    cmdValider_Click(cmdValider, new System.EventArgs());
                    //Fin Modif 17-07-2020
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAvoir_Click");
            }
        }

        public void fc_load(string sNofacture, bool bMessage = true)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_load() - Enter In The Function - sNofacture = {sNofacture}, bMessage = {bMessage}");
            //===> Fin Modif Mondir

            try
            {
                SablierOnOff(true);
                InitialiseEcran(this);
                adoManuel = new DataTable();
                ModAdo modAdoManuel = new ModAdo();
                adoManuel = modAdoManuel.fc_OpenRecordSet("select * from FactureManuelleEntete where NoFacture = '" + sNofacture + "'");
                if (adoManuel.Rows.Count > 0)
                {
                    var row = adoManuel.Rows[0];

                    //===> Mondir le 30.03.2021, https://groupe-dt.mantishub.io/view.php?id=2243
                    using (var tmpModAdo = new ModAdo())
                    {
                        var dt = tmpModAdo.fc_OpenRecordSet($"SELECT P3, ChaudCondensation FROM Immeuble WHERE CodeImmeuble = '{row["Affaire"]}'");
                        if (dt.Rows.Count > 0)
                        {
                            if (dt.Rows[0]["P3"]?.ToString() == "True")
                            {
                                lblP3.Visible = true;
                            }
                            else
                            {
                                lblP3.Visible = false;
                            }

                            if (dt.Rows[0]["ChaudCondensation"]?.ToString() == "1")
                            {
                                lblChaudCondensation.Visible = true;
                            }
                            else
                            {
                                lblChaudCondensation.Visible = false;
                            }
                        }
                    }
                    //===> Fin Modif Mondir

                    txtEnCours.Text = row["NoFacture"] + "";
                    longCle = row["CleFactureManuelle"] != DBNull.Value && row["CleFactureManuelle"] + "" != "" ? Convert.ToInt32(row["CleFactureManuelle"] + "") : 0;
                    txtCodeimmeuble.Text = row["NomIntervention"] + "";
                    txtObjet1.Text = row["Objet1"] + "";
                    txtObjet2.Text = row["Objet2"] + "";
                    txtGerNom.Text = row["Client"] + "";
                    txtCommentaire1.Text = row["CommentaireFacture1"] + "";
                    txtCommentaire2.Text = row["CommentaireFacture2"] + "";

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                    txt_0.Text = row["Adresse1Fact"] + "";
                    txt_1.Text = row["Adresse2Fact"] + "";
                    txt_2.Text = row["Adresse3Fact"] + "";
                    txt_3.Text = row["Adresse4Fact"] + "";
                    txt_4.Text = row["Adresse5Fact"] + "";
                    txt_5.Text = row["CPFact"] + "";
                    txt_6.Text = row["VilleFact"] + "";
                    txt_7.Text = row["SIREN"] + "";
                    txt_8.Text = row["TVAintra"] + "";
                    txt_9.Text = row["AdresseAutre"] + "";
                    //===> Fin Modif Mondir

                    txtGerAdresse1.Text = row["ClientAdresse1"] + "";
                    txtGerAdresse2.Text = row["ClientAdresse2"] + "";
                    txtGerAdresse2.Text = row["ClientAdresse3"] + "";
                    txtGerCP.Text = row["ClientCP"] + "";
                    txtGerVille.Text = row["ClientVille"] + "";
                    txtImmAdresse1.Text = row["AdrIntervention1"] + "";
                    txtImmAdresse2.Text = row["AdrIntervention2"] + "";
                    txtimmAdresse3.Text = row["AdrIntervention3"] + "";
                    txtImmCP.Text = row["CpIntervention"] + "";
                    txtImmVille.Text = row["VilleIntervention"] + "";
                    txtMode.Text = row["ModeReglement"] + "";
                    txtModeLibelle.Text = row["LibelleReglement"] + "";
                    txtTypeReglement.Text = row["typeReglement"] + "";
                    txtDateDeFacture.Text = row["DateFacture"] + "";
                    cmbService.Text = row["ServiceAnalytique"] + "";
                    fc_LibService();
                    chkDefinitive.Checked = General.nz(row["Definitive"], 0) + "" == "0" ? false : true;
                    blnChangeManu = false;//'indique que le chargement de txtDateFacture s'éffectue de manière automatique
                    txtNCompte.Text = row["Ncompte"] + "";
                    strComptabilisee = row["Comptabilisee"] + "";
                    strTRouTP = row["CodeTVA"] + "";
                    txtTVA.Text = strTRouTP;
                    txtNoIntervention.Text = row["NoIntervention"] + "";
                    txtVIR_Code.Text = row["VIR_Code"] + "";
                    txtINT_AnaCode.Text = row["INT_AnaCode"] + "";

                    if (row["Ncompte"] != DBNull.Value)
                    {
                        if (row["Ncompte"] + "" == "CPARTICULIERS" || row["Ncompte"] + "" == "PARTICULIERS")
                            Check1.Checked = true;
                        else
                            Check1.Checked = false;
                        if (General.Left(General.UCase(row["Ncompte"] + ""), 1) == General.UCase("d"))
                            optParticulier.Checked = true;
                        else
                            optGerant.Checked = true;
                    }
                    if (General.nz(row["RefClient"], "") + "" != "")
                    {
                        txtRefClient.Text = row["RefClient"] + "";
                    }

                }

                fc_ChargeContrat(txtCodeimmeuble.Text);
                if (optDevis.Checked == true)
                {
                    fc_ChargeInfosFacture(txtEnCours.Text);
                    //frmSynthese.Visible = true;
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    fraSynthese.Visible = true;
                    //===> Fin Modif Mondir
                }
                else if (optIntervention.Checked)
                {
                    fc_ChargeInfoFactInterv(txtEnCours.Text);
                    //frmSynthese.Visible = true;
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    fraSynthese.Visible = true;
                    //===> Fin Modif Mondir
                }
                else
                {
                    frmSynthese.Visible = false;
                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    fraSynthese.Visible = false;
                    //===> Fin Modif Mondir
                }

                //===> Mondir 08.09.2020, Ajout du LOG
                Program.SaveException(null, $"fc_load() - Enter In The Function - BUG Application Hang Apres clique sur Annulation - Ligne = 12105");
                //===> Fin Modif Mondir
                SSOleDBGrid1.DataSource = null;
                //===> Mondir 08.09.2020, Ajout du LOG
                Program.SaveException(null, $"fc_load() - Enter In The Function - BUG Application Hang Apres clique sur Annulation - Ligne = 12109");
                //===> Fin Modif Mondir
                FirstRemplitGrille();
                SSOleDBGrid2.DataSource = null;
                SecondRemplitGrille();
                cmdSupprimer.Enabled = true;
                cmdAjouter.Enabled = true;
                cmdValider.Enabled = true;
                cmdAnnuler.Enabled = false;
                cmdRechercheImmeuble.Enabled = true;
                retabliEcran(this);
                if (strComptabilisee == "1" || General.Left(txtEnCours.Text, 2) != "XX")
                {
                    EcranLecture(this);
                    cmdSupprimer.Enabled = false;
                    cmdValider.Enabled = false;
                }
                else
                {
                    cmdSupprimer.Enabled = true;
                    cmdValider.Enabled = true;
                }
                if (boolIntervention == true)
                {
                    SSTab1.Tabs[4].Visible = false;
                    boolinsertInterv = true;
                }
                if (bMessage == true)
                {
                    if (adoManuel.Rows.Count > 0 && strNomPersonne != adoManuel.Rows[0]["NomPersonne"] + "")
                    {
                        CustomMessageBox.Show("Attention, cette facture a été crée par " + adoManuel.Rows[0]["NomPersonne"], "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                SablierOnOff(false);
                if (SSTab1.SelectedTab.Index == 0 && SSOleDBGrid1.DisplayLayout.Bands[0].Columns[1].CellActivation == Activation.AllowEdit)
                    txtCodeimmeuble.Focus();
                else if (SSTab1.SelectedTab.Index == 1)
                    SSOleDBGrid1.Focus();
                else if (SSTab1.SelectedTab.Index == 2)
                    Option5.Focus();
                else
                    CmdRechercher.Focus();
                fc_LockCpt();
                //===> Modif de V31.08.2020 ajouté par Mondir le 31.08.2020
                fc_LoadInter();
                //===> Fin Modif Mondir
                modAdoManuel.Close();
                ModAdo.fc_CloseRecordset(adoManuel);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fload");
            }
        }

        private void SSOleDBGrid1_KeyDown(object sender, KeyEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_KeyDown() - Enter In The Function");
            //===> Fin Modif Mondir

            if (e.KeyCode == Keys.Left || e.KeyCode == Keys.Right || e.KeyCode == Keys.Tab)
                leftOrTab = true;
        }

        private void SSOleDBGrid2_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid2_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            SSOleDBGrid2.DisplayLayout.Bands[0].Columns["Designation"].Hidden = true;
            SSOleDBGrid2.DisplayLayout.Bands[0].Columns["CleFactureManuelle"].Hidden = true;

        }

        private void cmdMial_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMial_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            fc_Export(true);
        }

        private void cmdDevis_Click(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var sql = "SELECT CodeImmeuble as \"Immeuble\"," +
                  " NumeroDevis as \"No Devis\"," +
                  " TitreDevis AS \"Titre Devis\"," +
                  " DateCreation as \"Date creation\"," +
                  " NumFicheStandard as \"No appel\"," +
                  "  CodeEtat as \"Code Etat\"" +
                  " FROM  DevisEntete";
                var searchTemplate = new SearchTemplate(null, null, sql, "", " ORDER BY DevisEntete.DateCreation DESC") { Text = "Recherche des devis" };
                searchTemplate.SetValues(new Dictionary<string, string>() { { "CodeImmeuble", txtImmeuble.Text } });

                searchTemplate.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (ev.Row != null)
                    {
                        txtNoDevis.Text = ev.Row.Cells["No Devis"].Value + "";
                        txtImmeuble.Text = ev.Row.Cells["Immeuble"].Value + "";
                    }
                    searchTemplate.Close();
                };
                searchTemplate.ugResultat.KeyPress += (se, ev) =>
                {
                    if (((int)ev.KeyChar) == 13)
                    {
                        txtNoDevis.Text = searchTemplate.ugResultat.ActiveRow?.Cells["No Devis"].Value + "";
                        txtImmeuble.Text = searchTemplate.ugResultat.ActiveRow?.Cells["Immeuble"].Value + "";
                    }
                    searchTemplate.Close();
                };
                searchTemplate.ShowDialog();
                searchTemplate.Close();

                ///===============> Mondir 15-06-2020 : Added This Line to luch the search right after selecting the Devis Number, Requested By Fabiola !
                txtImmeuble_KeyPress(txtImmeuble, new KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";cmdRechNoDev_Click");
            }
        }
        private void fc_Export(bool bSendMail)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_Export() - Enter In The Function - bSendMail = {bSendMail}");
            //===> Fin Modif Mondir

            string strMiseAJour = null;
            string sReturn = null;
            string sCodeImmeuble = null;
            string sNumeroFact = null;
            string sRep = null;
            string sPathFile = null;
            string sSQL = null;
            string SQLTypeFacture = null;
            string sWhere = null;
            string sSelectionFormula = null;
            DataTable rs = default(DataTable);
            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            DataTable rsOS = default(DataTable);
            ModAdo rsOSModAdo = new ModAdo();
            //===> Fin Modif Mondir
            bool bExportV2 = false;
            string sSQLos;

            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
            bool bExportV3 = false;
            string sSQlMail = "";
            //===> Fin Modif Mondir

            bExportV2 = false;

            strMiseAJour = "";
            try
            {
                if (optPersonne.Checked == true)
                {

                    strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + strNomPersonne + "' and ";//
                }
                else if (OptNomChoisis.Checked == true)
                {

                    strMiseAJour = "{FactureManuelleEntete.NomPersonne} ='" + Combo1.Text + "' and ";
                }
                else if (OptTous.Checked == true)
                {

                    //---Ne fait rien.
                }

                //== critére sur numéro de facture.
                if (Option1.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtVisu1.Text) || !(General.IsNumeric(txtVisu1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisu1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtVisu2.Text) || !(General.IsNumeric(txtVisu2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisu2.Focus();
                        return;
                    }

                    strMiseAJour = "{FactureManuelleEntete.NumFacture} >=" + txtVisu1.Text + " and {FactureManuelleEntete.NumFacture} <=" + txtVisu2.Text
                        + "  AND {FactureManuelleEntete.TypeFacture} ='" + TypeFacture + "' and {FactureManuelleEntete.Facture} = true and " + " {FactureManuelleEntete.Imprimer} = true";

                    SQLTypeFacture = " WHERE FactureManuelleEntete.NumFacture >= " + txtVisu1.Text + " And FactureManuelleEntete.NumFacture<= "
                        + txtVisu2.Text + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";

                    //=== critére sur période.
                }
                else if (Option4.Checked == true)
                {

                    if (string.IsNullOrEmpty(txtVisuDate1.Text) || !(General.IsDate(txtVisuDate1.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisuDate1.Focus();
                        return;
                    }
                    else if (string.IsNullOrEmpty(txtVisuDate2.Text) || !(General.IsDate(txtVisuDate2.Text)))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        SablierOnOff(false);
                        txtVisuDate2.Focus();
                        return;
                    }

                    dbdate1 = Convert.ToDateTime(txtVisuDate1.Text).ToString("yyyy,MM,dd");

                    dbdate2 = Convert.ToDateTime(txtVisuDate2.Text).ToString("yyyy,MM,dd");


                    strMiseAJour = "{FactureManuelleEntete.datefacture}>= Date (" + dbdate1 + ") and "
                        + " {FactureManuelleEntete.dateFacture}<= Date (" + dbdate2 + ")  AND {FactureManuelleEntete.TypeFacture} ='"
                        + TypeFacture + "'" + " and {FactureManuelleEntete.Facture} = true and {FactureManuelleEntete.Imprimer} = true";

                    SQLTypeFacture = " WHERE FactureManuelleEntete.DateFacture >='" + Convert.ToDateTime(txtVisuDate1.Text).ToString("dd/MM/yyyy") + "'"
                        + " And  FactureManuelleEntete.DateFacture <='" + Convert.ToDateTime(txtVisuDate2.Text).ToString("dd/MM/yyyy") + "'"
                        + " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";

                }
                else if (OptFactEnCours.Checked)
                {
                    strMiseAJour = "{FactureManuelleEntete.NoFacture} ='" + txtEnCours.Text + "'"
                                    + " and {FactureManuelleEntete.Facture} = true and "
                                     + " {FactureManuelleEntete.Imprimer} = true";
                    SQLTypeFacture = " WHERE FactureManuelleEntete.NoFacture ='" + txtEnCours.Text + "'" +
                                    " AND FactureManuelleEntete.TypeFacture='" + TypeFacture + "'";
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //=== exporte l'état.
                //fc_ExportFactMan strMiseAJour, ETATFACTUREMANUELExport

                sSQL = "SELECT     CleFactureManuelle, NoFacture, Affaire, NumFacture, DateFacture " + " From FactureManuelleEnTete ";
                sWhere = SQLTypeFacture + " AND FactureManuelleEntete.Facture=1 and FactureManuelleEntete.Imprimer=1";

                sSQL = sSQL + sWhere;

                if (chkDuplicata.Checked)
                {
                    ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                    ModCrystalPDF.tabFormulas[0].sNom = "Duplicata";
                    ModCrystalPDF.tabFormulas[0].sType = ModCrystalPDF.cExportChaine;
                    ModCrystalPDF.tabFormulas[0].sCritere = "1";
                }
                else
                {
                    ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                    ModCrystalPDF.tabFormulas[0].sNom = "";
                    ModCrystalPDF.tabFormulas[0].sType = "";
                    ModCrystalPDF.tabFormulas[0].sCritere = "";
                }
                ModAdo ModAdors = new ModAdo();
                rs = ModAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)//tested
                {
                    foreach (DataRow rowRs in rs.Rows)
                    {
                        sSelectionFormula = "{FactureManuelleEntete.NumFacture} =" + rowRs["NumFacture"].ToString();

                        if (General.IsDate(rowRs["DateFacture"].ToString()))
                        {
                            //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                            if (Convert.ToDateTime(rowRs["DateFacture"].ToString()) >= General.dtdateFactManuV3)
                            {
                                sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExportV3, intNoFieldSelection: CheckState.Checked);
                                bExportV3 = true;
                            }
                            //===> Fin Modif Mondir
                            else if (Convert.ToDateTime(rowRs["DateFacture"].ToString()) >= General.dtdateFactManuV2)
                            {
                                // sReturn = devModeCrystal.fc_ExportCrystalSansFormule(sSelectionFormula, General.ETATFACTUREMANUELExportV2);
                                sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExportV2, intNoFieldSelection: CheckState.Checked);
                                //sReturn = fc_ExportCrystalSansFormule(sSelectionFormula, ETATFACTUREMANUELExportV2)
                                bExportV2 = true;
                            }
                            else//tested
                            {
                                sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExport, intNoFieldSelection: CheckState.Checked);
                                // sReturn = devModeCrystal.fc_ExportCrystalSansFormule(sSelectionFormula, General.ETATFACTUREMANUELExport);
                            }
                        }
                        else
                        {
                            //sReturn = devModeCrystal.fc_ExportCrystalSansFormule(sSelectionFormula, General.ETATFACTUREMANUELExport);
                            sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExport, intNoFieldSelection: CheckState.Checked);
                        }

                        sCodeImmeuble = rowRs["Affaire"].ToString().Trim() + "";
                        sNumeroFact = rowRs["NoFacture"].ToString().Replace("/", "-");
                        sRep = "\\Factures\\out";

                        sPathFile = General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact + ".pdf";

                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble);
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Factures ");
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + sRep);

                        if (Dossier.fc_ControleFichier(sPathFile) == true)
                        {
                            Dossier.fc_SupprimeFichier(sPathFile);
                        }
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        Dossier.fc_DeplaceFichier(sReturn, sPathFile);

                    }
                }

                rs = null;
                //=== affiche l'état.
                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (bExportV3 == true)
                {
                    sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExportV3, intNoFieldSelection: CheckState.Checked);
                }
                //===> Fin Modif Mondir
                else if (bExportV2 == true)
                {
                    //sReturn = devModeCrystal.fc_ExportCrystalSansFormule(strMiseAJour, General.ETATFACTUREMANUELExportV2);
                    sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExportV2, intNoFieldSelection: CheckState.Checked);
                }
                else//tested
                {
                    //sReturn = devModeCrystal.fc_ExportCrystalSansFormule(strMiseAJour, General.ETATFACTUREMANUELExport);
                    sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.ETATFACTUREMANUELExport, intNoFieldSelection: CheckState.Checked);
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //=== ajout de l'os à lexport.
                if (!bSendMail)
                {
                    if (chkOS.Checked)
                    {
                        sSQLos = " SELECT        GestionStandard.CheminOS "
                            + " FROM  FactureManuelleEnTete INNER JOIN "
                            + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture INNER JOIN "
                            + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                            + " WHERE        (FactureManuelleEnTete.NoFacture = '" +
                            txtEnCours.Text + "') "
                            + " AND (GestionStandard.FichierOSorigine = 2)";

                        //sSQLos = fc_ADOlibelle(sSQLos)
                        rsOS = rsOSModAdo.fc_OpenRecordSet(sSQLos);
                        foreach (DataRow rsOSRow in rsOS.Rows)
                        {
                            if (!string.IsNullOrEmpty(General.nz(rsOSRow["CheminOS"], "")?.ToString()))
                            {
                                if (rsOSRow["CheminOS"].ToString().FileExists())
                                    Process.Start(rsOSRow["CheminOS"]?.ToString());
                            }
                        }
                        rsOSModAdo.Dispose();
                    }

                    if (chkBonPourAccord.Checked)
                    {
                        sSQLos = " SELECT        GestionStandard.document "
                                 + " FROM  FactureManuelleEnTete INNER JOIN "
                                 + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture INNER JOIN "
                                 + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                                 + " WHERE        (FactureManuelleEnTete.NoFacture = '" + txtEnCours.Text + "')";

                        //sSQLos = fc_ADOlibelle(sSQLos)
                        rsOS = rsOSModAdo.fc_OpenRecordSet(sSQLos);
                        foreach (DataRow rsOSRow in rsOS.Rows)
                        {
                            if (!string.IsNullOrEmpty(General.nz(rsOSRow["Document"], "")?.ToString()))
                            {
                                if (rsOSRow["Document"].ToString().FileExists())
                                    Process.Start(rsOSRow["Document"]?.ToString());
                            }
                        }
                        rsOSModAdo.Dispose();
                    }
                }
                //===> Fin Modif Mondir


                if (!string.IsNullOrEmpty(sReturn))//tested
                {
                    if (bSendMail)
                    {
                        //'sSQL = "SELECT     Table1.eMail " _
                        //    & " FROM         Immeuble INNER JOIN " _
                        //    & " Table1 ON Immeuble.Code1 = Table1.Code1 WHERE     (Immeuble.CodeImmeuble = '" & gFr_DoublerQuote(txtCodeImmeuble) & "')"

                        //'                sSQL = fc_ADOlibelle(sSQL)
                        //'                MsgDestinataire = sSQL
                        //'                FichierAttache = sReturn
                        //'                MesCodeImmeuble = txtCodeImmeuble.Text
                        //'                MesEmplacement = sReturn

                        //'fc_Navigue Me, gsCheminPackage & "UserDocMail.vbd"

                        ModMain.bActivate = true;
                        var frmMail = new frmMail();
                        ModCourrier.MessageMail = "";
                        frmMail.txtMessage.Text = "";

                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021
                        frmMail.txtObjet.Text = txtCodeimmeuble.Text + " - Facture n°" + txtEnCours.Text;
                        sSQlMail = "SELECT        TOP (1) Mail From Immeuble WHERE   CodeImmeuble = '" +
                                   StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                        {
                            sSQlMail = tmpModAdo.fc_ADOlibelle(sSQL);
                        }
                        frmMail.SSOleDBcmbA.Text = sSQlMail;
                        //===> Fin Modif Mondir

                        ModCourrier.FichierAttache = sReturn;


                        if (chkOS.Checked)
                        {
                            sSQLos = " SELECT        GestionStandard.CheminOS "
                                              + " FROM  FactureManuelleEnTete INNER JOIN "
                                              + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture INNER JOIN "
                                              + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                                              + " WHERE        (FactureManuelleEnTete.NoFacture = '" + txtEnCours.Text + "')";

                            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                            //using (var ado1 = new ModAdo())
                            //    sSQLos = ado1.fc_ADOlibelle(sSQLos);
                            //if (sSQLos != "")
                            //{
                            //    ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + sSQLos;
                            //}
                            rsOS = rsOSModAdo.fc_OpenRecordSet(sSQLos);
                            foreach (DataRow rsOSRow in rsOS.Rows)
                            {
                                if (!string.IsNullOrEmpty(General.nz(rsOSRow["CheminOS"], "")?.ToString()))
                                {
                                    ModCourrier.FichierAttache =
                                        ModCourrier.FichierAttache + "\t" + rsOSRow["CheminOS"];
                                }
                            }
                            rsOSModAdo.Close();
                            //===> Fin Modif Mondir
                        }

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        if (chkBonPourAccord.Checked)
                        {
                            sSQLos = " SELECT        GestionStandard.document "
                                + " FROM  FactureManuelleEnTete INNER JOIN "
                                + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture INNER JOIN "
                                + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                                + " WHERE        (FactureManuelleEnTete.NoFacture = '" + txtEnCours.Text + "')";

                            //'sSQLos = fc_ADOlibelle(sSQLos)
                            //'If sSQLos <> "" Then
                            //'        FichierAttache = FichierAttache & vbTab & sSQLos
                            //'Else
                            //'        MsgBox "Aucun bon pour accord attaché à cette facture.", vbCritical, "Opération annulée"
                            //'End If

                            rsOS = rsOSModAdo.fc_OpenRecordSet(sSQLos);
                            foreach (DataRow rsOSRow in rsOS.Rows)
                            {
                                //===> Mondir le 03.12.2020, changé CheminOS par document
                                //if (!string.IsNullOrEmpty(General.nz(rsOSRow["CheminOS"], "")?.ToString()))
                                //===> Fin Modif Mondir
                                if (!string.IsNullOrEmpty(General.nz(rsOSRow["document"], "")?.ToString()))
                                {
                                    ModCourrier.FichierAttache =
                                        ModCourrier.FichierAttache + "\t" + rsOSRow["Document"];
                                }
                            }
                            rsOSModAdo.Close();
                        }
                        //===> Fin Modif Mondir

                        frmMail.txtDocuments.Text = ModCourrier.FichierAttache;
                        frmMail.txtDossierSauvegarde.Text = "";
                        sSQL = "SELECT CodeGestionnaire, code1 from Immeuble"
                                + " Where CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                        var ado = new ModAdo();
                        rs = ado.fc_OpenRecordSet(sSQL);
                        if (rs.Rows.Count > 0)
                        {
                            frmMail.SSOleDBcmbA.Text = ado.fc_ADOlibelle("SELECT Gestionnaire.Email_GES"
                                        + " FROM Gestionnaire "
                                        + " WHERE  code1 ='" + StdSQLchaine.gFr_DoublerQuote(rs.Rows[0]["code1"] + "") + "'"
                                        + " AND CodeGestinnaire='" + StdSQLchaine.gFr_DoublerQuote(rs.Rows[0]["CodeGestionnaire"] + "") + "'");
                        }
                        ado.Close();
                        rs = null;
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        General.bSignatureServFact = true;
                        //===> Fin Modif Mondir
                        frmMail.ShowDialog();
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        General.bSignatureServFact = false;
                        //===> Fin Modif Mondir
                        ModCourrier.FichierAttache = "";
                        frmMail.Close();
                    }
                    else
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        ModuleAPI.Ouvrir(sReturn);
                    }

                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdVisu_Click");
                if (ex.InnerException != null)
                    Program.SaveException(null, "error in crystal report function Fc_Export ===> details : " + ex.InnerException.Message);
            }
        }

        private void optDeselect_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optDeselect_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (optDeselect.Checked)
                {
                    foreach (var row in SSOleDBGrid3.Rows)
                    {
                        row.Appearance.BackColor = Color.White;
                        //row.Cells["Choix"].Value = false;//TODO 
                        row.Cells["Choix"].Value = "Non";//TODO 
                        row.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";optSelect_Click");
            }
        }

        private void optSelect_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"optSelect_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (optSelect.Checked)
                {
                    foreach (var row in SSOleDBGrid3.Rows)
                    {
                        row.Appearance.BackColor = Color.FromArgb(255, 255, 128);
                        //row.Cells["Choix"].Value = true;//TODO 
                        row.Cells["Choix"].Value = "Oui";
                        row.Update();
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";optSelect_Click");
            }
        }
        private void txtNoDevi_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                txtImmeuble_KeyPress(txtImmeuble, new KeyPressEventArgs((char)13));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sdate"></param>
        /// <param name="sHeureDeb"></param>
        /// <returns></returns>
        private string fc_GetArticleMO(string sdate, string sHeureDeb)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetArticleMO() - Enter In The Function - sdate = {sdate}, sHeureDeb = {sHeureDeb}");
            //===> Fin Modif Mondir

            TimeSpan dtTime = new TimeSpan();
            string functionReturnValue = null;
            if (General.IsDate(sdate))
            {
                if (Convert.ToDateTime(sdate).DayOfWeek == DayOfWeek.Sunday)
                {
                    functionReturnValue = cMOTPNUIT;
                    return functionReturnValue;
                }
                if (Convert.ToDateTime(sdate).DayOfWeek == DayOfWeek.Saturday && dtTime > new TimeSpan(20, 00, 00))
                {
                    functionReturnValue = cMOTPNUIT;
                    return functionReturnValue;
                }
                else
                {
                    functionReturnValue = cMOTPS;

                }
            }
            if (General.IsDate(sHeureDeb))
            {
                dtTime = Convert.ToDateTime(sHeureDeb).TimeOfDay;

                if (dtTime < new TimeSpan(06, 00, 00))//am
                {
                    functionReturnValue = cMOTPNUIT;
                    return functionReturnValue;
                }
                if (dtTime > new TimeSpan(20, 00, 00))//pm
                {
                    functionReturnValue = cMOTPNUIT;
                    return functionReturnValue;
                }
            }

            functionReturnValue = cMOTPS;
            return functionReturnValue;
        }


        private string fc_GetArticleDep(string sdate, string sHeureDeb)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetArticleDep() - Enter In The Function - sdate = {sdate}, sHeureDeb = {sHeureDeb}");
            //===> Fin Modif Mondir

            TimeSpan dtTime = new TimeSpan();
            string functionReturnValue = null;
            if (General.IsDate(sdate))
            {
                if (Convert.ToDateTime(sdate).DayOfWeek == DayOfWeek.Sunday)
                {
                    functionReturnValue = cMODNuit;
                    return functionReturnValue;
                }
                if (Convert.ToDateTime(sdate).DayOfWeek == DayOfWeek.Saturday && dtTime > new TimeSpan(20, 00, 00))
                {
                    functionReturnValue = cMODNuit;
                    return functionReturnValue;
                }
                else
                {
                    functionReturnValue = cMODS;

                }
            }
            if (General.IsDate(sHeureDeb))
            {
                dtTime = Convert.ToDateTime(sHeureDeb).TimeOfDay;
                if (dtTime < new TimeSpan(06, 00, 00))//am
                {
                    functionReturnValue = cMODNuit;
                    return functionReturnValue;
                }
                if (dtTime > new TimeSpan(20, 00, 00))//pm
                {
                    functionReturnValue = cMODNuit;
                    return functionReturnValue;
                }
            }

            functionReturnValue = cMODS;
            return functionReturnValue;
        }

        private void fc_GetAchat(int lInterOrDevis, string sWhere)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetAchat() - Enter In The Function - lInterOrDevis = {lInterOrDevis}, sWhere = {sWhere}");
            //===> Fin Modif Mondir

            DataTable adotemp = new DataTable();
            ModAdo modAdotmp = new ModAdo();
            DataTable rsPrixVente = new DataTable();
            ModAdo rsPrixVenteAdo = new ModAdo();
            string sSQL;
            double dblPrixVente = 0;

            try
            {

                if (lInterOrDevis == 1)
                {
                    adotemp = modAdotmp.fc_OpenRecordSet("SELECT BCD_Detail.BCD_Designation, BCD_Detail.BCD_Quantite, "
                            + " BCD_Detail.BCD_PrixHT From BCD_Detail INNER JOIN BonDeCommande ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande "
                            + sWhere);

                    if (adotemp.Rows.Count > 0)
                    {
                        foreach (DataRow adotempR in adotemp.Rows)
                        {

                            if (Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) > 0)
                            {
                                sSQL = "SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente "
                                      + " WHERE PalierAchat>=" + Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0"))
                                      * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0"))
                                      + " ORDER BY PalierAchat ASC";

                                rsPrixVente = rsPrixVenteAdo.fc_OpenRecordSet(sSQL);

                                if (rsPrixVente.Rows.Count > 0)
                                {
                                    if (Convert.ToInt32(General.nz(rsPrixVente.Rows[0]["AfficherFormule"], 0)) != 0)
                                    {
                                        var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                        Row1.Cells["Designation"].Value = "Formule utilisée : (" + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], "0"))
                                                + "*" + ((Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0"))
                                                * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0"))) + ") + "
                                                + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], "0")));
                                        Row1.Update();
                                        SSOleDBGrid1.UpdateData();
                                    }
                                    dblPrixVente = ((Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0")))
                                            * Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], "0"))) + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], "0"));
                                }
                                else
                                {
                                    rsPrixVente.Dispose();
                                    rsPrixVente = rsPrixVenteAdo.fc_OpenRecordSet("SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente WHERE PalierAchat<="
                                        + Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0"))
                                        + " ORDER BY PalierAchat DESC");

                                    if (rsPrixVente.Rows.Count > 0)
                                    {
                                        if (Convert.ToInt32(General.nz(rsPrixVente.Rows[0]["AfficherFormule"], 0)) != 0)
                                        {
                                            var Row2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                            Row2.Cells["Designation"].Value = "";
                                            Row2.Cells["Designation"].Value = "Formule utilisée : (" + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], "0")) + "*"
                                                    + ((Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) *
                                                    Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0"))) + ") + "
                                                    + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], "0")));

                                            Row2.Update();
                                            SSOleDBGrid1.UpdateData();
                                        }
                                        dblPrixVente = ((Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0")))
                                                * Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["a"], "0"))) + Convert.ToDouble(General.nz(rsPrixVente.Rows[0]["b"], "0"));
                                    }
                                    else
                                    {
                                        dblPrixVente = ((Convert.ToDouble(General.nz(adotempR["BCD_PrixHT"], "0")) * Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], "0"))));
                                    }
                                    rsPrixVente.Dispose();
                                }

                            }
                            else
                            {
                                dblPrixVente = 0;

                            }
                            var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                            Row.Cells["Article"].Value = General.sArticleAchat;
                            Row.Cells["Designation"].Value = adotempR["BCD_Designation"] + "";
                            Row.Cells["Quantite"].Value = adotempR["BCD_Quantite"] + "";
                            if (Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], 0)) != 0)
                            {
                                Row.Cells["PrixUnitaire"].Value = (dblPrixVente / Convert.ToDouble(General.nz(adotempR["BCD_Quantite"], 0))).ToString();
                            }
                            Row.Cells["MontantLigne"].Value = dblPrixVente + "";



                            if (txtTVA.Text == "TP")
                            {
                                Row.Cells["TVA"].Value = sTauxTvaAchatTP;
                                Row.Cells["Compte"].Value = sCompteTvaAchatTP;
                                Row.Cells["Compte7"].Value = sCompte7AchatTP;
                            }
                            else if (txtTVA.Text == "TR")
                            {
                                Row.Cells["TVA"].Value = sTauxTvaAchatTR;
                                Row.Cells["Compte"].Value = sCompteTvaAchatTR;
                                Row.Cells["Compte7"].Value = sCompte7AchatTR;
                            }
                            Row.Cells["AnaActivite"].Value = cDefautActivite;
                            Row.Update();
                            SSOleDBGrid1.UpdateData();
                        }
                    }

                    if (rsPrixVente != null)
                    {
                        rsPrixVente.Dispose();
                        rsPrixVente = null;
                    }
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetAchat");
            }
        }

        /// <summary>
        /// =======> Mondir le 16.06.2020 : Aded by ref, Person that integrated thi forgot the ref keyword
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="dbHT"></param>
        /// <param name="dbTTC"></param>
        private void fc_CalculDevisHorsCEE(string sNodevis, ref double dbHT, ref double dbTTC)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CalculDevisHorsCEE() - Enter In The Function - sNodevis = {sNodevis}, dbHT = {dbHT}, dbTTC = {dbTTC}");
            //===> Fin Modif Mondir

            //'=== deuit les CEE du devis.
            //'=== attention dbHT et DbTCC sont utilisées des pointeurs.
            string sSQL;

            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            double dbTVAtemp = 0;
            double dbTCCtemp = 0;

            try
            {

                sSQL = "SELECT        CodeArticle, TotalVenteApresCoef, CodeTVA"
                   + " From DevisDetail "
                   + " WHERE  NumeroDevis = '" + sNodevis + "' AND CodeArticle ='" + cArticleCee + "'";
                rs = rsAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    dbHT = dbHT - Convert.ToDouble(General.nz(rs.Rows[0]["TotalVenteApresCoef"], 0));

                    //======> Mohammed 09.07.2020 : modifs of V09.07.2020

                    //if (Convert.ToDouble(General.nz(rs.Rows[0]["CodeTVA"], 0)) > 0)
                    //{
                    //    dbTVAtemp = (dbHT * Convert.ToDouble(General.nz(rs.Rows[0]["CodeTVA"], 0))) / 100;
                    //    dbTCCtemp = Convert.ToDouble(General.nz(rs.Rows[0]["TotalVenteApresCoef"], 0)) + dbTVAtemp;
                    //    dbTCCtemp = General.FncArrondir(dbTTC, 2);
                    //    dbTTC = dbTTC - dbTCCtemp;
                    //}

                    dbTTC = dbTTC - Convert.ToDouble(General.nz(rs.Rows[0]["TotalVenteApresCoef"], 0));

                    //======> End of modifs V09.07.2020
                }
                rs.Dispose();
                rs = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CalculDevisHorsCEE");
            }

        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// Mondir le 17.09.2020, pour ajouter les modifs de la version V15.09.2020
        /// Modif de la version V02.07.2020 ajoutées par Mondir
        /// Modif de la version V02.07.2020 testées par Mondir
        /// Modifs de la version V05.07.2020 Ajoutées par Mondir
        /// Modifs de la version V05.07.2020 Testées par Mondir
        /// </summary>
        /// <param name="sTypeTva"></param>
        /// <param name="lTypeFacture"></param>
        private void fc_InsertCorpDevisV2(string sTypeTva, int lTypeFacture)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InsertCorpDevisV2() - Enter In The Function - sTypeTva = {sTypeTva}, lTypeFacture = {lTypeFacture}");
            //===> Fin Modif Mondir

            //'=== ltypefacture est à 1 => Facture interv accompte
            //'=== ltypefacture est 2 => Facture interv definitive.
            //'=== ltypefacture est => Facture devis de garantie.

            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsADo = new ModAdo();
            DataTable rs2 = new DataTable();
            ModAdo rs2ADo = new ModAdo();
            DataTable rsArt = new DataTable();
            ModAdo rsArtADo = new ModAdo();

            string lNointervention;
            string stemp;
            string sNodevis;

            int lNumficheStandard;
            int dbTaux;
            SituationDevis[] tSit = null;
            int i = 0;
            double dbTauxCee = 0;
            double dbHTCee = 0;
            string sService;

            string sSQlcee;
            double dbHtDev;
            double dbTTCDev;
            //'Dim bSituation              As Boolean
            string sTypeDevis;
            string sTYD_Code;
            string sCodeArticle = null;
            string sCpteVenteTR = null;
            string sCpteTVATR = null;
            string sCpteVenteTP = null;
            string sCpteTVATP = null;
            int lNbSituation = 0;
            //===> Mondir le 05.07.2020 : Modifs de la version V05.07.2020
            bool bFirst;
            //===> Fin Modif Mondir
            //===> Mondir le 17.09.2020 : Modifs de la version V15.09.2020
            string sdate = "";
            //===> Fin Modif Mondir

            try
            {

                //var vVar = SSOleDBGrid3.ActiveRow.Index;
                var vVar = SSOleDBGrid3.Rows[0].Index;
                lNointervention = (General.nz(SSOleDBGrid3.Rows[vVar].Cells["nointervention"].Value, 0)).ToString();

                sSQL = "SELECT        GestionStandard.NumFicheStandard, DevisEnTete.NumeroDevis , GestionStandard.RefOS, GestionStandard.DateOS "
                        + " , DevisEnTete.TotalHT, DevisEnTete.TotalTTC , DevisEnTete.TitreDevis, MetierID, TYD_Code "
                        + " FROM            DevisEnTete INNER JOIN "
                        + " Intervention INNER JOIN "
                        + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard ON DevisEnTete.NumeroDevis = GestionStandard.NoDevis";

                sSQL = sSQL + " WHERE   Intervention.NoIntervention = " + lNointervention;

                SSTab1.Tabs[1].Selected = true;
                SSTab1.Tabs[1].Active = true;

                rs = rsADo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }
                else
                {
                    lNumficheStandard = Convert.ToInt32(General.nz(rs.Rows[0]["NumFicheStandard"], 0));
                    sNodevis = General.nz(rs.Rows[0]["numerodevis"], "").ToString();
                    sService = General.nz(rs.Rows[0]["MetierID"], "") + "";
                    sTYD_Code = rs.Rows[0]["TYD_Code"].ToString().Trim(' ');
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lNbSituation = fc_CtrlSiAcompte(lNumficheStandard);
                if (lTypeFacture == 1 && lNbSituation == 0)
                {
                    //=== facture d'acompte.
                    stemp = "******** FACTURE D'ACOMPTE ********";
                }
                else if (chkSolderDevis.Checked)
                {
                    stemp = "******** FACTURE DEFINITIVE ********";
                }
                else
                {
                    stemp = "******** FACTURE DE SITUATION N°" + (lNbSituation + 1) + " ********";
                }

                var newRow = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                newRow.Cells["Designation"].Value = stemp;
                SSOleDBGrid1.UpdateData();

                //===saut de ligne.
                var newRow2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                SSOleDBGrid1.UpdateData();

                //=== titre du devis.
                var newRow3 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                //===> Mondir le 18.02.2021, les modifs de la version V16.02.2021, line bellow is commented
                //newRow3.Cells["Designation"].Value = "Chantier : " + rs.Rows[0]["TitreDevis"];
                newRow3.Cells["Designation"].Value = "Travaux : " + rs.Rows[0]["TitreDevis"];
                //===> Fin Modif Mondir
                SSOleDBGrid1.UpdateData();

                //===saut de ligne.
                var newRow4 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                SSOleDBGrid1.UpdateData();

                if (chkSolderDevis.Checked)
                {
                    //=== recherche de l'intervtion avec la date la plus récente.
                    sSQL = "SELECT        TOP (1) DateRealise From Intervention "
                           + " Where NumFicheStandard =" + lNumficheStandard + "  ORDER BY DateRealise DESC";
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        //===> Mondir le 18.02.2021 pour ajouter les modifs de la version V16.02.2021 - add sSQL.ToDate().ToShortDateString()
                        stemp = "Date de fin des travaux : " + sSQL.ToDate().ToShortDateString();
                        //===> Fin Modif Mondir
                        var newRow5 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                        newRow5.Cells["Designation"].Value = stemp;
                        SSOleDBGrid1.UpdateData();

                        //===saut de ligne.
                        var newRow6 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                        SSOleDBGrid1.UpdateData();
                    }
                }

                stemp = "Travaux réalisés suivant notre offre " + rs.Rows[0]["numerodevis"];

                //==== Ajout de l'OS.
                if (General.nz(rs.Rows[0]["RefOS"], "").ToString() != "")
                {
                    //===> Mondir le 17.09.2020, this line removed in the version V15.09.2020
                    //stemp = stemp + " et votre accord n°" + rs.Rows[0]["RefOS"] + " du " + Convert.ToDateTime(rs.Rows[0]["DateOS"]).ToShortDateString();

                    //===> Mondir le 17.09.2020, pour ajouter les modifs de la version 15.09.2020
                    sdate = General.nz(rs.Rows[0]["DateOS"], "").ToString();
                    if (sdate.Contains("1900") || sdate.Contains("1899"))
                        sdate = "";

                    stemp = stemp + " et votre accord n°" + rs.Rows[0]["RefOS"];
                    if (!string.IsNullOrEmpty(sdate))
                    {
                        stemp = stemp + " du " + Convert.ToDateTime(rs.Rows[0]["DateOS"]).ToShortDateString();
                    }
                    //===> Fin Modif Mondir
                }
                var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row.Cells["Designation"].Value = stemp;
                Row.Update();
                SSOleDBGrid1.UpdateData();

                dbHtDev = Convert.ToDouble(General.nz(rs.Rows[0]["TotalHT"], 0));
                dbTTCDev = Convert.ToDouble(General.nz(rs.Rows[0]["TotalTTC"], 0));

                fc_CalculDevisHorsCEE(General.nz(rs.Rows[0]["numerodevis"], "").ToString(), ref dbHtDev, ref dbTTCDev);
                stemp = "pour un montant de " + dbHtDev + " € HT et " + dbTTCDev + " € TTC";

                var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row1.Cells["Designation"].Value = stemp;
                Row1.Update();
                SSOleDBGrid1.UpdateData();

                //===saut de ligne.
                var Row2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row2.Update();
                SSOleDBGrid1.UpdateData();

                //=== Bug sur grille sheridan, le dernier saut de ligne avec addnew pose pb
                //=== remplacé par additem.
                //====> Nous avons pas ce probleme sur la version C#
                //sSQL = ""
                //SSOleDBGrid1.AddItem sSQL

                Cursor = Cursors.WaitCursor;

                //'===saut de ligne.
                //'SSOleDBGrid1.AddNew
                //'SSOleDBGrid1.Update

                //=== facture de situation
                sSQL = "SELECT        NumeroDevis, SUM(TotalVenteApresCoef) AS HT, CodeTVA, CodeArticle"
                       + " From DevisDetail "
                       + " GROUP BY NumeroDevis, CodeTVA, CodeArticle "
                       + " HAVING        (SUM(TotalVenteApresCoef) IS NOT NULL) "
                       + " AND (SUM(TotalVenteApresCoef) <> 0) AND (NumeroDevis = '" + sNodevis + "') "
                       + " ORDER BY CodeTVA ";

                rs2 = rs2ADo.fc_OpenRecordSet(sSQL);
                if (rs2.Rows.Count > 0)
                {
                    dbTaux = Convert.ToInt32(General.nz(rs2.Rows[0]["CodeTVA"], 0));
                    i = 0;
                    Array.Resize(ref tSit, i + 1);
                    tSit[i].dbTaux = Convert.ToDouble(General.nz(rs2.Rows[0]["CodeTVA"], 0));

                    foreach (DataRow r2 in rs2.Rows)
                    {
                        if (tSit[i].dbTaux != Convert.ToDouble(General.nz(r2["CodeTVA"], 0))
                            && General.UCase(General.nz(r2["CodeArticle"], "")) != General.UCase(cArticleCee))
                        {
                            //====> Mondir le 02.07.2020, modif ajouté suite de l'appel de Rachid
                            i = i + 1;
                            //====> Fin Modif Mondir
                            Array.Resize(ref tSit, i + 1);
                            tSit[i].dbTaux = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                        }

                        if (General.UCase(General.nz(r2["CodeArticle"], "")) == General.UCase(cArticleCee))
                        {
                            dbTauxCee = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                            //=======> Mohammed 09.07.2020 : Modifs of V09.07.2020

                            dbTauxCee = 0;

                            //===> End Modifs of v09.07.2020

                            dbHTCee = General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);
                            //'sCodeArticle = fc_GetArticle(cmbService, dbHtDev, sTYD_Code, nz(rs2!CodeTVA, 0))
                            sCodeArticle = cArticleCee;
                            //'=== recherche le cpt 7

                            sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                            + " From FacArticle ";


                            sSQL = sSQL + " WHERE CodeArticle = '" + sCodeArticle + "'";
                            rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                            if (rsArt.Rows.Count > 0)
                            {
                                sCpteVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                                sCpteTVATR = fc_getCptTva(sCpteVenteTR);


                                sCpteVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                                sCpteTVATP = fc_getCptTva(sCpteVenteTP);
                            }
                            rsArt.Dispose();
                            rsArt = null;
                            //====> Mondir le 05.07.2020, ajouter les modifs de la version V05.07.2020 ===> ligne : tSit[i].sCodeArticle = sCodeArticle; est mises en commentaire
                            //=== mise en commentaire le 05 02 2020
                            ///====> Mondir le 02.07.2020, Modif de la version V02.07.2020
                            //tSit[i].sCodeArticle = sCodeArticle;
                            ///====> Fin Modif Mondir
                        }
                        else
                        {
                            //Mondir le 29.06.2020 General.FncArrondir was fogten while inseting modifs
                            //===> Mondir le 03.05.2021, line bellow is commented https://groupe-dt.mantishub.io/view.php?id=2379
                            //tSit[i].dbHT = tSit[i].dbHT + General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);
                            tSit[i].dbHT = tSit[i].dbHT + Convert.ToDouble(General.nz(r2["HT"], 0));
                            if (string.IsNullOrEmpty(tSit[i].sCodeArticle))
                            {
                                tSit[i].sCodeArticle = fc_GetArticle(cmbService.Text, dbHtDev, sTYD_Code, tSit[i].dbTaux);


                                //'=== recherche le cpt 7
                                sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                                    + " From FacArticle ";


                                sSQL = sSQL + " WHERE CodeArticle = '" + tSit[i].sCodeArticle + "'";
                                rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                                if (rsArt.Rows.Count > 0)
                                {
                                    tSit[i].sCptVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                                    tSit[i].sCptTvaTR = fc_getCptTva(tSit[i].sCptVenteTR);


                                    tSit[i].sCptVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                                    tSit[i].sCptTvaTP = fc_getCptTva(tSit[i].sCptVenteTP);

                                    //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                                    tSit[i].sLibelleArticle = rsArt.Rows[0]["Designation1"] + "";
                                    //===> Fin Modif Mondir
                                }
                                rsArt.Dispose();
                                rsArt = null;
                            }
                        }
                    }
                }

                DataTable dt = ((SSOleDBGrid1.DataSource) as DataTable);
                var newRowX = dt.NewRow();

                //===> Mondir le 10.02.2021, ajout du ? car des fois le tableau est null https://groupe-dt.mantishub.io/view.php?id=2246
                for (i = 0; i < tSit?.Length; i++)
                {
                    //===> Mondir le 03.05.2021 https://groupe-dt.mantishub.io/view.php?id=2379
                    tSit[i].dbHT = Math.Round(tSit[i].dbHT, 2, MidpointRounding.AwayFromZero);
                    //===> Fin Modif Mondir

                    ///===> Mondir le 02.07.2020, condition ajouté dans la version V02.07.2020
                    ///===> Mohammed 10.07.2020 , add && tSit[i].dbHT != 0 to condition
                    if (tSit[i].sCodeArticle?.ToUpper() != sCodeArticle?.ToUpper() && tSit[i].dbHT != 0)
                    {
                        //'=== controle si c'est un accompte.
                        //'bSituation = fc_CtrlSiAcompte(lNumficheStandard)
                        //lNbSituation = fc_CtrlSiAcompte(lNumficheStandard);

                        //===> Mondir le 18.11.2020 commented in VV12.11.2020
                        //if (chkSolderDevis.Checked)
                        //{
                        //    newRow["Article"] = tSit[i].sCodeArticle;
                        //    newRow["Designation"] = "FACTURE DEFINITIVE";
                        //}
                        //else if (lTypeFacture == 1 && lNbSituation == 0)
                        //{
                        //    newRow["Article"] = tSit[i].sCodeArticle;
                        //    newRow["Designation"] = "Facture d'Acompte";
                        //}
                        //else
                        //{
                        //    newRow["Article"] = tSit[i].sCodeArticle;
                        //    newRow["Designation"] =
                        //        "Facture de situation n°" + (lNbSituation + 1) + " - Avancement des travaux";
                        //}

                        newRowX["Article"] = tSit[i].sCodeArticle;
                        newRowX["Designation"] = tSit[i].sLibelleArticle;

                        newRowX["PrixUnitaire"] = tSit[i].dbHT;
                        newRowX["TVA"] = tSit[i].dbTaux;

                        if (General.UCase(sTypeTva) == General.UCase("TR"))
                        {
                            newRowX["compte"] = tSit[i].sCptTvaTR;
                            newRowX["compte7"] = tSit[i].sCptVenteTR;
                        }
                        else
                        {
                            newRowX["compte"] = tSit[i].sCptTvaTP;
                            newRowX["compte7"] = tSit[i].sCptVenteTP;

                        }

                        ///======> Mondir 16.06.2020 : This Line Was removed by the person who integrated the modfis, added 16.06.2020
                        newRowX["AnaActivite"] = cDefautActivite;
                        dt.Rows.Add(newRowX.ItemArray);
                        SSOleDBGrid1.UpdateData();
                    }
                }

                newRowX = dt.NewRow();
                //'=== rachat des CEE.
                if (dbHTCee != 0)
                {
                    //    'sSQlcee = "!!;!!;!!;!" & cArticleCee & "!;" _
                    //    & "!" & "Rachat des CEE!;!!;!!;!" & IIf(lTypeFacture = 1, -dbHTCee, dbHTCee) & "!;" _
                    //    & "!!;!" & dbTauxCee & "!;"

                    newRowX["Article"] = cArticleCee;
                    newRowX["Designation"] = "Rachat des CEE";
                    newRowX["PrixUnitaire"] = dbHTCee;
                    newRowX["TVA"] = dbTauxCee;

                    if (General.UCase(sTypeTva) == General.UCase("TR"))
                    {
                        newRowX["compte"] = sCpteTVATR;
                        newRowX["compte7"] = sCpteVenteTR;
                    }
                    else
                    {
                        newRowX["compte"] = sCpteTVATP;
                        newRowX["compte7"] = sCpteVenteTP;

                    }

                    newRowX["AnaActivite"] = cDefautActivite;

                    dt.Rows.Add(newRowX.ItemArray);
                    SSOleDBGrid1.UpdateData();
                }

                Cursor = Cursors.WaitCursor;

                //'=== facture definitive.
                //'If lTypeFacture = 2 Then
                //'If bSituation = True Then

                if (lNbSituation > 0)
                {
                    // '=== controle si facture existante.
                    fc_GetAccompte(lNumficheStandard);
                }

                Cursor = Cursors.Default;

                //===> Fin Modif Mondir

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + "fc_InsertCorpDevisV2");
            }
        }

        private void fc_InsertCorpDevisV2_OLD_25092020(string sTypeTva, int lTypeFacture)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InsertCorpDevisV2() - Enter In The Function - sTypeTva = {sTypeTva}, lTypeFacture = {lTypeFacture}");
            //===> Fin Modif Mondir

            //'=== ltypefacture est à 1 => Facture interv accompte
            //'=== ltypefacture est 2 => Facture interv definitive.
            //'=== ltypefacture est => Facture devis de garantie.

            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsADo = new ModAdo();
            DataTable rs2 = new DataTable();
            ModAdo rs2ADo = new ModAdo();
            DataTable rsArt = new DataTable();
            ModAdo rsArtADo = new ModAdo();

            string lNointervention;
            string stemp;
            string sNodevis;

            int lNumficheStandard;
            int dbTaux;
            SituationDevis[] tSit = null;
            int i = 0;
            double dbTauxCee = 0;
            double dbHTCee = 0;
            string sService;

            string sSQlcee;
            double dbHtDev;
            double dbTTCDev;
            //'Dim bSituation              As Boolean
            string sTypeDevis;
            string sTYD_Code;
            string sCodeArticle = null;
            string sCpteVenteTR = null;
            string sCpteTVATR = null;
            string sCpteVenteTP = null;
            string sCpteTVATP = null;
            int lNbSituation = 0;
            //===> Mondir le 05.07.2020 : Modifs de la version V05.07.2020
            bool bFirst;
            //===> Fin Modif Mondir
            //===> Mondir le 17.09.2020 : Modifs de la version V15.09.2020
            string sdate = "";
            //===> Fin Modif Mondir

            try
            {

                //var vVar = SSOleDBGrid3.ActiveRow.Index;
                var vVar = SSOleDBGrid3.Rows[0].Index;
                lNointervention = (General.nz(SSOleDBGrid3.Rows[vVar].Cells["nointervention"].Value, 0)).ToString();

                sSQL = "SELECT        GestionStandard.NumFicheStandard, DevisEnTete.NumeroDevis , GestionStandard.RefOS, GestionStandard.DateOS "
                        + " , DevisEnTete.TotalHT, DevisEnTete.TotalTTC , DevisEnTete.TitreDevis, MetierID, TYD_Code "
                        + " FROM            DevisEnTete INNER JOIN "
                        + " Intervention INNER JOIN "
                        + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard ON DevisEnTete.NumeroDevis = GestionStandard.NoDevis";

                sSQL = sSQL + " WHERE   Intervention.NoIntervention = " + lNointervention;

                SSTab1.Tabs[1].Selected = true;
                SSTab1.Tabs[1].Active = true;

                rs = rsADo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }
                else
                {
                    lNumficheStandard = Convert.ToInt32(General.nz(rs.Rows[0]["NumFicheStandard"], 0));
                    sNodevis = General.nz(rs.Rows[0]["numerodevis"], "").ToString();
                    sService = General.nz(rs.Rows[0]["MetierID"], "") + "";
                    sTYD_Code = rs.Rows[0]["TYD_Code"].ToString().Trim(' ');
                }

                stemp = "Travaux réalisés suivant notre offre " + rs.Rows[0]["numerodevis"];

                //'==== Ajout de l'OS.
                if (General.nz(rs.Rows[0]["RefOS"], "").ToString() != "")
                {
                    //===> Mondir le 17.09.2020, this line removed in the version V15.09.2020
                    //stemp = stemp + " et votre accord n°" + rs.Rows[0]["RefOS"] + " du " + Convert.ToDateTime(rs.Rows[0]["DateOS"]).ToShortDateString();

                    //===> Mondir le 17.09.2020, pour ajouter les modifs de la version 15.09.2020
                    sdate = General.nz(rs.Rows[0]["DateOS"], "").ToString();
                    if (sdate.Contains("1900") || sdate.Contains("1899"))
                        sdate = "";

                    stemp = stemp + " et votre accord n°" + rs.Rows[0]["RefOS"];
                    if (!string.IsNullOrEmpty(sdate))
                    {
                        stemp = stemp + " du " + Convert.ToDateTime(rs.Rows[0]["DateOS"]).ToShortDateString();
                    }
                    //===> Fin Modif Mondir
                }

                var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row.Cells["Designation"].Value = stemp;
                Row.Update();
                SSOleDBGrid1.UpdateData();

                dbHtDev = Convert.ToDouble(General.nz(rs.Rows[0]["TotalHT"], 0));
                dbTTCDev = Convert.ToDouble(General.nz(rs.Rows[0]["TotalTTC"], 0));

                fc_CalculDevisHorsCEE(General.nz(rs.Rows[0]["numerodevis"], "").ToString(), ref dbHtDev, ref dbTTCDev);
                stemp = "pour un montant de " + dbHtDev + " € HT et " + dbTTCDev + " € TTC";

                var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row1.Cells["Designation"].Value = stemp;
                Row1.Update();
                SSOleDBGrid1.UpdateData();

                //'===saut de ligne.
                var Row2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row2.Update();
                SSOleDBGrid1.UpdateData();

                //'=== titre du devis.
                var Row3 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row3.Cells["Designation"].Value = rs.Rows[0]["TitreDevis"].ToString();
                Row3.Update();
                SSOleDBGrid1.UpdateData();

                //'===saut de ligne.
                //'SSOleDBGrid1.AddNew
                //'SSOleDBGrid1.Update

                // '=== facture de situation

                sSQL = "SELECT        NumeroDevis, SUM(TotalVenteApresCoef) AS HT, CodeTVA, CodeArticle"
                        + " From DevisDetail "
                        + " GROUP BY NumeroDevis, CodeTVA, CodeArticle "
                        + " HAVING        (SUM(TotalVenteApresCoef) IS NOT NULL) "
                        + " AND (SUM(TotalVenteApresCoef) <> 0) AND (NumeroDevis = '" + sNodevis + "') "
                        + " ORDER BY CodeTVA ";
                rs2 = rs2ADo.fc_OpenRecordSet(sSQL);
                if (rs2.Rows.Count > 0)
                {
                    dbTaux = Convert.ToInt32(General.nz(rs2.Rows[0]["CodeTVA"], 0));
                    i = 0;
                    Array.Resize(ref tSit, i + 1);
                    tSit[i].dbTaux = Convert.ToDouble(General.nz(rs2.Rows[0]["CodeTVA"], 0));

                    foreach (DataRow r2 in rs2.Rows)
                    {
                        if (tSit[i].dbTaux != Convert.ToDouble(General.nz(r2["CodeTVA"], 0))
                            && General.UCase(General.nz(r2["CodeArticle"], "")) != General.UCase(cArticleCee))
                        {
                            //====> Mondir le 02.07.2020, modif ajouté suite de l'appel de Rachid
                            i = i + 1;
                            //====> Fin Modif Mondir
                            Array.Resize(ref tSit, i + 1);
                            tSit[i].dbTaux = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                        }

                        if (General.UCase(General.nz(r2["CodeArticle"], "")) == General.UCase(cArticleCee))
                        {
                            dbTauxCee = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                            //=======> Mohammed 09.07.2020 : Modifs of V09.07.2020

                            dbTauxCee = 0;

                            //===> End Modifs of v09.07.2020

                            dbHTCee = General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);
                            //'sCodeArticle = fc_GetArticle(cmbService, dbHtDev, sTYD_Code, nz(rs2!CodeTVA, 0))
                            sCodeArticle = cArticleCee;
                            //'=== recherche le cpt 7

                            sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                            + " From FacArticle ";


                            sSQL = sSQL + " WHERE CodeArticle = '" + sCodeArticle + "'";
                            rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                            if (rsArt.Rows.Count > 0)
                            {
                                sCpteVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                                sCpteTVATR = fc_getCptTva(sCpteVenteTR);


                                sCpteVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                                sCpteTVATP = fc_getCptTva(sCpteVenteTP);
                            }
                            rsArt.Dispose();
                            rsArt = null;
                            //====> Mondir le 05.07.2020, ajouter les modifs de la version V05.07.2020 ===> ligne : tSit[i].sCodeArticle = sCodeArticle; est mises en commentaire
                            //=== mise en commentaire le 05 02 2020
                            ///====> Mondir le 02.07.2020, Modif de la version V02.07.2020
                            //tSit[i].sCodeArticle = sCodeArticle;
                            ///====> Fin Modif Mondir
                        }
                        else
                        {
                            //Mondir le 29.06.2020 General.FncArrondir was fogten while inseting modifs
                            tSit[i].dbHT = tSit[i].dbHT + General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);
                            if (string.IsNullOrEmpty(tSit[i].sCodeArticle))
                            {
                                tSit[i].sCodeArticle = fc_GetArticle(cmbService.Text, dbHtDev, sTYD_Code, tSit[i].dbTaux);


                                //'=== recherche le cpt 7
                                sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                                    + " From FacArticle ";


                                sSQL = sSQL + " WHERE CodeArticle = '" + tSit[i].sCodeArticle + "'";
                                rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                                if (rsArt.Rows.Count > 0)
                                {
                                    tSit[i].sCptVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                                    tSit[i].sCptTvaTR = fc_getCptTva(tSit[i].sCptVenteTR);


                                    tSit[i].sCptVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                                    tSit[i].sCptTvaTP = fc_getCptTva(tSit[i].sCptVenteTP);
                                }
                                rsArt.Dispose();
                                rsArt = null;
                            }
                        }
                    }
                }
                DataTable dt = ((SSOleDBGrid1.DataSource) as DataTable);
                var newRow = dt.NewRow();

                for (i = 0; i < tSit.Length; i++)
                {
                    ///===> Mondir le 02.07.2020, condition ajouté dans la version V02.07.2020
                    ///===> Mohammed 10.07.2020 , add && tSit[i].dbHT != 0 to condition
                    if (tSit[i].sCodeArticle?.ToUpper() != sCodeArticle?.ToUpper() && tSit[i].dbHT != 0)
                    {
                        //'=== controle si c'est un accompte.
                        //'bSituation = fc_CtrlSiAcompte(lNumficheStandard)
                        lNbSituation = fc_CtrlSiAcompte(lNumficheStandard);

                        if (chkSolderDevis.Checked)
                        {
                            //sSQL = "!!;!!;!!;!" & tSit(i).sCodeArticle & "!;" _
                            //    & "!" & "FACTURE DEFINITIVE!;"
                            newRow["Article"] = tSit[i].sCodeArticle;
                            newRow["Designation"] = "FACTURE DEFINITIVE";
                        }
                        //'ElseIf lTypeFacture = 1 And bSituation = False Then
                        else if (lTypeFacture == 1 && lNbSituation == 0)
                        {
                            //sSQL = "!!;!!;!!;!" & tSit(i).sCodeArticle & "!;" _
                            //   & "!" & "Facture d'Acompte!;"
                            newRow["Article"] = tSit[i].sCodeArticle;
                            newRow["Designation"] = "Facture d'Acompte";
                        }
                        else
                        {
                            //sSQL = "!!;!!;!!;!" & tSit(i).sCodeArticle & "!;" _
                            //      & "!" & "Facture de situation n°" & lNbSituation + 1 & " - Avancement des travaux!;"
                            newRow["Article"] = tSit[i].sCodeArticle;
                            newRow["Designation"] =
                                "Facture de situation n°" + (lNbSituation + 1) + " - Avancement des travaux";
                        }

                        //sSQL = sSQL & "!!;!!;!" & tSit(i).dbHT & "!;" _
                        //&"!!;!" & tSit(i).dbTaux & "!;"
                        newRow["PrixUnitaire"] = tSit[i].dbHT;
                        newRow["TVA"] = tSit[i].dbTaux;
                        //'If tSit(i).dbTaux >= 0 And tSit(i).dbTaux < 9.9 Then
                        //'        sSQL = sSQL & "!" & sCpteTVATR & "!;" & "!" & sCpteVenteTR & "!;"
                        //'Else
                        //'        sSQL = sSQL & "!" & sCpteTVATP & "!;" & "!" & sCpteVenteTP & "!;"
                        //'End If
                        if (General.UCase(sTypeTva) == General.UCase("TR"))
                        {
                            //sSQL = sSQL & "!" & tSit(i).sCptTvaTR & "!;" & "!" & tSit(i).sCptVenteTR & "!;"
                            newRow["compte"] = tSit[i].sCptTvaTR;
                            newRow["compte7"] = tSit[i].sCptVenteTR;
                        }
                        else
                        {
                            //sSQL = sSQL & "!" & tSit(i).sCptTvaTP & "!;" & "!" & tSit(i).sCptVenteTP & "!;"
                            newRow["compte"] = tSit[i].sCptTvaTP;
                            newRow["compte7"] = tSit[i].sCptVenteTP;

                        }

                        ///======> Mondir 16.06.2020 : This Line Was removed by the person who integrated the modfis, added 16.06.2020
                        newRow["AnaActivite"] = cDefautActivite;
                        dt.Rows.Add(newRow.ItemArray);
                        SSOleDBGrid1.UpdateData();
                    }
                }

                newRow = dt.NewRow();
                //'=== rachat des CEE.
                if (dbHTCee != 0)
                {
                    //    'sSQlcee = "!!;!!;!!;!" & cArticleCee & "!;" _
                    //    & "!" & "Rachat des CEE!;!!;!!;!" & IIf(lTypeFacture = 1, -dbHTCee, dbHTCee) & "!;" _
                    //    & "!!;!" & dbTauxCee & "!;"


                    //sSQlcee = "!!;!!;!!;!" & cArticleCee & "!;" _
                    //    & "!" & "Rachat des CEE!;!!;!!;!" & dbHTCee & "!;" _
                    //    & "!!;!" & dbTauxCee & "!;"

                    newRow["Article"] = cArticleCee;
                    newRow["Designation"] = "Rachat des CEE";
                    newRow["PrixUnitaire"] = dbHTCee;
                    newRow["TVA"] = dbTauxCee;

                    if (General.UCase(sTypeTva) == General.UCase("TR"))
                    {
                        // sSQlcee = sSQlcee & "!" & sCpteTVATR & "!;" & "!" & sCpteVenteTR & "!;"
                        newRow["compte"] = sCpteTVATR;
                        newRow["compte7"] = sCpteVenteTR;
                    }
                    else
                    {
                        //  sSQlcee = sSQlcee & "!" & sCpteTVATP & "!;" & "!" & sCpteVenteTP & "!;"
                        newRow["compte"] = sCpteTVATP;
                        newRow["compte7"] = sCpteVenteTP;

                    }
                    //sSQlcee = sSQlcee & "!" & cDefautActivite & "!;"
                    newRow["AnaActivite"] = cDefautActivite;

                    dt.Rows.Add(newRow.ItemArray);
                    SSOleDBGrid1.UpdateData();
                }


                //'=== facture definitive.
                //'If lTypeFacture = 2 Then
                //'If bSituation = True Then

                if (lNbSituation > 0)
                {
                    // '=== controle si facture existante.
                    fc_GetAccompte(lNumficheStandard);
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + "fc_InsertCorpDevisV2");
            }
        }

        /// <summary>
        ///
        /// Mondir le 03.07.2020, Modif demandé par Rachid via Teams ===> Testées
        /// </summary>
        /// <param name="sService"></param>
        /// <param name="dbHtDev"></param>
        /// <param name="sTypeDevis"></param>
        /// <param name="dbTaux"></param>
        /// <returns></returns>
        private string fc_GetArticle(string sService, double dbHtDev, string sTypeDevis, double dbTaux)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetArticle() - Enter In The Function - sService = {sService}, dbHtDev = {dbHtDev}, sTypeDevis = {sTypeDevis}, dbTaux = {dbTaux}");
            //===> Fin Modif Mondir

            string returnFunctionValue = null;
            try
            {
                if (General.UCase(sTypeDevis) == General.UCase("P3"))
                {
                    if (dbHtDev < 30000)
                    {
                        if (dbTaux == 5.5)
                        {
                            returnFunctionValue = "TRP355";
                        }
                        else if (dbTaux == 10)
                        {
                            returnFunctionValue = "TRP310";
                        }
                        else if (dbTaux == 20)
                        {
                            returnFunctionValue = "TRP320";
                        }
                        else
                        {
                            returnFunctionValue = "TRP300";
                        }
                    }
                    else
                    {
                        if (dbTaux == 5.5)
                        {
                            //===> Mondir le 03.07.2020, Modif demandé par Rachid via Teams ===> Testées
                            //returnFunctionValue = "CPT355";
                            //===> Fin Modif Mondir
                            returnFunctionValue = "TRP355";
                        }
                        else if (dbTaux == 10)
                        {
                            //===> Mondir le 03.07.2020, Modif demandé par Rachid via Teams ===> Testées
                            //returnFunctionValue = "CPT310";
                            //===> Fin Modif Mondir
                            returnFunctionValue = "TRP310";
                        }
                        else if (dbTaux == 20)
                        {
                            //===> Mondir le 03.07.2020, Modif demandé par Rachid via Teams ===> Testées
                            //returnFunctionValue = "CPT320";
                            //===> Fin Modif Mondir
                            returnFunctionValue = "TRP320";
                        }
                        else
                        {
                            //===> Mondir le 03.07.2020, Modif demandé par Rachid via Teams ===> Testées
                            //returnFunctionValue = "CPT300";
                            //===> Fin Modif Mondir
                            returnFunctionValue = "TRP300";
                        }
                    }
                }
                else if (General.UCase(sService) == General.UCase("CLIM"))
                {
                    if (dbHtDev >= 30000)
                    {
                        if (dbTaux == 5.5)
                        {
                            returnFunctionValue = "TRGT55";
                        }
                        else if (dbTaux == 10)
                        {
                            returnFunctionValue = "TRGT10";
                        }
                        else if (dbTaux == 20)
                        {
                            returnFunctionValue = "TRGT20";
                        }
                        else
                        {
                            returnFunctionValue = "TRGT00";
                        }
                    }
                    else
                    {
                        if (dbHtDev >= 30000)
                        {
                            if (dbTaux == 5.5)
                            {
                                returnFunctionValue = "TRGH55";
                            }
                            else if (dbTaux == 10)
                            {
                                returnFunctionValue = "TRGH10";
                            }
                            else if (dbTaux == 20)
                            {
                                returnFunctionValue = "TRGH20";
                            }
                            else
                            {
                                returnFunctionValue = "TRGH00";
                            }
                        }
                        else
                        {
                            if (dbTaux == 5.5)
                            {
                                returnFunctionValue = "TRPT55";
                            }
                            else if (dbTaux == 10)
                            {
                                returnFunctionValue = "TRPT10";
                            }
                            else if (dbTaux == 20)
                            {
                                returnFunctionValue = "TRPT20";
                            }
                            else
                            {
                                returnFunctionValue = "TRPT00";
                            }

                        }
                    }
                }
                else
                {

                    if (dbHtDev >= 30000)
                    {
                        if (dbTaux == 5.5)
                        {
                            returnFunctionValue = "TRGH55";
                        }
                        else if (dbTaux == 10)
                        {
                            returnFunctionValue = "TRGH10";
                        }
                        else if (dbTaux == 20)
                        {
                            returnFunctionValue = "TRGH20";
                        }
                        else
                        {
                            returnFunctionValue = "TRGH00";
                        }
                    }
                    else
                    {
                        if (dbTaux == 5.5)
                        {
                            returnFunctionValue = "TRPT55";
                        }
                        else if (dbTaux == 10)
                        {
                            returnFunctionValue = "TRPT10";
                        }
                        else if (dbTaux == 20)
                        {
                            returnFunctionValue = "TRPT20";
                        }
                        else
                        {
                            returnFunctionValue = "TRPT00";
                        }

                    }
                }
                return returnFunctionValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetArticle");
                return returnFunctionValue;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCpt7"></param>
        /// <returns></returns>
        private string fc_getCptTva(string sCpt7)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_getCptTva() - Enter In The Function - sCpt7 = {sCpt7}");
            //===> Fin Modif Mondir

            string returnFunctionValue = null;
            DataTable rs2 = new DataTable();
            ModAdo rs2Ado = new ModAdo();
            string sSQL;
            try
            {
                SAGE.fc_OpenConnSage();
                sSQL = "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM as cptTva, F_TAXE.TA_TAUX "
                        + " FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                        + " WHERE (F_COMPTEG.CG_NUM ='" + sCpt7 + "')  "
                        + "  ORDER BY F_COMPTEG.CG_NUM";
                rs2 = rs2Ado.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);
                if (rs2.Rows.Count > 0)
                {
                    returnFunctionValue = rs2.Rows[0]["CptTVA"] + "";
                }
                rs2.Dispose();
                rs2 = null;

                return returnFunctionValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetArticle");
                return returnFunctionValue;
            }
        }

        /// <summary>
        /// Modif de la version V28.06.2020 de VB6 ajoutée par Mondir le 28.06.2020
        /// Modif de la version V28.06.2020 de VB6 testées par Mondir le 28.06.2020
        /// Modif de la version V29.06.2020 de VB6 ajoutée par Mondir le 30.06.2020
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        private void fc_GetAccompte(int lNumficheStandard)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetAccompte() - Enter In The Function - lNumficheStandard = {lNumficheStandard}");
            //===> Fin Modif Mondir

            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            double dbQte;
            double dbHT;
            double dbPxUnit;
            Deduction[] tpD = null;
            int i = 0;
            string sNofacture;
            string stemp = "";
            int lNbSituation = 0;
            bool bFirst;
            //===> Mondir le 30.06.2020 pour ajouter les modifs de la version V29.06.2020
            double dbQTeOld = 0;
            double dbPrxUnitOld = 0;
            bool bMaj;
            //===> Fin modif Mondir

            //==> Ahmed le 02/09/2020 pour integrer les modifications du 01.09.2020
            string sWhere = "";
            //Fin modif Ahmed

            try
            {
                //==> Ahmed le 02/09/2020 pour integrer les modifications du 01.09.2020
                sSQL = "SELECT DISTINCT NoFacture From Intervention "
                       + " WHERE    NumFicheStandard = " + lNumficheStandard +
                       " AND (NoFacture IS NOT NULL AND NoFacture <> N'')"
                       + " and (NOT (Intervention.NoFacture LIKE N'XX%')) ";
                rs = rsAdo.fc_OpenRecordSet(sSQL);

                //===> Mondir le 07.09.2020, ajout condition pour corriger : https://groupe-dt.mantishub.io/view.php?id=1972
                if (rs == null || rs.Rows.Count == 0)
                    return;

                foreach (DataRow row in rs.Rows)

                    sWhere = (string.IsNullOrEmpty(sWhere)) ?
                        " WHERE (FactureManuelleEnTete.NoFacture = '" + row["NoFacture"] + "'"
                        : sWhere + " OR FactureManuelleEnTete.NoFacture = '" + row["NoFacture"] + "'";

                if ((string.IsNullOrEmpty(sWhere))) return;

                //logging
                Program.SaveException(null, $"fc_GetAccompte() - sWhere = {sWhere} ");

                sWhere += ")";


                //Fin modif Ahmed

                sSQL =
                    "SELECT        FactureManuelleDetail.Article, FactureManuelleDetail.Quantite AS Qte, FactureManuelleEnTete.NoFacture, "
                    + " FactureManuelleDetail.MontantLigne AS HT, FactureManuelleDetail.Compte7, "
                    + " FactureManuelleDetail.Compte, FactureManuelleDetail.TVA, FactureManuelleDetail.PrixUnitaire AS PxUnit, FactureManuelleDetail.CleFactureManuelle ";

                //==> Ahmed le 02/09/2020 pour integrer les modifications du 01.09.2020

                //Old one :
                //sSQL = sSQL + " FROM            FactureManuelleDetail INNER JOIN "
                //            + " FactureManuelleEnTete ON FactureManuelleDetail.CleFactureManuelle = FactureManuelleEnTete.CleFactureManuelle INNER JOIN"
                //            + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture";

                sSQL = sSQL + " FROM            FactureManuelleDetail INNER JOIN "
                            + " FactureManuelleEnTete ON FactureManuelleDetail.CleFactureManuelle = FactureManuelleEnTete.CleFactureManuelle ";


                //sSQL = sSQL + "  WHERE        (Intervention.NoFacture IS NOT NULL) "
                //            + " AND (FactureManuelleDetail.MontantLigne IS NOT NULL) "
                //            + " AND (Intervention.NumFicheStandard = " + lNumficheStandard + ") "
                //    + " AND (Intervention.NoFacture <> N'') AND "
                //    + " (NOT (Intervention.NoFacture LIKE N'XX%')) ";

                sSQL = sSQL + sWhere + " AND (FactureManuelleDetail.MontantLigne IS NOT NULL) ";

                sSQL += " ORDER BY FactureManuelleEnTete.NoFacture ASC, FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA,  PxUnit, Qte";
                //==> Fin modif Ahmed

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }

                //'=== comppte le nombre de factures.
                //foreach (DataRow r in rs.Rows)
                //{
                //    if (General.UCase(stemp) != General.UCase(r["NoFacture"]))
                //    {
                //        lNbSituation = lNbSituation + 1;
                //        stemp = r["NoFacture"] + "";
                //    }
                //}

                i = 0;
                Array.Resize(ref tpD, i + 1);
                bFirst = true;

                //===> Mondir le 30.06.2020 pour ajouter les Modifs de la version V29.06.2020 
                bMaj = false;
                //===> Fin modif Mondir

                foreach (DataRow r in rs.Rows)
                {
                    if (General.UCase(tpD[i].sArticle) != General.UCase(r["article"]?.ToString())
                        || tpD[i].dbTaux != Convert.ToDouble(General.nz(r["TVA"], 0))
                        || General.UCase(tpD[i].sCpt7) != General.UCase(r["compte7"]?.ToString())
                        || General.UCase(tpD[i].sCptTva) != General.UCase(r["compte"]?.ToString())
                        || General.UCase(tpD[i].sNofacture) != General.UCase(r["NoFacture"]?.ToString()))
                    {
                        if (!bFirst)
                        {
                            i += 1;
                            Array.Resize(ref tpD, i + 1);
                            //===> Mondir le 30.06.2020 pour ajouter les Modif de la version V29.06.2020
                            dbQTeOld = 0;
                            //====> Fin Modif Mondir
                        }
                        bFirst = false;
                        //===> Mondir le 30.06.2020 pour ajouter les Modif de la version V29.06.2020
                        bMaj = true;
                        //====> Fin Modif Mondir
                    }
                    else if (dbQTeOld != Convert.ToDouble(General.nz(r["Qte"], 0)) && dbPrxUnitOld != Convert.ToDouble(General.nz(r["PxUnit"], 0)))
                    {
                        i += 1;
                        Array.Resize(ref tpD, i + 1);
                        //===> Mondir le 30.06.2020 pour ajouter les Modif de la version V29.06.2020
                        dbQTeOld = 0;
                        bMaj = true;
                        //====> Fin Modif Mondir
                    }
                    else if (dbPrxUnitOld == Convert.ToDouble(General.nz(r["PxUnit"], 0)))
                    {
                        //=== si les prix unitaire sont égaux on somme les quantités.
                        //===> Mondir le 30.06.2020, Commented this line on version V29.06.2020
                        //dbQte = Math.Abs(tpD[i].dbQte) + Convert.ToDouble(r["Qte"]);
                        //dbQte = -dbQte;
                        //===> Fin Modif Mondir

                        //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                        dbQTeOld += Convert.ToDouble(General.nz(r["Qte"], 0));
                        dbQte = dbQTeOld;

                        dbQte = -dbQte;

                        //===> Mondir le 02.07.2020, Modifié suite de l'appel de Rachid
                        //tpD[i].dbQte = -dbQte;
                        //===> Fin Modif Mondir
                        tpD[i].dbQte = dbQte;

                        dbHT = General.FncArrondir(tpD[i].dbPxUni * dbQte);
                        tpD[i].dbTot = dbHT;
                        bMaj = false;
                        //===> Fin Modif Mondir
                    }
                    else if (dbQTeOld == Convert.ToDouble(General.nz(r["Qte"], 0)))
                    {
                        //=== si les quantités sont égales on somme les prix unitaire.
                        tpD[i].dbPxUni += Convert.ToDouble(General.nz(r["PxUnit"], 0));


                        dbHT = General.FncArrondir(tpD[i].dbPxUni * tpD[i].dbQte, 2);
                        tpD[i].dbTot = dbHT;

                        //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                        bMaj = false;
                        //===> Fin Modif Mondir
                    }

                    //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                    if (bMaj)
                    {
                        dbQTeOld = General.FncArrondir(Convert.ToDouble(General.nz(r["Qte"]?.ToString(), 0)), 2);
                        dbPrxUnitOld = Convert.ToDouble(General.nz(r["PxUnit"], 0));


                        dbPxUnit = Convert.ToDouble(General.nz(r["PxUnit"], 0));
                        dbQte = -General.FncArrondir(Convert.ToDouble(General.nz(r["Qte"]?.ToString(), 0)), 2);
                        dbHT = General.FncArrondir(dbPxUnit * dbQte, 2);


                        tpD[i].sArticle = r["article"] + "";
                        tpD[i].dbQte = dbQte;
                        tpD[i].dbPxUni = dbPxUnit;
                        tpD[i].dbTot = dbHT;
                        tpD[i].dbTaux = Convert.ToDouble(General.nz(r["TVA"]?.ToString(), 0));
                        tpD[i].sCpt7 = r["compte7"] + "";
                        tpD[i].sCptTva = r["compte"] + "";
                        tpD[i].sNofacture = r["NoFacture"] + "";
                    }
                }


                rs.Dispose();
                rs = null;
                sNofacture = tpD[0].sNofacture;

                var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row.Update();
                SSOleDBGrid1.UpdateData();

                //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                bFirst = true;
                lNbSituation = 1;
                //===> Fin Modif Mondir

                for (i = 0; i < tpD.Length; i++)
                {
                    if (General.UCase(sNofacture) != General.UCase(tpD[i].sNofacture))
                    {
                        //fc_GetRachatCEE(sNofacture, true);
                        sNofacture = tpD[i].sNofacture;
                        var Row0 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                        Row0.Update();
                        SSOleDBGrid1.UpdateData();
                        //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                        //lNbSituation = lNbSituation - 1;
                        lNbSituation += 1;
                        //===> Fin Modif Mondir
                    }

                    var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();

                    if (lNbSituation == 1)
                    {
                        // '=== facture d 'accompte
                        //'SSOleDBGrid1.Columns("Designation").value = "Déduction de la Facture d'acompte n°" & tpD(i).sNofacture & ""
                        //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                        if (General.UCase(cArticleCee) == General.UCase(tpD[i].sArticle))
                        {
                            Row1.Cells["Designation"].Value =
                                "Déduction de la Facture d'acompte n°" + tpD[i].sNofacture + " - Rachat des CEE";
                        }
                        else
                        {
                            Row1.Cells["Designation"].Value =
                                "Déduction de la Facture d'acompte n°" + tpD[i].sNofacture + " - Travaux";
                        }
                        bFirst = false;
                        //===> Fin Modif Mondir
                    }
                    else
                    {
                        //===> Mondir le 30.06.2020, pour ajouter les modfis de la version V29.06.2020
                        if (General.UCase(cArticleCee) == General.UCase(tpD[i].sArticle))
                        {
                            Row1.Cells["Designation"].Value = "Déduction de la Facture de situation n°" + lNbSituation + " " + tpD[i].sNofacture + " - Rachat des CEE";
                        }
                        else
                        {
                            Row1.Cells["Designation"].Value = "Déduction de la Facture de situation n°" + lNbSituation + " " + tpD[i].sNofacture + " - Travaux";
                        }
                        //===> Fin Modif Mondir
                    }
                    Row1.Cells["Article"].Value = tpD[i].sArticle;
                    Row1.Cells["Quantite"].Value = tpD[i].dbQte;
                    Row1.Cells["PrixUnitaire"].Value = tpD[i].dbPxUni;
                    Row1.Cells["MontantLigne"].Value = tpD[i].dbTot;
                    Row1.Cells["TVA"].Value = tpD[i].dbTaux;
                    Row1.Cells["Compte"].Value = tpD[i].sCptTva + "";
                    Row1.Cells["Compte7"].Value = tpD[i].sCpt7 + "";
                    Row1.Cells["AnaActivite"].Value = cDefautActivite;
                    Row1.Update();
                    SSOleDBGrid1.UpdateData();
                }

                //=== mise en commentaire le 29 06 2020.
                //fc_GetRachatCEE(sNofacture, false);

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetAccompte");

            }
        }
        /// <summary>
        /// Tested
        /// Not used in version V28.06.23020
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        private void fc_GetAccompte_27062020(int lNumficheStandard)
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            double dbQte;
            double dbHT;
            double dbPxUnit;
            Deduction[] tpD = null;
            int i = 0;
            string sNofacture;
            string stemp = "";
            int lNbSituation = 0;
            bool bFirst;

            try
            {


                sSQL = "SELECT      FactureManuelleDetail.Article,  Intervention.NumFicheStandard, Intervention.NoFacture, FactureManuelleDetail.MontantLigne AS HT,"
                        + " FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA, "
                        + " FactureManuelleDetail.PrixUnitaire AS PxUnit, FactureManuelleDetail.CleFactureManuelle, FactureManuelleDetail.Quantite Qte";

                sSQL = sSQL + " FROM            FactureManuelleDetail INNER JOIN"
                            + " FactureManuelleEnTete ON FactureManuelleDetail.CleFactureManuelle = FactureManuelleEnTete.CleFactureManuelle INNER JOIN "
                            + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture  ";


                sSQL = sSQL + " WHERE        (FactureManuelleDetail.Article <> '" + cArticleCee + "')";


                //sSQL = sSQL + "GROUP BY Intervention.NumFicheStandard, Intervention.NoFacture, FactureManuelleDetail.CleFactureManuelle,"
                //            + " FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA, "
                //            + " FactureManuelleEnTete.DateFacture , FactureManuelleDetail.PrixUnitaire, FactureManuelleDetail.Article ";

                sSQL = sSQL + "GROUP BY Intervention.NumFicheStandard, Intervention.NoFacture, FactureManuelleDetail.CleFactureManuelle,"
                        + " FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA, "
                        + " FactureManuelleEnTete.DateFacture , FactureManuelleDetail.PrixUnitaire, FactureManuelleDetail.Article, FactureManuelleDetail.MontantLigne "
                        + " ,  FactureManuelleDetail.Quantite ";

                sSQL = sSQL + " HAVING        (Intervention.NoFacture IS NOT NULL) AND (SUM(FactureManuelleDetail.MontantLigne) IS NOT NULL)"
                            + " AND (Intervention.NumFicheStandard = " + lNumficheStandard + ") AND (Intervention.NoFacture <> N'') AND "
                            + " (NOT (Intervention.NoFacture LIKE N'XX%'))";

                sSQL = sSQL + " ORDER BY FactureManuelleEnTete.DateFacture, Intervention.NoFacture DESC, Intervention.NumFicheStandard, "
                            + " FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }

                //'=== comppte le nombre de factures.
                foreach (DataRow r in rs.Rows)
                {
                    if (General.UCase(stemp) != General.UCase(r["NoFacture"]))
                    {
                        lNbSituation = lNbSituation + 1;
                        stemp = r["NoFacture"] + "";
                    }
                }

                i = 0;

                foreach (DataRow r in rs.Rows)
                {
                    Array.Resize(ref tpD, i + 1);
                    dbPxUnit = Convert.ToDouble(General.nz(r["PxUnit"], 0));
                    dbQte = -General.FncArrondir(Convert.ToDouble(General.nz(r["Qte"], 0)), 2);

                    //'If dbPxUnit <> 0 Then
                    //'dbQte = FncArrondir(dbHT / dbPxUnit, 2)
                    //'End If

                    dbHT = General.FncArrondir(dbPxUnit * dbQte, 2);
                    tpD[i].sArticle = r["article"] + "";
                    tpD[i].dbQte = dbQte;
                    tpD[i].dbPxUni = dbPxUnit;
                    //'=== atention pb de centimes.
                    //'tpD(i).dbTot = nz(rs!HT, 0)
                    tpD[i].dbTot = General.FncArrondir(dbQte * dbPxUnit);
                    tpD[i].dbTaux = Convert.ToDouble(General.nz(r["TVA"], 0));
                    tpD[i].sCpt7 = r["compte7"] + "";
                    tpD[i].sCptTva = r["compte"] + "";
                    tpD[i].sNofacture = r["NoFacture"] + "";


                    i = i + 1;


                }
                rs.Dispose();
                rs = null;
                sNofacture = tpD[0].sNofacture;

                var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                Row.Update();
                SSOleDBGrid1.UpdateData();

                for (i = 0; i < tpD.Length; i++)
                {
                    if (General.UCase(sNofacture) != General.UCase(tpD[i].sNofacture))
                    {
                        fc_GetRachatCEE(sNofacture, true);
                        sNofacture = tpD[i].sNofacture;
                        var Row0 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                        Row0.Update();
                        SSOleDBGrid1.UpdateData();
                        lNbSituation = lNbSituation - 1;
                    }

                    var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();

                    if (lNbSituation == 1)
                    {
                        // '=== facture d 'accompte
                        //'SSOleDBGrid1.Columns("Designation").value = "Déduction de la Facture d'acompte n°" & tpD(i).sNofacture & ""
                        Row1.Cells["Designation"].Value = "Déduction de la Facture d'acompte n°" + tpD[i].sNofacture + " - Travaux";

                    }
                    else
                    {
                        Row1.Cells["Designation"].Value = "Déduction de la Facture de situation n°" + lNbSituation + " " + tpD[i].sNofacture + " - Travaux";

                    }
                    Row1.Cells["Article"].Value = tpD[i].sArticle;
                    Row1.Cells["Quantite"].Value = tpD[i].dbQte;
                    Row1.Cells["PrixUnitaire"].Value = tpD[i].dbPxUni;
                    Row1.Cells["MontantLigne"].Value = tpD[i].dbTot;
                    Row1.Cells["TVA"].Value = tpD[i].dbTaux;
                    Row1.Cells["Compte"].Value = tpD[i].sCptTva + "";
                    Row1.Cells["Compte7"].Value = tpD[i].sCpt7 + "";
                    Row1.Cells["AnaActivite"].Value = cDefautActivite;
                    Row1.Update();
                    SSOleDBGrid1.UpdateData();
                }
                fc_GetRachatCEE(sNofacture, false);

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetAccompte");

            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sNofacture"></param>
        /// <param name="bacompte"></param>
        private void fc_GetRachatCEE(string sNofacture, bool bacompte)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetRachatCEE() - Enter In The Function - sNofacture = {sNofacture}, bacompte = {bacompte}");
            //===> Fin Modif Mondir

            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            double dbQte = 0;
            double dbHT = 0;
            double dbPxUnit = 0;
            try
            {
                sSQL = " SELECT        Intervention.NoFacture, SUM(FactureManuelleDetail.MontantLigne) AS HT,"
                   + " FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, FactureManuelleDetail.TVA,"
                   + " FactureManuelleDetail.PrixUnitaire AS PxUnit, "
                   + " SUM(FactureManuelleDetail.Quantite) AS Qte ";

                sSQL = sSQL + " FROM            FactureManuelleDetail INNER JOIN "
                        + " FactureManuelleEnTete ON FactureManuelleDetail.CleFactureManuelle = FactureManuelleEnTete.CleFactureManuelle INNER JOIN "
                        + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture ";

                sSQL = sSQL + " WHERE        (FactureManuelleDetail.Article = '" + cArticleCee + "') "
                                    ;
                sSQL = sSQL + " GROUP BY Intervention.NoFacture, FactureManuelleDetail.Compte7, FactureManuelleDetail.Compte, "
                        + " FactureManuelleDetail.TVA, FactureManuelleDetail.PrixUnitaire ";

                sSQL = sSQL + " HAVING        (Intervention.NoFacture = '" + sNofacture + "') AND (SUM(FactureManuelleDetail.MontantLigne) IS NOT NULL)";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    dbPxUnit = General.FncArrondir(Convert.ToDouble(General.nz(rs.Rows[0]["PxUnit"], 0)), 2);

                    //'If dbPxUnit <> 0 Then
                    //'        dbQte = dbHT / dbPxUnit
                    //'End If
                    dbQte = -General.FncArrondir(Convert.ToDouble(General.nz(rs.Rows[0]["Qte"], 0)), 0);


                    dbHT = General.FncArrondir(dbPxUnit * dbQte);


                    var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row.Cells["Article"].Value = cArticleCee;
                    Row.Cells["Designation"].Value = "Rachat des CEE";
                    Row.Cells["Quantite"].Value = dbQte;
                    Row.Cells["PrixUnitaire"].Value = dbPxUnit;
                    Row.Cells["MontantLigne"].Value = dbHT;
                    Row.Cells["TVA"].Value = General.nz(rs.Rows[0]["TVA"], 0);
                    Row.Cells["Compte"].Value = rs.Rows[0]["Compte"] + "";
                    Row.Cells["Compte7"].Value = rs.Rows[0]["Compte7"] + "";
                    Row.Cells["AnaActivite"].Value = cDefautActivite;
                    Row.Update();
                    SSOleDBGrid1.UpdateData();

                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_GetRachatCEE");

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        /// <returns></returns>
        private int fc_CtrlSiAcompte(int lNumficheStandard)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlSiAcompte() - Enter In The Function - lNumficheStandard = {lNumficheStandard}");
            //===> Fin Modif Mondir

            int returnFunctionValue = 0;
            string sSQL = "";

            try
            {
                //'=== si l'utilisateur a choisi d'établir une facture acompte/situation,
                //'=== controle si il existe deja une facture, si aucune facture, il s'agit d'un acompte
                //'=== retouren le nombre de facture.
                //==> Modif Ahmed le 02.09.2020 pour integrer les changements du 01.09.2020
                //Old query:
                //sSQL = " SELECT    count(  FactureManuelleEnTete.NoFacture) as Tot "
                //       + " FROM            FactureManuelleEnTete INNER JOIN "
                //        + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture "
                //        + " WHERE        (NOT (FactureManuelleEnTete.NoFacture LIKE N'XX%')) "
                //        + " AND (Intervention.NumFicheStandard = " + lNumficheStandard + ")";

                sSQL = " SELECT   DISTINCT FactureManuelleEnTete.NoFacture "
                    + " FROM            FactureManuelleEnTete INNER JOIN "
                    + " Intervention ON FactureManuelleEnTete.NoFacture = Intervention.NoFacture "
                    + " WHERE        (NOT (FactureManuelleEnTete.NoFacture LIKE N'XX%')) "
                    + " AND (Intervention.NumFicheStandard = " + lNumficheStandard + ")";

                using (var tmpAdo = new ModAdo())
                {
                    // sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                    var rs = tmpAdo.fc_OpenRecordSet(sSQL);
                    if (rs != null) returnFunctionValue = rs.Rows.Count;
                    tmpAdo.Dispose();

                }
                return returnFunctionValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CtrlSiAcompte");
                return returnFunctionValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="dbTemp"></param>
        /// <returns></returns>
        private double fc_CalParDemiHeure(double dbTemp)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CalParDemiHeure() - Enter In The Function - dbTemp = {dbTemp}");
            //===> Fin Modif Mondir

            double returnFunctionValue = 0;
            double dbDecimal = 0;
            double dbNbDemiHeure = 0;
            double dbTot = 0;

            //'===dtemp contient une valeur des heures en  bases cent.

            try
            {
                //'=== decoupe par demi heure ( 1 = 1 heure, 0.5 = une demi heure)
                dbNbDemiHeure = dbTemp / 0.5;

                dbTot = (int)dbNbDemiHeure * 0.5;

                dbDecimal = (int)dbNbDemiHeure - dbNbDemiHeure;
                dbDecimal = Math.Abs(dbDecimal);
                if (dbDecimal > 0)
                {
                    dbTot = dbTot + 0.5;
                }

                returnFunctionValue = dbTot;
                return returnFunctionValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CalParDemiHeure");
                return returnFunctionValue;
            }
        }

        /// <summary>
        /// Modif de la version V15.09.2020 Ajoutée par Mondir le 17.09.2020
        /// Modif de la version V28.06.2020 Ajoutée par Mondir le 28.06.2020
        /// Modif de la version V28.06.2020 Testée par Mondir le 28.06.2020
        /// Modif de l aversion V02.07.2020V2 Ajoutées par Mondir le 02.07.2020
        /// Modif de l aversion V02.07.2020V2 testées par Mondir le 02.07.2020
        /// </summary>
        /// <param name="sTypeTva"></param>
        /// <param name="lTypeFacture"></param>
        private void fc_InsertCorpTravauxV2(string sTypeTva, int lTypeFacture)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InsertCorpTravauxV2() - Enter In The Function - sTypeTva = {sTypeTva}, lTypeFacture = {lTypeFacture}");
            //===> Fin Modif Mondir

            int k = 0;
            string sWhere;
            string sSQL;
            string sCodeMO;
            string sTempCodeMO;

            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            DataTable rsArt = new DataTable();
            ModAdo rsArtAdo = new ModAdo();
            string sDateReal;
            DateTime dtTemp = new DateTime();
            bool bFirst;
            string sDureeCumul;
            CumulDuree[] tpCumulDuree = null;

            int i = 0;
            string sWhereBon;
            string[] sTab;
            int X;
            string sCG_Num = "";
            string sCodeDEP;
            double dbQte;
            double dbTotal;
            int lNumficheStandard;
            double dbTauxAcc = 0;
            string sCptTVAAcc = "";

            //===> Mondir : Modifs de la version V28.06.2020
            int lNumSav = 0;
            int[] tNumFiche = null;
            int lx1;
            //===> Fin modfis Mondir
            //===> Mondir : Modif de la version V15.09.2020 de VB6
            string sdate = "";
            //===> Fin modfis Mondir

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            int lNumSaut;
            string sOSmultiInterv = "";
            //===> Fin Modif Mondir

            try
            {
                sWhere = "";
                sWhereBon = "";
                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                lNumSaut = 0;
                //===> Fin Modif Mondir
                for (k = 0; k < SSOleDBGrid3.Rows.Count; k++)
                {

                    if (SSOleDBGrid3.Rows[k].Cells["Choix"].Value.ToString() == "Oui")
                    {
                        if (sWhere == "")
                        {
                            sWhere = " WHERE intervention.NoIntervention = " + General.nz(SSOleDBGrid3.Rows[k].Cells["nointervention"].Value, 0);
                        }
                        else
                        {
                            sWhere = sWhere + " OR intervention.NoIntervention = " + General.nz(SSOleDBGrid3.Rows[k].Cells["nointervention"].Value, 0);
                        }
                    }
                }
                SSTab1.SelectedTab = SSTab1.Tabs[1];
                sSQL = "SELECT     GestionStandard.RefOS, GestionStandard.DateOS, Intervention.DateRealise,Intervention.datesaisie, "
                        + " Intervention.Deplacement, Intervention.HeureDebut, "
                        + " Intervention.HeureFin , Intervention.Duree, Intervention.NoIntervention,  "
                        + " Intervention.NumFicheStandard "
                        + " FROM         GestionStandard INNER JOIN "
                        + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard";


                sSQL = sSQL + sWhere;

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020, change Intervention.DateRealise DESC to asc
                sSQL = sSQL + " ORDER BY Intervention.DateRealise asc, Intervention.datesaisie, Intervention.HeureDebut ";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                bFirst = true;
                sTempCodeMO = "";
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;
                }
                else
                {
                    //ReDim Preserve tNumFiche(lx1)
                    lNumficheStandard = Convert.ToInt32(General.nz(rs.Rows[0]["NumFicheStandard"], 0));
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //=== retourne le texte OS si multi fiche d'appel.
                sOSmultiInterv = fc_GetOsMultiIntervention(sWhere);
                if (!string.IsNullOrEmpty(sOSmultiInterv))
                {
                    var dt = SSOleDBGrid1.DataSource as DataTable;
                    var NewRow = dt.NewRow();
                    NewRow["Designation"] = sOSmultiInterv;
                    dt.Rows.Add(NewRow);
                    SSOleDBGrid1.UpdateData();
                }

                //===> Mondir : Modifs de la version V28.06.2020
                lx1 = 0;
                //===> Fin Modifs Mondir

                foreach (DataRow r in rs.Rows)
                {
                    if (bFirst == true)
                    {
                        //'==== Ajout de l'OS.
                        //====> Old before V28.06.2020 if (General.nz(r["RefOS"], "").ToString() != "")
                        //===> Mondir: Modifs de la version V28.06.2020
                        //if (General.nz(r["RefOS"], "").ToString() != "" && lNumSav != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0)))
                        //===> Ahmed : Modification le 08 09 2020 V 07 09 2020 
                        Program.SaveException(null, $"fc_InsertCorpTravauxV2() - RefOS={General.nz(r["RefOS"], "")} - DateOS={ General.nz(r["DateOS"], "").ToString()} -" +
                                                    $" lNumSav={lNumSav} -NumFicheStandard={Convert.ToInt32(General.nz(r["NumFicheStandard"], 0))} ");

                        //===> Mondir le 17.09.2020, this line commented in V15.09.2020
                        //if ((General.nz(r["RefOS"], "").ToString() != "" || General.nz(r["DateOS"], "").ToString() != "")
                        //    && lNumSav != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0)))
                        //===> Fin Modif

                        //===> Mondir le 17.09.2020 pour ajouter les modifs de la version V15.09.2020 de VB6
                        sdate = General.nz(r["DateOS"], "").ToString();
                        if (sdate.Contains("1900") || sdate.Contains("1899"))
                        {
                            sdate = "";
                        }
                        //===> Fin Modif

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        //===> Ajout du string.IsNullOrEmpty(sOSmultiInterv) à la condition
                        if ((General.nz(r["RefOS"], "").ToString() != "" || sdate != "")
                            && lNumSav != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0))
                            && string.IsNullOrEmpty(sOSmultiInterv))
                        {
                            //===> Mondir: Modifs de la version V28.06.2020
                            lNumSav = Convert.ToInt32(General.nz(r["NumFicheStandard"], 0));

                            Array.Resize(ref tNumFiche, lx1 + 1);
                            tNumFiche[lx1] = lNumSav;
                            lx1 = lx1 + 1;
                            //===> Fin Modifs Mondir

                            var Row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                            //===> Ajout de la condition en bas
                            if (!string.IsNullOrEmpty(sdate))
                            {
                                Row.Cells["Designation"].Value = "Travaux réalisés selon votre accord " + r["RefOS"] + " du " + Convert.ToDateTime(r["DateOS"].ToString()).ToShortDateString();
                            }
                            else
                            {
                                Row.Cells["Designation"].Value = "Travaux réalisés selon votre accord " + r["RefOS"];
                            }

                            Row.Update();
                            SSOleDBGrid1.UpdateData();
                        }
                        //===> Mondir: Modifs de la version V28.06.2020
                        else if (lNumSav != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0)))
                        {
                            lNumSav = Convert.ToInt32(General.nz(r["NumFicheStandard"], 0));
                            Array.Resize(ref tNumFiche, lx1 + 1);
                            tNumFiche[lx1] = lNumSav;
                            lx1 = lx1 + 1;

                        }
                        //===> Fin Modifs Mondir

                        //'=== fcature d'acompte.
                        if (lTypeFacture == 1)//===>Tested
                        {
                            //==> Ahmed: Modif 02.09.2020 pour integrer les changements du V01.09.2020
                            //old:
                            //if (fc_CtrlSiAcompte(lNumficheStandard) == 0) 
                            if (fc_CtrlSiAcompte(tNumFiche[lx1 - 1]) == 0)
                            //==> Fin modifs Ahmed
                            {
                                //'=== fature d'accompte.


                                //            '.AddNew
                                //            '.Columns("Designation").value = "facture d'acompte"
                                //            '.Columns("quantite").value = 1
                                //            '.Columns("PrixUnitaire").value = dbAccompteInterv
                                //            '.Columns("MontantLigne").value = dbAccompteInterv
                                //            '.Columns("AnaActivite").value = cDefautActivite
                                //            '.Update

                                sSQL = "";
                                DataTable dt = ((SSOleDBGrid1.DataSource) as DataTable);
                                var newRow = dt.NewRow();
                                dt.Rows.Add(newRow);
                                SSOleDBGrid1.UpdateData();
                                //.AddItem sSQL
                                //.MoveNext

                                newRow = dt.NewRow();
                                sSQL =
                                    "SELECT     CodeArticle, Designation1, CG_Num, CG_Num2, CG_Intitule, CG_Intitule2, totalHT, PrixAchat "
                                    + " From FacArticle "
                                    + " WHERE     CodeArticle = '" + cArticleAcc + "'";
                                rsArt = rsArtAdo.fc_OpenRecordSet(sSQL);
                                if (rsArt.Rows.Count > 0)
                                {
                                    if (General.UCase(sTypeTva) == General.UCase("TR"))
                                    {
                                        sCG_Num = rsArt.Rows[0]["CG_NUM"] + "";
                                    }
                                    else
                                    {
                                        sCG_Num = rsArt.Rows[0]["CG_Num2"] + "";
                                    }

                                    rsArt.Dispose();
                                    rsArt = null;

                                    SAGE.fc_OpenConnSage();

                                    //'=== recherche du taux et du compte de TVA.
                                    sSQL =
                                        "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                                        + " WHERE (F_COMPTEG.CG_NUM ='" + sCG_Num + "')  "
                                        + "  ORDER BY F_COMPTEG.CG_NUM";

                                    rsArt = rsArtAdo.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                                    if (rsArt.Rows.Count > 0)
                                    {
                                        dbTauxAcc = Convert.ToDouble(General.nz(rsArt.Rows[0]["TA_Taux"], 0));
                                        sCptTVAAcc = rsArt.Rows[0]["CG_NUM"] + "";
                                    }

                                    //sSQL = "!!;!!;!!;!" & cArticleAcc & "!;" _
                                    //                        & "!" & "facture d'acompte!;!" & 1 & "!;!!;!" & dbAccompteInterv & "!;!" & dbAccompteInterv & "!;" _
                                    //                        & "!" & dbTauxAcc & "!;" & "!" & sCptTVAAcc & "!;" & "!" & sCG_Num & "!;" & cDefautActivite



                                    newRow["Article"] = cArticleAcc;
                                    newRow["Designation"] = "facture d'acompte";
                                    newRow["Quantite"] = 1;
                                    newRow["TVA"] = dbTauxAcc;
                                    newRow["compte"] = sCptTVAAcc;
                                    newRow["PrixUnitaire"] = Variable.dbAccompteInterv;
                                    newRow["MontantLigne"] = Variable.dbAccompteInterv;
                                    newRow["compte7"] = sCG_Num;
                                    newRow["AnaActivite"] = cDefautActivite;
                                    sCG_Num = "";
                                }
                                else
                                {
                                    //sSQL = "!!;!!;!!;!!;" _
                                    //                        & "!" & "facture d'acompte!;!" & 1 & "!;!!;!" & dbAccompteInterv & "!;!" & dbAccompteInterv & "!;" _
                                    //                        & "!!;" & "!!;" & "!!;" & cDefautActivite
                                    newRow["Designation"] = "facture d'acompte";
                                    newRow["Quantite"] = 1;
                                    newRow["PrixUnitaire"] = Variable.dbAccompteInterv;
                                    newRow["MontantLigne"] = Variable.dbAccompteInterv;
                                    newRow["AnaActivite"] = cDefautActivite;
                                }

                                dt.Rows.Add(newRow.ItemArray);
                                SSOleDBGrid1.UpdateData();


                                rsArt.Dispose();
                                rsArt = null;

                                //.AddItem sSQL
                                // .MoveNext

                                rs.Dispose();
                                rs = null;
                                return;
                            }
                        }
                    }

                    //'=== ne pas oublier de gerer les jours fériées
                    sCodeMO = fc_GetArticleMO((General.nz(r["DateRealise"], "")).ToString(), (General.nz(r["HeureDebut"], "").ToString()));
                    if (General.UCase(sCodeMO) == General.UCase(cMOTPNUIT))
                    {
                        sCodeDEP = cMODNuit;
                    }
                    else
                    {
                        sCodeDEP = cMODS;
                    }

                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    //===> Ajout du lNumSaut != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0))
                    if (dtTemp != Convert.ToDateTime(General.nz(r["DateRealise"], r["DateSaisie"]))
                        || General.UCase(sTempCodeMO) != General.UCase(sCodeMO)
                        || lNumSaut != Convert.ToInt32(General.nz(r["NumFicheStandard"], 0)))
                    {
                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        lNumSaut = Convert.ToInt32(General.nz(r["NumFicheStandard"], 0));
                        //===> Fin Modif Mondir

                        dtTemp = Convert.ToDateTime(General.nz(r["DateRealise"], r["DateSaisie"]));
                        sTempCodeMO = sCodeMO;

                        Array.Resize(ref tpCumulDuree, i + 1);
                        i = i + 1;
                        tpCumulDuree[i - 1].sdate = dtTemp.ToShortDateString(); //' CStr(rs!DateRealise);
                        tpCumulDuree[i - 1].sCodeMO = sCodeMO;
                        tpCumulDuree[i - 1].sCodeDEP = sCodeDEP;

                        //'=== main d'oeuvre.
                        sSQL = "SELECT     CodeArticle, Designation1, CG_Num, CG_Num2, CG_Intitule, CG_Intitule2, totalHT, PrixAchat "
                                   + " From FacArticle "
                                   + " WHERE     CodeArticle = '" + sCodeMO + "'";
                        rsArt = rsArtAdo.fc_OpenRecordSet(sSQL);

                        if (rsArt.Rows.Count > 0)//Tested
                        {
                            tpCumulDuree[i - 1].sLibelleArtMO = rsArt.Rows[0]["Designation1"].ToString();
                            if (General.UCase(sTypeTva) == General.UCase("TR"))//tested
                            {
                                //'tpCumulDuree(i - 1).dbPrixUnitaireMo = nz(rsArt!TotalHT, 0)
                                tpCumulDuree[i - 1].dbPrixUnitaireMo = Convert.ToDouble(General.nz(rsArt.Rows[0]["PrixAchat"], 0));
                                tpCumulDuree[i - 1].sCpteVente = rsArt.Rows[0]["CG_NUM"] + "";
                                sCG_Num = rsArt.Rows[0]["CG_NUM"] + "";
                            }
                            else
                            {
                                tpCumulDuree[i - 1].dbPrixUnitaireMo = Convert.ToDouble(General.nz(rsArt.Rows[0]["PrixAchat"], 0));
                                tpCumulDuree[i - 1].sCpteVente = rsArt.Rows[0]["CG_Num2"] + "";
                                sCG_Num = rsArt.Rows[0]["CG_Num2"] + "";
                            }

                            rsArt.Dispose();
                            rsArt = null;

                            SAGE.fc_OpenConnSage();

                            //'=== recherche du taux et du compte de TVA.
                            sSQL = "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                                + " WHERE (F_COMPTEG.CG_NUM ='" + sCG_Num + "')  "
                                + "  ORDER BY F_COMPTEG.CG_NUM";
                            rsArt = rsArtAdo.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                            if (rsArt.Rows.Count > 0)//Tested
                            {
                                tpCumulDuree[i - 1].dbTaux = Convert.ToInt32(General.nz(rsArt.Rows[0]["TA_Taux"], 0));
                                tpCumulDuree[i - 1].sCpteTVA = rsArt.Rows[0]["CG_NUM"] + "";
                            }
                        }

                        rsArt.Dispose();
                        rsArt = null;

                        // '=== deplacement.
                        sSQL = "SELECT     CodeArticle, Designation1, CG_Num, CG_Num2, CG_Intitule, CG_Intitule2, totalHT, PrixAchat "
                           + " From FacArticle "
                           + " WHERE     CodeArticle = '" + sCodeDEP + "'";

                        rsArt = rsArtAdo.fc_OpenRecordSet(sSQL);
                        if (rsArt.Rows.Count > 0)
                        {
                            tpCumulDuree[i - 1].sLibelleArtDEP = rsArt.Rows[0]["Designation1"].ToString();

                            if (General.UCase(sTypeTva) == General.UCase("TR"))//Tested
                            {
                                //'tpCumulDuree(i - 1).dbPrixUnitaireMo = nz(rsArt!TotalHT, 0)
                                tpCumulDuree[i - 1].dbPrixUnitaireDep = Convert.ToDouble(General.nz(rsArt.Rows[0]["PrixAchat"], 0));
                                tpCumulDuree[i - 1].sCpteVenteDep = rsArt.Rows[0]["CG_NUM"] + "";
                                sCG_Num = rsArt.Rows[0]["CG_NUM"] + "";
                            }
                            else
                            {
                                tpCumulDuree[i - 1].dbPrixUnitaireDep = Convert.ToDouble(General.nz(rsArt.Rows[0]["PrixAchat"], 0));
                                tpCumulDuree[i - 1].sCpteVenteDep = rsArt.Rows[0]["CG_Num2"] + "";
                                sCG_Num = rsArt.Rows[0]["CG_Num2"] + "";
                            }
                            rsArt.Dispose();
                            rsArt = null;

                            SAGE.fc_OpenConnSage();

                            //'=== recherche du taux et du compte de TVA.
                            sSQL = "SELECT F_COMPTEG.CG_NUM AS CG_Num1, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                                              + " WHERE (F_COMPTEG.CG_NUM ='" + sCG_Num + "')  "
                                              + "  ORDER BY F_COMPTEG.CG_NUM";


                            rsArt = rsArtAdo.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                            if (rsArt.Rows.Count > 0)//Tested
                            {
                                tpCumulDuree[i - 1].dbTauxDep = Convert.ToInt32(General.nz(rsArt.Rows[0]["TA_Taux"], 0));
                                tpCumulDuree[i - 1].sCpteTVADep = rsArt.Rows[0]["CG_NUM"] + "";
                            }
                        }

                        rsArt.Dispose();
                        rsArt = null;
                    }

                    tpCumulDuree[i - 1].sNoInter = tpCumulDuree[i - 1].sNoInter + r["NoIntervention"] + "-";
                    sDureeCumul = "";

                    //if (General.IsDate(r["HeureDebut"].ToString()) && General.IsDate(r["HeureFin"].ToString()))//Tested
                    //{

                    //====> Modif le 27.07.2020  Salma
                    sDureeCumul = General.nz(General.fc_GetHourIntSeconde(Convert.ToDateTime(General.nz(r["HeureDebut"], new DateTime(1900, 01, 01, 00, 00, 00))), Convert.ToDateTime(General.nz(r["HeureFin"], new DateTime(1900, 01, 01, 00, 00, 00)))), "0").ToString();
                    //Fin Modif le 27.07.2020 

                    //}
                    //else if (General.IsDate(r["Duree"].ToString()))
                    //{
                    //}

                    ///====> Mondir le 02.07.2020, modif de la version 02.07.2020 V2
                    if (General.IsDate(r["Duree"]?.ToString()))
                    {
                        var tmpdate = Convert.ToDateTime("01/01/1900 00:00:00").TimeOfDay;
                        var dtDuree = Convert.ToDateTime(r["Duree"]).TimeOfDay;

                        sDureeCumul = (dtDuree - tmpdate).TotalSeconds.ToString();
                    }
                    ///====> Fin Modif Mondir
                    tpCumulDuree[i - 1].sDuree = (Convert.ToInt32(General.nz(tpCumulDuree[i - 1].sDuree, "0")) + Convert.ToInt32(General.nz(sDureeCumul, "0"))).ToString();
                    //'tpCumulDuree(i - 1).sDuree = CStr(fc_GetSecondToHourBase100(CDbl(tpCumulDuree(i - 1).sDuree)))
                }

                rs.Dispose();
                rs = null;

                //'If lTypeFacture = 1 Then
                //'        Exit Sub
                //'End If
                for (i = 0; i < tpCumulDuree.Length; i++)
                {
                    var Row0 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row0.Update();
                    SSOleDBGrid1.UpdateData();

                    var Row1 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row1.Cells["Designation"].Value = "Intervention du " + tpCumulDuree[i].sdate;
                    Row1.Update();
                    SSOleDBGrid1.UpdateData();

                    sWhereBon = "";
                    sTab = tpCumulDuree[i].sNoInter.Split('-');

                    for (X = 0; X < sTab.Length; X++)//Tested
                    {
                        if (sTab[X] != null && sTab[X] != "")
                        {
                            if (sWhereBon == "")
                            {
                                sWhereBon = "  WHERE (BonDeCommande.NoIntervention= " + General.nz(sTab[X], 0)
                                  + " AND BCD_Detail.BCD_Quantite<>'' AND NOT BCD_Quantite IS NULL AND ISNUMERIC(BCD_Quantite)=1) ";
                            }
                            else
                            {
                                sWhereBon = sWhereBon + "  OR (BonDeCommande.NoIntervention= " + General.nz(sTab[X], 0)
                                   + " AND BCD_Detail.BCD_Quantite<>'' AND NOT BCD_Quantite IS NULL AND ISNUMERIC(BCD_Quantite)=1) ";

                            }
                        }
                    }

                    fc_GetAchat(1, sWhereBon);
                    var Row2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row2.Update();
                    SSOleDBGrid1.UpdateData();

                    //=== main d'oeuvre
                    var Row3 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row3.Cells["Article"].Value = tpCumulDuree[i].sCodeMO;
                    Row3.Cells["Designation"].Value = tpCumulDuree[i].sLibelleArtMO;   //' "Tarif horaire simple"

                    dbQte = fDate.fc_GetSecondToHourBase100(Convert.ToDouble(tpCumulDuree[i].sDuree));//tested
                    //'=== une demi heure entamée est due.
                    dbQte = fc_CalParDemiHeure(dbQte);//tested

                    //====> Mondir : Modif de la version V28.06.2020
                    Row3.Cells["quantite"].Value = dbQte;
                    Row3.Cells["PrixUnitaire"].Value = General.FncArrondir(tpCumulDuree[i].dbPrixUnitaireMo, 2);
                    dbTotal = General.FncArrondir(dbQte * General.FncArrondir(tpCumulDuree[i].dbPrixUnitaireMo), 2);
                    //====> Fin Modif Mondir

                    Row3.Cells["MontantLigne"].Value = dbTotal;

                    Row3.Cells["TVA"].Value = tpCumulDuree[i].dbTaux;
                    Row3.Cells["Compte"].Value = tpCumulDuree[i].sCpteTVA;
                    Row3.Cells["Compte7"].Value = tpCumulDuree[i].sCpteVente;
                    Row3.Cells["AnaActivite"].Value = cDefautActivite;
                    //'NumeroLigne
                    Row3.Update();
                    SSOleDBGrid1.UpdateData();

                    //'=== deplacement.
                    var Row4 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                    Row4.Cells["Article"].Value = tpCumulDuree[i].sCodeDEP;
                    Row4.Cells["Designation"].Value = tpCumulDuree[i].sLibelleArtDEP; //"Traif horaire simple"

                    dbQte = 1;
                    Row4.Cells["quantite"].Value = dbQte;

                    //====> Mondir : Modif de la version V28.06.2020
                    Row4.Cells["PrixUnitaire"].Value = General.FncArrondir(tpCumulDuree[i].dbPrixUnitaireDep, 2);
                    dbTotal = General.FncArrondir(dbQte * General.FncArrondir(tpCumulDuree[i].dbPrixUnitaireDep, 2), 2);
                    //====> Fin Modif Mondir

                    Row4.Cells["MontantLigne"].Value = dbTotal;

                    Row4.Cells["TVA"].Value = tpCumulDuree[i].dbTauxDep;
                    Row4.Cells["Compte"].Value = tpCumulDuree[i].sCpteTVADep;
                    Row4.Cells["Compte7"].Value = tpCumulDuree[i].sCpteVenteDep;
                    Row4.Cells["AnaActivite"].Value = cDefautActivite;

                    //'SSOleDBGrid1.Columns("AnaActivite").value = tpCumulDuree(i)
                    //'NumeroLigne
                    Row4.Update();
                    SSOleDBGrid1.UpdateData();

                }

                //'=== facture d'accompte.

                //'''fc_GetAccomte lNumficheStandard
                //'=== modif du 01/04/2020, les deductions des factures d'acompte sont reprises depuis la table
                //'=== facture manuelle et non du montant acompte de l'intervention.
                //======> Old before V28.06.2020 fc_GetAccompte(lNumficheStandard);

                //====> Mondir : Modif de la version V28.06.2020
                for (lx1 = 0; lx1 < tNumFiche.Length; lx1++)
                {
                    fc_GetAccompte(tNumFiche[lx1]);
                }
                //====> Fin Modif Mondir

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //===> La ligne en bas est ajoutée dans cette version mais on en a pas besoin
                //SSOleDBGrid1.MoveFirst

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + "fc_InsertCorpV2");
            }
        }
        private void fc_LockCpt()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LockCpt() - Enter In The Function");
            //===> Fin Modif Mondir

            if (General.sCptBloqueSurFacture == "1")
            {
                if (TypeFacture == "TA")
                {
                    if (SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Exists("Compte7"))
                    {
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].CellActivation = Activation.Disabled;
                        if (SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Exists("Compte"))
                            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].CellAppearance.BackColor = SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte"].CellAppearance.BackColor;
                        SSOleDBDropDown5.Enabled = false;
                    }
                }
                else
                {
                    if (SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Exists("Compte7"))
                    {
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].CellActivation = Activation.AllowEdit;
                        if (SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Exists("Compte"))
                            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Compte7"].CellAppearance.BackColor = Color.White;
                        SSOleDBDropDown5.Enabled = true;
                    }
                }
            }
        }

        /// <summary>
        /// TESTED
        /// ===> Mondir le 17.09.2020 pour ajouté les modifs de la version VB6 15.09.2020
        /// </summary>
        private void BisRechercheGecet()
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"BisRechercheGecet() - Enter In The Function");
            //===> Fin Modif Mondir

            bool boolLastDesignation, bPrixAchat;
            bPrixAchat = false;
            double dbmontant = 0;

            //===> Mondir le 17.09.2020 pour ajouté les modifs de la version VB615.09.2020
            double dbMontantCalcule = 0;
            double dbQte = 0;
            double dbPrixUnit = 0;
            //===> Fin Modfi Mondir

            DataTable rsPanier;
            string sR;
            rsPanier = new DataTable();
            sR = "SELECT * FROM PanierGecet WHERE Utilisateur='" + General.fncUserName() + "' ORDER BY CleAuto DESC";
            try
            {
                var ado = new ModAdo();
                rsPanier = ado.fc_OpenRecordSet(sR);
                if (rsPanier.Rows.Count > 0)
                {
                    i = 1;
                    var data = SSOleDBGrid1.DataSource as DataTable;
                    var bookMark = SSOleDBGrid1.ActiveRow != null ? SSOleDBGrid1.ActiveRow.Index : data.Rows.Count;
                    foreach (DataRow dr in rsPanier.Rows)
                    {
                        var row = data.NewRow();
                        if (boolIntervention && boolInsertionArticle)
                        {
                            row["NumeroLigne"] = DBNull.Value;
                            row["Poste"] = DBNull.Value;
                            row["CleFactureManuelle"] = DBNull.Value;
                            row["Article"] = General.sArticleAchat;
                            row["Designation"] = strIntervention;
                        }
                        else
                        {
                            row["NumeroLigne"] = DBNull.Value;
                            row["Poste"] = DBNull.Value;
                            row["CleFactureManuelle"] = DBNull.Value;
                            row["Article"] = General.sArticleAchat;
                            row["Designation"] = dr["Designation"];
                        }

                        //===> Mondir le 17.09.2020 pour les modif de VB6 V15.09.2020
                        dbQte = Convert.ToDouble(General.nz(dr["Quantite"], 0));
                        row["Quantite"] = dbQte;
                        //===> Fin Modif Mondir

                        row["Unite"] = DBNull.Value;

                        if (General.IsNumeric(dr["PxVenteFOSC"] + ""))
                        {
                            if (General.sUseCoefGecet == "1")
                            {
                                //=== mise en commenatire le 10/09/2020
                                //row["PrixUnitaire"] = Convert.ToDouble(dr["PxVenteFOSC"]) * General.dbGenCoefGecet;

                                //====> Mohammed 09.07.02020 ====> Modifs of V09.07.02020
                                dbmontant = Convert.ToDouble(dr["PxVenteFOSC"]) * General.dbGenCoefGecet;
                                // end Modifs  V09.07.02020
                            }
                            else
                            {
                                //=== mise en commenatire le 10/09/2020
                                //row["PrixUnitaire"] = dr["PxVenteFOSC"];

                                //====> Mohammed 09.07.02020 ====> Modifs of V09.07.02020
                                dbmontant = Convert.ToDouble(General.nz(dr["PxVenteFOSC"], 0));
                                // end Modifs  V09.07.02020
                            }
                            bPrixAchat = true;
                            //===> Mondir le 17.09.2020 pour ajouiter les modif de la version 15.09.2020
                            dbMontantCalcule = fc_GetFormule(dbmontant, Convert.ToDouble(General.nz(dr["Quantite"], 0)),
                                boolAddrow, 0, false);
                            if (dbQte != 0)
                            {
                                dbPrixUnit = General.FncArrondir(dbMontantCalcule / dbQte, 2);
                                row["PrixUnitaire"] = dbPrixUnit;
                            }
                            //===> Fin Modif
                        }
                        else
                        {
                            bPrixAchat = false;
                            row["PrixUnitaire"] = DBNull.Value;
                        }

                        //===> Mondir Modif de la version VB6 V15.09.2020
                        row["MontantLigne"] = General.FncArrondir(dbQte * dbPrixUnit, 2);
                        //=== mise en commenatire le 10/09/2020
                        //row["MontantLigne"] = Convert.ToDouble(General.IsNumeric(dr["PxVenteFOSC"] + "") ? General.nz(dr["PxVenteFOSC"], 0) : 0) * General.dbGenCoefGecet * Convert.ToDouble(General.nz(dr["Quantite"], 0));
                        //=== fin mise en commantaire
                        //===> Fin Modif Mondir

                        //'If TestTauxTVA = True Then
                        //'         SQL = SQL & "!" & fc_SelectCa_Num(adotemp!ca_num & "") & "!;"
                        //'Else

                        if (txtTVA.Text == "TP")
                        {
                            row["TVA"] = sTauxTvaAchatTP;
                            row["Compte"] = sCompteTvaAchatTP;
                            row["Compte7"] = sCompte7AchatTP;
                            row["AnaActivite"] = cDefautActivite;
                        }
                        else if (txtTVA.Text == "TR")
                        {
                            row["TVA"] = sTauxTvaAchatTR;
                            row["Compte"] = sCompteTvaAchatTR;
                            row["Compte7"] = sCompte7AchatTR;
                            row["AnaActivite"] = cDefautActivite;
                        }

                        if (boolAddrow)
                        {
                            data.Rows.Add(row);
                            //====> Mohammed 17.09.2020 ====> Modifs of V17.09.02020, added true to fc_GetFormule
                            //====> Mohammed 09.07.02020 ====> Modifs of V09.07.02020
                            fc_GetFormule(dbmontant, Convert.ToDouble(General.nz(dr["Quantite"], 0)), boolAddrow, 0, true);
                            // end Modifs  V09.07.02020
                        }
                        else
                        {
                            //====> Mohammed 17.09.2020 ====> Modifs of V17.09.02020, added true to fc_GetFormule
                            //====> Mohammed 09.07.02020 ====> Modifs of V09.07.02020
                            fc_GetFormule(dbmontant, Convert.ToDouble(General.nz(dr["Quantite"], 0)), boolAddrow, bookMark, true);
                            // end Modifs  V09.07.02020
                            data.Rows.InsertAt(row, bookMark);
                        }
                        i++;
                    }
                }
                ado.Close();
                rsPanier = null;
                SSOleDBGrid1.Update();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";BisRechercheArticle");
            }
        }

        private void txtNoDevis_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"txtNoDevis_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                txtImmeuble_KeyPress(txtImmeuble, new KeyPressEventArgs((char)13));
            }
        }

        private void UserDocFacManuel_Validating(object sender, CancelEventArgs e)
        {

        }

        /// <summary>
        /// ===> Mondir : Pour ajouté les modif de la version 15.09.2020 de VB6 le 17.09.2020
        /// ===================> Mohammed 09.07.2020 ==> function added in Modifs of 09.07.2020
        /// </summary>
        /// <param name="dbmontant"></param>
        /// <param name="dbQte"></param>
        /// <param name="boolAddrow"></param>
        /// <param name="varBoolmark"></param>
        public double fc_GetFormule(double dbmontant, double dbQte, bool boolAddrow, int varBoolmark, bool bAdditem)
        {
            //===> Mondir 03.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetFormule() - Enter In The Function - dbmontant = {dbmontant}, dbQte = {dbQte}, boolAddrow = {boolAddrow}, varBoolmark = {varBoolmark}");
            //===> Fin Modif Mondir

            DataTable rsPrixVente = default(DataTable);
            string sSQL;
            string SQL;
            double dblPrixVente = 0;

            try
            {
                sSQL = "SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente WHERE PalierAchat>=" + (Convert.ToDouble(General.nz(dbmontant, "0")) * Convert.ToDouble(General.nz(dbQte, "0"))) + " ORDER BY PalierAchat ASC";
                var ado = new ModAdo();

                rsPrixVente = ado.fc_OpenRecordSet(sSQL);

                if (rsPrixVente.Rows.Count > 0)
                {
                    if (rsPrixVente.Rows[0]["AfficherFormule"] + "" != "0")
                    {
                        if (bAdditem)
                        {
                            if (boolAddrow == true)
                            {
                                var row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                row.Cells["Designation"].Value =
                                    "Formule utilisée : (" + rsPrixVente.Rows[0]["a"] + "*" +
                                    (Convert.ToDouble(General.nz(dbmontant, 0)) *
                                     Convert.ToDouble(General.nz(dbQte, 0))) + ") + " + rsPrixVente.Rows[0]["b"];
                                row.Update();
                            }
                            else
                            {
                                var data = SSOleDBGrid1.DataSource as DataTable;
                                var row = data.NewRow();
                                row["Designation"] = "Formule utilisée : (" + rsPrixVente.Rows[0]["a"] + "*" +
                                                     (Convert.ToDouble(General.nz(dbmontant, 0)) *
                                                      Convert.ToDouble(General.nz(dbQte, 0))) + ") + " +
                                                     rsPrixVente.Rows[0]["b"];
                                data.Rows.InsertAt(row, varBoolmark);
                                SSOleDBGrid1.UpdateData();
                                SSOleDBGrid1.Update();

                            }
                        }
                    }
                    dblPrixVente = ((Convert.ToDouble(General.nz(dbmontant, 0)) * Convert.ToDouble(General.nz(dbQte, 0))) * Convert.ToDouble(rsPrixVente.Rows[0]["a"])) + Convert.ToDouble(rsPrixVente.Rows[0]["b"]);
                }
                else
                {
                    //===> Mondir le 17.09.2020, this ELSE wasnt integrated from le version V09.07.2020 of VB6
                    rsPrixVente = ado.fc_OpenRecordSet("SELECT PalierAchat,a,b,AfficherFormule FROM ConvertAchatVente " +
                        "WHERE PalierAchat<=" + Convert.ToDouble(General.nz(dbmontant, "0")) * Convert.ToDouble(General.nz(dbmontant, "0")) + " ORDER BY PalierAchat DESC");

                    if (rsPrixVente != null && rsPrixVente.Rows.Count > 0)
                    {
                        if (rsPrixVente.Rows[0]["AfficherFormule"] + "" != "0")
                        {
                            if (bAdditem)
                            {
                                if (boolAddrow == true)
                                {
                                    var row = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                    row.Cells["Designation"].Value =
                                        "Formule utilisée : (" + rsPrixVente.Rows[0]["a"] + "*" +
                                        (Convert.ToDouble(General.nz(dbmontant, 0)) *
                                         Convert.ToDouble(General.nz(dbQte, 0))) + ") + " + rsPrixVente.Rows[0]["b"];
                                    row.Update();
                                }
                                else
                                {
                                    var data = SSOleDBGrid1.DataSource as DataTable;
                                    var row = data.NewRow();
                                    row["Designation"] = "Formule utilisée : (" + rsPrixVente.Rows[0]["a"] + "*" +
                                                         (Convert.ToDouble(General.nz(dbmontant, 0)) *
                                                          Convert.ToDouble(General.nz(dbQte, 0))) + ") + " +
                                                         rsPrixVente.Rows[0]["b"];
                                    data.Rows.InsertAt(row, varBoolmark);
                                    SSOleDBGrid1.UpdateData();
                                    SSOleDBGrid1.Update();

                                }
                            }
                        }
                        dblPrixVente = (Convert.ToDouble(General.nz(dbmontant, 0)) * Convert.ToDouble(General.nz(dbQte, 0)) * Convert.ToDouble(rsPrixVente.Rows[0]["a"])) + Convert.ToDouble(rsPrixVente.Rows[0]["b"]);
                    }
                    else
                    {
                        dblPrixVente = Convert.ToDouble(General.nz(dbmontant, 0)) * Convert.ToDouble(General.nz(dbQte, 0));
                    }
                }
                ado.Close();
                rsPrixVente?.Dispose();
                //===> Mondir : Pour ajouté les modif de la version 15.09.2020
                if (bAdditem)
                {
                    SSOleDBGrid1.UpdateData();
                    SSOleDBGrid1.Update();
                }
                //===> Fin Modif
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_GetFormule");
            }

            return dblPrixVente;
        }

        private void GridInter_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["NumFicheStandard"].Header.Caption = "N°Fiche d'appel";
            e.Layout.Bands[0].Columns["NumFicheStandard"].Header.VisiblePosition = 1;
            e.Layout.Bands[0].Columns["NoIntervention"].Header.Caption = "N° Intervention";
            e.Layout.Bands[0].Columns["NoIntervention"].Header.VisiblePosition = 2;
            e.Layout.Bands[0].Columns["CodeImmeuble"].Header.VisiblePosition = 3;
            e.Layout.Bands[0].Columns["Article"].Header.VisiblePosition = 4;
            e.Layout.Bands[0].Columns["Commentaire"].Header.VisiblePosition = 5;
            e.Layout.Bands[0].Columns["DateSaisie"].Header.Caption = "Date Saisie";
            e.Layout.Bands[0].Columns["DateSaisie"].Header.VisiblePosition = 6;
            e.Layout.Bands[0].Columns["DateRealise"].Header.Caption = "Date Realise";
            e.Layout.Bands[0].Columns["DateRealise"].Header.VisiblePosition = 7;
            e.Layout.Bands[0].Columns["Deplacement"].Hidden = true;
            e.Layout.Bands[0].Columns["HeureDebut"].Header.Caption = "Heure Debut";
            e.Layout.Bands[0].Columns["HeureDebut"].Header.VisiblePosition = 8;
            e.Layout.Bands[0].Columns["HeureFin"].Header.Caption = "Heure Fin";
            e.Layout.Bands[0].Columns["HeureFin"].Header.VisiblePosition = 9;
            e.Layout.Bands[0].Columns["Duree"].Header.VisiblePosition = 10;
            e.Layout.Bands[0].Columns["CodeEtat"].Header.Caption = "Code Etat";
            e.Layout.Bands[0].Columns["CodeEtat"].Header.VisiblePosition = 11;
            e.Layout.Bands[0].Columns["NoDevis"].Header.VisiblePosition = 12;
            e.Layout.Bands[0].Columns["NoFacture"].Header.VisiblePosition = 13;

            e.Layout.Bands[0].Columns["HeureDebut"].Format = "HH:mm:ss";
            e.Layout.Bands[0].Columns["HeureFin"].Format = "HH:mm:ss";
            e.Layout.Bands[0].Columns["Duree"].Format = "HH:mm:ss";

            foreach (UltraGridColumn col in e.Layout.Bands[0].Columns)
                col.CellActivation = Activation.NoEdit;
        }

        /// <summary>
        /// Modif de la version V31.08.2020 ajouté le 31.08.2020
        /// </summary>
        private void fc_LoadInter()
        {
            //===> Mondir 31.08.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadInter() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = "";
            sSQL = "SELECT    NumFicheStandard, NoIntervention, CodeImmeuble, Article, Commentaire,"
                + " DateSaisie, DateRealise, Deplacement, HeureDebut, HeureFin, Duree, CodeEtat, NoDevis, NoFacture"
                + " From Intervention "
                + " WHERE        (NoFacture = '" + StdSQLchaine.gFr_DoublerQuote(txtEnCours.Text) + "') "
                + " ORDER BY DateRealise";
            Cursor = Cursors.WaitCursor;
            rsInterADO = new ModAdo();
            rsInter = rsInterADO.fc_OpenRecordSet(sSQL);

            GridInter.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            GridInter.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.False;

            Cursor = Cursors.Default;
            GridInter.DataSource = rsInter;

        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        private void fc_ADDTVA300(int lCleFactureManuelle, ref bool bLoadGrille)
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ADDTVA300() - Enter In The Function");
            //===> Fin Modif Mondir

            int lNoLigneTva;
            string sSQLTVA = "";
            string sLibArticleTva = "";
            DataTable rsTVA300 = null;
            ModAdo rsTVA300ModAdo = new ModAdo();
            double dbTTCtva300 = 0;
            bool bTVA300 = false;
            DateTime dtTva;

            try
            {
                if (!string.IsNullOrEmpty(General.sTVA300))
                {
                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> testé
                    //=== controle si la catégorie de l'immeuble est sujet à la TVA 300.
                    sSQLTVA = "SELECT        TOP (1) CAT_CategorieImmeuble.CAT_TVA300" +
                         " FROM            CAT_CategorieImmeuble INNER JOIN   Immeuble ON CAT_CategorieImmeuble.CAT_Code = Immeuble.CAT_Code " +
                         "  WHERE        Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                    using (var tmpModAdo = new ModAdo())
                        sSQLTVA = tmpModAdo.fc_ADOlibelle(sSQLTVA);

                    if (sSQLTVA != "1")
                        return;
                    //===> Fin Modif Mondir

                    //=== Controle si il existe une date d'achévement.
                    sSQLTVA = "SELECT   TOP 1     IDA_DateAchevement From IDA_ImmDateAchevement " +
                              " WHERE        Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) +
                              "' ORDER BY IDA_DateAchevement DESC";

                    using (var tmpModAdo = new ModAdo())
                        sSQLTVA = tmpModAdo.fc_ADOlibelle(sSQLTVA);

                    bTVA300 = false;

                    if (!string.IsNullOrEmpty(sSQLTVA))
                    {
                        if (sSQLTVA.IsDate())
                        {
                            dtTva = DateTime.Now.Date.AddYears(-2);

                            //===> Mondir le 29.11.2020 pour ajouter les modifs de la version V29.11.2020 ===> testé
                            //=== this line is commented
                            //if (Convert.ToDateTime(sSQLTVA) >= dtTva)
                            if (Convert.ToDateTime(sSQLTVA) <= dtTva)
                            {
                                bTVA300 = true;
                            }
                        }
                    }

                    if (bTVA300)
                    {
                        //=== controle si le code TVA existe dans la facture manuelle.
                        sSQLTVA = "SELECT   FactureManuelleDetail.article,  FactureManuelleDetail.NumeroLigne,"
                            + " FactureManuellePied.MontantTTC "
                            + " FROM FactureManuelleDetail INNER JOIN"
                            + " FactureManuellePied ON FactureManuelleDetail.CleFactureManuelle = FactureManuellePied.CleFactureManuelle "
                            + " WHERE        FactureManuellePied.CleFactureManuelle = " + lCleFactureManuelle
                            + " ORDER BY NumeroLigne DESC ";


                        rsTVA300 = rsTVA300ModAdo.fc_OpenRecordSet(sSQLTVA);
                        bTVA300 = false;
                        lNoLigneTva = 0;

                        foreach (DataRow rsTVA300Row in rsTVA300.Rows)
                        {
                            if (lNoLigneTva == 0)
                            {
                                lNoLigneTva = Convert.ToInt32(General.nz(rsTVA300Row["NumeroLigne"], 0));
                                lNoLigneTva = lNoLigneTva + 1;
                                dbTTCtva300 = Convert.ToDouble(General.nz(rsTVA300Row["MontantTTc"], 0));
                            }

                            //=== controle si cette ligne TVA existe déja.
                            if (General.nz(rsTVA300Row["Article"], "").ToString().ToUpper() ==
                                General.sTVA300?.ToUpper())
                            {
                                bTVA300 = true;
                                break;
                            }
                        }

                        if (dbTTCtva300 > General.dbSeuilTTCtva300 && bTVA300)
                        {
                            // === supprime cette ligne si existante et que le montant est supérieur au seuil.
                            General.sSQL = "DELETE FROM FactureManuelleDetail " +
                                    " WHERE  CleFactureManuelle =" + lCleFactureManuelle +
                                    "  AND Article = '" + StdSQLchaine.gFr_DoublerQuote(General.sTVA300) + "'";
                            General.Execute(General.sSQL);
                            //=== attention bLoadGrille est un pointeur
                            bLoadGrille = true;
                        }

                        if (!bTVA300)
                        {
                            if (dbTTCtva300 < General.dbSeuilTTCtva300)
                            {
                                sLibArticleTva = "SELECT Designation1 From FacArticle "
                                                 + " WHERE  CodeArticle = '" + General.sTVA300 + "'";
                                using (var tmpModAdo = new ModAdo())
                                    sLibArticleTva = tmpModAdo.fc_ADOlibelle(sLibArticleTva);

                                if (!string.IsNullOrEmpty(sLibArticleTva))
                                {
                                    //=== insére une ligne vide.
                                    sSQLTVA = "INSERT INTO FactureManuelleDetail"
                                              + " ( NumeroLigne, Designation, CleFactureManuelle) "
                                              + " VALUES        (" + lNoLigneTva + ", ''," + lCleFactureManuelle + ")";

                                    using (var tmpModAdo = new ModAdo())
                                        General.Execute(sSQLTVA);

                                    var NewRow = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                    NewRow.Cells["NumeroLigne"].Value = lNoLigneTva;
                                    SSOleDBGrid1.UpdateData();

                                    lNoLigneTva = lNoLigneTva + 1;

                                    sSQLTVA = "INSERT INTO FactureManuelleDetail"
                                              + " (Article, NumeroLigne, Designation, CleFactureManuelle) "
                                              + " VALUES        ('" + General.sTVA300 + "',"
                                              + lNoLigneTva + ", '" + StdSQLchaine.gFr_DoublerQuote(sLibArticleTva) +
                                              "'," + lCleFactureManuelle + ")";

                                    General.Execute(sSQLTVA);

                                    var NewRow2 = SSOleDBGrid1.DisplayLayout.Bands[0].AddNew();
                                    NewRow2.Cells["NumeroLigne"].Value = lNoLigneTva;
                                    NewRow2.Cells["Article"].Value = General.sTVA300;
                                    NewRow2.Cells["Designation"].Value = sLibArticleTva;
                                    SSOleDBGrid1.UpdateData();

                                }
                            }
                        }

                        rsTVA300ModAdo.Close();
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        public void fc_Synthese(int lNumficheStandard, string sSQLWhereStandard = "")
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_Synthese() - Enter In The Function - lNumficheStandard = {lNumficheStandard}");
            //===> Fin Modif Mondir

            int lETAE_NoAuto;
            int[] tAffaire = new int[1];
            double dbAchatReel;
            double dbMo;
            double dbVentesReel;
            double dbFo;
            double dbSSTCap;
            double dbSSTComp;

            try
            {
                if (lNumficheStandard == 0)
                {
                    lblMOReel.Text = "";
                    lblAchatsReels.Text = "";
                    lblVentesReelles.Text = "";
                    txtVenteMem.Text = "";
                    lblSScapacite.Text = "";
                    lblSScompetence.Text = "";
                    lblChargesReelles.Text = "";
                    lblSstReel.Text = "";
                    lblCoefficientReel.Text = "";

                    return;
                }

                tAffaire[0] = lNumficheStandard;

                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> ajout du params false, sSQLWhereStandard)
                lETAE_NoAuto = ModAnalytique.fc_AnalyAffaire(tAffaire, "", "", 0, false, "", false, sSQLWhereStandard);
                //===> Fin Modif Mondir

                dbAchatReel = Math.Round(ModAnalytique.tCalcaffaire[0].dbAchatReel, 2);
                dbMo = Math.Round(ModAnalytique.tCalcaffaire[0].dbMoReel, 2);
                dbVentesReel = Math.Round(ModAnalytique.tCalcaffaire[0].dbVentesreel, 2);
                dbFo = Math.Round(ModAnalytique.tCalcaffaire[0].dbHTBCD, 2);
                dbSSTCap = Math.Round(ModAnalytique.tCalcaffaire[0].dbSstCapacite, 2);
                dbSSTComp = Math.Round(ModAnalytique.tCalcaffaire[0].dbSstCompetence, 2);

                lblMOReel.Text = dbMo.ToString();
                //'lblAchatsReels = txtHTBCD
                lblAchatsReels.Text = dbAchatReel.ToString();
                lblVentesReelles.Text = dbVentesReel.ToString();
                txtVenteMem.Text = dbVentesReel.ToString();
                lblSScapacite.Text = dbSSTCap.ToString();
                lblSScompetence.Text = dbSSTComp.ToString();

                lblChargesReelles.Text = (dbMo + Convert.ToDouble(lblAchatsReels.Text) + dbSSTCap + dbSSTComp).ToString();
                lblSstReel.Text = (dbSSTCap + dbSSTComp).ToString();

                if (lblChargesReelles.Text != "")
                {
                    lblCoefficientReel.Text = (Convert.ToDouble(General.nz(lblVentesReelles.Text, 0)) /
                                                   Convert.ToDouble(General.nz(lblChargesReelles.Text, 0))).ToString();
                }

                lblCoefficientReel.Text = Math.Round(Convert.ToDouble(General.nz(lblCoefficientReel.Text, 0)), 3)
                    .ToString("##.000");
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void GridInter_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridInter_DoubleClickRow() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                //===> Mondir le 18.06.2021 https://groupe-dt.mantishub.io/view.php?id=2500
                if (GridInter.ActiveRow == null)
                    return;

                var row = GridInter.ActiveRow;

                if (string.IsNullOrEmpty(row.Cells["NoIntervention"].Text))
                {
                    return;
                }

                //=== intervention.
                //===> Mondir le 18.06.2021, added the param Name https://groupe-dt.mantishub.io/view.php?id=2500
                ModParametre.fc_SaveParamPosition(Name, Variable.cUserIntervention,
                    row.Cells["NoIntervention"].Text);
                View.Theme.Theme.Navigate(typeof(UserIntervention));
                //===> Fin Modif Mondir
            }
            catch (Exception ee)
            {
                Program.SaveException(ee);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInter_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir le 30.11.2020, no need for this event

            return;
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridInter_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (!string.IsNullOrEmpty(e.Row.Cells["HeureDebut"].Text))
                {
                    e.Row.Cells["HeureDebut"].Value = Convert.ToDateTime(e.Row.Cells["HeureDebut"].Value).TimeOfDay;
                }
                if (!string.IsNullOrEmpty(e.Row.Cells["HeureFin"].Text))
                {
                    e.Row.Cells["HeureFin"].Value = Convert.ToDateTime(e.Row.Cells["HeureFin"].Value).TimeOfDay;
                }
                if (!string.IsNullOrEmpty(e.Row.Cells["Duree"].Text))
                {
                    e.Row.Cells["Duree"].Value = Convert.ToDateTime(e.Row.Cells["Duree"].Value).TimeOfDay;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label1_1_Click(object sender, EventArgs e)
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label1_1_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (GridInter.ActiveRow == null)
                return;

            var Label = sender as Label;
            var Index = Convert.ToInt32(Label.Tag);
            var row = GridInter.ActiveRow;

            switch (Index)
            {
                case 1:
                    if (string.IsNullOrEmpty(row.Cells["NumFicheStandard"].Text))
                    {
                        return;
                    }
                    //=== appel.
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocStandard,
                        row.Cells["NumFicheStandard"].Text);
                    View.Theme.Theme.Navigate(typeof(UserDocStandard));
                    break;
                case 2:
                    if (string.IsNullOrEmpty(row.Cells["NoIntervention"].Text))
                    {
                        return;
                    }
                    //=== intervention.
                    //===> Mondir le 18.06.2021, added the param Name https://groupe-dt.mantishub.io/view.php?id=2500
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserIntervention,
                        row.Cells["NoIntervention"].Text);
                    View.Theme.Theme.Navigate(typeof(UserIntervention));
                    break;
                case 3:
                    if (string.IsNullOrEmpty(row.Cells["CodeImmeuble"].Text))
                    {
                        return;
                    }
                    //=== intervention.
                    //===> Mondir le 18.06.2021, added the param Name https://groupe-dt.mantishub.io/view.php?id=2500 and changed cUserIntervention par cUserDocImmeuble
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocImmeuble,
                        row.Cells["CodeImmeuble"].Text);
                    View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
                    break;
                //===> Mondir le 18.06.2021 Demande d'ajout du https://groupe-dt.mantishub.io/view.php?id=2500
                case 4:
                    if (string.IsNullOrEmpty(row.Cells["NoDevis"].Text))
                    {
                        //===> Mondir le 22.06.2021 https://groupe-dt.mantishub.io/view.php?id=2500#c6183
                        CustomMessageBox.Show("Il n'y pas de devis associé à cette facture", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    //=== intervention.
                    //===> Mondir le 18.06.2021, added the param Name https://groupe-dt.mantishub.io/view.php?id=2500 and changed cUserIntervention par cUserDocImmeuble
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocDevis,
                        row.Cells["NoDevis"].Text);
                    View.Theme.Theme.Navigate(typeof(UserDocDevis));
                    break;
                    //===> Fin Modif Mondir
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBGrid1_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            if (e.Row.Cells["article"].Column.Header.VisiblePosition ==
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["article"].Header.VisiblePosition)
            {
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Quantite"].Header.VisiblePosition =
                    SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Quantite"].Header.VisiblePosition;
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sWhere"></param>
        private string fc_GetOsMultiIntervention(string sWhere)
        {
            //===> Mondir 19.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetOsMultiIntervention() - Enter In The Function - sWhere = {sWhere}");
            //===> Fin Modif Mondir

            string returnValue = "";

            string sSQL = "";
            string sdate = "";
            DataTable rs = null;
            ModAdo rsModAdo = new ModAdo();
            string sTexte = "";
            int X;
            int x1;
            string[] sTab = new string[1];
            string sWherePar = "";

            //=== controle si ce sont des interventions avec plusieurs fiche d'appel si
            //=== c est le cas on retourne un texte avec les accords.

            try
            {
                sWherePar = sWhere.ToUpper();
                sWherePar = sWhere.Replace("WHERE", " WHERE ( ");
                sWherePar = sWherePar + ")";

                sSQL = "SELECT  distinct GestionStandard.RefOS, GestionStandard.DateOS "
                       + " FROM         GestionStandard INNER JOIN "
                       + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard"
                       + "" + sWherePar + " ";
                //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Line commented
                //+ " AND (RefOS is not null and RefOS <> '')";
                //===> Fin Modif Mondir

                rs = rsModAdo.fc_OpenRecordSet(sSQL);
                sTexte = "";
                X = 0;

                foreach (DataRow Row in rs.Rows)
                {
                    X = X + 1;
                    sdate = Convert.ToString(General.nz(Row["DateOS"], ""));
                    if (sdate.Contains("1900") || sdate.Contains("1899"))
                    {
                        sdate = "";
                    }

                    //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Line commented
                    //if (!string.IsNullOrEmpty(General.nz(Row["RefOS"], "").ToString()))
                    //{
                    //===> Fin Modif Mondir

                    if (X == 1)
                    {
                        if (!string.IsNullOrEmpty(sdate))
                        {
                            sTexte = Row["RefOS"] + " du " + Convert.ToDateTime(Row["DateOS"]).ToShortDateString();
                        }
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Ajout du if (General.nz(Row["RefOS"], "").ToString() != "")
                        else if (General.nz(Row["RefOS"], "").ToString() != "")
                        {
                            sTexte = Row["RefOS"]?.ToString();
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(sdate))
                        {
                            sTexte = sTexte + "," + Row["RefOS"] + " du " + Convert.ToDateTime(Row["DateOS"]).ToShortDateString();
                        }
                        //===> Mondir le 13.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Ajout du if (General.nz(Row["RefOS"], "").ToString() != "")
                        else if (General.nz(Row["RefOS"], "").ToString() != "")
                        {
                            sTexte = sTexte + "," + Row["RefOS"]?.ToString();
                        }
                    }
                    //}
                }

                rsModAdo.Close();

                sTab = sTexte.Split(',');
                sTexte = "";

                //=== mise en forme de la virgule et du point.
                for (x1 = 0; x1 <= sTab.Length - 1; x1++)
                {
                    if (x1 == 0)
                    {
                        //===> Mondir le 22.01.2021, https://groupe-dt.mantishub.io/view.php?id=2222
                        if (sTab.Length > 1)
                        {
                            sTexte = "Selon vos accords " + sTab[x1];
                        }
                        else
                        {
                            sTexte = "Selon votre accord " + sTab[x1];
                        }
                        //===> Fin Modif Mondir
                    }
                    else
                    {
                        sTexte = sTexte + sTab[x1];
                    }

                    if (x1 < sTab.Length - 1)
                    {
                        if (x1 == X - 2)
                        {
                            sTexte = sTexte + " et ";
                        }
                        else
                        {
                            sTexte = sTexte + ", ";
                        }
                    }
                }

                if (X == 1)
                {
                    returnValue = "";
                }
                else if (X > 1)
                {
                    returnValue = sTexte;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }

            return returnValue;
        }

        /// <summary>
        /// Mondir le 10.02.2021, this control is visible false in VB6 but i am keeping it visible true coz we are testing if it is checked
        //  Coz in C#, if visible false so checked = false
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocFacManuel_Load(object sender, EventArgs e)
        {
            optDevis1.Size = new Size(0, 0);
            optAf.Size = new Size(0, 0);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var sSQL = "SELECT        NumeroDevis, SUM(TotalVenteApresCoef) AS HT, CodeTVA, CodeArticle"
                      + " From DevisDetail "
                      + " GROUP BY NumeroDevis, CodeTVA, CodeArticle "
                      + " HAVING        (SUM(TotalVenteApresCoef) IS NOT NULL) "
                      + " AND (SUM(TotalVenteApresCoef) <> 0) AND (NumeroDevis = '202101709/A') "
                      + " ORDER BY CodeTVA ";

            var rs2ADo = new ModAdo();
            var rs2 = rs2ADo.fc_OpenRecordSet(sSQL);
            var dbTaux = 0.0;
            var dbTauxCee = 0.0;
            var dbHTCee = 0.0;
            SituationDevis[] tSit = null;
            var sCodeArticle = "";
            var sCpteTVATR = "";
            var sCpteVenteTR = "";
            var sCpteVenteTP = "";
            var sCpteTVATP = "";
            var dbHtDev = 0.0;
            var sTYD_Code = "";
            if (rs2.Rows.Count > 0)
            {
                dbTaux = Convert.ToInt32(General.nz(rs2.Rows[0]["CodeTVA"], 0));
                i = 0;
                Array.Resize(ref tSit, i + 1);
                tSit[i].dbTaux = Convert.ToDouble(General.nz(rs2.Rows[0]["CodeTVA"], 0));

                foreach (DataRow r2 in rs2.Rows)
                {
                    if (tSit[i].dbTaux != Convert.ToDouble(General.nz(r2["CodeTVA"], 0))
                        && General.UCase(General.nz(r2["CodeArticle"], "")) != General.UCase(cArticleCee))
                    {
                        //====> Mondir le 02.07.2020, modif ajouté suite de l'appel de Rachid
                        i = i + 1;
                        //====> Fin Modif Mondir
                        Array.Resize(ref tSit, i + 1);
                        tSit[i].dbTaux = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                    }

                    if (General.UCase(General.nz(r2["CodeArticle"], "")) == General.UCase(cArticleCee))
                    {
                        dbTauxCee = Convert.ToDouble(General.nz(r2["CodeTVA"], 0));
                        //=======> Mohammed 09.07.2020 : Modifs of V09.07.2020

                        dbTauxCee = 0;

                        //===> End Modifs of v09.07.2020

                        dbHTCee = General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);
                        //'sCodeArticle = fc_GetArticle(cmbService, dbHtDev, sTYD_Code, nz(rs2!CodeTVA, 0))
                        sCodeArticle = cArticleCee;
                        //'=== recherche le cpt 7

                        sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                        + " From FacArticle ";


                        sSQL = sSQL + " WHERE CodeArticle = '" + sCodeArticle + "'";
                        var rsArtADo = new ModAdo();
                        var rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                        if (rsArt.Rows.Count > 0)
                        {
                            sCpteVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                            sCpteTVATR = fc_getCptTva(sCpteVenteTR);


                            sCpteVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                            sCpteTVATP = fc_getCptTva(sCpteVenteTP);
                        }
                        rsArt.Dispose();
                        rsArt = null;
                        //====> Mondir le 05.07.2020, ajouter les modifs de la version V05.07.2020 ===> ligne : tSit[i].sCodeArticle = sCodeArticle; est mises en commentaire
                        //=== mise en commentaire le 05 02 2020
                        ///====> Mondir le 02.07.2020, Modif de la version V02.07.2020
                        //tSit[i].sCodeArticle = sCodeArticle;
                        ///====> Fin Modif Mondir
                    }
                    else
                    {
                        //Mondir le 29.06.2020 General.FncArrondir was fogten while inseting modifs
                        tSit[i].dbHT = tSit[i].dbHT + Math.Round(Convert.ToDouble(General.nz(r2["HT"], 0)), 2, MidpointRounding.AwayFromZero);
                        if (string.IsNullOrEmpty(tSit[i].sCodeArticle))
                        {
                            tSit[i].sCodeArticle = fc_GetArticle(cmbService.Text, dbHtDev, sTYD_Code, tSit[i].dbTaux);


                            //'=== recherche le cpt 7
                            sSQL = "SELECT        CodeArticle, Designation1, CG_Num, CG_Num2"
                                + " From FacArticle ";


                            sSQL = sSQL + " WHERE CodeArticle = '" + tSit[i].sCodeArticle + "'";
                            var rsArtADo = new ModAdo();
                            var rsArt = rsArtADo.fc_OpenRecordSet(sSQL);

                            if (rsArt.Rows.Count > 0)
                            {
                                tSit[i].sCptVenteTR = rsArt.Rows[0]["CG_NUM"] + "";
                                tSit[i].sCptTvaTR = fc_getCptTva(tSit[i].sCptVenteTR);


                                tSit[i].sCptVenteTP = rsArt.Rows[0]["CG_NUM2"] + "";
                                tSit[i].sCptTvaTP = fc_getCptTva(tSit[i].sCptVenteTP);

                                //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
                                tSit[i].sLibelleArticle = rsArt.Rows[0]["Designation1"] + "";
                                //===> Fin Modif Mondir
                            }
                            rsArt.Dispose();
                            rsArt = null;
                        }
                    }
                }
            }
        }
    }
}
