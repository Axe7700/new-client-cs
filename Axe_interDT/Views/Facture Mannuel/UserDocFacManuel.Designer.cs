namespace Axe_interDT.Views.Facture_Mannuel
{
    partial class UserDocFacManuel
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocFacManuel));
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinGrid.UltraGridBand ultraGridBand1 = new Infragistics.Win.UltraWinGrid.UltraGridBand("", -1);
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab9 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab10 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab11 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label33 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lblDateEcheance = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbService = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.Frame1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.optParticulier = new System.Windows.Forms.RadioButton();
            this.optGerant = new System.Windows.Forms.RadioButton();
            this.Check1 = new System.Windows.Forms.CheckBox();
            this.Frame8 = new System.Windows.Forms.Panel();
            this.chkFTM_Compta = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheReg = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.SSOleDBDropDown5 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown3 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown4 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdAction = new System.Windows.Forms.Button();
            this.cmdfactScann = new System.Windows.Forms.Button();
            this.cmdArticleGecet = new System.Windows.Forms.Button();
            this.cdmDevisPDA = new System.Windows.Forms.Button();
            this.lblP3 = new System.Windows.Forms.Label();
            this.lblChaudCondensation = new System.Windows.Forms.Label();
            this.chkDefinitive = new System.Windows.Forms.CheckBox();
            this.cmdAvoir = new System.Windows.Forms.Button();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.Label1_3 = new System.Windows.Forms.Label();
            this.Label1_2 = new System.Windows.Forms.Label();
            this.Label1_1 = new System.Windows.Forms.Label();
            this.GridInter = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Frame3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdActualiser = new System.Windows.Forms.Button();
            this.SSOleDBGrid2 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.frame6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.chk3bacsVisu = new System.Windows.Forms.CheckBox();
            this.lblFin = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.Option4 = new System.Windows.Forms.RadioButton();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.lblDebut = new System.Windows.Forms.Label();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.cmdExport = new System.Windows.Forms.Button();
            this.cmdMial = new System.Windows.Forms.Button();
            this.OptFactEnCours = new System.Windows.Forms.RadioButton();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.Combo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label21 = new System.Windows.Forms.Label();
            this.lblNom = new System.Windows.Forms.Label();
            this.optPersonne = new System.Windows.Forms.RadioButton();
            this.OptNomChoisis = new System.Windows.Forms.RadioButton();
            this.OptTous = new System.Windows.Forms.RadioButton();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.Option5 = new System.Windows.Forms.RadioButton();
            this.Option6 = new System.Windows.Forms.RadioButton();
            this.Option3 = new System.Windows.Forms.RadioButton();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.Frame5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.Option7 = new System.Windows.Forms.RadioButton();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.CmdMiseAjour = new System.Windows.Forms.Button();
            this.cmdAnalytique = new System.Windows.Forms.Button();
            this.Option2 = new System.Windows.Forms.RadioButton();
            this.Option8 = new System.Windows.Forms.RadioButton();
            this.chk3bacs = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdSyntheseMAJ = new System.Windows.Forms.Button();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label22 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.chkDuplicata = new System.Windows.Forms.CheckBox();
            this.chkOS = new System.Windows.Forms.CheckBox();
            this.chkBonPourAccord = new System.Windows.Forms.CheckBox();
            this.lblMail = new System.Windows.Forms.Label();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.SSOleDBGrid3 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdRechImmeuble = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.chkSolderDevis = new System.Windows.Forms.CheckBox();
            this.CmdFax = new System.Windows.Forms.Button();
            this.cmdAchat = new System.Windows.Forms.Button();
            this.cmdInserer = new System.Windows.Forms.Button();
            this.PicDevis = new System.Windows.Forms.TableLayoutPanel();
            this.cmdDevis = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.optSelect = new System.Windows.Forms.RadioButton();
            this.optDeselect = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.optDevis1 = new System.Windows.Forms.RadioButton();
            this.optAf = new System.Windows.Forms.RadioButton();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.GridContrat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblCodeImmeuble = new System.Windows.Forms.Label();
            this.cmdHistorique = new System.Windows.Forms.Button();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.button1 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.txt_9 = new System.Windows.Forms.TextBox();
            this.txt_6 = new System.Windows.Forms.TextBox();
            this.txt_8 = new System.Windows.Forms.TextBox();
            this.txt_7 = new System.Windows.Forms.TextBox();
            this.txt_5 = new System.Windows.Forms.TextBox();
            this.txt_4 = new System.Windows.Forms.TextBox();
            this.txt_3 = new System.Windows.Forms.TextBox();
            this.txt_2 = new System.Windows.Forms.TextBox();
            this.txt_1 = new System.Windows.Forms.TextBox();
            this.txt_0 = new System.Windows.Forms.TextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdValider = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.cmdMenu = new System.Windows.Forms.Button();
            this.cmdValidOpt = new System.Windows.Forms.Button();
            this.txtVenteMem = new System.Windows.Forms.TextBox();
            this.lblSstReel = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Label243 = new System.Windows.Forms.Label();
            this.Label242 = new System.Windows.Forms.Label();
            this.Label241 = new System.Windows.Forms.Label();
            this.Label240 = new System.Windows.Forms.Label();
            this.lblTotalAchats = new System.Windows.Forms.Label();
            this.lblTotalHeures = new System.Windows.Forms.Label();
            this.lblTotalSituations = new System.Windows.Forms.Label();
            this.lblMontantDevis = new System.Windows.Forms.Label();
            this.frmSynthese = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.fraSynthese = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.lblCoefficientReel = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.lblhtDevis = new System.Windows.Forms.Label();
            this.lbl6 = new System.Windows.Forms.Label();
            this.lblVentesReelles = new System.Windows.Forms.Label();
            this.lblChargesReelles = new System.Windows.Forms.Label();
            this.lblAchatsReels = new System.Windows.Forms.Label();
            this.lblSScompetence = new System.Windows.Forms.Label();
            this.lblSScapacite = new System.Windows.Forms.Label();
            this.lblMOReel = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.Frame9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.optIntervention = new System.Windows.Forms.RadioButton();
            this.optDevis = new System.Windows.Forms.RadioButton();
            this.OptTravMan = new System.Windows.Forms.RadioButton();
            this.OptConMan = new System.Windows.Forms.RadioButton();
            this.OptFioulMan = new System.Windows.Forms.RadioButton();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.Label1_4 = new System.Windows.Forms.Label();
            this.lblLibService = new iTalk.iTalk_TextBox_Small2();
            this.txtObjet1 = new iTalk.iTalk_TextBox_Small2();
            this.txtObjet2 = new iTalk.iTalk_TextBox_Small2();
            this.txtCommentaire1 = new iTalk.iTalk_TextBox_Small2();
            this.txtCommentaire2 = new iTalk.iTalk_TextBox_Small2();
            this.txtGerNom = new iTalk.iTalk_TextBox_Small2();
            this.txtGerAdresse1 = new iTalk.iTalk_TextBox_Small2();
            this.txtGerCP = new iTalk.iTalk_TextBox_Small2();
            this.txtGerAdresse2 = new iTalk.iTalk_TextBox_Small2();
            this.txtGerVille = new iTalk.iTalk_TextBox_Small2();
            this.txtTypeReglement = new iTalk.iTalk_TextBox_Small2();
            this.txtImmAdresse1 = new iTalk.iTalk_TextBox_Small2();
            this.txtimmAdresse3 = new iTalk.iTalk_TextBox_Small2();
            this.txtImmAdresse2 = new iTalk.iTalk_TextBox_Small2();
            this.txtImmCP = new iTalk.iTalk_TextBox_Small2();
            this.txtMode = new iTalk.iTalk_TextBox_Small2();
            this.txtVIR_Code = new iTalk.iTalk_TextBox_Small2();
            this.txtNCompte = new iTalk.iTalk_TextBox_Small2();
            this.txtDateEcheance = new iTalk.iTalk_TextBox_Small2();
            this.txtDateDeFacture = new iTalk.iTalk_TextBox_Small2();
            this.txtTVA = new iTalk.iTalk_TextBox_Small2();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtINT_AnaCode = new iTalk.iTalk_TextBox_Small2();
            this.txtGerAdresse3 = new iTalk.iTalk_TextBox_Small2();
            this.txtImmVille = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtModeLibelle = new iTalk.iTalk_TextBox_Small2();
            this.txtRefClient = new iTalk.iTalk_TextBox_Small2();
            this.txtVisu1 = new iTalk.iTalk_TextBox_Small2();
            this.txtVisuDate1 = new iTalk.iTalk_TextBox_Small2();
            this.txtVisu2 = new iTalk.iTalk_TextBox_Small2();
            this.txtVisuDate2 = new iTalk.iTalk_TextBox_Small2();
            this.txtImpDate1 = new iTalk.iTalk_TextBox_Small2();
            this.txtDebut = new iTalk.iTalk_TextBox_Small2();
            this.txtImpDate2 = new iTalk.iTalk_TextBox_Small2();
            this.txtFin = new iTalk.iTalk_TextBox_Small2();
            this.txtMiseAjour2 = new iTalk.iTalk_TextBox_Small2();
            this.txtMiseAjour1 = new iTalk.iTalk_TextBox_Small2();
            this.txtMiseDate1 = new iTalk.iTalk_TextBox_Small2();
            this.txtMiseDate2 = new iTalk.iTalk_TextBox_Small2();
            this.txtEnCours = new iTalk.iTalk_TextBox_Small2();
            this.txtImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtNoDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtSituation = new iTalk.iTalk_TextBox_Small2();
            this.txtCharges = new iTalk.iTalk_TextBox_Small2();
            this.ultraTabPageControl1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbService)).BeginInit();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.Frame8.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.ultraTabPageControl3.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridInter)).BeginInit();
            this.Frame3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid2)).BeginInit();
            this.ultraTabPageControl4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.frame6.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Combo1)).BeginInit();
            this.Frame4.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.Frame5.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.ultraTabPageControl5.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid3)).BeginInit();
            this.PicDevis.SuspendLayout();
            this.panel2.SuspendLayout();
            this.ultraTabPageControl6.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridContrat)).BeginInit();
            this.ultraTabPageControl7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.frmSynthese.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.panel1.SuspendLayout();
            this.fraSynthese.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.Frame9.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel3);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1842, 797);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 10;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.31754F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.31385F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.31325F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.31325F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.09813F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 2.33019F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.31378F));
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 15);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 16);
            this.tableLayoutPanel3.Controls.Add(this.label9, 0, 17);
            this.tableLayoutPanel3.Controls.Add(this.lblDateEcheance, 0, 18);
            this.tableLayoutPanel3.Controls.Add(this.lblLibService, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtObjet1, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtObjet2, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtCommentaire1, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.txtCommentaire2, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.txtGerNom, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.txtGerAdresse1, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.txtGerCP, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.txtGerAdresse2, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.txtGerVille, 3, 10);
            this.tableLayoutPanel3.Controls.Add(this.txtTypeReglement, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.txtImmAdresse1, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.txtimmAdresse3, 1, 13);
            this.tableLayoutPanel3.Controls.Add(this.txtImmAdresse2, 1, 12);
            this.tableLayoutPanel3.Controls.Add(this.txtImmCP, 1, 14);
            this.tableLayoutPanel3.Controls.Add(this.txtMode, 1, 15);
            this.tableLayoutPanel3.Controls.Add(this.txtVIR_Code, 1, 16);
            this.tableLayoutPanel3.Controls.Add(this.txtNCompte, 1, 17);
            this.tableLayoutPanel3.Controls.Add(this.txtDateEcheance, 1, 18);
            this.tableLayoutPanel3.Controls.Add(this.cmbService, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label10, 7, 3);
            this.tableLayoutPanel3.Controls.Add(this.label11, 7, 4);
            this.tableLayoutPanel3.Controls.Add(this.label12, 7, 0);
            this.tableLayoutPanel3.Controls.Add(this.label13, 4, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtDateDeFacture, 5, 1);
            this.tableLayoutPanel3.Controls.Add(this.Frame1, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtTVA, 8, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtNoIntervention, 8, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtINT_AnaCode, 8, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtGerAdresse3, 9, 2);
            this.tableLayoutPanel3.Controls.Add(this.Check1, 5, 0);
            this.tableLayoutPanel3.Controls.Add(this.Frame8, 5, 16);
            this.tableLayoutPanel3.Controls.Add(this.txtImmVille, 3, 14);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 3, 15);
            this.tableLayoutPanel3.Controls.Add(this.label24, 0, 19);
            this.tableLayoutPanel3.Controls.Add(this.txtRefClient, 1, 19);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 21;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1842, 797);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 30);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(112, 18);
            this.label33.TabIndex = 502;
            this.label33.Text = "Code Immeuble";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 60);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(57, 18);
            this.label5.TabIndex = 509;
            this.label5.Text = "Service";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 90);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(43, 18);
            this.label4.TabIndex = 508;
            this.label4.Text = "Objet";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 150);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 18);
            this.label3.TabIndex = 507;
            this.label3.Text = "En tete";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 210);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 18);
            this.label2.TabIndex = 506;
            this.label2.Text = "Adresse(gérant)";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 450);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 18);
            this.label6.TabIndex = 510;
            this.label6.Text = "Mode de réglement";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 480);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(32, 18);
            this.label8.TabIndex = 512;
            this.label8.Text = "RIB";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 510);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(61, 18);
            this.label9.TabIndex = 513;
            this.label9.Text = "Compte";
            // 
            // lblDateEcheance
            // 
            this.lblDateEcheance.AutoSize = true;
            this.lblDateEcheance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateEcheance.Location = new System.Drawing.Point(3, 540);
            this.lblDateEcheance.Name = "lblDateEcheance";
            this.lblDateEcheance.Size = new System.Drawing.Size(118, 18);
            this.lblDateEcheance.TabIndex = 511;
            this.lblDateEcheance.Text = "Date d\'échéance";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 330);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 18);
            this.label1.TabIndex = 505;
            this.label1.Text = "Lieu d\'intervenant";
            // 
            // cmbService
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.cmbService, 2);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbService.DisplayLayout.Appearance = appearance1;
            this.cmbService.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbService.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbService.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbService.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbService.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbService.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbService.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbService.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbService.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbService.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbService.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbService.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbService.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbService.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbService.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbService.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbService.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbService.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbService.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbService.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbService.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbService.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbService.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbService.Location = new System.Drawing.Point(144, 63);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(417, 27);
            this.cmbService.TabIndex = 578;
            this.cmbService.AfterCloseUp += new System.EventHandler(this.cmbService_AfterCloseUp);
            this.cmbService.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbService_BeforeDropDown);
            this.cmbService.Validated += new System.EventHandler(this.cmbService_Validated);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(1489, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 18);
            this.label10.TabIndex = 534;
            this.label10.Text = "N°Intervention";
            this.label10.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(1489, 120);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(44, 18);
            this.label11.TabIndex = 535;
            this.label11.Text = "Code";
            this.label11.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1489, 0);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(35, 18);
            this.label12.TabIndex = 536;
            this.label12.Text = "TVA";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(778, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(108, 18);
            this.label13.TabIndex = 537;
            this.label13.Text = "Date de facture";
            // 
            // Frame1
            // 
            this.Frame1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel3.SetColumnSpan(this.Frame1, 3);
            this.Frame1.Controls.Add(this.tableLayoutPanel4);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Location = new System.Drawing.Point(143, 2);
            this.Frame1.Margin = new System.Windows.Forms.Padding(2);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(630, 26);
            this.Frame1.TabIndex = 539;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.optParticulier, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.optGerant, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(628, 24);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // optParticulier
            // 
            this.optParticulier.AutoSize = true;
            this.optParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optParticulier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optParticulier.Location = new System.Drawing.Point(314, 0);
            this.optParticulier.Margin = new System.Windows.Forms.Padding(0);
            this.optParticulier.Name = "optParticulier";
            this.optParticulier.Size = new System.Drawing.Size(314, 24);
            this.optParticulier.TabIndex = 539;
            this.optParticulier.Text = "Autre Adresse";
            this.optParticulier.UseVisualStyleBackColor = true;
            // 
            // optGerant
            // 
            this.optGerant.AutoSize = true;
            this.optGerant.Checked = true;
            this.optGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optGerant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optGerant.Location = new System.Drawing.Point(0, 0);
            this.optGerant.Margin = new System.Windows.Forms.Padding(0);
            this.optGerant.Name = "optGerant";
            this.optGerant.Size = new System.Drawing.Size(314, 24);
            this.optGerant.TabIndex = 538;
            this.optGerant.TabStop = true;
            this.optGerant.Text = "Gérant";
            this.optGerant.UseVisualStyleBackColor = true;
            // 
            // Check1
            // 
            this.Check1.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.Check1, 2);
            this.Check1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Check1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Check1.Location = new System.Drawing.Point(892, 3);
            this.Check1.Name = "Check1";
            this.Check1.Size = new System.Drawing.Size(591, 24);
            this.Check1.TabIndex = 570;
            this.Check1.Text = "Paiement comptant";
            this.Check1.UseVisualStyleBackColor = true;
            this.Check1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Check1_KeyPress);
            // 
            // Frame8
            // 
            this.Frame8.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel3.SetColumnSpan(this.Frame8, 2);
            this.Frame8.Controls.Add(this.chkFTM_Compta);
            this.Frame8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame8.Location = new System.Drawing.Point(890, 481);
            this.Frame8.Margin = new System.Windows.Forms.Padding(1);
            this.Frame8.Name = "Frame8";
            this.tableLayoutPanel3.SetRowSpan(this.Frame8, 2);
            this.Frame8.Size = new System.Drawing.Size(595, 58);
            this.Frame8.TabIndex = 571;
            this.Frame8.Visible = false;
            // 
            // chkFTM_Compta
            // 
            this.chkFTM_Compta.AutoSize = true;
            this.chkFTM_Compta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkFTM_Compta.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chkFTM_Compta.Location = new System.Drawing.Point(0, 0);
            this.chkFTM_Compta.Margin = new System.Windows.Forms.Padding(6);
            this.chkFTM_Compta.Name = "chkFTM_Compta";
            this.chkFTM_Compta.Padding = new System.Windows.Forms.Padding(6);
            this.chkFTM_Compta.Size = new System.Drawing.Size(593, 56);
            this.chkFTM_Compta.TabIndex = 570;
            this.chkFTM_Compta.Text = "Ne pas  comptabiliser";
            this.chkFTM_Compta.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel5, 3);
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.Controls.Add(this.cmdRechercheImmeuble, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtCodeimmeuble, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(141, 30);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(634, 30);
            this.tableLayoutPanel5.TabIndex = 577;
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheImmeuble.Image")));
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(607, 3);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(24, 20);
            this.cmdRechercheImmeuble.TabIndex = 573;
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel6, 4);
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.cmdRechercheReg, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtModeLibelle, 1, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(564, 450);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(922, 30);
            this.tableLayoutPanel6.TabIndex = 578;
            // 
            // cmdRechercheReg
            // 
            this.cmdRechercheReg.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheReg.FlatAppearance.BorderSize = 0;
            this.cmdRechercheReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheReg.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheReg.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheReg.Image")));
            this.cmdRechercheReg.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheReg.Name = "cmdRechercheReg";
            this.cmdRechercheReg.Size = new System.Drawing.Size(24, 20);
            this.cmdRechercheReg.TabIndex = 572;
            this.cmdRechercheReg.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheReg, "Recherche de l\'immeuble");
            this.cmdRechercheReg.UseVisualStyleBackColor = false;
            this.cmdRechercheReg.Click += new System.EventHandler(this.cmdRechercheReg_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(3, 570);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(72, 18);
            this.label24.TabIndex = 579;
            this.label24.Text = "Ref Client";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.groupBox1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1842, 797);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.tableLayoutPanel7);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1842, 797);
            this.groupBox1.TabIndex = 411;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Corps de facture";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel7.Controls.Add(this.groupBox2, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.cmdAction, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.cmdfactScann, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.cmdArticleGecet, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.cdmDevisPDA, 5, 0);
            this.tableLayoutPanel7.Controls.Add(this.lblP3, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.lblChaudCondensation, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.chkDefinitive, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.cmdAvoir, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1836, 774);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel7.SetColumnSpan(this.groupBox2, 6);
            this.groupBox2.Controls.Add(this.SSOleDBDropDown5);
            this.groupBox2.Controls.Add(this.SSOleDBDropDown3);
            this.groupBox2.Controls.Add(this.SSOleDBDropDown1);
            this.groupBox2.Controls.Add(this.SSOleDBDropDown2);
            this.groupBox2.Controls.Add(this.SSOleDBDropDown4);
            this.groupBox2.Controls.Add(this.SSOleDBGrid1);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox2.Location = new System.Drawing.Point(3, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1830, 715);
            this.groupBox2.TabIndex = 584;
            this.groupBox2.TabStop = false;
            // 
            // SSOleDBDropDown5
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown5.DisplayLayout.Appearance = appearance13;
            this.SSOleDBDropDown5.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown5.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown5.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown5.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSOleDBDropDown5.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown5.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSOleDBDropDown5.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown5.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown5.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown5.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSOleDBDropDown5.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown5.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown5.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown5.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSOleDBDropDown5.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown5.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown5.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSOleDBDropDown5.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSOleDBDropDown5.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown5.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown5.DisplayLayout.Override.RowAppearance = appearance23;
            this.SSOleDBDropDown5.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown5.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSOleDBDropDown5.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown5.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown5.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown5.Location = new System.Drawing.Point(558, 129);
            this.SSOleDBDropDown5.Name = "SSOleDBDropDown5";
            this.SSOleDBDropDown5.Size = new System.Drawing.Size(186, 27);
            this.SSOleDBDropDown5.TabIndex = 507;
            this.SSOleDBDropDown5.Visible = false;
            // 
            // SSOleDBDropDown3
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown3.DisplayLayout.Appearance = appearance25;
            this.SSOleDBDropDown3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown3.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.SSOleDBDropDown3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown3.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.SSOleDBDropDown3.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown3.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown3.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.SSOleDBDropDown3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown3.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown3.DisplayLayout.Override.CellAppearance = appearance32;
            this.SSOleDBDropDown3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown3.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown3.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.SSOleDBDropDown3.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.SSOleDBDropDown3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown3.DisplayLayout.Override.RowAppearance = appearance35;
            this.SSOleDBDropDown3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown3.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.SSOleDBDropDown3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown3.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown3.Location = new System.Drawing.Point(99, 174);
            this.SSOleDBDropDown3.Name = "SSOleDBDropDown3";
            this.SSOleDBDropDown3.Size = new System.Drawing.Size(186, 27);
            this.SSOleDBDropDown3.TabIndex = 506;
            this.SSOleDBDropDown3.Visible = false;
            // 
            // SSOleDBDropDown1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown1.DisplayLayout.Appearance = appearance37;
            this.SSOleDBDropDown1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.SSOleDBDropDown1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown1.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown1.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.SSOleDBDropDown1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellAppearance = appearance44;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown1.DisplayLayout.Override.RowAppearance = appearance47;
            this.SSOleDBDropDown1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown1.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.SSOleDBDropDown1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown1.Location = new System.Drawing.Point(351, 129);
            this.SSOleDBDropDown1.Name = "SSOleDBDropDown1";
            this.SSOleDBDropDown1.Size = new System.Drawing.Size(186, 27);
            this.SSOleDBDropDown1.TabIndex = 505;
            this.SSOleDBDropDown1.Visible = false;
            // 
            // SSOleDBDropDown2
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown2.DisplayLayout.Appearance = appearance49;
            this.SSOleDBDropDown2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.SSOleDBDropDown2.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown2.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown2.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.SSOleDBDropDown2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellAppearance = appearance56;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown2.DisplayLayout.Override.RowAppearance = appearance59;
            this.SSOleDBDropDown2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown2.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.SSOleDBDropDown2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown2.Location = new System.Drawing.Point(99, 120);
            this.SSOleDBDropDown2.Name = "SSOleDBDropDown2";
            this.SSOleDBDropDown2.Size = new System.Drawing.Size(186, 27);
            this.SSOleDBDropDown2.TabIndex = 504;
            this.SSOleDBDropDown2.Visible = false;
            // 
            // SSOleDBDropDown4
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown4.DisplayLayout.Appearance = appearance61;
            this.SSOleDBDropDown4.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown4.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown4.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown4.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.SSOleDBDropDown4.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown4.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.SSOleDBDropDown4.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown4.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown4.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown4.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.SSOleDBDropDown4.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown4.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown4.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown4.DisplayLayout.Override.CellAppearance = appearance68;
            this.SSOleDBDropDown4.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown4.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown4.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.SSOleDBDropDown4.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.SSOleDBDropDown4.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown4.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown4.DisplayLayout.Override.RowAppearance = appearance71;
            this.SSOleDBDropDown4.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown4.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.SSOleDBDropDown4.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown4.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown4.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown4.Location = new System.Drawing.Point(351, 174);
            this.SSOleDBDropDown4.Name = "SSOleDBDropDown4";
            this.SSOleDBDropDown4.Size = new System.Drawing.Size(186, 27);
            this.SSOleDBDropDown4.TabIndex = 503;
            this.SSOleDBDropDown4.Visible = false;
            // 
            // SSOleDBGrid1
            // 
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance73;
            appearance74.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance74.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(253)))));
            ultraGridBand1.Override.ActiveRowAppearance = appearance74;
            appearance75.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance75.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(253)))));
            appearance75.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance75.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal;
            appearance75.ForeColor = System.Drawing.Color.Black;
            ultraGridBand1.Override.SelectedRowAppearance = appearance75;
            this.SSOleDBGrid1.DisplayLayout.BandsSerializer.Add(ultraGridBand1);
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance76;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance77;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance78.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance78.BackColor2 = System.Drawing.SystemColors.Control;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance78;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance79.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance79.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance79;
            appearance80.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance80.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(253)))));
            appearance80.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance80.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal;
            appearance80.ForeColor = System.Drawing.Color.Black;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance80;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance81;
            appearance82.BorderColor = System.Drawing.Color.Silver;
            appearance82.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance82;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance83.BackColor = System.Drawing.SystemColors.Control;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance83;
            appearance84.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance84;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance85.ForeColor = System.Drawing.Color.Black;
            this.SSOleDBGrid1.DisplayLayout.Override.HotTrackRowAppearance = appearance85;
            appearance86.ForeColor = System.Drawing.Color.Black;
            this.SSOleDBGrid1.DisplayLayout.Override.HotTrackRowSelectorAppearance = appearance86;
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance87;
            appearance88.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance88.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(253)))));
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BackHatchStyle = Infragistics.Win.BackHatchStyle.Horizontal;
            appearance88.ForeColor = System.Drawing.Color.Black;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectorAppearance = appearance88;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            appearance89.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(219)))), ((int)(((byte)(241)))), ((int)(((byte)(252)))));
            appearance89.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(240)))), ((int)(((byte)(248)))), ((int)(((byte)(253)))));
            appearance89.ForeColor = System.Drawing.Color.Black;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectedRowAppearance = appearance89;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance90;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(3, 20);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.SSOleDBGrid1.Size = new System.Drawing.Size(1824, 692);
            this.SSOleDBGrid1.TabIndex = 412;
            this.SSOleDBGrid1.Tag = "UserDocFacManuel";
            this.SSOleDBGrid1.UseAppStyling = false;
            this.SSOleDBGrid1.UseOsThemes = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.AfterRowInsert += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSOleDBGrid1_AfterRowInsert);
            this.SSOleDBGrid1.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSOleDBGrid1_AfterRowUpdate);
            this.SSOleDBGrid1.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.SSOleDBGrid1_BeforeRowUpdate);
            this.SSOleDBGrid1.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.SSOleDBGrid1_BeforeExitEditMode);
            this.SSOleDBGrid1.BeforeRowInsert += new Infragistics.Win.UltraWinGrid.BeforeRowInsertEventHandler(this.SSOleDBGrid1_BeforeRowInsert);
            this.SSOleDBGrid1.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.SSOleDBGrid1_BeforeRowsDeleted);
            this.SSOleDBGrid1.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.SSOleDBGrid1_Error);
            this.SSOleDBGrid1.DoubleClickCell += new Infragistics.Win.UltraWinGrid.DoubleClickCellEventHandler(this.SSOleDBGrid1_DoubleClickCell);
            this.SSOleDBGrid1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SSOleDBGrid1_KeyDown);
            this.SSOleDBGrid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSOleDBGrid1_KeyPress);
            // 
            // cmdAction
            // 
            this.cmdAction.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAction.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAction.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAction.FlatAppearance.BorderSize = 0;
            this.cmdAction.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAction.ForeColor = System.Drawing.Color.White;
            this.cmdAction.Location = new System.Drawing.Point(2, 2);
            this.cmdAction.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAction.Name = "cmdAction";
            this.cmdAction.Size = new System.Drawing.Size(271, 26);
            this.cmdAction.TabIndex = 577;
            this.cmdAction.Tag = "4";
            this.cmdAction.Text = "abc Correcteur";
            this.cmdAction.UseVisualStyleBackColor = false;
            this.cmdAction.Click += new System.EventHandler(this.cmdAction_Click);
            // 
            // cmdfactScann
            // 
            this.cmdfactScann.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdfactScann.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdfactScann.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdfactScann.FlatAppearance.BorderSize = 0;
            this.cmdfactScann.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdfactScann.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdfactScann.ForeColor = System.Drawing.Color.White;
            this.cmdfactScann.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdfactScann.Location = new System.Drawing.Point(919, 2);
            this.cmdfactScann.Margin = new System.Windows.Forms.Padding(2);
            this.cmdfactScann.Name = "cmdfactScann";
            this.cmdfactScann.Size = new System.Drawing.Size(455, 26);
            this.cmdfactScann.TabIndex = 578;
            this.cmdfactScann.Tag = "BCD";
            this.cmdfactScann.Text = "Fact. Fournisseurs scannées";
            this.cmdfactScann.UseVisualStyleBackColor = false;
            this.cmdfactScann.Click += new System.EventHandler(this.cmdfactScann_Click);
            // 
            // cmdArticleGecet
            // 
            this.cmdArticleGecet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdArticleGecet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdArticleGecet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdArticleGecet.FlatAppearance.BorderSize = 0;
            this.cmdArticleGecet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdArticleGecet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdArticleGecet.ForeColor = System.Drawing.Color.White;
            this.cmdArticleGecet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdArticleGecet.Location = new System.Drawing.Point(1378, 2);
            this.cmdArticleGecet.Margin = new System.Windows.Forms.Padding(2);
            this.cmdArticleGecet.Name = "cmdArticleGecet";
            this.cmdArticleGecet.Size = new System.Drawing.Size(225, 26);
            this.cmdArticleGecet.TabIndex = 579;
            this.cmdArticleGecet.Text = "Article Gecet";
            this.cmdArticleGecet.UseVisualStyleBackColor = false;
            this.cmdArticleGecet.Click += new System.EventHandler(this.cmdArticleGecet_Click);
            // 
            // cdmDevisPDA
            // 
            this.cdmDevisPDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cdmDevisPDA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cdmDevisPDA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cdmDevisPDA.FlatAppearance.BorderSize = 0;
            this.cdmDevisPDA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cdmDevisPDA.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cdmDevisPDA.ForeColor = System.Drawing.Color.White;
            this.cdmDevisPDA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cdmDevisPDA.Location = new System.Drawing.Point(1607, 2);
            this.cdmDevisPDA.Margin = new System.Windows.Forms.Padding(2);
            this.cdmDevisPDA.Name = "cdmDevisPDA";
            this.cdmDevisPDA.Size = new System.Drawing.Size(227, 26);
            this.cdmDevisPDA.TabIndex = 580;
            this.cdmDevisPDA.Tag = "BCD";
            this.cdmDevisPDA.Text = "Devis PDA";
            this.cdmDevisPDA.UseVisualStyleBackColor = false;
            this.cdmDevisPDA.Click += new System.EventHandler(this.cdmDevisPDA_Click);
            // 
            // lblP3
            // 
            this.lblP3.AutoSize = true;
            this.lblP3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblP3.ForeColor = System.Drawing.Color.Blue;
            this.lblP3.Location = new System.Drawing.Point(278, 30);
            this.lblP3.Name = "lblP3";
            this.lblP3.Size = new System.Drawing.Size(407, 23);
            this.lblP3.TabIndex = 581;
            this.lblP3.Text = "Immeuble avec contrat P3";
            this.lblP3.Visible = false;
            // 
            // lblChaudCondensation
            // 
            this.lblChaudCondensation.AutoSize = true;
            this.lblChaudCondensation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChaudCondensation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChaudCondensation.ForeColor = System.Drawing.Color.Blue;
            this.lblChaudCondensation.Location = new System.Drawing.Point(920, 30);
            this.lblChaudCondensation.Name = "lblChaudCondensation";
            this.lblChaudCondensation.Size = new System.Drawing.Size(453, 23);
            this.lblChaudCondensation.TabIndex = 582;
            this.lblChaudCondensation.Text = "Chaudiere à condensation";
            this.lblChaudCondensation.Visible = false;
            // 
            // chkDefinitive
            // 
            this.chkDefinitive.AutoSize = true;
            this.chkDefinitive.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkDefinitive.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chkDefinitive.Location = new System.Drawing.Point(278, 3);
            this.chkDefinitive.Name = "chkDefinitive";
            this.chkDefinitive.Size = new System.Drawing.Size(407, 24);
            this.chkDefinitive.TabIndex = 583;
            this.chkDefinitive.Text = "Facturé en totalité";
            this.chkDefinitive.UseVisualStyleBackColor = true;
            this.chkDefinitive.Visible = false;
            // 
            // cmdAvoir
            // 
            this.cmdAvoir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAvoir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAvoir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAvoir.FlatAppearance.BorderSize = 0;
            this.cmdAvoir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAvoir.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAvoir.ForeColor = System.Drawing.Color.White;
            this.cmdAvoir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAvoir.Location = new System.Drawing.Point(690, 2);
            this.cmdAvoir.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAvoir.Name = "cmdAvoir";
            this.cmdAvoir.Size = new System.Drawing.Size(225, 26);
            this.cmdAvoir.TabIndex = 579;
            this.cmdAvoir.Text = "Annulation";
            this.cmdAvoir.UseVisualStyleBackColor = false;
            this.cmdAvoir.Click += new System.EventHandler(this.cmdAvoir_Click);
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel21);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1842, 797);
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Controls.Add(this.groupBox3, 0, 1);
            this.tableLayoutPanel21.Controls.Add(this.Frame3, 0, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1842, 797);
            this.tableLayoutPanel21.TabIndex = 414;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tableLayoutPanel22);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox3.Location = new System.Drawing.Point(3, 401);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1836, 393);
            this.groupBox3.TabIndex = 414;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Interventions liées à la facture";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel23, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.GridInter, 0, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 390F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1830, 370);
            this.tableLayoutPanel22.TabIndex = 414;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 4;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99813F));
            this.tableLayoutPanel23.Controls.Add(this.Label1_4, 3, 0);
            this.tableLayoutPanel23.Controls.Add(this.Label1_3, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.Label1_2, 1, 0);
            this.tableLayoutPanel23.Controls.Add(this.Label1_1, 0, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1824, 19);
            this.tableLayoutPanel23.TabIndex = 414;
            // 
            // Label1_3
            // 
            this.Label1_3.AutoSize = true;
            this.Label1_3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label1_3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.Label1_3.ForeColor = System.Drawing.Color.Blue;
            this.Label1_3.Location = new System.Drawing.Point(915, 0);
            this.Label1_3.Name = "Label1_3";
            this.Label1_3.Size = new System.Drawing.Size(112, 18);
            this.Label1_3.TabIndex = 55;
            this.Label1_3.Tag = "3";
            this.Label1_3.Text = "Fiche Immeuble";
            this.Label1_3.Click += new System.EventHandler(this.Label1_1_Click);
            // 
            // Label1_2
            // 
            this.Label1_2.AutoSize = true;
            this.Label1_2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label1_2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.Label1_2.ForeColor = System.Drawing.Color.Blue;
            this.Label1_2.Location = new System.Drawing.Point(459, 0);
            this.Label1_2.Name = "Label1_2";
            this.Label1_2.Size = new System.Drawing.Size(123, 18);
            this.Label1_2.TabIndex = 1;
            this.Label1_2.Tag = "2";
            this.Label1_2.Text = "Fiche Intervention";
            this.Label1_2.Click += new System.EventHandler(this.Label1_1_Click);
            // 
            // Label1_1
            // 
            this.Label1_1.AutoSize = true;
            this.Label1_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label1_1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.Label1_1.ForeColor = System.Drawing.Color.Blue;
            this.Label1_1.Location = new System.Drawing.Point(3, 0);
            this.Label1_1.Name = "Label1_1";
            this.Label1_1.Size = new System.Drawing.Size(84, 18);
            this.Label1_1.TabIndex = 0;
            this.Label1_1.Tag = "1";
            this.Label1_1.Text = "Fiche Appel";
            this.Label1_1.Click += new System.EventHandler(this.Label1_1_Click);
            // 
            // GridInter
            // 
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridInter.DisplayLayout.Appearance = appearance91;
            this.GridInter.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridInter.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance92.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance92.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance92.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance92.BorderColor = System.Drawing.SystemColors.Window;
            this.GridInter.DisplayLayout.GroupByBox.Appearance = appearance92;
            appearance93.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridInter.DisplayLayout.GroupByBox.BandLabelAppearance = appearance93;
            this.GridInter.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance94.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance94.BackColor2 = System.Drawing.SystemColors.Control;
            appearance94.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance94.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridInter.DisplayLayout.GroupByBox.PromptAppearance = appearance94;
            this.GridInter.DisplayLayout.MaxColScrollRegions = 1;
            this.GridInter.DisplayLayout.MaxRowScrollRegions = 1;
            appearance95.BackColor = System.Drawing.SystemColors.Window;
            appearance95.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridInter.DisplayLayout.Override.ActiveCellAppearance = appearance95;
            appearance96.BackColor = System.Drawing.SystemColors.Highlight;
            appearance96.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridInter.DisplayLayout.Override.ActiveRowAppearance = appearance96;
            this.GridInter.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridInter.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridInter.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridInter.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridInter.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            this.GridInter.DisplayLayout.Override.CardAreaAppearance = appearance97;
            appearance98.BorderColor = System.Drawing.Color.Silver;
            appearance98.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridInter.DisplayLayout.Override.CellAppearance = appearance98;
            this.GridInter.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridInter.DisplayLayout.Override.CellPadding = 0;
            appearance99.BackColor = System.Drawing.SystemColors.Control;
            appearance99.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance99.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance99.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance99.BorderColor = System.Drawing.SystemColors.Window;
            this.GridInter.DisplayLayout.Override.GroupByRowAppearance = appearance99;
            appearance100.TextHAlignAsString = "Left";
            this.GridInter.DisplayLayout.Override.HeaderAppearance = appearance100;
            this.GridInter.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridInter.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance101.BackColor = System.Drawing.SystemColors.Window;
            appearance101.BorderColor = System.Drawing.Color.Silver;
            this.GridInter.DisplayLayout.Override.RowAppearance = appearance101;
            this.GridInter.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridInter.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance102.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridInter.DisplayLayout.Override.TemplateAddRowAppearance = appearance102;
            this.GridInter.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridInter.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridInter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridInter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.GridInter.Location = new System.Drawing.Point(3, 28);
            this.GridInter.Name = "GridInter";
            this.GridInter.Size = new System.Drawing.Size(1824, 384);
            this.GridInter.TabIndex = 413;
            this.GridInter.Text = "ultraGrid1";
            this.GridInter.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridInter_InitializeLayout);
            this.GridInter.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridInter_InitializeRow);
            this.GridInter.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridInter_DoubleClickRow);
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.Transparent;
            this.Frame3.Controls.Add(this.tableLayoutPanel8);
            this.Frame3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Frame3.Location = new System.Drawing.Point(3, 3);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(1836, 392);
            this.Frame3.TabIndex = 413;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Arrete";
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.cmdActualiser, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.SSOleDBGrid2, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1830, 369);
            this.tableLayoutPanel8.TabIndex = 414;
            // 
            // cmdActualiser
            // 
            this.cmdActualiser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdActualiser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdActualiser.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdActualiser.FlatAppearance.BorderSize = 0;
            this.cmdActualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdActualiser.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdActualiser.ForeColor = System.Drawing.Color.White;
            this.cmdActualiser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdActualiser.Location = new System.Drawing.Point(1681, 2);
            this.cmdActualiser.Margin = new System.Windows.Forms.Padding(2);
            this.cmdActualiser.Name = "cmdActualiser";
            this.cmdActualiser.Size = new System.Drawing.Size(147, 27);
            this.cmdActualiser.TabIndex = 579;
            this.cmdActualiser.Text = "Actualiser";
            this.cmdActualiser.UseVisualStyleBackColor = false;
            this.cmdActualiser.Visible = false;
            this.cmdActualiser.Click += new System.EventHandler(this.cmdActualiser_Click);
            // 
            // SSOleDBGrid2
            // 
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            appearance103.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid2.DisplayLayout.Appearance = appearance103;
            this.SSOleDBGrid2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance104.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance104.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance104.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance104.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.Appearance = appearance104;
            appearance105.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance105;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance106.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance106.BackColor2 = System.Drawing.SystemColors.Control;
            appearance106.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance106.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid2.DisplayLayout.GroupByBox.PromptAppearance = appearance106;
            this.SSOleDBGrid2.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance107.BackColor = System.Drawing.SystemColors.Window;
            appearance107.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid2.DisplayLayout.Override.ActiveCellAppearance = appearance107;
            appearance108.BackColor = System.Drawing.SystemColors.Highlight;
            appearance108.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid2.DisplayLayout.Override.ActiveRowAppearance = appearance108;
            this.SSOleDBGrid2.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSOleDBGrid2.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid2.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.Override.CardAreaAppearance = appearance109;
            appearance110.BorderColor = System.Drawing.Color.Silver;
            appearance110.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid2.DisplayLayout.Override.CellAppearance = appearance110;
            this.SSOleDBGrid2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid2.DisplayLayout.Override.CellPadding = 0;
            appearance111.BackColor = System.Drawing.SystemColors.Control;
            appearance111.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance111.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance111.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance111.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid2.DisplayLayout.Override.GroupByRowAppearance = appearance111;
            appearance112.TextHAlignAsString = "Left";
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderAppearance = appearance112;
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance113.BackColor = System.Drawing.SystemColors.Window;
            appearance113.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid2.DisplayLayout.Override.RowAppearance = appearance113;
            this.SSOleDBGrid2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid2.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance114.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid2.DisplayLayout.Override.TemplateAddRowAppearance = appearance114;
            this.SSOleDBGrid2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.SSOleDBGrid2.Location = new System.Drawing.Point(3, 34);
            this.SSOleDBGrid2.Name = "SSOleDBGrid2";
            this.SSOleDBGrid2.Size = new System.Drawing.Size(1824, 332);
            this.SSOleDBGrid2.TabIndex = 413;
            this.SSOleDBGrid2.Text = "ultraGrid1";
            this.SSOleDBGrid2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid2_InitializeLayout);
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel13);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1842, 797);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 4;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.21951F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 72.19139F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 113F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 11.5891F));
            this.tableLayoutPanel13.Controls.Add(this.frame6, 1, 7);
            this.tableLayoutPanel13.Controls.Add(this.groupBox4, 0, 3);
            this.tableLayoutPanel13.Controls.Add(this.Frame4, 1, 3);
            this.tableLayoutPanel13.Controls.Add(this.Frame5, 1, 5);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel14, 2, 3);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel15, 1, 1);
            this.tableLayoutPanel13.Controls.Add(this.tabControl1, 3, 7);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel20, 1, 8);
            this.tableLayoutPanel13.Controls.Add(this.lblMail, 2, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 10;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1842, 797);
            this.tableLayoutPanel13.TabIndex = 583;
            // 
            // frame6
            // 
            this.frame6.BackColor = System.Drawing.Color.Transparent;
            this.frame6.Controls.Add(this.tableLayoutPanel12);
            this.frame6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.frame6.Location = new System.Drawing.Point(283, 313);
            this.frame6.Name = "frame6";
            this.frame6.Size = new System.Drawing.Size(1242, 114);
            this.frame6.TabIndex = 414;
            this.frame6.TabStop = false;
            this.frame6.Text = "Visualisation et impression des factures mise à jours";
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 6;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 109F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel12.Controls.Add(this.chk3bacsVisu, 5, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtVisu1, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtVisuDate1, 2, 1);
            this.tableLayoutPanel12.Controls.Add(this.txtVisu2, 4, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtVisuDate2, 4, 1);
            this.tableLayoutPanel12.Controls.Add(this.lblFin, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.label19, 3, 1);
            this.tableLayoutPanel12.Controls.Add(this.Option4, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.Option1, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.lblDebut, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.cmdVisu, 2, 2);
            this.tableLayoutPanel12.Controls.Add(this.cmdExport, 5, 2);
            this.tableLayoutPanel12.Controls.Add(this.cmdMial, 4, 2);
            this.tableLayoutPanel12.Controls.Add(this.OptFactEnCours, 0, 2);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 3;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1236, 91);
            this.tableLayoutPanel12.TabIndex = 413;
            // 
            // chk3bacsVisu
            // 
            this.chk3bacsVisu.AutoSize = true;
            this.chk3bacsVisu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk3bacsVisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chk3bacsVisu.Location = new System.Drawing.Point(890, 3);
            this.chk3bacsVisu.Name = "chk3bacsVisu";
            this.chk3bacsVisu.Size = new System.Drawing.Size(343, 24);
            this.chk3bacsVisu.TabIndex = 588;
            this.chk3bacsVisu.Text = "Impression 3 Bacs";
            this.chk3bacsVisu.UseVisualStyleBackColor = true;
            this.chk3bacsVisu.Visible = false;
            // 
            // lblFin
            // 
            this.lblFin.AutoSize = true;
            this.lblFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFin.Location = new System.Drawing.Point(495, 0);
            this.lblFin.Name = "lblFin";
            this.lblFin.Size = new System.Drawing.Size(42, 18);
            this.lblFin.TabIndex = 514;
            this.lblFin.Text = "à FM";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(495, 30);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(42, 30);
            this.label19.TabIndex = 515;
            this.label19.Text = "Au";
            // 
            // Option4
            // 
            this.Option4.AutoSize = true;
            this.Option4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option4.Location = new System.Drawing.Point(3, 33);
            this.Option4.Name = "Option4";
            this.Option4.Size = new System.Drawing.Size(103, 24);
            this.Option4.TabIndex = 587;
            this.Option4.TabStop = true;
            this.Option4.Text = "A partir du";
            this.Option4.UseVisualStyleBackColor = true;
            this.Option4.CheckedChanged += new System.EventHandler(this.Option4_CheckedChanged);
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Checked = true;
            this.Option1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option1.Location = new System.Drawing.Point(3, 3);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(103, 24);
            this.Option1.TabIndex = 586;
            this.Option1.TabStop = true;
            this.Option1.Text = "De facture n°";
            this.Option1.UseVisualStyleBackColor = true;
            this.Option1.CheckedChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // lblDebut
            // 
            this.lblDebut.AutoSize = true;
            this.lblDebut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDebut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDebut.Location = new System.Drawing.Point(112, 0);
            this.lblDebut.Name = "lblDebut";
            this.lblDebut.Size = new System.Drawing.Size(30, 30);
            this.lblDebut.TabIndex = 587;
            this.lblDebut.Text = "FM";
            this.lblDebut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdVisu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdVisu.ForeColor = System.Drawing.Color.White;
            this.cmdVisu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisu.Location = new System.Drawing.Point(147, 62);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(343, 27);
            this.cmdVisu.TabIndex = 581;
            this.cmdVisu.Text = "Visu";
            this.cmdVisu.UseVisualStyleBackColor = false;
            this.cmdVisu.Click += new System.EventHandler(this.cmdVisu_Click);
            // 
            // cmdExport
            // 
            this.cmdExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdExport.ForeColor = System.Drawing.Color.White;
            this.cmdExport.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExport.Location = new System.Drawing.Point(889, 62);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(345, 27);
            this.cmdExport.TabIndex = 582;
            this.cmdExport.Text = "Export pdf avec papier en tete";
            this.cmdExport.UseVisualStyleBackColor = false;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            // 
            // cmdMial
            // 
            this.cmdMial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMial.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMial.FlatAppearance.BorderSize = 0;
            this.cmdMial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMial.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdMial.ForeColor = System.Drawing.Color.White;
            this.cmdMial.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMial.Location = new System.Drawing.Point(542, 62);
            this.cmdMial.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMial.Name = "cmdMial";
            this.cmdMial.Size = new System.Drawing.Size(343, 27);
            this.cmdMial.TabIndex = 582;
            this.cmdMial.Text = "Mail";
            this.cmdMial.UseVisualStyleBackColor = false;
            this.cmdMial.Click += new System.EventHandler(this.cmdMial_Click);
            // 
            // OptFactEnCours
            // 
            this.OptFactEnCours.AutoSize = true;
            this.tableLayoutPanel12.SetColumnSpan(this.OptFactEnCours, 2);
            this.OptFactEnCours.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptFactEnCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptFactEnCours.Location = new System.Drawing.Point(3, 63);
            this.OptFactEnCours.Name = "OptFactEnCours";
            this.OptFactEnCours.Size = new System.Drawing.Size(139, 25);
            this.OptFactEnCours.TabIndex = 587;
            this.OptFactEnCours.TabStop = true;
            this.OptFactEnCours.Text = "Facture en cours";
            this.OptFactEnCours.UseVisualStyleBackColor = true;
            this.OptFactEnCours.CheckedChanged += new System.EventHandler(this.Option4_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.tableLayoutPanel9);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox4.Location = new System.Drawing.Point(3, 53);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(274, 114);
            this.groupBox4.TabIndex = 411;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Crée par";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Controls.Add(this.Combo1, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label21, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.lblNom, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.optPersonne, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.OptNomChoisis, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.OptTous, 0, 2);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(268, 91);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // Combo1
            // 
            appearance115.BackColor = System.Drawing.SystemColors.Window;
            appearance115.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.Combo1.DisplayLayout.Appearance = appearance115;
            this.Combo1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.Combo1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance116.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance116.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance116.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance116.BorderColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.GroupByBox.Appearance = appearance116;
            appearance117.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Combo1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance117;
            this.Combo1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance118.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance118.BackColor2 = System.Drawing.SystemColors.Control;
            appearance118.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance118.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Combo1.DisplayLayout.GroupByBox.PromptAppearance = appearance118;
            this.Combo1.DisplayLayout.MaxColScrollRegions = 1;
            this.Combo1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance119.BackColor = System.Drawing.SystemColors.Window;
            appearance119.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Combo1.DisplayLayout.Override.ActiveCellAppearance = appearance119;
            appearance120.BackColor = System.Drawing.SystemColors.Highlight;
            appearance120.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Combo1.DisplayLayout.Override.ActiveRowAppearance = appearance120;
            this.Combo1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.Combo1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance121.BackColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.Override.CardAreaAppearance = appearance121;
            appearance122.BorderColor = System.Drawing.Color.Silver;
            appearance122.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.Combo1.DisplayLayout.Override.CellAppearance = appearance122;
            this.Combo1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.Combo1.DisplayLayout.Override.CellPadding = 0;
            appearance123.BackColor = System.Drawing.SystemColors.Control;
            appearance123.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance123.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance123.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance123.BorderColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.Override.GroupByRowAppearance = appearance123;
            appearance124.TextHAlignAsString = "Left";
            this.Combo1.DisplayLayout.Override.HeaderAppearance = appearance124;
            this.Combo1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.Combo1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance125.BackColor = System.Drawing.SystemColors.Window;
            appearance125.BorderColor = System.Drawing.Color.Silver;
            this.Combo1.DisplayLayout.Override.RowAppearance = appearance125;
            this.Combo1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance126.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Combo1.DisplayLayout.Override.TemplateAddRowAppearance = appearance126;
            this.Combo1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.Combo1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.Combo1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.Combo1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Combo1.Location = new System.Drawing.Point(23, 33);
            this.Combo1.Name = "Combo1";
            this.Combo1.Size = new System.Drawing.Size(242, 27);
            this.Combo1.TabIndex = 544;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(23, 60);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(242, 31);
            this.label21.TabIndex = 543;
            this.label21.Text = "Tous";
            // 
            // lblNom
            // 
            this.lblNom.AutoSize = true;
            this.lblNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNom.Location = new System.Drawing.Point(23, 0);
            this.lblNom.Name = "lblNom";
            this.lblNom.Size = new System.Drawing.Size(242, 30);
            this.lblNom.TabIndex = 542;
            // 
            // optPersonne
            // 
            this.optPersonne.AutoSize = true;
            this.optPersonne.Checked = true;
            this.optPersonne.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optPersonne.Location = new System.Drawing.Point(3, 3);
            this.optPersonne.Name = "optPersonne";
            this.optPersonne.Size = new System.Drawing.Size(14, 22);
            this.optPersonne.TabIndex = 541;
            this.optPersonne.TabStop = true;
            this.optPersonne.Text = "radioButton13";
            this.optPersonne.UseVisualStyleBackColor = true;
            this.optPersonne.CheckedChanged += new System.EventHandler(this.optPersonne_CheckedChanged);
            this.optPersonne.VisibleChanged += new System.EventHandler(this.optPersonne_VisibleChanged);
            // 
            // OptNomChoisis
            // 
            this.OptNomChoisis.AutoSize = true;
            this.OptNomChoisis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptNomChoisis.Location = new System.Drawing.Point(3, 33);
            this.OptNomChoisis.Name = "OptNomChoisis";
            this.OptNomChoisis.Size = new System.Drawing.Size(14, 22);
            this.OptNomChoisis.TabIndex = 539;
            this.OptNomChoisis.Text = "radioButton11";
            this.OptNomChoisis.UseVisualStyleBackColor = true;
            this.OptNomChoisis.CheckedChanged += new System.EventHandler(this.OptNomChoisis_CheckedChanged);
            // 
            // OptTous
            // 
            this.OptTous.AutoSize = true;
            this.OptTous.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptTous.Location = new System.Drawing.Point(3, 63);
            this.OptTous.Name = "OptTous";
            this.OptTous.Size = new System.Drawing.Size(14, 22);
            this.OptTous.TabIndex = 540;
            this.OptTous.Text = "radioButton12";
            this.OptTous.UseVisualStyleBackColor = true;
            this.OptTous.CheckedChanged += new System.EventHandler(this.OptTous_CheckedChanged);
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.tableLayoutPanel10);
            this.Frame4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Frame4.Location = new System.Drawing.Point(283, 53);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(1242, 114);
            this.Frame4.TabIndex = 411;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Impression des factures non mise à jours";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 5;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Controls.Add(this.Option5, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.Option6, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.Option3, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label16, 2, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtImpDate1, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtDebut, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtImpDate2, 3, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtFin, 3, 0);
            this.tableLayoutPanel10.Controls.Add(this.label17, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.cmdImprimer, 3, 2);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 3;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1236, 91);
            this.tableLayoutPanel10.TabIndex = 413;
            // 
            // Option5
            // 
            this.Option5.AutoSize = true;
            this.Option5.Checked = true;
            this.Option5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option5.Location = new System.Drawing.Point(3, 3);
            this.Option5.Name = "Option5";
            this.Option5.Size = new System.Drawing.Size(134, 24);
            this.Option5.TabIndex = 591;
            this.Option5.TabStop = true;
            this.Option5.Text = "De facture n°XX";
            this.Option5.UseVisualStyleBackColor = true;
            this.Option5.CheckedChanged += new System.EventHandler(this.Option5_CheckedChanged);
            // 
            // Option6
            // 
            this.Option6.AutoSize = true;
            this.Option6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option6.Location = new System.Drawing.Point(3, 63);
            this.Option6.Name = "Option6";
            this.Option6.Size = new System.Drawing.Size(134, 25);
            this.Option6.TabIndex = 590;
            this.Option6.Text = "En cours";
            this.Option6.UseVisualStyleBackColor = true;
            this.Option6.CheckedChanged += new System.EventHandler(this.Option6_CheckedChanged);
            this.Option6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.Option6_KeyPress);
            // 
            // Option3
            // 
            this.Option3.AutoSize = true;
            this.Option3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option3.Location = new System.Drawing.Point(3, 33);
            this.Option3.Name = "Option3";
            this.Option3.Size = new System.Drawing.Size(134, 24);
            this.Option3.TabIndex = 589;
            this.Option3.Text = "A partir du";
            this.Option3.UseVisualStyleBackColor = true;
            this.Option3.CheckedChanged += new System.EventHandler(this.Option3_CheckedChanged);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(493, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 30);
            this.label16.TabIndex = 306;
            this.label16.Text = "à XX";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(493, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(40, 30);
            this.label17.TabIndex = 308;
            this.label17.Text = "Au";
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdImprimer.ForeColor = System.Drawing.Color.White;
            this.cmdImprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImprimer.Location = new System.Drawing.Point(538, 62);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(346, 27);
            this.cmdImprimer.TabIndex = 579;
            this.cmdImprimer.Text = "Imprimer";
            this.cmdImprimer.UseVisualStyleBackColor = false;
            this.cmdImprimer.Click += new System.EventHandler(this.cmdImprimer_Click);
            // 
            // Frame5
            // 
            this.Frame5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.Frame5.Controls.Add(this.tableLayoutPanel11);
            this.Frame5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Frame5.Location = new System.Drawing.Point(283, 183);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(1242, 114);
            this.Frame5.TabIndex = 412;
            this.Frame5.TabStop = false;
            this.Frame5.Text = "Impression et mise à jours";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 5;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel11.Controls.Add(this.Option7, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.label15, 2, 0);
            this.tableLayoutPanel11.Controls.Add(this.label14, 2, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtMiseAjour2, 3, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtMiseAjour1, 1, 0);
            this.tableLayoutPanel11.Controls.Add(this.txtMiseDate1, 1, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtMiseDate2, 3, 1);
            this.tableLayoutPanel11.Controls.Add(this.CmdMiseAjour, 3, 2);
            this.tableLayoutPanel11.Controls.Add(this.cmdAnalytique, 4, 2);
            this.tableLayoutPanel11.Controls.Add(this.Option2, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.Option8, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.chk3bacs, 4, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1236, 91);
            this.tableLayoutPanel11.TabIndex = 413;
            // 
            // Option7
            // 
            this.Option7.AutoSize = true;
            this.Option7.Checked = true;
            this.Option7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option7.Location = new System.Drawing.Point(3, 3);
            this.Option7.Name = "Option7";
            this.Option7.Size = new System.Drawing.Size(134, 24);
            this.Option7.TabIndex = 583;
            this.Option7.TabStop = true;
            this.Option7.Text = "De facture n°XX";
            this.Option7.UseVisualStyleBackColor = true;
            this.Option7.CheckedChanged += new System.EventHandler(this.Option7_CheckedChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(493, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(40, 30);
            this.label15.TabIndex = 504;
            this.label15.Text = "à XX";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(493, 30);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(40, 30);
            this.label14.TabIndex = 502;
            this.label14.Text = "Au";
            // 
            // CmdMiseAjour
            // 
            this.CmdMiseAjour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.CmdMiseAjour.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdMiseAjour.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CmdMiseAjour.FlatAppearance.BorderSize = 0;
            this.CmdMiseAjour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdMiseAjour.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdMiseAjour.ForeColor = System.Drawing.Color.White;
            this.CmdMiseAjour.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdMiseAjour.Location = new System.Drawing.Point(538, 62);
            this.CmdMiseAjour.Margin = new System.Windows.Forms.Padding(2);
            this.CmdMiseAjour.Name = "CmdMiseAjour";
            this.CmdMiseAjour.Size = new System.Drawing.Size(346, 27);
            this.CmdMiseAjour.TabIndex = 581;
            this.CmdMiseAjour.Text = "mise à jour";
            this.CmdMiseAjour.UseVisualStyleBackColor = false;
            this.CmdMiseAjour.Click += new System.EventHandler(this.CmdMiseAjour_Click);
            // 
            // cmdAnalytique
            // 
            this.cmdAnalytique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnalytique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnalytique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAnalytique.FlatAppearance.BorderSize = 0;
            this.cmdAnalytique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnalytique.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnalytique.ForeColor = System.Drawing.Color.White;
            this.cmdAnalytique.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnalytique.Location = new System.Drawing.Point(888, 62);
            this.cmdAnalytique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnalytique.Name = "cmdAnalytique";
            this.cmdAnalytique.Size = new System.Drawing.Size(346, 27);
            this.cmdAnalytique.TabIndex = 582;
            this.cmdAnalytique.Text = "Analytique";
            this.cmdAnalytique.UseVisualStyleBackColor = false;
            this.cmdAnalytique.Click += new System.EventHandler(this.cmdAnalytique_Click);
            // 
            // Option2
            // 
            this.Option2.AutoSize = true;
            this.Option2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option2.Location = new System.Drawing.Point(3, 33);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(134, 24);
            this.Option2.TabIndex = 584;
            this.Option2.Text = "A partir du";
            this.Option2.UseVisualStyleBackColor = true;
            this.Option2.CheckedChanged += new System.EventHandler(this.Option2_CheckedChanged);
            // 
            // Option8
            // 
            this.Option8.AutoSize = true;
            this.Option8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option8.Location = new System.Drawing.Point(3, 63);
            this.Option8.Name = "Option8";
            this.Option8.Size = new System.Drawing.Size(134, 25);
            this.Option8.TabIndex = 585;
            this.Option8.Text = "En cours";
            this.Option8.UseVisualStyleBackColor = true;
            this.Option8.CheckedChanged += new System.EventHandler(this.Option8_CheckedChanged);
            // 
            // chk3bacs
            // 
            this.chk3bacs.AutoSize = true;
            this.chk3bacs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk3bacs.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chk3bacs.Location = new System.Drawing.Point(889, 3);
            this.chk3bacs.Name = "chk3bacs";
            this.chk3bacs.Size = new System.Drawing.Size(344, 24);
            this.chk3bacs.TabIndex = 589;
            this.chk3bacs.Text = "Impression 3 Bacs";
            this.chk3bacs.UseVisualStyleBackColor = true;
            this.chk3bacs.Visible = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 1;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Controls.Add(this.Command1, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.cmdSyntheseMAJ, 0, 3);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(1531, 53);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 5;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(107, 114);
            this.tableLayoutPanel14.TabIndex = 583;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(2, 13);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(103, 31);
            this.Command1.TabIndex = 580;
            this.Command1.Text = "F. non M.A.J";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdSyntheseMAJ
            // 
            this.cmdSyntheseMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSyntheseMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSyntheseMAJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSyntheseMAJ.FlatAppearance.BorderSize = 0;
            this.cmdSyntheseMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSyntheseMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSyntheseMAJ.ForeColor = System.Drawing.Color.White;
            this.cmdSyntheseMAJ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSyntheseMAJ.Location = new System.Drawing.Point(2, 58);
            this.cmdSyntheseMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSyntheseMAJ.Name = "cmdSyntheseMAJ";
            this.cmdSyntheseMAJ.Size = new System.Drawing.Size(103, 31);
            this.cmdSyntheseMAJ.TabIndex = 581;
            this.cmdSyntheseMAJ.Text = "Synthèse";
            this.cmdSyntheseMAJ.UseVisualStyleBackColor = false;
            this.cmdSyntheseMAJ.Click += new System.EventHandler(this.cmdSyntheseMAJ_Click);
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Controls.Add(this.txtEnCours, 1, 0);
            this.tableLayoutPanel15.Controls.Add(this.label22, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(281, 11);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(1246, 30);
            this.tableLayoutPanel15.TabIndex = 584;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 0);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(156, 30);
            this.label22.TabIndex = 582;
            this.label22.Text = "N° de facture en cours";
            // 
            // tabControl1
            // 
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(1644, 313);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(195, 114);
            this.tabControl1.TabIndex = 585;
            this.tabControl1.Visible = false;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 4;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 241F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 171F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 206F));
            this.tableLayoutPanel20.Controls.Add(this.chkDuplicata, 3, 0);
            this.tableLayoutPanel20.Controls.Add(this.chkOS, 2, 0);
            this.tableLayoutPanel20.Controls.Add(this.chkBonPourAccord, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(283, 433);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1242, 32);
            this.tableLayoutPanel20.TabIndex = 586;
            // 
            // chkDuplicata
            // 
            this.chkDuplicata.AutoSize = true;
            this.chkDuplicata.Location = new System.Drawing.Point(1039, 3);
            this.chkDuplicata.Name = "chkDuplicata";
            this.chkDuplicata.Size = new System.Drawing.Size(88, 22);
            this.chkDuplicata.TabIndex = 0;
            this.chkDuplicata.Text = "Duplicata";
            this.chkDuplicata.UseVisualStyleBackColor = true;
            // 
            // chkOS
            // 
            this.chkOS.AutoSize = true;
            this.chkOS.Location = new System.Drawing.Point(868, 3);
            this.chkOS.Name = "chkOS";
            this.chkOS.Size = new System.Drawing.Size(112, 22);
            this.chkOS.TabIndex = 1;
            this.chkOS.Text = "Joindre l\'OS.";
            this.chkOS.UseVisualStyleBackColor = true;
            // 
            // chkBonPourAccord
            // 
            this.chkBonPourAccord.AutoSize = true;
            this.chkBonPourAccord.Location = new System.Drawing.Point(627, 3);
            this.chkBonPourAccord.Name = "chkBonPourAccord";
            this.chkBonPourAccord.Size = new System.Drawing.Size(204, 22);
            this.chkBonPourAccord.TabIndex = 2;
            this.chkBonPourAccord.Text = "Joindre le bon pour accord";
            this.chkBonPourAccord.UseVisualStyleBackColor = true;
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.tableLayoutPanel13.SetColumnSpan(this.lblMail, 2);
            this.lblMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold);
            this.lblMail.ForeColor = System.Drawing.Color.Yellow;
            this.lblMail.Location = new System.Drawing.Point(1548, 16);
            this.lblMail.Margin = new System.Windows.Forms.Padding(20, 6, 3, 0);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(253, 18);
            this.lblMail.TabIndex = 587;
            this.lblMail.Text = "ENVOI PAR MAIL OBLIGATOIRE";
            this.lblMail.Visible = false;
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.tableLayoutPanel16);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1842, 797);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 8;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 114F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 159F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 173F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 28.57143F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.85714F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 630F));
            this.tableLayoutPanel16.Controls.Add(this.SSOleDBGrid3, 0, 3);
            this.tableLayoutPanel16.Controls.Add(this.cmdRechImmeuble, 7, 1);
            this.tableLayoutPanel16.Controls.Add(this.txtImmeuble, 6, 1);
            this.tableLayoutPanel16.Controls.Add(this.label23, 5, 1);
            this.tableLayoutPanel16.Controls.Add(this.chkSolderDevis, 4, 1);
            this.tableLayoutPanel16.Controls.Add(this.CmdFax, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.cmdAchat, 1, 1);
            this.tableLayoutPanel16.Controls.Add(this.cmdInserer, 2, 1);
            this.tableLayoutPanel16.Controls.Add(this.PicDevis, 5, 2);
            this.tableLayoutPanel16.Controls.Add(this.optSelect, 1, 2);
            this.tableLayoutPanel16.Controls.Add(this.optDeselect, 2, 2);
            this.tableLayoutPanel16.Controls.Add(this.panel2, 3, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 4;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(1842, 797);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // SSOleDBGrid3
            // 
            this.tableLayoutPanel16.SetColumnSpan(this.SSOleDBGrid3, 8);
            appearance127.BackColor = System.Drawing.SystemColors.Window;
            appearance127.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid3.DisplayLayout.Appearance = appearance127;
            this.SSOleDBGrid3.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid3.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance128.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance128.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance128.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance128.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid3.DisplayLayout.GroupByBox.Appearance = appearance128;
            appearance129.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid3.DisplayLayout.GroupByBox.BandLabelAppearance = appearance129;
            this.SSOleDBGrid3.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance130.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance130.BackColor2 = System.Drawing.SystemColors.Control;
            appearance130.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance130.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid3.DisplayLayout.GroupByBox.PromptAppearance = appearance130;
            this.SSOleDBGrid3.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid3.DisplayLayout.MaxRowScrollRegions = 1;
            appearance131.BackColor = System.Drawing.SystemColors.Window;
            appearance131.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid3.DisplayLayout.Override.ActiveCellAppearance = appearance131;
            appearance132.BackColor = System.Drawing.SystemColors.Highlight;
            appearance132.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid3.DisplayLayout.Override.ActiveRowAppearance = appearance132;
            this.SSOleDBGrid3.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid3.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance133.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid3.DisplayLayout.Override.CardAreaAppearance = appearance133;
            appearance134.BorderColor = System.Drawing.Color.Silver;
            appearance134.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid3.DisplayLayout.Override.CellAppearance = appearance134;
            this.SSOleDBGrid3.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid3.DisplayLayout.Override.CellPadding = 0;
            appearance135.BackColor = System.Drawing.SystemColors.Control;
            appearance135.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance135.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance135.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance135.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid3.DisplayLayout.Override.GroupByRowAppearance = appearance135;
            appearance136.TextHAlignAsString = "Left";
            this.SSOleDBGrid3.DisplayLayout.Override.HeaderAppearance = appearance136;
            this.SSOleDBGrid3.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid3.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance137.BackColor = System.Drawing.SystemColors.Window;
            appearance137.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid3.DisplayLayout.Override.RowAppearance = appearance137;
            this.SSOleDBGrid3.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid3.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Extended;
            appearance138.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid3.DisplayLayout.Override.TemplateAddRowAppearance = appearance138;
            this.SSOleDBGrid3.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid3.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.SSOleDBGrid3.Location = new System.Drawing.Point(3, 86);
            this.SSOleDBGrid3.Name = "SSOleDBGrid3";
            this.SSOleDBGrid3.Size = new System.Drawing.Size(1836, 708);
            this.SSOleDBGrid3.TabIndex = 583;
            this.SSOleDBGrid3.Text = "ultraGrid1";
            this.SSOleDBGrid3.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid3_InitializeLayout);
            this.SSOleDBGrid3.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSOleDBGrid3_InitializeRow);
            this.SSOleDBGrid3.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.SSOleDBGrid3_ClickCellButton);
            this.SSOleDBGrid3.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.SSOleDBGrid3_BeforeRowsDeleted);
            this.SSOleDBGrid3.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid3_DoubleClickRow);
            // 
            // cmdRechImmeuble
            // 
            this.cmdRechImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechImmeuble.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechImmeuble.Image")));
            this.cmdRechImmeuble.Location = new System.Drawing.Point(1214, 23);
            this.cmdRechImmeuble.Name = "cmdRechImmeuble";
            this.cmdRechImmeuble.Size = new System.Drawing.Size(24, 20);
            this.cmdRechImmeuble.TabIndex = 503;
            this.cmdRechImmeuble.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechImmeuble, "Recherche de l\'immeuble");
            this.cmdRechImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechImmeuble.Click += new System.EventHandler(this.cmdRechImmeuble_Click);
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(819, 26);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(112, 18);
            this.label23.TabIndex = 384;
            this.label23.Text = "Code Immeuble";
            // 
            // chkSolderDevis
            // 
            this.chkSolderDevis.AutoSize = true;
            this.chkSolderDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkSolderDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chkSolderDevis.Location = new System.Drawing.Point(634, 23);
            this.chkSolderDevis.Name = "chkSolderDevis";
            this.chkSolderDevis.Size = new System.Drawing.Size(179, 25);
            this.chkSolderDevis.TabIndex = 570;
            this.chkSolderDevis.Text = "Solder les devis sélectionnées";
            this.chkSolderDevis.UseVisualStyleBackColor = true;
            this.chkSolderDevis.Visible = false;
            // 
            // CmdFax
            // 
            this.CmdFax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdFax.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdFax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CmdFax.FlatAppearance.BorderSize = 0;
            this.CmdFax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.CmdFax.ForeColor = System.Drawing.Color.White;
            this.CmdFax.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdFax.Location = new System.Drawing.Point(2, 22);
            this.CmdFax.Margin = new System.Windows.Forms.Padding(2);
            this.CmdFax.Name = "CmdFax";
            this.CmdFax.Size = new System.Drawing.Size(110, 27);
            this.CmdFax.TabIndex = 580;
            this.CmdFax.Text = "Fax/Courrier";
            this.CmdFax.UseVisualStyleBackColor = false;
            this.CmdFax.Click += new System.EventHandler(this.CmdFax_Click);
            // 
            // cmdAchat
            // 
            this.cmdAchat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAchat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAchat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAchat.FlatAppearance.BorderSize = 0;
            this.cmdAchat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAchat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdAchat.ForeColor = System.Drawing.Color.White;
            this.cmdAchat.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAchat.Location = new System.Drawing.Point(116, 22);
            this.cmdAchat.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAchat.Name = "cmdAchat";
            this.cmdAchat.Size = new System.Drawing.Size(155, 27);
            this.cmdAchat.TabIndex = 581;
            this.cmdAchat.Text = "Visu";
            this.cmdAchat.UseVisualStyleBackColor = false;
            this.cmdAchat.Click += new System.EventHandler(this.cmdAchat_Click);
            // 
            // cmdInserer
            // 
            this.cmdInserer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdInserer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdInserer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdInserer.FlatAppearance.BorderSize = 0;
            this.cmdInserer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdInserer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdInserer.ForeColor = System.Drawing.Color.White;
            this.cmdInserer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdInserer.Location = new System.Drawing.Point(275, 22);
            this.cmdInserer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdInserer.Name = "cmdInserer";
            this.cmdInserer.Size = new System.Drawing.Size(169, 27);
            this.cmdInserer.TabIndex = 582;
            this.cmdInserer.Text = "Inserer";
            this.cmdInserer.UseVisualStyleBackColor = false;
            this.cmdInserer.Click += new System.EventHandler(this.cmdInserer_Click);
            // 
            // PicDevis
            // 
            this.PicDevis.ColumnCount = 3;
            this.tableLayoutPanel16.SetColumnSpan(this.PicDevis, 3);
            this.PicDevis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.PicDevis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PicDevis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 349F));
            this.PicDevis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.PicDevis.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.PicDevis.Controls.Add(this.cmdDevis, 2, 0);
            this.PicDevis.Controls.Add(this.txtNoDevis, 1, 0);
            this.PicDevis.Controls.Add(this.label7, 0, 0);
            this.PicDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PicDevis.Location = new System.Drawing.Point(817, 52);
            this.PicDevis.Margin = new System.Windows.Forms.Padding(1);
            this.PicDevis.Name = "PicDevis";
            this.PicDevis.RowCount = 1;
            this.PicDevis.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.PicDevis.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.PicDevis.Size = new System.Drawing.Size(1024, 30);
            this.PicDevis.TabIndex = 586;
            // 
            // cmdDevis
            // 
            this.cmdDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDevis.FlatAppearance.BorderSize = 0;
            this.cmdDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDevis.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdDevis.Image = ((System.Drawing.Image)(resources.GetObject("cmdDevis.Image")));
            this.cmdDevis.Location = new System.Drawing.Point(678, 3);
            this.cmdDevis.Name = "cmdDevis";
            this.cmdDevis.Size = new System.Drawing.Size(24, 20);
            this.cmdDevis.TabIndex = 503;
            this.cmdDevis.Tag = "";
            this.cmdDevis.UseVisualStyleBackColor = false;
            this.cmdDevis.Click += new System.EventHandler(this.cmdDevis_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(110, 18);
            this.label7.TabIndex = 384;
            this.label7.Text = "No devis :";
            // 
            // optSelect
            // 
            this.optSelect.AutoSize = true;
            this.optSelect.Location = new System.Drawing.Point(117, 54);
            this.optSelect.Name = "optSelect";
            this.optSelect.Size = new System.Drawing.Size(140, 22);
            this.optSelect.TabIndex = 584;
            this.optSelect.TabStop = true;
            this.optSelect.Text = "Tout sélectionner";
            this.optSelect.UseVisualStyleBackColor = true;
            this.optSelect.CheckedChanged += new System.EventHandler(this.optSelect_CheckedChanged);
            // 
            // optDeselect
            // 
            this.optDeselect.AutoSize = true;
            this.optDeselect.Location = new System.Drawing.Point(276, 54);
            this.optDeselect.Name = "optDeselect";
            this.optDeselect.Size = new System.Drawing.Size(156, 22);
            this.optDeselect.TabIndex = 584;
            this.optDeselect.TabStop = true;
            this.optDeselect.Text = "Tout désélectionner";
            this.optDeselect.UseVisualStyleBackColor = true;
            this.optDeselect.CheckedChanged += new System.EventHandler(this.optDeselect_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.optDevis1);
            this.panel2.Controls.Add(this.optAf);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(446, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.tableLayoutPanel16.SetRowSpan(this.panel2, 2);
            this.panel2.Size = new System.Drawing.Size(185, 51);
            this.panel2.TabIndex = 587;
            // 
            // optDevis1
            // 
            this.optDevis1.Enabled = false;
            this.optDevis1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optDevis1.Location = new System.Drawing.Point(32, 1);
            this.optDevis1.Margin = new System.Windows.Forms.Padding(0);
            this.optDevis1.Name = "optDevis1";
            this.optDevis1.Size = new System.Drawing.Size(67, 20);
            this.optDevis1.TabIndex = 538;
            this.optDevis1.Text = "Devis";
            this.optDevis1.UseVisualStyleBackColor = true;
            // 
            // optAf
            // 
            this.optAf.Checked = true;
            this.optAf.Enabled = false;
            this.optAf.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optAf.Location = new System.Drawing.Point(35, 24);
            this.optAf.Name = "optAf";
            this.optAf.Size = new System.Drawing.Size(71, 25);
            this.optAf.TabIndex = 539;
            this.optAf.TabStop = true;
            this.optAf.Text = "Editée";
            this.optAf.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.groupBox9);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1842, 797);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.tableLayoutPanel17);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox9.Location = new System.Drawing.Point(0, 0);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1842, 797);
            this.groupBox9.TabIndex = 411;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Informations sur ls contrats liés à l\'immeuble";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel17.Controls.Add(this.GridContrat, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.lblCodeImmeuble, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.cmdHistorique, 1, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1836, 774);
            this.tableLayoutPanel17.TabIndex = 1;
            // 
            // GridContrat
            // 
            this.tableLayoutPanel17.SetColumnSpan(this.GridContrat, 2);
            appearance139.BackColor = System.Drawing.SystemColors.Window;
            appearance139.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridContrat.DisplayLayout.Appearance = appearance139;
            this.GridContrat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridContrat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridContrat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance140.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance140.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance140.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance140.BorderColor = System.Drawing.SystemColors.Window;
            this.GridContrat.DisplayLayout.GroupByBox.Appearance = appearance140;
            appearance141.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridContrat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance141;
            this.GridContrat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance142.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance142.BackColor2 = System.Drawing.SystemColors.Control;
            appearance142.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance142.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridContrat.DisplayLayout.GroupByBox.PromptAppearance = appearance142;
            this.GridContrat.DisplayLayout.MaxColScrollRegions = 1;
            this.GridContrat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance143.BackColor = System.Drawing.SystemColors.Window;
            appearance143.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridContrat.DisplayLayout.Override.ActiveCellAppearance = appearance143;
            appearance144.BackColor = System.Drawing.SystemColors.Highlight;
            appearance144.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridContrat.DisplayLayout.Override.ActiveRowAppearance = appearance144;
            this.GridContrat.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridContrat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridContrat.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridContrat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridContrat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance145.BackColor = System.Drawing.SystemColors.Window;
            this.GridContrat.DisplayLayout.Override.CardAreaAppearance = appearance145;
            appearance146.BorderColor = System.Drawing.Color.Silver;
            appearance146.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridContrat.DisplayLayout.Override.CellAppearance = appearance146;
            this.GridContrat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridContrat.DisplayLayout.Override.CellPadding = 0;
            appearance147.BackColor = System.Drawing.SystemColors.Control;
            appearance147.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance147.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance147.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance147.BorderColor = System.Drawing.SystemColors.Window;
            this.GridContrat.DisplayLayout.Override.GroupByRowAppearance = appearance147;
            appearance148.TextHAlignAsString = "Left";
            this.GridContrat.DisplayLayout.Override.HeaderAppearance = appearance148;
            this.GridContrat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridContrat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance149.BackColor = System.Drawing.SystemColors.Window;
            appearance149.BorderColor = System.Drawing.Color.Silver;
            this.GridContrat.DisplayLayout.Override.RowAppearance = appearance149;
            this.GridContrat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridContrat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance150.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridContrat.DisplayLayout.Override.TemplateAddRowAppearance = appearance150;
            this.GridContrat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridContrat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.GridContrat.Location = new System.Drawing.Point(3, 41);
            this.GridContrat.Name = "GridContrat";
            this.GridContrat.Size = new System.Drawing.Size(1830, 730);
            this.GridContrat.TabIndex = 583;
            this.GridContrat.Text = "ultraGrid1";
            this.GridContrat.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridContrat_InitializeLayout);
            this.GridContrat.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridContrat_InitializeRow);
            // 
            // lblCodeImmeuble
            // 
            this.lblCodeImmeuble.AutoSize = true;
            this.lblCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodeImmeuble.Location = new System.Drawing.Point(3, 0);
            this.lblCodeImmeuble.Name = "lblCodeImmeuble";
            this.lblCodeImmeuble.Size = new System.Drawing.Size(1757, 38);
            this.lblCodeImmeuble.TabIndex = 585;
            // 
            // cmdHistorique
            // 
            this.cmdHistorique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdHistorique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHistorique.FlatAppearance.BorderSize = 0;
            this.cmdHistorique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHistorique.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHistorique.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistorique.Image")));
            this.cmdHistorique.Location = new System.Drawing.Point(1765, 2);
            this.cmdHistorique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHistorique.Name = "cmdHistorique";
            this.cmdHistorique.Size = new System.Drawing.Size(69, 34);
            this.cmdHistorique.TabIndex = 584;
            this.cmdHistorique.Tag = "";
            this.cmdHistorique.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdHistorique, "Historique des interventions pour l\'immeuble");
            this.cmdHistorique.UseVisualStyleBackColor = false;
            this.cmdHistorique.Click += new System.EventHandler(this.cmdHistorique_Click);
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.button1);
            this.ultraTabPageControl7.Controls.Add(this.groupBox5);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(1842, 797);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(1137, 167);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 416;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.txt_9);
            this.groupBox5.Controls.Add(this.txt_6);
            this.groupBox5.Controls.Add(this.txt_8);
            this.groupBox5.Controls.Add(this.txt_7);
            this.groupBox5.Controls.Add(this.txt_5);
            this.groupBox5.Controls.Add(this.txt_4);
            this.groupBox5.Controls.Add(this.txt_3);
            this.groupBox5.Controls.Add(this.txt_2);
            this.groupBox5.Controls.Add(this.txt_1);
            this.groupBox5.Controls.Add(this.txt_0);
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox5.Location = new System.Drawing.Point(24, 24);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(812, 294);
            this.groupBox5.TabIndex = 415;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Adresse de facturation";
            // 
            // txt_9
            // 
            this.txt_9.Location = new System.Drawing.Point(334, 251);
            this.txt_9.Name = "txt_9";
            this.txt_9.Size = new System.Drawing.Size(322, 24);
            this.txt_9.TabIndex = 593;
            this.txt_9.Visible = false;
            // 
            // txt_6
            // 
            this.txt_6.Location = new System.Drawing.Point(6, 251);
            this.txt_6.Name = "txt_6";
            this.txt_6.Size = new System.Drawing.Size(322, 24);
            this.txt_6.TabIndex = 592;
            this.txt_6.Visible = false;
            // 
            // txt_8
            // 
            this.txt_8.Location = new System.Drawing.Point(475, 173);
            this.txt_8.Name = "txt_8";
            this.txt_8.Size = new System.Drawing.Size(322, 24);
            this.txt_8.TabIndex = 591;
            this.txt_8.Visible = false;
            // 
            // txt_7
            // 
            this.txt_7.Location = new System.Drawing.Point(481, 53);
            this.txt_7.Name = "txt_7";
            this.txt_7.Size = new System.Drawing.Size(322, 24);
            this.txt_7.TabIndex = 590;
            this.txt_7.Visible = false;
            // 
            // txt_5
            // 
            this.txt_5.Location = new System.Drawing.Point(6, 173);
            this.txt_5.Name = "txt_5";
            this.txt_5.Size = new System.Drawing.Size(322, 24);
            this.txt_5.TabIndex = 589;
            this.txt_5.Visible = false;
            // 
            // txt_4
            // 
            this.txt_4.Location = new System.Drawing.Point(6, 143);
            this.txt_4.Name = "txt_4";
            this.txt_4.Size = new System.Drawing.Size(322, 24);
            this.txt_4.TabIndex = 588;
            this.txt_4.Visible = false;
            // 
            // txt_3
            // 
            this.txt_3.Location = new System.Drawing.Point(6, 113);
            this.txt_3.Name = "txt_3";
            this.txt_3.Size = new System.Drawing.Size(322, 24);
            this.txt_3.TabIndex = 587;
            this.txt_3.Visible = false;
            // 
            // txt_2
            // 
            this.txt_2.Location = new System.Drawing.Point(6, 83);
            this.txt_2.Name = "txt_2";
            this.txt_2.Size = new System.Drawing.Size(322, 24);
            this.txt_2.TabIndex = 586;
            this.txt_2.Visible = false;
            // 
            // txt_1
            // 
            this.txt_1.Location = new System.Drawing.Point(6, 53);
            this.txt_1.Name = "txt_1";
            this.txt_1.Size = new System.Drawing.Size(322, 24);
            this.txt_1.TabIndex = 585;
            this.txt_1.Visible = false;
            // 
            // txt_0
            // 
            this.txt_0.Location = new System.Drawing.Point(6, 23);
            this.txt_0.Name = "txt_0";
            this.txt_0.Size = new System.Drawing.Size(322, 24);
            this.txt_0.TabIndex = 584;
            this.txt_0.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.cmdValider);
            this.flowLayoutPanel1.Controls.Add(this.cmdSupprimer);
            this.flowLayoutPanel1.Controls.Add(this.cmdMenu);
            this.flowLayoutPanel1.Controls.Add(this.cmdValidOpt);
            this.flowLayoutPanel1.Controls.Add(this.txtVenteMem);
            this.flowLayoutPanel1.Controls.Add(this.lblSstReel);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1396, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(451, 124);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = ((System.Drawing.Image)(resources.GetObject("CmdRechercher.Image")));
            this.CmdRechercher.Location = new System.Drawing.Point(2, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 576;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = ((System.Drawing.Image)(resources.GetObject("cmdAjouter.Image")));
            this.cmdAjouter.Location = new System.Drawing.Point(66, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 578;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = ((System.Drawing.Image)(resources.GetObject("cmdAnnuler.Image")));
            this.cmdAnnuler.Location = new System.Drawing.Point(130, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 579;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdValider
            // 
            this.cmdValider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdValider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValider.FlatAppearance.BorderSize = 0;
            this.cmdValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValider.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValider.Image = ((System.Drawing.Image)(resources.GetObject("cmdValider.Image")));
            this.cmdValider.Location = new System.Drawing.Point(194, 2);
            this.cmdValider.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(60, 35);
            this.cmdValider.TabIndex = 577;
            this.cmdValider.Tag = "";
            this.toolTip1.SetToolTip(this.cmdValider, "Enregistrer");
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Click += new System.EventHandler(this.cmdValider_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = ((System.Drawing.Image)(resources.GetObject("cmdSupprimer.Image")));
            this.cmdSupprimer.Location = new System.Drawing.Point(258, 2);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 581;
            this.cmdSupprimer.Tag = "";
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdSupprimer, "Supprimer");
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Click += new System.EventHandler(this.cmdSupprimer_Click);
            // 
            // cmdMenu
            // 
            this.cmdMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMenu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdMenu.FlatAppearance.BorderSize = 0;
            this.cmdMenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMenu.Image = ((System.Drawing.Image)(resources.GetObject("cmdMenu.Image")));
            this.cmdMenu.Location = new System.Drawing.Point(322, 2);
            this.cmdMenu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMenu.Name = "cmdMenu";
            this.cmdMenu.Size = new System.Drawing.Size(60, 35);
            this.cmdMenu.TabIndex = 580;
            this.cmdMenu.UseVisualStyleBackColor = false;
            this.cmdMenu.Click += new System.EventHandler(this.cmdMenu_Click);
            // 
            // cmdValidOpt
            // 
            this.cmdValidOpt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdValidOpt.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdValidOpt.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdValidOpt.FlatAppearance.BorderSize = 0;
            this.cmdValidOpt.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValidOpt.ForeColor = System.Drawing.Color.White;
            this.cmdValidOpt.Location = new System.Drawing.Point(386, 2);
            this.cmdValidOpt.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValidOpt.Name = "cmdValidOpt";
            this.cmdValidOpt.Size = new System.Drawing.Size(60, 35);
            this.cmdValidOpt.TabIndex = 582;
            this.cmdValidOpt.Text = "Entrer";
            this.cmdValidOpt.UseVisualStyleBackColor = false;
            this.cmdValidOpt.Click += new System.EventHandler(this.cmdValidOpt_Click);
            // 
            // txtVenteMem
            // 
            this.txtVenteMem.Location = new System.Drawing.Point(3, 42);
            this.txtVenteMem.Name = "txtVenteMem";
            this.txtVenteMem.Size = new System.Drawing.Size(100, 20);
            this.txtVenteMem.TabIndex = 583;
            this.txtVenteMem.Visible = false;
            // 
            // lblSstReel
            // 
            this.lblSstReel.Location = new System.Drawing.Point(109, 39);
            this.lblSstReel.Name = "lblSstReel";
            this.lblSstReel.Size = new System.Drawing.Size(119, 25);
            this.lblSstReel.TabIndex = 584;
            this.lblSstReel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSstReel.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Controls.Add(this.Label243, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label242, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label241, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Label240, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtSituation, 4, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtCharges, 4, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTotalAchats, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTotalHeures, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblTotalSituations, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblMontantDevis, 3, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1384, 67);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Label243
            // 
            this.Label243.AutoSize = true;
            this.Label243.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label243.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label243.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label243.Location = new System.Drawing.Point(1008, 0);
            this.Label243.Margin = new System.Windows.Forms.Padding(0);
            this.Label243.Name = "Label243";
            this.Label243.Size = new System.Drawing.Size(336, 30);
            this.Label243.TabIndex = 387;
            this.Label243.Tag = "3";
            this.Label243.Text = "Montant total du devis";
            // 
            // Label242
            // 
            this.Label242.AutoSize = true;
            this.Label242.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label242.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label242.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label242.Location = new System.Drawing.Point(672, 0);
            this.Label242.Margin = new System.Windows.Forms.Padding(0);
            this.Label242.Name = "Label242";
            this.Label242.Size = new System.Drawing.Size(336, 30);
            this.Label242.TabIndex = 386;
            this.Label242.Tag = "2";
            this.Label242.Text = "Total des situations";
            // 
            // Label241
            // 
            this.Label241.AutoSize = true;
            this.Label241.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label241.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label241.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label241.Location = new System.Drawing.Point(336, 0);
            this.Label241.Margin = new System.Windows.Forms.Padding(0);
            this.Label241.Name = "Label241";
            this.Label241.Size = new System.Drawing.Size(336, 30);
            this.Label241.TabIndex = 385;
            this.Label241.Tag = "1";
            this.Label241.Text = "Nombre d\'heures";
            // 
            // Label240
            // 
            this.Label240.AutoSize = true;
            this.Label240.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label240.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label240.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label240.Location = new System.Drawing.Point(0, 0);
            this.Label240.Margin = new System.Windows.Forms.Padding(0);
            this.Label240.Name = "Label240";
            this.Label240.Size = new System.Drawing.Size(336, 30);
            this.Label240.TabIndex = 384;
            this.Label240.Tag = "0";
            this.Label240.Text = "Achats réels";
            // 
            // lblTotalAchats
            // 
            this.lblTotalAchats.AutoSize = true;
            this.lblTotalAchats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotalAchats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalAchats.Location = new System.Drawing.Point(0, 30);
            this.lblTotalAchats.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotalAchats.Name = "lblTotalAchats";
            this.lblTotalAchats.Size = new System.Drawing.Size(336, 37);
            this.lblTotalAchats.TabIndex = 504;
            this.lblTotalAchats.Text = "lblTotalAchats";
            // 
            // lblTotalHeures
            // 
            this.lblTotalHeures.AutoSize = true;
            this.lblTotalHeures.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotalHeures.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalHeures.Location = new System.Drawing.Point(336, 30);
            this.lblTotalHeures.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotalHeures.Name = "lblTotalHeures";
            this.lblTotalHeures.Size = new System.Drawing.Size(336, 37);
            this.lblTotalHeures.TabIndex = 505;
            this.lblTotalHeures.Text = "lblTotalHeures";
            // 
            // lblTotalSituations
            // 
            this.lblTotalSituations.AutoSize = true;
            this.lblTotalSituations.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblTotalSituations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalSituations.Location = new System.Drawing.Point(672, 30);
            this.lblTotalSituations.Margin = new System.Windows.Forms.Padding(0);
            this.lblTotalSituations.Name = "lblTotalSituations";
            this.lblTotalSituations.Size = new System.Drawing.Size(336, 37);
            this.lblTotalSituations.TabIndex = 506;
            this.lblTotalSituations.Text = "lblTotalSituations";
            // 
            // lblMontantDevis
            // 
            this.lblMontantDevis.AutoSize = true;
            this.lblMontantDevis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMontantDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMontantDevis.Location = new System.Drawing.Point(1008, 30);
            this.lblMontantDevis.Margin = new System.Windows.Forms.Padding(0);
            this.lblMontantDevis.Name = "lblMontantDevis";
            this.lblMontantDevis.Size = new System.Drawing.Size(336, 37);
            this.lblMontantDevis.TabIndex = 507;
            this.lblMontantDevis.Text = "lblMontantDevis";
            // 
            // frmSynthese
            // 
            this.frmSynthese.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.frmSynthese.BackColor = System.Drawing.Color.Transparent;
            this.frmSynthese.Controls.Add(this.tableLayoutPanel1);
            this.frmSynthese.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.frmSynthese.Location = new System.Drawing.Point(0, -1);
            this.frmSynthese.Margin = new System.Windows.Forms.Padding(0);
            this.frmSynthese.Name = "frmSynthese";
            this.frmSynthese.Size = new System.Drawing.Size(1390, 90);
            this.frmSynthese.TabIndex = 411;
            this.frmSynthese.TabStop = false;
            this.frmSynthese.Text = "Synthèse";
            this.frmSynthese.Visible = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.SSTab1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel19, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel2.TabIndex = 412;
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Controls.Add(this.ultraTabPageControl3);
            this.SSTab1.Controls.Add(this.ultraTabPageControl4);
            this.SSTab1.Controls.Add(this.ultraTabPageControl5);
            this.SSTab1.Controls.Add(this.ultraTabPageControl6);
            this.SSTab1.Controls.Add(this.ultraTabPageControl7);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.SSTab1.Location = new System.Drawing.Point(3, 133);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1844, 821);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab6.TabPage = this.ultraTabPageControl1;
            ultraTab6.Text = "EN TETE";
            ultraTab7.TabPage = this.ultraTabPageControl2;
            ultraTab7.Text = "CORPS";
            ultraTab8.TabPage = this.ultraTabPageControl3;
            ultraTab8.Text = "ARRETE";
            ultraTab9.TabPage = this.ultraTabPageControl4;
            ultraTab9.Text = "IMPRESSION";
            ultraTab10.TabPage = this.ultraTabPageControl5;
            ultraTab10.Text = "PREFACT.";
            ultraTab11.TabPage = this.ultraTabPageControl6;
            ultraTab11.Text = "CONTRAT";
            ultraTab1.TabPage = this.ultraTabPageControl7;
            ultraTab1.Text = "tab1";
            ultraTab1.Visible = false;
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab6,
            ultraTab7,
            ultraTab8,
            ultraTab9,
            ultraTab10,
            ultraTab11,
            ultraTab1});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.SSTab1.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SSTab1_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1842, 797);
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 460F));
            this.tableLayoutPanel19.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1850, 130);
            this.tableLayoutPanel19.TabIndex = 411;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.fraSynthese);
            this.panel1.Controls.Add(this.frmSynthese);
            this.panel1.Controls.Add(this.Frame9);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1390, 130);
            this.panel1.TabIndex = 1;
            // 
            // fraSynthese
            // 
            this.fraSynthese.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.fraSynthese.BackColor = System.Drawing.Color.Transparent;
            this.fraSynthese.Controls.Add(this.tableLayoutPanel25);
            this.fraSynthese.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.fraSynthese.Location = new System.Drawing.Point(3, -1);
            this.fraSynthese.Margin = new System.Windows.Forms.Padding(0);
            this.fraSynthese.Name = "fraSynthese";
            this.fraSynthese.Size = new System.Drawing.Size(1384, 126);
            this.fraSynthese.TabIndex = 412;
            this.fraSynthese.TabStop = false;
            this.fraSynthese.Text = "Synthèse";
            this.fraSynthese.Visible = false;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 5;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel25.Controls.Add(this.lblCoefficientReel, 4, 3);
            this.tableLayoutPanel25.Controls.Add(this.label40, 3, 3);
            this.tableLayoutPanel25.Controls.Add(this.lblhtDevis, 1, 3);
            this.tableLayoutPanel25.Controls.Add(this.lbl6, 0, 3);
            this.tableLayoutPanel25.Controls.Add(this.lblVentesReelles, 4, 1);
            this.tableLayoutPanel25.Controls.Add(this.lblChargesReelles, 0, 2);
            this.tableLayoutPanel25.Controls.Add(this.lblAchatsReels, 3, 1);
            this.tableLayoutPanel25.Controls.Add(this.lblSScompetence, 2, 1);
            this.tableLayoutPanel25.Controls.Add(this.lblSScapacite, 1, 1);
            this.tableLayoutPanel25.Controls.Add(this.lblMOReel, 0, 1);
            this.tableLayoutPanel25.Controls.Add(this.label30, 4, 0);
            this.tableLayoutPanel25.Controls.Add(this.label29, 3, 0);
            this.tableLayoutPanel25.Controls.Add(this.label28, 2, 0);
            this.tableLayoutPanel25.Controls.Add(this.label27, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 4;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(1378, 103);
            this.tableLayoutPanel25.TabIndex = 413;
            // 
            // lblCoefficientReel
            // 
            this.lblCoefficientReel.AutoSize = true;
            this.lblCoefficientReel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCoefficientReel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCoefficientReel.Location = new System.Drawing.Point(1100, 75);
            this.lblCoefficientReel.Margin = new System.Windows.Forms.Padding(0);
            this.lblCoefficientReel.Name = "lblCoefficientReel";
            this.lblCoefficientReel.Size = new System.Drawing.Size(278, 28);
            this.lblCoefficientReel.TabIndex = 14;
            this.lblCoefficientReel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label40.Location = new System.Drawing.Point(828, 75);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(269, 28);
            this.label40.TabIndex = 13;
            this.label40.Text = "Coef. réel réalisé";
            this.label40.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblhtDevis
            // 
            this.lblhtDevis.AutoSize = true;
            this.lblhtDevis.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblhtDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblhtDevis.Location = new System.Drawing.Point(275, 75);
            this.lblhtDevis.Margin = new System.Windows.Forms.Padding(0);
            this.lblhtDevis.Name = "lblhtDevis";
            this.lblhtDevis.Size = new System.Drawing.Size(275, 28);
            this.lblhtDevis.TabIndex = 12;
            this.lblhtDevis.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbl6
            // 
            this.lbl6.AutoSize = true;
            this.lbl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbl6.Location = new System.Drawing.Point(3, 75);
            this.lbl6.Name = "lbl6";
            this.lbl6.Size = new System.Drawing.Size(269, 28);
            this.lbl6.TabIndex = 11;
            this.lbl6.Text = "HT DEVIS";
            this.lbl6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblVentesReelles
            // 
            this.lblVentesReelles.AutoSize = true;
            this.lblVentesReelles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblVentesReelles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblVentesReelles.Location = new System.Drawing.Point(1100, 25);
            this.lblVentesReelles.Margin = new System.Windows.Forms.Padding(0);
            this.lblVentesReelles.Name = "lblVentesReelles";
            this.tableLayoutPanel25.SetRowSpan(this.lblVentesReelles, 2);
            this.lblVentesReelles.Size = new System.Drawing.Size(278, 50);
            this.lblVentesReelles.TabIndex = 10;
            this.lblVentesReelles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblChargesReelles
            // 
            this.lblChargesReelles.AutoSize = true;
            this.lblChargesReelles.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel25.SetColumnSpan(this.lblChargesReelles, 4);
            this.lblChargesReelles.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChargesReelles.Location = new System.Drawing.Point(0, 50);
            this.lblChargesReelles.Margin = new System.Windows.Forms.Padding(0);
            this.lblChargesReelles.Name = "lblChargesReelles";
            this.lblChargesReelles.Size = new System.Drawing.Size(1100, 25);
            this.lblChargesReelles.TabIndex = 9;
            this.lblChargesReelles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblAchatsReels
            // 
            this.lblAchatsReels.AutoSize = true;
            this.lblAchatsReels.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblAchatsReels.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAchatsReels.Location = new System.Drawing.Point(825, 25);
            this.lblAchatsReels.Margin = new System.Windows.Forms.Padding(0);
            this.lblAchatsReels.Name = "lblAchatsReels";
            this.lblAchatsReels.Size = new System.Drawing.Size(275, 25);
            this.lblAchatsReels.TabIndex = 8;
            this.lblAchatsReels.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSScompetence
            // 
            this.lblSScompetence.AutoSize = true;
            this.lblSScompetence.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSScompetence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSScompetence.Location = new System.Drawing.Point(550, 25);
            this.lblSScompetence.Margin = new System.Windows.Forms.Padding(0);
            this.lblSScompetence.Name = "lblSScompetence";
            this.lblSScompetence.Size = new System.Drawing.Size(275, 25);
            this.lblSScompetence.TabIndex = 7;
            this.lblSScompetence.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblSScapacite
            // 
            this.lblSScapacite.AutoSize = true;
            this.lblSScapacite.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblSScapacite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSScapacite.Location = new System.Drawing.Point(275, 25);
            this.lblSScapacite.Margin = new System.Windows.Forms.Padding(0);
            this.lblSScapacite.Name = "lblSScapacite";
            this.lblSScapacite.Size = new System.Drawing.Size(275, 25);
            this.lblSScapacite.TabIndex = 6;
            this.lblSScapacite.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblMOReel
            // 
            this.lblMOReel.AutoSize = true;
            this.lblMOReel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMOReel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMOReel.Location = new System.Drawing.Point(0, 25);
            this.lblMOReel.Margin = new System.Windows.Forms.Padding(0);
            this.lblMOReel.Name = "lblMOReel";
            this.lblMOReel.Size = new System.Drawing.Size(275, 25);
            this.lblMOReel.TabIndex = 5;
            this.lblMOReel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Location = new System.Drawing.Point(1103, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(272, 25);
            this.label30.TabIndex = 4;
            this.label30.Text = "Ventes";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Location = new System.Drawing.Point(828, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(269, 25);
            this.label29.TabIndex = 3;
            this.label29.Text = "Achats";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Location = new System.Drawing.Point(553, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(269, 25);
            this.label28.TabIndex = 2;
            this.label28.Text = "SS competence";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Location = new System.Drawing.Point(278, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(269, 25);
            this.label27.TabIndex = 1;
            this.label27.Text = "SS capacite";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(269, 25);
            this.label26.TabIndex = 0;
            this.label26.Text = "Main d\'oeuvre";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Frame9
            // 
            this.Frame9.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Frame9.BackColor = System.Drawing.Color.Transparent;
            this.Frame9.Controls.Add(this.tableLayoutPanel18);
            this.Frame9.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Frame9.Location = new System.Drawing.Point(3, 30);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(1374, 46);
            this.Frame9.TabIndex = 413;
            this.Frame9.TabStop = false;
            this.Frame9.Text = "Facture Manuelle";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 5;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel18.Controls.Add(this.optIntervention, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.optDevis, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.OptTravMan, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.OptConMan, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.OptFioulMan, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1368, 23);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // optIntervention
            // 
            this.optIntervention.AutoSize = true;
            this.optIntervention.Checked = true;
            this.optIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optIntervention.Location = new System.Drawing.Point(819, 0);
            this.optIntervention.Margin = new System.Windows.Forms.Padding(0);
            this.optIntervention.Name = "optIntervention";
            this.optIntervention.Size = new System.Drawing.Size(273, 23);
            this.optIntervention.TabIndex = 542;
            this.optIntervention.TabStop = true;
            this.optIntervention.Text = "Intervention";
            this.optIntervention.UseVisualStyleBackColor = true;
            this.optIntervention.CheckedChanged += new System.EventHandler(this.optIntervention_CheckedChanged);
            this.optIntervention.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.optIntervention_KeyPress);
            // 
            // optDevis
            // 
            this.optDevis.AutoSize = true;
            this.optDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optDevis.Location = new System.Drawing.Point(1092, 0);
            this.optDevis.Margin = new System.Windows.Forms.Padding(0);
            this.optDevis.Name = "optDevis";
            this.optDevis.Size = new System.Drawing.Size(276, 23);
            this.optDevis.TabIndex = 541;
            this.optDevis.Text = "Devis";
            this.optDevis.UseVisualStyleBackColor = true;
            this.optDevis.CheckedChanged += new System.EventHandler(this.optDevis_CheckedChanged);
            // 
            // OptTravMan
            // 
            this.OptTravMan.AutoSize = true;
            this.OptTravMan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptTravMan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptTravMan.Location = new System.Drawing.Point(546, 0);
            this.OptTravMan.Margin = new System.Windows.Forms.Padding(0);
            this.OptTravMan.Name = "OptTravMan";
            this.OptTravMan.Size = new System.Drawing.Size(273, 23);
            this.OptTravMan.TabIndex = 540;
            this.OptTravMan.Text = "Travaux";
            this.OptTravMan.UseVisualStyleBackColor = true;
            this.OptTravMan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OptTravMan_KeyPress);
            // 
            // OptConMan
            // 
            this.OptConMan.AutoSize = true;
            this.OptConMan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptConMan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptConMan.Location = new System.Drawing.Point(0, 0);
            this.OptConMan.Margin = new System.Windows.Forms.Padding(0);
            this.OptConMan.Name = "OptConMan";
            this.OptConMan.Size = new System.Drawing.Size(273, 23);
            this.OptConMan.TabIndex = 539;
            this.OptConMan.Text = "Contrat";
            this.OptConMan.UseVisualStyleBackColor = true;
            this.OptConMan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OptConMan_KeyPress);
            // 
            // OptFioulMan
            // 
            this.OptFioulMan.AutoSize = true;
            this.OptFioulMan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptFioulMan.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.OptFioulMan.Location = new System.Drawing.Point(273, 0);
            this.OptFioulMan.Margin = new System.Windows.Forms.Padding(0);
            this.OptFioulMan.Name = "OptFioulMan";
            this.OptFioulMan.Size = new System.Drawing.Size(273, 23);
            this.OptFioulMan.TabIndex = 538;
            this.OptFioulMan.Text = "Fioul";
            this.OptFioulMan.UseVisualStyleBackColor = true;
            this.OptFioulMan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OptFioulMan_KeyPress);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // timer2
            // 
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // Label1_4
            // 
            this.Label1_4.AutoSize = true;
            this.Label1_4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label1_4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Underline);
            this.Label1_4.ForeColor = System.Drawing.Color.Blue;
            this.Label1_4.Location = new System.Drawing.Point(1371, 0);
            this.Label1_4.Name = "Label1_4";
            this.Label1_4.Size = new System.Drawing.Size(85, 18);
            this.Label1_4.TabIndex = 56;
            this.Label1_4.Tag = "4";
            this.Label1_4.Text = "Fiche Devis";
            this.Label1_4.Click += new System.EventHandler(this.Label1_1_Click);
            // 
            // lblLibService
            // 
            this.lblLibService.AccAcceptNumbersOnly = false;
            this.lblLibService.AccAllowComma = false;
            this.lblLibService.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibService.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibService.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibService.AccHidenValue = "";
            this.lblLibService.AccNotAllowedChars = null;
            this.lblLibService.AccReadOnly = false;
            this.lblLibService.AccReadOnlyAllowDelete = false;
            this.lblLibService.AccRequired = false;
            this.lblLibService.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.lblLibService, 4);
            this.lblLibService.CustomBackColor = System.Drawing.Color.White;
            this.lblLibService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibService.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblLibService.ForeColor = System.Drawing.Color.Black;
            this.lblLibService.Location = new System.Drawing.Point(566, 62);
            this.lblLibService.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibService.MaxLength = 32767;
            this.lblLibService.Multiline = false;
            this.lblLibService.Name = "lblLibService";
            this.lblLibService.ReadOnly = false;
            this.lblLibService.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibService.Size = new System.Drawing.Size(918, 27);
            this.lblLibService.TabIndex = 514;
            this.lblLibService.Tag = "BCD";
            this.lblLibService.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibService.UseSystemPasswordChar = false;
            // 
            // txtObjet1
            // 
            this.txtObjet1.AccAcceptNumbersOnly = false;
            this.txtObjet1.AccAllowComma = false;
            this.txtObjet1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObjet1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObjet1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObjet1.AccHidenValue = "";
            this.txtObjet1.AccNotAllowedChars = null;
            this.txtObjet1.AccReadOnly = false;
            this.txtObjet1.AccReadOnlyAllowDelete = false;
            this.txtObjet1.AccRequired = false;
            this.txtObjet1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtObjet1, 6);
            this.txtObjet1.CustomBackColor = System.Drawing.Color.White;
            this.txtObjet1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObjet1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtObjet1.ForeColor = System.Drawing.Color.Black;
            this.txtObjet1.Location = new System.Drawing.Point(143, 92);
            this.txtObjet1.Margin = new System.Windows.Forms.Padding(2);
            this.txtObjet1.MaxLength = 32767;
            this.txtObjet1.Multiline = false;
            this.txtObjet1.Name = "txtObjet1";
            this.txtObjet1.ReadOnly = false;
            this.txtObjet1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObjet1.Size = new System.Drawing.Size(1341, 27);
            this.txtObjet1.TabIndex = 579;
            this.txtObjet1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObjet1.UseSystemPasswordChar = false;
            // 
            // txtObjet2
            // 
            this.txtObjet2.AccAcceptNumbersOnly = false;
            this.txtObjet2.AccAllowComma = false;
            this.txtObjet2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObjet2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObjet2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObjet2.AccHidenValue = "";
            this.txtObjet2.AccNotAllowedChars = null;
            this.txtObjet2.AccReadOnly = false;
            this.txtObjet2.AccReadOnlyAllowDelete = false;
            this.txtObjet2.AccRequired = false;
            this.txtObjet2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtObjet2, 6);
            this.txtObjet2.CustomBackColor = System.Drawing.Color.White;
            this.txtObjet2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObjet2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtObjet2.ForeColor = System.Drawing.Color.Black;
            this.txtObjet2.Location = new System.Drawing.Point(143, 122);
            this.txtObjet2.Margin = new System.Windows.Forms.Padding(2);
            this.txtObjet2.MaxLength = 32767;
            this.txtObjet2.Multiline = false;
            this.txtObjet2.Name = "txtObjet2";
            this.txtObjet2.ReadOnly = false;
            this.txtObjet2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObjet2.Size = new System.Drawing.Size(1341, 27);
            this.txtObjet2.TabIndex = 580;
            this.txtObjet2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObjet2.UseSystemPasswordChar = false;
            // 
            // txtCommentaire1
            // 
            this.txtCommentaire1.AccAcceptNumbersOnly = false;
            this.txtCommentaire1.AccAllowComma = false;
            this.txtCommentaire1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCommentaire1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCommentaire1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaire1.AccHidenValue = "";
            this.txtCommentaire1.AccNotAllowedChars = null;
            this.txtCommentaire1.AccReadOnly = false;
            this.txtCommentaire1.AccReadOnlyAllowDelete = false;
            this.txtCommentaire1.AccRequired = false;
            this.txtCommentaire1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtCommentaire1, 6);
            this.txtCommentaire1.CustomBackColor = System.Drawing.Color.White;
            this.txtCommentaire1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaire1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCommentaire1.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaire1.Location = new System.Drawing.Point(143, 152);
            this.txtCommentaire1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCommentaire1.MaxLength = 32767;
            this.txtCommentaire1.Multiline = false;
            this.txtCommentaire1.Name = "txtCommentaire1";
            this.txtCommentaire1.ReadOnly = false;
            this.txtCommentaire1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCommentaire1.Size = new System.Drawing.Size(1341, 27);
            this.txtCommentaire1.TabIndex = 581;
            this.txtCommentaire1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCommentaire1.UseSystemPasswordChar = false;
            // 
            // txtCommentaire2
            // 
            this.txtCommentaire2.AccAcceptNumbersOnly = false;
            this.txtCommentaire2.AccAllowComma = false;
            this.txtCommentaire2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCommentaire2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCommentaire2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaire2.AccHidenValue = "";
            this.txtCommentaire2.AccNotAllowedChars = null;
            this.txtCommentaire2.AccReadOnly = false;
            this.txtCommentaire2.AccReadOnlyAllowDelete = false;
            this.txtCommentaire2.AccRequired = false;
            this.txtCommentaire2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtCommentaire2, 6);
            this.txtCommentaire2.CustomBackColor = System.Drawing.Color.White;
            this.txtCommentaire2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaire2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCommentaire2.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaire2.Location = new System.Drawing.Point(143, 182);
            this.txtCommentaire2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCommentaire2.MaxLength = 32767;
            this.txtCommentaire2.Multiline = false;
            this.txtCommentaire2.Name = "txtCommentaire2";
            this.txtCommentaire2.ReadOnly = false;
            this.txtCommentaire2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCommentaire2.Size = new System.Drawing.Size(1341, 27);
            this.txtCommentaire2.TabIndex = 582;
            this.txtCommentaire2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCommentaire2.UseSystemPasswordChar = false;
            // 
            // txtGerNom
            // 
            this.txtGerNom.AccAcceptNumbersOnly = false;
            this.txtGerNom.AccAllowComma = false;
            this.txtGerNom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerNom.AccHidenValue = "";
            this.txtGerNom.AccNotAllowedChars = null;
            this.txtGerNom.AccReadOnly = false;
            this.txtGerNom.AccReadOnlyAllowDelete = false;
            this.txtGerNom.AccRequired = false;
            this.txtGerNom.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtGerNom, 6);
            this.txtGerNom.CustomBackColor = System.Drawing.Color.White;
            this.txtGerNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerNom.ForeColor = System.Drawing.Color.Black;
            this.txtGerNom.Location = new System.Drawing.Point(143, 212);
            this.txtGerNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerNom.MaxLength = 32767;
            this.txtGerNom.Multiline = false;
            this.txtGerNom.Name = "txtGerNom";
            this.txtGerNom.ReadOnly = false;
            this.txtGerNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerNom.Size = new System.Drawing.Size(1341, 27);
            this.txtGerNom.TabIndex = 583;
            this.txtGerNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerNom.UseSystemPasswordChar = false;
            // 
            // txtGerAdresse1
            // 
            this.txtGerAdresse1.AccAcceptNumbersOnly = false;
            this.txtGerAdresse1.AccAllowComma = false;
            this.txtGerAdresse1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerAdresse1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerAdresse1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerAdresse1.AccHidenValue = "";
            this.txtGerAdresse1.AccNotAllowedChars = null;
            this.txtGerAdresse1.AccReadOnly = false;
            this.txtGerAdresse1.AccReadOnlyAllowDelete = false;
            this.txtGerAdresse1.AccRequired = false;
            this.txtGerAdresse1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtGerAdresse1, 6);
            this.txtGerAdresse1.CustomBackColor = System.Drawing.Color.White;
            this.txtGerAdresse1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerAdresse1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerAdresse1.ForeColor = System.Drawing.Color.Black;
            this.txtGerAdresse1.Location = new System.Drawing.Point(143, 242);
            this.txtGerAdresse1.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerAdresse1.MaxLength = 32767;
            this.txtGerAdresse1.Multiline = false;
            this.txtGerAdresse1.Name = "txtGerAdresse1";
            this.txtGerAdresse1.ReadOnly = false;
            this.txtGerAdresse1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerAdresse1.Size = new System.Drawing.Size(1341, 27);
            this.txtGerAdresse1.TabIndex = 584;
            this.txtGerAdresse1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerAdresse1.UseSystemPasswordChar = false;
            // 
            // txtGerCP
            // 
            this.txtGerCP.AccAcceptNumbersOnly = false;
            this.txtGerCP.AccAllowComma = false;
            this.txtGerCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerCP.AccHidenValue = "";
            this.txtGerCP.AccNotAllowedChars = null;
            this.txtGerCP.AccReadOnly = false;
            this.txtGerCP.AccReadOnlyAllowDelete = false;
            this.txtGerCP.AccRequired = false;
            this.txtGerCP.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtGerCP, 2);
            this.txtGerCP.CustomBackColor = System.Drawing.Color.White;
            this.txtGerCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerCP.ForeColor = System.Drawing.Color.Black;
            this.txtGerCP.Location = new System.Drawing.Point(143, 302);
            this.txtGerCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerCP.MaxLength = 32767;
            this.txtGerCP.Multiline = false;
            this.txtGerCP.Name = "txtGerCP";
            this.txtGerCP.ReadOnly = false;
            this.txtGerCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerCP.Size = new System.Drawing.Size(419, 27);
            this.txtGerCP.TabIndex = 586;
            this.txtGerCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerCP.UseSystemPasswordChar = false;
            // 
            // txtGerAdresse2
            // 
            this.txtGerAdresse2.AccAcceptNumbersOnly = false;
            this.txtGerAdresse2.AccAllowComma = false;
            this.txtGerAdresse2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerAdresse2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerAdresse2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerAdresse2.AccHidenValue = "";
            this.txtGerAdresse2.AccNotAllowedChars = null;
            this.txtGerAdresse2.AccReadOnly = false;
            this.txtGerAdresse2.AccReadOnlyAllowDelete = false;
            this.txtGerAdresse2.AccRequired = false;
            this.txtGerAdresse2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtGerAdresse2, 6);
            this.txtGerAdresse2.CustomBackColor = System.Drawing.Color.White;
            this.txtGerAdresse2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerAdresse2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerAdresse2.ForeColor = System.Drawing.Color.Black;
            this.txtGerAdresse2.Location = new System.Drawing.Point(143, 272);
            this.txtGerAdresse2.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerAdresse2.MaxLength = 32767;
            this.txtGerAdresse2.Multiline = false;
            this.txtGerAdresse2.Name = "txtGerAdresse2";
            this.txtGerAdresse2.ReadOnly = false;
            this.txtGerAdresse2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerAdresse2.Size = new System.Drawing.Size(1341, 27);
            this.txtGerAdresse2.TabIndex = 585;
            this.txtGerAdresse2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerAdresse2.UseSystemPasswordChar = false;
            // 
            // txtGerVille
            // 
            this.txtGerVille.AccAcceptNumbersOnly = false;
            this.txtGerVille.AccAllowComma = false;
            this.txtGerVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerVille.AccHidenValue = "";
            this.txtGerVille.AccNotAllowedChars = null;
            this.txtGerVille.AccReadOnly = false;
            this.txtGerVille.AccReadOnlyAllowDelete = false;
            this.txtGerVille.AccRequired = false;
            this.txtGerVille.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtGerVille, 4);
            this.txtGerVille.CustomBackColor = System.Drawing.Color.White;
            this.txtGerVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGerVille.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerVille.ForeColor = System.Drawing.Color.Black;
            this.txtGerVille.Location = new System.Drawing.Point(566, 302);
            this.txtGerVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerVille.MaxLength = 32767;
            this.txtGerVille.Multiline = false;
            this.txtGerVille.Name = "txtGerVille";
            this.txtGerVille.ReadOnly = false;
            this.txtGerVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerVille.Size = new System.Drawing.Size(918, 27);
            this.txtGerVille.TabIndex = 587;
            this.txtGerVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerVille.UseSystemPasswordChar = false;
            // 
            // txtTypeReglement
            // 
            this.txtTypeReglement.AccAcceptNumbersOnly = false;
            this.txtTypeReglement.AccAllowComma = false;
            this.txtTypeReglement.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTypeReglement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTypeReglement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTypeReglement.AccHidenValue = "";
            this.txtTypeReglement.AccNotAllowedChars = null;
            this.txtTypeReglement.AccReadOnly = false;
            this.txtTypeReglement.AccReadOnlyAllowDelete = false;
            this.txtTypeReglement.AccRequired = false;
            this.txtTypeReglement.BackColor = System.Drawing.Color.White;
            this.txtTypeReglement.CustomBackColor = System.Drawing.Color.White;
            this.txtTypeReglement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtTypeReglement.ForeColor = System.Drawing.Color.Black;
            this.txtTypeReglement.Location = new System.Drawing.Point(2, 242);
            this.txtTypeReglement.Margin = new System.Windows.Forms.Padding(2);
            this.txtTypeReglement.MaxLength = 32767;
            this.txtTypeReglement.Multiline = false;
            this.txtTypeReglement.Name = "txtTypeReglement";
            this.txtTypeReglement.ReadOnly = false;
            this.txtTypeReglement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTypeReglement.Size = new System.Drawing.Size(80, 27);
            this.txtTypeReglement.TabIndex = 524;
            this.txtTypeReglement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTypeReglement.UseSystemPasswordChar = false;
            this.txtTypeReglement.Visible = false;
            // 
            // txtImmAdresse1
            // 
            this.txtImmAdresse1.AccAcceptNumbersOnly = false;
            this.txtImmAdresse1.AccAllowComma = false;
            this.txtImmAdresse1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmAdresse1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmAdresse1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmAdresse1.AccHidenValue = "";
            this.txtImmAdresse1.AccNotAllowedChars = null;
            this.txtImmAdresse1.AccReadOnly = false;
            this.txtImmAdresse1.AccReadOnlyAllowDelete = false;
            this.txtImmAdresse1.AccRequired = false;
            this.txtImmAdresse1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtImmAdresse1, 6);
            this.txtImmAdresse1.CustomBackColor = System.Drawing.Color.White;
            this.txtImmAdresse1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmAdresse1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImmAdresse1.ForeColor = System.Drawing.Color.Black;
            this.txtImmAdresse1.Location = new System.Drawing.Point(143, 332);
            this.txtImmAdresse1.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmAdresse1.MaxLength = 32767;
            this.txtImmAdresse1.Multiline = false;
            this.txtImmAdresse1.Name = "txtImmAdresse1";
            this.txtImmAdresse1.ReadOnly = false;
            this.txtImmAdresse1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmAdresse1.Size = new System.Drawing.Size(1341, 27);
            this.txtImmAdresse1.TabIndex = 588;
            this.txtImmAdresse1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmAdresse1.UseSystemPasswordChar = false;
            // 
            // txtimmAdresse3
            // 
            this.txtimmAdresse3.AccAcceptNumbersOnly = false;
            this.txtimmAdresse3.AccAllowComma = false;
            this.txtimmAdresse3.AccBackgroundColor = System.Drawing.Color.White;
            this.txtimmAdresse3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtimmAdresse3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtimmAdresse3.AccHidenValue = "";
            this.txtimmAdresse3.AccNotAllowedChars = null;
            this.txtimmAdresse3.AccReadOnly = false;
            this.txtimmAdresse3.AccReadOnlyAllowDelete = false;
            this.txtimmAdresse3.AccRequired = false;
            this.txtimmAdresse3.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtimmAdresse3, 6);
            this.txtimmAdresse3.CustomBackColor = System.Drawing.Color.White;
            this.txtimmAdresse3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtimmAdresse3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtimmAdresse3.ForeColor = System.Drawing.Color.Black;
            this.txtimmAdresse3.Location = new System.Drawing.Point(143, 392);
            this.txtimmAdresse3.Margin = new System.Windows.Forms.Padding(2);
            this.txtimmAdresse3.MaxLength = 32767;
            this.txtimmAdresse3.Multiline = false;
            this.txtimmAdresse3.Name = "txtimmAdresse3";
            this.txtimmAdresse3.ReadOnly = false;
            this.txtimmAdresse3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtimmAdresse3.Size = new System.Drawing.Size(1341, 27);
            this.txtimmAdresse3.TabIndex = 590;
            this.txtimmAdresse3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtimmAdresse3.UseSystemPasswordChar = false;
            // 
            // txtImmAdresse2
            // 
            this.txtImmAdresse2.AccAcceptNumbersOnly = false;
            this.txtImmAdresse2.AccAllowComma = false;
            this.txtImmAdresse2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmAdresse2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmAdresse2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmAdresse2.AccHidenValue = "";
            this.txtImmAdresse2.AccNotAllowedChars = null;
            this.txtImmAdresse2.AccReadOnly = false;
            this.txtImmAdresse2.AccReadOnlyAllowDelete = false;
            this.txtImmAdresse2.AccRequired = false;
            this.txtImmAdresse2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtImmAdresse2, 6);
            this.txtImmAdresse2.CustomBackColor = System.Drawing.Color.White;
            this.txtImmAdresse2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmAdresse2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImmAdresse2.ForeColor = System.Drawing.Color.Black;
            this.txtImmAdresse2.Location = new System.Drawing.Point(143, 362);
            this.txtImmAdresse2.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmAdresse2.MaxLength = 32767;
            this.txtImmAdresse2.Multiline = false;
            this.txtImmAdresse2.Name = "txtImmAdresse2";
            this.txtImmAdresse2.ReadOnly = false;
            this.txtImmAdresse2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmAdresse2.Size = new System.Drawing.Size(1341, 27);
            this.txtImmAdresse2.TabIndex = 589;
            this.txtImmAdresse2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmAdresse2.UseSystemPasswordChar = false;
            // 
            // txtImmCP
            // 
            this.txtImmCP.AccAcceptNumbersOnly = false;
            this.txtImmCP.AccAllowComma = false;
            this.txtImmCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmCP.AccHidenValue = "";
            this.txtImmCP.AccNotAllowedChars = null;
            this.txtImmCP.AccReadOnly = false;
            this.txtImmCP.AccReadOnlyAllowDelete = false;
            this.txtImmCP.AccRequired = false;
            this.txtImmCP.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtImmCP, 2);
            this.txtImmCP.CustomBackColor = System.Drawing.Color.White;
            this.txtImmCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmCP.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImmCP.ForeColor = System.Drawing.Color.Black;
            this.txtImmCP.Location = new System.Drawing.Point(143, 422);
            this.txtImmCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmCP.MaxLength = 32767;
            this.txtImmCP.Multiline = false;
            this.txtImmCP.Name = "txtImmCP";
            this.txtImmCP.ReadOnly = false;
            this.txtImmCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmCP.Size = new System.Drawing.Size(419, 27);
            this.txtImmCP.TabIndex = 591;
            this.txtImmCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmCP.UseSystemPasswordChar = false;
            // 
            // txtMode
            // 
            this.txtMode.AccAcceptNumbersOnly = false;
            this.txtMode.AccAllowComma = false;
            this.txtMode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMode.AccHidenValue = "";
            this.txtMode.AccNotAllowedChars = null;
            this.txtMode.AccReadOnly = false;
            this.txtMode.AccReadOnlyAllowDelete = false;
            this.txtMode.AccRequired = false;
            this.txtMode.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtMode, 2);
            this.txtMode.CustomBackColor = System.Drawing.Color.White;
            this.txtMode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtMode.ForeColor = System.Drawing.Color.Black;
            this.txtMode.Location = new System.Drawing.Point(143, 452);
            this.txtMode.Margin = new System.Windows.Forms.Padding(2);
            this.txtMode.MaxLength = 32767;
            this.txtMode.Multiline = false;
            this.txtMode.Name = "txtMode";
            this.txtMode.ReadOnly = false;
            this.txtMode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMode.Size = new System.Drawing.Size(419, 27);
            this.txtMode.TabIndex = 593;
            this.txtMode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMode.UseSystemPasswordChar = false;
            this.txtMode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMode_KeyPress);
            // 
            // txtVIR_Code
            // 
            this.txtVIR_Code.AccAcceptNumbersOnly = false;
            this.txtVIR_Code.AccAllowComma = false;
            this.txtVIR_Code.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVIR_Code.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVIR_Code.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVIR_Code.AccHidenValue = "";
            this.txtVIR_Code.AccNotAllowedChars = null;
            this.txtVIR_Code.AccReadOnly = false;
            this.txtVIR_Code.AccReadOnlyAllowDelete = false;
            this.txtVIR_Code.AccRequired = false;
            this.txtVIR_Code.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtVIR_Code, 4);
            this.txtVIR_Code.CustomBackColor = System.Drawing.Color.White;
            this.txtVIR_Code.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVIR_Code.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtVIR_Code.ForeColor = System.Drawing.Color.Black;
            this.txtVIR_Code.Location = new System.Drawing.Point(143, 482);
            this.txtVIR_Code.Margin = new System.Windows.Forms.Padding(2);
            this.txtVIR_Code.MaxLength = 32767;
            this.txtVIR_Code.Multiline = false;
            this.txtVIR_Code.Name = "txtVIR_Code";
            this.txtVIR_Code.ReadOnly = false;
            this.txtVIR_Code.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVIR_Code.Size = new System.Drawing.Size(744, 27);
            this.txtVIR_Code.TabIndex = 595;
            this.txtVIR_Code.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVIR_Code.UseSystemPasswordChar = false;
            // 
            // txtNCompte
            // 
            this.txtNCompte.AccAcceptNumbersOnly = false;
            this.txtNCompte.AccAllowComma = false;
            this.txtNCompte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNCompte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNCompte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNCompte.AccHidenValue = "";
            this.txtNCompte.AccNotAllowedChars = null;
            this.txtNCompte.AccReadOnly = true;
            this.txtNCompte.AccReadOnlyAllowDelete = false;
            this.txtNCompte.AccRequired = false;
            this.txtNCompte.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtNCompte, 4);
            this.txtNCompte.CustomBackColor = System.Drawing.Color.White;
            this.txtNCompte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNCompte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtNCompte.ForeColor = System.Drawing.Color.Black;
            this.txtNCompte.Location = new System.Drawing.Point(143, 512);
            this.txtNCompte.Margin = new System.Windows.Forms.Padding(2);
            this.txtNCompte.MaxLength = 32767;
            this.txtNCompte.Multiline = false;
            this.txtNCompte.Name = "txtNCompte";
            this.txtNCompte.ReadOnly = false;
            this.txtNCompte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNCompte.Size = new System.Drawing.Size(744, 27);
            this.txtNCompte.TabIndex = 596;
            this.txtNCompte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNCompte.UseSystemPasswordChar = false;
            // 
            // txtDateEcheance
            // 
            this.txtDateEcheance.AccAcceptNumbersOnly = false;
            this.txtDateEcheance.AccAllowComma = false;
            this.txtDateEcheance.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateEcheance.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateEcheance.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateEcheance.AccHidenValue = "";
            this.txtDateEcheance.AccNotAllowedChars = null;
            this.txtDateEcheance.AccReadOnly = false;
            this.txtDateEcheance.AccReadOnlyAllowDelete = false;
            this.txtDateEcheance.AccRequired = false;
            this.txtDateEcheance.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtDateEcheance, 4);
            this.txtDateEcheance.CustomBackColor = System.Drawing.Color.White;
            this.txtDateEcheance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateEcheance.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtDateEcheance.ForeColor = System.Drawing.Color.Black;
            this.txtDateEcheance.Location = new System.Drawing.Point(143, 542);
            this.txtDateEcheance.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateEcheance.MaxLength = 32767;
            this.txtDateEcheance.Multiline = false;
            this.txtDateEcheance.Name = "txtDateEcheance";
            this.txtDateEcheance.ReadOnly = false;
            this.txtDateEcheance.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateEcheance.Size = new System.Drawing.Size(744, 27);
            this.txtDateEcheance.TabIndex = 597;
            this.txtDateEcheance.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateEcheance.UseSystemPasswordChar = false;
            // 
            // txtDateDeFacture
            // 
            this.txtDateDeFacture.AccAcceptNumbersOnly = false;
            this.txtDateDeFacture.AccAllowComma = false;
            this.txtDateDeFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateDeFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateDeFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateDeFacture.AccHidenValue = "";
            this.txtDateDeFacture.AccNotAllowedChars = null;
            this.txtDateDeFacture.AccReadOnly = false;
            this.txtDateDeFacture.AccReadOnlyAllowDelete = false;
            this.txtDateDeFacture.AccRequired = false;
            this.txtDateDeFacture.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtDateDeFacture, 2);
            this.txtDateDeFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtDateDeFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateDeFacture.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtDateDeFacture.ForeColor = System.Drawing.Color.Black;
            this.txtDateDeFacture.Location = new System.Drawing.Point(891, 32);
            this.txtDateDeFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateDeFacture.MaxLength = 32767;
            this.txtDateDeFacture.Multiline = false;
            this.txtDateDeFacture.Name = "txtDateDeFacture";
            this.txtDateDeFacture.ReadOnly = false;
            this.txtDateDeFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateDeFacture.Size = new System.Drawing.Size(593, 27);
            this.txtDateDeFacture.TabIndex = 538;
            this.txtDateDeFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateDeFacture.UseSystemPasswordChar = false;
            this.txtDateDeFacture.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDateDeFacture_KeyPress);
            this.txtDateDeFacture.Leave += new System.EventHandler(this.txtDateDeFacture_Leave);
            // 
            // txtTVA
            // 
            this.txtTVA.AccAcceptNumbersOnly = false;
            this.txtTVA.AccAllowComma = false;
            this.txtTVA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTVA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTVA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTVA.AccHidenValue = "";
            this.txtTVA.AccNotAllowedChars = null;
            this.txtTVA.AccReadOnly = false;
            this.txtTVA.AccReadOnlyAllowDelete = false;
            this.txtTVA.AccRequired = false;
            this.txtTVA.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtTVA, 2);
            this.txtTVA.CustomBackColor = System.Drawing.Color.White;
            this.txtTVA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTVA.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtTVA.ForeColor = System.Drawing.Color.Black;
            this.txtTVA.Location = new System.Drawing.Point(1594, 2);
            this.txtTVA.Margin = new System.Windows.Forms.Padding(2);
            this.txtTVA.MaxLength = 32767;
            this.txtTVA.Multiline = false;
            this.txtTVA.Name = "txtTVA";
            this.txtTVA.ReadOnly = false;
            this.txtTVA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTVA.Size = new System.Drawing.Size(246, 27);
            this.txtTVA.TabIndex = 540;
            this.txtTVA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTVA.UseSystemPasswordChar = false;
            this.txtTVA.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTVA_KeyPress);
            this.txtTVA.Leave += new System.EventHandler(this.txtTVA_Leave);
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtNoIntervention, 2);
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(1594, 92);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(246, 27);
            this.txtNoIntervention.TabIndex = 541;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // txtINT_AnaCode
            // 
            this.txtINT_AnaCode.AccAcceptNumbersOnly = false;
            this.txtINT_AnaCode.AccAllowComma = false;
            this.txtINT_AnaCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtINT_AnaCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtINT_AnaCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtINT_AnaCode.AccHidenValue = "";
            this.txtINT_AnaCode.AccNotAllowedChars = null;
            this.txtINT_AnaCode.AccReadOnly = false;
            this.txtINT_AnaCode.AccReadOnlyAllowDelete = false;
            this.txtINT_AnaCode.AccRequired = false;
            this.txtINT_AnaCode.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtINT_AnaCode, 2);
            this.txtINT_AnaCode.CustomBackColor = System.Drawing.Color.White;
            this.txtINT_AnaCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtINT_AnaCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtINT_AnaCode.ForeColor = System.Drawing.Color.Black;
            this.txtINT_AnaCode.Location = new System.Drawing.Point(1594, 122);
            this.txtINT_AnaCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtINT_AnaCode.MaxLength = 32767;
            this.txtINT_AnaCode.Multiline = false;
            this.txtINT_AnaCode.Name = "txtINT_AnaCode";
            this.txtINT_AnaCode.ReadOnly = false;
            this.txtINT_AnaCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtINT_AnaCode.Size = new System.Drawing.Size(246, 27);
            this.txtINT_AnaCode.TabIndex = 542;
            this.txtINT_AnaCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtINT_AnaCode.UseSystemPasswordChar = false;
            this.txtINT_AnaCode.Visible = false;
            // 
            // txtGerAdresse3
            // 
            this.txtGerAdresse3.AccAcceptNumbersOnly = false;
            this.txtGerAdresse3.AccAllowComma = false;
            this.txtGerAdresse3.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerAdresse3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerAdresse3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerAdresse3.AccHidenValue = "";
            this.txtGerAdresse3.AccNotAllowedChars = null;
            this.txtGerAdresse3.AccReadOnly = false;
            this.txtGerAdresse3.AccReadOnlyAllowDelete = false;
            this.txtGerAdresse3.AccRequired = false;
            this.txtGerAdresse3.BackColor = System.Drawing.Color.White;
            this.txtGerAdresse3.CustomBackColor = System.Drawing.Color.White;
            this.txtGerAdresse3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtGerAdresse3.ForeColor = System.Drawing.Color.Black;
            this.txtGerAdresse3.Location = new System.Drawing.Point(1628, 62);
            this.txtGerAdresse3.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerAdresse3.MaxLength = 32767;
            this.txtGerAdresse3.Multiline = false;
            this.txtGerAdresse3.Name = "txtGerAdresse3";
            this.txtGerAdresse3.ReadOnly = false;
            this.txtGerAdresse3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerAdresse3.Size = new System.Drawing.Size(52, 27);
            this.txtGerAdresse3.TabIndex = 543;
            this.txtGerAdresse3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerAdresse3.UseSystemPasswordChar = false;
            this.txtGerAdresse3.Visible = false;
            // 
            // txtImmVille
            // 
            this.txtImmVille.AccAcceptNumbersOnly = false;
            this.txtImmVille.AccAllowComma = false;
            this.txtImmVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmVille.AccHidenValue = "";
            this.txtImmVille.AccNotAllowedChars = null;
            this.txtImmVille.AccReadOnly = false;
            this.txtImmVille.AccReadOnlyAllowDelete = false;
            this.txtImmVille.AccRequired = false;
            this.txtImmVille.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtImmVille, 4);
            this.txtImmVille.CustomBackColor = System.Drawing.Color.White;
            this.txtImmVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmVille.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImmVille.ForeColor = System.Drawing.Color.Black;
            this.txtImmVille.Location = new System.Drawing.Point(566, 422);
            this.txtImmVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmVille.MaxLength = 32767;
            this.txtImmVille.Multiline = false;
            this.txtImmVille.Name = "txtImmVille";
            this.txtImmVille.ReadOnly = false;
            this.txtImmVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmVille.Size = new System.Drawing.Size(918, 27);
            this.txtImmVille.TabIndex = 592;
            this.txtImmVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmVille.UseSystemPasswordChar = false;
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(2, 2);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = false;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(600, 27);
            this.txtCodeimmeuble.TabIndex = 503;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            this.txtCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeimmeuble_KeyPress);
            // 
            // txtModeLibelle
            // 
            this.txtModeLibelle.AccAcceptNumbersOnly = false;
            this.txtModeLibelle.AccAllowComma = false;
            this.txtModeLibelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtModeLibelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtModeLibelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtModeLibelle.AccHidenValue = "";
            this.txtModeLibelle.AccNotAllowedChars = null;
            this.txtModeLibelle.AccReadOnly = false;
            this.txtModeLibelle.AccReadOnlyAllowDelete = false;
            this.txtModeLibelle.AccRequired = false;
            this.txtModeLibelle.BackColor = System.Drawing.Color.White;
            this.txtModeLibelle.CustomBackColor = System.Drawing.Color.White;
            this.txtModeLibelle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtModeLibelle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtModeLibelle.ForeColor = System.Drawing.Color.Black;
            this.txtModeLibelle.Location = new System.Drawing.Point(32, 2);
            this.txtModeLibelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtModeLibelle.MaxLength = 32767;
            this.txtModeLibelle.Multiline = false;
            this.txtModeLibelle.Name = "txtModeLibelle";
            this.txtModeLibelle.ReadOnly = false;
            this.txtModeLibelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtModeLibelle.Size = new System.Drawing.Size(888, 27);
            this.txtModeLibelle.TabIndex = 594;
            this.txtModeLibelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtModeLibelle.UseSystemPasswordChar = false;
            // 
            // txtRefClient
            // 
            this.txtRefClient.AccAcceptNumbersOnly = false;
            this.txtRefClient.AccAllowComma = false;
            this.txtRefClient.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefClient.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefClient.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRefClient.AccHidenValue = "";
            this.txtRefClient.AccNotAllowedChars = null;
            this.txtRefClient.AccReadOnly = false;
            this.txtRefClient.AccReadOnlyAllowDelete = false;
            this.txtRefClient.AccRequired = false;
            this.txtRefClient.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtRefClient, 4);
            this.txtRefClient.CustomBackColor = System.Drawing.Color.White;
            this.txtRefClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRefClient.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtRefClient.ForeColor = System.Drawing.Color.Black;
            this.txtRefClient.Location = new System.Drawing.Point(143, 572);
            this.txtRefClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefClient.MaxLength = 32767;
            this.txtRefClient.Multiline = false;
            this.txtRefClient.Name = "txtRefClient";
            this.txtRefClient.ReadOnly = false;
            this.txtRefClient.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefClient.Size = new System.Drawing.Size(744, 27);
            this.txtRefClient.TabIndex = 598;
            this.txtRefClient.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefClient.UseSystemPasswordChar = false;
            // 
            // txtVisu1
            // 
            this.txtVisu1.AccAcceptNumbersOnly = false;
            this.txtVisu1.AccAllowComma = false;
            this.txtVisu1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVisu1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVisu1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVisu1.AccHidenValue = "";
            this.txtVisu1.AccNotAllowedChars = null;
            this.txtVisu1.AccReadOnly = false;
            this.txtVisu1.AccReadOnlyAllowDelete = false;
            this.txtVisu1.AccRequired = false;
            this.txtVisu1.BackColor = System.Drawing.Color.White;
            this.txtVisu1.CustomBackColor = System.Drawing.Color.White;
            this.txtVisu1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVisu1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtVisu1.ForeColor = System.Drawing.Color.Black;
            this.txtVisu1.Location = new System.Drawing.Point(147, 2);
            this.txtVisu1.Margin = new System.Windows.Forms.Padding(2);
            this.txtVisu1.MaxLength = 32767;
            this.txtVisu1.Multiline = false;
            this.txtVisu1.Name = "txtVisu1";
            this.txtVisu1.ReadOnly = false;
            this.txtVisu1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVisu1.Size = new System.Drawing.Size(343, 27);
            this.txtVisu1.TabIndex = 510;
            this.txtVisu1.Tag = "Valide";
            this.txtVisu1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVisu1.UseSystemPasswordChar = false;
            this.txtVisu1.Enter += new System.EventHandler(this.txtVisu1_Enter);
            this.txtVisu1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVisu1_KeyPress);
            // 
            // txtVisuDate1
            // 
            this.txtVisuDate1.AccAcceptNumbersOnly = false;
            this.txtVisuDate1.AccAllowComma = false;
            this.txtVisuDate1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVisuDate1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVisuDate1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVisuDate1.AccHidenValue = "";
            this.txtVisuDate1.AccNotAllowedChars = null;
            this.txtVisuDate1.AccReadOnly = false;
            this.txtVisuDate1.AccReadOnlyAllowDelete = false;
            this.txtVisuDate1.AccRequired = false;
            this.txtVisuDate1.BackColor = System.Drawing.Color.White;
            this.txtVisuDate1.CustomBackColor = System.Drawing.Color.White;
            this.txtVisuDate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVisuDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtVisuDate1.ForeColor = System.Drawing.Color.Black;
            this.txtVisuDate1.Location = new System.Drawing.Point(147, 32);
            this.txtVisuDate1.Margin = new System.Windows.Forms.Padding(2);
            this.txtVisuDate1.MaxLength = 32767;
            this.txtVisuDate1.Multiline = false;
            this.txtVisuDate1.Name = "txtVisuDate1";
            this.txtVisuDate1.ReadOnly = false;
            this.txtVisuDate1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVisuDate1.Size = new System.Drawing.Size(343, 27);
            this.txtVisuDate1.TabIndex = 512;
            this.txtVisuDate1.Tag = "Valide";
            this.txtVisuDate1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVisuDate1.UseSystemPasswordChar = false;
            this.txtVisuDate1.Enter += new System.EventHandler(this.txtVisuDate1_Enter);
            this.txtVisuDate1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVisuDate1_KeyPress);
            this.txtVisuDate1.Leave += new System.EventHandler(this.txtVisuDate1_Leave);
            // 
            // txtVisu2
            // 
            this.txtVisu2.AccAcceptNumbersOnly = false;
            this.txtVisu2.AccAllowComma = false;
            this.txtVisu2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVisu2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVisu2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVisu2.AccHidenValue = "";
            this.txtVisu2.AccNotAllowedChars = null;
            this.txtVisu2.AccReadOnly = false;
            this.txtVisu2.AccReadOnlyAllowDelete = false;
            this.txtVisu2.AccRequired = false;
            this.txtVisu2.BackColor = System.Drawing.Color.White;
            this.txtVisu2.CustomBackColor = System.Drawing.Color.White;
            this.txtVisu2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVisu2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtVisu2.ForeColor = System.Drawing.Color.Black;
            this.txtVisu2.Location = new System.Drawing.Point(542, 2);
            this.txtVisu2.Margin = new System.Windows.Forms.Padding(2);
            this.txtVisu2.MaxLength = 32767;
            this.txtVisu2.Multiline = false;
            this.txtVisu2.Name = "txtVisu2";
            this.txtVisu2.ReadOnly = false;
            this.txtVisu2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVisu2.Size = new System.Drawing.Size(343, 27);
            this.txtVisu2.TabIndex = 511;
            this.txtVisu2.Tag = "Valide";
            this.txtVisu2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVisu2.UseSystemPasswordChar = false;
            this.txtVisu2.Enter += new System.EventHandler(this.txtVisu2_Enter);
            this.txtVisu2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVisu2_KeyPress);
            // 
            // txtVisuDate2
            // 
            this.txtVisuDate2.AccAcceptNumbersOnly = false;
            this.txtVisuDate2.AccAllowComma = false;
            this.txtVisuDate2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVisuDate2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVisuDate2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVisuDate2.AccHidenValue = "";
            this.txtVisuDate2.AccNotAllowedChars = null;
            this.txtVisuDate2.AccReadOnly = false;
            this.txtVisuDate2.AccReadOnlyAllowDelete = false;
            this.txtVisuDate2.AccRequired = false;
            this.txtVisuDate2.BackColor = System.Drawing.Color.White;
            this.txtVisuDate2.CustomBackColor = System.Drawing.Color.White;
            this.txtVisuDate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVisuDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtVisuDate2.ForeColor = System.Drawing.Color.Black;
            this.txtVisuDate2.Location = new System.Drawing.Point(542, 32);
            this.txtVisuDate2.Margin = new System.Windows.Forms.Padding(2);
            this.txtVisuDate2.MaxLength = 32767;
            this.txtVisuDate2.Multiline = false;
            this.txtVisuDate2.Name = "txtVisuDate2";
            this.txtVisuDate2.ReadOnly = false;
            this.txtVisuDate2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVisuDate2.Size = new System.Drawing.Size(343, 27);
            this.txtVisuDate2.TabIndex = 513;
            this.txtVisuDate2.Tag = "Valide";
            this.txtVisuDate2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVisuDate2.UseSystemPasswordChar = false;
            this.txtVisuDate2.Enter += new System.EventHandler(this.txtVisuDate2_Enter);
            this.txtVisuDate2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtVisuDate2_KeyPress);
            this.txtVisuDate2.Leave += new System.EventHandler(this.txtVisuDate2_Leave);
            // 
            // txtImpDate1
            // 
            this.txtImpDate1.AccAcceptNumbersOnly = false;
            this.txtImpDate1.AccAllowComma = false;
            this.txtImpDate1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImpDate1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImpDate1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImpDate1.AccHidenValue = "";
            this.txtImpDate1.AccNotAllowedChars = null;
            this.txtImpDate1.AccReadOnly = false;
            this.txtImpDate1.AccReadOnlyAllowDelete = false;
            this.txtImpDate1.AccRequired = false;
            this.txtImpDate1.BackColor = System.Drawing.Color.White;
            this.txtImpDate1.CustomBackColor = System.Drawing.Color.White;
            this.txtImpDate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImpDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImpDate1.ForeColor = System.Drawing.Color.Black;
            this.txtImpDate1.Location = new System.Drawing.Point(142, 32);
            this.txtImpDate1.Margin = new System.Windows.Forms.Padding(2);
            this.txtImpDate1.MaxLength = 32767;
            this.txtImpDate1.Multiline = false;
            this.txtImpDate1.Name = "txtImpDate1";
            this.txtImpDate1.ReadOnly = false;
            this.txtImpDate1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImpDate1.Size = new System.Drawing.Size(346, 27);
            this.txtImpDate1.TabIndex = 507;
            this.txtImpDate1.Tag = "Valide";
            this.txtImpDate1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImpDate1.UseSystemPasswordChar = false;
            this.txtImpDate1.Enter += new System.EventHandler(this.txtImpDate1_Enter);
            this.txtImpDate1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImpDate1_KeyPress);
            this.txtImpDate1.Leave += new System.EventHandler(this.txtImpDate1_Leave);
            // 
            // txtDebut
            // 
            this.txtDebut.AccAcceptNumbersOnly = false;
            this.txtDebut.AccAllowComma = false;
            this.txtDebut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDebut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDebut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDebut.AccHidenValue = "";
            this.txtDebut.AccNotAllowedChars = null;
            this.txtDebut.AccReadOnly = false;
            this.txtDebut.AccReadOnlyAllowDelete = false;
            this.txtDebut.AccRequired = false;
            this.txtDebut.BackColor = System.Drawing.Color.White;
            this.txtDebut.CustomBackColor = System.Drawing.Color.White;
            this.txtDebut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDebut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtDebut.ForeColor = System.Drawing.Color.Black;
            this.txtDebut.Location = new System.Drawing.Point(142, 2);
            this.txtDebut.Margin = new System.Windows.Forms.Padding(2);
            this.txtDebut.MaxLength = 32767;
            this.txtDebut.Multiline = false;
            this.txtDebut.Name = "txtDebut";
            this.txtDebut.ReadOnly = false;
            this.txtDebut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDebut.Size = new System.Drawing.Size(346, 27);
            this.txtDebut.TabIndex = 505;
            this.txtDebut.Tag = "Valide";
            this.txtDebut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDebut.UseSystemPasswordChar = false;
            this.txtDebut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDebut_KeyPress);
            // 
            // txtImpDate2
            // 
            this.txtImpDate2.AccAcceptNumbersOnly = false;
            this.txtImpDate2.AccAllowComma = false;
            this.txtImpDate2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImpDate2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImpDate2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImpDate2.AccHidenValue = "";
            this.txtImpDate2.AccNotAllowedChars = null;
            this.txtImpDate2.AccReadOnly = false;
            this.txtImpDate2.AccReadOnlyAllowDelete = false;
            this.txtImpDate2.AccRequired = false;
            this.txtImpDate2.BackColor = System.Drawing.Color.White;
            this.txtImpDate2.CustomBackColor = System.Drawing.Color.White;
            this.txtImpDate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImpDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImpDate2.ForeColor = System.Drawing.Color.Black;
            this.txtImpDate2.Location = new System.Drawing.Point(538, 32);
            this.txtImpDate2.Margin = new System.Windows.Forms.Padding(2);
            this.txtImpDate2.MaxLength = 32767;
            this.txtImpDate2.Multiline = false;
            this.txtImpDate2.Name = "txtImpDate2";
            this.txtImpDate2.ReadOnly = false;
            this.txtImpDate2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImpDate2.Size = new System.Drawing.Size(346, 27);
            this.txtImpDate2.TabIndex = 508;
            this.txtImpDate2.Tag = "Valide";
            this.txtImpDate2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImpDate2.UseSystemPasswordChar = false;
            this.txtImpDate2.Enter += new System.EventHandler(this.txtImpDate2_Enter);
            this.txtImpDate2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImpDate2_KeyPress);
            this.txtImpDate2.Leave += new System.EventHandler(this.txtImpDate2_Leave);
            // 
            // txtFin
            // 
            this.txtFin.AccAcceptNumbersOnly = false;
            this.txtFin.AccAllowComma = false;
            this.txtFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFin.AccHidenValue = "";
            this.txtFin.AccNotAllowedChars = null;
            this.txtFin.AccReadOnly = false;
            this.txtFin.AccReadOnlyAllowDelete = false;
            this.txtFin.AccRequired = false;
            this.txtFin.BackColor = System.Drawing.Color.White;
            this.txtFin.CustomBackColor = System.Drawing.Color.White;
            this.txtFin.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtFin.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtFin.ForeColor = System.Drawing.Color.Black;
            this.txtFin.Location = new System.Drawing.Point(538, 2);
            this.txtFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtFin.MaxLength = 32767;
            this.txtFin.Multiline = false;
            this.txtFin.Name = "txtFin";
            this.txtFin.ReadOnly = false;
            this.txtFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFin.Size = new System.Drawing.Size(346, 27);
            this.txtFin.TabIndex = 506;
            this.txtFin.Tag = "Valide";
            this.txtFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFin.UseSystemPasswordChar = false;
            this.txtFin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFin_KeyPress);
            // 
            // txtMiseAjour2
            // 
            this.txtMiseAjour2.AccAcceptNumbersOnly = false;
            this.txtMiseAjour2.AccAllowComma = false;
            this.txtMiseAjour2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMiseAjour2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMiseAjour2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMiseAjour2.AccHidenValue = "";
            this.txtMiseAjour2.AccNotAllowedChars = null;
            this.txtMiseAjour2.AccReadOnly = false;
            this.txtMiseAjour2.AccReadOnlyAllowDelete = false;
            this.txtMiseAjour2.AccRequired = false;
            this.txtMiseAjour2.BackColor = System.Drawing.Color.White;
            this.txtMiseAjour2.CustomBackColor = System.Drawing.Color.White;
            this.txtMiseAjour2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMiseAjour2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtMiseAjour2.ForeColor = System.Drawing.Color.Black;
            this.txtMiseAjour2.Location = new System.Drawing.Point(538, 2);
            this.txtMiseAjour2.Margin = new System.Windows.Forms.Padding(2);
            this.txtMiseAjour2.MaxLength = 32767;
            this.txtMiseAjour2.Multiline = false;
            this.txtMiseAjour2.Name = "txtMiseAjour2";
            this.txtMiseAjour2.ReadOnly = false;
            this.txtMiseAjour2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMiseAjour2.Size = new System.Drawing.Size(346, 27);
            this.txtMiseAjour2.TabIndex = 507;
            this.txtMiseAjour2.Tag = "Valide";
            this.txtMiseAjour2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMiseAjour2.UseSystemPasswordChar = false;
            this.txtMiseAjour2.Enter += new System.EventHandler(this.txtMiseAjour2_Enter);
            this.txtMiseAjour2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMiseAjour2_KeyPress);
            // 
            // txtMiseAjour1
            // 
            this.txtMiseAjour1.AccAcceptNumbersOnly = false;
            this.txtMiseAjour1.AccAllowComma = false;
            this.txtMiseAjour1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMiseAjour1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMiseAjour1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMiseAjour1.AccHidenValue = "";
            this.txtMiseAjour1.AccNotAllowedChars = null;
            this.txtMiseAjour1.AccReadOnly = false;
            this.txtMiseAjour1.AccReadOnlyAllowDelete = false;
            this.txtMiseAjour1.AccRequired = false;
            this.txtMiseAjour1.BackColor = System.Drawing.Color.White;
            this.txtMiseAjour1.CustomBackColor = System.Drawing.Color.White;
            this.txtMiseAjour1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMiseAjour1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtMiseAjour1.ForeColor = System.Drawing.Color.Black;
            this.txtMiseAjour1.Location = new System.Drawing.Point(142, 2);
            this.txtMiseAjour1.Margin = new System.Windows.Forms.Padding(2);
            this.txtMiseAjour1.MaxLength = 32767;
            this.txtMiseAjour1.Multiline = false;
            this.txtMiseAjour1.Name = "txtMiseAjour1";
            this.txtMiseAjour1.ReadOnly = false;
            this.txtMiseAjour1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMiseAjour1.Size = new System.Drawing.Size(346, 27);
            this.txtMiseAjour1.TabIndex = 506;
            this.txtMiseAjour1.Tag = "Valide";
            this.txtMiseAjour1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMiseAjour1.UseSystemPasswordChar = false;
            this.txtMiseAjour1.Enter += new System.EventHandler(this.txtMiseAjour1_Enter);
            this.txtMiseAjour1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMiseAjour1_KeyPress);
            // 
            // txtMiseDate1
            // 
            this.txtMiseDate1.AccAcceptNumbersOnly = false;
            this.txtMiseDate1.AccAllowComma = false;
            this.txtMiseDate1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMiseDate1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMiseDate1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMiseDate1.AccHidenValue = "";
            this.txtMiseDate1.AccNotAllowedChars = null;
            this.txtMiseDate1.AccReadOnly = false;
            this.txtMiseDate1.AccReadOnlyAllowDelete = false;
            this.txtMiseDate1.AccRequired = false;
            this.txtMiseDate1.BackColor = System.Drawing.Color.White;
            this.txtMiseDate1.CustomBackColor = System.Drawing.Color.White;
            this.txtMiseDate1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMiseDate1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtMiseDate1.ForeColor = System.Drawing.Color.Black;
            this.txtMiseDate1.Location = new System.Drawing.Point(142, 32);
            this.txtMiseDate1.Margin = new System.Windows.Forms.Padding(2);
            this.txtMiseDate1.MaxLength = 32767;
            this.txtMiseDate1.Multiline = false;
            this.txtMiseDate1.Name = "txtMiseDate1";
            this.txtMiseDate1.ReadOnly = false;
            this.txtMiseDate1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMiseDate1.Size = new System.Drawing.Size(346, 27);
            this.txtMiseDate1.TabIndex = 508;
            this.txtMiseDate1.Tag = "Valide";
            this.txtMiseDate1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMiseDate1.UseSystemPasswordChar = false;
            this.txtMiseDate1.Enter += new System.EventHandler(this.txtMiseDate1_Enter);
            this.txtMiseDate1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMiseDate1_KeyPress);
            this.txtMiseDate1.Leave += new System.EventHandler(this.txtMiseDate1_Leave);
            // 
            // txtMiseDate2
            // 
            this.txtMiseDate2.AccAcceptNumbersOnly = false;
            this.txtMiseDate2.AccAllowComma = false;
            this.txtMiseDate2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMiseDate2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMiseDate2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMiseDate2.AccHidenValue = "";
            this.txtMiseDate2.AccNotAllowedChars = null;
            this.txtMiseDate2.AccReadOnly = false;
            this.txtMiseDate2.AccReadOnlyAllowDelete = false;
            this.txtMiseDate2.AccRequired = false;
            this.txtMiseDate2.BackColor = System.Drawing.Color.White;
            this.txtMiseDate2.CustomBackColor = System.Drawing.Color.White;
            this.txtMiseDate2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMiseDate2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtMiseDate2.ForeColor = System.Drawing.Color.Black;
            this.txtMiseDate2.Location = new System.Drawing.Point(538, 32);
            this.txtMiseDate2.Margin = new System.Windows.Forms.Padding(2);
            this.txtMiseDate2.MaxLength = 32767;
            this.txtMiseDate2.Multiline = false;
            this.txtMiseDate2.Name = "txtMiseDate2";
            this.txtMiseDate2.ReadOnly = false;
            this.txtMiseDate2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMiseDate2.Size = new System.Drawing.Size(346, 27);
            this.txtMiseDate2.TabIndex = 509;
            this.txtMiseDate2.Tag = "Valide";
            this.txtMiseDate2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMiseDate2.UseSystemPasswordChar = false;
            this.txtMiseDate2.Enter += new System.EventHandler(this.txtMiseDate2_Enter);
            this.txtMiseDate2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMiseDate2_KeyPress);
            this.txtMiseDate2.Leave += new System.EventHandler(this.txtMiseDate2_Leave);
            // 
            // txtEnCours
            // 
            this.txtEnCours.AccAcceptNumbersOnly = false;
            this.txtEnCours.AccAllowComma = false;
            this.txtEnCours.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEnCours.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEnCours.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEnCours.AccHidenValue = "";
            this.txtEnCours.AccNotAllowedChars = null;
            this.txtEnCours.AccReadOnly = false;
            this.txtEnCours.AccReadOnlyAllowDelete = false;
            this.txtEnCours.AccRequired = false;
            this.txtEnCours.BackColor = System.Drawing.Color.White;
            this.txtEnCours.CustomBackColor = System.Drawing.Color.White;
            this.txtEnCours.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEnCours.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtEnCours.ForeColor = System.Drawing.Color.Black;
            this.txtEnCours.Location = new System.Drawing.Point(164, 2);
            this.txtEnCours.Margin = new System.Windows.Forms.Padding(2);
            this.txtEnCours.MaxLength = 32767;
            this.txtEnCours.Multiline = false;
            this.txtEnCours.Name = "txtEnCours";
            this.txtEnCours.ReadOnly = false;
            this.txtEnCours.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEnCours.Size = new System.Drawing.Size(1080, 27);
            this.txtEnCours.TabIndex = 510;
            this.txtEnCours.Tag = "Fermer";
            this.txtEnCours.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEnCours.UseSystemPasswordChar = false;
            // 
            // txtImmeuble
            // 
            this.txtImmeuble.AccAcceptNumbersOnly = false;
            this.txtImmeuble.AccAllowComma = false;
            this.txtImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmeuble.AccHidenValue = "";
            this.txtImmeuble.AccNotAllowedChars = null;
            this.txtImmeuble.AccReadOnly = false;
            this.txtImmeuble.AccReadOnlyAllowDelete = false;
            this.txtImmeuble.AccRequired = false;
            this.txtImmeuble.BackColor = System.Drawing.Color.White;
            this.txtImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmeuble.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtImmeuble.Location = new System.Drawing.Point(936, 22);
            this.txtImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmeuble.MaxLength = 32767;
            this.txtImmeuble.Multiline = false;
            this.txtImmeuble.Name = "txtImmeuble";
            this.txtImmeuble.ReadOnly = false;
            this.txtImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmeuble.Size = new System.Drawing.Size(273, 27);
            this.txtImmeuble.TabIndex = 502;
            this.txtImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmeuble.UseSystemPasswordChar = false;
            this.txtImmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImmeuble_KeyPress);
            // 
            // txtNoDevis
            // 
            this.txtNoDevis.AccAcceptNumbersOnly = false;
            this.txtNoDevis.AccAllowComma = false;
            this.txtNoDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoDevis.AccHidenValue = "";
            this.txtNoDevis.AccNotAllowedChars = null;
            this.txtNoDevis.AccReadOnly = false;
            this.txtNoDevis.AccReadOnlyAllowDelete = false;
            this.txtNoDevis.AccRequired = false;
            this.txtNoDevis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNoDevis.BackColor = System.Drawing.Color.White;
            this.txtNoDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNoDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtNoDevis.ForeColor = System.Drawing.Color.Black;
            this.txtNoDevis.Location = new System.Drawing.Point(118, 2);
            this.txtNoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoDevis.MaxLength = 32767;
            this.txtNoDevis.Multiline = false;
            this.txtNoDevis.Name = "txtNoDevis";
            this.txtNoDevis.ReadOnly = true;
            this.txtNoDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoDevis.Size = new System.Drawing.Size(555, 27);
            this.txtNoDevis.TabIndex = 585;
            this.txtNoDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoDevis.UseSystemPasswordChar = false;
            this.txtNoDevis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoDevis_KeyPress);
            // 
            // txtSituation
            // 
            this.txtSituation.AccAcceptNumbersOnly = false;
            this.txtSituation.AccAllowComma = false;
            this.txtSituation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSituation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSituation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSituation.AccHidenValue = "";
            this.txtSituation.AccNotAllowedChars = null;
            this.txtSituation.AccReadOnly = false;
            this.txtSituation.AccReadOnlyAllowDelete = false;
            this.txtSituation.AccRequired = false;
            this.txtSituation.BackColor = System.Drawing.Color.White;
            this.txtSituation.CustomBackColor = System.Drawing.Color.White;
            this.txtSituation.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtSituation.ForeColor = System.Drawing.Color.Black;
            this.txtSituation.Location = new System.Drawing.Point(1346, 2);
            this.txtSituation.Margin = new System.Windows.Forms.Padding(2);
            this.txtSituation.MaxLength = 32767;
            this.txtSituation.Multiline = false;
            this.txtSituation.Name = "txtSituation";
            this.txtSituation.ReadOnly = false;
            this.txtSituation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSituation.Size = new System.Drawing.Size(19, 27);
            this.txtSituation.TabIndex = 502;
            this.txtSituation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSituation.UseSystemPasswordChar = false;
            this.txtSituation.Visible = false;
            // 
            // txtCharges
            // 
            this.txtCharges.AccAcceptNumbersOnly = false;
            this.txtCharges.AccAllowComma = false;
            this.txtCharges.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCharges.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCharges.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCharges.AccHidenValue = "";
            this.txtCharges.AccNotAllowedChars = null;
            this.txtCharges.AccReadOnly = false;
            this.txtCharges.AccReadOnlyAllowDelete = false;
            this.txtCharges.AccRequired = false;
            this.txtCharges.BackColor = System.Drawing.Color.White;
            this.txtCharges.CustomBackColor = System.Drawing.Color.White;
            this.txtCharges.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCharges.ForeColor = System.Drawing.Color.Black;
            this.txtCharges.Location = new System.Drawing.Point(1346, 32);
            this.txtCharges.Margin = new System.Windows.Forms.Padding(2);
            this.txtCharges.MaxLength = 32767;
            this.txtCharges.Multiline = false;
            this.txtCharges.Name = "txtCharges";
            this.txtCharges.ReadOnly = false;
            this.txtCharges.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCharges.Size = new System.Drawing.Size(19, 27);
            this.txtCharges.TabIndex = 503;
            this.txtCharges.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCharges.UseSystemPasswordChar = false;
            this.txtCharges.Visible = false;
            // 
            // UserDocFacManuel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel2);
            this.Name = "UserDocFacManuel";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Facture des Travaux";
            this.Load += new System.EventHandler(this.UserDocFacManuel_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDocFacManuel_VisibleChanged);
            this.Validating += new System.ComponentModel.CancelEventHandler(this.UserDocFacManuel_Validating);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbService)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.Frame8.ResumeLayout(false);
            this.Frame8.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridInter)).EndInit();
            this.Frame3.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid2)).EndInit();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.frame6.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Combo1)).EndInit();
            this.Frame4.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.Frame5.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.ultraTabPageControl5.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid3)).EndInit();
            this.PicDevis.ResumeLayout(false);
            this.PicDevis.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.ultraTabPageControl6.ResumeLayout(false);
            this.groupBox9.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridContrat)).EndInit();
            this.ultraTabPageControl7.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.frmSynthese.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.fraSynthese.ResumeLayout(false);
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.Frame9.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdMenu;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdValider;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdSupprimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.GroupBox frmSynthese;
        public System.Windows.Forms.Label Label243;
        public System.Windows.Forms.Label Label242;
        public System.Windows.Forms.Label Label241;
        public System.Windows.Forms.Label Label240;
        public iTalk.iTalk_TextBox_Small2 txtSituation;
        public iTalk.iTalk_TextBox_Small2 txtCharges;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.RadioButton optParticulier;
        public System.Windows.Forms.RadioButton optGerant;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label lblDateEcheance;
        public iTalk.iTalk_TextBox_Small2 lblLibService;
        public iTalk.iTalk_TextBox_Small2 txtObjet1;
        public iTalk.iTalk_TextBox_Small2 txtObjet2;
        public iTalk.iTalk_TextBox_Small2 txtCommentaire1;
        public iTalk.iTalk_TextBox_Small2 txtCommentaire2;
        public iTalk.iTalk_TextBox_Small2 txtGerNom;
        public iTalk.iTalk_TextBox_Small2 txtGerAdresse1;
        public iTalk.iTalk_TextBox_Small2 txtGerCP;
        public iTalk.iTalk_TextBox_Small2 txtGerAdresse2;
        public iTalk.iTalk_TextBox_Small2 txtGerVille;
        public iTalk.iTalk_TextBox_Small2 txtTypeReglement;
        public iTalk.iTalk_TextBox_Small2 txtImmAdresse1;
        public iTalk.iTalk_TextBox_Small2 txtimmAdresse3;
        public iTalk.iTalk_TextBox_Small2 txtImmAdresse2;
        public iTalk.iTalk_TextBox_Small2 txtImmCP;
        public iTalk.iTalk_TextBox_Small2 txtMode;
        public iTalk.iTalk_TextBox_Small2 txtVIR_Code;
        public iTalk.iTalk_TextBox_Small2 txtNCompte;
        public iTalk.iTalk_TextBox_Small2 txtDateEcheance;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbService;
        public System.Windows.Forms.Label label10;
        public System.Windows.Forms.Label label11;
        public System.Windows.Forms.Label label12;
        public System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtDateDeFacture;
        private System.Windows.Forms.Panel Frame1;
        public iTalk.iTalk_TextBox_Small2 txtTVA;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public iTalk.iTalk_TextBox_Small2 txtINT_AnaCode;
        public iTalk.iTalk_TextBox_Small2 txtGerAdresse3;
        public System.Windows.Forms.CheckBox Check1;
        private System.Windows.Forms.Panel Frame8;
        public System.Windows.Forms.CheckBox chkFTM_Compta;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtImmVille;
        public System.Windows.Forms.Button cmdRechercheReg;
        public iTalk.iTalk_TextBox_Small2 txtModeLibelle;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Button cmdAction;
        public System.Windows.Forms.Button cmdfactScann;
        public System.Windows.Forms.Button cmdArticleGecet;
        public System.Windows.Forms.Button cdmDevisPDA;
        public System.Windows.Forms.Label lblP3;
        public System.Windows.Forms.Label lblChaudCondensation;
        public System.Windows.Forms.CheckBox chkDefinitive;
        public System.Windows.Forms.GroupBox groupBox2;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown4;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown3;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown1;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown2;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        public System.Windows.Forms.GroupBox Frame3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.Button cmdActualiser;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public System.Windows.Forms.GroupBox frame6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public System.Windows.Forms.GroupBox Frame5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        public System.Windows.Forms.GroupBox Frame4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public System.Windows.Forms.GroupBox groupBox4;
        public System.Windows.Forms.Label label19;
        public iTalk.iTalk_TextBox_Small2 txtVisu1;
        public iTalk.iTalk_TextBox_Small2 txtVisuDate1;
        public iTalk.iTalk_TextBox_Small2 txtVisu2;
        public iTalk.iTalk_TextBox_Small2 txtVisuDate2;
        public System.Windows.Forms.Label lblFin;
        public iTalk.iTalk_TextBox_Small2 txtEnCours;
        public System.Windows.Forms.Label label15;
        public System.Windows.Forms.Label label14;
        public iTalk.iTalk_TextBox_Small2 txtMiseAjour2;
        public iTalk.iTalk_TextBox_Small2 txtMiseAjour1;
        public iTalk.iTalk_TextBox_Small2 txtMiseDate1;
        public iTalk.iTalk_TextBox_Small2 txtMiseDate2;
        public System.Windows.Forms.Label label16;
        public iTalk.iTalk_TextBox_Small2 txtImpDate1;
        public iTalk.iTalk_TextBox_Small2 txtDebut;
        public iTalk.iTalk_TextBox_Small2 txtImpDate2;
        public iTalk.iTalk_TextBox_Small2 txtFin;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Button cmdVisu;
        public System.Windows.Forms.Button cmdExport;
        public System.Windows.Forms.Button CmdMiseAjour;
        public System.Windows.Forms.Button cmdAnalytique;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button cmdImprimer;
        public System.Windows.Forms.RadioButton Option1;
        public System.Windows.Forms.RadioButton Option4;
        public System.Windows.Forms.RadioButton Option7;
        public System.Windows.Forms.RadioButton Option2;
        public System.Windows.Forms.RadioButton Option8;
        public System.Windows.Forms.RadioButton Option5;
        public System.Windows.Forms.RadioButton Option6;
        public System.Windows.Forms.RadioButton Option3;
        public System.Windows.Forms.RadioButton optPersonne;
        public System.Windows.Forms.RadioButton OptNomChoisis;
        public System.Windows.Forms.RadioButton OptTous;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label lblNom;
        public System.Windows.Forms.Label lblDebut;
        public Infragistics.Win.UltraWinGrid.UltraCombo Combo1;
        public System.Windows.Forms.Button cmdSyntheseMAJ;
        public System.Windows.Forms.CheckBox chk3bacsVisu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        public System.Windows.Forms.Label label22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        public System.Windows.Forms.Label label23;
        public iTalk.iTalk_TextBox_Small2 txtImmeuble;
        public System.Windows.Forms.Button cmdRechImmeuble;
        public System.Windows.Forms.RadioButton optDevis1;
        public System.Windows.Forms.RadioButton optAf;
        public System.Windows.Forms.CheckBox chkSolderDevis;
        public System.Windows.Forms.Button CmdFax;
        public System.Windows.Forms.Button cmdAchat;
        public System.Windows.Forms.Button cmdInserer;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridContrat;
        public System.Windows.Forms.GroupBox groupBox9;
        public System.Windows.Forms.Button cmdHistorique;
        public System.Windows.Forms.GroupBox Frame9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        public System.Windows.Forms.RadioButton optIntervention;
        public System.Windows.Forms.RadioButton optDevis;
        public System.Windows.Forms.RadioButton OptTravMan;
        public System.Windows.Forms.RadioButton OptConMan;
        public System.Windows.Forms.RadioButton OptFioulMan;
        public System.Windows.Forms.Button cmdValidOpt;
        public System.Windows.Forms.CheckBox chk3bacs;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer timer2;
        public System.Windows.Forms.Label label24;
        public iTalk.iTalk_TextBox_Small2 txtRefClient;
        private System.Windows.Forms.Label lblCodeImmeuble;
        private System.Windows.Forms.Label lblTotalAchats;
        private System.Windows.Forms.Label lblTotalHeures;
        private System.Windows.Forms.Label lblTotalSituations;
        private System.Windows.Forms.Label lblMontantDevis;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Button cmdAvoir;
        private System.Windows.Forms.RadioButton optSelect;
        private System.Windows.Forms.RadioButton optDeselect;
        public System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtNoDevis;
        public System.Windows.Forms.Button cmdMial;
        public System.Windows.Forms.Button cmdDevis;
        public System.Windows.Forms.RadioButton OptFactEnCours;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.CheckBox chkDuplicata;
        private System.Windows.Forms.CheckBox chkOS;
        private System.Windows.Forms.TableLayoutPanel PicDevis;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridInter;
        private System.Windows.Forms.CheckBox chkBonPourAccord;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.Label Label1_3;
        private System.Windows.Forms.Label Label1_2;
        private System.Windows.Forms.Label Label1_1;
        public System.Windows.Forms.GroupBox fraSynthese;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.Label lblCoefficientReel;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label lblhtDevis;
        private System.Windows.Forms.Label lbl6;
        private System.Windows.Forms.Label lblVentesReelles;
        private System.Windows.Forms.Label lblChargesReelles;
        private System.Windows.Forms.Label lblAchatsReels;
        private System.Windows.Forms.Label lblSScompetence;
        private System.Windows.Forms.Label lblSScapacite;
        private System.Windows.Forms.Label lblMOReel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtVenteMem;
        private System.Windows.Forms.Label lblSstReel;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        public System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox txt_9;
        private System.Windows.Forms.TextBox txt_6;
        private System.Windows.Forms.TextBox txt_8;
        private System.Windows.Forms.TextBox txt_7;
        private System.Windows.Forms.TextBox txt_5;
        private System.Windows.Forms.TextBox txt_4;
        private System.Windows.Forms.TextBox txt_3;
        private System.Windows.Forms.TextBox txt_2;
        private System.Windows.Forms.TextBox txt_1;
        private System.Windows.Forms.TextBox txt_0;
        private System.Windows.Forms.Label lblMail;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label Label1_4;
    }
}
