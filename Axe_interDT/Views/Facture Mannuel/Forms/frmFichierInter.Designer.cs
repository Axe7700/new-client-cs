﻿namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    partial class frmFichierInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Frame1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optOriSiteWeb = new System.Windows.Forms.RadioButton();
            this.txtDocument = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeOrigine1 = new iTalk.iTalk_TextBox_Small2();
            this.lblMail = new System.Windows.Forms.Label();
            this.lblCourier = new System.Windows.Forms.Label();
            this.lblFax = new System.Windows.Forms.Label();
            this.txtCheminOS = new iTalk.iTalk_TextBox_Small2();
            this.txtDateOs = new iTalk.iTalk_TextBox_Small2();
            this.lblFaxouCourier = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRefOS = new iTalk.iTalk_TextBox_Small2();
            this.Label19 = new System.Windows.Forms.Label();
            this.optOriCourrier = new System.Windows.Forms.RadioButton();
            this.OptOriAstreinte = new System.Windows.Forms.RadioButton();
            this.OptOSfichier = new System.Windows.Forms.RadioButton();
            this.optOriMail = new System.Windows.Forms.RadioButton();
            this.optOriFax = new System.Windows.Forms.RadioButton();
            this.OptOSori = new System.Windows.Forms.RadioButton();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.optOriDirect = new System.Windows.Forms.RadioButton();
            this.optOriRepondeur = new System.Windows.Forms.RadioButton();
            this.optOriRadio = new System.Windows.Forms.RadioButton();
            this.optOriTelephone = new System.Windows.Forms.RadioButton();
            this.GridDoc = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Label138 = new Infragistics.Win.Misc.UltraLabel();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtNumficheStandard = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FileIntervention = new System.Windows.Forms.ListBox();
            this.DirNoInter = new System.Windows.Forms.TreeView();
            this.Label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoc)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "N° Fiche d\'appel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(16, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(96, 17);
            this.label2.TabIndex = 0;
            this.label2.Text = "N°Intervention";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(16, 72);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 17);
            this.label3.TabIndex = 0;
            this.label3.Text = "Code immeuble";
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.optOriSiteWeb);
            this.Frame1.Controls.Add(this.txtDocument);
            this.Frame1.Controls.Add(this.txtCodeOrigine1);
            this.Frame1.Controls.Add(this.lblMail);
            this.Frame1.Controls.Add(this.lblCourier);
            this.Frame1.Controls.Add(this.lblFax);
            this.Frame1.Controls.Add(this.txtCheminOS);
            this.Frame1.Controls.Add(this.txtDateOs);
            this.Frame1.Controls.Add(this.lblFaxouCourier);
            this.Frame1.Controls.Add(this.label6);
            this.Frame1.Controls.Add(this.label5);
            this.Frame1.Controls.Add(this.txtRefOS);
            this.Frame1.Controls.Add(this.Label19);
            this.Frame1.Controls.Add(this.optOriCourrier);
            this.Frame1.Controls.Add(this.OptOriAstreinte);
            this.Frame1.Controls.Add(this.OptOSfichier);
            this.Frame1.Controls.Add(this.optOriMail);
            this.Frame1.Controls.Add(this.optOriFax);
            this.Frame1.Controls.Add(this.OptOSori);
            this.Frame1.Controls.Add(this.Option1);
            this.Frame1.Controls.Add(this.optOriDirect);
            this.Frame1.Controls.Add(this.optOriRepondeur);
            this.Frame1.Controls.Add(this.optOriRadio);
            this.Frame1.Controls.Add(this.optOriTelephone);
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame1.Location = new System.Drawing.Point(10, 100);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(663, 222);
            this.Frame1.TabIndex = 503;
            // 
            // optOriSiteWeb
            // 
            this.optOriSiteWeb.AutoSize = true;
            this.optOriSiteWeb.Location = new System.Drawing.Point(250, 112);
            this.optOriSiteWeb.Name = "optOriSiteWeb";
            this.optOriSiteWeb.Size = new System.Drawing.Size(80, 21);
            this.optOriSiteWeb.TabIndex = 0;
            this.optOriSiteWeb.Text = "Site Web";
            this.optOriSiteWeb.UseVisualStyleBackColor = true;
            this.optOriSiteWeb.CheckedChanged += new System.EventHandler(this.optOriSiteWeb_CheckedChanged);
            // 
            // txtDocument
            // 
            this.txtDocument.AccAcceptNumbersOnly = false;
            this.txtDocument.AccAllowComma = false;
            this.txtDocument.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDocument.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDocument.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDocument.AccHidenValue = "";
            this.txtDocument.AccNotAllowedChars = null;
            this.txtDocument.AccReadOnly = false;
            this.txtDocument.AccReadOnlyAllowDelete = false;
            this.txtDocument.AccRequired = false;
            this.txtDocument.BackColor = System.Drawing.Color.White;
            this.txtDocument.CustomBackColor = System.Drawing.Color.White;
            this.txtDocument.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDocument.ForeColor = System.Drawing.Color.Black;
            this.txtDocument.Location = new System.Drawing.Point(612, 163);
            this.txtDocument.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocument.MaxLength = 32767;
            this.txtDocument.Multiline = false;
            this.txtDocument.Name = "txtDocument";
            this.txtDocument.ReadOnly = true;
            this.txtDocument.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDocument.Size = new System.Drawing.Size(39, 27);
            this.txtDocument.TabIndex = 502;
            this.txtDocument.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDocument.UseSystemPasswordChar = false;
            this.txtDocument.Visible = false;
            // 
            // txtCodeOrigine1
            // 
            this.txtCodeOrigine1.AccAcceptNumbersOnly = false;
            this.txtCodeOrigine1.AccAllowComma = false;
            this.txtCodeOrigine1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOrigine1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeOrigine1.AccHidenValue = "";
            this.txtCodeOrigine1.AccNotAllowedChars = null;
            this.txtCodeOrigine1.AccReadOnly = false;
            this.txtCodeOrigine1.AccReadOnlyAllowDelete = false;
            this.txtCodeOrigine1.AccRequired = false;
            this.txtCodeOrigine1.BackColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOrigine1.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOrigine1.Location = new System.Drawing.Point(612, 132);
            this.txtCodeOrigine1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOrigine1.MaxLength = 32767;
            this.txtCodeOrigine1.Multiline = false;
            this.txtCodeOrigine1.Name = "txtCodeOrigine1";
            this.txtCodeOrigine1.ReadOnly = true;
            this.txtCodeOrigine1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOrigine1.Size = new System.Drawing.Size(39, 27);
            this.txtCodeOrigine1.TabIndex = 502;
            this.txtCodeOrigine1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOrigine1.UseSystemPasswordChar = false;
            this.txtCodeOrigine1.Visible = false;
            // 
            // lblMail
            // 
            this.lblMail.AutoSize = true;
            this.lblMail.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMail.Location = new System.Drawing.Point(270, 56);
            this.lblMail.Name = "lblMail";
            this.lblMail.Size = new System.Drawing.Size(40, 17);
            this.lblMail.TabIndex = 0;
            this.lblMail.Text = "Email";
            this.lblMail.Click += new System.EventHandler(this.lblMail_Click);
            // 
            // lblCourier
            // 
            this.lblCourier.AutoSize = true;
            this.lblCourier.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCourier.Location = new System.Drawing.Point(30, 108);
            this.lblCourier.Name = "lblCourier";
            this.lblCourier.Size = new System.Drawing.Size(58, 17);
            this.lblCourier.TabIndex = 0;
            this.lblCourier.Text = "Courrier";
            this.lblCourier.Click += new System.EventHandler(this.lblCourier_Click);
            // 
            // lblFax
            // 
            this.lblFax.AutoSize = true;
            this.lblFax.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFax.Location = new System.Drawing.Point(30, 82);
            this.lblFax.Name = "lblFax";
            this.lblFax.Size = new System.Drawing.Size(28, 17);
            this.lblFax.TabIndex = 0;
            this.lblFax.Text = "Fax";
            this.lblFax.Click += new System.EventHandler(this.lblFax_Click);
            // 
            // txtCheminOS
            // 
            this.txtCheminOS.AccAcceptNumbersOnly = false;
            this.txtCheminOS.AccAllowComma = false;
            this.txtCheminOS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCheminOS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCheminOS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCheminOS.AccHidenValue = "";
            this.txtCheminOS.AccNotAllowedChars = null;
            this.txtCheminOS.AccReadOnly = false;
            this.txtCheminOS.AccReadOnlyAllowDelete = false;
            this.txtCheminOS.AccRequired = false;
            this.txtCheminOS.BackColor = System.Drawing.Color.White;
            this.txtCheminOS.CustomBackColor = System.Drawing.Color.White;
            this.txtCheminOS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCheminOS.ForeColor = System.Drawing.Color.Black;
            this.txtCheminOS.Location = new System.Drawing.Point(612, 101);
            this.txtCheminOS.Margin = new System.Windows.Forms.Padding(2);
            this.txtCheminOS.MaxLength = 32767;
            this.txtCheminOS.Multiline = false;
            this.txtCheminOS.Name = "txtCheminOS";
            this.txtCheminOS.ReadOnly = true;
            this.txtCheminOS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCheminOS.Size = new System.Drawing.Size(39, 27);
            this.txtCheminOS.TabIndex = 502;
            this.txtCheminOS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCheminOS.UseSystemPasswordChar = false;
            this.txtCheminOS.Visible = false;
            // 
            // txtDateOs
            // 
            this.txtDateOs.AccAcceptNumbersOnly = false;
            this.txtDateOs.AccAllowComma = false;
            this.txtDateOs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateOs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateOs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateOs.AccHidenValue = "";
            this.txtDateOs.AccNotAllowedChars = null;
            this.txtDateOs.AccReadOnly = false;
            this.txtDateOs.AccReadOnlyAllowDelete = false;
            this.txtDateOs.AccRequired = false;
            this.txtDateOs.BackColor = System.Drawing.Color.White;
            this.txtDateOs.CustomBackColor = System.Drawing.Color.White;
            this.txtDateOs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDateOs.ForeColor = System.Drawing.Color.Black;
            this.txtDateOs.Location = new System.Drawing.Point(310, 160);
            this.txtDateOs.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateOs.MaxLength = 32767;
            this.txtDateOs.Multiline = false;
            this.txtDateOs.Name = "txtDateOs";
            this.txtDateOs.ReadOnly = true;
            this.txtDateOs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateOs.Size = new System.Drawing.Size(141, 27);
            this.txtDateOs.TabIndex = 502;
            this.txtDateOs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateOs.UseSystemPasswordChar = false;
            // 
            // lblFaxouCourier
            // 
            this.lblFaxouCourier.AutoSize = true;
            this.lblFaxouCourier.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaxouCourier.Location = new System.Drawing.Point(448, 111);
            this.lblFaxouCourier.Name = "lblFaxouCourier";
            this.lblFaxouCourier.Size = new System.Drawing.Size(0, 17);
            this.lblFaxouCourier.TabIndex = 0;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(526, 6);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 17);
            this.label6.TabIndex = 0;
            this.label6.Text = "Fichier OS";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(251, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 17);
            this.label5.TabIndex = 0;
            this.label5.Text = "Date";
            // 
            // txtRefOS
            // 
            this.txtRefOS.AccAcceptNumbersOnly = false;
            this.txtRefOS.AccAllowComma = false;
            this.txtRefOS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefOS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefOS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRefOS.AccHidenValue = "";
            this.txtRefOS.AccNotAllowedChars = null;
            this.txtRefOS.AccReadOnly = false;
            this.txtRefOS.AccReadOnlyAllowDelete = false;
            this.txtRefOS.AccRequired = false;
            this.txtRefOS.BackColor = System.Drawing.Color.White;
            this.txtRefOS.CustomBackColor = System.Drawing.Color.White;
            this.txtRefOS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRefOS.ForeColor = System.Drawing.Color.Black;
            this.txtRefOS.Location = new System.Drawing.Point(59, 160);
            this.txtRefOS.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefOS.MaxLength = 32767;
            this.txtRefOS.Multiline = false;
            this.txtRefOS.Name = "txtRefOS";
            this.txtRefOS.ReadOnly = true;
            this.txtRefOS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefOS.Size = new System.Drawing.Size(141, 27);
            this.txtRefOS.TabIndex = 502;
            this.txtRefOS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefOS.UseSystemPasswordChar = false;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label19.Location = new System.Drawing.Point(10, 160);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(25, 17);
            this.Label19.TabIndex = 0;
            this.Label19.Text = "OS";
            // 
            // optOriCourrier
            // 
            this.optOriCourrier.AutoSize = true;
            this.optOriCourrier.Location = new System.Drawing.Point(10, 108);
            this.optOriCourrier.Name = "optOriCourrier";
            this.optOriCourrier.Size = new System.Drawing.Size(14, 13);
            this.optOriCourrier.TabIndex = 0;
            this.optOriCourrier.UseVisualStyleBackColor = true;
            this.optOriCourrier.CheckedChanged += new System.EventHandler(this.optOriCourrier_CheckedChanged);
            // 
            // OptOriAstreinte
            // 
            this.OptOriAstreinte.AutoSize = true;
            this.OptOriAstreinte.Location = new System.Drawing.Point(250, 85);
            this.OptOriAstreinte.Name = "OptOriAstreinte";
            this.OptOriAstreinte.Size = new System.Drawing.Size(84, 21);
            this.OptOriAstreinte.TabIndex = 0;
            this.OptOriAstreinte.Text = "Astreinte";
            this.OptOriAstreinte.UseVisualStyleBackColor = true;
            this.OptOriAstreinte.CheckedChanged += new System.EventHandler(this.OptOriAstreinte_CheckedChanged);
            // 
            // OptOSfichier
            // 
            this.OptOSfichier.AutoSize = true;
            this.OptOSfichier.Location = new System.Drawing.Point(529, 58);
            this.OptOSfichier.Name = "OptOSfichier";
            this.OptOSfichier.Size = new System.Drawing.Size(67, 21);
            this.OptOSfichier.TabIndex = 0;
            this.OptOSfichier.Text = "Fichier";
            this.OptOSfichier.UseVisualStyleBackColor = true;
            this.OptOSfichier.CheckedChanged += new System.EventHandler(this.radioButton12_CheckedChanged);
            // 
            // optOriMail
            // 
            this.optOriMail.AutoSize = true;
            this.optOriMail.Location = new System.Drawing.Point(250, 58);
            this.optOriMail.Name = "optOriMail";
            this.optOriMail.Size = new System.Drawing.Size(14, 13);
            this.optOriMail.TabIndex = 0;
            this.optOriMail.UseVisualStyleBackColor = true;
            this.optOriMail.CheckedChanged += new System.EventHandler(this.optOriMail_CheckedChanged);
            // 
            // optOriFax
            // 
            this.optOriFax.AutoSize = true;
            this.optOriFax.Location = new System.Drawing.Point(10, 84);
            this.optOriFax.Name = "optOriFax";
            this.optOriFax.Size = new System.Drawing.Size(14, 13);
            this.optOriFax.TabIndex = 0;
            this.optOriFax.UseVisualStyleBackColor = true;
            this.optOriFax.CheckedChanged += new System.EventHandler(this.optOriFax_CheckedChanged);
            // 
            // OptOSori
            // 
            this.OptOSori.AutoSize = true;
            this.OptOSori.Location = new System.Drawing.Point(529, 26);
            this.OptOSori.Name = "OptOSori";
            this.OptOSori.Size = new System.Drawing.Size(70, 21);
            this.OptOSori.TabIndex = 0;
            this.OptOSori.Text = "Origine";
            this.OptOSori.UseVisualStyleBackColor = true;
            this.OptOSori.CheckedChanged += new System.EventHandler(this.radioButton11_CheckedChanged);
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Location = new System.Drawing.Point(250, 31);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(108, 21);
            this.Option1.TabIndex = 0;
            this.Option1.Text = "Transmetteur";
            this.Option1.UseVisualStyleBackColor = true;
            this.Option1.CheckedChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // optOriDirect
            // 
            this.optOriDirect.AutoSize = true;
            this.optOriDirect.Location = new System.Drawing.Point(10, 58);
            this.optOriDirect.Name = "optOriDirect";
            this.optOriDirect.Size = new System.Drawing.Size(70, 21);
            this.optOriDirect.TabIndex = 0;
            this.optOriDirect.Text = "Interne";
            this.optOriDirect.UseVisualStyleBackColor = true;
            this.optOriDirect.CheckedChanged += new System.EventHandler(this.optOriDirect_CheckedChanged);
            // 
            // optOriRepondeur
            // 
            this.optOriRepondeur.AutoSize = true;
            this.optOriRepondeur.Location = new System.Drawing.Point(10, 31);
            this.optOriRepondeur.Name = "optOriRepondeur";
            this.optOriRepondeur.Size = new System.Drawing.Size(93, 21);
            this.optOriRepondeur.TabIndex = 0;
            this.optOriRepondeur.Text = "Répondeur";
            this.optOriRepondeur.UseVisualStyleBackColor = true;
            this.optOriRepondeur.CheckedChanged += new System.EventHandler(this.optOriRepondeur_CheckedChanged);
            // 
            // optOriRadio
            // 
            this.optOriRadio.AutoSize = true;
            this.optOriRadio.Location = new System.Drawing.Point(250, 4);
            this.optOriRadio.Name = "optOriRadio";
            this.optOriRadio.Size = new System.Drawing.Size(60, 21);
            this.optOriRadio.TabIndex = 0;
            this.optOriRadio.Text = "Radio";
            this.optOriRadio.UseVisualStyleBackColor = true;
            this.optOriRadio.CheckedChanged += new System.EventHandler(this.optOriRadio_CheckedChanged);
            // 
            // optOriTelephone
            // 
            this.optOriTelephone.AutoSize = true;
            this.optOriTelephone.Checked = true;
            this.optOriTelephone.Location = new System.Drawing.Point(10, 4);
            this.optOriTelephone.Name = "optOriTelephone";
            this.optOriTelephone.Size = new System.Drawing.Size(91, 21);
            this.optOriTelephone.TabIndex = 0;
            this.optOriTelephone.TabStop = true;
            this.optOriTelephone.Text = "Téléphone";
            this.optOriTelephone.UseVisualStyleBackColor = true;
            this.optOriTelephone.CheckedChanged += new System.EventHandler(this.optOriTelephone_CheckedChanged);
            // 
            // GridDoc
            // 
            this.GridDoc.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridDoc.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridDoc.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridDoc.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridDoc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDoc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDoc.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridDoc.DisplayLayout.UseFixedHeaders = true;
            this.GridDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDoc.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridDoc.Location = new System.Drawing.Point(3, 31);
            this.GridDoc.Name = "GridDoc";
            this.GridDoc.Size = new System.Drawing.Size(657, 185);
            this.GridDoc.TabIndex = 572;
            this.GridDoc.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridDoc_InitializeLayout);
            this.GridDoc.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridDoc_DoubleClickRow);
            // 
            // Label138
            // 
            appearance2.TextHAlignAsString = "Center";
            appearance2.TextVAlignAsString = "Middle";
            this.Label138.Appearance = appearance2;
            this.Label138.BorderStyleInner = Infragistics.Win.UIElementBorderStyle.Etched;
            this.Label138.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label138.Location = new System.Drawing.Point(3, 3);
            this.Label138.Name = "Label138";
            this.Label138.Size = new System.Drawing.Size(657, 22);
            this.Label138.TabIndex = 573;
            this.Label138.Text = "Fichiers associés à l\'intervention";
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(127, 67);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(258, 27);
            this.txtCodeimmeuble.TabIndex = 502;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(127, 36);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = true;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(258, 27);
            this.txtNoIntervention.TabIndex = 502;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            // 
            // txtNumficheStandard
            // 
            this.txtNumficheStandard.AccAcceptNumbersOnly = false;
            this.txtNumficheStandard.AccAllowComma = false;
            this.txtNumficheStandard.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumficheStandard.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumficheStandard.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumficheStandard.AccHidenValue = "";
            this.txtNumficheStandard.AccNotAllowedChars = null;
            this.txtNumficheStandard.AccReadOnly = false;
            this.txtNumficheStandard.AccReadOnlyAllowDelete = false;
            this.txtNumficheStandard.AccRequired = false;
            this.txtNumficheStandard.BackColor = System.Drawing.Color.White;
            this.txtNumficheStandard.CustomBackColor = System.Drawing.Color.White;
            this.txtNumficheStandard.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumficheStandard.ForeColor = System.Drawing.Color.Black;
            this.txtNumficheStandard.Location = new System.Drawing.Point(127, 5);
            this.txtNumficheStandard.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumficheStandard.MaxLength = 32767;
            this.txtNumficheStandard.Multiline = false;
            this.txtNumficheStandard.Name = "txtNumficheStandard";
            this.txtNumficheStandard.ReadOnly = true;
            this.txtNumficheStandard.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumficheStandard.Size = new System.Drawing.Size(258, 27);
            this.txtNumficheStandard.TabIndex = 502;
            this.txtNumficheStandard.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumficheStandard.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Label138, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.GridDoc, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Label11, 0, 2);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 328);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(663, 438);
            this.tableLayoutPanel1.TabIndex = 574;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.FileIntervention, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.DirNoInter, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 250);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(657, 185);
            this.tableLayoutPanel2.TabIndex = 575;
            // 
            // FileIntervention
            // 
            this.FileIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.FileIntervention.FormattingEnabled = true;
            this.FileIntervention.ItemHeight = 18;
            this.FileIntervention.Location = new System.Drawing.Point(331, 3);
            this.FileIntervention.Name = "FileIntervention";
            this.FileIntervention.Size = new System.Drawing.Size(323, 179);
            this.FileIntervention.TabIndex = 45;
            this.FileIntervention.DoubleClick += new System.EventHandler(this.FileIntervention_DoubleClick);
            // 
            // DirNoInter
            // 
            this.DirNoInter.AllowDrop = true;
            this.DirNoInter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DirNoInter.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.DirNoInter.Location = new System.Drawing.Point(3, 3);
            this.DirNoInter.Name = "DirNoInter";
            this.DirNoInter.Size = new System.Drawing.Size(322, 179);
            this.DirNoInter.TabIndex = 44;
            this.DirNoInter.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.DirNoInter_AfterSelect);
            this.DirNoInter.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.DirNoInter_NodeMouseClick);
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label11.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Bold);
            this.Label11.Location = new System.Drawing.Point(3, 222);
            this.Label11.Margin = new System.Windows.Forms.Padding(3);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(657, 22);
            this.Label11.TabIndex = 576;
            this.Label11.Text = "Dossier associé à l\'intervention.";
            this.Label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmFichierInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(684, 771);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.Frame1);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNumficheStandard);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(700, 810);
            this.MinimumSize = new System.Drawing.Size(700, 810);
            this.Name = "frmFichierInter";
            this.Text = "frmFichierInter";
            this.Load += new System.EventHandler(this.frmFichierInter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoc)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtNumficheStandard;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        public iTalk.iTalk_TextBox_Small2 txtDateOs;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtRefOS;
        private System.Windows.Forms.Label Label19;
        public iTalk.iTalk_TextBox_Small2 txtDocument;
        public iTalk.iTalk_TextBox_Small2 txtCodeOrigine1;
        public iTalk.iTalk_TextBox_Small2 txtCheminOS;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridDoc;
        public Infragistics.Win.Misc.UltraGroupBox Frame1;
        public System.Windows.Forms.RadioButton optOriSiteWeb;
        public System.Windows.Forms.RadioButton optOriCourrier;
        public System.Windows.Forms.RadioButton OptOriAstreinte;
        public System.Windows.Forms.RadioButton OptOSfichier;
        public System.Windows.Forms.RadioButton optOriMail;
        public System.Windows.Forms.RadioButton optOriFax;
        public System.Windows.Forms.RadioButton OptOSori;
        public System.Windows.Forms.RadioButton Option1;
        public System.Windows.Forms.RadioButton optOriDirect;
        public System.Windows.Forms.RadioButton optOriRepondeur;
        public System.Windows.Forms.RadioButton optOriRadio;
        public System.Windows.Forms.RadioButton optOriTelephone;
        public System.Windows.Forms.Label lblCourier;
        public System.Windows.Forms.Label lblFax;
        public System.Windows.Forms.Label lblMail;
        public Infragistics.Win.Misc.UltraLabel Label138;
        private System.Windows.Forms.Label lblFaxouCourier;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TreeView DirNoInter;
        private System.Windows.Forms.ListBox FileIntervention;
        private System.Windows.Forms.Label Label11;
    }
}