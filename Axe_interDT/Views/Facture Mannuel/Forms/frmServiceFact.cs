﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;

namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    public partial class frmServiceFact : Form
    {
        public frmServiceFact()
        {
            InitializeComponent();
        }
        private void fc_LibService()
        {
            try
            {
                var rs = new DataTable();
                if (cmbService.Text == "")
                {
                    lblLibService.Text = "";
                    return;
                }
                //'===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    lblLibService.Text = "";
                    return;
                }
                SAGE.fc_OpenConnSage();

                var sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";
                var ado = new ModAdo();
                rs = ado.fc_OpenRecordSet(sSQL, Conn: SAGE.adoSage);
                if (rs.Rows.Count > 0)
                    lblLibService.Text = rs.Rows[0]["CA_Intitule"] + "";
                else
                    lblLibService.Text = "";
                ado.Close();
                SAGE.fc_CloseConnSage();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LibService");
            }
        }

        private void cmbService_AfterCloseUp(object sender, EventArgs e)
        {
            fc_LibService();
        }

        private void cmbService_Validating(object sender, CancelEventArgs e)
        {
            fc_LibService();

            if (!ValidateService())
                e.Cancel = true;
        }

        private void cmbService_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                var sSQL = "SELECT     CA_Num as Code, CA_Intitule AS Intitulé " + " From F_COMPTEA";
                sSQL = sSQL + ModAnalytique.fc_WhereServiceAnalytique(" Where N_Analytique = 6");
                sSQL = sSQL + " ORDER BY CA_Num";

                // '===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    SAGE.fc_OpenConnSage();
                    sheridan.InitialiseCombo(cmbService, sSQL, "Code", AdoConnection: SAGE.adoSage);
                    SAGE.fc_CloseConnSage();
                    if (cmbService.DisplayLayout.Bands[0].Columns.Count > 1)
                        cmbService.DisplayLayout.Bands[0].Columns[1].Width = 500;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbService_DropDown");
            }
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            ModMain.sServiceFact = "";
            this.Close();
        }

        /// <summary>
        /// TESTED -  Mondir 15.06.2020, check if Code Service is Valide
        /// </summary>
        /// <returns></returns>
        private bool ValidateService()
        {

            var sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";
            using (var tmpModAdo = new ModAdo())
            {
                SAGE.fc_OpenConnSage();
                sSQL = tmpModAdo.fc_ADOlibelle(sSQL, adoLibelle: SAGE.adoSage);
                if (string.IsNullOrEmpty(sSQL))
                {
                    CustomMessageBox.Show("Code Service Invalide", "Code Invalide", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    cmbService.Focus();
                    return false;
                }
            }
            return true;
        }

        private void cmdValider_Click(object sender, EventArgs e)
        {
            if (cmbService.Text == "")
            {
                Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un service.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (!ValidateService())
                return;

            ///=====> Mondir le 02.07.2020, pour ajouter les modifs de la version V02.09.2020
            ModMain.sTVAfactXX = "";

            if (txtDevis.Text != "1")
            {
                if (!optTVAimm.Checked && !OptTR.Checked && !optTP.Checked)
                {
                    Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner le type de TVA.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (optTVAimm.Checked)
                    ModMain.sTVAfactXX = "";
                else if (OptTR.Checked)
                    ModMain.sTVAfactXX = "TR";
                else if (optTP.Checked)
                    ModMain.sTVAfactXX = "TP";
            }
            //====> Fin Modif Mondir

            ModMain.sServiceFact = cmbService.Text;

            this.Close();
        }
    }
}
