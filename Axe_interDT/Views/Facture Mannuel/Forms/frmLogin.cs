﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        private void cmdOK_Click(object sender, EventArgs e)
        {
            ModMain.londMsgb = 7;
            Close();
        }

        private void cmdCancel_Click(object sender, EventArgs e)
        {
            ModMain.londMsgb = 0;
            Close();
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {
            ModMain.londMsgb = 0;
        }
    }
}
