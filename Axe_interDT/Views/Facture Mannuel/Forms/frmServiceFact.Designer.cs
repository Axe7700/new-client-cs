﻿namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    partial class frmServiceFact
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.label1 = new System.Windows.Forms.Label();
            this.label2s = new System.Windows.Forms.Label();
            this.cmbService = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblLibService = new iTalk.iTalk_TextBox_Small2();
            this.Label2 = new System.Windows.Forms.Label();
            this.optTVAimm = new System.Windows.Forms.RadioButton();
            this.OptTR = new System.Windows.Forms.RadioButton();
            this.optTP = new System.Windows.Forms.RadioButton();
            this.cmdValider = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.txtDevis = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.cmbService)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(502, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Veuillez sélectionner un service et  le type de Tva à affecter à la facture.";
            // 
            // label2s
            // 
            this.label2s.AutoSize = true;
            this.label2s.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2s.Location = new System.Drawing.Point(17, 59);
            this.label2s.Name = "label2s";
            this.label2s.Size = new System.Drawing.Size(65, 19);
            this.label2s.TabIndex = 1;
            this.label2s.Text = "SERVICE";
            // 
            // cmbService
            // 
            this.cmbService.AutoSize = false;
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbService.DisplayLayout.Appearance = appearance1;
            this.cmbService.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbService.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbService.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbService.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbService.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbService.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbService.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbService.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbService.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbService.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbService.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbService.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbService.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbService.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbService.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbService.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbService.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbService.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbService.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbService.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbService.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbService.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbService.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbService.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbService.Location = new System.Drawing.Point(125, 56);
            this.cmbService.Name = "cmbService";
            this.cmbService.Size = new System.Drawing.Size(100, 27);
            this.cmbService.TabIndex = 2;
            this.cmbService.AfterCloseUp += new System.EventHandler(this.cmbService_AfterCloseUp);
            this.cmbService.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbService_BeforeDropDown);
            this.cmbService.Validating += new System.ComponentModel.CancelEventHandler(this.cmbService_Validating);
            // 
            // lblLibService
            // 
            this.lblLibService.AccAcceptNumbersOnly = false;
            this.lblLibService.AccAllowComma = false;
            this.lblLibService.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibService.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibService.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibService.AccHidenValue = "";
            this.lblLibService.AccNotAllowedChars = null;
            this.lblLibService.AccReadOnly = false;
            this.lblLibService.AccReadOnlyAllowDelete = false;
            this.lblLibService.AccRequired = false;
            this.lblLibService.BackColor = System.Drawing.Color.White;
            this.lblLibService.CustomBackColor = System.Drawing.Color.White;
            this.lblLibService.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblLibService.ForeColor = System.Drawing.Color.Black;
            this.lblLibService.Location = new System.Drawing.Point(230, 56);
            this.lblLibService.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibService.MaxLength = 32767;
            this.lblLibService.Multiline = false;
            this.lblLibService.Name = "lblLibService";
            this.lblLibService.ReadOnly = true;
            this.lblLibService.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibService.Size = new System.Drawing.Size(285, 27);
            this.lblLibService.TabIndex = 502;
            this.lblLibService.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibService.UseSystemPasswordChar = false;
            // 
            // Label2
            // 
            this.Label2.AutoSize = true;
            this.Label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label2.Location = new System.Drawing.Point(12, 106);
            this.Label2.Name = "Label2";
            this.Label2.Size = new System.Drawing.Size(97, 19);
            this.Label2.TabIndex = 1;
            this.Label2.Text = "TYPE DE TVA";
            // 
            // optTVAimm
            // 
            this.optTVAimm.AutoSize = true;
            this.optTVAimm.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTVAimm.Location = new System.Drawing.Point(125, 102);
            this.optTVAimm.Name = "optTVAimm";
            this.optTVAimm.Size = new System.Drawing.Size(154, 23);
            this.optTVAimm.TabIndex = 503;
            this.optTVAimm.TabStop = true;
            this.optTVAimm.Text = "TVA de l\'immeuble";
            this.optTVAimm.UseVisualStyleBackColor = true;
            // 
            // OptTR
            // 
            this.OptTR.AutoSize = true;
            this.OptTR.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptTR.Location = new System.Drawing.Point(125, 131);
            this.OptTR.Name = "OptTR";
            this.OptTR.Size = new System.Drawing.Size(44, 23);
            this.OptTR.TabIndex = 503;
            this.OptTR.TabStop = true;
            this.OptTR.Text = "TR";
            this.OptTR.UseVisualStyleBackColor = true;
            // 
            // optTP
            // 
            this.optTP.AutoSize = true;
            this.optTP.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optTP.Location = new System.Drawing.Point(125, 160);
            this.optTP.Name = "optTP";
            this.optTP.Size = new System.Drawing.Size(45, 23);
            this.optTP.TabIndex = 503;
            this.optTP.TabStop = true;
            this.optTP.Text = "TP";
            this.optTP.UseVisualStyleBackColor = true;
            // 
            // cmdValider
            // 
            this.cmdValider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdValider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValider.FlatAppearance.BorderSize = 0;
            this.cmdValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValider.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValider.ForeColor = System.Drawing.Color.White;
            this.cmdValider.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdValider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdValider.Location = new System.Drawing.Point(413, 206);
            this.cmdValider.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(110, 35);
            this.cmdValider.TabIndex = 578;
            this.cmdValider.Tag = "";
            this.cmdValider.Text = "  Valider";
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Click += new System.EventHandler(this.cmdValider_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(302, 206);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(107, 35);
            this.cmdAnnuler.TabIndex = 580;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.Text = "  Annuler";
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // txtDevis
            // 
            this.txtDevis.AccAcceptNumbersOnly = false;
            this.txtDevis.AccAllowComma = false;
            this.txtDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDevis.AccHidenValue = "";
            this.txtDevis.AccNotAllowedChars = null;
            this.txtDevis.AccReadOnly = false;
            this.txtDevis.AccReadOnlyAllowDelete = false;
            this.txtDevis.AccRequired = false;
            this.txtDevis.BackColor = System.Drawing.Color.White;
            this.txtDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDevis.ForeColor = System.Drawing.Color.Black;
            this.txtDevis.Location = new System.Drawing.Point(21, 206);
            this.txtDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtDevis.MaxLength = 32767;
            this.txtDevis.Multiline = false;
            this.txtDevis.Name = "txtDevis";
            this.txtDevis.ReadOnly = true;
            this.txtDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDevis.Size = new System.Drawing.Size(128, 27);
            this.txtDevis.TabIndex = 581;
            this.txtDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDevis.UseSystemPasswordChar = false;
            this.txtDevis.Visible = false;
            // 
            // frmServiceFact
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(534, 250);
            this.Controls.Add(this.txtDevis);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.cmdValider);
            this.Controls.Add(this.optTP);
            this.Controls.Add(this.OptTR);
            this.Controls.Add(this.optTVAimm);
            this.Controls.Add(this.lblLibService);
            this.Controls.Add(this.cmbService);
            this.Controls.Add(this.Label2);
            this.Controls.Add(this.label2s);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(550, 289);
            this.MinimumSize = new System.Drawing.Size(550, 289);
            this.Name = "frmServiceFact";
            this.Text = "frmServiceFact";
            ((System.ComponentModel.ISupportInitialize)(this.cmbService)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2s;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbService;
        public iTalk.iTalk_TextBox_Small2 lblLibService;
        public System.Windows.Forms.Button cmdValider;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.RadioButton optTVAimm;
        public System.Windows.Forms.RadioButton OptTR;
        public System.Windows.Forms.RadioButton optTP;
        public iTalk.iTalk_TextBox_Small2 txtDevis;
        public System.Windows.Forms.Label Label2;
    }
}