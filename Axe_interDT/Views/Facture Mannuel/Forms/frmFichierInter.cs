﻿using Axe_interDT.Shared;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Forms;
using System;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    public partial class frmFichierInter : Form
    {
        public frmFichierInter()
        {
            InitializeComponent();
        }
        DataTable rsDoc = new DataTable();
        ModAdo rsDocAdo = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Load()
        {
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            string sSQL;
            try
            {
                sSQL = "SELECT        GestionStandard.[Document], GestionStandard.RefOS, GestionStandard.DateOS, GestionStandard.CheminOS,"
                       + " GestionStandard.FichierOSorigine, Intervention.NoIntervention,  GestionStandard.CodeOrigine1, "
                       + " GestionStandard.CodeImmeuble, GestionStandard.NumFicheStandard "
                       + " FROM            GestionStandard INNER JOIN "
                       + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard "
                       + " WHERE        (Intervention.NoIntervention = " + General.nz(txtNoIntervention.Text, 0) + ")";

                rs = rsAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    txtDocument.Text = rs.Rows[0]["Document"] + "";
                    txtRefOS.Text = rs.Rows[0]["RefOS"] + "";
                    txtDateOs.Text = rs.Rows[0]["DateOS"] + "";
                    txtCheminOS.Text = rs.Rows[0]["CheminOS"] + "";
                    txtCodeimmeuble.Text = rs.Rows[0]["CodeImmeuble"] + "";
                    txtNumficheStandard.Text = rs.Rows[0]["NumFicheStandard"] + "";

                    fc_Option(General.nz(rs.Rows[0]["CodeOrigine1"], "").ToString());
                    fc_origine(General.nz(rs.Rows[0]["CodeOrigine1"], "").ToString());

                    Label19.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
                    Label19.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                    if (Convert.ToInt32(General.nz(rs.Rows[0]["FichierOSorigine"], 0)) == 1)
                    {
                        OptOSori.Checked = true;
                    }
                    else if (Convert.ToInt32(General.nz(rs.Rows[0]["FichierOSorigine"], 0)) == 2)
                    {
                        OptOSfichier.Checked = true;
                        Label19.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
                        Label19.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    }
                    else
                    {
                        OptOSori.Checked = false;
                        OptOSfichier.Checked = false;
                    }
                    fc_LoadDoc(Convert.ToInt32(General.nz(txtNoIntervention.Text, 0)));
                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_Load");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNointervention"></param>
        private void fc_LoadDoc(int lNointervention)
        {

            string sSQL;
            try
            {

                sSQL = "SELECT     TypeFichier, Chemin, NomFichier, CreeLe, CreePar"
                        + " From InterventionDoc "
                        + " Where Nointervention = " + lNointervention
                        + " ORDER BY TypeFichier";

                rsDoc = rsDocAdo.fc_OpenRecordSet(sSQL);
                GridDoc.DataSource = rsDoc;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_LoadDoc");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_origine(string sCode)
        {

            lblFax.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
            lblFax.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblCourier.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
            lblCourier.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            lblFaxouCourier.Text = "";

            lblMail.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
            lblMail.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

            if (sCode == General.cTel.ToString())
            {
                txtCodeOrigine1.Text = General.cTel.ToString();
            }
            else if (sCode == General.cFax.ToString())
            {
                txtCodeOrigine1.Text = General.cFax.ToString();
                if (txtDocument.Text != "")
                {
                    lblFax.ForeColor = ColorTranslator.FromOle(General.cForeColorLien);
                    lblFax.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
            else if (sCode == General.cInternet.ToString())
            {
                txtCodeOrigine1.Text = General.cInternet.ToString();
            }
            else if (sCode == General.cRepondeur.ToString())
            {
                txtCodeOrigine1.Text = General.cRepondeur.ToString();
            }
            else if (sCode == General.cCourrier.ToString())
            {
                txtCodeOrigine1.Text = General.cCourrier.ToString();
                if (txtDocument.Text != "")
                {
                    lblCourier.ForeColor = ColorTranslator.FromOle(General.cForeColorLien);
                    lblCourier.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }

            }
            else if (sCode == General.cRadio.ToString())
            {
                txtCodeOrigine1.Text = General.cRadio.ToString();
            }
            else if (sCode == General.cDirect.ToString())
            {
                txtCodeOrigine1.Text = General.cDirect.ToString();
            }
            else if (sCode == General.CEmail.ToString())
            {
                txtCodeOrigine1.Text = General.CEmail.ToString();
                if (txtDocument.Text != "")
                {
                    lblMail.ForeColor = ColorTranslator.FromOle(General.cForeColorLien);
                    lblMail.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                }
            }
            else if (sCode == General.CAstreinte.ToString())
            {
                txtCodeOrigine1.Text = General.CAstreinte.ToString();
            }

            return;

        }
        private void radioButton11_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioButton12_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void optOriRadio_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void Option1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblMail_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDocument.Text))
            {
                ModuleAPI.Ouvrir(txtDocument.Text);
            }
        }

        private void optOriMail_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void OptOriAstreinte_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void optOriSiteWeb_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblCourier_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDocument.Text))
            {
                ModuleAPI.Ouvrir(txtDocument.Text);
            }
        }

        private void optOriCourrier_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void optOriFax_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblFax_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDocument.Text))
            {
                ModuleAPI.Ouvrir(txtDocument.Text);
            }
        }

        private void optOriDirect_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void optOriRepondeur_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void optOriTelephone_CheckedChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// TEsted
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFichierInter_Load(object sender, EventArgs e)
        {
            fc_Load();

            //===> Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
            if (Dossier.fc_ControleDossier(General.DossierIntervention + txtNoIntervention.Text))
            {
                DirNoInter.Tag = General.DossierIntervention + txtNoIntervention.Text;
                Label11.Text = "Dossier associé à l'intervention.";
                Label11.ForeColor = Color.Black;

                var node = new TreeNode(txtNoIntervention.Text) { Tag = DirNoInter.Tag };
                var random = Guid.NewGuid().ToString();
                node.Name = random;
                node.ImageIndex = 1;
                node.SelectedImageIndex = 0;
                if (node.Text.Length < 248)
                {
                    DirNoInter.Nodes.Add(node);
                }

                ListSubFolders(node);

                //ListDirectory(DirNoInter, DirNoInter.Tag.ToString());
            }
            else
            {
                Label11.Text = "AUCUN DOSSIER N'EST ASSOCIE A CETTE INTERVENTION.";
                Label11.ForeColor = Color.Red;
            }
            //===> Fin Modif Mondir
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoc_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (e.Row != null)
            {
                if (e.Row.Cells["Chemin"].Text != "")
                    ModuleAPI.Ouvrir(General.DossierIntervention + e.Row.Cells["Chemin"].Text);
            }
        }

        private void GridDoc_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            e.Layout.Bands[0].Columns[0].Header.Caption = "Type Fichier";
            e.Layout.Bands[0].Columns[0].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            e.Layout.Bands[0].Columns[0].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            e.Layout.Bands[0].Columns["NomFichier"].Hidden = true;
            e.Layout.Bands[0].Columns["CreeLe"].Hidden = true;
            e.Layout.Bands[0].Columns["CreePar"].Hidden = true;
        }
        private void fc_Option(string sCode)
        {
            /*
             * ' positionne les boutons options
             *' en donnant la valeur true au bouton option on active l'événement click de celui ci,
             *' dans cet événement une fonction est lancée, mais si ce bouton a déja la valeur true
             *' l'événement ne sera pas activé, pour le forcer on initialise sa valeur à false.
             */
            try
            {
                if (sCode == General.cTel + "")
                {
                    optOriTelephone.Checked = false;
                    optOriTelephone.Checked = true;
                }
                else if (sCode == General.cFax + "")
                {
                    optOriFax.Checked = false;
                    optOriFax.Checked = true;
                }
                else if (sCode == General.cInternet + "")
                {
                    optOriSiteWeb.Checked = false;
                    optOriSiteWeb.Checked = true;
                }
                else if (sCode == General.cCourrier + "")
                {
                    optOriCourrier.Checked = false;
                    optOriCourrier.Checked = true;
                }
                else if (sCode == General.cRadio + "")
                {
                    optOriRadio.Checked = false;
                    optOriRadio.Checked = true;
                }
                else if (sCode == General.cDirect + "")
                {
                    optOriDirect.Checked = false;
                    optOriDirect.Checked = true;
                }
                else if (sCode == General.CEmail + "")
                {
                    optOriMail.Checked = false;
                    optOriMail.Checked = true;
                }
                else if (sCode == General.CAstreinte + "")
                {
                    OptOriAstreinte.Checked = false;
                    OptOriAstreinte.Checked = true;
                }
                else
                {

                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + " fc_Option ");
            }
        }

        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirNoInter_AfterSelect(object sender, TreeViewEventArgs e)
        {
            FileIntervention.Tag = DirNoInter.Tag;
            FilesInDirectory(FileIntervention, new DirectoryInfo(e.Node.Tag.ToString()));
        }

        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="listBox"></param>
        /// <param name="directoryInfo"></param>
        private void FilesInDirectory(ListBox listBox, DirectoryInfo directoryInfo)
        {
            try
            {
                listBox.Items.Clear();
                if (directoryInfo.Exists)
                    foreach (var file in directoryInfo.GetFiles())
                    {
                        listBox.Items.Add(file.Name);
                    }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }


        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirNoInter_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
                DirNoInter.SelectedNode = e.Node;
        }

        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileIntervention_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                string sChemin = "";
                if (FileIntervention.SelectedItem != null)
                {
                    if (DirNoInter.SelectedNode != null && DirNoInter.SelectedNode.Tag != null)
                    {
                        sChemin = DirNoInter.SelectedNode.Tag + "\\" + FileIntervention.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }
                    else
                    {
                        sChemin = DirNoInter.Tag + "\\" + FileIntervention.SelectedItem.ToString();
                        Process.Start(sChemin);
                    }

                }

            }
            catch (Exception ee) { Erreurs.gFr_debug(ee, this.Name + ";File1_DblClick"); }
        }

        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="treeView"></param>
        /// <param name="path"></param>
        private void ListDirectory(TreeView treeView, string path)
        {
            try
            {
                treeView.Nodes.Clear();
                treeView.SuspendLayout();

                //var stack = new Stack<TreeNode>();
                //var rootDirectory = new DirectoryInfo(path);
                //var node = new TreeNode(rootDirectory.Name) { Tag = rootDirectory };
                //stack.Push(node);

                //while (stack.Count > 0)
                //{
                //    var currentNode = stack.Pop();
                //    currentNode.EnsureVisible();
                //    var directoryInfo = (DirectoryInfo)currentNode.Tag;
                //    if(directoryInfo.Exists)
                //    foreach (var directory in directoryInfo.GetDirectories())
                //    {
                //        var childDirectoryNode = new TreeNode(directory.Name) { Tag = directory.FullName };
                //        childDirectoryNode.ImageIndex = 1;
                //        childDirectoryNode.SelectedImageIndex = 0;
                //        currentNode.Nodes.Add(childDirectoryNode);
                //        // stack.Push(childDirectoryNode);
                //    }

                //}

                //treeView.Nodes.Add(node);

                //treeView.ExpandAll();
                //treeView.ResumeLayout();
                //foreach (TreeNode TN in treeView.Nodes)
                //    ListSubFolders(TN);


                if (Directory.Exists(path))
                {
                    var rootDirectory = new DirectoryInfo(path);
                    if (rootDirectory.GetDirectories().Length > 0)
                        foreach (var Dir in rootDirectory.GetDirectories())
                        {
                            var node = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            var random = Guid.NewGuid().ToString();
                            node.Name = random;
                            node.ImageIndex = 1;
                            node.SelectedImageIndex = 0;
                            if (node.Text.Length < 248)
                            {
                                treeView.Nodes.Add(node);
                            }
                        }

                    treeView.ExpandAll();
                    treeView.ResumeLayout();
                    foreach (TreeNode TN in treeView.Nodes)
                        ListSubFolders(TN);
                }

            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Cursor = Cursors.Default;
            }
        }

        /// <summary>
        /// Mondir le 13.11.2020, ajout des modifs de la version V12.11.2020
        /// </summary>
        /// <param name="TN"></param>
        private void ListSubFolders(TreeNode TN)
        {
            try
            {

                DirectoryInfo DI = new DirectoryInfo(TN.Tag.ToString());
                if (DI.Exists)
                    if (DI.GetDirectories().Length > 0)
                    {
                        foreach (var Dir in DI.GetDirectories())
                        {
                            var childDirectoryNode = new TreeNode(Dir.Name) { Tag = Dir.FullName };
                            childDirectoryNode.ImageIndex = 1;
                            childDirectoryNode.SelectedImageIndex = 0;
                            var random = Guid.NewGuid().ToString();
                            childDirectoryNode.Name = random;
                            TN.Nodes.Add(childDirectoryNode);
                            ListSubFolders(childDirectoryNode);
                        }
                    }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
    }
}

