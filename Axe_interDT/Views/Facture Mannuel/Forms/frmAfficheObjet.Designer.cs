﻿namespace Axe_interDT.Views.Facture_Mannuel.Forms
{
    partial class frmAfficheObjet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdFermer = new System.Windows.Forms.Button();
            this.OLE1 = new iTalk.iTalk_RichTextBox();
            this.SuspendLayout();
            // 
            // cmdFermer
            // 
            this.cmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFermer.FlatAppearance.BorderSize = 0;
            this.cmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFermer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdFermer.ForeColor = System.Drawing.Color.White;
            this.cmdFermer.Image = global::Axe_interDT.Properties.Resources.cancel_24;
            this.cmdFermer.Location = new System.Drawing.Point(411, 11);
            this.cmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFermer.Name = "cmdFermer";
            this.cmdFermer.Size = new System.Drawing.Size(60, 35);
            this.cmdFermer.TabIndex = 575;
            this.cmdFermer.Text = "   Annuler";
            this.cmdFermer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdFermer.UseVisualStyleBackColor = false;
            this.cmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // OLE1
            // 
            this.OLE1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.OLE1.AutoWordSelection = false;
            this.OLE1.BackColor = System.Drawing.Color.Transparent;
            this.OLE1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OLE1.ForeColor = System.Drawing.Color.Black;
            this.OLE1.Location = new System.Drawing.Point(12, 51);
            this.OLE1.Name = "OLE1";
            this.OLE1.ReadOnly = false;
            this.OLE1.Size = new System.Drawing.Size(459, 256);
            this.OLE1.TabIndex = 576;
            this.OLE1.WordWrap = true;
            // 
            // frmAfficheObjet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(481, 319);
            this.Controls.Add(this.OLE1);
            this.Controls.Add(this.cmdFermer);
            this.Name = "frmAfficheObjet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAfficheObjet";
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button cmdFermer;
        public iTalk.iTalk_RichTextBox OLE1;
    }
}