﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Documents.Excel;
using Infragistics.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Axe_interDT.View.SharedViews
{
    public partial class SearchTemplate : Form
    {

        private String lblGridResultat = " Enrg.";
        private Dictionary<String, iTalk.iTalk_TextBox_Small2> champs = new Dictionary<String, iTalk.iTalk_TextBox_Small2>();
        private String requete = "";
        private Dictionary<String, String[]> columns = new Dictionary<string, string[]>();
        private HashSet<Critere> ctrs = new HashSet<Critere>();
        private object myUC;
        private HashSet<Object> controls;
        public String where;
        private int PageSize;
        private int Page;
        private string OrderBy;
        private string FirstColumn = "";
        private DataTable DataTableResult = new DataTable();
        private bool FirstDataSource = true;
        private AxeGrid AxeGrid;
        private ModAdo ModAdo;
        string RegFolder = General.fncUserName() + "_SearchTemplate";
        const string RegFormWidth = "SearchTemplate_Width";
        const string RegFormHeight = "SearchTemplate_Height";
        public SqlConnection Connection { get; set; }
        public SearchTemplate()
        {
            InitializeComponent();

            //Location = new Point(Location.X + 1, Location.Y);

            //this.Scale(new SizeF(2, 2));
        }
        public SearchTemplate(object activeUC, HashSet<Object> controls, String requete, String where, string OrderBy, int PageSize = 5000, int Page = 1, SqlConnection Connection = null) : this()
        {


            ugResultat.DisplayLayout.Override.CellClickAction = CellClickAction.RowSelect;

            //MaximizeBox = false;
            //MinimizeBox = false;
            this.myUC = activeUC;
            this.controls = controls;
            this.requete = requete;
            this.where = where;
            this.PageSize = PageSize;
            this.Page = Page;
            this.OrderBy = OrderBy;
            this.Connection = Connection;
            ModAdo = new ModAdo();
            lectureChamp();

            //clsResize _form_resize = new clsResize(this);

            //this.Load += (se, ev) => _form_resize._get_initial_size();
            //this.Resize += (se, ev) => _form_resize._resize();
            //this.Resize += (se, ev) => _form_resize._resize();
            //this.SizeChanged += (se, ev) => _form_resize._resize();

        }



        private void FiltreGenerique_Load(object sender, EventArgs e)
        {

            Theme.Theme.recursiveLoopOnFrms(this);

            ResourceCustomizer rc;

            // Get the resource customizer object for the
            // 'Infragistics.Win.UltraWinTree' assembly.
            // and customize a couple of string resources.
            rc = Infragistics.Win.UltraWinGrid.Resources.Customizer;

            rc.SetCustomizedString("RowFilterDropDown_Operator_Like", "Comme");

            rc.SetCustomizedString("RowFilterDropDown_Operator_Match", "Correspond");
            rc.SetCustomizedString("RowFilterDropDown_Operator_NotEquals", "Different");
            rc.SetCustomizedString("RowFilterDropDown_Operator_NotLike", "Pas comme");
            rc.SetCustomizedString("RowFilterDropDown_Operator_StartsWith", "Commence  par");
            rc.SetCustomizedString("RowFilterDropDownAllItem", "Tout");
            rc.SetCustomizedString("RowFilterDropDownBlanksItem", "Vierge");
            rc.SetCustomizedString("RowFilterDropDownCustomItem", "Personnalisée");
            rc.SetCustomizedString("RowFilterDropDownEquals", "Équivaut à");
            rc.SetCustomizedString("RowFilterDropDownErrorsItem", "Erreurs");
            rc.SetCustomizedString("RowFilterDropDownGreaterThan", "Supérieur à");
            rc.SetCustomizedString("RowFilterDropDownGreaterThanOrEqualTo", "Supérieur  ou égal à");
            rc.SetCustomizedString("RowFilterDropDownLessThan", "Inférieur à");
            rc.SetCustomizedString("RowFilterDropDownLessThanOrEqualTo", "Inférieur ou égal à");
            rc.SetCustomizedString("RowFilterDropDownLike", "Comme l'expression régulière");
            rc.SetCustomizedString("RowFilterDropDownMatch", "Correspond à l'expression régulière");
            rc.SetCustomizedString("RowFilterDropDownNonBlanksItem", "(SansVides)");
            rc.SetCustomizedString("RowFilterDropDownNonErrorsItem", "(SansErreurs)");
            rc.SetCustomizedString("RowFilterDropDownNotEquals", "N'est pas égal");
            rc.SetCustomizedString("RowFilterLogicalOperator_And", "ET");
            rc.SetCustomizedString("RowFilterLogicalOperator_Or", "OU");

            rc.SetCustomizedString("RowFilterDropDown_Operator_Contains", "Contient");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotContain", "Ne contient pas");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotEndWith", "Ne se termine pas par");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotMatch", "Ne Correspond pas à");
            rc.SetCustomizedString("RowFilterDropDown_Operator_DoesNotStartWith", "Ne commence pas par");
            rc.SetCustomizedString("RowFilterDropDown_Operator_EndsWith", "se termine pas");

            rc.SetCustomizedString("SummaryDialogAverage", "Moyenne");
            rc.SetCustomizedString("SummaryDialogCount", "Total");
            rc.SetCustomizedString("SummaryTypeCustom", "Personnalisée");
            rc.SetCustomizedString("SummaryDialogMaximum", "Maximum");
            rc.SetCustomizedString("SummaryDialogMinimum", "Minimum");
            rc.SetCustomizedString("SummaryDialogSum", "Somme");
            rc.SetCustomizedString("LDR_SelectSummaries", "Sélectionner des résumés");

            // Get the resource customizer object for the
            // 'Infragistics.Win' assembly to customize one
            // of its string resources.
            rc = Infragistics.Win.Resources.Customizer;
            this.Width = Convert.ToInt32(General.getFrmReg(RegFolder, RegFormWidth, "650"));
            this.Height = Convert.ToInt32(General.getFrmReg(RegFolder, RegFormHeight, "650"));
            this.StartPosition = FormStartPosition.CenterParent;
        }
        public void RecControls(Control C)
        {
            foreach (Control CC in C.Controls)
            {
                //if (CC.HasChildren)

                RecControls(CC);

                if (CC is iTalk.iTalk_TextBox_Small2)
                {
                    var tmp = CC as iTalk.iTalk_TextBox_Small2;
                    if (!string.IsNullOrEmpty(tmp.Text.ToString()))
                    {
                        tmp.iTalkTB.Focus();
                        //tmp.iTalkTB.Select(tmp.iTalkTB.Text.Length,0);
                    }

                }

                if (CC is UltraCombo)
                {
                    var tmp = CC as UltraCombo;
                    if (!string.IsNullOrEmpty(tmp.Text.ToString()))
                    {
                        tmp.Focus();
                        tmp.Select();
                    }

                }

            }
        }

        //
        // Résumé :
        //    Permet d'extracter les champs et les designations de chaque colonne.
        //
        // Paramètres :
        //   value:
        //     la requete SQL sous le format suivant .
        // SELECT table.[col1] as \"premiere colonne \", table.[col2] as \"Deuxieme colonne\" FROM Immeuble
        // Retourne :
        //     rien.

        public void lectureChamp()
        {
            using (var tmpModAdo = new ModAdo())
            {
                Regex columnRegEx = new Regex(@"\[.*?\]");
                MatchCollection fields = columnRegEx.Matches(requete);
                Regex designationRegEx = new Regex("\".*?\"");
                MatchCollection designations = designationRegEx.Matches(requete);

                SqlDataReader reader = null;
                // create a connection object
                string condition = "";

                if (where == "") condition = " where 1 <> 1";
                else condition = " where 1 <> 1 and " + where;
                String selectTableInfo = requete + condition;

                if (Connection == null)
                    Connection = General.adocnn;

                if (Connection.State == ConnectionState.Closed)
                    Connection.Open();

                SqlCommand cmd = new SqlCommand(selectTableInfo, Connection);
                ugResultat.DataSource = tmpModAdo.fc_OpenRecordSet(selectTableInfo);

                // open the connection
                // 1. get an instance of the SqlDataReader

                reader = cmd.ExecuteReader(CommandBehavior.KeyInfo);

                // 2. print necessary columns of each record
                DataTable schemaTable = reader.GetSchemaTable();
                //For each field in the table...
                int i = 0;
                foreach (DataRow row in schemaTable.Rows)
                {
                    if (i < designations.Count)
                    {
                        if (FirstColumn == "")
                            FirstColumn = $"{row["BaseTableName"]}.{row["BaseColumnName"]}";
                        columns.Add(row["BaseColumnName"].ToString(),
                            new string[]
                            {
                                row["DataTypeName"].ToString(), row["ColumnName"].ToString(),
                                row["BaseTableName"].ToString()
                            });
                        i++;
                    }
                    else
                    {
                        break;
                    }
                }
                // 3. close the reader
                if (reader != null)
                {
                    reader.Close();
                }
                // close the connection

                if (Connection.State == ConnectionState.Open)
                    Connection.Close();

                creationDeschamps();
            }
        }
        private void AddRowToPanel(TableLayoutPanel panel, List<Control> rowElements)
        {
            if (panel.ColumnCount != rowElements.Count)
                throw new Exception("Elements number doesn't match!");

            //get a reference to the previous existent row
            RowStyle temp = panel.RowStyles[panel.RowCount - 1];
            //increase panel rows count by one
            if (panel.RowCount != 1)
                panel.RowCount++;
            //add a new RowStyle as a copy of the previous one
            panel.RowStyles.Add(new RowStyle(temp.SizeType, temp.Height));

            //add the control
            for (int i = 0; i < rowElements.Count - 1; i++)
            {
                //rowElements[i].Dock = DockStyle.Fill;
                //panel.Controls.Add(rowElements[i], i, panel.RowCount - 1);
                //if (i == rowElements.Count - 1)
                //  panel.SetColumnSpan(rowElements[i], 2);
                if (rowElements[i] is UltraTextEditor)
                {
                    UltraTextEditor ctrl = rowElements[i] as UltraTextEditor;
                    ctrl.UseAppStyling = false;
                }
                panel.Controls.Add(rowElements[i]);
                panel.SetCellPosition(rowElements[i], new TableLayoutPanelCellPosition(i, panel.RowCount - 1));
                if (i == rowElements.Count - 2)
                    panel.SetColumnSpan(rowElements[i], 2);
            }
            if (panel.RowCount == 1)
            {
                panel.RowCount++;
            }
        }

        public void creationDeschamps()
        {
            // Bind combobox to dictionary
            Dictionary<string, string> cbxTxtDictionnary = new Dictionary<string, string>();
            cbxTxtDictionnary.Add("1", "=");
            cbxTxtDictionnary.Add("2", "Est différent...");
            cbxTxtDictionnary.Add("3", "Débute par...");
            cbxTxtDictionnary.Add("4", "Se termine par...");
            cbxTxtDictionnary.Add("5", "Contient...");
            cbxTxtDictionnary.Add("6", "Ne contient pas...");
            int groupBox2Height = columns.Count * 35;


            foreach (KeyValuePair<string, string[]> entry in columns)
            {
                // Input Label Handler
                List<Control> p = new List<Control>();
                List<FlowLayoutPanel> rowElements = new List<FlowLayoutPanel>();
                //p.Anchor = AnchorStyles.Right;
                //p.Size = new Size(groupBox2.Width, groupBox2Height / columns.Count);


                Label lbl = new Label();
                lbl.Text = entry.Value[1];
                lbl.ForeColor = Color.Black;

                p.Add(lbl);
                switch (entry.Value[0])
                {
                    case "datetime":
                        ComboBox cbx = new ComboBox();
                        cbx.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbx.Name = "cbx" + entry.Key;
                        cbx.Dock = DockStyle.Fill;
                        cbx.BackColor = Color.White;
                        cbx.Items.Add("=");
                        cbx.Items.Add(">");
                        cbx.Items.Add("<");
                        cbx.Items.Add(">=");
                        cbx.Items.Add("<=");
                        cbx.SelectedIndex = 0;
                        p.Add(cbx);

                        UltraDateTimeEditor udp = new UltraDateTimeEditor();
                        udp.Value = null;
                        udp.Name = "udp" + entry.Key;
                        udp.Anchor = AnchorStyles.Right;
                        udp.Dock = DockStyle.Fill;
                        udp.DisplayStyle = EmbeddableElementDisplayStyle.ScenicRibbon;
                        udp.Width = 120;
                        udp.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(udp);

                        Label errorDate = new Label();
                        errorDate.Name = "date" + entry.Key;
                        p.Add(errorDate);
                        break;
                    case "real":
                        ComboBox cbxReal = new ComboBox();
                        cbxReal.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxReal.Name = "real" + entry.Key;
                        cbxReal.Dock = DockStyle.Fill;
                        cbxReal.Items.Add("=");
                        cbxReal.Items.Add(">");
                        cbxReal.Items.Add("<");
                        cbxReal.Items.Add(">=");
                        cbxReal.Items.Add("<=");
                        cbxReal.SelectedIndex = 0;

                        p.Add(cbxReal);

                        iTalk.iTalk_TextBox_Small2 realBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        realBoxVarchar.BackColor = Color.White;
                        realBoxVarchar.Name = entry.Key;
                        realBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        realBoxVarchar.Dock = DockStyle.Fill;
                        realBoxVarchar.Width = 120;

                        realBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);
                        realBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(realBoxVarchar);

                        Label errorReal = new Label();
                        errorReal.ForeColor = System.Drawing.Color.Red;

                        errorReal.Name = "realLbl" + entry.Key;
                        p.Add(errorReal);

                        break;
                    case "ntext":
                    case "nvarchar":
                        ComboBox cbxText = new ComboBox();
                        cbxText.DropDownWidth = 100;
                        cbxText.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxText.Dock = DockStyle.Fill;
                        cbxText.Name = "txt" + entry.Key;
                        cbxText.Items.Add("=");
                        cbxText.Items.Add("Est différent...");
                        cbxText.Items.Add("Débute par...");
                        cbxText.Items.Add("Se termine par...");
                        cbxText.Items.Add("Contient...");
                        cbxText.Items.Add("Ne contient pas...");
                        cbxText.SelectedIndex = 4;

                        p.Add(cbxText);

                        iTalk.iTalk_TextBox_Small2 txtBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        txtBoxVarchar.BackColor = Color.White;
                        txtBoxVarchar.Name = entry.Key;
                        txtBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        txtBoxVarchar.Dock = DockStyle.Fill;
                        txtBoxVarchar.Width = 120;

                        txtBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);
                        txtBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };

                        p.Add(txtBoxVarchar);

                        Label errorChar = new Label();
                        errorChar.Name = "Str" + entry.Key;

                        p.Add(errorChar);
                        //cbxText.SelectedItem = cbxText.Items.Count;
                        break;

                    case "Float":
                        ComboBox cbxFloat = new ComboBox();
                        cbxFloat.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxFloat.Name = "float" + entry.Key;
                        cbxFloat.Dock = DockStyle.Fill;
                        cbxFloat.Items.Add("=");
                        cbxFloat.Items.Add(">");
                        cbxFloat.Items.Add("<");
                        cbxFloat.Items.Add(">=");
                        cbxFloat.Items.Add("<=");
                        cbxFloat.SelectedIndex = 0;

                        p.Add(cbxFloat);

                        iTalk.iTalk_TextBox_Small2 floatBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        floatBoxVarchar.BackColor = Color.White;
                        floatBoxVarchar.Name = entry.Key;
                        floatBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        floatBoxVarchar.Dock = DockStyle.Fill;
                        floatBoxVarchar.Width = 120;
                        floatBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);
                        floatBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(floatBoxVarchar);

                        Label errorFloat = new Label();
                        errorFloat.ForeColor = System.Drawing.Color.Red;

                        errorFloat.Name = "floatLbl" + entry.Key;
                        p.Add(errorFloat);

                        break;

                    case "int":
                        ComboBox cbxInt = new ComboBox();
                        cbxInt.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxInt.Name = "Int" + entry.Key;
                        cbxInt.Dock = DockStyle.Fill;
                        cbxInt.Items.Add("=");
                        cbxInt.Items.Add(">");
                        cbxInt.Items.Add("<");
                        cbxInt.Items.Add(">=");
                        cbxInt.Items.Add("<=");
                        cbxInt.SelectedIndex = 0;

                        p.Add(cbxInt);

                        iTalk.iTalk_TextBox_Small2 intBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        intBoxVarchar.BackColor = Color.White;
                        intBoxVarchar.Name = entry.Key;
                        intBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        intBoxVarchar.Dock = DockStyle.Fill;
                        intBoxVarchar.Width = 120;
                        intBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);

                        intBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(intBoxVarchar);

                        Label errorNbr = new Label();
                        errorNbr.Width = 200;
                        errorNbr.ForeColor = System.Drawing.Color.Red;
                        errorNbr.Name = "IntLbl" + entry.Key;
                        p.Add(errorNbr);
                        break;
                    case "decimal":
                        ComboBox cbxdecimal = new ComboBox();
                        cbxdecimal.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxdecimal.Name = "decimal" + entry.Key;
                        cbxdecimal.Dock = DockStyle.Fill;
                        cbxdecimal.Items.Add("=");
                        cbxdecimal.Items.Add(">");
                        cbxdecimal.Items.Add("<");
                        cbxdecimal.Items.Add(">=");
                        cbxdecimal.Items.Add("<=");
                        cbxdecimal.SelectedIndex = 0;

                        p.Add(cbxdecimal);

                        iTalk.iTalk_TextBox_Small2 decimalBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        decimalBoxVarchar.BackColor = Color.White;
                        decimalBoxVarchar.Name = entry.Key;
                        decimalBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        decimalBoxVarchar.Dock = DockStyle.Fill;
                        decimalBoxVarchar.Width = 92;

                        decimalBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);

                        decimalBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(decimalBoxVarchar);

                        Label errordecimal = new Label();
                        errordecimal.Width = 200;
                        errordecimal.ForeColor = System.Drawing.Color.Red;
                        errordecimal.Name = "decimalLbl" + entry.Key;
                        p.Add(errordecimal);
                        break;
                    case "smallint":
                        ComboBox cbxSmallInt = new ComboBox();
                        cbxSmallInt.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxSmallInt.Name = "smallInt" + entry.Key;
                        cbxSmallInt.Dock = DockStyle.Fill;
                        cbxSmallInt.Items.Add("=");
                        cbxSmallInt.Items.Add(">");
                        cbxSmallInt.Items.Add("<");
                        cbxSmallInt.Items.Add(">=");
                        cbxSmallInt.Items.Add("<=");
                        cbxSmallInt.SelectedIndex = 0;

                        p.Add(cbxSmallInt);

                        iTalk.iTalk_TextBox_Small2 intSmallBoxVarchar = new iTalk.iTalk_TextBox_Small2();
                        intSmallBoxVarchar.BackColor = Color.White;
                        intSmallBoxVarchar.Name = entry.Key;
                        intSmallBoxVarchar.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        intSmallBoxVarchar.Dock = DockStyle.Fill;
                        intSmallBoxVarchar.Width = 120;
                        intSmallBoxVarchar.KeyPress += new KeyPressEventHandler(keyManager);

                        intSmallBoxVarchar.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };
                        p.Add(intSmallBoxVarchar);

                        Label errorNbrsmall = new Label();
                        errorNbrsmall.Width = 200;
                        errorNbrsmall.ForeColor = System.Drawing.Color.Red;
                        errorNbrsmall.Name = "IntSmallLbl" + entry.Key;
                        p.Add(errorNbrsmall);
                        break;

                    //this Lines Added by Mohammed 
                    // Staaart
                    case "varchar":
                        ComboBox cbxText1 = new ComboBox();
                        cbxText1.DropDownWidth = 100;
                        cbxText1.DropDownStyle = ComboBoxStyle.DropDownList;
                        cbxText1.Dock = DockStyle.Fill;
                        cbxText1.Name = "txt" + entry.Key;
                        cbxText1.Items.Add("=");
                        cbxText1.Items.Add("Est différent...");
                        cbxText1.Items.Add("Débute par...");
                        cbxText1.Items.Add("Se termine par...");
                        cbxText1.Items.Add("Contient...");
                        cbxText1.Items.Add("Ne contient pas...");
                        cbxText1.SelectedIndex = 4;

                        p.Add(cbxText1);

                        iTalk.iTalk_TextBox_Small2 txtBoxVarchar1 = new iTalk.iTalk_TextBox_Small2();
                        txtBoxVarchar1.BackColor = Color.White;
                        txtBoxVarchar1.Name = entry.Key;
                        txtBoxVarchar1.Anchor = AnchorStyles.Left & AnchorStyles.Right;
                        txtBoxVarchar1.Dock = DockStyle.Fill;
                        txtBoxVarchar1.Width = 120;
                        txtBoxVarchar1.KeyPress += new KeyPressEventHandler(keyManager);
                        txtBoxVarchar1.KeyDown += delegate (object sender, KeyEventArgs e)
                        {
                            if (e.KeyCode == Keys.Enter)
                            {
                                clearCritere();
                                rechercher();
                            }
                        };

                        p.Add(txtBoxVarchar1);

                        Label errorVarChar = new Label();
                        errorVarChar.Name = "Str" + entry.Key;

                        p.Add(errorVarChar);
                        //cbxText.SelectedItem = cbxText.Items.Count;
                        break;
                    // END

                    default:
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Type Incorrect");

                        break;
                }
                AddRowToPanel(criteres, p);

            }
        }
        private void HandleTextBoxKeyDownEvent(object sender, KeyEventArgs e)
        {
            clearCritere();
            rechercher();
        }
        private void btnRechercher_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            clearCritere();
            Page = 1;
            //AxeGrid = new AxeGrid(true);
            DataTableResult = new DataTable();
            rechercher();
            if (ugResultat.Rows.Count == 0)
                loopTextBoxToSetFocus(criteres);
            else
                ugResultat.Focus();
            Cursor.Current = Cursors.Default;
        }
        private void loopTextBoxToSetFocus(Control control)
        {
            foreach (Control cc in control.Controls)
            {
                loopTextBoxToSetFocus(cc);
            }
            if (control is iTalk.iTalk_TextBox_Small2)
            {
                var italk = control as iTalk.iTalk_TextBox_Small2;
                if (italk.Text != "")
                {
                    //italk.Focus();
                    italk.Select();
                    //italk.iTalkTB.Select(0, 0);
                    italk.iTalkTB.SelectionStart = italk.Text.Length;
                }
            }
        }
        public void rechercher()
        {
            //AxeGrid = new AxeGrid(true);
            // declare the SqlDataReader, which is used in
            // both the try block and the finally block

            SqlConnection conn = Connection;
            SqlDataAdapter adapter = new SqlDataAdapter();
            try
            {
                // create a command object open the connection

                SqlCommand cmd = new SqlCommand(requete, conn);
                var _Where = "";
                // set params
                foreach (KeyValuePair<string, string[]> entry in columns)
                {
                    switch (entry.Value[0])
                    {
                        case "datetime":
                            if ((Controls.Find("udp" + entry.Key, true)[0] as UltraDateTimeEditor).Value != null)
                            {
                                DateTime date = (DateTime)(Controls.Find("udp" + entry.Key, true)[0] as UltraDateTimeEditor).Value;
                                String operateur = (this.Controls.Find("cbx" + entry.Key, true)[0] as ComboBox).Text;
                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateur, date, "and", "", ""));
                                //===> Mondir le 05.03.2021, sometime the date contains the time, so the equal will not work, so for datetime type, i remove the time from the datetime
                                //===> to fix : #2282
                                //===> line bellow is the old line
                                //===> Where += $"{entry.Value[2]}.{entry.Key} {operateur} '{date}' and ";
                                _Where += $" CONVERT(DATE, {entry.Value[2]}.{entry.Key}) {operateur} '{date}' and ";
                                //===> Fin Modif Mondir
                            }
                            break;

                        case "Float":
                            String floatBox = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text;
                            (Controls.Find("floatLbl" + entry.Key, true)[0] as Label).Text = "";

                            String operateurFloatBox = (this.Controls.Find("float" + entry.Key, true)[0] as ComboBox).Text;
                            if (floatBox.Trim() != "")
                            {
                                float param;
                                if (!float.TryParse(floatBox, out param) && param < 0)
                                {
                                    if (Controls.Find("floatLbl" + entry.Key, true).Count() > 0)
                                    {
                                        (Controls.Find("floatLbl" + entry.Key, true)[0] as Label).Text = "Uniquement des chiffres décimaux";
                                        return;
                                    }
                                }

                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateurFloatBox, param, "and", "", ""));
                                _Where += $"{entry.Value[2]} . {entry.Key} {operateurFloatBox} {param} and ";
                            }
                            break;
                        case "real":

                            String realBox = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text;
                            //(Controls.Find("realLbl" + entry.Key, true)[0] as Label).Text = "";
                            String operateurRealBox = (this.Controls.Find("real" + entry.Key, true)[0] as ComboBox).Text;
                            if (realBox.Trim() != "")
                            {
                                double param;
                                if (!double.TryParse(realBox, out param))
                                {
                                    if (Controls.Find("realLbl" + entry.Key, true).Count() > 0)
                                    {
                                        (Controls.Find("realLbl" + entry.Key, true)[0] as Label).Text = "Uniquement des chiffres entiers";

                                        return;
                                    }
                                }
                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateurRealBox, param, "and", "", ""));
                                _Where += $"{entry.Value[2]} . {entry.Key} {operateurRealBox} {param} and ";
                            }
                            break;
                        case "int":

                            String intBox = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text;
                            //(Controls.Find("IntLbl" + entry.Key, true)[0] as Label).Text = "";
                            String operateurIntBox = (this.Controls.Find("Int" + entry.Key, true)[0] as ComboBox).Text;
                            if (intBox.Trim() != "")
                            {
                                int param;
                                if (!int.TryParse(intBox, out param))
                                {
                                    if (Controls.Find("IntLbl" + entry.Key, true).Count() > 0)
                                    {
                                        (Controls.Find("IntLbl" + entry.Key, true)[0] as Label).Text = "Uniquement des chiffres entiers";
                                        return;
                                    }
                                }
                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateurIntBox, param, "and", "", ""));
                                _Where += $"{entry.Value[2]} . {entry.Key} {operateurIntBox} {param} and ";
                            }
                            break;
                        case "smallint":

                            String SmallintBox = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text;
                            //(Controls.Find("IntSmallLbl" + entry.Key, true)[0] as Label).Text = "";

                            String operateurSmallIntBox = (this.Controls.Find("smallInt" + entry.Key, true)[0] as ComboBox).Text;
                            if (SmallintBox.Trim() != "")
                            {
                                int param;
                                if (!int.TryParse(SmallintBox, out param))
                                {
                                    if (Controls.Find("IntSmallLbl" + entry.Key, true).Count() > 0)
                                    {
                                        (Controls.Find("IntSmallLbl" + entry.Key, true)[0] as Label).Text = "Uniquement des chiffres entiers";

                                        return;
                                    }
                                }
                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateurSmallIntBox, param, "and", "", ""));
                                _Where += $"{entry.Value[2]} . {entry.Key} {operateurSmallIntBox} {param} and ";
                            }
                            break;
                        case "decimal":

                            String decimalBox = (criteres.Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text;
                            //(criteres.Controls.Find("decimalLbl" + entry.Key, true)[0] as Label).Text = "";
                            String operateurdecimalBox = (this.Controls.Find("decimal" + entry.Key, true)[0] as ComboBox).Text;
                            if (decimalBox.Trim() != "")
                            {
                                int param;
                                if (!int.TryParse(decimalBox, out param))
                                {
                                    if (Controls.Find("decimalLbl" + entry.Key, true).Count() > 0)
                                    {
                                        (Controls.Find("decimalLbl" + entry.Key, true)[0] as Label).Text = "Uniquement des chiffres entiers";

                                        return;
                                    }

                                }
                                ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, operateurdecimalBox, param, "and", "", ""));
                                _Where += $"{entry.Value[2]} . {entry.Key} {operateurdecimalBox} {param} and ";
                            }
                            break;

                        case "ntext":
                        case "nvarchar":
                            String txtBox = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text.Replace("'", "''");
                            //  String operateurTxtBox = (this.Controls.Find("txt" + entry.Key, true)[0] as ComboBox).Text;
                            //string value = (Convert.ToInt32(((KeyValuePair<string, string>)(this.Controls.Find("txt" + entry.Key, true)[0] as ComboBox).SelectedItem).Key) + 1).ToString();
                            string value = (this.Controls.Find("txt" + entry.Key, true)[0] as ComboBox).SelectedIndex.ToString();
                            if (txtBox.Trim() != "")
                            {
                                switch (value)
                                {
                                    case "0":
                                        //=
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", txtBox, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '{txtBox}' and ";
                                        break;

                                    case "1":
                                        //Est différent
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "<>", txtBox, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} <> '{txtBox}' and ";
                                        break;

                                    case "2":
                                        //Commence par
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", txtBox + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '{txtBox}%' and ";
                                        break;

                                    case "3":
                                        //Se termine par
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", "%" + txtBox, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '%{txtBox}' and ";
                                        break;

                                    case "4":
                                        //Contient
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", "%" + txtBox + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '%{txtBox}%' and ";
                                        break;

                                    case "5":
                                        //Ne contient pas
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "NOT LIKE", "%" + txtBox + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} NOT LIKE '%{txtBox}%' and ";
                                        break;
                                }

                            }
                            break;


                        //These Lines Added By Mohammed
                        // BEGIN

                        case "varchar":
                            String txtBoxVarchar = (Controls.Find(entry.Key, true)[0] as iTalk.iTalk_TextBox_Small2).Text.Replace("'", "''");
                            string val = (this.Controls.Find("txt" + entry.Key, true)[0] as ComboBox).SelectedIndex.ToString();
                            if (txtBoxVarchar.Trim() != "")
                            {
                                switch (val)
                                {
                                    case "0":
                                        //=
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", txtBoxVarchar, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '{txtBoxVarchar}' and ";
                                        break;

                                    case "1":
                                        //Est différent
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "<>", txtBoxVarchar, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} <> '{txtBoxVarchar}' and ";
                                        break;

                                    case "2":
                                        //Commence par
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", txtBoxVarchar + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '{txtBoxVarchar}%' and ";
                                        break;

                                    case "3":
                                        //Se termine par
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", "%" + txtBoxVarchar, "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '%{txtBoxVarchar}' and ";
                                        break;

                                    case "4":
                                        //Contient
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "LIKE", "%" + txtBoxVarchar + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} LIKE '%{txtBoxVarchar}%' and ";
                                        break;

                                    case "5":
                                        //Ne contient pas
                                        ctrs.Add(new Critere(entry.Value[2] + "." + entry.Key, "NOT LIKE", "%" + txtBoxVarchar + "%", "and", "", ""));
                                        _Where += $"{entry.Value[2]} . {entry.Key} NOT LIKE '%{txtBoxVarchar}%' and ";
                                        break;
                                }
                            }
                            break;
                        //end

                        default:
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Incorrect Type");
                            break;
                    }
                }

                Console.WriteLine(requete + " " + Critere.getQueryCritere(ctrs));
                SqlCommand selectCommand;
                String laison = "";
                if (ctrs.Count == 0)
                {
                    var reqFinal = "";
                    if (where != "")
                    {
                        laison = " where " + where;
                        reqFinal = requete + " " + laison;
                    }
                    else
                    {
                        reqFinal = requete;
                    }



                    //selectCommand = new SqlCommand(requete + " " + laison, GetConnection.conn);

                    //AxeGrid.Start(ugResultat, reqFinal, OrderBy != "" ? OrderBy : FirstColumn, Page, PageSize, false);
                    //selectCommand = new SqlCommand("Pagination", GetConnection.conn);
                    //selectCommand.Parameters.AddWithValue("Query", requete);
                    //selectCommand.Parameters.AddWithValue("OrderBy", OrderBy != "" ? OrderBy : FirstColumn);
                    //selectCommand.Parameters.AddWithValue("PageSize", PageSize);
                    //selectCommand.Parameters.AddWithValue("Page", Page);

                    var result = ModAdo.fc_OpenRecordSet(reqFinal);
                    ugResultat.DataSource = result;
                    ugResultat.Text = $"{result.Rows.Count} Enrg.";

                }
                else
                {
                    laison = requete;
                    if (where != "") laison += " where " + where;

                    if (laison.Contains("where"))
                        laison += " and " + _Where;
                    else if (_Where != "") laison += " where " + _Where;

                    if (laison.Substring(laison.Length - 4) == "and ")
                        laison = laison.Substring(0, laison.Length - 4);

                    //selectCommand = new SqlCommand(requete + " " + Critere.getQueryCritere(ctrs) + " " + laison, GetConnection.conn);


                    //AxeGrid.Start(ugResultat, laison, OrderBy != "" ? OrderBy : FirstColumn, Page, PageSize, false);
                    //selectCommand = new SqlCommand("Pagination", GetConnection.conn);
                    //selectCommand.Parameters.AddWithValue("Query", laison);
                    //selectCommand.Parameters.AddWithValue("OrderBy", OrderBy != "" ? OrderBy : FirstColumn);
                    //selectCommand.Parameters.AddWithValue("PageSize", PageSize);
                    //selectCommand.Parameters.AddWithValue("Page", Page);

                    var result = ModAdo.fc_OpenRecordSet(laison);
                    ugResultat.DataSource = result;
                    ugResultat.Text = $"{result.Rows.Count} Enrg.";

                }
                // SqlCommand selectCommand = new SqlCommand(requete + " " + Critere.getQueryCritere(ctrs), GetConnection.conn);
                //Critere.addParametersToQuery(ref selectCommand, ctrs);

                //string req = selectCommand.CommandText;
                //selectCommand.CommandType = CommandType.StoredProcedure;

                //adapter.SelectCommand = selectCommand;
                //DataSet dsResultat = new DataSet();
                //string query = selectCommand.CommandText;

                //adapter.Fill(dsResultat, "resultat");
                //adapter.Fill(DataTableResult);
                //int dsSize = ugResultat.Rows.Count;
                //ugResultat.Text = dsSize.ToString() + lblGridResultat;

                //if (dsSize > 0)
                //{
                //ugResultat.DataSource = DataTableResult;
                //ugResultat.ActiveRowScrollRegion.ScrollPosition = FirstDataSource ? 0 : PageSize * Page - 1;
                //}
                //if (ugResultat.Rows.Count > 0)
                //{
                //    ugResultat.Rows[0].Selected = true;
                //    ugResultat.Focus();
                //}
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void clearCritere()
        {
            ctrs.Clear();
        }

        private void ugResultat_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            // e.Row.Appearance.BackColor = Color.Red;
        }

        private void keyManager(Object o, KeyPressEventArgs e)
        {
            // The keypressed method uses the KeyChar property to check
            // whether the ENTER key is pressed.

            // If the ENTER key is pressed, the Handled property is set to true,
            // to indicate the event is handled.
            if (e.KeyChar == (char)Keys.Return)
            {
                btnRechercher_Click(o, e);
            }
        }

        private void ugResultat_AfterRowFilterChanged(object sender, AfterRowFilterChangedEventArgs e)
        {
            ugResultat.Text = e.Rows.FilteredInRowCount.ToString() + lblGridResultat;
        }

        private void btnExporter_Click(object sender, EventArgs e)
        {
            try
            {
                //===> Mondir le 12.03.2021, demande de Rachid
                using (var tmpModAdo = new ModAdo())
                {
                    var sSQL = "SELECT     AUT_Formulaire, AUT_Nom" +
                               " From AUT_Autorisation" +
                               " WHERE  AUT_Nom = '" + General.fncUserName() +
                               "' and    AUT_Objet = 'ExportExcelRecherche'";
                    var dt = tmpModAdo.fc_OpenRecordSet(sSQL);
                    if (dt.Rows.Count == 0)
                    {
                        CustomMessageBox.Show("Navigation annulée, Vous n'avez pas les autorisations nécessaires pour exporter ces données sous excel.", "Export annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //===> Fin Modif Mondir


                var SFD = new SaveFileDialog { FileName = this.Text + ".xls", Filter = "Fichier Excel (*.xls)|*.xls" };
                var R = SFD.ShowDialog();
                if (R == DialogResult.OK)
                {
                    SaveToExcel(SFD.FileName);
                    Process.Start(SFD.FileName);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " Fiche FicheDispatchDevis Erreur : " + ex.Message);
            }
        }
        //private Task SaveToExcelAsync(string FileName) => Task.Factory.StartNew(() => SaveToExcel(FileName));
        private void SaveToExcel(string FileName)
        {
            int countRow = Convert.ToInt32(ugResultat.Text.Substring(0, ugResultat.Text.Length - 5));
            int maxRowRangeExcel = 65536, counter = 0;// the max row in excel
            if (countRow <= 65536)
            {
                Cursor = Cursors.WaitCursor;
                ultraGridExcelExporter1.Export(ugResultat, FileName);
                Cursor = Cursors.Arrow;
                return;
            }
            Workbook w = new Workbook();//fichier excel
            this.ultraGridExcelExporter1.FileLimitBehaviour = Infragistics.Win.UltraWinGrid.ExcelExport.FileLimitBehaviour.TruncateData;
            int i = 0;
            var dt = (DataTable)ugResultat.DataSource;
            while (counter < countRow)
            {
                Cursor = Cursors.WaitCursor;
                gridToExport.DataSource = dt.Rows.Cast<DataRow>().Skip(counter).Take(maxRowRangeExcel).CopyToDataTable();
                var workSheet = w.Worksheets.Add("feuille " + (i + 1));// la feuille 
                this.ultraGridExcelExporter1.Export(gridToExport, w.Worksheets[i]);
                Cursor = Cursors.Arrow;
                i++;
                counter = counter + maxRowRangeExcel;
            }
            Cursor = Cursors.Arrow;
            dt = null;
            w.Save(FileName);
            w = null;
            gridToExport = null;
        }



        public void SetValues(Dictionary<string, string> DictionaryOfValues)
        {
            foreach (var D in DictionaryOfValues)
            {

                foreach (Control CC in criteres.Controls)
                    if ((CC is iTalk.iTalk_TextBox_Small2 || CC is TextBox) && CC.Name.ToUpper() == (D.Key).ToUpper())
                        CC.Text = D.Value;

                    else if (CC is UltraDateTimeEditor && CC.Name.ToUpper() == ("udp" + D.Key).ToUpper())
                        ((UltraDateTimeEditor)CC).Value = Convert.ToDateTime(D.Value);
            }

        }
        bool firstTime = true;
        private void SearchTemplate_Activated(object sender, EventArgs e)
        {
            if (firstTime)
            {
                firstTime = false;
                /// Mohammed added this condition because this event always fires when user click on search Template Form and that made the operation of search more slower specially when they search about " fiche Appel ".
                btnRechercher_Click(sender, e);
                if (ugResultat.Rows.Count > 0)
                    ugResultat.Focus();
                else
                {
                    ///this code added to set focus of the first textBox.
                    if (criteres.Controls.Count >= 3 && ugResultat.Rows.Count > 0)
                    {
                        var control = criteres.Controls[2];
                        if (control != null && control is iTalk.iTalk_TextBox_Small2)
                        {
                            control.Focus();
                        }
                    }
                }
            }

            foreach (Control C in Controls)
            {
                if (C is UltraTextEditor)
                    ((UltraTextEditor)C).UseAppStyling = false;
                C.Refresh();
                C.Invalidate();
            }
            if (ugResultat.Rows.Count == 0)
            {
                //RecControls(this);
            }
            else
            {
                //ugResultat.Focus();
            }

        }

        private void ugResultat_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            if (!e.Layout.Bands[0].Columns.Cast<UltraGridColumn>().Any(col => col.Key.ToUpper() == "Hidden".ToUpper()))
            {
                e.Layout.Bands[0].Columns.Insert(0, "Hidden");
                e.Layout.Bands[0].Columns[0].Hidden = true;
            }
        }

        private void SearchTemplate_SizeChanged(object sender, EventArgs e)
        {

        }

        private void SearchTemplate_FormClosed(object sender, FormClosedEventArgs e)
        {
            General.blControle = false;
            var height = this.Height;
            var width = this.Width;
            General.saveInReg(RegFolder, RegFormWidth, width.ToString());
            General.saveInReg(RegFolder, RegFormHeight, height.ToString());
        }

    }
}