﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CrystalDecisions.CrystalReports.Engine;

namespace Axe_interDT.Views.SharedViews
{
    public partial class CrystalReportFormView : Form
    {
        public CrystalReportFormView()
        {
            InitializeComponent();
        }
        ReportDocument report;
        public CrystalReportFormView(ReportDocument Report, string SelectionFormula = "") : this()
        {
            report = Report;
            CrystalReportViewer.ReportSource = Report;
            if (!string.IsNullOrWhiteSpace(SelectionFormula))
                CrystalReportViewer.SelectionFormula = SelectionFormula;
        }

        private void CrystalReportFormView_FormClosing(object sender, FormClosingEventArgs e)
        {
            //Added by mohammed 02.07.2020 to close and dispose report document 
            report?.Close();
            report?.Dispose();
            report = null;
            GC.Collect();
        }
    }
}
