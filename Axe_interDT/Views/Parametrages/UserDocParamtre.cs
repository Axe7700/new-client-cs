﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Parametrages.ChargePersonnel;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Parametrages
{
    public partial class UserDocParamtre : UserControl
    {
        const short cQualite = 0;
        const short cPersonnel = 1;
        const short cTypeOperation = 2;
        const short cStatutInterv = 3;
        const short cTransmission = 4;
        const short cStatutCommande = 5;
        const short cAnalytique = 6;
        const short cCodeEtatDevis = 7;
        const short cCONTRAT = 8;
        const short cTypeFacture = 0;
        const short cIndice = 1;
        const short cTypeRatio = 2;
        const short cFormule = 3;
        const short cParametres = 9;
        const short cAchatsVentes = 10;
        const short cBE = 11;
        const short cConccurent = 12;
        const short ctomtom = 14;
        const short cAnalySecteur = 15;

        const short cPrime = 17;
        const short cVehicule = 18;
        const short cMobilite = 19;
        const short cAnalySectCtr = 20;
        const short cUtilisateur = 21;
        const short cModeReglement = 22;
        const short cModeMomentJour = 23;
        const short cPeriodeFact = 24;
        const short cTypeDevis = 25;
        const short cTypeEnc = 26;
        //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        const short cTypeCat = 27;
        //===> Fin Modif Mondir

        DataTable rsCAICategoriInterv;
        DataTable rsFacArticle;
        DataTable rsQualite;
        DataTable rsPersonnel;
        DataTable rsStatutInterv;
        DataTable rsTransmission;
        DataTable rsStatutCommande;
        DataTable rsAnaActivite;
        DataTable rsAnaCode;
        DataTable rsSecteur;
        DataTable rsSpecifQualif;
        DataTable rsCodeEtatDevis;
        DataTable rsTypeFacture;
        DataTable rsIndice;
        DataTable rsTypeRatio;
        DataTable rsFormule;
        DataTable rsParametres;
        DataTable rsAchatsVentes;
        DataTable rsBE;
        DataTable rsCNC_Conccurent;
        DataTable rsGroupe;
        DataTable rsTomtom;
        DataTable rsAnalySecteur;
        DataTable rsUtilClim;
        DataTable rsSrvCpte;
        DataTable rsPrime;
        DataTable rsARV;
        DataTable rsPRV;
        DataTable rsFLO;
        DataTable rsAnalySectCtr;
        DataTable rsUtilWin;
        DataTable rsREP;
        DataTable rsTypeDev;
        DataTable rsMOM;
        DataTable rsPCF;
        DataTable rsTypeEnc;

        //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        private DataTable rsCat;
        private ModAdo rsCatModAdo;
        //===> Fin Modif Mondir

        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cNiv3 = "C";

        string sIDService;

        Boolean insertVar = false;
        ModAdo tmpAdorsSrvCpte;
        ModAdo tmpAdorsAchatsVentes;
        ModAdo tmpAdorsGraphique;
        ModAdo tmpAdorsQualite;
        ModAdo tmpAdorsSpecifQualif;
        ModAdo tmpAdorsCodeEtatDevis;
        ModAdo tmpAdorsPersonnel;
        ModAdo tmpAdorsCAICategoriInterv;
        ModAdo tmpAdorsStatutInterv;
        ModAdo tmpAdorsTransmission;
        ModAdo tmpAdorsStatutCommande;
        ModAdo tmpAdorsTypeFacture;
        SqlDataAdapter SDArsAnaActivite;
        SqlCommandBuilder SCBrsAnaActivite;
        SqlCommand cmd;
        ModAdo tmpAdorsIndice;
        ModAdo tmpAdorsTypeRatio;
        ModAdo tmpAdorsFormule;
        ModAdo tmpAdorsParametres;
        ModAdo tmpAdorsBE;
        ModAdo tmpAdorsCNC_Conccurent;
        ModAdo tmpAdorsTomtom;
        ModAdo tmpAdorsUtilClim;
        ModAdo tmpAdorsPrime;
        ModAdo tmpAdorsARV;
        ModAdo tmpAdorsPRV;
        ModAdo tmpAdorsFLO;
        ModAdo tmpAdorsAnalySectCtr;
        ModAdo tmpAdorsUtilWin;
        ModAdo rsREPModAdo;
        ModAdo rsMOMModAdo;
        ModAdo rsPCFModAdo;
        ModAdo rsTypeDevModAdo;
        ModAdo rsTypeEncModAdo;

        System.Windows.Forms.ToolTip tooltip = new System.Windows.Forms.ToolTip();
        Point? clickPosition = null;
        public UserDocParamtre()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadModeReglement()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     REP_CODE, REP_Designation" + " From REP_ReglementPDA " + " ORDER BY REP_CODE";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                rsREP = new DataTable();
                rsREPModAdo = new ModAdo();
                rsREP = rsREPModAdo.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                this.GridRep.DataSource = rsREP;
                GridRep.UpdateData();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadModeReglement");
            }

        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        private void fc_LoadCAT()
        {
            string sSQL = "";

            try
            {
                sSQL = "SELECT        CAT_Code, CAT_Libelle, CAT_TVA300"
                       + " From CAT_CategorieImmeuble "
                       + " ORDER BY CAT_Code";

                Cursor = Cursors.WaitCursor;

                rsCatModAdo = new ModAdo();
                rsCat = rsCatModAdo.fc_OpenRecordSet(sSQL);

                Cursor = Cursors.Default;

                GridCAT.DataSource = rsCat;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// teSTED
        /// </summary>
        private void fc_MomentJournee()
        {
            string sSQL = null;
            try
            {
                sSQL = "SELECT     TYMO_Noauto, TYMO_ID, TYMO_LIBELLE, TYMO_LibelleClient, TYMO_Heure "
                    + " From TYMO_TypeDeMoment "
                    + " ORDER BY TYMO_ID";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                rsMOM = new DataTable();
                rsMOMModAdo = new ModAdo();
                rsMOM = rsMOMModAdo.fc_OpenRecordSet(sSQL, GridMOM, "TYMO_Noauto");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                this.GridMOM.DataSource = rsMOM;
                GridMOM.UpdateData();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "; fc_MomentJournee");
            }

        }

        private void GridRep_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridRep.ActiveCell.Column.Index == GridRep.ActiveRow.Cells["REP_CODE"].Column.Index)
            {
                GridRep.ActiveRow.Cells["REP_CODE"].Value = GridRep.ActiveRow.Cells["REP_CODE"].Text.ToString().Trim();
            }
        }

        private void GridRep_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void chkMarqueurs_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMarqueurs.CheckState == 0)
            {

                GraphAchatVente.Series[0].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.None;
            }
            else
            {

                GraphAchatVente.Series[0].MarkerStyle = System.Windows.Forms.DataVisualization.Charting.MarkerStyle.Circle;

            }
        }

        private void chkMDP_CheckedChanged(object sender, EventArgs e)
        {
            if (chkMDP.CheckState == CheckState.Checked)//tested
            {
                chkLecture.Enabled = false;
                chkModification.Enabled = false;
                chkAjout.Enabled = false;
                chkSuppression.Enabled = false;
                txtFicheMDP.Visible = true;
                cmdAddDroit.Enabled = false;
            }
            else//tested
            {
                chkLecture.Enabled = true;
                chkModification.Enabled = true;
                chkAjout.Enabled = true;
                chkSuppression.Enabled = true;
                txtFicheMDP.Visible = false;
                cmdAddDroit.Enabled = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDROG_Nom_AfterCloseUp(object sender, EventArgs e)
        {
            fc_AffGroupe(cmbDROG_Nom.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDROG_Nom_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            sSQL = "SELECT  DROG_Nom, DROG_Drescription FROM   DROG_Groupe order by DROG_Nom";
            sheridan.InitialiseCombo(cmbDROG_Nom, sSQL, "DROG_Nom");//tested
            fc_AffGroupe(cmbDROG_Nom.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sGroupe"></param>
        private void fc_AffGroupe(string sGroupe)
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;

                //== controle si ce groupe existant.
                sSQL = "SELECT   DROG_Nom, DROG_Drescription FROM  DROG_Groupe " + " WHERE DROG_Nom ='" + sGroupe + "'";
                var tmpModAdo = new ModAdo();
                sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))//tested
                {
                    ListView2.Enabled = false;
                }
                else
                {
                    ListView2.Enabled = true;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = "SELECT     USR_No, USR_Nt, USR_Name, DROG_Nom"
                    + " From USR_Users "
                    + " WHERE  DROG_Nom ='" + sGroupe + "'";
                sSQL = sSQL + " ORDER BY USR_Name";

                var rstmpModAdo = new ModAdo();
                rstmp = rstmpModAdo.fc_OpenRecordSet(sSQL);

                /*TODO
                 * ListView2.ColumnHeaders.Clear();
                ListView2.ColumnHeaders.Add(, , "Utilisateur", Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsX(ListView1.Width) - 100);
                ListView2.BorderStyle = ComctlLib.BorderStyleConstants.ccFixedSingle;
                ListView2.View = ComctlLib.ListViewConstants.lvwReport;
                ListView2.ListItems.Clear();
                ListView2.LabelEdit = ComctlLib.ListLabelEditConstants.lvwManual;
                ListView2.MultiSelect = true;
                ListView2.HideSelection = false;
                */
                ListView2.Items.Clear();
                ListView2.HeaderStyle = ColumnHeaderStyle.Nonclickable;
                //ColumnHeader columnHeader1 = new ColumnHeader();
                // columnHeader1.Text = "Utilisateur";
                // columnHeader1.TextAlign = HorizontalAlignment.Left;
                //columnHeader1.Width = 100;
                // ListView2.Columns.Add(columnHeader1);
                // ListView2.View = System.Windows.Forms.View.List;
                ListView2.LabelEdit = true;
                ListView2.MultiSelect = true;
                ListView2.HideSelection = false;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rstmpRow in rstmp.Rows)//tested
                {

                    ListView2.Items.Add(cNiv1 + rstmpRow["USR_No"], rstmpRow["USR_Name"].ToString(), 1);
                }
                rstmp.Dispose();
                rstmp = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_AffGroupe");
            }
        }

        private void cmdAddDroit_Click(object sender, EventArgs e)
        {
            try
            {
                string sSQL = null;
                UltraTreeNode Node = null;
                string sDROF_NomFicheReel = null;
                string sDROG_Nom = null;
                DataTable rs = default(DataTable);
                int i = 0;
                int j = 0;

                foreach (UltraTreeNode iNode in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode child in iNode.Nodes)
                    {
                        if (child.Selected)//tested
                        {
                            sDROF_NomFicheReel = General.Mid(child.Parent.Key, 2, child.Parent.Key.Length - 1);

                            // sDROG_Nom = Mid(Node.Key, 2, Len(Node.Key) - 1)

                            i = child.Key.IndexOf("@");
                            j = child.Key.Length;
                            sDROG_Nom = General.Mid(child.Key, i + 1, j - i);
                            break;
                        }
                    }
                }

                sSQL = "SELECT     DROF_NomFicheReel, DROF_NoAuto,  "
                    + " DROG_Nom,  DROA_Lecture, DROA_Modif, DROA_Supp, DROA_Ajout"
                    + " From DROA_Administration"
                    + " WHERE     DROF_NomFicheReel ='" + sDROF_NomFicheReel + "'" + " AND DROG_Nom = '" + sDROG_Nom + "'";

                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    rs.Rows.Add();
                }
                var NewRow = rs.NewRow();
                NewRow["DROF_NomFicheReel"] = sDROF_NomFicheReel;
                NewRow["DROF_NoAuto"] = General.nz(tmpAdors.fc_ADOlibelle("SELECT DROF_NoAuto FROM DROF_DroitFiche "
                    + " where DROF_NomFicheReel= '" + sDROF_NomFicheReel + "'"), 0);//verifier
                NewRow["DROG_Nom"] = sDROG_Nom;
                NewRow["DROA_Lecture"] = chkLecture.CheckState;
                NewRow["DROA_Modif"] = chkModification.CheckState;
                NewRow["DROA_Supp"] = chkSuppression.CheckState;
                NewRow["DROA_Ajout"] = chkAjout.CheckState;
                rs.Rows.Add(NewRow);
                tmpAdors.Update();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdAddDroit_Click");
            }
        }

        private void cmdAddDroitFiche_Click(object sender, EventArgs e)
        {
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                string sDROF_NomFicheReel = null;
                UltraTreeNode Node = null;

                //== affectation par mot de passe.
                foreach (UltraTreeNode iNode in TreeView1.Nodes)
                {
                    if (iNode.Selected == true)
                    {
                        sDROF_NomFicheReel = General.Mid(iNode.Key, 2, iNode.Key.Length - 1);
                        break;
                    }
                }

                if (string.IsNullOrEmpty(txtFicheMDP.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Mot de passe obligatoire", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sSQL = "SELECT   DROF_Noauto, DROF_NomFicheReel, DROF_MDP, DROF_MdpOuUtil" + " From DROF_DroitFiche"
                    + " WHERE     (DROF_NomFicheReel = '" + sDROF_NomFicheReel + "')";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    rs.Rows[0]["DROF_MdpOuUtil"] = chkMDP.CheckState;
                    rs.Rows[0]["DROF_MDP"] = txtFicheMDP.Text;
                    tmpAdors.Update();
                }

                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdAddDroitFiche_Click");
            }
        }

        private void cmdAddUtil_Click(object sender, EventArgs e)
        {
            Forms.frmAjoutUtil frmAjoutUtil = new Forms.frmAjoutUtil();
            frmAjoutUtil.txtUSR_Nt.Text = "";
            frmAjoutUtil.txtUSR_Name.Text = "";
            frmAjoutUtil.txtDROG_Nom.Text = "";
            frmAjoutUtil.ShowDialog();
            fc_Utilisateur();
        }

        private void cmdAffichage_Click(object sender, EventArgs e)
        {
            try
            {
                //===> Mondir le 12.03.2021 changer le mot de pass, demanadé oar Fréd
                var aut = ModAutorisation.fc_DroitDetail("UserDocParamtre-cmdAddDroitFiche");
                if (aut == 0)
                {
                    return;
                }

                SSTab3.Visible = true;

                //===> Mondir le 12.03.2021, code bellow is no longer used, demande de Fred
                //string sMdp = null;
                //if (General.fncUserName().ToUpper().ToString() != "rachid abbouchi".ToUpper().ToString())
                //{
                //    sMdp = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir un mot de passe.", "Mot de passe");
                //    if (sMdp != "dt2012")
                //    {
                //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Mot de passe invalide, vous n'avez pas les autorisations nécessaires pour accéder à cette onglet.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //    }
                //    else
                //    {
                //        SSTab3.Visible = true;
                //    }
                //}
                //else
                //{
                //    SSTab3.Visible = true;
                //}
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdAffichage_Click");
            }
        }

        private void CmdAffUtil_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                int j = 0;
                string[] tsCle = new string[] { };
                int o = 0;
                bool bItem = false;
                var _with7 = ListView1;
                bool ss = _with7.FullRowSelect;

                if (ListView2.Enabled == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord sélectionner un groupe.", "Groupe inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (ListView2.SelectedIndices.Count > 0 && ListView1.SelectedIndices.Count == 0) return;
                //tsCle.Clear();
                o = 0;
                bItem = false;

                for (i = 0; i <= _with7.Items.Count; i++)
                {
                    if (_with7.SelectedItems[i].Selected)
                    {
                        bItem = true;
                        Array.Resize(ref tsCle, o + 1);
                        tsCle[o] = _with7.SelectedItems[i].Text;
                        o = o + 1;
                        break;
                    }
                }

                if (bItem == true)
                {
                    for (i = 0; i <= tsCle.Length - 1; i++)
                    {
                        for (j = 0; j <= _with7.Items.Count; j++)
                        {
                            if (_with7.Items[j].Text.ToString().ToUpper() == tsCle[i].ToString().ToUpper())
                            {
                                ListView2.Items.Add(_with7.Items[j].Text);//verifier
                                General.sSQL = "UPDATE USR_Users set DROG_Nom ='" + StdSQLchaine.gFr_DoublerQuote(cmbDROG_Nom.Text) + "'"
                                    + " WHERE USR_No =" + General.Mid(_with7.Items[j].Name, 2, _with7.Items[j].Name.Length - 1);
                                General.Execute(General.sSQL);
                                _with7.Items.Remove(_with7.Items[j]);
                                break;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "CmdAffUtil__Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCodeBE_Click(object sender, EventArgs e)
        {

            string req = null;
            string where = "";

            req = "SELECT BE.CodeBE AS \"Code\", BE.Libelle AS \"Libelle\", BE.Contact AS \"Contact\", BE.Tel AS \"Tel\", BE.Fax AS \"Fax\", "
                + " BE.eMail AS \"E-Mail\"" + " FROM BE  ";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un code" };
            fg.SetValues(new Dictionary<string, string> { { "BE.CodeBE", txtCodeBE.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                // charge les enregistrements de l'immeuble
                txtCodeBE.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                fc_ssGridBE();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    // charge les enregistrements de l'immeuble
                    txtCodeBE.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_ssGridBE();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdContactBE_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT BE.CodeBE AS \"Code\", BE.Libelle AS \"Libelle\", BE.Contact AS \"Contact\", BE.Tel AS \"Tel\", BE.Fax AS \"Fax\", "
                + " BE.eMail AS \"E-Mail\"" + " FROM BE  ";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un contact" };
            fg.SetValues(new Dictionary<string, string> { { "BE.Contact", txtContactBE.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtContactBE.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                fc_ssGridBE();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtContactBE.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fc_ssGridBE();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void cmdDesAffUtil_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                int j = 0;
                string[] tsCle = new string[] { };
                int o = 0;
                bool bItem = false;
                var _with10 = ListView2;

                if (ListView2.Enabled == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord sélectionner un groupe.", "Groupe inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (ListView2.SelectedIndices.Count == 0) return;
                // tsCle.Clone;
                o = 0;
                bItem = false;

                for (i = 0; i <= _with10.Items.Count; i++)
                {
                    if (_with10.SelectedItems[i].Selected)
                    {
                        bItem = true;
                        Array.Resize(ref tsCle, o + 1);
                        tsCle[o] = _with10.SelectedItems[i].Text;
                        o = o + 1;
                        break;
                    }


                }

                if (bItem == true)
                {
                    for (i = 0; i <= tsCle.Length - 1; i++)
                    {
                        for (j = 0; j <= _with10.Items.Count; j++)
                        {
                            if (_with10.Items[j].Text.ToString().ToUpper() == tsCle[i].ToString().ToUpper())
                            {
                                ListView1.Items.Add(_with10.Items[j].Text);
                                General.sSQL = "UPDATE USR_Users set DROG_Nom =NULL "
                                     + " WHERE USR_No =" + General.Mid(ListView2.Items[j].Name, 2, ListView2.Items[j].Name.Length - 1);
                                General.Execute(General.sSQL);
                                _with10.Items.Remove(_with10.Items[j]);
                                break; // TODO: might not be correct. Was : Exit For
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdDesAffUtil_Click");
            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLibelleBE_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT BE.CodeBE AS \"Code\", BE.Libelle AS \"Libelle\", BE.Contact AS \"Contact\", BE.Tel AS \"Tel\", BE.Fax AS \"Fax\", "
                + " BE.eMail AS \"E-Mail\"" + " FROM BE  ";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un libelle" };
            fg.SetValues(new Dictionary<string, string> { { "BE.Libelle", txtLibelleBE.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtLibelleBE.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                fc_ssGridBE();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtLibelleBE.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fc_ssGridBE();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            fc_sauver();
            View.Theme.Theme.AfficheAlertSucces();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_sauver()
        {

            var _with12 = this;
            switch (encaissement.SelectedTab.Index)
            {
                case cQualite:
                    _with12.ssGridQualite.UpdateData();
                    _with12.GridSpecifQualif.UpdateData();
                    break;
                case cPersonnel:
                    _with12.ssGridPersonnel.UpdateData();
                    break;
                case cTypeOperation:
                    _with12.ssCAI_CategoriInterv.UpdateData();
                    _with12.ssFacArticle.UpdateData();
                    break;
                //            Case cTypeOperation
                //                    .ssCAI_CategoriInterv.Update
                //                    .ssFacArticle.Update
                case cStatutInterv:

                    _with12.ssGrilleStatutInterv.UpdateData();
                    break;
                case cTransmission:

                    _with12.ssGridTransmission.UpdateData();
                    break;
                case cStatutCommande:

                    _with12.ssGridStatutCommande.UpdateData();
                    break;
                case cAnalytique:

                    _with12.ssGridAna.UpdateData();
                    _with12.ssGridAnaActivite.UpdateData();
                    break;
                case cCodeEtatDevis:

                    _with12.GridCodeEtatDevis.UpdateData();
                    break;
                case cParametres:

                    _with12.GridParametres.UpdateData();
                    break;
                case cAchatsVentes:

                    _with12.GridAchatVente.UpdateData();
                    break;
                case cAnalySecteur:

                    ssGridAnalySecteur.UpdateData();
                    ssGridUtilClim.UpdateData();
                    break;
                case cPrime:

                    GridPrime.UpdateData();
                    break;
                case cVehicule:

                    GridARV.UpdateData();
                    GridPRV.UpdateData();
                    GridFLO.UpdateData();
                    break;
                case cMobilite:
                    fc_MajHistoPDA();
                    break;
                case cAnalySectCtr:
                    GridService2.UpdateData();
                    break;
                case cUtilisateur:
                    GridUser.UpdateData();
                    break;
                case cModeReglement:
                    GridRep.UpdateData();
                    break;
                case cModeMomentJour:
                    GridMOM.UpdateData();
                    break;
                case cPeriodeFact:
                    ssGridPCF.UpdateData();
                    break;
                case cTypeDevis:
                    GridTypeDev.UpdateData();
                    break;
                case cTypeEnc:
                    GridEnc.UpdateData();
                    break;
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                case cTypeCat:
                    GridCAT.UpdateData();
                    break;
                    //===> Fin Modif Mondir
            }

            switch (SSTab2.SelectedTab.Index)
            {
                case cTypeFacture:
                    _with12.GridTypeFacture.UpdateData();
                    break;
                case cIndice:
                    _with12.GridIndice.UpdateData();
                    break;
                case cTypeRatio:
                    _with12.GridTypeRatio.UpdateData();
                    break;
                case cFormule:
                    _with12.GridFormule.UpdateData();
                    break;

            }
        }
        /// <summary>
        /// TESTED   
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCode0_Click(object sender, EventArgs e)
        {
            var cmd = sender as Button;
            int index = Convert.ToInt16(cmd.Tag);
            string req = "";
            if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                req = "SELECT  Lien.Code AS \"Code\", Lien.Adresse AS \"Valeur\" FROM LienV2 ";
            else
                req = "SELECT  Lien.Code AS \"Code\", Lien.Adresse AS \"Valeur\" FROM Lien ";
            string where = " Masquer=0 ";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un paramètre" };

            if (index == 0)
            {
                fg.SetValues(new Dictionary<string, string> { { "Lien.Code", txtFiltreCode.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtFiltreCode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_GrilleParametres();
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtFiltreCode.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_GrilleParametres();
                        fg.Dispose(); fg.Close();
                    }
                };
            }
            else if (index == 1)
            {
                fg.SetValues(new Dictionary<string, string> { { "Lien.Adresse", txtFiltreValeur.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtFiltreValeur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fc_GrilleParametres();
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtFiltreValeur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fc_GrilleParametres();
                        fg.Dispose(); fg.Close();
                    }
                };

            }
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";
            req = "SELECT     DROG_Nom as \"Groupe\", DROG_Drescription as \"Drescription\"  FROM   DROG_Groupe";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un groupe" };
            fg.SetValues(new Dictionary<string, string> { { "DROG_Nom", txtDROG_Nom.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtDROG_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtDROG_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void cmdRechercheMatricule_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\","
                + " Personnel.NumRadio as \"NumRadio\" FROM Personnel LEFT JOIN Qualification ON Personnel.CodeQualif=Qualification.CodeQualif";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un matricule" };
            fg.SetValues(new Dictionary<string, string> { { "Personnel.Matricule", txtFiltreMatricule.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtFiltreMatricule.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                fc_GrillePersonnel();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtFiltreMatricule.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_GrillePersonnel();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void cmdRechercheNom_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\","
                + " Personnel.NumRadio as \"NumRadio\" FROM Personnel LEFT JOIN Qualification ON Personnel.CodeQualif=Qualification.CodeQualif";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un nom" };
            fg.SetValues(new Dictionary<string, string> { { "Personnel.Nom", txtFiltreNom.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtFiltreNom.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                fc_GrillePersonnel();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtFiltreNom.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fc_GrillePersonnel();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void cmdRechercheQualif_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT  Qualification.CodeQualif as \"Code qualif.\",Qualification.Qualification as \"Qualification\" FROM Qualification ";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un qualif" };
            fg.SetValues(new Dictionary<string, string> { { "Qualification.CodeQualif", txtFiltreQualif.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtFiltreQualif.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                fc_GrillePersonnel();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtFiltreQualif.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_GrillePersonnel();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void cmdRechQualif_Click(object sender, EventArgs e)
        {
            string req = null;
            string where = "";

            req = "SELECT Personnel.Matricule as \"Matricule\" ,Personnel.Nom as \"Nom\",Personnel.Initiales as \"Initiales\" ,"
                + " Qualification.Qualification as \"Qualification\", Personnel.Memoguard as \"MemoGuard\",Personnel.NumRadio as \"NumRadio\" "
                + " FROM Personnel LEFT JOIN Qualification ON Personnel.CodeQualif=Qualification.CodeQualif";

            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un initiale" };
            fg.SetValues(new Dictionary<string, string> { { "Personnel.Initiales", txtFiltreInit.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtFiltreInit.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                fc_GrillePersonnel();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtFiltreInit.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fc_GrillePersonnel();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            Forms.frmAjoutGroupe frmAjoutGroupe = new Forms.frmAjoutGroupe();
            frmAjoutGroupe.txtDROG_Drescription.Text = "";
            frmAjoutGroupe.txtDROG_Nom.Text = "";
            frmAjoutGroupe.ShowDialog();
            fc_Groupe();
        }

        private void Command4_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable rs = default(DataTable);
                string sSQL = null;
                txtUSR_Nt.Text = txtUSR_Nt.Text.Trim();
                var tmpAdors = new ModAdo();
                if (string.IsNullOrEmpty(txtUSR_Name.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le nom est obligatoire.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrEmpty(txtDROG_Nom.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le groupe est obligatoire", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    sSQL = "SELECT  DROG_Nom From DROG_Groupe WHERE DROG_Nom ='" + txtDROG_Nom.Text + "'";
                    sSQL = tmpAdors.fc_ADOlibelle(sSQL);

                    if (string.IsNullOrEmpty(sSQL))//tested
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce groupe n'existe pas.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                sSQL = "SELECT  USR_Nt, USR_Name, DROG_Nom, USR_No" + " From USR_Users" + " WHERE   USR_Nt ='" + txtUSR_Nt.Text + "'";
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    rs.Rows[0]["USR_Nt"] = txtUSR_Nt.Text;
                    rs.Rows[0]["USR_Name"] = txtUSR_Name.Text;
                    rs.Rows[0]["DROG_Nom"] = txtDROG_Nom.Text;
                    tmpAdors.Update();
                }

                rs.Dispose();
                rs = null;
                fc_Utilisateur();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "Command4_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridAchatVente_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsAchatsVentes.Update();
            sub_AfficheGraph();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridAchatVente_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            int xx = tmpAdorsAchatsVentes.Update();
            sub_AfficheGraph();
            sub_GrilleAchatVente();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void sub_AfficheGraph()
        {
            string sqlSelect = null;
            DataTable rsGraphique = default(DataTable);
            int i = 0;
            double lngPalierPrec = 0;
            double PalierAchat = 0;
            double div = 0;
            double prod = 0;
            double add = 0;
            double a = 0;
            double b = 0;
            double x = 0;
            double y = 0;
            GraphAchatVente.Series.Clear();
            GraphAchatVente.Legends.Clear();
            GraphAchatVente.ChartAreas.Clear();

            GraphAchatVente.ChartAreas.Add("Chart1");
            GraphAchatVente.ChartAreas[0].AxisX.TitleFont = new Font("Vendera", 11, FontStyle.Bold);
            GraphAchatVente.ChartAreas[0].AxisY.TitleFont = new Font("Vendera", 11, FontStyle.Bold);
            GraphAchatVente.ChartAreas[0].AxisY2.TitleFont = new Font("Vendera", 11, FontStyle.Bold);
            GraphAchatVente.ChartAreas[0].AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            GraphAchatVente.ChartAreas[0].BorderDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            tmpAdorsGraphique = new ModAdo();


            if (optCoef.Checked == true)
            {
                GraphAchatVente.Legends.Add("Coefficient");
                GraphAchatVente.Series.Add("Coefficient");
                GraphAchatVente.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                GraphAchatVente.Series[0].Color = Color.Red;
                rsGraphique = new DataTable();
                sqlSelect = "SELECT ConvertAchatVente.PalierAchat, ConvertAchatVente.a AS Coefficient FROM ConvertAchatVente ";
                sqlSelect = sqlSelect + "ORDER BY ConvertAchatVente.PalierAchat";

                rsGraphique = tmpAdorsGraphique.fc_OpenRecordSet(sqlSelect);
                //GraphAchatVente.ColumnLabel = rsGraphique.Fields(1).Name;
                GraphAchatVente.DataSource = rsGraphique;

                if (rsGraphique.Rows.Count > 0)
                {
                    // rsGraphique.MoveFirst();
                    foreach (DataRow rsGraphiqueRow in rsGraphique.Rows)
                    {
                        GraphAchatVente.Series[0].Points.AddXY(rsGraphiqueRow[0], rsGraphiqueRow[1]);

                        /*GraphAchatVente.Row = i;
                        GraphAchatVente.Data = rsGraphique.Fields(1).value;
                        GraphAchatVente.RowLabel = rsGraphique.Fields(0).value;                      
                        rsGraphique.MoveNext();*/
                        i = i + 1;
                    }
                }


            }

            else if (optFormule.Checked == true)
            {
                i = 1;
                GraphAchatVente.Legends.Add("Prix de vente");
                GraphAchatVente.Series.Add("Prix de vente");
                GraphAchatVente.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
                GraphAchatVente.Series[0].Color = Color.Red;
                if (GridAchatVente.ActiveRow != null)
                {
                    if (GridAchatVente.ActiveRow.Cells["PalierAchat"].Value == DBNull.Value)
                    {
                        GridAchatVente.ActiveRow.Cells["PalierAchat"].Value = 0;
                    }

                    string s = GridAchatVente.ActiveRow.Cells["PalierAchat"].Value.ToString();
                    a = Convert.ToDouble(GridAchatVente.ActiveRow.Cells["a"].Value);
                    b = Convert.ToDouble(GridAchatVente.ActiveRow.Cells["b"].Value);
                    lngPalierPrec = Convert.ToDouble(General.nz(tmpAdorsGraphique.fc_ADOlibelle(
                         "SELECT PalierAchat FROM ConvertAchatVente WHERE PalierAchat<"
                         + s + " ORDER BY PalierAchat DESC"), "0"));
                }


                for (i = 1; i <= 10; i++)
                {
                    if (GridAchatVente.ActiveRow != null)
                    {
                        if (!string.IsNullOrEmpty(GridAchatVente.ActiveRow.Cells["PalierAchat"].Value.ToString())
                             && GridAchatVente.ActiveRow.Cells["PalierAchat"].Value.ToString() != "0")

                        {
                            PalierAchat = Convert.ToDouble(GridAchatVente.ActiveRow.Cells["PalierAchat"].Value.ToString());
                            div = ((PalierAchat - lngPalierPrec) / 10);
                            prod = div * i;
                            add = lngPalierPrec + prod;

                            x = add * a + b;
                            y = add;
                            GraphAchatVente.Series[0].Points.AddXY(y, x);

                            //GraphAchatVente.Row = i;
                            //GraphAchatVente.Data = ((lngPalierPrec + ((GridAchatVente.Columns["PalierAchat"].Value - lngPalierPrec) / 10) * i) * GridAchatVente.Columns["a"].Value) + GridAchatVente.Columns["b"].Value;
                            //GraphAchatVente.RowLabel = Convert.ToString(lngPalierPrec + ((GridAchatVente.Columns["PalierAchat"].Value - lngPalierPrec) / 10) * i);
                        }

                        else
                        {
                            x = ((10 * i) * a) + b;
                            y = 10 * i;
                            GraphAchatVente.Series[0].Points.AddXY(y, x);

                            //GraphAchatVente.Row = i;
                            //GraphAchatVente.Data = ((10 * i) * GridAchatVente.Columns["a"].Value) + GridAchatVente.Columns["b"].Value;
                            //GraphAchatVente.RowLabel = Convert.ToString(10 * i);
                        }
                    }
                }
            }


            // GraphAchatVente.ShowLegend = true;
            // GraphAchatVente.ColumnCount = 1;
            // GraphAchatVente.Column = 1;
            GraphAchatVente.Show();

            if ((rsGraphique != null))
            {
                rsGraphique.Dispose();
                rsGraphique = null;
            }

        }

        private void GridAchatVente_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void GridAchatVente_AfterExitEditMode(object sender, EventArgs e)
        {
            //verifier
            if (string.IsNullOrEmpty(GridAchatVente.ActiveRow.Cells["PalierAchat"].Value.ToString()))
            {
                GridAchatVente.ActiveRow.Cells["PalierAchat"].Value = GridAchatVente.ActiveRow.Cells["PalierAchat"].OriginalValue;
            }
            if (string.IsNullOrEmpty(GridAchatVente.ActiveRow.Cells["a"].Value.ToString()))

            {
                GridAchatVente.ActiveRow.Cells["a"].Value = GridAchatVente.ActiveRow.Cells["a"].OriginalValue;
            }
            if (string.IsNullOrEmpty(GridAchatVente.ActiveRow.Cells["b"].Value.ToString()))
            {
                GridAchatVente.ActiveRow.Cells["b"].Value = GridAchatVente.ActiveRow.Cells["b"].OriginalValue;
            }
            if (optFormule.Checked == true && GridAchatVente.ActiveRow.Index <= GridAchatVente.Rows.Count - 1 && GridAchatVente.ActiveRow.Index.ToString() != e.ToString())
            {
                sub_AfficheGraph();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCodeEtatDevis_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {//verifier
            if (GridCodeEtatDevis.ActiveCell.Column.Index == 0)
            {
                if (GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString() == "00" ||
                  GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString() == "04" ||
                   GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString() == "A" ||
                    GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString() == "R" ||
                   GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString() == "T")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas modifier ce code", "Misa à jour du code impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    GridCodeEtatDevis.ActiveRow.Cells["Code"].Value = GridCodeEtatDevis.ActiveRow.Cells["Code"].Value.ToString();
                }
            }

            if (GridCodeEtatDevis.ActiveCell.Column.Index == GridCodeEtatDevis.ActiveRow.Cells["CodeEtatManuel"].Column.Index)
            {
                if (GridCodeEtatDevis.ActiveRow.Cells["CodeEtatManuel"].Text == "-1")
                {
                    GridCodeEtatDevis.ActiveRow.Cells["CodeEtatManuel"].Value = 1;
                }
            }

            if (GridCodeEtatDevis.ActiveCell.Column.Index == GridCodeEtatDevis.ActiveRow.Cells["ApImprime"].Column.Index)
            {
                if (GridCodeEtatDevis.ActiveRow.Cells["ApImprime"].Text == "-1")
                {
                    GridCodeEtatDevis.ActiveRow.Cells["ApImprime"].Value = 1;
                }
            }
        }

        private void GridCodeEtatDevis_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void GridIndice_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridIndice.ActiveRow.Cells["Libelle"].Column.Index == GridIndice.ActiveCell.Column.Index)
            {
                GridIndice.ActiveRow.Cells["Libelle"].Value = GridIndice.ActiveRow.Cells["Libelle"].Text.Trim();
            }
        }

        private void GridIndice_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["Libelle"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne libelle est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }

            if (string.IsNullOrEmpty(e.Row.Cells["date"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne date est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }

            if (string.IsNullOrEmpty(e.Row.Cells["Valeur"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Valeur est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }

            if (string.IsNullOrEmpty(e.Row.Cells["Connu"].Text) && string.IsNullOrEmpty(GridIndice.ActiveRow.Cells["Reel"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez spécifier s'il s'agit d'un indice réel ou connu.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Libelle"].DataChanged || e.Row.Cells["date"].DataChanged)
            {
                if (e.Row.Cells["Libelle"].OriginalValue.ToString() != e.Row.Cells["Libelle"].Text
                        && e.Row.Cells["date"].OriginalValue.ToString() != e.Row.Cells["date"].Text
                       )
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FacIndice WHERE Libelle = '{e.Row.Cells["Libelle"].Text}' and date='{e.Row.Cells["date"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridService_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(sIDService) && string.IsNullOrEmpty(GridService.ActiveRow.Cells["CodeServiceAnalytique"].Text))
                {
                    GridService.ActiveRow.Cells["CodeServiceAnalytique"].Value = sIDService;
                }

                if (GridService.ActiveCell.Column.Key.ToString().ToUpper() == "CptVte".ToString().ToUpper())
                {
                    GridService.ActiveRow.Cells["libelle"].Value = fc_lblCpte(GridService.ActiveRow.Cells["CptVte"].Text);
                }

                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridService_BeforeColUpdate");
            }
        }

        private void GridService_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                e.Row.Cells["libelle"].Value = fc_lblCpte(e.Row.Cells["CptVte"].Text);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridService_InitializeRow");
            }
        }

        private void GridService2_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(sIDService) && string.IsNullOrEmpty(GridService2.ActiveRow.Cells["CodeServiceAnalytique"].Text))
                {
                    GridService2.ActiveRow.Cells["CodeServiceAnalytique"].Value = sIDService;
                }

                //If UCase(GridService2.Columns(ColIndex).Name) = UCase("CptVte") Then
                //        GridService.Columns("lblCompte").value = fc_lblCpte(GridService.Columns("CptVte").Text)
                //End If

                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridService_BeforeColUpdate");
            }

        }

        private void GridSpecifQualif_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }
        private void button5_Click(object sender, EventArgs e)
        {
            if (this.ssGridStatutCommande.Rows.Count > 0)
            {
                this.ssGridStatutCommande.DeleteSelectedRows();
                tmpAdorsStatutCommande.Update();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        private void lblSupPersonnel_Click(object sender, EventArgs e)
        {
            if (ssGridPersonnel.Rows.Count > 0)
            {
                ssGridPersonnel.DeleteSelectedRows();
                tmpAdorsPersonnel.Update();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lblSuprQualParam_Click(object sender, EventArgs e)
        {
            if (GridSpecifQualif.Rows.Count > 0)
            {
                GridSpecifQualif.DeleteSelectedRows();
                tmpAdorsSpecifQualif.Update();

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lbSupStatutInterv_Click(object sender, EventArgs e)
        {
            if (ssGrilleStatutInterv.Rows.Count > 0)
            {
                ssGrilleStatutInterv.DeleteSelectedRows();
                tmpAdorsStatutInterv.Update();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void optCoef_CheckedChanged(object sender, EventArgs e)
        {
            if (optCoef.Checked)
            {
                sub_AfficheGraph();

                if (chkMarqueurs.Checked)
                {
                    chkMarqueurs_CheckedChanged(null, null);
                }
            }
        }

        private void optFormule_CheckedChanged(object sender, EventArgs e)
        {
            if (optFormule.Checked)
            {
                sub_AfficheGraph();

                if (chkMarqueurs.Checked)
                {
                    chkMarqueurs_CheckedChanged(null, null);
                }
            }
        }

        private void OptGroupe_CheckedChanged(object sender, EventArgs e)
        {
            if (OptGroupe.Checked)
            {
                fc_Utilisateur();
            }
        }

        private void optUtilisateur_CheckedChanged(object sender, EventArgs e)
        {
            if (optUtilisateur.Checked)
            {
                fc_Utilisateur();
            }
        }

        private void ssCAI_CategoriInterv_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            ssCAI_CategoriInterv.ActiveRow.Cells["CAI_Noauto"].Value = 0;
        }

        private void ssCAI_CategoriInterv_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssCAI_CategoriInterv_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (ssFacArticle.ActiveRow.Cells["CAI_NoAuto"].Value != ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoAuto"].Value)
                {
                    fc_GrillessFacArticle(ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoAuto"].Value.ToString());
                }
                if (string.IsNullOrEmpty(ssCAI_CategoriInterv.ActiveRow.Cells["CAI_NoAuto"].Value.ToString()))
                {
                    ssFacArticle.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                }
                else
                {
                    ssFacArticle.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssCAI_CategoriInterv_AfterExitEditMode");
            }
        }

        private void ssFacArticle_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (ssFacArticle.ActiveCell.Column.Index == ssFacArticle.ActiveRow.Cells["ACT_Code"].Column.Index)
                {

                    if (sheridan.fc_FindLibelle(ssDropACT_AnaActivite, ssFacArticle.ActiveRow.Cells["ACT_Code"].Value.ToString(), "ACT_CODE") == false)
                    {
                        ssFacArticle.ActiveRow.Cells["ACT_Code"].Value = "";
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssCAI_CategoriInterv_AfterExitEditMode");
            }
        }

        private void ssGridPersonnel_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            try
            {
                /* SELECT Matricule, Nom, Prenom, Initiales, ActiverPDA, CodeQualif, CRespExploit,
                          Commercial, DevRadiateur, DevExploit, RenovChauffage1, RenovChauffage2, Chauffagiste1,
                          Chauffagiste2, CSecteur,
                         Dep2, Rondier, RespService, NumRadio, MemoGuard, Email, PouvoirAchat, Login, SousTraitant, NePasAfficherOcean
                       , EnvoieMail, LogTomTom, ActiveTomTom, NonActif, SCH_ServiceCtrlHeure.SCH_Code, SCH_ServiceCtrlHeure.SCH_Libelle as Libelle
                       FROM Personnel
                       LEFT JOIN SCH_ServiceCtrlHeure ON SCH_ServiceCtrlHeure.SCH_Code = Personnel.SCH_Code
                     */



                string reqInsert = $"insert into Personnel (Matricule,Nom,Prenom,Initiales,ActiverPDA,CodeQualif,CRespExploit,Commercial," +
                             $"DevRadiateur,DevExploit,RenovChauffage1,RenovChauffage2,Chauffagiste2,CSecteur,Dep2,Rondier,RespService,NumRadio,MemoGuard," +
                             //====> Mondir le 29.06.2020 : Ajouter CSecteur2 à la requette
                             $"Email,PouvoirAchat,Login,SousTraitant,NePasAfficherOcean,EnvoieMail,LogTomTom,ActiveTomTom,NonActif,SCH_Code, CSecteur2) values " +
                              //====> Fin modif Mondir
                              $"( '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Matricule"].Text)}' " +
                              $" ,  '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Nom"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Prenom"].Text)}'" +
                         $" ,  '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Initiales"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["ActiverPDA"].Text)}'" +
                         $" ,  '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeQualif"].Text)}'" +
                         $" ,   '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CRespExploit"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Commercial"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["DevRadiateur"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["DevExploit"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RenovChauffage1"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RenovChauffage2"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Chauffagiste2"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CSecteur"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Dep2"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Rondier"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RespService"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NumRadio"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["MemoGuard"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Email"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["PouvoirAchat"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Login"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["SousTraitant"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NePasAfficherOcean"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["EnvoieMail"].Text)}'" +
                         $" ,'{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["LogTomTom"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["ActiveTomTom"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NonActif"].Text)}'" +
                         $" , '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["SCH_Code"].Text)}'" +
                             $", '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CSecteur2"].Text)}')";

                string reqUpdate = $"UPDATE Personnel SET Matricule= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Matricule"].Text)}'" +
                         $" ,  Nom= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Nom"].Text)}'" +
                         $" , Prenom= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Prenom"].Text)}'" +
                         $" , Initiales= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Initiales"].Text)}'" +
                         $" , ActiverPDA= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["ActiverPDA"].Text)}'" +
                         $" , CodeQualif= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeQualif"].Text)}'" +
                         $" ,  CRespExploit= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CRespExploit"].Text)}'" +
                         $" ,Commercial= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Commercial"].Text)}'" +
                         $" ,DevRadiateur= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["DevRadiateur"].Text)}'" +
                         $" ,DevExploit= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["DevExploit"].Text)}'" +
                         $" ,RenovChauffage1= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RenovChauffage1"].Text)}'" +
                         $" ,RenovChauffage2= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RenovChauffage2"].Text)}'" +
                         $" ,Chauffagiste2= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Chauffagiste2"].Text)}'" +
                         $" ,CSecteur= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CSecteur"].Text)}'" +
                         $" ,Dep2= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Dep2"].Text)}'" +
                         $" ,Rondier= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Rondier"].Text)}'" +
                         $" ,RespService= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["RespService"].Text)}'" +
                         $" ,NumRadio= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NumRadio"].Text)}'" +
                         $" ,MemoGuard= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["MemoGuard"].Text)}'" +
                         $" ,Email= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Email"].Text)}'" +
                         $" ,PouvoirAchat= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["PouvoirAchat"].Text)}'" +
                         $" ,Login= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Login"].Text)}'" +
                         $" ,SousTraitant= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["SousTraitant"].Text)}'" +
                         $" ,NePasAfficherOcean= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NePasAfficherOcean"].Text)}'" +
                         $" ,EnvoieMail= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["EnvoieMail"].Text)}'" +
                         $" ,LogTomTom= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["LogTomTom"].Text)}'" +
                         $" ,ActiveTomTom= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["ActiveTomTom"].Text)}'" +
                         $" ,NonActif= '{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["NonActif"].Text)}'" +
                         //====> Mondir le 29.06.2020 : Ajouter CSecteur2 à la requette
                         $",CSecteur2='{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CSecteur2"].Text)}'" +
                         //====> Fin modif Mondir
                         $" WHERE  Matricule='{StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Matricule"].Value.ToString())}' ";




                using (var tmpModAdo = new ModAdo())
                {
                    var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Personnel WHERE Matricule = '{e.Row.Cells["Matricule"].Text}'  "));
                    if (inDb > 0)
                    {
                        int x = General.Execute(reqUpdate);

                    }
                    else
                    {
                        int yy = General.Execute(reqInsert);

                    }
                }
                ///////////////////////////////////////////////////////////
                string sSqlDel = null;
                string sSQL = null;
                sSqlDel = "DELETE Personnel WHERE Personnel.Initiales='XXX'";
                sSQL = "SELECT Personnel.* ,'' as Libelle FROM Personnel WHERE Personnel.Initiales='XXX'";
                General.rstmp = new DataTable();
                var tmpAdors = new ModAdo();
                General.rstmp = tmpAdors.fc_OpenRecordSet(sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    int xx = General.Execute(sSqlDel);
                    fc_GrillePersonnel();
                    //ssGridPersonnel.DataSource = rsPersonnel;
                }
                General.rstmp.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridPersonnel_AfterRowUpdate");
            }
        }

        private void ssGridPersonnel_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (ssGridPersonnel.ActiveRow.Cells["SousTraitant"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                {
                    if (ssGridPersonnel.ActiveRow.Cells["SousTraitant"].Text == "-1")
                    {
                        ssGridPersonnel.ActiveRow.Cells["SousTraitant"].Value = "1";
                    }
                }

                if (ssGridPersonnel.ActiveRow.Cells["NePasAfficherOcean"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                {
                    if (ssGridPersonnel.ActiveRow.Cells["NePasAfficherOcean"].Text == "-1")
                    {
                        ssGridPersonnel.ActiveRow.Cells["NePasAfficherOcean"].Value = "1";
                    }
                }

                if (ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                {
                    if (ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Text == "-1")
                    {
                        ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Value = "1";
                    }
                }

                if (ssGridPersonnel.ActiveRow.Cells["NonActif"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                {
                    if (ssGridPersonnel.ActiveRow.Cells["NonActif"].Text == "-1")
                    {
                        ssGridPersonnel.ActiveRow.Cells["NonActif"].Value = "1";
                    }
                }

                /*   if (ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                   {
                       if (ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Text == "-1")
                       {
                           ssGridPersonnel.ActiveRow.Cells["ActiveTomTom"].Value = "1";
                       }
                   }
                   */
                if (ssGridPersonnel.ActiveRow.Cells["ActiverPDA"].Column.Index == ssGridPersonnel.ActiveCell.Column.Index)
                {
                    if (ssGridPersonnel.ActiveRow.Cells["ActiverPDA"].Text == "-1")
                    {
                        ssGridPersonnel.ActiveRow.Cells["ActiverPDA"].Value = "1";
                    }
                }


                //if (ssGridPersonnel.ActiveRow.Cells["Matricule"].OriginalValue.ToString() != ssGridPersonnel.ActiveRow.Cells["Matricule"].Text || ssGridPersonnel.ActiveRow.IsAddRow)
                //{
                //    using (var tmpModAdo = new ModAdo())
                //    {
                //        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Personnel WHERE Matricule = '{ssGridPersonnel.ActiveRow.Cells["Matricule"].Text}' " +
                //                                                           $" or Initiales= '{ssGridPersonnel.ActiveRow.Cells["Initiales"].Text}' "));
                //        if (inDb > 0)
                //        {
                //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //            ssGridPersonnel.ActiveRow.CancelUpdate();
                //            e.Cancel = true;
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridPersonnel_BeforeExitEditMode");
            }
        }

        private void ssGridPersonnel_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["Initiales"].Text.ToString()))
            {

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il manque les initiales de cette personne. Souhaitez-vous annuler la saisie de l'intervenant ?", "Initiales manquantes", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    //Cancel = True
                    e.Row.Cells["Initiales"].Value = "XXX";
                    e.Cancel = false;
                }
                else
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["MemoGuard"].Text))
                    {
                        e.Row.Cells["MemoGuard"].Value = e.Row.Cells["Matricule"].Text;
                    }
                    e.Cancel = true;
                }
            }

            //===> Mondir le 23.06.2021, https://groupe-dt.mantishub.io/view.php?id=2506 added DataChanged Condition
            if (e.Row.Cells["Matricule"].DataChanged)
            {
                if (e.Row.Cells["Matricule"].OriginalValue.ToString() != e.Row.Cells["Matricule"].Text || e.Row.IsAddRow)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Personnel WHERE Matricule = '{e.Row.Cells["Matricule"].Text}'  or Initiales= '{e.Row.Cells["Initiales"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
            if (e.Row.Cells["NonActif"].Text == "-1")
            {
                e.Row.Cells["NonActif"].Value = 1;
            }
            else if (e.Row.Cells["NonActif"].Text == "1")
            {

            }
            else
            {
                e.Row.Cells["NonActif"].Value = 0;
            }


        }

        private void ssGridPersonnel_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {


            string sSQL = null;
            /* if (!string.IsNullOrEmpty(e.Row.Cells["SCH_Code"].Text))
             {
                sSQL = "SELECT   SCH_Libelle" + " From SCH_ServiceCtrlHeure WHERE  SCH_Code = '"
                     + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["SCH_Code"].Text) + "'";
                 var tmpAdo = new ModAdo();
                 sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                 e.Row.Cells["Libelle"].Value = sSQL;
             }
             else
             {
                 e.Row.Cells["Libelle"].Value = "";
             }*/
            if (e.Row.Cells["NonActif"].Value == DBNull.Value)
            {
                e.Row.Cells["NonActif"].Value = false;
            }
            if (e.Row.Cells["SousTraitant"].Value == DBNull.Value)
            {
                e.Row.Cells["SousTraitant"].Value = false;
            }
            if (e.Row.Cells["NepasAfficherOcean"].Value == DBNull.Value)
            {
                e.Row.Cells["NepasAfficherOcean"].Value = false;
            }
            if (e.Row.Cells["EnvoieMail"].Value == DBNull.Value)
            {
                e.Row.Cells["EnvoieMail"].Value = false;
            }
            if (e.Row.Cells["ActiverPDA"].Value == DBNull.Value)
            {
                e.Row.Cells["ActiverPDA"].Value = false;
            }

        }

        private void ssGridPersonnel_AfterExitEditMode(object sender, EventArgs e)
        {

            try
            {
                if (ssGridQualite.ActiveRow.Cells["PDA"].Text == "-1")
                {
                    ssGridQualite.ActiveRow.Cells["PDA"].Value = 1;
                }


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridQualite_AfterColUpdate");
            }

        }

        private void ssGridQualite_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            //Réactualise la combo en drop down des qualifications
            General.sSQL = "";
            General.sSQL = "SELECT codeQualif, Qualification" + " FROM Qualification";
            sheridan.InitialiseCombo(this.SSDropQualif, General.sSQL, "CodeQualif", true);
            //this.GridSpecifQualif.DisplayLayout.Bands[0].Columns["Code Qualification"].ValueList = this.SSDropQualif;
            e.Cancel = Cancel;
        }

        private void SSGridSecteur_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssGridTomTom_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["TOT_Vehicule"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le code du véhicule est obligatoire", "Saisie incompléte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }

            if (string.IsNullOrEmpty(ssGridTomTom.ActiveRow.Cells["matricule"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le matricule du personnel est obligatoire", "Saisie incompléte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
            }
        }
        /// <summary>
        ///   tested
        /// </summary>

        private void sstab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            TimerSSTab1.Enabled = true;

            if (encaissement.SelectedTab.Index == 13)
            {
                //== Affectation des droits.
                fc_loadTreeView();
                //== Groupe/Utilisateur.
                fc_Groupe();
                fc_Utilisateur();
                //== onglet Affectation des utilisateurs.
                fc_AffUtil();
                fc_AffGroupe("");

            }
            else if (encaissement.SelectedTab.Index == 16)
            {
                fc_TreevService();
                fc_DropCPte();
            }
            else if (encaissement.SelectedTab.Index == 20)//tested
            {
                fc_TreevService2();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCpte"></param>
        /// <returns></returns>
        private string fc_lblCpte(string sCpte)
        {
            string functionReturnValue = null;
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                functionReturnValue = "";
                SAGE.fc_OpenConnSage();
                sSQL = "SELECT  CG_Intitule " + " From F_COMPTEG " + " WHERE CG_Num = '" + StdSQLchaine.gFr_DoublerQuote(sCpte) + "'";
                rs = new DataTable();
                SqlDataAdapter sda = new SqlDataAdapter(sSQL, SAGE.adoSage);
                sda.Fill(rs);

                if (rs.Rows.Count > 0)
                {
                    functionReturnValue = rs.Rows[0]["CG_Intitule"].ToString();
                }

                rs.Dispose();
                rs = null;
                SAGE.fc_CloseConnSage();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_lblCpte");
                return functionReturnValue;
            }
        }

        private void fc_DropCPte()
        {
            try
            {
                string sSQL = null;
                SAGE.fc_OpenConnSage();

                //sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] " _
                //& " From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  " _
                //& " (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 ) ORDER BY F_COMPTEG.CG_NUM "

                sSQL = "SELECT F_COMPTEG.CG_NUM as [Code comptable], F_COMPTEG.CG_INTITULE as [Intitulé] "
                    + " From F_COMPTEG WHERE (F_COMPTEG.CG_NUM) Like '488%' or  "
                    + " (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 ) ORDER BY F_COMPTEG.CG_NUM ";

                sheridan.InitialiseCombo(DropCpte, sSQL, "Code comptable", true, SAGE.adoSage);
                SAGE.fc_CloseConnSage();
                // GridService.Columns["CptVte"].DropDownHwnd = DropCpte.hWnd;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropCPte");
            }

        }
        private void fc_TreevService()
        {
            try
            {
                DataTable rstmp = default(DataTable);
                DataTable rsG = default(DataTable);
                string sSQL = null;
                bool bFind = false;
                System.Windows.Forms.TreeNode Node = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = " SELECT     CodeServiceAnalytique"
                        + " From ServiceAnalytique "
                        + " Where (Afficher = 1) " + " ORDER BY CodeServiceAnalytique ";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);

                //==reinitialise le treview.
                TreeService.Nodes.Clear();

                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        //==Ajout du 1er niveau de hiérarchie.( nom des fiches)
                        TreeService.Nodes.Add(cNiv1 + rstmpRow["CodeServiceAnalytique"], rstmpRow["CodeServiceAnalytique"].ToString());
                        TreeService.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    }
                }
                rstmp.Dispose();
                rstmp = null;
                //== Positionne
                bFind = false;
                foreach (UltraTreeNode iNode in TreeService.Nodes)
                {
                    if (iNode.Selected == true)
                    {
                        TreeService_AfterSelect(TreeService, null);
                        bFind = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridService.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_TreevService");
            }

        }
        private void fc_TreevService2()
        {
            try
            {
                DataTable rstmp = default(DataTable);
                DataTable rsG = default(DataTable);
                string sSQL = null;
                bool bFind = false;
                UltraTreeNode Node = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = " SELECT     CodeServiceAnalytique"
                    + " From ServiceAnalytique "
                    + " Where (Afficher = 1) "
                    + " ORDER BY CodeServiceAnalytique ";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);
                var _with69 = rstmp;

                //==reinitialise le treview.
                TreeService2.Nodes.Clear();

                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        //==Ajout du 1er niveau de hiérarchie.( nom des fiches)
                        TreeService2.Nodes.Add(cNiv1 + rstmpRow["CodeServiceAnalytique"], rstmpRow["CodeServiceAnalytique"].ToString());
                        TreeService2.Nodes.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    }
                }
                rstmp.Dispose();
                rstmp = null;

                //== Positionne
                bFind = false;
                foreach (UltraTreeNode iNode in TreeService2.Nodes)
                {
                    if (iNode.Selected == true)
                    {
                        //TreeService_AfterSelect(TreeService, null);

                        bFind = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridService.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_TreevService2");
            }

        }
        private void fc_Utilisateur()
        {
            fc_LoadtreeviewUtil();
        }
        private void fc_AffUtil()
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;
                string sGroupe = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = "SELECT     USR_No, USR_Nt, USR_Name, DROG_Nom" + " From USR_Users " + " WHERE     (DROG_Nom IS NULL) OR" + " (DROG_Nom = '')";
                sSQL = sSQL + " ORDER BY USR_Name";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);
                ListView1.Items.Clear();
                // ListView1.Columns.Add("Utilisateur", 20, HorizontalAlignment.Center);

                /*
                 ListView1.ColumnHeaders.Add(, , "Utilisateur", Microsoft.VisualBasic.Compatibility.VB6.Support.PixelsToTwipsX(ListView1.Width) - 100);
                 //===Définit la propriété BorderStyle.
                 ListView1.BorderStyle = ComctlLib.BorderStyleConstants.ccFixedSingle;
                 //===Choisit la vue Report.
                 ListView1.View = ComctlLib.ListViewConstants.lvwReport;
                 ListView1.ListItems.Clear();
                 ListView1.LabelEdit = ComctlLib.ListLabelEditConstants.lvwManual;
                 ListView1.MultiSelect = true;
                 ListView1.HideSelection = false;
                 */
                ListView1.Items.Clear();
                ListView1.HeaderStyle = ColumnHeaderStyle.Nonclickable;
                /*ColumnHeader columnHeader1 = new ColumnHeader();
                columnHeader1.Text = "Utilisateur";
                columnHeader1.TextAlign = HorizontalAlignment.Left;
                columnHeader1.Width = 100;
                ListView1.Columns.Add(columnHeader1*/
                //ListView1.View = System.Windows.Forms.View.List;
                ListView1.LabelEdit = true;
                ListView1.MultiSelect = true;
                ListView1.HideSelection = false;
                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        //==Ajout du 1er niveau de hiérarchie.( nom des fiches)                      
                        ListView1.Items.Add(cNiv1 + rstmpRow["USR_No"], rstmpRow["USR_Name"].ToString(), 1);
                    }

                }
                rstmp.Dispose();
                rstmp = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_AffUtil");
            }
        }
        /// <summary>
        /// tested
        /// </summary>

        private void fc_LoadtreeviewUtil()
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;
                string sGroupe = "";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = "SELECT     USR_No, USR_Nt, USR_Name, DROG_Nom" + " From USR_Users";
                if (optUtilisateur.Checked == true)
                {
                    sSQL = sSQL + " ORDER BY USR_Name";
                }
                else if (OptGroupe.Checked == true)
                {
                    sSQL = sSQL + " ORDER BY DROG_Nom,USR_Name";
                }
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);
                //==reinitialise le treview.
                TreeView2.Nodes.Clear();
                int k = 0;
                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        if (optUtilisateur.Checked == true)
                        {
                            //==Ajout du 1er niveau de hiérarchie.( nom des fiches)
                            TreeView2.Nodes.Add(cNiv1 + rstmpRow["USR_No"], rstmpRow["USR_Name"].ToString());
                            TreeView2.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                        }

                        else if (OptGroupe.Checked == true)
                        {
                            if (sGroupe.ToUpper() != General.nz(rstmpRow["DROG_Nom"], "99999").ToString().ToUpper())
                            {
                                sGroupe = General.nz(rstmpRow["DROG_Nom"], "99999").ToString().ToUpper() + "";
                                TreeView2.Nodes.Add(cNiv1 + sGroupe, General.nz(rstmpRow["DROG_Nom"], "?").ToString());
                                TreeView2.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                            }
                            var node = TreeView2.GetNodeByKey(cNiv1 + sGroupe);
                            var childNode = node.Nodes.Add(cNiv2 + rstmpRow["USR_No"].ToString().ToUpper(), General.nz(rstmpRow["USR_Name"], "").ToString());


                        }

                    }

                }

                rstmp.Dispose();
                rstmp = null;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_LoadtreeviewUtil");
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        private void fc_Groupe()
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;
                string sGroupe = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = "SELECT     DROG_Noauto, DROG_Nom, DROG_Drescription" + " FROM     DROG_Groupe" + " order by DROG_Nom";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);
                //==reinitialise le treview.
                TreeGroupe.Nodes.Clear();

                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        TreeGroupe.Nodes.Add(cNiv1 + rstmpRow["DROG_Nom"], rstmpRow["DROG_Nom"].ToString());
                        TreeGroupe.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    }
                }

                rstmp = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Groupe");

            }

        }

        private void SSTab2_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {

            /*   if ((rsTypeFacture != null))
                  {
                      if (rsTypeFacture.State == ADODB.ObjectStateEnum.adStateOpen)
                      {
                          rsTypeFacture.Dispose();                 
                          rsTypeFacture = null;
                      }
                  }

                  if ((rsIndice != null))
                  {
                      if (rsIndice.State == ADODB.ObjectStateEnum.adStateOpen)
                      {
                          rsIndice.Dispose();
                          rsIndice = null;
                      }
                  }

                  if ((rsTypeRatio != null))
                  {
                      if (rsTypeRatio.State == ADODB.ObjectStateEnum.adStateOpen)
                      {
                          rsTypeRatio.Dispose();
                          rsTypeRatio = null;
                      }
                  }

                  if ((rsFormule != null))
                  {
                      if (rsFormule.State == ADODB.ObjectStateEnum.adStateOpen)
                      {
                          rsFormule.Dispose();
                          rsFormule = null;
                      }
                  }
                  */
            TimerSSTab2.Enabled = true;

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_loadTreeView()
        {
            try
            {

                DataTable rstmp = default(DataTable);
                DataTable rsG = default(DataTable);
                string sSQL = null;
                bool bFind = false;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                sSQL = "SELECT     DROF_Noauto, DROF_NomFiche, DROF_NomFicheReel, DROF_MdpOuUtil" + " FROM    DROF_DroitFiche order by DROF_NomFiche";
                var tmpAdorstmp = new ModAdo();
                rstmp = tmpAdorstmp.fc_OpenRecordSet(sSQL);
                var _with73 = rstmp;
                int i = 0;



                //==reinitialise le treview.
                TreeView1.Nodes.Clear();
                if (rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        //==Ajout du 1er niveau de hiérarchie.( nom des fiches)
                        TreeView1.Nodes.Add(cNiv1 + rstmpRow["DROF_NomFicheReel"], rstmpRow["DROF_NomFiche"].ToString());
                        TreeView1.Override.NodeAppearance.Image = Properties.Resources.close_folder;

                        //== Ajout du 2eme niveau de hiérarchie.(nom des groupes)

                        sSQL = "SELECT     DROG_Noauto, DROG_Nom, DROG_Drescription" + " From DROG_Groupe" + " ORDER BY DROG_Nom";
                        var tmpAdorsG = new ModAdo();
                        rsG = tmpAdorsG.fc_OpenRecordSet(sSQL);

                        foreach (DataRow rsGRow in rsG.Rows)
                        {
                            var node = TreeView1.GetNodeByKey(cNiv1 + rstmpRow["DROF_NomFicheReel"]);
                            var childNode = node.Nodes.Add(cNiv2 + rstmpRow["DROF_NomFicheReel"].ToString().ToUpper() + "@" + rsGRow["DROG_Nom"].ToString().ToUpper(), General.nz(rsGRow["DROG_Nom"], "").ToString());
                            string cc = childNode.Parent.Key;
                            childNode.Override.NodeAppearance.Image = Properties.Resources.close_folder;

                        }

                    }
                    rsG.Dispose();
                    rsG = null;
                }
                rstmp.Dispose();
                rstmp = null;


                //                    '==Ajout du 3eme niveau de hiérarchie.
                //                    If Not IsNull(!FAP_NoAuto) Then
                //                        TreeView1.Nodes.Add cNiv2 & !EQM_NoAuto, tvwChild, cNiv3 & !FAP_NoAuto, nz(!FAP_Designation1, ""), 3
                //                    End If
                //== Positionne
                bFind = false;
                foreach (UltraTreeNode Node in TreeView1.Nodes)
                {
                    if (Node.Selected == true)
                    {
                        TreeView1_AfterSelect(TreeView1, null);
                        bFind = true;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                }

                if (bFind == false)//tested
                {
                    lblNomFiche.Visible = false;
                    fraAddDroit.Visible = false;
                    chkMDP.Visible = false;
                    txtFicheMDP.Visible = false;
                    cmdAddDroitFiche.Visible = false;
                }
                else
                {
                    lblNomFiche.Visible = true;
                    fraAddDroit.Visible = true;
                    chkMDP.Visible = true;
                    txtFicheMDP.Visible = true;
                    cmdAddDroitFiche.Visible = true;
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Loadtreeview");
            }
        }

        private void SSTab3_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {

            //== Groupe/Utilisateur.
            // fc_Groupe
            // FC_Utilisateur

            //== onglet Affectation des utilisateurs.
            fc_AffUtil();//tested
            fc_AffGroupe("");//tested

            //== Affectation des droits.
            fc_loadTreeView();

        }

        private void SSTab4_DoubleClick(object sender, EventArgs e)
        {
            GridARV.UpdateData();
            fc_LoadArval();
            GridPRV.UpdateData();
            fc_LoadProblemeVehi();
            GridFLO.UpdateData();
            fc_LoadFlocage();

        }

        private void supQualite_Click(object sender, EventArgs e)
        {
            if (ssGridQualite.Rows.Count > 0)
            {
                ssGridQualite.DeleteSelectedRows();
                tmpAdorsQualite.Update();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void TimerSSTab1_Tick(object sender, EventArgs e)
        {
            switch (encaissement.SelectedTab.Index)
            {

                case cTypeOperation:
                    // lblGoQualite_DoubleClick(lblGoQualite, new System.EventArgs());
                    fc_AfficheParametre(cTypeOperation);
                    break;
                case cQualite://Tested
                    // lblGoQualiter_DoubleClick(lblGoQualiter, new System.EventArgs());
                    fc_AfficheParametre(cQualite);
                    break;
                case cPersonnel://tested
                    //lblGoPersonnel_DoubleClick(lblGoPersonnel, new System.EventArgs());
                    fc_AfficheParametre(cPersonnel);
                    break;
                case cStatutInterv://tested
                    //lblGoStatutInterv_DoubleClick(lblGoStatutInterv, new System.EventArgs());
                    fc_AfficheParametre(cStatutInterv);
                    break;
                case cTransmission:
                    // lblGoTransmission_DoubleClick(lblGoTransmission, new System.EventArgs());
                    fc_AfficheParametre(cTransmission);
                    break;
                case cStatutCommande://TESTED
                    // lblGoStatutCommande_DoubleClick(lblGoStatutCommande, new System.EventArgs());
                    fc_AfficheParametre(cStatutCommande);
                    break;
                case cAnalytique://tested
                    // Label2_DoubleClick(Label2, new System.EventArgs());
                    fc_AfficheParametre(cAnalytique);
                    break;
                case cCodeEtatDevis://tested
                    fc_AfficheParametre(cCodeEtatDevis);
                    break;
                case cCONTRAT://tsted
                    fc_AfficheParametre(cCONTRAT);
                    break;
                case cParametres://tested
                    fc_AfficheParametre(cParametres);
                    break;
                case cAchatsVentes://tested
                    fc_AfficheParametre(cAchatsVentes);
                    break;
                case cBE://tested
                    fc_ssGridBE();
                    break;
                case cConccurent://tested
                    fc_Concurrent();
                    break;
                case ctomtom://tested
                    fc_LogTomtom();
                    break;
                case cAnalySecteur://tested
                    fc_LoadAnalySecteur();
                    fc_LoadUtilClim();
                    break;
                case cPrime://tested
                    fc_LoadpRIME();
                    break;
                case cVehicule://tested
                    fc_LoadArval();
                    fc_LoadProblemeVehi();
                    fc_LoadFlocage();
                    break;
                case cMobilite://TESTED
                    fc_HistoPDA();
                    break;
                case cUtilisateur://tested
                    fc_UtilisateurWin();
                    break;
                case cModeReglement://tested
                    fc_LoadModeReglement();
                    break;
                //Case cAnalySectCtr
                //    fc_LoadAnalySecteurContrat
                case cModeMomentJour://tested
                    fc_MomentJournee();
                    break;
                case cPeriodeFact:
                    fc_PeriodeCloture();
                    break;
                case cTypeDevis:
                    fc_LoadTypeDedevis();
                    break;
                case cTypeEnc:
                    fc_LoadTypeEncaissement();
                    break;
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                case cTypeCat:
                    fc_LoadCAT();
                    break;
                    //===> Fin Modif Mondir
            }
            TimerSSTab1.Enabled = false;
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_PeriodeCloture()
        {
            string sSQl = "";

            try
            {
                sSQl = "SELECT     PCF_ID, PCF_Periode, PCF_Libelle,PCF_Journal, PCF_Date, PCF_Cloture" + " From PCF_PeriodeClotureFact "
                + " ORDER BY PCF_Periode";

                Cursor = Cursors.WaitCursor;

                rsPCFModAdo = new ModAdo();
                rsPCF = rsPCFModAdo.fc_OpenRecordSet(sSQl, ssGridPCF, "PCF_ID");

                ssGridPCF.DataSource = rsPCF;

                Cursor = Cursors.Default;

                sSQl = "SELECT     JO_Num, JO_Intitule From F_JOURNAUX ORDER BY JO_Num";

                SAGE.fc_OpenConnSage();

                sheridan.InitialiseCombo(this.DropDown0, sSQl, "JO_Num", true, SAGE.adoSage);

                SAGE.fc_CloseConnSage();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_UtilisateurWin()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT   USR_No, USR_Nt, USR_Name " + " From USR_Users " + " ORDER BY USR_Nt";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsUtilWin = new ModAdo();

                rsUtilWin = tmpAdorsUtilWin.fc_OpenRecordSet(sSQL, GridUser, "USR_No");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridUser.DataSource = rsUtilWin;
                GridUser.UpdateData();
                //=== responsable d'exploitation.
                sSQL = "";
                sSQL = "SELECT Matricule, Nom, Prenom";
                sSQL = sSQL + " FROM Personnel Order by matricule ";

                //InitialiseCombo Me.DropPersonnel, Adodc7, _
                //sSQL, "Matricule", True

                //Me.GridUser.Columns("Matricule").DropDownHwnd = Me.DropPersonnel.hwnd

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_UtilisateurWin");
            }


        }

        private void GridUser_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_HistoPDA()
        {
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                sSQL = "SELECT     NbHistoInter, NbHistoDevis From PDA_Param WHERE     (Code = 'HISTO')";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    txtNbHistoInter.Text = General.nz(rs.Rows[0]["NbHistoInter"], 0).ToString();
                    txtNbHistoDevis.Text = General.nz(rs.Rows[0]["NbHistoDevis"], 0).ToString();
                }
                else
                {
                    txtNbHistoInter.Text = "";
                    txtNbHistoDevis.Text = "";
                }

                rs.Dispose();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_PDA");
            }

        }

        private void fc_MajHistoPDA()
        {
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                sSQL = "SELECT     NbHistoInter, NbHistoDevis From PDA_Param  WHERE (Code = 'HISTO')";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    rs.Rows[0]["NbHistoInter"] = General.nz(txtNbHistoInter.Text, 0);
                    rs.Rows[0]["NbHistoDevis"] = General.nz(txtNbHistoDevis.Text, 0);
                }
                else
                {
                    //rs.AddNew();   
                    var NewRow = rs.NewRow();
                    NewRow["NbHistoInter"] = General.nz(txtNbHistoInter.Text, 0);
                    NewRow["NbHistoDevis"] = General.nz(txtNbHistoDevis.Text, 0);
                    NewRow["Code"] = "HISTO";
                    rs.Rows.Add(NewRow);
                }

                tmpAdors.Update();
                rs.Dispose();
                rs = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_PDA");
            }
        }

        private void ssGridUtilClim_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadUtilClim()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT      ANCL_User, '' as Nom" + " FROM         ANCL_AnalyClim";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsUtilClim = new ModAdo();
                rsUtilClim = tmpAdorsUtilClim.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridUtilClim.DataSource = rsUtilClim;
                ssGridUtilClim.UpdateData();
                sSQL = "SELECT    USR_Nt, USR_Name" + " From USR_Users" + " ORDER BY USR_Name";
                sheridan.InitialiseCombo(this.DropUser, sSQL, "USR_Nt", true);
                //this.ssGridUtilClim.Columns["ANCL_User"].DropDownHwnd = this.DropUser.hWnd;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadUtilClim");
            }
        }

        private void fc_LoadAnalySecteur()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT   ANSE_secteur, Matricule, '' as Nom" + " FROM   ANSE_AnalySecteur ";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var tmpAdorsAnalySecteur = new ModAdo();
                rsAnalySecteur = tmpAdorsAnalySecteur.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridAnalySecteur.DataSource = rsAnalySecteur;
                ssGridAnalySecteur.UpdateData();
                //=== responsable d'exploitation.
                sSQL = "";
                sSQL = "SELECT Matricule, Nom, Prenom";
                sSQL = sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
                sSQL = sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
                sSQL = sSQL + " WHERE SpecifQualif.QualifRExp=1";
                sheridan.InitialiseCombo(this.DropExploit, sSQL, "Matricule", true);
                // this.ssGridAnalySecteur.Columns["Matricule"].DropDownHwnd = this.DropExploit.hWnd;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadAnalySecteur");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LogTomtom()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT  TOT_TomtomLogAuto.TOT_Noauto, TOT_TomtomLogAuto.TOT_Vehicule, "
                    + " TOT_TomtomLogAuto.matricule, '' as Nom, '' as  Prenom" + " FROM   TOT_TomtomLogAuto ";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsTomtom = new ModAdo();
                rsTomtom = tmpAdorsTomtom.fc_OpenRecordSet(sSQL, ssGridTomTom, "TOT_Noauto");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridTomTom.DataSource = rsTomtom;
                ssGridTomTom.UpdateData();
                sSQL = "SELECT matricule, Nom, Prenom";
                sSQL = sSQL + " FROM Personnel ";
                sheridan.InitialiseCombo(ssDropTech, sSQL, "matricule", true);
                // this.ssGridTomTom.Columns["matricule"].DropDownHwnd = this.ssDropTech.hWnd;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LogTomtom");
            }

        }

        private void ssGridTomTom_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Concurrent()
        {
            General.sSQL = "SELECT CNC_Noauto,CNC_Code, CNC_Libelle" + " FROM  CNC_Conccurent";
            rsCNC_Conccurent = new DataTable();
            tmpAdorsCNC_Conccurent = new ModAdo();
            rsCNC_Conccurent = tmpAdorsCNC_Conccurent.fc_OpenRecordSet(General.sSQL, ssgCNC_Conccurent, "CNC_Noauto");
            ssgCNC_Conccurent.DataSource = rsCNC_Conccurent;
            ssgCNC_Conccurent.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ssGridBE()
        {
            try
            {
                string sWhere = null;
                General.sSQL = "";
                General.sSQL = "SELECT BE.CodeBE,BE.Libelle,BE.Adresse,Be.CP, BE.Ville, BE.Contact, BE.Tel, BE.Fax, BE.eMail FROM BE  ";
                sWhere = "";
                if (!string.IsNullOrEmpty(txtCodeBE.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE BE.CodeBE LIKE '" + txtCodeBE.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND BE.CodeBE LIKE '" + txtCodeBE.Text.Replace("*", "%") + "'";
                    }
                }


                if (!string.IsNullOrEmpty(txtLibelleBE.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE BE.Libelle LIKE '" + txtLibelleBE.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND BE.Libelle LIKE '" + txtLibelleBE.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtContactBE.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE BE.Contact LIKE '" + txtContactBE.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND BE.Contact LIKE '" + txtContactBE.Text.Replace("*", "%") + "'";
                    }
                }

                General.sSQL = General.sSQL + sWhere + " ORDER BY BE.CodeBE";

                if (rsBE == null)
                {
                    rsBE = new DataTable();
                }

                tmpAdorsBE = new ModAdo();
                rsBE = tmpAdorsBE.fc_OpenRecordSet(General.sSQL, ssGridBE, "CodeBE");
                this.ssGridBE.DataSource = rsBE;
                ssGridBE.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ssGridBE");
            }

        }

        private void TimerSSTab2_Tick(object sender, EventArgs e)
        {
            switch (SSTab2.SelectedTab.Index)
            {
                case cTypeFacture://tested
                    fc_AfficheContrat(cTypeFacture);
                    break;
                case cIndice://tested
                    fc_AfficheContrat(cIndice);
                    break;
                case cTypeRatio://tested
                    fc_AfficheContrat(cTypeRatio);
                    break;
                case cFormule://tested
                    fc_AfficheContrat(cFormule);
                    break;
            }
            TimerSSTab2.Enabled = false;
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="Node"></param>
        private void fc_LoadServiceCpte(UltraTreeNode Node)
        {
            try
            {
                string sSQL = null;
                short i = 0;
                DataTable rs = default(DataTable);
                sIDService = General.Mid(Node.Key, 2, Node.Key.Length - 1);
                lbl2.Text = "Compte de vente du service " + sIDService;
                sSQL = "SELECT     CodeServiceAnalytique, CptVte, '' as libelle " + " From ServiceAnalytiqueCpte "
                    + " WHERE   CodeServiceAnalytique = '" + StdSQLchaine.gFr_DoublerQuote(sIDService) + "'";
                tmpAdorsSrvCpte = new ModAdo();
                rsSrvCpte = tmpAdorsSrvCpte.fc_OpenRecordSet(sSQL);
                GridService.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                GridService.DataSource = rsSrvCpte;
                GridService.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadServiceCpte");
            }

        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="Node"></param>
        private void fc_LoadServiceCTR(UltraTreeNode Node)
        {
            try
            {
                string sSQL = null;
                short i = 0;
                DataTable rs = default(DataTable);
                sIDService = General.Mid(Node.Key, 2, Node.Key.Length - 1);
                lbl2.Text = "Compte de vente du service " + sIDService;
                sSQL = "SELECT     CodeServiceAnalytique, CodeRespExploit, '' as libelle "
                    + " From ServiceAnalytiqueCtr " + " WHERE   CodeServiceAnalytique = '" + StdSQLchaine.gFr_DoublerQuote(sIDService) + "'";
                tmpAdorsAnalySectCtr = new ModAdo();
                rsAnalySectCtr = tmpAdorsAnalySectCtr.fc_OpenRecordSet(sSQL);
                GridService2.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                GridService2.DataSource = rsAnalySectCtr;
                GridService2.UpdateData();
                //=== responsable d'exploitation.
                sSQL = "";
                sSQL = "SELECT Matricule, Nom, Prenom";
                sSQL = sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
                sSQL = sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
                sSQL = sSQL + " WHERE SpecifQualif.QualifRExp=1";

                sheridan.InitialiseCombo(this.DropExploit2, sSQL, "Matricule", true);
                //this.GridService2.Columns["CodeRespExploit"].DropDownHwnd = this.DropExploit2.hWnd;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadServiceCTR");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>       
        private void TreeView2_AfterSelect(object sender, SelectEventArgs e)
        {

            UltraTreeNode Node = TreeView2.ActiveNode;
            Node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            string sID = null;
            sID = General.Mid(Node.Key, 2, Node.Key.Length - 1);


            switch (General.Left(Node.Key, 1))
            {
                case cNiv1:
                    if (OptGroupe.Checked)
                    {
                        return;
                    }
                    else
                    {
                        fc_InfoUtil(sID);
                    }
                    break;
                case cNiv2:
                    fc_InfoUtil(sID);
                    break;
            }
            return;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///       
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode node = TreeView1.ActiveNode;
            node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            fc_AffDroit(node);
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="Node"></param>
        private void fc_AffDroit(UltraTreeNode Node)
        {
            string sID = null;
            sID = Node.Text;

            switch (General.Left(Node.Key, 1))
            {
                case cNiv1://tested
                    groupBox2.Visible = true;
                    lblNomFiche.Visible = true;

                    lblNomFiche.Text = "FICHE " + sID;
                    fraAddDroit.Visible = false;
                    chkMDP.Visible = true;
                    txtFicheMDP.Visible = true;
                    fc_afficheDroit(General.Mid(Node.Key, 2, Node.Key.Length - 1).ToString());
                    cmdAddDroitFiche.Visible = true;
                    break;
                case cNiv2://tested
                    if (chkMDP.CheckState == CheckState.Checked)//tested
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez coché un accés par mot de passe," + " vous ne pouvez pas acceder à cette partie", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else//tested
                    {

                        lblNomFiche.Visible = true;
                        fraAddDroit.Visible = true;
                        lblGroupe.Text = "Groupe => " + sID;
                        chkMDP.Visible = false;
                        txtFicheMDP.Visible = false;
                        cmdAddDroitFiche.Visible = false;
                        fc_AffChkDroit();
                    }
                    break;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_AffChkDroit()
        {
            try
            {
                string sSQL = null;
                //System.Windows.Forms.TreeNode Node = null;
                string sDROF_NomFicheReel = null;
                string sDROG_Nom = null;
                DataTable rs = default(DataTable);
                int i = 0;
                int j = 0;


                foreach (UltraTreeNode Node in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode child in Node.Nodes)
                    {
                        if (child.Selected)//tested
                        {
                            sDROF_NomFicheReel = General.Mid(child.Parent.Key, 2, child.Parent.Key.Length - 1);
                            // sDROG_Nom = Mid(Node.Key, 2, Len(Node.Key) - 1)

                            i = child.Key.IndexOf("@") + 1;
                            j = child.Key.Length;
                            string cc = child.Text;
                            sDROG_Nom = General.Mid(child.Key, i + 1, j - i);
                            break;
                        }
                    }
                }

                sSQL = "SELECT   distinct     " + "   DROA_Lecture, DROA_Modif, DROA_Supp, DROA_Ajout" + " From DROA_Administration"
                    + " WHERE     DROF_NomFicheReel ='" + sDROF_NomFicheReel + "'" + " AND DROG_Nom = '" + sDROG_Nom + "'";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)//tested
                {
                    chkLecture.Checked = General.nz(rs.Rows[0]["DROA_Lecture"], 0).ToString() == "1";
                    chkModification.Checked = General.nz(rs.Rows[0]["DROA_Modif"], 0).ToString() == "1";
                    chkSuppression.Checked = General.nz(rs.Rows[0]["DROA_Supp"], 0).ToString() == "1";
                    chkAjout.Checked = General.nz(rs.Rows[0]["DROA_Ajout"], 0).ToString() == "1";
                }
                else//tested
                {
                    chkLecture.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    chkModification.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    chkSuppression.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    chkAjout.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_AffChkDroit");
            }


        }
        private void fc_afficheDroit(string sNomFicheReel)
        {
            try
            {
                string sSQL = null;
                DataTable rs = default(DataTable);
                sSQL = "SELECT     DROF_NomFicheReel, DROF_MDP, DROF_MdpOuUtil" + " From DROF_DroitFiche"
                    + " WHERE     (DROF_NomFicheReel = '" + sNomFicheReel + "')";
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    if (Convert.ToInt16(General.nz(rs.Rows[0]["DROF_MdpOuUtil"], 0)) == 1)//tested
                    {
                        chkMDP.CheckState = System.Windows.Forms.CheckState.Checked;
                        txtFicheMDP.Text = rs.Rows[0]["DROF_MDP"] + "";
                    }
                    else//tested
                    {
                        chkMDP.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    }
                    chkMDP_CheckedChanged(chkMDP, new System.EventArgs());
                }

                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_AffChkDroit");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sID"></param>
        private void fc_InfoUtil(string sID)
        {
            try
            {
                DataTable rs = default(DataTable);
                string sSQL = null;
                sSQL = "SELECT     USR_No, USR_Nt, USR_Name, DROG_Nom" + " From USR_Users" + " WHERE    USR_No = " + sID;
                var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    txtUSR_Nt.Text = rs.Rows[0]["USR_Nt"] + "";
                    txtUSR_Name.Text = rs.Rows[0]["USR_Name"] + "";
                    txtDROG_Nom.Text = rs.Rows[0]["DROG_Nom"] + "";

                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_AffChkDroit");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeBE_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_ssGridBE();
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtContactBE_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_ssGridBE();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrilleParametres();
            }
        }

        private void txtFiltreInit_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrillePersonnel();
            }
        }

        private void txtFiltreMatricule_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrillePersonnel();
            }
        }

        private void txtFiltreNom_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrillePersonnel();
            }
        }

        private void txtFiltreQualif_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrillePersonnel();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreValeur_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_GrilleParametres();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibelleBE_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                fc_ssGridBE();
            }
        }

        private void UserDocParametre_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            ssGridPersonnel.DisplayLayout.LoadStyle = LoadStyle.LoadOnDemand;
            View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocParamtre");
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

            General.open_conn();
            encaissement.SelectedTab = encaissement.Tabs[0];
            SSTab2.SelectedTab = SSTab2.Tabs[0];

            /* if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
             {
                 imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                 //    If IsNumeric(imgTop) Then
                 //        imgLogoSociete.Top = imgTop
                 //    End If
                 //    If IsNumeric(imgLeft) Then
                 //        imgLogoSociete.Left = imgLeft
                 //    End If
             }

             if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
             {
                 lblNomSociete.Text = General.NomSousSociete;
                 System.Windows.Forms.Application.DoEvents();
             }
             */
            fc_VisibleFalseTab();

            switch (General.ParamVisu)//Paramvisu tjr null
            {

                case "Type d'operation":
                    //lblGoQualite_DoubleClick(lblGoQualite, new System.EventArgs());
                    fc_AfficheParametre(cTypeOperation);
                    break;

                case "Qualite":
                    // lblGoQualiter_DoubleClick(lblGoQualiter, new System.EventArgs());
                    fc_AfficheParametre(cQualite);
                    break;

                case "Personnel-Secteur":
                    //lblGoPersonnel_DoubleClick(lblGoPersonnel, new System.EventArgs());
                    fc_AfficheParametre(cPersonnel);
                    break;

                case "Statut d'intervention":
                    fc_AfficheParametre(cStatutInterv);
                    // lblGoStatutInterv_DoubleClick(lblGoStatutInterv, new System.EventArgs());
                    break;

                case "Transmission":
                    fc_AfficheParametre(cTransmission);
                    // lblGoTransmission_DoubleClick(lblGoTransmission, new System.EventArgs());
                    break;

                case "Statut bon de commande":
                    fc_AfficheParametre(cStatutCommande);
                    // lblGoStatutCommande_DoubleClick(lblGoStatutCommande, new System.EventArgs());
                    break;

                case "Analytique":
                    fc_AfficheParametre(cAnalytique);
                    //Label2_DoubleClick(Label2, new System.EventArgs());
                    break;

                case "Contrat Type Facture":
                    encaissement.SelectedTab = encaissement.Tabs[cCONTRAT];
                    fc_AfficheContrat(cTypeFacture);
                    break;

                case "Contrat Indice":
                    encaissement.SelectedTab = encaissement.Tabs[cCONTRAT];
                    fc_AfficheContrat(cIndice);
                    break;

                case "Contrat Type Ratio":
                    encaissement.SelectedTab = encaissement.Tabs[cCONTRAT];
                    fc_AfficheContrat(cTypeRatio);
                    break;

                case "Contrat Formule":
                    encaissement.SelectedTab = encaissement.Tabs[cCONTRAT];
                    fc_AfficheContrat(cFormule);
                    break;

            }

            if (string.IsNullOrEmpty(General.ParamVisu))
            {
                encaissement.SelectedTab = encaissement.Tabs[0];
                sstab1_SelectedTabChanged(encaissement, null);
            }

            General.ParamVisu = "";
            //== applique les droits par fiche.
            ModAutorisation.fc_DroitParFiche(this.Name);
            SSTab3.Visible = false;

            /*lblNavigation[0].Text = General.sNomLien0;
            lblNavigation[1].Text = General.sNomLien1;
            lblNavigation[2].Text = General.sNomLien2;
            */

        }
        private void fc_AfficheParametre(int lOnglet)
        {
            fc_VisibleFalseTab();
            encaissement.Visible = false;
            cmdMAJ.Visible = true;
            switch (lOnglet)
            {
                case cQualite://Tested
                    encaissement.Tabs[cQualite].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cQualite];
                    fc_GrilleQualite();
                    break;
                case cPersonnel://tested
                    encaissement.Tabs[cPersonnel].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cPersonnel];
                    fc_GrillePersonnel();
                    break;
                case cTypeOperation:
                    encaissement.Tabs[cTypeOperation].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cTypeOperation];
                    fc_GrillessCAI_CategoriInterv();
                    break;
                //                    fc_GrillessFacArticle Me.ssCAI_CategoriInterv.Columns("CAI_NoAuto").value
                //                    fc_dropcompte
                case cStatutInterv:
                    encaissement.Tabs[cStatutInterv].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cStatutInterv];
                    fc_GrilleStatutInterv();
                    break;
                case cTransmission:
                    encaissement.Tabs[cTransmission].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cTransmission];
                    fc_GrilleTransmission();
                    break;
                case cStatutCommande://tESTED
                    encaissement.Tabs[cStatutCommande].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cStatutCommande];
                    fc_GrilleStatutCommande();
                    break;
                case cAnalytique://tested
                    encaissement.Tabs[cAnalytique].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cAnalytique];
                    fc_grilleAnalytique();
                    break;
                case cCodeEtatDevis://tested
                    encaissement.Tabs[cCodeEtatDevis].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cCodeEtatDevis];
                    fc_GrilleCodeEtatDevis();
                    break;
                case cCONTRAT://teste
                    encaissement.Tabs[cTypeFacture].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cCONTRAT];
                    TimerSSTab2.Enabled = true;
                    break;
                //                    SSTab2.Tab = cTypeFacture
                //                    fc_GrilleTypeFacture
                case cParametres://tested
                    encaissement.Tabs[cParametres].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cParametres];
                    fc_GrilleParametres();
                    break;
                case cAchatsVentes://tested
                    encaissement.Tabs[cAchatsVentes].Visible = true;
                    encaissement.SelectedTab = encaissement.Tabs[cAchatsVentes];
                    sub_GrilleAchatVente();
                    break;
            }
            encaissement.Visible = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void sub_GrilleAchatVente()
        {
            General.sSQL = "";
            General.sSQL = "SELECT CleAuto, PalierAchat, a, b, AfficherFormule " + " FROM ConvertAchatVente ";
            General.sSQL = General.sSQL + " ORDER BY PalierAchat";

            if (rsAchatsVentes == null)
            {
                rsAchatsVentes = new DataTable();
            }
            else
            {
                if (General.adocnn.State == ConnectionState.Open)
                {
                    General.adocnn.Close();
                }
            }
            tmpAdorsAchatsVentes = new ModAdo();
            rsAchatsVentes = tmpAdorsAchatsVentes.fc_OpenRecordSet(General.sSQL, GridAchatVente, "CleAuto");
            this.GridAchatVente.DataSource = rsAchatsVentes;
            GridAchatVente.UpdateData();
            //this.GraphAchatVente.DoSetCursor = true;
            sub_AfficheGraph();//tested
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_GrilleParametres()
        {
            string sqlWhere = null;
            General.sSQL = "";
            if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                General.sSQL = "SELECT  CodeUO,Code, Adresse, NomBase, TableLien, ChampLien, ToolTipText, '' AS AideLien " + " FROM LienV2 Where Masquer=0 ";
            else
                General.sSQL = "SELECT  CodeUO,Code, Adresse, NomBase, TableLien, ChampLien, ToolTipText, '' AS AideLien " + " FROM Lien Where Masquer=0 ";


            if (!string.IsNullOrEmpty(txtFiltreCode.Text))
            {
                sqlWhere = " AND Code like '%" + txtFiltreCode.Text + "%'";
            }

            if (!string.IsNullOrEmpty(txtFiltreValeur.Text))
            {
                sqlWhere = sqlWhere + " AND Adresse like '%" + txtFiltreValeur.Text + "%'";
            }

            General.sSQL = General.sSQL + sqlWhere + " ORDER BY Code";
            rsParametres = new DataTable();
            tmpAdorsParametres = new ModAdo();
            rsParametres = tmpAdorsParametres.fc_OpenRecordSet(General.sSQL);

            this.GridParametres.DataSource = rsParametres;
            GridParametres.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lOnglet"></param>
        private void fc_AfficheContrat(int lOnglet)
        {
            //fc_VisibleFalseTab
            SSTab2.Visible = false;
            cmdMAJ.Visible = true;
            switch (lOnglet)
            {
                case cTypeFacture://tested
                    SSTab2.Tabs[cTypeFacture].Visible = true;
                    SSTab2.SelectedTab = SSTab2.Tabs[cTypeFacture];
                    fc_GrilleTypeFacture();
                    break;
                case cIndice://tested
                    SSTab2.Tabs[cIndice].Visible = true;
                    SSTab2.SelectedTab = SSTab2.Tabs[cIndice];
                    fc_GrilleIndice();
                    break;
                case cTypeRatio://tested
                    SSTab2.Tabs[cTypeRatio].Visible = true;
                    SSTab2.SelectedTab = SSTab2.Tabs[cTypeRatio];
                    fc_GrilleTypeRatio();
                    break;
                case cFormule://tested
                    SSTab2.Tabs[cFormule].Visible = true;
                    SSTab2.SelectedTab = SSTab2.Tabs[cFormule];
                    fc_GrilleFormule();
                    break;

            }
            SSTab2.Visible = true;
        }


        private void fc_dropcompte()
        {
            General.sSQL = "";
            // sSQL = "SELECT F_COMPTEG.CG_NUM AS [Code comptable]," _
            //& " F_COMPTEG.CG_INTITULE AS Intitulé," _
            //& " F_TAXE.CG_NUM, F_TAXE.TA_TAUX " _
            //& " FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN " _
            //& " F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
            //& " WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 )  "

            General.sSQL = "SELECT F_COMPTEG.CG_NUM AS [Code comptable]," + " F_COMPTEG.CG_INTITULE AS Intitulé,"
                        + " F_TAXE.CG_NUM, F_TAXE.TA_TAUX " + " FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN "
                        + " F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM"
                        + " WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 )  ";

            if (ssDropCompte.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropCompte, General.sSQL, "Code comptable", true);
            }
            //this.ssFacArticle.Columns["cg_Num"].DropDownHwnd = this.ssDropCompte.hWnd;
            //this.ssFacArticle.Columns["cg_Num2"].DropDownHwnd = this.ssDropCompte.hWnd;

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_grilleAnalytique()
        {
            //j'ai ajouter la colonne cbMarq(clé primaire) et  N_ANALYTIQUE(cette colone n'accepte pas la valeur=Null

            //===@@@ modif du 29 05 2017, desactive Sage.
            if (General.sDesActiveSage == "1")
            {

            }
            else
            {
                SAGE.fc_OpenConnSage();
                General.sSQL = "";
                General.sSQL = "SELECT cbMarq,CA_Num, CA_Intitule,N_ANALYTIQUE, ActiviteDevis, ActiviteTravaux, ActiviteContrat " + " FROM F_CompteA WHERE N_ANALYTIQUE='1'";
                rsAnaActivite = new DataTable();

                SDArsAnaActivite = new SqlDataAdapter(General.sSQL, SAGE.adoSage);
                SCBrsAnaActivite = new SqlCommandBuilder(SDArsAnaActivite);
                SDArsAnaActivite.SelectCommand = SCBrsAnaActivite.DataAdapter.SelectCommand;
                SDArsAnaActivite.UpdateCommand = SCBrsAnaActivite.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)SCBrsAnaActivite.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                SDArsAnaActivite.InsertCommand = insertCmd;
                SDArsAnaActivite.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {
                    if (e.StatementType == StatementType.Insert)
                    {
                        if (ssGridAnaActivite.ActiveRow != null)
                        {
                            ssGridAnaActivite.ActiveRow.Cells["cbMarq"].Value = e.Command.Parameters["@ID"].Value;

                        }

                    }
                });


                SDArsAnaActivite.Fill(rsAnaActivite);
                this.ssGridAnaActivite.DataSource = rsAnaActivite;
                ssGridAnaActivite.UpdateData();
                SAGE.fc_OpenConnSage();
            }

            //sSQl = ""
            //sSQl = "SELECT Code" _
            //'        & " FROM ANA_Analytique"
            //Set rsAnaCode = New ADODB.Recordset
            //rsAnaCode.Open sSQl, adocnn, adOpenKeyset, adLockOptimistic
            //Me.ssGridAna.ReBind

            //fc_CloseConnSage
        }
        /// <summary>
        /// tESTED
        /// </summary>
        private void fc_GrilleStatutCommande()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Code, Libelle" + " FROM TypeEtatCommande";
            rsStatutCommande = new DataTable();
            tmpAdorsStatutCommande = new ModAdo();
            rsStatutCommande = tmpAdorsStatutCommande.fc_OpenRecordSet(General.sSQL);
            this.ssGridStatutCommande.DataSource = rsStatutCommande;
            ssGridStatutCommande.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_VisibleFalseTab()
        {
            encaissement.Tabs[cQualite].Visible = true;
            encaissement.Tabs[cPersonnel].Visible = true;
            encaissement.Tabs[cTypeOperation].Visible = false;
            encaissement.Tabs[cStatutInterv].Visible = true;
            encaissement.Tabs[cTransmission].Visible = false;
            encaissement.Tabs[cStatutCommande].Visible = true;
            encaissement.Tabs[cAnalytique].Visible = true;
            encaissement.Tabs[cCodeEtatDevis].Visible = true;
            encaissement.Tabs[cTypeFacture].Visible = true;
            encaissement.Tabs[cParametres].Visible = true;
            encaissement.Tabs[cBE].Visible = true;
            encaissement.Visible = true;
            encaissement.TabsPerRow = 11;

        }
        /// <summary>
        /// tESTED
        /// </summary>
        private void fc_GrilleTransmission()
        {
            General.sSQL = "";
            General.sSQL = "SELECT CodeTransmission_MOT, Libelle_MOT" + " FROM MOT_TRANSMISSION";
            rsTransmission = new DataTable();
            tmpAdorsTransmission = new ModAdo();
            rsTransmission = tmpAdorsTransmission.fc_OpenRecordSet(General.sSQL);
            this.ssGridTransmission.DataSource = rsTransmission;
            ssGridTransmission.UpdateData();
        }
        /// <summary>
        /// tSSED
        /// </summary>
        private void fc_GrilleStatutInterv()
        {
            bool bAdd = false;
            bool bUpdate = false;
            bool bDelete = false;

            General.sSQL = "";
            General.sSQL = "SELECT CodeEtat, LibelleCodeEtat, Texte " + " FROM TypeCodeEtat";
            rsStatutInterv = new DataTable();
            tmpAdorsStatutInterv = new ModAdo();
            rsStatutInterv = tmpAdorsStatutInterv.fc_OpenRecordSet(General.sSQL);
            this.ssGrilleStatutInterv.DataSource = rsStatutInterv;
            ssGrilleStatutInterv.UpdateData();
            //=== modif pour pezant.
            if (General.sActiveVersionPz == "1")
            {
                ModAutorisation.fc_Droit(Variable.cStatutIntervention, Variable.cUserDocParamtre, ref bAdd, ref bUpdate, ref bDelete);
                if (bAdd == false)
                {
                    ssGrilleStatutInterv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    ssGrilleStatutInterv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                }
                else
                {
                    ssGrilleStatutInterv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    ssGrilleStatutInterv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                }
            }

        }
        /// <summary>
        /// tESTED
        /// </summary>
        private void fc_GrillePersonnel()
        {
            string sqlWhere = null;
            General.sSQL = "";
            General.sSQL = "SELECT Matricule, Nom, Prenom, Initiales, ActiverPDA, CodeQualif ,  CRespExploit,"
                        + " Commercial, DevRadiateur, DevExploit, RenovChauffage1, RenovChauffage2,Chauffagiste1, "
                        + " Chauffagiste2, CSecteur, CSecteur2, "
                        + " Dep2,Rondier,RespService, NumRadio, MemoGuard,Email, PouvoirAchat, Login, SousTraitant, NePasAfficherOcean "
                        + " , EnvoieMail, LogTomTom, ActiveTomTom, NonActif, Dispatcheur, "
                        + "SCH_ServiceCtrlHeure.SCH_Code, SCH_ServiceCtrlHeure.SCH_Libelle as Libelle, Dispatcheur"
                       + "  FROM Personnel"
                     + " LEFT JOIN SCH_ServiceCtrlHeure ON SCH_ServiceCtrlHeure.SCH_Code = Personnel.SCH_Code";

            if (!string.IsNullOrEmpty(txtFiltreMatricule.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Personnel.Matricule LIKE '" + txtFiltreMatricule.Text.Replace("*", "%") + "'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Personnel.Matricule LIKE '" + txtFiltreMatricule.Text.Replace("*", "%") + "'";
                }
            }

            if (!string.IsNullOrEmpty(txtFiltreNom.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Personnel.Nom LIKE '%" + txtFiltreNom.Text.Replace("*", "%") + "%'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Personnel.Nom LIKE '%" + txtFiltreNom.Text.Replace("*", "%") + "%'";
                }
            }

            if (!string.IsNullOrEmpty(txtFiltreQualif.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Personnel.CodeQualif LIKE '%" + txtFiltreQualif.Text.Replace("*", "%") + "%'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Personnel.CodeQualif LIKE '%" + txtFiltreQualif.Text.Replace("*", "%") + "%'";
                }
            }

            if (!string.IsNullOrEmpty(txtFiltreInit.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = " WHERE Personnel.Initiales LIKE '%" + txtFiltreInit.Text.Replace("*", "%") + "%'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND Personnel.Initiales LIKE '%" + txtFiltreInit.Text.Replace("*", "%") + "%'";
                }
            }

            ///===============================> Code Commenté par Mondir, Ajouté une classe de compare ' MySortComparer '
            //string orderBy = " ORDER BY  case IsNumeric(Matricule)  when 1 then Replicate('0', 100 - Len(Matricule)) + Matricule else Matricule end ";

            rsPersonnel = new DataTable();
            tmpAdorsPersonnel = new ModAdo();
            rsPersonnel = tmpAdorsPersonnel.fc_OpenRecordSet(General.sSQL + sqlWhere /*+ orderBy*/);
            ssGridPersonnel.Visible = false;

            this.ssGridPersonnel.DataSource = rsPersonnel;
            this.ssGridPersonnel.UpdateData();
            ssGridPersonnel.Visible = true;

            General.sSQL = "";
            General.sSQL = "SELECT codeQualif, Qualification" + " FROM Qualification";
            sheridan.InitialiseCombo(this.ssDropQualite, General.sSQL, "CodeQualif", true);//tested
            //this.ssGridPersonnel.Columns["CodeQualif"].DropDownHwnd = this.ssDropQualite.hWnd;todo ajouter le dans initialzelayout

            //=== responsable d'exploitation.
            General.sSQL = "";
            General.sSQL = "SELECT Initiales, Nom, Prenom";
            General.sSQL = General.sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
            General.sSQL = General.sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
            General.sSQL = General.sSQL + " WHERE SpecifQualif.QualifRExp=1";
            sheridan.InitialiseCombo((this.SSdropRespExploit), General.sSQL, "Initiales", true);//tested
            //this.ssGridPersonnel.Columns["RespExploit"].DropDownHwnd = this.SSdropRespExploit.hWnd;todo ajouter le dans initialzelayout


            //=== commercial.
            General.sSQL = "";
            General.sSQL = "SELECT Initiales, Nom, Prenom";
            General.sSQL = General.sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
            General.sSQL = General.sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
            General.sSQL = General.sSQL + " WHERE SpecifQualif.QualifCom=1";
            sheridan.InitialiseCombo((this.ssDropCommercial), General.sSQL, "Initiales", true);//Tested
            // this.ssGridPersonnel.Columns["commercial"].DropDownHwnd = this.ssDropCommercial.hWnd;todo ajouter le dans initialzelayout

            //=== deviseur radiateur.
            General.sSQL = "";
            General.sSQL = "SELECT Initiales, Nom, Prenom";
            General.sSQL = General.sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
            General.sSQL = General.sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
            // sSQL = sSQL & " WHERE SpecifQualif.QualifCom=1"
            General.sSQL = General.sSQL + " ORDER BY Personnel.Initiales";

            sheridan.InitialiseCombo((this.ssDropDevRadia), General.sSQL, "Initiales", true);//tested
            //this.ssGridPersonnel.Columns["DevRadiateur"].DropDownHwnd = this.ssDropDevRadia.hWnd;todo ajouter le dans initialzelayout

            //=== devis expoitation.
            sheridan.InitialiseCombo((this.ssDropDevExploit), General.sSQL, "Initiales", true);//Tested
            //this.ssGridPersonnel.Columns["DevExploit"].DropDownHwnd = this.ssDropDevExploit.hWnd;todo ajouter le dans initialzelayout

            //=== renovation chauffage 1.
            sheridan.InitialiseCombo((this.ssDropRenov1), General.sSQL, "Initiales", true);//tested
            //this.ssGridPersonnel.Columns["RenovChauffage1"].DropDownHwnd = this.ssDropRenov1.hWnd;todo ajouter le dans initialzelayout

            //=== renovation chauffage 2.
            sheridan.InitialiseCombo((this.ssDropRenov2), General.sSQL, "Initiales", true);//tested
            // this.ssGridPersonnel.Columns["RenovChauffage2"].DropDownHwnd = this.ssDropRenov2.hWnd;todo ajouter le dans initialzelayout

            //=== modif du 12 03 2019.
            sheridan.InitialiseCombo((this.ssDropRenov2), General.sSQL, "Initiales", true);

            //=== chauffagiste 1.
            sheridan.InitialiseCombo((this.ssDropChauff1), General.sSQL, "Initiales", true);//tested
            //this.ssGridPersonnel.Columns["Chauffagiste1"].DropDownHwnd = this.ssDropChauff1.hWnd;todo ajouter le dans initialzelayout

            //=== chauffagiste 2.
            sheridan.InitialiseCombo((this.ssDropChauff2), General.sSQL, "Initiales", true);//tested
                                                                                            //this.ssGridPersonnel.Columns["Chauffagiste2"].DropDownHwnd = this.ssDropChauff2.hWnd;todo ajouter le dans initialzelayout

            //'=== Chef de secteur                                                                             
            sheridan.InitialiseCombo(SSdropSecteur, General.sSQL, "Initiales", true);//tested

            //'=== Chef de secteur  2                                                                           
            sheridan.InitialiseCombo(SSdropSecteur, General.sSQL, "Initiales", true);

            //=== renovation chauffage 2.
            sheridan.InitialiseCombo((this.ssDropRespService), General.sSQL, "Initiales", true);//tested
            // this.ssGridPersonnel.Columns["RespService"].DropDownHwnd = this.ssDropRespService.hWnd;todo ajouter le dans initialzelayout

            //=== rondier et depanneur 2.
            General.sSQL = "SELECT Personnel.Initiales, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.Qualification, SpecifQualif.QualifTech"
                + " FROM         Qualification RIGHT OUTER JOIN"
                + " SpecifQualif ON Qualification.CodeQualif = SpecifQualif.CodeQualif RIGHT OUTER JOIN"
                + " Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                + " Where (SpecifQualif.QualifTech = 1)" + " ORDER BY Personnel.Initiales";

            sheridan.InitialiseCombo((this.ssDropTechnicien), General.sSQL, "Initiales", true);//tested
            //this.ssGridPersonnel.Columns["DEP2"].DropDownHwnd = this.ssDropTechnicien.hWnd;todo ajouter le dans initialzelayout
            // this.ssGridPersonnel.Columns["Rondier"].DropDownHwnd = this.ssDropTechnicien.hWnd;todo ajouter le dans initialzelayout

            General.sSQL = "SELECT     SCH_Code as code, SCH_Libelle as Libelle  From SCH_ServiceCtrlHeure  ORDER BY SCH_Code";

            //=== code service pour controle heure.
            sheridan.InitialiseCombo((this.ssDropService), General.sSQL, "code", true);//tested
            // this.ssGridPersonnel.Columns["SCH_Code"].DropDownHwnd = this.ssDropService.hWnd;todo ajouter le dans initialzelayout

            General.sSQL = "SELECT     USR_Nt as [LogIn], USR_Name as [Nom] " + " From USR_Users " + " ORDER BY USR_Nt";


            //=== code service pour controle heure.
            sheridan.InitialiseCombo((this.DropUsersWindows), General.sSQL, "LogIn", true);//tested
                                                                                           //this.ssGridPersonnel.Columns["Login"].DropDownHwnd = this.DropUsersWindows.hWnd;todo ajouter le dans initialzelayout



        }
        private void fc_Secteur()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Sec_Code, Sec_Libelle  FROM SEC_Secteur";
            sheridan.InitialiseCombo((this.SSdropSecteur), General.sSQL, "Sec_Code", true);
        }
        private void fc_GrilleQualite()
        {
            General.sSQL = "";
            General.sSQL = "SELECT     CodeQualif, Qualification, Correspondant_COR" + ", Facturable_QUA, PDA" + " FROM Qualification";
            rsQualite = new DataTable();
            tmpAdorsQualite = new ModAdo();
            rsQualite = tmpAdorsQualite.fc_OpenRecordSet(General.sSQL);
            this.ssGridQualite.DataSource = rsQualite;
            ssGridQualite.UpdateData();
            General.sSQL = "";
            General.sSQL = "SELECT codeQualif, Qualification" + " FROM Qualification";
            sheridan.InitialiseCombo((this.SSDropQualif), General.sSQL, "CodeQualif", true);
            // this.GridSpecifQualif.Columns["Code Qualification"].DropDownHwnd = this.SSDropQualif.hWnd;todo ajouter le dans initialzelayout

            General.sSQL = "";
            General.sSQL = "SELECT CodeQualif,libelleQualif,QualifTech,QualifCom," + "QualifDisp,QualifDev,QualifAdmin,QualifRTrav,QualifRExp,QualifPart,"
                        + " QualifSousTraitant, QualifSpecialiste " + " FROM SpecifQualif";
            rsSpecifQualif = new DataTable();
            tmpAdorsSpecifQualif = new ModAdo();
            rsSpecifQualif = tmpAdorsSpecifQualif.fc_OpenRecordSet(General.sSQL);
            this.GridSpecifQualif.DataSource = rsSpecifQualif;
            GridSpecifQualif.UpdateData();

        }
        private void fc_GrilleCodeEtatDevis()
        {
            bool bAdd = false;
            bool bUpdate = false;
            bool bDelete = false;
            General.sSQL = "";
            General.sSQL = "SELECT Code,Libelle,CodeEtatManuel,ApImprime  " + " FROM DevisCodeEtat";
            /* if ((rsCodeEtatDevis != null))
             {
                 if (General.adocnn.State == ConnectionState.Open)
                 {
                             General.adocnn.Close();
                 }
             }
             else
             {
                 rsCodeEtatDevis = new DataTable();
             }*/
            tmpAdorsCodeEtatDevis = new ModAdo();
            rsCodeEtatDevis = tmpAdorsCodeEtatDevis.fc_OpenRecordSet(General.sSQL);
            this.GridCodeEtatDevis.DataSource = rsCodeEtatDevis;
            GridCodeEtatDevis.UpdateData();
            //=== modif pour pezant.
            if (General.sActiveVersionPz == "1")
            {
                ModAutorisation.fc_Droit(Variable.cStatutDevis, Variable.cUserDocParamtre, ref bAdd, ref bUpdate, ref bDelete);
                if (bAdd == false)
                {
                    GridCodeEtatDevis.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    GridCodeEtatDevis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                }
                else
                {
                    GridCodeEtatDevis.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    GridCodeEtatDevis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                }
            }

            GridCodeEtatDevis.Visible = false;
            GridCodeEtatDevis.Visible = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_GrilleTypeFacture()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Code,Libelle,Libelle2" + " FROM TypeFacture";
            rsTypeFacture = new DataTable();
            tmpAdorsTypeFacture = new ModAdo();
            rsTypeFacture = tmpAdorsTypeFacture.fc_OpenRecordSet(General.sSQL);
            this.GridTypeFacture.DataSource = rsTypeFacture;
            GridTypeFacture.UpdateData();
            GridTypeFacture.Visible = false;
            GridTypeFacture.Visible = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_GrilleIndice()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Libelle,[date],Valeur,Connu,Reel" + " FROM FacIndice";
            rsIndice = new DataTable();
            tmpAdorsIndice = new ModAdo();
            rsIndice = tmpAdorsIndice.fc_OpenRecordSet(General.sSQL);
            GridIndice.Visible = false;
            this.GridIndice.DataSource = rsIndice;
            GridIndice.UpdateData();
            GridIndice.Visible = true;
        }
        /// <summary>
        /// TEsted
        /// </summary>
        private void fc_GrilleTypeRatio()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Code,Libelle,Libelle2" + " FROM TypeRatio";
            rsTypeRatio = new DataTable();
            tmpAdorsTypeRatio = new ModAdo();
            rsTypeRatio = tmpAdorsTypeRatio.fc_OpenRecordSet(General.sSQL);
            this.GridTypeRatio.DataSource = rsTypeRatio;
            GridTypeRatio.Visible = false;
            GridTypeRatio.Visible = true;
        }
        /// <summary>
        /// TEsted
        /// </summary>
        private void fc_GrilleFormule()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Type,Libelle" + " FROM FacFormule";
            rsFormule = new DataTable();
            tmpAdorsFormule = new ModAdo();
            rsFormule = tmpAdorsFormule.fc_OpenRecordSet(General.sSQL);
            this.GridFormule.DataSource = rsFormule;
            GridFormule.UpdateData();
            GridFormule.Visible = false;
            GridFormule.Visible = true;
        }

        private void fc_GrillessCAI_CategoriInterv()
        {
            // charge le grille ssCAI_CategoriInterv avec la table CAI_CategoriInterv.
            General.sSQL = "";
            General.sSQL = "SELECT CAI_CategoriInterv.CAI_Code, CAI_CategoriInterv.CAI_Libelle" + " , CAI_NoAuto" + " FROM CAI_CategoriInterv order by CAI_Code";
            rsCAICategoriInterv = new DataTable();
            tmpAdorsCAICategoriInterv = new ModAdo();
            rsCAICategoriInterv = tmpAdorsCAICategoriInterv.fc_OpenRecordSet(General.sSQL);
            this.ssCAI_CategoriInterv.DataSource = rsCAICategoriInterv;
            ssCAI_CategoriInterv.UpdateData();
        }
        private void fc_GrillessFacArticle(string sCode)
        {
            // alimente la grille ssFacArticle en relation avec la grille ssCAI_CategoriInterv


            if (string.IsNullOrEmpty(sCode))
            {
                // permet de vider la grille
                sCode = "0";
            }
            General.sSQL = "SELECT ACT_Code, ACT_Libelle" + " FROM  ACT_AnaActivite order by ACT_Code";
            sheridan.InitialiseCombo((this.ssDropACT_AnaActivite), General.sSQL, "ACT_Code", true);
            //this.ssFacArticle.Columns["ACT_Code"].DropDownHwnd = this.ssDropACT_AnaActivite.hWnd;
            General.sSQL = "";
            General.sSQL = "SELECT CodeArticle , CAI_Noauto, Designation1 , ACT_Code ,"
                        + " FAC_TransKobby, FAC_TransDispapt, FAC_AttentMat, FAC_Contratuel,"
                        + " FAC_Facturable, " + " FAC_NonFacturable," + "  FacArticle.cg_Num, FacArticle.cg_Num2,"
                        + " FacArticle.PrixAchat ," + " FacArticle.Designation2,FacArticle.Designation3,"
                        + " FacArticle.Designation4, FacArticle.Designation5," + " FacArticle.Designation6, FacArticle.Designation7,"
                        + " FacArticle.Designation8, FacArticle.Designation9," + " FacArticle.Designation10" + " FROM FacArticle"
                        + " where CAI_NoAuto ='" + sCode + "' order by CodeArticle";

            //-- alimante le dropdown avec la table concernant l'activité
            rsFacArticle = new DataTable();
            var tmpAdorsFacArticle = new ModAdo();
            rsFacArticle = tmpAdorsFacArticle.fc_OpenRecordSet(General.sSQL);
            this.ssFacArticle.DataSource = rsFacArticle;
            ssFacArticle.UpdateData();
        }

        private void ssFacArticle_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssGridQualite_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void ssGridPersonnel_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                string reqDelete = $"delete from Personnel where Matricule='{ssGridPersonnel.ActiveRow.Cells["Matricule"].Text}'";
                int dd = General.Execute(reqDelete);
            }
            // tmpAdorsPersonnel.Update();
        }

        private void ssGrilleStatutInterv_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void ssGridTransmission_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssGridStatutCommande_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void ssGridAnaActivite_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }

        }

        private void ssGridAna_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            string sCode = null;
            var _with125 = ssgCNC_Conccurent;
            if (ssgCNC_Conccurent.ActiveRow.Cells["CNC_Code"].Column.Index == ssgCNC_Conccurent.ActiveCell.Column.Index
                && !string.IsNullOrEmpty(ssgCNC_Conccurent.ActiveRow.Cells["CNC_Code"].Text))
            {
                sCode = General.fc_FormatCode(ssgCNC_Conccurent.ActiveRow.Cells["CNC_Code"].Text);
                if (string.IsNullOrEmpty(sCode))
                {
                    e.Cancel = true;
                }
                else
                {
                    ssgCNC_Conccurent.ActiveRow.Cells["CNC_Code"].Value = sCode;
                }

            }
        }

        private void GridService_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadpRIME()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT     PRI_ID, PRI_Code, PRI_Libelle" + " From PRI_PRIME " + " ORDER BY PRI_Code";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsPrime = new ModAdo();
                rsPrime = tmpAdorsPrime.fc_OpenRecordSet(sSQL, GridPrime, "PRI_ID");
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridPrime.DataSource = rsPrime;
                GridPrime.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadpRIME");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadArval()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT     ARV_ID, ARV_Libelle From ARV_ARVAL ORDER BY ARV_ID";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsARV = new ModAdo();
                rsARV = tmpAdorsARV.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridARV.DataSource = rsARV;
                GridARV.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadArval");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadProblemeVehi()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT     PRV_ID, PRV_Libelle From PRV_ProblemeVehicule ORDER BY PRV_ID";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsPRV = new ModAdo();
                rsPRV = tmpAdorsPRV.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridPRV.DataSource = rsPRV;
                GridPRV.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadProblemeVehi");
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        private void fc_LoadFlocage()
        {
            try
            {
                string sSQL = null;
                sSQL = "SELECT     FLO_ID, FLO_Libelle From FLO_FLOCAGE ORDER BY FLO_ID";
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                tmpAdorsFLO = new ModAdo();
                rsFLO = tmpAdorsFLO.fc_OpenRecordSet(sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridFLO.DataSource = rsFLO;
                GridFLO.UpdateData();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadFlocage");
            }
        }

        private void GridPrime_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {

        }

        private void GridPRV_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {

        }

        private void GridFLO_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void GridService2_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void GridMOM_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssGridTomTom_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.ssGridTomTom.DisplayLayout.Bands[0].Columns["matricule"].ValueList = this.ssDropTech;
            ssGridTomTom.DisplayLayout.Bands[0].Columns["matricule"].Header.Caption = "matricule(personnel)";
            ssGridTomTom.DisplayLayout.Bands[0].Columns["TOT_Vehicule"].Header.Caption = "Vehicule(GPS)";
            ssGridTomTom.DisplayLayout.Bands[0].Columns["Nom"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            ssGridTomTom.DisplayLayout.Bands[0].Columns["Nom"].CellAppearance.BackColor = Color.Gray;
            ssGridTomTom.DisplayLayout.Bands[0].Columns["Prenom"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            ssGridTomTom.DisplayLayout.Bands[0].Columns["Prenom"].CellAppearance.BackColor = Color.Gray;
            ssGridTomTom.DisplayLayout.Bands[0].Columns["TOT_Noauto"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
        }

        private void ssGridAnalySecteur_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["Matricule"].ValueList = this.DropExploit;
            ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["ANSE_Secteur"].Header.Caption = "Secteur";
            ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["ANSE_Secteur"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["ANSE_Secteur"].CellAppearance.BackColor = Color.Gray;
            ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["Nom"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            ssGridAnalySecteur.DisplayLayout.Bands[0].Columns["Nom"].CellAppearance.BackColor = Color.Gray;
        }

        private void ssGridUtilClim_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.ssGridUtilClim.DisplayLayout.Bands[0].Columns["ANCL_User"].ValueList = this.DropUser;
            ssGridUtilClim.DisplayLayout.Bands[0].Columns["ANCL_User"].Header.Caption = "Utilsateur";
            ssGridUtilClim.DisplayLayout.Bands[0].Columns["Nom"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            ssGridUtilClim.DisplayLayout.Bands[0].Columns["Nom"].CellAppearance.BackColor = Color.Gray;
        }

        private void GridService_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridService.DisplayLayout.Bands[0].Columns["CodeServiceAnalytique"].CellActivation = Activation.NoEdit;
            GridService.DisplayLayout.Bands[0].Columns["CptVte"].ValueList = DropCpte;
            GridService.DisplayLayout.Bands[0].Columns["CptVte"].Header.Caption = "Compte de vente";
            GridService.DisplayLayout.Bands[0].Columns["libelle"].Header.Caption = "Libelle de compte";
        }

        private void GridService2_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.GridService2.DisplayLayout.Bands[0].Columns["CodeRespExploit"].ValueList = this.DropExploit2;
            this.GridService2.DisplayLayout.Bands[0].Columns["CodeServiceAnalytique"].Header.Caption = "Code Service Analytique";
            this.GridService2.DisplayLayout.Bands[0].Columns["CodeServiceAnalytique"].CellActivation = Activation.NoEdit;
            this.GridService2.DisplayLayout.Bands[0].Columns["CodeRespExploit"].Header.Caption = "Code Resp.Exploit";
            this.GridService2.DisplayLayout.Bands[0].Columns["libelle"].Hidden = true;
        }

        private void GridSpecifQualif_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.GridSpecifQualif.DisplayLayout.Bands[0].Columns["codeQualif"].ValueList = this.SSDropQualif;
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["CodeQualif"].Header.Caption = "Code Qualification";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["libelleQualif"].Header.Caption = "Libellé Qualification";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifTech"].Header.Caption = "Qualification Technicien";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifCom"].Header.Caption = " Qualification Commercial";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifDisp"].Header.Caption = " Qualification Deviseur";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifDev"].Header.Caption = "Qualification Dispatch";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifAdmin"].Header.Caption = " Qualification Administratif";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifRTrav"].Header.Caption = " Qualification Resp.Trav.";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifRExp"].Header.Caption = "Qualification Resp.Expl.";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifPart"].Header.Caption = " Qualification Copro.";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifSousTraitant"].Header.Caption = " Qualification Sous-traitant";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["QualifSpecialiste"].Header.Caption = " Qualification Spécialiste";
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["libelleQualif"].CellActivation = Activation.NoEdit;
            GridSpecifQualif.DisplayLayout.Bands[0].Columns["libelleQualif"].CellAppearance.BackColor = Color.Gray;
        }

        private void ssCAI_CategoriInterv_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

        }

        private void ssGridPersonnel_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ////======================> Ajouté Par Mondir : Voir le bug : https://groupe-dt.mantishub.io/view.php?id=1637
            e.Layout.Bands[0].Columns["Matricule"].SortComparer = new SortComparer();
            e.Layout.Bands[0].SortedColumns.Add("Matricule", false);
            //e.Layout.Bands[0].ov

            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["CSecteur2"].ValueList = this.SSdropSecteur;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["CodeQualif"].ValueList = this.ssDropQualite;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["CRespExploit"].ValueList = this.SSdropRespExploit;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["commercial"].ValueList = this.ssDropCommercial;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["DevRadiateur"].ValueList = this.ssDropDevRadia;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["DevExploit"].ValueList = this.ssDropDevExploit;
            //=== renovation chauffage 1.      
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["RenovChauffage1"].ValueList = this.ssDropRenov1;
            //=== renovation chauffage 2.      
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["RenovChauffage2"].ValueList = this.ssDropRenov2;
            //=== chauffagiste 1.
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Chauffagiste1"].ValueList = this.ssDropChauff1;
            //=== chauffagiste 2.          
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Chauffagiste2"].ValueList = this.ssDropChauff2;
            //=== Chef de secteur
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["CSecteur"].ValueList = this.SSdropSecteur;
            //=== renovation chauffage 2.      
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["RespService"].ValueList = this.ssDropRespService;
            //=== rondier et depanneur 2.           
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["DEP2"].ValueList = this.ssDropTechnicien;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Rondier"].ValueList = this.ssDropTechnicien;
            //=== code service pour controle heure.          
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["SCH_Code"].ValueList = this.ssDropService;
            //=== code service pour controle heure.         
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Login"].ValueList = this.DropUsersWindows;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Login"].Header.Caption = "Login Windows";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["CRespExploit"].Header.Caption = "Resp.Exploitation";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["DevRadiateur"].Header.Caption = "Devis Radiateur";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["DevExploit"].Header.Caption = "Devis Exploit";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["RenovChauffage1"].Header.Caption = "Rénovation Chauffage1";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["RenovChauffage2"].Header.Caption = "Rénovation Chauffage2";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["CSecteur"].Header.Caption = "Chef Secteur";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["CSecteur2"].Header.Caption = "Chef Secteur 2";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["Dep2"].Header.Caption = "Depanneur 2";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["Rondier"].Header.Caption = "Ramoneur";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["RespService"].Header.Caption = "Responsable Service";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["NumRadio"].Header.Caption = "Numerode telephone";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["NepasAfficherOcean"].Header.Caption = "Ne pas Afficher (Ocean)";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["SCH_Code"].Header.Caption = "Code service controle heure";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["CodeQualif"].Header.Caption = "Code Qualif";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["NonActif"].Header.Caption = "Non Actif";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["NonActif"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["SousTraitant"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["NepasAfficherOcean"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["EnvoieMail"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["ActiverPDA"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["Libelle"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["Libelle"].CellAppearance.BackColor = Color.Gray;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Dispatcheur"].ValueList = ssDropRenov2;
            this.ssGridPersonnel.DisplayLayout.Bands[0].Columns["Dispatcheur1"].Header.Caption = "Dispatcheur";
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["Dispatcheur"].ValueList = ssDropRenov2;
            ssGridPersonnel.DisplayLayout.Bands[0].Columns["ActiveTomTom"].Hidden = true;
        }

        private void GridCodeEtatDevis_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridCodeEtatDevis.DisplayLayout.Bands[0].Columns["CodeEtatManuel"].Header.Caption = "Code Manuel";
            GridCodeEtatDevis.DisplayLayout.Bands[0].Columns["CodeEtatManuel"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridCodeEtatDevis.DisplayLayout.Bands[0].Columns["ApImprime"].Header.Caption = "Ap Impression";
            GridCodeEtatDevis.DisplayLayout.Bands[0].Columns["ApImprime"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }

        private void GridCodeEtatDevis_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["CodeEtatManuel"].Value == DBNull.Value)
                {
                    e.Row.Cells["CodeEtatManuel"].Value = false;
                }
                if (e.Row.Cells["ApImprime"].Value == DBNull.Value)
                {
                    e.Row.Cells["ApImprime"].Value = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridCodeEtatDevis_InitializeRow");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridParametres_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridParametres.DisplayLayout.Bands[0].Columns["Adresse"].Header.Caption = "Valeur";
            GridParametres.DisplayLayout.Bands[0].Columns["AideLien"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            GridParametres.DisplayLayout.Bands[0].Columns["AideLien"].CellButtonAppearance.Image = Properties.Resources.Help_16x16;
            GridParametres.DisplayLayout.Bands[0].Columns["AideLien"].CellAppearance.Image = Properties.Resources.Help_16x16;
            GridParametres.DisplayLayout.Bands[0].Columns["AideLien"].Width = 35;
            GridParametres.DisplayLayout.Bands[0].Columns["AideLien"].Header.Caption = "Aide";
            GridParametres.DisplayLayout.Bands[0].Columns["NomBase"].Hidden = true;
            GridParametres.DisplayLayout.Bands[0].Columns["TableLien"].Hidden = true;
            GridParametres.DisplayLayout.Bands[0].Columns["ChampLien"].Hidden = true;
            GridParametres.DisplayLayout.Bands[0].Columns["ToolTipText"].Hidden = true;
            GridParametres.DisplayLayout.Bands[0].Columns["CodeUO"].Hidden = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridParametres_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (!string.IsNullOrEmpty(GridParametres.ActiveRow.Cells["ToolTipText"].Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aide sur :" + GridParametres.ActiveRow.Cells["Code"].Value.ToString() + "\n" + GridParametres.ActiveRow.Cells["ToolTipText"].Value.ToString(), "Aide sur un paramètre", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void GridAchatVente_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridAchatVente.DisplayLayout.Bands[0].Columns["PalierAchat"].Header.Caption = "Palier Achat";
            GridAchatVente.DisplayLayout.Bands[0].Columns["AfficherFormule"].Header.Caption = "Afficher pour controle";
            GridAchatVente.DisplayLayout.Bands[0].Columns["AfficherFormule"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridAchatVente.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;
        }

        private void ssGridQualite_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridQualite.DisplayLayout.Bands[0].Columns["CodeQualif"].Header.Caption = "Code";
            ssGridQualite.DisplayLayout.Bands[0].Columns["Correspondant_COR"].Header.Caption = "Correspondant";
            ssGridQualite.DisplayLayout.Bands[0].Columns["Facturable_QUA"].Header.Caption = "Facturable";
            ssGridQualite.DisplayLayout.Bands[0].Columns["PDA"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridQualite.DisplayLayout.Bands[0].Columns["Facturable_QUA"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridQualite.DisplayLayout.Bands[0].Columns["Correspondant_COR"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }

        private void ssGridQualite_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["Correspondant_COR"].Value == DBNull.Value)
                {
                    e.Row.Cells["Correspondant_COR"].Value = false;
                }
                if (e.Row.Cells["PDA"].Value == DBNull.Value)
                {
                    e.Row.Cells["PDA"].Value = false;
                }
                if (e.Row.Cells["Facturable_QUA"].Value == DBNull.Value)
                {
                    e.Row.Cells["Facturable_QUA"].Value = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridQualite_InitializeRow");
            }
        }

        private void GridSpecifQualif_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["QualifTech"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifTech"].Value = false;
                }
                if (e.Row.Cells["QualifCom"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifCom"].Value = false;
                }
                if (e.Row.Cells["QualifDisp"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifDisp"].Value = false;
                }
                if (e.Row.Cells["QualifAdmin"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifAdmin"].Value = false;
                }

                if (e.Row.Cells["QualifPart"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifPart"].Value = false;
                }
                if (e.Row.Cells["QualifDev"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifDev"].Value = false;
                }
                if (e.Row.Cells["QualifRTrav"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifRTrav"].Value = false;
                }
                if (e.Row.Cells["QualifRExp"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifRExp"].Value = false;
                }
                if (e.Row.Cells["QualifSousTraitant"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifSousTraitant"].Value = false;
                }
                if (e.Row.Cells["QualifSpecialiste"].Value == DBNull.Value)
                {
                    e.Row.Cells["QualifSpecialiste"].Value = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridSpecifQualif_InitializeRow");
            }
        }

        private void ssGridAnaActivite_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["N_Analytique"].Value == DBNull.Value)
                {
                    e.Row.Cells["N_Analytique"].Value = 1;
                }



                if (e.Row.Cells["ActiviteDevis"].Value == DBNull.Value)
                {
                    e.Row.Cells["ActiviteDevis"].Value = false;
                }
                if (e.Row.Cells["ActiviteTravaux"].Value == DBNull.Value)
                {
                    e.Row.Cells["ActiviteTravaux"].Value = false;
                }
                if (e.Row.Cells["ActiviteContrat"].Value == DBNull.Value)
                {
                    e.Row.Cells["ActiviteContrat"].Value = false;
                }
                ssGridAnaActivite.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridAnaActivite_InitializeRow");
            }
        }

        private void ssGridAnaActivite_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["cbMarq"].Hidden = true;
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["N_Analytique"].CellActivation = Activation.NoEdit;
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["CA_Num"].Header.Caption = "Code";
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["CA_Intitule"].Header.Caption = "Libelle";
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["ActiviteDevis"].Header.Caption = "Activité Devis";
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["ActiviteTravaux"].Header.Caption = "Activité Travaux";
            ssGridAnaActivite.DisplayLayout.Bands[0].Columns["ActiviteContrat"].Header.Caption = "Activité Contrat";

        }

        private void ssgCNC_Conccurent_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["CNC_Noauto"].Hidden = true;
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["CNC_Code"].Header.Caption = "Code";
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["CNC_Libelle"].Header.Caption = "Libelle";
        }



        private void GridPrime_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridPrime.DisplayLayout.Bands[0].Columns["PRI_ID"].Hidden = true;
            GridPrime.DisplayLayout.Bands[0].Columns["PRI_Code"].Header.Caption = "Code";
            GridPrime.DisplayLayout.Bands[0].Columns["PRI_Libelle"].Header.Caption = "Libelle";
        }

        private void GridARV_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridARV.DisplayLayout.Bands[0].Columns["ARV_ID"].Header.Caption = "Code";
            GridARV.DisplayLayout.Bands[0].Columns["ARV_Libelle"].Header.Caption = "Libelle";
        }

        private void GridPRV_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridPRV.DisplayLayout.Bands[0].Columns["PRV_ID"].Header.Caption = "Code";
            GridPRV.DisplayLayout.Bands[0].Columns["PRV_Libelle"].Header.Caption = "Libelle";
        }

        private void GridFLO_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridFLO.DisplayLayout.Bands[0].Columns["FLO_ID"].Header.Caption = "Code";
            GridFLO.DisplayLayout.Bands[0].Columns["FLO_Libelle"].Header.Caption = "Libelle";
        }

        private void GridUser_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridUser.DisplayLayout.Bands[0].Columns["USR_No"].Hidden = true;
            GridUser.DisplayLayout.Bands[0].Columns["USR_Nt"].Header.Caption = "Login-in";
            GridUser.DisplayLayout.Bands[0].Columns["USR_Name"].Header.Caption = "Nom";
        }

        private void GridRep_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridRep.DisplayLayout.Bands[0].Columns["REP_CODE"].Header.Caption = "Code";
            GridRep.DisplayLayout.Bands[0].Columns["REP_Designation"].Header.Caption = "Désignation";
        }

        private void GridMOM_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridMOM.DisplayLayout.Bands[0].Columns["TYMO_Noauto"].Hidden = true;
            GridMOM.DisplayLayout.Bands[0].Columns["TYMO_ID"].Header.Caption = "Code";
            GridMOM.DisplayLayout.Bands[0].Columns["TYMO_LIBELLE"].Header.Caption = "Désignation";
        }

        private void lblGoCharge_Click(object sender, EventArgs e)
        {
            int LDroit = ModAutorisation.fc_DroitDetail("UserDocCharge");
            if (LDroit == 0)
            {
                return;
            }
            View.Theme.Theme.Navigate(typeof(UserDocCharge));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridService_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key.ToUpper() == "CptVte".ToUpper())
            {
                var row = DropCpte.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                if (row != null)
                {
                    GridService.ActiveRow.Cells["libelle"].Value = row.Cells[1].Value;
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridService_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsSrvCpte.Update();
        }

        private void GraphAchatVente_MouseClick(object sender, MouseEventArgs e)
        {

            var pos = e.Location;
            clickPosition = pos;
            var results = GraphAchatVente.HitTest(pos.X, pos.Y, false,
                                         ChartElementType.PlottingArea);
            foreach (var result in results)
            {
                if (result.ChartElementType == ChartElementType.PlottingArea)
                {
                    var xVal = result.ChartArea.AxisX.PixelPositionToValue(pos.X);
                    var yVal = result.ChartArea.AxisY.PixelPositionToValue(pos.Y);

                    tooltip.Show("X=" + xVal + ", Y=" + yVal,
                                 this.GraphAchatVente, e.Location.X, e.Location.Y - 15);
                }
            }
        }
        private void GraphAchatVente_MouseMove(object sender, MouseEventArgs e)
        {
            if (clickPosition.HasValue && e.Location != clickPosition)
            {
                tooltip.RemoveAll();
                clickPosition = null;
            }
        }

        private void GridAchatVente_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["AfficherFormule"].Value == DBNull.Value)
            {
                e.Row.Cells["AfficherFormule"].Value = false;
            }

            if (e.Row.Cells["AfficherFormule"].Value != DBNull.Value
                && e.Row.Cells["a"].Value == DBNull.Value
                && e.Row.Cells["b"].Value == DBNull.Value
                && e.Row.Cells["PalierAchat"].Value == DBNull.Value
                )
            {
                e.Row.Cells["a"].Value = 0;
                e.Row.Cells["b"].Value = 0;
                e.Row.Cells["PalierAchat"].Value = 0;
            }

            if (e.Row.Cells["a"].Value == DBNull.Value
                 && e.Row.Cells["b"].Value == DBNull.Value
                && e.Row.Cells["PalierAchat"].Value == DBNull.Value)
            {
                e.Row.Cells["a"].Value = 0;
                e.Row.Cells["b"].Value = 0;
                e.Row.Cells["PalierAchat"].Value = 0;
            }
            if (e.Row.Cells["a"].Value != DBNull.Value
                 && e.Row.Cells["b"].Value == DBNull.Value
                && e.Row.Cells["PalierAchat"].Value == DBNull.Value)
            {
                e.Row.Cells["b"].Value = 0;
                e.Row.Cells["PalierAchat"].Value = 0;
            }
            if (e.Row.Cells["b"].Value != DBNull.Value
                 && e.Row.Cells["a"].Value == DBNull.Value
                && e.Row.Cells["PalierAchat"].Value == DBNull.Value)
            {
                e.Row.Cells["a"].Value = 0;
                e.Row.Cells["PalierAchat"].Value = 0;
            }
            if (e.Row.Cells["PalierAchat"].Value != DBNull.Value
                 && e.Row.Cells["a"].Value == DBNull.Value
                && e.Row.Cells["b"].Value == DBNull.Value
                )
            {
                e.Row.Cells["a"].Value = 0;
                e.Row.Cells["b"].Value = 0;
            }

        }

        private void ssGridQualite_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsQualite.Update();
        }

        private void GridSpecifQualif_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsSpecifQualif.Update();
        }

        private void GridSpecifQualif_CellChange(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Key.ToUpper() == "CodeQualif".ToUpper())
            {
                var row = SSDropQualif.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                if (row != null)
                {
                    GridSpecifQualif.ActiveRow.Cells["libelleQualif"].Value = row.Cells[1].Value;
                }
            }
        }

        private void ssGrilleStatutInterv_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            ssGrilleStatutInterv.DisplayLayout.Bands[0].Columns["CodeEtat"].Header.Caption = "Code";
            ssGrilleStatutInterv.DisplayLayout.Bands[0].Columns["LibelleCodeEtat"].Header.Caption = "Libelle";

        }

        private void ssGrilleStatutInterv_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsStatutInterv.Update();
        }

        private void ssGridTransmission_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsTransmission.Update();
        }

        private void lblSupTRansmission_Click(object sender, EventArgs e)
        {
            if (ssGridTransmission.Rows.Count > 0)
            {
                ssGridTransmission.DeleteSelectedRows();
                tmpAdorsTransmission.Update();
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pas d'enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ssGridStatutCommande_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int yy = tmpAdorsStatutCommande.Update();
        }

        private void ssGridAnaActivite_AfterRowUpdate(object sender, RowEventArgs e)
        {
            SDArsAnaActivite.Update(rsAnaActivite);
        }
        /// <summary>
        /// Testede
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCodeEtatDevis_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int XX = tmpAdorsCodeEtatDevis.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeFacture_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int yy = tmpAdorsTypeFacture.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIndice_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsIndice.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIndice_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {

            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void GridTypeFacture_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Code"].DataChanged)
            {
                if (e.Row.Cells["Code"].OriginalValue.ToString() != e.Row.Cells["Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TypeFacture WHERE Code = '{e.Row.Cells["Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridIndice_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsIndice.Update();
        }

        private void GridTypeRatio_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsTypeRatio.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeRatio_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Code"].DataChanged)
            {
                if (e.Row.Cells["Code"].OriginalValue.ToString() != e.Row.Cells["Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TypeRatio WHERE Code = '{e.Row.Cells["Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeRatio_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeRatio_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsTypeRatio.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFormule_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsFormule.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFormule_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFormule_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsFormule.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFormule_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["Type"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Type est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Type"].DataChanged)
            {
                if (e.Row.Cells["Type"].OriginalValue.ToString() != e.Row.Cells["Type"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FacFormule WHERE Type = '{e.Row.Cells["Type"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCodeEtatDevis_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (GridCodeEtatDevis.ActiveRow.Cells["Code"].Text.Length > 5)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code ne depasse pas 5 caractere.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Code"].DataChanged)
            {
                if (e.Row.Cells["Code"].OriginalValue.ToString() != e.Row.Cells["Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM DevisCodeEtat WHERE Code = '{e.Row.Cells["Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCodeEtatDevis_AfterRowsDeleted(object sender, EventArgs e)
        {
            int yy = tmpAdorsCodeEtatDevis.Update();
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridParametres_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsParametres.Update();
        }

        private void ssGridBE_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssGridBE.DisplayLayout.Bands[0].Columns["CodeBE"].CellActivation = Activation.NoEdit;
            ssGridBE.DisplayLayout.Bands[0].Columns["CodeBE"].CellAppearance.BackColor = Color.Gray;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsBE.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridBE_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsBE.Update();


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(les) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsCNC_Conccurent.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void ssgCNC_Conccurent_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsCNC_Conccurent.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridTomTom_AfterRowUpdate(object sender, RowEventArgs e)
        {

            int xx = tmpAdorsTomtom.Update();
            ssGridTomTom.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridTomTom_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsTomtom.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridUtilClim_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsUtilClim.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridUtilClim_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsUtilClim.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridService_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = tmpAdorsSrvCpte.Update();
        }

        private void GridPrime_AfterRowUpdate(object sender, RowEventArgs e)
        {
            int xx = tmpAdorsPrime.Update();
            GridPrime.UpdateData();
        }

        private void GridPrime_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["PRI_Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["PRI_Code"].DataChanged)
            {
                if (e.Row.Cells["PRI_Code"].OriginalValue.ToString() != e.Row.Cells["PRI_Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM PRI_PRIME WHERE PRI_Code= '{e.Row.Cells["PRI_Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridARV_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsARV.Update();
        }

        private void GridARV_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["ARV_ID"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["ARV_ID"].DataChanged)
            {
                if (e.Row.Cells["ARV_ID"].OriginalValue.ToString() != e.Row.Cells["ARV_ID"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM  ARV_ARVAL WHERE ARV_ID= '{e.Row.Cells["ARV_ID"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridPRV_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["PRV_ID"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["PRV_ID"].DataChanged)
            {
                if (e.Row.Cells["PRV_ID"].OriginalValue.ToString() != e.Row.Cells["PRV_ID"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM PRV_ProblemeVehicule WHERE PRV_ID= '{e.Row.Cells["PRV_ID"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPRV_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsPRV.Update();
        }/// <summary>
         /// TEsted
         /// </summary>
         /// <param name="sender"></param>
         /// <param name="e"></param>

        private void GridFLO_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsFLO.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFLO_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["FLO_ID"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["FLO_ID"].DataChanged)
            {
                if (e.Row.Cells["FLO_ID"].OriginalValue.ToString() != e.Row.Cells["FLO_ID"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FLO_FLOCAGE WHERE FLO_ID= '{e.Row.Cells["FLO_ID"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridService2_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsAnalySectCtr.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridService2_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsAnalySectCtr.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridUser_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsUtilWin.Update();
            GridUser.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRep_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsREPModAdo.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRep_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["REP_CODE"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["REP_CODE"].DataChanged)
            {
                if (e.Row.Cells["REP_CODE"].OriginalValue.ToString() != e.Row.Cells["REP_CODE"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM REP_ReglementPDA WHERE REP_CODE = '{e.Row.Cells["REP_CODE"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRep_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsREPModAdo.Update();
        }

        private void GridMOM_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsMOMModAdo.Update();
            GridMOM.UpdateData();
        }

        private void GridMOM_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsMOMModAdo.Update();
        }

        private void GridMOM_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["TYMO_ID"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["TYMO_ID"].DataChanged)
            {
                if (e.Row.Cells["TYMO_ID"].OriginalValue.ToString() != e.Row.Cells["TYMO_ID"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TYMO_TypeDeMoment WHERE TYMO_ID = '{e.Row.Cells["TYMO_ID"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void ssGridQualite_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CodeQualif"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne code est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["CodeQualif"].DataChanged)
            {
                if (e.Row.Cells["CodeQualif"].OriginalValue.ToString() != e.Row.Cells["CodeQualif"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Qualification WHERE CodeQualif = '{e.Row.Cells["CodeQualif"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridSpecifQualif_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CodeQualif"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Code Qualification est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["CodeQualif"].DataChanged)
            {
                if (e.Row.Cells["CodeQualif"].OriginalValue.ToString() != e.Row.Cells["CodeQualif"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM SpecifQualif WHERE CodeQualif = '{e.Row.Cells["CodeQualif"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void ssGridQualite_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsQualite.Update();
        }

        private void GridSpecifQualif_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsSpecifQualif.Update();
        }

        private void ssGrilleStatutInterv_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["CodeEtat"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Code  est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["CodeEtat"].DataChanged)
            {
                if (e.Row.Cells["CodeEtat"].OriginalValue.ToString() != e.Row.Cells["CodeEtat"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TypeCodeEtat WHERE CodeEtat = '{e.Row.Cells["CodeEtat"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void ssGrilleStatutInterv_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsStatutInterv.Update();
        }

        private void ssGridStatutCommande_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["Code"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Code  est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["Code"].DataChanged)
            {
                if (e.Row.Cells["Code"].OriginalValue.ToString() != e.Row.Cells["Code"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TypeEtatCommande WHERE Code = '{e.Row.Cells["Code"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void ssGridStatutCommande_AfterRowsDeleted(object sender, EventArgs e)
        {
            int yy = tmpAdorsStatutCommande.Update();
        }

        private void ssGridAnaActivite_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CA_Num"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le colonne Code  est obligatoire.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
            if (e.Row.Cells["CA_Num"].DataChanged)
            {
                if (e.Row.Cells["CA_Num"].OriginalValue.ToString() != e.Row.Cells["CA_Num"].Text)
                {
                    SqlDataAdapter sda = new SqlDataAdapter($"SELECT COUNT(*) FROM F_CompteA WHERE CA_Num= '{e.Row.Cells["CA_Num"].Text}' ", SAGE.adoSage);
                    DataTable rs = new DataTable();
                    sda.Fill(rs);
                    if (rs.Rows[0][0].ToString() != "0")
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Row.CancelUpdate();
                        e.Cancel = true;
                    }
                }

            }
        }

        private void GridIndice_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["Connu"].Value == DBNull.Value)
            {
                e.Row.Cells["Connu"].Value = false;
            }
            if (e.Row.Cells["Reel"].Value == DBNull.Value)
            {
                e.Row.Cells["Reel"].Value = false;
            }
        }

        private void ssGridUtilClim_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["ANCL_User"].DataChanged)
            {
                if (e.Row.Cells["ANCL_User"].OriginalValue.ToString() != e.Row.Cells["ANCL_User"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ANCL_AnalyClim WHERE ANCL_User= '{e.Row.Cells["ANCL_User"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }

        }

        private void GridUser_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["USR_Nt"].DataChanged)
            {
                if (e.Row.Cells["USR_Nt"].OriginalValue.ToString() != e.Row.Cells["USR_Nt"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM USR_Users WHERE USR_Nt= '{e.Row.Cells["USR_Nt"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridService2_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["CodeRespExploit"].DataChanged)
            {
                if (e.Row.Cells["CodeRespExploit"].OriginalValue.ToString() != e.Row.Cells["CodeRespExploit"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ServiceAnalytiqueCtr WHERE CodeRespExploit= '{e.Row.Cells["CodeRespExploit"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void GridService_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["CptVte"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La colonne Compte de vente est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Row.CancelUpdate();
                e.Cancel = true;
            }
            if (string.IsNullOrEmpty(e.Row.Cells["CodeServiceAnalytique"].Text))
            {

                e.Row.CancelUpdate();
                e.Cancel = true;
            }
            if (e.Row.Cells["CptVte"].OriginalValue.ToString() != e.Row.Cells["CptVte"].Text)
            {
                using (var tmpModAdo = new ModAdo())
                {
                    var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM ServiceAnalytiqueCpte WHERE CptVte= '{e.Row.Cells["CptVte"].Text}' "));
                    if (inDb > 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Row.CancelUpdate();
                        e.Cancel = true;
                    }
                }
            }
        }

        private void ssGridPersonnel_AfterRowsDeleted(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridAnaActivite_AfterRowsDeleted(object sender, EventArgs e)
        {

            SDArsAnaActivite.Update(rsAnaActivite);
        }



        private void TreeService_AfterSelect(object sender, SelectEventArgs e)
        {

            UltraTreeNode node = TreeService.ActiveNode;
            node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            fc_LoadServiceCpte(node);
        }

        private void GridAchatVente_Click(object sender, EventArgs e)
        {
            if (optFormule.Checked == true && GridAchatVente.ActiveRow.Index <= GridAchatVente.Rows.Count - 1)
            {
                if (chkMarqueurs.Checked)
                {
                    sub_AfficheGraph();
                    chkMarqueurs_CheckedChanged(null, null);
                }
                else
                {
                    sub_AfficheGraph();
                }
            }

        }

        private void TreeService2_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode node = TreeService2.ActiveNode;
            node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
            fc_LoadServiceCTR(node);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (e.Row.Cells["PCF_Journal"].Text == "")
            {
                CustomMessageBox.Show("Le code Journal est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }

            if (e.Row.Cells["PCF_Periode"].Text == "")
            {
                CustomMessageBox.Show("La période est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }

            if (e.Row.Cells["PCF_Periode"].Text.Length != 6)
            {
                CustomMessageBox.Show("La période est incorrecte, veuillez respecter le format YYYYMM.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["PCF_Journal"].ValueList = DropDown0;
            e.Layout.Bands[0].Columns["PCF_ID"].Hidden = true;
            e.Layout.Bands[0].Columns["PCF_Periode"].Header.Caption = "Période (yyyymm)";
            e.Layout.Bands[0].Columns["PCF_Libelle"].Header.Caption = "Libelle";
            e.Layout.Bands[0].Columns["PCF_Journal"].Header.Caption = "Journal";
            e.Layout.Bands[0].Columns["PCF_Date"].Header.Caption = "Date";
            e.Layout.Bands[0].Columns["PCF_Cloture"].Header.Caption = "Cloture";
            e.Layout.Bands[0].Columns["PCF_Cloture"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;

            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_AfterRowUpdate(object sender, RowEventArgs e)
        {
            var xx = rsPCFModAdo.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_AfterRowsDeleted(object sender, EventArgs e)
        {
            var xx = rsPCFModAdo.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeDev_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["TYD_Code"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["TYD_Libelle"].Header.Caption = "Désignation";
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadTypeDedevis()
        {
            string sSQL = "";

            try
            {
                sSQL = "SELECT     TYD_Code, TYD_Libelle"

                    + " From TYD_TypeDevis "

                    + " ORDER BY TYD_Code";

                Cursor = Cursors.WaitCursor;

                rsTypeDevModAdo = new ModAdo();
                rsTypeDev = rsTypeDevModAdo.fc_OpenRecordSet(sSQL);

                GridTypeDev.DataSource = rsTypeDev;

                Cursor = Cursors.Default;

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeDev_AfterRowsDeleted(object sender, EventArgs e)
        {
            var xx = rsTypeDevModAdo.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeDev_AfterRowUpdate(object sender, RowEventArgs e)
        {
            var xx = rsTypeDevModAdo.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeDev_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["PCF_Cloture"].Text == "")
                e.Row.Cells["PCF_Cloture"].Value = 0;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridTypeDev_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            //"SELECT     TYD_Code, TYD_Libelle"

            //        + " From TYD_TypeDevis "

            //        + " ORDER BY TYD_Code";
            using (var tmpModAdo = new ModAdo())
                if (Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM TYD_TypeDevis WHERE TYD_Code = '{e.Row.Cells["TYD_Code"].Text}'")) > 0)
                {
                    CustomMessageBox.Show("Ce code existe déja", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPCF_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (ssGridPCF.ActiveCell == null)
                return;
            if (ssGridPCF.ActiveCell.Column.Key.ToUpper() == "PCF_Cloture".ToUpper())
            {
                if (ssGridPCF.ActiveCell.Text == "")
                    ssGridPCF.ActiveCell.Value = 0;
            }
        }

        //modifs 22/10/2019

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadTypeEncaissement()
        {
            string sSQL;
            try
            {
                sSQL = "SELECT  CodeTypeEnc, LibelleTypeEnc from TypeEncaissement ORDER BY CodeTypeEnc";
                Cursor = Cursors.WaitCursor;
                rsTypeEncModAdo = new ModAdo();
                rsTypeEnc = rsTypeEncModAdo.fc_OpenRecordSet(sSQL);
                GridEnc.DataSource = rsTypeEnc;
                Cursor = Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_LoadTypeEncaissement");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            try
            {

                if (!string.IsNullOrEmpty(e.Row.GetCellValue("CodeTypeEnc") + ""))
                {
                    if (e.Row.Cells["CodeTypeEnc"].DataChanged)
                    {
                        if (e.Row.Cells["CodeTypeEnc"].OriginalValue.ToString() != e.Row.Cells["CodeTypeEnc"].Text)
                        {
                            var modado = new ModAdo();
                            string sql = modado.fc_ADOlibelle("select count(CodeTypeEnc) from TypeEncaissement where CodeTypeEnc='" + e.Row.Cells["CodeTypeEnc"].Text + "'");
                            if (Convert.ToInt16(sql) > 0)
                            {
                                Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                e.Row.CancelUpdate();
                            }
                        }
                    }
                }
                else
                {
                    Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    e.Row.CancelUpdate();
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";GridEnc_BeforeRowUpdate");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            var result = Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (result == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsTypeEncModAdo.Update();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsTypeEncModAdo.Update();
        }

        private void ssGridPersonnel_AfterSortChange(object sender, BandEventArgs e)
        {
            //
            //e.
        }

        private void ssGridPersonnel_BeforeSortChange(object sender, Infragistics.Win.UltraWinGrid.BeforeSortChangeEventArgs e)
        {
            //e.
            //fc_GrillePersonnel();
            //e.Cancel = true;
        }

        /// <summary>
        /// =========================> Mondir : Voir le bug https://groupe-dt.mantishub.io/view.php?id=1637
        /// </summary>
        private class SortComparer : IComparer
        {
            internal SortComparer()
            {
            }

            int IComparer.Compare(object x, object y)
            {
                UltraGridCell xCell = (UltraGridCell)x;
                UltraGridCell yCell = (UltraGridCell)y;

                string text1 = xCell.Text;
                string text2 = yCell.Text;

                if (!General.IsNumeric(text1) && !General.IsNumeric(text2))
                {
                    if (text1.Length > text2.Length)
                        return 1;
                    else if (text1.Length < text2.Length)
                        return -1;
                    else
                        return 0;
                }
                //return text1.Length > text2.Length ? 1 : /*String.Compare(text1, text2, true)*/;
                else if (General.IsNumeric(text1) && General.IsNumeric(text2))
                {
                    if (Convert.ToDouble(text1) > Convert.ToDouble(text2))
                        return 1;
                    else if (Convert.ToDouble(text1) < Convert.ToDouble(text2))
                        return -1;
                    else
                        return 0;
                }
                //else if (!General.IsNumeric(text1) || !General.IsNumeric(text2))
                //    return String.Compare(text1, text2, true);
                else if (!General.IsNumeric(text1) && General.IsNumeric(text2))
                    return 1;
                else if (General.IsNumeric(text1) && !General.IsNumeric(text2))
                    return -1;
                else
                    return 0;
            }
        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCAT_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["CAT_TVA300"].Style = ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["CAT_TVA300"].DefaultCellValue = false;
            e.Layout.Bands[0].Columns["CAT_Code"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["CAT_Libelle"].Header.Caption = "Désignation";
            e.Layout.Bands[0].Columns["CAT_TVA300"].Header.Caption = "TVA 300";
        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCAT_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsCatModAdo.Update();
        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCAT_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsCatModAdo.Update();
        }

        /// <summary>
        /// Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCAT_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            var dr = CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
