namespace Axe_interDT.Views.Parametrages.ChargePersonnel
{
    partial class UserDocCharge
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.optMatricule = new System.Windows.Forms.RadioButton();
            this.optNom = new System.Windows.Forms.RadioButton();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.optActif = new System.Windows.Forms.RadioButton();
            this.optNonActif = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.ssDropMois = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssCHP_ChargePers = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblMat = new System.Windows.Forms.Label();
            this.TreeView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMois)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCHP_ChargePers)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.71795F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.28205F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TreeView1, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 99F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(780, 631);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.tableLayoutPanel2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox6.Location = new System.Drawing.Point(3, 26);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(257, 93);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Classement";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.optMatricule, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.optNom, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Frame1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(251, 70);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // optMatricule
            // 
            this.optMatricule.AutoSize = true;
            this.optMatricule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optMatricule.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optMatricule.Location = new System.Drawing.Point(5, 38);
            this.optMatricule.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.optMatricule.Name = "optMatricule";
            this.optMatricule.Size = new System.Drawing.Size(117, 29);
            this.optMatricule.TabIndex = 539;
            this.optMatricule.Text = "Matricule";
            this.optMatricule.UseVisualStyleBackColor = true;
            this.optMatricule.CheckedChanged += new System.EventHandler(this.optMatricule_CheckedChanged);
            // 
            // optNom
            // 
            this.optNom.AutoSize = true;
            this.optNom.Checked = true;
            this.optNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optNom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optNom.Location = new System.Drawing.Point(5, 3);
            this.optNom.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.optNom.Name = "optNom";
            this.optNom.Size = new System.Drawing.Size(117, 29);
            this.optNom.TabIndex = 538;
            this.optNom.TabStop = true;
            this.optNom.Text = "Nom";
            this.optNom.UseVisualStyleBackColor = true;
            this.optNom.CheckedChanged += new System.EventHandler(this.optNom_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel4);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame1.Location = new System.Drawing.Point(125, 0);
            this.Frame1.Margin = new System.Windows.Forms.Padding(0);
            this.Frame1.Name = "Frame1";
            this.Frame1.Padding = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.SetRowSpan(this.Frame1, 2);
            this.Frame1.Size = new System.Drawing.Size(126, 70);
            this.Frame1.TabIndex = 540;
            this.Frame1.TabStop = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.optActif, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.optNonActif, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 17);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(126, 53);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // optActif
            // 
            this.optActif.AutoSize = true;
            this.optActif.Checked = true;
            this.optActif.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optActif.Location = new System.Drawing.Point(3, 3);
            this.optActif.Name = "optActif";
            this.optActif.Size = new System.Drawing.Size(59, 20);
            this.optActif.TabIndex = 538;
            this.optActif.TabStop = true;
            this.optActif.Text = "Actif";
            this.optActif.UseVisualStyleBackColor = true;
            this.optActif.CheckedChanged += new System.EventHandler(this.optActif_CheckedChanged);
            // 
            // optNonActif
            // 
            this.optNonActif.AutoSize = true;
            this.optNonActif.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optNonActif.Location = new System.Drawing.Point(3, 29);
            this.optNonActif.Name = "optNonActif";
            this.optNonActif.Size = new System.Drawing.Size(90, 21);
            this.optNonActif.TabIndex = 539;
            this.optNonActif.Text = "Non actif";
            this.optNonActif.UseVisualStyleBackColor = true;
            this.optNonActif.CheckedChanged += new System.EventHandler(this.optNonActif_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.ssDropMois);
            this.groupBox1.Controls.Add(this.ssCHP_ChargePers);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox1.Location = new System.Drawing.Point(266, 125);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(511, 503);
            this.groupBox1.TabIndex = 504;
            this.groupBox1.TabStop = false;
            // 
            // ssDropMois
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropMois.DisplayLayout.Appearance = appearance1;
            this.ssDropMois.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropMois.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMois.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMois.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssDropMois.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMois.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssDropMois.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropMois.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropMois.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropMois.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssDropMois.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropMois.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropMois.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropMois.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssDropMois.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropMois.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMois.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssDropMois.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssDropMois.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropMois.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssDropMois.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssDropMois.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropMois.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssDropMois.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropMois.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropMois.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropMois.Location = new System.Drawing.Point(152, 239);
            this.ssDropMois.Name = "ssDropMois";
            this.ssDropMois.Size = new System.Drawing.Size(186, 27);
            this.ssDropMois.TabIndex = 503;
            this.ssDropMois.Visible = false;
            // 
            // ssCHP_ChargePers
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssCHP_ChargePers.DisplayLayout.Appearance = appearance13;
            this.ssCHP_ChargePers.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssCHP_ChargePers.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssCHP_ChargePers.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCHP_ChargePers.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCHP_ChargePers.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssCHP_ChargePers.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssCHP_ChargePers.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssCHP_ChargePers.DisplayLayout.MaxColScrollRegions = 1;
            this.ssCHP_ChargePers.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssCHP_ChargePers.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssCHP_ChargePers.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssCHP_ChargePers.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
            this.ssCHP_ChargePers.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssCHP_ChargePers.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssCHP_ChargePers.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssCHP_ChargePers.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssCHP_ChargePers.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssCHP_ChargePers.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssCHP_ChargePers.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssCHP_ChargePers.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssCHP_ChargePers.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssCHP_ChargePers.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssCHP_ChargePers.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssCHP_ChargePers.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssCHP_ChargePers.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssCHP_ChargePers.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssCHP_ChargePers.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssCHP_ChargePers.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssCHP_ChargePers.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssCHP_ChargePers.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssCHP_ChargePers.Location = new System.Drawing.Point(3, 20);
            this.ssCHP_ChargePers.Margin = new System.Windows.Forms.Padding(0);
            this.ssCHP_ChargePers.Name = "ssCHP_ChargePers";
            this.ssCHP_ChargePers.Size = new System.Drawing.Size(505, 480);
            this.ssCHP_ChargePers.TabIndex = 412;
            this.ssCHP_ChargePers.Text = "ultraGrid1";
            this.ssCHP_ChargePers.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssCHP_ChargePers_InitializeLayout);
            this.ssCHP_ChargePers.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssCHP_ChargePers_InitializeRow);
            this.ssCHP_ChargePers.AfterRowsDeleted += new System.EventHandler(this.ssCHP_ChargePers_AfterRowsDeleted);
            this.ssCHP_ChargePers.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssCHP_ChargePers_AfterRowUpdate);
            this.ssCHP_ChargePers.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.ssCHP_ChargePers_BeforeRowUpdate);
            this.ssCHP_ChargePers.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssCHP_ChargePers_BeforeExitEditMode);
            this.ssCHP_ChargePers.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssCHP_ChargePers_BeforeRowsDeleted);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.lblMat, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(266, 26);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(511, 93);
            this.tableLayoutPanel3.TabIndex = 505;
            // 
            // lblMat
            // 
            this.lblMat.AutoSize = true;
            this.lblMat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblMat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMat.Font = new System.Drawing.Font("Ubuntu", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMat.Location = new System.Drawing.Point(50, 46);
            this.lblMat.Margin = new System.Windows.Forms.Padding(50, 0, 50, 0);
            this.lblMat.Name = "lblMat";
            this.lblMat.Size = new System.Drawing.Size(411, 47);
            this.lblMat.TabIndex = 506;
            this.lblMat.Text = "Charges du personnel";
            this.lblMat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // TreeView1
            // 
            this.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeView1.Location = new System.Drawing.Point(3, 125);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.Size = new System.Drawing.Size(257, 503);
            this.TreeView1.TabIndex = 506;
            this.TreeView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeView1_AfterSelect);
            // 
            // UserDocCharge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocCharge";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Charge Personnel";
            this.VisibleChanged += new System.EventHandler(this.UserDocChrage_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMois)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssCHP_ChargePers)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.RadioButton optMatricule;
        public System.Windows.Forms.RadioButton optNom;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.GroupBox groupBox1;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssCHP_ChargePers;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssDropMois;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label lblMat;
        public System.Windows.Forms.RadioButton optActif;
        public System.Windows.Forms.RadioButton optNonActif;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Infragistics.Win.UltraWinTree.UltraTree TreeView1;
    }
}
