﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinTree;

namespace Axe_interDT.Views.Parametrages.ChargePersonnel
{
    
    public partial class UserDocCharge : UserControl
    {
        DataTable rsCHP_ChargePers;
        ModAdo rsCHP_ChargePersAdo;
        string sSQL;
        string sMatEncours;
        public UserDocCharge()
        {
            InitializeComponent();
        }
        private void optActif_CheckedChanged(object sender, EventArgs e)
        {
            if (optActif.Checked)
            {
                fc_LoadtreeviewPer();
                fc_CHP_ChargePers( "",  "");
                fc_DropMois();
            }
        }
        private void optNom_CheckedChanged(object sender, EventArgs e)
        {
            if (optNom.Checked)
            {
                fc_LoadtreeviewPer();
                fc_CHP_ChargePers("", "");
                fc_DropMois();
            }
        }

        private void optNonActif_CheckedChanged(object sender, EventArgs e)
        {
            if (optNonActif.Checked)
            {
                fc_LoadtreeviewPer();
                fc_CHP_ChargePers( "",  "");
                fc_DropMois();
            }
        }
        private void optMatricule_CheckedChanged(object sender, EventArgs e)
        {
            if (optMatricule.Checked)
            {
                fc_LoadtreeviewPer();
                fc_CHP_ChargePers("", "");
                fc_DropMois();
            }
        }

        private void UserDocChrage_VisibleChanged(object sender, EventArgs e)
        {
            if(Visible)
            {
                fc_LoadtreeviewPer();
                fc_CHP_ChargePers("", "");
                fc_DropMois();
            }
            else
            {
                ModCalcul.fc_LoadChargePers("");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadtreeviewPer()
        {
            DataTable rstmp = default(DataTable);
            var tmpAdo = new ModAdo();
            string sName = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ssCHP_ChargePers.UpdateData();

                sSQL = "SELECT Nom, Prenom, Matricule" + " FROM Personnel ";

                if (optActif.Checked == true)//Tested
                {
                    sSQL = sSQL + "  WHERE  (NonActif = 0 OR NonActif IS NULL)";

                }
                else if (optNonActif.Checked == true)
                {
                    sSQL = sSQL + "  WHERE  NonActif = 1 ";
                }

                if (optMatricule.Checked == true)
                {
                    sSQL = sSQL + " Order by Matricule";
                }
                else if (optNom.Checked == true)
                {
                    sSQL = sSQL + " Order by Nom";
                }

                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
                var _with1 = rstmp;

                //==reinitialise le treview.
                TreeView1.Visible = false;
                TreeView1.Nodes.Clear();

                if (rstmp.Rows.Count > 0)
                {

                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {
                        if (optMatricule.Checked == true)
                        {
                            sName = rstmpRow["Matricule"] + "   - " 
                                + rstmpRow["Nom"] + " " + rstmpRow["Prenom"];
                        }
                        else if (optNom.Checked == true)
                        {
                            sName = rstmpRow["Nom"] + " " 
                                + rstmpRow["Prenom"] + "   - "
                                + rstmpRow["Matricule"];
                        }
                        //==Ajout du 1er niveau de hiérarchie.
                      
                        TreeView1.Nodes.Add("A" + rstmpRow["Matricule"], sName);
                        TreeView1.Nodes.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    }
               
                    
                    TreeView1.Visible = true;
                }
                rstmp.Dispose();
                rstmp = null;
          
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex,this.Name + "fc_LoadtreeviewPer");
            }
          
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMat"></param>
        /// <param name="sValue"></param>
        private void fc_CHP_ChargePers( string sMat, string sValue)
        {
            rsCHP_ChargePersAdo = new ModAdo(); 
            ssCHP_ChargePers.UpdateData();

            if (!string.IsNullOrEmpty(sMat))
                sMat = General.Mid(sMat, 2, General.Len(sMat) - 1);

            sSQL = "SELECT CHP_Noauto, matricule, CHP_Mois, CHP_Annee,  CHP_Mtlibre,CHP_Charge, CHP_SA," 
                + " CHP_TotHeureA," + " CHP_KCP, CHP_Ftrans, CHP_Divers, CHP_TotheureEff" 
                + " From CHP_ChargePers" + " WHERE (matricule = '" + sMat + "')";

            rsCHP_ChargePers = rsCHP_ChargePersAdo.fc_OpenRecordSet(sSQL, ssCHP_ChargePers, "CHP_Noauto");
            if (string.IsNullOrEmpty(sMat))
            {
                ssCHP_ChargePers.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                sMatEncours = "";
                lblMat.Text = "Charges du personnel";
                
            }
            else
            {
                ssCHP_ChargePers.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                sMatEncours = sMat;
                lblMat.Text = sValue;
            }

            ssCHP_ChargePers.DataSource=rsCHP_ChargePers;
            ssCHP_ChargePers.UpdateData();

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropMois()
        {
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[12];
            for (i = 0; i <= 11; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }

            sheridan.AlimenteCmbAdditemTableau( ssDropMois, tabJour,  "");
            //this.ssCHP_ChargePers.Columns["CHP_MOIS"].DropDownHwnd = this.ssDropMois.hWnd;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCHP_ChargePers_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            DialogResult dr = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dr==DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCHP_ChargePers_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            var _with6 = ssCHP_ChargePers;
            if (ssCHP_ChargePers.ActiveCell.Column.Index == ssCHP_ChargePers.ActiveRow.Cells["CHP_MtLibre"].Column.Index)
            {          
                if (General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_MtLibre"].Text, 0).ToString() == "-1")
                {
                    ssCHP_ChargePers.ActiveRow.Cells["CHP_MtLibre"].Value= "1";
                }
                //' Debug.Print .Columns("CHP_SA").Text
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCHP_ChargePers_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            try
            {
                if (string.IsNullOrEmpty(sMatEncours))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation annulée, vous devez d'abord sélectionner un membre du personnel", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    ssCHP_ChargePers.ActiveRow.Cells["Matricule"].Value = sMatEncours;

                    if (General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_MtLibre"].Text, 0).ToString() != "1")
                    {

                        ssCHP_ChargePers.ActiveRow.Cells["CHP_Charge"].Value = ModCalcul.fc_CalculTauxTech(false,
                            Convert.ToDouble(General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_SA"].Value, 0)),
                            Convert.ToDouble(General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_KCP"].Value, 0)),
                            Convert.ToDouble(General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_Divers"].Value, 0)),
                            Convert.ToDouble(General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_Ftrans"].Value, 0)),
                            Convert.ToDouble(General.nz(ssCHP_ChargePers.ActiveRow.Cells["CHP_TotHeureA"].Value, 0)));

                     /*if (ssCHP_ChargePers.ActiveRow.Cells["CHP_Charge"].Value.ToString() =="0")//à Véifier
                        {
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_SA"].Value = 0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_Charge"].Value = 0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_Ftrans"].Value = 0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_Divers"].Value = 0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_TotHeureA"].Value = 0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_TotheureEff"].Value =0;
                            ssCHP_ChargePers.ActiveRow.Cells["CHP_KCP"].Value = 1;

                        }*/
                    }

                    if (string.IsNullOrEmpty(ssCHP_ChargePers.ActiveRow.Cells["CHP_Mois"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous Devez saisir un mois valide.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    if (General.IsDate(ssCHP_ChargePers.ActiveRow.Cells["CHP_Annee"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une année valide.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else
                    {
                        if (General.Len(ssCHP_ChargePers.ActiveRow.Cells["CHP_Annee"].Value.ToString()) != 4 
                            && General.IsNumeric(ssCHP_ChargePers.ActiveRow.Cells["CHP_Mois"].Value.ToString()))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'année doit être sur 4 chiffres", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                          //  e.Cancel = true;
                            return;
                        }
                    }

                }

                return;
            }catch(Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
           
        }

        private void ssCHP_ChargePers_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            
            this.ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_MOIS"].ValueList = this.ssDropMois;
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Noauto"].Hidden = true;
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["matricule"].Hidden = true;
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Mois"].Header.Caption = "Mois";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Annee"].Header.Caption = "Année";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Mtlibre"].Header.Caption = "Mtlibre";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Mtlibre"].Style= Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Charge"].Header.Caption = "Charge";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_SA"].Header.Caption = "Salaire Annuel";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_TotHeureA"].Header.Caption = "Heure Théorique annuel";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_KCP"].Header.Caption = "K Congés Payés";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Ftrans"].Header.Caption = "Frais Transport";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_Divers"].Header.Caption = "Frais Divers";
            ssCHP_ChargePers.DisplayLayout.Bands[0].Columns["CHP_TotheureEff"].Header.Caption = "Base Heures";

        }

        private void ssCHP_ChargePers_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.Row.Cells["CHP_Mtlibre"].Value == DBNull.Value)
            {
                e.Row.Cells["CHP_Mtlibre"].Value = false;
            }
            ssCHP_ChargePers.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCHP_ChargePers_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            int xx = rsCHP_ChargePersAdo.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCHP_ChargePers_AfterRowsDeleted(object sender, EventArgs e)
        {
            int xx = rsCHP_ChargePersAdo.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode node = TreeView1.ActiveNode;
          
                node.Override.ActiveNodeAppearance.Image = Properties.Resources.folder_opened16;
                fc_CHP_ChargePers(TreeView1.ActiveNode.Key, TreeView1.ActiveNode.Text);
          
        }
    }
}
