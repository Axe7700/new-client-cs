﻿namespace Axe_interDT.Views.Parametrages.Forms
{
    partial class frmAjoutUtil
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtUSR_Nt = new iTalk.iTalk_TextBox_Small2();
            this.txtUSR_Name = new iTalk.iTalk_TextBox_Small2();
            this.txtDROG_Nom = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(283, 11);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 45);
            this.cmdMAJ.TabIndex = 582;
            this.cmdMAJ.Tag = "";
            this.cmdMAJ.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(12, 95);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(103, 19);
            this.label33.TabIndex = 583;
            this.label33.Text = "Nom/Prenom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(12, 57);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 19);
            this.label1.TabIndex = 584;
            this.label1.Text = "Login Windows";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(12, 130);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 19);
            this.label2.TabIndex = 585;
            this.label2.Text = "Groupe";
            // 
            // txtUSR_Nt
            // 
            this.txtUSR_Nt.AccAcceptNumbersOnly = false;
            this.txtUSR_Nt.AccAllowComma = false;
            this.txtUSR_Nt.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUSR_Nt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUSR_Nt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUSR_Nt.AccHidenValue = "";
            this.txtUSR_Nt.AccNotAllowedChars = null;
            this.txtUSR_Nt.AccReadOnly = false;
            this.txtUSR_Nt.AccReadOnlyAllowDelete = false;
            this.txtUSR_Nt.AccRequired = false;
            this.txtUSR_Nt.BackColor = System.Drawing.Color.White;
            this.txtUSR_Nt.CustomBackColor = System.Drawing.Color.White;
            this.txtUSR_Nt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtUSR_Nt.ForeColor = System.Drawing.Color.Black;
            this.txtUSR_Nt.Location = new System.Drawing.Point(131, 57);
            this.txtUSR_Nt.Margin = new System.Windows.Forms.Padding(2);
            this.txtUSR_Nt.MaxLength = 32767;
            this.txtUSR_Nt.Multiline = false;
            this.txtUSR_Nt.Name = "txtUSR_Nt";
            this.txtUSR_Nt.ReadOnly = false;
            this.txtUSR_Nt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUSR_Nt.Size = new System.Drawing.Size(135, 27);
            this.txtUSR_Nt.TabIndex = 586;
            this.txtUSR_Nt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUSR_Nt.UseSystemPasswordChar = false;
            // 
            // txtUSR_Name
            // 
            this.txtUSR_Name.AccAcceptNumbersOnly = false;
            this.txtUSR_Name.AccAllowComma = false;
            this.txtUSR_Name.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUSR_Name.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUSR_Name.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUSR_Name.AccHidenValue = "";
            this.txtUSR_Name.AccNotAllowedChars = null;
            this.txtUSR_Name.AccReadOnly = false;
            this.txtUSR_Name.AccReadOnlyAllowDelete = false;
            this.txtUSR_Name.AccRequired = false;
            this.txtUSR_Name.BackColor = System.Drawing.Color.White;
            this.txtUSR_Name.CustomBackColor = System.Drawing.Color.White;
            this.txtUSR_Name.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtUSR_Name.ForeColor = System.Drawing.Color.Black;
            this.txtUSR_Name.Location = new System.Drawing.Point(131, 90);
            this.txtUSR_Name.Margin = new System.Windows.Forms.Padding(2);
            this.txtUSR_Name.MaxLength = 32767;
            this.txtUSR_Name.Multiline = false;
            this.txtUSR_Name.Name = "txtUSR_Name";
            this.txtUSR_Name.ReadOnly = false;
            this.txtUSR_Name.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUSR_Name.Size = new System.Drawing.Size(135, 27);
            this.txtUSR_Name.TabIndex = 587;
            this.txtUSR_Name.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUSR_Name.UseSystemPasswordChar = false;
            // 
            // txtDROG_Nom
            // 
            this.txtDROG_Nom.AccAcceptNumbersOnly = false;
            this.txtDROG_Nom.AccAllowComma = false;
            this.txtDROG_Nom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDROG_Nom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDROG_Nom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDROG_Nom.AccHidenValue = "";
            this.txtDROG_Nom.AccNotAllowedChars = null;
            this.txtDROG_Nom.AccReadOnly = false;
            this.txtDROG_Nom.AccReadOnlyAllowDelete = false;
            this.txtDROG_Nom.AccRequired = false;
            this.txtDROG_Nom.BackColor = System.Drawing.Color.White;
            this.txtDROG_Nom.CustomBackColor = System.Drawing.Color.White;
            this.txtDROG_Nom.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDROG_Nom.ForeColor = System.Drawing.Color.Black;
            this.txtDROG_Nom.Location = new System.Drawing.Point(131, 125);
            this.txtDROG_Nom.Margin = new System.Windows.Forms.Padding(2);
            this.txtDROG_Nom.MaxLength = 32767;
            this.txtDROG_Nom.Multiline = false;
            this.txtDROG_Nom.Name = "txtDROG_Nom";
            this.txtDROG_Nom.ReadOnly = false;
            this.txtDROG_Nom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDROG_Nom.Size = new System.Drawing.Size(135, 27);
            this.txtDROG_Nom.TabIndex = 588;
            this.txtDROG_Nom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDROG_Nom.UseSystemPasswordChar = false;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(283, 125);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheIntervenant.TabIndex = 589;
            this.cmdRechercheIntervenant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheIntervenant, "Recherche du deviseur");
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // frmAjoutUtil
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(354, 205);
            this.Controls.Add(this.cmdRechercheIntervenant);
            this.Controls.Add(this.txtDROG_Nom);
            this.Controls.Add(this.txtUSR_Name);
            this.Controls.Add(this.txtUSR_Nt);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.cmdMAJ);
            this.MaximumSize = new System.Drawing.Size(370, 244);
            this.MinimumSize = new System.Drawing.Size(370, 244);
            this.Name = "frmAjoutUtil";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAjoutUtil";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtUSR_Nt;
        public iTalk.iTalk_TextBox_Small2 txtUSR_Name;
        public iTalk.iTalk_TextBox_Small2 txtDROG_Nom;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}