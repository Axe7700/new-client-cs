﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Parametrages.Forms
{
    public partial class frmAjoutUtil : Form
    {
        public frmAjoutUtil()
        {
            InitializeComponent();
        }

        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            //== controle si ce login existe deja
            try { 
            txtUSR_Nt.Text = txtUSR_Nt.Text.Trim();
            sSQL = "SELECT     USR_Nt" + " From USR_Users" + " WHERE     (USR_Nt ='" + txtUSR_Nt.Text.Trim() + "')";
            var tmpAdo = new ModAdo();
            sSQL = tmpAdo.fc_ADOlibelle(sSQL);
           
            if (!string.IsNullOrEmpty(sSQL))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce login Windows existe déjà.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;               
            }


            if (string.IsNullOrEmpty(txtUSR_Name.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le nom est obligatoire.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;             
            }

            if (string.IsNullOrEmpty(txtDROG_Nom.Text))
            {
                //  MsgBox "Le groupe est obligatoire", vbInformation, "Ajout annulée"
                //  Exit Sub
            }
            else
            {
                sSQL = "SELECT     DROG_Nom From DROG_Groupe WHERE DROG_Nom ='" + txtDROG_Nom.Text + "'";
                sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                if (string.IsNullOrEmpty(sSQL))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce groupe existe déjà.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

            }

            sSQL = "SELECT     USR_Nt, USR_Name, DROG_Nom, USR_No"
                + " From USR_Users" 
                + " WHERE     (USR_No = 0)";

            rs = tmpAdo.fc_OpenRecordSet(sSQL);
            var NewRow = rs.NewRow();
            NewRow["USR_Nt"] = txtUSR_Nt.Text;
            NewRow["USR_Name"] = txtUSR_Name.Text;
            NewRow["DROG_Nom"] = General.nz( txtDROG_Nom.Text, System.DBNull.Value);
            rs.Rows.Add(NewRow);
            int xx=tmpAdo.Update();

            rs.Dispose();
            rs = null;
            this.Close();
            return;
        }catch(Exception ex)
            {             
                Erreurs.gFr_debug(ex,this.Name + ";cmdMAJ_Click");
            }

}
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {

            string req = null;
            string where = "";
            req = "SELECT DROG_Nom as \"Groupe\", DROG_Drescription as \"Drescription\"  FROM DROG_Groupe";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un groupe" };
            fg.SetValues(new Dictionary<string, string> { { "DROG_Nom", txtDROG_Nom.Text } });
            fg.ugResultat.DoubleClickRow += (se, ev) => {
                txtDROG_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) => {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {                 
                    txtDROG_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }
    }
}
