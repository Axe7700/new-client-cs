﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Parametrages.Forms
{
    public partial class frmAjoutGroupe : Form
    {
        public frmAjoutGroupe()
        {
            InitializeComponent();
        }

        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            //== controle si ce login existe deja
            try
            {
                txtDROG_Nom.Text = txtDROG_Nom.Text.Trim();
                sSQL = "SELECT     DROG_Nom" + " From DROG_Groupe" + " WHERE     (DROG_Nom ='" + txtDROG_Nom.Text.Trim()+ "')";
                var tmpAdo = new ModAdo();
                sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                if (!string.IsNullOrEmpty(sSQL))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce groupe existe déjà.",  "Ajout annulée",MessageBoxButtons.OK,MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrEmpty(txtDROG_Nom.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le groupe est obligatoire.", "Ajout annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sSQL = "SELECT      DROG_Nom, DROG_Drescription" 
                    + " FROM         DROG_Groupe" 
                    + " WHERE DROG_Noauto = 0";

                rs =tmpAdo.fc_OpenRecordSet(sSQL);
                var NewRow = rs.NewRow();
                NewRow["DROG_Nom"] = txtDROG_Nom.Text;
                NewRow["DROG_Drescription"] = txtDROG_Drescription.Text;
                rs.Rows.Add(NewRow);
                int xx=tmpAdo.Update();

                rs.Dispose();
                rs = null;
                this.Close();
                return;
            }catch(Exception ex)
            {             
                Erreurs.gFr_debug(ex,this.Name + ";cmdMAJ_Click");
            }
           
        }
    }
}
