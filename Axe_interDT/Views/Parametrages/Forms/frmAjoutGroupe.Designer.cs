﻿namespace Axe_interDT.Views.Parametrages.Forms
{
    partial class frmAjoutGroupe
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtDROG_Drescription = new iTalk.iTalk_TextBox_Small2();
            this.txtDROG_Nom = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.SuspendLayout();
            // 
            // txtDROG_Drescription
            // 
            this.txtDROG_Drescription.AccAcceptNumbersOnly = false;
            this.txtDROG_Drescription.AccAllowComma = false;
            this.txtDROG_Drescription.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDROG_Drescription.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDROG_Drescription.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDROG_Drescription.AccHidenValue = "";
            this.txtDROG_Drescription.AccNotAllowedChars = null;
            this.txtDROG_Drescription.AccReadOnly = false;
            this.txtDROG_Drescription.AccReadOnlyAllowDelete = false;
            this.txtDROG_Drescription.AccRequired = false;
            this.txtDROG_Drescription.BackColor = System.Drawing.Color.White;
            this.txtDROG_Drescription.CustomBackColor = System.Drawing.Color.White;
            this.txtDROG_Drescription.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDROG_Drescription.ForeColor = System.Drawing.Color.Black;
            this.txtDROG_Drescription.Location = new System.Drawing.Point(136, 109);
            this.txtDROG_Drescription.Margin = new System.Windows.Forms.Padding(2);
            this.txtDROG_Drescription.MaxLength = 32767;
            this.txtDROG_Drescription.Multiline = false;
            this.txtDROG_Drescription.Name = "txtDROG_Drescription";
            this.txtDROG_Drescription.ReadOnly = false;
            this.txtDROG_Drescription.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDROG_Drescription.Size = new System.Drawing.Size(135, 27);
            this.txtDROG_Drescription.TabIndex = 1;
            this.txtDROG_Drescription.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDROG_Drescription.UseSystemPasswordChar = false;
            // 
            // txtDROG_Nom
            // 
            this.txtDROG_Nom.AccAcceptNumbersOnly = false;
            this.txtDROG_Nom.AccAllowComma = false;
            this.txtDROG_Nom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDROG_Nom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDROG_Nom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDROG_Nom.AccHidenValue = "";
            this.txtDROG_Nom.AccNotAllowedChars = null;
            this.txtDROG_Nom.AccReadOnly = false;
            this.txtDROG_Nom.AccReadOnlyAllowDelete = false;
            this.txtDROG_Nom.AccRequired = false;
            this.txtDROG_Nom.BackColor = System.Drawing.Color.White;
            this.txtDROG_Nom.CustomBackColor = System.Drawing.Color.White;
            this.txtDROG_Nom.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDROG_Nom.ForeColor = System.Drawing.Color.Black;
            this.txtDROG_Nom.Location = new System.Drawing.Point(136, 66);
            this.txtDROG_Nom.Margin = new System.Windows.Forms.Padding(2);
            this.txtDROG_Nom.MaxLength = 32767;
            this.txtDROG_Nom.Multiline = false;
            this.txtDROG_Nom.Name = "txtDROG_Nom";
            this.txtDROG_Nom.ReadOnly = false;
            this.txtDROG_Nom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDROG_Nom.Size = new System.Drawing.Size(135, 27);
            this.txtDROG_Nom.TabIndex = 0;
            this.txtDROG_Nom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDROG_Nom.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(17, 66);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 19);
            this.label1.TabIndex = 592;
            this.label1.Text = "Groupe";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(17, 109);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(89, 19);
            this.label33.TabIndex = 591;
            this.label33.Text = "Description";
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(288, 11);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 45);
            this.cmdMAJ.TabIndex = 590;
            this.cmdMAJ.Tag = "";
            this.cmdMAJ.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // frmAjoutGroupe
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(364, 173);
            this.Controls.Add(this.txtDROG_Drescription);
            this.Controls.Add(this.txtDROG_Nom);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.cmdMAJ);
            this.MaximumSize = new System.Drawing.Size(380, 212);
            this.MinimumSize = new System.Drawing.Size(380, 212);
            this.Name = "frmAjoutGroupe";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAjoutGroupe";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public iTalk.iTalk_TextBox_Small2 txtDROG_Drescription;
        public iTalk.iTalk_TextBox_Small2 txtDROG_Nom;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button cmdMAJ;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}