﻿namespace Axe_interDT.Views.Commande.UserPreCommande.Forms
{
    partial class FrmMajFourn
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.txtPRE_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.ssPRC_PreCmdCorps = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdAppliqueClose = new System.Windows.Forms.Button();
            this.txtcleAuto = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            this.cmdSelect = new System.Windows.Forms.Button();
            this.txtPRC_FournisseurRef = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCodeFournisseur = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.cmdRechercheFourn = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ssPRC_PreCmdCorps)).BeginInit();
            this.SuspendLayout();
            // 
            // txtPRE_NoAuto
            // 
            this.txtPRE_NoAuto.AccAcceptNumbersOnly = false;
            this.txtPRE_NoAuto.AccAllowComma = false;
            this.txtPRE_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRE_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPRE_NoAuto.AccHidenValue = "";
            this.txtPRE_NoAuto.AccNotAllowedChars = null;
            this.txtPRE_NoAuto.AccReadOnly = false;
            this.txtPRE_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtPRE_NoAuto.AccRequired = false;
            this.txtPRE_NoAuto.BackColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRE_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtPRE_NoAuto.Location = new System.Drawing.Point(407, 22);
            this.txtPRE_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRE_NoAuto.MaxLength = 32767;
            this.txtPRE_NoAuto.Multiline = false;
            this.txtPRE_NoAuto.Name = "txtPRE_NoAuto";
            this.txtPRE_NoAuto.ReadOnly = false;
            this.txtPRE_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRE_NoAuto.Size = new System.Drawing.Size(39, 27);
            this.txtPRE_NoAuto.TabIndex = 583;
            this.txtPRE_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRE_NoAuto.UseSystemPasswordChar = false;
            this.txtPRE_NoAuto.Visible = false;
            // 
            // ssPRC_PreCmdCorps
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssPRC_PreCmdCorps.DisplayLayout.Appearance = appearance1;
            this.ssPRC_PreCmdCorps.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssPRC_PreCmdCorps.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssPRC_PreCmdCorps.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssPRC_PreCmdCorps.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssPRC_PreCmdCorps.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssPRC_PreCmdCorps.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssPRC_PreCmdCorps.DisplayLayout.MaxColScrollRegions = 1;
            this.ssPRC_PreCmdCorps.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssPRC_PreCmdCorps.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssPRC_PreCmdCorps.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssPRC_PreCmdCorps.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssPRC_PreCmdCorps.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssPRC_PreCmdCorps.Location = new System.Drawing.Point(12, 112);
            this.ssPRC_PreCmdCorps.Name = "ssPRC_PreCmdCorps";
            this.ssPRC_PreCmdCorps.Size = new System.Drawing.Size(860, 520);
            this.ssPRC_PreCmdCorps.TabIndex = 584;
            this.ssPRC_PreCmdCorps.Text = "ultraGrid1";
            this.ssPRC_PreCmdCorps.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssPRC_PreCmdCorps_InitializeLayout);
            this.ssPRC_PreCmdCorps.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssPRC_PreCmdCorps_InitializeRow);
            this.ssPRC_PreCmdCorps.AfterRowsDeleted += new System.EventHandler(this.ssPRC_PreCmdCorps_AfterRowsDeleted);
            this.ssPRC_PreCmdCorps.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssPRC_PreCmdCorps_AfterRowUpdate);
            this.ssPRC_PreCmdCorps.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ssPRC_PreCmdCorps_ClickCellButton);
            this.ssPRC_PreCmdCorps.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssPRC_PreCmdCorps_BeforeRowsDeleted);
            // 
            // cmdAppliqueClose
            // 
            this.cmdAppliqueClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliqueClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliqueClose.FlatAppearance.BorderSize = 0;
            this.cmdAppliqueClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliqueClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliqueClose.ForeColor = System.Drawing.Color.White;
            this.cmdAppliqueClose.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdAppliqueClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliqueClose.Location = new System.Drawing.Point(718, 9);
            this.cmdAppliqueClose.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.cmdAppliqueClose.Name = "cmdAppliqueClose";
            this.cmdAppliqueClose.Size = new System.Drawing.Size(157, 35);
            this.cmdAppliqueClose.TabIndex = 582;
            this.cmdAppliqueClose.Text = "Appliquer et fermer";
            this.cmdAppliqueClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAppliqueClose.UseVisualStyleBackColor = false;
            this.cmdAppliqueClose.Click += new System.EventHandler(this.cmdAppliqueClose_Click);
            // 
            // txtcleAuto
            // 
            this.txtcleAuto.AccAcceptNumbersOnly = false;
            this.txtcleAuto.AccAllowComma = false;
            this.txtcleAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcleAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcleAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcleAuto.AccHidenValue = "";
            this.txtcleAuto.AccNotAllowedChars = null;
            this.txtcleAuto.AccReadOnly = false;
            this.txtcleAuto.AccReadOnlyAllowDelete = false;
            this.txtcleAuto.AccRequired = false;
            this.txtcleAuto.BackColor = System.Drawing.Color.White;
            this.txtcleAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtcleAuto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtcleAuto.ForeColor = System.Drawing.Color.Black;
            this.txtcleAuto.Location = new System.Drawing.Point(407, 9);
            this.txtcleAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtcleAuto.MaxLength = 32767;
            this.txtcleAuto.Multiline = false;
            this.txtcleAuto.Name = "txtcleAuto";
            this.txtcleAuto.ReadOnly = false;
            this.txtcleAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcleAuto.Size = new System.Drawing.Size(39, 27);
            this.txtcleAuto.TabIndex = 585;
            this.txtcleAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcleAuto.UseSystemPasswordChar = false;
            this.txtcleAuto.Visible = false;
            // 
            // label1
            // 
            this.label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.label1.Location = new System.Drawing.Point(9, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(393, 61);
            this.label1.TabIndex = 586;
            this.label1.Text = "Pour modifier le fournisseur d\'un article, Sélectionnez d\'abord un fournisseur en" +
    "suite sélectionnez dans la grille les articles à mettre à jour et cliquez sur le" +
    " bouton Apliquer.";
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(593, 9);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(123, 35);
            this.cmdAppliquer.TabIndex = 587;
            this.cmdAppliquer.Text = "       Appliquer";
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // cmdSelect
            // 
            this.cmdSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSelect.FlatAppearance.BorderSize = 0;
            this.cmdSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSelect.ForeColor = System.Drawing.Color.White;
            this.cmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSelect.Location = new System.Drawing.Point(468, 9);
            this.cmdSelect.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.cmdSelect.Name = "cmdSelect";
            this.cmdSelect.Size = new System.Drawing.Size(123, 35);
            this.cmdSelect.TabIndex = 588;
            this.cmdSelect.Text = "Tout selectionner";
            this.cmdSelect.UseVisualStyleBackColor = false;
            this.cmdSelect.Click += new System.EventHandler(this.cmdSelect_Click);
            // 
            // txtPRC_FournisseurRef
            // 
            this.txtPRC_FournisseurRef.AccAcceptNumbersOnly = false;
            this.txtPRC_FournisseurRef.AccAllowComma = false;
            this.txtPRC_FournisseurRef.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRC_FournisseurRef.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRC_FournisseurRef.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPRC_FournisseurRef.AccHidenValue = "";
            this.txtPRC_FournisseurRef.AccNotAllowedChars = null;
            this.txtPRC_FournisseurRef.AccReadOnly = false;
            this.txtPRC_FournisseurRef.AccReadOnlyAllowDelete = false;
            this.txtPRC_FournisseurRef.AccRequired = false;
            this.txtPRC_FournisseurRef.BackColor = System.Drawing.Color.White;
            this.txtPRC_FournisseurRef.CustomBackColor = System.Drawing.Color.White;
            this.txtPRC_FournisseurRef.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtPRC_FournisseurRef.ForeColor = System.Drawing.Color.Black;
            this.txtPRC_FournisseurRef.Location = new System.Drawing.Point(159, 82);
            this.txtPRC_FournisseurRef.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRC_FournisseurRef.MaxLength = 32767;
            this.txtPRC_FournisseurRef.Multiline = false;
            this.txtPRC_FournisseurRef.Name = "txtPRC_FournisseurRef";
            this.txtPRC_FournisseurRef.ReadOnly = false;
            this.txtPRC_FournisseurRef.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRC_FournisseurRef.Size = new System.Drawing.Size(278, 27);
            this.txtPRC_FournisseurRef.TabIndex = 0;
            this.txtPRC_FournisseurRef.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRC_FournisseurRef.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(71, 81);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(83, 19);
            this.label33.TabIndex = 589;
            this.label33.Text = "Remplacer";
            // 
            // txtCodeFournisseur
            // 
            this.txtCodeFournisseur.AccAcceptNumbersOnly = false;
            this.txtCodeFournisseur.AccAllowComma = false;
            this.txtCodeFournisseur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFournisseur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFournisseur.AccHidenValue = "";
            this.txtCodeFournisseur.AccNotAllowedChars = null;
            this.txtCodeFournisseur.AccReadOnly = false;
            this.txtCodeFournisseur.AccReadOnlyAllowDelete = false;
            this.txtCodeFournisseur.AccRequired = false;
            this.txtCodeFournisseur.BackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFournisseur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeFournisseur.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFournisseur.Location = new System.Drawing.Point(507, 82);
            this.txtCodeFournisseur.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFournisseur.MaxLength = 32767;
            this.txtCodeFournisseur.Multiline = false;
            this.txtCodeFournisseur.Name = "txtCodeFournisseur";
            this.txtCodeFournisseur.ReadOnly = false;
            this.txtCodeFournisseur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFournisseur.Size = new System.Drawing.Size(278, 27);
            this.txtCodeFournisseur.TabIndex = 1;
            this.txtCodeFournisseur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFournisseur.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(465, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 19);
            this.label2.TabIndex = 591;
            this.label2.Text = "Par";
            // 
            // cmdRechercheFourn
            // 
            this.cmdRechercheFourn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheFourn.FlatAppearance.BorderSize = 0;
            this.cmdRechercheFourn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheFourn.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheFourn.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheFourn.Location = new System.Drawing.Point(789, 82);
            this.cmdRechercheFourn.Margin = new System.Windows.Forms.Padding(2, 2, 2, 8);
            this.cmdRechercheFourn.Name = "cmdRechercheFourn";
            this.cmdRechercheFourn.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheFourn.TabIndex = 593;
            this.cmdRechercheFourn.Tag = "BCD";
            this.cmdRechercheFourn.UseVisualStyleBackColor = false;
            this.cmdRechercheFourn.Click += new System.EventHandler(this.cmdRechercheFourn_Click);
            // 
            // FrmMajFourn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(884, 644);
            this.Controls.Add(this.cmdRechercheFourn);
            this.Controls.Add(this.txtCodeFournisseur);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtPRC_FournisseurRef);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.cmdSelect);
            this.Controls.Add(this.cmdAppliquer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtcleAuto);
            this.Controls.Add(this.ssPRC_PreCmdCorps);
            this.Controls.Add(this.txtPRE_NoAuto);
            this.Controls.Add(this.cmdAppliqueClose);
            this.MaximumSize = new System.Drawing.Size(900, 683);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(900, 683);
            this.Name = "FrmMajFourn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.FrmMajFourn_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ssPRC_PreCmdCorps)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button cmdAppliqueClose;
        public iTalk.iTalk_TextBox_Small2 txtPRE_NoAuto;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssPRC_PreCmdCorps;
        public iTalk.iTalk_TextBox_Small2 txtcleAuto;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdAppliquer;
        public System.Windows.Forms.Button cmdSelect;
        public iTalk.iTalk_TextBox_Small2 txtPRC_FournisseurRef;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtCodeFournisseur;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Button cmdRechercheFourn;
    }
}