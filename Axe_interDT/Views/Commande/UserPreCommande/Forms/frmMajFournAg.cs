﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Commande.UserPreCommande.Forms
{
    public partial class frmMajFournAg : Form
    {
        DataTable rsFournAg = null;
        ModAdo modAdorsFournAg = new ModAdo();
        public frmMajFournAg()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauverPre_Click(object sender, EventArgs e)
        {
            ssFournAg.UpdateData();
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmMajFournAg_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            string sSQL = "SELECT  distinct PRC_FournisseurRef,PRC_FournDTCle, PRC_FournisseurDT, fns_no, '' AS FNS_Agence" + " FROM   PRC_PreCmdCorps" + " WHERE PRE_NoAuto =" + txtPRE_NoAuto.Text +
               " AND PRC_Sel = 1 and (PRC_histo = 0 or PRC_Histo is  null) ";

            var _with1 = ssFournAg;

            rsFournAg = modAdorsFournAg.fc_OpenRecordSet(sSQL);

            var _with2 = rsFournAg;
            ssFournAg.DataSource = _with2;

            rsFournAg = null;
            ssFournAg.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Header.Caption = "Fournisseur Base " + General.sDefautFournisseur;
            ssFournAg.DisplayLayout.Bands[0].Columns["FNS_Agence"].Header.Caption = "Agence Base " + General.sDefautFournisseur;
            _with1.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Header.Caption = "Fournisseur base " + General.NomSociete;
            _with1.DisplayLayout.Bands[0].Columns["FNS_Agence"].Header.Caption = "Agence base " + General.NomSociete;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="nargag"></param>
        /// <returns></returns>
        private string selectfournAg(double nargag)
        {
            string functionReturnValue = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sSQL = null;

            modAdors = new ModAdo();

            sSQL = "SELECT FNS_Agence, FNS_NO, FNS_Fax FROM FNS_Agences where FNS_No =" + nargag;

            rs = modAdors.fc_OpenRecordSet(sSQL);

            if (rs.Rows.Count == 0)
                return functionReturnValue;
            functionReturnValue = General.nz(rs.Rows[0]["FNS_Agence"], "").ToString();
            return functionReturnValue;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFournAg_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.Row.Cells["FNS_No"].Value != DBNull.Value)
            {
                e.Row.Cells["FNS_Agence"].Value = selectfournAg(Convert.ToDouble(e.Row.Cells["FNS_No"].Value));
            }
            ssFournAg.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFournAg_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            string sFournRef = null;
            string sFournDT = null;
            string sAgenceDT = null;
            int lPRC_FournDTCle = 0;

            string sSQL = "";

            if (string.IsNullOrEmpty(e.Row.Cells["PRC_FournisseurDT"].Text))
                return;

            sFournDT = e.Row.Cells["PRC_FournisseurDT"].Text.ToUpper().Trim();
            sFournRef = e.Row.Cells["PRC_FournisseurRef"].Text.ToUpper().Trim();
            lPRC_FournDTCle = Convert.ToInt32(General.nz((e.Row.Cells["PRC_FournDTCle"].Text), 0));

            if (!string.IsNullOrEmpty(sFournDT) && !string.IsNullOrEmpty(e.Row.Cells["FNS_Agence"].Text))
            {
                sAgenceDT = e.Row.Cells["fns_no"].Text;

            }

            if (sFournDT != sFournRef)
                sFournRef = sFournDT;


            sSQL = "UPDATE PRC_PreCmdCorps  set " + " PRC_FournisseurRef ='" + sFournRef + "'" + " , PRC_FournDTCle =" + lPRC_FournDTCle + " ,PRC_FournisseurDT ='" + sFournDT + "'";
            sSQL = sSQL + " ,fns_no ='" + General.nz(sAgenceDT, 0) + "'";

            sSQL = sSQL + " WHERE PRE_NOAuto =" + txtPRE_NoAuto.Text + " and PRC_histo = 0 AND PRC_Sel = 1 and PRC_FournisseurRef ='" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["PRC_FournisseurRef"].Text) + "'";
            General.Execute(sSQL);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFournAg_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            var _with4 = ssFournAg;
            if (ssFournAg.ActiveCell.Column.Key.ToUpper() == "FNS_Agence".ToUpper())
            {
                if (string.IsNullOrEmpty(ssFournAg.ActiveCell.Text))
                {
                    _with4.ActiveRow.Cells["fns_no"].Value = DBNull.Value;
                }
            }

            if (ssFournAg.ActiveCell.Column.Key.ToUpper() == "PRC_FournisseurDT".ToUpper())
            {
                if (string.IsNullOrEmpty(ssFournAg.ActiveCell.Text))
                {
                    _with4.ActiveRow.Cells["PRC_FournDTCle"].Value = DBNull.Value;
                }

            }
        }
            
      

        private void ssFournAg_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            double nCode = 0;

            if (e.Cell.Column.Key.ToUpper() == "PRC_FournisseurDT".ToUpper())//====================>Tested
            {
                string requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\"" + " , CleAuto as \"Numero Interne\"" + " FROM fournisseurArticle";
                string where_order = "  (code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 )  ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un fournisseur à votre bon de pre commande" };
                fg.SetValues(new Dictionary<string, string> { { "Code", ssFournAg.ActiveRow.Cells["PRC_Fournisseurdt"].Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    ssFournAg.ActiveRow.Cells["PRC_Fournisseurdt"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                    ssFournAg.ActiveRow.Cells["PRC_FournDTCle"].Value = fg.ugResultat.ActiveRow.Cells["Numero Interne"].Value;
                    ssFournAg.Update();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        ssFournAg.ActiveRow.Cells["PRC_Fournisseurdt"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                        ssFournAg.ActiveRow.Cells["PRC_FournDTCle"].Value = fg.ugResultat.ActiveRow.Cells["Numero Interne"].Value;
                        ssFournAg.Update();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

            }
            else if (e.Cell.Column.Key.ToUpper() == "FNS_Agence".ToUpper())
            {

                if (Convert.ToInt32(General.nz(ssFournAg.ActiveRow.Cells["PRC_FournDTCle"].Text, 0)) == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord sélectionner un fournisseur", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                //RechercheMultiCritere.Height = CInt(Line43.Y1)
                //.stxt = "SELECT FNS_No as [FNS_No], FNS_Agence as [Agence] , Fns_Contact as [Contact]," _
                //& " Fns_Ville as [Ville] FROM FNS_Agences"


                //.Move CInt(Frame9.Width + Frame9.Left) + 200, 1400



                string requete = "SELECT code as \"code\" , FNS_Agence as \"Agence\" , Fns_Contact as \"Contact\", Fns_Ville as \"Ville\", " +
                    " FNS_No as \"Numero interne\"" + " FROM FNS_Agences inner join FournisseurArticle on FournisseurArticle.cleAuto = FNS_Agences.Fns_Cle";
                string where_order = "Fns_Cle = " + General.nz(ssFournAg.ActiveRow.Cells["PRC_FournDTCle"].Text, 0);
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher une agence fournisseur à votre bon de pre commande" };
                fg.Activated += (se, ev) =>
                {
                    fg.ugResultat.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    nCode = Convert.ToDouble(fg.ugResultat.ActiveRow.Cells["Numero interne"].Value);
                    ssFournAg.ActiveRow.Cells["FNS_NO"].Value = nCode;
                    ssFournAg.ActiveRow.Cells["FNS_Agence"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                    ssFournAg.UpdateData();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        nCode = Convert.ToDouble(fg.ugResultat.ActiveRow.Cells["Numero interne"].Value);
                        ssFournAg.ActiveRow.Cells["FNS_NO"].Value = nCode;
                        ssFournAg.ActiveRow.Cells["FNS_Agence"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                        ssFournAg.UpdateData();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssFournAg_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssFournAg.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        
            ssFournAg.DisplayLayout.Bands[0].Columns["PRC_FournDTCle"].Hidden = true;
          
            ssFournAg.DisplayLayout.Bands[0].Columns["fns_no"].Hidden = true;

           
            ssFournAg.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            ssFournAg.DisplayLayout.Bands[0].Columns["FNS_Agence"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;

            ssFournAg.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].CellButtonAppearance.Image = Properties.Resources.Edit_16x16;

            ssFournAg.DisplayLayout.Bands[0].Columns["FNS_Agence"].CellButtonAppearance.Image = Properties.Resources.Edit_16x16;
        }

        
    }
}
