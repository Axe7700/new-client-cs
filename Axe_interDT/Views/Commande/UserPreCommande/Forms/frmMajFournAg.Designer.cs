﻿namespace Axe_interDT.Views.Commande.UserPreCommande.Forms
{
    partial class frmMajFournAg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.CmdSauverPre = new System.Windows.Forms.Button();
            this.txtPRE_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.ssFournAg = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ssFournAg)).BeginInit();
            this.SuspendLayout();
            // 
            // CmdSauverPre
            // 
            this.CmdSauverPre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauverPre.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauverPre.FlatAppearance.BorderSize = 0;
            this.CmdSauverPre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauverPre.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauverPre.ForeColor = System.Drawing.Color.White;
            this.CmdSauverPre.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauverPre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdSauverPre.Location = new System.Drawing.Point(302, 9);
            this.CmdSauverPre.Margin = new System.Windows.Forms.Padding(2, 0, 0, 0);
            this.CmdSauverPre.Name = "CmdSauverPre";
            this.CmdSauverPre.Size = new System.Drawing.Size(193, 35);
            this.CmdSauverPre.TabIndex = 582;
            this.CmdSauverPre.Text = "Appliquer et fermer";
            this.CmdSauverPre.UseVisualStyleBackColor = false;
            this.CmdSauverPre.Click += new System.EventHandler(this.CmdSauverPre_Click);
            // 
            // txtPRE_NoAuto
            // 
            this.txtPRE_NoAuto.AccAcceptNumbersOnly = false;
            this.txtPRE_NoAuto.AccAllowComma = false;
            this.txtPRE_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRE_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPRE_NoAuto.AccHidenValue = "";
            this.txtPRE_NoAuto.AccNotAllowedChars = null;
            this.txtPRE_NoAuto.AccReadOnly = false;
            this.txtPRE_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtPRE_NoAuto.AccRequired = false;
            this.txtPRE_NoAuto.BackColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtPRE_NoAuto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRE_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtPRE_NoAuto.Location = new System.Drawing.Point(21, 22);
            this.txtPRE_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRE_NoAuto.MaxLength = 32767;
            this.txtPRE_NoAuto.Multiline = false;
            this.txtPRE_NoAuto.Name = "txtPRE_NoAuto";
            this.txtPRE_NoAuto.ReadOnly = false;
            this.txtPRE_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRE_NoAuto.Size = new System.Drawing.Size(39, 27);
            this.txtPRE_NoAuto.TabIndex = 583;
            this.txtPRE_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRE_NoAuto.UseSystemPasswordChar = false;
            this.txtPRE_NoAuto.Visible = false;
            // 
            // ssFournAg
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssFournAg.DisplayLayout.Appearance = appearance1;
            this.ssFournAg.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssFournAg.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFournAg.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFournAg.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssFournAg.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssFournAg.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssFournAg.DisplayLayout.MaxColScrollRegions = 1;
            this.ssFournAg.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssFournAg.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssFournAg.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssFournAg.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssFournAg.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssFournAg.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssFournAg.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssFournAg.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssFournAg.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssFournAg.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssFournAg.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssFournAg.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssFournAg.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssFournAg.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssFournAg.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssFournAg.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssFournAg.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssFournAg.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssFournAg.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssFournAg.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssFournAg.Location = new System.Drawing.Point(12, 49);
            this.ssFournAg.Name = "ssFournAg";
            this.ssFournAg.Size = new System.Drawing.Size(483, 583);
            this.ssFournAg.TabIndex = 584;
            this.ssFournAg.Text = "ultraGrid1";
            this.ssFournAg.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssFournAg_InitializeLayout);
            this.ssFournAg.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssFournAg_InitializeRow);
            this.ssFournAg.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssFournAg_AfterRowUpdate);
            this.ssFournAg.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ssFournAg_ClickCellButton);
            this.ssFournAg.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssFournAg_BeforeExitEditMode);
            // 
            // frmMajFournAg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(504, 644);
            this.Controls.Add(this.ssFournAg);
            this.Controls.Add(this.txtPRE_NoAuto);
            this.Controls.Add(this.CmdSauverPre);
            this.MaximumSize = new System.Drawing.Size(520, 683);
            this.MinimumSize = new System.Drawing.Size(520, 683);
            this.Name = "frmMajFournAg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Modifier le fournisseur/agence par bon";
            this.Load += new System.EventHandler(this.frmMajFournAg_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ssFournAg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button CmdSauverPre;
        public iTalk.iTalk_TextBox_Small2 txtPRE_NoAuto;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssFournAg;
    }
}