﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Commande.UserPreCommande.Forms
{
    public partial class FrmMajFourn : Form
    {
        DataTable rsSElFourn = null;
        ModAdo modAdorsSElFourn = null;
        public FrmMajFourn()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliqueClose_Click(object sender, EventArgs e)
        {
            fc_MajFourn();
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_MajFourn()
        {
            string sCleAuto = null;
            bool sSelect = false;

            using (var tmpModAdo = new ModAdo())
                sCleAuto = tmpModAdo.fc_ADOlibelle("SELECT cleAuto FROM fournisseurArticle" + " where Code='" + StdSQLchaine.gFr_DoublerQuote(txtCodeFournisseur.Text) + "'");

            try
            {
                if (string.IsNullOrEmpty(sCleAuto))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fournisseur n'existe pas.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                else
                {
                    txtcleAuto.Text = sCleAuto;
                }
                var _with1 = rsSElFourn;
                ssPRC_PreCmdCorps.UpdateData();
                sSelect = false;
                foreach (DataRow rsSElFournRow in rsSElFourn.Rows)
                {
                    if (Convert.ToInt32(General.nz(rsSElFournRow["PRC_SelEnCours"], 0)) == 1)
                    {
                        sSelect = true;
                        rsSElFournRow["PRC_SelEnCours"] = 0;
                        rsSElFournRow["PRC_FournisseurRef"] = txtCodeFournisseur.Text;
                        rsSElFournRow["PRC_FournisseurDT"] = txtCodeFournisseur.Text;
                        rsSElFournRow["PRC_FournDTCle"] = txtcleAuto.Text;
                    }
                }
                modAdorsSElFourn.Update();
                ssPRC_PreCmdCorps.Refresh();
                if (sSelect == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas sélectionnée de ligne.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_MajFourn;");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliquer_Click(object sender, EventArgs e)
        {
            fc_MajFourn();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheFourn_Click(object sender, EventArgs e)
        {
            string sCode = null;
            double nCode = 0;
            try
            {
                string requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\"" + ", cleAuto as \"Numéro interne\"" + " FROM fournisseurArticle";
                string where_order = " (code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 ) ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un fournisseur à votre bon de commande" };
                fg.SetValues(new Dictionary<string, string> { { "Code", txtCodeFournisseur.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeFournisseur.Text = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                    txtcleAuto.Text = fg.ugResultat.ActiveRow.Cells["Numéro interne"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeFournisseur.Text = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                        txtcleAuto.Text = fg.ugResultat.ActiveRow.Cells["Numéro interne"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheFourn_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSelect_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodeFournisseur.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }
            var _with3 = rsSElFourn;
            ssPRC_PreCmdCorps.UpdateData();
            foreach (DataRow rsSElFournRow in rsSElFourn.Rows)
            {
                rsSElFournRow["PRC_SelEnCours"] = 1;
            }
            modAdorsSElFourn.Update();
            ssPRC_PreCmdCorps.DataSource = rsSElFourn;

        }


        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmMajFourn_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_SelFourn(txtPRE_NoAuto.Text);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        private void fc_SelFourn(string sNoAuto)
        {

            General.sSQL = "SELECT '' as button, PRC_Sel, PRC_NoAuto, PRE_NoAuto,PRC_Chrono, PRC_References, PRC_Designation," + " PRC_Quantite, PRC_PrixHT, PRC_Unite, PRC_NoLigne," +
                " PRC_Fournisseur,PRC_FournisseurDT,PRC_FournisseurRef, " + "  NoAutoGecet, PRC_fabref, PRC_fourref, PRC_NobonDeCommande, PRC_SelEnCours, PRC_FournDTCle" + " FROM PRC_PreCmdCorps";


            General.sSQL = General.sSQL + " WHERE PRE_NoAuto=" + sNoAuto + " and (PRC_Histo = 0 OR PRC_Histo IS NULL) and PRC_Sel = 1";

            if (!string.IsNullOrEmpty(txtPRC_FournisseurRef.Text) && General.UCase(txtPRC_FournisseurRef.Text) != General.UCase(General.cFournFrmMajFourn))
            {
                General.sSQL = General.sSQL + " AND PRC_FournisseurRef ='" + StdSQLchaine.gFr_DoublerQuote(txtPRC_FournisseurRef.Text) + "'";
            }
            if (string.IsNullOrEmpty(txtPRC_FournisseurRef.Text))
            {
                txtPRC_FournisseurRef.Text = General.cFournFrmMajFourn;
            }
            modAdorsSElFourn = new ModAdo();
            rsSElFourn = modAdorsSElFourn.fc_OpenRecordSet(General.sSQL);

            foreach (DataRow rsSElFournRow in rsSElFourn.Rows)
            {
                rsSElFournRow["PRC_SelEnCours"] = 0;
            }

            modAdorsSElFourn.Update();


            ssPRC_PreCmdCorps.DataSource = rsSElFourn;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodeFournisseur.Text))//tested
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_SelEnCours"].Text == "1")
            {
                ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_SelEnCours"].Value = 0;
            }
            else
            {
                ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_SelEnCours"].Value = 1;
            }
            ssPRC_PreCmdCorps.UpdateData();
        }

        private void ssPRC_PreCmdCorps_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            int i = 0;

            if (e.Row.Cells["PRC_SelEnCours"].Text == "1")
            {
                //ssPRC_PreCmdCorps.Columns("button").CellStyleSet "coche"

                for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                {
                    // e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(55, 84, 96);
                    e.Row.Cells[i].Appearance.BackColor = Color.Gray;
                }

                //====> Mondir le 06.07.2020 : Commented this line, Must change the image of only the first column
                //ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].CellButtonAppearance.Image = Properties.Resources.check_mark_2_16;
                e.Row.Cells["button"].ButtonAppearance.Image = Properties.Resources.check_mark_2_16;
                //====> Fin  Modif Mondir

                //e.Row.Cells["button"].ButtonAppearance.ImageBackground = Properties.Resources.check_mark_2_16;===> l'image n'apparait pas 
            }
            else if (e.Row.Cells["PRC_SelEnCours"].Text == "2")
            {
                for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(0, 0, 128);
                }

            }
            else
            {

                //TODO Mondir le 06.07.2020, Must reset the default color
                //for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                //{
                //    // e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(55, 84, 96);
                //    e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(0, 0, 0);
                //}

                //====> Mondir le 06.07.2020 : Commented this line, Must change the image of only the first column
                //ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].CellButtonAppearance.Image = Properties.Resources.square_outline_16;
                // e.Row.Cells["button"].ButtonAppearance.ImageBackground= Properties.Resources.square_outline_16;
                e.Row.Cells["button"].ButtonAppearance.Image = Properties.Resources.square_outline_16;
                //====> Fin  Modif Mondir
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsSElFourn.Update();
        }

        private void ssPRC_PreCmdCorps_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsSElFourn.Update();
        }

        private void ssPRC_PreCmdCorps_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }

        private void ssPRC_PreCmdCorps_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].Header.Caption = "Choix";
            // ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Sel"].Hidden = true;
            //  ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NoAuto"].Hidden = true;
            //  ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRE_NoAuto"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Chrono"].Header.Caption = "Chrono";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_References"].Header.Caption = "References";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Designation"].Header.Caption = "Designation";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Designation"].Width = 350;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Quantite"].Header.Caption = "Qte";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_PrixHT"].Header.Caption = "PrixHT";

            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Unite"].Header.Caption = "Unite";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NoLigne"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Fournisseur"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournDTCle"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].Header.Caption = "Fournisseur";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["NoAutoGecet"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fabref"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fourref"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NobonDeCommande"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_SelEnCours"].Hidden = true;

            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }


    }
}
