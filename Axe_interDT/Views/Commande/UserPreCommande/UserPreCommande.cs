﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Commande.UserPreCommande.Forms;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Fournisseur.HistoriqueDesBonsDeCommande;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Intervention;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Axe_interDT.Views.Commande.UserPreCommande
{
    public partial class UserPreCommande : UserControl
    {
        int nOk;
        double nNoEnt;
        double nInt;
        string[] tabCmd;
        string[] tabCmdFD;
        string[] tabTemp;
        string[] tabTemp1;
        string[] tabTemp2;
        string sUtilisateur;
        string[][] tabSetting;
        object[] tabTmp = new object[2];
        double dValueCom;
        bool bLoadBon;

        //Dim nNoEnt As Double
        object nStockMontant;
        object nStockPrice;
        double[] tabMontant;
        const string sValideRetour = "ValideRetour";
        const string sAjoutModif = "AjoutModif";
        //Empêche la boucle infinie lors du chargement
        bool blnSaveUpdate;
        // des données pendant l'évenement AfterUpdate
        // de la grille du corps (Grid0)
        bool blnSaveOnUpdate;
        //== =========== =============== =============== ============

        DataTable rsPRC;
        ModAdo modAdorsPRC = null;
        DataTable rsPRCHisto;
        ModAdo modAdorsPRCHisto = null;
        DataTable rstmp;
        ModAdo modAdorstmp = null;
        DataTable rsBCD_Detail;
        ModAdo modAdorsBCD_Detail = null;


        string sSQL;
        string sSelectFourn;

        object[] vBook;
        object obj;

        int i1;
        int j1;
        int nbCol;
        int i;

        bool boolEntree;
        bool bBonComplet;
        bool bBonLocked;
        bool bMove;
        bool bOnClick;
        bool bRechMat;
        bool bRechNom;

        double Xt;
        double x1;
        double Xref;
        double Xref2;
        double lTotWidth;
        double lTotBcdWidth;

        bool bAscii;

        //Mondir 25.06.2020 : Demande Via Mantis https://groupe-dt.mantishub.io/view.php?id=989 , Il faut afficher l'email de l'agence s'il existe, sinon on affiche l'email de fournisseur
        //- Pour faire cela, la variable CodeAg contient le code agence liée à ce fournisseur
        //- On récupére le code agence dans la fonction SelectBCmd et aussi dans CmdRerchercheAg_Click, des lignes sont ajoutées dans ces deux fonctions
        //- cmdMail_Click pour afficher la fentre d'envoie de mail
        private double CodeAg { get; set; } = 0;
        //Fin Modif Mondir 25.06.2020

        private struct ArtFourn
        {
            public int lSel;
            public int lSelEnCours;
            public string sChrono;
            public string sFour;
            public string sNoAuto;
            public DataRow vBookMark;
        }

        ArtFourn[] tArtFourn;

        Variable.ArtDupli[] tArtDupli;

        const string cChrono = "PRC_Chrono";
        const string cFourn = "PRC_Fournisseur";
        const string cSel = "PRC_Sel";
        const string CNoAuto = "PRC_NoAuto";

        const string cSansFournisseur = "Fourn non connu";
        const string cML = "MoveLeft";
        const string cMR = "MoveRight";
        const string cMLBcd = "MoveLeftBcd";
        const string cMRBcd = "MoveRightBcd";


        const short cLimG = 1860;
        const short climD = 10335;
        const string cMArtDouble = "Vous avez deja sélectionné cet article, Voulez vous l'ajouter de nouveau?";
        //gris
        const int cRowSelectColor = 0xffc0c0;
        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cNiv3 = "C";
        const string cLibelleNiv1 = "Tous";
        const string cLIbelleNiv1Vide = "Vide";
        const string cBCD = "BCD";
        const string cBCDlbl = "BCDlbl";
        const string clibButton = "Modifier le fournisseur/agence par bon";
        const short cNbColGrid = 9;

        private struct POINTAPI
        {
            public int X;
            public int Y;
        }

        POINTAPI Pt;

        const string cEtatEnCours = "CE";

        public UserPreCommande()
        {
            InitializeComponent();
        }


        private void fc_Deselectionne()
        {

            //Dim i                       As Long
            //Dim bkmrk
            //
            //
            //    With ssPRC_PreCmdCorps
            //
            //       bkmrk = .Bookmark
            //
            //       For i = 0 To UBound(tArtFourn)
            //
            //            If tArtFourn(i).lSelEnCours = 1 Then
            //                rsPRC.Bookmark = tArtFourn(i).vBookMark
            //                '== passe à 0 donc déselectionne.
            //                rsPRC!PRC_Sel = 0
            //
            //                tArtFourn(i).lSelEnCours = 0
            //                rsPRC.Update
            //           End If
            //
            //        Next
            //
            //        .Refresh
            //        '== se repositionne sur l'enregistrement d'origine.
            //        rsPRC.Bookmark = bkmrk
            //
            //
            //
            //
            //    End With


        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_EnabledMenu()
        {


            //
            //    If chkDupliquer.value = 1 Then
            //        chkDupliquer.ForeColor = vbRed
            //        smnuNewAgence.Enabled = False '
            //        smnuNewFourn.Enabled = False
            //        mnuDupliquer.Enabled = True
            //        fc_Deselectionne
            //
            //    ElseIf chkDupliquer.value = 0 Then
            //        chkDupliquer.ForeColor = vbBlack
            //        Screen.MousePointer = 1
            //        smnuNewAgence.Enabled = True '
            //        smnuNewFourn.Enabled = True
            //        mnuDupliquer.Enabled = False
            //
            //
            //    End If
            //
            //
            //
        }

        private void chkInterEnCours_CheckedChanged(object sender, EventArgs e)
        {
            if ((int)chkInterEnCours.CheckState == 1)
            {
                fc_BCD_Detail(1, " Where NoIntervention =" + txtNoIntervention.Text + "", true);

            }
            else
            {
                fc_BCD_Detail(1, " WHERE (BonDeCommande.NumFicheStandard =" + txtNumFicheStandard.Text + ") ", true);
            }
            fc_VisibleBCD(false);
        }

        private void fc_VisibleBCD(bool bVisible)
        {
            Program.SaveException(null, "function:fc_VisibleBCD();params==>bVisible=" + bVisible);
            CheckControlReq(this, bVisible);
            if (bVisible == false)//====>Tested
            {
                cmdSupprimer.Visible = false;
            }
            else
            {
                if (General.sAutoriseSuppBDC == "1")
                {
                    cmdSupprimer.Visible = true;
                }
                ///===============> Tested
                else
                {
                    cmdSupprimer.Visible = false;
                }
            }
            label26.Visible = false;
            txtEstimation.Visible = false;
        }

        private void CheckControlReq(Control C, bool bVisible)
        {
            foreach (Control CC in C.Controls)
            {
                if (CC.HasChildren)
                    CheckControlReq(CC, bVisible);
                var v = CC;
                if ((v.Tag != null && v.Tag.ToString().ToUpper() == cBCD.ToUpper()) || (v.Tag != null && v.Tag.ToString().ToUpper() == cBCDlbl.ToUpper()))
                {
                    v.Visible = bVisible;
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lGroupe"></param>
        /// <param name="sWhere"></param>
        /// <param name="bLoadTree"></param>
        private void fc_BCD_Detail(int lGroupe, string sWhere = null, bool bLoadTree = false)
        {
            try
            {
                int i = 0;
                modAdorsBCD_Detail = new ModAdo();
                Program.SaveException(null, "function:fc_BCD_Detail();params==>lGroupe=" + lGroupe + " | sWhere=" + sWhere + " | bLoadTree=" + bLoadTree);
                ///======================> Tested
                if (lGroupe == 1)
                {
                    bBonComplet = true;
                    sSQL = "SELECT BCD_No, BCD_Cle, BCD_References, BCD_Designation, BCD_Quantite,QTE_Libelle,"
                        + " BCD_PrixHT, BCD_TotHT,BCD_Unite, BCD_NoLigne, BCD_Fournisseur," +
                        " BCD_Chrono, " + " BCD_NoAutoGecet" + " FROM         BCD_Detail INNER JOIN"
                        + " BonDeCommande ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande " +
                        " Left Join QTE_Unite On BCD_Detail.BCD_Unite = QTE_Unite.QTE_No " + sWhere;
                    sSQL = sSQL + " order by BonDeCommande.NoIntervention DESC, BonDeCommande.NoBondeCommande DESC, BCD_NoLigne";
                    rsBCD_Detail = modAdorsBCD_Detail.fc_OpenRecordSet(sSQL);
                }
                else
                {
                    bBonComplet = false;
                    sSQL = "SELECT BCD_No, BCD_Cle, BCD_References, BCD_Designation, BCD_Quantite,'' as QTE_Libelle, "
                        + " BCD_PrixHT, BCD_TotHT, BCD_Unite, BCD_NoLigne, BCD_Fournisseur," +
                        " BCD_Chrono, " + " BCD_NoAutoGecet" + " From BCD_Detail " + sWhere + " order by BCD_NoLigne ";
                    rsBCD_Detail = modAdorsBCD_Detail.fc_OpenRecordSet(sSQL, ssBCD_Detail, "BCD_No");
                }



                ///=======================> Tested First
                if (lGroupe == 1 || bBonLocked == true)
                {
                    ssBCD_Detail.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
                    ssBCD_Detail.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
                    ssBCD_Detail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
                }
                else
                {
                    ssBCD_Detail.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                    ssBCD_Detail.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
                    ssBCD_Detail.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
                }

                if (bLoadTree == true)//======================>Tested
                {
                    fc_LoadtreeBDC(Convert.ToInt32(txtNumFicheStandard.Text));

                }

                ssBCD_Detail.DataSource = rsBCD_Detail;
                ssBCD_Detail.UpdateData();
                //==modif rachid si aucun enregistrement trouvé alors on debloque la grille
                if (rsBCD_Detail.Rows.Count == 0)
                {
                    for (i = 0; i <= ssBCD_Detail.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                    {
                        ssBCD_Detail.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
                    }
                }


                fc_TotEnLigne(rsBCD_Detail);//tested
                                            ////===================> Tested
                if (lGroupe == 1)
                {
                    //lblTotHT = FncArrondir(nz(fc_ADOlibelle("SELECT     SUM(ISNUMERIC( BCD_Quantite )* BCD_PrixHT) AS tot" _
                    //& " FROM BCD_Detail INNER JOIN" _
                    //& " BonDeCommande ON BCD_Detail.BCD_Cle = BonDeCommande.NoBonDeCommande " _
                    //& sWhere & " "), 0), 2)

                }
                else
                {
                    //lblTotHT = FncArrondir(nz(fc_ADOlibelle("SELECT  SUM( ISNUMERIC(BCD_Quantite) * BCD_PrixHT) AS tot" _
                    //& " FROM         BCD_Detail" & sWhere & " "), 0), 2)

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BCD_Detail");
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        private void fc_LoadtreeBDC(int lnumfichestandard)
        {

            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;
            string sSQL = null;
            int lNoBonDeCommande = 0;
            int lNointervention = 0;
            string sFourn = null;
            UltraTreeNode oNode = null;
            Program.SaveException(null, "function:fc_LoadtreeBDC();params==>lnumfichestandard=" + lnumfichestandard);
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //==reinitialise le treview.
                TreeView3.Nodes.Clear();


                sSQL = "SELECT Intervention.DateSaisie, Intervention.Intervenant, Intervention.Designation," + " BonDeCommande.NoBonDeCommande, BonDeCommande.Fournisseur,BonDeCommande.Verrou," +
                    " fournisseurArticle.Raisonsocial, BonDeCommande.NumFicheStandard, Intervention.NoIntervention" + " FROM (Intervention INNER JOIN BonDeCommande " +
                    " ON Intervention.NoIntervention = BonDeCommande.NoIntervention) " + " INNER JOIN fournisseurArticle ON BonDeCommande.CodeFourn = fournisseurArticle.Cleauto";
                ///===========================> Tested First
                if ((int)chkInterEnCours.CheckState == 1)
                {
                    sSQL = sSQL + " WHERE Intervention.Nointervention =" + txtNoIntervention.Text;
                }
                else
                {
                    sSQL = sSQL + " WHERE Intervention.NumFicheStandard =" + lnumfichestandard;
                }

                sSQL = sSQL + " order by Intervention.NoIntervention DESC, NoBondeCommande DESC";

                modAdorstmp = new ModAdo();
                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);
                //==================> Tested
                if (rstmp.Rows.Count == 0)
                {
                    TreeView3.Nodes.Add(cNiv1 + lnumfichestandard, cLIbelleNiv1Vide);
                    TreeView3.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    return;
                }

                var _with26 = rstmp;


                //==Ajout du 1er niveau de hiérarchie.

                UltraTreeNode TN_N1 = TreeView3.Nodes.Add(cNiv1 + lnumfichestandard, cLibelleNiv1);
                TN_N1.Override.NodeAppearance.Image = Properties.Resources.close_folder;

                //TODO : Mondir - Removing This Line Coz It Is Doing A Rcursive Call
                //TreeView3.SelectedNode = TN_N1;
                //TN_N1.Expanded();



                if (_with26.Rows.Count > 0)
                {
                    lNoBonDeCommande = 0;
                    lNointervention = 0;
                    foreach (DataRow Row in _with26.Rows)
                    {

                        //==Ajout du 2eme niveau de hiérarchie(intervention).
                        ///============================> Tested
                        UltraTreeNode TN_N2 = new UltraTreeNode();
                        if (lNointervention != Convert.ToInt32(General.nz(Row["Nointervention"], 0)))
                        {
                            TN_N2 = TN_N1.Nodes.Add(cNiv2 + Row["Nointervention"], Row["DateSaisie"] + "-" + Row["Intervenant"] + "-" + Row["Designation"]);
                            TN_N2.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                            lNointervention = Convert.ToInt32(General.nz(Row["Nointervention"], 0));
                        }

                        //==ajout du 3 eme niveau du treeview. (
                        //========= >Tested
                        if (lNoBonDeCommande != Convert.ToInt32(General.nz(Row["NoBonDeCommande"], 0)))
                        {
                            ///==========================> Tested First
                            if (!string.IsNullOrEmpty(General.nz(Row["RaisonSocial"], "").ToString()))
                            {
                                sFourn = Row["RaisonSocial"].ToString();
                            }
                            else
                            {
                                sFourn = General.nz(Row["Fournisseur"], "").ToString();
                            }

                            UltraTreeNode TmpTN = FindTreeNode(cNiv2 + Row["NoIntervention"], TreeView3);
                            UltraTreeNode TmpTNFils = new UltraTreeNode();
                            if (Row["Verrou"] == DBNull.Value || Convert.ToBoolean(Row["Verrou"]) == false)
                            {
                                TmpTNFils = TmpTN.Nodes.Add(cNiv3 + Row["NoBonDeCommande"], Row["NoBonDeCommande"] + "-" + sFourn);
                                TmpTNFils.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                            }
                            ///=====================> Tested
                            else
                            {
                                TmpTNFils = TmpTN.Nodes.Add(cNiv3 + Row["NoBonDeCommande"], Row["NoBonDeCommande"] + "-" + sFourn);
                                TmpTNFils.Override.NodeAppearance.Image = Properties.Resources.minus_4_16;
                            }

                            lNoBonDeCommande = Convert.ToInt32(General.nz(Row["NoBonDeCommande"], 0));
                        }

                    }

                }
                modAdorstmp.Close();

                //== si selection de l inter en cours, on affiche tout les bons.
                ////===========================> Tested
                if ((int)chkInterEnCours.CheckState == 1)
                {

                    foreach (UltraTreeNode oNode_loopVariable in TreeView3.Nodes)
                    {
                        oNode = oNode_loopVariable;
                        oNode.ExpandAll();

                        foreach (UltraTreeNode N2 in oNode.Nodes)
                        {
                            N2.ExpandAll();
                            foreach (UltraTreeNode N3 in N2.Nodes)
                                N3.ExpandAll();
                        }

                    }
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Loadtreeview");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="rs"></param>
        private void fc_TotEnLigne(DataTable rs)
        {
            double dtHT = 0;

            var _with12 = rs;
            dtHT = 0;
            foreach (DataRow Row in _with12.Rows)
            {
                dtHT = dtHT + Convert.ToDouble(General.nz(Row["BCD_Quantite"], 0)) * Convert.ToDouble(General.nz(Row["BCD_PrixHT"], 0));
            }
            lblTotHT.Text = Convert.ToString(dtHT);

        }

        private void CMB_Analytique_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT     ANAB_Code as Code" + " From ANAB_AnalytiqueBCD" + " WHERE     (ANAB_Visible = 1) " + " ORDER BY ANAB_Code";


            sheridan.InitialiseCombo(CMB_Analytique, sSQL, "Code");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_AfterCloseUp(object sender, EventArgs e)
        {
            fc_LibService();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LibService()
        {
            string sSQL = null;
            DataTable rs = null;
            ModAdo modAdors = null;

            try
            {
                ///======================> Tested
                if (string.IsNullOrEmpty(cmbService.Text))
                {
                    lblLibService.Text = "";
                    return;
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    lblLibService.Text = "";
                    return;
                }

                SAGE.fc_OpenConnSage();

                sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                if (rs.Rows.Count > 0)
                {
                    lblLibService.Text = rs.Rows[0]["CA_Intitule"] + "";
                }

                modAdors.Close();

                SAGE.fc_CloseConnSage();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LibService");
            }
        }

        private void cmbService_Validated(object sender, EventArgs e)
        {
            fc_LibService();
        }

        private bool fc_CtrlService()
        {
            bool functionReturnValue = false;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            //===  retourne true si tout est correct.

            functionReturnValue = true;

            try
            {
                if (General.sServiceAnalytique != "1")
                {
                    return functionReturnValue;
                }

                if (!General.IsDate(txtDateCommande.Text))
                {
                    return functionReturnValue;
                }

                //=== controle si le bon de commande a été crée avant la date contenue dans dtServiceAnalytique
                //===  si vrai on n'oblige pas la saisie du champs service.
                if (Convert.ToDateTime(txtDateCommande.Text) < General.dtServiceAnalytique)
                {
                    return functionReturnValue;
                }

                //Mondir 18.05.2020 : Added Condition on the active row of the cmbService to fix this https://groupe-dt.mantishub.io/view.php?id=1809
                //Mondir 25.06.2020 : Fixed, || to &&
                if (string.IsNullOrEmpty(cmbService.Text) && cmbService.ActiveRow == null)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code service est désormais obligatoire, veuillez sélectionner un code service dans la liste déroulante.", "Saisie incomplète.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    if (!string.IsNullOrEmpty(cmbService.Text))
                    {
                        //===  controle si le code analytique service est correct.
                        SAGE.fc_OpenConnSage();

                        sSQL = "SELECT CA_Intitule From F_COMPTEA WHERE CA_Num = '" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "'";

                        modAdors = new ModAdo();

                        rs = modAdors.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);

                        if (rs.Rows.Count == 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code service " + cmbService.Text + " n'existe pas, veuillez en sélectionner un autre code service dans la liste déroulante.");

                            functionReturnValue = false;
                        }

                        modAdors.Close();

                        SAGE.fc_CloseConnSage();
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlService");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbService_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            try
            {
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {
                    return;
                }

                sSQL = "SELECT     CA_Num as Code, CA_Intitule AS Intitulé " + " From F_COMPTEA" + " ";

                sSQL = sSQL + ModAnalytique.fc_WhereServiceAnalytique(" Where N_Analytique = 6");

                sSQL = sSQL + " ORDER BY CA_Num";

                SAGE.fc_OpenConnSage();

                sheridan.InitialiseCombo(cmbService, sSQL, "Code", false, SAGE.adoSage);

                SAGE.fc_CloseConnSage();

                cmbService.DisplayLayout.Bands[0].Columns[1].Width = 300;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbService_DropDown");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAddFourn_Click(object sender, EventArgs e)
        {
            string sFourn = null;
            object oNode = null;
            UltraTreeNode NodeSelected = null;

            int lPRC_FournDTCle = 0;
            try
            {

                if (string.IsNullOrEmpty(txtPRE_NoAuto.Text))
                {
                    MessageBox
                        .Show("Votre precommande n'est associée a aucune intervention.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //========================================>Tested
                string requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\", Ville as \"Ville\"" + ", cleAuto as \"Numéro interne\"" + " FROM fournisseurArticle";
                string where_order = " (code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 ) ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un fournisseur à votre bon de commande" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sFourn = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                    lPRC_FournDTCle = Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["Numéro interne"].Value);
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sFourn = fg.ugResultat.ActiveRow.Cells["Code"].Value.ToString();
                        lPRC_FournDTCle = Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["Numéro interne"].Value);
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();



                //== ajoute un enregistrement avec le fournisseur selectionné.=====================>//tested
                var rsPRCNewRow = rsPRC.NewRow();
                rsPRCNewRow["PRE_NoAuto"] = Convert.ToInt32(txtPRE_NoAuto.Text);
                rsPRCNewRow["PRC_FournisseurRef"] = sFourn;
                rsPRCNewRow["PRC_FournisseurDT"] = sFourn;
                rsPRCNewRow["PRC_FournDTCle"] = lPRC_FournDTCle;
                rsPRCNewRow["prc_Sel"] = 1;
                rsPRC.Rows.Add(rsPRCNewRow);
                ssPRC_PreCmdCorps.UpdateData();
                modAdorsPRC.Update();

                //== rafraichis le treevieux et la grille
                fc_PRC_PreCmdCorps(txtPRE_NoAuto.Text);//Tested

                //== Positionne le treeview sur le Fournisseur selectionné ci-dessus.==================>Tested
                foreach (UltraTreeNode oNode_loopVariable in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode TN in oNode_loopVariable.Nodes)
                        if ((General.Left(TN.Key, 1)).ToUpper() == cNiv2.ToUpper())
                        {
                            // Debug.Print oNode.Text
                            if (sFourn != null && TN.Text.ToUpper() == sFourn.ToUpper())
                            {
                                TN.Selected = true;
                                NodeSelected = TN;
                                break;
                            }
                        }
                }

                //== Positionne ur le bon fournisseur.
                //TODO : Mondir - Uncomment Last The Next Line
                // TreeView1_AfterSelect(TreeView1, new TreeViewEventArgs(NodeSelected));
                TreeView1.ActiveNode = NodeSelected;
                TreeView1.Enabled = false;
                Timer2.Enabled = true;
                // fc_PopulateTreeview cNiv1 & .Columns("PRE_NoAuto").Text, cLibelleNiv1, _
                //cNiv2 & sFourn, sFourn, _
                //cNiv3 & .Columns("PRC_Noauto").Text, .Columns("PRC_Designation").Text, True
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAddFourn_click()");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        /// <param name="sWhere"></param>
        private void fc_PRC_PreCmdCorps(string sNoAuto, string sWhere = null)
        {
            bool bMissing = false;
            try
            {
                Program.SaveException(null, "function:fc_PRC_PreCmdCorps();params==>sNoAuto=" + sNoAuto);
                sSQL = "SELECT '' as button, PRC_Sel, PRC_NoAuto, PRE_NoAuto,PRC_Chrono, PRC_References," + " PRC_Designation,"
                    + " PRC_Quantite, PRC_PrixHT, PRC_TotHT , PRC_Unite, PRC_NoLigne, PRC_Fournisseur,PRC_FournDTCle" + " ,PRC_FournisseurDT,PRC_FournisseurRef, "
                    + "  NoAutoGecet, PRC_fabref, PRC_fourref, PRC_NobonDeCommande, PRC_SelEnCours, FNS_No " + " FROM PRC_PreCmdCorps";

                if ((sWhere == null))
                {
                    sWhere = " WHERE PRE_NoAuto='" + sNoAuto + "' and (PRC_Histo = 0 OR PRC_Histo IS NULL) ";
                    sSQL = sSQL + sWhere;
                    bMissing = true;
                }
                else
                {
                    sSQL = sSQL + sWhere;
                }

                modAdorsPRC = new ModAdo();
                rsPRC = modAdorsPRC.fc_OpenRecordSet(sSQL, ssPRC_PreCmdCorps, "PRC_NoAuto");
                if (bMissing == true)
                {
                    fc_Loadtreeview(sNoAuto);//tested
                    fc_LoadArt(rsPRC);
                }
                //ssPRC_PreCmdCorps.DataSource = null;
                ssPRC_PreCmdCorps.DataSource = rsPRC;
                ssPRC_PreCmdCorps.UpdateData();
                using (var tmpModAdo = new ModAdo())
                    lblPreTotHT.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(tmpModAdo.fc_ADOlibelle("SELECT     SUM(PRC_Quantite * PRC_PrixHT) AS tot" + " FROM         PRC_PreCmdCorps " + sWhere), 0)), 2));


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_PRC_PReBCmdCorps()");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        private void fc_Loadtreeview(string sNoAuto)
        {
            string sFourn = null;
            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;
            string sSQL = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //==reinitialise le treview.
                TreeView1.Nodes.Clear();

                sSQL = "SELECT PRC_Sel, PRC_NoAuto,  PRC_Designation," + " PRC_NoLigne,  PRE_NoAuto, PRC_FournisseurRef"
                    + " FROM PRC_PreCmdCorps" + " WHERE PRE_NoAuto='" + sNoAuto + "' and   PRC_Sel = 1 and (PRC_Histo = 0 OR PRC_Histo IS NULL) "
                    + " Order by PRC_FournisseurRef";
                modAdorstmp = new ModAdo();
                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count == 0)//========>   Tested
                {
                    TreeView1.Nodes.Add(cNiv1 + sNoAuto, cLIbelleNiv1Vide);
                    TreeView1.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                    return;
                }

                var _with28 = rstmp;

                UltraTreeNode TN_N2 = new UltraTreeNode();
                UltraTreeNode TN_N3 = new UltraTreeNode();

                //==>Tested
                //==Ajout du 1er niveau de hiérarchie.
                var TN_N1 = TreeView1.Nodes.Add(cNiv1 + _with28.Rows[0]["PRE_NoAuto"].ToString(), cLibelleNiv1);
                TN_N1.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                TN_N1.Selected = true;
                TreeView1.Nodes[cNiv1 + _with28.Rows[0]["PRE_NoAuto"].ToString()].ExpandAll();
                if (_with28.Rows.Count > 0)
                {
                    sFourn = "";
                    foreach (DataRow Row in _with28.Rows)
                    {
                        if (!string.IsNullOrEmpty(General.nz(Row["PRC_FournisseurRef"], "").ToString()))
                        {
                            //==Ajout du 2eme niveau de hiérarchie(fournisseur).

                            if (sFourn.ToUpper() != General.nz(Row["PRC_FournisseurRef"], "").ToString().ToUpper())
                            {
                                //TreeView1.Nodes.Add , , cNiv2 & !PRC_Fournisseur, !PRC_Fournisseur, 1
                                TN_N2 = TN_N1.Nodes.Add(cNiv2 + Row["PRC_FournisseurRef"].ToString().ToUpper(), General.nz(Row["PRC_FournisseurRef"], "").ToString());
                                TN_N2.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                                sFourn = General.nz(Row["PRC_FournisseurRef"], "").ToString();
                            }
                            // 
                            //==Ajout du 3eme niveau de hiérarchie(designation de l'article).
                            if (Row["PRC_Designation"] != DBNull.Value)
                            {
                                TN_N3 = TN_N2.Nodes.Add(cNiv3 + Row["PRC_NoAuto"], General.nz(Row["PRC_Designation"].ToString(), "").ToString());
                                TN_N3.Override.NodeAppearance.Image = Properties.Resources.check_mark_2_16;
                            }
                        }
                    }
                }
                modAdorstmp.Close();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Loadtreeview");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rs"></param>
        private void fc_LoadArt(DataTable rs)
        {
            int i = 0;

            tArtFourn = null;

            try
            {
                //===============> Tested
                if (rs.Rows.Count == 0)
                    return;

                foreach (DataRow Row in rs.Rows)
                {
                    ///Mohammed 29.06.2019 =======>  changed by mohammed to solve this exception "Le format de la chaîne d'entrée est incorrect" because "PRC_Chrono" is string(varchar) we can't convert it to int . 
                    if (General.nz(Row[cChrono], 0) + "" != "0" && General.nz(Row[cChrono], "") + "" != "")
                    {
                        Array.Resize(ref tArtFourn, i + 1);
                        tArtFourn[i].lSel = Convert.ToInt32(General.nz(Row[cSel], 0));
                        tArtFourn[i].sChrono = Row[cChrono].ToString();
                        tArtFourn[i].sFour = General.nz(Row[cFourn], "").ToString();
                        tArtFourn[i].sNoAuto = Row[CNoAuto].ToString();
                        tArtFourn[i].vBookMark = Row;
                        //If nz(rs.rows[0][cSel).value, 0) = 2 Then
                        //    tArtFourn(i).lSelEnCours = 1
                        //Else
                        tArtFourn[i].lSelEnCours = Convert.ToInt32(General.nz(Row[cSel], 0));
                        // End If
                    }
                    i = i + 1;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
            //== recordset vide, quitte la procedure.
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treev"></param>
        private void fc_BoldNode(UltraTree treev)
        {
            UltraTreeNode oNode = null;

            foreach (UltraTreeNode oNode_loopVariable in treev.Nodes)
            {
                /* if (oNode_loopVariable.NodeFont == null)
                     oNode_loopVariable.NodeFont = new Font("Ubuntu", 9);*/

                if (oNode_loopVariable.Selected == true)
                {
                    oNode_loopVariable.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                }
                else
                {
                    oNode_loopVariable.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.False;
                }
                foreach (UltraTreeNode TN_N2 in oNode_loopVariable.Nodes)
                {
                    /*if (TN_N2.NodeFont == null)
                        TN_N2.NodeFont = new Font("Ubuntu", 9);*/

                    if (TN_N2.Selected == true)
                    {
                        TN_N2.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                    }
                    else
                    {
                        TN_N2.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.False;
                    }

                    foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                    {
                        /*if (TN_N3.NodeFont == null)
                            TN_N3.NodeFont = new Font("Ubuntu", 9);*/

                        if (TN_N3.Selected == true)
                        {
                            TN_N3.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                        }
                        else
                        {
                            TN_N3.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.False;
                        }
                    }
                }
            }
        }

        private void cmdAddFournAgences_Click(object sender, EventArgs e)
        {
            try
            {
                if (General.nz(txtPRE_NoAuto.Text, 0).ToString() != "0")
                {
                    frmMajFournAg frmMajFournAg = new frmMajFournAg();
                    frmMajFournAg.txtPRE_NoAuto.Text = txtPRE_NoAuto.Text;
                    frmMajFournAg.ShowDialog();
                    frmMajFournAg.Close();
                    fc_PRC_PreCmdCorps(txtPRE_NoAuto.Text);
                    //, " WHERE PRE_NoAuto=" & txtPRE_NoAuto |                    & " and (PRC_Histo = 0 OR PRC_Histo IS NULL) "
                }
                else
                {

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAddFournAgences");
            }
        }

        private void cmdCreateBCD_Click(object sender, EventArgs e)
        {
            int lOneNoBonDeCommande = 0;
            UltraTreeNode oNode = null;
            string sSQLDev = null;
            lOneNoBonDeCommande = 0;
            try
            {
                //=== modif du 13 02 2017, interdit la cré&tionde devis si
                //=== celui ci est cloturé.
                if (General.sBloqueAchatSurDevis == "1")
                {
                    if (!string.IsNullOrEmpty(txtNumeroDevis.Text))
                    {
                        sSQLDev = "SELECT     ClotureDevis From DevisEnTete WHERE  NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(txtNumeroDevis.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQLDev = tmpModAdo.fc_ADOlibelle(sSQLDev);
                        if (sSQLDev == "1")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le devis " + txtNumeroDevis.Text + " a été clôturé, vous ne pouvez pas créer de bon de commande." + "\n" + "Seul un responsable peut vous autoriser à créer des bons de commande.",
                                 "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }

                //=== modif du 15 08 2019
                if (txtNumeroDevis.Text != "")
                {
                    if (General.fc_CtrResponsableTravaux(txtNumeroDevis.Text, 0) == false)
                        return;
                }


                if (fc_CreateBcmd(ref lOneNoBonDeCommande) == false)
                    return;
                fc_ChargeEnregistrement(txtNoIntervention.Text, txtNumFicheStandard.Text);
                fc_LoadtreeviewHisto(txtPRE_NoAuto.Text);
                fc_VisibleBCD(false);
                SSTab3.SelectedTab = SSTab3.Tabs[1];
                chkInterEnCours.CheckState = System.Windows.Forms.CheckState.Checked;

                fc_BCD_Detail(1, " Where NoIntervention =" + txtNoIntervention.Text + "", true);

                //== si lOneNoBonDeCommande <> 0, un seul bon sera crée, on positionne l'utilisateur
                //== sur celui-ci.
                if (lOneNoBonDeCommande == 0)
                {
                    Program.SaveException(null, "function:cmdCreateBCD_Click() in the middle of this function line 1083 inside if(lOneBonDeCommande == 0) we log this line;params==>lOneNoBonDeCommande=" + lOneNoBonDeCommande);
                    fc_VisibleBCD(false);
                }
                else
                {
                    //== positionne le treeview sur le bon de commande.
                    foreach (UltraTreeNode oNode_loopVariable in TreeView3.Nodes)
                    {
                        oNode = oNode_loopVariable;
                        foreach (UltraTreeNode TN_N2 in oNode.Nodes)
                            foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                //== recherche sur le niveau de bon de commande.
                                if (General.Left(TN_N3.Key, 1).ToUpper() == cNiv3.ToUpper())
                                {
                                    if (Convert.ToDouble(General.Right(TN_N3.Key, TN_N3.Key.Length - 1)) == lOneNoBonDeCommande)
                                    {
                                        TreeView3.ActiveNode = TN_N3;
                                        TN_N3.Selected = true;
                                        TN_N3.Expanded = true;
                                        TN_N3.Override.NodeAppearance.FontData.Bold = Infragistics.Win.DefaultableBoolean.True;
                                        // TreeView3_AfterSelect(TreeView3, new TreeViewEventArgs(TN_N3));
                                        TreeView3.ActiveNode = TN_N3;
                                        // TN_N3.EnsureVisible();
                                        return;
                                    }
                                }

                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdCreateBCD");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lOneNoBonDeCommande"></param>
        /// <returns></returns>
        private bool fc_CreateBcmd(ref int lOneNoBonDeCommande)
        {
            bool functionReturnValue = false;
            int i = 0;
            int j = 0;
            int lNoBonDeCommande = 0;
            DataTable rsBDC = default(DataTable);
            ModAdo modAdorsBDC = null;
            DataTable rsBCD_Detail = default(DataTable);
            ModAdo modAdorsBCD_Detail = null;
            DataTable rsPRCbis = default(DataTable);
            ModAdo modAdorsPRCbis = null;
            string sFourn = null;
            int X = 0;
            bool bNull = false;
            DataRow DR = null;
            int NbBon = 0;
            string sTexte = null;
            Program.SaveException(null, "function:fc_CreateBcmd();params==>lOneNoBonDeCommande=" + lOneNoBonDeCommande);
            lOneNoBonDeCommande = 0;
            try
            {
                functionReturnValue = true;

                //== Récupére dans le corps de la precommande tous les articles sélectionnés(PRC_Sel = 1)
                fc_PRC_PreCmdCorps("", " WHERE PRE_NoAuto='" + txtPRE_NoAuto.Text + "' and (PRC_Histo = 0 OR PRC_Histo IS NULL) ");

                modAdorsPRCbis = new ModAdo();
                rsPRCbis = modAdorsPRCbis.fc_OpenRecordSet(modAdorsPRC.SDArsAdo.SelectCommand.CommandText + " order by PRC_FournisseurDT");

                //== controle si tous les articles ont bien un fournisseur et compte===============================>Tested
                //== le nombre de bon de commande.
                var rsPRCbisFilter = rsPRCbis.Select("PRC_SEL = 1");

                sFourn = "";
                NbBon = 0;
                foreach (DataRow rsPRCbisFilterRow in rsPRCbisFilter)
                {
                    if (string.IsNullOrEmpty(General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString()))
                    {
                        bNull = true;
                        DR = rsPRCbisFilterRow;
                        break;
                    }
                    if (sFourn != General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString())
                    {
                        NbBon = NbBon + 1;
                        sFourn = General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString();
                    }
                }
                if (bNull == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("l'article " + DR["PRC_Designation"] + " n'est pas rattaché à un fournisseur " + " existant dans la base " + General.NomSociete + "." + "Cliquez sur le bouton '" + clibButton + "' pour mettre à jour le fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    modAdorsPRCbis.Close();
                    rsPRCbis = null;
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                if (NbBon == 1)
                {
                    sTexte = "Vous allez créer " + NbBon + " bon de commande, Voulez vous continuer?";
                }
                else
                {
                    sTexte = "Vous allez créer " + NbBon + " bons de commandes, Voulez vous continuer?";
                }
                var dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dg == DialogResult.No)
                {
                    functionReturnValue = false;
                    return functionReturnValue;
                }


                //==Si Il existe au moins un enregistrement on crée au moins un bon de commande.
                if (rsPRCbisFilter.Length <= 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune precommande n'a été trouvé.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                //== Entete du bon de commande.
                sSQL = "SELECT Acheteur, CodeImmeuble, Commentaire1, DateCommande, Fournisseur, NoIntervention" + " ,NumeroDevis, NumFicheStandard, CodeFourn, CodeE, Verrou, Ordre, EtatLivraison " + " ,EtatFacturation, Technicien, Estimation, Priorite, NoBonDeCommande,CodeAG " + " FROM BonDeCommande " + " WHERE NoBondeCommande = 0";

                modAdorsBDC = new ModAdo();
                rsBDC = modAdorsBDC.fc_OpenRecordSet(sSQL);

                //== Corps du bon de commande.
                sSQL = "SELECT BCD_Cle, BCD_References, BCD_Designation, BCD_Quantite" + " ,BCD_PrixHT, BCD_TotHT,BCD_Unite, BCD_NoLigne, BCD_Fournisseur, BCD_Chrono, BCD_NoAutoGecet, ReferenceGecet, PRC_NoAuto " + " FROM BCD_DETAIL  " + " WHERE BCD_No = 0";

                modAdorsBCD_Detail = new ModAdo();
                rsBCD_Detail = modAdorsBCD_Detail.fc_OpenRecordSet(sSQL);

                //== creation des bons de commandes.
                var _with6 = rsPRCbis;
                //== classe le jeu d'enregistrement par fournisseur.
                rsPRCbisFilter = rsPRCbis.Select("PRC_SEL = 1", "PRC_FournisseurDT");

                sFourn = "";
                X = 1;
                foreach (DataRow rsPRCbisFilterRow in rsPRCbisFilter)
                {
                    //== Pour chaque fournisseur, on crée un nouveau bon de commande.
                    if (sFourn.ToUpper() != General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString().ToUpper())
                    {
                        //== creation de l'entéte du bon de commande.
                        sFourn = rsPRCbisFilterRow["PRC_FournisseurDT"].ToString();
                        var rsBDCNewRow = rsBDC.NewRow();
                        //rsBDC!Acheteur = fc_ADOlibelle("SELECT Personnel.Initiales FROM Personnel " _
                        //& " INNER JOIN DevisEntete ON Personnel.Matricule=DevisEntete.CodeDeviseur" _
                        //& " WHERE DevisEntete.NumeroDevis='" & txtNumeroDevis & "'") & "'"
                        rsBDCNewRow["Acheteur"] = General.fncUserName();
                        rsBDCNewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                        if (!string.IsNullOrEmpty(txtNumeroDevis.Text))
                        {
                            using (var tmpModAdo = new ModAdo())
                                rsBDCNewRow["Commentaire1"] = "Suite Devis N°: " + txtNumeroDevis.Text + " :" + "\n" + StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle("SELECT TitreDevis FROM DevisEntete " +
                                    " WHERE NumeroDevis='" + txtNumeroDevis.Text + "'"));
                        }

                        rsBDCNewRow["DateCommande"] = DateTime.Now;
                        rsBDCNewRow["Fournisseur"] = rsPRCbisFilterRow["PRC_FournisseurDT"];

                        //rsBDC!NoIntervention = fc_ADOlibelle("SELECT TOP 1 Intervention.NoIntervention FROM Intervention" _
                        //& " WHERE NumFicheStandard=" & txtNumFicheStandard _
                        //& " ORDER BY NoIntervention")
                        rsBDCNewRow["Nointervention"] = txtNoIntervention.Text;
                        rsBDCNewRow["numerodevis"] = txtNumeroDevis.Text;
                        rsBDCNewRow["Numfichestandard"] = txtNumFicheStandard.Text;
                        rsBDCNewRow["CodeFourn"] = rsPRCbisFilterRow["PRC_FournDTCle"];
                        //nz(fc_ADOlibelle("SELECT CleAuto FROM FournisseurArticle " |                                        '& " WHERE Nom LIKE '%" & nz(!PRC_Fournisseur, "") & "%'"), "0")
                        rsBDCNewRow["CodeFourn"] = rsPRCbisFilterRow["PRC_FournDTCle"];
                        rsBDCNewRow["codeag"] = rsPRCbisFilterRow["FNS_No"];

                        rsBDCNewRow["CodeE"] = "CA";
                        rsBDCNewRow["Verrou"] = 0;
                        rsBDCNewRow["ordre"] = 1;
                        rsBDCNewRow["EtatLivraison"] = "NL";
                        rsBDCNewRow["EtatFacturation"] = "NF";
                        rsBDCNewRow["Technicien"] = "000";
                        rsBDCNewRow["Estimation"] = "0";
                        rsBDCNewRow["Priorite"] = "0";
                        rsBDC.Rows.Add(rsBDCNewRow);
                        var xx = modAdorsBDC.Update();

                        using (var tmpModAdo = new ModAdo())
                            lNoBonDeCommande = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoBonDeCommande) FROM BonDeCommande"));
                        //== si un bon de commande on retourne en parametre le numero de celui-ci
                        if (NbBon == 1)
                        {
                            lOneNoBonDeCommande = lNoBonDeCommande;
                        }
                    }
                    //== met à jour le numéro de bons de commande dans le corps de la precommande.
                    rsPRCbisFilterRow["prc_NoBonDeCommande"] = lNoBonDeCommande;

                    //== detail du bon de commande.
                    var rsBCD_DetailNewRow = rsBCD_Detail.NewRow();
                    rsBCD_DetailNewRow["BCD_Cle"] = lNoBonDeCommande;

                    rsBCD_DetailNewRow["BCD_References"] = rsPRCbisFilterRow["PRC_References"];
                    rsBCD_DetailNewRow["BCD_Designation"] = rsPRCbisFilterRow["PRC_Designation"];
                    rsBCD_DetailNewRow["BCD_Quantite"] = rsPRCbisFilterRow["PRC_Quantite"];
                    rsBCD_DetailNewRow["BCD_PrixHT"] = rsPRCbisFilterRow["PRC_PrixHT"];
                    rsBCD_DetailNewRow["BCD_TotHT"] = rsPRCbisFilterRow["PRC_TotHT"];
                    rsBCD_DetailNewRow["BCD_Unite"] = rsPRCbisFilterRow["PRC_Unite"];
                    rsBCD_DetailNewRow["BCD_NoLigne"] = X;
                    rsBCD_DetailNewRow["PRC_NoAuto"] = rsPRCbisFilterRow["PRC_NoAuto"];
                    rsBCD_DetailNewRow["ReferenceGecet"] = rsPRCbisFilterRow["PRC_References"];
                    X = X + 1;
                    rsBCD_DetailNewRow["BCD_Fournisseur"] = rsPRCbisFilterRow["PRC_Fournisseur"];
                    rsBCD_DetailNewRow["BCD_Chrono"] = rsPRCbisFilterRow["PRC_Chrono"];
                    rsBCD_DetailNewRow["BCD_NoAutoGecet"] = rsPRCbisFilterRow["NoAutoGecet"];
                    rsBCD_DetailNewRow["ReferenceGecet"] = rsPRCbisFilterRow["PRC_References"];
                    rsBCD_Detail.Rows.Add(rsBCD_DetailNewRow);
                    var xxx = modAdorsBCD_Detail.Update();
                    if (rsPRCbisFilterRow["PRC_Chrono"].ToString() == General.cArticleDefaut
                        || General.UCase(General.Left(General.nz(rsPRCbisFilterRow["PRC_Chrono"], 0).ToString(), General.cArtiNonQualifie.Length)) == General.UCase(General.cArtiNonQualifie))
                    {

                        sSQL = "UPDATE PRC_PreCmdCorps set  PRC_Histo = 1 WHERE PRC_NoAuto = '" + rsPRCbisFilterRow["PRC_NoAuto"] + "'";
                    }
                    else
                    {
                        sSQL = "UPDATE PRC_PreCmdCorps set  PRC_Histo = 1 " + " WHERE PRE_Noauto ='" + rsPRCbisFilterRow["PRE_NoAuto"] + "'  and PRC_Chrono =" +
                            General.nz(rsPRCbisFilterRow["PRC_Chrono"], General.cArticleDefaut);
                    }
                    xxx = General.Execute(sSQL);
                }


                modAdorsPRCbis.Close();
                modAdorsBCD_Detail.Close();
                modAdorsBDC.Close();
                Program.SaveException(null, "function:fc_CreateBcmd();params==>lOneNoBonDeCommande=" + lOneNoBonDeCommande + " ||| this paramas lOneNoBonDeCommande passed by reference to this function lOneNoBonDeCommande=" + lOneNoBonDeCommande);
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CreateBCmd");
                return functionReturnValue = false;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNewVar"></param>
        /// <param name="sNumFicheStandard"></param>
        private void fc_ChargeEnregistrement(string sNewVar, string sNumFicheStandard)
        {
            try
            {
                Program.SaveException(null, "function:fc_ChargeEnregistrement();params==>sNewVar=" + sNewVar + "  | sNumFicheStandard=" + sNumFicheStandard);
                //==Numéro de fiche d'appel = sNewVar
                sSQL = "SELECT PRE_CreePar, PRE_CreeLe, PRE_ValiderPar, PRE_ValiderLe," + "Codeimmeuble, NoIntervention, NumeroDevis, NumFicheStandard, PRE_NoAuto" + " From PRE_PreCmdEnTete" +
                    " WHERE     (NumFicheStandard =" + sNumFicheStandard + ")" + "";

                modAdorstmp = new ModAdo();
                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);

                var _with23 = rstmp;
                if (_with23.Rows.Count > 0)
                {
                    txtNumeroDevis.Text = General.nz(_with23.Rows[0]["numerodevis"], "").ToString();
                    txtCodeImmeuble.Text = General.nz(_with23.Rows[0]["CodeImmeuble"], "").ToString();
                    txtCodeImmeuble2.Text = General.nz(_with23.Rows[0]["CodeImmeuble"], "").ToString();
                    txtNumFicheStandard.Text = General.nz(_with23.Rows[0]["Numfichestandard"], "").ToString();
                    //txtNoIntervention = nz(!NoIntervention, "")
                    txtNoIntervention.Text = sNewVar;
                    txtPRE_NoAuto.Text = General.nz(_with23.Rows[0]["PRE_NoAuto"], 0).ToString();
                    lblCreeLe.Text = General.nz(_with23.Rows[0]["PRE_CreeLe"], "").ToString();
                    lblCreePar.Text = General.nz(_with23.Rows[0]["PRE_CreePar"], "").ToString();
                    lblValiderLe.Text = General.nz(_with23.Rows[0]["PRE_ValiderLe"], "").ToString();
                    lblValiderPar.Text = General.nz(_with23.Rows[0]["PRE_ValiderPar"], "").ToString();
                    fc_PRC_PreCmdCorps(General.nz(_with23.Rows[0]["PRE_NoAuto"], 0).ToString());
                    modAdorstmp.Close();
                    return;
                }


                //== si aucun enregistrement trouvée, on l ajoute.
                sSQL = "SELECT NumFicheStandard, NoIntervention, CodeImmeuble, NoDevis" + " From Intervention" + " WHERE NoIntervention = " + sNewVar;

                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);
                var _with24 = rstmp;

                ///====================> Tested
                if (_with24.Rows.Count > 0)
                {
                    txtNumeroDevis.Text = General.nz(_with24.Rows[0]["NoDevis"], "").ToString();
                    if (string.IsNullOrEmpty(txtNumeroDevis.Text))
                    {
                        txtNumeroDevis.Enabled = false;

                    }
                    txtCodeImmeuble.Text = General.nz(_with24.Rows[0]["CodeImmeuble"], "").ToString();
                    txtCodeImmeuble2.Text = General.nz(_with24.Rows[0]["CodeImmeuble"], "").ToString();
                    txtNumFicheStandard.Text = General.nz(_with24.Rows[0]["Numfichestandard"], "").ToString();
                    txtNoIntervention.Text = General.nz(_with24.Rows[0]["Nointervention"], "").ToString();
                    lblCreeLe.Text = Convert.ToString(DateTime.Today);
                    lblCreePar.Text = General.fncUserName();
                    lblValiderPar.Text = General.fncUserName();
                    lblValiderLe.Text = Convert.ToString(DateTime.Today);

                }
                else
                {
                    modAdorstmp.Close();
                    return;
                }


                sSQL = "SELECT PRE_CreePar, PRE_CreeLe, PRE_ValiderPar, PRE_ValiderLe," + "Codeimmeuble, NoIntervention, NumeroDevis, NumFicheStandard, PRE_NoAuto" + " From PRE_PreCmdEnTete" + " WHERE     (PRE_NoAuto = 0)";

                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);

                var _with25 = rstmp;
                var rstmpNewRow = rstmp.NewRow();
                rstmpNewRow["PRE_CreePar"] = lblCreePar.Text;
                rstmpNewRow["PRE_CreeLe"] = lblCreeLe.Text;
                rstmpNewRow["PRE_ValiderPar"] = lblValiderPar.Text;
                rstmpNewRow["PRE_ValiderLe"] = lblValiderLe.Text;
                rstmpNewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                rstmpNewRow["Nointervention"] = txtNoIntervention.Text;
                rstmpNewRow["numerodevis"] = General.nz(txtNumeroDevis, System.DBNull.Value);
                rstmpNewRow["Numfichestandard"] = txtNumFicheStandard.Text;
                rstmp.Rows.Add(rstmpNewRow);
                int x = modAdorstmp.Update();
                using (var tmpModAdo = new ModAdo())
                    txtPRE_NoAuto.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(PRE_NoAuto) FROM PRE_PreCmdEnTete");
                modAdorstmp.Close();
                fc_PRC_PreCmdCorps(General.nz(txtPRE_NoAuto.Text, 0).ToString());
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_chargeEnregistrement");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        private void fc_LoadtreeviewHisto(string sNoAuto)
        {
            string sFourn = null;
            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;
            string sSQL = null;
            UltraTreeNode TN_N2 = new UltraTreeNode();
            UltraTreeNode TN_N3 = new UltraTreeNode();
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                Program.SaveException(null, "function:fc_LoadtreeviewHisto();params==>sNoAuto=" + sNoAuto);
                //==reinitialise le treview.
                TreeView2.Nodes.Clear();

                sSQL = "SELECT PRC_Sel, PRC_NoAuto,  PRC_Designation," + " PRC_NoLigne, PRC_FournisseurRef, PRE_NoAuto" + " FROM PRC_PreCmdCorps" + " WHERE PRE_NoAuto='" + sNoAuto + "'  and PRC_Histo = 1 " + " Order by PRC_FournisseurRef";

                modAdorstmp = new ModAdo();
                rstmp = modAdorstmp.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count == 0)//tested
                    return;

                var _with29 = rstmp;

                var TN_N1 = TreeView2.Nodes.Add(cNiv1 + _with29.Rows[0]["PRE_NoAuto"], cLibelleNiv1);
                TN_N1.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                TN_N1.Selected = true;
                TN_N1.ExpandAll();

                if (_with29.Rows.Count > 0)
                {
                    sFourn = "";
                    foreach (DataRow _with29Row in _with29.Rows)
                    {
                        if (Convert.ToInt32(General.nz(_with29Row["prc_Sel"], 0)) == 1)
                        {
                            //==Ajout du 1er niveau de hiérarchie.

                            if (sFourn.ToUpper() != General.nz(_with29Row["PRC_FournisseurRef"], "").ToString().ToUpper())
                            {
                                //TREEVIEw2.Nodes.Add , , cNiv2 & !PRC_Fournisseur, !PRC_Fournisseur, 1
                                TN_N2 = TN_N1.Nodes.Add(cNiv2 + _with29Row["PRC_FournisseurRef"].ToString().ToUpper(), General.nz(_with29Row["PRC_FournisseurRef"], "").ToString());
                                TN_N2.Override.NodeAppearance.Image = Properties.Resources.minus_4_16;
                                sFourn = General.nz(_with29Row["PRC_FournisseurRef"].ToString(), "").ToString();
                            }

                            //==Ajout du 2eme niveau de hiérarchie.

                            if (_with29Row["PRC_Designation"] != DBNull.Value)
                            {
                                TN_N3 = TN_N2.Nodes.Add(cNiv3 + _with29Row["PRC_NoAuto"], General.nz(_with29Row["PRC_Designation"], "").ToString());
                                TN_N3.Override.NodeAppearance.Image = Properties.Resources.check_mark_2_16;
                            }
                        }
                    }
                }
                modAdorstmp.Close();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Loadtreeview");
            }
        }



        private void SelectBCmd(double iargcmd)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string stemp = null;

            try
            {
                tabTmp[0] = "NoBCmd";

                tabSetting = new string[1][];

                sSQL = "SELECT BonDeCommande.ContactDivers, BonDeCommande.Estimation, BonDeCommande.Acheteur," + " BonDeCommande.Commentaire1, BonDeCommande.Codeimmeuble, BonDeCommande.CodeE," +
                    " BonDeCommande.DateCommande, BonDeCommande.Signataire,BonDeCommande.LieuLivraison," + " BonDeCommande.Lieu, BonDeCommande.DateLivraison, BonDeCommande.NoBonDeCommande," +
                    " BonDeCommande.NoIntervention, BonDeCommande.Verrou, BonDeCommande.CodeFourn," + " BonDeCommande.Type, BonDeCommande.CodeAg,BonDeCommande.verrou," +
                    " BonDeCommande.Technicien, BonDeCommande.Condition, BonDeCommande.Ordre, " + " BonDeCommande.EtatLivraison , BonDeCommande.EtatFacturation, BonDeCommande.ExportPDF,BonDeCommande.ServiceAnalytique, " +
                    " Intervention.CodeEtat, fournisseurArticle.Cleauto," + " fournisseurArticle.Raisonsocial, fournisseurArticle.Code,fournisseurArticle.contact , " + " fournisseurArticle.FAX, " +
                    " FNS_Agences.Fns_No , Personnel.Nom AS NomTech, FNS_Agences.Fns_Agence, " + " FNS_Agences.Fns_Fax , fns_contact ";

                if (General.sAnalytiqueBCD == "1")
                {
                    sSQL = sSQL + ", ANAB_Code ";
                }

                sSQL = sSQL + " FROM Personnel RIGHT OUTER JOIN" + " BonDeCommande LEFT OUTER JOIN" + " fournisseurArticle ON BonDeCommande.CodeFourn = fournisseurArticle.Cleauto" +
                    " LEFT OUTER JOIN" + " FNS_Agences ON BonDeCommande.CodeAg = FNS_Agences.Fns_No LEFT OUTER JOIN" + " TypeEtatCommande ON BonDeCommande.CodeE = TypeEtatCommande.Code INNER JOIN" +
                    " Intervention ON BonDeCommande.NoIntervention = Intervention.NoIntervention " + " ON Personnel.Matricule = BonDeCommande.Technicien" + " Where  BondeCommande.NoBonDeCommande = " + iargcmd;

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                ///===================> Tested
                if (rs.Rows.Count == 0)
                {

                    CodeAg = 0;
                    return;
                    // Init True, False, False, False
                }

                //Mondir le 25.06.2020, ragarder la declaration du variable CodeAg
                CodeAg = Convert.ToInt32(General.nz(rs.Rows[0]["CodeAg"], "0"));
                //Fin Modi Mondir 25.06.2020


                if (!string.IsNullOrEmpty(rs.Rows[0]["Verrou"].ToString()) && Convert.ToBoolean(rs.Rows[0]["Verrou"]) == true)
                {

                    ////==================> Tested First
                    //if (General.sBloqueBonImprime == "1")
                    //{
                    //== bloque les champs et la grille du bons de commande.
                    fc_Locked(true);
                    bBonLocked = true;
                    //}
                    //else
                    //{
                    //    //== Debloque les champs et la grille du bons de commande.
                    //    fc_Locked(false);
                    //    bBonLocked = false;
                    //}
                }
                else
                {
                    //== Debloque les champs et la grille du bons de commande.
                    fc_Locked(false);
                    bBonLocked = false;

                }

                txtNoBonDeCommande.Text = General.nz(rs.Rows[0]["NoBonDeCommande"], "").ToString();

                //tabTmp[1] = txtNoBonDeCommande.Text;
                //Array.Copy(tabTmp, tabSetting[0], 1);
                //GeneralXav.stockRegParam("UserBCmdHead", tabSetting);
                General.saveInReg("UserBCmdHead", "NoBCmd", txtNoBonDeCommande.Text);

                txtCodeFournisseur.Text = General.nz(rs.Rows[0]["Code"], "").ToString();

                //tabTmp[0] = "NoFns";
                ////tabTmp(1) = txtCodeFournisseur
                //tabTmp[1] = General.nz(rs.Rows[0]["CleAuto"], "");
                //Array.Copy(tabTmp, tabSetting[0], 1);
                //GeneralXav.stockRegParam("UserBCmdHead", tabSetting);
                General.saveInReg("UserBCmdHead", "NoFns", General.nz(rs.Rows[0]["CleAuto"], "").ToString());

                HidCleFourn.Text = General.nz(rs.Rows[0]["CleAuto"], "").ToString();

                txtDateCommande.Text = Convert.ToDateTime(rs.Rows[0]["DateCommande"]).ToString("dd/MM/yy HH:mm:ss");
                ///===========================================> Tested
                if (txtDateCommande.Text == "01/01/00")
                    txtDateCommande.Text = "";

                txtOrdre.Text = General.nz(rs.Rows[0]["ordre"], "0").ToString();

                Combo1.Rows[Convert.ToInt32(General.nz(rs.Rows[0]["Lieu"], 0))].Selected = true;
                cmbService.Text = rs.Rows[0]["ServiceAnalytique"] + "";
                fc_LibService();
                //txtB02.Caption = nz(Trim(rs("Typeint")), "")

                cmbType.Rows[Convert.ToInt32(General.nz(rs.Rows[0]["Type"], 0))].Selected = true;
                txtRaisonSocial.Text = General.nz(rs.Rows[0]["RaisonSocial"], "").ToString();
                txtCodeImmeuble2.Text = General.nz(rs.Rows[0]["Codeimmeuble"], "").ToString();
                txtLieuLivraison.Text = General.nz(rs.Rows[0]["LieuLivraison"], "").ToString();
                txtCommentaire1.Text = General.nz(rs.Rows[0]["Commentaire1"], "").ToString();
                txtEstimation.Text = General.nz(rs.Rows[0]["Estimation"], "").ToString();
                txtAcheteur.Text = General.nz(rs.Rows[0]["Signataire"], rs.Rows[0]["acheteur"]).ToString();
                //txtB13.ListIndex = nz(rs("Priorite"), -1)

                stemp = General.nz(rs.Rows[0]["CodeE"], "C").ToString();

                txtContactDivers.Text = General.nz(rs.Rows[0]["ContactDivers"], "").ToString();

                ///=======================> Tested First
                if (string.IsNullOrEmpty(General.nz(rs.Rows[0]["fns_No"], "").ToString()))
                {
                    if (string.IsNullOrEmpty(txtContactDivers.Text))
                        txtContactDivers.Text = General.nz(rs.Rows[0]["Contact"], "").ToString();
                    txtFns_No.Text = "";
                }
                else
                {
                    if (string.IsNullOrEmpty(txtContactDivers.Text))
                        txtContactDivers.Text = General.nz(rs.Rows[0]["Fns_Contact"], "").ToString();
                    txtFns_No.Text = General.nz(rs.Rows[0]["FNS_No"], "").ToString();

                }

                txtFNS_Agence.Text = General.nz(rs.Rows[0]["FNS_Agence"], "").ToString();
                txtFNS_Fax.Text = General.nz(rs.Rows[0]["FNS_Fax"], "").ToString();
                txtFax.Text = General.nz(rs.Rows[0]["fax"], "").ToString();
                txtMatricule.Text = General.nz(rs.Rows[0]["Technicien"], "").ToString();

                ///====================> Tested First
                if (!string.IsNullOrEmpty(txtMatricule.Text))
                {
                    SelectTech(txtMatricule.Text);
                }
                else
                {
                    txtTechnicien.Text = "";
                    txtNumRadio.Text = "";
                }

                txtNoIntervention2.Text = General.nz(rs.Rows[0]["NoIntervention"], "").ToString();
                txtDateLivraison.Text = General.nz(rs.Rows[0]["DateLivraison"], "").ToString();
                if (General.sAnalytiqueBCD == "1")
                {

                    CMB_Analytique.Text = rs.Rows[0]["ANAB_Code"] + "";
                }
                if (txtDateLivraison.Text == "01/01/1900")
                    txtDateLivraison.Text = "";

                fc_EtatFactLivr(rs.Rows[0]["EtatLivraison"] + "", rs.Rows[0]["EtatFacturation"] + "");

                ModAdo.fc_CloseRecordset(rs);

                sSQL = "select TypeEtatCommande.Libelle,TypeEtatCommande.Code from TypeEtatCommande";
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                cmbStatut.DataSource = null;
                hidEtat.DataSource = null;
                int k = 0;
                if (rs.Rows.Count > 0)//================>Tested
                {
                    i = 0;
                    k = -1;
                    DataTable cmbStatutSource = new DataTable();
                    cmbStatutSource.Columns.Add("Code");
                    DataTable hidEtatSource = new DataTable();
                    hidEtatSource.Columns.Add("Code");
                    foreach (DataRow rsRow in rs.Rows)
                    {
                        cmbStatutSource.Rows.Add((rsRow["Libelle"]));
                        hidEtatSource.Rows.Add((rsRow["Code"]));
                        cmbStatut.DataSource = cmbStatutSource;
                        hidEtat.DataSource = hidEtatSource;
                        if (rsRow["Code"].ToString() == stemp)
                            k = i;
                        i = i + 1;
                    }
                    if (k == -1)
                    {
                        hidEtat.Text = "C";
                        using (var tmpModAdo = new ModAdo())
                            cmbStatut.Text = tmpModAdo.fc_ADOlibelle("select TypeEtatCommande.Libelle FROM TypeEtatCommande WHERE TypeEtatCommande.Code='C'");
                    }
                    ////=====================> Tested
                    else
                    {
                        cmbStatut.Rows[k].Selected = true;
                        hidEtat.Rows[k].Selected = true;
                    }
                    //== affiche les satuts de livraison et de facturation.

                    ModAdo.fc_CloseRecordset(rs);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SelectBCmd");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sEtatLivraison"></param>
        /// <param name="sEtatFacturation"></param>
        private void fc_EtatFactLivr(string sEtatLivraison, string sEtatFacturation)
        {

            switch (sEtatLivraison)
            {
                case "NL":
                    lblEtatLivraison.Text = "Non Livré";
                    break;
                case "LP":
                    lblEtatLivraison.Text = "Livraison Partielle";
                    break;
                case "LT":
                    lblEtatLivraison.Text = "Livraison Totale";
                    break;
                default:
                    lblEtatLivraison.Text = "Non Livré";
                    break;
            }

            switch (sEtatFacturation)
            {
                case "NF":
                    lblEtatFacturation.Text = "Non Facturé";
                    break;
                case "FP":
                    lblEtatFacturation.Text = "Facturation Partielle";
                    break;
                case "FT":
                    lblEtatFacturation.Text = "Facturation Totale";
                    break;
                default:
                    lblEtatFacturation.Text = "Non Facturé";
                    break;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sargtech"></param>
        private void SelectTech(string sargtech)
        {
            DataTable rsTech = default(DataTable);
            ModAdo modAdorsTech = null;
            string sSQL = null;

            try
            {
                sSQL = "SELECT  Matricule, Nom, NumRadio  From Personnel " + " Where  Matricule ='" + sargtech + "'";

                modAdorsTech = new ModAdo();
                rsTech = modAdorsTech.fc_OpenRecordSet(sSQL);

                if (rsTech.Rows.Count == 0)
                    return;

                txtMatricule.Text = General.nz(rsTech.Rows[0]["Matricule"], "").ToString();
                txtTechnicien.Text = General.nz(rsTech.Rows[0]["Nom"], "").ToString();
                txtNumRadio.Text = General.nz(rsTech.Rows[0]["NumRadio"], "").ToString();

                ModAdo.fc_CloseRecordset(rsTech);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SelectTech");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        /// <param name="bLocked"></param>
        private void fc_LockedReq(Control C, bool bLocked)
        {
            foreach (Control CC in C.Controls)
            {
                if (CC.HasChildren)
                    fc_LockedReq(CC, bLocked);
                var v = CC;
                if (CC.Tag != null)
                {
                    if (CC.Tag.ToString().ToUpper() == cBCD.ToUpper())
                    {
                        if (v is iTalk.iTalk_TextBox_Small2 || v is System.Windows.Forms.ComboBox || v is System.Windows.Forms.TextBox || v is UltraCombo)
                        {
                            if (v is iTalk.iTalk_TextBox_Small2)
                            {
                                var ctl = v as iTalk.iTalk_TextBox_Small2;
                                ctl.ReadOnly = bLocked;
                            }
                            if (v is UltraCombo || v is ComboBox)
                            {
                                if (v is UltraCombo)
                                {
                                    var ctrl = v as UltraCombo;
                                    ctrl.ReadOnly = bLocked;
                                }
                            }
                        }

                        if (!(v is System.Windows.Forms.Button))
                        {
                            if (bLocked == true)
                            {
                                var ctl = v as Button;//Todo verifier avec Mondir
                                v.ForeColor = System.Drawing.Color.Blue;
                            }
                            else if (!(v is System.Windows.Forms.Label) && bLocked == false)
                            {
                                v.ForeColor = System.Drawing.Color.Black;
                            }
                        }

                        if (v is System.Windows.Forms.Button)
                        {
                            v.Enabled = !bLocked;
                        }


                        if (v is System.Windows.Forms.Label)
                        {
                            v.ForeColor = System.Drawing.Color.Blue;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bLocked"></param>
        private void fc_Locked(bool bLocked)
        {
            fc_LockedReq(this, bLocked);
            cmdVisu.Enabled = true;
            CmdEditer.Enabled = true;
            cmdFax.Enabled = true;
            cmdMail.Enabled = true;
            txtDateLivraison.ReadOnly = false;
            txtDateLivraison.ForeColor = System.Drawing.Color.Black;
            CmdSauver.Enabled = true;
            CMB_Analytique.Enabled = !bLocked;
            txtAcheteur.ReadOnly = true;
            txtAcheteur.ForeColor = System.Drawing.Color.Blue;

            cmbStatut.Enabled = true;
            cmbStatut.ReadOnly = false;
            cmbStatut.ForeColor = System.Drawing.Color.Black;

        }

        private void cmdDupliquer_Click(object sender, EventArgs e)
        {
            //====================================> Commented By Mondir : This Controle Is Hidden
            //frmDupliPreCom.txtPRE_NoAuto.Text = txtPRE_NoAuto.Text;
            //frmDupliPreCom.ShowDialog();
            //fc_ChargeEnregistrement(ref txtNoIntervention.Text, ref txtNumFicheStandard.Text);
        }

        private void CmdEditer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            bool fok = false;
            bool sport = false;
            bool fCheck = false;
            //If CmdSauver.Enabled = True Then
            //    nOk = MsgBox("Souhaitez vous enregistrer les modifications.", vbYesNo)
            //    If nOk = 6 Then Save
            //End If
            try
            {
                Save();

                if (General.sInterditAchatAZero == "1")
                {
                    if (Convert.ToDouble(General.nz(lblTotHT.Text, 0)) == 0)//verifier 
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, le montant total HT de votre bon de commande doit être supérieur à 0.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }

                //If cmdSauverVerrou.Visible = False Then
                nOk = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous êtes sur le point d'imprimer un bon de commande." + " Après confirmation le bon de commande ne sera plus modifiable." + "\n" + "Souhaitez-vous continuez ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //   nOk = fct_Locked(CInt(txt00.Text))
                if (nOk == (int)DialogResult.No)
                    return;
                fCheck = Check();
                if (fCheck == false)
                    return;

                Save();//tested

                fc_MajVerrou(Convert.ToInt32(txtNoBonDeCommande.Text));//tested
                fc_Locked(true);//tsted
                fc_RefreshBCD();
                fc_MajStatutBCD();

                //End If

                string sWhere = null;
                //sWhere = "{BonDeCommande.NoBonDeCommande}=" & CLng(txtNoBonDeCommande)

                sWhere = "{BonDeCommande.NoBonDeCommande}=" + txtNoBonDeCommande.Text;
                //fc_Print gsRpt & gfr_liaison("ReportBcmd"), sWhere


                //Exit Sub

                //Me.CrystalReport1.Reset

                ReportDocument ReportDocument = new ReportDocument();

                ReportDocument.Load(General.gsRpt + General.gfr_liaison("ReportBcmd"));
                ReportDocument.RecordSelectionFormula = sWhere;
                //TODO : Mondir - Must Check This Line : _with2.set_SortFields(0, "+{BCD_Detail.BCD_NoLigne}");
                //_with2.set_SortFields(0, "+{BCD_Detail.BCD_NoLigne}");
                ReportDocument.PrintToPrinter(1, true, 0, 0);


                General.fc_VerrifAchatDevis(txtNoIntervention.Text, txtNoBonDeCommande.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdEditer");
            }

        }


        private void Save()
        {
            string sCodeE = "";
            string sSQL = "";
            string dCmd = "";
            string dLiv = "";
            string IntervImm = "";
            string MaxOrdre = "";
            string sCptC = "";
            string sCptTVA = "";
            string sTempDF = "";
            string strCodeInt = "";
            string strCodeE = "";

            int nNo = 0;
            int nHtTemp = 0;
            int nInternalKeyDetail = 0;
            int nNoD = 0;

            object vCodeAg = null;

            double nId = 0;
            double nInternalKey = 0;
            object[] tabTemp = new object[3];

            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            DataTable rs1 = default(DataTable);
            ModAdo modAdors1 = null;
            DataTable rs2 = default(DataTable);
            ModAdo modAdors2 = null;
            DataTable rs3 = default(DataTable);
            ModAdo modAdors3 = null;

            bool fChange = false;
            bool fTemp = false;

            try
            {
                ssBCD_Detail.UpdateData();

                tabSetting = new string[1][];

                //Mondir 31.03.2020 : Added this to fix this bug : https://groupe-dt.mantishub.io/view.php?id=1747
                if (cmbStatut.ActiveRow != null)
                    strCodeE = hidEtat.Rows[cmbStatut.ActiveRow.Index].Cells[0].Text;

                fChange = true;

                strCodeInt = "AM";

                //If strCodeE = "" Then
                //    strCodeE = "CA"
                //ElseIf strCodeE = "CS" And txtB02.Caption = "R" Then
                //    Set rs = fc_OpenRecordSet("select codee,nobondecommande from bondecommande where Nointervention=" & CDbl(txt02.Caption))
                //    If Not (rs.EOF Or rs.BOF) Then
                //        Do Until (rs.EOF)
                //            If rs("codee") <> "CS" And rs("Nobondecommande") <> txt00.Text Then
                //                fChange = False
                //                Exit Do
                //            End If
                //            rs.MoveNext
                //        Loop
                //    End If
                //    fc_CloseRecordset rs
                //    If fChange Then strCodeInt = "AF"
                //ElseIf strCodeE <> "CS" Then
                //    strCodeInt = "AM"
                //End If

                if (!string.IsNullOrEmpty(txtFns_No.Text))
                {
                    vCodeAg = Convert.ToInt32(txtFns_No.Text);
                }

                //sSQl = "select TIN_CodeBon from intervention left join TIN_TypeIntervention on intervention.INT_typeintervention = TIN_TypeIntervention.INT_typeintervention where noIntervention=" & CDbl(txt02.Caption)
                //Set rs = fc_OpenRecordSet(sSQl)
                //If Not (rs.EOF Or rs.BOF) Then sTypeInt = nz(rs("TIN_CodeBon"), "")
                if (string.IsNullOrEmpty(txtNoBonDeCommande.Text))
                {
                    dCmd = DateTime.Now.ToString("dd/MM/yy HH:mm");
                    dLiv = "01/01/1900";
                    if ((rs != null))
                    {
                        modAdors.Dispose();
                    }
                    sSQL = "Select * from BonDeCommande where NoBonDeCommande = 0";
                    modAdors = new ModAdo();
                    rs = modAdors.fc_OpenRecordSet(sSQL);
                    var rsNewRow = rs.NewRow();
                    //        rs("TypeInt") = sTypeInt
                    rsNewRow["Signataire"] = StdSQLchaine.gFr_DoublerQuote(txtAcheteur.Text);
                    rsNewRow["Acheteur"] = StdSQLchaine.gFr_DoublerQuote(sUtilisateur);
                    rsNewRow["DateCommande"] = StdSQLchaine.gFr_DoublerQuote(dCmd);
                    if (!General.IsDate(txtDateLivraison.Text))
                    {
                        rsNewRow["DateLivraison"] = "01/01/1900";
                    }
                    else
                    {
                        rsNewRow["DateLivraison"] = StdSQLchaine.gFr_DoublerQuote(txtDateLivraison.Text);
                    }

                    rsNewRow["ANAB_Code"] = CMB_Analytique.Text + "";

                    rsNewRow["CodeE"] = strCodeE;
                    //If txt02.Caption = "Pas d'intervention" Then txt02.Caption = -1
                    rsNewRow["NoIntervention"] = txtNoIntervention2.Text;
                    rsNewRow["NumFicheStandard"] = txtNumFicheStandard.Text;

                    using (var tmpModAdo = new ModAdo())
                        MaxOrdre = tmpModAdo.fc_ADOlibelle("SELECT MAX(Ordre) as [MaxOrdre] FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text);

                    if (string.IsNullOrEmpty(MaxOrdre))
                        MaxOrdre = "0";

                    rsNewRow["Ordre"] = Convert.ToInt16(MaxOrdre) + 1;
                    txtOrdre.Text = rsNewRow["Ordre"].ToString();
                    rsNewRow["CodeFourn"] = Convert.ToInt32(HidCleFourn.Text);
                    //        IntervImm = fc_ADOlibelle("SELECT GestionStandard.CodeImmeuble FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard WHERE Intervention.NoIntervention=" & nz(txt02.Caption, "0"))
                    //        If txt50.Text <> IntervImm Then
                    //            txt50.Text = IntervImm
                    //        End If

                    rsNewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                    rsNewRow["LieuLivraison"] = StdSQLchaine.gFr_DoublerQuote(txtLieuLivraison.Text);
                    rsNewRow["Commentaire1"] = StdSQLchaine.gFr_DoublerQuote(txtCommentaire1.Text);
                    rsNewRow["CodeAg"] = vCodeAg;
                    rsNewRow["Technicien"] = txtMatricule.Text;
                    //        rs("Condition") = gFr_DoublerQuote(txt91.Text)
                    rsNewRow["Lieu"] = Combo1.ActiveRow.Index;
                    rsNewRow["ServiceAnalytique"] = cmbService.Text + "";

                    //        rs("Type") = txt03.ListIndex
                    rsNewRow["Estimation"] = StdSQLchaine.gFr_DoublerQuote(Convert.ToDouble(txtEstimation.Text).ToString("0.00"));
                    rsNewRow["ContactDivers"] = StdSQLchaine.gFr_DoublerQuote(txtContactDivers.Text);
                    rsNewRow["Priorite"] = 0;
                    rs.Rows.Add(rsNewRow);

                    modAdors.Update();
                    using (var tmpModAdo = new ModAdo())
                        txtNoBonDeCommande.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(NoBonDeCommande) FROM BonDeCommande");
                    //    Title.Caption = "Bon de Commande N° " & txt00.Text
                    ModAdo.fc_CloseRecordset(rs);
                    General.fc_VerrifAchatInterv(txtNoIntervention.Text, txtNoBonDeCommande.Text);
                }
                else
                {
                    //    IntervImm = fc_ADOlibelle("SELECT GestionStandard.CodeImmeuble FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard WHERE Intervention.NoIntervention=" & nz(txt02.Caption, "0"))
                    //    If txt50.Text <> IntervImm Then
                    //        txt50.Text = IntervImm
                    //    End If
                    //


                    sSQL = "Update BondeCommande set DateLivraison='" + StdSQLchaine.gFr_DoublerQuote(txtDateLivraison.Text) + "',CodeE='" + StdSQLchaine.gFr_DoublerQuote(strCodeE) + "',"
                         + "ANAB_Code='" + StdSQLchaine.gFr_DoublerQuote(CMB_Analytique.Text) + "',"
                         + " CodeFourn=" + HidCleFourn.Text + ",CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',"
                         + " LieuLivraison='" + StdSQLchaine.gFr_DoublerQuote(txtLieuLivraison.Text) + "',"
                         + " Priorite= 0, Signataire='" + StdSQLchaine.gFr_DoublerQuote(txtAcheteur.Text) + "',"
                         + " Commentaire1='" + StdSQLchaine.gFr_DoublerQuote(txtCommentaire1.Text) + "',CodeAg='" + txtFns_No.Text + "',"
                         + " Technicien='" + txtMatricule.Text + "'" + " ,Lieu='" + (Combo1.ActiveRow == null ? 0 : Combo1.ActiveRow.Index) + "',"
                         + " ServiceAnalytique='" + StdSQLchaine.gFr_DoublerQuote(cmbService.Text) + "',"
                        + " Estimation='" + StdSQLchaine.gFr_DoublerQuote(Convert.ToDouble(General.nz(txtEstimation.Text, 0)).ToString("#.##")) + "',"
                         + "ContactDivers='" + StdSQLchaine.gFr_DoublerQuote(txtContactDivers.Text) + "'"
                         + " From BondeCommande where NoBonDeCommande=" + txtNoBonDeCommande.Text;
                    //& " ,TypeInt='" & nz(sTypeInt, "") & "' " _
                    //',Condition='" & gFr_DoublerQuote(txt91.Text) & "'
                    //" Type=" & txt03.ListIndex &
                    int xx = General.Execute(sSQL);

                }
                //sSql = "Update Intervention set codeEtat ='" & strCodeInt & "',Achat = 1 where NoIntervention=" & txt02.Caption
                sSQL = "Update Intervention set Achat = 1 where NoIntervention=" + txtNoIntervention.Text;
                General.Execute(sSQL);
                nId = Convert.ToInt32(txtNoBonDeCommande.Text);
                // tabTmp[0] = "NoBCmd";
                //  tabTmp[1] = nId;               
                //  Array.Copy(tabTmp, tabSetting, 2);
                //   GeneralXav.stockRegParam("UserBCmdHead", tabSetting);

                General.saveInReg("UserBCmdHead", "NoBCmd", txtNoBonDeCommande.Text);

                // tabTmp[0] = "NoFns";
                //tabTmp[1] = StdSQLchaine.gFr_DoublerQuote(txtFNS_Agence.Text);
                // Array.Copy(tabTmp, tabSetting[0], 2);
                //GeneralXav.stockRegParam("UserBCmdHead", tabSetting);

                General.saveInReg("UserBCmdHead", "NoFns", txtFNS_Agence.Text);

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Sub Save ", false);
            }
        }

        private bool Check()
        {
            bool functionReturnValue = false;
            string sSQL = null;

            try
            {

                functionReturnValue = true;
                if (string.IsNullOrEmpty(txtCodeFournisseur.Text))
                {
                    functionReturnValue = false;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le code fournisseur.");
                    return functionReturnValue;
                }
                //if (string.IsNullOrEmpty(txtEstimation.Text))
                //{
                //    functionReturnValue = false;
                //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez préciser l'estimation.");
                //    return functionReturnValue;
                //}

                //=== modif du 30 septembre 2014, ajout du champs service,
                //=== champs obligatoire apres une date defini.
                if (fc_CtrlService() == false)
                {
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                //If txt03.Text = "" Then
                //    Check = False
                //    MsgBox ("Indiquez si il s'agit de sous-traitance ou d'achat fourniture.")
                //    Exit Function
                //End If
                if (string.IsNullOrEmpty(txtCodeImmeuble2.Text))
                {
                    functionReturnValue = false;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le code Immeuble.");
                    return functionReturnValue;
                }
                if (string.IsNullOrEmpty(txtTechnicien.Text) && General.sTechObligatoire == "1")
                {
                    functionReturnValue = false;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez affecter un technicien à ce bon de commande.");

                    txtMatricule.ReadOnly = false;
                    txtTechnicien.ReadOnly = false;
                    txtMatricule.Enabled = true;
                    txtTechnicien.Enabled = true;
                    txtTechnicien.ForeColor = System.Drawing.Color.Black;
                    txtMatricule.ForeColor = System.Drawing.Color.Black;
                    cmdRechercheTech.Enabled = true;
                    return functionReturnValue;
                }

                //=== modif du 25 01 2013, analutyque obligatoire.

                if (General.sAnalytiqueBCD == "1")
                {
                    if (General.IsDate(General.sDateAnalytiqueBDC) && General.IsDate(txtDateCommande.Text))
                    {
                        if (Convert.ToDateTime(txtDateCommande.Text) >= Convert.ToDateTime(General.sDateAnalytiqueBDC))
                        {
                            if (string.IsNullOrEmpty(CMB_Analytique.Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord présider le code analytique (plomberie ou couverture).", "Validation incomplète", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                functionReturnValue = false;
                                return functionReturnValue;
                            }
                            else
                            {
                                using (var tmpModAdo = new ModAdo())
                                    sSQL = tmpModAdo.fc_ADOlibelle("SELECT     ANAB_Code From ANAB_AnalytiqueBCD WHERE ANAB_Code = '" + StdSQLchaine.gFr_DoublerQuote(CMB_Analytique.Text) + "'");
                                if (string.IsNullOrEmpty(sSQL))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code analytique " + CMB_Analytique.Text + " n'existe pas, veuillez en sélectionner un autre dans la liste déroulante.", "Validation incomplète", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    functionReturnValue = false;
                                    return functionReturnValue;
                                }
                            }
                        }

                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Check");
                return false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNoBonDeCommande"></param>
        private void fc_MajVerrou(int lNoBonDeCommande)
        {

            General.Execute("UPDATE BonDECommande set Verrou = -1 " + " WHERE NoBonDeCommande=" + lNoBonDeCommande);

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_RefreshBCD()
        {
            try
            {

                foreach (UltraTreeNode oNode in TreeView3.Nodes)
                {
                    foreach (UltraTreeNode oNode_N1 in oNode.Nodes)
                    {
                        foreach (UltraTreeNode oNode_N2 in oNode_N1.Nodes)
                        {
                            if (oNode_N2.Selected == true)
                            {
                                // TreeView3_AfterSelect(TreeView3, new TreeViewEventArgs(oNode_N2));
                                TreeView3.ActiveNode = oNode_N2;
                                loopOnTheNodes();
                                //TreeView3_AfterSelect(TreeView3, null);


                                //Added by Mondir
                                //Was commented, Must change the image after click on the print 
                                oNode_N2.Override.NodeAppearance.Image = Properties.Resources.minus_4_16;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_RefreshBCmd");
            }
        }

        private void fc_MajStatutBCD()
        {
            string sSQL = null;
            int i = 0;
            try
            {

                if (hidEtat.Text.ToUpper() == "CA".ToUpper())
                {
                    sSQL = "UPDATE BonDeCommande set CodeE ='" + cEtatEnCours + "'" + " WHERE NoBonDecommande =" + txtNoBonDeCommande.Text;
                    General.Execute(sSQL);

                    for (i = 0; i < hidEtat.Rows.Count; i++)
                    {
                        //         cmbStatut.ListIndex = k
                        //        hidEtat.ListIndex = k
                        if (hidEtat.Rows[i].Cells[0].Text.ToUpper() == cEtatEnCours.ToUpper())
                        {
                            cmbStatut.ActiveRow = cmbStatut.Rows[i];
                            hidEtat.ActiveRow = hidEtat.Rows[i];
                            //cmbStatut = cEtatEnCours
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_MajStatutBCd");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdfactScann_Click(object sender, EventArgs e)
        {
            string[] stNobonDeCommande = new string[1];
            try
            {
                stNobonDeCommande[0] = txtNoBonDeCommande.Text;
                if (!string.IsNullOrEmpty(txtNoBonDeCommande.Text))//added by mohammed to catch an error in SQL QUERY when txtNoBonDeCommande is empty.
                    General.fc_FindFactFournRD(stNobonDeCommande);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdfactScann_click");
            }
        }

        private void cmdFax_Click(object sender, EventArgs e)
        {
            string sTexte = null;
            string sport = null;
            bool fok = false;
            string sWhere = null;
            string versionWindows = null;
            bool fCheck = false;
            string sSortFields = null;
            //Dim interfSendFax   As InterfaxCOM.Client

            try
            {
                //If CmdSauver.Enabled = True Then
                //    nOk = MsgBox("Souhaitez vous enregistrer les modifications.", vbYesNo)
                //If nOk = 6 Then
                Save();
                //End If
                //If hidfax = "" Then
                //    MsgBox "Numero de fax inexistant."
                //    Exit Sub
                //End If
                if (General.sInterditAchatAZero == "1")
                {
                    if (Convert.ToDouble(General.nz(lblTotHT.Text, 0)) == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, le montant total HT de votre bon de commande doit être supérieur à 0.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }

                //If cmdSauverVerrou.Visible = False Then
                nOk = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous êtes sur le point d'envoyer ce bon de commande par fax." + " Après confirmation le bon de commande ne sera plus modifiable." + "\n" + "Souhaitez-vous continuez ?",
                    "", MessageBoxButtons.YesNo
                    , MessageBoxIcon.Question);
                //    fok = fct_Locked(CInt(txt00.Caption))
                if (nOk == (int)DialogResult.No)
                    return;

                fCheck = Check();
                if (fCheck == false)
                    return;


                //End If
                string sNumber = null;
                using (var tmpModAdo = new ModAdo())
                    if (string.IsNullOrEmpty(txtFNS_Agence.Text))
                    {
                        sNumber = tmpModAdo.fc_ADOlibelle("SELECT fax FROM FournisseurArticle WHERE code='" + txtCodeFournisseur.Text + "'");
                    }
                    else
                    {
                        sNumber = tmpModAdo.fc_ADOlibelle("SELECT Fns_Fax FROM FNS_Agences WHERE FNS_NO='" + txtFns_No.Text + "'");
                    }

                if (string.IsNullOrEmpty(sNumber))
                {
                    if (!string.IsNullOrEmpty(txtFNS_Agence.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le numero de fax de l'agence du fournisseur" + txtCodeFournisseur.Text + " est introuvable.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le numero de fax du fournisseur" + txtCodeFournisseur.Text + " est introuvable.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    return;
                }

                //== envoie de fax via Faxmaker.
                sWhere = "{BonDeCommande.NoBonDeCommande}=" + Convert.ToInt32(txtNoBonDeCommande.Text);
                sSortFields = "+{BCD_Detail.BCD_NoLigne}";

                ReportDocument CrystalReport1 = new ReportDocument();

                if (Convert.ToDouble(General.FAXMAKER) == 1)
                {
                    if (General.fc_sendFaxmaker(CrystalReport1, General.gsRpt + General.gfr_liaison("ReportBcmd"), sWhere, 0, General.Left(SAGE.SuppCar(sNumber, " "), 10), txtCodeFournisseur.Text, sSortFields) == 1)
                    {
                        fc_MajVerrou(Convert.ToInt32(txtNoBonDeCommande.Text));
                        fc_Locked(true);
                        fc_RefreshBCD();
                    }
                    //  fc_VerrifAchatDevis txt02.Caption, txt00.Caption
                    return;
                }

                fc_MajStatutBCD();
                General.fc_VerrifAchatDevis(txtNoIntervention.Text, txtNoBonDeCommande.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " ; cmdFax_Click ;");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervention_Click(object sender, EventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
            // If CmdSauver.Enabled = True Then
            //    fok = MsgBox("Souhaitez vous enregistrer les modifications.", vbYesNo)
            //    If fok = 6 Then Save
            //End If

            View.Theme.Theme.Navigate(typeof(UserIntervention));
        }

        private void cmdMail_Click(object sender, EventArgs e)
        {
            int nOk = 0;
            bool fCheck = false;
            var sSQL = "";

            try
            {
                ModCourrier.EnvoieMail = true;

                //If CmdSauver.Enabled = True Then
                //    nOk = MsgBox("Souhaitez vous enregistrer les modifications.", vbYesNo)
                //    If nOk = 6 Then

                Save();
                //End If
                if (General.sInterditAchatAZero == "1")
                {
                    if (Convert.ToDouble(General.nz(lblTotHT.Text, 0)) == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, le montant total HT de votre bon de commande doit être supérieur à 0.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }

                //If cmdSauverVerrou.Visible = False Then
                nOk = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous êtes sur le point d'envoyer ce bon de commande par mail. Après confirmation le bon de commande ne sera plus modifiable." + "\n" + "Souhaitez-vous continuez ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //    fok = fct_Locked(CInt(txt00.Text))
                if (nOk == (int)DialogResult.No)
                    return;

                // End If
                fCheck = Check();

                if (fCheck == false)
                    return;
                Save();
                fc_MajVerrou(Convert.ToInt32(txtNoBonDeCommande.Text));
                fc_Locked(true);
                fc_RefreshBCD();

                if (ModCourrier.EnvoieMail == true)
                {
                    ModCourrier.FichierAttache = fc_ExportBcmd(txtNoBonDeCommande.Text);
                }
                ModMain.bActivate = true;
                frmMail frmMail = new frmMail();

                //===> Mondir le 12.02.2021, look at the diff of PreCommande https://groupe-dt.mantishub.io/view.php?id=2261
                frmMail.PreCommande = true;
                //===> Fin Modif Mondir

                //Mondir le 25.06.2020, ragarder la declaration du variable CodeAg
                using (var tmpADO = new ModAdo())
                {
                    //En cas d'une agance
                    if (CodeAg != 0)
                    {
                        sSQL = $@"SELECT Email, Fns_Email FROM fournisseurArticle
                        JOIN FNS_Agences ON FNS_Agences.Fns_Cle = fournisseurArticle.Cleauto
                        WHERE Fns_No = {CodeAg}";
                        var dt = tmpADO.fc_OpenRecordSet(sSQL);

                        //Pas de ligne
                        if (dt.Rows.Count == 0)
                            sSQL = "";
                        //Email agence existe
                        else if (!string.IsNullOrEmpty(dt.Rows[0]["Fns_Email"]?.ToString()))
                            sSQL = dt.Rows[0]["Fns_Email"].ToString();
                        //Email fournisseur existe
                        else if (!string.IsNullOrEmpty(dt.Rows[0]["Email"]?.ToString()))
                            sSQL = dt.Rows[0]["Email"].ToString();
                        //KO
                        else
                            sSQL = "";
                    }
                    //En cas pas d'agence
                    else
                    {
                        //======> J'ai replacé le WHERE 'CODE = xx' par 'CleAuto = xx' car il y a plusieurs fournisseurs avec le meme code
                        sSQL = "SELECT     Email From fournisseurArticle WHERE fournisseurArticle.Cleauto = " +
                               HidCleFourn.Text;
                        sSQL = tmpADO.fc_ADOlibelle(sSQL);
                    }

                    //===> Mondir le 21.02.2021 pour corriger https://groupe-dt.mantishub.io/view.php?id=2292
                    sSQL = sSQL.Trim();
                    //===> Fin Modif Mondir

                    //Changer la valeur de SSOleDBcmbA
                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        frmMail.SSOleDBcmbA.Text = sSQL;
                    }
                }
                //Fin Modif Mondir

                frmMail.txtDocuments.Text = ModCourrier.FichierAttache;
                frmMail.ShowDialog();
                if (ModCourrier.EnvoieMail == true)
                {
                    // MonExplorer.Close
                }

                frmMail.Close();
                //Réinitialise l'envoi du mail
                ModCourrier.EnvoieMail = false;
                fc_MajStatutBCD();

                General.fc_VerrifAchatDevis(txtNoIntervention.Text, txtNoBonDeCommande.Text);
            }
            catch (Exception ex)
            {
                ModCourrier.EnvoieMail = false;
                Erreurs.gFr_debug(ex, this.Name + ";cmdMail_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNumeroBcmd"></param>
        /// <returns></returns>
        public string fc_ExportBcmd(string sNumeroBcmd)
        {
            string functionReturnValue = null;

            string sCodeImmeuble = null;
            string sTitreBcmd = null;
            int i = 0;
            object X = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            ReportDocument Report = null;

            try
            {
                modAdors = new ModAdo();
                General.SQL = "SELECT BonDeCommande.NoBonDeCommande,BonDeCommande.CodeImmeuble,BonDeCommande.DateCommande FROM BonDeCommande WHERE BonDeCommande.NoBonDeCommande='" + sNumeroBcmd + "'";
                rs = modAdors.fc_OpenRecordSet(General.SQL);

                Report = new ReportDocument();
                Report.Load(General.gsRpt + "Bcmd-V9.rpt");

                if (rs.Rows.Count > 0)
                {
                    sCodeImmeuble = General.Trim(rs.Rows[0]["CodeImmeuble"] + "");
                    sNumeroBcmd = General.Replace(rs.Rows[0]["NoBonDeCommande"] + "", "/", "-");
                    sTitreBcmd = "Commande fourniture pour " + sCodeImmeuble + " du " + rs.Rows[0]["DateCommande"];
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Achats ");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Achats\\out");
                    if (Dossier.fc_ControleFichier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Achats\\out\\Bcmd" + sNumeroBcmd + ".pdf") == false)
                    {
                        //TODO : Mondir - Ask About This 
                        //Report.ParameterFields[1].AddDefaultValue(rs.rows[0]["NoBonDeCommande").value);
                        Report.SetParameterValue("BCmdID", rs.Rows[0]["NoBonDeCommande"].ToString());

                        ExportOptions CrExportOptions;
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                        CrDiskFileDestinationOptions.DiskFileName = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Achats\\out\\Bcmd-" + General.Replace(sNumeroBcmd, "/", "-") + ".pdf";
                        CrExportOptions = Report.ExportOptions;
                        {
                            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                            CrExportOptions.FormatOptions = CrFormatTypeOptions;
                        }

                        Report.Export();

                    }
                    General.Execute("UPDATE BonDeCommande SET ExportPDF=1 WHERE NoBonDeCommande='" + sNumeroBcmd + "'");
                }

                functionReturnValue = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Achats\\out\\Bcmd-" + General.Replace(sNumeroBcmd, "/", "-") + ".pdf";
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                functionReturnValue = "";
                return functionReturnValue;
            }
        }

        private void cmdModifierFourn_Click(object sender, EventArgs e)
        {
            UltraTreeNode oNode = new UltraTreeNode();
            int i = 0;
            string skeyFourn = "";
            string sFourn = "";

            try
            {

                if (General.nz(txtPRE_NoAuto.Text, 0).ToString() != "0")//=========>Tested
                {

                    sFourn = "";

                    foreach (UltraTreeNode oNode_loopVariable in TreeView1.Nodes)
                    {
                        oNode = oNode_loopVariable;

                        foreach (UltraTreeNode oNode2 in oNode.Nodes)
                        {
                            if (oNode2.Selected == true)
                            {
                                if (General.UCase(General.Left(oNode2.Key, 1)) == cNiv2.ToUpper())
                                {
                                    sFourn = General.Right(oNode2.Key, General.Len(oNode2.Key) - 1);
                                    break;
                                }
                            }
                        }
                    }

                    FrmMajFourn FrmMajFourn = new FrmMajFourn();
                    FrmMajFourn.txtPRC_FournisseurRef.Visible = true;
                    FrmMajFourn.txtPRC_FournisseurRef.Text = sFourn;
                    FrmMajFourn.txtPRE_NoAuto.Text = txtPRE_NoAuto.Text;
                    FrmMajFourn.ShowDialog();
                    fc_PRC_PreCmdCorps(txtPRE_NoAuto.Text);

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdModifierFourn");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sargfourn"></param>
        private void selectfourn(int sargfourn)
        {
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sSQL = null;

            try
            {
                //sSQL = "SELECT   CleAuto, Code, Raisonsocial, fax From fournisseurArticle where Code='" & sargfourn & "'"
                sSQL = "SELECT   CleAuto, Code, Raisonsocial, fax From fournisseurArticle where CleAuto='" + sargfourn + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                    return;

                HidCleFourn.Text = General.nz(rs.Rows[0]["CleAuto"], "").ToString();

                txtCodeFournisseur.Text = General.nz(rs.Rows[0]["Code"], "").ToString();

                txtRaisonSocial.Text = General.nz(rs.Rows[0]["Raisonsocial"], "").ToString();
                txtFax.Text = General.nz(rs.Rows[0]["fax"], "").ToString();
                txtFns_No.Text = "";
                txtFNS_Agence.Text = "";
                txtFNS_Fax.Text = "";

                ModAdo.fc_CloseRecordset(rs);

                CmdRerchercheAg.Visible = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Selectfourn");
            }

        }
        /// <summary>
        /// /tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheFourn_Click(object sender, EventArgs e)
        {
            string sCode = null;
            double nCode = 0;

            try
            {
                string requete = "SELECT  Code as \"Code\", Raisonsocial as \"RaisonSociale\",  Ville as \"Ville\", Cleauto as \"N°Fiche\"" + " FROM fournisseurArticle";
                string where_order = "(code <> '' and code is not null)  and (NePasAfficher is null or NePasAfficher = 0 )";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un fournisseur à votre bon de commande" };
                fg.SetValues(new Dictionary<string, string> { { "Code", txtCodeFournisseur.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble

                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    //selectfourn sCode
                    selectfourn(Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["N°Fiche"].Value));//tested

                    CmdRerchercheAg.Visible = true;
                    //== met à jour le nom du frounisseur sur le treeview.
                    foreach (UltraTreeNode TN_N1 in TreeView3.Nodes)
                    {
                        if (General.Right(TN_N1.Key, General.Len(TN_N1.Key) - 1) == txtNoBonDeCommande.Text)
                        {

                            TN_N1.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                            TN_N1.Selected = true;
                            break;
                        }
                        foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                        {
                            if (General.Right(TN_N2.Key, General.Len(TN_N2.Key) - 1) == txtNoBonDeCommande.Text)
                            {

                                TN_N2.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                                TN_N2.Selected = true;
                                break;
                            }
                            foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)//====>Tested
                            {
                                if (General.Right(TN_N3.Key, General.Len(TN_N3.Key) - 1) == txtNoBonDeCommande.Text)
                                {
                                    TN_N3.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                                    TN_N3.Selected = true;
                                    break;
                                }
                            }
                        }
                    }

                    TreeView3.Enabled = false;
                    Timer1.Enabled = true;
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble

                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        //selectfourn sCode
                        selectfourn(Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["N°Fiche"].Value));

                        CmdRerchercheAg.Visible = true;
                        //== met à jour le nom du frounisseur sur le treeview.
                        foreach (UltraTreeNode TN_N1 in TreeView3.Nodes)
                        {
                            if (General.Right(TN_N1.Key, General.Len(TN_N1.Key) - 1) == txtNoBonDeCommande.Text)
                            {

                                TN_N1.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                                TN_N1.Selected = true;
                                break;
                            }
                            foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                            {
                                if (General.Right(TN_N2.Key, General.Len(TN_N2.Key) - 1) == txtNoBonDeCommande.Text)
                                {

                                    TN_N2.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                                    TN_N2.Selected = true;
                                    break;
                                }
                                foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                {
                                    if (General.Right(TN_N3.Key, General.Len(TN_N3.Key) - 1) == txtNoBonDeCommande.Text)
                                    {
                                        TN_N3.Text = txtNoBonDeCommande.Text + "-" + General.nz(txtRaisonSocial.Text, txtCodeFournisseur.Text);
                                        TN_N3.Selected = true;
                                        break;
                                    }
                                }
                            }
                        }

                        TreeView3.Enabled = false;
                        Timer1.Enabled = true;
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheTech_Click(object sender, EventArgs e)
        {
            string sCode = null;

            try
            {
                string requete = "SELECT  Matricule as \"Matricule\", Nom as \"Nom\", Prenom as \"Prenom\"" + " From Personnel";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher un technicien à votre bon de commande" };
                if (bRechMat == true)
                {
                    fg.SetValues(new Dictionary<string, string> { { "Matricule", txtMatricule.Text } });
                }
                else if (bRechNom == true)
                {
                    fg.SetValues(new Dictionary<string, string> { { "Nom", txtTechnicien.Text } });
                }

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                    SelectTech(sCode);//tested
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                        SelectTech(sCode);
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRerchercheAg_Click(object sender, EventArgs e)
        {
            double nCode = 0;

            try
            {
                string requete = "SELECT code as \"code\" , FNS_Agence as \"Agence\" , Fns_Contact as \"Contact\", Fns_Ville as \"Ville\", " +
                " FNS_No as \"Numero interne\"" + " FROM FNS_Agences inner join FournisseurArticle on FournisseurArticle.cleAuto = FNS_Agences.Fns_Cle";
                string where_order = " Fns_Cle =" + Convert.ToInt32(HidCleFourn.Text);
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Attacher une agence fournisseur à votre bon de commande" };
                fg.SetValues(new Dictionary<string, string> { { "code", txtCodeFournisseur.Text } });
                fg.ugResultat.InitializeLayout += (se, ev) =>
                {
                    fg.ugResultat.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    nCode = Convert.ToDouble(fg.ugResultat.ActiveRow.Cells["Numero interne"].Value);
                    //Mondir le 25.06.2020, ragarder la declaration du variable CodeAg
                    CodeAg = nCode;
                    //Fin Modi Mondir 25.06.2020
                    selectfournAg(nCode);
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        nCode = Convert.ToDouble(fg.ugResultat.ActiveRow.Cells["Numero interne"].Value);
                        //Mondir le 25.06.2020, ragarder la declaration du variable CodeAg
                        CodeAg = nCode;
                        //Fin Modi Mondir 25.06.2020
                        selectfournAg(nCode);
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                TreeView3.Enabled = false;
                Timer1.Enabled = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
            }
        }

        private void selectfournAg(double nargag)
        {
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            string sSQL = null;
            try
            {
                sSQL = "SELECT FNS_Agence, FNS_NO, FNS_Fax FROM FNS_Agences where FNS_No =" + nargag;

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                    return;

                txtFns_No.Text = rs.Rows[0]["FNS_No"].ToString();
                txtFNS_Agence.Text = General.nz(rs.Rows[0]["FNS_Agence"], "").ToString();
                txtFNS_Fax.Text = General.nz(rs.Rows[0]["FNS_Fax"], "").ToString();
                ModAdo.fc_CloseRecordset(rs);
                CmdRerchercheAg.Visible = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";selectfournAg");
            }
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            try
            {
                bool fCheck = false;
                fCheck = Check();

                if (fCheck == false)
                    return;

                Save();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSauver");
            }
        }
        private void save2()
        {
            //Dim stemp As String
            //Dim i As Double
            //Grid0.Update
            //For i = 0 To UBound(tabCmd, 2) - 1
            //    nNo = 0
            //    nNoD = 0
            //    If CLng(tabCmd(4, i)) > 0 Then nNo = tabCmd(4, i)
            //    Set rs1 = fc_OpenRecordSet("Select * From BCF_FactFourn where BCF_Factfourn.Bcf_No=" & nNo)
            //    If nNo = 0 Then
            //        rs1.AddNew
            //    End If
            //    rs1("BCF_Cle") = CLng(txt00.Text)
            //    rs1("BCF_NoFact") = gFr_DoublerQuote(tabCmd(0, i))
            //    rs1("BCF_DL") = nz(tabCmd(1, i), "01/01/1900")
            //    rs1("BCF_TVA") = gFr_DoublerQuote(Format(tabCmd(3, i), "0.00"))
            //    rs1("BCF_HT") = gFr_DoublerQuote(Format(tabCmd(2, i), "0.00"))
            //    If InStr(tabCmd(5, i), "/") <= 0 And InStr(tabCmd(5, i), ".") < 0 Then
            //        If Len(tabCmd(5, i)) = 8 Then
            //            sTempDF = Left(tabCmd(5, i), 2) & "/" & Mid(tabCmd(5, i), 3, 2) & "/" & Right(tabCmd(5, i), 4)
            //        ElseIf Len(tabCmd(5, i)) = 7 Then
            //            sTempDF = Left(tabCmd(5, i), 1) & "/" & Mid(tabCmd(5, i), 2, 2) & "/" & Right(tabCmd(5, i), 4)
            //        ElseIf Len(tabCmd(5, i)) = 6 Then
            //            sTempDF = Left(tabCmd(5, i), 2) & "/" & Mid(tabCmd(5, i), 3, 2) & "/" & Right(tabCmd(5, i), 2)
            //        ElseIf Len(tabCmd(5, i)) = 5 Then
            //            sTempDF = Left(tabCmd(5, i), 1) & "/" & Mid(tabCmd(5, i), 2, 1) & "/" & Right(tabCmd(5, i), 2)
            //        End If
            //    ElseIf InStr(tabCmd(5, i), ".") > 0 Then
            //       sTempDF = Split(tabCmd(5, i), ".")(0) & "/" & Split(tabCmd(5, i), ".")(1) & "/" & Split(tabCmd(5, i), ".")(2)
            //    Else
            //        sTempDF = tabCmd(5, i)
            //    End If
            //    rs1("BCF_DF") = nz(sTempDF, "01/01/1900")
            //    rs1("BCF_SCAN") = nz(gFr_DoublerQuote(tabCmd(7, i)), "")
            //    rs1("BCF_TotalHT") = gFr_DoublerQuote(Format(nz(tabCmd(8, i), tabCmd(2, i)), "0.00"))
            //    rs1("BCF_TTC") = gFr_DoublerQuote(Format(nz(tabCmd(9, i), 0), "0.00"))
            //    rs1.Update
            //    nInternalKey = rs1("BCF_NoFact")
            //    fc_CloseRecordset rs1
            //    If txt03.ListIndex = 0 Then
            //        sCptC = "60110000"
            //        sCptTVA = "44566000"
            //    ElseIf txt03.ListIndex = 1 Then
            //        sCptC = "60410000"
            //        sCptTVA = "445661000"
            //    ElseIf txt03.ListIndex = 2 Then
            //        ReDim tabCmd(9, 0)
            //        ReDim tabCmdFD(0)
            //        Init True, True, True, False
            //        Label13.Enabled = True
            //        Exit Sub
            //    End If
            //    Set rs = fc_OpenRecordSet("Select NoBondeCommande From Bondecommande inner join BCF_FactFourn on Bondecommande.NoBondecommande = BCF_FactFourn.BCF_Cle where BCF_Nofact =" & tabCmd(0, i) & "And BCF_Cle <>" & txt00.Text)
            //    If Not (rs.EOF Or rs.BOF) Then
            //        Do Until rs.EOF
            //            fTemp = True
            //            Set rs1 = fc_OpenRecordSet("Select sum(Bcf_HT) as sumHT,Bcf_TotalHT,Bcf_NoFact From BCF_FactFourn Group by Bcf_NoFact,Bcf_TotalHT having Bcf_Nofact in(Select Bcf_NoFact From BCF_FactFourn where Bcf_cle=" & rs("NoBondeCommande") & ")")
            //            Do Until rs1.EOF
            //                If rs1("SumHT") <> rs1("BCF_TotalHT") Then fTemp = False
            //                rs1.MoveNext
            //            Loop
            //            strCodeE = "CF"
            //            If fTemp Then strCodeE = "CS"
            //            adocnn.Execute ("Update Bondecommande set codee='" & strCodeE & "' where nobondecommande = " & rs("NoBondeCommande"))
            //            rs.MoveNext
            //        Loop
            //        fc_CloseRecordset rs1
            //    End If
            //    fc_CloseRecordset rs
            //    Set rs = fc_OpenRecordSet("Select BCF_Detail.BCF_No as [NumInterne] From BCF_FactFourn inner Join BCF_Detail on BCF_FactFourn.Bcf_Nofact = BCF_Detail.Bcf_Cle and BCF_FactFourn.Bcf_Cle=BCF_Detail.Bcf_bon where BCF_Factfourn.Bcf_No=" & nNo)
            //    If rs.EOF And rs.BOF Then
            //        stemp = "Insert into BCF_Detail(BCF_Cle,BCF_Bon,BCF_Des,BCF_Cpt,BCF_cptTVA,BCF_HT,BCF_TVA)" _
            //'                & " Values(" & nInternalKey & ",'" & gFr_DoublerQuote(txt00.Text) & "','" _
            //'                & "','" & sCptC & "','" & sCptTVA & "'," & gFr_DoublerQuote(Format(nz(tabCmd(2, i), 0), "0.00")) & ",19.6" & ")"
            //    Else
            //         stemp = "Update BCF_Detail set BCF_Cpt='" & sCptC & "',Bcf_cptTVA='" & sCptTVA & "'" & ",BCF_HT='" & gFr_DoublerQuote(Format(nz(tabCmd(2, i), 0), "0.00")) & "'" _
            //'                & "where BCF_No =" & rs("NumInterne")
            //    End If
            //    adocnn.Execute (stemp)
            //    fc_CloseRecordset rs
            //Next
            //For i = 0 To UBound(tabCmdFD) - 1
            //     adocnn.Execute ("Delete From BCF_Detail where Bcf_Bon=" & txt00.Text & " and BCF_cle in(Select BCF_NoFact From BCF_FactFourn where Bcf_No=" & CLng(nz(tabCmdFD(i), 0)) & ")")
            //    adocnn.Execute ("Delete From BCF_FactFourn where BCF_No=" & CLng(nz(tabCmdFD(i), 0)))
            //
            //Next
            //ReDim tabCmd(9, 0)
            //ReDim tabCmdFD(0)
            //nInt = 1
            //Init True, True, True, False
            //Label13.Enabled = True
            //    Exit Sub
            //Erreur:
            //    gFr_debug Me.Name & " Sub Save ", False

        }

        private void CmdSauverPre_Click(object sender, EventArgs e)
        {
            //==onglet du treeview precommande.
            if (SSTab2.ActiveTab.Index == 0)
            {
                ssPRC_PreCmdCorps.UpdateData();
            }
            else if (SSTab2.ActiveTab.Index == 1)
            {

                ssPRC_PreCmdCorpsHisto.UpdateData();
            }
            else if (SSTab2.ActiveTab.Index == 2)
            {
                ssBCD_Detail.UpdateData();
            }
        }

        public void fc_Bcmd()
        {
            //    Dim strInsert   As String
            //    Dim strRequete  As String
            //    Dim rsBcmd      As ADODB.Recordset
            //    Dim i           As Long
            //    Dim NoBcmd      As String
            //    Dim strPrix     As String
            //
            //    On Error GoTo ErrFc_Bcmd
            //
            //    Set rsBcmd = New ADODB.Recordset
            //
            //    'Si un bon de commande existe alors on sort
            //  '  strRequete = "SELECT  NoBonDeCommande FROM BonDeCommande WHERE NumeroDevis='" & txtNoDevis.Text & "'"
            //    rsBcmd.Open strRequete, adocnn
            //    If Not rsBcmd.EOF And Not rsBcmd.BOF Then
            //        rsBcmd.Close
            //        Set rsBcmd = ENothing
            //        Exit Function
            //    End If
            //
            //    rsBcmd.Close
            //
            //    strInsert = "INSERT INTO BonDeCommande ("
            //        strInsert = strInsert & "Acheteur"
            //        strInsert = strInsert & ",CodeImmeuble"
            //        strInsert = strInsert & ",Commentaire1"
            //        strInsert = strInsert & ",DateCommande"
            //        strInsert = strInsert & ",Fournisseur"
            //        strInsert = strInsert & ",NoIntervention"
            //        strInsert = strInsert & ",NumeroDevis"
            //        strInsert = strInsert & ",NumFicheStandard"
            //        strInsert = strInsert & ",CodeFourn"
            //        strInsert = strInsert & ",CodeE"
            //        strInsert = strInsert & ",Verrou"
            //        strInsert = strInsert & ",Ordre"
            //        strInsert = strInsert & ",EtatLivraison"
            //        strInsert = strInsert & ",EtatFacturation"
            //        strInsert = strInsert & ",Technicien"
            //        strInsert = strInsert & ",Estimation"
            //        strInsert = strInsert & ",Priorite"
            //        strInsert = strInsert & ") VALUES ("
            //        strInsert = strInsert & "'" & fc_ADOlibelle("SELECT Personnel.Initiales FROM Personnel INNER JOIN DevisEntete ON Personnel.Matricule=DevisEntete.CodeDeviseur WHERE DevisEntete.NumeroDevis='" & txtNoDevis.Text & "'") & "',"
            //        strInsert = strInsert & "'" & txtCodeImmeuble.Text & "',"
            //        strInsert = strInsert & "'Suite Devis N°: " & txtNoDevis.Text & " :" & vbCrLf & ""
            //        strInsert = strInsert & gFr_DoublerQuote(fc_ADOlibelle("SELECT TitreDevis FROM DevisEntete WHERE NumeroDevis='" & txtNoDevis.Text & "'")) & "',"
            //        strInsert = strInsert & "'" & Date & "',"
            //        strInsert = strInsert & "'" & nz(fc_ADOlibelle("SELECT FournisseurArticle.Nom FROM FournisseurArticle WHERE RaisonSocial LIKE '%" & strFournisseurInterne & "%'"), strFournisseurInterne) & "',"
            //        strInsert = strInsert & fc_ADOlibelle("SELECT TOP 1 Intervention.NoIntervention FROM Intervention WHERE NumFicheStandard=" & txtNumFicheStandard.Text & " ORDER BY NoIntervention") & ","
            //        strInsert = strInsert & "'" & txtNoDevis.Text & "',"
            //        strInsert = strInsert & txtNumFicheStandard.Text & ","
            //        strInsert = strInsert & nz(fc_ADOlibelle("SELECT CleAuto FROM FournisseurArticle WHERE Nom LIKE '%" & strFournisseurInterne & "%'"), "''") & "," 'CodeFourn
            //        strInsert = strInsert & "'CA'," 'Code Etat
            //        strInsert = strInsert & "0," 'Verrou
            //        strInsert = strInsert & "1," 'Ordre
            //        strInsert = strInsert & "'NL'," 'Etat Livraison
            //        strInsert = strInsert & "'NF'," 'EtatFacturation
            //        strInsert = strInsert & "'" & "000" & "'," 'Technicien : 000 en attente d'affectation
            //        strInsert = strInsert & "0," 'Estimation
            //        strInsert = strInsert & "0" 'Priorité
            //        strInsert = strInsert & ")"
            //
            //adocnn.Execute strInsert
            //
            //rsBcmd.Open "SELECT BonDeCommande.NoBonDeCommande FROM BonDeCommande WHERE NumeroDevis='" & txtNoDevis.Text & "'", adocnn
            //If Not rsBcmd.EOF And Not rsBcmd.BOF Then
            //    NoBcmd = rsBcmd!NoBonDeCommande & ""
            //End If
            //rsBcmd.Close
            //
            //strRequete = "SELECT DevisDetail.CodeSousArticle,DevisDetail.TexteLigne,"
            //strRequete = strRequete & "DevisDetail.Quantite,DevisDetail.Foud,"
            //strRequete = strRequete & "BaseGecet.dbo.DtiPrix.FouUnite,"
            //strRequete = strRequete & "BaseGecet.dbo.DtiPrix.Fournisseur,"
            //strRequete = strRequete & "BaseGecet.dbo.DtiPrix.Fourref, "
            //strRequete = strRequete & "BaseGecet.dbo.DtiPrix.[prix net achat] as Prix, "
            //strRequete = strRequete & "QTE_Unite.Qte_No "
            //strRequete = strRequete & "FROM DevisDetail LEFT JOIN BaseGecet.dbo.DtiPrix ON "
            //strRequete = strRequete & "DevisDetail.CodeSousArticle=BaseGecet.dbo.DtiPrix.Chrono "
            //strRequete = strRequete & " LEFT JOIN QTE_Unite ON BaseGecet.dbo.DtiPrix.FouUnite=QTE_Unite.QTE_Libelle "
            //strRequete = strRequete & " WHERE DevisDetail.NumeroDevis='" & txtNoDevis.Text & "' "
            //strRequete = strRequete & " AND NOT DevisDetail.CodeSousArticle IS NULL "
            //strRequete = strRequete & " AND DevisDetail.CodeSousArticle<>'' "
            //strRequete = strRequete & " ORDER BY DevisDetail.NumeroLigne"
            //rsBcmd.Open strRequete, adocnn
            //i = 1
            //If Not rsBcmd.EOF And Not rsBcmd.BOF Then
            //    While Not rsBcmd.EOF
            //        strInsert = "INSERT INTO BCD_DETAIL ("
            //        strInsert = strInsert & "BCD_Cle"
            //        strInsert = strInsert & ",BCD_References"
            //        strInsert = strInsert & ",BCD_Designation"
            //        strInsert = strInsert & ",BCD_Quantite"
            //        strInsert = strInsert & ",BCD_PrixHT"
            //        strInsert = strInsert & ",BCD_Unite"
            //        strInsert = strInsert & ",BCD_NoLigne"
            //        strInsert = strInsert & ",BCD_Fournisseur"
            //        strInsert = strInsert & ") VALUES ("
            //        strInsert = strInsert & NoBcmd & ","
            //        strInsert = strInsert & "'" & nz(rsBcmd!fourref, rsBcmd!CodeSousArticle) & "" & "',"
            //        strInsert = strInsert & "'" & gFr_DoublerQuote(rsBcmd!TexteLigne) & "',"
            //        strInsert = strInsert & "" & nz(rsBcmd!Quantite, "0") & ","
            //        If IsNull(rsBcmd!Prix) Or rsBcmd!Prix = "" Then
            //            strInsert = strInsert & "" & nz(rsBcmd!Foud, "0") & ","
            //        Else
            //            strPrix = fc_FormatNumber(rsBcmd!Prix & "")
            //            strInsert = strInsert & "" & strPrix & ","
            //        End If
            //        strInsert = strInsert & "" & nz(rsBcmd!Qte_No, "0") & ","
            //        strInsert = strInsert & i & ", "
            //        strInsert = strInsert & "'" & rsBcmd!Fournisseur & "" & "')"
            //        adocnn.Execute strInsert
            //        i = i + 1
            //        rsBcmd.MoveNext
            //    Wend
            //End If
            //
            //rsBcmd.Close
            //Set rsBcmd = Nothing
            //Exit Function
            //ErrFc_Bcmd:
            //    Debug.Print err.Description
            //    gFr_debug Me.Name & " fc_Bcmd "

        }

        //====================> Another Methode With The Same Name That I Found, Somebody added it when testing
        //private bool fc_CreateBcmd(ref int lOneNoBonDeCommande)
        //{
        //    bool functionReturnValue = false;
        //    int i = 0;
        //    int j = 0;
        //    int lNoBonDeCommande = 0;
        //    DataTable rsBDC = default(DataTable);
        //    ModAdo modAdorsBDC = null;
        //    DataTable rsBCD_Detail = default(DataTable);
        //    ModAdo modAdorsBCD_Detail = null;
        //    DataTable rsPRCbis = default(DataTable);
        //    ModAdo modAdorsPRCbis = null;
        //    string sFourn = null;
        //    int X = 0;
        //    bool bNull = false;
        //    int NbBon = 0;
        //    string sTexte = null;

        //    lOneNoBonDeCommande = 0;

        //    functionReturnValue = true;

        //    //== Récupére dans le corps de la precommande tous les articles sélectionnés(PRC_Sel = 1)
        //    fc_PRC_PreCmdCorps("", " WHERE PRE_NoAuto=" + txtPRE_NoAuto.Text + " and (PRC_Histo = 0 OR PRC_Histo IS NULL) ");

        //    modAdorsPRCbis = new ModAdo();
        //    rsPRCbis = modAdorsPRCbis.fc_OpenRecordSet(modAdorsPRCbis.SDArsAdo.SelectCommand.CommandText + " order by PRC_FournisseurDT");

        //    //== controle si tous les articles ont bien un fournisseur et compte
        //    //== le nombre de bon de commande.
        //    var rsPRCbisFilter = rsPRCbis.Select("PRC_SEL = 1");

        //    sFourn = "";
        //    NbBon = 0;
        //    DataRow ActiveDataRow = null;
        //    foreach (DataRow rsPRCbisFilterRow in rsPRCbisFilter)
        //    {
        //        ActiveDataRow = rsPRCbisFilterRow;
        //        if (string.IsNullOrEmpty(General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString()))
        //        {
        //            bNull = true;
        //            break;
        //        }
        //        if (sFourn != General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString())
        //        {
        //            NbBon = NbBon + 1;
        //            sFourn = General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString();
        //        }
        //    }
        //    if (bNull == true)
        //    {
        //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("l'article " + ActiveDataRow["PRC_Designation"] + " n'est pas rattaché à un fournisseur " + " existant dans la base " + General.NomSociete + "." + "Cliquez sur le bouton '" + clibButton + "' pour mettre à jour le fournisseur.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        modAdorsPRCbis.Close();
        //        functionReturnValue = false;
        //        return functionReturnValue;
        //    }

        //    if (NbBon == 1)
        //    {
        //        sTexte = "Vous allez créer " + NbBon + " bon de commande, Voulez vous continuer?";
        //    }
        //    else
        //    {
        //        sTexte = "Vous allez créer " + NbBon + " bons de commandes, Voulez vous continuer?";
        //    }

        //    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
        //    {
        //        functionReturnValue = false;
        //        return functionReturnValue;
        //    }


        //    //==Si Il existe au moins un enregistrement on crée au moins un bon de commande.
        //    if (rsPRCbis.Rows.Count <= 0)
        //    {
        //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune precommande n'a été trouvé.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        return functionReturnValue;
        //    }

        //    //== Entete du bon de commande.
        //    sSQL = "SELECT Acheteur, CodeImmeuble, Commentaire1, DateCommande, Fournisseur, NoIntervention" + " ,NumeroDevis, NumFicheStandard, CodeFourn, CodeE, Verrou," +
        //        " Ordre, EtatLivraison " + " ,EtatFacturation, Technicien, Estimation, Priorite, NoBonDeCommande,CodeAG " + " FROM BonDeCommande " + " WHERE NoBondeCommande = 0";

        //    modAdorsBDC = new ModAdo();
        //    rsBDC = modAdorsBDC.fc_OpenRecordSet(sSQL);

        //    //== Corps du bon de commande.
        //    sSQL = "SELECT BCD_Cle, BCD_References, BCD_Designation, BCD_Quantite" + " ,BCD_PrixHT, BCD_TotHT,BCD_Unite, BCD_NoLigne, BCD_Fournisseur, BCD_Chrono," +
        //        " BCD_NoAutoGecet, ReferenceGecet, PRC_NoAuto " + " FROM BCD_DETAIL  " + " WHERE BCD_No = 0";

        //    modAdorsBCD_Detail = new ModAdo();
        //    rsBCD_Detail = modAdorsBCD_Detail.fc_OpenRecordSet(sSQL);

        //    //== creation des bons de commandes.
        //    //== classe le jeu d'enregistrement par fournisseur.
        //    rsPRCbisFilter = rsPRCbis.Select("PRC_SEL = 1", "PRC_FournisseurDT");
        //    sFourn = "";
        //    X = 1;
        //    foreach (DataRow rsPRCbisFilterRow in rsPRCbisFilter)
        //    {
        //        //== Pour chaque fournisseur, on crée un nouveau bon de commande.
        //        if (General.UCase(sFourn) != General.UCase(General.nz(rsPRCbisFilterRow["PRC_FournisseurDT"], "").ToString()))
        //        {
        //            //== creation de l'entéte du bon de commande.
        //            sFourn = rsPRCbisFilterRow["PRC_FournisseurDT"].ToString();
        //            var rsBDCNewRow = rsBDC.NewRow();
        //            //rsBDC!Acheteur = fc_ADOlibelle("SELECT Personnel.Initiales FROM Personnel " _
        //            //& " INNER JOIN DevisEntete ON Personnel.Matricule=DevisEntete.CodeDeviseur" _
        //            //& " WHERE DevisEntete.NumeroDevis='" & txtNumeroDevis & "'") & "'"
        //            rsBDCNewRow["Acheteur"] = General.fncUserName();
        //            rsBDCNewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
        //            if (!string.IsNullOrEmpty(txtNumeroDevis.Text))
        //            {
        //                using (var tmpModAdo = new ModAdo())
        //                    rsBDCNewRow["Commentaire1"] = "Suite Devis N°: " + txtNumeroDevis.Text + " :" + "\n" + StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle("SELECT TitreDevis FROM DevisEntete " +
        //                        " WHERE NumeroDevis='" + txtNumeroDevis.Text + "'"));
        //            }

        //            rsBDCNewRow["DateCommande"] = DateTime.Now;
        //            rsBDCNewRow["Fournisseur"] = rsPRCbisFilterRow["PRC_FournisseurDT"];

        //            //rsBDC!NoIntervention = fc_ADOlibelle("SELECT TOP 1 Intervention.NoIntervention FROM Intervention" _
        //            //& " WHERE NumFicheStandard=" & txtNumFicheStandard _
        //            //& " ORDER BY NoIntervention")
        //            rsBDCNewRow["Nointervention"] = txtNoIntervention.Text;
        //            rsBDCNewRow["numerodevis"] = txtNumeroDevis.Text;
        //            rsBDCNewRow["Numfichestandard"] = txtNumFicheStandard.Text;
        //            rsBDCNewRow["CodeFourn"] = rsPRCbisFilterRow["PRC_FournDTCle"];
        //            //nz(fc_ADOlibelle("SELECT CleAuto FROM FournisseurArticle " |                                        '& " WHERE Nom LIKE '%" & nz(!PRC_Fournisseur, "") & "%'"), "0")
        //            rsBDCNewRow["CodeFourn"] = rsPRCbisFilterRow["PRC_FournDTCle"];
        //            rsBDCNewRow["codeag"] = rsPRCbisFilterRow["FNS_No"];

        //            rsBDCNewRow["CodeE"] = "CA";
        //            rsBDCNewRow["Verrou"] = 0;
        //            rsBDCNewRow["ordre"] = 1;
        //            rsBDCNewRow["EtatLivraison"] = "NL";
        //            rsBDCNewRow["EtatFacturation"] = "NF";
        //            rsBDCNewRow["Technicien"] = "000";
        //            rsBDCNewRow["Estimation"] = "0";
        //            rsBDCNewRow["Priorite"] = "0";
        //            rsBDC.Rows.Add(rsBDCNewRow);
        //            modAdorsBDC.Update();

        //            using (var tmpModAdo = new ModAdo())
        //                lNoBonDeCommande = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoBonDeCommande) FROM BonDeCommande"));
        //            //== si un bon de commande on retourne en parametre le numero de celui-ci
        //            if (NbBon == 1)
        //            {
        //                lOneNoBonDeCommande = lNoBonDeCommande;
        //            }
        //        }
        //        //== met à jour le numéro de bons de commande dans le corps de la precommande.
        //        rsPRCbisFilterRow["prc_NoBonDeCommande"] = lNoBonDeCommande;

        //        //== detail du bon de commande.
        //        var rsBCD_DetailNewRow = rsBCD_Detail.NewRow();
        //        rsBCD_DetailNewRow["BCD_Cle"] = lNoBonDeCommande;

        //        rsBCD_DetailNewRow["BCD_References"] = rsPRCbisFilterRow["PRC_References"];
        //        rsBCD_DetailNewRow["BCD_Designation"] = rsPRCbisFilterRow["PRC_Designation"];
        //        rsBCD_DetailNewRow["BCD_Quantite"] = rsPRCbisFilterRow["PRC_Quantite"];
        //        rsBCD_DetailNewRow["BCD_PrixHT"] = rsPRCbisFilterRow["PRC_PrixHT"];
        //        rsBCD_DetailNewRow["BCD_TotHT"] = rsPRCbisFilterRow["PRC_TotHT"];
        //        rsBCD_DetailNewRow["BCD_Unite"] = rsPRCbisFilterRow["PRC_Unite"];
        //        rsBCD_DetailNewRow["BCD_NoLigne"] = X;
        //        rsBCD_DetailNewRow["PRC_NoAuto"] = rsPRCbisFilterRow["PRC_NoAuto"];
        //        rsBCD_DetailNewRow["ReferenceGecet"] = rsPRCbisFilterRow["PRC_References"];
        //        X = X + 1;
        //        rsBCD_DetailNewRow["BCD_Fournisseur"] = rsPRCbisFilterRow["PRC_Fournisseur"];
        //        rsBCD_DetailNewRow["BCD_Chrono"] = rsPRCbisFilterRow["PRC_Chrono"];
        //        rsBCD_DetailNewRow["BCD_NoAutoGecet"] = rsPRCbisFilterRow["NoAutoGecet"];
        //        rsBCD_DetailNewRow["ReferenceGecet"] = rsPRCbisFilterRow["PRC_References"];
        //        rsBCD_Detail.Rows.Add(rsBCD_DetailNewRow);
        //        modAdorsBCD_Detail.Update();
        //        if (rsPRCbisFilterRow["PRC_Chrono"].ToString() == General.cArticleDefaut)
        //        {

        //            sSQL = "UPDATE PRC_PreCmdCorps set  PRC_Histo = 1 WHERE PRC_NoAuto = " + rsPRCbisFilterRow["PRC_NoAuto"];
        //        }
        //        else
        //        {
        //            sSQL = "UPDATE PRC_PreCmdCorps set  PRC_Histo = 1 " + " WHERE PRE_Noauto =" + rsPRCbisFilterRow["PRE_NoAuto"] +
        //                " and PRC_Chrono =" + General.nz(rsPRCbisFilterRow["PRC_Chrono"], General.cArticleDefaut);
        //        }
        //        General.Execute(sSQL);
        //    }


        //    modAdorsPRCbis.Close();
        //    modAdorsBCD_Detail.Close();
        //    modAdorsBDC.Close();
        //    return functionReturnValue;


        //}

        private void cmdSupprimer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //=========================================> Mondir : This Controle Is Hidden
            //string sVerrou = null;
            //ADODB.Recordset rs = default(ADODB.Recordset);

            //sVerrou = ModAdo.fc_ADOlibelle("SELECT Verrou FROM BonDeCommande " + " WHERE NoBondeCommande =" + txtNoBonDeCommande.Text);

            //if (Convert.ToBoolean(sVerrou) == true)
            //{
            //    Interaction.MsgBox("Vous ne pouvez pas supprimer un bon de commande qui a déjà été édité.", MsgBoxStyle.Information, "Opération annulée.");
            //    return;
            //}
            //if (Interaction.MsgBox("Attention vous allez supprimer le bon de commande n° " + txtNoBonDeCommande.Text + ". Voulez vous continuer?", MsgBoxStyle.Critical + MsgBoxStyle.YesNo, "Suppression") == MsgBoxResult.Yes)
            //{

            //    rs = ModAdo.fc_OpenRecordSet("SELECT * FROM bondecommande WHERE Nobondecommande=" + txtNoBonDeCommande.Text);
            //    while (rs.EOF == false)
            //    {
            //        rs.Delete();
            //        rs.MoveNext();
            //    }

            //    rs.Close();
            //    rs = ModAdo.fc_OpenRecordSet("SELECT * FROM BCD_Detail WHERE BCD_Cle =" + txtNoBonDeCommande.Text);
            //    while (rs.EOF == false)
            //    {
            //        rs.Delete();
            //        rs.MoveNext();
            //    }

            //    //            Do
            //    //                Set rs = fc_OpenRecordSet("SELECT BCD_Cle FROM BCD_Detail WHERE BCD_Cle =" & txtNoBonDeCommande)
            //    //                If rs.EOF And rs.BOF Then Exit Do
            //    //
            //    //            Loop

            //    rs.Close();
            //    rs = null;

            //    fc_BCD_Detail(ref 1, ref " Where NoIntervention =" + txtNoIntervention.Text + "", ref true);
            //    fc_VisibleBCD(ref false);
            //}

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisu_Click(object sender, EventArgs e)
        {
            int fok = 0;
            bool fCheck = false;

            try
            {
                if (CmdSauver.Enabled == true)
                {
                    fok = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez vous enregistrer les modifications.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (fok == 6)
                    {
                        fCheck = Check();
                        if (fCheck == false)
                            return;

                        Save();
                    }
                }


                cmdVisu.Enabled = false;

                string sWhere = null;

                //sWhere = "{BonDeCommande.NoBonDeCommande}=" & CLng(txtNoBonDeCommande)

                sWhere = "{BonDeCommande.NoBonDeCommande}=" + txtNoBonDeCommande.Text;
                General.fc_Apercu(General.gsRpt + General.gfr_liaison("ReportBcmd"), sWhere, true, false);

                cmdVisu.Enabled = true;
                //return;

                ////Me.CrystalReport1.Reset

                //var _with7 = this.CrystalReport1;
                //_with7.Connect = Interaction.GetSetting(General.cFrNomApp, "App", "source_odbc");
                //_with7.PrinterCollation = Crystal.PrinterCollationConstants.crptUncollated;
                //_with7.WindowShowPrintBtn = false;
                //_with7.ReportFileName = General.gsRpt + General.gfr_liaison("ReportBcmd");
                //_with7.SelectionFormula = sWhere;
                //_with7.set_SortFields(0, "+{BCD_Detail.BCD_NoLigne}");
                //_with7.Destination = Crystal.DestinationConstants.crptToWindow;
                //_with7.WindowTitle = "Bon de commande N°" + txtNoBonDeCommande.Text;
                //_with7.Action = 1;
                //_with7.SelectionFormula = "";
                //_with7.ReportFileName = "";
                //_with7.Connect = "";
                //_with7.Reset();


                //cmdvisu.Enabled = true;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                cmdVisu.Enabled = true;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// tessted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DropUnite_AfterCloseUp(object sender, EventArgs e)
        {
            if (DropUnite.ActiveRow != null)
            {
                ssBCD_Detail.ActiveRow.Cells["BCD_Unite"].Value = DropUnite.ActiveRow.Cells["QTE_NO"].Value;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserPreCommande_Load(object sender, EventArgs e)
        {
            label26.Visible = false;
            InitLieu();
            txtEstimation.Visible = false;

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void InitLieu()
        {
            var Combo1Source = new DataTable();
            Combo1Source.Columns.Add("");
            Combo1Source.Rows.Add("Bureaux");
            Combo1Source.Rows.Add("Chantier");
            Combo1Source.Rows.Add("Comptoir");
            Combo1Source.Rows.Add("Enlevement");
            Combo1Source.Rows.Add("Bureaux Clichy");
            Combo1.DataSource = Combo1Source;
            Combo1.ActiveRow = Combo1.Rows[0];
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label1_Click(object sender, EventArgs e)
        {
            try
            {
                General.saveInReg(Variable.cUserPreCommande2, "CodeFournisseur", txtCodeFournisseur.Text);
                General.saveInReg(Variable.cUserPreCommande2, "CleAutoFourn", HidCleFourn.Text);
                ModParametre.fc_SaveParamPosition(this.Name, Variable.Cuserdocfourn, "");
                View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Label1_click");
            }
        }

        private void lblAffaireEnCours_Click(object sender, EventArgs e)
        {
            try
            {
                Variable.bHistoBcndAffaire = true;

                General.saveInReg("UserBCmdHead", "NoInt", txtNoIntervention.Text);
                General.saveInReg("UserBCmdHead", "NoFicheAppel", txtNumFicheStandard.Text);

                ModParametre.fc_SaveParamPosition(Variable.cUserPreCommande2, Variable.cUserBCLivraison, "");

                View.Theme.Theme.Navigate(typeof(UserBCLivraison));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblAffaireEnCours_click");
            }
        }

        private void lblGoAgence_Click(object sender, EventArgs e)
        {
            try
            {
                General.saveInReg(Variable.cUserPreCommande2, "CodeFournisseur", txtCodeFournisseur.Text);
                ModParametre.fc_SaveParamPosition(this.Name, Variable.Cuserdocfourn, "");
                View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoAgence_click");
            }
        }

        private void lblGoFourn_Click(object sender, EventArgs e)
        {
            try
            {
                General.saveInReg(Variable.cUserPreCommande2, "CodeFournisseur", txtCodeFournisseur.Text);
                General.saveInReg(Variable.cUserPreCommande2, "CleAutoFourn", HidCleFourn.Text);
                //fc_SaveParamPosition Me.Name, Cuserdocfourn, ""
                ModParametre.fc_SaveParamPosition(Variable.cUserPreCommande2, Variable.Cuserdocfourn, "");

                View.Theme.Theme.Navigate(typeof(UserDocFournisseur));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoFourn");
            }
        }

        private void lblGoHistorique_Click(object sender, EventArgs e)
        {
            try
            {
                ModParametre.fc_SaveParamPosition(Variable.cUserPreCommande2, Variable.cUserBCLivraison, "");

                View.Theme.Theme.Navigate(typeof(UserBCLivraison));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoHistorique");
            }

        }

        private void dupliquerEn80000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int j = 0;
            int i = 0;
            UltraGridRow bkmrk = null;
            bool bArtPresent = false;

            try
            {
                //== fonction permettant de dupliquer une ligne.
                if (tArtFourn == null)
                    return;
                var _with8 = ssPRC_PreCmdCorps;
                //== récupére tous les élements en mode sélection et les
                //== insére dans un tableau.
                if (_with8.ActiveRow != null)
                    bkmrk = _with8.ActiveRow;
                tArtDupli = null;
                j = 0;
                for (i = 0; i <= tArtFourn.Length - 1; i++)//tested
                {

                    if (tArtFourn[i].lSelEnCours == 1)
                    {
                        tArtFourn[i].vBookMark["PRC_SelEnCours"] = 0;
                        modAdorsPRC.Update();
                        Array.Resize(ref tArtDupli, j + 1);

                        tArtDupli[j].Qte = Convert.ToDouble(General.nz(tArtFourn[i].vBookMark["PRC_Quantite"], 0));
                        tArtDupli[j].sUnit = General.nz(tArtFourn[i].vBookMark["PRC_Unite"], "").ToString();
                        tArtDupli[j].sDesignation = General.nz(tArtFourn[i].vBookMark["PRC_Designation"], "").ToString();
                        bArtPresent = true;
                        tArtFourn[i].lSelEnCours = 0;
                        j = j + 1;
                    }

                }

                //== On ajoute ces éléments dans la grille.
                if (bArtPresent == true)
                {
                    for (i = 0; i <= tArtDupli.Length - 1; i++)
                    {
                        var Row = ssPRC_PreCmdCorps.DisplayLayout.Bands[0].AddNew();
                        Row.Cells["PRC_Chrono"].Value = Convert.ToInt32(General.cArticleDefaut);
                        //.Columns(PRC_NoLigne").value =
                        if (string.IsNullOrEmpty(tArtDupli[i].sUnit))
                        {
                            Row.Cells["PRC_Unite"].Value = DBNull.Value;
                        }
                        else
                        {
                            Row.Cells["PRC_Unite"].Value = Convert.ToInt32(tArtDupli[i].sUnit);
                        }
                        Row.Cells["PRC_Designation"].Value = tArtDupli[i].sDesignation;
                        Row.Cells["PRC_Quantite"].Value = Convert.ToDouble(tArtDupli[i].Qte);
                        ssPRC_PreCmdCorps.UpdateData();

                        //  fc_addTabArt rsPRC!PRC_chrono, rsPRC!PRC_Fournisseur, rsPRC!PRC_NoAuto _
                        //, rsPRC.Bookmark, False*/


                        //  fc_addTabArt rsPRC!PRC_chrono, rsPRC!PRC_Fournisseur, rsPRC!PRC_NoAuto _
                        //, rsPRC.Bookmark, False
                    }
                    //== se repositionne sur l'enregistrement d'origine.
                    if (bkmrk != null)
                        ssPRC_PreCmdCorps.ActiveRow = bkmrk;
                    _with8.Refresh();
                    tArtDupli = null;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";dupliquerEn80000TollStripMenuItem");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_AfterCellUpdate(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                if (e.Cell.Column.Key.ToUpper() == "BCD_References".ToUpper())
                {
                    if (!string.IsNullOrEmpty(ssBCD_Detail.ActiveRow.Cells["BCD_References"].Text))
                    {
                        if (General.sGecetV3 == "1")
                        {
                            sub_InsereArtGecetV3((ssBCD_Detail.ActiveRow.Cells["BCD_References"].Value.ToString()));
                        }
                        else
                        {
                            sub_InsereArtGecet((ssBCD_Detail.ActiveRow.Cells["BCD_References"].Value.ToString()));
                        }

                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBCD_Detail_AfterCellUpdate");
            }
        }
        private void sub_InsereArtGecetV3(string sRefFourn)
        {
            DataTable rsFournGecet = default(DataTable);
            ModAdo modAdorsFournGecet = new ModAdo();
            string sqlSelect = null;
            double dblPrix = 0;
            string sUnit = null;

            try
            {
                sqlSelect = "SELECT     AXECIEL_GECET.dbo.ArticleTarif.REF_FOURNISSEUR AS Fourref, AXECIEL_GECET.dbo.Article.DESIGNATION_ARTICLE AS libelle,"
                            + " AXECIEL_GECET.dbo.ArticleTarif.PRX_DEVIS_NET_UD AS [Prix net achat], "
                            + " AXECIEL_GECET.dbo.Article.CHRONO_INITIAL AS chrono, AXECIEL_GECET.dbo.ArticleTarif.CODE_UNITE_DEVIS as FouUnite "
                            + " FROM         AXECIEL_GECET.dbo.Article INNER JOIN "
                            + " AXECIEL_GECET.dbo.ArticleTarif ON AXECIEL_GECET.dbo.Article.CHRONO_INITIAL = AXECIEL_GECET.dbo.ArticleTarif.CHRONO_INITIAL"
                            + " WHERE     AXECIEL_GECET.dbo.ArticleTarif.REF_FOURNISSEUR = '" + StdSQLchaine.gFr_DoublerQuote(sRefFourn) + "' ";

                rsFournGecet = modAdorsFournGecet.fc_OpenRecordSet(sqlSelect, null, "", ModParametre.adoGecet);

                if (rsFournGecet.Rows.Count > 0)
                {
                    //===> Mondir le 07.06.2021, mouline sans arret
                    ssBCD_Detail.EventManager.AllEventsEnabled = false;
                    //===> Fin Modif Mondir

                    ssBCD_Detail.ActiveRow.Cells["BCD_chrono"].Value = rsFournGecet.Rows[0]["Chrono"] + "";//Références
                    ssBCD_Detail.ActiveRow.Cells["BCD_References"].Value = rsFournGecet.Rows[0]["fourref"] + ""; //Références
                    ssBCD_Detail.ActiveRow.Cells["BCD_Designation"].Value = rsFournGecet.Rows[0]["Libelle"] + "";//Désignation
                    ssBCD_Detail.ActiveRow.Cells["BCD_Quantite"].Value = "1";

                    using (var tmpModAdo = new ModAdo())
                        ssBCD_Detail.ActiveRow.Cells["BCD_Unite"].Value = tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite" + " WHERE QTE_Libelle='" + rsFournGecet.Rows[0]["fouunite"] + "'");

                    ssBCD_Detail.ActiveRow.Cells["QTE_Libelle"].Value = rsFournGecet.Rows[0]["fouunite"] + "";//Unité
                    dblPrix = Convert.ToDouble(General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString())));
                    ssBCD_Detail.ActiveRow.Cells["BCD_PrixHT"].Value = Convert.ToDouble(General.nz(dblPrix, "0")); //Prix HT

                    //===> Mondir le 07.06.2021, mouline sans arret
                    ssBCD_Detail.EventManager.AllEventsEnabled = true;
                    //===> Fin Modif Mondi
                }

                modAdorsFournGecet.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " sub_InsereArtGecet Ref fournisseur : " + sRefFourn);
            }
        }
        private void sub_InsereArtGecet(string sRefFourn)
        {
            DataTable rsFournGecet = default(DataTable);
            ModAdo modAdorsFournGecet = new ModAdo();
            string sqlSelect = null;
            double dblPrix = 0;
            string sUnit = null;

            try
            {
                sqlSelect = "SELECT Dtiprix.Fourref , dtiarti.libelle , ";
                sqlSelect = sqlSelect + " Dtiprix.[Px tarif], Dtiprix.[Prix net achat],";
                sqlSelect = sqlSelect + " Dtiprix.FouUnite, dtiarti.chrono ";
                sqlSelect = sqlSelect + " FROM Dtiprix INNER JOIN dtiarti ON ";
                sqlSelect = sqlSelect + " Dtiprix.Chrono=dtiarti.chrono ";
                sqlSelect = sqlSelect + " WHERE Dtiprix.FourRef='" + sRefFourn + "'";

                rsFournGecet = modAdorsFournGecet.fc_OpenRecordSet(sqlSelect, null, "", ModParametre.adoGecet);

                if (rsFournGecet.Rows.Count > 0)
                {
                    ssBCD_Detail.ActiveRow.Cells["BCD_chrono"].Value = rsFournGecet.Rows[0]["Chrono"] + "";
                    //Références
                    ssBCD_Detail.ActiveRow.Cells["BCD_References"].Value = rsFournGecet.Rows[0]["fourref"] + "";
                    //Références
                    ssBCD_Detail.ActiveRow.Cells["BCD_Designation"].Value = rsFournGecet.Rows[0]["Libelle"] + "";
                    //Désignation
                    ssBCD_Detail.ActiveRow.Cells["BCD_Quantite"].Value = "1";

                    using (var tmpModAdo = new ModAdo())
                        ssBCD_Detail.ActiveRow.Cells["BCD_Unite"].Value = tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite" + " WHERE QTE_Libelle='" + rsFournGecet.Rows[0]["fouunite"] + "'");

                    ssBCD_Detail.ActiveRow.Cells["QTE_Libelle"].Value = rsFournGecet.Rows[0]["fouunite"] + "";
                    //Unité
                    dblPrix = Convert.ToDouble(General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString())));
                    ssBCD_Detail.ActiveRow.Cells["BCD_PrixHT"].Value = Convert.ToDouble(General.nz(dblPrix, "0"));
                    //Prix HT
                }

                modAdorsFournGecet.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " sub_InsereArtGecet Ref fournisseur : " + sRefFourn);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_AfterRowsDeleted(object sender, EventArgs e)
        {
            try
            {
                if (bBonComplet == false)
                {
                    modAdorsBCD_Detail.Update();
                }
                fc_TotEnLigne(rsBCD_Detail);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBCD_Detail_AfterRowDeleted");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_AfterRowUpdate(object sender, RowEventArgs e)
        {
            try
            {
                if (bBonComplet == false)
                {
                    modAdorsBCD_Detail.Update();
                }

                fc_TotEnLigne(rsBCD_Detail);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBCD_Detail_AfterRowUpdate");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            string sUnit = null;
            try
            {
                var _with13 = ssBCD_Detail;
                //=================== Colonne des unités.
                if (_with13.ActiveCell.Column.Key.ToUpper() == "QTE_Libelle".ToString())
                {
                    if (string.IsNullOrEmpty(ssBCD_Detail.ActiveRow.Cells["QTE_Libelle"].Text))
                    {
                        _with13.ActiveRow.Cells["BCD_UNITE"].Value = 0;
                    }
                    else if (General.UCase(_with13.ActiveCell.OriginalValue) != ssBCD_Detail.ActiveRow.Cells["QTE_Libelle"].Text)
                    {
                        //== controle si l'unite saisie est correcte, si oui recherche son ID
                        using (var tmpModAdo = new ModAdo())
                            sUnit = tmpModAdo.fc_ADOlibelle("SELECT QTE_NO " + " FROM QTE_Unite WHERE QTE_Libelle ='" + StdSQLchaine.gFr_DoublerQuote(_with13.ActiveRow.Cells["QTE_Libelle"].Text) + "'");

                        if (string.IsNullOrEmpty(sUnit))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("cette unité n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                        _with13.ActiveRow.Cells["BCD_UNITE"].Value = General.nz(sUnit, 0);
                    }

                }
                //================== Calcul du total ht (qte * prix ht.)

                if (_with13.ActiveCell.Column.Key.ToUpper() == "BCD_Quantite".ToUpper())//tested
                {
                    if (!General.IsNumeric(General.nz((_with13.ActiveRow.Cells["BCD_Quantite"].Text), 0).ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Quantité non numérique", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }

                }

                if (_with13.ActiveCell.Column.Key.ToUpper() == "BCD_PrixHT".ToUpper())//tested
                {
                    if (!General.IsNumeric(General.nz((_with13.ActiveRow.Cells["BCD_PrixHT"].Text), 0).ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("prix HT non numérique", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }

                }
                if (_with13.ActiveCell.Column.Key.ToUpper() == "BCD_Quantite".ToUpper() || _with13.ActiveCell.Column.Key.ToUpper() == "BCD_PrixHT".ToUpper())//tested
                {
                    _with13.ActiveRow.Cells["BCD_TotHT"].Value = General.FncArrondir(Convert.ToDouble(General.nz((_with13.ActiveRow.Cells["BCD_Quantite"].Text), 0)) *
                        Convert.ToDouble(General.nz((_with13.ActiveRow.Cells["BCD_PrixHT"].Text), 0)), 2);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBCD_Detail_beforeExitEditMode");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtNoBonDeCommande.Text))
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["BCD_Cle"].Text))
                    {
                        e.Row.Cells["BCD_Cle"].Value = txtNoBonDeCommande.Text;
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssBCD_Detail_BeforeRowUpdate");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_KeyPress(object sender, KeyPressEventArgs e)
        {
            object vTemp = null;
            string dblPrix = null;

            //Modif ajout colone Fournisseur OK

            try
            {
                var _with14 = ssBCD_Detail;


                if ((int)e.KeyChar == 13)
                {
                    if (General.sGecetV3 == "1")
                    {
                        boolEntree = true;
                        if (_with14.ActiveCell.Column.Key.ToUpper() == "BCD_References".ToUpper() && bBonComplet == false && !string.IsNullOrEmpty(txtNoBonDeCommande.Text) && bBonLocked == false)
                        {
                            frmGecetV3 frmGecetV3 = new frmGecetV3();
                            frmGecetV3.chkTriPrix.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            frmGecetV3.txtCle.Text = txtNoBonDeCommande.Text;
                            if (_with14.Rows.Count > 0)
                            {
                                if (!_with14.ActiveRow.IsAddRow)//tested
                                {
                                    frmGecetV3.txtNumLigne.Text = Convert.ToString(ssBCD_Detail.ActiveRow.Index);
                                }
                                else
                                {
                                    frmGecetV3.txtNumLigne.Text = Convert.ToString(Convert.ToInt32(ssBCD_Detail.Rows.Count) + 1);
                                }
                            }
                            else
                            {
                                frmGecetV3.txtNumLigne.Text = "0";
                            }
                            frmGecetV3.txtOrigine.Text = Variable.cUserPreCommande2;
                            General.sModeFermeture = "0";
                            //Fermeture sans rien sauvegarder
                            frmGecetV3.ShowDialog();

                            fc_BCD_Detail(0, " Where BCD_CLe ='" + txtNoBonDeCommande.Text + "'");

                        }
                    }
                    else if (General.sGecetV2 == "1")
                    {
                        boolEntree = true;
                        if (_with14.ActiveCell.Column.Key.ToUpper() == "BCD_References".ToUpper() && bBonComplet == false && !string.IsNullOrEmpty(txtNoBonDeCommande.Text) && bBonLocked == false)
                        {
                            frmGecet2 frmGecet2 = new frmGecet2();
                            frmGecet2.chkTriPrix.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            frmGecet2.txtCle.Text = txtNoBonDeCommande.Text;
                            if (_with14.Rows.Count > 0)
                            {
                                if (!_with14.ActiveRow.IsAddRow)//tested
                                {
                                    frmGecet2.txtNumLigne.Text = Convert.ToString(ssBCD_Detail.ActiveRow.Index);
                                }
                                else
                                {
                                    frmGecet2.txtNumLigne.Text = Convert.ToString(Convert.ToInt32(ssBCD_Detail.Rows.Count) + 1);
                                }
                            }
                            else
                            {
                                frmGecet2.txtNumLigne.Text = "0";
                            }
                            frmGecet2.txtOrigine.Text = Variable.cUserPreCommande2;
                            General.sModeFermeture = "0";
                            //Fermeture sans rien sauvegarder
                            frmGecet2.ShowDialog();

                            fc_BCD_Detail(0, " Where BCD_CLe ='" + txtNoBonDeCommande.Text + "'");

                        }
                    }
                    else//tested
                    {
                        boolEntree = true;
                        if (_with14.ActiveCell.Column.Key.ToUpper() == "BCD_References".ToUpper() && bBonComplet == false && !string.IsNullOrEmpty(txtNoBonDeCommande.Text) && bBonLocked == false)
                        {
                            frmGecet frmGecet = new frmGecet();
                            frmGecet.chkTriPrix.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            frmGecet.txtCle.Text = txtNoBonDeCommande.Text;
                            if (_with14.Rows.Count > 0)
                            {
                                if (!_with14.ActiveRow.IsAddRow)//tested
                                {
                                    frmGecet.txtNumLigne.Text = Convert.ToString(ssBCD_Detail.ActiveRow.Index);
                                }
                                else
                                {
                                    frmGecet.txtNumLigne.Text = Convert.ToString(Convert.ToInt32(ssBCD_Detail.Rows.Count) + 1);
                                }
                            }
                            else
                            {
                                frmGecet.txtNumLigne.Text = "0";
                            }
                            frmGecet.txtOrigine.Text = Variable.cUserPreCommande2;
                            General.sModeFermeture = "0";
                            //Fermeture sans rien sauvegarder
                            frmGecet.ShowDialog();

                            fc_BCD_Detail(0, " Where BCD_CLe ='" + txtNoBonDeCommande.Text + "'");

                        }
                    }
                }
                boolEntree = false;
            }
            catch (Exception ex)
            {
                boolEntree = false;
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_KeyPress");
            }
        }

        //fonction initule
        /* private void ChargeEnregistrement(double sNoBCmd)
         {
             int cleAutoFNS = 0;

             using (var tmpModAdo = new ModAdo())
             {
                 cleAutoFNS = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CodeFourn FROM BonDeCommande WHERE NoBonDeCommande=" + sNoBCmd), 0));

                 General.saveInReg("UserBCmdHead", "NoBCmd", Convert.ToString(sNoBCmd));
                 General.saveInReg("UserBCmdHead", "NoInt", tmpModAdo.fc_ADOlibelle("SELECT NoIntervention FROM BonDeCommande WHERE NoBonDeCommande=" + sNoBCmd));
                 General.saveInReg("UserBCmdHead", "NoFNS", tmpModAdo.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE Cleauto=" + cleAutoFNS));
             }

             //    ssBCD_Detail.AllowAddNew = True
             //    ssBCD_Detail.AllowUpdate = True
             LoadEnt(sNoBCmd);
           //  InitGrid0(sNoBCmd);
             //    codeRgt txtG00
             //    txtG00.DataFieldList = "Column 0"
             //    Grid0.Columns(3).DropDownHwnd = txtG00.hWnd
             //    If blnSaveOnUpdate = False Then
             //        Init False, False
             //    End If
             blnSaveUpdate = false;
         }*/

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="nargE"></param>
        private void LoadEnt(double nargE)
        {
            //Dim rs As ADODB.Recordset, sSQl As String
            //sSQl = " SELECT BonDeCommande.Verrou, BonDeCommande.Acheteur ,BonDeCommande.NoIntervention, BonDeCommande.Codeimmeuble, BonDeCommande.DateCommande ,BonDeCommande.CodeImmeuble, " _
            //'       & " BonDeCommande.DateLivraison,BonDeCommande.Ordre, fournisseurarticle.RaisonSocial,fournisseurArticle.fax,FNS_Agences.FNS_fax , TypeEtatCommande.Libelle, " _
            //'       & " BonDeCommande.EtatLivraison,BonDeCommande.EtatFacturation " _
            //'       & " FROM BonDeCommande LEFT JOIN  fournisseurArticle ON BonDeCommande.CodeFourn = fournisseurArticle.cleauto" _
            //'       & " LEFT JOIN FNS_Agences On Bondecommande.codeag = FNS_Agences.FNS_no " _
            //'       & " LEFT Join TypeEtatCommande ON BonDeCommande.CodeE = TypeEtatCommande.Code" _
            //'       & " Where BonDeCommande.NoBonDeCommande =" & nargE
            //Set rs = fc_OpenRecordSet(sSQl, 1)
            //If Not rs.EOF And Not rs.BOF Then
            //    If rs("Verrou") = True Then
            //        cmdSauverVerrou.Visible = True
            //        cmdSupprimerVerrou.Visible = True
            //        cmdSupprimer.Visible = False
            //        CmdSauver.Visible = False
            //        ssBCD_Detail.Enabled = False
            //    End If
            //    'hidfax.Text = nz(nz(rs("FNS_fax"), rs("Fax")), "")
            //    txt00 = nargE
            //    txt01 = nz(rs("Acheteur"), "")
            //    txt02.Caption = nz(rs("NoIntervention"), "")
            //    txt10.Caption = nz(rs("DateCommande"), "")
            //    'txt11 = nz(rs("Libelle"), "")
            //    'RACHID 010802 AJOUT DU NZ
            //    txt12 = nz(rs("DateLivraison"), "")
            //   ' If txt12 = "01/01/1900" Then txt12.Caption = ""
            //    txt20 = nz(rs("Raisonsocial"), "")
            //   ' txt11 = nz(rs("Libelle"), "")
            //    txtCodeImmeuble2 = nz(rs("CodeImmeuble"), "")
            //    txtOrdre.Text = nz(rs("Ordre"), "")
            //    fc_EtatFactLivr rs!EtatLivraison & "", rs!EtatFacturation & ""
            //Else
            //    cmdSauverVerrou.Visible = True
            //    cmdSupprimerVerrou.Visible = True
            //    cmdSupprimer.Visible = False
            //    CmdSauver.Visible = False
            //    ssBCD_Detail.Enabled = False
            //End If
            //    fc_CloseRecordset rs
        }


        //fonction initule
        /* private void InitGrid0(double arg)
         {
             object stemp = null;
             string sTemp2 = null;
             string sSQL = null;
             string sTemp1 = null;
             DataTable rs = default(DataTable);
             ModAdo modAdors = new ModAdo();
             double nTotal = 0;
             double nMontant = 0;
             sSQL = "SELECT BCD_No,BCD_References,BCD_Designation,BCD_PrixHT ,BCD_Quantite,BCD_NoLigne,BCD_Fournisseur,BCD_NoAutoGecet,QTE_Libelle  From BCD_Detail Left Join QTE_Unite On BCD_Detail.BCD_Unite = QTE_Unite.QTE_No ";
             //sSQl = sSQl & " LEFT JOIN BaseGecet.dbo.DtiPrix ON BCD_Detail.BCD_References=BaseGecet.dbo.DtiPrix.FourRef "
             sSQL = sSQL + " WHERE BCD_Cle =" + General.nz(arg, "''") + " ORDER BY BCD_NoLigne";

             nTotal = 0;
             rs = modAdors.fc_OpenRecordSet(sSQL);
             ssBCD_Detail.DataSource = null;

             var ssBCD_DetailSource = new DataTable();
             ssBCD_DetailSource.Columns.Add("BCD_No");
             ssBCD_DetailSource.Columns.Add("BCD_Cle");
             ssBCD_DetailSource.Columns.Add("BCD_References");
             ssBCD_DetailSource.Columns.Add("BCD_Designation");
             ssBCD_DetailSource.Columns.Add("BCD_Quantite");
             ssBCD_DetailSource.Columns.Add("QTE_Libelle");
             ssBCD_DetailSource.Columns.Add("BCD_PrixHT");
             ssBCD_DetailSource.Columns.Add("BCD_TotHT");
             ssBCD_DetailSource.Columns.Add("BCD_Unite");
             ssBCD_DetailSource.Columns.Add("BCD_NoLigne");
             ssBCD_DetailSource.Columns.Add("BCD_Fournisseur");
             ssBCD_DetailSource.Columns.Add("BCD_Chrono");
             ssBCD_DetailSource.Columns.Add("BCD_NoAutoGecet");

             if (rs.Rows.Count == 0)
             {
                 // Init False, False
                 return;
             }

             foreach (DataRow rsRow in rs.Rows)
             {
                 var ssBCD_DetailSourceNewRow = ssBCD_DetailSource.NewRow();
                 if (General.IsNumeric(rsRow["BCD_Quantite"].ToString()))
                 {
                     nMontant = Convert.ToDouble(General.nz(rsRow["BCD_Quantite"], "0")) * Convert.ToDouble(General.nz(Convert.ToDouble(General.nz(rsRow["BCD_PrixHT"], "0")), "0"));
                 }
                 else
                 {
                     nMontant = 0;
                 }
                 if (Convert.ToInt32(rsRow["BCD_PrixHT"]) == 0)
                 {
                     sTemp1 = "";
                     sTemp2 = "";
                 }
                 else
                 {
                     sTemp1 = Convert.ToString(General.nz(rsRow["BCD_PrixHT"], ""));
                     sTemp2 = (General.IsNumeric(rsRow["BCD_Quantite"].ToString()) ? Convert.ToString(nMontant) : "");
                 }

                 ssBCD_DetailSourceNewRow["BCD_Fournisseur"] = General.nz(rsRow["BCD_Fournisseur"], "");
                 ssBCD_DetailSourceNewRow["BCD_References"] = General.nz(rsRow["BCD_References"], "");
                 ssBCD_DetailSourceNewRow["BCD_Designation"] = General.nz(rsRow["BCD_Designation"], "");
                 ssBCD_DetailSourceNewRow["BCD_Quantite"] = General.nz(rsRow["BCD_Quantite"], "");
                 ssBCD_DetailSourceNewRow["QTE_Libelle"] = General.nz(rsRow["QTE_Libelle"], "");
                 ssBCD_DetailSourceNewRow["BCD_PrixHT"] = sTemp1;
                 ssBCD_DetailSourceNewRow["BCD_TotHT"] = sTemp2;
                 ssBCD_DetailSourceNewRow["BCD_No"] = General.nz(rsRow["BCD_No"], "");
                 ssBCD_DetailSourceNewRow["BCD_NoLigne"] = General.nz(rsRow["BCD_NoLigne"], "");
                 ssBCD_DetailSourceNewRow["BCD_NoAutoGecet"] = General.nz(rsRow["BCD_NoAutoGecet"], "");
                 ssBCD_DetailSource.Rows.Add(ssBCD_DetailSourceNewRow);
                 ssBCD_Detail.DataSource = ssBCD_DetailSource;
                 ssBCD_Detail.UpdateData();
                 nTotal = nTotal + Convert.ToDouble(General.nz(nMontant, 0));

                 if (ssBCD_Detail.Rows.Count > tabMontant.Length)
                 {
                     Array.Resize(ref tabMontant, ssBCD_Detail.Rows.Count + 1);
                 }

                 tabMontant[ssBCD_Detail.Rows.Count - 1] = Convert.ToDouble(General.nz((ssBCD_Detail.Rows[ssBCD_Detail.Rows.Count - 1].Cells[6].Value), 0));
             }
             nStockPrice = nTotal.ToString("0.00");
             Label4.Text = nTotal.ToString("0.00");
             ModAdo.fc_CloseRecordset(rs);
         }*/

        private void ssBCD_Detail_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssBCD_Detail.EventManager.AllEventsEnabled = false;
            e.Layout.Bands[0].Columns["BCD_No"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_Cle"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_Unite"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_NoLigne"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_Fournisseur"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_Chrono"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_NoAutoGecet"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_NoAutoGecet"].Hidden = true;
            e.Layout.Bands[0].Columns["BCD_Fournisseur"].MaxLength = 50;
            e.Layout.Bands[0].Columns["BCD_References"].Header.Caption = "References";
            e.Layout.Bands[0].Columns["BCD_References"].MaxLength = 70;
            e.Layout.Bands[0].Columns["BCD_Designation"].Header.Caption = "Designation";
            e.Layout.Bands[0].Columns["BCD_Designation"].MaxLength = 250;
            e.Layout.Bands[0].Columns["BCD_Quantite"].Header.Caption = "Qte";
            e.Layout.Bands[0].Columns["BCD_Quantite"].MaxLength = 50;
            e.Layout.Bands[0].Columns["BCD_PrixHT"].Header.Caption = "PrixHT";
            e.Layout.Bands[0].Columns["BCD_TotHT"].Header.Caption = "TotHT";
            e.Layout.Bands[0].Columns["QTE_Libelle"].Header.Caption = "Unite";

            //===> Mondir le 15.02.2021, commanted to fix https://groupe-dt.mantishub.io/view.php?id=2237
            //===> The with of all columns on the app is auto
            //e.Layout.Bands[0].Columns["BCD_References"].Width = 200;
            //e.Layout.Bands[0].Columns["BCD_Designation"].Width = 380;
            //===> Fin Modif Mondir

            this.ssBCD_Detail.DisplayLayout.Bands[0].Columns["QTE_Libelle"].ValueList = this.DropUnite;
            ssBCD_Detail.EventManager.AllEventsEnabled = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            var _with15 = ssBCD_Detail;

            ////======================> Tested First
            if (bBonComplet == true || bBonLocked == true)
            {
                e.Row.Activation = Activation.NoEdit;
            }
            else
            {
                e.Row.Activation = Activation.AllowEdit;
            }

            ///===================> Tested
            if (!string.IsNullOrEmpty(e.Row.Cells["BCD_Unite"].Text))
            {
                using (var tmpModAdo = new ModAdo())
                    e.Row.Cells["QTE_Libelle"].Value = tmpModAdo.fc_ADOlibelle("select QTE_Libelle FROM Qte_Unite" + " WHERE QTE_NO =" + e.Row.Cells["BCD_Unite"].Value);

            }

            e.Row.Cells["BCD_TotHT"].Value = General.FncArrondir(Convert.ToDouble(General.nz(e.Row.Cells["BCD_Quantite"].Text, 0)) *
                Convert.ToDouble(General.nz(e.Row.Cells["BCD_PrixHT"].Text, 0)), 2);

        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sRefFourn"></param>
        private void sub_PreInsereArtGecet(string sRefFourn)
        {
            DataTable rsFournGecet = new DataTable();
            ModAdo modAdorsFournGecet = new ModAdo();
            string sqlSelect = null;
            double dblPrix = 0;

            try
            {
                sqlSelect = "SELECT Dtiprix.Fourref , dtiarti.libelle , ";
                sqlSelect = sqlSelect + " Dtiprix.[Px tarif], Dtiprix.[Prix net achat],";
                sqlSelect = sqlSelect + " Dtiprix.FouUnite , dtiarti.chrono ";
                sqlSelect = sqlSelect + " FROM Dtiprix INNER JOIN dtiarti ON ";
                sqlSelect = sqlSelect + " Dtiprix.Chrono=dtiarti.chrono ";
                sqlSelect = sqlSelect + " WHERE Dtiprix.Fourref='" + sRefFourn + "'";


                if (ModParametre.adoGecet == null)
                {
                    ModParametre.adoGecet = new System.Data.SqlClient.SqlConnection(ModParametre.cODBCGecet);
                    ModParametre.adoGecet.Open();
                }
                else if (ModParametre.adoGecet.State == ConnectionState.Closed)
                {
                    ModParametre.adoGecet.Open();
                }
                rsFournGecet = modAdorsFournGecet.fc_OpenRecordSet(sqlSelect, null, "", ModParametre.adoGecet);

                if (rsFournGecet.Rows.Count > 0)
                {
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Chrono"].Value = rsFournGecet.Rows[0]["Chrono"] + "";
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_References"].Value = rsFournGecet.Rows[0]["fourref"] + "";
                    //Références
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Designation"].Value = rsFournGecet.Rows[0]["Libelle"] + "";
                    //Désignation
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Quantite"].Value = "1";
                    using (var tmpModAdo = new ModAdo())
                        ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Unite"].Value = tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite WHERE QTE_Libelle='" + rsFournGecet.Rows[0]["fouunite"] + "'");
                    string frmN = General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString()));
                    if (!string.IsNullOrEmpty(frmN) && General.IsNumeric(frmN))
                    {
                        dblPrix = Convert.ToDouble(General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString())));
                    }
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_PrixHT"].Value = Convert.ToDouble(General.nz(dblPrix, "0"));
                    //Prix HT
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Totht"].Value = General.FncArrondir(Convert.ToDouble(General.nz((ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Quantite"].Text), 0)) * Convert.ToDouble(General.nz((ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_prixHT"].Text), 0)), 2);
                }

                modAdorsFournGecet.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " sub_InsereArtGecet Ref fournisseur : " + sRefFourn);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_AfterRowsDeleted(object sender, EventArgs e)
        {
            try
            {
                modAdorsPRC.Update();
                fc_LoadArt(rsPRC);
                fc_PreTotEnLigne(rsPRC);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_AfterRowsDeleted");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rs"></param>
        private void fc_PreTotEnLigne(DataTable rs)
        {
            double dtHT = 0;

            if (rs == null)
            {
                return;
            }
            dtHT = 0;
            foreach (DataRow rsRow in rs.Rows)
            {
                dtHT = dtHT + Convert.ToDouble(General.nz(rsRow["PRC_Quantite"], 0)) * Convert.ToDouble(General.nz(rsRow["PRC_PrixHT"], 0));
            }
            lblPreTotHT.Text = Convert.ToString(dtHT);

        }
        /// <summary>
        /// testeed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_AfterRowUpdate(object sender, RowEventArgs e)
        {

            try
            {
                modAdorsPRC.Update();

                fc_PreTotEnLigne(rsPRC);

                /// ---- <<e.Row.IsAddRow>> -- ======> ADDED BY Mohammed to fix a bug: when the user add multiple articles some rows in the grid  if they were checked they changed to uncheked & vice versa because of this if statement. that's why i added this e.Row.IsAddRow condition to check if the current is newRow or old one.
                if (string.IsNullOrEmpty(e.Row.Cells["PRC_FOURNISSEURref"].Text) && e.Row.IsAddRow)
                {
                    ssPRC_PreCmdCorps_ClickCellButton(ssPRC_PreCmdCorps, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_AfterRowsUpdate");
            }
        }

        private void ssPRC_PreCmdCorps_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            int i = 0;
            bool bRemoveTree = false;
            string sFourn = null;
            UltraTreeNode Node = null;
            UltraTreeNode nodeSelect = null;
            bool bNodeSel = false;

            try
            {
                var _with19 = ssPRC_PreCmdCorps;
                //== si PRC_Sel est à 2, celui-ci est en cours de sélection, donc non présent dans le treeview.
                //== si PRC_Sel est à 1, celui-ci est selectionné et present dans le treeview.
                //
                //    If chkDupliquer.value = 1 Then
                //        If nz(.Columns("PRC_SelEnCours").Text, "0") = "0" Then
                //            .Columns("PRC_SelEnCours").value = "1"
                //            fc_DeselectArt .Columns("PRC_Noauto").value, True
                //            ssPRC_PreCmdCorps.Update
                //            ssPRC_PreCmdCorps.Refresh
                //        ElseIf .Columns("PRC_SelEnCours").Text = "1" Then
                //            .Columns("PRC_SelEnCours").value = "0"
                //            fc_DeselectArt .Columns("PRC_Noauto").value
                //            ssPRC_PreCmdCorps.Update
                //            ssPRC_PreCmdCorps.Refresh
                //
                //        End If



                if (_with19.ActiveRow != null)
                {
                    //If .Columns("PRC_Sel").Text = "2" Or .Columns("PRC_Sel").Text = "1" Then
                    if (_with19.ActiveRow.Cells["PRC_Sel"].Text == "1")
                    {

                        bRemoveTree = true;
                        //== retire la sélection.
                        _with19.ActiveRow.Cells["PRC_Sel"].Value = 0;

                        sFourn = _with19.ActiveRow.Cells["PRC_FournisseurRef"].Value.ToString();
                        // .Columns("PRC_FournisseurDT").value = ""
                        //.Columns("PRC_FournisseurRef").value = ""

                        bOnClick = true;
                        ssPRC_PreCmdCorps.UpdateData();
                        bOnClick = false;

                        ssPRC_PreCmdCorps.UpdateData();

                        //                 If .Columns("PRC_Sel").value = "2" Then
                        //                        fc_DeselectArt .Columns("PRC_Noauto").value, True
                        //                 Else
                        fc_DeselectArt((_with19.ActiveRow.Cells["PRC_Noauto"].Value.ToString()));//tested
                        //                 End If

                        //== Met à jour le treeview.
                        if (bRemoveTree == true)
                        {
                            fc_PopulateTreeview(cNiv1 + _with19.ActiveRow.Cells["PRE_NoAuto"].Text, cLibelleNiv1, cNiv2 + sFourn, sFourn, cNiv3 + _with19.ActiveRow.Cells["PRC_Noauto"].Text,
                                 (_with19.ActiveRow.Cells["PRC_Designation"].Text), true);
                        }
                    }
                    else
                    {

                        if (fc_FindSelect((_with19.ActiveRow.Cells["PRC_Chrono"].Value.ToString()), (_with19.ActiveRow.Cells["PRC_NoAuto"].Value.ToString())) == false)
                        {

                            _with19.ActiveRow.Cells["PRC_Sel"].Value = 1;

                            //== si fournisseur vide, on ajoute le fournisseur par défaut DT.
                            if (string.IsNullOrEmpty(_with19.ActiveRow.Cells["PRC_FOURNISSEURref"].Text))
                            {
                                //.Columns("PRC_FOURNISSEUR").value = sDefautFournisseur
                                _with19.ActiveRow.Cells["PRC_FOURNISSEURref"].Value = General.sDefautFournisseur;
                                _with19.ActiveRow.Cells["PRC_FOURNISSEURdt"].Value = General.sDefautFournisseur;
                                using (var tmpModAdo = new ModAdo())
                                    _with19.ActiveRow.Cells["PRC_FournDTCle"].Value = General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" +
                                        General.sDefautFournisseur + "'"), 0);
                            }

                            bOnClick = true;
                            ssPRC_PreCmdCorps.UpdateData();
                            bOnClick = false;
                            ssPRC_PreCmdCorps.Refresh();

                            bNodeSel = false;
                            var TN = FindSelectedNote(TreeView1);
                            if (TN != null)
                            {
                                nodeSelect = TN;
                                bNodeSel = true;
                            }

                            fc_PopulateTreeview(cNiv1 + _with19.ActiveRow.Cells["PRE_NoAuto"].Value.ToString(), cLibelleNiv1, cNiv2 + _with19.ActiveRow.Cells["PRC_FOURNISSEURref"].Value.ToString(),
                                (_with19.ActiveRow.Cells["PRC_FOURNISSEURref"].Value.ToString()), cNiv3 + _with19.ActiveRow.Cells["PRC_NoAuto"].Value.ToString(), (_with19.ActiveRow.Cells["PRC_Designation"].Value.ToString()));
                            if (bNodeSel == true)
                            {
                                TreeView1.EventManager.AllEventsEnabled = false;
                                nodeSelect.Selected = true;
                                TreeView1.EventManager.AllEventsEnabled = true;
                            }
                        }

                    }


                    //    TreeView1.Nodes(1).Selected = True
                    TreeView1.ExpandAll();
                    TreeView1.Focus();
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_BtnClick");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="TV"></param>
        /// <returns></returns>
        private UltraTreeNode FindSelectedNote(UltraTree TV)
        {
            UltraTreeNode TN = null;
            try
            {

                foreach (UltraTreeNode TN_N1 in TV.Nodes)
                {
                    if (TN_N1.Selected == true)
                    {
                        TN = TN_N1;
                        break;
                    }
                    foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                    {
                        if (TN_N2.Selected)
                        {
                            TN = TN_N2;
                            break;
                        }
                        foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                        {
                            if (TN_N3.Selected)
                            {
                                TN = TN_N3;
                                break;
                            }
                        }
                    }
                }
                return TN;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FindSelectNote");
                return TN;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        /// <param name="bModeSelection"></param>
        private void fc_DeselectArt(string sNoAuto, bool bModeSelection = false)
        {
            int i = 0;
            //== attends en parametre le numéro de ligne de l'article présent dans le corps
            //== de la precommande et met à 0 le champs lSel du tableau pour preciser
            //== que cette article ne fait plus partie de la selection.

            try
            {
                if (tArtFourn == null)
                    return;

                for (i = 0; i <= tArtFourn.Length - 1; i++)
                {

                    if (!string.IsNullOrEmpty(tArtFourn[i].sNoAuto) && General.UCase(tArtFourn[i].sNoAuto) == General.UCase(sNoAuto))
                    {

                        tArtFourn[i].lSel = 0;
                        if (bModeSelection == true)
                        {
                            tArtFourn[i].lSelEnCours = 1;
                        }
                        else//tested
                        {
                            tArtFourn[i].lSelEnCours = 0;
                        }
                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DeselectArt");
            }
        }

        private void fc_PopulateTreeview(string scle1, string sLibelleCle1, string scle2, string SlibelleCle2, string sCle3, string sLibelleCle3, bool bRemove = false)
        {
            object v = null;
            UltraTreeNode oNod = null;
            string sKey2 = null;
            string sKey3 = null;
            object V1 = null;
            bool BIn = false;

            try
            {
                scle1 = General.UCase(scle1);
                scle2 = General.UCase(scle2);
                sCle3 = General.UCase(sCle3);

                TreeView1.Visible = false;
                //== permet d'ajouter ou de supprimer certaines clés.============>Tested

                foreach (UltraTreeNode TN_N1 in TreeView1.Nodes)
                {
                    if (General.UCase(TN_N1.Key) == General.UCase(scle2))
                    {
                        sKey2 = TN_N1.Key;
                        // ElseIf UCase(v.Key) = UCase(sCle3) Then
                        //     sKey3 = v.Key
                        oNod = TN_N1;
                        break;
                    }
                    foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)//tested
                    {
                        if (General.UCase(TN_N2.Key) == General.UCase(scle2))
                        {
                            sKey2 = TN_N2.Key;
                            // ElseIf UCase(v.Key) = UCase(sCle3) Then
                            //     sKey3 = v.Key
                            oNod = TN_N2;
                            break;
                        }
                        foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                        {
                            if (General.UCase(TN_N3.Key) == General.UCase(scle2))
                            {
                                sKey2 = TN_N3.Key;
                                // ElseIf UCase(v.Key) = UCase(sCle3) Then
                                //     sKey3 = v.Key
                                oNod = TN_N3;
                                break;
                            }
                        }
                    }
                }

                //== si bRemove est à true on supprime une clé.
                UltraTreeNode TN = null;
                if (bRemove == false)
                {
                    if (string.IsNullOrEmpty(sKey2))//tested
                    {
                        var TNX = FindTreeNode(scle1, TreeView1);
                        if (TNX != null)
                        {
                            TNX.Nodes.Add(scle2, General.nz(SlibelleCle2, "").ToString());
                            TNX.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                        }

                    }
                    BIn = false;
                    TN = FindTreeNode(sCle3, TreeView1);
                    if (TN != null)
                    {
                        BIn = true;
                    }

                    if (BIn == false)//tested
                    {
                        UltraTreeNode node = new UltraTreeNode();
                        var TNS = FindTreeNode(scle2, TreeView1);
                        if (TNS != null)
                        {
                            node = TNS.Nodes.Add(sCle3, General.nz(sLibelleCle3, "").ToString());
                            node.Override.NodeAppearance.Image = Properties.Resources.check_mark_2_16;
                        }

                    }
                }
                else
                {//verifier cette suppression
                    TN = FindTreeNode(sCle3, TreeView1);
                    if (TN != null) TN.Remove();
                    //TreeView1.Nodes.Remove(TN);
                    //if (oNod.Nodes.Count == 0)
                    //{
                    //    oNod.Nodes.Remove(TN);
                    //    //TNS.Remove();

                    //    // TreeView1.Nodes.Remove(TNS);
                    //}
                }
                TreeView1.EventManager.AllEventsEnabled = false;
                TreeView1.Visible = true;
                TreeView1.Nodes[0].Selected = true;
                TreeView1.Nodes[0].ExpandAll();
                TreeView1.Focus();
                TreeView1.EventManager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                TreeView1.Visible = true;
                Erreurs.gFr_debug(ex, this.Name + ";fc_PopulateTreeview;");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="key"></param>
        /// <param name="TV"></param>
        /// <returns></returns>
        private UltraTreeNode FindTreeNode(string key, UltraTree TV)
        {
            UltraTreeNode TN = null;
            try
            {
                foreach (UltraTreeNode TN_N1 in TV.Nodes)
                {
                    if (General.UCase(TN_N1.Key) == General.UCase(key))
                    {
                        TN = TN_N1;
                        break;
                    }
                    foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                    {
                        if (General.UCase(TN_N2.Key) == General.UCase(key))
                        {
                            TN = TN_N2;
                            break;
                        }
                        foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                        {
                            if (General.UCase(TN_N3.Key) == General.UCase(key))
                            {
                                TN = TN_N3;
                                break;
                            }
                        }
                    }
                }
                return TN;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FindTreeNote");
                return TN;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sChrono"></param>
        /// <param name="sNoAuto"></param>
        /// <returns></returns>
        private bool fc_FindSelect(string sChrono, string sNoAuto)
        {
            bool functionReturnValue = false;
            //Dim sFourn      As String
            bool BIn = false;
            int i = 0;
            //== retourne true si l'article a déja été ajouté le panier
            //== excepté pour l'article par defaut.

            //sFourn = ""
            try
            {
                if (tArtFourn == null)
                    return false;
                //== le code chrono 80000 est un code à part, ce code presice
                //== que cet article n'existe pas dans la base gecet.
                if (General.UCase(sChrono) != General.UCase(General.cArticleDefaut))
                {


                    //== contrôle si l'article(codechrono) fait déjà partie de la sélection.
                    for (i = 0; i <= tArtFourn.Length - 1; i++)
                    {

                        if (General.UCase(tArtFourn[i].sChrono) == General.UCase(sChrono))
                        {

                            // sFourn = tArtFourn(i).sFour

                            if (tArtFourn[i].lSel == 1)
                            {

                                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(cMArtDouble, "Question", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                                {
                                    functionReturnValue = true;
                                    return functionReturnValue;
                                }
                                else
                                {
                                    break;
                                }

                            }

                        }

                    }
                }
                //== Recherche l'article dans le tableau si pour mettre à jour le champs lSel du tableau
                //== pour indiquer que ce champs est sélectionné.


                for (i = 0; i <= tArtFourn.Length - 1; i++)
                {
                    if (!string.IsNullOrEmpty(tArtFourn[i].sNoAuto) && General.UCase(tArtFourn[i].sNoAuto) == General.UCase(sNoAuto))
                    {
                        tArtFourn[i].lSel = 1;
                        tArtFourn[i].lSelEnCours = 1;
                        break;
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_FindSelect");
                return false;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            try
            {
                var _with17 = ssPRC_PreCmdCorps;
                if (ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_Quantite".ToUpper())//tested
                {
                    if (!General.IsNumeric(General.nz((_with17.ActiveRow.Cells["PRC_Quantite"].Text), 0).ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Quantité non numérique", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }

                }

                if (ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_prixHT".ToUpper())
                {
                    if (!General.IsNumeric(General.nz((_with17.ActiveRow.Cells["PRC_prixHT"].Text), 0).ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("prix HT non numérique", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }

                }
                if (ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_Quantite".ToUpper() || ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_prixHT".ToUpper())//tested
                {

                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Totht"].Value = General.FncArrondir(Convert.ToDouble(General.nz((ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Quantite"].Text), 0)) *
                        Convert.ToDouble(General.nz((_with17.ActiveRow.Cells["PRC_prixHT"].Text), 0)), 2);

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_BeforeExitEditMode");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            string sFournEncours = null;
            UltraTreeNode Node = null;
            UltraTreeNode nodeParent = null;

            try
            {
                var _with18 = ssPRC_PreCmdCorps;
                //== met a jour le numero de la precommande
                if (e.Row == null)
                    return;
                if (string.IsNullOrEmpty(e.Row.Cells["PRE_NoAuto"].Text))//tested
                {
                    e.Row.Cells["PRE_NoAuto"].Value = txtPRE_NoAuto.Text;
                }

                //== met le code chrono par defaut.
                if (string.IsNullOrEmpty(e.Row.Cells["PRC_chrono"].Text))//tested
                {
                    e.Row.Cells["PRC_chrono"].Value = General.cArticleDefaut;
                }


                if (string.IsNullOrEmpty(e.Row.Cells["PRC_FOURNISSEURref"].Text))
                {///==================================================================================>Tested

                    //== controle si l'utilsateur est positionné sur un noeud du treeview
                    //== correspondant au fournisseur.
                    sFournEncours = "";
                    foreach (UltraTreeNode TN_N1 in TreeView1.Nodes)
                    {
                        foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                        {
                            if (TN_N2.Selected == true)
                            {
                                if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv2))
                                {
                                    sFournEncours = TN_N2.Text;
                                }
                                else if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv3))
                                {
                                    nodeParent = TN_N2.Parent;
                                    sFournEncours = TN_N2.Text;
                                }
                                break;
                            }
                            foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                            {
                                if (TN_N3.Selected == true)
                                {
                                    if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv2))
                                    {
                                        sFournEncours = TN_N3.Text;
                                    }
                                    else if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv3))
                                    {
                                        nodeParent = TN_N3.Parent;
                                        sFournEncours = nodeParent.Text;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    if (string.IsNullOrEmpty(sFournEncours))//tested
                    {
                        e.Row.Cells["PRC_FOURNISSEURref"].Value = General.sDefautFournisseur;
                        e.Row.Cells["PRC_FOURNISSEURdt"].Value = General.sDefautFournisseur;
                        //  e.Row.Cells["PRC_FOURNISSEURref"].Value= General.sDefautFournisseur;
                        //  e.Row.Cells["PRC_FOURNISSEURdt"].Value = General.sDefautFournisseur;
                        using (var tmpModAdo = new ModAdo())
                            e.Row.Cells["PRC_FournDTCle"].Value = General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" +
                                General.sDefautFournisseur + "'"), 0);
                    }
                    else//TESTED
                    {
                        e.Row.Cells["PRC_FOURNISSEURref"].Value = sFournEncours;
                        using (var tmpModAdo = new ModAdo())
                            e.Row.Cells["PRC_FournDTCle"].Value = General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" + sFournEncours + "'"), 0);
                        if (!string.IsNullOrEmpty(e.Row.Cells["PRC_FournDTCle"].Text))
                        {
                            e.Row.Cells["PRC_FOURNISSEURdt"].Value = sFournEncours;
                        }

                    }


                }
                if (e.Row.Cells["PRC_Sel"].Text != "1" && bOnClick == false)//tested
                {
                    // .Columns("PRC_Sel").Text = "1"
                    ssPRC_PreCmdCorps_ClickCellButton(ssPRC_PreCmdCorps, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_BeforeRowUpdate");
            }
        }

        private void ssPRC_PreCmdCorps_KeyPress(object sender, KeyPressEventArgs e)
        {
            string sFournEncours = "";
            UltraTreeNode Node = null;
            UltraTreeNode nodeParent = null;
            string skey = "";

            try
            {
                if ((int)e.KeyChar == 13)
                {
                    if (ssPRC_PreCmdCorps.ActiveCell == null)
                        return;
                    if (ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_References".ToUpper())
                    {
                        if (General.fncUserName() == "mohammed")
                        {
                            General.sGecetV3 = "1";
                            General.sGecetV2 = "1";
                        }
                        if (General.sGecetV3 == "1")
                        {
                            General.tArtGecetPre = null;
                            frmGecetV3 frmGecetV3 = new frmGecetV3();
                            frmGecetV3.chkTriPrix.Checked = false;

                            frmGecetV3.txtCle.Text = txtPRE_NoAuto.Text;

                            frmGecetV3.txtOrigine.Text = Variable.cUserPreCommande;

                            General.sModeFermeture = "0"; //'Fermeture sans rien sauvegarder

                            //== controle si l'utilsateur est positionn� sur un noeud du treeview
                            //== correspondant au fournisseur.
                            sFournEncours = "";

                            foreach (UltraTreeNode TN_N1 in TreeView1.Nodes)
                            {
                                if (TN_N1.Selected == true)
                                {
                                    foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                                    {
                                        if (TN_N2.Selected == true)
                                        {
                                            if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv2))
                                            {
                                                sFournEncours = TN_N2.Text;
                                                skey = TN_N2.Key;
                                            }
                                            else if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv3))
                                            {
                                                nodeParent = TN_N2.Parent;
                                                sFournEncours = TN_N2.Text;
                                                skey = TN_N2.Key;
                                            }
                                            break;
                                        }
                                        foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                        {
                                            if (TN_N3.Selected == true)
                                            {
                                                if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv2))
                                                {
                                                    sFournEncours = TN_N3.Text;
                                                    skey = TN_N3.Key;
                                                }
                                                else if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv3))
                                                {
                                                    nodeParent = TN_N3.Parent;
                                                    sFournEncours = TN_N3.Text;
                                                    skey = TN_N3.Key;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            frmGecetV3.ShowDialog();

                            fc_addArtGecet2(sFournEncours, General.sGecetV3);

                            var TN = FindTreeNode(skey, TreeView1);
                            if (TN != null)
                                TN.Selected = true;
                            TreeView1.ExpandAll();
                            TreeView1.Focus();

                        }
                        else if (General.sGecetV2 == "1")///==================>Tested
                        {
                            General.tArtGecetPre = null;
                            frmGecet2 frmGecet2 = new frmGecet2();
                            frmGecet2.chkTriPrix.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            frmGecet2.txtCle.Text = txtPRE_NoAuto.Text;
                            frmGecet2.txtOrigine.Text = Variable.cUserPreCommande;
                            General.sModeFermeture = "0";
                            //Fermeture sans rien sauvegarder

                            //== controle si l'utilsateur est positionné sur un noeud du treeview
                            //== correspondant au fournisseur.
                            sFournEncours = "";
                            foreach (UltraTreeNode TN_N1 in TreeView1.Nodes)
                            {
                                if (TN_N1.Selected == true)
                                {
                                    foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                                    {
                                        if (TN_N2.Selected == true)
                                        {
                                            if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv2))
                                            {
                                                sFournEncours = TN_N2.Text;
                                                skey = TN_N2.Key;
                                            }
                                            else if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv3))
                                            {
                                                nodeParent = TN_N2.Parent;
                                                sFournEncours = TN_N2.Text;
                                                skey = TN_N2.Key;
                                            }
                                            break;
                                        }
                                        foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                        {
                                            if (TN_N3.Selected == true)
                                            {
                                                if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv2))
                                                {
                                                    sFournEncours = TN_N3.Text;
                                                    skey = TN_N3.Key;
                                                }
                                                else if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv3))
                                                {
                                                    nodeParent = TN_N3.Parent;
                                                    sFournEncours = TN_N3.Text;
                                                    skey = TN_N3.Key;
                                                }
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            frmGecet2.ShowDialog();

                            fc_addArtGecet2(sFournEncours);
                            frmGecet2.Close();

                            var TN = FindTreeNode(skey, TreeView1);
                            if (TN != null)
                                TN.Selected = true;

                            TreeView1.ExpandAll();
                            TreeView1.Focus();
                        }
                        else//tested
                        {

                            General.tArtGecetPre = null;
                            frmGecet frmGecet = new frmGecet();
                            frmGecet.chkTriPrix.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            frmGecet.txtCle.Text = txtPRE_NoAuto.Text;
                            frmGecet.txtOrigine.Text = Variable.cUserPreCommande;
                            General.sModeFermeture = "0";
                            //Fermeture sans rien sauvegarder

                            //== controle si l'utilsateur est positionné sur un noeud du treeview
                            //== correspondant au fournisseur.
                            sFournEncours = "";
                            foreach (UltraTreeNode TN_N1 in TreeView1.Nodes)
                            {
                                foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                                {
                                    if (TN_N2.Selected == true)
                                    {
                                        if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv2))
                                        {
                                            sFournEncours = TN_N2.Text;
                                            skey = TN_N2.Key;
                                        }
                                        else if (General.UCase(General.Left(TN_N2.Key, 1)) == General.UCase(cNiv3))
                                        {
                                            nodeParent = TN_N2.Parent;
                                            sFournEncours = TN_N2.Text;
                                            skey = TN_N2.Key;
                                        }
                                        break;
                                    }
                                    foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                    {
                                        if (TN_N3.Selected == true)
                                        {
                                            if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv2))
                                            {
                                                sFournEncours = TN_N3.Text;
                                                skey = TN_N3.Key;
                                            }
                                            else if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv3))
                                            {
                                                nodeParent = TN_N3.Parent;
                                                sFournEncours = TN_N3.Text;
                                                skey = TN_N3.Key;
                                            }
                                            break;
                                        }
                                    }
                                }
                            }

                            frmGecet.ShowDialog();

                            fc_addArtGecet(sFournEncours);
                            frmGecet.Close();

                            var TN = FindTreeNode(skey, TreeView1);
                            if (TN != null)
                                TN.Selected = true;
                            TreeView1.ExpandAll();
                            TreeView1.Focus();
                        }

                    }
                    //===================================> Mondir : Deleted By mondir, All Lines Are Commented
                    //else if (ssPRC_PreCmdCorps.Col == ssPRC_PreCmdCorps.Columns["PRC_FournisseurRef"].Position)
                    //{
                    //    //            With RechercheMultiCritere
                    //    //                 RechercheMultiCritere.Width = 12950
                    //    //                 .stxt = "SELECT  Code as [Code], CleAuto as [CleAuto], Raisonsocial as [Raison Sociale], Ville as [Ville]" _
                    //    //'                 & " FROM fournisseurArticle"
                    //    //                 .Caption = "Attacher un fournisseur à votre bon de commande"
                    //    //
                    //    //                 .Champs0.Text = ssPRC_PreCmdCorps.Columns("PRC_FournisseurRef").Text
                    //    //                 '.dbgrid1.Columns("CleAuto").Visible = False
                    //    //                 .Show vbModal
                    //    //                 If nz(.LigneChoisie, "") <> "" Then
                    //    //
                    //    //                      ssPRC_PreCmdCorps.Columns("PRC_FournisseurRef").Text = .dbgrid1.Columns(0).value
                    //    //                      ssPRC_PreCmdCorps.Columns("PRC_FournisseurDT").Text = .dbgrid1.Columns(0).value
                    //    //                      ssPRC_PreCmdCorps.Columns("PRC_FournDTCle").Text = .dbgrid1.Columns(1).value
                    //    //                      ssPRC_PreCmdCorps.Columns("PRC_Sel").Text = "0"
                    //    //                      ssPRC_PreCmdCorps.Update
                    //    //                 '     ssPRC_PreCmdCorps_BtnClick
                    //    //
                    //    //                 End If
                    //    //                 Unload RechercheMultiCritere
                    //    //            End With
                    //}
                }
                else
                {
                    bAscii = true;

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_KeyPress");
            }
        }

        private void fc_addArtGecet(string sFournisseur)
        {
            //== insére les articles selectionnés dans la fiche Gecet.
            int i = 0;
            DataTable rsDtiPrix = default(DataTable);
            ModAdo modAdorsDtiPrix = new ModAdo();
            DataTable rsFindFourn = default(DataTable);
            ModAdo modAdorsFindFourn = new ModAdo();
            int lPRC_FournDTCle = 0;


            try
            {
                if (General.tArtGecetPre == null)
                    return;
                for (i = 0; i <= General.tArtGecetPre.Length - 1; i++)
                {
                    rsDtiPrix = modAdorsDtiPrix.fc_OpenRecordSet("SELECT DtiArti.Chrono,Dtiarti.Libelle," + " Dtiprix.Fournisseur,Dtiprix.Fourref,Dtiprix.Fouunite,Dtiprix.[prix net achat]" +
                        " , fournisseur, fabref " + " FROM Dtiarti INNER JOIN Dtiprix ON DtiArti.Chrono=Dtiprix.Chrono" + " WHERE Dtiprix.NoAuto=" +
                        General.tArtGecetPre[i].sNoAuto, null, "", ModParametre.adoGecet);

                    if (rsDtiPrix.Rows.Count > 0)
                    {
                        var _with21 = ssPRC_PreCmdCorps;
                        var NewRow = _with21.DisplayLayout.Bands[0].AddNew();

                        NewRow.Cells["PRC_Sel"].Value = 1;
                        NewRow.Cells["PRC_Chrono"].Value = rsDtiPrix.Rows[0]["Chrono"];
                        NewRow.Cells["PRC_References"].Value = General.nz(rsDtiPrix.Rows[0]["fourref"], "");
                        //.Columns(PRC_NoLigne").value =

                        using (var tmpModAdo = new ModAdo())
                            NewRow.Cells["PRC_Unite"].Value = tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite WHERE QTE_Libelle='" + rsDtiPrix.Rows[0]["fouunite"] + "'");

                        if (!string.IsNullOrEmpty(sFournisseur))
                        {
                            modAdorsFindFourn = new ModAdo();
                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sFournisseur) + "'");
                            if (rsFindFourn.Rows.Count > 0)
                            {
                                NewRow.Cells["PRC_FournisseurDT"].Value = sFournisseur;
                                NewRow.Cells["PRC_FournisseurRef"].Value = sFournisseur;
                                NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];
                            }
                            else
                            {
                                NewRow.Cells["PRC_FournisseurRef"].Value = sFournisseur;
                            }
                        }
                        else if (string.IsNullOrEmpty(General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "").ToString()) ||
                            General.UCase(General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "")) == General.UCase(cSansFournisseur))
                        {
                            modAdorsFindFourn = new ModAdo();
                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE Code = '" +
                                StdSQLchaine.gFr_DoublerQuote(General.sDefautFournisseur) + "'");
                            NewRow.Cells["PRC_FournisseurDT"].Value = General.sDefautFournisseur;
                            NewRow.Cells["PRC_FournisseurRef"].Value = General.sDefautFournisseur;
                            NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];

                        }
                        else//tested
                        {
                            NewRow.Cells["PRC_Fournisseur"].Value = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "");

                            //== recherche si le fournisseur gecet a une correspondance
                            //== avec le fournisseur de la base de prod.
                            modAdorsFindFourn = new ModAdo();
                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE CodeFournGecet = '" +
                                StdSQLchaine.gFr_DoublerQuote(rsDtiPrix.Rows[0]["Fournisseur"].ToString()) + "'");
                            if (rsFindFourn.Rows.Count > 0)//tested
                            {
                                NewRow.Cells["PRC_FournisseurDT"].Value = General.nz(rsFindFourn.Rows[0]["Code"], "");
                                NewRow.Cells["PRC_FournisseurRef"].Value = General.nz(rsFindFourn.Rows[0]["Code"], "");
                                NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];
                            }
                            else
                            {
                                NewRow.Cells["PRC_FournisseurRef"].Value = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "");
                            }

                        }

                        //.Columns("PRC_FournisseurRef").value = .Columns("PRC_Fournisseur").Text
                        NewRow.Cells["NoAutoGecet"].Value = General.tArtGecetPre[i].sNoAuto;
                        NewRow.Cells["PRC_fabref"].Value = General.nz(rsDtiPrix.Rows[0]["fabref"], "");
                        NewRow.Cells["PRC_fourref"].Value = General.nz(rsDtiPrix.Rows[0]["fourref"], "");
                        NewRow.Cells["PRC_Designation"].Value = General.nz(rsDtiPrix.Rows[0]["Libelle"], "");
                        NewRow.Cells["PRC_Quantite"].Value = General.tArtGecetPre[i].Qte;
                        NewRow.Cells["PRC_PrixHt"].Value = General.fc_FormatNumber(General.nz((rsDtiPrix.Rows[0]["prix net achat"]), 0).ToString());
                        NewRow.Cells["PRC_TotHT"].Value = General.FncArrondir(Convert.ToDouble(General.nz((NewRow.Cells["PRC_PrixHt"].Value), 0)) *
                            Convert.ToDouble(General.nz(General.tArtGecetPre[i].Qte, 0)), 2);
                        //_with21.UpdateData();

                        //=========> changed by mohammed to fix an error.12/06/2020
                        NewRow.Update();
                        // fc_PopulateTreeview cNiv1 & .Columns("PRE_NoAuto").Text, cLibelleNiv1, cNiv2 & .Columns("PRC_FOURNISSEUR").Text, .Columns("PRC_FOURNISSEUR").Text, _
                        //cNiv3 & .Columns("PRC_Noauto").Text, .Columns("PRC_Designation").Text
                    }
                }

                fc_Loadtreeview(txtPRE_NoAuto.Text);

                modAdorsDtiPrix.Close();
                rsDtiPrix = null;
                modAdorsFindFourn.Close();
                rsFindFourn = null;
                General.tArtGecetPre = null;
                fc_LoadArt(rsPRC);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_AddArtGecet");
            }
        }

        /// <summary>
        ///
        /// Modif V26.06.2020 Ajouté par Mondir le 27.06.2020
        /// Modif V26.06.2020 testée par Mondir le 29.06.2020
        /// </summary>
        /// <param name="sFournisseur"></param>
        /// <param name="sV3Gecet"></param>
        private void fc_addArtGecet2(string sFournisseur, string sV3Gecet = "")
        {
            //== insére les articles selectionnés dans la fiche Gecet.
            int i = 0;
            DataTable rsDtiPrix = default(DataTable);
            ModAdo modAdorsDtiPrix = null;
            DataTable rsFindFourn = default(DataTable);
            ModAdo modAdorsFindFourn = null;
            int lPRC_FournDTCle = 0;

            //Modfi Mondir le 27.06.2020 pour ajouter les modfis de Rachid de la version V26.06.2020
            string sFournBD2 = "";
            //Fin Modif Mondir

            try
            {
                if (General.tArtGecetPre == null)
                {
                    if (ssPRC_PreCmdCorps.ActiveRow != null)
                        ssPRC_PreCmdCorps.ActiveRow.CancelUpdate();
                    return;
                }

                for (i = 0; i <= General.tArtGecetPre.Length - 1; i++)
                {
                    // rsDtiPrix.Open "SELECT DtiArti.Chrono,Dtiarti.Libelle," _
                    //& " Dtiprix.Fournisseur, Dtiprix.Fourref ,Dtiprix.Fouunite ,Dtiprix.[prix net achat]" _
                    //& " , fournisseur, fabref " _
                    //& " FROM Dtiarti INNER JOIN Dtiprix ON DtiArti.Chrono=Dtiprix.Chrono" _
                    //& " WHERE Dtiprix.NoAuto=" & tArtGecetPre(i).sNoAuto, adoGecet


                    //rsDtiPrix.Open "SELECT     ART_Article.ART_Chrono as Chrono, ART_Article.ART_Libelle AS libelle," _
                    //& " ARTP_ArticlePrix.ARTP_Nofourn AS Fournisseur, " _
                    //& " ARTP_ArticlePrix.ARTP_RefFourn AS Fourref, ARTP_ArticlePrix.ARTP_UniteFourn AS Fouunite," _
                    //& " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], " _
                    //& " ARTP_RefFab as fabref " _
                    //& " FROM         ARTP_ArticlePrix INNER JOIN" _
                    //& " ART_Article ON ARTP_ArticlePrix.ART_Chrono = ART_Article.ART_Chrono" _
                    //& " WHERE ARTP_ArticlePrix.ARTP_NoAuto =" & tArtGecetPre(i).sNoAuto, adoGecet
                    modAdorsDtiPrix = new ModAdo();

                    if (sV3Gecet != "")
                    {
                        //=== modif du 17 07 2019, ajout de la BDD2.
                        if (General.tArtGecetPre[i].lidBDD2 != 0)
                        {
                            //rsDtiPrix = modAdorsDtiPrix.fc_OpenRecordSet("SELECT " + General.cArt80000 + " as Chrono,  LIBELLE_ARTICLE AS Libelle, "

                            //        + " FOURNISSEUR AS fournisseur,"

                            //        + " REF_FOURNISSEUR AS fourref, UNITE AS fouunite, "

                            //        + " PRIX_GECET AS [prix net achat], "

                            //        + " REF_FOURNISSEUR AS fabref "

                            //        + " From BDD2 "

                            //        + " WHERE    IDGEC_DONNEES_NON_QUALIFIEES = " + General.tArtGecetPre[i].lidBDD2, null, "", ModParametre.adoGecet);
                            rsDtiPrix = modAdorsDtiPrix.fc_OpenRecordSet("SELECT IDGEC_DONNEES_NON_QUALIFIEES ,  LIBELLE_ARTICLE AS Libelle,"

                              + " FOURNISSEUR AS fournisseur,"

                              + " REF_FOURNISSEUR AS fourref, UNITE AS fouunite,  "

                              + " PRIX_GECET AS [prix net achat], "

                              //==========> Old before 26.06.2020 + " REF_FOURNISSEUR AS fabref "
                              //Modfi Mondir le 27.06.2020 pour ajouter les modfis de Rachid de la version V26.06.2020
                              + " REF_FOURNISSEUR AS fabref, CODE_FOURNISSEUR_ADHERENT "
                          //Fin Modif Mondir

                          + " From BDD2 "

                              + " WHERE    IDGEC_DONNEES_NON_QUALIFIEES = " + General.tArtGecetPre[i].lidBDD2, null, "", ModParametre.adoGecet);
                        }
                        else
                        {
                            //==== modif du 17 11 2014, changmenet de l'unité devis.
                            rsDtiPrix = modAdorsDtiPrix.fc_OpenRecordSet("SELECT     Article.CHRONO_INITIAL AS Chrono, Article.DESIGNATION_ARTICLE AS Libelle,"

                                    + " ArticleTarif.NOM_FOURNISSEUR AS fournisseur, "

                                    + " ArticleTarif.REF_FOURNISSEUR AS fourref, ArticleTarif.CODE_UNITE_DEVIS AS fouunite,  "

                                    + " ArticleTarif.PRX_DEVIS_NET_UD AS [prix net achat], "

                                    + " ArticleTarif.REF_FABRICANT As fabref "

                                    + " FROM         Article INNER JOIN "

                                    + " ArticleTarif ON Article.CHRONO_INITIAL = ArticleTarif.CHRONO_INITIAL"

                                    + " WHERE ArticleTarif.IDGEC_LIGNE_PRIX =" + General.tArtGecetPre[i].sNoAuto, null, "", ModParametre.adoGecet);
                        }
                    }
                    else
                    {
                        //==== modif du 17 11 2014, changmenet de l'unité devis.
                        rsDtiPrix = modAdorsDtiPrix.fc_OpenRecordSet("SELECT     ART_Article.ART_Chrono as Chrono, ART_Article.ART_Libelle AS libelle," +
                            " ARTP_ArticlePrix.ARTP_Nofourn AS Fournisseur, " + " ARTP_ArticlePrix.ARTP_RefFourn AS Fourref, ARTP_ArticlePrix.ARTP_UniteDevis AS Fouunite," +
                            " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], " + " ARTP_RefFab as fabref " + " FROM         ARTP_ArticlePrix INNER JOIN" +
                            " ART_Article ON ARTP_ArticlePrix.ART_Chrono = ART_Article.ART_Chrono" + " WHERE ARTP_ArticlePrix.ARTP_NoAuto =" + General.tArtGecetPre[i].sNoAuto, null, "", ModParametre.adoGecet);

                    }
                    if (rsDtiPrix.Rows.Count > 0)
                    {
                        //ssPRC_PreCmdCorps.EventManager.AllEventsEnabled = false;

                        var _with20 = ssPRC_PreCmdCorps;
                        var NewRow = _with20.DisplayLayout.Bands[0].AddNew();

                        NewRow.Cells["PRC_Sel"].Value = 1;
                        //NewRow.Cells["PRC_Chrono"].Value = rsDtiPrix.Rows[0]["Chrono"];
                        if (General.tArtGecetPre[i].lidBDD2 != 0)
                            NewRow.Cells["PRC_Chrono"].Value = General.cArtiNonQualifie + rsDtiPrix.Rows[0]["IDGEC_DONNEES_NON_QUALIFIEES"];
                        else
                            NewRow.Cells["PRC_Chrono"].Value = rsDtiPrix.Rows[0]["Chrono"];

                        NewRow.Cells["PRC_References"].Value = General.nz(rsDtiPrix.Rows[0]["fourref"], "");
                        //.Columns(PRC_NoLigne").value =
                        //===> Mondir le 28.09.2020 to fix a bug reported by mail, the cell is deleted ..., must not put an empty string
                        using (var tmpModAdoX = new ModAdo())
                        {
                            var val = tmpModAdoX.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite WHERE QTE_Libelle='" + rsDtiPrix.Rows[0]["fouunite"] + "'");
                            if (!string.IsNullOrEmpty(val))
                                NewRow.Cells["PRC_Unite"].Value = Convert.ToInt32(val);
                        }
                        //===> Fin Modif Mondir
                        //NewRow.Cells["PRC_Unite"].Value = 1;

                        if (!string.IsNullOrEmpty(sFournisseur))
                        {
                            modAdorsFindFourn = new ModAdo();
                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(sFournisseur) + "'");
                            if (rsFindFourn.Rows.Count > 0)
                            {
                                NewRow.Cells["PRC_FournisseurDT"].Value = sFournisseur;
                                NewRow.Cells["PRC_FournisseurRef"].Value = sFournisseur;
                                NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];
                            }
                            else
                            {
                                NewRow.Cells["PRC_FournisseurRef"].Value = sFournisseur;
                            }

                        }
                        else if (string.IsNullOrEmpty(General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "").ToString()) ||
                            General.UCase(General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "")) == General.UCase(cSansFournisseur))
                        {
                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(General.sDefautFournisseur) + "'");
                            NewRow.Cells["PRC_FournisseurDT"].Value = General.sDefautFournisseur;
                            NewRow.Cells["PRC_FournisseurRef"].Value = General.sDefautFournisseur;
                            NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];
                        }
                        else
                        {
                            //=====> Old before 26.06.2020 NewRow.Cells["PRC_Fournisseur"].Value = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "");
                            //=====> Modfi Mondir le 27.06.2020 Pour les modfis de la version 26.06.2020
                            if (General.tArtGecetPre[i].lidBDD2 != 0)
                            {
                                if (!string.IsNullOrEmpty(General.nz(rsDtiPrix.Rows[0]["CODE_FOURNISSEUR_ADHERENT"], "")?.ToString()))
                                {
                                    sFournBD2 = General.nz(rsDtiPrix.Rows[0]["CODE_FOURNISSEUR_ADHERENT"], "")
                                        ?.ToString();
                                }
                                else
                                {
                                    sFournBD2 = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "")?.ToString();
                                }
                            }
                            else
                            {
                                sFournBD2 = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "")?.ToString();
                            }

                            NewRow.Cells["PRC_Fournisseur"].Value = sFournBD2;
                            //=====> Fin Modif Mondir

                            //== recherche si le fournisseur gecet a une correspondance
                            //== avec le fournisseur de la base de prod.
                            //=====> Modfi Mondir le 27.06.2020 Pour les modfis de la version 26.06.2020
                            modAdorsFindFourn = new ModAdo();
                            if (General.tArtGecetPre[i].lidBDD2 != 0)
                            {
                                rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet(
                                    "SELECT Code, cleAuto From fournisseurArticle "
                                    + " WHERE Ncompte = '" + StdSQLchaine.gFr_DoublerQuote(sFournBD2) + "'");
                            }
                            ///================> Tested
                            else
                            {
                                rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet(
                                    "SELECT Code, cleAuto From fournisseurArticle "
                                    + " WHERE CodeFournGecet = '" + StdSQLchaine.gFr_DoublerQuote(sFournBD2) + "'");
                            }
                            //=====> Fin Modif Mondir
                            //modAdorsFindFourn = new ModAdo();
                            //rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE CodeFournGecet = '" + StdSQLchaine.gFr_DoublerQuote(rsDtiPrix.Rows[0]["Fournisseur"].ToString()) + "'");
                            if (rsFindFourn.Rows.Count > 0)
                            {
                                NewRow.Cells["PRC_FournisseurDT"].Value = General.nz(rsFindFourn.Rows[0]["Code"], "");
                                NewRow.Cells["PRC_FournisseurRef"].Value = General.nz(rsFindFourn.Rows[0]["Code"], "");
                                NewRow.Cells["PRC_FournDTCle"].Value = rsFindFourn.Rows[0]["cleAuto"];
                            }
                            else
                            {
                                NewRow.Cells["PRC_FournisseurRef"].Value = General.nz(rsDtiPrix.Rows[0]["Fournisseur"], "");
                            }

                        }

                        //.Columns("PRC_FournisseurRef").value = .Columns("PRC_Fournisseur").Text


                        NewRow.Cells["NoAutoGecet"].Value = General.tArtGecetPre[i].sNoAuto;
                        /// ===> Changed by mohammed 26.06.2020 ==> mohammed added these lines to solve this exception "Les données de chaîne ou binaires seront tronquées.L'instruction a été arrêtée.
                        var PRC_fabref = General.nz(rsDtiPrix.Rows[0]["fabref"], "") + "";
                        if (PRC_fabref.Length > 15)
                            PRC_fabref = General.Left(PRC_fabref, 15);
                        var PRC_fourref = General.nz(rsDtiPrix.Rows[0]["fourref"], "") + "";
                        if (PRC_fourref.Length > 15)
                            PRC_fourref = General.Left(PRC_fourref, 15);
                        NewRow.Cells["PRC_fabref"].Value = PRC_fabref;
                        NewRow.Cells["PRC_fourref"].Value = PRC_fourref;
                        ///end mohammed's modifs

                        NewRow.Cells["PRC_Designation"].Value = General.nz(rsDtiPrix.Rows[0]["Libelle"], "");
                        NewRow.Cells["PRC_Quantite"].Value = General.tArtGecetPre[i].Qte;
                        ///============> Mondir : I Added  this Replace(".",",") coz the grid dont accepte . as float it is a problem of cultures
                        NewRow.Cells["PRC_PrixHt"].Value = General.fc_FormatNumber(General.nz((rsDtiPrix.Rows[0]["prix net achat"]), 0).ToString())/*.Replace(".",",")*/;
                        NewRow.Cells["PRC_TotHT"].Value = General.FncArrondir(Convert.ToDouble(General.nz((NewRow.Cells["PRC_PrixHt"].Value), 0).ToString()) * Convert.ToDouble(General.nz(General.tArtGecetPre[i].Qte, 0)), 2);

                        ///Changed by mohammed 26.06.2020
                        NewRow.Update();
                        ssPRC_PreCmdCorps.ActiveRow = NewRow;
                        ssPRC_PreCmdCorps.UpdateData();
                        if (ssPRC_PreCmdCorps.ActiveRow != null)
                            ssPRC_PreCmdCorps.ActiveRow.Update();
                        //modAdorsPRC.Update();

                        //ssPRC_PreCmdCorps.EventManager.AllEventsEnabled = true;

                        // fc_PopulateTreeview cNiv1 & .Columns("PRE_NoAuto").Text, cLibelleNiv1, cNiv2 & .Columns("PRC_FOURNISSEUR").Text, .Columns("PRC_FOURNISSEUR").Text, _
                        //cNiv3 & .Columns("PRC_Noauto").Text, .Columns("PRC_Designation").Text
                    }
                }

                //ssPRC_PreCmdCorps.Rows.Refresh(RefreshRow.FireInitializeRow);

                fc_Loadtreeview(txtPRE_NoAuto.Text);

                modAdorsDtiPrix.Close();
                modAdorsFindFourn.Close();
                General.tArtGecetPre = null;
                fc_LoadArt(rsPRC);
            }
            catch (Exception ex)
            {
                ssPRC_PreCmdCorps.EventManager.AllEventsEnabled = true;
                Erreurs.gFr_debug(ex, this.Name + "fc_AddArtGecet");
            }
        }

        //==============================> Mondir - Controle Is Hidden
        //private void ssPRC_PreCmdCorpsHisto_RowLoaded(System.Object eventSender, AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_RowLoadedEvent eventArgs)
        //{
        //    int i = 0;

        //    if (ssPRC_PreCmdCorpsHisto.Columns["PRC_sel"].Text == "1")
        //    {
        //        ssPRC_PreCmdCorpsHisto.Columns["button"].CellStyleSet("coche");
        //        for (i = 1; i <= ssPRC_PreCmdCorpsHisto.Cols - 1; i++)
        //        {
        //            ssPRC_PreCmdCorpsHisto.Columns[i].CellStyleSet("ColorSelectRow");

        //        }

        //    }
        //    else
        //    {
        //        ssPRC_PreCmdCorpsHisto.Columns[0].CellStyleSet("decoche");
        //        for (i = 1; i <= ssPRC_PreCmdCorpsHisto.Cols - 1; i++)
        //        {
        //            ssPRC_PreCmdCorpsHisto.Columns[i].CellStyleSet("");
        //        }
        //    }

        //    for (i = 0; i <= ssPRC_PreCmdCorpsHisto.Cols - 1; i++)
        //    {
        //        ssPRC_PreCmdCorpsHisto.Columns[i].Locked = true;

        //    }

        //}

        private void ssPRC_PreCmdCorps_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int i = 0;

            //   If chkDupliquer.value = 1 Then
            //        If ssPRC_PreCmdCorps.Columns("PRC_SelENcours").Text = "1" Then
            //            For i = 1 To ssPRC_PreCmdCorps.Cols - 1
            //                ssPRC_PreCmdCorps.Columns(i).CellStyleSet "selection"
            //            Next
            //
            //            If ssPRC_PreCmdCorps.Columns("PRC_sel").Text = "1" Then
            //                ssPRC_PreCmdCorps.Columns("button").CellStyleSet "coche"
            //            End If
            //
            //            Exit Sub
            //
            //        End If
            //
            //   End If

            //if (e.ReInitialize)
            //    return;
            try
            {
                if (e.Row.Cells["PRC_sel"].Text == "1")
                {
                    for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                    {
                        e.Row.Cells[i].Appearance.BackColor = Color.LightGray;

                    }
                    e.Row.Cells["button"].ButtonAppearance.Image = Properties.Resources.check_mark_2_16;
                }
                else if (e.Row.Cells["PRC_sel"].Text == "2")
                {
                    e.Row.CellAppearance.BackColor = Color.FromArgb(0, 0, 128);
                }
                else
                {
                    e.Row.Cells["button"].ButtonAppearance.Image = Properties.Resources.close_folder;
                }
                e.Row.Cells["PRC_TotHT"].Activation = Activation.NoEdit;
                e.Row.Cells["PRC_Fournisseur"].Activation = Activation.NoEdit;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "ssPRC_PreCmdCorps_InitializeRow");
            }
        }

        private void SSTab2_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            try
            {
                if (SSTab2.ActiveTab != null && SSTab2.ActiveTab.Index == 0)
                {
                    fc_PRC_PreCmdCorps(txtPRE_NoAuto.Text);
                    Label10.Text = "PRECOMMANDE";
                    ssPRC_PreCmdCorpsHisto.Visible = false;

                }
                else if (SSTab2.ActiveTab != null && SSTab2.ActiveTab.Index == 1)
                {
                    fc_PRC_PreCmdCorpsHisto(txtPRE_NoAuto.Text);
                    Label10.Text = "Histo";
                    ssPRC_PreCmdCorpsHisto.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "SSTab2_SelectedTabChanged");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoAuto"></param>
        /// <param name="sWhere"></param>
        private void fc_PRC_PreCmdCorpsHisto(string sNoAuto, object sWhere = null)
        {
            try
            {
                bool bMissing = false;
                sSQL = "SELECT '' as button, PRC_Sel, PRC_NoAuto, PRE_NoAuto,PRC_Chrono, " + " PRC_References, PRC_Designation," + " PRC_Quantite, PRC_PrixHT, PRC_Unite, PRC_NoLigne, PRC_Fournisseur," + " PRC_FournisseurDT,PRC_FournisseurRef,  " + "  NoAutoGecet, PRC_fabref, PRC_fourref, PRC_NobonDeCommande" + " FROM PRC_PreCmdCorps";
                if ((sWhere == null))
                {
                    sSQL = sSQL + " WHERE PRE_NoAuto='" + sNoAuto + "' and PRC_Histo = 1 ";
                    bMissing = true;
                }
                else
                {
                    sSQL = sSQL + sWhere;

                }

                modAdorsPRCHisto = new ModAdo();
                rsPRCHisto = modAdorsPRCHisto.fc_OpenRecordSet(sSQL);

                if (bMissing == true)
                {
                    fc_LoadtreeviewHisto(sNoAuto);
                }

                ssPRC_PreCmdCorpsHisto.DataSource = rsPRCHisto;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_PRC_PreCmdCorpsHisto");
            }
        }

        private void SSTab3_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            try
            {
                //==bon de commande.
                if (SSTab3.ActiveTab != null && SSTab3.ActiveTab.Index == 1 && bLoadBon == true)
                {
                    fc_VisibleBCD(false);
                    fc_BCD_Detail(1, " WHERE BonDeCommande.NumFicheStandard =" + txtNumFicheStandard.Text, true);
                    bLoadBon = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSTab3_SelectedTabChanged");
            }
        }

        private void fc_DropUnite()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            sSQL = "";
            sSQL = "SELECT  QTE_Libelle,QTE_NO FROM QTE_Unite";
            sheridan.InitialiseCombo(DropUnite, sSQL, "QTE_Libelle", true);

        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            TreeView3.Enabled = true;
            Timer1.Enabled = false;
        }

        private void Timer2_Tick(object sender, EventArgs e)
        {
            TreeView1.Enabled = true;
            Timer2.Enabled = false;
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtMatricule_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            try
            {
                if (KeyAscii == 13)
                {
                    bRechMat = true;
                    cmdRechercheTech_Click(cmdRechercheTech, new System.EventArgs());
                    KeyAscii = 0;
                    bRechMat = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtMatricule_KeyPress");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtTechnicien_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            try
            {
                if (KeyAscii == 13)
                {
                    bRechNom = true;
                    cmdRechercheTech_Click(cmdRechercheTech, null);
                    KeyAscii = 0;
                    bRechNom = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtTechnicien_KeyPress");
            }
        }

        private void UserPreCommande_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (Visible)
                {

                    View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");

                    if (Combo1.Rows.Count == 0)
                        InitLieu();//tested

                    //  ImmTrew.Images.Clear();
                    // ImmTrew.Images.Add(Properties.Resources.Stop_512x512);
                    // ImmTrew.Images.Add(Properties.Resources.Stop_512x512);
                    //  ImmTrew.Images.Add(Properties.Resources.Stop_512x512);

                    double nNoFNS = 0;
                    string stemp = null;
                    int lNoBonDeCommande = 0;
                    // System.Windows.Forms.TreeNode oNode = null;

                    //=== modif du 25 janvier 2013, ajout
                    //=== de l'analytique pour Ammann.
                    if (General.sAnalytiqueBCD == "1")
                    {
                        lblAnalytique.Visible = true;
                        CMB_Analytique.Visible = true;
                    }
                    else
                    {
                        lblAnalytique.Visible = false;
                        CMB_Analytique.Visible = false;
                    }

                    General.open_conn();
                    fc_DropUnite();//tested
                    Sub_Type();//tested
                    ModParametre.fc_OpenConnGecet();

                    var _with22 = this;
                    //- positionne sur le dernier enregistrement.
                    if (ModParametre.fc_RecupParam(this.Name) == true)
                    {

                        fc_ChargeEnregistrement(ModParametre.sNewVar, ModParametre.sGetParam1);//tested
                        fc_ControlTagMove();//tested
                        ///===============> Tested
                        if (SSTab3.ActiveTab != null && SSTab3.ActiveTab.Index != 0)
                            SSTab3.SelectedTab = SSTab3.Tabs[0];
                        SSTab3.Visible = true;
                        ///===============> Tested
                        if (SSTab2.ActiveTab != null && SSTab2.ActiveTab.Index != 0)
                            SSTab2.SelectedTab = SSTab2.Tabs[0];

                        ///=============> Tested
                        if (General.sServiceAnalytique == "1")
                        {
                            cmbService.Visible = true;
                            lblLibService.Visible = true;
                            Label9.Visible = true;
                        }
                        else
                        {
                            cmbService.Visible = false;
                            lblLibService.Visible = false;
                            Label9.Visible = false;
                        }

                        //== si la fiche appelente est la fiche histo alors on positionne
                        //== le treeview sur le bon selctionné sur la grille.
                        if (General.UCase(ModParametre.sFicheAppelante) == General.UCase(Variable.cUserBCLivraison) || General.UCase(ModParametre.sFicheAppelante)
                            == General.UCase(Variable.cUserDocAnalytique2))
                        {
                            SSTab3.ActiveTab = SSTab3.Tabs[1];
                            SSTab3.SelectedTab = SSTab3.Tabs[1];
                            lNoBonDeCommande = Convert.ToInt32(ModParametre.sGetParam2);
                            fc_BCD_Detail(1, " WHERE BonDeCommande.NumFicheStandard =" + txtNumFicheStandard.Text, true);

                            //== positionne le treeview sur le bon de commande.
                            foreach (UltraTreeNode TN_N1 in TreeView3.Nodes)
                            {
                                foreach (UltraTreeNode TN_N2 in TN_N1.Nodes)
                                    foreach (UltraTreeNode TN_N3 in TN_N2.Nodes)
                                        //== recherche sur le niveau de bon de commande.
                                        if (General.UCase(General.Left(TN_N3.Key, 1)) == General.UCase(cNiv3))
                                        {
                                            if (General.Right(TN_N3.Key, General.Len(TN_N3.Key) - 1) == ModParametre.sGetParam2)
                                            {
                                                //TN_N3.Selected=true;
                                                // TreeView3_AfterSelect(TreeView3, new TreeViewEventArgs(TN_N3));
                                                TreeView3.ActiveNode = TN_N3;
                                                TN_N3.Selected = true;
                                                //TN_N3.EnsureVisible();
                                                return;
                                            }
                                        }

                            }

                        }

                    }



                    cmdAddFournAgences.Text = clibButton;
                    bLoadBon = true;
                    //    lblNavigation(0).Caption = sNomLien0
                    //    lblNavigation(1).Caption = sNomLien1
                    //    lblNavigation(2).Caption = sNomLien2
                }
                else
                {
                    // ModParametre.fc_CloseConnGecet();
                }
                ModMain.bActivate = false;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";UserPreCommande_VisibleChanged");
            }

            //==================================================================================
            //== Modif du lundi 02 Mai, Fusion des fiches corps et entete des bons de commande avec la fiche
            //== de precommande.

            //Screen.MousePointer = 0
            //
            //
            //fc_RecupParam (cUserBCmdHead)
            //
            //SaveSetting cFrNomApp, "UserDocFourn", "sFicheAppelante", "-1"
            //
            //If Not (sFicheAppelante = "UderBCmdBody" Or sFicheAppelante = "UserBCLivraison" Or sFicheAppelante = "UserDocFourn" Or sFicheAppelante = "UserFactFourn" Or sFicheAppelante = "UserIntervention") Then
            //    SaveSetting cFrNomApp, "UserBCmdHead", "NoBcmd", ""
            //    SaveSetting cFrNomApp, "UserBCmdHead", "NoFNS", ""
            //End If
            //
            //nNoEnt = CDbl(nz(GetSetting(cFrNomApp, "UserBCmdHead", "NoBcmd"), -1))
            //
            //ReDim Preserve tabCmd(9, 0)
            //ReDim Preserve tabCmdFD(0)
            //
            //sUtilisateur = fncUserName
            //

            //txt02.Caption = GetSetting(cFrNomApp, "UserBCmdHead", "NoInt")
            //stemp = GetSetting(cFrNomApp, "UserBCmdHead", "NoImmeuble")
            //If txt02.Caption <> "" And txt50.Text = "" Then SelectIm stemp
            //If txt02.Caption = "" Or txt02.Caption = "-1" Then
            //    txt02.Caption = "Pas d'intervention"
            //Else
            //    SelectTech fc_ADOlibelle("SELECT Intervenant FROM Intervention WHERE NoIntervention=" & txt02.Caption)
            //End If

            //nInt = 1
            //txtB13.AddItem "Normale", 0
            //txtB13.AddItem "Urgente", 1
            //txtB13.ListIndex = 0
            //
            //
            //
            //Init True, False, False, False
            //If nNoEnt > -1 Then
            //    InitGrid0 (nNoEnt)
            //    Call SelectBCmd(nNoEnt)
            //Else
            //    Grid0.Enabled = False
            //End If

        }

        //'Grille UNBOUND

        //===========================================================> Mondir - this Controle is Hiden
        //public void smnuNewFourn_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    string sCode = null;
        //    double nCode = 0;

        //    // ERROR: Not supported in C#: OnErrorStatement



        //    var _with10 = RechercheMultiCritere;

        //    RechercheMultiCritere.Width = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(12950);

        //    _with10.stxt.Text = "SELECT  Code as [Code], Raisonsocial as [RaisonSociale], Ville as [Ville] FROM fournisseurArticle";
        //    _with10.Text = "Attacher un fournisseur à votre Precommande";

        //    _with10.ShowDialog();

        //    if (!string.IsNullOrEmpty(General.nz(ref ref _with10.LigneChoisie, ref ref "")))
        //    {
        //        if (string.IsNullOrEmpty(_with10.dbgrid1.Columns("code").Text))
        //        {
        //            Interaction.MsgBox("Le fournisseur sélectionné n'a pas de code unique, permettant ainsi de l'identifier." + "Veuillez mettre à jour ce fournisseur ou en sélectionner un autre.", MsgBoxStyle.Information, "");
        //            RechercheMultiCritere.Close();
        //            return;
        //        }
        //        fc_MajFourn(ref ref (_with10.dbgrid1.Columns("code").value));

        //    }


        //    RechercheMultiCritere.Close();



        //    return;
        //erreur:
        //    Erreurs.gFr_debug(this.Name + ";Command1_Click");
        //    return;

        //}

        /// <summary>
        /// Tested
        /// </summary>
        private void Sub_Type()
        {
            var cmbTypeSource = new DataTable();
            cmbTypeSource.Columns.Add("Col");
            cmbTypeSource.Rows.Add("Achat fourniture");
            cmbTypeSource.Rows.Add("Sous-traitance");
            cmbTypeSource.Rows.Add("Mixte");
            cmbType.DataSource = cmbTypeSource;
            cmbType.ActiveRow = cmbType.Rows[0];
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ControlTagMove()
        {
            //== Identifie les contrôles qui sont suceptible d'etre redimensionnés
            //== de part et d'autre du treeview.


            //== onglet pre commande
            TreeView1.Tag = cML;
            TreeView2.Tag = cML;


            ssPRC_PreCmdCorps.Tag = cMR;
            //Mondir =======================> Controle Hideen
            //ssPRC_PreCmdCorpsHisto.Tag = cMR;
            Label10.Tag = cMR;
            //Mondir =======================> Controle Hideen
            //lblTypeTab.Tag = cMR;

            //== onglet bon de commande
            lblInter.Tag = cMRBcd;
            ssBCD_Detail.Tag = cMRBcd;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssBCD_Detail_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssPRC_PreCmdCorps.EventManager.AllEventsEnabled = false;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].Header.Caption = "Choix";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Sel"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NoAuto"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRE_NoAuto"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Chrono"].Header.Caption = "Chrono";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_References"].Header.Caption = "References";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_References"].MaxLength = 70;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Designation"].Header.Caption = "Designation";

            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Designation"].MaxLength = 250;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Quantite"].Header.Caption = "Qte";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_PrixHT"].Header.Caption = "PrixHT";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_TotHT"].Header.Caption = "TotHT";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Unite"].Header.Caption = "Unite";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NoLigne"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Fournisseur"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Fournisseur"].MaxLength = 50;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournDTCle"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].MaxLength = 50;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].Header.Caption = "Fournisseur";
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].MaxLength = 50;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["NoAutoGecet"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fabref"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fabref"].MaxLength = 15;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fourref"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_fourref"].MaxLength = 15;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_NobonDeCommande"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_SelEnCours"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["FNS_No"].Hidden = true;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["button"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].CellActivation = Activation.NoEdit;

            //===> Mondir le 15.02.2021, to fix https://groupe-dt.mantishub.io/view.php?id=2237
            //===> With is saved auto in the register
            //ssPRC_PreCmdCorps.DisplayLayout.Bands[0].Columns["PRC_Designation"].Width = 350;
            //===> Fin Modif Mondir

            ssPRC_PreCmdCorps.EventManager.AllEventsEnabled = true;
        }

        /// <summary>
        /// MONDIR
        /// This function is added to loop on all elements
        /// It is duplicated in TreeView3_AfterSelect but we need it to by pass this condition '  if (e.NewSelections.Count == 0) '
        /// It Must be ere
        /// </summary>
        /// <param name="Node"></param>
        private void loopOnTheNodes()
        {
            UltraTreeNode Node = TreeView3.ActiveNode;
            UltraTreeNode nodeParent = null;
            //If bClick = True Then Exit Sub
            ssBCD_Detail.UpdateData();

            lblInter.Text = "";
            if (General.Left(Node.Key, 1).ToUpper() == cNiv1.ToUpper())
            {
                if ((int)chkInterEnCours.CheckState == 1)
                {
                    fc_BCD_Detail(1, " Where NoIntervention =" + txtNoIntervention.Text + "", true);
                }
                else
                {
                    fc_BCD_Detail(1, " WHERE (BonDeCommande.NumFicheStandard =" + txtNumFicheStandard.Text + ") ", true);
                }
                fc_VisibleBCD(false);

            }
            else if (General.Left(Node.Key, 1).ToUpper() == cNiv2.ToUpper())//====>Tested
            {
                fc_BCD_Detail(1, " Where NoIntervention =" + General.Right(Node.Key, Node.Key.Length - 1) + "");
                lblInter.Text = Node.Text;
                fc_VisibleBCD(false);

            }
            else if (General.Left(Node.Key, 1).ToUpper() == cNiv3.ToUpper())//=====>Tested
            {
                SelectBCmd(Convert.ToDouble(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                fc_BCD_Detail(0, " Where BCD_Cle =" + General.Right(Node.Key, General.Len(Node.Key) - 1));
                fc_VisibleBCD(true);
                nodeParent = Node.Parent;
                lblInter.Text = nodeParent.Text;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView3_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {

                if (e.NewSelections.Count == 0)
                    return;

                UltraTreeNode Node = TreeView3.ActiveNode;
                UltraTreeNode nodeParent = null;
                //If bClick = True Then Exit Sub
                ssBCD_Detail.UpdateData();

                lblInter.Text = "";
                if (General.Left(Node.Key, 1).ToUpper() == cNiv1.ToUpper())
                {
                    if ((int)chkInterEnCours.CheckState == 1)
                    {
                        Program.SaveException(null, "fc_BCD_Detail called by this event TreevView3_AfterSelect line 6175");
                        fc_BCD_Detail(1, " Where NoIntervention =" + txtNoIntervention.Text + "", true);
                    }
                    else
                    {
                        Program.SaveException(null, "fc_BCD_Detail called by this event TreevView3_AfterSelect line 6180");
                        fc_BCD_Detail(1, " WHERE (BonDeCommande.NumFicheStandard =" + txtNumFicheStandard.Text + ") ", true);
                    }
                    Program.SaveException(null, "fc_VisibleBCD called by this event TreevView3_AfterSelect line 6183");
                    fc_VisibleBCD(false);

                }
                else if (General.Left(Node.Key, 1).ToUpper() == cNiv2.ToUpper())//====>Tested
                {
                    Program.SaveException(null, "fc_BCD_Detail called by this event TreevView3_AfterSelect line 6189");
                    fc_BCD_Detail(1, " Where NoIntervention =" + General.Right(Node.Key, Node.Key.Length - 1) + "");
                    lblInter.Text = Node.Text;
                    Program.SaveException(null, "fc_VisibleBCD called by this event TreevView3_AfterSelect line 6191");
                    fc_VisibleBCD(false);

                }
                else if (General.Left(Node.Key, 1).ToUpper() == cNiv3.ToUpper())//=====>Tested
                {
                    Program.SaveException(null, "SelectBCmd called by this event TreevView3_AfterSelect line 6198");
                    SelectBCmd(Convert.ToDouble(General.Right(Node.Key, General.Len(Node.Key) - 1)));
                    Program.SaveException(null, "fc_BCD_Detail called by this event TreevView3_AfterSelect line 6200");
                    fc_BCD_Detail(0, " Where BCD_Cle =" + General.Right(Node.Key, General.Len(Node.Key) - 1));
                    Program.SaveException(null, "fc_VisibleBCD called by this event TreevView3_AfterSelect line 6202");
                    fc_VisibleBCD(true);
                    nodeParent = Node.Parent;
                    lblInter.Text = nodeParent.Text;

                }

                fc_BoldNode(TreeView3);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";TreeView3_AfterSelect");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (e.NewSelections.Count == 0)
                    return;
                UltraTreeNode Node = TreeView1.ActiveNode;
                if (Node == null)
                {
                    Node = TreeView1.ActiveNode;
                }

                sSelectFourn = "";

                if (General.Left(Node.Key, 1).ToUpper() == cNiv1.ToUpper())//tested
                {
                    fc_PRC_PreCmdCorps("", " WHERE PRE_NoAuto='" + General.Right(Node.Key, Node.Key.Length - 1) + "'  and (PRC_Histo = 0 OR PRC_Histo IS NULL) ");

                }
                else if (General.Left(Node.Key, 1).ToUpper() == cNiv2.ToUpper())//tested
                {
                    fc_PRC_PreCmdCorps("", " Where PRC_FournisseurRef ='" + StdSQLchaine.gFr_DoublerQuote(General.Right(Node.Key, Node.Key.Length - 1)) + "'" + " and (PRC_Histo = 0 OR PRC_Histo IS NULL) AND pre_noAuto = '" +
                        txtPRE_NoAuto.Text + "'  and PRC_SEl = 1 ");
                    sSelectFourn = Node.Text.ToUpper();

                }
                else if (General.Left(Node.Key, 1).ToUpper() == cNiv3.ToUpper())//tested
                {
                    fc_PRC_PreCmdCorps("", " Where PRC_NoAuto ='" + General.Right(Node.Key, Node.Key.Length - 1) + "'  and (PRC_Histo = 0 OR PRC_Histo IS NULL) ");

                }

                fc_BoldNode(TreeView1);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";TreeView1_AfterSelect");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView2_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {
                if (e.NewSelections.Count == 0)
                    return;

                UltraTreeNode Node = TreeView2.ActiveNode;
                if (Node == null)
                {
                    Node = TreeView2.ActiveNode;
                }

                if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv1))//tested
                {
                    fc_PRC_PreCmdCorpsHisto("", " WHERE PRE_NoAuto='" + General.Right(Node.Key, General.Len(Node.Key) - 1) + "'  and (PRC_Histo = 1) ");

                }
                else if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv2))//tested
                {
                    fc_PRC_PreCmdCorpsHisto("", " Where PRC_Fournisseur='" + StdSQLchaine.gFr_DoublerQuote(General.Right(Node.Key, General.Len(Node.Key) - 1)) +
                        "'" + " and PRC_Histo = 1 " + " and PRC_SEl = 1 ");

                }
                else if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv3))//tested
                {
                    fc_PRC_PreCmdCorpsHisto("", " Where PRC_NoAuto ='" + General.Right(Node.Key, General.Len(Node.Key) - 1) + "' ");

                }

                fc_BoldNode(TreeView2);//tested
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";TreeView2_AfterSelect");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorps_AfterExitEditMode(object sender, EventArgs e)
        {
            try
            {
                if (ssPRC_PreCmdCorps.ActiveCell == null || ssPRC_PreCmdCorps.ActiveRow == null)
                    return;

                if (ssPRC_PreCmdCorps.ActiveCell.Column.Key.ToUpper() == "PRC_References".ToUpper() && ssPRC_PreCmdCorps.ActiveCell.DataChanged == true)
                {
                    if (!string.IsNullOrEmpty(ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_References"].Text))
                    {
                        if (General.sGecetV3 == "1")
                        {
                            sub_PreInsereArtGecetV3(ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_References"].Value.ToString());

                        }
                        else
                        {
                            sub_PreInsereArtGecet(ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_References"].Value.ToString());

                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorps_AfterExitEditMode");
            }
        }
        private void sub_PreInsereArtGecetV3(string sRefFourn)
        {
            DataTable rsFournGecet = new DataTable();
            ModAdo modAdorsFournGecet = new ModAdo();
            string sqlSelect = null;
            double dblPrix = 0;

            try
            {
                sqlSelect = "SELECT     AXECIEL_GECET.dbo.ArticleTarif.REF_FOURNISSEUR AS Fourref,"
                            + " AXECIEL_GECET.dbo.Article.DESIGNATION_ARTICLE AS libelle,"
                            + " AXECIEL_GECET.dbo.ArticleTarif.PRX_DEVIS_NET_UD AS [Prix net achat], "
                            + " AXECIEL_GECET.dbo.Article.CHRONO_INITIAL AS chrono,"
                            + " AXECIEL_GECET.dbo.ArticleTarif.CODE_UNITE_DEVIS as FouUnite "
                            + " FROM         AXECIEL_GECET.dbo.Article INNER JOIN "
                            + " AXECIEL_GECET.dbo.ArticleTarif ON AXECIEL_GECET.dbo.Article.CHRONO_INITIAL = AXECIEL_GECET.dbo.ArticleTarif.CHRONO_INITIAL"
                            + " WHERE     AXECIEL_GECET.dbo.ArticleTarif.REF_FOURNISSEUR = '" + StdSQLchaine.gFr_DoublerQuote(sRefFourn) + "' ";


                if (ModParametre.adoGecet == null)
                {
                    ModParametre.adoGecet = new System.Data.SqlClient.SqlConnection(ModParametre.cODBCGecet);
                    ModParametre.adoGecet.Open();
                }
                else if (ModParametre.adoGecet.State == ConnectionState.Closed)
                {
                    ModParametre.adoGecet.Open();
                }
                rsFournGecet = modAdorsFournGecet.fc_OpenRecordSet(sqlSelect, null, "", ModParametre.adoGecet);

                if (rsFournGecet.Rows.Count > 0)
                {
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Chrono"].Value = rsFournGecet.Rows[0]["Chrono"] + "";
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_References"].Value = rsFournGecet.Rows[0]["fourref"] + "";//Références
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Designation"].Value = rsFournGecet.Rows[0]["Libelle"] + ""; //Désignation
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Quantite"].Value = "1";
                    using (var tmpModAdo = new ModAdo())
                        ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Unite"].Value = tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite WHERE QTE_Libelle='" + rsFournGecet.Rows[0]["fouunite"] + "'");
                    string frmN = General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString()));
                    if (!string.IsNullOrEmpty(frmN) && General.IsNumeric(frmN))
                    {
                        dblPrix = Convert.ToDouble(General.fc_FormatNumber((rsFournGecet.Rows[0]["Prix net achat"].ToString())));
                    }
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_PrixHT"].Value = Convert.ToDouble(General.nz(dblPrix, "0")); //Prix HT
                    ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Totht"].Value = General.FncArrondir(Convert.ToDouble(General.nz((ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_Quantite"].Text), 0)) * Convert.ToDouble(General.nz((ssPRC_PreCmdCorps.ActiveRow.Cells["PRC_prixHT"].Text), 0)), 2);
                }

                modAdorsFournGecet.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " sub_InsereArtGecet Ref fournisseur : " + sRefFourn);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorpsHisto_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int i = 0;
            try
            {
                if (e.ReInitialize)
                    return;
                if (e.Row.Cells["PRC_sel"].Text == "1")
                {
                    //TODO : Mondir - This Is A Image
                    //ssPRC_PreCmdCorps.Columns["button"].CellStyleSet("coche");

                    //TODO : Mondir - Must Check The Color After DataSource
                    for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                    {
                        e.Row.Cells[i].Appearance.BackColor = Color.Gray;
                    }
                    ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["button"].CellButtonAppearance.Image = Properties.Resources.check_mark_2_16;
                }
                else
                {
                    //TODO : Mondir - Back To Normale
                    //ssPRC_PreCmdCorps.Columns[0].CellStyleSet("decoche");          
                    ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["button"].CellButtonAppearance.Image = Properties.Resources.close_folder;

                    //for (i = 1; i <= ssPRC_PreCmdCorps.Cols - 1; i++)
                    //{
                    //    ssPRC_PreCmdCorps.Columns[i].CellStyleSet("");
                    //}
                }
                for (i = 1; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Activation = Activation.NoEdit;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssPRC_PreCmdCorpsHisto_InitializeRow");
            }
        }

        private void ssPRC_PreCmdCorpsHisto_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPRC_PreCmdCorpsHisto_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssPRC_PreCmdCorpsHisto.EventManager.AllEventsEnabled = false;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["button"].Header.Caption = "Choix";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Sel"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_NoAuto"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRE_NoAuto"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_fourref"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Chrono"].Header.Caption = "Chrono";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_References"].Header.Caption = "References";
            //ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_References"].MaxLength = 70;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Designation"].Header.Caption = "Designation";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Designation"].Width = 350;
            //ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Designation"].MaxLength = 250;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Quantite"].Header.Caption = "Qte";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_PrixHT"].Header.Caption = "PrixHT";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Unite"].Header.Caption = "Unite";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_NoLigne"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Fournisseur"].Header.Caption = "Fournisseur";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_Fournisseur"].MaxLength = 50;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_FournisseurDT"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].Header.Caption = "Fournisseur base Gecet";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_FournisseurRef"].MaxLength = 550;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["NoAutoGecet"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_fabref"].Header.Caption = "fabref";
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_fabref"].MaxLength = 15;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["PRC_NobonDeCommande"].Hidden = true;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["button"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            ssPRC_PreCmdCorpsHisto.DisplayLayout.Bands[0].Columns["button"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            ssPRC_PreCmdCorpsHisto.EventManager.AllEventsEnabled = true;
        }

        private void ssPRC_PreCmdCorps_Error(object sender, ErrorEventArgs e)
        {
            if (ssPRC_PreCmdCorps.ActiveRow != null)
                ssPRC_PreCmdCorps.ActiveRow.CancelUpdate();

            e.Cancel = true;
        }

        private void cmbStatut_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //Mondir : The column is small so the user cant see the text of the column
            cmbStatut.DisplayLayout.Bands[0].Columns[0].Width = 250;
        }
    }
}
