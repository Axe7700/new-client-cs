﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Contrat.FicheContrat.ListeDesContrats;
using Axe_interDT.Views.Parametrages;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using PaperSource = CrystalDecisions.Shared.PaperSource;

namespace Axe_interDT.Views.Facturation
{
    public partial class UserDocImprimeFacture : UserControl
    {
        public UserDocImprimeFacture()
        {
            InitializeComponent();
        }

        DataTable rs;
        DataTable rs2;
        DataTable rs3;
        short i;
        DateTime dtTemp;
        DateTime dtTempbis;


        int indi1;
        int indi2;
        int indi3;

        decimal curtemp;

        double doubtemp;

        object strDate1;
        object strdate2;

        bool boolFactureReel;
        bool boolRequeteErreur;
        bool boolNonFacturable;
        bool blnFacReel;
        bool blnRecalcul;
        bool BLNERROR;

        int LongTemp;
        int longRetourneEnr;
        int intFacEssai;
        int longTAB;
        int j;
        int intT;

        string[] Tabe;
        string strformule2;
        string strCompte7;
        string sql2;
        string strReq;
        string sSQL;
        string strRequeteErreur;
        string strRequete;
        string strTEMP;
        string strTemp2;
        string strTemp3;
        string strtemp4;
        string[] strTABSage;
        string strCodeImmeuble;
        string strTRouTP;
        string strTEMPUNO;
        string strFormule;
        ModAdo ModAdors = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimeReel_Click(object sender, EventArgs e)
        {
            cmdImprimeReel.Enabled = false;

            if (fc_ControleFacture() == false)
                return;
            // indi1 = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous imprimer les factures corréspondantes à la période du " + txtFacture1.Value + " au " + txtFacture2.Value + "?", "Confirmation",MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous imprimer les factures corréspondantes à la période du " + txtFacture1.Value.ToShortDateString()
                + " au " + txtFacture2.Value.ToShortDateString() + "?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                blnFacReel = true;
                InsereFacture();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ControleV2()
        {
            // return false;
            //=== retourne true si tout est correct

            string sSQL;
            string sWhere = null;
            DataTable rs = new DataTable();
            int i = 0;
            int X = 0;
            string[] sIndice = null;
            string stemp = null;
            string sDoublons = "";
            string s = null;
            string sIndiceConnu = null;
            string sIndiceReel = null;
            string sdate = null;
            string sValeur = null;
            string sMessage = null;
            string sMesFinale = null;
            int k = 0;


            //try
            //{

            sSQL = "";
            sWhere = "";

            // return false;

            sSQL = "SELECT DISTINCT FacArticle.CodeArticle,FacArticle.CG_NUM, FacArticle.CG_NUM2" + " FROM Contrat INNER JOIN " + " FacCalendrier ON Contrat.NumContrat = FacCalendrier.NumContrat AND Contrat.Avenant = FacCalendrier.Avenant INNER JOIN " + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle";


            //option par date uniquement
            if (Option1.Checked == false)
            {

                sWhere = " WHERE  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                    + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and FacCalendrier.nonFacturable =" + General.fc_bln(false) + " and FacCalendrier.facture = "
                    + General.fc_bln(false) + "" + " and FacCalendrier.histo = " + General.fc_bln(false)
                    + " and FacCalendrier.resiliee = " + General.fc_bln(false);

                sWhere = sWhere + " AND (FacArticle.CG_Num IS NULL OR FacArticle.CG_Num =  '') ";
                sWhere = sWhere + " OR ";

                sWhere = sWhere + "  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                    + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1) + " and FacCalendrier.nonFacturable ="
                    + General.fc_bln(false) + " and FacCalendrier.facture = " + General.fc_bln(false)
                    + "" + " and FacCalendrier.histo = " + General.fc_bln(false) + " and FacCalendrier.resiliee = "
                    + General.fc_bln(false);

                sWhere = sWhere + " AND (FacArticle.CG_Num2 IS NULL OR FacArticle.CG_Num2 =  '') ";

                //option par date et par contrat
            }
            else if (Option1.Checked == true)
            {

                sWhere = " WHERE  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                    + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and FacCalendrier.nonFacturable =" + General.fc_bln(false) + " and FacCalendrier.facture = "
                    + General.fc_bln(false) + "" + " and FacCalendrier.histo = " + General.fc_bln(false)
                    + " and FacCalendrier.resiliee = " + General.fc_bln(false) + " and FacCalendrier.NumContrat >='"
                    + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }

            sSQL = sSQL + sWhere;

            rs = ModAdors.fc_OpenRecordSet(sSQL);


            sMessage = "";

            foreach (DataRow rowRs in rs.Rows)
            {
                if (General.nz(rowRs["CG_NUM"], "").ToString() == "" || General.nz(rowRs["CG_Num2"], "").ToString() == "")
                {

                    if (string.IsNullOrEmpty(sMessage))
                    {
                        sMessage = "Avant de facturer vous devez associer des comptes comptables aux articles suivant (fiche article) :" + Environment.NewLine;
                    }
                    sMessage = sMessage + "    -" + rowRs["CodeArticle"] + Environment.NewLine;
                }
                // rs.MoveNext();
                k++;
            }
            k = 0;
            if (!string.IsNullOrEmpty(sMessage))
            {

                rs.Clear();
                rs = null;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return false;
            }
            rs.Clear();

            sSQL = "";
            sMessage = "";

            X = 0;
            for (i = 1; i <= 6; i++)
            {
                sSQL = "";


                sSQL = "SELECT DISTINCT Contrat.Indice" + i + " FROM Contrat INNER JOIN "
                    + " FacCalendrier ON Contrat.NumContrat = FacCalendrier.NumContrat AND Contrat.Avenant = FacCalendrier.Avenant";

                //option par date uniquement
                if (!Option1.Checked)
                {

                    sWhere = " WHERE  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                        + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + " and FacCalendrier.nonFacturable =" + General.fc_bln(false)
                        + " and FacCalendrier.facture = " + General.fc_bln(false) + ""
                        + " and FacCalendrier.histo = " + General.fc_bln(false) + " and FacCalendrier.resiliee = "
                        + General.fc_bln(false);

                    //option par date et par contrat
                }
                else if (Option1.Checked == true)
                {

                    sWhere = " WHERE  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                        + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + " and FacCalendrier.nonFacturable =" + General.fc_bln(false) + " and FacCalendrier.facture = "
                        + General.fc_bln(false) + "" + " and FacCalendrier.histo = " + General.fc_bln(false)
                        + " and FacCalendrier.resiliee = " + General.fc_bln(false) + " and FacCalendrier.NumContrat >='"
                        + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
                }

                sSQL = sSQL + sWhere;

                rs = ModAdors.fc_OpenRecordSet(sSQL);

                foreach (DataRow rowRs in rs.Rows)
                {
                    if (General.nz(rowRs[$"Indice{i}"], "").ToString() != "")
                    {
                        stemp = General.Mid(rowRs["Indice" + i].ToString(), 1, rowRs["Indice" + i].ToString().Length - 1);

                        if (sDoublons == "" || sDoublons?.IndexOf((stemp + "!"), 0, StringComparison.CurrentCultureIgnoreCase) == -1)
                        {
                            sDoublons = sDoublons + stemp + "!";
                            Array.Resize(ref sIndice, X + 1);
                            sIndice[X] = stemp;
                            X = X + 1;
                        }

                    }

                    //rs.MoveNext();
                    k++;
                }
                k = 0;
                rs.Clear();

            }


            sIndiceConnu = ("INDICES CONNUS :").ToUpper() + Environment.NewLine;
            sIndiceReel = ("INDICES REELS :").ToUpper() + Environment.NewLine;

            if (X == 0)
            {
                return true;
            }
            //=== boucle sour tous les indices connus puis réeels.
            for (X = 0; X <= sIndice.Length - 1; X++)
            {

                //=== connus.
                sSQL = "SELECT     MAX(date) AS MaXdATE From FacIndice WHERE  Libelle = '" + StdSQLchaine.gFr_DoublerQuote(sIndice[X]) + "' AND (Connu = 1)";
                sdate = ModAdors.fc_ADOlibelle(sSQL);

                if (General.IsDate(sdate))
                {
                    sValeur = ModAdors.fc_ADOlibelle("SELECT Valeur From FacIndice WHERE (Libelle = '" + StdSQLchaine.gFr_DoublerQuote(sIndice[X]) + "') AND (Connu = 1)");

                    sIndiceConnu = sIndiceConnu + "   - Indice " + sIndice[X] + " au " + Convert.ToDateTime(sdate).ToShortDateString() + " = " + sValeur + Environment.NewLine;
                }

                //=== rééls
                sSQL = "SELECT  MAX(date) AS MaXdATE From FacIndice WHERE  Libelle = '" + StdSQLchaine.gFr_DoublerQuote(sIndice[X]) + "' AND (Reel = 1)";
                sdate = ModAdors.fc_ADOlibelle(sSQL);

                if (General.IsDate(sdate))
                {
                    sValeur = ModAdors.fc_ADOlibelle("SELECT Valeur From FacIndice WHERE (Libelle = '" + StdSQLchaine.gFr_DoublerQuote(sIndice[X]) + "') AND (Reel = 1)");
                    sIndiceReel = sIndiceReel + "   - Indice " + sIndice[X] + " au " + Convert.ToDateTime(sdate).Date + " = " + sValeur + Environment.NewLine;
                }

            }

            if (sIndiceConnu.ToUpper() != "INDICES CONNUS :".ToUpper() + Environment.NewLine)
            {
                sMessage = sIndiceConnu + Environment.NewLine;
            }

            if (sIndiceReel.ToUpper() != "INDICES REELS :".ToUpper() + Environment.NewLine)
            {
                sMessage = sMessage + sIndiceReel + Environment.NewLine;
            }

            rs = null;

            sMesFinale = "Récapitulatif des indices utilisés pour cette période de facturation." + Environment.NewLine + Environment.NewLine;
            sMesFinale = sMesFinale + sMessage + Environment.NewLine;

            sMesFinale = sMesFinale + "Voulez-vous continuer la facturation ?";

            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMesFinale, "Indices", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                return true;
            }
            else
            {
                return false;

            }
            return false;
            //}
            //catch (Exception e)
            //{
            //    General.gfr_liaison(e + ";fc_ControleV2");
            //    return false;
            //}

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public double fc_ExportFactCont(ref string sWhere)
        {


            //CRAXDRT.Report Report = null;
            //CRAXDRT.Application crAppl = null;
            ReportDocument report = new ReportDocument();
            //CrystalReportFormView C = new CrystalReportFormView(Report, General.SQL);

            //report.Load("FacturationEuroOnly-V9.rpt");
            DataTable rs = new DataTable();
            string sCodeImmeuble = "";
            string sNumeroFact = "";
            string sTitre = "";
            int i = 0;
            double nbRec = 0;
            object X = null;
            string sFiltreDevis1 = "";
            string sRep = "";

            try
            {

                rs = new DataTable();

                General.SQL = "SELECT FactEntete.NoFacture,FactEntete.CodeImmeuble,FactEntete.DateFacture FROM FactEntete ";
                General.SQL = General.SQL + sWhere;
                General.SQL = General.SQL + " ORDER BY NoFacture DESC ";
                rs = ModAdors.fc_OpenRecordSet(General.SQL);

                // crAppl = new CRAXDRT.Application();
                // Report = crAppl.OpenReport(General.gsRpt + "FacturationEuroOnly-V9.rpt");


                i = 0;

                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (txtDateFacture.Text.IsDate() && txtDateFacture.Text.ToDate() >= General.dtdateFactManuV3)
                {
                    report.Load(General.gsRpt + General.ETATfacturationContratExportV2);
                }
                else
                {
                    report.Load(General.gsRpt + "FacturationEuroOnly-V9.rpt");
                }
                //===> Fin Modif Mondir

                int y = 0;
                nbRec = rs.Rows.Count;
                lblLabelDeroule.Visible = true;
                lblDeroule.Visible = true;
                lblDeroule.Text = i + " / " + nbRec;

                if (rs.Rows.Count > 0)
                {

                    // rs.MoveFirst();

                    foreach (DataRow rsRow in rs.Rows)
                    {
                        i = i + 1;

                        lblDeroule.Text = i + " / " + nbRec;


                        sCodeImmeuble = (rsRow["CodeImmeuble"].ToString()
                                         .Replace(".", ""))
                                         .Replace(@"\", "")
                                         .Trim();

                        sNumeroFact = rsRow["NoFacture"].ToString().Replace("/", "-");

                        sTitre = "Facture semi-automatique pour " + sCodeImmeuble + " du " + Convert.ToDateTime(rsRow["DateFacture"].ToString()).ToShortDateString();

                        sRep = "\\Factures\\out";
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble);
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Factures ");
                        Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + sRep);

                        if (Dossier.fc_ControleFichier(General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact + ".pdf") == false)
                        {


                            report.SetParameterValue("NoFactID", rsRow["NoFacture"]);



                            // Declare variables and get the export options.// Declare variables and get the export options.


                            report.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;

                            //TODO: Verifier pourquoi la requete prend trop de temp
                            report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf");

                            //  report.Export();

                            General.Execute("UPDATE FactEntete SET ExportPDF=1 WHERE NoFacture='" + sNumeroFact + "'");
                            General.SQL = "INSERT INTO DOC_Documents (";
                            General.SQL = General.SQL + "DOC_Nat,";
                            General.SQL = General.SQL + "DOC_PhyPath,";
                            General.SQL = General.SQL + "DOC_VirPath,";
                            General.SQL = General.SQL + "DOC_DTPath,";
                            General.SQL = General.SQL + "DOC_Name,";
                            General.SQL = General.SQL + "DOC_DCreat,";
                            General.SQL = General.SQL + "DOC_DModif,";
                            General.SQL = General.SQL + "DOC_FType,";
                            General.SQL = General.SQL + "DOC_Imm,";
                            General.SQL = General.SQL + "DOC_InOut,";
                            General.SQL = General.SQL + "DOC_Sujet,";
                            General.SQL = General.SQL + "DOC_NoPiece";
                            General.SQL = General.SQL + ") VALUES (";
                            General.SQL = General.SQL + "'Factures',";
                            //SQL = SQL & "'\\DELOSTAL-DC-01\DataDT\" & sCodeimmeuble & sRep & "\Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                            General.SQL = General.SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            General.SQL = General.SQL + "'../Data/" + sCodeImmeuble + sRep + "/Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            General.SQL = General.SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + sRep + "\\Facture-" + sNumeroFact.Replace("/", "-") + ".pdf" + "',";
                            General.SQL = General.SQL + "'Facture-" + sNumeroFact.Replace("/", "-") + ".pdf',";
                            General.SQL = General.SQL + "'" + DateTime.Now + "',";
                            General.SQL = General.SQL + "'" + DateTime.Now + "',";
                            General.SQL = General.SQL + "'PDF File',";
                            General.SQL = General.SQL + "'" + sCodeImmeuble + "',";
                            General.SQL = General.SQL + "'out',";
                            General.SQL = General.SQL + "'" + StdSQLchaine.gFr_DoublerQuote(sTitre) + "',";
                            General.SQL = General.SQL + "'" + sNumeroFact.Replace("/", "-") + ".pdf'";
                            General.SQL = General.SQL + ")";
                            General.Execute(General.SQL);
                        }
                        General.Execute("UPDATE FactEntete SET ExportPDF=1 WHERE NoFacture='" + sNumeroFact + "'");
                        //rs.MoveNext();
                        y++;
                    }
                }
                lblLabelDeroule.Visible = false;
                lblDeroule.Visible = false;

                return nbRec;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "fc_ExportFactCont");
                lblLabelDeroule.Visible = false;
                lblDeroule.Visible = false;
                return 0;
            }

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="bRecalcul"></param>
        private void InsereFacture(bool bRecalcul = false)
        {
            try { 
            string sqlExportPDF = null;
            string sReturn = null;

            //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
            string sFileReport = "";
            //===> Fin Modif Mondir

            //try
            //{
            SablierOnOff(true);

            if (fc_ControleV2() == false)
            {
                SablierOnOff(false);
                return;
            }
            //adocnn.Execute "Delete FactEnTete from FactEnTete where Facture = false"
            if (blnRecalcul == false)
            {
                sSQL = "Delete FactEnTete from FactEnTete where Facture = " + General.fc_bln(false);
            }
            else
            {
                rs = new DataTable();

                strReq = "SELECT NumContrat, Avenant, CalanDate, Comptabilisee" + " From FactEnTete"
                                                                                + "  Where CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1) + ""
                                                                                + " And CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1) + ""
                                                                                + " and NumContrat >='" + txtNumContrat1.Text + "'"
                                                                                + " and NumContrat <='" + txtNumContrat2.Text + "'";
                rs = ModAdors.fc_OpenRecordSet(strReq);
                int i = 0;
                if (rs.Rows.Count > 0)
                {

                    foreach (DataRow rowRs in rs.Rows)
                    {

                        if (Convert.ToInt16(rowRs["Comptabilisee"].ToString()) == 1 || Convert.ToInt16(rowRs["Comptabilisee"].ToString()) == -1)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le contrat n°" + rs.Rows[i]["NumContrat"].ToString() + " est comptabilisé, si vous voulez recalculer n'incluez pas" + " ce numéro de contrat.");
                            ModAdors.Dispose();
                            rs = null;
                            SablierOnOff(false);
                            return;
                        }
                        i++;
                    }
                }

                rs = null;
                ModAdors.Dispose();
                sSQL = "Delete FactEnTete from FactEnTete " + " Where CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1) + "" + " and NumContrat >='" + txtNumContrat1.Text + "'" + " and NumContrat <='" + txtNumContrat2.Text + "'";
                General.Execute(sSQL);
                sSQL = "UPDATE FacCalendrier set facture = " + General.fc_bln(false)
                    + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1) + "" + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text + "'" + " and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            General.Execute(sSQL);

            Application.DoEvents();
            // Refait:
            BLNERROR = false;
            if (blnRecalcul == true)
            {
                // ne donne aucun numéro de facture
                boolFactureReel = true;
            }
            else if (blnFacReel == false)
            {
                NumeroFactureTemporaire();
                boolFactureReel = false;
            }
            else
            {
                NumeroFacture();
                boolFactureReel = true;
            }



            if (blnRecalcul == false)
            {

                strReq = "UPDATE FacCalendrier set DateFacture =" + General.fc_Fdate(txtDateFacture.Value, 1)
                                                                  + "" + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                                                                  + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                                                                  + "" + " And  FacCalendrier.Facture =" + General.fc_bln(false) + "  and histo ="
                                                                  + General.fc_bln(false) + "  and resiliee =" + General.fc_bln(false)
                                                                  + "" + " and nonFacturable =" + General.fc_bln(false) + "";
                if (Option1.Checked == true)
                {
                    strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text
                             + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
                }
            }
            else if (blnRecalcul == true)
            {

                strReq = "UPDATE FacCalendrier set DateFacture =" + General.fc_Fdate(txtDateFacture.Value, 1)
                                                                  + "" + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                                                                  + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                                                                  + "" + "  and histo =" + General.fc_bln(false) + "  and resiliee ="
                                                                  + General.fc_bln(false) + "" + " and nonFacturable =" + General.fc_bln(false) + "";
                if (Option1.Checked == true)
                {
                    strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text
                             + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
                }
            }
            General.Execute(strReq);
            Application.DoEvents();
            rs = new DataTable();
            rs2 = new DataTable();

            if (blnFacReel == false && blnRecalcul == false)
            {
                strReq = "SELECT distinct NoFactureTemp from FacCalendrier";
            }
            else
            {
                strReq = "SELECT distinct NoFacture from FacCalendrier";
            }
            strReq = strReq + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                     + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                     + "" + " And  FacCalendrier.Facture =" + General.fc_bln(false) + " and nonFacturable ="
                     + General.fc_bln(false) + "" + " and histo =" + General.fc_bln(false) + " and resiliee ="
                     + General.fc_bln(false) + "";
            if (Option1.Checked == true)
            {
                strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            rs = ModAdors.fc_OpenRecordSet(strReq);
            Application.DoEvents();

            foreach (DataRow rsRow in rs.Rows)
            {

                Application.DoEvents();
                strReq = "SELECT TOP 1 Table1.Nom, Table1.Adresse1, Table1.CodePostal as CP,"
                         + " Table1.Ville AS Vi, Contrat.CodeImmeuble, Immeuble.Adresse,"
                         + " Immeuble.CodePostal as CPi, Immeuble.Ville as VIi, FacCalendrier.FacEssai,"
                         + " Contrat.NumContrat, Contrat.Avenant, FacCalendrier.Date,"
                         + " FacCalendrier.Facture, FacCalendrier.DateFacture,"
                         + " FacCalendrier.nonFacturable, Immeuble.CptClientGerant,"
                         + " Immeuble.CommentaireFacture1, Immeuble.CommentaireFacture2,"
                         + " Immeuble.NCompte, AdresseFact1_IMM, AdresseFact2_IMM, "
                         + " AdresseFact3_IMM, CON_CompAdresse1, CON_CompAdresse2"
                         + " , Immeuble.CodePostal_IMM, Immeuble.VilleFact_IMM, contrat.CalcEcheanceImm " + " "
                         + " FROM ((Contrat LEFT JOIN Immeuble"
                         + " ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble) LEFT JOIN"
                         + " FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant)"
                         + " AND (Contrat.NumContrat = FacCalendrier.NumContrat))"
                         + " LEFT JOIN Table1 ON Immeuble.Code1 = Table1.Code1" + " Where ";
                if (blnRecalcul == false)
                {
                    strReq = strReq + "FacCalendrier.Facture =" + General.fc_bln(false) + " And";

                }
                if (blnFacReel == false && blnRecalcul == false)
                {
                    strReq = strReq + " FacCalendrier.NoFactureTemp ='" + rsRow["NoFactureTemp"].ToString() + "'";
                }
                else
                {
                    strReq = strReq + " FacCalendrier.NoFacture ='" + rsRow["NoFacture"].ToString() + "'";
                }
                strReq = strReq + " and histo =" + General.fc_bln(false) + "";

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                //===> Ligne commenté en bas est ajoutée dans cette version, mais Ahmed a apporté une correction ==> order by Contrat.Avenant Desc
                //strReq = strReq + " ORDER BY Contrat.NumContrat, Contrat.Avenant ";
                rs2 = ModAdors.fc_OpenRecordSet(strReq + " order by  Contrat.NumContrat,Contrat.Avenant Desc");
                //===> Fin Modif Mondir

                if (rs2.Rows.Count > 0)
                {
                    strReq = "INSERT INTO FactEnTete" + " (NoFacture,FactNomImmeuble,FactAdresse,FactCP,"
                                                      + " FactVille,CodeImmeuble,IntervAdresse,IntervCP,"
                                                      + " IntervVille,DateFacture,NumContrat,FacEssai,CalanDate,"
                                                      + " Avenant,CptClientGerant, CommentaireFacture1,"
                                                      + " CommentaireFacture2, NCompte,FactAdresse2, FET_CompAdresse1, FET_CompAdresse2, CalcEcheanceImm )";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " VALUES ('" + rsRow["NoFactureTemp"].ToString() + "',";
                    }
                    else
                    {
                        strReq = strReq + " VALUES ('" + rsRow["NoFacture"].ToString() + "',";
                    }

                    strReq = strReq + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["AdresseFact1_IMM"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["AdresseFact2_IMM"].ToString()) + "',"
                             + "'" + rs2.Rows[0]["CodePostal_IMM"].ToString() + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["VilleFact_IMM"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CodeImmeuble"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["Adresse"].ToString()) + "',"
                             + "'" + rs2.Rows[0]["CPi"].ToString() + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["VIi"].ToString()) + "',"
                             + "" + General.fc_Fdate(rs2.Rows[0]["DateFacture"], 1) + ","
                             + "'" + rs2.Rows[0]["NumContrat"].ToString() + "',"
                             + "'" + rs2.Rows[0]["FacEssai"].ToString() + "',"
                             + "" + General.fc_Fdate(rs2.Rows[0]["Date"], 1) + ","
                             + "'" + rs2.Rows[0]["avenant"].ToString() + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CptClientGerant"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CommentaireFacture1"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CommentaireFacture2"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["Ncompte"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["AdresseFact3_IMM"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CON_CompAdresse1"].ToString()) + "',"
                             + "'" + StdSQLchaine.gFr_DoublerQuote(rs2.Rows[0]["CON_CompAdresse2"].ToString()) + "',"
                             + "" + General.nz(General.fc_bln(Convert.ToBoolean(General.nz(rs2.Rows[0]["CalcEcheanceImm"], false))), General.fc_bln(false)) + ")";

                    General.Execute(strReq);

                    strReq = "INSERT INTO FacPied (NoFacture, FacEssai, DateFacture," + " NumContrat, CalanDate )";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " VALUES ('" + rsRow["NoFactureTemp"].ToString() + "',";
                    }
                    else
                    {
                        strReq = strReq + " VALUES ('" + rsRow["NoFacture"].ToString() + "',";
                    }
                    strReq = strReq + "'" + rs2.Rows[0]["FacEssai"].ToString()
                             + "'," + "" + General.fc_Fdate(Convert.ToDateTime(rs2.Rows[0]["DateFacture"].ToString()), 1)
                             + "," + "'" + rs2.Rows[0]["NumContrat"].ToString() + "',"
                             + "" + General.fc_Fdate(Convert.ToDateTime(rs2.Rows[0]["Date"].ToString()), 1) + ")";
                    General.Execute(strReq);
                    Application.DoEvents();
                }
                else
                {
                    //===sequence ci dessous ??? (rachid)
                    strReq = "INSERT INTO FactEntete (NoFacture,NumContrat)";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " Select NoFactureTemp,";
                    }
                    else
                    {
                        strReq = strReq + " Select NoFacture,";
                    }
                    strReq = strReq + " NumContrat From FacCalendrier";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " where Nofacturetemp ='" + rsRow["NoFactureTemp"].ToString() + "'";
                    }
                    else
                    {
                        strReq = strReq + " where Nofacture ='" + rsRow["NoFacture"].ToString() + "'";
                    }
                    General.Execute(strReq);

                    strReq = "INSERT INTO FacPied (NoFacture, FacEssai, DateFacture," + " NumContrat, CalanDate )";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " SELECT TOP 1 FacCalendrier.NoFactureTemp,";
                    }
                    else
                    {
                        strReq = strReq + " SELECT TOP 1 FacCalendrier.NoFacture,";
                    }
                    strReq = strReq + " FacCalendrier.FacEssai, FacCalendrier.DateFacture," + " Contrat.NumContrat,FacCalendrier.Date"
                             + " FROM Contrat INNER JOIN FacCalendrier ON" + " (Contrat.Avenant = FacCalendrier.Avenant) AND"
                             + " (Contrat.NumContrat = FacCalendrier.NumContrat)" + " WHERE FacCalendrier.Facture = " + General.fc_bln(false) + " AND";
                    if (blnFacReel == false)
                    {
                        strReq = strReq + " FacCalendrier.NoFactureTemp ='" + rsRow["NoFactureTemp"].ToString() + "'";
                    }
                    else
                    {
                        strReq = strReq + " FacCalendrier.NoFacture ='" + rsRow["NoFacture"].ToString() + "'";
                    }

                    //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                    //===> Ligne commenté en bas est ajoutée dans cette version, mais Ahmed a aporté une correction ==> order by Contrat.Avenant Desc
                    //strReq = strReq + " ORDER BY Contrat.NumContrat, Contrat.Avenant ";
                    General.Execute(strReq + " order by Contrat.NumContrat,Contrat.Avenant Desc");
                    //===> Fin Modif Mondir
                }
                rs2.Clear();
                //rs.MoveNext();
                i++;
                Application.DoEvents();
            }

            rs = null;
            rs2 = null;
            Application.DoEvents();
            //--- requete sur le corps de facture en base revisee et sur non revisee mais sur base actualisé.
            strReq = "INSERT INTO FacCorpsEtat(NoFacture, FacEssai, LibelleCont1,"
                     + " LibelleCont2, CodeArticle, Designation1, Designation2, Echeance,"
                     + " PCdu, Au, DateBase, BaseContractuelle, CalanDate, Facture,"
                     + " Indice1, Valeur1, Date1, Indice2, Valeur2, Date2, Indice3,"
                     + " Valeur3, Date3," + " Indice4, Valeur4, Date4,Indice5, Valeur5, Date5,Indice6, Valeur6, Date6,"
                     + " DateFacture, FormuleDeRevision, Reel, Connu,"
                     + " TypeRevision, BaseRevisee, DateRevisee, Ratio, Avenant, NumContrat,"
                     + " NonFacturable,BaseActualisee,DateActualisee,FCE_Coef, CON_TypeContrat, "
                     + " FCE_Activite,codeimmeuble,FCE_secteur, Fac_SansRevision, Fac_MoisIndice)";
            if (blnFacReel == false)
            {
                strReq = strReq + " SELECT FacCalendrier.NoFactureTemp,";
            }
            else
            {
                strReq = strReq + " SELECT FacCalendrier.NoFacture,";
            }
            strReq = strReq + "  FacCalendrier.FacEssai,Contrat.LibelleCont1,"
                            + " Contrat.LibelleCont2, Contrat.CodeArticle,"
                            + " FacArticle.Designation1, FacArticle.Designation2, FacCalendrier.Echeance"
                            + " , FacCalendrier.PCdu, FacCalendrier.Au, Contrat.DateBase,"
                            + " Contrat.BaseContractuelle, FacCalendrier.Date,"
                            + "  FacCalendrier.Facture, Contrat.Indice1, Contrat.Valeur1,"
                            + " Contrat.Date1, Contrat.Indice2, Contrat.Valeur2, Contrat.Date2,"
                            + " Contrat.Indice3, Contrat.Valeur3, Contrat.Date3," + " Contrat.Indice4, Contrat.Valeur4, Contrat.Date4,"
                            + " Contrat.Indice5, Contrat.Valeur5, Contrat.Date5,Contrat.Indice6, Contrat.Valeur6, Contrat.Date6,"
                            + " FacCalendrier.DateFacture, FacFormule.Libelle, Contrat.Reel,"
                            + " Contrat.Connu, FacCalendrier.TypeRevision, Contrat.BaseRevisee,"
                            + " Contrat.DateRevisee, FacCalendrier.Ratio, Contrat.Avenant, Contrat.NumContrat, FacCalendrier.nonFacturable"
                            + " ,BaseActualisee,DateActualisee, Contrat.CON_Coef, Contrat.CON_TypeContrat"
                            + " , contrat.AnalytiqueActivite, contrat.codeimmeuble, contrat.AnalytiqueImmeuble "
                            + " , FacCalendrier.Fac_SansRevision, Fac_MoisIndice" + " FROM ((FacArticle RIGHT JOIN Contrat ON FacArticle.CodeArticle ="
                            + " Contrat.CodeArticle) LEFT JOIN FacFormule ON Contrat.CodeFormule"
                            + " = FacFormule.Type) LEFT JOIN FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant) AND (Contrat.NumContrat = FacCalendrier.NumContrat)"
                            + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <="
                            + General.fc_Fdate(txtFacture2.Value, 1) + "" + " And  FacCalendrier.Facture =" + General.fc_bln(false)
                            + " and FacCalendrier.TypeRevision = 'B'" + " and Contrat.nonFacturable =" + General.fc_bln(false)
                            + " and histo =" + General.fc_bln(false) + "" + " and contrat.resiliee =" + General.fc_bln(false) + "";
            if (Option1.Checked == true)
            {
                strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            General.Execute(strReq);
            Application.DoEvents();


            strReq = "INSERT INTO FacCorpsEtat(NoFacture, FacEssai, LibelleCont1, LibelleCont2," + " CodeArticle, Designation1, Designation2, Echeance, PCdu, Au, DateBase," + " BaseContractuelle, CalanDate, Facture, DateFacture, Reel, Connu, " +
                     "TypeRevision" +
                     "," + " BaseRevisee, DateRevisee, Ratio, Avenant, NumContrat, NonFacturable,BaseActualisee,DateActualisee,FCE_Coef," + " CON_TypeContrat" + " , FCE_Activite, codeimmeuble, FCE_secteur,Fac_SansRevision, Fac_MoisIndice)";
            if (blnFacReel == false)
            {
                strReq = strReq + " SELECT FacCalendrier.NoFactureTemp,";
            }
            else
            {
                strReq = strReq + " SELECT FacCalendrier.NoFacture,";
            }
            strReq = strReq + " FacCalendrier.FacEssai, Contrat.LibelleCont1," + " Contrat.LibelleCont2, Contrat.CodeArticle, FacArticle.Designation1," + " FacArticle.Designation2, FacCalendrier.Echeance, FacCalendrier.PCdu," + " FacCalendrier.Au, Contrat.DateBase, Contrat.BaseContractuelle, FacCalendrier.Date," + " FacCalendrier.Facture, FacCalendrier.DateFacture, Contrat.Reel," + " Contrat.Connu, FacCalendrier.TypeRevision, Contrat.BaseRevisee," + " Contrat.DateRevisee, FacCalendrier.Ratio, Contrat.Avenant, Contrat.NumContrat," + " FacCalendrier.nonFacturable,BaseActualisee,DateActualisee,Contrat.CON_Coef, Contrat.CON_TypeContrat" + " , contrat.AnalytiqueActivite, contrat.codeimmeuble, contrat.AnalytiqueImmeuble " + " , FacCalendrier.Fac_SansRevision, Fac_MoisIndice" + " FROM ((FacArticle RIGHT JOIN Contrat ON FacArticle.CodeArticle = Contrat.CodeArticle) LEFT JOIN FacFormule ON Contrat.CodeFormule = FacFormule.Type) LEFT JOIN FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant) AND (Contrat.NumContrat = FacCalendrier.NumContrat)" + " Where FacCalendrier.Date >="
                     + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                     + "" + " And  FacCalendrier.Facture = " + General.fc_bln(false) + " and FacCalendrier.TypeRevision = 'N'" + " "
                     + " and Contrat.nonFacturable =" + General.fc_bln(false) + " and histo =" + General.fc_bln(false) + "" + " and contrat.resiliee ="
                     + General.fc_bln(false) + "";
            if (Option1.Checked == true)
            {
                strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            General.Execute(strReq);
            Application.DoEvents();

            //----sur base actualisée avec affichage de la formule ou bien du coefficent fixe selon le contrat.
            strReq = "INSERT INTO FacCorpsEtat(NoFacture, FacEssai, LibelleCont1,"
                     + " LibelleCont2, CodeArticle, Designation1, Designation2, Echeance,"
                     + " PCdu, Au, DateBase, BaseContractuelle, CalanDate, Facture,"
                     + " Indice1, Valeur1, Date1, Indice2, Valeur2, Date2, Indice3,"
                     + " Valeur3, Date3,"
                     + " Indice4, Valeur4, Date4, Indice5, Valeur5, Date5,Indice6, Valeur6, Date6, "
                     + " DateFacture, FormuleDeRevision, Reel, Connu,"
                     + " TypeRevision, BaseRevisee, DateRevisee, Ratio, Avenant, NumContrat,"
                     + " NonFacturable,BaseActualisee,DateActualisee"
                     + " ,Indice1Bis, Valeur1Bis, date1Bis," + " Indice2Bis, Valeur2Bis, Date2Bis,"
                     + " Indice3Bis, Valeur3Bis, Date3Bis,"
                     + " Indice4Bis, Valeur4Bis, date4Bis," + " Indice5Bis, Valeur5Bis, Date5Bis,"
                     + " Indice6Bis, Valeur6Bis, Date6Bis,"
                     + "   FCE_Coef, CON_TypeContrat, FCE_Activite, codeimmeuble, FCE_secteur, Fac_SansRevision, Fac_MoisIndice)";
            if (blnFacReel == false)
            {
                strReq = strReq + " SELECT FacCalendrier.NoFactureTemp,";
            }
            else
            {
                strReq = strReq + " SELECT FacCalendrier.NoFacture,";
            }
            strReq = strReq + "  FacCalendrier.FacEssai,Contrat.LibelleCont1," + " Contrat.LibelleCont2, Contrat.CodeArticle,"
                     + " FacArticle.Designation1, FacArticle.Designation2, FacCalendrier.Echeance"
                     + " , FacCalendrier.PCdu, FacCalendrier.Au, Contrat.DateBase,"
                     + " Contrat.BaseContractuelle, FacCalendrier.Date,"
                     + "  FacCalendrier.Facture, Contrat.Indice1, Contrat.Valeur1,"
                     + " Contrat.Date1, Contrat.Indice2, Contrat.Valeur2, Contrat.Date2,Contrat.Indice3, Contrat.Valeur3, Contrat.Date3,";

            strReq = strReq + " Contrat.Indice4, Contrat.Valeur4," + " Contrat.Date4, Contrat.Indice5, Contrat.Valeur5, Contrat.Date5,Contrat.Indice6, Contrat.Valeur6, Contrat.Date6,"
                     + " FacCalendrier.DateFacture, FacFormule.Libelle, Contrat.Reel,"
                     + " Contrat.Connu, FacCalendrier.TypeRevision, Contrat.BaseRevisee,"
                     + " Contrat.DateRevisee, FacCalendrier.Ratio, Contrat.Avenant, Contrat.NumContrat, FacCalendrier.nonFacturable"
                     + " ,BaseActualisee,DateActualisee" + " ,Contrat.CON_Indice1rev, Contrat.CON_Valeur1rev, Contrat.CON_Date1rev,"
                     + " Contrat.CON_Indice2rev, Contrat.CON_Valeur2rev, Contrat.CON_Date2rev,"
                     + " Contrat.CON_Indice3rev, Contrat.CON_Valeur3rev, Contrat.CON_Date3rev"
                     + " ,Contrat.CON_Indice4rev, Contrat.CON_Valeur4rev, Contrat.CON_Date4rev,"
                     + " Contrat.CON_Indice5rev, Contrat.CON_Valeur5rev, Contrat.CON_Date5rev,"
                     + " Contrat.CON_Indice6rev, Contrat.CON_Valeur6rev, Contrat.CON_Date6rev,"
                     + "  Contrat.CON_Coef, Contrat.CON_TypeContrat "
                     + " , contrat.AnalytiqueActivite, contrat.codeimmeuble, contrat.AnalytiqueImmeuble , FacCalendrier.Fac_SansRevision,Fac_MoisIndice "
                     + " FROM ((FacArticle RIGHT JOIN Contrat ON FacArticle.CodeArticle ="
                     + " Contrat.CodeArticle) LEFT JOIN FacFormule ON Contrat.CodeFormule"
                     + " = FacFormule.Type) LEFT JOIN FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant) AND (Contrat.NumContrat = FacCalendrier.NumContrat)"
                     + " Where FacCalendrier.Date >="
                     + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1) + ""
                     + " And  FacCalendrier.Facture =" + General.fc_bln(false) + " and (FacCalendrier.TypeRevision = 'R' or FacCalendrier.TypeRevision = 'C')"
                     + " and Contrat.nonFacturable =" + General.fc_bln(false) + " and histo =" + General.fc_bln(false) + "" + " and contrat.resiliee ="
                     + General.fc_bln(false) + "";
            if (Option1.Checked == true)
            {
                strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            General.Execute(strReq);
            Application.DoEvents();

            //????????????????????????????
            strReq = "INSERT INTO FacCorpsEtat(NoFacture, FacEssai, LibelleCont1, LibelleCont2,"
                     + " CodeArticle, Designation1, Designation2, Echeance, PCdu, Au, DateBase,"
                     + " BaseContractuelle, CalanDate, Facture, DateFacture, Reel, Connu, TypeRevision,"
                     + " BaseRevisee, DateRevisee, Ratio, Avenant, NumContrat, NonFacturable,BaseActualisee,"
                     + " DateActualisee,FCE_Coef, CON_TypeContrat, FCE_Activite, codeimmeuble, FCE_secteur,Fac_SansRevision, Fac_MoisIndice)";
            if (blnFacReel == false)
            {
                strReq = strReq + " SELECT FacCalendrier.NoFactureTemp,";
            }
            else
            {
                strReq = strReq + " SELECT FacCalendrier.NoFacture,";
            }
            strReq = strReq + " FacCalendrier.FacEssai, Contrat.LibelleCont1,"
                            + " Contrat.LibelleCont2, Contrat.CodeArticle, FacArticle.Designation1,"
                            + " FacArticle.Designation2, FacCalendrier.Echeance, FacCalendrier.PCdu,"
                            + " FacCalendrier.Au, Contrat.DateBase, Contrat.BaseContractuelle,"
                            + " FacCalendrier.Date, FacCalendrier.Facture, FacCalendrier.DateFacture,"
                            + " Contrat.Reel, Contrat.Connu, FacCalendrier.TypeRevision,"
                            + " Contrat.BaseRevisee, Contrat.DateRevisee, FacCalendrier.Ratio,"
                            + " Contrat.Avenant, Contrat.NumContrat, FacCalendrier.nonFacturable"
                            + " ,BaseActualisee,DateActualisee,Contrat.CON_Coef, Contrat.CON_TypeContrat"
                            + " , contrat.AnalytiqueActivite, contrat.codeimmeuble, contrat.AnalytiqueImmeuble , FacCalendrier.Fac_SansRevision,  Fac_MoisIndice"
                            + " FROM ((FacArticle RIGHT JOIN Contrat ON FacArticle.CodeArticle = Contrat.CodeArticle) LEFT JOIN FacFormule ON Contrat.CodeFormule = FacFormule.Type) LEFT JOIN FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant) AND (Contrat.NumContrat = FacCalendrier.NumContrat)"
                            + "Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1)
                            + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                            + "" + " And  FacCalendrier.Facture =" + General.fc_bln(false) + " and FacCalendrier.TypeRevision is null"
                            + " and Contrat.nonFacturable =" + General.fc_bln(false) + " and histo =" + General.fc_bln(false)
                            + "" + " and contrat.resiliee =" + General.fc_bln(false) + "";
            if (Option1.Checked == true)
            {
                strReq = strReq + " and FacCalendrier.NumContrat >='" + txtNumContrat1.Text
                         + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            General.Execute(strReq);
            //
            Application.DoEvents();


            //modif du 19092002 nouveau type de facture sur base revisee.
            //strReq = "INSERT INTO FacCorpsEtat(NoFacture, FacEssai, LibelleCont1, LibelleCont2," _
            //& " CodeArticle, Designation1, Designation2, Echeance, PCdu, Au, DateBase," _
            //& " BaseContractuelle, CalanDate, Facture, DateFacture, Reel, Connu, TypeRevision," _
            //& " BaseRevisee, DateRevisee, Ratio, Avenant, NumContrat, NonFacturable,BaseActualisee,DateActualisee)"
            // If blnFacReel = False Then
            //   strReq = strReq & " SELECT FacCalendrier.NoFactureTemp,"
            //Else
            //    strReq = strReq & " SELECT FacCalendrier.NoFacture,"
            //End If
            //strReq = strReq & " FacCalendrier.FacEssai, Contrat.LibelleCont1," _
            //& " Contrat.LibelleCont2, Contrat.CodeArticle, FacArticle.Designation1," _
            //& " FacArticle.Designation2, FacCalendrier.Echeance, FacCalendrier.PCdu," _
            //& " FacCalendrier.Au, Contrat.DateBase, Contrat.BaseContractuelle, FacCalendrier.Date," _
            //& " FacCalendrier.Facture, FacCalendrier.DateFacture, Contrat.Reel," _
            //& " Contrat.Connu, FacCalendrier.TypeRevision, Contrat.BaseRevisee," _
            //& " Contrat.DateRevisee, FacCalendrier.Ratio, Contrat.Avenant, Contrat.NumContrat," _
            //& " FacCalendrier.nonFacturable,BaseActualisee,DateActualisee" _
            //& " FROM ((FacArticle RIGHT JOIN Contrat ON FacArticle.CodeArticle = Contrat.CodeArticle) LEFT JOIN FacFormule ON Contrat.CodeFormule = FacFormule.Type) LEFT JOIN FacCalendrier ON (Contrat.Avenant = FacCalendrier.Avenant) AND (Contrat.NumContrat = FacCalendrier.NumContrat)" _
            //& " Where FacCalendrier.Date >=#" & Format(txtFacture1, "mm/dd/yyyy") & "#" _
            //& " And FacCalendrier.Date <=#" & Format(txtFacture2, "mm/dd/yyyy") & "#" _
            //& " And  FacCalendrier.Facture = false and FacCalendrier.TypeRevision = 'R'" _
            //& " and Contrat.nonFacturable = false and histo = false" _
            //& " and contrat.resiliee = false"
            //If Option1.Value = 1 Then
            //    strReq = strReq & " and FacCalendrier.NumContrat >='" & txtNumContrat1.Text & "' and FacCalendrier.NumContrat <='" & txtNumContrat2.Text & "'"
            //End If

            //adocnn.Execute strReq

            //=== modifie le champs Annee dans FacCorpsEtat, par rapport
            //=== au textbox (txtDateFacture)
            if (txtDateFacture.Value.Month <= 8)
            {
                strTEMP = (txtDateFacture.Value.Year - 1) + "/" + txtDateFacture.Value.Year;
                strReq = "Update FacCorpsEtat set Annee = '" + strTEMP + "'" + " Where FacCorpsEtat.CalanDate >="
                         + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCorpsEtat.CalanDate <="
                         + General.fc_Fdate(txtFacture2.Value, 1) + "" + " And  FacCorpsEtat.Facture =" + General.fc_bln(false) + " ";
                if (Option1.Checked == true)
                {
                    strReq = strReq + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text + "' and FacCorpsEtat.NumContrat <='" + txtNumContrat2.Text + "'";
                }
                General.Execute(strReq);
                Application.DoEvents();
            }
            else if (txtDateFacture.Value.Year > 8)
            {
                strTEMP = txtDateFacture.Value.Year + "/" + (txtDateFacture.Value.Year + 1);
                strReq = "Update FacCorpsEtat set Annee = '" + strTEMP + "'" + " Where FacCorpsEtat.CalanDate >="
                         + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCorpsEtat.CalanDate <="
                         + General.fc_Fdate(txtFacture2.Value, 1) + "" + " And  FacCorpsEtat.Facture =" + General.fc_bln(false) + "";
                if (Option1.Checked == true)
                {
                    strReq = strReq + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text
                             + "' and FacCorpsEtat.NumContrat <='" + txtNumContrat2.Text + "'";
                }
                General.Execute(strReq);
                Application.DoEvents();
            }
            RechercheDesIndices();
            Application.DoEvents();
            if (bRecalcul == true)
            {
                RechercheQuotient(bRecalcul);
                Application.DoEvents();
            }
            else
            {
                RechercheQuotient();
                Application.DoEvents();
            }
            //if(DateTime.Now.ToString()=="17/05/2020" && General.UCase(General.fncUserName())== General.UCase("rachid abbouchi"))
            if (General.sContratMultiTVA == "1")
            {
                facturePiedv2();
            }
            else
            {
                facturePied();
            }


            Application.DoEvents();
            if (blnFacReel == true || blnRecalcul == true)
            {
                CocherChampsFactureTrue();
                Application.DoEvents();
                // ChangementCalendrier
                boolFactureReel = false;
            }

            ChangementCalendrier();

            Application.DoEvents();
            DataTable Adodc1 = new DataTable();

            sSQL = "SELECT FacPied.NumContrat, FacPied.NoFacture," +
                   " FacPied.TotalHT, FacPied.TauxTVA, FacPied.MontantTVA," +
                   " FacPied.TTC"
                   + " FROM FacPied RIGHT JOIN FactEnTete ON FacPied.NoFacture"
                   + " = FactEnTete.NoFacture"
                   + " where  FactEnTete.DateFacture ="
                   + General.fc_Fdate(txtDateFacture.Value, 1) + " ";

            if (Option1.Checked == true)
            {
                sSQL = sSQL + " and FacPied.NumContrat >='" + txtNumContrat1.Text + "' and FacPied.NumContrat <='" + txtNumContrat2.Text + "'";
            }


            sSQL = sSQL + "ORDER BY FacPied.NoFacture";
            Adodc1 = ModAdors.fc_OpenRecordSet(sSQL);
            SSOleDBGrid1.DataSource = Adodc1;
            //_with2.Refresh();
            Application.DoEvents();
            lblTot.Text = "TOTAL DES FACTURES: ";
            lblTot.Text = lblTot.Text + " " + Adodc1.Rows.Count;
            Application.DoEvents();

            strDate1 = txtFacture1.Value.ToString("yyyy,MM,dd");
            strdate2 = txtFacture2.Value.ToString("yyyy,MM,dd");
            string sDateFacture = null;
            sDateFacture = txtDateFacture.Value.ToString("yyyy,MM,dd");

            if (blnFacReel == false)
            {
                if (Option1.Checked == true)
                {
                    strRequete = "{FactEnTete.CalanDate}>= Date (" + strDate1 + ")"
                                 + " and {FactEnTete.CalanDate}<= Date (" + strdate2 + ")"
                                 + " and {FactEnTete.NumContrat} >='" + txtNumContrat1.Text + "'" + " and {FactEnTete.NumContrat} <='" + txtNumContrat2.Text + "'";

                    sqlExportPDF = " WHERE FactEnTete.CalanDate>='" + txtFacture1.Value.ToShortDateString() + "'" + " and FactEnTete.CalanDate<='"
                                   + txtFacture2.Value.ToShortDateString() + "'" + " and FactEnTete.NumContrat>='" + txtNumContrat1.Text + "'"
                                   + " and FactEnTete.NumContrat<='" + txtNumContrat2.Text + "'";

                }
                else
                {
                    strRequete = "{FactEnTete.CalanDate}>= Date (" + strDate1 + ")" + " and {FactEnTete.CalanDate}<= Date (" + strdate2 + ")";

                    sqlExportPDF = " WHERE FactEnTete.CalanDate>='" + txtFacture1.Value
                                                                    + "'" + " and FactEnTete.CalanDate<='" + txtFacture2.Value + "'";
                    //& " and {FactEnTete.datefacture}= Date (" & sDateFacture & ")"
                }
            }
            else
            {
                if (Option1.Checked == true)
                {
                    strRequete = "{FactEnTete.CalanDate}>= Date (" + strDate1 + ")"
                                 + " and {FactEnTete.CalanDate}<= Date (" + strdate2 + ")" + " and {FactEnTete.NumContrat} >='"
                                 + txtNumContrat1.Text + "'" + " and {FactEnTete.NumContrat} <='" + txtNumContrat2.Text + "'";

                    sqlExportPDF = " WHERE FactEnTete.CalanDate>='" + txtFacture1.Value.ToShortDateString() + "'" + " and FactEnTete.CalanDate<='"
                                   + txtFacture2.Value.ToShortDateString() + "'" + " and FactEnTete.NumContrat>='" + txtNumContrat1.Text + "'"
                                   + " and FactEnTete.NumContrat<='" + txtNumContrat2.Text + "'";

                }
                else
                {
                    strRequete = "{FactEnTete.CalanDate}>= Date (" + strDate1 + ")" + " and {FactEnTete.CalanDate}<= Date (" + strdate2 + ")";
                    //& " and {FactEnTete.datefacture} = date(" & sDateFacture & ")"

                    sqlExportPDF = " WHERE FactEnTete.CalanDate>='" + txtFacture1.Value.ToShortDateString() + "'" + " and FactEnTete.CalanDate<='"
                                   + txtFacture2.Value.ToShortDateString() + "'";

                }
                if (chk3bacs.Checked == true)
                {
                    fc_ImprimeBacs(ref General.CHEMINEUROONLY, ref strRequete);
                    // ReportDocument R = new ReportDocument();
                    // R.Load(General.CHEMINEUROONLY);
                    //CrystalReportFormView cr = new CrystalReportFormView(R,strRequete);
                }
            }

            //If CDate(txtFacture1) < #6/1/2001# Then
            //     CrystalReport1.ReportFileName = CHEMINFACTURECONTRAT
            //ElseIf CDate(txtFacture1) < #8/26/2002# Then
            //     CrystalReport1.ReportFileName = CHEMINNEWFACTURECONTRAT
            //Else


            if (General.sExportPdfContrat == "1")
            {
                SablierOnOff(true);
                sReturn = devModeCrystal.fc_ExportCrystalSansFormule(strRequete, General.gsRpt + "FacturationEuroOnly-V9.rpt");
                //sReturn = fc_ExportCrystalSansFormule(strRequete, CHEMINEUROONLY)
                if (!string.IsNullOrEmpty(sReturn))
                {

                    ModuleAPI.Ouvrir(sReturn);
                    Cursor.Current = Cursors.Arrow;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur a été décelé, veuillez consulter votre fournisseur.", "Opération annulée");
                }
            }
            else
            {
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (txtDateFacture.Text.IsDate() && txtDateFacture.Text.ToDate() >= General.dtdateFactManuV3)
                {
                    ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                    ModCrystalPDF.tabFormulas[0].sNom = "";
                    ModCrystalPDF.tabFormulas[0].sType = "";
                    ModCrystalPDF.tabFormulas[0].sCritere = "";
                    sFileReport = devModeCrystal.fc_ExportCrystal(strRequete, General.ETATfacturationContratV2,
                        intNoFieldSelection: CheckState.Checked);

                        if (File.Exists(sFileReport))
                            Process.Start(sFileReport);
                        else
                            Program.SaveException(null, "InsereFacture=> File {sFileReport} not found");
                }
                //===> Fin Modif Mondir
                else
                {
                    ReportDocument CrystalReport1 = new ReportDocument();
                    CrystalReport1.Load(General.CHEMINEUROONLY);
                    CrystalReportFormView CV = new CrystalReportFormView(CrystalReport1, strRequete);
                    CV.Show();
                }
            }

            if (blnFacReel == true && General.adocnn.Database.ToUpper() != ("AXECIEL_DELOSTAL_PDA").ToUpper())
            {
                fc_ExportFactCont(ref sqlExportPDF);
            }
            SablierOnOff(false);

            if (BLNERROR == true)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Des erreurs ont éte décelées. Cliquer sur OK pour les visualiser si le fichier d'erreurs n'apparait pas au premier plan ");
                ModuleAPI.Ouvrir(General.cCheminFichierFactContrat);

                //Shell "C:\Program Files\Windows NT\Accessoires\wordpad.exe " &
            }
            }
            catch (Exception e)
            {
                Program.SaveException(e, ShowAlert: true);
            }
            //err_Renamed:

            //=== si l'erreur est un doublons (dans le champs FacEssai,'cle Primaire'
            //=== alors le programma effacera ces lignes si celle-çi ne
            //=== sont pas facturées
            //if (Err().Number == -2147467259)
            //{
            //    // MsgBox "Attention ce(s) numero(s) de contrat ont déja été facturé", vbCritical, "Données invalidées"
            //   // Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(Err().Description,  "",MessageBoxButtons.OK,MessageBoxIcon.Error, Convert.ToString(Err().Number));
            //    boolFactureReel = false;
            //    SablierOnOff(false);
            //}
            //else
            //{
            //    // Print #1, err.Number & " " & err.Description
            //    // ERROR: Not supported in C#: ResumeStatement

            //    // BLNERROR = True
            //}
            return;
            //}
            //catch (Exception e)
            //{
            //    boolFactureReel = false;
            //    SablierOnOff(false);
            //    Erreurs.gFr_debug(e, "InsereFacture");
            //}
        }
        private void cmdIntegration_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //cod commanter dans VB

            // Dim rs As ADODB.Recordset

            // Set rs = New ADODB.Recordset

            // With rs
            //recherche des factures à intégrer
            // SQL = "SELECT * FROM FactEntete WHERE Comptabilisee='0' and Facture = true"
            // .Open SQL, adocnn
            // If Not (.BOF Or .EOF) Then

            //    prcIntegrationFacture1 rs
            //     Label4 = ""
            // Else
            //     MsgBox "Pas de facture à intégrer"
            // End If

            //.Close
            // End With
            // Set rs = Nothing
            // Exit Sub

        }
        private void cmdRecalculer_Click(object sender, EventArgs e)
        {
            if (fc_ControleFacture() == false)
                return;
            if (string.IsNullOrEmpty(txtNumContrat1.Text) || string.IsNullOrEmpty(txtNumContrat2.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour recalculer vous devez saisir un numéro de contrat.", "Opération annulée", MessageBoxButtons.OK);
                return;
            }

            indi1 = Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous recalculer les factures corréspondantes à la période du "
                + txtFacture1.Value + " au " + txtFacture2.Value + " pour les contrats " + txtNumContrat1.Text
                + " à " + txtNumContrat2.Text + " ?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question));

            if (indi1 == Convert.ToInt16(DialogResult.Yes))
            {
                blnRecalcul = true;
                blnFacReel = true;
                InsereFacture(true);
            }
        }
        private void cmdRectif_Click(object sender, EventArgs e)
        {
            General.Execute("UPDATE (FactEnTete INNER JOIN FacCorpsEtat ON FactEnTete.NoFacture = FacCorpsEtat.NoFacture) INNER JOIN FacPied ON FactEnTete.NoFacture = FacPied.NoFacture SET FacCorpsEtat.Facture = False, FactEnTete.Facture = False WHERE FacPied.TotalHT=0 ");
            RechercheDesIndices();
            RechercheQuotient();
            facturePied();

            //CrystalReport1.SelectionFormula = strRequete;
            // CrystalReport1.Action = 1;
            ReportDocument RD = new ReportDocument();
            RD.Load(General.gsRpt + General.CHEMINEUROONLY);//Todoo /verifier le chemain VB
            CrystalReportFormView C = new CrystalReportFormView(RD, strRequete);

            CocherChampsFactureTrue();
            boolFactureReel = false;
            SablierOnOff(false);
        }
        private void Command1_Click(object sender, EventArgs e)
        {

            if (fc_ControleFacture() == false)
                return;
            InsereFacture();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void RechercheDesIndices()
        {

            int lMoisIndice = 0;
            string sConnuReel = null;
            string sAutoLiquidation = null;
            string sSQLAutoliqui = null;
            //try
            //{
            rs = new DataTable();
            rs2 = new DataTable();
            rs3 = new DataTable();

            //-----Alimente la table contenant les comptes de la classe7  et les taux de tva correspondants
            // strTABSage = new string[301, 4];
            string[,] strTABSage = new string[301, 4];
            //rs.Open "SELECT F_COMPTEG.CG_NUM AS [Code comptable], F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM WHERE (F_COMPTEG.CG_NUM) Like '488#####' or  (F_COMPTEG.CG_NUM) Like '7#######'", adocnn, adOpenForwardOnly, adLockReadOnly
            // modif du 30 avril 2002 les postes en 2000 ne gére pas les # comme pour win 98

            //sSQL = "SELECT F_COMPTEG.CG_NUM AS [Code comptable], F_COMPTEG.CG_INTITULE AS Intitulé, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" _
            //& " WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) = 8 )  "

            sSQL = "SELECT F_COMPTEG.CG_NUM AS [Code comptable], F_COMPTEG.CG_INTITULE AS Intitulé, F_TAXE.CG_NUM, F_TAXE.TA_TAUX FROM F_COMPTEG LEFT JOIN (F_TAXE RIGHT JOIN F_ETAXE ON F_TAXE.TA_NO = F_ETAXE.TA_NO) ON F_COMPTEG.CG_NUM = F_ETAXE.CG_NUM" + " WHERE (F_COMPTEG.CG_NUM) Like '488%' or  (F_COMPTEG.CG_NUM Like '7%' and len(F_COMPTEG.CG_NUM) >= 3 )  ";

            SAGE.fc_OpenConnSage();

            //rs.Open sSQL, adocnn, adOpenForwardOnly, adLockReadOnly
            rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, SAGE.adoSage);
            longTAB = 0;

            if (rs.Rows.Count > 0)
            {
                // while (rs.Rows.Count > 0)
                foreach (DataRow dr in rs.Rows)
                {
                    strTABSage[longTAB, 1] = dr["Code comptable"].ToString() + "";
                    strTABSage[longTAB, 2] = dr["TA_Taux"].ToString() + "";
                    strTABSage[longTAB, 3] = dr["CG_NUM"].ToString() + "";
                    longTAB = longTAB + 1;
                    //rs.MoveNext();

                    Application.DoEvents();
                }
            }
            rs.Clear();

            SAGE.fc_CloseConnSage();

            //selection par date uniquement
            if (!Option1.Checked)
            {
                sSQL = "SELECT FacCorpsEtat.Indice1, FacCorpsEtat.Indice2, FacCorpsEtat.Indice3,"
                       + " FacCorpsEtat.Indice4, FacCorpsEtat.Indice5, FacCorpsEtat.Indice6," + " FacCorpsEtat.FacEssai, FacCorpsEtat.Reel, FacCorpsEtat.Connu, FacCorpsEtat.typeRevision," + " FacCorpsEtat.FormuleDeRevision, facCorpsEtat.NumContrat," + " facCorpsEtat.Avenant,FacCorpsEtat.CodeArticle," + " facCorpsEtat.Fac_SansRevision, Fac_MoisIndice, NoFacture " + " From FacCorpsEtat" + " Where FacCorpsEtat.CalanDate >="
                    + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " And  FacCorpsEtat.Facture = " + General.fc_bln(false);
                rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn/*, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic)*/);
                Application.DoEvents();

                //selection par date uniquement et par contrat
            }
            else if (Option1.Checked == true)
            {
                sSQL = "SELECT  FacCorpsEtat.Indice1, FacCorpsEtat.Indice2, FacCorpsEtat.Indice3,"
                       + " FacCorpsEtat.Indice4, FacCorpsEtat.Indice5, FacCorpsEtat.Indice6,"
                       + " FacCorpsEtat.FacEssai, FacCorpsEtat.Reel, FacCorpsEtat.Connu," + " FacCorpsEtat.typeRevision, FacCorpsEtat.FormuleDeRevision," + " facCorpsEtat.NumContrat, facCorpsEtat.Avenant,FacCorpsEtat.CodeArticle," + " facCorpsEtat.Fac_SansRevision , Fac_MoisIndice, NoFacture " + " From FacCorpsEtat Where FacCorpsEtat.CalanDate >="
                    + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " And  FacCorpsEtat.Facture =" + General.fc_bln(false) + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text
                    + "' and FacCorpsEtat.NumContrat <='" + txtNumContrat2.Text + "'";
                rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn/*, ADODB.CursorTypeEnum.adOpenKeyset, ADODB.LockTypeEnum.adLockOptimistic*/);
                Application.DoEvents();

            }
            strRequeteErreur = "";
            j = 0;
            foreach (DataRow rowRs in rs.Rows)
            {
                if (rowRs["typerevision"].ToString().ToUpper() == "B" && Convert.ToInt16(General.nz(rowRs["Fac_SansRevision"], 0).ToString()) != 1)
                {
                    //stocke le champs FacEssai pour la requete UPDATE plus bas
                    strTemp2 = rowRs["FacEssai"].ToString();
                    if (!string.IsNullOrEmpty(rowRs["FormuleDeRevision"].ToString()))
                    {
                        i = 0;
                        //Initialise le tableau a 0
                        Tabe = new string[i + 1];
                        Tabe[i] = "";
                        //la boucle permet de stocker les noms des indices
                        foreach (DataColumn column in rs.Columns)
                        {
                            if (rowRs[column.ColumnName] != DBNull.Value
                                &&
                                !string.IsNullOrEmpty(rowRs[column.ColumnName].ToString())
                                ) /*!string.IsNullOrEmpty((rs.Rows[i]))*/
                            {
                                //cette condition empéche de stocker les champs
                                //different des indices
                                //== modif rachid ajout de 3 indices.
                                if (i == 6)
                                {
                                    //If i = 3 Then
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                                else
                                {
                                    Array.Resize(ref Tabe, i + 1);
                                    //stocke les noms des indices sans le o
                                    Tabe[i] = General.Left(rowRs[column.ColumnName].ToString(), General.Len(rowRs[column.ColumnName].ToString()) - 1);
                                }
                            }

                            i++;
                        }


                        //la boucle çi-dessous prépare la clause Where de la requete avec les noms des indices
                        //recupéres.
                        for (i = 0; i <= Tabe.Length - 1; i++)
                        {
                            strTEMP = " where FacIndice.Libelle = '" + Tabe[i] + "'";
                            if (string.IsNullOrEmpty(Tabe[i]))
                            {
                                BLNERROR = true;
                                File.WriteAllText(General.cCheminFichierFactContrat, "Le contrat " + rowRs["NumContrat"].ToString() + " Avenant " + rowRs["avenant"].ToString() + " a un type de revision avec formule mais ne comporte pas d'indice.");
                                if (boolFactureReel == true)
                                {
                                    boolRequeteErreur = true;
                                    //boolean indiquant q'une erreur est survenus préparant
                                    //ainsi la requete dans la procedure CocherChampsFacturetrue
                                    strRequeteErreur = strRequeteErreur + " and NumContrat <>'" + rowRs["NumContrat"].ToString() + "'";
                                    //adocnn.Execute "Delete from FactEntete where FactEntete.NumContrat ='" & rs!NumContrat & "' and FactEntete.Facture = false"
                                    rs2.Clear();
                                    ModAdors.Dispose();
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }

                            //recherche de la date de l'indice dans la table FacIndice
                            //inferieur ou égale à la date de facturation.
                            if (Convert.ToBoolean(rowRs["Connu"].ToString()) == true)
                            {
                                rs2 = ModAdors.fc_OpenRecordSet("SELECT date from FacCalendrier  where facessai =" + Convert.ToInt32(rowRs["FacEssai"].ToString()));
                                if (rs2.Rows.Count > 0)
                                {
                                    dtTempbis = Convert.ToDateTime(rs2.Rows[0]["Date"].ToString());
                                }
                                rs2.Clear();
                                sSQL = "SELECT TOP 1 Max(FacIndice.date) AS MaxDedate" + " From FacIndice GROUP BY FacIndice.Libelle, FacIndice.date, FacIndice.Connu"
                                    + " Having FacIndice.Libelle ='" + Tabe[i] + "'" + " And (FacIndice.Date) <="
                                    + General.fc_Fdate(Convert.ToDateTime(dtTempbis), 1) + " And FacIndice.Connu = " + General.fc_bln(true);

                                //== Modif Rachid, forcer un indice pour un mois donnée.
                                lMoisIndice = Convert.ToInt16(General.nz(rowRs["Fac_MoisIndice"], 0));
                                if (lMoisIndice != 0)
                                {
                                    sSQL = sSQL + " AND (MONTH([date]) = " + lMoisIndice + ")";
                                }
                                sSQL = sSQL + " ORDER BY FacIndice.date DESC";

                                rs2 = ModAdors.fc_OpenRecordSet(sSQL);
                                Application.DoEvents();
                            }
                            else if (Convert.ToBoolean(rowRs["Reel"].ToString()) == true)
                            {
                                //rs2.Open "select date from faccorpsetat where numcontrat where facessai =" & rs!FacEssai & "", adocnn, adOpenForwardOnly, adLockReadOnly
                                //modif 20092002

                                rs2 = ModAdors.fc_OpenRecordSet("SELECT date from FacCalendrier  where facessai =" + Convert.ToInt32(rowRs["FacEssai"].ToString()));
                                //rs2.Open "select date from faccorpsetat where facessai =" & rs!FacEssai & "", adocnn, adOpenForwardOnly, adLockReadOnly
                                if (rs2.Rows.Count > 0)
                                {
                                    dtTempbis = Convert.ToDateTime(rs2.Rows[0]["Date"].ToString());
                                }
                                rs2.Clear();
                                ModAdors.Dispose();
                                sSQL = "SELECT TOP 1 Max(FacIndice.date) AS MaxDedate"
                                    + " From FacIndice GROUP BY FacIndice.Libelle, FacIndice.date, FacIndice.Reel"
                                    + " Having FacIndice.Libelle ='" + Tabe[i] + "'" + " And (FacIndice.Date) <="
                                    + General.fc_Fdate(Convert.ToDateTime(dtTempbis), 1) + " And FacIndice.Reel =" + General.fc_bln(true);

                                //== Modif Rachid, forcer un indice pour un mois donnée.
                                lMoisIndice = Convert.ToInt16(General.nz(rowRs["Fac_MoisIndice"], 0));
                                if (lMoisIndice != 0)
                                {
                                    sSQL = sSQL + " AND (MONTH([date]) = " + lMoisIndice + ")";
                                }
                                sSQL = sSQL + " ORDER BY FacIndice.date DESC";

                                rs2 = ModAdors.fc_OpenRecordSet(sSQL);
                                Application.DoEvents();
                            }
                            else
                            {
                                //=== modif du 16 octobre 2013, signale une erreur rien n'est saisie pour rs.Fields!Reel et rs.Fields!connuu
                                File.AppendAllText(General.cCheminFichierFactContrat, $@"Vous devez préciser si le contrat {rowRs["NumContrat"]} - { rowRs["avenant"]} se base sur des indices connus ou réels  {Environment.NewLine}");
                                BLNERROR = true;
                                //goto SUITE;

                            }

                            if (rs2.Rows.Count > 0)
                            {
                                dtTemp = Convert.ToDateTime(rs2.Rows[0]["MaxDedate"].ToString());
                            }
                            else
                            {
                                BLNERROR = true;
                                //== modif rachid du 1 fevrier 2005
                                if (Convert.ToBoolean(rowRs["Reel"].ToString()) == true)
                                {
                                    sConnuReel = "réel";
                                }
                                else if (Convert.ToBoolean(rowRs["Connu"].ToString()) == true)
                                {
                                    sConnuReel = "connu";
                                }

                                File.AppendAllText(General.cCheminFichierFactContrat,
                                    lMoisIndice != 0
                                        ? $@"l'indice {Tabe[i]} {sConnuReel} n' a pas de valeur pour le mois {lMoisIndice}. pour le contrat {rowRs["NumContrat"]} - {rowRs["avenant"]}  {Environment.NewLine}"
                                        : $@"l'indice {Tabe[i]} {sConnuReel} n' a pas de valeur par rapport à la date d' écheance. pour le contrat {rowRs["NumContrat"]} - {rowRs["avenant"]} {Environment.NewLine}");

                                if (boolFactureReel == true)
                                {
                                    boolRequeteErreur = true;
                                    //boolean indiquant q'une erreur est survenus préparant
                                    //ainsi la requete dans la procedure CocherChampsFacturetrue
                                    strRequeteErreur = strRequeteErreur + " and NumContrat <>'" + rowRs["NumContrat"].ToString() + "'";
                                    //adocnn.Execute "Delete from FactEntete where FactEntete.NumContrat ='" & rs!NumContrat & "' and FactEntete.Facture = false"
                                    rs2.Clear();
                                    ModAdors.Dispose();
                                    break; // TODO: might not be correct. Was : Exit For
                                }
                            }
                            rs2.Clear();
                            ModAdors.Dispose();
                            Application.DoEvents();
                            //recherche du libelle ,de la date de l'indice trouvé
                            //par rapport à la date trouvéé ci-dessus

                            if (Convert.ToBoolean(rowRs["Connu"].ToString()) == true)
                            {
                                sSQL = "SELECT FacIndice.Libelle, FacIndice.date, FacIndice.Valeur" + " FROM FacIndice" + strTEMP
                                    + " and FacIndice.Date =" + General.fc_Fdate(dtTemp, 1) + " And FacIndice.Connu ="
                                    + General.fc_bln(true);
                                rs2 = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);
                                Application.DoEvents();
                            }
                            else if (Convert.ToBoolean(rowRs["Reel"].ToString()) == true)
                            {
                                sSQL = "SELECT FacIndice.Libelle, FacIndice.date, FacIndice.Valeur"
                                    + " FROM FacIndice" + strTEMP + " and FacIndice.Date =" + General.fc_Fdate(dtTemp, 1)
                                    + " And FacIndice.Reel =" + General.fc_bln(true);
                                rs2 = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);
                                Application.DoEvents();
                            }
                            if (rs2.Rows.Count > 0)
                            {
                                //affecte les nouveaux indices dans  FacCorpsEtat
                                //(table servant à imprimer le corps de l'état)
                                //==== bug expiration du delai.
                                sSQL = "UPDATE FacCorpsEtat" + " set Indice" + (i + 1) + "Bis = '"
                                    + rs2.Rows[0]["Libelle"].ToString() + "'" + ", Valeur" + (i + 1) + "Bis = "
                                    + rs2.Rows[0]["valeur"].ToString() + " ," + " Date" + (i + 1) + "Bis ="
                                    + General.fc_Fdate(Convert.ToDateTime(rs2.Rows[0]["Date"].ToString()), 1) + " where FacCorpsEtat.FacEssai ='"
                                    + strTemp2 + "' and FacCorpsEtat.Facture =" + General.fc_bln(false);
                                General.Execute(sSQL);
                                Application.DoEvents();

                                //Modif du 081002
                                sSQL = "UPDATE contrat" + " set CON_Indice" + (i + 1) + "rev = '" + rs2.Rows[0]["Libelle"].ToString()
                                    + "'" + ", CON_Valeur" + (i + 1) + "rev = " + rs2.Rows[0]["valeur"].ToString() + " ," + " CON_Date" + (i
                                    + 1) + "rev =" + General.fc_Fdate(Convert.ToDateTime(rs2.Rows[0]["Date"]), 1) + " where numcontrat ='"
                                    + rowRs["NumContrat"].ToString() + "' and avenant =" + rowRs["avenant"].ToString();
                                General.Execute(sSQL);
                                Application.DoEvents();
                            }
                            else
                            {
                                sSQL = "UPDATE FacCorpsEtat set Indice" + (i + 1) + "Bis = '" + Tabe[i] + "' ," + " Valeur"
                                    + (i + 1) + "Bis = 0  where FacCorpsEtat.FacEssai ='" + strTemp2 + "'"
                                    + " and facCorpsEtat.Facture =" + General.fc_bln(false);
                                General.Execute(sSQL);
                            }
                            rs2.Clear();
                            ModAdors.Dispose();
                            Application.DoEvents();
                        }
                    }
                    else
                    {
                        intFacEssai = Convert.ToInt32(strTemp2);
                        BLNERROR = true;

                        File.AppendAllText(General.cCheminFichierFactContrat, "La Formule de revision n'éxiste pas dans le contrat " + rowRs["NumContrat"].ToString()
                                + ", Avenant: " + rowRs["avenant"].ToString() + " " + Environment.NewLine);
                        if (boolFactureReel == true)
                        {
                            boolRequeteErreur = true;
                            //boolean indiqant q'une erreur est survenus preparant
                            //ainsi la requete dans la procedure CocherChampsFacturetrue
                            strRequeteErreur = strRequeteErreur + " and NumContrat <>'" + rowRs["NumContrat"].ToString() + "'";
                            //adocnn.Execute "Delete from FactEntete where FactEntete.NumContrat ='" & rs!NumContrat & "' and FactEntete.Facture = false"
                        }
                    }
                }
                else if (rowRs["typerevision"].ToString().ToUpper() != "N" && rowRs["typerevision"].ToString().ToUpper() != "R" &&
                         rowRs["typerevision"].ToString().ToUpper() != "C" && Convert.ToInt32(General.nz(rowRs["Fac_SansRevision"], 0)) != 1)
                {
                    BLNERROR = true;
                    File.AppendAllText(General.cCheminFichierFactContrat, "le Contrat " + rowRs["NumContrat"].ToString() + ", avenant " + rowRs["avenant"].ToString()
                            + " contient un type de revision invalide \t" + Environment.NewLine);
                    if (boolFactureReel == true)
                    {
                        boolRequeteErreur = true;
                        //boolean indiqant q'une erreur est survenus preparant
                        //ainsi la requete dans la procedure CocherChampsFacturetrue
                        strRequeteErreur = strRequeteErreur + " and NumContrat <>'" + rowRs["NumContrat"].ToString() + "'";
                        //adocnn.Execute "Delete from FactEntete where FactEntete.NumContrat ='" & rs!NumContrat & "' and Facture = false"
                    }

                }
                //recherche du taux de TVA
                if (!string.IsNullOrEmpty(rowRs["CodeArticle"].ToString()))
                {
                    //Recherche du code immeuble afin de trouver le taux TR ou TP (taux réduit
                    //ou taux plein) de l'immeuble.
                    rs3 = ModAdors.fc_OpenRecordSet("SELECT FactEnTete.CodeImmeuble FROM FactEnTete LEFT JOIN FacCorpsEtat ON FactEnTete.NoFacture = FacCorpsEtat.NoFacture Where FacCorpsEtat.FacEssai ='"
                        + rowRs["FacEssai"].ToString() + "'");
                    strCodeImmeuble = "";
                    if (rs3.Rows.Count > 0)
                    {
                        strCodeImmeuble = rs3.Rows[0]["CodeImmeuble"].ToString() + "";
                    }
                    rs3.Clear();
                    ModAdors.Dispose();
                    //recherche du taux TR ou TP (voir commmentaire çi-dessus)
                    rs3 = ModAdors.fc_OpenRecordSet("SELECT Immeuble.CodeTVA From Immeuble Where Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(strCodeImmeuble) + "'", null, null, General.adocnn);
                    strTRouTP = "";
                    if (rs3.Rows.Count > 0)
                    {
                        strTRouTP = rs3.Rows[0]["CodeTVA"].ToString() + "";
                        if (string.IsNullOrEmpty(strTRouTP))
                        {
                            BLNERROR = true;

                            File.WriteAllText(General.cCheminFichierFactContrat, "Il n'y a pas de de codeTVA (TR ou TP) sur l'immeuble " + strCodeImmeuble + Environment.NewLine);
                        }
                    }
                    rs3.Clear();
                    ModAdors.Dispose();
                    //recherche des comptes de classe 7 pour l'article correspondant
                    rs3 = ModAdors.fc_OpenRecordSet("SELECT FacArticle.CG_Num, FacArticle.CG_Num2, AutoLiquidation From FacArticle WHERE FacArticle.CodeArticle='"
                        + rowRs["CodeArticle"].ToString() + "'");

                    //=== modif du 10/06/2014, gestion de l'autoquidation.
                    if ((rs3.Rows.Count > 0))
                    {
                        sAutoLiquidation = rs3.Rows[0]["AutoLiquidation"].ToString();

                        strCompte7 = "";
                        if (rs3.Rows[0]["CG_NUM"].ToString().TrimStart() == rs3.Rows[0]["CG_Num2"].ToString().TrimStart())
                        {
                            strCompte7 = rs3.Rows[0]["CG_NUM"].ToString() + "";
                        }
                        else if (strTRouTP == "TR")
                        {
                            strCompte7 = rs3.Rows[0]["CG_NUM"].ToString() + "";
                        }
                        else if (strTRouTP == "TP")
                        {
                            strCompte7 = rs3.Rows[0]["CG_Num2"].ToString() + "";
                        }
                    }
                    else
                    {
                        sAutoLiquidation = "";
                        strCompte7 = "";
                    }
                    if (!string.IsNullOrEmpty(strTRouTP) || !string.IsNullOrEmpty(strCompte7))
                    {
                        if (!string.IsNullOrEmpty(strCompte7))
                        {
                            for (longTAB = 0; longTAB <= strTABSage.Length - 1; longTAB++)
                            {
                                // adodc7 champs code Comptable
                                if (strCompte7.TrimStart() == strTABSage[longTAB, 1].TrimStart())
                                {
                                    Double.TryParse(strTABSage[longTAB, 2], out var tva);
                                    strTemp2 = tva + "%";

                                    //Taux TVA
                                    General.Execute("UPDATE FacCorpsEtat SET FacCorpsEtat.TVA = '" + tva + "'," + " FacCorpsEtat.Compte = '" + strTABSage[longTAB, 3] + "', FacCorpsEtat.Compte7 ='" + strTABSage[longTAB, 1] + "'" + " Where FacCorpsEtat.FacEssai ='" + rowRs["FacEssai"].ToString() + "'");
                                    break;
                                }
                            }


                        }
                    }
                    rs3.Clear();
                    ModAdors.Dispose();
                    if (sAutoLiquidation.ToUpper() == Variable.cContratAutoLiquidation.ToUpper())
                    {
                        sSQLAutoliqui = "UPDATE FactEnTete Set AutoLiquidation = '" + Variable.cContratAutoLiquidation + "' WHERE NoFacture = '"
                            + rowRs["NoFacture"].ToString() + "'";
                        General.Execute(sSQLAutoliqui);
                    }

                }
                //SUITE:

                // rs.MoveNext();
                j++;
            }

            rs.Clear();
            ModAdors.Dispose();

            rs = null;
            rs2 = null;
            rs3 = null;

            strTABSage = null;
            i = 0;
            Tabe = new string[i + 1];
            return;

            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, "RechercheDesIndices");
            //}
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bRecalcul"></param>
        private void RechercheQuotient(bool bRecalcul = false)/*F1Book1*/
        {
            double oldBaseActualisee = 0;

            try
            {

                rs = new DataTable();

                CalcEngine.CalcEngine ce;

                //selection par date uniquement
                if (!Option1.Checked)
                {
                    sSQL = "Select Indice1, Valeur1 ,Indice2, Valeur2, Indice3, Valeur3," + " Indice4, Valeur4 ,Indice5, Valeur5, Indice6, Valeur6,"
                        + " Indice1Bis, Valeur1Bis, Indice2Bis, Valeur2Bis, Indice3Bis, Valeur3Bis,"
                        + " Indice4Bis, Valeur4Bis, Indice5Bis, Valeur5Bis, Indice6Bis, Valeur6Bis," + " FacEssai, FormuleDeRevision, TypeRevision,"
                        + " BaseRevisee, Ratio, BaseContractuelle, NumContrat, Avenant,"
                        + " BaseActualisee,DateActualisee, CON_TypeContrat,FCE_Coef, codeimmeuble, Fac_SansRevision " + " From FacCorpsEtat"
                        + " Where FacCorpsEtat.CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCorpsEtat.CalanDate <="
                        + General.fc_Fdate(txtFacture2.Value, 1) + "" + " And  FacCorpsEtat.Facture =" + General.fc_bln(false);
                    rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);
                    //selection par date uniquement et par contrat
                }
                else if (Option1.Checked == true)
                {

                    sSQL = "Select Indice1, Valeur1 ,Indice2, Valeur2, Indice3," + " Valeur3," + " Indice4, Valeur4 ,Indice5, Valeur5, Indice6, Valeur6,"
                        + " Indice1Bis, Valeur1Bis, Indice2Bis, Valeur2Bis, Indice3Bis, Valeur3Bis,"
                        + " Indice4Bis, Valeur4Bis, Indice5Bis, Valeur5Bis, Indice6Bis, Valeur6Bis,"
                        + " FacEssai, FormuleDeRevision, TypeRevision, BaseRevisee, Ratio, BaseContractuelle,"
                        + " NumContrat, Avenant ,BaseActualisee,DateActualisee, CON_TypeContrat,FCE_Coef, codeimmeuble, Fac_SansRevision "
                        + " From FacCorpsEtat" + " Where FacCorpsEtat.CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1)
                        + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1) + " And  FacCorpsEtat.Facture ="
                        + General.fc_bln(false) + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text + "'" + " and FacCorpsEtat.NumContrat <='"
                        + txtNumContrat2.Text + "'";
                    rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);
                }
                int k = 0;
                //strtempX = rs.Fields!FacEssai
                foreach (DataRow rowRs in rs.Rows)
                {
                    strTemp2 = rowRs["FacEssai"].ToString();
                    if (rowRs["typerevision"].ToString().ToUpper() == "B" && Convert.ToDouble(General.nz(rowRs["Fac_SansRevision"].ToString(), "0")) != 1)
                    {
                        indi1 = 0;
                        indi2 = 1;
                        Tabe = new string[Convert.ToInt16(indi1) + 1];
                        strFormule = "";
                        strformule2 = "";
                        strTEMP = rowRs["FormuleDeRevision"].ToString().ToUpper();
                        if (!string.IsNullOrEmpty(strTEMP))
                        {

                            while (indi2 <= strTEMP.Length)
                            {
                                switch (General.Mid(strTEMP, indi2, 1))
                                {
                                    case "+":
                                    case "-":
                                    case "*":
                                    case "/":
                                    case "(":
                                    case ")":
                                    case "=":
                                        indi1 = indi1 + 1;
                                        Array.Resize(ref Tabe, indi1 + 1);
                                        Tabe[indi1] = Tabe[indi1] + General.Mid(strTEMP, indi2, 1);
                                        indi1 = indi1 + 1;
                                        if (indi2 == strTEMP.Length)
                                            break; // TODO: might not be correct. Was : Exit Do

                                        Array.Resize(ref Tabe, indi1 + 1);
                                        break;
                                    // modif rachid du 08102002

                                    case " ":
                                        //enleve les espaces
                                        break;
                                    default:
                                        Tabe[indi1] = StdSQLchaine.gFr_NombreAng(Tabe[indi1] + General.Mid(strTEMP, indi2, 1)).ToString();
                                        break;
                                }
                                indi2 = indi2 + 1;

                            }
                            indi1 = 0;
                            for (indi2 = 0; indi2 <= Tabe.Length - 1; indi2++)
                            {
                                switch (Tabe[indi1])
                                {
                                    case "+":
                                    case "-":
                                    case "*":
                                    case "(":
                                    case ")":
                                    case "=":
                                        break;
                                    case "/":
                                        break;
                                    default:
                                        if (VerifieNumText(ref Tabe[indi1]) == false && !string.IsNullOrEmpty(Tabe[indi1]))
                                        {
                                            for (i = 0; i <= rs.Columns.Count - 1; i++)
                                            {
                                                //clearfin' If Not (IsNull(rs.Fields(i).Value)) And (rs.Fields(i).Value) <> "" Then
                                                if (General.Left(rs.Columns[i].ColumnName, 6) == "Indice")
                                                {
                                                    //la boucle çi_dessous permet de rechercher dans le recordset un
                                                    //libelle d'indice égale au contenu du tableau.
                                                    if (Tabe[indi1].ToUpper() == rowRs[i].ToString().ToUpper())
                                                    {

                                                        if (General.Right(rs.Columns[i].ColumnName, 3) == "Bis")
                                                        {
                                                            strTemp3 = "Valeur" + General.Right(rs.Columns[i].ColumnName, 4);
                                                            Tabe[indi1] = rowRs[strTemp3].ToString();
                                                            break;
                                                        }
                                                        else
                                                        {
                                                            strTemp3 = "Valeur" + General.Right(rs.Columns[i].ColumnName, 1);
                                                            Tabe[indi1] = rowRs[strTemp3].ToString();
                                                            break;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                }
                                strFormule = strFormule + Tabe[indi1];
                                switch (Tabe[indi1])
                                {
                                    case "(":
                                    case ")":
                                        break;
                                    case "*":
                                        strformule2 = strformule2 + " x ";
                                        break;
                                    case "+":
                                    case "-":
                                    case "/":
                                        strformule2 = strformule2 + " " + Tabe[indi1] + " ";
                                        break;
                                    default:
                                        strformule2 = strformule2 + Tabe[indi1];
                                        break;
                                }
                                //inserer un message ou cas ou l'utilisateur a oubliée de
                                //saisir une valeur pour So qui est par défaut = 0

                                indi1 = indi1 + 1;
                            }
                            if (rowRs["BaseRevisee"] != DBNull.Value && Math.Abs(Convert.ToDouble(rowRs["BaseRevisee"].ToString())) > 0.0000001)
                            {

                                strFormule = "(" + strFormule + ")" + "*" + rowRs["BaseRevisee"].ToString();
                                strformule2 = strformule2 + " x " + rowRs["BaseRevisee"].ToString();

                                ce = new CalcEngine.CalcEngine();
                                doubtemp = (double)ce.Evaluate(strFormule);
                                doubtemp = General.FncArrondir(doubtemp);

                                sSQL = "Update FacCorpsEtat set Resultat =" + doubtemp + " where FacEssai ='" + strTemp2
                                    + "' and Facture = " + General.fc_bln(false);
                                General.Execute(sSQL);
                                if (boolFactureReel == true)
                                {
                                    General.SQL = "UPDATE Contrat SET Contrat.BaseActualisee = " + doubtemp + ", Contrat.DateActualisee="
                                        + General.fc_Fdate(txtDateFacture.Value, 1) + "  Where Contrat.NumContrat ='"
                                        + rowRs["NumContrat"].ToString() + "' and Contrat.Avenant ='" + rowRs["avenant"].ToString() + "'";
                                    General.Execute(sSQL);
                                }
                                //adocnn.Execute "Update FacCorpsEtat set Resultat =" & doubTemp & " where FacEssai ='" & strTemp2 & "' and Facture = true"

                            }
                            else if (rowRs["BaseContractuelle"] != DBNull.Value && Math.Abs(Convert.ToDouble(General.nz(rowRs["BaseContractuelle"].ToString(), 0))) > 0.0000001)
                            {
                                strFormule = "(" + strFormule + ")" + "*" + rowRs["BaseContractuelle"].ToString();
                                strformule2 = strformule2 + " x " + rowRs["BaseContractuelle"].ToString();


                                ce = new CalcEngine.CalcEngine();
                                doubtemp = (double)ce.Evaluate(strFormule);


                                //doubtemp =Convert.ToDouble(strFormule);
                                doubtemp = General.FncArrondir(doubtemp);

                                sSQL = "Update FacCorpsEtat set Resultat =" + doubtemp + ", BaseActualisee =" + doubtemp + " where FacEssai ='" + strTemp2 + "' and Facture =" + General.fc_bln(false);
                                General.Execute(sSQL);
                                if (boolFactureReel == true)
                                {
                                    sSQL = "UPDATE Contrat SET Contrat.BaseActualisee = " + doubtemp + ", Contrat.DateActualisee="
                                        + General.fc_Fdate(txtDateFacture.Value, 1) + "  Where Contrat.NumContrat ='"
                                        + rowRs["NumContrat"].ToString() + "' and Contrat.Avenant ='"
                                        + rowRs["avenant"].ToString() + "'";
                                    General.Execute(sSQL);
                                }

                            }
                            //strFormule = "(" & strFormule & ")" & "*" & rs.Fields!Ratio
                            //modif rachid
                            strFormule = doubtemp + "*" + rowRs["Ratio"].ToString();
                            strformule2 = strformule2 + " x " + rowRs["Ratio"].ToString();


                            ce = new CalcEngine.CalcEngine();
                            doubtemp = (double)ce.Evaluate(strFormule);
                            doubtemp = General.FncArrondir(doubtemp);


                            sSQL = "Update FacCorpsEtat set ResultatRatio =" + doubtemp + ", formuleimpression ='" + strformule2
                                + "' where FacEssai ='" + strTemp2 + "'and Facture =" + General.fc_bln(false);
                            General.Execute(sSQL);


                        }


                        //== sans revision
                    }
                    else if (rowRs["typerevision"].ToString().ToUpper() == "N")
                    {
                        strFormule = rowRs["BaseContractuelle"].ToString() + "*" + rowRs["Ratio"].ToString();

                        ce = new CalcEngine.CalcEngine();
                        doubtemp = (double)ce.Evaluate(strFormule);
                        doubtemp = General.FncArrondir(doubtemp);
                        sSQL = "Update FacCorpsEtat set ResultatRatio =" + doubtemp + " where FacEssai ='" + strTemp2 + "' and facture =" + General.fc_bln(false);
                        General.Execute(sSQL);
                        //ssql = "UPDATE FacCorpsEtat SET FacCorpsEtat.PCdu = 2, FacCorpsEtat.Au = 2" _
                        //& "WHERE (((FacCorpsEtat.FacEssai)="6"))"
                        // adocnn.Execute ssql
                        //'inserer un message si il n' y a aucun type de revision


                    }
                    else if (rowRs["typerevision"].ToString().ToUpper() == "R"
                            || Convert.ToInt16(General.nz(rowRs["Fac_SansRevision"].ToString(), 0)) == 1)
                    {
                        // type de facture sans révision divisée par un ratio sur base actualisée.
                        if (rowRs["BaseActualisee"] != DBNull.Value && Convert.ToDouble(General.nz(rowRs["BaseActualisee"].ToString(), 0)) > 0)
                        {

                            strFormule = rowRs["BaseActualisee"].ToString() + "*" + rowRs["Ratio"].ToString();


                        }
                        else
                        {
                            strFormule = rowRs["BaseContractuelle"].ToString() + "*" + rowRs["Ratio"].ToString();

                        }


                        ce = new CalcEngine.CalcEngine();
                        doubtemp = (double)ce.Evaluate(strFormule);
                        doubtemp = General.FncArrondir(doubtemp);

                        // modif du 10082002

                        sSQL = "Update FacCorpsEtat set ResultatRatio =" + doubtemp + "" + " where FacEssai ='" + strTemp2
                            + "' and facture =" + General.fc_bln(false);
                        General.Execute(sSQL);

                        //'inserer un message si il n' y a aucun type de revision


                    }
                    else if (rowRs["typerevision"].ToString().ToUpper() == "C")
                    {
                        if (rowRs["FCE_Coef"] == DBNull.Value || Convert.ToInt16(rowRs["FCE_Coef"].ToString()) == 0)
                        {
                            BLNERROR = true;
                            File.WriteAllText(General.cCheminFichierFactContrat, "Le contrat n°" + rowRs["NumContrat"].ToString() + " avenant n°" + rowRs["avenant"].ToString() + " n'a pas de coefficent" + Environment.NewLine);
                            if (boolFactureReel == true)
                            {
                                boolRequeteErreur = true;
                                //boolean indiquant q'une erreur est survenus préparant
                                //ainsi la requete dans la procedure CocherChampsFacturetrue
                                strRequeteErreur = strRequeteErreur + " and NumContrat <>'" + rowRs["NumContrat"].ToString() + "'";
                                rs2.Clear();

                            }
                        }

                        //--Actualise la base contractuelle ou reactualise la base actualisée
                        //--par le coefficient fixe se trouvant dans la table contrat.

                        if (rowRs["BaseActualisee"] == DBNull.Value || Convert.ToInt16(rowRs["BaseActualisee"].ToString()) == 0)
                        {
                            strFormule = rowRs["BaseContractuelle"].ToString() + "*" + rowRs["FCE_Coef"].ToString();
                            //met à jour le champs base actualisé pour l'état.
                            //rs.Fields!BaseActualisee = rs.Fields!BaseContractuelle
                            oldBaseActualisee = Convert.ToDouble(rowRs["BaseContractuelle"].ToString());
                        }
                        else
                        {
                            strFormule = rowRs["BaseActualisee"].ToString() + "*" + rowRs["FCE_Coef"].ToString();

                            oldBaseActualisee = Convert.ToDouble(rowRs["BaseActualisee"].ToString());
                        }




                        // modif du 09 01 2003 si un nouveau calcul est demandée alors on
                        //  récupere seulement la  base contractuelle.
                        if (bRecalcul == true)
                        {
                            strFormule = rowRs["BaseContractuelle"].ToString() + "*" + rowRs["FCE_Coef"].ToString();
                            //met à jour le champs base actualisé pour l'état.
                            //rs.Fields!BaseActualisee = rs.Fields!BaseContractuelle
                            oldBaseActualisee = Convert.ToDouble(rowRs["BaseContractuelle"].ToString());
                        }
                        ce = new CalcEngine.CalcEngine();
                        doubtemp = (double)ce.Evaluate(strFormule);
                        doubtemp = General.FncArrondir(doubtemp);
                        oldBaseActualisee = General.FncArrondir(oldBaseActualisee);
                        sSQL = "Update FacCorpsEtat set Resultat =" + doubtemp + ", BaseActualisee =" + doubtemp + " , basecontractuelle =" + oldBaseActualisee + " where FacEssai ='" + strTemp2 + "' and Facture = " + General.fc_bln(false);
                        General.Execute(sSQL);

                        if (boolFactureReel == true)
                        {
                            sSQL = "UPDATE Contrat SET Contrat.BaseActualisee = " + doubtemp + ","
                                + " Contrat.DateActualisee=" + General.fc_Fdate(txtDateFacture.Value, 1) + " , BaseContractuelle ="
                                + oldBaseActualisee + " , DateBase=" + General.fc_Fdate(Convert.ToDateTime(rowRs["DateActualisee"].ToString()), 1)
                                + "  Where Contrat.NumContrat ='" + rowRs["NumContrat"].ToString() + "'" + " and Contrat.Avenant ='"
                                + rowRs["avenant"].ToString() + "'";
                            General.Execute(sSQL);
                        }


                        //--calcul du ratio sur la base actualisée
                        strFormule = "(" + strFormule + ")" + "*" + rowRs["Ratio"].ToString();
                        // F1Book1.set_FormulaRC(1, 1, strFormule);
                        ce = new CalcEngine.CalcEngine();
                        doubtemp = (double)ce.Evaluate(strFormule);
                        doubtemp = General.FncArrondir(doubtemp);
                        sSQL = "Update FacCorpsEtat set ResultatRatio =" + doubtemp + " where FacEssai ='" + strTemp2 + "'and Facture =" + General.fc_bln(false);

                        General.Execute(sSQL);

                    }
                    // fc_MajSecteur nz(rs!codeImmeuble, ""), rs!facessai
                    // rs.MoveNext();
                    k++;
                }
                rs = null;
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "RechercheQuotient");
            }
        }
        private void fc_MajSecteur(ref string sCode, ref int lFacessai)
        {
            sSQL = "";
            sSQL = "SELECT Personnel.SEC_Code " + " FROM IntervenantImm_INM INNER JOIN Personnel"
                + " ON IntervenantImm_INM.intervenant_INM = Personnel.Matricule"
                + " Where (((IntervenantImm_INM.Codeimmeuble_IMM) ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "')"
                + " And ((IntervenantImm_INM.Nordre_INM) = 1))" + " ORDER BY IntervenantImm_INM.Noauto";



            var rsSec = new DataTable();
            rsSec = ModAdors.fc_OpenRecordSet(sSQL);
            if (rsSec.Rows.Count > 0)
            {
                sSQL = " update facCorpsetat set FCE_secteur ='" + rsSec.Rows[0]["SEC_Code"].ToString()
                    + "' where facessai =" + lFacessai;
                General.Execute(sSQL);
            }
            else
            {
                File.WriteAllText(General.cCheminFichierFactContrat, "L'immeuble " + sCode + " n'a pas de secteur.");
                BLNERROR = true;
            }
            rsSec.Clear();
            ModAdors.Dispose();
            // rsSec = null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Temp"></param>
        /// <returns></returns>
        private bool VerifieNumText(ref string Temp)
        {
            Program.SaveException(null, $"UserDocImprimeFacture=>VerifieNumText=>Temp= {Temp}");
            bool functionReturnValue = false;
            indi3 = 1;
            if (General.IsNumeric(Temp))
            {
                return true;
            }
            while (indi3 < Temp?.Length)
            {
                if (General.Mid(Temp, indi3, 1) == "F")
                {
                    functionReturnValue = false;
                    break; // TODO: might not be correct. Was : Exit Do
                }
                indi3 = indi3 + 1;
            }
            return functionReturnValue;
        }
        private void Command2_Click(object sender, EventArgs e)
        {
            string sReturn = null;
            string strRequete = null;
            //var _with3 = Adodc1;

            sSQL = "SELECT FacPied.NumContrat, FacPied.NoFacture," + " FacPied.TotalHT, FacPied.TauxTVA, FacPied.MontantTVA,"
                + " FacPied.TTC" + " FROM FacPied RIGHT JOIN FactEnTete ON FacPied.NoFacture" + " = FactEnTete.NoFacture"
                + " where  FactEnTete.DateFacture =" + General.fc_Fdate(txtDateFacture.Value, 1) + " ";

            if (Option1.Checked == true)
            {
                sSQL = sSQL + " and FacPied.NumContrat >='" + txtNumContrat1.Text + "' and FacPied.NumContrat <='" + txtNumContrat2.Text + "'";
            }
            // var _with4 = Adodc1;
            //_with4.ConnectionString = General.adocnn.ConnectionString;
            sSQL = sSQL + "ORDER BY FacPied.NoFacture";
            //_with4.RecordSource = sSQL;
            //_with4.Refresh();
            DataTable Adodc1 = new DataTable();
            Adodc1 = ModAdors.fc_OpenRecordSet(sSQL);
            SSOleDBGrid1.DataSource = Adodc1;

            Application.DoEvents();
            lblTot.Text = "TOTAL DES FACTURES: ";
            lblTot.Text = lblTot.Text + " " + Adodc1.Rows.Count /*_with4.Recordset.RecordCount*/;
            Application.DoEvents();



            strDate1 = string.Format(Convert.ToDateTime(txtFacture1.Value).ToString(), "yyyy,mm,dd");
            strdate2 = string.Format(Convert.ToDateTime(txtFacture2.Value).ToString(), "yyyy,mm,dd");
            string sDateFacture = string.Format(Convert.ToDateTime(txtDateFacture.Value).ToString(), "yyyy,mm,dd");


            if (Option1.Checked == true)
            {
                strRequete = "{FactEnTete.CalanDate}>= Date ('" + strDate1 + "')" + " and {FactEnTete.CalanDate}<= Date ('"
                    + strdate2 + "')" + " and {FactEnTete.NumContrat} >='" + txtNumContrat1.Text + "'"
                    + " and {FactEnTete.NumContrat} <='" + txtNumContrat2.Text + "'";

            }
            else
            {
                strRequete = "{FactEnTete.CalanDate}>= Date ('" + strDate1 + "')" + " and {FactEnTete.CalanDate}<= Date ('" + strdate2 + "')";


            }
            ReportDocument RD = new ReportDocument();
            RD.Load(General.CHEMINEUROONLY);
            CrystalReportFormView C = new CrystalReportFormView(RD, strRequete);
            C.Show();


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ControleFacture()
        {
            blnRecalcul = false;
            blnFacReel = false;
            if (!(General.IsDate(txtFacture1.Value)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFacture1.Focus();
                return false;
            }
            else if (!(General.IsDate(txtFacture2.Value)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFacture1.Focus();
                return false;
            }
            else if (!(General.IsDate(txtDateFacture.Value)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateFacture.Focus();
                return false;
            }
            if (txtFacture1.Value.Month != txtFacture2.Value.Month)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La période de facturation s'effectue sur un seul mois.", "Date Invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFacture2.Focus();
                return false;
            }
            if (txtFacture1.Value.Year != txtFacture2.Value.Year)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La période de facturation s'effectue sur un seul mois.", "Date Invalide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtFacture2.Focus();
                return false;
            }

            return true;
        }


        /// <summary>
        /// TODO :======> cette fct va etre vérifié par Mondir 
        /// je l'ai changé par load de crystaReportDocument et l'affiché
        /// </summary>
        /// <param name="sFile"></param>
        /// <param name="sSelectionFormula"></param>
        private void fc_ImprimeBacs(ref string sFile, ref string sSelectionFormula)
        {
            //TODO: Verifier le code suivant dans un environement réel afin de tester l'impression sur les 3 Bacs
            try
            {
                int copies = 1;
                using (ReportDocument rd = new ReportDocument())
                {
                    rd.RecordSelectionFormula = sSelectionFormula;
                    rd.Load(sFile);

                    rd.PrintOptions.PrinterName = @"\\SVR-GESTEN\Lexmark T620";
                    rd.PrintOptions.PaperOrientation = PaperOrientation.Portrait;

                    rd.PrintOptions.PaperSource = PaperSource.Upper;
                    rd.PrintToPrinter(copies, false, 1, 1);

                    rd.PrintOptions.PaperSource = PaperSource.Lower;
                    rd.PrintToPrinter(copies, false, 1, 1);

                    rd.PrintOptions.PaperSource = PaperSource.Middle;
                    rd.PrintToPrinter(copies, false, 1, 1);
                }

            }
            catch (Exception err)
            {
                Program.SaveException(err);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(err.ToString());
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option1_CheckedChanged(object sender, EventArgs e)
        {
            if (Option1.Checked == true)
            {
                //Option1.Value = False
                txtNumContrat1.Enabled = true;
                txtNumContrat2.Enabled = true;
                txtNumContrat1.Focus();
                return;
            }
            if (Option1.CheckState == 0)
            {
                //Option1.Value = True
                txtNumContrat1.Enabled = false;
                txtNumContrat2.Enabled = false;
                return;
            }
        }
        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            string sReturn = null;
            string sSelect = null;

            if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.GetCellValue("nofacture").ToString()))
            {

                sSelect = "{FactEnTete.Nofacture}='" + SSOleDBGrid1.ActiveRow.GetCellValue("nofacture") + "'";

                if (General.sExportPdfContrat == "1")
                {

                    sReturn = devModeCrystal.fc_ExportCrystalSansFormule(sSelect, General.gsRpt + "FacturationEuroOnly-V9.rpt");
                    //sReturn = fc_ExportCrystalSansFormule(strRequete, CHEMINEUROONLY)

                    if (!string.IsNullOrEmpty(sReturn))
                    {

                        ModuleAPI.Ouvrir(sReturn);
                        Cursor.Current = Cursors.Arrow;

                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur a été décelé, veuillez consulter votre fournisseur.", "Opération annulée"/*, Convert.ToDouble(Err().Description)*/);

                    }
                }
                else
                {
                    ReportDocument CrystalReport1 = new ReportDocument();

                    if (!CrystalReport1.IsLoaded)
                    {

                        CrystalReport1.Load(General.CHEMINEUROONLY);
                    }




                    //// If SSOleDBGrid1.Columns("nofacture").value <> "" Then

                    CrystalReportFormView CV = new CrystalReportFormView(CrystalReport1, sSelect);
                    CV.Show();
                    //// "{FactEnTete.Nofacture}='" & SSOleDBGrid1.Columns("nofacture").value & "'"
                    //CrystalReport1.Action = 1;
                    //CrystalReport1.SelectionFormula = "";
                    //CrystalReport1.ReportFileName = "";

                }
            }
        }
        private void SSOleDBGrid1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            int i = 0;
            //var _with5 = SSOleDBGrid1;
            if (e.Row.Cells["TTC"].Value != DBNull.Value && Convert.ToInt32(e.Row.Cells["TTC"].Value) == 0)
            {
                // SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.Red /*.CellStyleSet("Erreur", _with5.Row);*/;

                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateFacture_Validated(object sender, EventArgs e)
        {

            if (txtDateFacture.Value.ToString() == "Rectif")//jamais le fair!!
            {
                cmdRectif.Visible = cmdRectif.Visible == false;
                return;
            }
            if (!(General.IsDate(txtDateFacture.Value)) && !string.IsNullOrEmpty(txtDateFacture.Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            else if (!string.IsNullOrEmpty(txtDateFacture.Value.ToString()))
            {
                txtDateFacture.Value = Convert.ToDateTime(string.Format(txtDateFacture.Text.ToString(), "dd/mm/yyyy"));
            }
            return;

        }
        /// <summary>
        ///tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFacture1_Validated(object sender, EventArgs e)
        {

            if (!(General.IsDate(txtFacture1.Value)) && !string.IsNullOrEmpty(txtFacture1.Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            if (!string.IsNullOrEmpty(txtFacture1.Value.ToString()))
            {
                txtFacture1.Value = Convert.ToDateTime(string.Format(txtFacture1.Value.ToString(), "dd/mm/yyyy"));
                txtFacture2.Value = Convert.ToDateTime(General.CalculMaxiJourMois(Convert.ToInt16(txtFacture1.Value.Month), txtFacture1.Value.Year) + "/" + txtFacture1.Value.Month + "/" + txtFacture1.Value.Year);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void NumeroFacture()
        {
            string sSQLbaseCtr = null;

            rs = new DataTable();
            rs = ModAdors.fc_OpenRecordSet("SELECT Max(FacCalendrier.BaseNoFacture) AS MaxDeNoFacture FROM FacCalendrier");
            // If rs.EOF = False Then
            if (rs.Rows[0]["MaxDeNoFacture"] == DBNull.Value || Convert.ToInt32(rs.Rows[0]["MaxDeNoFacture"].ToString()) == 0)
            {

                //=== modif du 07 06 2018, recherche dans la table lien la valeur de base.
                //intT = 500000
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    sSQLbaseCtr = "SELECT     adresse From LienV2 WHERE     (Code = 'BaseCptFactureContrat')";
                else
                    sSQLbaseCtr = "SELECT     adresse From Lien WHERE     (Code = 'BaseCptFactureContrat')";
                sSQLbaseCtr = General.nz(ModAdors.fc_ADOlibelle(sSQLbaseCtr), 0).ToString();
                intT = Convert.ToInt32(sSQLbaseCtr);

            }
            else
            {
                intT = Convert.ToInt32(rs.Rows[0]["MaxDeNoFacture"].ToString());
                intT = intT + 1;
            }
            strTEMP = "CA" + intT;
            //If Len(strtemp) < 6 Then
            // Do While Len(strtemp) < 6
            //  strtemp = "0" & strtemp
            // Loop
            // End If
            // strtemp = "CA" & strtemp
            rs.Clear();
            ModAdors.Dispose();
            //option par date uniquement
            if (Option1.Checked == false)
            {
                //rs.Open "SELECT Distinct FacCalendrier.NumContrat From FacCalendrier WHERE  FacCalendrier.Date >=#" & Format(txtFacture1, "mm/dd/yyyy") & "# AND FacCalendrier.Date<=#" & Format(txtFacture2, "mm/dd/yyyy") & "# and nonFacturable = False", adocnn
                sSQL = "SELECT Distinct FacCalendrier.NumContrat From FacCalendrier" + " WHERE  FacCalendrier.Date >="
                    + General.fc_Fdate(txtFacture1.Value, 1) + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and nonFacturable =" + General.fc_bln(false) + " and facture = " + General.fc_bln(false) + "" + " and histo = "
                    + General.fc_bln(false) + " and resiliee = " + General.fc_bln(false);

                rs = ModAdors.fc_OpenRecordSet(sSQL);

                //option par date et par contrat
            }
            else if (Option1.Checked == true)
            {
                //rs.Open "SELECT Distinct FacCalendrier.NumContrat From FacCalendrier WHERE  FacCalendrier.Date >=#" & Format(txtFacture1, "mm/dd/yyyy") & "# AND FacCalendrier.Date<=#" & Format(txtFacture2, "mm/dd/yyyy") & "# and NumContrat >='" & txtNumContrat1.Text & "' and NumContrat <='" & txtNumContrat2.Text & "' and nonFacturable = False", adocnn
                rs = ModAdors.fc_OpenRecordSet("SELECT Distinct FacCalendrier.NumContrat" + " From FacCalendrier" + " WHERE  FacCalendrier.Date >="
                    + General.fc_Fdate(txtFacture1.Value, 1) + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and nonFacturable =" + General.fc_bln(false) + "and facture = " + General.fc_bln(false) + "" + " and histo = "
                    + General.fc_bln(false) + " and resiliee = " + General.fc_bln(false) + " and NumContrat >='" + txtNumContrat1.Text
                    + "' and NumContrat <='" + txtNumContrat2.Text + "'");

            }
            j = 0;
            foreach (DataRow rowRs in rs.Rows)
            {

                longRetourneEnr = General.Execute("Update FacCalendrier set FacCalendrier.NoFacture ='" + strTEMP + "', FacCalendrier.BaseNoFacture ="
                    + intT + " where (FacCalendrier.NumContrat ='" + rowRs["NumContrat"].ToString() + "' and FacCalendrier.Date >= "
                    + General.fc_Fdate(txtFacture1.Value, 1) + " AND FacCalendrier.Date <= " + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and FacCalendrier.NoFacture = '' and FacCalendrier.nonFacturable =" + General.fc_bln(false) + " and histo = "
                    + General.fc_bln(false) + " and resiliee = " + General.fc_bln(false) + ")" + " or (FacCalendrier.NumContrat ='"
                    + rowRs["NumContrat"].ToString() + "'" + " and FacCalendrier.Date >= " + General.fc_Fdate(txtFacture1.Value, 1)
                    + " AND FacCalendrier.Date <= " + General.fc_Fdate(txtFacture2.Value, 1)
                    + " and FacCalendrier.NoFacture is null and FacCalendrier.nonFacturable = " + General.fc_bln(false)
                    + " and histo = " + General.fc_bln(false) + " and resiliee = " + General.fc_bln(false) + ")");





                //la condition çi-dessous vérifie le nombre d'enregistrements retournées
                //dans notre cas 0 ou 1
                if (longRetourneEnr != 0)
                {
                    intT = intT + 1;
                    //strtemp = intT
                    //If Len(strtemp) < 6 Then
                    //Do While Len(strtemp) < 6
                    // strtemp = "0" & strtemp
                    // Loop
                    //End If
                    strTEMP = "CA" + intT;
                }
                //rs.MoveNext();
                j++;
            }
            rs.Clear();
            ModAdors.Dispose();
            rs = null;
        }
        private void NumeroFactureTemporaire()
        {

            try
            {
                rs = new DataTable();
                rs = ModAdors.fc_OpenRecordSet("SELECT Max(FacCalendrier.BaseNoFactureTemp) AS MaxDeNoFacture FROM FacCalendrier");
                // If rs.EOF = False Then
                if (string.IsNullOrEmpty(rs.Rows[0]["MaxDeNoFacture"].ToString()))
                {
                    intT = 1;
                    strTEMP = "C99" + intT;
                }
                else
                {
                    intT = Convert.ToInt32(rs.Rows[0]["MaxDeNoFacture"].ToString() + "");
                    intT = intT + 1;
                    strTEMP = "C99" + intT;
                }
                rs.Clear();
                ModAdors.Dispose();
                //option par date uniquement
                if (Option1.Checked == false)
                {
                    // Ahmed le 10/04/2021 : Ajout de order by FacCalendrier.NumContrat pour avoir le meme clasement des contrat comme VB (TODO: ajouter la meme clause sur VB)
                    strReq = "SELECT Distinct FacCalendrier.NumContrat,FacCalendrier.avenant From FacCalendrier" + " WHERE  FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value, 1) + "" + " AND FacCalendrier.Date<=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + "" + " and nonFacturable =" + General.fc_bln(false) + "and histo = " + General.fc_bln(false) + ""
                        + " and resiliee = " + General.fc_bln(false) + " order by FacCalendrier.NumContrat, FacCalendrier.avenant";

                    //option par date et par contrat
                }
                else if (Option1.Checked == true)
                {
                    //UPGRADE_WARNING: Couldn't resolve default property of object fc_bln(False). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    strReq = "SELECT Distinct FacCalendrier.NumContrat,FacCalendrier.avenant From FacCalendrier" + " WHERE  FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " AND FacCalendrier.Date<="
                        + General.fc_Fdate(txtFacture2.Value, 1) + "" + " and NumContrat >='" + txtNumContrat1.Text + "'" + " and NumContrat <='" + txtNumContrat2.Text + "'" + " and nonFacturable ="
                        + General.fc_bln(false) + " and histo =" + General.fc_bln(false) + "" + " and resiliee =" + General.fc_bln(false)
                        + " order by FacCalendrier.NumContrat,FacCalendrier.avenant";

                    // Fin modification Ahmed
                }
                rs = ModAdors.fc_OpenRecordSet(strReq);
                int j = 0;

                foreach (DataRow rowRs in rs.Rows)
                {

                    sSQL = "Update FacCalendrier set NoFactureTemp ='" + strTEMP + "'," + " BaseNoFactureTemp =" + intT
                           + " where (FacCalendrier.NumContrat ='" + rowRs["NumContrat"].ToString() + "'" + " and FacCalendrier.Date >= "
                           + General.fc_Fdate(txtFacture1.Value, 1) + " AND FacCalendrier.Date <= " + General.fc_Fdate(txtFacture2.Value, 1)
                           + " and FacCalendrier.nonFacturable =" + General.fc_bln(false) + " and histo =" + General.fc_bln(false)
                           + " and resiliee =" + General.fc_bln(false) + " )" + " or (FacCalendrier.NumContrat ='" + rowRs["NumContrat"].ToString()
                           + "'" + " and FacCalendrier.Date >= " + General.fc_Fdate(txtFacture1.Value, 1)
                           + " AND FacCalendrier.Date <= " + General.fc_Fdate(txtFacture2.Value, 1) + " and FacCalendrier.nonFacturable ="
                           + General.fc_bln(false) + " and histo =" + General.fc_bln(false) + " and resiliee =" + General.fc_bln(false) + ")";

                    General.Execute(sSQL);


                    intT = intT + 1;
                    strTEMP = "C991" + intT;
                    //rs.MoveNext();
                    j++;
                }

                rs.Clear();
                ModAdors.Dispose();
                rs = null;
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "NumeroFactureTemporaire");
            }


        }
        private DateTime fc_ModeRegl(DateTime dtDateFacture, DateTime dtTemp, string sCodeImmeuble, bool bCalcEcheanceImm = false)
        {
            DateTime functionReturnValue = default(DateTime);

            string sReq = null;
            string sModeReg = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                if (General.sModeRegClient == "1")
                {
                    //UPGRADE_WARNING: Couldn't resolve default property of object gFr_DoublerQuote(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sReq = "SELECT     Table1.CodeReglement" + " FROM   Immeuble INNER JOIN" + " Table1 ON Immeuble.Code1 = Table1.Code1"
                        + "  WHERE     Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }
                else
                {
                    //UPGRADE_WARNING: Couldn't resolve default property of object gFr_DoublerQuote(). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sReq = "SELECT CodeReglement_IMM FROM immeuble " + " WHERE codeimmeuble='"
                        + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }

                if (General.sModeRegFacContrat == "1" || bCalcEcheanceImm == true)
                {

                    sReq = ModAdors.fc_ADOlibelle(sReq);

                    if (!string.IsNullOrEmpty(sReq))
                    {
                        sModeReg = sReq;
                        functionReturnValue = General.CalcEch(dtDateFacture, sModeReg);
                    }
                    else
                    {
                        functionReturnValue = dtTemp;
                    }

                }
                else
                {
                    functionReturnValue = dtTemp;

                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, ";fc_ModeRegl");
            }
            return functionReturnValue;

        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void facturePied()
        {
            string sReqSql = null;
            DateTime dtDateEchDefaut = default(DateTime);

            try
            {

                rs = new DataTable();
                rs2 = new DataTable();
                rs3 = new DataTable();
                dtTemp = txtDateFacture.Value;

                //la boucle çi-dessous recherche le dernier jour du mois par rapport à la date de facture.
                dtTemp = new DateTime(year: txtDateFacture.Value.Year, month: txtDateFacture.Value.Month, day: DateTime.DaysInMonth(txtDateFacture.Value.Year, txtDateFacture.Value.Month));
                dtDateEchDefaut = dtTemp;

                //=== Recherche des factures à calculer.
                //selection par date uniquement
                if (!Option1.Checked)
                {
                    //sSQL = "SELECT FacCorpsEtat.NoFacture, FacCorpsEtat.CodeArticle " _
                    //& " FROM FacCorpsEtat Where FacCorpsEtat.CalanDate >=" & fc_Fdate(txtFacture1, 1) _
                    //& " And FacCorpsEtat.CalanDate <=" & fc_Fdate(txtFacture2, 1) _
                    //& " And  FacCorpsEtat.Facture =" & fc_bln(False)

                    //=== modif du 9 dec 2015 ajout du champ CalcEcheanceImm
                    sSQL = "SELECT FacCorpsEtat.NoFacture, FacCorpsEtat.CodeArticle , FactEnTete.CalcEcheanceImm "
                        + " FROM         FacCorpsEtat INNER JOIN" + " FactEnTete ON FacCorpsEtat.NoFacture = FactEnTete.NoFacture "
                        + " Where FacCorpsEtat.CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCorpsEtat.CalanDate <="
                        + General.fc_Fdate(txtFacture2.Value, 1) + " And  FacCorpsEtat.Facture =" + General.fc_bln(false);


                    rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);

                    //selection par date uniquement et par contrat
                }
                else if (Option1.Checked == true)
                {
                    //sSQL = "SELECT FacCorpsEtat.NoFacture, FacCorpsEtat.CodeArticle" _
                    //& " FROM FacCorpsEtat" _
                    //& " Where FacCorpsEtat.CalanDate >= " & fc_Fdate(txtFacture1, 1) _
                    //& " And FacCorpsEtat.CalanDate <=" & fc_Fdate(txtFacture2, 1) _
                    //& " And  FacCorpsEtat.Facture =" & fc_bln(False) _
                    //& " and FacCorpsEtat.NumContrat >='" & txtNumContrat1 _
                    //& "' and FacCorpsEtat.NumContrat <='" & txtNumContrat2 & "'"

                    //=== modif du 9 dec 2015 ajout du champ CalcEcheanceImm
                    //UPGRADE_WARNING: Couldn't resolve default property of object fc_bln(False). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sSQL = "SELECT FacCorpsEtat.NoFacture, FacCorpsEtat.CodeArticle , FactEnTete.CalcEcheanceImm "
                        + " FROM         FacCorpsEtat INNER JOIN" + " FactEnTete ON FacCorpsEtat.NoFacture = FactEnTete.NoFacture"
                        + " Where FacCorpsEtat.CalanDate >= " + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCorpsEtat.CalanDate <="
                        + General.fc_Fdate(txtFacture2.Value, 1) + " And  FacCorpsEtat.Facture =" + General.fc_bln(false)
                        + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text + "' and FacCorpsEtat.NumContrat <='"
                        + txtNumContrat2.Text + "'";

                    rs = ModAdors.fc_OpenRecordSet(sSQL);

                }

                //=== Debut du calcul du pied de facture (boucle pour chaque facture).
                // while (rs.EOF == false)
                foreach (DataRow r in rs.Rows)
                {
                    if (!String.IsNullOrEmpty(ModAppel.sCodeReglementDefContrat))
                    {
                        dtTemp = General.CalcEch(txtDateFacture.Value, ModAppel.sCodeReglementDefContrat);
                    }

                    else if (General.sModeRegFacContrat == "1"
                        || Convert.ToBoolean(General.nz(r["CalcEcheanceImm"], false)) == true)
                    {
                        sReqSql = "SELECT  CodeImmeuble" + " From FactEnTete"
                                                         + " WHERE  NoFacture = '" + r["NoFacture"] + "'";
                        sReqSql = ModAdors.fc_ADOlibelle(sReqSql);

                        dtTemp = fc_ModeRegl(txtDateFacture.Value, dtTemp, sReqSql, Convert.ToBoolean(General.nz(r["CalcEcheanceImm"], false)));
                    }
                    else
                    {
                        dtTemp = dtDateEchDefaut;
                    }

                    //=== recherche de la somme des factures pour chaque contrat.
                    rs2 = ModAdors.fc_OpenRecordSet("SELECT Sum(FacCorpsEtat.ResultatRatio) AS SommeDeResultatRatio "
                        + " From FacCorpsEtat GROUP BY FacCorpsEtat.NoFacture" + " HAVING FacCorpsEtat.NoFacture='" + r["NoFacture"] + "'");
                    strTEMP = rs2.Rows[0]["SommeDeResultatRatio"].ToString();
                    //Montant H.T.
                    rs2.Clear();
                    ModAdors.Dispose();
                    strTEMP = Convert.ToString(General.FncArrondir(Convert.ToDouble(strTEMP), 2));
                    if (!String.IsNullOrEmpty(strTEMP))
                    {
                        //recherche du taux de TVA
                        if (!string.IsNullOrEmpty(r["CodeArticle"].ToString()))
                        {
                            rs3 = ModAdors.fc_OpenRecordSet("SELECT FacCorpsEtat.TVA From FacCorpsEtat Where FacCorpsEtat.NoFacture ='" + r["NoFacture"] + "'");
                            if (rs3.Rows.Count > 0)
                            {
                                strTemp2 = rs3.Rows[0]["TVA"].ToString() + "%";
                                //Taux TVA
                            }
                            rs3.Clear();
                            ModAdors.Dispose();
                        }
                        // F1Book1.FormulaRC(1, 1) = "0"
                        //  F1Book1.set_FormulaRC(1, 1, );
                        string expression = @"^\d+[.]?\d*%?$";

                        System.Text.RegularExpressions.Regex objNotNumberPattern = new System.Text.RegularExpressions.Regex(expression);
                        if (objNotNumberPattern.IsMatch(strTemp2))
                        {
                            CalcEngine.CalcEngine ce = new CalcEngine.CalcEngine();
                            doubtemp = (double)ce.Evaluate(strTEMP + "*" + strTemp2);
                            strtemp4 = Convert.ToString(doubtemp);
                            //Base TVA
                            strtemp4 = Convert.ToString(General.FncArrondir(Convert.ToDouble(strtemp4), 2));

                            strTemp3 = Convert.ToString(doubtemp);

                            strTemp3 = Convert.ToString(General.FncArrondir(Convert.ToDouble(strTemp3), 2));
                            //F1Book1.set_FormulaRC(1, 1, strTemp3 + "+" + strTEMP);
                            // doubtemp = F1Book1.get_NumberRC(1, 1);
                            ce = new CalcEngine.CalcEngine();
                            doubtemp = (double)ce.Evaluate(strTemp3 + "+" + strTEMP);

                            strTemp3 = Convert.ToString(doubtemp);
                            //Montant TTC
                            strTemp2 = General.Left(strTemp2, strTemp2.Length - 1);

                            sSQL = "UPDATE FacPied set TotalHT =" + strTEMP + ",  MontantTVA = " + strtemp4 + ", TauxTVA =" + strTemp2
                                + ", TTC =" + strTemp3 + ", CalculEcheance =" + General.fc_Fdate(dtTemp, 1) + ", ok = 'ok' where NoFacture ='"
                                + r["NoFacture"] + "'";
                            General.Execute(sSQL);
                        }
                    }

                }

                rs.Clear();
                ModAdors.Dispose();
                rs = null;
                rs2 = null;
                rs3 = null;


                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "facturePied");
            }

        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void CocherChampsFactureTrue()
        {
            if (Option1.Checked == false)
            {
                if (boolRequeteErreur == false)
                {
                    General.Execute("update FacCalendrier set facture = " + General.fc_bln(true)
                        + " Where FacCalendrier.Date >=" + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <="
                        + General.fc_Fdate(txtFacture2.Value, 1) + "" + " And  FacCalendrier.Facture =" + General.fc_bln(false)
                        + " and FacCalendrier.NonFacturable = " + General.fc_bln(false) + "" + " and histo= " + General.fc_bln(false));
                    //si une erreur est survenus dans le calcul de facture
                }
                else
                {
                    General.Execute("update FacCalendrier set facture = " + General.fc_bln(true) + " Where FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + "" + " And  FacCalendrier.Facture = " + General.fc_bln(false) + " and FacCalendrier.NonFacturable ="
                        + General.fc_bln(false) + " and histo =" + General.fc_bln(false) + strRequeteErreur);
                    boolRequeteErreur = false;
                }
                General.Execute("update FacCorpsEtat set facture = " + General.fc_bln(true)
                    + " Where FacCorpsEtat.CalanDate >=" + General.fc_Fdate(txtFacture1.Value, 1) + ""
                    + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1) + ""
                    + " And  FacCorpsEtat.Facture = " + General.fc_bln(false));
                General.Execute("update FactEntete set facture = " + General.fc_bln(true) + " Where FactEnTete.Calandate >= "
                    + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FactEntete.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + "" + " And  FactEnTete.Facture = " + General.fc_bln(false));
            }
            else
            {
                if (boolRequeteErreur == false)
                {
                    General.Execute("update FacCalendrier set facture = " + General.fc_bln(true) + " Where FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + " And  FacCalendrier.Facture = " + General.fc_bln(false) + "  and facCalendrier.NumContrat >='"
                        + txtNumContrat1.Text + "' and FacCalendrier.NumContrat <='" + txtNumContrat2.Text
                        + "' and FacCalendrier.NonFacturable = " + General.fc_bln(false) + "  and histo = " + General.fc_bln(false) + " ");
                    //si une erreur est survenus dans le calcul de facture
                }
                else
                {
                    General.Execute("update FacCalendrier set facture = " + General.fc_bln(true) + " Where FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value, 1) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value, 1)
                        + "" + " And  FacCalendrier.Facture = " + General.fc_bln(false) + "" + " and facCalendrier.NumContrat >='"
                        + txtNumContrat1.Text + "'" + " and FacCalendrier.NumContrat <='" + txtNumContrat2.Text + "'"
                        + " and FacCalendrier.NonFacturable =  " + General.fc_bln(false) + " and histo = " + General.fc_bln(false)
                        + strRequeteErreur);
                    boolRequeteErreur = false;
                }
                General.Execute("update FacCorpsEtat set facture = " + General.fc_bln(true) + "  Where FacCorpsEtat.CalanDate >="
                    + General.fc_Fdate(txtFacture1.Value, 1) + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Value, 1)
                    + " And  FacCorpsEtat.Facture = " + General.fc_bln(false) + "  and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text
                    + "' and FacCorpsEtat.NumContrat <='" + txtNumContrat2.Text + "'");
                General.Execute("update FactEntete set facture = " + General.fc_bln(true) + "  Where FactEnTete.Calandate >= "
                    + General.fc_Fdate(txtFacture1.Value, 1) + " And FactEntete.CalanDate <="
                    + General.fc_Fdate(txtFacture2.Value, 1) + " And  FactEnTete.Facture = " + General.fc_bln(false)
                    + "  and factEnTete.NumContrat >='" + txtNumContrat1.Text + "' and factEnTete.NumContrat <='" + txtNumContrat2.Text + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFacture2_Validated(object sender, EventArgs e)
        {
            //   bool Cancel ;
            if (!(General.IsDate(txtFacture2.Value)) && !string.IsNullOrEmpty(txtFacture2.Value.ToString()))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez inseré une date non valide");
                //Cancel = true;
                return;
            }
            else if (!string.IsNullOrEmpty(txtFacture2.Value.ToString()))
            {
                txtFacture2.Value = Convert.ToDateTime(string.Format(txtFacture2.Value.ToString(), "dd/mm/yyyy"));
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Sablier"></param>
        public void SablierOnOff(bool Sablier)
        {
            /// transforme le pointeur en sabier si fleche et inversement
            Application.DoEvents();
            Cursor = Sablier ? Cursors.WaitCursor : Cursors.Arrow;
            Application.DoEvents();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void ChangementCalendrier()
        {
            string strTEMP = null;
            List<List<string>>
                tabTemp = new List<List<string>>();

            try
            {
                rs = new DataTable();
                rs2 = new DataTable();

                SablierOnOff(true);
                //Ahmed le 13/04/2021 ajout de la clause and nonFacturable = 0 pour limiter le nombre des factures recuperer
                //TODO: Ajouter la meme clause sur la version VB
                if (!Option1.Checked)
                {
                    strTEMP = "SELECT distinct numcontrat, avenant from FacCalendrier" + " Where FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value)
                        + "" + " and Histo =" + General.fc_bln(false) + " " + " and resiliee = " + General.fc_bln(false) + " and nonFacturable = 0";
                }
                else if (Option1.Checked == true)
                {
                    strTEMP = "SELECT distinct numcontrat, avenant from FacCalendrier" + " Where FacCalendrier.Date >="
                        + General.fc_Fdate(txtFacture1.Value) + "" + " And FacCalendrier.Date <=" + General.fc_Fdate(txtFacture2.Value)
                        + "" + " and Histo =" + General.fc_bln(false) + " " + " and resiliee = " + General.fc_bln(false) + " and NumContrat >='"
                        + txtNumContrat1.Text + "'" + " and NumContrat <='" + txtNumContrat2.Text + "'";
                }

                rs = ModAdors.fc_OpenRecordSet(strTEMP);
                //ChangementCalendrier
                if (rs.Rows.Count > 0)
                {
                    // while (rs.EOF == false)
                    foreach (DataRow r in rs.Rows)
                    {

                        strTEMP = "SELECT FacCalendrier.Echeance, FacCalendrier.Date," + " FacCalendrier.Facture, FacCalendrier.NoFacture, FacCalendrier.Ratio,"
                                                                                       + " FacCalendrier.PCdu, FacCalendrier.Au, FacCalendrier.TypeCalcul," + " FacCalendrier.TypeRevision, FacCalendrier.NumContrat,"
                                                                                       + " FacCalendrier.Avenant, FacCalendrier.nonFacturable, histo, Fac_MoisIndice,FacCalendrier.FacEssai" + " FROM FacCalendrier where numcontrat='"
                                                                                       + r["NumContrat"] + "'" + " and Histo =" + General.fc_bln(false) + " " + " and avenant ='" + r["avenant"]
                                                                                       + "' and date is not null order by date desc";
                        rs2 = ModAdors.fc_OpenRecordSet(strTEMP);

                        if (rs2.Rows.Count > 0)
                        {

                            if ((rs2.Rows[0]["NoFacture"] != DBNull.Value ||
                                 !string.IsNullOrEmpty(rs2.Rows[0]["NoFacture"].ToString())) &&
                                Convert.ToBoolean(rs2.Rows[0]["Facture"]) == true)
                            {
                                j = 0;
                                //while (rs2.EOF == false)
                                for (int ii = 0; ii < rs2.Columns.Count - 1; ii++)
                                {
                                    tabTemp?.Add(new List<string>());
                                }

                                foreach (DataRow dr in rs2.Rows)
                                {
                                    i = 0;
                                    foreach (DataColumn col in rs2.Columns)
                                    {
                                        if (col.ColumnName.ToUpper() == "FacEssai".ToUpper()) continue;
                                        tabTemp?[i].Add(dr[col.ColumnName] + "");
                                        i++;
                                    }
                                    dr["histo"] = General.fc_bln(true);
                                    //rs2.Update();
                                    //rs2.MoveNext();

                                    j = j + 1;
                                }
                                indi1 = j - 1;
                                for (j = 0; j <= indi1; j++)
                                {
                                    //rs2.CancelUpdate

                                    DataRow row = rs2.NewRow();

                                    for (i = 0; i < tabTemp?.Count; i++)
                                    {
                                        if (i != 2 && i != 3 && i != 12)
                                        {
                                            if (tabTemp[i][j] == "True" || tabTemp[i][j] == "Vrai")
                                            {
                                                row[i] = General.fc_bln(true);
                                            }
                                            else if (tabTemp[i][j] == "False" || tabTemp[i][j] == "Faux")
                                            {
                                                row[i] = General.fc_bln(false);
                                            }
                                            else if (i == 5 || i == 6 || i == 1)
                                            {
                                                //==== i = 5 colonne PCdu
                                                //==== i = 6 colonne Au
                                                //==== i = 1 colonne  date.
                                                //=== modif du 16 octobre 2013, dans certains cas il arrive que les colonnes
                                                //=== PCdu et Au ne soient pas alimenté et provoqué ainsi une erreur et negenrait pas de calendrier.
                                                if ((i == 5 && string.IsNullOrEmpty(tabTemp[i][j])))
                                                {
                                                    //== nefait rien.
                                                }
                                                else if ((i == 6 && string.IsNullOrEmpty(tabTemp[i][j])))
                                                {
                                                    //=== ne fait rien.
                                                }
                                                else
                                                {

                                                    strTEMP = Convert.ToDateTime(tabTemp[i][j]).AddYears(1).ToShortDateString();
                                                    row[i] = strTEMP;
                                                }
                                            }
                                            else if (string.IsNullOrEmpty(tabTemp[i][j]))
                                            {
                                            }
                                            else
                                            {
                                                row[i] = tabTemp[i][j];
                                            }
                                        }
                                    }

                                    rs2.Rows.Add(row);
                                    //rs2.Update();
                                    ModAdors.Update();
                                }
                                tabTemp = null;
                            }


                        }
                        rs2.Clear();
                        ModAdors.Dispose();
                        //rs.MoveNext();
                    }
                }
                rs.Clear();
                ModAdors.Dispose();
                rs = null;
                rs2 = null;
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "ChangementCalendrier");
            }
        }

        private void UserDocImprimeFacture_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocImprimeFacture");
            General.open_conn();
            InitializeGrid();
            txtNumContrat1.Enabled = false;
            txtNumContrat2.Enabled = false;
            //---rouge

            //SSOleDBGrid1.StyleSets["erreur"].BackColor = 0x8080ff;
            //SSOleDBGrid1.StyleSets["erreur"].ForeColor = 0xffffff;

            SSOleDBGrid1.DisplayLayout.Appearance.BackColor = Color.FromArgb(128, 128, 255);
            SSOleDBGrid1.DisplayLayout.Appearance.ForeColor = Color.FromArgb(255, 255, 255);

            txtDateFacture.Value = DateTime.Now;
            txtDateFacture.MinDate = General.GetDateDebutExercice();
            txtFacture1.Text = "01/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            txtFacture2.Text = General.CalculMaxiJourMois(Convert.ToInt16(DateTime.Now.Month)) + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            //lblNavigation[0].Text = General.sNomLien0;
            //lblNavigation[1].Text = General.sNomLien1;
            //lblNavigation[2].Text = General.sNomLien2;

        }

        private void InitializeGrid()
        {
            string sSQL = "SELECT FacPied.NumContrat, FacPied.NoFacture," + " FacPied.TotalHT, FacPied.TauxTVA, FacPied.MontantTVA,"
                            + " FacPied.TTC" + " FROM FacPied RIGHT JOIN FactEnTete ON FacPied.NoFacture" + " = FactEnTete.NoFacture"
                            + " where  FactEnTete.DateFacture =0 ";
            SSOleDBGrid1.DataSource = ModAdors.fc_OpenRecordSet(sSQL);

        }

        private void lblGofichecontrat_Click(object sender, EventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
        }

        private void lblGoFormule_Click(object sender, EventArgs e)
        {
            General.ParamVisu = "Contrat Formule";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
        }

        private void lblGoTypeRevision_Click(object sender, EventArgs e)
        {
            General.ParamVisu = "Contrat Type Ratio";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
        }

        private void lblGoIndice_Click(object sender, EventArgs e)
        {
            General.ParamVisu = "Contrat Indice";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
        }

        private void Label53_Click(object sender, EventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserListeContrat));
        }

        private void lblGofichecontrat_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
        }

        private void lblGoFormule_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.ParamVisu = "Contrat Formule";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));

        }

        private void lblGoTypeRevision_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.ParamVisu = "Contrat Type Ratio";

            View.Theme.Theme.Navigate(typeof(UserDocParamtre));

        }

        private void lblGoIndice_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            General.ParamVisu = "Contrat Type Ratio";

            View.Theme.Theme.Navigate(typeof(UserDocParamtre));

        }

        private void Label53_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserListeContrat));

        }

        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {


            e.Layout.Bands[0].Columns["NumContrat"].Header.Caption = "N° Contrat";
            e.Layout.Bands[0].Columns["NoFacture"].Header.Caption = "N° Facture";
            e.Layout.Bands[0].Columns["TotalHT"].Header.Caption = "Total Ht";
            e.Layout.Bands[0].Columns["TauxTVA"].Header.Caption = "Taux TVA";
            e.Layout.Bands[0].Columns["MontantTVA"].Header.Caption = "Montant TVA";
            e.Layout.Bands[0].Columns["TTC"].Header.Caption = "TTC";


        }
        private void facturePiedv2()
        {
            string sReqSql = null;
            DateTime dtDateEchDefaut = default(DateTime);
            string sSQLp = null;
            string sSQLreq = null;
            int lngNo = 0;
            double dbHT = 0;
            double dbTTC = 0;
            double dbMontantTva = 0;
            try
            {

                rs = new DataTable();
                rs2 = new DataTable();
                rs3 = new DataTable();
                dtTemp = txtDateFacture.Value;

                //la boucle çi-dessous recherche le dernier jour du mois par rapport à la date de facture.
                dtTemp = new DateTime(year: txtDateFacture.Value.Year, month: txtDateFacture.Value.Month, day: DateTime.DaysInMonth(txtDateFacture.Value.Year, txtDateFacture.Value.Month));
                dtDateEchDefaut = dtTemp;

                //=== Recherche des factures à calculer.
                //selection par date uniquement
                if (!Option1.Checked)
                {

                    //=== modif du 9 dec 2015 ajout du champ CalcEcheanceImm
                    sSQL = "SELECT distinct FacCorpsEtat.NoFacture,  FactEnTete.CalcEcheanceImm "
                            + " FROM  FacCorpsEtat INNER JOIN"
                            + " FactEnTete ON FacCorpsEtat.NoFacture = FactEnTete.NoFacture "
                            + " Where FacCorpsEtat.CalanDate >=" + General.fc_Fdate(txtFacture1.Text, 1)
                            + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Text, 1)
                            + " And  FacCorpsEtat.Facture =" + General.fc_bln(false);

                    rs = ModAdors.fc_OpenRecordSet(sSQL, null, null, General.adocnn);

                    //selection par date uniquement et par contrat
                }
                else if (Option1.Checked == true)
                {
                    //sSQL = "SELECT FacCorpsEtat.NoFacture, FacCorpsEtat.CodeArticle" _
                    //& " FROM FacCorpsEtat" _
                    //& " Where FacCorpsEtat.CalanDate >= " & fc_Fdate(txtFacture1, 1) _
                    //& " And FacCorpsEtat.CalanDate <=" & fc_Fdate(txtFacture2, 1) _
                    //& " And  FacCorpsEtat.Facture =" & fc_bln(False) _
                    //& " and FacCorpsEtat.NumContrat >='" & txtNumContrat1 _
                    //& "' and FacCorpsEtat.NumContrat <='" & txtNumContrat2 & "'"

                    //=== modif du 9 dec 2015 ajout du champ CalcEcheanceImm
                    //UPGRADE_WARNING: Couldn't resolve default property of object fc_bln(False). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
                    sSQL = "SELECT distinct FacCorpsEtat.NoFacture,  FactEnTete.CalcEcheanceImm "
                        + " FROM         FacCorpsEtat INNER JOIN"
                        + " FactEnTete ON FacCorpsEtat.NoFacture = FactEnTete.NoFacture"
                        + " Where FacCorpsEtat.CalanDate >= " + General.fc_Fdate(txtFacture1.Text, 1)
                        + " And FacCorpsEtat.CalanDate <=" + General.fc_Fdate(txtFacture2.Text, 1)
                        + " And  FacCorpsEtat.Facture =" + General.fc_bln(false)
                        + " and FacCorpsEtat.NumContrat >='" + txtNumContrat1.Text
                        + "' and FacCorpsEtat.NumContrat <='" + txtNumContrat2.Text + "'";

                    rs = ModAdors.fc_OpenRecordSet(sSQL);

                }

                //=== Debut du calcul du pied de facture (boucle pour chaque facture).
                // while (rs.EOF == false)
                foreach (DataRow r in rs.Rows)
                {
                    //'=== HT ET TVA.
                    sSQLp = "SELECT        SUM(ResultatRatio) AS HT, TVA"
                         + " From FacCorpsEtat "
                         + " GROUP BY NoFacture, TVA"
                         + " HAVING FacCorpsEtat.NoFacture='" + r["NoFacture"] + "'";

                    rs2 = ModAdors.fc_OpenRecordSet(sSQLp);
                    lngNo = 1;


                    dbHT = 0;
                    dbTTC = 0;
                    dbMontantTva = 0;
                    sSQLreq = "";
                    sSQLreq = sSQLreq + "UPDATE FacPied set ";

                    foreach (DataRow r2 in rs2.Rows)
                    {
                        //'=== Montant H.T.
                        strTEMP = General.nz(r2["HT"], 0).ToString();
                        strTEMP = General.FncArrondir(Convert.ToDouble(strTEMP), 2).ToString();
                        //'=== cumul HT.
                        dbHT = dbHT + General.FncArrondir(Convert.ToDouble(General.nz(r2["HT"], 0)), 2);


                        //'=== taux de tva.
                        strTemp2 = r2["TVA"] + "%";

                        //'=== montant TVA.
                        string expression = @"^\d+[.]?\d*%?$";

                        System.Text.RegularExpressions.Regex objNotNumberPattern = new System.Text.RegularExpressions.Regex(expression);
                        if (objNotNumberPattern.IsMatch(strTemp2))
                        {
                            CalcEngine.CalcEngine ce = new CalcEngine.CalcEngine();
                            doubtemp = (double)ce.Evaluate(strTEMP + "*" + strTemp2);
                            strtemp4 = Convert.ToString(doubtemp);
                            strtemp4 = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(strtemp4, 0)), 2));
                            //'=== cumul montant tva.
                            dbMontantTva = dbMontantTva + General.FncArrondir(Convert.ToDouble(General.nz(doubtemp, 0)), 2);

                            //=== calcul du TTC
                            strTemp3 = Convert.ToString(strtemp4);
                            //F1Book1.set_FormulaRC(1, 1, strTemp3 + "+" + strTEMP);
                            // doubtemp = F1Book1.get_NumberRC(1, 1);
                            ce = new CalcEngine.CalcEngine();
                            doubtemp = (double)ce.Evaluate(strTemp3 + "+" + strTEMP);
                            strTemp3 = Convert.ToString(doubtemp);

                            //cumul TTC
                            dbTTC = dbTTC + doubtemp;

                            //'=== Extrait le caractére %.
                            strTemp2 = General.Left(strTemp2, strTemp2.Length - 1);

                            if (lngNo == 1)
                            {
                                sSQLreq = sSQLreq + " TotalHT" + lngNo + "=" + strTEMP
                                   + ",  MontantTVA" + lngNo + " = " + strtemp4
                                   + ", TauxTVA" + lngNo + "  =" + strTemp2
                                   + ", TTC" + lngNo + "  =" + strTemp3;
                            }
                            else
                            {
                                sSQLreq = sSQLreq + ", TotalHT" + lngNo + "=" + strTEMP
                                   + ",  MontantTVA" + lngNo + " = " + strtemp4
                                   + ", TauxTVA" + lngNo + "  =" + strTemp2
                                   + ", TTC" + lngNo + "  =" + strTemp3;
                            }
                            lngNo = lngNo + 1;
                        }

                    }
                    rs2.Dispose();

                    //'=== date d'echeance.
                    if (!string.IsNullOrEmpty(ModAppel.sCodeReglementDefContrat))
                    {
                        dtTemp = General.CalcEch(Convert.ToDateTime(txtDateFacture.Text), ModAppel.sCodeReglementDefContrat);
                    }
                    else if (General.sModeRegFacContrat == "1" || Convert.ToBoolean(General.nz(r["CalcEcheanceImm"], false)) == true)
                    {
                        sReqSql = "SELECT      CodeImmeuble"
                                         + " From FactEnTete"
                                         + " WHERE  NoFacture = '" + r["NoFacture"] + "'";
                        using (var tmpAdo = new ModAdo())
                            sReqSql = tmpAdo.fc_ADOlibelle(sReqSql);

                        dtTemp = fc_ModeRegl(Convert.ToDateTime(txtDateFacture.Text), dtTemp, sReqSql, Convert.ToBoolean(General.nz(r["CalcEcheanceImm"], false)));
                    }
                    else
                    {
                        dtTemp = dtDateEchDefaut;
                    }

                    sSQL = sSQLreq
                            + ", TotalHT =" + dbHT
                            + ",  MontantTVA = " + dbMontantTva
                            + ", TTC =" + dbTTC
                            + ", CalculEcheance =" + General.fc_Fdate(dtTemp, 1)
                            + ", ok = 'VERSION MULTI TAUX' ";


                    sSQL = sSQL + " where NoFacture ='" + r["NoFacture"] + "'";
                    General.Execute(sSQL);
                }

                rs.Clear();
                ModAdors.Dispose();
                rs = null;
                rs2 = null;
                rs3 = null;


                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "facturePied");
            }

        }
    }


}
