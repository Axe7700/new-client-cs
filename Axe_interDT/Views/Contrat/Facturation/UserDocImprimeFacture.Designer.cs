namespace Axe_interDT.Views.Facturation
{
    partial class UserDocImprimeFacture
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocImprimeFacture));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.chk3bacs = new System.Windows.Forms.CheckBox();
            this.cmdRecalculer = new System.Windows.Forms.Button();
            this.cmdImprimeReel = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Option1 = new System.Windows.Forms.CheckBox();
            this.txtNumContrat1 = new iTalk.iTalk_TextBox_Small2();
            this.txtNumContrat2 = new iTalk.iTalk_TextBox_Small2();
            this.txtDateFacture = new System.Windows.Forms.DateTimePicker();
            this.txtFacture1 = new System.Windows.Forms.DateTimePicker();
            this.txtFacture2 = new System.Windows.Forms.DateTimePicker();
            this.Text3 = new iTalk.iTalk_TextBox_Small2();
            this.Text5 = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblLabelDeroule = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Command1 = new System.Windows.Forms.Button();
            this.lblDeroule = new System.Windows.Forms.Label();
            this.cmdRectif = new System.Windows.Forms.Button();
            this.lblTot = new System.Windows.Forms.Label();
            this.cmdIntegration = new System.Windows.Forms.Button();
            this.frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblGoIndice = new iTalk.iTalk_LinkLabel();
            this.Label53 = new iTalk.iTalk_LinkLabel();
            this.lblGoTypeRevision = new iTalk.iTalk_LinkLabel();
            this.lblGofichecontrat = new iTalk.iTalk_LinkLabel();
            this.lblGoFormule = new iTalk.iTalk_LinkLabel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.Frame5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.frame1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 35F));
            this.tableLayoutPanel1.Controls.Add(this.Frame5, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 40);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 112);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Frame5
            // 
            this.Frame5.BackColor = System.Drawing.Color.Transparent;
            this.Frame5.Controls.Add(this.tableLayoutPanel4);
            this.Frame5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Frame5.Location = new System.Drawing.Point(1205, 3);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(642, 106);
            this.Frame5.TabIndex = 411;
            this.Frame5.TabStop = false;
            this.Frame5.Text = "Impression et mise";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.chk3bacs, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdRecalculer, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.cmdImprimeReel, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Command2, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(636, 83);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // chk3bacs
            // 
            this.chk3bacs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chk3bacs.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.chk3bacs.Location = new System.Drawing.Point(321, 3);
            this.chk3bacs.Name = "chk3bacs";
            this.chk3bacs.Size = new System.Drawing.Size(312, 35);
            this.chk3bacs.TabIndex = 570;
            this.chk3bacs.Text = "Impression 3 Bacs";
            this.chk3bacs.UseVisualStyleBackColor = true;
            // 
            // cmdRecalculer
            // 
            this.cmdRecalculer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRecalculer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRecalculer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdRecalculer.FlatAppearance.BorderSize = 0;
            this.cmdRecalculer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRecalculer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdRecalculer.ForeColor = System.Drawing.Color.White;
            this.cmdRecalculer.Image = global::Axe_interDT.Properties.Resources.Calculator_16x16;
            this.cmdRecalculer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRecalculer.Location = new System.Drawing.Point(2, 43);
            this.cmdRecalculer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRecalculer.Name = "cmdRecalculer";
            this.cmdRecalculer.Size = new System.Drawing.Size(314, 38);
            this.cmdRecalculer.TabIndex = 398;
            this.cmdRecalculer.Text = "         Recalculer";
            this.cmdRecalculer.UseVisualStyleBackColor = false;
            this.cmdRecalculer.Click += new System.EventHandler(this.cmdRecalculer_Click);
            // 
            // cmdImprimeReel
            // 
            this.cmdImprimeReel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdImprimeReel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimeReel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdImprimeReel.FlatAppearance.BorderSize = 0;
            this.cmdImprimeReel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimeReel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdImprimeReel.ForeColor = System.Drawing.Color.White;
            this.cmdImprimeReel.Image = ((System.Drawing.Image)(resources.GetObject("cmdImprimeReel.Image")));
            this.cmdImprimeReel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImprimeReel.Location = new System.Drawing.Point(2, 2);
            this.cmdImprimeReel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimeReel.Name = "cmdImprimeReel";
            this.cmdImprimeReel.Size = new System.Drawing.Size(314, 37);
            this.cmdImprimeReel.TabIndex = 393;
            this.cmdImprimeReel.Text = "     Mise a jour";
            this.cmdImprimeReel.UseVisualStyleBackColor = false;
            this.cmdImprimeReel.Click += new System.EventHandler(this.cmdImprimeReel_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Image = global::Axe_interDT.Properties.Resources.Eye_16x16;
            this.Command2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command2.Location = new System.Drawing.Point(320, 43);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(314, 38);
            this.Command2.TabIndex = 572;
            this.Command2.Text = "     Visu";
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.Label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Option1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtNumContrat1, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtNumContrat2, 2, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtDateFacture, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtFacture1, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtFacture2, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.Text3, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.Text5, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 19F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1196, 106);
            this.tableLayoutPanel3.TabIndex = 412;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(3, 0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(175, 25);
            this.Label1.TabIndex = 384;
            this.Label1.Text = "Date de facture:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 25);
            this.label2.TabIndex = 385;
            this.label2.Text = "Periode facturée :";
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Option1.Location = new System.Drawing.Point(3, 53);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(175, 24);
            this.Option1.TabIndex = 570;
            this.Option1.Text = " Selection par contrat :";
            this.Option1.UseVisualStyleBackColor = true;
            this.Option1.CheckedChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // txtNumContrat1
            // 
            this.txtNumContrat1.AccAcceptNumbersOnly = false;
            this.txtNumContrat1.AccAllowComma = false;
            this.txtNumContrat1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumContrat1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumContrat1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumContrat1.AccHidenValue = "";
            this.txtNumContrat1.AccNotAllowedChars = null;
            this.txtNumContrat1.AccReadOnly = false;
            this.txtNumContrat1.AccReadOnlyAllowDelete = false;
            this.txtNumContrat1.AccRequired = false;
            this.txtNumContrat1.BackColor = System.Drawing.Color.White;
            this.txtNumContrat1.CustomBackColor = System.Drawing.Color.White;
            this.txtNumContrat1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumContrat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtNumContrat1.ForeColor = System.Drawing.Color.Black;
            this.txtNumContrat1.Location = new System.Drawing.Point(183, 52);
            this.txtNumContrat1.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumContrat1.MaxLength = 32767;
            this.txtNumContrat1.Multiline = false;
            this.txtNumContrat1.Name = "txtNumContrat1";
            this.txtNumContrat1.ReadOnly = false;
            this.txtNumContrat1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumContrat1.Size = new System.Drawing.Size(503, 27);
            this.txtNumContrat1.TabIndex = 574;
            this.txtNumContrat1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumContrat1.UseSystemPasswordChar = false;
            // 
            // txtNumContrat2
            // 
            this.txtNumContrat2.AccAcceptNumbersOnly = false;
            this.txtNumContrat2.AccAllowComma = false;
            this.txtNumContrat2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumContrat2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumContrat2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumContrat2.AccHidenValue = "";
            this.txtNumContrat2.AccNotAllowedChars = null;
            this.txtNumContrat2.AccReadOnly = false;
            this.txtNumContrat2.AccReadOnlyAllowDelete = false;
            this.txtNumContrat2.AccRequired = false;
            this.txtNumContrat2.BackColor = System.Drawing.Color.White;
            this.txtNumContrat2.CustomBackColor = System.Drawing.Color.White;
            this.txtNumContrat2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumContrat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtNumContrat2.ForeColor = System.Drawing.Color.Black;
            this.txtNumContrat2.Location = new System.Drawing.Point(690, 52);
            this.txtNumContrat2.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumContrat2.MaxLength = 32767;
            this.txtNumContrat2.Multiline = false;
            this.txtNumContrat2.Name = "txtNumContrat2";
            this.txtNumContrat2.ReadOnly = false;
            this.txtNumContrat2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumContrat2.Size = new System.Drawing.Size(504, 27);
            this.txtNumContrat2.TabIndex = 575;
            this.txtNumContrat2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumContrat2.UseSystemPasswordChar = false;
            // 
            // txtDateFacture
            // 
            this.txtDateFacture.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtDateFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateFacture.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDateFacture.Location = new System.Drawing.Point(184, 3);
            this.txtDateFacture.Name = "txtDateFacture";
            this.txtDateFacture.Size = new System.Drawing.Size(501, 20);
            this.txtDateFacture.TabIndex = 578;
            this.txtDateFacture.Validated += new System.EventHandler(this.txtDateFacture_Validated);
            // 
            // txtFacture1
            // 
            this.txtFacture1.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtFacture1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFacture1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtFacture1.Location = new System.Drawing.Point(184, 28);
            this.txtFacture1.Name = "txtFacture1";
            this.txtFacture1.Size = new System.Drawing.Size(501, 20);
            this.txtFacture1.TabIndex = 579;
            this.txtFacture1.Validated += new System.EventHandler(this.txtFacture1_Validated);
            // 
            // txtFacture2
            // 
            this.txtFacture2.CalendarFont = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtFacture2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFacture2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtFacture2.Location = new System.Drawing.Point(691, 28);
            this.txtFacture2.Name = "txtFacture2";
            this.txtFacture2.Size = new System.Drawing.Size(502, 20);
            this.txtFacture2.TabIndex = 580;
            this.txtFacture2.Validated += new System.EventHandler(this.txtFacture2_Validated);
            // 
            // Text3
            // 
            this.Text3.AccAcceptNumbersOnly = false;
            this.Text3.AccAllowComma = false;
            this.Text3.AccBackgroundColor = System.Drawing.Color.White;
            this.Text3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text3.AccHidenValue = "";
            this.Text3.AccNotAllowedChars = null;
            this.Text3.AccReadOnly = false;
            this.Text3.AccReadOnlyAllowDelete = false;
            this.Text3.AccRequired = false;
            this.Text3.BackColor = System.Drawing.Color.White;
            this.Text3.CustomBackColor = System.Drawing.Color.White;
            this.Text3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Text3.ForeColor = System.Drawing.Color.Black;
            this.Text3.Location = new System.Drawing.Point(183, 82);
            this.Text3.Margin = new System.Windows.Forms.Padding(2);
            this.Text3.MaxLength = 32767;
            this.Text3.Multiline = false;
            this.Text3.Name = "Text3";
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text3.Size = new System.Drawing.Size(503, 27);
            this.Text3.TabIndex = 577;
            this.Text3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text3.UseSystemPasswordChar = false;
            // 
            // Text5
            // 
            this.Text5.AccAcceptNumbersOnly = false;
            this.Text5.AccAllowComma = false;
            this.Text5.AccBackgroundColor = System.Drawing.Color.White;
            this.Text5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text5.AccHidenValue = "";
            this.Text5.AccNotAllowedChars = null;
            this.Text5.AccReadOnly = false;
            this.Text5.AccReadOnlyAllowDelete = false;
            this.Text5.AccRequired = false;
            this.Text5.BackColor = System.Drawing.Color.White;
            this.Text5.CustomBackColor = System.Drawing.Color.White;
            this.Text5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text5.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Text5.ForeColor = System.Drawing.Color.Black;
            this.Text5.Location = new System.Drawing.Point(690, 82);
            this.Text5.Margin = new System.Windows.Forms.Padding(2);
            this.Text5.MaxLength = 32767;
            this.Text5.Multiline = false;
            this.Text5.Name = "Text5";
            this.Text5.ReadOnly = false;
            this.Text5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text5.Size = new System.Drawing.Size(504, 27);
            this.Text5.TabIndex = 576;
            this.Text5.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text5.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 26);
            this.label3.TabIndex = 387;
            this.label3.Text = "Type de facture :";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Controls.Add(this.SSOleDBGrid1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblLabelDeroule, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 152);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 471F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1850, 805);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // SSOleDBGrid1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance1;
            this.SSOleDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(3, 3);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(1844, 779);
            this.SSOleDBGrid1.TabIndex = 412;
            this.SSOleDBGrid1.Text = "FACTURE";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSOleDBGrid1_InitializeRow);
            this.SSOleDBGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid1_DoubleClickRow);
            // 
            // lblLabelDeroule
            // 
            this.lblLabelDeroule.AutoSize = true;
            this.lblLabelDeroule.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLabelDeroule.Location = new System.Drawing.Point(3, 785);
            this.lblLabelDeroule.Name = "lblLabelDeroule";
            this.lblLabelDeroule.Size = new System.Drawing.Size(0, 18);
            this.lblLabelDeroule.TabIndex = 397;
            this.lblLabelDeroule.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Command1);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1709, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(141, 40);
            this.flowLayoutPanel1.TabIndex = 2;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Printer_16x16;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(2, 2);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(137, 35);
            this.Command1.TabIndex = 392;
            this.Command1.Text = "      Impr.";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // lblDeroule
            // 
            this.lblDeroule.AutoSize = true;
            this.lblDeroule.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDeroule.Location = new System.Drawing.Point(129, 5);
            this.lblDeroule.Name = "lblDeroule";
            this.lblDeroule.Size = new System.Drawing.Size(0, 18);
            this.lblDeroule.TabIndex = 398;
            this.lblDeroule.Visible = false;
            // 
            // cmdRectif
            // 
            this.cmdRectif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRectif.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdRectif.FlatAppearance.BorderSize = 0;
            this.cmdRectif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRectif.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdRectif.ForeColor = System.Drawing.Color.White;
            this.cmdRectif.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRectif.Location = new System.Drawing.Point(824, 201);
            this.cmdRectif.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRectif.Name = "cmdRectif";
            this.cmdRectif.Size = new System.Drawing.Size(70, 36);
            this.cmdRectif.TabIndex = 573;
            this.cmdRectif.Text = "  Rectif";
            this.cmdRectif.UseVisualStyleBackColor = false;
            this.cmdRectif.Visible = false;
            this.cmdRectif.Click += new System.EventHandler(this.cmdRectif_Click);
            // 
            // lblTot
            // 
            this.lblTot.AutoSize = true;
            this.lblTot.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTot.Location = new System.Drawing.Point(237, 14);
            this.lblTot.Name = "lblTot";
            this.lblTot.Size = new System.Drawing.Size(0, 18);
            this.lblTot.TabIndex = 396;
            // 
            // cmdIntegration
            // 
            this.cmdIntegration.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdIntegration.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdIntegration.FlatAppearance.BorderSize = 0;
            this.cmdIntegration.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntegration.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdIntegration.ForeColor = System.Drawing.Color.White;
            this.cmdIntegration.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdIntegration.Location = new System.Drawing.Point(31, 20);
            this.cmdIntegration.Margin = new System.Windows.Forms.Padding(2);
            this.cmdIntegration.Name = "cmdIntegration";
            this.cmdIntegration.Size = new System.Drawing.Size(101, 36);
            this.cmdIntegration.TabIndex = 574;
            this.cmdIntegration.Text = "Integration factures";
            this.cmdIntegration.UseVisualStyleBackColor = false;
            this.cmdIntegration.Visible = false;
            // 
            // frame1
            // 
            this.frame1.BackColor = System.Drawing.Color.Transparent;
            this.frame1.Controls.Add(this.cmdIntegration);
            this.frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.frame1.Location = new System.Drawing.Point(819, 242);
            this.frame1.Name = "frame1";
            this.frame1.Size = new System.Drawing.Size(208, 67);
            this.frame1.TabIndex = 575;
            this.frame1.TabStop = false;
            this.frame1.Text = "Intégration";
            this.frame1.Visible = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.66667F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 432F));
            this.tableLayoutPanel5.Controls.Add(this.lblGoIndice, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.Label53, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblGoTypeRevision, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblGofichecontrat, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.lblGoFormule, 0, 0);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1446, 37);
            this.tableLayoutPanel5.TabIndex = 576;
            // 
            // lblGoIndice
            // 
            this.lblGoIndice.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoIndice.AutoSize = true;
            this.lblGoIndice.BackColor = System.Drawing.Color.Transparent;
            this.lblGoIndice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoIndice.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblGoIndice.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGoIndice.LinkColor = System.Drawing.Color.Blue;
            this.lblGoIndice.Location = new System.Drawing.Point(507, 0);
            this.lblGoIndice.Name = "lblGoIndice";
            this.lblGoIndice.Size = new System.Drawing.Size(162, 37);
            this.lblGoIndice.TabIndex = 5;
            this.lblGoIndice.TabStop = true;
            this.lblGoIndice.Text = "Indice";
            this.lblGoIndice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGoIndice.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoIndice.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoIndice_LinkClicked);
            this.lblGoIndice.Click += new System.EventHandler(this.lblGoIndice_Click);
            // 
            // Label53
            // 
            this.Label53.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.Label53.AutoSize = true;
            this.Label53.BackColor = System.Drawing.Color.Transparent;
            this.Label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Label53.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.Label53.LinkColor = System.Drawing.Color.Blue;
            this.Label53.Location = new System.Drawing.Point(675, 0);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(162, 37);
            this.Label53.TabIndex = 4;
            this.Label53.TabStop = true;
            this.Label53.Text = "Contrat a facturer";
            this.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Label53.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.Label53.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.Label53_LinkClicked);
            this.Label53.Click += new System.EventHandler(this.Label53_Click);
            // 
            // lblGoTypeRevision
            // 
            this.lblGoTypeRevision.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoTypeRevision.AutoSize = true;
            this.lblGoTypeRevision.BackColor = System.Drawing.Color.Transparent;
            this.lblGoTypeRevision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoTypeRevision.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblGoTypeRevision.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGoTypeRevision.LinkColor = System.Drawing.Color.Blue;
            this.lblGoTypeRevision.Location = new System.Drawing.Point(339, 0);
            this.lblGoTypeRevision.Name = "lblGoTypeRevision";
            this.lblGoTypeRevision.Size = new System.Drawing.Size(162, 37);
            this.lblGoTypeRevision.TabIndex = 3;
            this.lblGoTypeRevision.TabStop = true;
            this.lblGoTypeRevision.Text = "Type de revision";
            this.lblGoTypeRevision.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGoTypeRevision.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoTypeRevision.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoTypeRevision_LinkClicked);
            this.lblGoTypeRevision.Click += new System.EventHandler(this.lblGoTypeRevision_Click);
            // 
            // lblGofichecontrat
            // 
            this.lblGofichecontrat.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGofichecontrat.AutoSize = true;
            this.lblGofichecontrat.BackColor = System.Drawing.Color.Transparent;
            this.lblGofichecontrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGofichecontrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGofichecontrat.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGofichecontrat.LinkColor = System.Drawing.Color.Blue;
            this.lblGofichecontrat.Location = new System.Drawing.Point(3, 0);
            this.lblGofichecontrat.Name = "lblGofichecontrat";
            this.lblGofichecontrat.Size = new System.Drawing.Size(162, 37);
            this.lblGofichecontrat.TabIndex = 2;
            this.lblGofichecontrat.TabStop = true;
            this.lblGofichecontrat.Text = "Fiche Contrat";
            this.lblGofichecontrat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGofichecontrat.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGofichecontrat.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGofichecontrat_LinkClicked);
            this.lblGofichecontrat.Click += new System.EventHandler(this.lblGofichecontrat_Click);
            // 
            // lblGoFormule
            // 
            this.lblGoFormule.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoFormule.AutoSize = true;
            this.lblGoFormule.BackColor = System.Drawing.Color.Transparent;
            this.lblGoFormule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoFormule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.lblGoFormule.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGoFormule.LinkColor = System.Drawing.Color.Blue;
            this.lblGoFormule.Location = new System.Drawing.Point(171, 0);
            this.lblGoFormule.Name = "lblGoFormule";
            this.lblGoFormule.Size = new System.Drawing.Size(162, 37);
            this.lblGoFormule.TabIndex = 1;
            this.lblGoFormule.TabStop = true;
            this.lblGoFormule.Text = "Formule";
            this.lblGoFormule.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblGoFormule.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoFormule.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoFormule_LinkClicked);
            this.lblGoFormule.Click += new System.EventHandler(this.lblGoFormule_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel7, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 3;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 112F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel6.TabIndex = 577;
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 78.16594F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.83406F));
            this.tableLayoutPanel7.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1850, 40);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // UserDocImprimeFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel6);
            this.Controls.Add(this.frame1);
            this.Controls.Add(this.cmdRectif);
            this.Controls.Add(this.lblDeroule);
            this.Controls.Add(this.lblTot);
            this.Name = "UserDocImprimeFacture";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Total des Factures";
            this.VisibleChanged += new System.EventHandler(this.UserDocImprimeFacture_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.Frame5.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.frame1.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.GroupBox Frame5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.CheckBox Option1;
        public iTalk.iTalk_TextBox_Small2 Text5;
        public iTalk.iTalk_TextBox_Small2 txtNumContrat1;
        public iTalk.iTalk_TextBox_Small2 txtNumContrat2;
        public iTalk.iTalk_TextBox_Small2 Text3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button cmdImprimeReel;
        public System.Windows.Forms.Button cmdRecalculer;
        public System.Windows.Forms.Button Command2;
        public System.Windows.Forms.CheckBox chk3bacs;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        private System.Windows.Forms.DateTimePicker txtDateFacture;
        private System.Windows.Forms.DateTimePicker txtFacture1;
        private System.Windows.Forms.DateTimePicker txtFacture2;
        public System.Windows.Forms.Label lblLabelDeroule;
        public System.Windows.Forms.Label lblDeroule;
        public System.Windows.Forms.Button cmdRectif;
        public System.Windows.Forms.Label lblTot;
        public System.Windows.Forms.Button cmdIntegration;
        public System.Windows.Forms.GroupBox frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private iTalk.iTalk_LinkLabel lblGoIndice;
        private iTalk.iTalk_LinkLabel Label53;
        private iTalk.iTalk_LinkLabel lblGoTypeRevision;
        private iTalk.iTalk_LinkLabel lblGofichecontrat;
        private iTalk.iTalk_LinkLabel lblGoFormule;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
    }
}
