﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.GoConteur.Forms
{
    public partial class FrmMessageEcartIndex : Form
    {
        public FrmMessageEcartIndex()
        {
            InitializeComponent();
        }

        private UserInter_RelevesCompteurs ficheAppelante;
        public FrmMessageEcartIndex(UserInter_RelevesCompteurs ficheAppelante)
        {
            this.ficheAppelante = ficheAppelante;
            InitializeComponent();
        }
        private void CmdOk_Click(object sender, EventArgs e)
        {
            if (Strings.UCase(FrmAppel.Text) == Strings.UCase("UserInter_RelevesCompteurs"))
            {
                //UserInter_RelevesCompteurs UIRC = new UserInter_RelevesCompteurs();
                if (Opt1.Checked == true)
                {
                    ficheAppelante.OptEcart.Text = "Opt1";
                    this.Close();
                }
                else if (Opt2.Checked == true)
                {
                    ficheAppelante.OptEcart.Text = "Opt2";
                    this.Close();
                }
                else if (Opt3.Checked == true)
                {
                    ficheAppelante.OptEcart.Text = "Opt3";
                    this.Close();
                }
            }


        }
    }
}
