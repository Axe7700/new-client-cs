﻿namespace Axe_interDT.Views.GoConteur.Forms
{
    partial class UserInter_RelevesCompteurs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.lblNoIntervention = new System.Windows.Forms.Label();
            this.cmdValider = new System.Windows.Forms.Button();
            this.chkLectureSeule = new System.Windows.Forms.CheckBox();
            this.lblLectureSeule = new System.Windows.Forms.Label();
            this.chkValideReleve = new System.Windows.Forms.CheckBox();
            this.chkTransfertP1 = new System.Windows.Forms.CheckBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtAdresse3 = new System.Windows.Forms.Label();
            this.txtAdresse2_IMM = new System.Windows.Forms.Label();
            this.txtAdresse = new System.Windows.Forms.Label();
            this.txtVille = new System.Windows.Forms.Label();
            this.txtRaisonSociale = new System.Windows.Forms.Label();
            this.txtRaisonSocial_IMM = new System.Windows.Forms.Label();
            this.txtCodeNumOrdre = new System.Windows.Forms.Label();
            this.txtCodePostal = new System.Windows.Forms.Label();
            this.txtCode1 = new System.Windows.Forms.Label();
            this.txtCodeImmeuble = new System.Windows.Forms.Label();
            this.txtCodeContrat = new iTalk.iTalk_TextBox_Small2();
            this.P2 = new System.Windows.Forms.CheckBox();
            this.P3 = new System.Windows.Forms.CheckBox();
            this.P4 = new System.Windows.Forms.CheckBox();
            this.P1 = new System.Windows.Forms.CheckBox();
            this.txtCOP_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtContrat = new iTalk.iTalk_RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodeChaufferie = new iTalk.iTalk_TextBox_Small2();
            this.Label50 = new System.Windows.Forms.LinkLabel();
            this.Label24 = new System.Windows.Forms.LinkLabel();
            this.Label42 = new System.Windows.Forms.LinkLabel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EtatChauff = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.SSReleve = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.TimerLoad = new System.Windows.Forms.Timer(this.components);
            this.TimerRefresh = new System.Windows.Forms.Timer(this.components);
            this.SSTA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtLibIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.txtMatriculeIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.OptEcart = new iTalk.iTalk_TextBox_Small2();
            this.txtUtilVerrou = new iTalk.iTalk_TextBox_Small2();
            this.txtChauffage = new iTalk.iTalk_TextBox_Small2();
            this.txtDateAu = new iTalk.iTalk_TextBox_Small2();
            this.txtdateDu = new iTalk.iTalk_TextBox_Small2();
            this.txtDateIntervention = new iTalk.iTalk_TextBox_Small2();
            this.groupBox6.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSReleve)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTA)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(23, 25);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(276, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Relevé de compteurs sur l\'Intervention";
            // 
            // lblNoIntervention
            // 
            this.lblNoIntervention.AutoSize = true;
            this.lblNoIntervention.BackColor = System.Drawing.Color.Transparent;
            this.lblNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNoIntervention.Location = new System.Drawing.Point(411, 25);
            this.lblNoIntervention.Name = "lblNoIntervention";
            this.lblNoIntervention.Size = new System.Drawing.Size(29, 19);
            this.lblNoIntervention.TabIndex = 385;
            this.lblNoIntervention.Text = "N°:";
            // 
            // cmdValider
            // 
            this.cmdValider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdValider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValider.FlatAppearance.BorderSize = 0;
            this.cmdValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValider.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdValider.ForeColor = System.Drawing.Color.White;
            this.cmdValider.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdValider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdValider.Location = new System.Drawing.Point(751, 16);
            this.cmdValider.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(93, 35);
            this.cmdValider.TabIndex = 397;
            this.cmdValider.Text = "      Valider.";
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Click += new System.EventHandler(this.cmdValider_Click);
            // 
            // chkLectureSeule
            // 
            this.chkLectureSeule.AutoSize = true;
            this.chkLectureSeule.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkLectureSeule.Location = new System.Drawing.Point(616, 12);
            this.chkLectureSeule.Name = "chkLectureSeule";
            this.chkLectureSeule.Size = new System.Drawing.Size(124, 23);
            this.chkLectureSeule.TabIndex = 570;
            this.chkLectureSeule.Text = "Lecture seule";
            this.chkLectureSeule.UseVisualStyleBackColor = true;
            this.chkLectureSeule.Visible = false;
            // 
            // lblLectureSeule
            // 
            this.lblLectureSeule.AutoSize = true;
            this.lblLectureSeule.BackColor = System.Drawing.Color.Transparent;
            this.lblLectureSeule.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLectureSeule.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.lblLectureSeule.Location = new System.Drawing.Point(82, 61);
            this.lblLectureSeule.Name = "lblLectureSeule";
            this.lblLectureSeule.Size = new System.Drawing.Size(105, 19);
            this.lblLectureSeule.TabIndex = 576;
            this.lblLectureSeule.Text = "Lecture seule";
            // 
            // chkValideReleve
            // 
            this.chkValideReleve.AutoSize = true;
            this.chkValideReleve.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkValideReleve.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkValideReleve.Location = new System.Drawing.Point(332, 60);
            this.chkValideReleve.Name = "chkValideReleve";
            this.chkValideReleve.Size = new System.Drawing.Size(523, 23);
            this.chkValideReleve.TabIndex = 577;
            this.chkValideReleve.Text = "Valider le relevé en cours (transférable vers le suivi des consommations)";
            this.chkValideReleve.UseVisualStyleBackColor = true;
            this.chkValideReleve.CheckedChanged += new System.EventHandler(this.chkValideReleve_CheckedChanged);
            // 
            // chkTransfertP1
            // 
            this.chkTransfertP1.AutoSize = true;
            this.chkTransfertP1.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkTransfertP1.Enabled = false;
            this.chkTransfertP1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkTransfertP1.Location = new System.Drawing.Point(465, 87);
            this.chkTransfertP1.Name = "chkTransfertP1";
            this.chkTransfertP1.Size = new System.Drawing.Size(390, 23);
            this.chkTransfertP1.TabIndex = 578;
            this.chkTransfertP1.Text = "Transférer le relevé dans le suivi des consommations";
            this.chkTransfertP1.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.txtAdresse3);
            this.groupBox6.Controls.Add(this.txtAdresse2_IMM);
            this.groupBox6.Controls.Add(this.txtAdresse);
            this.groupBox6.Controls.Add(this.txtVille);
            this.groupBox6.Controls.Add(this.txtRaisonSociale);
            this.groupBox6.Controls.Add(this.txtRaisonSocial_IMM);
            this.groupBox6.Controls.Add(this.txtCodeNumOrdre);
            this.groupBox6.Controls.Add(this.txtCodePostal);
            this.groupBox6.Controls.Add(this.txtCode1);
            this.groupBox6.Controls.Add(this.txtCodeImmeuble);
            this.groupBox6.Controls.Add(this.txtCodeContrat);
            this.groupBox6.Controls.Add(this.P2);
            this.groupBox6.Controls.Add(this.P3);
            this.groupBox6.Controls.Add(this.P4);
            this.groupBox6.Controls.Add(this.P1);
            this.groupBox6.Controls.Add(this.txtCOP_NoAuto);
            this.groupBox6.Controls.Add(this.txtNoIntervention);
            this.groupBox6.Controls.Add(this.txtContrat);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtCodeChaufferie);
            this.groupBox6.Controls.Add(this.Label50);
            this.groupBox6.Controls.Add(this.Label24);
            this.groupBox6.Controls.Add(this.Label42);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.groupBox6.Location = new System.Drawing.Point(5, 116);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(839, 177);
            this.groupBox6.TabIndex = 579;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Identification du chantier";
            // 
            // txtAdresse3
            // 
            this.txtAdresse3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtAdresse3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAdresse3.ForeColor = System.Drawing.Color.Blue;
            this.txtAdresse3.Location = new System.Drawing.Point(112, 110);
            this.txtAdresse3.Name = "txtAdresse3";
            this.txtAdresse3.Size = new System.Drawing.Size(369, 19);
            this.txtAdresse3.TabIndex = 5;
            // 
            // txtAdresse2_IMM
            // 
            this.txtAdresse2_IMM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse2_IMM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtAdresse2_IMM.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAdresse2_IMM.ForeColor = System.Drawing.Color.Blue;
            this.txtAdresse2_IMM.Location = new System.Drawing.Point(112, 85);
            this.txtAdresse2_IMM.Name = "txtAdresse2_IMM";
            this.txtAdresse2_IMM.Size = new System.Drawing.Size(369, 19);
            this.txtAdresse2_IMM.TabIndex = 4;
            // 
            // txtAdresse
            // 
            this.txtAdresse.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtAdresse.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAdresse.ForeColor = System.Drawing.Color.Blue;
            this.txtAdresse.Location = new System.Drawing.Point(112, 52);
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.Size = new System.Drawing.Size(369, 19);
            this.txtAdresse.TabIndex = 3;
            // 
            // txtVille
            // 
            this.txtVille.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtVille.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtVille.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtVille.ForeColor = System.Drawing.Color.Blue;
            this.txtVille.Location = new System.Drawing.Point(272, 135);
            this.txtVille.Name = "txtVille";
            this.txtVille.Size = new System.Drawing.Size(209, 19);
            this.txtVille.TabIndex = 7;
            // 
            // txtRaisonSociale
            // 
            this.txtRaisonSociale.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRaisonSociale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtRaisonSociale.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtRaisonSociale.ForeColor = System.Drawing.Color.Blue;
            this.txtRaisonSociale.Location = new System.Drawing.Point(652, 12);
            this.txtRaisonSociale.Name = "txtRaisonSociale";
            this.txtRaisonSociale.Size = new System.Drawing.Size(176, 19);
            this.txtRaisonSociale.TabIndex = 9;
            // 
            // txtRaisonSocial_IMM
            // 
            this.txtRaisonSocial_IMM.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtRaisonSocial_IMM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtRaisonSocial_IMM.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtRaisonSocial_IMM.ForeColor = System.Drawing.Color.Blue;
            this.txtRaisonSocial_IMM.Location = new System.Drawing.Point(272, 18);
            this.txtRaisonSocial_IMM.Name = "txtRaisonSocial_IMM";
            this.txtRaisonSocial_IMM.Size = new System.Drawing.Size(209, 19);
            this.txtRaisonSocial_IMM.TabIndex = 2;
            // 
            // txtCodeNumOrdre
            // 
            this.txtCodeNumOrdre.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodeNumOrdre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtCodeNumOrdre.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeNumOrdre.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeNumOrdre.Location = new System.Drawing.Point(204, 18);
            this.txtCodeNumOrdre.Name = "txtCodeNumOrdre";
            this.txtCodeNumOrdre.Size = new System.Drawing.Size(69, 19);
            this.txtCodeNumOrdre.TabIndex = 1;
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodePostal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtCodePostal.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodePostal.ForeColor = System.Drawing.Color.Blue;
            this.txtCodePostal.Location = new System.Drawing.Point(112, 135);
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.Size = new System.Drawing.Size(92, 19);
            this.txtCodePostal.TabIndex = 6;
            // 
            // txtCode1
            // 
            this.txtCode1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCode1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCode1.ForeColor = System.Drawing.Color.Blue;
            this.txtCode1.Location = new System.Drawing.Point(560, 12);
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.Size = new System.Drawing.Size(92, 19);
            this.txtCode1.TabIndex = 8;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtCodeImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(112, 18);
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.Size = new System.Drawing.Size(92, 19);
            this.txtCodeImmeuble.TabIndex = 0;
            // 
            // txtCodeContrat
            // 
            this.txtCodeContrat.AccAcceptNumbersOnly = false;
            this.txtCodeContrat.AccAllowComma = false;
            this.txtCodeContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeContrat.AccHidenValue = "";
            this.txtCodeContrat.AccNotAllowedChars = null;
            this.txtCodeContrat.AccReadOnly = false;
            this.txtCodeContrat.AccReadOnlyAllowDelete = false;
            this.txtCodeContrat.AccRequired = false;
            this.txtCodeContrat.BackColor = System.Drawing.Color.White;
            this.txtCodeContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeContrat.ForeColor = System.Drawing.Color.Black;
            this.txtCodeContrat.Location = new System.Drawing.Point(526, 67);
            this.txtCodeContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeContrat.MaxLength = 32767;
            this.txtCodeContrat.Multiline = false;
            this.txtCodeContrat.Name = "txtCodeContrat";
            this.txtCodeContrat.ReadOnly = false;
            this.txtCodeContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeContrat.Size = new System.Drawing.Size(23, 27);
            this.txtCodeContrat.TabIndex = 576;
            this.txtCodeContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeContrat.UseSystemPasswordChar = false;
            this.txtCodeContrat.Visible = false;
            // 
            // P2
            // 
            this.P2.AutoCheck = false;
            this.P2.AutoSize = true;
            this.P2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.P2.Location = new System.Drawing.Point(611, 146);
            this.P2.Name = "P2";
            this.P2.Size = new System.Drawing.Size(45, 23);
            this.P2.TabIndex = 575;
            this.P2.Text = "P2";
            this.P2.UseVisualStyleBackColor = true;
            // 
            // P3
            // 
            this.P3.AutoCheck = false;
            this.P3.AutoSize = true;
            this.P3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.P3.Location = new System.Drawing.Point(660, 146);
            this.P3.Name = "P3";
            this.P3.Size = new System.Drawing.Size(45, 23);
            this.P3.TabIndex = 574;
            this.P3.Text = "P3";
            this.P3.UseVisualStyleBackColor = true;
            // 
            // P4
            // 
            this.P4.AutoCheck = false;
            this.P4.AutoSize = true;
            this.P4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.P4.Location = new System.Drawing.Point(704, 146);
            this.P4.Name = "P4";
            this.P4.Size = new System.Drawing.Size(45, 23);
            this.P4.TabIndex = 573;
            this.P4.Text = "P4";
            this.P4.UseVisualStyleBackColor = true;
            // 
            // P1
            // 
            this.P1.AutoCheck = false;
            this.P1.AutoSize = true;
            this.P1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.P1.Location = new System.Drawing.Point(562, 146);
            this.P1.Name = "P1";
            this.P1.Size = new System.Drawing.Size(45, 23);
            this.P1.TabIndex = 572;
            this.P1.Text = "P1";
            this.P1.UseVisualStyleBackColor = true;
            // 
            // txtCOP_NoAuto
            // 
            this.txtCOP_NoAuto.AccAcceptNumbersOnly = false;
            this.txtCOP_NoAuto.AccAllowComma = false;
            this.txtCOP_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoAuto.AccHidenValue = "";
            this.txtCOP_NoAuto.AccNotAllowedChars = null;
            this.txtCOP_NoAuto.AccReadOnly = false;
            this.txtCOP_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoAuto.AccRequired = false;
            this.txtCOP_NoAuto.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCOP_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoAuto.Location = new System.Drawing.Point(811, 144);
            this.txtCOP_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoAuto.MaxLength = 32767;
            this.txtCOP_NoAuto.Multiline = false;
            this.txtCOP_NoAuto.Name = "txtCOP_NoAuto";
            this.txtCOP_NoAuto.ReadOnly = false;
            this.txtCOP_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoAuto.Size = new System.Drawing.Size(23, 27);
            this.txtCOP_NoAuto.TabIndex = 571;
            this.txtCOP_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoAuto.UseSystemPasswordChar = false;
            this.txtCOP_NoAuto.Visible = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(11, 131);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(23, 27);
            this.txtNoIntervention.TabIndex = 570;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // txtContrat
            // 
            this.txtContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtContrat.AutoWordSelection = false;
            this.txtContrat.BackColor = System.Drawing.Color.Transparent;
            this.txtContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtContrat.ForeColor = System.Drawing.Color.Blue;
            this.txtContrat.Location = new System.Drawing.Point(560, 38);
            this.txtContrat.Name = "txtContrat";
            this.txtContrat.ReadOnly = true;
            this.txtContrat.Size = new System.Drawing.Size(273, 105);
            this.txtContrat.TabIndex = 10;
            this.txtContrat.WordWrap = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(229, 138);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 19);
            this.label6.TabIndex = 514;
            this.label6.Text = "Ville";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(79, 138);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(28, 19);
            this.label5.TabIndex = 513;
            this.label5.Text = "CP";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(8, 86);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(65, 19);
            this.label4.TabIndex = 512;
            this.label4.Text = "Adresse";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(8, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 19);
            this.label3.TabIndex = 511;
            this.label3.Text = "Raison sociale";
            // 
            // txtCodeChaufferie
            // 
            this.txtCodeChaufferie.AccAcceptNumbersOnly = false;
            this.txtCodeChaufferie.AccAllowComma = false;
            this.txtCodeChaufferie.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeChaufferie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeChaufferie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeChaufferie.AccHidenValue = "";
            this.txtCodeChaufferie.AccNotAllowedChars = null;
            this.txtCodeChaufferie.AccReadOnly = false;
            this.txtCodeChaufferie.AccReadOnlyAllowDelete = false;
            this.txtCodeChaufferie.AccRequired = false;
            this.txtCodeChaufferie.BackColor = System.Drawing.Color.White;
            this.txtCodeChaufferie.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeChaufferie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeChaufferie.ForeColor = System.Drawing.Color.Black;
            this.txtCodeChaufferie.Location = new System.Drawing.Point(489, 110);
            this.txtCodeChaufferie.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeChaufferie.MaxLength = 32767;
            this.txtCodeChaufferie.Multiline = false;
            this.txtCodeChaufferie.Name = "txtCodeChaufferie";
            this.txtCodeChaufferie.ReadOnly = false;
            this.txtCodeChaufferie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeChaufferie.Size = new System.Drawing.Size(60, 27);
            this.txtCodeChaufferie.TabIndex = 505;
            this.txtCodeChaufferie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeChaufferie.UseSystemPasswordChar = false;
            this.txtCodeChaufferie.Visible = false;
            // 
            // Label50
            // 
            this.Label50.AutoSize = true;
            this.Label50.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label50.Location = new System.Drawing.Point(7, 18);
            this.Label50.Name = "Label50";
            this.Label50.Size = new System.Drawing.Size(69, 19);
            this.Label50.TabIndex = 4;
            this.Label50.TabStop = true;
            this.Label50.Tag = "0";
            this.Label50.Text = "Chantier";
            // 
            // Label24
            // 
            this.Label24.AutoSize = true;
            this.Label24.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label24.Location = new System.Drawing.Point(499, 46);
            this.Label24.Name = "Label24";
            this.Label24.Size = new System.Drawing.Size(63, 19);
            this.Label24.TabIndex = 3;
            this.Label24.TabStop = true;
            this.Label24.Text = "Contrat";
            // 
            // Label42
            // 
            this.Label42.AutoSize = true;
            this.Label42.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label42.Location = new System.Drawing.Point(499, 18);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(50, 19);
            this.Label42.TabIndex = 2;
            this.Label42.TabStop = true;
            this.Label42.Text = "Client";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.EtatChauff);
            this.groupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(5, 299);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(237, 57);
            this.groupBox1.TabIndex = 580;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Etat du Chauffage";
            // 
            // EtatChauff
            // 
            this.EtatChauff.AutoSize = true;
            this.EtatChauff.BackColor = System.Drawing.Color.Transparent;
            this.EtatChauff.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.EtatChauff.ForeColor = System.Drawing.Color.Red;
            this.EtatChauff.Location = new System.Drawing.Point(68, 27);
            this.EtatChauff.Name = "EtatChauff";
            this.EtatChauff.Size = new System.Drawing.Size(0, 19);
            this.EtatChauff.TabIndex = 384;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(351, 313);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(219, 19);
            this.label8.TabIndex = 583;
            this.label8.Text = "Techicien en charge du relevé ";
            // 
            // SSReleve
            // 
            this.SSReleve.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance1.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSReleve.DisplayLayout.Appearance = appearance1;
            this.SSReleve.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSReleve.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSReleve.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSReleve.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSReleve.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSReleve.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSReleve.DisplayLayout.MaxColScrollRegions = 1;
            this.SSReleve.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSReleve.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColorAlpha = Infragistics.Win.Alpha.Transparent;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSReleve.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSReleve.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSReleve.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSReleve.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSReleve.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSReleve.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.Edit;
            this.SSReleve.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSReleve.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSReleve.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSReleve.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSReleve.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSReleve.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSReleve.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSReleve.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSReleve.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSReleve.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSReleve.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSReleve.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSReleve.Location = new System.Drawing.Point(5, 362);
            this.SSReleve.Name = "SSReleve";
            this.SSReleve.Size = new System.Drawing.Size(850, 251);
            this.SSReleve.TabIndex = 584;
            this.SSReleve.Text = "ultraGrid1";
            this.SSReleve.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSReleve_InitializeLayout);
            this.SSReleve.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSReleve_InitializeRow);
            this.SSReleve.AfterExitEditMode += new System.EventHandler(this.SSReleve_AfterExitEditMode);
            this.SSReleve.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSReleve_AfterRowUpdate);
            this.SSReleve.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.SSReleve_BeforeExitEditMode);
            this.SSReleve.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SSReleve_KeyDown);
            // 
            // TimerLoad
            // 
            this.TimerLoad.Tick += new System.EventHandler(this.TimerLoad_Tick);
            // 
            // TimerRefresh
            // 
            this.TimerRefresh.Tick += new System.EventHandler(this.TimerRefresh_Tick);
            // 
            // SSTA
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSTA.DisplayLayout.Appearance = appearance13;
            this.SSTA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSTA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSTA.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSTA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSTA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSTA.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSTA.DisplayLayout.MaxColScrollRegions = 1;
            this.SSTA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSTA.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSTA.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSTA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSTA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSTA.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSTA.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSTA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSTA.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSTA.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSTA.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSTA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSTA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSTA.DisplayLayout.Override.RowAppearance = appearance23;
            this.SSTA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSTA.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSTA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSTA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSTA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSTA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSTA.Location = new System.Drawing.Point(220, 422);
            this.SSTA.Name = "SSTA";
            this.SSTA.Size = new System.Drawing.Size(148, 27);
            this.SSTA.TabIndex = 585;
            this.SSTA.Visible = false;
            // 
            // txtLibIntervenant
            // 
            this.txtLibIntervenant.AccAcceptNumbersOnly = false;
            this.txtLibIntervenant.AccAllowComma = false;
            this.txtLibIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibIntervenant.AccHidenValue = "";
            this.txtLibIntervenant.AccNotAllowedChars = null;
            this.txtLibIntervenant.AccReadOnly = false;
            this.txtLibIntervenant.AccReadOnlyAllowDelete = false;
            this.txtLibIntervenant.AccRequired = false;
            this.txtLibIntervenant.BackColor = System.Drawing.Color.White;
            this.txtLibIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtLibIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibIntervenant.ForeColor = System.Drawing.Color.Blue;
            this.txtLibIntervenant.Location = new System.Drawing.Point(565, 313);
            this.txtLibIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibIntervenant.MaxLength = 32767;
            this.txtLibIntervenant.Multiline = false;
            this.txtLibIntervenant.Name = "txtLibIntervenant";
            this.txtLibIntervenant.ReadOnly = false;
            this.txtLibIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibIntervenant.Size = new System.Drawing.Size(283, 27);
            this.txtLibIntervenant.TabIndex = 2;
            this.txtLibIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibIntervenant.UseSystemPasswordChar = false;
            // 
            // txtMatriculeIntervenant
            // 
            this.txtMatriculeIntervenant.AccAcceptNumbersOnly = false;
            this.txtMatriculeIntervenant.AccAllowComma = false;
            this.txtMatriculeIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMatriculeIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMatriculeIntervenant.AccHidenValue = "";
            this.txtMatriculeIntervenant.AccNotAllowedChars = null;
            this.txtMatriculeIntervenant.AccReadOnly = false;
            this.txtMatriculeIntervenant.AccReadOnlyAllowDelete = false;
            this.txtMatriculeIntervenant.AccRequired = false;
            this.txtMatriculeIntervenant.BackColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMatriculeIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtMatriculeIntervenant.Location = new System.Drawing.Point(266, 328);
            this.txtMatriculeIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtMatriculeIntervenant.MaxLength = 32767;
            this.txtMatriculeIntervenant.Multiline = false;
            this.txtMatriculeIntervenant.Name = "txtMatriculeIntervenant";
            this.txtMatriculeIntervenant.ReadOnly = false;
            this.txtMatriculeIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMatriculeIntervenant.Size = new System.Drawing.Size(80, 27);
            this.txtMatriculeIntervenant.TabIndex = 1;
            this.txtMatriculeIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMatriculeIntervenant.UseSystemPasswordChar = false;
            this.txtMatriculeIntervenant.Visible = false;
            // 
            // OptEcart
            // 
            this.OptEcart.AccAcceptNumbersOnly = false;
            this.OptEcart.AccAllowComma = false;
            this.OptEcart.AccBackgroundColor = System.Drawing.Color.White;
            this.OptEcart.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.OptEcart.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.OptEcart.AccHidenValue = "";
            this.OptEcart.AccNotAllowedChars = null;
            this.OptEcart.AccReadOnly = false;
            this.OptEcart.AccReadOnlyAllowDelete = false;
            this.OptEcart.AccRequired = false;
            this.OptEcart.BackColor = System.Drawing.Color.White;
            this.OptEcart.CustomBackColor = System.Drawing.Color.White;
            this.OptEcart.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptEcart.ForeColor = System.Drawing.Color.Black;
            this.OptEcart.Location = new System.Drawing.Point(266, 299);
            this.OptEcart.Margin = new System.Windows.Forms.Padding(2);
            this.OptEcart.MaxLength = 32767;
            this.OptEcart.Multiline = false;
            this.OptEcart.Name = "OptEcart";
            this.OptEcart.ReadOnly = false;
            this.OptEcart.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.OptEcart.Size = new System.Drawing.Size(80, 27);
            this.OptEcart.TabIndex = 0;
            this.OptEcart.TextAlignment = Infragistics.Win.HAlign.Left;
            this.OptEcart.UseSystemPasswordChar = false;
            this.OptEcart.Visible = false;
            // 
            // txtUtilVerrou
            // 
            this.txtUtilVerrou.AccAcceptNumbersOnly = false;
            this.txtUtilVerrou.AccAllowComma = false;
            this.txtUtilVerrou.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUtilVerrou.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUtilVerrou.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUtilVerrou.AccHidenValue = "";
            this.txtUtilVerrou.AccNotAllowedChars = null;
            this.txtUtilVerrou.AccReadOnly = false;
            this.txtUtilVerrou.AccReadOnlyAllowDelete = false;
            this.txtUtilVerrou.AccRequired = false;
            this.txtUtilVerrou.BackColor = System.Drawing.Color.White;
            this.txtUtilVerrou.CustomBackColor = System.Drawing.Color.White;
            this.txtUtilVerrou.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtUtilVerrou.ForeColor = System.Drawing.Color.Black;
            this.txtUtilVerrou.Location = new System.Drawing.Point(209, 89);
            this.txtUtilVerrou.Margin = new System.Windows.Forms.Padding(2);
            this.txtUtilVerrou.MaxLength = 32767;
            this.txtUtilVerrou.Multiline = false;
            this.txtUtilVerrou.Name = "txtUtilVerrou";
            this.txtUtilVerrou.ReadOnly = false;
            this.txtUtilVerrou.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUtilVerrou.Size = new System.Drawing.Size(100, 27);
            this.txtUtilVerrou.TabIndex = 575;
            this.txtUtilVerrou.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUtilVerrou.UseSystemPasswordChar = false;
            this.txtUtilVerrou.Visible = false;
            // 
            // txtChauffage
            // 
            this.txtChauffage.AccAcceptNumbersOnly = false;
            this.txtChauffage.AccAllowComma = false;
            this.txtChauffage.AccBackgroundColor = System.Drawing.Color.White;
            this.txtChauffage.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtChauffage.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtChauffage.AccHidenValue = "";
            this.txtChauffage.AccNotAllowedChars = null;
            this.txtChauffage.AccReadOnly = false;
            this.txtChauffage.AccReadOnlyAllowDelete = false;
            this.txtChauffage.AccRequired = false;
            this.txtChauffage.BackColor = System.Drawing.Color.White;
            this.txtChauffage.CustomBackColor = System.Drawing.Color.White;
            this.txtChauffage.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtChauffage.ForeColor = System.Drawing.Color.Black;
            this.txtChauffage.Location = new System.Drawing.Point(237, 65);
            this.txtChauffage.Margin = new System.Windows.Forms.Padding(2);
            this.txtChauffage.MaxLength = 32767;
            this.txtChauffage.Multiline = false;
            this.txtChauffage.Name = "txtChauffage";
            this.txtChauffage.ReadOnly = false;
            this.txtChauffage.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtChauffage.Size = new System.Drawing.Size(72, 27);
            this.txtChauffage.TabIndex = 574;
            this.txtChauffage.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtChauffage.UseSystemPasswordChar = false;
            this.txtChauffage.Visible = false;
            // 
            // txtDateAu
            // 
            this.txtDateAu.AccAcceptNumbersOnly = false;
            this.txtDateAu.AccAllowComma = false;
            this.txtDateAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateAu.AccHidenValue = "";
            this.txtDateAu.AccNotAllowedChars = null;
            this.txtDateAu.AccReadOnly = false;
            this.txtDateAu.AccReadOnlyAllowDelete = false;
            this.txtDateAu.AccRequired = false;
            this.txtDateAu.BackColor = System.Drawing.Color.White;
            this.txtDateAu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateAu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateAu.ForeColor = System.Drawing.Color.Black;
            this.txtDateAu.Location = new System.Drawing.Point(105, 91);
            this.txtDateAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateAu.MaxLength = 32767;
            this.txtDateAu.Multiline = false;
            this.txtDateAu.Name = "txtDateAu";
            this.txtDateAu.ReadOnly = false;
            this.txtDateAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateAu.Size = new System.Drawing.Size(100, 27);
            this.txtDateAu.TabIndex = 573;
            this.txtDateAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateAu.UseSystemPasswordChar = false;
            this.txtDateAu.Visible = false;
            // 
            // txtdateDu
            // 
            this.txtdateDu.AccAcceptNumbersOnly = false;
            this.txtdateDu.AccAllowComma = false;
            this.txtdateDu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtdateDu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtdateDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtdateDu.AccHidenValue = "";
            this.txtdateDu.AccNotAllowedChars = null;
            this.txtdateDu.AccReadOnly = false;
            this.txtdateDu.AccReadOnlyAllowDelete = false;
            this.txtdateDu.AccRequired = false;
            this.txtdateDu.BackColor = System.Drawing.Color.White;
            this.txtdateDu.CustomBackColor = System.Drawing.Color.White;
            this.txtdateDu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtdateDu.ForeColor = System.Drawing.Color.Black;
            this.txtdateDu.Location = new System.Drawing.Point(4, 91);
            this.txtdateDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtdateDu.MaxLength = 32767;
            this.txtdateDu.Multiline = false;
            this.txtdateDu.Name = "txtdateDu";
            this.txtdateDu.ReadOnly = false;
            this.txtdateDu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtdateDu.Size = new System.Drawing.Size(100, 27);
            this.txtdateDu.TabIndex = 572;
            this.txtdateDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtdateDu.UseSystemPasswordChar = false;
            this.txtdateDu.Visible = false;
            // 
            // txtDateIntervention
            // 
            this.txtDateIntervention.AccAcceptNumbersOnly = false;
            this.txtDateIntervention.AccAllowComma = false;
            this.txtDateIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateIntervention.AccHidenValue = "";
            this.txtDateIntervention.AccNotAllowedChars = null;
            this.txtDateIntervention.AccReadOnly = false;
            this.txtDateIntervention.AccReadOnlyAllowDelete = false;
            this.txtDateIntervention.AccRequired = false;
            this.txtDateIntervention.BackColor = System.Drawing.Color.White;
            this.txtDateIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtDateIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtDateIntervention.Location = new System.Drawing.Point(4, 65);
            this.txtDateIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateIntervention.MaxLength = 32767;
            this.txtDateIntervention.Multiline = false;
            this.txtDateIntervention.Name = "txtDateIntervention";
            this.txtDateIntervention.ReadOnly = false;
            this.txtDateIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateIntervention.Size = new System.Drawing.Size(69, 27);
            this.txtDateIntervention.TabIndex = 571;
            this.txtDateIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateIntervention.UseSystemPasswordChar = false;
            this.txtDateIntervention.Visible = false;
            // 
            // UserInter_RelevesCompteurs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(856, 612);
            this.Controls.Add(this.SSTA);
            this.Controls.Add(this.SSReleve);
            this.Controls.Add(this.txtLibIntervenant);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtMatriculeIntervenant);
            this.Controls.Add(this.OptEcart);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.chkTransfertP1);
            this.Controls.Add(this.chkValideReleve);
            this.Controls.Add(this.lblLectureSeule);
            this.Controls.Add(this.txtUtilVerrou);
            this.Controls.Add(this.txtChauffage);
            this.Controls.Add(this.txtDateAu);
            this.Controls.Add(this.txtdateDu);
            this.Controls.Add(this.txtDateIntervention);
            this.Controls.Add(this.chkLectureSeule);
            this.Controls.Add(this.cmdValider);
            this.Controls.Add(this.lblNoIntervention);
            this.Controls.Add(this.label33);
            this.ForeColor = System.Drawing.Color.Black;
            this.MinimumSize = new System.Drawing.Size(780, 650);
            this.Name = "UserInter_RelevesCompteurs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Relevé de compteurs";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.UserInter_RelevesCompteurs_FormClosing);
            this.Load += new System.EventHandler(this.UserInter_RelevesCompteurs_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSReleve)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSTA)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label lblNoIntervention;
        public System.Windows.Forms.Button cmdValider;
        public System.Windows.Forms.CheckBox chkLectureSeule;
        public iTalk.iTalk_TextBox_Small2 txtDateIntervention;
        public iTalk.iTalk_TextBox_Small2 txtdateDu;
        public iTalk.iTalk_TextBox_Small2 txtDateAu;
        public iTalk.iTalk_TextBox_Small2 txtChauffage;
        public iTalk.iTalk_TextBox_Small2 txtUtilVerrou;
        public System.Windows.Forms.Label lblLectureSeule;
        public System.Windows.Forms.CheckBox chkValideReleve;
        public System.Windows.Forms.CheckBox chkTransfertP1;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.LinkLabel Label50;
        private System.Windows.Forms.LinkLabel Label24;
        private System.Windows.Forms.LinkLabel Label42;
        public iTalk.iTalk_TextBox_Small2 txtCodeChaufferie;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_RichTextBox txtContrat;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoAuto;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public System.Windows.Forms.CheckBox P2;
        public System.Windows.Forms.CheckBox P3;
        public System.Windows.Forms.CheckBox P4;
        public System.Windows.Forms.CheckBox P1;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.Label EtatChauff;
        public iTalk.iTalk_TextBox_Small2 OptEcart;
        public iTalk.iTalk_TextBox_Small2 txtMatriculeIntervenant;
        public System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtLibIntervenant;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSReleve;
        private System.Windows.Forms.Timer TimerLoad;
        private System.Windows.Forms.Timer TimerRefresh;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSTA;
        public iTalk.iTalk_TextBox_Small2 txtCodeContrat;
        public System.Windows.Forms.Label txtAdresse3;
        public System.Windows.Forms.Label txtAdresse2_IMM;
        public System.Windows.Forms.Label txtAdresse;
        public System.Windows.Forms.Label txtVille;
        public System.Windows.Forms.Label txtRaisonSociale;
        public System.Windows.Forms.Label txtRaisonSocial_IMM;
        public System.Windows.Forms.Label txtCodeNumOrdre;
        public System.Windows.Forms.Label txtCodePostal;
        public System.Windows.Forms.Label txtCode1;
        public System.Windows.Forms.Label txtCodeImmeuble;
    }
}