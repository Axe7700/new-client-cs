﻿namespace Axe_interDT.Views.GoConteur.Forms
{
    partial class FrmMessageEcartIndex
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Opt3 = new System.Windows.Forms.RadioButton();
            this.Opt2 = new System.Windows.Forms.RadioButton();
            this.Opt1 = new System.Windows.Forms.RadioButton();
            this.FrmAppel = new iTalk.iTalk_TextBox_Small2();
            this.CmdOk = new System.Windows.Forms.Button();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.Opt3);
            this.groupBox6.Controls.Add(this.Opt2);
            this.groupBox6.Controls.Add(this.Opt1);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.groupBox6.Location = new System.Drawing.Point(3, 4);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(276, 109);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            // 
            // Opt3
            // 
            this.Opt3.AutoSize = true;
            this.Opt3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Opt3.Location = new System.Drawing.Point(48, 71);
            this.Opt3.Name = "Opt3";
            this.Opt3.Size = new System.Drawing.Size(204, 23);
            this.Opt3.TabIndex = 540;
            this.Opt3.TabStop = true;
            this.Opt3.Text = "   Conso négative normale";
            this.Opt3.UseVisualStyleBackColor = true;
            // 
            // Opt2
            // 
            this.Opt2.AutoSize = true;
            this.Opt2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Opt2.Location = new System.Drawing.Point(48, 45);
            this.Opt2.Name = "Opt2";
            this.Opt2.Size = new System.Drawing.Size(231, 23);
            this.Opt2.TabIndex = 539;
            this.Opt2.TabStop = true;
            this.Opt2.Text = "   Passage à zéro du compteur";
            this.Opt2.UseVisualStyleBackColor = true;
            // 
            // Opt1
            // 
            this.Opt1.AutoSize = true;
            this.Opt1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Opt1.Location = new System.Drawing.Point(48, 19);
            this.Opt1.Name = "Opt1";
            this.Opt1.Size = new System.Drawing.Size(184, 23);
            this.Opt1.TabIndex = 538;
            this.Opt1.TabStop = true;
            this.Opt1.Text = "   Correction  de l\'Index";
            this.Opt1.UseVisualStyleBackColor = true;
            // 
            // FrmAppel
            // 
            this.FrmAppel.AccAcceptNumbersOnly = false;
            this.FrmAppel.AccAllowComma = false;
            this.FrmAppel.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.FrmAppel.AccHidenValue = "";
            this.FrmAppel.AccNotAllowedChars = null;
            this.FrmAppel.AccReadOnly = false;
            this.FrmAppel.AccReadOnlyAllowDelete = false;
            this.FrmAppel.AccRequired = false;
            this.FrmAppel.BackColor = System.Drawing.Color.Transparent;
            this.FrmAppel.CustomBackColor = System.Drawing.Color.White;
            this.FrmAppel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.FrmAppel.ForeColor = System.Drawing.Color.Black;
            this.FrmAppel.Location = new System.Drawing.Point(3, 118);
            this.FrmAppel.Margin = new System.Windows.Forms.Padding(2);
            this.FrmAppel.MaxLength = 32767;
            this.FrmAppel.Multiline = false;
            this.FrmAppel.Name = "FrmAppel";
            this.FrmAppel.ReadOnly = false;
            this.FrmAppel.Size = new System.Drawing.Size(93, 23);
            this.FrmAppel.TabIndex = 502;
            this.FrmAppel.TextAlignment = Infragistics.Win.HAlign.Left;
            this.FrmAppel.UseSystemPasswordChar = false;
            this.FrmAppel.Visible = false;
            // 
            // CmdOk
            // 
            this.CmdOk.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdOk.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdOk.FlatAppearance.BorderSize = 0;
            this.CmdOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdOk.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdOk.ForeColor = System.Drawing.Color.White;
            this.CmdOk.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdOk.Location = new System.Drawing.Point(126, 118);
            this.CmdOk.Margin = new System.Windows.Forms.Padding(2);
            this.CmdOk.Name = "CmdOk";
            this.CmdOk.Size = new System.Drawing.Size(43, 29);
            this.CmdOk.TabIndex = 503;
            this.CmdOk.Text = "Ok.";
            this.CmdOk.UseVisualStyleBackColor = false;
            this.CmdOk.Click += new System.EventHandler(this.CmdOk_Click);
            // 
            // FrmMessageEcartIndex
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(312, 173);
            this.Controls.Add(this.CmdOk);
            this.Controls.Add(this.FrmAppel);
            this.Controls.Add(this.groupBox6);
            this.MinimumSize = new System.Drawing.Size(300, 186);
            this.Name = "FrmMessageEcartIndex";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "OPTION(écart négatif)";
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox groupBox6;
        public System.Windows.Forms.RadioButton Opt3;
        public System.Windows.Forms.RadioButton Opt2;
        public System.Windows.Forms.RadioButton Opt1;
        public iTalk.iTalk_TextBox_Small2 FrmAppel;
        public System.Windows.Forms.Button CmdOk;
    }
}