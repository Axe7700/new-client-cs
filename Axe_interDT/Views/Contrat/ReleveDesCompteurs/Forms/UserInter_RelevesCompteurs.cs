﻿using Axe_interDT.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.GoConteur.Forms
{
    public partial class UserInter_RelevesCompteurs : Form
    {
        public UserInter_RelevesCompteurs()
        {
            InitializeComponent();
        }
        int lngMarcheArret;
        int Decade;
        int Nbj;
        string strEnergie;
        DataTable rsAdo;
        short ARRET;
        bool Temp;
        int wNb;
        bool Prem;
        bool Der;
        private Color YesStyle = ColorTranslator.FromOle(0xC0FFFF);
        private Color NoStyle = Color.White;

       
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkValideReleve_CheckedChanged(object sender, EventArgs e)
        {
            if (chkValideReleve.CheckState == System.Windows.Forms.CheckState.Checked)
            {
                chkTransfertP1.Enabled = true;
            }
            else
            {
                chkTransfertP1.Enabled = false;
                chkTransfertP1.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }
        }

        private DataTable AdodcReleves;

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private bool fc_ControleCoherence()
        {
            bool functionReturnValue = false;
            short i = 0;
            string sEtat = null;

            SSReleve.UpdateData();

            functionReturnValue = true;
            var ModAdoADOlibelle = new ModAdo();
            if (chkTransfertP1.CheckState == System.Windows.Forms.CheckState.Checked)//tested
            {
                //Contrôle de l'état de l'intervention : si ERR ou AN ne pas envoyer les relevés vers le P1
                sEtat = ModAdoADOlibelle.fc_ADOlibelle("SELECT CODEETAT FROM INTERVENTION WHERE NOINTERVENTION = '" + txtNoIntervention.Text + "'");
                if (sEtat == "ERR" || sEtat == "AN")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code état de l'intervention " + txtNoIntervention.Text + " (" + sEtat + ") ne vous permet pas transferer ces relevés vers le programme de suivi des consommations.", "Enregistrement annulé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }
            }


            if (SSReleve.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun relevé à enregistrer (le tableau des relevés est vide)", "Enregistrement annulé", MessageBoxButtons.OK, MessageBoxIcon
                    .Information);
                functionReturnValue = false;
                return functionReturnValue;
            }

            if (chkTransfertP1.CheckState == System.Windows.Forms.CheckState.Checked)
            {

                if (AdodcReleves.Rows.Count > 0)
                {
                    foreach (DataRow dr in AdodcReleves.Rows)
                    {
                        if (string.IsNullOrEmpty(dr["IndexFin"] + "") && dr["Evenement"] + "" != "9")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Tous les relevés n'ont pas été effectués ou saisis, vous ne pouvez pas transferer ces résultats vers le programme de suivi des consommations.", "Enregistrement annulé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            functionReturnValue = false;
                            return functionReturnValue;
                        }
                    }
                }
            }
            return functionReturnValue;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNoIntervention"></param>
        /// <returns></returns>
        private object fc_AffRelevesInter(string sNoIntervention)
        {
            object functionReturnValue = null;
            string SQL = null;
            int nbRecAff = 0;
            short i = 0;

            try
            {
                lblNoIntervention.Text = "N° " + String.Format(sNoIntervention, "### ##0");
                var ModAdoADOlibelle = new ModAdo();
                //Initisaliser le drop down type d'event
                sheridan.InitialiseCombo(SSTA, "SELECT Code AS [Event], Libelle" + " FROM TypeAnomalie WHERE Code = '1' OR Code = '2' OR code ='3' OR code = '4' OR code = '8' OR code = '9' ", "Event", true);
                //sheridan.InitialiseCombo((this.SSTA), Adodc5, "SELECT Code AS [Event], Libelle" + " FROM TypeAnomalie WHERE Code = '1' OR Code = '2' OR code ='3' OR code = '4' OR code = '8' OR code = '9' ", "Event", true);

                fc_AffImmeuble(sNoIntervention);

                //Si aucun relevé n'a été préchargé :
                // Création de ces derniers en fonction des compteurs du chantier
                SQL = "IF EXISTS(SELECT 'a' FROM INT_Releve WHERE NoIntervention = " + General.nz(sNoIntervention, "0") + ") SELECT '1' AS Chauff ELSE SELECT '0' AS Chauff";
                if (ModAdoADOlibelle.fc_ADOlibelle(SQL) == "0")
                {
                    //Aucun relevé pré-chargé
                    ModPDA.App_Info((txtCodeImmeuble.Text), (txtCodeNumOrdre.Text), (txtCodeChaufferie.Text), sNoIntervention);
                }
                else//TESTED
                {
                    //Affichage de l'état de la chaufferie
                    Rech_EtatChauff();
                }
                ModAdo ModAdoAdodcReleves = new ModAdo();
                txtUtilVerrou.Text = ModAdoADOlibelle.fc_ADOlibelle("SELECT COALESCE(UtilVerrou, '" + General.gsUtilisateur + "') FROM INT_Releve WHERE NoIntervention = " + General.nz(sNoIntervention, "0"));

                fc_GetLivraisons();

                //Efface les données temporaires de saisie des relevés propre à l'intervenrion affichée
                //    SQL = "DELETE FROM INT_ReleveTemp WHERE Utilisateur = '" & gsUtilisateur & "' AND INT_ReleveTemp.NoIntervention = " & nz(sNoIntervention, "0")
                SQL = "DELETE FROM INT_ReleveTemp WHERE Utilisateur = '" + General.gsUtilisateur + "'";
                General.Execute(SQL);

                //Stocke dans la table temporaire les relevés à contrôler : modification et validation
                SQL = "INSERT INTO INT_ReleveTemp (";
                SQL = SQL + " NoAuto, Etat, Utilisateur, NoFichierGraphe, NumAppareil, CodeUO, CodeAffaire, CodeNumOrdre, ";
                SQL = SQL + " CodeChaufferie, Saison, NoDecade, Periodedu, PeriodeAu, ";
                SQL = SQL + " CodeTransmission, CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, ";
                SQL = SQL + " Livraison, Consommation, Estimation, Anc_IndexDebut, Anc_IndexFin,";
                SQL = SQL + " Evenement, LibelleEvent, Date_Event, Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, ";
                SQL = SQL + " CoefPTA, CoefPCS, VolConverti, NomFic, DateImport,";
                SQL = SQL + " NoIntervention, Libel_App, chkLiv, SaisieJauge, SaisiePige, dPeriodeDu, dPeriodeAu, DiametreCuve)";
                SQL = SQL + " SELECT INT_Releve.NoAuto, INT_Releve.Etat, '" + General.gsUtilisateur + "' AS Utilisateur, INT_Releve.NoFichierGraphe, INT_Releve.NumAppareil, ";
                SQL = SQL + " INT_Releve.CodeUO, INT_Releve.CodeAffaire, INT_Releve.CodeNumOrdre, INT_Releve.CodeChaufferie, Saison, NoDecade, Periodedu, ";
                SQL = SQL + " PeriodeAu, CodeTransmission, ";
                SQL = SQL + " CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, INT_Releve.Livraison, Consommation, ";
                SQL = SQL + " Estimation, Anc_IndexDebut, Anc_IndexFin, Evenement, LibelleEvent,";
                SQL = SQL + " Date_Event , Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, CoefPTA, ";
                SQL = SQL + " CoefPCS, VolConverti, NomFic, DateImport, NoIntervention, Libel_App, COALESCE(P1_TypeCpt.Livraison, ChkLiv) AS CHKLIV";
                SQL = SQL + " , SaisieJauge, SaisiePige, COALESCE(dPeriodeDu, PeriodeDu), COALESCE(dPeriodeAu, PeriodeAu), INT_Releve.DiametreCuve ";
                SQL = SQL + " FROM INT_Releve INNER JOIN";
                SQL = SQL + " Imm_Appareils ON INT_Releve.CodeUO = Imm_Appareils.CodeUO ";
                SQL = SQL + " AND INT_Releve.NumAppareil = Imm_Appareils.NumAppareil ";
                SQL = SQL + " INNER JOIN P1_TypeCpt ON ";
                SQL = SQL + " Imm_Appareils.Type_Compteur = P1_TypeCpt.CODE ";
                SQL = SQL + " WHERE INT_Releve.NoIntervention = " + General.nz(sNoIntervention, "0");

                General.Execute(SQL);

                //Affiche, dans le tableau, les relevés associés à l'intervention
                SQL = " SELECT TypeEtatReleves.Designation AS DesignationEtat";
                SQL = SQL + " , Etat";
                SQL = SQL + " , NumAppareil";
                SQL = SQL + " , Libel_App";
                SQL = SQL + " , Evenement";
                SQL = SQL + " , LibelleEvent AS Libelle";
                SQL = SQL + " , Saison";
                SQL = SQL + " , NoDecade";
                SQL = SQL + " , dPeriodeDu";
                SQL = SQL + " , dPeriodeAu";
                SQL = SQL + " , PeriodeDu";
                SQL = SQL + " , PeriodeAu";
                SQL = SQL + " , NJours";
                SQL = SQL + " , IndexDebut";
                SQL = SQL + " , SaisieJauge";
                SQL = SQL + " , SaisiePige";
                SQL = SQL + " , IndexFin";
                SQL = SQL + " , CoefPTA";
                SQL = SQL + " , VolConverti";
                SQL = SQL + " , CoefPCS";
                SQL = SQL + " , Livraison";
                SQL = SQL + " , Consommation";
                SQL = SQL + " , Estimation";
                SQL = SQL + " , Anc_IndexDebut";
                SQL = SQL + " , Anc_IndexFin";
                SQL = SQL + " , ChkLiv";
                SQL = SQL + " , DiametreCuve";
                SQL = SQL + " , NoAuto";
                SQL = SQL + " , Chauffage";
                SQL = SQL + " , bModifDecade";
                SQL = SQL + " FROM INT_ReleveTemp ";
                SQL = SQL + " LEFT OUTER JOIN TypeEtatReleves ON ";
                SQL = SQL + " INT_ReleveTemp.Etat = TypeEtatReleves.Code ";
                //    SQL = SQL & " LEFT OUTER JOIN TypeAnomalie ON "
                //    SQL = SQL & " INT_ReleveTemp.CodeAnomalie = TypeAnomalie.Code "
                SQL = SQL + " WHERE Utilisateur = '" + General.gsUtilisateur + "'";
                SQL = SQL + " AND NoIntervention = " + sNoIntervention;
                SQL = SQL + " ORDER BY NumAppareil";

                AdodcReleves = new DataTable();
                //_with2.CommandType = ADODB.CommandTypeEnum.adCmdText;
                //if (string.IsNullOrEmpty(_with2.ConnectionString))
                //{
                //    _with2.ConnectionString = General.adocnn.ConnectionString;
                //}
                var dt = new DataTable();
                dt.Columns.Add("DesignationEtat");
                dt.Columns.Add("Etat");
                dt.Columns.Add("No Compteur");
                dt.Columns.Add("Libel_App");
                dt.Columns.Add("Event");
                dt.Columns.Add("Libel");
                dt.Columns.Add("Saison");
                dt.Columns.Add("NoDecade");
                dt.Columns.Add("Periode_Du");
                dt.Columns.Add("Periode_Au");
                dt.Columns.Add("Releve_Du");
                dt.Columns.Add("Releve_Au");
                dt.Columns.Add("Nbj");
                dt.Columns.Add("Index_Deb");
                dt.Columns.Add("Jauge");
                dt.Columns.Add("Pige");
                dt.Columns.Add("Index_Fin");
                dt.Columns.Add("CoefPTA");
                dt.Columns.Add("VolumeConverti");
                dt.Columns.Add("PCSMoyen");
                dt.Columns.Add("Livraison");
                dt.Columns.Add("Conso");
                dt.Columns.Add("Estim");
                dt.Columns.Add("A_indexDeb");
                dt.Columns.Add("A_IndexFin");
                dt.Columns.Add("ChkLiv");
                dt.Columns.Add("DiametreCuve");
                dt.Columns.Add("NoAuto");
                dt.Columns.Add("Chauffage");
                dt.Columns.Add("bModifDecade");

                AdodcReleves = ModAdoAdodcReleves.fc_OpenRecordSet(SQL);
                foreach (DataRow dr in AdodcReleves.Rows)
                {
                    var drdt = dt.NewRow();
                    drdt.ItemArray = dr.ItemArray;
                    dt.Rows.Add(drdt);
                }
                SSReleve.DataSource = dt;
                //_with2.RecordSource = SQL;
                //_with2.Refresh();
                SSReleve.UpdateData();
                //this.SSReleve.ActiveRow.Cells["Event"].DropDownHwnd = this.SSTA.hWnd;
                //SSReleve.DisplayLayout.Bands[0].Columns["Event"].ValueList = SSTA;//TODO this line occurs an excpetion null reference .i moved this line to initialise layout event 

                if (txtUtilVerrou.Text != General.gsUtilisateur && !string.IsNullOrEmpty(txtUtilVerrou.Text))
                {
                    chkLectureSeule.CheckState = CheckState.Checked;
                    fc_VerrouilleForm();
                }
                else
                {
                    chkLectureSeule.CheckState = CheckState.Unchecked;
                    fc_DeVerrouilleForm();
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_AffRelevesInter;");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_VerrouilleForm()
        {
            //Cas lecture seule
            this.Text = "Relevé de compteurs - Lecture seule - Vérouillé par " + txtUtilVerrou.Text;
            lblLectureSeule.Visible = true;
            cmdValider.Enabled = false;
            //SSReleve.AllowUpdate = false;
            SSReleve.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
            chkValideReleve.Enabled = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DeVerrouilleForm()
        {
            //Cas Lecture + écriture
            this.Text = "Relevé de compteurs";
            lblLectureSeule.Visible = false;
            cmdValider.Enabled = true;
            //SSReleve.AllowUpdate = true;
            SSReleve.DisplayLayout.Override.AllowUpdate = DefaultableBoolean.True;
            chkValideReleve.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_GetLivraisons()
        {
            object functionReturnValue = null;
            string[] tabChantier = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {

                General.sSQL = "EXEC Write_Livraisons_Releves ";

                if (General.IsDate(txtdateDu.Text))//tested
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + txtdateDu.Text + "'";
                }
                else
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + DateTime.Today + "'";
                }

                if (General.IsDate(txtDateAu.Text))//tested
                {
                    General.sSQL = General.sSQL + " , @DateAu = '" + txtDateAu.Text + "'";
                }
                else
                {
                    General.sSQL = General.sSQL + " , @DateAu = '" + DateTime.Today + "'";
                }

                General.sSQL = General.sSQL + ", @CodeUO = '" + General.strCodeUO + "'";

                General.sSQL = General.sSQL + ", @CodeImmeuble = '" + txtCodeImmeuble.Text + "'";

                General.sSQL = General.sSQL + ", @CodeNumOrdre = '" + txtCodeNumOrdre.Text + "'";

                General.Execute(General.sSQL);
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_GetLivraisons;");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNoIntervention"></param>
        /// <returns></returns>
        private object fc_AffImmeuble(string sNoIntervention)
        {
            object functionReturnValue = null;
            string sSql = null;
            short i = 0;
            var ModAdoADOlibelle = new ModAdo();
            try
            {
                sSql = "";
                sSql = "SELECT Immeuble.Adresse, Immeuble.Adresse2_IMM," + "Immeuble.CodePostal, Immeuble.Ville," + " Immeuble.AngleRue, " + " Immeuble.Observations, Immeuble.ObservationComm_IMM," + " Immeuble.CodeImmeuble AS RaisonSociale, Immeuble.codeAcces1, Immeuble.CodeAcces2," + " Immeuble.code1, Immeuble.sec_code," + " Immeuble.CodeImmeuble, '00' AS CodeNumOrdre, '0000' AS CodeChaufferie, Immeuble.CS1, Immeuble.Gardien, " + " Immeuble.CodeDepanneur, Immeuble.BoiteAClef, Immeuble.SitBoiteAClef, " + " Immeuble.SpecifAcces, Immeuble.TelGardien, Immeuble.CodeCommercial, " + " Immeuble.CodeDispatcheur, Immeuble.CodeDepanneur, Immeuble.CodeChefSecteur, " + " Immeuble.CodeDepanneur1," + " Immeuble.VMC,Immeuble.SURP";
                sSql = sSql + " , Intervention.DPeriodeDu, Intervention.DPeriodeAu ";
                sSql = sSql + " , Intervention.Intervenant, Personnel.Prenom + ' ' + Personnel.Nom AS Technicien ";
                sSql = sSql + " FROM Immeuble ";

                sSql = sSql + " LEFT OUTER JOIN Intervention ON ";
                sSql = sSql + " Immeuble.CodeImmeuble = Intervention.CodeImmeuble ";
                //    sSql = sSql & " AND Immeuble.CodeNumOrdre = Intervention.CodeNumOrdre "
                //    If sUChaufferie = "1" Then
                //        sSql = sSql & " AND Immeuble.CodeChaufferie = Intervention.CodeChaufferie "
                //    End If

                sSql = sSql + " LEFT OUTER JOIN Personnel ON ";
                sSql = sSql + " Intervention.Intervenant = Personnel.Matricule ";

                //    sSql = sSql & " LEFT OUTER JOIN IMM_Adresses ON "
                //    sSql = sSql & " Immeuble.CodeImmeuble = IMM_Adresses.CodeImmeuble "
                //    sSql = sSql & " AND Immeuble.CodeNumOrdre = IMM_Adresses.CodeNumOrdre "
                //    If sUChaufferie = "1" Then
                //        sSql = sSql & " AND Immeuble.CodeChaufferie = IMM_Adresses.CodeChaufferie "
                //    End If
                sSql = sSql + " WHERE Intervention.NoIntervention = " + General.nz(sNoIntervention, "0") + "";
                ModAdo ModAdorstmp = new ModAdo();
                General.rstmp = ModAdorstmp.fc_OpenRecordSet(sSql);

                if (General.rstmp.Rows.Count > 0)
                {
                    txtCodeImmeuble.Text = General.rstmp.Rows[0]["CodeImmeuble"] + "";
                    txtCodeNumOrdre.Text = "00";
                    // rstmp!CodeNumOrdre & ""
                    txtCodeChaufferie.Text = "0000";
                    //rstmp!CodeChaufferie & ""
                    //        If rstmp!AdrImm & "" = "" Then
                    this.txtAdresse.Text = General.nz(General.rstmp.Rows[0]["Adresse"] + "", "").ToString();
                    this.txtAdresse2_IMM.Text = General.nz(General.rstmp.Rows[0]["Adresse2_Imm"], "").ToString();
                    this.txtAdresse3.Text = General.nz(General.rstmp.Rows[0]["AngleRue"], "").ToString();
                    this.txtCodePostal.Text = General.nz(General.rstmp.Rows[0]["CodePostal"], "").ToString();
                    this.txtVille.Text = General.nz(General.rstmp.Rows[0]["Ville"], "").ToString();
                    //        Else
                    //            Me.txtAdresse = nz(rstmp!Adresse1 & "", "")
                    //            Me.txtAdresse2_IMM = nz(rstmp!Adresse2, "")
                    //            Me.txtAdresse3.Text = nz(rstmp!Adresse3, "")
                    //            Me.txtCodePostal = nz(rstmp!CP, "")
                    //            Me.txtVille = nz(rstmp!VilleImm, "")
                    //        End If
                    this.txtCode1.Text = General.nz(General.rstmp.Rows[0]["Code1"], "").ToString();
                    this.txtRaisonSociale.Text = ModAdoADOlibelle.fc_ADOlibelle("SELECT NOM FROM TABLE1 WHERE CODE1='" + General.nz(General.rstmp.Rows[0]["Code1"], "") + "'").ToString();
                    this.txtRaisonSocial_IMM.Text = General.nz(General.rstmp.Rows[0]["RaisonSociale"], "").ToString();

                    txtMatriculeIntervenant.Text = General.rstmp.Rows[0]["Intervenant"] + "";
                    txtLibIntervenant.Text = General.rstmp.Rows[0]["Intervenant"] + " - " + General.rstmp.Rows[0]["Technicien"] + "";

                    if (General.IsDate(General.rstmp.Rows[0]["dPeriodeDu"] + ""))
                    {
                        txtdateDu.Text = General.rstmp.Rows[0]["dPeriodeDu"] + "";
                    }
                    else
                    {
                        txtdateDu.Text = Convert.ToString(DateTime.Today);
                    }

                    if (General.IsDate(General.rstmp.Rows[0]["dPeriodeAu"] + ""))
                    {
                        txtDateAu.Text = General.rstmp.Rows[0]["dPeriodeAu"] + "";
                    }
                    else
                    {
                        txtDateAu.Text = Convert.ToString(DateTime.Today);
                    }

                }
                else
                {
                    txtCodeImmeuble.Text = "";
                    txtCodeNumOrdre.Text = "";
                    txtCodeChaufferie.Text = "";
                    this.txtRaisonSocial_IMM.Text = "";
                    this.txtAdresse.Text = "";
                    this.txtAdresse2_IMM.Text = "";
                    this.txtCodePostal.Text = "";
                    this.txtVille.Text = "";
                    this.txtCode1.Text = "";
                    this.txtRaisonSociale.Text = "";
                    this.txtdateDu.Text = Convert.ToString(DateTime.Today);
                    this.txtDateAu.Text = Convert.ToString(DateTime.Today);

                    txtMatriculeIntervenant.Text = "";
                    txtLibIntervenant.Text = "";

                }

                fc_RechercheGMAO((txtCodeImmeuble.Text), (txtCodeNumOrdre.Text), (txtCodeChaufferie.Text));
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_AffImmeuble;");
                return functionReturnValue;
            }



        }

        //UNUSED function . return in first line
        private void fc_RechercheGMAO(string sCodeImmeuble, string sCodeNumOrdre, string sCodeChaufferie)
        {
            int i = 0;
            short intResilie = 0;
            DateTime dtDateFin = default(DateTime);

            //intResilie = -1    => pas de contrat pour ce chantier
            //intResilie = 0     => le contrat est en fonctionnement normal (il n'est pas résilié)
            //intResilie = 1     => le contrat est en cours de résiliation
            //intResilie = 2     => le contrat est résilié

            return;
            try
            {
                if (General.rstmp == null)
                {
                    General.rstmp = new DataTable();
                }
                else
                {
                    //if (General.rstmp.State == ADODB.ObjectStateEnum.adStateOpen)
                    //{
                    General.rstmp.Clear();
                    //}
                }

                General.sSQL = "";
                General.sSQL = "SELECT COP_ContratP2.COP_NoAuto, COP_ContratP2.Cop_Avenant as CodeAvenant, COP_ContratP2.Cop_NoContrat AS NumContrat, COP_ContratP2.Cop_Titre AS Libelle, ";
                General.sSQL = General.sSQL + " COP_ContratP2.COP_DateEffet AS DateDebutContrat,COP_ContratP2.COP_DateFin AS DateFinContrat,";
                General.sSQL = General.sSQL + " COP_ContratP2.COP_TaciteRecond AS TaciteRecond, COP_ContratP2.COP_DateFinExpl AS DateFinExpl, ";
                General.sSQL = General.sSQL + " COP_ContratP2.COP_P1, COP_ContratP2.COP_P2, COP_ContratP2.COP_P3, COP_ContratP2.COP_P4 ";
                General.sSQL = General.sSQL + " FROM COP_ContratP2 ";
                General.sSQL = General.sSQL + " WHERE COP_ContratP2.CodeImmeuble = '" + sCodeImmeuble + "' ";
                General.sSQL = General.sSQL + "AND COP_ContratP2.CodeNumOrdre = '" + sCodeNumOrdre + "' ";
                if (General.sUChaufferie == "1")
                {
                    General.sSQL = General.sSQL + "AND COP_ContratP2.CodeChaufferie = '" + sCodeChaufferie + "' ";
                }
                General.sSQL = General.sSQL + "AND COP_ContratP2.CodeUO = '" + General.strCodeUO + "' ";
                General.sSQL = General.sSQL + "ORDER BY COP_ContratP2.COP_DateFin ASC,COP_ContratP2.COP_DateFinExpl ASC";
                ModAdo ModAdorstmp = new ModAdo();
                General.rstmp = ModAdorstmp.fc_OpenRecordSet(General.sSQL);

                txtContrat.Text = "";
                //txtContrat.Length = 0;

                //Rappel :
                //intResilie = -1    => pas de contrat pour ce chantier
                //intResilie = 0     => le contrat est en fonctionnement normal (il n'est pas résilié)
                //intResilie = 1     => le contrat est en cours de résiliation
                //intResilie = 2     => le contrat est résilié

                if (General.rstmp.Rows.Count > 0)
                {

                    for (i = 0; i <= General.rstmp.Rows.Count - 1; i++)
                    {
                        if (General.IsDate(General.rstmp.Rows[i]["DateFinContrat"] + ""))
                        {
                            dtDateFin = Convert.ToDateTime(General.rstmp.Rows[i]["DateFinContrat"] + "");
                        }
                        else
                        {
                            //dtDateFin = DateAndTime.DateAdd(Microsoft.VisualBasic.DateInterval.Year, 2, DateAndTime.Today);
                            dtDateFin = DateTime.Now.AddYears(2);
                        }

                        if (General.rstmp.Rows[i]["TaciteRecond"] + "" == "0")
                        {
                            if (General.IsDate(General.rstmp.Rows[i]["DateFinExpl"] + ""))
                            {
                                //Si le contrat est résilié dans les 3 mois à venir
                                if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Now && DateTime.Now.AddMonths(3) <= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                                {
                                    intResilie = 1;
                                }
                                else if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Now && DateTime.Now.AddMonths(3) >= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                                {
                                    intResilie = 1;
                                }
                                else
                                {
                                    intResilie = 2;
                                }
                            }
                            else if (dtDateFin.Year == DateTime.Today.Year)
                            {
                                //Contrat bientôt résilié (résiliable)
                                if (dtDateFin > DateTime.Now)
                                {
                                    intResilie = 1;
                                    //Contrat résilié
                                }
                                else
                                {
                                    if (General.IsDate(dtDateFin))
                                    {
                                        //Si le contrat est résilié dans les 3 mois à venir
                                        if (dtDateFin > DateTime.Now && DateTime.Now.AddMonths(3) <= dtDateFin)
                                        {
                                            intResilie = 1;
                                        }
                                        else if (dtDateFin > DateTime.Now && DateTime.Now.AddMonths(3) >= dtDateFin)
                                        {
                                            intResilie = 1;
                                        }
                                        else
                                        {
                                            intResilie = 2;
                                        }
                                        //Le contrat n'a pas été encore résilié officiellement
                                    }
                                    else
                                    {
                                        intResilie = 1;
                                    }
                                }
                                //Le contrat est résilié
                            }
                            else if (dtDateFin < DateTime.Now)
                            {
                                intResilie = 2;
                                //Le contrat est en cours d'exécution
                            }
                            else
                            {
                                intResilie = 0;
                            }
                        }
                        else
                        {
                            if (General.IsDate(General.rstmp.Rows[i]["DateFinExpl"] + ""))
                            {
                                if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]).Year == DateTime.Today.Year)
                                {
                                    intResilie = 1;
                                }
                                else if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]).Year > DateTime.Today.Year)
                                {
                                    intResilie = 1;
                                }
                                else
                                {
                                    intResilie = 2;
                                }
                            }
                            else
                            {
                                intResilie = 0;
                                txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                            }
                        }

                    }
                    if (General.rstmp.Rows.Count == 0)
                    {
                        // General.rstmp.MovePrevious();
                    }
                    if (General.rstmp.Rows[i]["TaciteRecond"] + "" == "0")
                    {
                        if (General.IsDate(General.rstmp.Rows[i]["DateFinExpl"] + ""))
                        {
                            //Si le contrat est résilié dans plus de 3 mois : pas d'alerte
                            if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && DateTime.Now.AddMonths(3) <= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                intResilie = 1;

                                //Si le contrat est résilié dans les 3 mois à venir
                            }
                            else if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && DateTime.Now.AddMonths(3) >= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                intResilie = 1;

                                //Le contrat est résilié
                            }
                            else
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(0xff);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                intResilie = 2;
                            }
                        }
                        else if (dtDateFin.Year == DateTime.Today.Year)
                        {
                            //Contrat bientôt résilié (résiliable)
                            if (dtDateFin > DateTime.Today)
                            {
                                intResilie = 1;
                                txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                txtContrat.Text = dtDateFin + " : ";
                                //Contrat résilié
                            }
                            else
                            {
                                if (General.IsDate(General.rstmp.Rows[i]["DateFinExpl"] + ""))
                                {
                                    //Si le contrat est résilié dans les 3 mois à venir
                                    if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && DateTime.Now.AddMonths(3) <= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                                    {
                                        txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                        txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                        intResilie = 1;
                                    }
                                    else if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && DateTime.Now.AddMonths(3) >= Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]))
                                    {
                                        txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                                        txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                        intResilie = 1;
                                    }
                                    else
                                    {
                                        txtContrat.BackColor = ColorTranslator.FromOle(0xff);
                                        txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                        intResilie = 2;
                                    }
                                    //Le contrat n'a pas été encore résilié officiellement
                                }
                                else
                                {
                                    txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                    txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                                    intResilie = 1;
                                }
                            }
                            //Le contrat est résilié
                        }
                        else if (dtDateFin < DateTime.Today)
                        {
                            txtContrat.BackColor = ColorTranslator.FromOle(0xff);
                            txtContrat.Text = dtDateFin + " : ";
                            intResilie = 2;
                            //Le contrat est en cours d'exécution
                        }
                        else
                        {
                            txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                            intResilie = 0;
                        }
                    }
                    else
                    {
                        if (General.IsDate(General.rstmp.Rows[i]["DateFinExpl"] + ""))
                        {
                            if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]).Year == DateTime.Today.Year)
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                            }
                            else if (Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]) > DateTime.Today && Convert.ToDateTime(General.rstmp.Rows[i]["DateFinExpl"]).Year > DateTime.Today.Year)
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(0x80ff);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                            }
                            else
                            {
                                txtContrat.BackColor = ColorTranslator.FromOle(0xff);
                                txtContrat.Text = General.rstmp.Rows[i]["DateFinExpl"] + " : ";
                            }
                        }
                        else
                        {
                            intResilie = 0;
                            txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                        }
                    }
                    txtContrat.Text = txtContrat.Text + General.rstmp.Rows[i]["Libelle"] + "";
                    txtCodeContrat.Text = General.rstmp.Rows[i]["NumContrat"] + "";
                    txtCOP_NoAuto.Text = General.rstmp.Rows[i]["COP_NoAuto"] + "";


                    foreach (DataRow Dr in General.rstmp.Rows)
                    {
                        if (Dr["COP_P1"] + "" == "1")
                        {
                            P1.CheckState = CheckState.Checked;
                        }
                        if (Dr["COP_P2"] + "" == "1")
                        {
                            P2.CheckState = CheckState.Checked;
                        }
                        if (Dr["COP_P3"] + "" == "1")
                        {
                            P3.CheckState = CheckState.Checked;
                        }
                        if (Dr["COP_P4"] + "" == "1")
                        {
                            P4.CheckState = CheckState.Checked;
                        }
                    }
                }
                else
                {
                    txtContrat.Text = "Pas de contrat pour ce chantier";
                    txtCodeContrat.Text = "";
                    txtContrat.BackColor = ColorTranslator.FromOle(14737632);
                    intResilie = -1;
                    P1.CheckState = CheckState.Unchecked;
                    P2.CheckState = CheckState.Unchecked;
                    P3.CheckState = CheckState.Unchecked;
                    P4.CheckState = CheckState.Unchecked;
                }
                General.rstmp.Clear();

                if (General.FacturesP2Lisa == "0")
                {
                    General.sSQL = "SELECT CodeAffaire,CodeNumOrdre,Resiliee FROM P12000 WHERE CodeAffaire='" + sCodeImmeuble + "' AND CodeNumOrdre='" + sCodeNumOrdre + "' AND CodeUO='" + General.strCodeUO + "'";
                    General.rstmp = ModAdorstmp.fc_OpenRecordSet(General.sSQL, null, null, ModP1.adoP1);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        P1.CheckState = CheckState.Checked;
                        if (General.rstmp.Rows[0]["Resiliee"] + "" == "1")
                        {
                            P1.ForeColor = ColorTranslator.FromOle(0xff);
                        }
                        else
                        {
                            P1.ForeColor = ColorTranslator.FromOle(0x800);
                        }
                    }
                    else
                    {
                        P1.ForeColor = ColorTranslator.FromOle(0x800);
                        P1.CheckState = CheckState.Unchecked;
                    }
                    General.rstmp.Clear();
                }
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Fc_RechercheGMAO;");
                return;
            }
        }

        //Uncalled function
        private string Rech_Daterel()
        {
            string functionReturnValue = null;
            General.SQL = "SELECT PeriodeAu, Saison, Periodedu," + " NoFichierGraphe" + " From INT_Releve" + " WHERE  " + " CodeImmeuble = '" + txtCodeImmeuble.Text + "' " + " AND CodeNumOrdre = '" + txtCodeNumOrdre.Text + "' " + " AND Codechaufferie = '" + txtCodeChaufferie.Text + "' " + " ORDER BY INT_Releve.PeriodeAu DESC, Saison DESC";
            ModAdo ModAdorsAdo = new ModAdo();
            rsAdo = ModAdorsAdo.fc_OpenRecordSet(General.SQL);

            if (rsAdo.Rows.Count > 0)
            {
                functionReturnValue = rsAdo.Rows[0]["PeriodeAu"] + "";
                Rech_EtatChauff();
            }
            else
            {
                functionReturnValue = "";
                Rech_EtatChauff();
            }
            rsAdo = null;
            return functionReturnValue;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void Rech_EtatChauff()
        {
            rsAdo = new DataTable();

            General.SQL = "SELECT * " + " FROM P1_Releve" + " WHERE  " + " CodeAffaire = '" + txtCodeImmeuble.Text + "' " + " AND CodeNumOrdre = '" + txtCodeNumOrdre.Text + "' " + " AND (Evenement = '1' or Evenement = '2') " + " ORDER BY SAISON DESC , P1_Releve.PeriodeAu DESC";

            rsAdo = new DataTable();
            ModAdo ModAdorsAdo = new ModAdo();
            rsAdo = ModAdorsAdo.fc_OpenRecordSet(General.SQL, null, "", ModP1.adoP1);
            if (rsAdo.Rows.Count > 0)
            {
                if (Convert.ToInt32(General.nz(rsAdo.Rows[0]["Evenement"], 0)) == 1)
                {
                    this.EtatChauff.Text = "Chauffage en marche depuis le " + rsAdo.Rows[0]["PeriodeAu"] + "";
                    EtatChauff.ForeColor = ColorTranslator.FromOle(0x8000);
                    lngMarcheArret = 1;
                }
                else if (Convert.ToInt32(General.nz(rsAdo.Rows[0]["Evenement"], 0)) == 2)
                {
                    this.EtatChauff.Text = "Chauffage à l'arrêt depuis le " + rsAdo.Rows[0]["PeriodeAu"] + "";
                    EtatChauff.ForeColor = ColorTranslator.FromOle(0xff);
                    lngMarcheArret = 0;
                }
            }
            else//TESTED
            {
                this.EtatChauff.Text = "Chauffage à l'arrêt";
                EtatChauff.ForeColor = ColorTranslator.FromOle(0xff);
                lngMarcheArret = 0;
            }

            rsAdo.Clear();
            rsAdo = null;
        }
        
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_AfterRowUpdate(object sender, RowEventArgs e)
        {
            var _with3 = SSReleve;
            //Allumage
            if (SSReleve.ActiveRow != null)
            {
                if (SSReleve.ActiveRow.Cells["Event"].Text == "1")
                {
                    General.Execute("UPDATE INT_RELEVETEMP SET Chauffage = '1', Evenement = '1', LibelleEvent = '" + General.nz(ConnectExtern.gFr_ADO_dLookup("Libelle", "TypeAnomalie", "Code = '1'", null), "") + "' WHERE NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    txtChauffage.Text = "1";
                    //Arrêt
                }
                else if (SSReleve.ActiveRow.Cells["Event"].Text == "2")
                {
                    General.Execute("UPDATE INT_RELEVETEMP SET Chauffage = '0', Evenement = '2', LibelleEvent = '" + General.nz(ConnectExtern.gFr_ADO_dLookup("Libelle", "TypeAnomalie", "Code = '2'", null), "") + "' WHERE NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    txtChauffage.Text = "0";
                }
                else
                {
                    txtChauffage.Text = "";
                }
                string periodeAu = General.IsDate(SSReleve.ActiveRow.Cells["Periode_Au"].Value)
                    ? Convert.ToDateTime(SSReleve.ActiveRow.Cells["Periode_Au"].Value).ToString()
                    : "";
                string req = "UPDATE INT_RELEVETEMP SET ";
                req = req + " Etat='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Etat"].Value.ToString()) ? "" : SSReleve.ActiveRow.Cells["Etat"].Value) + "',";
                req = req + " bModifDecade=" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["bModifDecade"].Value + "") ? "NULL" : SSReleve.ActiveRow.Cells["bModifDecade"].Value) + ",";
                req = req + " Evenement=" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Event"].Value + "") ? "NULL" : SSReleve.ActiveRow.Cells["Event"].Value) + ",";
                req = req + " LibelleEvent='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Libel"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Libel"].Value) + "',";
                req = req + " NoDecade=" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["NoDecade"].Value + "") ? "NULL" : SSReleve.ActiveRow.Cells["NoDecade"].Value) + ",";
                req = req + " PeriodeDu='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Releve_Du"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Releve_Du"].Value) + "',";
                req = req + " PeriodeAu='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Releve_Au"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Releve_Au"].Value) + "',";
                req = req + " Anc_IndexDebut='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["A_indexDeb"].Value + "") ? "" : SSReleve.ActiveRow.Cells["A_indexDeb"].Value) + "',";
                req = req + " Anc_IndexFin='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["A_indexFin"].Value + "") ? "" : SSReleve.ActiveRow.Cells["A_indexFin"].Value) + "',";
                req = req + " IndexDebut='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Deb"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Index_Deb"].Value) + "',";
                req = req + " IndexFin='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Fin"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Index_Fin"].Value) + "',";
                req = req + " Estimation='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Estim"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Estim"].Value) + "',";
                req = req + " SaisieJauge='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Jauge"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Jauge"].Value) + "',";
                req = req + " SaisiePige='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Pige"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Pige"].Value) + "',";
                req = req + " CoefPTA='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["CoefPTA"].Value + "") ? "" : SSReleve.ActiveRow.Cells["CoefPTA"].Value) + "',";
                req = req + " VolConverti='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["VolumeConverti"].Value + "") ? "" : SSReleve.ActiveRow.Cells["VolumeConverti"].Value) + "',";
                req = req + " Saison='" + (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Saison"].Value + "") ? "" : SSReleve.ActiveRow.Cells["Saison"].Value) + "'";
                if (!String.IsNullOrEmpty(periodeAu))
                    req = req + ",dPeriodeAu='" + periodeAu + "'";
                req = req + " WHERE NoAuto='" + SSReleve.ActiveRow.Cells["NoAuto"].Value + "'";
                General.Execute(req);
                TimerRefresh.Enabled = true;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            double Resultat = 0;
            int introw = 0;
            string Libel = null;
            double ConsoPige = 0;
            double ConsoJauge = 0;
            double dblSurface = 0;
            string sDateFinPeriode = null;

            try
            {
                var ModAdoADOlibelle = new ModAdo();
                introw = SSReleve.ActiveRow.Index - 1 /* _with4.Bookmark - 1*/;

                //if (e.colIndex == SSReleve.ActiveRow.Cells["NoDecade"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["NoDecade"].Column.Index)
                {
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["NoDecade"].Text))
                    {
                        sDateFinPeriode = ModAdoADOlibelle.fc_ADOlibelle("SELECT Date_Decade FROM P1_DECADE WHERE NO_DECADE = '" + SSReleve.ActiveRow.Cells["NoDecade"].Text + "' AND SAISON = '" + SSReleve.ActiveRow.Cells["Saison"].Text + "'", false, ModP1.adoP1);

                        if (!string.IsNullOrEmpty(sDateFinPeriode))//TESTED
                        {
                            if (General.IsDate(sDateFinPeriode))
                            {
                                if (Convert.ToDateTime(General.nz((SSReleve.ActiveRow.Cells["Periode_Du"].Text), sDateFinPeriode)) > Convert.ToDateTime(sDateFinPeriode))//TESTED
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La décade saisie ne peut pas être prise en compte : la période de fin est inférieure à la période début.", "Incohérence liée à la période du relevé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    e.Cancel = true;
                                    return;
                                }
                                else
                                {
                                    SSReleve.ActiveRow.Cells["Periode_Au"].Value = Convert.ToDateTime(sDateFinPeriode).ToString("dd/MM/yyyy");
                                    SSReleve.ActiveRow.Cells["bModifDecade"].Value = "1";
                                }
                            }
                        }
                        else//TESTED
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La décade saisie n'existe pas, veuillez en saisir une autre.", "Incohérence liée à la période du relevé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                //Colonne "Event"
                // if (eventArgs.colIndex == SSReleve.Columns["Event"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Event"].Column.Index)//TESTED
                {
                    if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Event"].Text))//TESTED
                    {
                        if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["A_indexDeb"].Text))
                        {
                            SSReleve.ActiveRow.Cells["Index_Deb"].Value = SSReleve.ActiveRow.Cells["A_indexDeb"].Text;
                        }
                        SSReleve.ActiveRow.Cells["A_indexFin"].Value = "";
                        SSReleve.ActiveRow.Cells["A_indexdeb"].Value = "";
                        SSReleve.ActiveRow.Cells["Estim"].Value = "";
                        SSReleve.ActiveRow.Cells[3].Value = "";
                        //SSReleve.ActiveRow.Cells["Estim"].Locked = true;
                        SSReleve.ActiveRow.Cells["Estim"].Column.CellActivation = Activation.NoEdit;
                        //SSReleve.ActiveRow.Cells["index_Fin"].Locked = false;
                        SSReleve.ActiveRow.Cells["index_Fin"].Column.CellActivation = Activation.AllowEdit;
                        SSReleve.ActiveRow.Cells["Estim"].Column.CellAppearance.BackColor = YesStyle;
                        SSReleve.ActiveRow.Cells["Index_Fin"].Column.CellAppearance.BackColor = NoStyle; 
                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = "";
                        //SSReleve.ActiveRow.Cells["A_IndexDeb"].Locked = true;
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellAppearance.BackColor = YesStyle;
                        //SSReleve.ActiveRow.Cells["A_IndexFin"].Locked = true;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellAppearance.BackColor = YesStyle;
                        //SSReleve.ActiveRow.Cells["index_deb"].Locked = true;
                        SSReleve.ActiveRow.Cells["index_deb"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["index_deb"].Column.CellAppearance.BackColor = YesStyle;
                        //SSReleve.ActiveRow.Cells["index_Deb"].Locked = true;
                        SSReleve.ActiveRow.Cells["Libel"].Value = "";
                        //_with4.col = SSReleve.ActiveRow.Cells["Index_Fin"].Position;
                        SSReleve.ActiveRow.Cells["Index_Fin"].Selected = true;//TODO

                        return;
                    }
                    else//tested
                    {
                        Libel = General.nz(ConnectExtern.gFr_ADO_dLookup("Libelle", "TypeAnomalie", "Code = '" + SSReleve.ActiveRow.Cells["Event"].Text + "'"), "").ToString();
                        if (string.IsNullOrEmpty(Libel))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Evènement inconnu", "Erreur de Saisie", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            e.Cancel = true;
                            return;
                        }
                        else//tested
                        {
                            SSReleve.ActiveRow.Cells["Libel"].Value = Libel;
                        }
                    }

                    //Allumage
                    if (SSReleve.ActiveRow.Cells["Event"].Text == "1")//TESTED
                    {
                        SSReleve.ActiveRow.Cells["Chauffage"].Value = "1";
                        //Arrêt
                    }
                    else if (SSReleve.ActiveRow.Cells["Event"].Text == "2")//TESTED
                    {
                        SSReleve.ActiveRow.Cells["Chauffage"].Value = "0";
                    }

                    if (SSReleve.ActiveRow.Cells["Event"].Text != "3")//tested
                    {
                        SSReleve.ActiveRow.Cells["A_indexFin"].Value = "";
                        SSReleve.ActiveRow.Cells["A_indexdeb"].Value = "";
                    }
                    if (SSReleve.ActiveRow.Cells["Event"].Text == "4")//tested
                    {
                        //SSReleve.ActiveRow.Cells["Estim"].Locked = false;
                        SSReleve.ActiveRow.Cells["Estim"].Column.CellActivation = Activation.AllowEdit;

                        //SSReleve.ActiveRow.Cells["index_Fin"].Locked = true;
                        SSReleve.ActiveRow.Cells["index_Fin"].Column.CellActivation = Activation.NoEdit;

                        SSReleve.ActiveRow.Cells["Estim"].Column.CellAppearance.BackColor = NoStyle;
                        SSReleve.ActiveRow.Cells["Index_Fin"].Column.CellAppearance.BackColor = YesStyle;
                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = SSReleve.ActiveRow.Cells["Index_Deb"].Text;
                        // SSReleve.ActiveRow.Cells["A_IndexDeb"].Locked = true;
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellAppearance.BackColor = YesStyle;
                        //SSReleve.ActiveRow.Cells["A_IndexFin"].Locked = true;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellAppearance.BackColor = YesStyle;
                        //SSReleve.ActiveRow.Cells["index_deb"].Locked = true;
                        SSReleve.ActiveRow.Cells["index_deb"].Column.CellActivation = Activation.NoEdit;
                        SSReleve.ActiveRow.Cells["index_deb"].Column.CellAppearance.BackColor = YesStyle;

                        //_with4.col = SSReleve.ActiveRow.Cells["Estim"].Position;
                        //SSReleve.ActiveCell.Column.Key = "Estim";
                        //SSReleve.ActiveRow.Cells["Estim"].Activated = true;//todo
                        SSReleve.ActiveRow.Cells["Estim"].Selected = true;
                    }
                    else
                    {
                        //SSReleve.ActiveRow.Cells["index_Fin"].Locked = false;
                        SSReleve.ActiveRow.Cells["index_Fin"].Column.CellActivation = Activation.AllowEdit;

                    }
                    if (SSReleve.ActiveRow.Cells["Event"].Text == "3")//tested
                    {
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellAppearance.BackColor = NoStyle;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellAppearance.BackColor = NoStyle;
                        //SSReleve.ActiveRow.Cells["A_IndexDeb"].Locked = false;
                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Column.CellActivation = Activation.AllowEdit;

                        //SSReleve.ActiveRow.Cells["A_IndexFin"].Locked = false;
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.AllowEdit;

                        SSReleve.ActiveRow.Cells["Index_Deb"].Column.CellAppearance.BackColor = NoStyle;
                        // SSReleve.ActiveRow.Cells["Index_Deb"].Locked = false;
                        SSReleve.ActiveRow.Cells["Index_Deb"].Column.CellActivation = Activation.AllowEdit;

                        SSReleve.ActiveRow.Cells["A_IndexDeb"].Value = SSReleve.ActiveRow.Cells["Index_Deb"].Text;
                        SSReleve.ActiveRow.Cells["Index_Deb"].Value = "";
                        //_with4.col = SSReleve.ActiveRow.Cells["A_IndexFin"].Position;
                        //SSReleve.ActiveCell.Column.Key = "";
                        //SSReleve.ActiveCell = SSReleve.ActiveRow.Cells["A_IndexFin"];//TODO
                        SSReleve.ActiveRow.Cells["A_IndexFin"].Selected = true;
                    }
                }
                // if (eventArgs.colIndex == SSReleve.Columns["Index_Deb"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Deb"].Column.Index)//tested
                {
                    if (SSReleve.ActiveRow.Cells["Event"].Text == "3")
                    {
                        if (General.IsNumeric(SSReleve.ActiveRow.Cells["Index_Deb"].Text))//tested
                        {
                            if (Test_Index((SSReleve.ActiveRow.Cells["Index_Deb"].Text), Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Text), 0))) == false)//tested
                            {
                                SSReleve.ActiveRow.Cells["Index_Deb"].Value = "";
                                e.Cancel = true;
                                return;
                            }
                        }
                        else//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie Erronée ");
                            SSReleve.ActiveRow.Cells["Index_Deb"].Value = "";
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                //if (eventArgs.colIndex == SSReleve.Columns["Jauge"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Jauge"].Column.Index)//tested
                {
                    if (General.nz((SSReleve.ActiveRow.Cells["Pige"].Text), "0").ToString() == "0")//tested
                    {
                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = SSReleve.ActiveRow.Cells["Jauge"].Text;
                    }
                }

                //if (eventArgs.colIndex == SSReleve.Columns["Pige"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Pige"].Column.Index)
                {
                    if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["DiametreCuve"].Text))
                    {
                        SSReleve.ActiveRow.Cells["DiametreCuve"].Value = ModAdoADOlibelle.fc_ADOlibelle("SELECT DiametreCuve FROM Imm_Appareils WHERE NumAppareil = " + General.nz((SSReleve.ActiveRow.Cells["No Compteur"].Text), "0") + "");
                    }
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Pige"].Text) && General.IsNumeric(SSReleve.ActiveRow.Cells["DiametreCuve"].Text))
                    {

                        ConsoPige = ModPDA.fc_GetVolumePige(Convert.ToDouble(SSReleve.ActiveRow.Cells["Pige"].Text), General.nz((SSReleve.ActiveRow.Cells["No Compteur"].Text), "0").ToString());

                        if (ConsoPige != -1)
                        {
                            SSReleve.ActiveRow.Cells["Index_Fin"].Value = Convert.ToString(ConsoPige);
                        }
                        else//tested
                        {
                            SSReleve.ActiveRow.Cells["Index_Fin"].Value = SSReleve.ActiveRow.Cells["Jauge"].Text;
                        }

                        if (General.IsNumeric(SSReleve.ActiveRow.Cells["Index_Fin"].Text))//tested
                        {
                            SSReleve.ActiveRow.Cells["Index_Fin"].Value = Convert.ToString(General.FncArrondir(Convert.ToDouble(SSReleve.ActiveRow.Cells["Index_Fin"].Text), 3));
                        }
                    }
                    else if (General.IsNumeric(SSReleve.ActiveRow.Cells["Jauge"].Text))//tested
                    {
                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = SSReleve.ActiveRow.Cells["Jauge"].Text;
                    }
                }

                if (General.nz(SSReleve.ActiveRow.Cells["ChkLiv"].Text, 0).ToString() == "1")
                {
                    if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Pige"].Column.Index || SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Jauge"].Column.Index)
                    {
                        //Contrôle de la cohérence de saisie entre la pige et la jauge
                        if (General.IsNumeric(SSReleve.ActiveRow.Cells["Jauge"].Text))//tested
                        {
                            ConsoJauge = Convert.ToDouble(SSReleve.ActiveRow.Cells["Jauge"].Text);
                        }

                        if (General.IsNumeric(SSReleve.ActiveRow.Cells["Pige"].Text))//tested
                        {
                            ConsoPige = ModPDA.fc_GetVolumePige(Convert.ToDouble(SSReleve.ActiveRow.Cells["Pige"].Text), General.nz((SSReleve.ActiveRow.Cells["No Compteur"].Text), "0").ToString());
                        }

                        if (ConsoPige != -1 && ConsoPige != 0 && ConsoJauge != 0)
                        {
                            if (ConsoJauge != ConsoPige)
                            {
                                if (System.Math.Abs((ConsoJauge - ConsoPige) / ConsoJauge * 100) > Convert.ToInt32(General.nz(General.sEcartSaisieFOD, "10")))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, un écart de " + String.Format(Math.Abs((ConsoJauge - ConsoPige) / ConsoJauge * 100).ToString(), "0.00") + " % existe entre la saisie de la pige et la saisie de la jauge.", "Ecart important de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                    }
                }

                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Fin"].Column.Index)
                {
                    if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Fin"].Text))
                        return;
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Index_Fin"].Text))
                    {
                        if (Test_Index((SSReleve.ActiveRow.Cells["Index_Fin"].Text), Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Text), 0))) == false)
                        {
                            SSReleve.ActiveRow.Cells["Index_Fin"].Value = "";
                            e.Cancel = true;
                            return;
                        }

                        if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Deb"].Text))//tested
                        {
                            SSReleve.ActiveRow.Cells["Index_Deb"].Value = SSReleve.ActiveRow.Cells["Index_Fin"].Text;
                        }
                        else if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Deb"].Text) && Prem == true)
                        {
                            SSReleve.ActiveRow.Cells["Index_Deb"].Value = SSReleve.ActiveRow.Cells["Index_Fin"].Text;
                        }

                        if (SSReleve.ActiveRow.Cells["Event"].Text == "3")
                        {
                            if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["A_IndexFin"].Text))//TESTED
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir l'index fin d'ancien compteur", "Erreur de Saisie", MessageBoxButtons.OK);
                                //_with4.col = _with4.Columns("A_IndexFin").Position;
                                SSReleve.ActiveRow.Cells["A_IndexFin"].Selected = true;//todo
                                return;
                            }
                            if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Deb"].Text))//tested
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir l'index debut de nouveau compteur", "Erreur de Saisie", MessageBoxButtons.OK);
                                //_with4.col = _with4.Columns("Index_Deb").Position;
                                SSReleve.ActiveRow.Cells["Index_Deb"].Selected = true;//todo
                                return;
                            }

                            if (General.nz((SSReleve.ActiveRow.Cells["chkLiv"].Text), 0).ToString() == "0")//tested
                            {
                                SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString((Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Text)) + (Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexFin"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexdeb"].Text)));

                            }
                            else if (General.nz((SSReleve.ActiveRow.Cells["chkLiv"].Text), 0).ToString() == "1")
                            {
                                if (General.IsNumeric(SSReleve.ActiveRow.Cells["Livraison"].Text))//tested
                                {
                                    SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString((Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text) + Convert.ToDouble(SSReleve.ActiveRow.Cells["Livraison"].Text)) + (Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexFin"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexdeb"].Text)));
                                }
                                else
                                {
                                    SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text) + (Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexFin"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["A_indexdeb"].Text)));
                                }
                            }

                        }
                        //== remise a zero du compteur.
                        if (SSReleve.ActiveRow.Cells["Event"].Text == "8")
                        {
                            if (General.nz((SSReleve.ActiveRow.Cells["chkLiv"].Text), 0).ToString() == "0")//tested
                            {
                                SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(RemiseAZero(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Value), Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text), false));

                            }
                            else if (General.nz((SSReleve.ActiveRow.Cells["chkLiv"].Text), 0).ToString() == "1")
                            {
                                if (General.IsNumeric(SSReleve.ActiveRow.Cells["Livraison"].Text))//tested
                                {
                                    SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(RemiseAZero(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Text), Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text), true) + Convert.ToDouble(SSReleve.ActiveRow.Cells["Livraison"].Text));
                                }
                                else
                                {
                                    SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(RemiseAZero(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Value), Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Value), true));
                                }
                            }
                        }
                        else if (string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Event"].Text))//tested
                        {
                            if (General.nz(SSReleve.ActiveRow.Cells["chkLiv"].Text, 0).ToString() == "0")//tested
                            {
                                SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_fin"].Text) - Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Deb"].Text));
                            }
                            else if (General.nz(SSReleve.ActiveRow.Cells["chkLiv"].Text, 0).ToString() == "1")//tested
                            {
                                SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString((Convert.ToDouble(SSReleve.ActiveRow.Cells["Livraison"].Text) + Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Deb"].Text)) - Convert.ToDouble(SSReleve.ActiveRow.Cells["index_fin"].Text));
                            }
                        }
                        Resultat = Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Conso"].Text), 0));
                        SSReleve.ActiveRow.Cells["Conso"].Value = String.Format(SSReleve.ActiveRow.Cells["Conso"].Text, "0.000");
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie Erronée ");
                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = "";
                        e.Cancel = true;
                        return;
                    }

                    Resultat = 0;

                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Index_deb"].Text) && General.IsNumeric(SSReleve.ActiveRow.Cells["Index_fin"].Text))
                    {
                        if (SSReleve.ActiveRow.Cells["ChkLiv"].Text == "1")//tested
                        {
                            Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_deb"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_fin"].Text), 0));
                        }
                        else//tested
                        {
                            Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_fin"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_deb"].Text), 0));
                        }
                    }

                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["A_Indexdeb"].Text) && General.IsNumeric(SSReleve.ActiveRow.Cells["A_Indexfin"].Text))
                    {
                        if (SSReleve.ActiveRow.Cells["ChkLiv"].Text == "1")//tested
                        {
                            Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexdeb"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexfin"].Text), 0));
                        }
                        else//tested
                        {
                            Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexfin"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexdeb"].Text), 0));
                        }
                    }

                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Livraison"].Text))//tested
                    {
                        Resultat = Resultat + Convert.ToDouble(SSReleve.ActiveRow.Cells["Livraison"].Text);
                    }

                    SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(Resultat);

                    // si la conso négative
                    if (Resultat < 0)
                    {
                        if (General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Text), 0).ToString() == "0")//tested
                        {
                            try
                            {
                                FrmMessageEcartIndex frm = new FrmMessageEcartIndex(this);
                                frm.FrmAppel.Text = this.Name;
                                frm.ShowDialog();
                                switch (OptEcart.Text)
                                {
                                    case "Opt1"://tested
                                        SSReleve.ActiveRow.Cells["Conso"].Value = "";
                                        SSReleve.ActiveRow.Cells["Index_Fin"].Value = "";
                                        e.Cancel = true;
                                        SendKeys.Send("{TAB}");

                                        break;
                                    case "Opt2"://tested
                                        SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(RemiseAZero(Convert.ToDouble(SSReleve.ActiveRow.Cells["index_deb"].Text), Convert.ToDouble(SSReleve.ActiveRow.Cells["index_Fin"].Text), false));
                                        SSReleve.ActiveRow.Cells["Conso"].Value = String.Format(SSReleve.ActiveRow.Cells["Conso"].Text, "0.000");
                                        SendKeys.Send("{TAB}");

                                        break;
                                    case "Opt3"://tested
                                        SendKeys.Send("{TAB}");

                                        break;
                                    default:

                                        break;
                                }
                            }
                            catch (Exception ex) { Program.SaveException(ex); }

                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention consommation négative !", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                    }
                }

                // si on saisie la livraison
                //if (eventArgs.colIndex == SSReleve.Columns["Livraison"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Livraison"].Column.Index)
                {
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Livraison"].Text))
                    {
                        if (Convert.ToBoolean(Test_Index((SSReleve.ActiveRow.Cells["Livraison"].Text), Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Text), CheckState.Unchecked)))) == false)
                        {
                            SSReleve.ActiveRow.Cells["Livraison"].Value = "";
                            e.Cancel = true;
                            return;
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie Erronée ");
                        SSReleve.ActiveRow.Cells["Livraison"].Value = "";
                        e.Cancel = true;
                        return;
                    }
                }

                // si l'evenement Estimation
                //if (eventArgs.colIndex == SSReleve.Columns["Estim"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Estim"].Column.Index)
                {
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Estim"].Text) == true && !string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Estim"].Text))
                    {
                        SSReleve.ActiveRow.Cells["Conso"].Value = SSReleve.ActiveRow.Cells["Estim"].Text;
                        //SSReleve.ActiveRow.Cells["index_Fin"].Locked = false;
                        this.SSReleve.ActiveRow.Cells["index_Fin"].Column.CellActivation = Activation.AllowEdit;
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie Erronée");
                        SSReleve.ActiveRow.Cells["Estim"].Value = "";
                        e.Cancel = true;
                        return;
                    }
                }

                //if (eventArgs.colIndex == SSReleve.Columns["A_IndexFin"].Position)
                if (SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["A_IndexFin"].Column.Index)//tested
                {
                    if (SSReleve.ActiveRow.Cells["Event"].Text == "3")
                    {
                        if (General.IsNumeric(SSReleve.ActiveRow.Cells["A_IndexFin"].Text))
                        {
                            if (Test_Index((SSReleve.ActiveRow.Cells["A_IndexFin"].Text), Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Value), 0))) == false)//tested
                            {
                                SSReleve.ActiveRow.Cells["A_IndexFin"].Value = "";
                                e.Cancel = true;
                                return;
                            }
                        }
                        else//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie Erronée ");
                            SSReleve.ActiveRow.Cells["A_IndexFin"].Value = "";
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                //        If ColIndex = SSReleve.Columns("CoefPTA").Position Then
                if (General.IsNumeric(SSReleve.ActiveRow.Cells["CoefPTA"].Text))
                {
                    SSReleve.ActiveRow.Cells["VolumeConverti"].Value = Convert.ToString((Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["Index_Fin"].Text), "0")) - Convert.ToInt32(General.nz((SSReleve.ActiveRow.Cells["Index_Deb"].Text), "0"))) * Convert.ToDouble(SSReleve.ActiveRow.Cells["CoefPTA"].Text));
                }
                //        End If

                //        If ColIndex = SSReleve.Columns("PCSMoyen").Position Then
                if (General.IsNumeric(SSReleve.ActiveRow.Cells["PCSMoyen"].Text))
                {
                    Resultat = Convert.ToDouble(SSReleve.ActiveRow.Cells["VolumeConverti"].Text) * Convert.ToDouble(SSReleve.ActiveRow.Cells["PCSMoyen"].Text);
                }
                //        End If

                Resultat = 0;

                if (General.IsNumeric(SSReleve.ActiveRow.Cells["Index_deb"].Text) && General.IsNumeric(SSReleve.ActiveRow.Cells["Index_fin"].Text))
                {
                    if (SSReleve.ActiveRow.Cells["ChkLiv"].Text == "1")
                    {
                        Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_deb"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_fin"].Text), 0));
                    }
                    else//tested
                    {
                        Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_fin"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["Index_deb"].Text), 0));
                    }
                }

                if (General.IsNumeric(SSReleve.ActiveRow.Cells["A_Indexdeb"].Text) && General.IsNumeric(SSReleve.ActiveRow.Cells["A_Indexfin"].Text))
                {
                    if (SSReleve.ActiveRow.Cells["ChkLiv"].Text == "1")
                    {
                        Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexdeb"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexfin"].Text), 0));
                    }
                    else//tested
                    {
                        Resultat = Resultat + Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexfin"].Text), 0)) - Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["A_Indexdeb"].Text), 0));
                    }
                }
                if (General.nz((SSReleve.ActiveRow.Cells["ChkLiv"].Text), 0).ToString() == "1")
                {
                    if (General.IsNumeric(SSReleve.ActiveRow.Cells["Livraison"].Text))
                    {
                        Resultat = Resultat + Convert.ToDouble(SSReleve.ActiveRow.Cells["Livraison"].Text);
                    }
                }
                else
                {
                    SSReleve.ActiveRow.Cells["VolumeConverti"].Value = Convert.ToString(Resultat * Convert.ToDouble(General.nz((SSReleve.ActiveRow.Cells["CoefPTA"].Text), 1)));
                    //            Resultat = SSReleve.Columns("VolumeConverti").Text * nz(SSReleve.Columns("PCSMoyen").Text, 1)
                }
                SSReleve.ActiveRow.Cells["Conso"].Value = Convert.ToString(Resultat);


                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";SSREleve_BeforeColUpdate;");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_KeyDown(object sender, KeyEventArgs e)
        {
            short KeyAscii = (short)e.KeyCode;
            if (SSReleve.ActiveRow != null && SSReleve.ActiveRow.Index == SSReleve.Rows.Count - 1 && (KeyAscii == 13 || KeyAscii == 9))
            {
                if (SSReleve.ActiveRow.Cells["Event"].Text == "4")//tested
                {
                    if (SSReleve.ActiveCell.Column.Index == 11)
                    {
                        SSReleve_BeforeExitEditMode(SSReleve, new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs(false, false));
                        //SSREleve_BeforeColUpdate(SSReleve, new AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_BeforeColUpdateEvent(SSReleve.Col, 0, 0));
                        if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Conso"].Text))
                        {
                            SSReleve.UpdateData();
                            return;
                        }
                    }
                }
                else
                {
                    if ((string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Event"].Text) && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Fin"].Column.Index))//tested
                    {
                        SSReleve_BeforeExitEditMode(SSReleve, new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs(false, false));
                        //SSREleve_BeforeColUpdate(SSReleve, new AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_BeforeColUpdateEvent(SSReleve.Col, 0, 0));
                        if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Conso"].Text))
                        {
                            SSReleve.UpdateData();
                            return;
                        }
                    }
                }
                if ((SSReleve.ActiveRow.Cells["Chkliv"].Text == "1" && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Livraison"].Column.Index))
                {
                    if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Conso"].Text))
                    {
                        SSReleve.ActiveRow.Cells[8].Activated = true;//TODO I add this line because in vb when the call SSReleve_BeforeColUpdate(8,0,0) and they passed this argument to this event i can't do it with c# code so i add this line to activate this column before calling this event.
                        SSReleve_BeforeExitEditMode(SSReleve, new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs(false, false));
                        // SSREleve_BeforeColUpdate(SSReleve, new AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_BeforeColUpdateEvent(8, 0, 0));
                        SSReleve.UpdateData();
                        return;
                    }
                }
            }

            if (SSReleve.ActiveCell != null && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Fin"].Column.Index && SSReleve.ActiveRow.Cells["Event"].Text == "3" && (e.KeyValue == 13 || e.KeyValue == 9))//tested
            {
                //Todo 
                //SSReleve.Col = 13;
                SSReleve.ActiveRow.Cells[13].Activated = true;

                //SSReleve.Columns[13].Position = 13;
                //SSReleve.ActiveRow.Cells[13].Column.Index = 13;//todo index does not contains a definiton for setters
                return;
            }
            if (SSReleve.ActiveCell != null && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["A_IndexFin"].Column.Index && SSReleve.ActiveRow.Cells["Event"].Text == "3" && (e.KeyValue == 13 || e.KeyValue == 9))
            {
                if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells[13].Text))
                {
                    //Todo 
                    //SSReleve.Col = 7;
                    SSReleve.ActiveRow.Cells[7].Activated = true;

                    //SSReleve.Columns[7].Position = 7;
                    //SSReleve.ActiveRow.Cells[13].Column.Index = 7;//todo index does not contains a definiton for setters
                }
                return;
            }
            if (SSReleve.ActiveCell != null && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Fin"].Column.Index && SSReleve.ActiveRow.Cells["Event"].Text == "3" & (e.KeyValue == 13 || e.KeyValue == 9))
            {
                if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Index_Fin"].Text))
                {
                    SSReleve_BeforeExitEditMode(SSReleve, new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs(false, false));
                    //SSREleve_BeforeColUpdate(SSReleve, new AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_BeforeColUpdateEvent(SSReleve.Col, 0, 0));
                    //SSReleve.Bookmark = SSReleve.Bookmark + 1
                    //SSReleve.Col = 2;
                    SSReleve.ActiveRow.Cells[2].Activated = true;
                    //SSReleve.Columns[2].Position = 2;//TODO
                    //SSReleve.Columns["Index_deb"].Locked = true;
                    SSReleve.ActiveRow.Cells["Index_deb"].Column.CellActivation = Activation.NoEdit;
                    //SSReleve.Columns["A_Indexdeb"].Locked = true;
                    SSReleve.ActiveRow.Cells["A_Indexdeb"].Column.CellActivation = Activation.NoEdit;
                    //SSReleve.Columns["A_IndexFin"].Locked = true;
                    SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.NoEdit;
                    if (SSReleve.ActiveRow.Index == SSReleve.Rows.Count - 1)
                    {
                        //SSREleve_BeforeColUpdate SSReleve.Col, 0, 0
                        if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells["Conso"].Text))
                        {
                            SSReleve.UpdateData();
                            this.cmdValider.Focus();
                            return;
                        }
                    }
                    return;
                }
            }
            //if (SSReleve.Col == SSReleve.Columns["Index_Deb"].Position & SSReleve.Columns["Event"].Text == "3" & (e.KeyValue == 13 | e.KeyValue == 9))
            if (SSReleve.ActiveCell != null && SSReleve.ActiveCell.Column.Index == SSReleve.ActiveRow.Cells["Index_Deb"].Column.Index && SSReleve.ActiveRow.Cells["Event"].Text == "3" && (e.KeyValue == 13 || e.KeyValue == 9))
            {
                if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells[7].Text))
                {
                    //SSReleve.Col = 8;//TODO
                    SSReleve.ActiveRow.Cells[8].Activated = true;
                    //SSReleve.Columns[7].Position = 7;//TODO
                }
                return;
            }
            if ((Keys)KeyAscii == Keys.Return)
            {

                SendKeys.Send("{TAB}");

            }

            if (((Keys)KeyAscii == Keys.Return && SSReleve.ActiveRow != null && General.Len(SSReleve.ActiveRow.Cells["Index_Fin"].Text) == 10))
            {
                if (!string.IsNullOrEmpty(SSReleve.ActiveRow.Cells[9].OriginalValue.ToString()) || !string.IsNullOrEmpty(SSReleve.ActiveRow.Cells[9].Text))
                {
                    SendKeys.Send("{TAB}");
                }
            }
        }

        //private void SSREleve_RowColChange(System.Object eventSender, AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_RowColChangeEvent eventArgs)
        //{
        //    int introw = 0;
        //    //introw = SSReleve.Bookmark - 1;
        //    introw = SSReleve.ActiveRow.Index - 1;
        //    //----------------------------------------------------
        //    if (Convert.ToBoolean(General.nz((SSReleve.ActiveRow.Cells["Chkliv"].Text), CheckState.Checked)) == true)
        //    {

        //        //SReleve.ActiveRow.Cells["Livraison"].Locked = false;
        //        SSReleve.DisplayLayout.Bands[0].Columns["No Bon De Commande"].CellActivation = Activation.AllowEdit;
        //    }
        //    else
        //    {
        //        //SSReleve.ActiveRow.Cells["Livraison"].Locked = true;
        //        SSReleve.DisplayLayout.Bands[0].Columns["Livraison"].CellActivation = Activation.NoEdit;

        //    }
        //    //----------------------------------------------
        //}

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int introw = 0;

            if (e.ReInitialize)
                return;
            introw = e.Row.Index;
            e.Row.Appearance.BackColor = Color.FromArgb(255, 255, 192);
            if (General.nz(e.Row.Cells["ChkLiv"].Text, 0).ToString() == "1")
            {
                e.Row.Cells["Livraison"].Column.CellAppearance.BackColor = NoStyle;
                //e.Row.Cells["Livraison"].Visible = true;
                e.Row.Cells["Livraison"].Hidden = true;
                //e.Row.Cells["Jauge"].Locked = false;
                e.Row.Cells["Jauge"].Column.CellActivation = Activation.AllowEdit;

                e.Row.Cells["Jauge"].Column.CellAppearance.BackColor = NoStyle;
                //e.Row.Cells["Pige"].Locked = false;
                e.Row.Cells["Pige"].Column.CellActivation = Activation.AllowEdit;
                e.Row.Cells["Pige"].Column.CellAppearance.BackColor = NoStyle;
                //e.Row.Cells["Jauge"].Visible = true;
                e.Row.Cells["Jauge"].Hidden = false;
                //e.Row.Cells["Pige"].Visible = true;
                e.Row.Cells["Pige"].Hidden = false;
                //e.Row.Cells["Index_Fin"].Locked = false;
                e.Row.Cells["Index_Fin"].Column.CellActivation = Activation.AllowEdit;

                e.Row.Cells["Index_Fin"].Appearance.BackColor = YesStyle/* Color.FromArgb(192,255,255)*/ /*0xc0ffff*/;
                e.Row.Cells["CoefPTA"].Appearance.BackColor = YesStyle /*Color.FromArgb(192, 255, 255)*/ /*0xc0ffff*/;
                //e.Row.Cells["CoefPTA"].Locked = true;
                e.Row.Cells["CoefPTA"].Column.CellActivation = Activation.NoEdit;

                //e.Row.Cells["CoefPTA"].Visible = false;
                e.Row.Cells["CoefPTA"].Hidden = true;
                e.Row.Cells["VolumeConverti"].Appearance.BackColor = YesStyle/* Color.FromArgb(192, 255, 255)*/;
                //e.Row.Cells["VolumeConverti"].Locked = true;
                e.Row.Cells["VolumeConverti"].Column.CellActivation = Activation.NoEdit;

                //e.Row.Cells["VolumeConverti"].Visible = false;
                e.Row.Cells["VolumeConverti"].Hidden = true;
                e.Row.Cells["PCSMoyen"].Appearance.BackColor = YesStyle /*Color.FromArgb(192, 255, 255)*/;
                //e.Row.Cells["PCSMoyen"].Locked = true;
                e.Row.Cells["PCSMoyen"].Column.CellActivation = Activation.NoEdit;

                //e.Row.Cells["PCSMoyen"].Visible = false;
                e.Row.Cells["PCSMoyen"].Hidden = true;

            }
            else
            {
                // SSReleve.Columns["Livraison"].Locked = true;
                e.Row.Cells["Livraison"].Column.CellActivation = Activation.NoEdit;
                e.Row.Cells["Livraison"].Column.CellAppearance.BackColor = YesStyle;
                e.Row.Cells["Jauge"].Column.CellAppearance.BackColor = YesStyle /*Color.FromArgb(192, 255, 255)*/;
                e.Row.Cells["Pige"].Column.CellAppearance.BackColor = YesStyle /*Color.FromArgb(192, 255, 255)*/;

                //SSReleve.Columns["Jauge"].BackColor = 0xc0ffff;
                //SSReleve.Columns["Pige"].BackColor = 0xc0ffff;
                //SSReleve.Columns["Index_Fin"].Locked = false;

                e.Row.Cells["Index_Fin"].Column.CellActivation = Activation.AllowEdit;

                e.Row.Cells["Index_Fin"].Column.CellAppearance.BackColor = NoStyle; ;
                e.Row.Cells["CoefPTA"].Column.CellAppearance.BackColor = NoStyle; ;
                //SSReleve.Columns["CoefPTA"].Locked = false;
                e.Row.Cells["CoefPTA"].Column.CellActivation = Activation.AllowEdit;

                //SSReleve.Columns["CoefPTA"].Visible = true;
                e.Row.Cells["CoefPTA"].Hidden = false;
                e.Row.Cells["VolumeConverti"].Column.CellAppearance.BackColor = NoStyle; ;
                //SSReleve.Columns["VolumeConverti"].Locked = false;
                e.Row.Cells["VolumeConverti"].Column.CellActivation = Activation.AllowEdit;

                //SSReleve.Columns["VolumeConverti"].Visible = true;
                e.Row.Cells["VolumeConverti"].Hidden = false;
                e.Row.Cells["PCSMoyen"].Column.CellAppearance.BackColor = NoStyle; ;

                //SSReleve.Columns["PCSMoyen"].Locked = false;
                e.Row.Cells["PCSMoyen"].Column.CellActivation = Activation.AllowEdit;

                //SSReleve.Columns["PCSMoyen"].Visible = true;
                e.Row.Cells["PCSMoyen"].Hidden = false;
            }

            if (General.IsDate(e.Row.Cells["Periode_Du"].Text))
            {
                e.Row.Cells["Periode_Du"].Value = Convert.ToDateTime(e.Row.Cells["Periode_Du"].Text).ToString("MM/yyyy");
            }

            if (General.IsDate(e.Row.Cells["Periode_Au"].Text))
            {
                e.Row.Cells["Periode_Au"].Value = Convert.ToDateTime(e.Row.Cells["Periode_Au"].Text).ToString("MM/yyyy");
            }

            if (General.IsDate(e.Row.Cells["Releve_Du"].Text))
            {
                e.Row.Cells["Releve_Du"].Value = Convert.ToDateTime(e.Row.Cells["Releve_Du"].Text).ToString("dd/MM/yyyy");
            }

            if (General.IsDate(e.Row.Cells["Releve_Au"].Text))
            {
                e.Row.Cells["Releve_Au"].Value = Convert.ToDateTime(e.Row.Cells["Releve_Au"].Text).ToString("dd/MM/yyyy");
            }

            if (General.IsNumeric(e.Row.Cells["Conso"].Text))
            {
                e.Row.Cells["Conso"].Value = String.Format(e.Row.Cells["Conso"].Text, "### ### ##0.000");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
           
            foreach (var column in SSReleve.DisplayLayout.Bands[0].Columns)
            {
                if (column.Key.ToLower()=="Nbj"
                    ||column.Key.ToLower() == "no compteur".ToLower() 
                    ||column.Key.ToLower() == "libel_app".ToLower() 
                    ||column.Key.ToLower() == "libel".ToLower()
                    ||column.Key.ToLower() == "periode_du".ToLower() 
                    ||column.Key.ToLower() == "periode_au".ToLower()
                    ||column.Key.ToLower() == "chauffage".ToLower() 
                    ||column.Key.ToLower() == "bmodifdecade".ToLower()
                    ||column.Key.ToLower() == "noauto".ToLower() 
                    ||column.Key.ToLower() == "estim".ToLower() 
                    ||column.Key.ToLower() == "conso".ToLower())
                    column.CellActivation = Activation.ActivateOnly;
            }
            SSReleve.DisplayLayout.Bands[0].Columns["Index_Fin"].Header.Caption = "Index Fin";
            SSReleve.DisplayLayout.Bands[0].Columns["Periode_Du"].Header.Caption = "Periode Du";
            SSReleve.DisplayLayout.Bands[0].Columns["Periode_Au"].Header.Caption = "Periode Au";
            SSReleve.DisplayLayout.Bands[0].Columns["Releve_Du"].Header.Caption = "Releve Du";
            SSReleve.DisplayLayout.Bands[0].Columns["Releve_Au"].Header.Caption = "Releve Au";
            SSReleve.DisplayLayout.Bands[0].Columns["NoDecade"].Header.Caption = "No Decade";
            SSReleve.DisplayLayout.Bands[0].Columns["Index_Deb"].Header.Caption = "Index Début";
            SSReleve.DisplayLayout.Bands[0].Columns["NoDecade"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["Index_Fin"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["VolumeConverti"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["Event"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["CoefPTA"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["PCSMoyen"].CellAppearance.BackColor = Color.White;
            SSReleve.DisplayLayout.Bands[0].Columns["Event"].ValueList = SSTA;
            SSReleve.DisplayLayout.Bands[0].Columns["Etat"].Hidden = true;
            SSReleve.DisplayLayout.Bands[0].Columns["ChkLiv"].Hidden = true;
            SSReleve.DisplayLayout.Bands[0].Columns["Libel"].Header.Caption = "Libellé";
            SSReleve.DisplayLayout.Bands[0].Columns["Libel_App"].Header.Caption = "Compteur";
            SSReleve.DisplayLayout.Bands[0].Columns["PCSMoyen"].Header.Caption = "PCS Moy";
            SSReleve.DisplayLayout.Bands[0].Columns["A_indexDeb"].Header.Caption = "A_Index Début";
            SSReleve.DisplayLayout.Bands[0].Columns["CoefPTA"].Header.Caption = "Coef PTA";
            SSReleve.DisplayLayout.Bands[0].Columns["VolumeConverti"].Header.Caption = "Vol Converti";
            SSReleve.DisplayLayout.Bands[0].Columns["A_IndexFin"].Header.Caption = "A_Index Fin";
            SSReleve.DisplayLayout.Bands[0].Columns["PCSMoyen"].Header.Caption = "PCS Moy";
            SSReleve.DisplayLayout.Bands[0].Columns["DiametreCuve"].Hidden = true;
            //SSReleve.DisplayLayout.DefaultSelectedBackColor = Color.FromArgb(255, 255, 192);
            //SSReleve.DisplayLayout.Override.SelectedRowAppearance.BackColor= Color.FromArgb(255, 255, 192);
            //SSReleve.DisplayLayout.Override.ActiveRowAppearance.BackColor= Color.FromArgb(255, 255, 192);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="Index"></param>
        /// <param name="Liv"></param>
        /// <returns></returns>
        private bool Test_Index(string Index, int Liv)
        {
            bool functionReturnValue = false;

            if (Convert.ToDouble(Index) < 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Index négatif interdit", "Erreur de Saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = false;
                return functionReturnValue;
            }
            if (Liv == 0)
            {
                if (Index.Contains(".") || Index.Contains(","))
                {

                    functionReturnValue = true;
                    return functionReturnValue;
                }
            }
            functionReturnValue = true;
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="dbI1"></param>
        /// <param name="dbI2"></param>
        /// <param name="blnLivraison"></param>
        /// <returns></returns>
        private double RemiseAZero(double dbI1, double dbI2, bool blnLivraison)
        {
            double functionReturnValue = 0;
            int lngLen = 0;
            double dbSous = 0;
            int i = 0;
            //== cette fonction sert dans le cas ou un compteur est passé a zero
            //== exemple un relevé à 990 puis au prochain passage à 200, il
            //== aurait du être à 1200.
            lngLen = General.Len(Convert.ToString(dbI1));
            if (lngLen > 0)
            {
                dbSous = 1;
                for (i = 0; i <= lngLen - 1; i++)
                {
                    dbSous = dbSous * 10;
                }
                if (blnLivraison == false)
                {
                    functionReturnValue = (dbSous + dbI2) - dbI1;
                }
                else
                {
                    functionReturnValue = dbI1 - (dbSous + dbI2);
                }
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void TimerLoad_Tick(Object eventSender, EventArgs eventArgs)
        {
            TimerLoad.Enabled = false;
            fc_AffRelevesInter((txtNoIntervention.Text));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void TimerRefresh_Tick(Object eventSender, EventArgs eventArgs)
        {
            int lNumRow = 0;
            int lNumCol = 0;
            string sNoDecade = null;
            string sPeriodeAu = null;

            try
            {
                TimerRefresh.Enabled = false;
                var ModAdoADOlibelle = new ModAdo();
                if (ModAdoADOlibelle.fc_ADOlibelle("SELECT COALESCE(bModifDecade, '0') AS Decade_Modif FROM INT_RELEVETEMP WHERE bModifDecade = '1' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0")) == "1")
                {
                    sNoDecade = ModAdoADOlibelle.fc_ADOlibelle("SELECT NoDecade FROM INT_RELEVETEMP WHERE bModifDecade = '1' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    sPeriodeAu = ModAdoADOlibelle.fc_ADOlibelle("SELECT dPeriodeAu FROM INT_RELEVETEMP WHERE bModifDecade = '1' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    General.Execute("UPDATE INT_RELEVETEMP SET NoDecade = '" + sNoDecade + "', dPeriodeAu = '" + sPeriodeAu + "', bModifDecade = '2' WHERE NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                }
                if (SSReleve.ActiveRow != null)
                    lNumRow = SSReleve.ActiveRow.Index;
                if (SSReleve.ActiveCell != null)
                    lNumCol = SSReleve.ActiveCell.Column.Index;
                SSReleve.Refresh();
                //Todo
                SSReleve.ActiveRow = SSReleve.Rows[lNumRow];
                SSReleve.ActiveCell = SSReleve.ActiveRow.Cells[lNumCol];

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Feuille : UserInter_RelevesCompteurs;TimerRefresh_Timer;");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdValider_Click(object sender, EventArgs e)
        {
            string sNomBaseLisa = null;
            string sEtatReleve = null;
            string sNoDecade = null;
            string sPeriodeAu = null;

            try
            {
                SSReleve.UpdateData();

                if (fc_ControleCoherence() == false)
                    return;
                var ModAdoADOlibelle = new ModAdo();
                sNomBaseLisa = General.adocnn.Database.ToString();

                //    'Suppression du relevé précédement chargé
                //    SQL = " DELETE FROM INT_Releve"
                //    SQL = SQL & " WHERE INT_Releve.NoIntervention = " & nz(txtNoIntervention.Text, "0")
                //    adocnn.Execute SQL
                //
                //    'Envoi du nouveau relevé à partir du tableau saisi
                //    SQL = "INSERT INTO INT_Releve ("
                //    SQL = SQL & " Etat, NoFichierGraphe, NumAppareil, CodeUO, CodeAffaire, CodeNumOrdre, "
                //    SQL = SQL & " CodeChaufferie, Saison, NoDecade, Periodedu, PeriodeAu, "
                //    SQL = SQL & " CodeTransmission, CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, "
                //    SQL = SQL & " Livraison, Consommation, Estimation, Anc_IndexDebut, Anc_IndexFin,"
                //    SQL = SQL & " Evenement, Date_Event, Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, "
                //    SQL = SQL & " CoefPTA, CoefPCS, VolConverti, NomFic, DateImport,"
                //    SQL = SQL & " NoIntervention, Libel_App, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu, LibelleEvent)"
                //    SQL = SQL & " SELECT "
                //    If chkValideReleve.Value = vbChecked Then
                //        SQL = SQL & " 'RV'"
                //    Else
                //        If Not AdodcReleves.Recordset.EOF And Not AdodcReleves.Recordset.BOF Then
                //            sEtatReleve = AdodcReleves.Recordset.Fields("Etat").Value
                //        End If
                //        If sEtatReleve = "" Then
                //            sEtatReleve = gfr_liaisonV2("EtatReleveBase", "AT", False, "Compteurs")
                //        End If
                //        SQL = SQL & " '" & sEtatReleve & "'"
                //    End If
                //    SQL = SQL & " , NoFichierGraphe, NumAppareil, "
                //    SQL = SQL & " CodeUO, CodeAffaire, CodeNumOrdre, CodeChaufferie, Saison, NoDecade, Periodedu, "
                //    SQL = SQL & " PeriodeAu, CodeTransmission, "
                //    SQL = SQL & " CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, Livraison, Consommation, "
                //    SQL = SQL & " Estimation, Anc_IndexDebut, Anc_IndexFin, Evenement,"
                //    SQL = SQL & " Date_Event , Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, CoefPTA, "
                //    SQL = SQL & " CoefPCS, VolConverti, NomFic, DateImport, NoIntervention, Libel_App, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu, LibelleEvent "
                //    SQL = SQL & " FROM INT_ReleveTemp"
                //    SQL = SQL & " WHERE INT_ReleveTemp.Utilisateur = '" & gsUtilisateur & "' AND INT_ReleveTemp.NoIntervention = " & nz(txtNoIntervention.Text, "0")

                General.SQL = "UPDATE INT_Releve SET ";
                General.SQL = General.SQL + " INT_Releve.NoFichierGraphe = INT_ReleveTemp.NoFichierGraphe";
                General.SQL = General.SQL + ", INT_Releve.NumAppareil = INT_ReleveTemp.NumAppareil";
                General.SQL = General.SQL + ", INT_Releve.CodeUO = INT_ReleveTemp.CodeUO";
                General.SQL = General.SQL + ", INT_Releve.CodeAffaire = INT_ReleveTemp.CodeAffaire";
                General.SQL = General.SQL + ", INT_Releve.CodeNumOrdre = INT_ReleveTemp.CodeNumOrdre";
                General.SQL = General.SQL + ", INT_Releve.CodeChaufferie = INT_ReleveTemp.CodeChaufferie";
                General.SQL = General.SQL + ", INT_Releve.Saison = INT_ReleveTemp.Saison";
                General.SQL = General.SQL + ", INT_Releve.NoDecade = INT_ReleveTemp.NoDecade";
                General.SQL = General.SQL + ", INT_Releve.Periodedu = INT_ReleveTemp.Periodedu";
                General.SQL = General.SQL + ", INT_Releve.PeriodeAu = INT_ReleveTemp.PeriodeAu";
                General.SQL = General.SQL + ", INT_Releve.CodeTransmission = INT_ReleveTemp.CodeTransmission";
                General.SQL = General.SQL + ", INT_Releve.CodeAnomalie = INT_ReleveTemp.CodeAnomalie";
                General.SQL = General.SQL + ", INT_Releve.Chauffage = INT_ReleveTemp.Chauffage";
                General.SQL = General.SQL + ", INT_Releve.IndexDebut = INT_ReleveTemp.IndexDebut";
                General.SQL = General.SQL + ", INT_Releve.IndexFin = INT_ReleveTemp.IndexFin";
                General.SQL = General.SQL + ", INT_Releve.NJours = INT_ReleveTemp.NJours";
                General.SQL = General.SQL + ", INT_Releve.Livraison = INT_ReleveTemp.Livraison";
                General.SQL = General.SQL + ", INT_Releve.Consommation = INT_ReleveTemp.Consommation";
                General.SQL = General.SQL + ", INT_Releve.Estimation = INT_ReleveTemp.Estimation";
                General.SQL = General.SQL + ", INT_Releve.Anc_IndexDebut = INT_ReleveTemp.Anc_IndexDebut";
                General.SQL = General.SQL + ", INT_Releve.Anc_IndexFin = INT_ReleveTemp.Anc_IndexFin";
                General.SQL = General.SQL + ", INT_Releve.Evenement = INT_ReleveTemp.Evenement";
                General.SQL = General.SQL + ", INT_Releve.Date_Event = INT_ReleveTemp.Date_Event";
                General.SQL = General.SQL + ", INT_Releve.Index_Event = INT_ReleveTemp.Index_Event";
                General.SQL = General.SQL + ", INT_Releve.Etat_Cuve = INT_ReleveTemp.Etat_Cuve";
                General.SQL = General.SQL + ", INT_Releve.Type_Rel = INT_ReleveTemp.Type_Rel";
                General.SQL = General.SQL + ", INT_Releve.Appro_Prevu = INT_ReleveTemp.Appro_Prevu";
                General.SQL = General.SQL + ", INT_Releve.Facture = INT_ReleveTemp.Facture";
                General.SQL = General.SQL + ", INT_Releve.CoefPTA = INT_ReleveTemp.CoefPTA";
                General.SQL = General.SQL + ", INT_Releve.CoefPCS = INT_ReleveTemp.CoefPCS";
                General.SQL = General.SQL + ", INT_Releve.VolConverti = INT_ReleveTemp.VolConverti";
                General.SQL = General.SQL + ", INT_Releve.NomFic = INT_ReleveTemp.NomFic";
                General.SQL = General.SQL + ", INT_Releve.DateImport = INT_ReleveTemp.DateImport";
                General.SQL = General.SQL + ", INT_Releve.NoIntervention = INT_ReleveTemp.NoIntervention";
                General.SQL = General.SQL + ", INT_Releve.Libel_App = INT_ReleveTemp.Libel_App";
                General.SQL = General.SQL + ", INT_Releve.SaisieJauge = INT_ReleveTemp.SaisieJauge";
                General.SQL = General.SQL + ", INT_Releve.SaisiePige = INT_ReleveTemp.SaisiePige";
                General.SQL = General.SQL + ", INT_Releve.DiametreCuve = INT_ReleveTemp.DiametreCuve";
                General.SQL = General.SQL + ", INT_Releve.dPeriodeDu = INT_ReleveTemp.dPeriodeDu";
                General.SQL = General.SQL + ", INT_Releve.dPeriodeAu = INT_ReleveTemp.dPeriodeAu";

                if (chkValideReleve.CheckState == System.Windows.Forms.CheckState.Checked)
                {
                    General.SQL = General.SQL + ", INT_RELEVE.Etat = 'RV'";
                }
                else
                {
                    if (AdodcReleves.Rows.Count > 0)
                    {
                        sEtatReleve = AdodcReleves.Rows[0]["Etat"].ToString();
                    }
                    if (string.IsNullOrEmpty(sEtatReleve))
                    {
                        sEtatReleve = General.gfr_liaisonV2("EtatReleveBase", "AT", false, "Compteurs");
                    }
                    General.SQL = General.SQL + ", INT_RELEVE.Etat =  '" + sEtatReleve + "'";
                }
                General.SQL = General.SQL + ", INT_Releve.LibelleEvent = INT_ReleveTemp.LibelleEvent";
                General.SQL = General.SQL + " FROM INT_Releve ";
                General.SQL = General.SQL + " INNER JOIN INT_ReleveTemp ON ";
                General.SQL = General.SQL + " INT_Releve.NoIntervention = INT_ReleveTemp.NoIntervention";
                General.SQL = General.SQL + " AND INT_Releve.NoAuto = INT_ReleveTemp.NoAuto";
                General.SQL = General.SQL + " WHERE INT_ReleveTemp.Utilisateur = '" + General.gsUtilisateur + "' AND INT_ReleveTemp.NoIntervention = " + General.nz(txtNoIntervention.Text, "0");


                General.Execute(General.SQL);

                if (txtChauffage.Text == "0" || txtChauffage.Text == "1")
                {
                    General.Execute("UPDATE Intervention SET Chauffage = '" + txtChauffage.Text + "' WHERE NoIntervention = " + General.nz(txtNoIntervention.Text, "0"));
                }

                if (chkTransfertP1.CheckState == System.Windows.Forms.CheckState.Checked)
                {

                    if (ModP1.adoP1 == null)
                    {
                        ModP1.fc_OpenConnP1();
                    }

                    //Suppression des relevés envoyés précédemment
                    General.SQL = "DELETE FROM P1_Releve WHERE NoIntervention IN(";
                    General.SQL = General.SQL + " SELECT NoIntervention ";
                    General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve";
                    General.SQL = General.SQL + " LEFT OUTER JOIN [" + sNomBaseLisa + "].dbo.TypeEtatReleves ON ";
                    General.SQL = General.SQL + " [" + sNomBaseLisa + "].dbo.INT_Releve.Etat =  [" + sNomBaseLisa + "].dbo.TypeEtatReleves.Code";
                    General.SQL = General.SQL + " WHERE [" + sNomBaseLisa + "].dbo.INT_Releve.NoIntervention = " + General.nz((txtNoIntervention.Text), "0");
                    General.SQL = General.SQL + " AND [" + sNomBaseLisa + "].dbo.TypeEtatReleves.TransfertP1 = '1'";
                    General.SQL = General.SQL + " AND ([" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye = '0' OR [" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye IS NULL)";
                    General.SQL = General.SQL + ")";
                    SqlCommand cmd = new SqlCommand(General.SQL, ModP1.adoP1);
                    if (ModP1.adoP1.State != ConnectionState.Open)
                        ModP1.adoP1.Open();
                    cmd.ExecuteNonQuery();
                    //General.Execute(General.SQL,ModP1.adoP1);TODO.

                    //Transfert du relevé vers le P1 :
                    //Envoi du relevé à partir du tableau saisi
                    General.SQL = "INSERT INTO P1_Releve (";
                    General.SQL = General.SQL + " NoFichierGraphe, NumAppareil, CodeUO, CodeAffaire, CodeNumOrdre, ";
                    General.SQL = General.SQL + " Saison, NoDecade, Periodedu, PeriodeAu, ";
                    General.SQL = General.SQL + " CodeTransmission, CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, ";
                    General.SQL = General.SQL + " Livraison, Consommation, Estimation, Anc_IndexDebut, Anc_IndexFin,";
                    General.SQL = General.SQL + " Evenement, Date_Event, Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, ";
                    General.SQL = General.SQL + " CoefPTA, CoefPCS, VolConverti, NomFic, DateImport,";
                    General.SQL = General.SQL + " NoIntervention, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu)";
                    General.SQL = General.SQL + " SELECT NoFichierGraphe, NumAppareil, ";
                    General.SQL = General.SQL + " CodeUO, CodeAffaire, CodeNumOrdre, Saison, NoDecade, Periodedu, ";
                    General.SQL = General.SQL + " PeriodeAu, CodeTransmission, ";
                    General.SQL = General.SQL + " CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, Livraison, Consommation, ";
                    General.SQL = General.SQL + " Estimation, Anc_IndexDebut, Anc_IndexFin, Evenement,";
                    General.SQL = General.SQL + " Date_Event , Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, CoefPTA, ";
                    General.SQL = General.SQL + " CoefPCS, VolConverti, NomFic, DateImport, NoIntervention, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu ";
                    General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve";
                    General.SQL = General.SQL + " LEFT OUTER JOIN [" + sNomBaseLisa + "].dbo.TypeEtatReleves ON ";
                    General.SQL = General.SQL + " [" + sNomBaseLisa + "].dbo.INT_Releve.Etat =  [" + sNomBaseLisa + "].dbo.TypeEtatReleves.Code";
                    General.SQL = General.SQL + " WHERE [" + sNomBaseLisa + "].dbo.INT_Releve.NoIntervention = " + General.nz((txtNoIntervention.Text), "0");
                    General.SQL = General.SQL + " AND [" + sNomBaseLisa + "].dbo.TypeEtatReleves.TransfertP1 = '1'";
                    General.SQL = General.SQL + " AND ([" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye = '0' OR [" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye IS NULL)";
                    cmd = new SqlCommand(General.SQL, ModP1.adoP1);
                    if (ModP1.adoP1.State != ConnectionState.Open)
                        ModP1.adoP1.Open();
                    cmd.ExecuteNonQuery();
                    //General.Execute(General.SQL, ModP1.adoP1);//TODO

                    //Marquage des relevés comme étant transférés dans le P1
                    General.SQL = "UPDATE INT_Releve SET ";
                    General.SQL = General.SQL + " Etat = 'RP1', P1_Envoye = '1' ";
                    General.SQL = General.SQL + " WHERE INT_Releve.NoIntervention = " + General.nz((txtNoIntervention.Text), "0");
                    General.SQL = General.SQL + " AND INT_Releve.Etat IN ";
                    General.SQL = General.SQL + " (SELECT Code FROM TypeEtatReleves WHERE TypeEtatReleves.TransfertP1 = '1'";
                    General.SQL = General.SQL + ")";
                    General.Execute(General.SQL);

                }
                if (ModAdoADOlibelle.fc_ADOlibelle("SELECT COALESCE(bModifDecade, '0') AS Decade_Modif FROM INT_RELEVETEMP WHERE bModifDecade = '2' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0")) == "2")
                {

                    if (ModP1.adoP1 == null)
                    {
                        ModP1.fc_OpenConnP1();
                    }

                    sNoDecade = ModAdoADOlibelle.fc_ADOlibelle("SELECT NoDecade FROM INT_RELEVETEMP WHERE bModifDecade = '2' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    sPeriodeAu = ModAdoADOlibelle.fc_ADOlibelle("SELECT dPeriodeAu FROM INT_RELEVETEMP WHERE bModifDecade = '2' AND NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    General.Execute("UPDATE P1_RELEVE SET NoDecade = '" + sNoDecade + "', dPeriodeAu = '" + sPeriodeAu + "' WHERE NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                    General.Execute("UPDATE INT_RELEVETEMP SET bModifDecade = '0' WHERE NoIntervention = " + General.nz((txtNoIntervention.Text), "0"));
                }

                //Affichage du relevé modifié
                fc_AffRelevesInter(txtNoIntervention.Text);

                // ERROR: Not supported in C#: OnErrorStatement

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Feuille : UserInter_RelevesCompteurs;cmdValider_Click;");
                return;

            }
        }


        //private void UserInter_RelevesCompteurs_Resize(object sender, EventArgs e)
        //{
        //    short intEcartW = 0;
        //    short intEcartH = 0;

        //    //intEcartW = 210;
        //    //intEcartH = 600;

        //    //if (this.WindowState != FormWindowState.Minimized)
        //    //{
        //    //    SSReleve.Width = this.Width - SSReleve.Left - intEcartW;
        //    //    SSReleve.Height = this.Height - SSReleve.Top - intEcartH;
        //    //}
        //}

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="C"></param>
        void LoadDimensionGrille(Control C)
        {
            foreach (Control cc in C.Controls)
            {
                LoadDimensionGrille(cc);
            }
            if (C is UltraGrid)
            {
                UltraGrid objcontrolee = (UltraGrid)C;
                sheridan.fc_LoadDimensionGrille(this.Name, objcontrolee);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="C"></param>
        void SaveDimensionGrille(Control C)
        {
            foreach (Control cc in C.Controls)
            {
                SaveDimensionGrille(cc);
            }
            if (C is UltraGrid)
            {
                UltraGrid objcontrolee = (UltraGrid)C;
                sheridan.fc_SavDimensionGrille(this.Name, objcontrolee);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserInter_RelevesCompteurs_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            Control objcontrole = null;

            TimerRefresh.Enabled = false;
            //LoadDimensionGrille(this);

            //SSReleve.S4btyleSets["NoStyle"].BackColor = Color.White;
            //SSReleve.DisplayLayout.Appearance.BackColor = Color.White;
            //SSReleve.StyleSets["YesStyle"].BackColor = Color.Aquamarine;
            //SSReleve.DisplayLayout.Appearance.BackColor = Color.Aquamarine;

            TimerLoad.Interval = 50;
            TimerLoad.Enabled = true;
            General.setTransparentRowColorOfGrid(this);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSReleve_AfterExitEditMode(object sender, EventArgs e)
        {
            if (SSReleve.ActiveRow != null)
            {
                if (SSReleve.ActiveRow.Cells["Event"].Text == "3")
                {
                    //SSReleve.Columns["Index_deb"].Locked = false;
                    //SSReleve.Columns["A_Indexdeb"].Locked = false;
                    //SSReleve.Columns["A_IndexFin"].Locked = false;
                    SSReleve.ActiveRow.Cells["Index_deb"].Column.CellActivation = Activation.AllowEdit;
                    SSReleve.ActiveRow.Cells["A_Indexdeb"].Column.CellActivation = Activation.AllowEdit;
                    SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.AllowEdit;
                }
                else
                {
                    //SSReleve.Columns["Index_deb"].Locked = true;
                    //SSReleve.Columns["A_Indexdeb"].Locked = true;
                    //SSReleve.Columns["A_IndexFin"].Locked = true;
                    SSReleve.ActiveRow.Cells["Index_deb"].Column.CellActivation = Activation.NoEdit;
                    SSReleve.ActiveRow.Cells["A_Indexdeb"].Column.CellActivation = Activation.NoEdit;
                    SSReleve.ActiveRow.Cells["A_IndexFin"].Column.CellActivation = Activation.NoEdit;
                }
                if (SSReleve.ActiveRow.Cells["Event"].Text == "4")
                {
                    //SSReleve.ActiveRow.Cells["Estim"].Locked = false;
                    SSReleve.ActiveRow.Cells["Estim"].Column.CellActivation = Activation.AllowEdit;
                }
                else
                {
                    //SSReleve.ActiveRow.Cells["Estim"].Locked = true;
                    SSReleve.ActiveRow.Cells["Estim"].Column.CellActivation = Activation.NoEdit;
                }

                if (General.nz(SSReleve.ActiveRow.Cells["Chkliv"].Value, "1").ToString() == "1")
                {
                    SSReleve.ActiveRow.Cells["Livraison"].Column.CellActivation = Activation.AllowEdit;
                }
                else
                    SSReleve.ActiveRow.Cells["Livraison"].Column.CellActivation = Activation.ActivateOnly;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserInter_RelevesCompteurs_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveDimensionGrille(this);
        }
    }
}
