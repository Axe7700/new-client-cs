﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.ReleveDesCompteures
{
    public partial class frmLivraison_Releve : Form
    {
        public frmLivraison_Releve()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmLivraison_Releve_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            TimerLoad.Interval = 500;
            TimerLoad.Enabled = true;
        }

        private ModAdo ModAdoADOlibelle;
        ModAdo ModAdors ;
        private void Affich_Livraison_AXE()
        {
            //Affichage des Livraisons venant de la base ANTONA-COFI
            int wchantier = 0;
            string req = null;
            double wQte = 0;
            string sDateDebut = null;
            string sDateFin = null;
            DataTable rs = default(DataTable);
            ModAdoADOlibelle = new ModAdo();
            //GridLivraisons.RemoveAll();
            GridLivraisons.DataSource = null;
            if (General.IsDate(txtdateDu.Text))
            {
                sDateDebut = txtdateDu.Text;
            }

            if (General.IsDate(txtDateAu.Text))
            {
                sDateFin = txtDateAu.Text;
            }

            lblLibChantier.Text = txtCodeAffaire.Text + "/" + txtCodeNumOrdre.Text + " " + ModAdoADOlibelle.fc_ADOlibelle("SELECT Libelle FROM Immeuble WHERE CodeUO = '" + General.strCodeUO + "' AND CodeImmeuble = '" + txtCodeAffaire.Text + "' AND CodeNumOrdre = '" + txtCodeNumOrdre.Text + "'");
            lblLibDateReleves.Text = "du " + sDateDebut + " au " + sDateFin;
            Application.DoEvents();

            General.SQL = "SELECT [CleAuto]";
            General.SQL = General.SQL + ",[CodeUO]";
            General.SQL = General.SQL + ",[CodeChantier]";
            General.SQL = General.SQL + ",[CodeNumOrdre]";
            General.SQL = General.SQL + ",[DateComm]";
            General.SQL = General.SQL + ",[CodeClient]";
            General.SQL = General.SQL + ",[AdrLivr1]";
            General.SQL = General.SQL + ",[AdrLivr2]";
            General.SQL = General.SQL + ",[AdrLivr3]";
            General.SQL = General.SQL + ",[CPLivr]";
            General.SQL = General.SQL + ",[VilleLivr]";
            General.SQL = General.SQL + ",[LibelleClient]";
            General.SQL = General.SQL + ",[NomChantier]";
            General.SQL = General.SQL + ",[NomArticle]";
            General.SQL = General.SQL + ",[DateLivrDem]";
            General.SQL = General.SQL + ",[EtatComm]";
            General.SQL = General.SQL + ",[CodeLivreur]";
            General.SQL = General.SQL + ",[Reg]";
            General.SQL = General.SQL + ",[Expl]";
            General.SQL = General.SQL + ",[PFI]";
            General.SQL = General.SQL + ",[QCuve1]";
            General.SQL = General.SQL + ",[QCuve2]";
            General.SQL = General.SQL + ",[QCuve3]";
            General.SQL = General.SQL + ",[QCuve4]";
            General.SQL = General.SQL + ",[NumBL]";
            General.SQL = General.SQL + ",[DateLivr]";
            General.SQL = General.SQL + ",[QLivr]";
            General.SQL = General.SQL + ",[CodeFour]";
            General.SQL = General.SQL + ",[CodeDepot]";
            General.SQL = General.SQL + ",[NumFacture]";
            General.SQL = General.SQL + ",[DateFacture]";
            General.SQL = General.SQL + ",[Montant]";
            General.SQL = General.SQL + ",[Facture]";
            General.SQL = General.SQL + ",[Comm]";
            General.SQL = General.SQL + ",[Tarif]";
            General.SQL = General.SQL + ",[PayeLe]";
            General.SQL = General.SQL + " FROM [" + General.sBaseMetier + "].[dbo].[Commande]";
            General.SQL = General.SQL + " WHERE CodeChantier = '" + txtCodeAffaire.Text + "'";
            General.SQL = General.SQL + " AND CodeNumOrdre = '" + txtCodeNumOrdre.Text + "'";
            General.SQL = General.SQL + " AND (DateLivr >= '" + sDateDebut + "'";
            General.SQL = General.SQL + " AND DateLivr <= '" + sDateFin + "')";
            General.SQL = General.SQL + " ORDER BY DateLivr, DateComm";

            rs = new DataTable();
            rs = ModAdoADOlibelle.fc_OpenRecordSet(General.SQL);
            if (rs!=null&&rs.Rows.Count>0)
            {

                DataTable dt = new DataTable();
                dt.Columns.Add("Date Livraison");
                dt.Columns.Add("Date Command");
                dt.Columns.Add("Num Commande");
                dt.Columns.Add("Qte Commandée");
                dt.Columns.Add("Qte Livrée");
                dt.Columns.Add("Num Facture");
                dt.Columns.Add("Date Facture");
                foreach (DataRow dr in rs.Rows)
                {
                    DataRow dtdr = rs.NewRow();
                    wQte =Convert.ToDouble((General.nz(dr["QCuve1"],0).ToString())) + Convert.ToDouble(General.nz(dr["QCuve2"],   0)) + Convert.ToDouble(General.nz(dr["QCuve3"],   0)) + Convert.ToDouble(General.nz(dr["QCuve4"],   0));
                    wQte = Convert.ToDouble(string.Format(wQte.ToString(), "0.000"));
                    //req = "";
                    //req = req + "!" + General.nz(dr["DateLivr"], dr["DateLivrDem"]) + "!;";
                    //req = req + "!" + dr["DateComm"] + "!;";
                    //req = req + "!" + dr["CleAuto"] + "!;";
                    //req = req + "!" +string.Format(wQte.ToString(), "### ##0.000") + "!;";
                   
                    //req = req + "!" + string.Format(wQte.ToString(), "### ##0.000") + "!;";
                    //req = req + "!" + dr["NumFacture"] + "!;";
                    //req = req + "!" + dr["DateFacture"] + "!;";
                    dtdr[0] = General.nz(dr["DateLivr"], dr["DateLivrDem"]);
                    dtdr[1] = dr["DateComm"];
                    dtdr[2] = dr["CleAuto"];
                    dtdr[3] = string.Format(wQte.ToString(), "### ##0.000");
                    wQte = Convert.ToDouble(General.nz(dr["QLivr"], 0));
                    dtdr[4] = string.Format(wQte.ToString(), "### ##0.000");
                    dtdr[5] = dr["NumFacture"];
                    dtdr[6] = dr["DateFacture"];
                    dt.Rows.Add(dtdr);
                }
                //GridLivraisons.AddItem(req);
                GridLivraisons.DataSource = dt;
            }
            //rs.Clear();
            rs = null;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void TimerLoad_Tick(System.Object eventSender, System.EventArgs eventArgs)
        {
            TimerLoad.Enabled = false;
            Affich_Livraison_AXE();
        }
    }
}
