﻿namespace Axe_interDT.Views.ReleveDesCompteures
{
    partial class frmLivraison_Releve
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtCodeAffaire = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeNumOrdre = new iTalk.iTalk_TextBox_Small2();
            this.txtdateDu = new iTalk.iTalk_TextBox_Small2();
            this.txtDateAu = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lblLibChantier = new System.Windows.Forms.Label();
            this.lblLibDateReleves = new System.Windows.Forms.Label();
            this.GridLivraisons = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.TimerLoad = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridLivraisons)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCodeAffaire
            // 
            this.txtCodeAffaire.AccAcceptNumbersOnly = false;
            this.txtCodeAffaire.AccAllowComma = false;
            this.txtCodeAffaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeAffaire.AccHidenValue = "";
            this.txtCodeAffaire.AccNotAllowedChars = null;
            this.txtCodeAffaire.AccReadOnly = false;
            this.txtCodeAffaire.AccReadOnlyAllowDelete = false;
            this.txtCodeAffaire.AccRequired = false;
            this.txtCodeAffaire.BackColor = System.Drawing.Color.Transparent;
            this.txtCodeAffaire.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeAffaire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeAffaire.ForeColor = System.Drawing.Color.Black;
            this.txtCodeAffaire.Location = new System.Drawing.Point(371, 11);
            this.txtCodeAffaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeAffaire.MaxLength = 32767;
            this.txtCodeAffaire.Multiline = false;
            this.txtCodeAffaire.Name = "txtCodeAffaire";
            this.txtCodeAffaire.ReadOnly = false;
            this.txtCodeAffaire.Size = new System.Drawing.Size(95, 23);
            this.txtCodeAffaire.TabIndex = 502;
            this.txtCodeAffaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeAffaire.UseSystemPasswordChar = false;
            this.txtCodeAffaire.Visible = false;
            // 
            // txtCodeNumOrdre
            // 
            this.txtCodeNumOrdre.AccAcceptNumbersOnly = false;
            this.txtCodeNumOrdre.AccAllowComma = false;
            this.txtCodeNumOrdre.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeNumOrdre.AccHidenValue = "";
            this.txtCodeNumOrdre.AccNotAllowedChars = null;
            this.txtCodeNumOrdre.AccReadOnly = false;
            this.txtCodeNumOrdre.AccReadOnlyAllowDelete = false;
            this.txtCodeNumOrdre.AccRequired = false;
            this.txtCodeNumOrdre.BackColor = System.Drawing.Color.Transparent;
            this.txtCodeNumOrdre.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeNumOrdre.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeNumOrdre.ForeColor = System.Drawing.Color.Black;
            this.txtCodeNumOrdre.Location = new System.Drawing.Point(470, 11);
            this.txtCodeNumOrdre.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeNumOrdre.MaxLength = 32767;
            this.txtCodeNumOrdre.Multiline = false;
            this.txtCodeNumOrdre.Name = "txtCodeNumOrdre";
            this.txtCodeNumOrdre.ReadOnly = false;
            this.txtCodeNumOrdre.Size = new System.Drawing.Size(95, 23);
            this.txtCodeNumOrdre.TabIndex = 503;
            this.txtCodeNumOrdre.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeNumOrdre.UseSystemPasswordChar = false;
            this.txtCodeNumOrdre.Visible = false;
            // 
            // txtdateDu
            // 
            this.txtdateDu.AccAcceptNumbersOnly = false;
            this.txtdateDu.AccAllowComma = false;
            this.txtdateDu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtdateDu.AccHidenValue = "";
            this.txtdateDu.AccNotAllowedChars = null;
            this.txtdateDu.AccReadOnly = false;
            this.txtdateDu.AccReadOnlyAllowDelete = false;
            this.txtdateDu.AccRequired = false;
            this.txtdateDu.BackColor = System.Drawing.Color.Transparent;
            this.txtdateDu.CustomBackColor = System.Drawing.Color.White;
            this.txtdateDu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtdateDu.ForeColor = System.Drawing.Color.Black;
            this.txtdateDu.Location = new System.Drawing.Point(569, 11);
            this.txtdateDu.Margin = new System.Windows.Forms.Padding(2);
            this.txtdateDu.MaxLength = 32767;
            this.txtdateDu.Multiline = false;
            this.txtdateDu.Name = "txtdateDu";
            this.txtdateDu.ReadOnly = false;
            this.txtdateDu.Size = new System.Drawing.Size(95, 23);
            this.txtdateDu.TabIndex = 504;
            this.txtdateDu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtdateDu.UseSystemPasswordChar = false;
            this.txtdateDu.Visible = false;
            // 
            // txtDateAu
            // 
            this.txtDateAu.AccAcceptNumbersOnly = false;
            this.txtDateAu.AccAllowComma = false;
            this.txtDateAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateAu.AccHidenValue = "";
            this.txtDateAu.AccNotAllowedChars = null;
            this.txtDateAu.AccReadOnly = false;
            this.txtDateAu.AccReadOnlyAllowDelete = false;
            this.txtDateAu.AccRequired = false;
            this.txtDateAu.BackColor = System.Drawing.Color.Transparent;
            this.txtDateAu.CustomBackColor = System.Drawing.Color.White;
            this.txtDateAu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateAu.ForeColor = System.Drawing.Color.Black;
            this.txtDateAu.Location = new System.Drawing.Point(668, 11);
            this.txtDateAu.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateAu.MaxLength = 32767;
            this.txtDateAu.Multiline = false;
            this.txtDateAu.Name = "txtDateAu";
            this.txtDateAu.ReadOnly = false;
            this.txtDateAu.Size = new System.Drawing.Size(95, 23);
            this.txtDateAu.TabIndex = 505;
            this.txtDateAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateAu.UseSystemPasswordChar = false;
            this.txtDateAu.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.BackColor = System.Drawing.Color.Transparent;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(12, 17);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(69, 19);
            this.label33.TabIndex = 506;
            this.label33.Text = "Chantier";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 19);
            this.label1.TabIndex = 507;
            this.label1.Text = "Livraisons";
            // 
            // lblLibChantier
            // 
            this.lblLibChantier.AutoSize = true;
            this.lblLibChantier.BackColor = System.Drawing.Color.Transparent;
            this.lblLibChantier.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLibChantier.Location = new System.Drawing.Point(133, 17);
            this.lblLibChantier.Name = "lblLibChantier";
            this.lblLibChantier.Size = new System.Drawing.Size(0, 19);
            this.lblLibChantier.TabIndex = 508;
            // 
            // lblLibDateReleves
            // 
            this.lblLibDateReleves.AutoSize = true;
            this.lblLibDateReleves.BackColor = System.Drawing.Color.Transparent;
            this.lblLibDateReleves.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLibDateReleves.Location = new System.Drawing.Point(133, 48);
            this.lblLibDateReleves.Name = "lblLibDateReleves";
            this.lblLibDateReleves.Size = new System.Drawing.Size(0, 19);
            this.lblLibDateReleves.TabIndex = 509;
            // 
            // GridLivraisons
            // 
            this.GridLivraisons.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GridLivraisons.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridLivraisons.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridLivraisons.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridLivraisons.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridLivraisons.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridLivraisons.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridLivraisons.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridLivraisons.DisplayLayout.UseFixedHeaders = true;
            this.GridLivraisons.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridLivraisons.Location = new System.Drawing.Point(-3, 87);
            this.GridLivraisons.Name = "GridLivraisons";
            this.GridLivraisons.Size = new System.Drawing.Size(766, 348);
            this.GridLivraisons.TabIndex = 572;
            this.GridLivraisons.Text = "Livraissons effectuées associées au relevé";
            // 
            // TimerLoad
            // 
            this.TimerLoad.Tick += new System.EventHandler(this.TimerLoad_Tick);
            // 
            // frmLivraison_Releve
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(764, 433);
            this.Controls.Add(this.GridLivraisons);
            this.Controls.Add(this.lblLibDateReleves);
            this.Controls.Add(this.lblLibChantier);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtDateAu);
            this.Controls.Add(this.txtdateDu);
            this.Controls.Add(this.txtCodeNumOrdre);
            this.Controls.Add(this.txtCodeAffaire);
            this.Name = "frmLivraison_Releve";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmLivraison_Releve";
            this.Load += new System.EventHandler(this.frmLivraison_Releve_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridLivraisons)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtCodeAffaire;
        public iTalk.iTalk_TextBox_Small2 txtCodeNumOrdre;
        public iTalk.iTalk_TextBox_Small2 txtdateDu;
        public iTalk.iTalk_TextBox_Small2 txtDateAu;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label lblLibChantier;
        public System.Windows.Forms.Label lblLibDateReleves;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridLivraisons;
        private System.Windows.Forms.Timer TimerLoad;
    }
}