﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
//using Microsoft.VisualBasic;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.GoConteur.Forms;
using Axe_interDT.Views.ReleveDesCompteures;
using iTalk;
using Infragistics.Win.UltraWinGrid;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;
using Excel = Microsoft.Office.Interop.Excel;
using static Axe_interDT.Shared.Variable;

namespace Axe_interDT.Views.ReleveDesCompteurs
{
    public partial class UserDocSuiviRelevesV2 : UserControl
    {
        public UserDocSuiviRelevesV2()
        {
            InitializeComponent();
        }
        private const short cReleveStandard = 1;
        private const short cAllumage = 2;
        private const short cArret = 3;

        DataTable rsCompteurs;
        private string sqlOrderBy;
        private string sSort;
        ModAdo ModAdoLbNCC;
        ModAdo ModAdoNC;
        ModAdo ModAdoCompteurs;
        ModAdo ModAdoLI;
        ModAdo ModAdoADOlibelle;
        ModAdo ModAdorsTestEnvoi;
        private string SSQL = "";
        private struct tpNumSemaine
        {
            public string sNumSemaine;
            public string sDateDu;
            public string sDateAu;
        }

        private struct tpMois
        {
            public string sNumMois;
            public string sLibelle;
            public string sDateDu;
            public string sDateAu;
        }

        bool bGoToReleve;

        Color p1BackColor = ColorTranslator.FromOle(0xC0FFC0);/* Color.FromArgb(192, 255, 192);*/
        Color p1ForeColor = Color.Black;
        Color VERROUBackColor = ColorTranslator.FromOle(0x80C0FF);/* Color.FromArgb(255, 192, 128);*/
        Color VERROUForeColor = Color.Black;

        Color LOCKBackColor = ColorTranslator.FromOle(0xE0E0E0);/* Color.FromArgb(224, 224, 224);*/
        Color LOCKForeColor = Color.Black;
        Color UNLOCKBackColor = Color.White;
        Color UNLOCKForeColor = Color.Black;



        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbChefSecteur_AfterCloseUp(object sender, EventArgs e)
        {
            General.sSQL = "Select Personnel.Nom FROM Personnel INNER JOIN SpecifQualif ON";
            General.sSQL = General.sSQL + " Personnel.CodeQualif = SpecifQualif.CodeQualif ";
            //sSql = sSql & " WHERE QualifDisp = '1' AND CodeUO = '" & strCodeUO & "'"
            General.sSQL = General.sSQL + " WHERE QualifDisp = '1' ";
            General.sSQL = General.sSQL + " AND Personnel.Matricule = '" + cmbChefSecteur.Text + "'";
            ModAdoLbNCC = new ModAdo();
            lblNomChefCentre.Text = ModAdoLbNCC.fc_ADOlibelle(General.sSQL);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbChefSecteur_Validating_1(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            if (cmbChefSecteur.Rows.Count > 0)
            {
                if (cmbChefSecteur.Value != null && !string.IsNullOrEmpty(cmbChefSecteur.Value.ToString()) && !string.IsNullOrEmpty(cmbChefSecteur.Text))
                {
                    lblNomChefCentre.Text = cmbChefSecteur.ActiveRow.Cells["Nom"].Value.ToString();
                }
                else
                {
                    lblNomChefCentre.Text = "";
                }
            }
            e.Cancel = Cancel;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbChefSecteur_ValueChanged(object sender, EventArgs e)
        {
            if (cmbChefSecteur.ActiveRow != null)
                lblNomChefCentre.Text = cmbChefSecteur.ActiveRow.Cells["Nom"].Value.ToString();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbChefSecteur_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "";

            General.sSQL = "Select Personnel.Matricule,Personnel.Nom FROM Personnel INNER JOIN SpecifQualif ON";
            General.sSQL = General.sSQL + " Personnel.CodeQualif = SpecifQualif.CodeQualif ";
            General.sSQL = General.sSQL + " WHERE QualifDisp = '1' ";

            sheridan.InitialiseCombo(cmbChefSecteur, General.sSQL, "Matricule", true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbChefSecteur_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == (short)System.Windows.Forms.Keys.Return)
            {
                fc_Select();
                KeyAscii = 0;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeClient_AfterCloseUp(object sender, EventArgs e)
        {
            ModAdoNC = new ModAdo();
            lblNomClient.Text = ModAdoNC.fc_ADOlibelle("SELECT Nom FROM Table1 WHERE Code1 = '" + cmbCodeClient.Text + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeClient_ValueChanged(object sender, EventArgs e)
        {
            if (cmbCodeClient.ActiveRow != null)
                lblNomClient.Text = cmbCodeClient.ActiveRow.Cells["Raison Sociale"].Value.ToString();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeClient_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sWhere = null;
            var _with1 = this;
            General.sSQL = "SELECT Table1.Code1 as [Code Client], Table1.Nom as [Raison Sociale], Table1.Adresse1," + " Table1.CodePostal, Table1.Ville " + " From Table1";
            General.sSQL = General.sSQL + sWhere + " ORDER BY Table1.Code1";

            sheridan.InitialiseCombo(cmbCodeClient, General.sSQL, "Code Client", true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_ValueChanged(object sender, EventArgs e)
        {
            if (cmbCodeimmeuble.ActiveRow != null)
                lblLibImmeuble.Text = cmbCodeimmeuble.ActiveRow.Cells[1].Value.ToString();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "SELECT Immeuble.CodeImmeuble AS [Code chantier]";
                //            End If
                General.sSQL = General.sSQL + " " + " , Immeuble.Code1 + ' - ' + Table1.Nom AS [Client]" + " FROM Immeuble INNER JOIN Table1 ON Immeuble.Code1 = Table1.Code1 ";

                if (!string.IsNullOrEmpty(cmbCodeClient.Text))
                {
                    General.sSQL = General.sSQL + " WHERE Table1.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(cmbCodeClient.Text) + "' ";
                }

                General.sSQL = General.sSQL + " ORDER BY Immeuble.CodeImmeuble";

                sheridan.InitialiseCombo(cmbCodeimmeuble, General.sSQL, "Code Chantier");

                cmbCodeimmeuble.DisplayLayout.Bands[0].Columns[0].Width = Convert.ToInt32(cmbCodeimmeuble.DisplayLayout.Bands[0].Columns[0].Width * 0.8);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Feuille : UserDocSuiviReleves;cmbCodeimmeuble_DropDown;");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == (short)System.Windows.Forms.Keys.Return)
            {
                fc_Select();
                KeyAscii = 0;
            }
            e.KeyChar = (char)KeyAscii;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEtatReleve_ValueChanged(object sender, EventArgs e)
        {
            ModAdo ModAdoER = new ModAdo();
            General.sSQL = "SELECT ";
            General.sSQL = General.sSQL + " [Designation]";
            General.sSQL = General.sSQL + " FROM [TypeEtatReleves] WHERE Code = '" + cmbEtatReleve.Text + "'";
            lblLibEtatReleve.Text = ModAdoER.fc_ADOlibelle(General.sSQL);

            fc_GestAffichage();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEtatReleve_AfterCloseUp(object sender, EventArgs e)
        {
            if (cmbEtatReleve.ActiveRow != null)
            {
                lblLibEtatReleve.Text = cmbEtatReleve.ActiveRow.Cells[1].Text;
            }
            fc_GestAffichage();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEtatReleve_BeforeDropDown(object sender, CancelEventArgs e)
        {

            General.sSQL = "SELECT [Code]";
            General.sSQL = General.sSQL + " ,[Designation]";
            General.sSQL = General.sSQL + " FROM [TypeEtatReleves]";
            General.sSQL = General.sSQL + " ORDER BY Designation";


            sheridan.InitialiseCombo(cmbEtatReleve, General.sSQL, "Code", true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_ValueChanged(object sender, EventArgs e)
        {
            ModAdoLI = new ModAdo();
            lblLibIntervenant_PDA.Text = ModAdoLI.fc_ADOlibelle("SELECT Personnel.Nom + ' ' + Personnel.Prenom AS Interv FROM" + " Personnel INNER JOIN SpecifQualif ON " + " Personnel.CodeQualif = SpecifQualif.CodeQualif WHERE Personnel.Matricule='" + cmbIntervenant.Text + "' AND SpecifQualif.QualifTech = '1'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            ModAdoLI = new ModAdo();
            lblLibIntervenant_PDA.Text = ModAdoLI.fc_ADOlibelle("SELECT Personnel.Nom + ' ' + Personnel.Prenom AS Interv FROM" + " Personnel INNER JOIN SpecifQualif ON " + " Personnel.CodeQualif = SpecifQualif.CodeQualif WHERE Personnel.Matricule='" + cmbIntervenant.Value + "' AND SpecifQualif.QualifTech = '1'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom" + " FROM Personnel INNER JOIN" + " SpecifQualif ON Personnel.CodeQualif = SpecifQualif.CodeQualif" + " WHERE COALESCE(Personnel.NonActif, 0) <> 1 AND SpecifQualif.QualifTech = '1' ORDER BY Nom";
            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule", true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {

            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == (short)System.Windows.Forms.Keys.Return)
            {
                fc_Select();
                KeyAscii = 0;
            }
            e.KeyChar = (char)KeyAscii;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Select()
        {
            fc_GetLivraisons();
            TimerLoad.Interval = 50;
            TimerLoad.Enabled = true;
        }

        /// <summary>
        /// TESTED ==> Check vb6 column Chauffage dosen't exist
        /// </summary>
        /// <returns></returns>
        private object fc_FlagRelEnvoye()
        {
            object functionReturnValue = null;
            string sWhere = null;
            string sOperateur = null;
            // " OR " ou ""
            string sParentOuv = null;
            //Parenthèse ouverte "(" ou ""
            string sParentFerm = null;
            //Parenthèse fermée ")" ou ""
            int i = 0;
            object objcontrole = null;
            string sNoReleve = null;
            string sqlUpdate = null;

            try
            {
                var _with3 = this;

                General.sSQL = "";
                sWhere = "";


                General.sSQL = " FROM INT_Releve ";

                General.sSQL = General.sSQL + " INNER JOIN Intervention ON ";
                General.sSQL = General.sSQL + " INT_Releve.NoIntervention = Intervention.NoIntervention ";

                //sSql = sSql & " AND INT_Releve.CodeUO = Intervention.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN TypeAnomalie ON ";
                General.sSQL = General.sSQL + " INT_Releve.Evenement = TypeAnomalie.Code ";

                General.sSQL = General.sSQL + " INNER JOIN Immeuble ON";
                General.sSQL = General.sSQL + " Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";
                //sSql = sSql & " AND Intervention.CodeNumOrdre = Immeuble.CodeNumOrdre"
                //sSql = sSql & " AND Intervention.CodeUO = Immeuble.CodeUO "

                if (General.sUChaufferie == "1")
                {
                    General.sSQL = General.sSQL + " And Intervention.CodeChaufferie = Immeuble.CodeChaufferie";
                }

                General.sSQL = General.sSQL + " INNER JOIN Table1 ON";
                General.sSQL = General.sSQL + " Immeuble.Code1 = Table1.Code1 ";

                General.sSQL = General.sSQL + " INNER JOIN FacArticle ON ";
                General.sSQL = General.sSQL + " Intervention.Article = FacArticle.CodeArticle ";
                //sSql = sSql & " AND Intervention.CodeUO = FacArticle.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN Imm_Appareils ON ";
                General.sSQL = General.sSQL + " INT_Releve.NumAppareil = Imm_Appareils.NumAppareil ";
                // sSql = sSql & " AND INT_Releve.CodeUO = Imm_Appareils.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN P1_TypeCpt ON ";
                General.sSQL = General.sSQL + " Imm_Appareils.Type_Compteur = P1_TypeCpt.CODE ";

                General.sSQL = General.sSQL + " LEFT OUTER JOIN [" + ModP1.adoP1.Database + "].dbo.P1_Releve P1R ON Intervention.NoIntervention = P1R.NoIntervention ";

                sWhere = " WHERE Intervention.Rel_compteur = '1' ";

                //        sWhere = sWhere & " AND  (Immeuble.Archive = '0' OR Immeuble.Archive IS NULL)"
                //
                //        sWhere = sWhere & " AND  (Table1.Archive = '0' OR Table1.Archive IS NULL)"
                //
                //        sWhere = sWhere & " AND FacArticle.SaisieCompteur = '1' "

                if (General.IsNumeric(txtNoIntervention.Text))
                {
                    sWhere = sWhere + " AND Intervention.NoIntervention = '" + General.nz(txtNoIntervention.Text, "0") + "' ";
                }

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    sWhere = sWhere + " AND Intervention.Intervenant = '" + cmbIntervenant.Text + "'";
                }

                if (optAllumage.Checked == true)
                {
                    sWhere = sWhere + " AND  (FacArticle.ALAR_Code = 'ALL')";
                }
                else if (optArret.Checked == true)
                {
                    sWhere = sWhere + " AND  (FacArticle.ALAR_Code = 'ARR')";
                    //        Else
                    //            sWhere = sWhere & " AND (Intervention.TypeInterv = '3')"
                }
                //
                //        If sUChaufferie = "1" Then
                //            sWhere = sWhere & " AND  (Immeuble.CodeChaufferie >= '0000' AND Immeuble.CodeChaufferie <= '4999')"
                //        End If

                //        If cmbChefSecteur.Text <> "" Then
                //            sWhere = sWhere & " AND  Immeuble.CodeChefSecteur = '" & .cmbChefSecteur.Text & "'"
                //        End If

                if (!string.IsNullOrEmpty(cmbTitulaire.Text))
                {
                    sWhere = sWhere + " AND  Immeuble.CodeDepanneur = '" + _with3.cmbTitulaire.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbTypeCompteur.Text))
                {
                    if (General.IsNumeric(cmbTypeCompteur.Text))
                    {
                        sWhere = sWhere + " AND (P1_TypeCpt.Code = '" + cmbTypeCompteur.Text + "')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    //            If sUChaufferie = "1" Then
                    //                If sWhere = "" Then
                    //                    sWhere = " WHERE Immeuble.CodeImmeuble + '/' + Immeuble.CodeChaufferie LIKE '" & Replace(.cmbCodeimmeuble.Text, "*", "%") & "'"
                    //                Else
                    //                    sWhere = sWhere & " AND Immeuble.CodeImmeuble + '/' + Immeuble.CodeChaufferie LIKE '" & Replace(.cmbCodeimmeuble.Text, "*", "%") & "'"
                    //                End If
                    //            Else
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        //sWhere = " WHERE Immeuble.CodeImmeuble + '/' + Immeuble.CodeNumOrdre LIKE '" & Replace(.cmbCodeimmeuble.Text, "*", "%") & "'"
                        sWhere = " WHERE Immeuble.CodeImmeuble  LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    else
                    {
                        //sWhere = sWhere & " AND Immeuble.CodeImmeuble + '/' + Immeuble.CodeNumOrdre LIKE '" & Replace(.cmbCodeimmeuble.Text, "*", "%") & "'"
                        sWhere = sWhere + " AND Immeuble.CodeImmeuble  LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    //            End If
                }

                if (!string.IsNullOrEmpty(cmbCodeClient.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbEtatReleve.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                }

                if (chkArret.CheckState == CheckState.Checked && chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.Chauffage = '0' OR Intervention.Chauffage = '1')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.Chauffage = '0' OR Intervention.Chauffage = '1')";
                    }
                }
                else if (chkArret.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.Chauffage = '0')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.Chauffage = '0')";
                    }
                }
                else if (chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.Chauffage = '1')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.Chauffage = '1')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbStatut.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                }

                if (optAllumage.Checked == true || optArret.Checked == true)
                {
                    //sWhere = sWhere & " AND Immeuble.CodeUO + '/' + Immeuble.CodeImmeuble + '/' + Immeuble.CodeNumOrdre IN "
                    //sWhere = sWhere & " (SELECT DISTINCT COP_ContratP2.CodeUO + '/' + COP_ContratP2.CodeImmeuble + '/' + COP_ContratP2.CodeNumOrdre "

                    sWhere = sWhere + " AND  Immeuble.CodeImmeuble IN ";
                    sWhere = sWhere + " (SELECT DISTINCT  COP_ContratP2.CodeImmeuble ";

                    sWhere = sWhere + " FROM COP_ContratP2 INNER JOIN GMAO_Prestations ON";
                    sWhere = sWhere + " COP_ContratP2.COP_NoAuto = GMAO_Prestations.CleContrat ";
                    //sWhere = sWhere & " WHERE COP_ContratP2.CodeUO = '" & strCodeUO & " '"

                    //sWhere = sWhere & " AND (COP_ContratP2.COP_Archive = '0' OR COP_ContratP2.COP_Archive IS NULL)"
                    //sWhere = sWhere & " AND GMAO_Prestations.CodePrestation = '" & sPrestationAE & "' )"
                    sWhere = sWhere + " WHERE GMAO_Prestations.CodePrestation = '" + General.sPrestationAE + "' )";
                }

                sWhere = sWhere + " AND (COALESCE(UtilVerrou, '') = '') ";

                if (General.IsDate(txtSaisonDu.Text) && General.IsDate(txtSaisonAu.Text))
                {
                    //sWhere = sWhere & " AND Intervention.DatePrevue >= '" & Format(.txtSaisonDu.Text, FormatDateSQL) & "'"
                    //sWhere = sWhere & " AND Intervention.DatePrevue <= '" & Format(.txtSaisonAu.Text, FormatDateSQL) & "'"
                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(txtSaisonDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(txtSaisonAu.Text).ToString(General.FormatDateSQL) + "'";

                }

                if (!string.IsNullOrEmpty(cmbSemaine.Text))
                {
                    //sWhere = sWhere & " AND Intervention.DatePrevue >= '" & Format(.txtSem_DateDu.Text, FormatDateSQL) & "'"
                    //sWhere = sWhere & " AND Intervention.DatePrevue <= '" & Format(.txtSem_DateAu.Text, FormatDateSQL) & "'"
                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(txtSem_DateDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(txtSem_DateAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbMois.Text))
                {
                    //sWhere = sWhere & " AND Intervention.DatePrevue >= '" & Format(.txtMois_DateDu.Text, FormatDateSQL) & "'"
                    //sWhere = sWhere & " AND Intervention.DatePrevue <= '" & Format(.txtMois_DateAu.Text, FormatDateSQL) & "'"
                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(txtMois_DateDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(txtMois_DateAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                sqlUpdate = "UPDATE INT_RELEVE SET INT_RELEVE.P1_Envoye ='0' ";
                General.Execute(sqlUpdate + General.sSQL + sWhere + " AND P1R.NoIntervention IS NULL");//TODO this line Throw an exception in General.Execute() Column Chauffage doesn't exist. Check VB6 version.


                sqlUpdate = "UPDATE INT_RELEVE SET INT_RELEVE.Etat = 'RP1' ";
                General.Execute(sqlUpdate + General.sSQL + sWhere + " AND INT_RELEVE.P1_Envoye ='1' AND P1R.NoIntervention IS NOT NULL");
                //TODO this line Throw an exception in General.Execute() Column Chauffage doesn't exist. Check VB6 version.

                return functionReturnValue;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Feuille : UserDocSuiviReleves;fc_FlagRelEnvoye;");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private string fc_GetReleves()
        {
            string functionReturnValue = null;
            string sWhere = null;
            string sOperateur = null;
            // " OR " ou ""
            string sParentOuv = null;
            //Parenthèse ouverte "(" ou ""
            string sParentFerm = null;
            //Parenthèse fermée ")" ou ""
            int i = 0;
            object objcontrole = null;
            string sNoReleve = null;

            try
            {
                ModMajPDA.fc_SaveParamForm(this, this);

                var _with4 = this;

                General.Execute("UPDATE INT_Releve SET UtilVerrou = Null WHERE UtilVerrou = '" + StdSQLchaine.gFr_DoublerQuote(General.gsUtilisateur) + "'");

                fc_FlagRelEnvoye();

                General.sSQL = "";
                sWhere = "";

                General.sSQL = "SELECT INT_Releve.NoAuto FROM INT_Releve ";

                General.sSQL = General.sSQL + " INNER JOIN Intervention ON ";
                General.sSQL = General.sSQL + " INT_Releve.NoIntervention = Intervention.NoIntervention ";

                General.sSQL = General.sSQL + " LEFT OUTER JOIN TypeAnomalie ON ";
                General.sSQL = General.sSQL + " INT_Releve.Evenement = TypeAnomalie.Code ";

                General.sSQL = General.sSQL + " INNER JOIN Immeuble ON";
                General.sSQL = General.sSQL + " Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";
                General.sSQL = General.sSQL + " INNER JOIN Table1 ON";
                General.sSQL = General.sSQL + " Immeuble.Code1 = Table1.Code1 ";

                General.sSQL = General.sSQL + " INNER JOIN FacArticle ON ";
                General.sSQL = General.sSQL + " Intervention.Article = FacArticle.CodeArticle ";
                //        sSql = sSql & " AND Intervention.CodeUO = FacArticle.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN Imm_Appareils ON ";
                General.sSQL = General.sSQL + " INT_Releve.NumAppareil = Imm_Appareils.NumAppareil ";
                //        sSql = sSql & " AND INT_Releve.CodeUO = Imm_Appareils.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN P1_TypeCpt ON ";
                General.sSQL = General.sSQL + " Imm_Appareils.Type_Compteur = P1_TypeCpt.CODE ";

                sWhere = " WHERE Intervention.Rel_compteur = 1 ";

                if (General.IsNumeric(txtNoIntervention.Text))
                {
                    sWhere = sWhere + " AND Intervention.NoIntervention = '" + General.nz(txtNoIntervention.Text, "0") + "' ";
                }

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    sWhere = sWhere + " AND Intervention.Intervenant = '" + cmbIntervenant.Text + "'";
                }

                if (optAllumage.Checked == true)
                {
                    sWhere = sWhere + " AND  (FacArticle.ALAR_Code = 'ALL')";
                }
                else if (optArret.Checked == true)
                {
                    sWhere = sWhere + " AND  (FacArticle.ALAR_Code = 'ARR)";
                }
                if (!string.IsNullOrEmpty(cmbTitulaire.Text))
                {
                    sWhere = sWhere + " AND  Immeuble.CodeDepanneur = '" + cmbTitulaire.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbTypeCompteur.Text))
                {
                    if (General.IsNumeric(cmbTypeCompteur.Text))
                    {
                        sWhere = sWhere + " AND (P1_TypeCpt.Code = '" + cmbTypeCompteur.Text + "')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.CodeImmeuble LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.CodeImmeuble LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    //            End If
                }

                if (!string.IsNullOrEmpty(cmbCodeClient.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbEtatReleve.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                }

                if (chkArret.CheckState == CheckState.Checked && chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ARR' OR Intervention.ALAR_Code = 'ALL')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ARR' OR Intervention.ALAR_Code = 'ALL')";
                    }
                }
                else if (chkArret.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ARR')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ARR')";
                    }
                }
                else if (chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ALL')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ALL')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbStatut.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                }

                if (General.IsDate(txtSaisonDu.Text) && General.IsDate(txtSaisonAu.Text))
                {

                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(_with4.txtSaisonDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(_with4.txtSaisonAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbSemaine.Text))
                {

                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(_with4.txtSem_DateDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(_with4.txtSem_DateAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbMois.Text))
                {
                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(_with4.txtMois_DateDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(_with4.txtMois_DateAu.Text).ToString(General.FormatDateSQL) + "'";
                }



                //Requête de sélection des relevés à afficher dans la grille
                General.sSQL = " SELECT INT_Releve.NoAuto,INT_Releve.UtilVerrou AS [Verrouillé par] ";//TODO i add INT_Releve.NoAuto to the QUERY because i need the Primary Key when i update a Line.
                General.sSQL = General.sSQL + ", INT_Releve.P1_AEnvoyer AS [Envoyer] ";
                General.sSQL = General.sSQL + ", INT_Releve.P1_Envoye AS [Envoyé vers le P1] ";
                General.sSQL = General.sSQL + ", INT_Releve.Etat AS [Etat] ";
                General.sSQL = General.sSQL + ", Intervention.NoIntervention AS [No Intervention] ";
                General.sSQL = General.sSQL + ", Personnel.Prenom + ' ' + Personnel.Nom AS [Intervenant]";
                General.sSQL = General.sSQL + ", Immeuble.CodeImmeuble AS Chantier";

                General.sSQL = General.sSQL + ", Table1.Code1 + ' - ' + Table1.Nom AS [Client] ";

                General.sSQL = General.sSQL + ", INT_Releve.Libel_App AS [Compteur] ";
                General.sSQL = General.sSQL + ", INT_Releve.PeriodeAu AS [Date relevé] ";
                General.sSQL = General.sSQL + ", INT_Releve.Evenement AS [Evènement] ";
                General.sSQL = General.sSQL + ", INT_Releve.LibelleEvent AS [Désignation] ";
                General.sSQL = General.sSQL + ", INT_Releve.IndexDebut AS [Index début] ";
                General.sSQL = General.sSQL + ", INT_Releve.Livraison AS [Livraison] ";
                General.sSQL = General.sSQL + ", INT_Releve.SaisieJauge AS [Jauge] ";
                General.sSQL = General.sSQL + ", INT_Releve.SaisiePige AS [Pige] ";
                General.sSQL = General.sSQL + ", INT_Releve.IndexFin AS [Index fin] ";
                General.sSQL = General.sSQL + ", INT_Releve.Anc_IndexDebut AS [Ancien Index début] ";
                General.sSQL = General.sSQL + ", INT_Releve.Anc_IndexFin AS [Ancien Index fin] ";
                General.sSQL = General.sSQL + ", INT_Releve.Consommation AS [Conso] ";
                General.sSQL = General.sSQL + ", INT_Releve.dPeriodeDu AS [Période du] ";
                General.sSQL = General.sSQL + ", INT_Releve.dPeriodeAu AS [Période au] ";
                General.sSQL = General.sSQL + ", INT_Releve.Observations AS [Observation relevé] ";
                General.sSQL = General.sSQL + ", Imm_Appareils.[OrigineP1] AS [OrigineP1] ";
                General.sSQL = General.sSQL + ", P1_TypeCpt.[Livraison] AS [chkLivr] ";
                General.sSQL = General.sSQL + ", INT_Releve.PeriodeDu AS ReleveDu ";
                General.sSQL = General.sSQL + ", INT_Releve.NumAppareil ";
                General.sSQL = General.sSQL + ", INT_Releve.DiametreCuve AS [Diamètre cuve] ";
                General.sSQL = General.sSQL + ", INT_Releve.Chauffage ";
                General.sSQL = General.sSQL + " FROM INT_Releve ";

                General.sSQL = General.sSQL + " INNER JOIN Intervention ON ";
                General.sSQL = General.sSQL + " INT_Releve.NoIntervention = Intervention.NoIntervention ";

                General.sSQL = General.sSQL + " INNER JOIN Immeuble ON";
                General.sSQL = General.sSQL + " Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

                General.sSQL = General.sSQL + " INNER JOIN Table1 ON";
                General.sSQL = General.sSQL + " Immeuble.Code1 = Table1.Code1 ";

                General.sSQL = General.sSQL + " INNER JOIN FacArticle ON ";
                General.sSQL = General.sSQL + " Intervention.Article = FacArticle.CodeArticle ";
                //        sSql = sSql & " AND Intervention.CodeUO = FacArticle.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN Imm_Appareils ON ";
                General.sSQL = General.sSQL + " INT_Releve.NumAppareil = Imm_Appareils.NumAppareil ";
                //        sSql = sSql & " AND INT_Releve.CodeUO = Imm_Appareils.CodeUO "

                General.sSQL = General.sSQL + " LEFT OUTER JOIN P1_TypeCpt ON ";
                General.sSQL = General.sSQL + " Imm_Appareils.Type_Compteur = P1_TypeCpt.CODE ";

                General.sSQL = General.sSQL + " LEFT OUTER JOIN Personnel ON ";
                General.sSQL = General.sSQL + " Intervention.Intervenant = Personnel.Matricule ";

                sWhere = " WHERE Intervention.Rel_compteur = 1 AND (Intervention.CodeEtat <> 'ERR' AND Intervention.CodeEtat <> 'AN')";


                if (General.IsNumeric(txtNoIntervention.Text))
                {
                    sWhere = sWhere + " AND Intervention.NoIntervention = '" + General.nz((txtNoIntervention.Text), "0") + "' ";
                }

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    sWhere = sWhere + " AND Intervention.Intervenant = '" + cmbIntervenant.Text + "'";
                }

                if (optAllumage.Checked == true)
                {
                    sWhere = sWhere + " AND  (Intervention.ALAR_Code = 'ALL')";
                }
                else if (optArret.Checked == true)
                {
                    sWhere = sWhere + " AND  (Intervention.ALAR_Code = 'ARR')";
                    //        Else
                    //            sWhere = sWhere & " AND (Intervention.TypeInterv = '3')"
                }

                if (!string.IsNullOrEmpty(cmbTypeCompteur.Text))
                {
                    if (General.IsNumeric(cmbTypeCompteur.Text))
                    {
                        sWhere = sWhere + " AND (P1_TypeCpt.Code = '" + cmbTypeCompteur.Text + "')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbTitulaire.Text))
                {
                    sWhere = sWhere + " AND  Immeuble.CodeDepanneur = '" + cmbTitulaire.Text + "'";
                }

                if (General.IsDate(txtSaisonDu.Text) && General.IsDate(txtSaisonAu.Text))
                {
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(_with4.txtSaisonDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(_with4.txtSaisonAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbSemaine.Text))
                {
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + Convert.ToDateTime(txtSem_DateDu.Text).ToString(General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + Convert.ToDateTime(txtSem_DateAu.Text).ToString(General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbMois.Text))
                {
                    //=== modif du 15 07 2016, la date preuvue n est pas utilisé.
                    sWhere = sWhere + " AND Intervention.DateRealise >= '" + string.Format(txtMois_DateDu.Text, General.FormatDateSQL) + "'";
                    sWhere = sWhere + " AND Intervention.DateRealise <= '" + string.Format(txtMois_DateAu.Text, General.FormatDateSQL) + "'";
                }

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.CodeImmeuble LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.CodeImmeuble LIKE '" + General.Replace(cmbCodeimmeuble.Text, "*", "%") + "'";
                    }
                    //            End If
                }

                if (!string.IsNullOrEmpty(cmbCodeClient.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Table1.Code1 = '" + cmbCodeClient.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbEtatReleve.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND INT_Releve.Etat = '" + cmbEtatReleve.Text + "'";
                    }
                }

                if (chkArret.CheckState == CheckState.Checked && chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ARR' OR Intervention.ALAR_Code = 'ALL')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ARR' OR Intervention.ALAR_Code = 'ALL')";
                    }
                }
                else if (chkArret.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ARR')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ARR')";
                    }
                }
                else if (chkAllumage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.ALAR_Code = 'ALL')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.ALAR_Code = 'ALL')";
                    }
                }

                if (!string.IsNullOrEmpty(cmbStatut.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.CodeEtat = '" + cmbStatut.Text + "')";
                    }
                }


                if (!string.IsNullOrEmpty(sWhere))
                {
                    General.sSQL = General.sSQL + sWhere;
                }

                if (string.IsNullOrEmpty(sqlOrderBy))
                {
                    sqlOrderBy = " ORDER BY Immeuble.CodeImmeuble, INT_Releve.PeriodeAu ";
                }

                General.sSQL = General.sSQL + sqlOrderBy;

                functionReturnValue = General.sSQL;
                SSQL = General.sSQL;
                ModAdoCompteurs = new ModAdo();
                rsCompteurs = ModAdoCompteurs.fc_OpenRecordSet(General.sSQL);
                rsCompteurs.PrimaryKey = new DataColumn[] { rsCompteurs.Columns["NoAuto"] };
                lblTotal.Text = "";

                if (rsCompteurs.Rows.Count == 0)
                {
                    lblTotal.Text = "Aucun relevé en réponse";
                }
                else if (rsCompteurs.Rows.Count == 1)
                {
                    lblTotal.Text = string.Format(rsCompteurs.Rows.Count.ToString(), "### ##0") + " relevé en réponse";
                }
                else if (rsCompteurs.Rows.Count > 1)
                {
                    lblTotal.Text = string.Format(rsCompteurs.Rows.Count.ToString(), "### ##0") + " relevés en réponse";
                }

                //GridInterventions.Columns.RemoveAll();
                GridInterventions.DataSource = null;
                for (i = 0; i <= rsCompteurs.Columns.Count - 1; i++)
                {

                    GridInterventions.DisplayLayout.Bands[0].Columns.Add();
                    GridInterventions.DisplayLayout.Bands[0].Columns[i].Key = rsCompteurs.Columns[i].ColumnName;
                    GridInterventions.DisplayLayout.Bands[0].Columns[i].Header.Caption = rsCompteurs.Columns[i].ColumnName + "";


                    if (!string.IsNullOrEmpty(rsCompteurs.Columns[i].ExtendedProperties[1] + "") && i != 4)//TODO this  line must be checked with vb code
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Tag = rsCompteurs.Columns[i].ExtendedProperties[2] + "." + rsCompteurs.Columns[i].ExtendedProperties[1];//TODO this  line must be checked with vb code
                    }
                    else
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Tag = "[" + rsCompteurs.Columns[i].ColumnName + "]";
                    }
                    if (General.UCase(GridInterventions.DisplayLayout.Bands[0].Columns[i].Key).Contains(General.UCase("Column")))
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Key = rsCompteurs.Columns[i].ColumnName;
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Header.Caption = rsCompteurs.Columns[i].ColumnName;
                    }

                    //if (i == 1 || i == 2)
                    if (i == 2 || i == 3)
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Style =
                            Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
                    }

                    if (rsCompteurs.Columns[i].ColumnName == "Index début" || rsCompteurs.Columns[i].ColumnName == "Livraison" || rsCompteurs.Columns[i].ColumnName == "Jauge" || rsCompteurs.Columns[i].ColumnName == "Pige" || rsCompteurs.Columns[i].ColumnName == "Index fin" || rsCompteurs.Columns[i].ColumnName == "Ancien Index début" || rsCompteurs.Columns[i].ColumnName == "Ancien Index fin" || rsCompteurs.Columns[i].ColumnName == "Conso")
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].CellAppearance.TextHAlign =
                            Infragistics.Win.HAlign.Right;
                    }

                }

                GridInterventions.DisplayLayout.Bands[0].Columns[GridInterventions.DisplayLayout.Bands[0].Columns.Count - 1].Hidden = true;

                for (i = 0; i <= GridInterventions.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                {
                    if (i > GridInterventions.DisplayLayout.Bands[0].Columns["Envoyé vers le P1"].Index && i <= GridInterventions.DisplayLayout.Bands[0].Columns["Date relevé"].Index || i >= GridInterventions.DisplayLayout.Bands[0].Columns["Désignation"].Index && i <= GridInterventions.DisplayLayout.Bands[0].Columns["Désignation"].Index || i >= GridInterventions.DisplayLayout.Bands[0].Columns["Conso"].Index || i == GridInterventions.DisplayLayout.Bands[0].Columns["Verrouillé par"].Index)
                    {

                        GridInterventions.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
                    }

                    // Or i = GridInterventions.Columns("OrigineP1").Position Then
                    if (i == GridInterventions.DisplayLayout.Bands[0].Columns["chkLivr"].Index)
                    {
                        GridInterventions.DisplayLayout.Bands[0].Columns[i].Hidden = true;
                    }

                }

                //foreach (object objcontrole_loopVariable in this.Controls)
                //{
                //    objcontrole = objcontrole_loopVariable;
                //    if (objcontrole is AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid)
                //    {
                //        sheridan.fc_LoadDimensionGrille(this.Name, objcontrole);
                //    }
                //}
                //loopControlGrid(this,true);

                General.sSQL = "";
                General.sSQL = "SELECT TypeAnomalie.Code as [Code Evènement], TypeAnomalie.Libelle " + " FROM TypeAnomalie";

                if (ssDropEvenement.Rows.Count == 0)
                {
                    sheridan.InitialiseCombo((this.ssDropEvenement), General.sSQL, "Code Evènement", true);
                }

                this.GridInterventions.DisplayLayout.Bands[0].Columns["Evènement"].ValueList = ssDropEvenement;

                GridInterventions.DataSource = rsCompteurs;
                GridInterventions.UpdateData();
                if (bGoToReleve == true)
                {
                    bGoToReleve = false;

                    sNoReleve = General.getFrmReg(this.Name, "NoIntervention", "");
                    if (!string.IsNullOrEmpty(sNoReleve) && General.IsNumeric(sNoReleve))
                    {
                        if ((rsCompteurs != null))
                        {
                            //rsCompteurs.Find("[No Intervention] = " + sNoReleve, ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                            //if (rsCompteurs.Rows.Count > 0)
                            //{
                            //    GridInterventions.Bookmark = rsCompteurs.Bookmark;
                            //    GridInterventions.SelBookmarks.RemoveAll();
                            //    GridInterventions.SelBookmarks.Add((GridInterventions.GetBookmark(0)));
                            //}
                            if (rsCompteurs.Rows.Count > 0)
                                GridInterventions.Rows.Where(r => r.GetCellValue("No Intervention").ToString() == sNoReleve).ToList().ForEach(r => r.Selected = true);//TODO
                        }
                    }

                }
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_GetReleves;");
                return functionReturnValue;
            }
        }

        private void loopControlGrid(Control cc, bool isLoad = false)
        {
            if (cc.HasChildren)
                foreach (Control c in cc.Controls)
                    loopControlGrid(c, isLoad);
            if (cc is UltraGrid)
            {
                var c = cc as UltraGrid;
                if (isLoad)
                    sheridan.fc_LoadDimensionGrille(this.Name, c);
                else
                    sheridan.fc_SavDimensionGrille(this.Name, c);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLancer_Click_1(object sender, EventArgs e)
        {
            fc_Select();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_GetLivraisons()
        {
            object functionReturnValue = null;
            string[] tabChantier = null;

            try
            {

                General.sSQL = "EXEC Write_Livraisons_Releves ";

                if (!string.IsNullOrEmpty(cmbSemaine.Text))
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + txtSem_DateDu.Text + "', @DateAu = '" + txtSem_DateAu.Text + "'";

                }
                else if (!string.IsNullOrEmpty(cmbMois.Text))
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + txtMois_DateDu.Text + "', @DateAu = '" + txtMois_DateAu.Text + "'";

                }
                else if (General.IsDate(txtSaisonDu.Text) && General.IsDate(txtSaisonAu.Text))
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + txtSaisonDu.Text + "', @DateAu = '" + txtSaisonAu.Text + "'";

                }
                else
                {
                    General.sSQL = General.sSQL + " @DateDu = '" + txtSaisonDu.Text + "', @DateAu = '" + txtSaisonAu.Text + "'";
                }

                General.sSQL = General.sSQL + ", @CodeUO = '" + General.strCodeUO + "'";

                if (!string.IsNullOrEmpty(cmbCodeClient.Text))
                {
                    General.sSQL = General.sSQL + ", @CodeClient = '" + cmbCodeClient.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (cmbCodeimmeuble.Text.Contains("/"))
                    {
                        tabChantier = General.Split(cmbCodeimmeuble.Text, "/");
                    }
                    else
                    {
                        tabChantier = new string[2];
                        tabChantier[0] = cmbCodeimmeuble.Text;
                        tabChantier[1] = "";
                    }
                    General.sSQL = General.sSQL + ", @CodeImmeuble = '" + tabChantier[0] + "'";

                    General.sSQL = General.sSQL + ", @CodeNumOrdre = '" + tabChantier[1] + "'";
                }


                if (!string.IsNullOrEmpty(cmbChefSecteur.Text))
                {
                    General.sSQL = General.sSQL + ", @ChefSecteur = '" + cmbChefSecteur.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbIntervenant.Text))
                {
                    General.sSQL = General.sSQL + ", @Intervenant = '" + cmbIntervenant.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbTitulaire.Text))
                {
                    General.sSQL = General.sSQL + ", @TitulaireSite = '" + cmbTitulaire.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbTypeCompteur.Text))
                {
                    General.sSQL = General.sSQL + ", @TypeCompteur = '" + cmbTypeCompteur.Text + "'";
                }

                if (!string.IsNullOrEmpty(cmbEtatReleve.Text))
                {
                    General.sSQL = General.sSQL + ", @CodeEtatReleve = '" + cmbEtatReleve.Text + "'";
                }

                if (General.IsNumeric(txtNoIntervention.Text))
                {
                    General.sSQL = General.sSQL + ", @NoIntervention = '" + txtNoIntervention.Text + "'";
                }

                General.Execute(General.sSQL);
                return functionReturnValue;


            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_GetLivraisons;");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbMois_AfterCloseUp(object sender, EventArgs e)
        {
            string sNumMois = null;
            int i = 0;

            sNumMois = cmbMois.Text;

            lblLibMois.Text = "";
            txtMois_DateDu.Text = "";
            txtMois_DateAu.Text = "";

            if (!string.IsNullOrEmpty(cmbMois.Text))
            {
                cmbSemaine.Text = "";
                cmbSemaine.Value = "";
                if (cmbMois.Rows.Count > 0)
                {
                    /// cmbMois.MoveFirst();
                    for (i = 0; i <= cmbMois.Rows.Count - 1; i++)
                    {
                        if (cmbMois.Rows[i].Cells[0].Text == sNumMois)
                        {
                            lblLibMois.Text = cmbMois.Rows[i].Cells[1].Text + " : du " + cmbMois.Rows[i].Cells[2].Text + " au " + cmbMois.Rows[i].Cells[3].Text;

                            if (General.IsDate(cmbMois.Rows[i].Cells[2].Text))
                            {
                                txtMois_DateDu.Text = cmbMois.Rows[i].Cells[2].Text;
                            }
                            else
                            {
                                txtMois_DateDu.Text = "";
                            }

                            if (General.IsDate(cmbMois.Rows[i].Cells[3].Text))
                            {
                                txtMois_DateAu.Text = cmbMois.Rows[i].Cells[3].Text;
                            }
                            else
                            {
                                txtMois_DateAu.Text = "";
                            }

                        }
                        //cmbMois.MoveNext();
                    }
                }
            }
            else
            {
                lblLibMois.Text = "";
                txtMois_DateDu.Text = "";
                txtMois_DateAu.Text = "";
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbMois_ValueChanged(object sender, EventArgs e)
        {
            if (cmbMois.ActiveRow != null)
            {
                cmbSemaine.Text = "";
                lblLibSemaine.Text = "";
                if (cmbMois.ActiveRow != null)
                {
                    lblLibMois.Text = cmbMois.ActiveRow.Cells[1].Text + " : du " + cmbMois.ActiveRow.Cells[2].Text + " au " + cmbMois.ActiveRow.Cells[3].Text;
                    if (General.IsDate(cmbMois.ActiveRow.Cells[2].Text))
                    {
                        txtMois_DateDu.Text = cmbMois.ActiveRow.Cells[2].Text;
                    }
                    else
                    {
                        txtMois_DateDu.Text = "";
                    }

                    if (General.IsDate(cmbMois.ActiveRow.Cells[3].Text))
                    {
                        txtMois_DateAu.Text = cmbMois.ActiveRow.Cells[3].Text;
                    }
                    else
                    {
                        txtMois_DateAu.Text = "";
                    }
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbMois_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == (short)System.Windows.Forms.Keys.Return)
            {
                fc_Select();
                e.KeyChar = (char)0;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbMois_Validating_1(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            string sNumMois = null;
            int i = 0;

            sNumMois = cmbMois.Text;

            lblLibMois.Text = "";
            txtMois_DateDu.Text = "";
            txtMois_DateAu.Text = "";

            if (!string.IsNullOrEmpty(cmbMois.Text))
            {
                if (!General.IsNumeric(cmbMois.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un numéro de mois valide.", "Numéro de mois attendu", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Cancel = true;
                    cmbMois.Focus();
                }
                else
                {
                    cmbSemaine.Text = "";
                    if (cmbMois.Rows.Count > 0)
                    {
                        //cmbMois.MoveFirst();
                        for (i = 0; i <= cmbMois.Rows.Count - 1; i++)
                        {
                            if (cmbMois.Rows[i].Cells[0].Text == sNumMois)
                            {
                                lblLibMois.Text = cmbMois.Rows[i].Cells[1].Text + " : du " + cmbMois.Rows[i].Cells[2].Text + " au " + cmbMois.Rows[i].Cells[3].Text;

                                if (General.IsDate(cmbMois.Rows[i].Cells[2].Text))
                                {
                                    txtMois_DateDu.Text = cmbMois.Rows[i].Cells[2].Text;
                                }
                                else
                                {
                                    txtMois_DateDu.Text = "";
                                }

                                if (General.IsDate(cmbMois.Rows[i].Cells[3].Text))
                                {
                                    txtMois_DateAu.Text = cmbMois.ActiveRow.Cells[3].Text;
                                }
                                else
                                {
                                    txtMois_DateAu.Text = "";
                                }
                                break;
                            }
                            //cmbMois.MoveNext();
                            if (i + 1 > (cmbMois.Rows.Count - 1))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un mois valide.", "Numéro de mois inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                Cancel = true;
                                cmbMois.Focus();
                                goto EventExitSub;
                            }
                        }
                    }
                }
            }
            else
            {
                lblLibMois.Text = "";
                txtMois_DateDu.Text = "";
                txtMois_DateAu.Text = "";
            }
        EventExitSub:

            e.Cancel = Cancel;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSemaine_AfterCloseUp(object sender, EventArgs e)
        {
            string sNumSem = null;
            int i = 0;

            sNumSem = cmbSemaine.Text;

            lblLibSemaine.Text = "";
            txtSem_DateDu.Text = "";
            txtSem_DateAu.Text = "";

            if (!string.IsNullOrEmpty(cmbSemaine.Text))
            {
                cmbMois.Text = "";
                cmbMois.Value = "";
                if (cmbSemaine.Rows.Count > 0)
                {
                    //cmbSemaine.MoveFirst();
                    for (i = 0; i <= cmbSemaine.Rows.Count - 1; i++)
                    {
                        if (cmbSemaine.Rows[i].Cells[0].Text == sNumSem)
                        {
                            lblLibSemaine.Text = "Du " + Convert.ToDateTime(cmbSemaine.Rows[i].Cells[1].Text).ToString("dd/MM/yyyy") + " au " + Convert.ToDateTime(cmbSemaine.Rows[i].Cells[2].Text).ToString("dd/MM/yyyy");

                            if (General.IsDate(cmbSemaine.Rows[i].Cells[1].Text))
                            {
                                txtSem_DateDu.Text = cmbSemaine.Rows[i].Cells[1].Text;
                            }
                            else
                            {
                                txtSem_DateDu.Text = "";
                            }

                            if (General.IsDate(cmbSemaine.Rows[i].Cells[2].Text))
                            {
                                txtSem_DateAu.Text = cmbSemaine.Rows[i].Cells[2].Text;
                            }
                            else
                            {
                                txtSem_DateAu.Text = "";
                            }
                        }
                        // cmbSemaine.MoveNext();
                    }
                }
            }
            else
            {
                lblLibSemaine.Text = "";
                txtSem_DateDu.Text = "";
                txtSem_DateAu.Text = "";
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSemaine_ValueChanged(object sender, EventArgs e)
        {

            if (cmbSemaine.ActiveRow != null)
            {
                cmbMois.Text = "";
                lblLibMois.Text = "";
                if (cmbSemaine.ActiveRow != null)
                {
                    lblLibSemaine.Text = "Du " + cmbSemaine.ActiveRow.Cells[1].Text + " au " + cmbSemaine.ActiveRow.Cells[2].Text;
                    txtSem_DateDu.Text = cmbSemaine.ActiveRow.Cells[1].Text;
                    txtSem_DateAu.Text = cmbSemaine.ActiveRow.Cells[2].Text;
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSemaine_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == (short)System.Windows.Forms.Keys.Return)
            {
                fc_Select();
                e.KeyChar = (char)0;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbSemaine_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            string sNumSem = null;
            int i = 0;

            sNumSem = cmbSemaine.Text;

            if (!string.IsNullOrEmpty(cmbSemaine.Text))
            {
                if (cmbSemaine.Rows.Count > 0)
                {
                    //cmbSemaine.MoveFirst();
                    for (i = 0; i <= cmbSemaine.Rows.Count - 1; i++)
                    {
                        if (cmbSemaine.Rows[i].Cells[0].Text == sNumSem)
                        {
                            lblLibSemaine.Text = "Du " + Convert.ToDateTime(cmbSemaine.Rows[i].Cells[1].Text).ToString("dd/MM/yyyy") + " au " + Convert.ToDateTime(cmbSemaine.Rows[i].Cells[2].Text).ToString("dd/MM/yyyy");

                            if (General.IsDate(cmbSemaine.Rows[i].Cells[1].Text))
                            {
                                txtSem_DateDu.Text = cmbSemaine.Rows[i].Cells[1].Text;
                            }
                            else
                            {
                                txtSem_DateDu.Text = "";
                            }

                            if (General.IsDate(cmbSemaine.Rows[i].Cells[2].Text))
                            {
                                txtSem_DateAu.Text = cmbSemaine.Rows[i].Cells[2].Text;
                            }
                            else
                            {
                                txtSem_DateAu.Text = "";
                            }
                            break;
                        }
                        //cmbSemaine.MoveNext();
                        if (i + 1 > (cmbSemaine.Rows.Count - 1))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un numéro de semaine existant.", "Numéro de semaine inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            Cancel = true;
                            cmbSemaine.Focus();
                        }
                    }
                }
            }

            e.Cancel = Cancel;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatut_AfterCloseUp(object sender, EventArgs e)
        {
            ModAdoADOlibelle = new ModAdo();
            lblLibStatus.Text = ModAdoADOlibelle.fc_ADOlibelle("SELECT TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat WHERE TypeCodeEtat.CodeEtat='" + cmbStatut.Text + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatut_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat order by CodeEtat";
            if (cmbStatut.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbStatut, General.sSQL, "CodeEtat", true);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTitulaire_AfterCloseUp(object sender, EventArgs e)
        {
            ModAdoADOlibelle = new ModAdo();
            lblLibTitulaire.Text = ModAdoADOlibelle.fc_ADOlibelle("SELECT Personnel.Nom + ' ' + Personnel.Prenom AS Interv FROM" + " Personnel INNER JOIN SpecifQualif ON " + " Personnel.CodeQualif = SpecifQualif.CodeQualif WHERE Personnel.Matricule='" + cmbTitulaire.Text + "' AND SpecifQualif.QualifTech = '1'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTitulaire_ValueChanged(object sender, EventArgs e)
        {
            ModAdoADOlibelle = new ModAdo();
            lblLibTitulaire.Text = ModAdoADOlibelle.fc_ADOlibelle("SELECT Personnel.Nom + ' ' + Personnel.Prenom AS Interv FROM" + " Personnel INNER JOIN SpecifQualif ON " + " Personnel.CodeQualif = SpecifQualif.CodeQualif WHERE Personnel.Matricule='" + cmbTitulaire.Text + "' AND SpecifQualif.QualifTech = '1'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTitulaire_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom " + " FROM Personnel INNER JOIN " + " SpecifQualif ON Personnel.CodeQualif = SpecifQualif.CodeQualif " + " WHERE COALESCE(Personnel.NonActif, 0) <> 1 AND SpecifQualif.QualifTech = '1' ORDER BY Nom";
            sheridan.InitialiseCombo(cmbTitulaire, General.sSQL, "Matricule", true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypeCompteur_AfterCloseUp(object sender, EventArgs e)
        {
            General.sSQL = "SELECT ";
            General.sSQL = General.sSQL + " LIBELLE";
            General.sSQL = General.sSQL + " FROM P1_TypeCpt ";
            General.sSQL = General.sSQL + " WHERE Code = '" + cmbTypeCompteur.Text + "'";
            ModAdoADOlibelle = new ModAdo();
            lblLibTypeCompteur.Text = ModAdoADOlibelle.fc_ADOlibelle(General.sSQL);
            fc_GestAffichage();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_GestAffichage()
        {
            bool bEnableGenerer = false;

            bEnableGenerer = true;

            if (!string.IsNullOrEmpty(cmbTypeCompteur.Text))
            {
                bEnableGenerer = false;
            }

            if (!string.IsNullOrEmpty(cmbEtatReleve.Text))
            {
                bEnableGenerer = false;
            }

            cmdGenerer.Enabled = bEnableGenerer;

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypeCompteur_ValueChanged(object sender, EventArgs e)
        {
            if (cmbTypeCompteur.ActiveRow != null)
            {
                lblLibTypeCompteur.Text = cmbTypeCompteur.ActiveRow.Cells[1].Text;
            }
            fc_GestAffichage();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbTypeCompteur_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CODE";
            General.sSQL = General.sSQL + " ,LIBELLE";
            General.sSQL = General.sSQL + " FROM P1_TypeCpt ";
            General.sSQL = General.sSQL + " ORDER BY LIBELLE ";
            sheridan.InitialiseCombo(cmbTypeCompteur, General.sSQL, "CODE", true);
        }


        //TODO .column CodeUO not exist
        private void cmdChefCentre_Click(object sender, EventArgs e)
        {
            //TODO .column CodeUO not exist
            string requete;
            string where_order = "";
            try
            {
                General.sSQL = "Select Personnel.Matricule,Personnel.Nom FROM Personnel INNER JOIN SpecifQualif ON";
                General.sSQL = General.sSQL + " Personnel.CodeQualif = SpecifQualif.CodeQualif ";

                requete = General.sSQL;

                where_order = " QualifDisp = '1' AND CodeUO = '" + General.strCodeUO + "'";
                SearchTemplate fg = new SearchTemplate(null, null, requete, where_order, "") { Text = "Recherche d'un chef de secteur" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    cmbChefSecteur.Value = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };
                fg.ugResultat.KeyPress += (se, ev) =>
                {
                    if (Convert.ToInt32(ev.KeyChar) == 13)
                    {
                        cmbChefSecteur.Value = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdChefCentre_Click;");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            General.fc_CleanForm(this);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExporter_Click_1(object sender, EventArgs e)
        {
            cmdExporter.Enabled = false;
            fc_Editer();
            cmdExporter.Enabled = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_Editer()
        {
            object functionReturnValue = null;
            Excel.Application xlsInterv = null;
            //As Excel.application
            Excel.Workbook wrkbkInterv = null;
            //As Workbook
            int i = 0;
            int lngCol = 0;
            int lngLigne = 0;
            string sNoIntervention = null;

            string sNomFic = null;
            bool blnOpen = false;
            int nbRows = 0;
            string sChantier = null;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (GridInterventions.Rows.Count == 0)//tested
                {
                    fc_Select();
                    if (GridInterventions.Rows.Count == 0)
                    {
                        return functionReturnValue;
                    }
                }

                if (ProgressBar1.Visible == false)
                {
                    ProgressBar1.Visible = true;
                }
                //ProgressBar1.Appearance = ComctlLib.AppearanceConstants.ccFlat;//TODO progress bar doesn't have FLAT property
                ProgressBar1.Maximum = GridInterventions.Rows.Count;
                ProgressBar1.Minimum = 0;
                System.Windows.Forms.Application.DoEvents();

                sNomFic = "Inter_Releves_Compteurs_" + DateTime.Today.ToString("yyyyMMdd") + "_" + DateTime.Now.ToString("HHmmss") + ".xls";

                xlsInterv = new Microsoft.Office.Interop.Excel.Application();
                wrkbkInterv = xlsInterv.Workbooks.Add();

                xlsInterv.DisplayAlerts = false;


                wrkbkInterv.Sheets[1].PageSetup.LeftHeader = General.NomSociete;

                wrkbkInterv.Sheets[1].PageSetup.PrintTitleRows = "$1:$1";
                wrkbkInterv.Sheets[1].PageSetup.PrintTitleColumns = "";

                wrkbkInterv.Sheets[1].PageSetup.CenterHeader = "Suivi des relevés de compteurs - Saison " + txtSaison.Text;

                wrkbkInterv.Sheets[1].PageSetup.RightHeader = " " + Convert.ToString(DateTime.Now.ToString("dd/MM/yyyy HH:mm"));

                wrkbkInterv.Sheets[1].PageSetup.LeftFooter = General.gsUtilisateur;
                wrkbkInterv.Sheets[1].PageSetup.CenterFooter = sNomFic;
                //Affichage du numéro de pages : Numéro de la page / Nombre total de pages
                wrkbkInterv.Sheets[1].PageSetup.RightFooter = "&P / &N";

                wrkbkInterv.Sheets[1].PageSetup.LeftMargin = 56.6929134;
                wrkbkInterv.Sheets[1].PageSetup.RightMargin = 56.6929134;
                wrkbkInterv.Sheets[1].PageSetup.TopMargin = 70.866141768;
                wrkbkInterv.Sheets[1].PageSetup.BottomMargin = 70.866141768;
                wrkbkInterv.Sheets[1].PageSetup.HeaderMargin = 35.433070884;
                wrkbkInterv.Sheets[1].PageSetup.FooterMargin = 35.433070884;

                wrkbkInterv.Sheets[1].PageSetup.PrintHeadings = false;
                wrkbkInterv.Sheets[1].PageSetup.PrintGridLines = false;
                wrkbkInterv.Sheets[1].PageSetup.PrintComments = Microsoft.Office.Interop.Excel.XlPrintLocation.xlPrintNoComments;
                wrkbkInterv.Sheets[1].PageSetup.CenterHorizontally = false;
                wrkbkInterv.Sheets[1].PageSetup.CenterVertically = false;
                wrkbkInterv.Sheets[1].PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlPortrait;
                wrkbkInterv.Sheets[1].PageSetup.Draft = false;
                wrkbkInterv.Sheets[1].PageSetup.PaperSize = Microsoft.Office.Interop.Excel.XlPaperSize.xlPaperA4;
                wrkbkInterv.Sheets[1].PageSetup.FirstPageNumber = Microsoft.Office.Interop.Excel.Constants.xlAutomatic;
                wrkbkInterv.Sheets[1].PageSetup.Order = Microsoft.Office.Interop.Excel.XlOrder.xlDownThenOver;
                wrkbkInterv.Sheets[1].PageSetup.BlackAndWhite = false;
                wrkbkInterv.Sheets[1].PageSetup.Zoom = 100;
                wrkbkInterv.Sheets[1].PageSetup.PrintErrors = Microsoft.Office.Interop.Excel.XlPrintErrors.xlPrintErrorsDisplayed;
                if (GridInterventions.ActiveRow != null)
                    sNoIntervention = GridInterventions.ActiveRow.Cells["No Intervention"].Text;

                if (GridInterventions.Rows.Count != rsCompteurs.Rows.Count)
                {
                    nbRows = GridInterventions.Rows.Count;
                    while (GridInterventions.Rows.Count != rsCompteurs.Rows.Count)
                    {
                        //Continuer en stockant la nouvelle valeur
                        if (nbRows != GridInterventions.Rows.Count)
                        {
                            nbRows = GridInterventions.Rows.Count;
                            //Plus rien ne se passe
                        }
                        else
                        {
                            break;
                        }
                    }
                }

                for (i = GridInterventions.DisplayLayout.Bands[0].Columns["Chantier"].Index; i <= GridInterventions.DisplayLayout.Bands[0].Columns["OrigineP1"].Index - 1; i++)
                {

                    xlsInterv.Range[((char)(Convert.ToInt32('A') + (i - GridInterventions.DisplayLayout.Bands[0].Columns["Chantier"].Index))) + "1"].Value = GridInterventions.DisplayLayout.Bands[0].Columns[i].Key;
                    xlsInterv.Range[((char)(Convert.ToInt32('A') + (i - GridInterventions.DisplayLayout.Bands[0].Columns["Chantier"].Index))) + "1"].Font.Bold = true;
                }

                lngLigne = 1;
                int j = 0;
                if (rsCompteurs.Rows.Count > 0)
                {
                    //rsCompteurs.MoveFirst();

                    foreach (DataRow dr in rsCompteurs.Rows)
                    {
                        if (ProgressBar1.Maximum < lngLigne)
                        {
                            ProgressBar1.Maximum = lngLigne;
                        }
                        ProgressBar1.Value = lngLigne;
                        ProgressBar1.PerformStep();
                        //Application.DoEvents();
                        if (string.IsNullOrEmpty(sChantier))
                        {
                            sChantier = dr["Chantier"] + "";
                        }
                        else
                        {
                            if (sChantier != dr["Chantier"] + "")
                            {
                                sChantier = dr["Chantier"] + "";
                                lngLigne = lngLigne + 1;
                            }
                        }

                        for (i = GridInterventions.DisplayLayout.Bands[0].Columns["Chantier"].Index; i <= GridInterventions.DisplayLayout.Bands[0].Columns["OrigineP1"].Index - 1; i++)
                        {
                            lngCol = i - GridInterventions.DisplayLayout.Bands[0].Columns["Chantier"].Index;

                            if (i == GridInterventions.DisplayLayout.Bands[0].Columns["Période du"].Index || i == GridInterventions.DisplayLayout.Bands[0].Columns["Période au"].Index)//TESTED
                            {
                                xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].NumberFormat = "@";
                                xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].Value = Convert.ToDateTime(dr[i]).ToString("MM/yyyy");
                            }
                            else
                            {

                                if (dr[i].GetType() == typeof(DateTime))
                                {
                                    if (i == GridInterventions.DisplayLayout.Bands[0].Columns["Période Du"].Index && General.IsDate(dr[i]))
                                    {
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].NumberFormat = "MM/yyyy";
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].Value = Convert.ToDateTime(dr[i]).ToString("MM/yyyy");
                                    }
                                    else if (i == GridInterventions.DisplayLayout.Bands[0].Columns["Période Au"].Index & General.IsDate(dr[i]))
                                    {
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].NumberFormat = "MM/yyyy";
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].Value = Convert.ToDateTime(dr[i]).ToString("MM/yyyy");
                                    }
                                    else//TESTED
                                    {
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].NumberFormat = "M/d/yyyy";
                                        xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].Value = " " + Convert.ToDateTime(dr[i]).ToString("MM/dd/yyyy");
                                    }
                                }
                                else//TESTED
                                {
                                    xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].NumberFormat = "@";
                                    xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].Value = dr[i];
                                }
                            }
                            if (dr[i].GetType() != typeof(string) && dr[i].GetType() != typeof(char))//TESTED
                            {
                                xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlRight;
                            }
                            else//TESTED
                            {
                                xlsInterv.Range[((char)(Convert.ToInt32('A') + lngCol)).ToString() + (lngLigne + 1)].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlLeft;
                            }
                        }
                        lngLigne = lngLigne + 1;
                        //rsCompteurs.MoveNext();
                    }
                    //rsCompteurs.MoveFirst();
                }



                xlsInterv.Range["A:" + General.ConvertColExcel(GridInterventions.DisplayLayout.Bands[0].Columns.Count - 1)].Select();
                xlsInterv.Range["A:" + General.ConvertColExcel(GridInterventions.DisplayLayout.Bands[0].Columns.Count - 1)].EntireColumn.AutoFit();

                xlsInterv.Rows[2].Select();
                //Permet de figer les volets
                xlsInterv.ActiveWindow.FreezePanes = true;
                for (i = 1; i <= xlsInterv.Sheets.Count; i++)
                {
                    //    Affichage des onglets en paysage
                    xlsInterv.Sheets[i].PageSetup.Orientation = Microsoft.Office.Interop.Excel.XlPageOrientation.xlLandscape;
                }

                xlsInterv.Range["A1"].Select();

                //    Affichage des sauts de page
                //    xlsInterv.ActiveWindow.View = xlPageBreakPreview
                //    Fait glisser la zone d'impression vers la droite en ne laissant qu'une page verticale
                //    xlsInterv.ActiveSheet.VPageBreaks(1).DragOff Direction:=xlToRight, RegionIndex:=1
                //    Rétablit l'affichage en "Normal"
                //    xlsInterv.ActiveWindow.View = xlNormalView

                xlsInterv.Visible = true;
                //wrkbkInterv.SaveAs(General.sPathEdition + sNomFic);
                //xlsInterv.Workbooks.Close();

                //wrkbkInterv = null;

                //xlsInterv.Quit();
                //xlsInterv = null;
                ProgressBar1.Value = ProgressBar1.Minimum;
                ProgressBar1.Visible = false;
                System.Windows.Forms.Application.DoEvents();

                //ModuleAPI.Ouvrir(General.sPathEdition + sNomFic);

                if (GridInterventions.ActiveRow.Cells[3].Text != sNoIntervention)
                {
                    //rsCompteurs.Find("[No Intervention] = " + sNoIntervention, , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                    //if (!rsCompteurs.EOF)
                    //{
                    //    GridInterventions.Bookmark = rsCompteurs.Bookmark;
                    //    GridInterventions.SelBookmarks.RemoveAll();
                    //    GridInterventions.SelBookmarks.Add((GridInterventions.GetBookmark(0)));
                    //}
                    GridInterventions.Rows.Where(r => r.GetCellValue("No Intervention").ToString() == sNoIntervention).ToList().ForEach(r => r.Selected = true);//TODO
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                ProgressBar1.Value = ProgressBar1.Minimum;
                ProgressBar1.Visible = false;
                Erreurs.gFr_debug(ex, this.Name + ";fc_Editer;");
                return functionReturnValue;
            }
        }

        public string fc_GetFile(string sPath, string sTypeName)
        {
            string sResult = null;
            sResult = sPath + "\\" + sTypeName + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmss") + ".txt";
            return sResult;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGenerer_Click(object sender, EventArgs e)
        {
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Etes-vous sûr de vouloir envoyer les relevés sélectionnés vers le logiciel de suisi des consommations ?", "Envoi des relevés de consommations", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                fc_SendInterP1();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        private object fc_SendInterP1()
        {
            object functionReturnValue = null;
            string sNomBaseLisa = null;
            DataTable rsTestEnvoi = default(DataTable);
            string sUtilBloque = null;
            object bkmrk = null;
            int nbRecAff = 0;
            int nbRecMAJ = 0;


            try
            {
                nbRecMAJ = 0;

                sNomBaseLisa = General.adocnn.Database.ToString();

                if (ModP1.adoP1 == null)
                {
                    ModP1.fc_OpenConnP1();
                }

                sUtilBloque = "";

                rsTestEnvoi = new DataTable();
                General.SQL = "SELECT DISTINCT INT_Releve.UtilVerrou FROM INT_Releve WHERE NoIntervention IN(";
                General.SQL = General.SQL + " SELECT NoIntervention ";
                General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve";
                General.SQL = General.SQL + " WHERE [" + sNomBaseLisa + "].dbo.INT_Releve.P1_AEnvoyer = '1'";
                General.SQL = General.SQL + " AND (COALESCE([" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye, '0') = '0')";
                General.SQL = General.SQL + " AND [" + sNomBaseLisa + "].dbo.INT_Releve.UtilVerrou = '" + General.gsUtilisateur + "')";
                ModAdorsTestEnvoi = new ModAdo();
                rsTestEnvoi = ModAdorsTestEnvoi.fc_OpenRecordSet(General.SQL);
                if (rsTestEnvoi != null && rsTestEnvoi.Rows.Count > 0)
                {
                    foreach (DataRow dr in rsTestEnvoi.Rows)
                    {
                        if (dr["UtilVerrou"] + "" != General.gsUtilisateur)
                        {
                            if (string.IsNullOrEmpty(sUtilBloque))
                            {
                                sUtilBloque = dr["UtilVerrou"] + "";
                            }
                            else
                            {
                                sUtilBloque = "\r\n" + dr["UtilVerrou"];
                            }
                        }

                    }
                }
                rsTestEnvoi.Clear();
                rsTestEnvoi = null;

                if (rsCompteurs != null && rsCompteurs.Rows.Count > 0)//TESTED
                {
                    var drs1 = rsCompteurs.Select("[Verrouillé par] <> '" + General.gsUtilisateur + "'");
                    if (drs1.Length > 0)
                    {

                        foreach (DataRow drr in drs1)
                        {
                            if (drr["Verrouillé par"] + "" != General.gsUtilisateur)
                            {
                                if (string.IsNullOrEmpty(sUtilBloque))
                                {
                                    sUtilBloque = drr["Verrouillé par"] + "";
                                }
                                else
                                {
                                    sUtilBloque = "\r\n" + drr["Verrouillé par"] + "";
                                }
                            }
                            var drs2 = rsCompteurs.Select("[Verrouillé par] <> '" + General.gsUtilisateur + "' AND [Verrouillé par] <> '" + drr["Verrouillé par"] + "'");
                            if (drs2.Length == 0)
                            {
                                break;
                            }
                        }
                    }
                    //rsCompteurs.Filter = ADODB.FilterGroupEnum.adFilterNone;
                }

                if (!string.IsNullOrEmpty(sUtilBloque))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas envoyer les relevés dans le suivi des consommations, des interventions concernées par votre transfert sont bloquées par : " + sUtilBloque, "Transfert vers le suivi des consommations annulé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }

                //Suppression des relevés liés aux interventions envoyées précédemment
                General.SQL = "DELETE FROM P1_Releve WHERE ";
                General.SQL = General.SQL + " NoIntervention IN(";
                General.SQL = General.SQL + " SELECT NoIntervention ";
                General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve";
                General.SQL = General.SQL + " WHERE [" + sNomBaseLisa + "].dbo.INT_Releve.P1_AEnvoyer = '1'";
                General.SQL = General.SQL + " AND COALESCE([" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye, '') <> '1'";
                General.SQL = General.SQL + " AND COALESCE([" + sNomBaseLisa + "].dbo.INT_Releve.Etat, '') <> 'RP1'";
                General.SQL = General.SQL + " AND [" + sNomBaseLisa + "].dbo.INT_Releve.UtilVerrou = '" + General.gsUtilisateur + "')";

                //ModP1.adoP1.Execute(General.SQL);
                //TODO il faut changer ces lignes de codes par General.Execute(General.SQL, ModP1.adoP1);
                SqlCommand cmd = new SqlCommand(General.SQL, ModP1.adoP1);
                if (ModP1.adoP1.State != ConnectionState.Open)
                    ModP1.adoP1.Open();
                nbRecAff = cmd.ExecuteNonQuery();

                //Transfert du relevé vers le P1 :
                //Envoi des relevés à partir du tableau saisi
                General.SQL = "INSERT INTO P1_Releve (";
                General.SQL = General.SQL + " NoFichierGraphe, NumAppareil, CodeUO, CodeAffaire, CodeNumOrdre, ";
                General.SQL = General.SQL + " Saison, NoDecade, Periodedu, PeriodeAu, ";
                General.SQL = General.SQL + " CodeTransmission, CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, ";
                General.SQL = General.SQL + " Livraison, Consommation, Estimation, Anc_IndexDebut, Anc_IndexFin,";
                General.SQL = General.SQL + " Evenement, Date_Event, Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, ";
                General.SQL = General.SQL + " CoefPTA, CoefPCS, VolConverti, NomFic, DateImport,";
                General.SQL = General.SQL + " NoIntervention, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu)";

                General.SQL = General.SQL + " SELECT INT_R.NoFichierGraphe, INT_R.NumAppareil, ";
                General.SQL = General.SQL + " INT_R.CodeUO, INT_R.CodeAffaire, INT_R.CodeNumOrdre, INT_R.Saison, INT_R.NoDecade, INT_R.Periodedu, ";
                General.SQL = General.SQL + " INT_R.PeriodeAu, INT_R.CodeTransmission, ";
                General.SQL = General.SQL + " INT_R.CodeAnomalie, INT_R.Chauffage, INT_R.IndexDebut, INT_R.IndexFin, INT_R.NJours, INT_R.Livraison, INT_R.Consommation, ";
                General.SQL = General.SQL + " INT_R.Estimation, INT_R.Anc_IndexDebut, INT_R.Anc_IndexFin, INT_R.Evenement,";
                General.SQL = General.SQL + " INT_R.Date_Event , INT_R.Index_Event, INT_R.Etat_Cuve, INT_R.Type_Rel, INT_R.Appro_Prevu, INT_R.Facture, INT_R.CoefPTA, ";
                General.SQL = General.SQL + " INT_R.CoefPCS, INT_R.VolConverti, INT_R.NomFic, INT_R.DateImport, INT_R.NoIntervention, ";
                General.SQL = General.SQL + " INT_R.SaisieJauge, INT_R.DiametreCuve, INT_R.SaisiePige, INT_R.dPeriodeDu, INT_R.dPeriodeAu ";
                General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve INT_R ";
                General.SQL = General.SQL + " INNER JOIN [" + sNomBaseLisa + "].dbo.Intervention INTER ON  ";
                General.SQL = General.SQL + " INT_R.NoIntervention = INTER.NoIntervention ";
                General.SQL = General.SQL + " WHERE INT_R.P1_AEnvoyer = '1'";
                General.SQL = General.SQL + " AND COALESCE(INT_R.P1_Envoye, '0') = '0'";
                General.SQL = General.SQL + " AND INT_R.UtilVerrou = '" + General.gsUtilisateur + "'";
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'ERR'";
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'AN'";

                //ModP1.adoP1.Execute(General.SQL, nbRecAff, ADODB.ExecuteOptionEnum.adExecuteNoRecords);
                //TODO il faut changer ces lignes de codes par General.Execute(General.SQL, ModP1.adoP1);
                cmd = new SqlCommand(General.SQL, ModP1.adoP1);
                if (ModP1.adoP1.State != ConnectionState.Open)
                    ModP1.adoP1.Open();
                nbRecAff = cmd.ExecuteNonQuery();
                nbRecMAJ = nbRecAff;

                nbRecAff = 0;

                //Marquage des relevés comme étant transférés dans le P1
                General.SQL = "UPDATE INT_R SET ";
                General.SQL = General.SQL + " Etat = 'RP1', P1_Envoye = '1', P1_AEnvoyer = '0' ";
                General.SQL = General.SQL + " FROM INT_Releve INT_R ";
                General.SQL = General.SQL + " INNER JOIN Intervention INTER ON ";
                General.SQL = General.SQL + " INT_R.NoIntervention = INTER.NoIntervention ";
                General.SQL = General.SQL + " WHERE INT_R.P1_AEnvoyer = '1'";
                General.SQL = General.SQL + " AND COALESCE(INT_R.P1_Envoye, '0') = '0'";
                General.SQL = General.SQL + " AND INT_R.UtilVerrou = '" + General.gsUtilisateur + "'";
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'ERR'";
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'AN'";

                nbRecAff = General.Execute(General.SQL);

                //Envoi des interventions marquées comme envoyées mais absentes du P1
                //Envoi des relevés à partir du tableau saisi
                General.SQL = "INSERT INTO P1_Releve (";
                General.SQL = General.SQL + " NoFichierGraphe, NumAppareil, CodeUO, CodeAffaire, CodeNumOrdre, ";
                General.SQL = General.SQL + " Saison, NoDecade, Periodedu, PeriodeAu, ";
                General.SQL = General.SQL + " CodeTransmission, CodeAnomalie, Chauffage, IndexDebut, IndexFin, NJours, ";
                General.SQL = General.SQL + " Livraison, Consommation, Estimation, Anc_IndexDebut, Anc_IndexFin,";
                General.SQL = General.SQL + " Evenement, Date_Event, Index_Event, Etat_Cuve, Type_Rel, Appro_Prevu, Facture, ";
                General.SQL = General.SQL + " CoefPTA, CoefPCS, VolConverti, NomFic, DateImport,";
                General.SQL = General.SQL + " NoIntervention, SaisieJauge, SaisiePige, DiametreCuve, dPeriodeDu, dPeriodeAu)";

                General.SQL = General.SQL + " SELECT INT_R.NoFichierGraphe, INT_R.NumAppareil, ";
                General.SQL = General.SQL + " INT_R.CodeUO, INT_R.CodeAffaire, INT_R.CodeNumOrdre, INT_R.Saison, INT_R.NoDecade, INT_R.Periodedu, ";
                General.SQL = General.SQL + " INT_R.PeriodeAu, INT_R.CodeTransmission, ";
                General.SQL = General.SQL + " INT_R.CodeAnomalie, INT_R.Chauffage, INT_R.IndexDebut, INT_R.IndexFin, INT_R.NJours, INT_R.Livraison, INT_R.Consommation, ";
                General.SQL = General.SQL + " INT_R.Estimation, INT_R.Anc_IndexDebut, INT_R.Anc_IndexFin, INT_R.Evenement,";
                General.SQL = General.SQL + " INT_R.Date_Event , INT_R.Index_Event, INT_R.Etat_Cuve, INT_R.Type_Rel, INT_R.Appro_Prevu, INT_R.Facture, INT_R.CoefPTA, ";
                General.SQL = General.SQL + " INT_R.CoefPCS, INT_R.VolConverti, INT_R.NomFic, INT_R.DateImport, INT_R.NoIntervention, ";
                General.SQL = General.SQL + " INT_R.SaisieJauge, INT_R.DiametreCuve, INT_R.SaisiePige, INT_R.dPeriodeDu, INT_R.dPeriodeAu ";
                General.SQL = General.SQL + " FROM [" + sNomBaseLisa + "].dbo.INT_Releve INT_R ";
                General.SQL = General.SQL + " INNER JOIN [" + sNomBaseLisa + "].dbo.Intervention INTER ON  ";
                General.SQL = General.SQL + " INT_R.NoIntervention = INTER.NoIntervention ";
                General.SQL = General.SQL + " WHERE INT_R.P1_AEnvoyer = '1'";

                General.SQL = General.SQL + " AND INT_R.NOINTERVENTION IN(SELECT [" + sNomBaseLisa + "].dbo.INT_Releve.NoIntervention FROM [" + sNomBaseLisa + "].dbo.INT_RELEVE ";
                General.SQL = General.SQL + " LEFT OUTER JOIN P1_Releve P1R ON [" + sNomBaseLisa + "].dbo.INT_Releve.NoIntervention = P1R.NoIntervention";
                General.SQL = General.SQL + " WHERE COALESCE([" + sNomBaseLisa + "].dbo.INT_Releve.P1_Envoye, '') = '1' AND P1R.NoIntervention IS NULL ";
                General.SQL = General.SQL + " AND [" + sNomBaseLisa + "].dbo.INT_Releve.UtilVerrou = '" + General.gsUtilisateur + "') ";
                //    SQL = SQL & " AND INT_R.UtilVerrou = '" & gsUtilisateur & "'"
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'ERR'";
                General.SQL = General.SQL + " AND INTER.CodeEtat <> 'AN'";

                //ModP1.adoP1.Execute(General.SQL, nbRecAff, ADODB.ExecuteOptionEnum.adExecuteNoRecords);
                //TODO il faut changer ces lignes de codes par General.Execute(General.SQL, ModP1.adoP1);
                cmd = new SqlCommand(General.SQL, ModP1.adoP1);
                if (ModP1.adoP1.State != ConnectionState.Open)
                    ModP1.adoP1.Open();
                nbRecAff = cmd.ExecuteNonQuery();

                nbRecMAJ = nbRecMAJ + nbRecAff;

                if (nbRecMAJ > 0)
                {
                    //Suite au transfert des relevés vers l'outil de suivi des consommations, marquage des interventions concernées au statut P1
                    General.SQL = "UPDATE INTER SET ";
                    General.SQL = General.SQL + " CodeEtat = 'P1'";
                    General.SQL = General.SQL + " FROM INT_Releve INT_R ";
                    General.SQL = General.SQL + " INNER JOIN Intervention INTER ON ";
                    General.SQL = General.SQL + " INT_R.NoIntervention = INTER.NoIntervention ";
                    General.SQL = General.SQL + " WHERE INT_R.Etat = 'RP1'";
                    General.SQL = General.SQL + " AND COALESCE(INT_R.P1_Envoye, '0') = '1'";
                    General.SQL = General.SQL + " AND INT_R.UtilVerrou = '" + General.gsUtilisateur + "'";
                    General.SQL = General.SQL + " AND INTER.CodeEtat <> 'ERR'";
                    General.SQL = General.SQL + " AND INTER.CodeEtat <> 'AN'";

                    General.Execute(General.SQL);

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Relevés transférés.", "Transfert des relevés", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fc_Select();
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun relevé n'a été transféré.", "Transfert des relevés", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "Feuille : UserDocSuiviReleves;fc_SendInterP1;");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMajIndexFin_Click(object sender, EventArgs e)
        {
            ModPDA.fc_MajIndexFin();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheClient_Click(object sender, EventArgs e)
        {
            try
            {
                string requete;
                string where_order = "";

                requete = "SELECT Code1 as \"Code client\", " + " Nom AS \"Raison sociale\", " + " Adresse1 as \"adresse\", Ville as \"Ville\"," + " CodePostal as \"Code postal\"" + " FROM Table1 ";
                SearchTemplate fg = new SearchTemplate(null, null, requete, where_order, "") { Text = "Recherche d'un client" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    cmbCodeClient.Value = fg.ugResultat.ActiveRow.GetCellValue("Code client").ToString();
                    lblNomClient.Text = fg.ugResultat.ActiveRow.GetCellValue("Raison sociale").ToString();
                    fg.Dispose();
                    fg.Close();
                };
                fg.ugResultat.KeyPress += (se, ev) =>
                {
                    if (Convert.ToInt32(ev.KeyChar) == 13 && fg.ugResultat.ActiveRow != null)
                    {
                        cmbCodeClient.Value = fg.ugResultat.ActiveRow.GetCellValue("Code client").ToString();
                        lblNomClient.Text = fg.ugResultat.ActiveRow.GetCellValue("Raison sociale").ToString();

                        fg.Dispose();
                        fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_PDA_Click_1(object sender, EventArgs e)
        {
            try
            {
                string requete;
                string where_order = "";

                requete = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";

                SearchTemplate fg = new SearchTemplate(null, null, requete, where_order, "") { Text = "Recherche d'un chantier" };
                fg.SetValues(new Dictionary<string, string>() { { "Code1", cmbCodeClient.Text } });
                //fg.Champs4.Text = cmbCodeClient.Text;
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    cmbCodeimmeuble.Value = fg.ugResultat.ActiveRow.GetCellValue("Code Immeuble").ToString();

                    fg.Dispose();
                    fg.Close();
                };
                fg.ugResultat.KeyPress += (se, ev) =>
                {
                    if (Convert.ToInt32(ev.KeyChar) == 13 && fg.ugResultat.ActiveRow != null)
                    {
                        cmbCodeimmeuble.Value = fg.ugResultat.ActiveRow.GetCellValue("Code Immeuble").ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_PDA_Click;");
                return;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSaisonCalcMoins_Click_1(object sender, EventArgs e)
        {
            string sDateDu = null;
            string sDateAu = null;

            sDateDu = Convert.ToString(Convert.ToDateTime(txtSaisonDu.Text).AddYears(-1));
            txtSaisonDu.Text = sDateDu;
            sDateAu = Convert.ToString(Convert.ToDateTime(txtSaisonAu.Text).AddYears(-1));
            txtSaisonAu.Text = sDateAu;

            if (Convert.ToDateTime(sDateDu).Year == Convert.ToDateTime(sDateAu).Year)
            {
                txtSaison.Text = Convert.ToDateTime(sDateDu).ToString("yyyy");
            }
            else
            {
                txtSaison.Text = Convert.ToDateTime(sDateDu).ToString("yyyy") + "/" + Convert.ToDateTime(sDateAu).ToString("yyyy");
            }

            fc_ChargeComboSem((txtSaisonDu.Text), (txtSaisonAu.Text));
            fc_ChargeComboMois((txtSaisonDu.Text), (txtSaisonAu.Text));

            cmbSemaine_AfterCloseUp(cmbSemaine, new System.EventArgs());
            cmbMois_AfterCloseUp(cmbMois, new System.EventArgs());
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSaisonCalcPlus_Click(object sender, EventArgs e)
        {
            string sDateDu = null;
            string sDateAu = null;

            sDateDu = Convert.ToString(Convert.ToDateTime(txtSaisonDu.Text).AddYears(1));
            txtSaisonDu.Text = sDateDu;
            sDateAu = Convert.ToString(Convert.ToDateTime(txtSaisonAu.Text).AddYears(1));
            txtSaisonAu.Text = sDateAu;

            if (Convert.ToDateTime(sDateDu).Year == Convert.ToDateTime(sDateAu).Year)
            {
                txtSaison.Text = Convert.ToDateTime(sDateDu).ToString("yyyy");
            }
            else
            {
                txtSaison.Text = Convert.ToDateTime(sDateDu).ToString("yyyy") + "/" +
                                 Convert.ToDateTime(sDateAu).ToString("yyyy");
            }
            fc_ChargeComboMois((txtSaisonDu.Text), (txtSaisonAu.Text));
            fc_ChargeComboSem((txtSaisonDu.Text), (txtSaisonAu.Text));

            cmbSemaine_AfterCloseUp(cmbSemaine, new System.EventArgs());
            cmbMois_AfterCloseUp(cmbMois, new System.EventArgs());
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            //GridInterventions.CtlUpdate();
            GridInterventions.UpdateData();
        }



        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocSuiviRelevesV2_Load_1(object sender, EventArgs e)
        {
            string sDateDebutSaison = null;
            string sDateFinSaison = null;
            string sNumMois = null;
            string sNumSem = null;
            object objcontrole = null;

            //foreach (object objcontrole_loopVariable in this.Controls)
            //{
            //    objcontrole = objcontrole_loopVariable;
            //    if (objcontrole is AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid)
            //    {
            //        sheridan.fc_LoadDimensionGrille( this.Name,  objcontrole);
            //    }
            //}
            //loopControlGrid(this,true);

            if (General.gsUtilisateur == "Ludovic")
            {
                cmdMajIndexFin.Visible = true;
            }
            else
            {
                cmdMajIndexFin.Visible = false;
            }

            General.fc_chargeStyleColorInter(GridInterventions);

            //Relevé validé et envoyé vers le P1
            //GridInterventions.StyleSets["P1"].BackColor = 0xc0ffc0;
            //GridInterventions.StyleSets["P1"].ForeColor = 0x0;

            ////Relevé verrouillé par un autre utilisateur
            //GridInterventions.StyleSets["VERROU"].BackColor = 0x80c0ff;
            //GridInterventions.StyleSets["VERROU"].ForeColor = 0x0;

            ////Couleur chauffage éteint
            //GridInterventions.StyleSets["LOCK"].BackColor = 0xe0e0e0;
            //GridInterventions.StyleSets["LOCK"].ForeColor = 0x0;

            ////Couleur chauffage éteint
            //GridInterventions.StyleSets["UNLOCK"].BackColor = 0xffffff;
            //GridInterventions.StyleSets["UNLOCK"].ForeColor = 0x0;

            this.ParentForm.FormClosing += (se, ev) =>
            {
                General.Execute("UPDATE INT_Releve SET UtilVerrou = Null WHERE UtilVerrou = '" + StdSQLchaine.gFr_DoublerQuote(General.gsUtilisateur) + "'");

                //foreach (Control objcontrole_loopVariable in this.Controls)
                //{
                //    objcontrole = objcontrole_loopVariable;
                //    if (objcontrole is  AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid)
                //    {
                //        sheridan.fc_SavDimensionGrille(this.Name, objcontrole);
                //    }
                //}
                //loopControlGrid(this);

                ModMajPDA.fc_SaveParamForm(this, this);
                ModAdo.fc_CloseRecordset(rsCompteurs);
            };

            General.fc_GetParamForm(this, this);

            sNumMois = cmbMois.Text;
            sNumSem = cmbSemaine.Text;

            if (DateTime.Today.Month <= Convert.ToInt16(General.nz(General.sMoisDebutSaison, "07")))
            {
                sDateDebutSaison = "01/" + General.sMoisDebutSaison + "/" + (DateTime.Today.Year - 1);
                sDateFinSaison = Convert.ToString(Convert.ToDateTime(sDateDebutSaison).AddYears(1));
                sDateFinSaison = Convert.ToString(Convert.ToDateTime(sDateFinSaison).AddDays(-1));
            }
            else
            {
                sDateDebutSaison = "01/" + General.sMoisDebutSaison + "/" + (DateTime.Today.Year);
                sDateFinSaison = Convert.ToString(Convert.ToDateTime(sDateDebutSaison).AddYears(1));
                sDateFinSaison = Convert.ToString(Convert.ToDateTime(sDateFinSaison).AddDays(-1));
            }

            if (Convert.ToDateTime(sDateDebutSaison).Year == Convert.ToDateTime(sDateFinSaison).Year)
            {
                txtSaison_EnCours.Text = Convert.ToDateTime(sDateDebutSaison).ToString("yyyy");
            }
            else
            {
                txtSaison_EnCours.Text = Convert.ToDateTime(sDateDebutSaison).ToString("yyyy") + "/" + Convert.ToDateTime(sDateFinSaison).ToString("yyyy");
            }

            if (string.IsNullOrEmpty(txtSaison.Text) || string.IsNullOrEmpty(txtSaisonDu.Text) || string.IsNullOrEmpty(txtSaisonAu.Text))
            {
                if (Convert.ToDateTime(sDateDebutSaison).Year == Convert.ToDateTime(sDateFinSaison).Year)
                {
                    txtSaison.Text = Convert.ToDateTime(sDateDebutSaison).ToString("yyyy");
                }
                else
                {
                    txtSaison.Text = Convert.ToDateTime(sDateDebutSaison).ToString("yyyy") + "/" + Convert.ToDateTime(sDateFinSaison).ToString("yyyy");
                }
                txtSaisonDu.Text = sDateDebutSaison;
                txtSaisonAu.Text = sDateFinSaison;
            }

            fc_ChargeComboSem((txtSaisonDu.Text), (txtSaisonAu.Text));
            cmbSemaine.Text = "";
            cmbSemaine.Text = sNumSem;
            fc_ChargeComboMois((txtSaisonDu.Text), (txtSaisonAu.Text));
            cmbMois.Text = "";
            cmbMois.Text = sNumMois;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sDateDu"></param>
        /// <param name="sDateAu"></param>
        /// <returns></returns>
        private bool fc_ChargeComboSem(string sDateDu, string sDateAu)
        {
            bool functionReturnValue = false;
            tpNumSemaine[] tabSem = null;
            int i = 0;

            try
            {
                functionReturnValue = true;

                if (!General.IsDate(sDateDu) && !General.IsDate(sDateAu))
                    return functionReturnValue;

                if (cmbSemaine.Rows.Count > 0)
                    //cmbSemaine.RemoveAll();
                    cmbSemaine.DataSource = null;

                tabSem = fc_NumSemaine(sDateDu, sDateAu);
                DataTable dt = new DataTable();
                dt.Columns.Add("N° semaine");
                dt.Columns.Add("date debut");
                dt.Columns.Add("date fin");
                if (tabSem.Length > 0)
                {
                    for (i = 0; i <= tabSem.Length - 1; i++)
                    {
                        dt.Rows.Add(tabSem[i].sNumSemaine, Convert.ToDateTime(tabSem[i].sDateDu).ToString("dd/MM/yyyy"), Convert.ToDateTime(tabSem[i].sDateAu).ToString("dd/MM/yyyy"));
                        //cmbSemaine.AddItem( + Constants.vbTab + string.Format(tabSem[i].sDateDu, ) + Constants.vbTab + string.Format(tabSem[i].sDateAu, "dd/mm/yyyy"));
                    }
                    cmbSemaine.DataSource = dt;
                }
                return functionReturnValue;

            }
            catch (Exception e)
            {

                Erreurs.gFr_debug(e, this.Name + ";fc_ChargeComboSem;");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sDateDu"></param>
        /// <param name="sDateAu"></param>
        /// <returns></returns>
        private tpNumSemaine[] fc_NumSemaine(string sDateDu, string sDateAu)
        {

            tpNumSemaine[] tabSem = null;
            int i = 0;
            int j = 0;
            int k = 0;
            string sDateTemp = null;
            string sNumSemaine = null;

            tabSem = new tpNumSemaine[1];

            sNumSemaine = "";

            if (General.IsDate(sDateDu) && General.IsDate(sDateAu))
            {
                sDateTemp = sDateDu;
                for (i = 0; i < (Convert.ToDateTime(sDateAu) - Convert.ToDateTime(sDateDu)).TotalDays; i++)
                {
                    if (sNumSemaine != CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(Convert.ToDateTime(sDateTemp), CalendarWeekRule.FirstDay,
                                DayOfWeek.Monday).ToString())
                    {

                        if (tabSem.Length <= k)
                        {
                            Array.Resize(ref tabSem, k + 1);
                        }
                        sNumSemaine =
                            CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(Convert.ToDateTime(sDateTemp), CalendarWeekRule.FirstDay,
                                DayOfWeek.Monday).ToString();
                        //sNumSemaine = Convert.ToString(Convert.ToDateTime(sDateTemp).DayOfWeek);

                        tabSem[k].sDateDu = Convert.ToString(Convert.ToDateTime(sDateTemp).ToString("dd/MM/yyyy"));
                        tabSem[k].sNumSemaine = sNumSemaine;

                        if (k - 1 >= 0)
                        {
                            tabSem[k - 1].sDateAu = Convert.ToString(Convert.ToDateTime(sDateTemp).AddDays(-1).ToString("dd/MM/yyyy"));
                        }

                        k = k + 1;

                    }
                    sDateTemp = Convert.ToString(Convert.ToDateTime(sDateTemp).AddDays(1));
                }
            }

            return tabSem;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sDateDu"></param>
        /// <param name="sDateAu"></param>
        /// <returns></returns>
        private bool fc_ChargeComboMois(string sDateDu, string sDateAu)
        {
            bool functionReturnValue = false;
            tpMois[] tabMois = null;
            int i = 0;

            try
            {

                functionReturnValue = true;

                if (!General.IsDate(sDateDu) && !General.IsDate(sDateAu))
                    return functionReturnValue;

                if (cmbMois.Rows.Count > 0)
                    //cmbMois.RemoveAll();
                    cmbMois.DataSource = null;

                tabMois = fc_NumMois(sDateDu, sDateAu);
                DataTable dt = new DataTable();
                dt.Columns.Add("N° Mois");
                dt.Columns.Add("Libelle");
                dt.Columns.Add("Date Du");
                dt.Columns.Add("Date Au");
                if (tabMois.Length > 0)
                {
                    for (i = 0; i <= tabMois.Length - 1; i++)
                    {
                        dt.Rows.Add(tabMois[i].sNumMois, tabMois[i].sLibelle, tabMois[i].sDateDu, tabMois[i].sDateAu);
                    }
                    cmbMois.DataSource = dt;
                }
                return functionReturnValue;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ChargeComboMois;");
                return functionReturnValue;
            }


        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sDateDu"></param>
        /// <param name="sDateAu"></param>
        /// <returns></returns>
        private tpMois[] fc_NumMois(string sDateDu, string sDateAu)
        {

            tpMois[] tabMois = null;
            int i = 0;
            int j = 0;
            int k = 0;
            string sDateTemp = null;
            string sNumMois = null;

            tabMois = new tpMois[1];

            sNumMois = "";
            k = 0;

            if (General.IsDate(sDateDu) && General.IsDate(sDateAu))
            {
                sDateTemp = sDateDu;
                for (i = 0; i <= (((Convert.ToDateTime(sDateAu).Year - Convert.ToDateTime(sDateDu).Year) * 12) + Convert.ToDateTime(sDateAu).Month - Convert.ToDateTime(sDateDu).Month); i++)
                {
                    if (sNumMois != Convert.ToString(Convert.ToDateTime(sDateTemp).Month))
                    {
                        if (tabMois.Length <= k)
                        {
                            Array.Resize(ref tabMois, k + 1);
                        }
                        sNumMois = Convert.ToString(Convert.ToDateTime(sDateTemp).Month);
                        tabMois[k].sDateDu = Convert.ToDateTime(sDateTemp).ToString("dd/MM/yyyy");
                        tabMois[k].sDateAu = fDate.fc_FinDeMois(Convert.ToString(Convert.ToDateTime(sDateTemp).Month), Convert.ToString(Convert.ToDateTime(sDateTemp).Year)).ToString("dd/MM/yyyy");
                        tabMois[k].sNumMois = sNumMois;
                        tabMois[k].sLibelle = Convert.ToDateTime(sDateTemp).ToString("MMMM");
                        k = k + 1;
                    }
                    sDateTemp = Convert.ToString(Convert.ToDateTime(sDateTemp).AddMonths(1));
                }
            }
            return tabMois;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {

            string sCode = null;

            if (GridInterventions.ActiveRow.Cells["Verrouillé par"].Text != General.gsUtilisateur + "")//TESTED
            {
                GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value = GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value;
            }

            if (GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Index début"].Column.Index)
            {
                if (GridInterventions.ActiveRow.Cells["Evènement"].Text != "3")
                {
                    if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Index début"].Value.ToString()))
                    {
                        GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value = GridInterventions.ActiveRow.Cells["Index début"].Value;
                    }
                }
            }


            //Relevé envoyé vers le P1 : annulation de toutes modifications
            if (GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Value.ToString() == "1" || GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Value.ToString().ToLower() == "true")//TESTED
            {
                if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Text) && GridInterventions.ActiveCell.Column.Index != GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Column.Index)
                    GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value = GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value;
            }
            //if(GridInterventions.DisplayLayout.Bands[0].Columns[GridInterventions.ActiveCell.Column.Index].Style==ColumnStyle.CheckBox)
            if (GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Column.Style == ColumnStyle.CheckBox)//TESTED
            {
                if (GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Text == "-1" || GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Text.ToLower() == "true")
                {
                    GridInterventions.ActiveRow.Cells[GridInterventions.ActiveCell.Column.Index].Value = true;
                }
            }

            if (GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Column.Index)//TESTED
            {
                if (General.nz((GridInterventions.ActiveRow.Cells["OrigineP1"].Text), "0").ToString() != "0")
                {
                    GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Value = GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Value;
                }
                else
                {
                    if (GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Text == "-1")
                    {
                        GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Value = true;
                    }

                    if (GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Text == "1" || GridInterventions.ActiveRow.Cells["Envoyé vers le P1"].Text.ToLower() == "true")//tested
                    {
                        if (GridInterventions.ActiveRow.Cells["Etat"].Text != "RP1")
                        {
                            GridInterventions.ActiveRow.Cells["Etat"].Value = "RP1";
                        }
                    }
                }
            }


            if (GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Evènement"].Column.Index)//TESTED
            {
                var ModAdo = new ModAdo();
                GridInterventions.ActiveRow.Cells["Désignation"].Value = ModAdo.fc_ADOlibelle("SELECT Libelle FROM TypeAnomalie WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(GridInterventions.ActiveRow.Cells["Evènement"].Text) + "'");
                if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Evènement"].Text))//TESTED
                {
                    sCode = ModAdo.fc_ADOlibelle("SELECT Code FROM TypeAnomalie WHERE Code = '" + StdSQLchaine.gFr_DoublerQuote(GridInterventions.ActiveRow.Cells["Evènement"].Text) + "'");
                    if (string.IsNullOrEmpty(sCode))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un évènement existant.", "Code évènement inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }

                    //Allumage
                    if (GridInterventions.ActiveRow.Cells["Evènement"].Text == "1")//TESTED
                    {
                        GridInterventions.ActiveRow.Cells["Chauffage"].Value = "1";
                        //Arrêt
                    }
                    else if (GridInterventions.ActiveRow.Cells["Evènement"].Text == "2")
                    {
                        GridInterventions.ActiveRow.Cells["Chauffage"].Value = "0";
                    }

                }

            }

            if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Index début"].Text))//tested
            {
                if (!General.IsNumeric(GridInterventions.ActiveRow.Cells["Index début"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique dans le champ \"Index début\".", "Saisie incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Index fin"].Text))//tested
            {
                if (!General.IsNumeric(GridInterventions.ActiveRow.Cells["Index fin"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique dans le champ \"Index fin\".", "Saisie incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Ancien Index début"].Text))//tested
            {
                if (!General.IsNumeric(GridInterventions.ActiveRow.Cells["Ancien Index début"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique dans le champ \"Ancien Index début\".", "Saisie incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

            if (!string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Ancien Index fin"].Text))//tested
            {
                if (!General.IsNumeric(GridInterventions.ActiveRow.Cells["Ancien Index fin"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique dans le champ \"Ancien Index fin\".", "Saisie incorrecte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }

            if (GridInterventions.ActiveRow.Cells["Verrouillé par"].Text == General.gsUtilisateur + "")
            {
                var LOCKBackColor = ColorTranslator.FromOle(0xE0E0E0); /*Color.FromArgb(224, 224, 224);*/
                var LOCKForeColor = ColorTranslator.FromOle(0x0);/* Color.Black;*/
                var UNLOCKBackColor = Color.White;
                var UNLOCKForeColor = Color.Black;
                //Changement de compteur
                if (GridInterventions.ActiveRow.Cells["Evènement"].Text != "3")
                {
                    GridInterventions.ActiveRow.Cells["Index début"].Appearance.BackColor = LOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index début"].Appearance.BackColor = LOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index fin"].Appearance.BackColor = LOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Index début"].Appearance.ForeColor = LOCKForeColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index début"].Appearance.ForeColor = LOCKForeColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index fin"].Appearance.ForeColor = LOCKForeColor;
                }
                else
                {
                    GridInterventions.ActiveRow.Cells["Index début"].Appearance.BackColor = UNLOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index début"].Appearance.BackColor = UNLOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index fin"].Appearance.BackColor = UNLOCKBackColor;
                    GridInterventions.ActiveRow.Cells["Index début"].Appearance.ForeColor = UNLOCKForeColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index début"].Appearance.ForeColor = UNLOCKForeColor;
                    GridInterventions.ActiveRow.Cells["Ancien Index fin"].Appearance.ForeColor = UNLOCKForeColor;

                }
            }

            fc_CalcConso(Convert.ToInt16(GridInterventions.ActiveCell.Column.Index));

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="ColIndex"></param>
        private void fc_CalcConso(short ColIndex)
        {
            double ConsoPige = 0;
            double ConsoJauge = 0;
            double ConsoTotale = 0;
            double dblSurface = 0;

            var _with10 = GridInterventions;
            if (GridInterventions.ActiveRow != null)
            {
                if (ColIndex == GridInterventions.ActiveRow.Cells["Jauge"].Column.Index)
                {
                    if (General.nz((GridInterventions.ActiveRow.Cells["Pige"].Text), "0").ToString() == "0")
                    {
                        GridInterventions.ActiveRow.Cells["Index fin"].Value = GridInterventions.ActiveRow.Cells["Jauge"].Text;
                    }
                }

                if (ColIndex == GridInterventions.ActiveRow.Cells["Pige"].Column.Index)
                {
                    if (string.IsNullOrEmpty(GridInterventions.ActiveRow.Cells["Diamètre Cuve"].Text))
                    {
                        var ModAdo = new ModAdo();
                        GridInterventions.ActiveRow.Cells["Diamètre Cuve"].Value = ModAdo.fc_ADOlibelle("SELECT DiametreCuve FROM Imm_Appareils WHERE NumAppareil = " + General.nz((GridInterventions.ActiveRow.Cells["NumAppareil"].Text), "0") + "");
                    }
                    if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Pige"].Text))
                    {
                        ConsoPige = ModPDA.fc_GetVolumePige(Convert.ToDouble(GridInterventions.ActiveRow.Cells["Pige"].Text), General.nz((GridInterventions.ActiveRow.Cells["NumAppareil"].Text), "0").ToString());

                        if (ConsoPige != -1)
                        {
                            GridInterventions.ActiveRow.Cells["Index Fin"].Value = Convert.ToString(ConsoPige);
                        }
                        else
                        {
                            GridInterventions.ActiveRow.Cells["Index Fin"].Value = GridInterventions.ActiveRow.Cells["Jauge"].Text;
                        }
                    }
                    else
                    {
                        if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Jauge"].Text))
                        {
                            GridInterventions.ActiveRow.Cells["Index fin"].Value = GridInterventions.ActiveRow.Cells["Jauge"].Text;
                        }
                    }
                    if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Index Fin"].Text))
                    {
                        GridInterventions.ActiveRow.Cells["Index Fin"].Value = Convert.ToString(General.FncArrondir(Convert.ToDouble(GridInterventions.ActiveRow.Cells["Index Fin"].Text), 3));
                    }

                }

                ConsoPige = 0;
                ConsoJauge = 0;

                if (GridInterventions.ActiveRow.Cells["chkLivr"].Text == "1")
                {
                    if (ColIndex == GridInterventions.ActiveRow.Cells["Pige"].Column.Index || ColIndex == GridInterventions.ActiveRow.Cells["Jauge"].Column.Index)
                    {
                        //Contrôle de la cohérence de saisie entre la pige et la jauge
                        if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Jauge"].Text))
                        {
                            ConsoJauge = Convert.ToDouble(GridInterventions.ActiveRow.Cells["Jauge"].Text);
                        }

                        if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Pige"].Text))
                        {
                            ConsoPige = ModPDA.fc_GetVolumePige(Convert.ToDouble(GridInterventions.ActiveRow.Cells["Pige"].Text), General.nz((GridInterventions.ActiveRow.Cells["NumAppareil"].Text), "0").ToString());
                        }

                        if (ConsoPige != -1 & ConsoPige != 0 & ConsoJauge != 0)
                        {
                            if (ConsoJauge != ConsoPige)
                            {
                                if (System.Math.Abs((ConsoJauge - ConsoPige) / ConsoJauge * 100) > Convert.ToInt16(General.sEcartSaisieFOD))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, un écart de " + string.Format(System.Math.Abs((ConsoJauge - ConsoPige) / ConsoJauge * 100).ToString(), "0.00") + " % existe entre la saisie de la pige et la saisie de la jauge.", "Ecart important de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                            }
                        }
                    }
                }

                ConsoTotale = 0;

                if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Index début"].Text) && General.IsNumeric(GridInterventions.ActiveRow.Cells["Index fin"].Text))
                {
                    if (GridInterventions.ActiveRow.Cells["chkLivr"].Text == "1")
                    {
                        ConsoTotale = ConsoTotale + Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Index début"].Text), 0)) - Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Index fin"].Text), 0));
                    }
                    else//TESTED
                    {
                        ConsoTotale = ConsoTotale + Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Index fin"].Text), 0)) - Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Index début"].Text), 0));
                    }
                }

                if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Ancien Index début"].Text) && General.IsNumeric(GridInterventions.ActiveRow.Cells["Ancien Index fin"].Text))
                {
                    if (GridInterventions.ActiveRow.Cells["chkLivr"].Text == "1")
                    {
                        ConsoTotale = ConsoTotale + Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Ancien Index début"].Text), 0)) - Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Ancien Index fin"].Text), 0));
                    }
                    else//tested
                    {
                        ConsoTotale = ConsoTotale + Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Ancien Index fin"].Text), 0)) - Convert.ToDouble(General.nz((GridInterventions.ActiveRow.Cells["Ancien Index début"].Text), 0));
                    }
                }

                if (General.IsNumeric(GridInterventions.ActiveRow.Cells["Livraison"].Text))//TESTED
                {
                    ConsoTotale = ConsoTotale + Convert.ToDouble(GridInterventions.ActiveRow.Cells["Livraison"].Text);
                }

                GridInterventions.ActiveRow.Cells["Conso"].Value = Convert.ToString(ConsoTotale);

            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optAllumage_MouseUp(object sender, MouseEventArgs e)
        {
            fc_Select();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optArret_MouseUp(object sender, MouseEventArgs e)
        {
            fc_Select();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optReleveStandard_MouseUp(object sender, MouseEventArgs e)
        {
            fc_Select();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDropEvenement_AfterCloseUp(object sender, EventArgs e)
        {
            if (ssDropEvenement.ActiveRow != null)
                GridInterventions.ActiveRow.Cells["Désignation"].Value = ssDropEvenement.ActiveRow.Cells[1].Text;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TimerLoad_Tick(object sender, EventArgs e)
        {
            TimerLoad.Enabled = false;
            fc_GetReleves();
        }

        private void txtSaison_TextChanged(object sender, EventArgs e)
        {
            if (txtSaison.Text == txtSaison_EnCours.Text)
            {
                imgSaisonEnCours.Visible = true;
            }
            else
            {
                imgSaisonEnCours.Visible = false;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (GridInterventions.ActiveRow != null)
            {
                if (e.Row.Cells["Envoyer"].Text == "1" || e.Row.Cells["Envoyer"].Text.ToLower() == "true")
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["Evènement"].Text) && string.IsNullOrEmpty(e.Row.Cells["Index fin"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi l'index de fin : Vous ne pouvez donc pas envoyer ce relevé vers le P1.", "Envoi vers le P1 impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (e.Row.Cells["Evènement"].Text == "3")
                    {

                        if (string.IsNullOrEmpty(e.Row.Cells["Index début"].Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi l'index de début : Vous ne pouvez donc pas envoyer ce relevé vers le P1.", "Envoi vers le P1 impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }

                        if (string.IsNullOrEmpty(e.Row.Cells["Index fin"].Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi l'index de fin : Vous ne pouvez donc pas envoyer ce relevé vers le P1.", "Envoi vers le P1 impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }

                        if (string.IsNullOrEmpty(e.Row.Cells["Ancien Index début"].Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi l'ancien index de début : Vous ne pouvez donc pas envoyer ce relevé vers le P1.", "Envoi vers le P1 impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }

                        if (string.IsNullOrEmpty(e.Row.Cells["Ancien Index fin"].Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi l'ancien index de fin : Vous ne pouvez donc pas envoyer ce relevé vers le P1.", "Envoi vers le P1 impossible", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                    }
                }

                //Allumage
                if (e.Row.Cells["Evènement"].Text == "1" && e.Row.Cells["Chauffage"].Text == "1")
                {
                    General.Execute("UPDATE Intervention Set Chauffage = '1' WHERE NoIntervention = " + General.nz((e.Row.Cells["No Intervention"].Text), "0"));
                    //Arrêt
                }
                else if (e.Row.Cells["Evènement"].Text == "2" && e.Row.Cells["Chauffage"].Text == "0")
                {
                    General.Execute("UPDATE Intervention Set Chauffage = '0' WHERE NoIntervention = " + General.nz((e.Row.Cells["No Intervention"].Text), "0"));
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            string[] tabChantier = null;
            short iFld = 0;

            if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Livraison"].Column.Index)
            {
                var _with11 = new frmLivraison_Releve();
                tabChantier = e.Row.Cells["Chantier"].Text.Split('-');
                tabChantier = General.Trim(tabChantier[0]).Split('/');
                try
                {
                    _with11.txtCodeAffaire.Text = tabChantier[0];
                    _with11.txtCodeNumOrdre.Text = tabChantier[1];//todo verifier le code vb6. cette ligne lance une exception
                }
                catch (Exception ex) { Program.SaveException(ex); }
                _with11.txtdateDu.Text = e.Row.Cells["ReleveDu"].Text;
                _with11.txtDateAu.Text = e.Row.Cells["Date Relevé"].Text;
                _with11.ShowDialog();

            }
            else if (General.nz((e.Row.Cells["No Intervention"].Text), "0").ToString() != "0")
            {
                GridInterventions_AfterRowUpdate(GridInterventions, new RowEventArgs(e.Row));//todo dans le code vb6 l'evenement unboudwriteData declenche automatiquement lorsque la valeur d'une cellule est changée et appel la methode update d'object adodc.Recordet.
                var frm = new UserInter_RelevesCompteurs();
                frm.txtNoIntervention.Text = General.nz((e.Row.Cells["No Intervention"].Text), "0").ToString();
                frm.StartPosition = FormStartPosition.CenterParent;
                frm.ShowDialog();
                //var _with12 = UserInter_RelevesCompteurs;
                //_with12.txtNoIntervention.Text = General.nz(ref ref (e.Row.Cells["No Intervention"].Text), ref ref "0");
                //_with12.ShowDialog();
                //UserInter_RelevesCompteurs.Close();
                //        Else
                //            SaveSetting cFrNomApp, "frmInterPlanning", "Evenement", fc_ADOlibelle("SELECT Intervention.NumFicheStandard FROM Intervention WHERE Intervention.NoIntervention=" & nz(e.Row.Cells("No Intervention").Text, "0"))
                //            SaveSetting cFrNomApp, "frmInterPlanning", "Intervention", nz(e.Row.Cells("No Intervention").Text, "0")
                //'            frmInterPlanning.Show vbModal
                //        End If
                //rsCompteurs.Requery();//
                //GridInterventions.ReBind();//
                ModAdoCompteurs = new ModAdo();
                rsCompteurs = ModAdoCompteurs.fc_OpenRecordSet(SSQL);
                rsCompteurs.PrimaryKey = new DataColumn[] { rsCompteurs.Columns["NoAuto"] };
                GridInterventions.DataSource = rsCompteurs;
                GridInterventions.UpdateData();
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (GridInterventions.ActiveRow != null && GridInterventions.ActiveCell != null)
            {
                if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Ancien Index début"].Column.Index)
                {
                    if (GridInterventions.ActiveRow.Cells["Evènement"].Text != "3")
                    {
                        e.KeyChar = (char)0;
                    }
                }

                if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Ancien Index fin"].Column.Index)
                {
                    if (GridInterventions.ActiveRow.Cells["Evènement"].Text != "3")
                    {
                        e.KeyChar = (char)0;
                    }
                }

                if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Jauge"].Column.Index)
                {
                    if (GridInterventions.ActiveRow.Cells["chkLivr"].Text != "1")
                    {
                        GridInterventions.ActiveRow.Cells["Jauge"].Activation = Activation.NoEdit;
                        e.KeyChar = (char)0;
                    }
                    else
                    {
                        GridInterventions.ActiveRow.Cells["Jauge"].Activation = Activation.AllowEdit;
                    }
                }

                if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Pige"].Column.Index)
                {
                    if (GridInterventions.ActiveRow.Cells["chkLivr"].Text != "1")
                    {
                        GridInterventions.ActiveRow.Cells["Pige"].Activation = Activation.NoEdit;
                        e.KeyChar = (char)0;
                    }
                    else
                    {
                        GridInterventions.ActiveRow.Cells["Pige"].Activation = Activation.NoEdit;
                    }
                }

                if (GridInterventions.ActiveRow.Cells["ChkLivr"].Text == "1")
                {

                    if (GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Livraison"].Column.Index)
                    {
                        e.KeyChar = (char)0;
                    }

                    if (GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Index Fin"].Column.Index)
                    {
                        e.KeyChar = (char)0;
                    }

                }
                else
                {

                    if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Livraison"].Column.Index)
                    {
                        e.KeyChar = (char)0;
                    }
                    if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Jauge"].Column.Index)
                    {
                        e.KeyChar = (char)0;
                    }

                    if (GridInterventions.ActiveCell != null && GridInterventions.ActiveCell.Column.Index == GridInterventions.ActiveRow.Cells["Pige"].Column.Index)
                    {
                        e.KeyChar = (char)0;
                    }
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_Leave_1(object sender, EventArgs e)
        {

            //GridInterventions.CtlUpdate();
            GridInterventions.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// <param name="sen
        /// </summary>der"></param>
        /// <param name="e"></param>
        private void GridInterventions_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            short i = 0;
            string sColor = null;


            if (e.ReInitialize && !e.Row.DataChanged)
                return;

            if ((rsCompteurs != null))
            {
                if (e.Row.Cells.Count == rsCompteurs.Columns.Count)
                {
                    for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                    {
                        //if (GridInterventions.DisplayLayout.Bands[0].Columns[i].CellActivation == Activation.NoEdit)
                        if (e.Row.Cells[i].Column.CellActivation == Activation.NoEdit)
                        {
                            //GridInterventions.DisplayLayout.Bands[0].Columns[i].CellAppearance.BackColor = LOCKBackColor;
                            //GridInterventions.DisplayLayout.Bands[0].Columns[i].CellAppearance.ForeColor = LOCKForeColor;
                            e.Row.Cells[i].Appearance.BackColor = LOCKBackColor;
                            e.Row.Cells[i].Appearance.ForeColor = LOCKForeColor;
                        }
                        else
                        {
                            //GridInterventions.DisplayLayout.Bands[0].Columns[i].CellAppearance.BackColor = UNLOCKBackColor;
                            //GridInterventions.DisplayLayout.Bands[0].Columns[i].CellAppearance.ForeColor = UNLOCKForeColor;
                            e.Row.Cells[i].Appearance.BackColor = UNLOCKBackColor;
                            e.Row.Cells[i].Appearance.ForeColor = UNLOCKForeColor;
                        }

                        if (e.Row.Cells[i].Column.Key == "Index début" ||
                            e.Row.Cells[i].Column.Key == "Livraison" ||
                            e.Row.Cells[i].Column.Key == "Jauge" ||
                            e.Row.Cells[i].Column.Key == "Index fin" ||
                            e.Row.Cells[i].Column.Key == "Ancien Index début" ||
                            e.Row.Cells[i].Column.Key == "Ancien Index fin" ||
                            e.Row.Cells[i].Column.Key == "Conso")
                        {
                            //e.Row.Cells[i].Value = string.Format(e.Row.Cells[i].Text, "# ##0");
                            //GridInterventions.Columns[i].Text = Microsoft.VisualBasic.Compatibility.VB6.Support.Format(GridInterventions.Columns[i].Text, "# ##0");//TODO
                        }

                    }

                    //Changement de compteur
                    if (e.Row.Cells["Evènement"].Text != "3")
                    {
                        e.Row.Cells["Index début"].Appearance.BackColor = LOCKBackColor;
                        e.Row.Cells["Ancien Index début"].Appearance.BackColor = LOCKBackColor;
                        e.Row.Cells["Ancien Index fin"].Appearance.BackColor = LOCKBackColor;
                        e.Row.Cells["Index début"].Appearance.ForeColor = LOCKForeColor;
                        e.Row.Cells["Ancien Index début"].Appearance.ForeColor = LOCKForeColor;
                        e.Row.Cells["Ancien Index fin"].Appearance.ForeColor = LOCKForeColor;
                    }
                    else
                    {
                        e.Row.Cells["Index début"].Appearance.BackColor = UNLOCKBackColor;
                        e.Row.Cells["Ancien Index début"].Appearance.BackColor = UNLOCKBackColor;
                        e.Row.Cells["Ancien Index fin"].Appearance.BackColor = UNLOCKBackColor;
                        e.Row.Cells["Index début"].Appearance.ForeColor = UNLOCKForeColor;
                        e.Row.Cells["Ancien Index début"].Appearance.ForeColor = UNLOCKForeColor;
                        e.Row.Cells["Ancien Index fin"].Appearance.ForeColor = UNLOCKForeColor;
                    }

                    if (e.Row.Cells["Verrouillé par"].Text != General.gsUtilisateur + "")
                    {
                        for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                        {
                            e.Row.Cells[i].Appearance.BackColor = VERROUBackColor;
                            e.Row.Cells[i].Appearance.ForeColor = VERROUForeColor;
                        }
                    }

                    if (e.Row.Cells["Envoyé vers le P1"].Text == "1" || e.Row.Cells["Envoyé vers le P1"].Text.ToLower() == "true")
                    {
                        for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                        {
                            e.Row.Cells[i].Appearance.BackColor = p1BackColor;
                            e.Row.Cells[i].Appearance.ForeColor = p1ForeColor;
                        }
                    }

                    if (General.IsDate(e.Row.Cells["Période Du"].Text))
                    {
                        e.Row.Cells["Période Du"].Value = Convert.ToDateTime(e.Row.Cells["Période Du"].Text).ToString("MM/yyyy");
                    }

                    if (General.IsDate(e.Row.Cells["Période Au"].Text))
                    {
                        e.Row.Cells["Période Au"].Value = Convert.ToDateTime(e.Row.Cells["Période Au"].Text).ToString("MM/yyyy");
                    }
                    if (e.Row.Cells["Envoyé vers le P1"].Value != DBNull.Value)
                    {
                        if (e.Row.Cells["Envoyé vers le P1"].Text.ToString() == "1" || e.Row.Cells["Envoyé vers le P1"].Text.ToString().ToLower() == "true")
                            e.Row.Cells["Envoyé vers le P1"].Value = true;
                        else
                        {
                            e.Row.Cells["Envoyé vers le P1"].Value = false;
                        }
                    }
                    else
                    {
                        e.Row.Cells["Envoyé vers le P1"].Value = false;
                    }
                    if (e.Row.Cells["Envoyer"].Value != DBNull.Value)
                    {
                        if (e.Row.Cells["Envoyer"].Value.ToString() == "1" || e.Row.Cells["Envoyer"].Value.ToString().ToLower() == "true")
                            e.Row.Cells["Envoyer"].Value = true;
                        else
                        {
                            e.Row.Cells["Envoyer"].Value = false;
                        }
                    }
                    else
                    {
                        e.Row.Cells["Envoyer"].Value = false;
                    }
                }
            }
        }
        bool bfirst = true;

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocSuiviRelevesV2_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                //if (!bfirst)
                //    ModMajPDA.fc_SaveParamForm(this, this);
                return;
            }
            bfirst = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            GridInterventions.DisplayLayout.Bands[0].Columns["Envoyé vers le P1"].Style = ColumnStyle.CheckBox;
            GridInterventions.DisplayLayout.Bands[0].Columns["Envoyer"].Style = ColumnStyle.CheckBox;
            GridInterventions.DisplayLayout.Bands[0].Columns["NoAuto"].Hidden = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridInterventions_AfterRowUpdate(object sender, RowEventArgs e)
        {
            if (GridInterventions.ActiveRow != null)
            {
                try
                {
                    //foreach (DataRow dr in rsCompteurs.Rows)
                    //{
                    int i = 0;
                    var dr = rsCompteurs.Rows.Find(e.Row.Cells["NoAuto"].Value.ToString());
                    foreach (DataColumn column in rsCompteurs.Columns)
                    {
                        if (!string.IsNullOrEmpty(e.Row.Cells[i].Value.ToString()))
                        {
                            dr[i] = General.nz(e.Row.Cells[i].Value.ToString(), null);
                            if (column.ColumnName.ToString() == "Evènement")
                            {
                                //Mise à jour du champ "Evenement" de tous les relevés de compteur du même site sur le même jour (même intervention)
                                if (dr[i] != null && dr[i].ToString() == "1") //1 pour allumage
                                {
                                    General.Execute("UPDATE INT_RELEVE SET Evenement = '" + dr["Evènement"] +
                                                    "', Chauffage='1',LibelleEvent='" +
                                                    StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Désignation"].Text) +
                                                    "' WHERE NoIntervention='" + dr["No Intervention"] +
                                                    "' AND INT_Releve.NumAppareil <> '" + dr["NumAppareil"] + "'");
                                }
                                else if (dr[i] != null && dr[i].ToString() == "2") //1 pour arrêt
                                {
                                    General.Execute("UPDATE INT_RELEVE SET Evenement = '" + dr["Evènement"] +
                                                    "', Chauffage='0',LibelleEvent='" +
                                                    StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Désignation"].Text) +
                                                    "' WHERE NoIntervention='" + dr["No Intervention"] +
                                                    "' AND INT_Releve.NumAppareil <> '" + dr["NumAppareil"] + "'");
                                }
                                bGoToReleve = true;
                                General.saveInReg(this.Name, "NoIntervention", dr["No Intervention"].ToString());
                            }
                        }
                        i++;
                        //}
                    }
                    //ModAdoCompteurs.Update(); //TODO
                    string aEnvoyer = e.Row.Cells["Envoyer"].Text.ToLower() == "true" ? "1" : "0";
                    string EnvoyerVers = e.Row.Cells["Envoyé vers le P1"].Text.ToLower() == "true" ? "1" : "0";
                    string evenement = !string.IsNullOrEmpty(e.Row.Cells["Evènement"].Text) ? e.Row.Cells["Evènement"].Text : "NULL";
                    string req = "UPDATE INT_Releve set ";
                    req = req + "P1_AEnvoyer=" + aEnvoyer;
                    req = req + ",P1_Envoye=" + EnvoyerVers;
                    req = req + ",Evenement=" + evenement;
                    req = req + ",LibelleEvent='" + e.Row.Cells["Désignation"].Text + "',";
                    req = req + "IndexDebut='" + e.Row.Cells["Index début"].Text + "',";
                    req = req + "Livraison='" + e.Row.Cells["Livraison"].Text + "',";
                    req = req + "SaisieJauge='" + e.Row.Cells["Jauge"].Text + "',";
                    req = req + "SaisiePige='" + e.Row.Cells["Pige"].Text + "',";
                    req = req + "IndexFin='" + e.Row.Cells["Index fin"].Text + "',";
                    req = req + "Anc_IndexDebut='" + e.Row.Cells["Ancien Index début"].Text + "',";
                    req = req + "Anc_IndexFin='" + e.Row.Cells["Ancien Index fin"].Text + "',";
                    req = req + "Etat='" + e.Row.Cells["Etat"].Text + "',";
                    req = req + "DiametreCuve='" + e.Row.Cells["Diamètre cuve"].Text + "',";
                    req = req + "Consommation='" + e.Row.Cells["Conso"].Text + "'";
                    req = req + " Where NoAuto='" + e.Row.Cells["NoAuto"].Text + "'";
                    General.Execute(req);
                    //GridInterventions.UpdateData();
                    if (bGoToReleve == true)
                    {
                        TimerLoad.Interval = 50;
                        TimerLoad.Enabled = true;
                    }
                    return;
                }
                catch (Exception ex)
                {
                    Erreurs.gFr_debug(ex, ";GridInterventions_UnboundWriteData;");
                    return;
                }
            }

        }
    }
}
