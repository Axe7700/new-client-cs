﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.Forms
{
    public partial class frmResiliation : Form
    {
        private UserDocFicheContrat fiche = null;
        public frmResiliation()
        {
            InitializeComponent();
        }

        public frmResiliation(UserDocFicheContrat fiche) : this()
        {
            this.fiche = fiche;
        }

        private void cmdAppliquer_Click(object sender, EventArgs e)
        {
            if(chkFactureManuelle.CheckState==CheckState.Checked && chknonFacturable.CheckState == CheckState.Unchecked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez cliqué sur la case 'Facturé manuellement', vous devez aussi cocher la case 'non facturable' ", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            fiche.Check3.CheckState = chknonFacturable.CheckState;
            fiche.ChkResiliee.CheckState = ChkResiliee.CheckState;
            fiche.chkFactureManuelle.CheckState= chkFactureManuelle.CheckState;
            this.Close();

        }
    }
}
