﻿namespace Axe_interDT.Views.Contrat.Forms
{
    partial class frmResiliation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.lblContrat = new iTalk.iTalk_TextBox_Small2();
            this.txtAvenant = new iTalk.iTalk_TextBox_Small2();
            this.txtNumContrat = new iTalk.iTalk_TextBox_Small2();
            this.ChkResiliee = new System.Windows.Forms.CheckBox();
            this.chkFactureManuelle = new System.Windows.Forms.CheckBox();
            this.chknonFacturable = new System.Windows.Forms.CheckBox();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblImmeuble
            // 
            this.lblImmeuble.AccAcceptNumbersOnly = false;
            this.lblImmeuble.AccAllowComma = false;
            this.lblImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.lblImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblImmeuble.AccHidenValue = "";
            this.lblImmeuble.AccNotAllowedChars = null;
            this.lblImmeuble.AccReadOnly = false;
            this.lblImmeuble.AccReadOnlyAllowDelete = false;
            this.lblImmeuble.AccRequired = false;
            this.lblImmeuble.BackColor = System.Drawing.Color.White;
            this.lblImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.lblImmeuble.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblImmeuble.ForeColor = System.Drawing.Color.Black;
            this.lblImmeuble.Location = new System.Drawing.Point(92, 21);
            this.lblImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.lblImmeuble.MaxLength = 32767;
            this.lblImmeuble.Multiline = false;
            this.lblImmeuble.Name = "lblImmeuble";
            this.lblImmeuble.ReadOnly = false;
            this.lblImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblImmeuble.Size = new System.Drawing.Size(211, 27);
            this.lblImmeuble.TabIndex = 0;
            this.lblImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblImmeuble.UseSystemPasswordChar = false;
            // 
            // lblContrat
            // 
            this.lblContrat.AccAcceptNumbersOnly = false;
            this.lblContrat.AccAllowComma = false;
            this.lblContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.lblContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblContrat.AccHidenValue = "";
            this.lblContrat.AccNotAllowedChars = null;
            this.lblContrat.AccReadOnly = false;
            this.lblContrat.AccReadOnlyAllowDelete = false;
            this.lblContrat.AccRequired = false;
            this.lblContrat.BackColor = System.Drawing.Color.White;
            this.lblContrat.CustomBackColor = System.Drawing.Color.White;
            this.lblContrat.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblContrat.ForeColor = System.Drawing.Color.Black;
            this.lblContrat.Location = new System.Drawing.Point(92, 59);
            this.lblContrat.Margin = new System.Windows.Forms.Padding(2);
            this.lblContrat.MaxLength = 32767;
            this.lblContrat.Multiline = false;
            this.lblContrat.Name = "lblContrat";
            this.lblContrat.ReadOnly = false;
            this.lblContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblContrat.Size = new System.Drawing.Size(211, 27);
            this.lblContrat.TabIndex = 1;
            this.lblContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblContrat.UseSystemPasswordChar = false;
            // 
            // txtAvenant
            // 
            this.txtAvenant.AccAcceptNumbersOnly = false;
            this.txtAvenant.AccAllowComma = false;
            this.txtAvenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAvenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAvenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtAvenant.AccHidenValue = "";
            this.txtAvenant.AccNotAllowedChars = null;
            this.txtAvenant.AccReadOnly = false;
            this.txtAvenant.AccReadOnlyAllowDelete = false;
            this.txtAvenant.AccRequired = false;
            this.txtAvenant.BackColor = System.Drawing.Color.White;
            this.txtAvenant.CustomBackColor = System.Drawing.Color.White;
            this.txtAvenant.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAvenant.ForeColor = System.Drawing.Color.Black;
            this.txtAvenant.Location = new System.Drawing.Point(-12, 151);
            this.txtAvenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtAvenant.MaxLength = 32767;
            this.txtAvenant.Multiline = false;
            this.txtAvenant.Name = "txtAvenant";
            this.txtAvenant.ReadOnly = false;
            this.txtAvenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAvenant.Size = new System.Drawing.Size(99, 27);
            this.txtAvenant.TabIndex = 505;
            this.txtAvenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAvenant.UseSystemPasswordChar = false;
            this.txtAvenant.Visible = false;
            // 
            // txtNumContrat
            // 
            this.txtNumContrat.AccAcceptNumbersOnly = false;
            this.txtNumContrat.AccAllowComma = false;
            this.txtNumContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNumContrat.AccHidenValue = "";
            this.txtNumContrat.AccNotAllowedChars = null;
            this.txtNumContrat.AccReadOnly = false;
            this.txtNumContrat.AccReadOnlyAllowDelete = false;
            this.txtNumContrat.AccRequired = false;
            this.txtNumContrat.BackColor = System.Drawing.Color.White;
            this.txtNumContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtNumContrat.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumContrat.ForeColor = System.Drawing.Color.Black;
            this.txtNumContrat.Location = new System.Drawing.Point(286, 125);
            this.txtNumContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumContrat.MaxLength = 32767;
            this.txtNumContrat.Multiline = false;
            this.txtNumContrat.Name = "txtNumContrat";
            this.txtNumContrat.ReadOnly = false;
            this.txtNumContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumContrat.Size = new System.Drawing.Size(99, 27);
            this.txtNumContrat.TabIndex = 506;
            this.txtNumContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumContrat.UseSystemPasswordChar = false;
            this.txtNumContrat.Visible = false;
            // 
            // ChkResiliee
            // 
            this.ChkResiliee.AutoSize = true;
            this.ChkResiliee.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.ChkResiliee.Location = new System.Drawing.Point(92, 100);
            this.ChkResiliee.Name = "ChkResiliee";
            this.ChkResiliee.Size = new System.Drawing.Size(77, 20);
            this.ChkResiliee.TabIndex = 570;
            this.ChkResiliee.Text = "Resiliee";
            this.ChkResiliee.UseVisualStyleBackColor = true;
            // 
            // chkFactureManuelle
            // 
            this.chkFactureManuelle.AutoSize = true;
            this.chkFactureManuelle.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.chkFactureManuelle.Location = new System.Drawing.Point(92, 138);
            this.chkFactureManuelle.Name = "chkFactureManuelle";
            this.chkFactureManuelle.Size = new System.Drawing.Size(159, 20);
            this.chkFactureManuelle.TabIndex = 571;
            this.chkFactureManuelle.Text = "Facturé manuellement";
            this.chkFactureManuelle.UseVisualStyleBackColor = true;
            // 
            // chknonFacturable
            // 
            this.chknonFacturable.AutoSize = true;
            this.chknonFacturable.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.chknonFacturable.Location = new System.Drawing.Point(183, 100);
            this.chknonFacturable.Name = "chknonFacturable";
            this.chknonFacturable.Size = new System.Drawing.Size(119, 20);
            this.chknonFacturable.TabIndex = 572;
            this.chknonFacturable.Text = "Non Facturable";
            this.chknonFacturable.UseVisualStyleBackColor = true;
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(92, 181);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(93, 35);
            this.cmdAnnuler.TabIndex = 573;
            this.cmdAnnuler.Text = "  Annuler";
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(204, 181);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(98, 35);
            this.cmdAppliquer.TabIndex = 574;
            this.cmdAppliquer.Text = "Appliquer";
            this.cmdAppliquer.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // frmResiliation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(381, 240);
            this.Controls.Add(this.cmdAppliquer);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.chknonFacturable);
            this.Controls.Add(this.chkFactureManuelle);
            this.Controls.Add(this.ChkResiliee);
            this.Controls.Add(this.txtNumContrat);
            this.Controls.Add(this.txtAvenant);
            this.Controls.Add(this.lblContrat);
            this.Controls.Add(this.lblImmeuble);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(397, 279);
            this.MinimumSize = new System.Drawing.Size(397, 279);
            this.Name = "frmResiliation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmResiliation";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 lblImmeuble;
        public iTalk.iTalk_TextBox_Small2 lblContrat;
        public iTalk.iTalk_TextBox_Small2 txtAvenant;
        public iTalk.iTalk_TextBox_Small2 txtNumContrat;
        public System.Windows.Forms.CheckBox ChkResiliee;
        public System.Windows.Forms.CheckBox chkFactureManuelle;
        public System.Windows.Forms.CheckBox chknonFacturable;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdAppliquer;
    }
}