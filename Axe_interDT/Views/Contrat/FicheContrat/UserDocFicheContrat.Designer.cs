namespace Axe_interDT.Views.Contrat
{
    partial class UserDocFicheContrat
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocFicheContrat));
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.label28 = new System.Windows.Forms.Label();
            this.txtIntervVille = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.Text3 = new iTalk.iTalk_TextBox_Small2();
            this.Text2 = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_CompAdresse1 = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_CompAdresse2 = new iTalk.iTalk_TextBox_Small2();
            this.txtFactAdresse = new iTalk.iTalk_TextBox_Small2();
            this.txtcommentaire2 = new iTalk.iTalk_TextBox_Small2();
            this.txtFactNomImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleFact = new iTalk.iTalk_TextBox_Small2();
            this.txtFactCP = new iTalk.iTalk_TextBox_Small2();
            this.txtFactVille = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervAdresse = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleInterv1 = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleInterv2 = new iTalk.iTalk_TextBox_Small2();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.Check3 = new System.Windows.Forms.CheckBox();
            this.chkFactureManuelle = new System.Windows.Forms.CheckBox();
            this.txtcommentaire1 = new iTalk.iTalk_TextBox_Small2();
            this.iTalk_TextBox_Small21 = new iTalk.iTalk_TextBox_Small2();
            this.txtRefGerant = new iTalk.iTalk_TextBox_Small2();
            this.label20 = new System.Windows.Forms.Label();
            this.Text1 = new iTalk.iTalk_TextBox_Small2();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.txtIntervCP = new iTalk.iTalk_TextBox_Small2();
            this.label19 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNomAnalytique = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCodeArticle = new iTalk.iTalk_TextBox_Small2();
            this.txtBaseContractuelle = new iTalk.iTalk_TextBox_Small2();
            this.txtBaseActualisee = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtDesignation1 = new iTalk.iTalk_TextBox_Small2();
            this.txtBase = new iTalk.iTalk_TextBox_Small2();
            this.txtDateActualisee = new iTalk.iTalk_TextBox_Small2();
            this.cmdCodeArticle = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtBaseRevisee = new iTalk.iTalk_TextBox_Small2();
            this.txtDateRevision = new iTalk.iTalk_TextBox_Small2();
            this.txtDesignation2 = new iTalk.iTalk_TextBox_Small2();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCON_Coef = new iTalk.iTalk_TextBox_Small2();
            this.optRevCoefFixe = new System.Windows.Forms.RadioButton();
            this.optSansRev = new System.Windows.Forms.RadioButton();
            this.optRevFormule = new System.Windows.Forms.RadioButton();
            this.txtCON_TypeContrat = new iTalk.iTalk_TextBox_Small2();
            this.fraFormule = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice6 = new System.Windows.Forms.Button();
            this.label43 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice5 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice4 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice3 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice2 = new System.Windows.Forms.Button();
            this.label39 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.txtTypeFormule = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice1 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice3 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice2 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice4 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice6 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice1Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice2Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice3Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice4Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice5 = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice5Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice6Valeur = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice1Date = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice2Date = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice3Date = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice4Date = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice5Date = new iTalk.iTalk_TextBox_Small2();
            this.txtIndice6Date = new iTalk.iTalk_TextBox_Small2();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Check2 = new System.Windows.Forms.CheckBox();
            this.Check1 = new System.Windows.Forms.CheckBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdIndice1 = new System.Windows.Forms.Button();
            this.label38 = new System.Windows.Forms.Label();
            this.Command2 = new System.Windows.Forms.Button();
            this.txtFormule = new iTalk.iTalk_TextBox_Small2();
            this.label44 = new System.Windows.Forms.Label();
            this.txtCGINITULE = new iTalk.iTalk_TextBox_Small2();
            this.txtCGNUM = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleActivite = new iTalk.iTalk_TextBox_Small2();
            this.Frame14 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.txtActivite = new iTalk.iTalk_TextBox_Small2();
            this.txtAnalytique = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_AnaModif = new iTalk.iTalk_TextBox_Small2();
            this.cmdActivite = new System.Windows.Forms.Button();
            this.cmdSecteur = new System.Windows.Forms.Button();
            this.ckkCON_AnaModif = new System.Windows.Forms.CheckBox();
            this.Command4 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ultraCombo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBDropDown1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtCON_EchuEchoir = new iTalk.iTalk_TextBox_Small2();
            this.Frame11 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.label34 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.txtCON_Indice1rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Indice2rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Indice3rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Valeur1rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Valeur2rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Valeur3rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Date1rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Date2rev = new iTalk.iTalk_TextBox_Small2();
            this.txtCON_Date3rev = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.optAutre = new System.Windows.Forms.RadioButton();
            this.optAnnuel = new System.Windows.Forms.RadioButton();
            this.Option6 = new System.Windows.Forms.RadioButton();
            this.Option7 = new System.Windows.Forms.RadioButton();
            this.Frame5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.Option9 = new System.Windows.Forms.RadioButton();
            this.Option8 = new System.Windows.Forms.RadioButton();
            this.chkCalcEcheance = new System.Windows.Forms.CheckBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.txtANCC_DateReceptResil = new iTalk.iTalk_TextBox_Small2();
            this.label48 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.txtANCC_CommRESILIE = new iTalk.iTalk_RichTextBox();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.chkANCC_Reconduction = new System.Windows.Forms.CheckBox();
            this.chkANCC_RenovPerdue = new System.Windows.Forms.CheckBox();
            this.Frame6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.txtANCC_CommACCEPTE = new iTalk.iTalk_RichTextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.cmdResilié = new System.Windows.Forms.Button();
            this.Frame9 = new System.Windows.Forms.GroupBox();
            this.ChkResiliee = new System.Windows.Forms.CheckBox();
            this.lblChaudiereCondo = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.lblAutoliquidation = new System.Windows.Forms.Label();
            this.lblGoFacturation = new System.Windows.Forms.Label();
            this.lblGoTypeRevision = new System.Windows.Forms.Label();
            this.lblGoFormule = new System.Windows.Forms.Label();
            this.lblGoHisto = new System.Windows.Forms.Label();
            this.lblGoListeContrats = new System.Windows.Forms.Label();
            this.Label53 = new System.Windows.Forms.Label();
            this.lblGoIndice = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.txtLibelleCont2 = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelleCont1 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdPremier = new System.Windows.Forms.Button();
            this.cmdPrecedent = new System.Windows.Forms.Button();
            this.cmdSuivant = new System.Windows.Forms.Button();
            this.cmdDernier = new System.Windows.Forms.Button();
            this.txtAvenant = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNumContrat = new iTalk.iTalk_TextBox_Small2();
            this.cmdAjoutAvenant = new System.Windows.Forms.Button();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblContrat = new System.Windows.Forms.Label();
            this.lblSyndic = new System.Windows.Forms.Label();
            this.lblImmeuble = new System.Windows.Forms.Label();
            this.cmdSyndic = new System.Windows.Forms.Button();
            this.cmdImmeuble = new System.Windows.Forms.Button();
            this.cmdContrat = new System.Windows.Forms.Button();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.cmdRecherche = new System.Windows.Forms.Button();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.txtSyndic = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNomImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtNomSyndic = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraTabPageControl1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.fraFormule.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.flowLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.Frame14.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.Frame11.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.Frame5.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.Frame6.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.Frame9.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.flowLayoutPanel2.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel7);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 21);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1848, 693);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 6;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99875F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00125F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00125F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99875F));
            this.tableLayoutPanel7.Controls.Add(this.label28, 2, 16);
            this.tableLayoutPanel7.Controls.Add(this.txtIntervVille, 3, 16);
            this.tableLayoutPanel7.Controls.Add(this.label18, 0, 16);
            this.tableLayoutPanel7.Controls.Add(this.label23, 0, 3);
            this.tableLayoutPanel7.Controls.Add(this.Text3, 4, 1);
            this.tableLayoutPanel7.Controls.Add(this.Text2, 3, 1);
            this.tableLayoutPanel7.Controls.Add(this.txtCON_CompAdresse1, 1, 4);
            this.tableLayoutPanel7.Controls.Add(this.txtCON_CompAdresse2, 1, 5);
            this.tableLayoutPanel7.Controls.Add(this.txtFactAdresse, 1, 7);
            this.tableLayoutPanel7.Controls.Add(this.txtcommentaire2, 1, 8);
            this.tableLayoutPanel7.Controls.Add(this.txtFactNomImmeuble, 1, 9);
            this.tableLayoutPanel7.Controls.Add(this.txtLibelleFact, 1, 10);
            this.tableLayoutPanel7.Controls.Add(this.txtFactCP, 1, 11);
            this.tableLayoutPanel7.Controls.Add(this.txtFactVille, 3, 11);
            this.tableLayoutPanel7.Controls.Add(this.txtIntervAdresse, 1, 13);
            this.tableLayoutPanel7.Controls.Add(this.txtLibelleInterv1, 1, 14);
            this.tableLayoutPanel7.Controls.Add(this.txtLibelleInterv2, 1, 15);
            this.tableLayoutPanel7.Controls.Add(this.groupBox2, 5, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtcommentaire1, 5, 5);
            this.tableLayoutPanel7.Controls.Add(this.iTalk_TextBox_Small21, 5, 6);
            this.tableLayoutPanel7.Controls.Add(this.txtRefGerant, 1, 1);
            this.tableLayoutPanel7.Controls.Add(this.label20, 2, 0);
            this.tableLayoutPanel7.Controls.Add(this.Text1, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.label22, 2, 1);
            this.tableLayoutPanel7.Controls.Add(this.label21, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.label24, 0, 6);
            this.tableLayoutPanel7.Controls.Add(this.label25, 0, 11);
            this.tableLayoutPanel7.Controls.Add(this.label26, 2, 11);
            this.tableLayoutPanel7.Controls.Add(this.label27, 0, 12);
            this.tableLayoutPanel7.Controls.Add(this.txtIntervCP, 1, 16);
            this.tableLayoutPanel7.Controls.Add(this.label19, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 16;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1848, 693);
            this.tableLayoutPanel7.TabIndex = 2;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(465, 498);
            this.label28.Margin = new System.Windows.Forms.Padding(1);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(343, 194);
            this.label28.TabIndex = 523;
            this.label28.Text = "Ville";
            this.label28.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtIntervVille
            // 
            this.txtIntervVille.AccAcceptNumbersOnly = false;
            this.txtIntervVille.AccAllowComma = false;
            this.txtIntervVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervVille.AccHidenValue = "";
            this.txtIntervVille.AccNotAllowedChars = null;
            this.txtIntervVille.AccReadOnly = false;
            this.txtIntervVille.AccReadOnlyAllowDelete = false;
            this.txtIntervVille.AccRequired = false;
            this.txtIntervVille.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtIntervVille, 2);
            this.txtIntervVille.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervVille.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIntervVille.ForeColor = System.Drawing.Color.Blue;
            this.txtIntervVille.Location = new System.Drawing.Point(811, 499);
            this.txtIntervVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervVille.MaxLength = 32767;
            this.txtIntervVille.Multiline = false;
            this.txtIntervVille.Name = "txtIntervVille";
            this.txtIntervVille.ReadOnly = true;
            this.txtIntervVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervVille.Size = new System.Drawing.Size(688, 27);
            this.txtIntervVille.TabIndex = 16;
            this.txtIntervVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervVille.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 498);
            this.label18.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(79, 194);
            this.label18.TabIndex = 521;
            this.label18.Text = "CP";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label23, 2);
            this.label23.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(20, 94);
            this.label23.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(443, 29);
            this.label23.TabIndex = 391;
            this.label23.Text = "Commentaire";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Text3
            // 
            this.Text3.AccAcceptNumbersOnly = false;
            this.Text3.AccAllowComma = false;
            this.Text3.AccBackgroundColor = System.Drawing.Color.White;
            this.Text3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text3.AccHidenValue = "";
            this.Text3.AccNotAllowedChars = null;
            this.Text3.AccReadOnly = false;
            this.Text3.AccReadOnlyAllowDelete = false;
            this.Text3.AccRequired = false;
            this.Text3.BackColor = System.Drawing.Color.White;
            this.Text3.CustomBackColor = System.Drawing.Color.White;
            this.Text3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text3.ForeColor = System.Drawing.Color.Black;
            this.Text3.Location = new System.Drawing.Point(1157, 33);
            this.Text3.Margin = new System.Windows.Forms.Padding(2);
            this.Text3.MaximumSize = new System.Drawing.Size(10000, 29);
            this.Text3.MaxLength = 32767;
            this.Text3.MinimumSize = new System.Drawing.Size(50, 27);
            this.Text3.Multiline = false;
            this.Text3.Name = "Text3";
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text3.Size = new System.Drawing.Size(342, 27);
            this.Text3.TabIndex = 3;
            this.Text3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text3.UseSystemPasswordChar = false;
            this.Text3.TextChanged += new System.EventHandler(this.Text3_TextChanged);
            this.Text3.Leave += new System.EventHandler(this.Text3_Leave);
            // 
            // Text2
            // 
            this.Text2.AccAcceptNumbersOnly = false;
            this.Text2.AccAllowComma = false;
            this.Text2.AccBackgroundColor = System.Drawing.Color.White;
            this.Text2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text2.AccHidenValue = "";
            this.Text2.AccNotAllowedChars = null;
            this.Text2.AccReadOnly = false;
            this.Text2.AccReadOnlyAllowDelete = false;
            this.Text2.AccRequired = false;
            this.Text2.BackColor = System.Drawing.Color.White;
            this.Text2.CustomBackColor = System.Drawing.Color.White;
            this.Text2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text2.ForeColor = System.Drawing.Color.Black;
            this.Text2.Location = new System.Drawing.Point(811, 33);
            this.Text2.Margin = new System.Windows.Forms.Padding(2);
            this.Text2.MaximumSize = new System.Drawing.Size(10000, 29);
            this.Text2.MaxLength = 32767;
            this.Text2.MinimumSize = new System.Drawing.Size(50, 27);
            this.Text2.Multiline = false;
            this.Text2.Name = "Text2";
            this.Text2.ReadOnly = false;
            this.Text2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text2.Size = new System.Drawing.Size(342, 27);
            this.Text2.TabIndex = 2;
            this.Text2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text2.UseSystemPasswordChar = false;
            this.Text2.TextChanged += new System.EventHandler(this.Text2_TextChanged);
            this.Text2.Leave += new System.EventHandler(this.Text2_Leave);
            // 
            // txtCON_CompAdresse1
            // 
            this.txtCON_CompAdresse1.AccAcceptNumbersOnly = false;
            this.txtCON_CompAdresse1.AccAllowComma = false;
            this.txtCON_CompAdresse1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_CompAdresse1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_CompAdresse1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_CompAdresse1.AccHidenValue = "";
            this.txtCON_CompAdresse1.AccNotAllowedChars = null;
            this.txtCON_CompAdresse1.AccReadOnly = false;
            this.txtCON_CompAdresse1.AccReadOnlyAllowDelete = false;
            this.txtCON_CompAdresse1.AccRequired = false;
            this.txtCON_CompAdresse1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtCON_CompAdresse1, 4);
            this.txtCON_CompAdresse1.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_CompAdresse1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_CompAdresse1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCON_CompAdresse1.ForeColor = System.Drawing.Color.Black;
            this.txtCON_CompAdresse1.Location = new System.Drawing.Point(102, 126);
            this.txtCON_CompAdresse1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCON_CompAdresse1.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtCON_CompAdresse1.MaxLength = 32767;
            this.txtCON_CompAdresse1.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtCON_CompAdresse1.Multiline = false;
            this.txtCON_CompAdresse1.Name = "txtCON_CompAdresse1";
            this.txtCON_CompAdresse1.ReadOnly = false;
            this.txtCON_CompAdresse1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_CompAdresse1.Size = new System.Drawing.Size(1397, 27);
            this.txtCON_CompAdresse1.TabIndex = 4;
            this.txtCON_CompAdresse1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_CompAdresse1.UseSystemPasswordChar = false;
            this.txtCON_CompAdresse1.TextChanged += new System.EventHandler(this.txtCON_CompAdresse1_TextChanged);
            // 
            // txtCON_CompAdresse2
            // 
            this.txtCON_CompAdresse2.AccAcceptNumbersOnly = false;
            this.txtCON_CompAdresse2.AccAllowComma = false;
            this.txtCON_CompAdresse2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_CompAdresse2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_CompAdresse2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_CompAdresse2.AccHidenValue = "";
            this.txtCON_CompAdresse2.AccNotAllowedChars = null;
            this.txtCON_CompAdresse2.AccReadOnly = false;
            this.txtCON_CompAdresse2.AccReadOnlyAllowDelete = false;
            this.txtCON_CompAdresse2.AccRequired = false;
            this.txtCON_CompAdresse2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtCON_CompAdresse2, 4);
            this.txtCON_CompAdresse2.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_CompAdresse2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_CompAdresse2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCON_CompAdresse2.ForeColor = System.Drawing.Color.Black;
            this.txtCON_CompAdresse2.Location = new System.Drawing.Point(102, 157);
            this.txtCON_CompAdresse2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCON_CompAdresse2.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtCON_CompAdresse2.MaxLength = 32767;
            this.txtCON_CompAdresse2.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtCON_CompAdresse2.Multiline = false;
            this.txtCON_CompAdresse2.Name = "txtCON_CompAdresse2";
            this.txtCON_CompAdresse2.ReadOnly = false;
            this.txtCON_CompAdresse2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_CompAdresse2.Size = new System.Drawing.Size(1397, 27);
            this.txtCON_CompAdresse2.TabIndex = 5;
            this.txtCON_CompAdresse2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_CompAdresse2.UseSystemPasswordChar = false;
            this.txtCON_CompAdresse2.TextChanged += new System.EventHandler(this.txtCON_CompAdresse2_TextChanged);
            // 
            // txtFactAdresse
            // 
            this.txtFactAdresse.AccAcceptNumbersOnly = false;
            this.txtFactAdresse.AccAllowComma = false;
            this.txtFactAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFactAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFactAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFactAdresse.AccHidenValue = "";
            this.txtFactAdresse.AccNotAllowedChars = null;
            this.txtFactAdresse.AccReadOnly = false;
            this.txtFactAdresse.AccReadOnlyAllowDelete = false;
            this.txtFactAdresse.AccRequired = false;
            this.txtFactAdresse.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtFactAdresse, 4);
            this.txtFactAdresse.CustomBackColor = System.Drawing.Color.White;
            this.txtFactAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFactAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactAdresse.ForeColor = System.Drawing.Color.Blue;
            this.txtFactAdresse.Location = new System.Drawing.Point(102, 219);
            this.txtFactAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtFactAdresse.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtFactAdresse.MaxLength = 32767;
            this.txtFactAdresse.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtFactAdresse.Multiline = false;
            this.txtFactAdresse.Name = "txtFactAdresse";
            this.txtFactAdresse.ReadOnly = true;
            this.txtFactAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFactAdresse.Size = new System.Drawing.Size(1397, 27);
            this.txtFactAdresse.TabIndex = 6;
            this.txtFactAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFactAdresse.UseSystemPasswordChar = false;
            // 
            // txtcommentaire2
            // 
            this.txtcommentaire2.AccAcceptNumbersOnly = false;
            this.txtcommentaire2.AccAllowComma = false;
            this.txtcommentaire2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcommentaire2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcommentaire2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcommentaire2.AccHidenValue = "";
            this.txtcommentaire2.AccNotAllowedChars = null;
            this.txtcommentaire2.AccReadOnly = false;
            this.txtcommentaire2.AccReadOnlyAllowDelete = false;
            this.txtcommentaire2.AccRequired = false;
            this.txtcommentaire2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtcommentaire2, 4);
            this.txtcommentaire2.CustomBackColor = System.Drawing.Color.White;
            this.txtcommentaire2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcommentaire2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtcommentaire2.ForeColor = System.Drawing.Color.Blue;
            this.txtcommentaire2.Location = new System.Drawing.Point(102, 250);
            this.txtcommentaire2.Margin = new System.Windows.Forms.Padding(2);
            this.txtcommentaire2.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtcommentaire2.MaxLength = 32767;
            this.txtcommentaire2.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtcommentaire2.Multiline = false;
            this.txtcommentaire2.Name = "txtcommentaire2";
            this.txtcommentaire2.ReadOnly = true;
            this.txtcommentaire2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcommentaire2.Size = new System.Drawing.Size(1397, 27);
            this.txtcommentaire2.TabIndex = 7;
            this.txtcommentaire2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcommentaire2.UseSystemPasswordChar = false;
            // 
            // txtFactNomImmeuble
            // 
            this.txtFactNomImmeuble.AccAcceptNumbersOnly = false;
            this.txtFactNomImmeuble.AccAllowComma = false;
            this.txtFactNomImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFactNomImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFactNomImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFactNomImmeuble.AccHidenValue = "";
            this.txtFactNomImmeuble.AccNotAllowedChars = null;
            this.txtFactNomImmeuble.AccReadOnly = false;
            this.txtFactNomImmeuble.AccReadOnlyAllowDelete = false;
            this.txtFactNomImmeuble.AccRequired = false;
            this.txtFactNomImmeuble.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtFactNomImmeuble, 4);
            this.txtFactNomImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtFactNomImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFactNomImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactNomImmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtFactNomImmeuble.Location = new System.Drawing.Point(102, 281);
            this.txtFactNomImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtFactNomImmeuble.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtFactNomImmeuble.MaxLength = 32767;
            this.txtFactNomImmeuble.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtFactNomImmeuble.Multiline = false;
            this.txtFactNomImmeuble.Name = "txtFactNomImmeuble";
            this.txtFactNomImmeuble.ReadOnly = true;
            this.txtFactNomImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFactNomImmeuble.Size = new System.Drawing.Size(1397, 27);
            this.txtFactNomImmeuble.TabIndex = 8;
            this.txtFactNomImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFactNomImmeuble.UseSystemPasswordChar = false;
            // 
            // txtLibelleFact
            // 
            this.txtLibelleFact.AccAcceptNumbersOnly = false;
            this.txtLibelleFact.AccAllowComma = false;
            this.txtLibelleFact.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleFact.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleFact.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleFact.AccHidenValue = "";
            this.txtLibelleFact.AccNotAllowedChars = null;
            this.txtLibelleFact.AccReadOnly = false;
            this.txtLibelleFact.AccReadOnlyAllowDelete = false;
            this.txtLibelleFact.AccRequired = false;
            this.txtLibelleFact.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtLibelleFact, 4);
            this.txtLibelleFact.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleFact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleFact.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelleFact.ForeColor = System.Drawing.Color.Blue;
            this.txtLibelleFact.Location = new System.Drawing.Point(102, 312);
            this.txtLibelleFact.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleFact.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtLibelleFact.MaxLength = 32767;
            this.txtLibelleFact.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtLibelleFact.Multiline = false;
            this.txtLibelleFact.Name = "txtLibelleFact";
            this.txtLibelleFact.ReadOnly = true;
            this.txtLibelleFact.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleFact.Size = new System.Drawing.Size(1397, 27);
            this.txtLibelleFact.TabIndex = 9;
            this.txtLibelleFact.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleFact.UseSystemPasswordChar = false;
            // 
            // txtFactCP
            // 
            this.txtFactCP.AccAcceptNumbersOnly = false;
            this.txtFactCP.AccAllowComma = false;
            this.txtFactCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFactCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFactCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFactCP.AccHidenValue = "";
            this.txtFactCP.AccNotAllowedChars = null;
            this.txtFactCP.AccReadOnly = false;
            this.txtFactCP.AccReadOnlyAllowDelete = false;
            this.txtFactCP.AccRequired = false;
            this.txtFactCP.BackColor = System.Drawing.Color.White;
            this.txtFactCP.CustomBackColor = System.Drawing.Color.White;
            this.txtFactCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFactCP.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactCP.ForeColor = System.Drawing.Color.Blue;
            this.txtFactCP.Location = new System.Drawing.Point(102, 343);
            this.txtFactCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtFactCP.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtFactCP.MaxLength = 32767;
            this.txtFactCP.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtFactCP.Multiline = false;
            this.txtFactCP.Name = "txtFactCP";
            this.txtFactCP.ReadOnly = true;
            this.txtFactCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFactCP.Size = new System.Drawing.Size(360, 27);
            this.txtFactCP.TabIndex = 10;
            this.txtFactCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFactCP.UseSystemPasswordChar = false;
            // 
            // txtFactVille
            // 
            this.txtFactVille.AccAcceptNumbersOnly = false;
            this.txtFactVille.AccAllowComma = false;
            this.txtFactVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFactVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFactVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFactVille.AccHidenValue = "";
            this.txtFactVille.AccNotAllowedChars = null;
            this.txtFactVille.AccReadOnly = false;
            this.txtFactVille.AccReadOnlyAllowDelete = false;
            this.txtFactVille.AccRequired = false;
            this.txtFactVille.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtFactVille, 2);
            this.txtFactVille.CustomBackColor = System.Drawing.Color.White;
            this.txtFactVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFactVille.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFactVille.ForeColor = System.Drawing.Color.Blue;
            this.txtFactVille.Location = new System.Drawing.Point(811, 343);
            this.txtFactVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtFactVille.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtFactVille.MaxLength = 32767;
            this.txtFactVille.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtFactVille.Multiline = false;
            this.txtFactVille.Name = "txtFactVille";
            this.txtFactVille.ReadOnly = true;
            this.txtFactVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFactVille.Size = new System.Drawing.Size(688, 27);
            this.txtFactVille.TabIndex = 11;
            this.txtFactVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFactVille.UseSystemPasswordChar = false;
            // 
            // txtIntervAdresse
            // 
            this.txtIntervAdresse.AccAcceptNumbersOnly = false;
            this.txtIntervAdresse.AccAllowComma = false;
            this.txtIntervAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervAdresse.AccHidenValue = "";
            this.txtIntervAdresse.AccNotAllowedChars = null;
            this.txtIntervAdresse.AccReadOnly = false;
            this.txtIntervAdresse.AccReadOnlyAllowDelete = false;
            this.txtIntervAdresse.AccRequired = false;
            this.txtIntervAdresse.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtIntervAdresse, 4);
            this.txtIntervAdresse.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervAdresse.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIntervAdresse.ForeColor = System.Drawing.Color.Blue;
            this.txtIntervAdresse.Location = new System.Drawing.Point(102, 405);
            this.txtIntervAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervAdresse.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtIntervAdresse.MaxLength = 32767;
            this.txtIntervAdresse.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtIntervAdresse.Multiline = false;
            this.txtIntervAdresse.Name = "txtIntervAdresse";
            this.txtIntervAdresse.ReadOnly = true;
            this.txtIntervAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervAdresse.Size = new System.Drawing.Size(1397, 27);
            this.txtIntervAdresse.TabIndex = 12;
            this.txtIntervAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervAdresse.UseSystemPasswordChar = false;
            // 
            // txtLibelleInterv1
            // 
            this.txtLibelleInterv1.AccAcceptNumbersOnly = false;
            this.txtLibelleInterv1.AccAllowComma = false;
            this.txtLibelleInterv1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleInterv1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleInterv1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleInterv1.AccHidenValue = "";
            this.txtLibelleInterv1.AccNotAllowedChars = null;
            this.txtLibelleInterv1.AccReadOnly = false;
            this.txtLibelleInterv1.AccReadOnlyAllowDelete = false;
            this.txtLibelleInterv1.AccRequired = false;
            this.txtLibelleInterv1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtLibelleInterv1, 4);
            this.txtLibelleInterv1.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleInterv1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleInterv1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelleInterv1.ForeColor = System.Drawing.Color.Blue;
            this.txtLibelleInterv1.Location = new System.Drawing.Point(102, 436);
            this.txtLibelleInterv1.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleInterv1.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtLibelleInterv1.MaxLength = 32767;
            this.txtLibelleInterv1.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtLibelleInterv1.Multiline = false;
            this.txtLibelleInterv1.Name = "txtLibelleInterv1";
            this.txtLibelleInterv1.ReadOnly = true;
            this.txtLibelleInterv1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleInterv1.Size = new System.Drawing.Size(1397, 27);
            this.txtLibelleInterv1.TabIndex = 13;
            this.txtLibelleInterv1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleInterv1.UseSystemPasswordChar = false;
            // 
            // txtLibelleInterv2
            // 
            this.txtLibelleInterv2.AccAcceptNumbersOnly = false;
            this.txtLibelleInterv2.AccAllowComma = false;
            this.txtLibelleInterv2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleInterv2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleInterv2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleInterv2.AccHidenValue = "";
            this.txtLibelleInterv2.AccNotAllowedChars = null;
            this.txtLibelleInterv2.AccReadOnly = false;
            this.txtLibelleInterv2.AccReadOnlyAllowDelete = false;
            this.txtLibelleInterv2.AccRequired = false;
            this.txtLibelleInterv2.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel7.SetColumnSpan(this.txtLibelleInterv2, 4);
            this.txtLibelleInterv2.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleInterv2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleInterv2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelleInterv2.ForeColor = System.Drawing.Color.Blue;
            this.txtLibelleInterv2.Location = new System.Drawing.Point(102, 468);
            this.txtLibelleInterv2.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleInterv2.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtLibelleInterv2.MaxLength = 32767;
            this.txtLibelleInterv2.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtLibelleInterv2.Multiline = false;
            this.txtLibelleInterv2.Name = "txtLibelleInterv2";
            this.txtLibelleInterv2.ReadOnly = true;
            this.txtLibelleInterv2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleInterv2.Size = new System.Drawing.Size(1397, 27);
            this.txtLibelleInterv2.TabIndex = 14;
            this.txtLibelleInterv2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleInterv2.UseSystemPasswordChar = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.flowLayoutPanel6);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox2.Location = new System.Drawing.Point(1504, 3);
            this.groupBox2.Name = "groupBox2";
            this.tableLayoutPanel7.SetRowSpan(this.groupBox2, 4);
            this.groupBox2.Size = new System.Drawing.Size(341, 118);
            this.groupBox2.TabIndex = 519;
            this.groupBox2.TabStop = false;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Controls.Add(this.Check3);
            this.flowLayoutPanel6.Controls.Add(this.chkFactureManuelle);
            this.flowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(3, 18);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(335, 97);
            this.flowLayoutPanel6.TabIndex = 0;
            // 
            // Check3
            // 
            this.Check3.AutoSize = true;
            this.Check3.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Check3.Enabled = false;
            this.Check3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check3.Location = new System.Drawing.Point(3, 3);
            this.Check3.Name = "Check3";
            this.Check3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.Check3.Size = new System.Drawing.Size(134, 23);
            this.Check3.TabIndex = 570;
            this.Check3.Text = "Non Facturable";
            this.Check3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Check3.UseVisualStyleBackColor = true;
            // 
            // chkFactureManuelle
            // 
            this.chkFactureManuelle.AutoSize = true;
            this.chkFactureManuelle.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFactureManuelle.Enabled = false;
            this.chkFactureManuelle.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkFactureManuelle.Location = new System.Drawing.Point(143, 3);
            this.chkFactureManuelle.Name = "chkFactureManuelle";
            this.chkFactureManuelle.Size = new System.Drawing.Size(186, 23);
            this.chkFactureManuelle.TabIndex = 571;
            this.chkFactureManuelle.Text = "Facturé manuellement";
            this.chkFactureManuelle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkFactureManuelle.UseVisualStyleBackColor = true;
            // 
            // txtcommentaire1
            // 
            this.txtcommentaire1.AccAcceptNumbersOnly = false;
            this.txtcommentaire1.AccAllowComma = false;
            this.txtcommentaire1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcommentaire1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcommentaire1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcommentaire1.AccHidenValue = "";
            this.txtcommentaire1.AccNotAllowedChars = null;
            this.txtcommentaire1.AccReadOnly = false;
            this.txtcommentaire1.AccReadOnlyAllowDelete = false;
            this.txtcommentaire1.AccRequired = false;
            this.txtcommentaire1.BackColor = System.Drawing.Color.White;
            this.txtcommentaire1.CustomBackColor = System.Drawing.Color.White;
            this.txtcommentaire1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtcommentaire1.ForeColor = System.Drawing.Color.Black;
            this.txtcommentaire1.Location = new System.Drawing.Point(1503, 157);
            this.txtcommentaire1.Margin = new System.Windows.Forms.Padding(2);
            this.txtcommentaire1.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtcommentaire1.MaxLength = 32767;
            this.txtcommentaire1.MinimumSize = new System.Drawing.Size(50, 27);
            this.txtcommentaire1.Multiline = false;
            this.txtcommentaire1.Name = "txtcommentaire1";
            this.txtcommentaire1.ReadOnly = false;
            this.txtcommentaire1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcommentaire1.Size = new System.Drawing.Size(199, 27);
            this.txtcommentaire1.TabIndex = 520;
            this.txtcommentaire1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcommentaire1.UseSystemPasswordChar = false;
            this.txtcommentaire1.Visible = false;
            this.txtcommentaire1.TextChanged += new System.EventHandler(this.txtcommentaire1_TextChanged);
            // 
            // iTalk_TextBox_Small21
            // 
            this.iTalk_TextBox_Small21.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small21.AccAllowComma = false;
            this.iTalk_TextBox_Small21.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small21.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small21.AccHidenValue = "";
            this.iTalk_TextBox_Small21.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small21.AccReadOnly = false;
            this.iTalk_TextBox_Small21.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small21.AccRequired = false;
            this.iTalk_TextBox_Small21.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small21.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small21.Location = new System.Drawing.Point(1503, 188);
            this.iTalk_TextBox_Small21.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small21.MaxLength = 32767;
            this.iTalk_TextBox_Small21.Multiline = false;
            this.iTalk_TextBox_Small21.Name = "iTalk_TextBox_Small21";
            this.iTalk_TextBox_Small21.ReadOnly = false;
            this.iTalk_TextBox_Small21.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small21.Size = new System.Drawing.Size(135, 27);
            this.iTalk_TextBox_Small21.TabIndex = 524;
            this.iTalk_TextBox_Small21.TextAlignment = Infragistics.Win.HAlign.Left;
            this.iTalk_TextBox_Small21.UseSystemPasswordChar = false;
            this.iTalk_TextBox_Small21.Visible = false;
            // 
            // txtRefGerant
            // 
            this.txtRefGerant.AccAcceptNumbersOnly = false;
            this.txtRefGerant.AccAllowComma = false;
            this.txtRefGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRefGerant.AccHidenValue = "";
            this.txtRefGerant.AccNotAllowedChars = null;
            this.txtRefGerant.AccReadOnly = false;
            this.txtRefGerant.AccReadOnlyAllowDelete = false;
            this.txtRefGerant.AccRequired = false;
            this.txtRefGerant.BackColor = System.Drawing.Color.White;
            this.txtRefGerant.CustomBackColor = System.Drawing.Color.White;
            this.txtRefGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRefGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefGerant.ForeColor = System.Drawing.Color.Black;
            this.txtRefGerant.Location = new System.Drawing.Point(102, 33);
            this.txtRefGerant.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefGerant.MaximumSize = new System.Drawing.Size(10000, 29);
            this.txtRefGerant.MaxLength = 200;
            this.txtRefGerant.Multiline = false;
            this.txtRefGerant.Name = "txtRefGerant";
            this.txtRefGerant.ReadOnly = false;
            this.txtRefGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefGerant.Size = new System.Drawing.Size(360, 27);
            this.txtRefGerant.TabIndex = 0;
            this.txtRefGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefGerant.UseSystemPasswordChar = false;
            this.txtRefGerant.TextChanged += new System.EventHandler(this.txtRefGerant_TextChanged);
            // 
            // label20
            // 
            this.label20.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(465, 1);
            this.label20.Margin = new System.Windows.Forms.Padding(1);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(343, 29);
            this.label20.TabIndex = 385;
            this.label20.Text = "Date de signature";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Text1
            // 
            this.Text1.AccAcceptNumbersOnly = false;
            this.Text1.AccAllowComma = false;
            this.Text1.AccBackgroundColor = System.Drawing.Color.White;
            this.Text1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text1.AccHidenValue = "";
            this.Text1.AccNotAllowedChars = null;
            this.Text1.AccReadOnly = false;
            this.Text1.AccReadOnlyAllowDelete = false;
            this.Text1.AccRequired = false;
            this.Text1.BackColor = System.Drawing.Color.White;
            this.Text1.CustomBackColor = System.Drawing.Color.White;
            this.Text1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Text1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text1.ForeColor = System.Drawing.Color.Black;
            this.Text1.Location = new System.Drawing.Point(811, 2);
            this.Text1.Margin = new System.Windows.Forms.Padding(2);
            this.Text1.MaximumSize = new System.Drawing.Size(10000, 29);
            this.Text1.MaxLength = 32767;
            this.Text1.MinimumSize = new System.Drawing.Size(50, 27);
            this.Text1.Multiline = false;
            this.Text1.Name = "Text1";
            this.Text1.ReadOnly = false;
            this.Text1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text1.Size = new System.Drawing.Size(342, 27);
            this.Text1.TabIndex = 1;
            this.Text1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text1.UseSystemPasswordChar = false;
            this.Text1.TextChanged += new System.EventHandler(this.Text1_TextChanged);
            this.Text1.Leave += new System.EventHandler(this.Text1_Leave);
            // 
            // label22
            // 
            this.label22.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(465, 32);
            this.label22.Margin = new System.Windows.Forms.Padding(1);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(343, 29);
            this.label22.TabIndex = 387;
            this.label22.Text = "Date d\'effet ";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label21
            // 
            this.label21.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(1156, 1);
            this.label21.Margin = new System.Windows.Forms.Padding(1);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(344, 29);
            this.label21.TabIndex = 386;
            this.label21.Text = "Date de fin";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label24
            // 
            this.label24.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label24.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label24, 2);
            this.label24.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(20, 187);
            this.label24.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(443, 29);
            this.label24.TabIndex = 390;
            this.label24.Text = "Adresse de facturation(gérant)";
            // 
            // label25
            // 
            this.label25.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(20, 342);
            this.label25.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(79, 29);
            this.label25.TabIndex = 389;
            this.label25.Text = "CP";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(465, 342);
            this.label26.Margin = new System.Windows.Forms.Padding(1);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(343, 29);
            this.label26.TabIndex = 388;
            this.label26.Text = "Ville";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label27
            // 
            this.label27.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label27.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label27, 2);
            this.label27.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(20, 373);
            this.label27.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(443, 29);
            this.label27.TabIndex = 392;
            this.label27.Text = "Lieu d\'intervention (immeuble)";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIntervCP
            // 
            this.txtIntervCP.AccAcceptNumbersOnly = false;
            this.txtIntervCP.AccAllowComma = false;
            this.txtIntervCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervCP.AccHidenValue = "";
            this.txtIntervCP.AccNotAllowedChars = null;
            this.txtIntervCP.AccReadOnly = false;
            this.txtIntervCP.AccReadOnlyAllowDelete = false;
            this.txtIntervCP.AccRequired = false;
            this.txtIntervCP.BackColor = System.Drawing.Color.White;
            this.txtIntervCP.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervCP.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtIntervCP.ForeColor = System.Drawing.Color.Blue;
            this.txtIntervCP.Location = new System.Drawing.Point(102, 499);
            this.txtIntervCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervCP.MaxLength = 32767;
            this.txtIntervCP.Multiline = false;
            this.txtIntervCP.Name = "txtIntervCP";
            this.txtIntervCP.ReadOnly = true;
            this.txtIntervCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervCP.Size = new System.Drawing.Size(360, 27);
            this.txtIntervCP.TabIndex = 15;
            this.txtIntervCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervCP.UseSystemPasswordChar = false;
            // 
            // label19
            // 
            this.label19.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label19.AutoSize = true;
            this.tableLayoutPanel7.SetColumnSpan(this.label19, 2);
            this.label19.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 1);
            this.label19.Margin = new System.Windows.Forms.Padding(20, 1, 1, 1);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(443, 29);
            this.label19.TabIndex = 384;
            this.label19.Text = "Référence gérant";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel10);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1848, 693);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 7;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.32067F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.32774F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 3.735331F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.76051F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.72339F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.96387F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.1685F));
            this.tableLayoutPanel10.Controls.Add(this.txtNomAnalytique, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.label8, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.label9, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.txtCodeArticle, 1, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtBaseContractuelle, 1, 2);
            this.tableLayoutPanel10.Controls.Add(this.txtBaseActualisee, 1, 3);
            this.tableLayoutPanel10.Controls.Add(this.label10, 3, 1);
            this.tableLayoutPanel10.Controls.Add(this.label12, 3, 2);
            this.tableLayoutPanel10.Controls.Add(this.label11, 3, 3);
            this.tableLayoutPanel10.Controls.Add(this.txtDesignation1, 4, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtBase, 4, 2);
            this.tableLayoutPanel10.Controls.Add(this.txtDateActualisee, 4, 3);
            this.tableLayoutPanel10.Controls.Add(this.cmdCodeArticle, 2, 1);
            this.tableLayoutPanel10.Controls.Add(this.label14, 5, 1);
            this.tableLayoutPanel10.Controls.Add(this.label13, 5, 2);
            this.tableLayoutPanel10.Controls.Add(this.txtBaseRevisee, 6, 1);
            this.tableLayoutPanel10.Controls.Add(this.txtDateRevision, 6, 2);
            this.tableLayoutPanel10.Controls.Add(this.txtDesignation2, 5, 3);
            this.tableLayoutPanel10.Controls.Add(this.groupBox3, 1, 4);
            this.tableLayoutPanel10.Controls.Add(this.fraFormule, 2, 4);
            this.tableLayoutPanel10.Controls.Add(this.label44, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.txtCGINITULE, 4, 5);
            this.tableLayoutPanel10.Controls.Add(this.txtCGNUM, 5, 5);
            this.tableLayoutPanel10.Controls.Add(this.txtLibelleActivite, 6, 5);
            this.tableLayoutPanel10.Controls.Add(this.Frame14, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.Command4, 2, 5);
            this.tableLayoutPanel10.Controls.Add(this.label45, 3, 5);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 6;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 5F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1848, 693);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // txtNomAnalytique
            // 
            this.txtNomAnalytique.AccAcceptNumbersOnly = false;
            this.txtNomAnalytique.AccAllowComma = false;
            this.txtNomAnalytique.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomAnalytique.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomAnalytique.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomAnalytique.AccHidenValue = "";
            this.txtNomAnalytique.AccNotAllowedChars = null;
            this.txtNomAnalytique.AccReadOnly = false;
            this.txtNomAnalytique.AccReadOnlyAllowDelete = false;
            this.txtNomAnalytique.AccRequired = false;
            this.txtNomAnalytique.BackColor = System.Drawing.Color.White;
            this.txtNomAnalytique.CustomBackColor = System.Drawing.Color.White;
            this.txtNomAnalytique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNomAnalytique.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNomAnalytique.ForeColor = System.Drawing.Color.Black;
            this.txtNomAnalytique.Location = new System.Drawing.Point(359, 657);
            this.txtNomAnalytique.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomAnalytique.MaxLength = 32767;
            this.txtNomAnalytique.Multiline = false;
            this.txtNomAnalytique.Name = "txtNomAnalytique";
            this.txtNomAnalytique.ReadOnly = false;
            this.txtNomAnalytique.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomAnalytique.Size = new System.Drawing.Size(279, 27);
            this.txtNomAnalytique.TabIndex = 516;
            this.txtNomAnalytique.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomAnalytique.UseSystemPasswordChar = true;
            this.txtNomAnalytique.Visible = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(1, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(355, 28);
            this.label7.TabIndex = 384;
            this.label7.Text = "Prestation";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(1, 36);
            this.label8.Margin = new System.Windows.Forms.Padding(1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(355, 28);
            this.label8.TabIndex = 385;
            this.label8.Text = "Base contractuelle";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(1, 66);
            this.label9.Margin = new System.Windows.Forms.Padding(1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(355, 28);
            this.label9.TabIndex = 386;
            this.label9.Text = "Base actualisée";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCodeArticle
            // 
            this.txtCodeArticle.AccAcceptNumbersOnly = false;
            this.txtCodeArticle.AccAllowComma = false;
            this.txtCodeArticle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeArticle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeArticle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeArticle.AccHidenValue = "";
            this.txtCodeArticle.AccNotAllowedChars = null;
            this.txtCodeArticle.AccReadOnly = false;
            this.txtCodeArticle.AccReadOnlyAllowDelete = false;
            this.txtCodeArticle.AccRequired = false;
            this.txtCodeArticle.BackColor = System.Drawing.Color.White;
            this.txtCodeArticle.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeArticle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeArticle.ForeColor = System.Drawing.Color.Black;
            this.txtCodeArticle.Location = new System.Drawing.Point(358, 6);
            this.txtCodeArticle.Margin = new System.Windows.Forms.Padding(1);
            this.txtCodeArticle.MaxLength = 32767;
            this.txtCodeArticle.Multiline = false;
            this.txtCodeArticle.Name = "txtCodeArticle";
            this.txtCodeArticle.ReadOnly = false;
            this.txtCodeArticle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeArticle.Size = new System.Drawing.Size(281, 27);
            this.txtCodeArticle.TabIndex = 0;
            this.txtCodeArticle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeArticle.UseSystemPasswordChar = false;
            this.txtCodeArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeArticle_KeyPress);
            // 
            // txtBaseContractuelle
            // 
            this.txtBaseContractuelle.AccAcceptNumbersOnly = false;
            this.txtBaseContractuelle.AccAllowComma = false;
            this.txtBaseContractuelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtBaseContractuelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtBaseContractuelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtBaseContractuelle.AccHidenValue = "";
            this.txtBaseContractuelle.AccNotAllowedChars = null;
            this.txtBaseContractuelle.AccReadOnly = false;
            this.txtBaseContractuelle.AccReadOnlyAllowDelete = false;
            this.txtBaseContractuelle.AccRequired = false;
            this.txtBaseContractuelle.BackColor = System.Drawing.Color.White;
            this.txtBaseContractuelle.CustomBackColor = System.Drawing.Color.White;
            this.txtBaseContractuelle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBaseContractuelle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtBaseContractuelle.ForeColor = System.Drawing.Color.Black;
            this.txtBaseContractuelle.Location = new System.Drawing.Point(358, 36);
            this.txtBaseContractuelle.Margin = new System.Windows.Forms.Padding(1);
            this.txtBaseContractuelle.MaxLength = 32767;
            this.txtBaseContractuelle.Multiline = false;
            this.txtBaseContractuelle.Name = "txtBaseContractuelle";
            this.txtBaseContractuelle.ReadOnly = false;
            this.txtBaseContractuelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtBaseContractuelle.Size = new System.Drawing.Size(281, 27);
            this.txtBaseContractuelle.TabIndex = 1;
            this.txtBaseContractuelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtBaseContractuelle.UseSystemPasswordChar = false;
            this.txtBaseContractuelle.Leave += new System.EventHandler(this.txtBaseContractuelle_Leave);
            // 
            // txtBaseActualisee
            // 
            this.txtBaseActualisee.AccAcceptNumbersOnly = false;
            this.txtBaseActualisee.AccAllowComma = false;
            this.txtBaseActualisee.AccBackgroundColor = System.Drawing.Color.White;
            this.txtBaseActualisee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtBaseActualisee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtBaseActualisee.AccHidenValue = "";
            this.txtBaseActualisee.AccNotAllowedChars = null;
            this.txtBaseActualisee.AccReadOnly = false;
            this.txtBaseActualisee.AccReadOnlyAllowDelete = false;
            this.txtBaseActualisee.AccRequired = false;
            this.txtBaseActualisee.BackColor = System.Drawing.Color.White;
            this.txtBaseActualisee.CustomBackColor = System.Drawing.Color.White;
            this.txtBaseActualisee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBaseActualisee.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtBaseActualisee.ForeColor = System.Drawing.Color.Black;
            this.txtBaseActualisee.Location = new System.Drawing.Point(358, 66);
            this.txtBaseActualisee.Margin = new System.Windows.Forms.Padding(1);
            this.txtBaseActualisee.MaxLength = 32767;
            this.txtBaseActualisee.Multiline = false;
            this.txtBaseActualisee.Name = "txtBaseActualisee";
            this.txtBaseActualisee.ReadOnly = false;
            this.txtBaseActualisee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtBaseActualisee.Size = new System.Drawing.Size(281, 27);
            this.txtBaseActualisee.TabIndex = 2;
            this.txtBaseActualisee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtBaseActualisee.UseSystemPasswordChar = false;
            this.txtBaseActualisee.Leave += new System.EventHandler(this.txtBaseActualisee_Leave);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(710, 6);
            this.label10.Margin = new System.Windows.Forms.Padding(1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(326, 28);
            this.label10.TabIndex = 387;
            this.label10.Text = "Ref";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(710, 36);
            this.label12.Margin = new System.Windows.Forms.Padding(1);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(326, 28);
            this.label12.TabIndex = 389;
            this.label12.Text = "Date de base";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(710, 66);
            this.label11.Margin = new System.Windows.Forms.Padding(1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(326, 28);
            this.label11.TabIndex = 388;
            this.label11.Text = "Date d\'actualisation";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtDesignation1
            // 
            this.txtDesignation1.AccAcceptNumbersOnly = false;
            this.txtDesignation1.AccAllowComma = false;
            this.txtDesignation1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDesignation1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDesignation1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDesignation1.AccHidenValue = "";
            this.txtDesignation1.AccNotAllowedChars = null;
            this.txtDesignation1.AccReadOnly = false;
            this.txtDesignation1.AccReadOnlyAllowDelete = false;
            this.txtDesignation1.AccRequired = false;
            this.txtDesignation1.BackColor = System.Drawing.Color.White;
            this.txtDesignation1.CustomBackColor = System.Drawing.Color.White;
            this.txtDesignation1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDesignation1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDesignation1.ForeColor = System.Drawing.Color.Blue;
            this.txtDesignation1.Location = new System.Drawing.Point(1038, 6);
            this.txtDesignation1.Margin = new System.Windows.Forms.Padding(1);
            this.txtDesignation1.MaxLength = 32767;
            this.txtDesignation1.Multiline = false;
            this.txtDesignation1.Name = "txtDesignation1";
            this.txtDesignation1.ReadOnly = true;
            this.txtDesignation1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDesignation1.Size = new System.Drawing.Size(307, 27);
            this.txtDesignation1.TabIndex = 3;
            this.txtDesignation1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDesignation1.UseSystemPasswordChar = false;
            // 
            // txtBase
            // 
            this.txtBase.AccAcceptNumbersOnly = false;
            this.txtBase.AccAllowComma = false;
            this.txtBase.AccBackgroundColor = System.Drawing.Color.White;
            this.txtBase.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtBase.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtBase.AccHidenValue = "";
            this.txtBase.AccNotAllowedChars = null;
            this.txtBase.AccReadOnly = false;
            this.txtBase.AccReadOnlyAllowDelete = false;
            this.txtBase.AccRequired = false;
            this.txtBase.BackColor = System.Drawing.Color.White;
            this.txtBase.CustomBackColor = System.Drawing.Color.White;
            this.txtBase.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBase.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtBase.ForeColor = System.Drawing.Color.Black;
            this.txtBase.Location = new System.Drawing.Point(1038, 36);
            this.txtBase.Margin = new System.Windows.Forms.Padding(1);
            this.txtBase.MaxLength = 32767;
            this.txtBase.Multiline = false;
            this.txtBase.Name = "txtBase";
            this.txtBase.ReadOnly = false;
            this.txtBase.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtBase.Size = new System.Drawing.Size(307, 27);
            this.txtBase.TabIndex = 4;
            this.txtBase.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtBase.UseSystemPasswordChar = false;
            this.txtBase.Leave += new System.EventHandler(this.txtBase_Leave);
            // 
            // txtDateActualisee
            // 
            this.txtDateActualisee.AccAcceptNumbersOnly = false;
            this.txtDateActualisee.AccAllowComma = false;
            this.txtDateActualisee.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateActualisee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateActualisee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateActualisee.AccHidenValue = "";
            this.txtDateActualisee.AccNotAllowedChars = null;
            this.txtDateActualisee.AccReadOnly = false;
            this.txtDateActualisee.AccReadOnlyAllowDelete = false;
            this.txtDateActualisee.AccRequired = false;
            this.txtDateActualisee.BackColor = System.Drawing.Color.White;
            this.txtDateActualisee.CustomBackColor = System.Drawing.Color.White;
            this.txtDateActualisee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateActualisee.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateActualisee.ForeColor = System.Drawing.Color.Black;
            this.txtDateActualisee.Location = new System.Drawing.Point(1038, 66);
            this.txtDateActualisee.Margin = new System.Windows.Forms.Padding(1);
            this.txtDateActualisee.MaxLength = 32767;
            this.txtDateActualisee.Multiline = false;
            this.txtDateActualisee.Name = "txtDateActualisee";
            this.txtDateActualisee.ReadOnly = false;
            this.txtDateActualisee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateActualisee.Size = new System.Drawing.Size(307, 27);
            this.txtDateActualisee.TabIndex = 5;
            this.txtDateActualisee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateActualisee.UseSystemPasswordChar = false;
            this.txtDateActualisee.Leave += new System.EventHandler(this.txtDateActualisee_Leave);
            // 
            // cmdCodeArticle
            // 
            this.cmdCodeArticle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCodeArticle.FlatAppearance.BorderSize = 0;
            this.cmdCodeArticle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCodeArticle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdCodeArticle.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdCodeArticle.Location = new System.Drawing.Point(643, 8);
            this.cmdCodeArticle.Name = "cmdCodeArticle";
            this.cmdCodeArticle.Size = new System.Drawing.Size(22, 19);
            this.cmdCodeArticle.TabIndex = 508;
            this.cmdCodeArticle.UseVisualStyleBackColor = false;
            this.cmdCodeArticle.Click += new System.EventHandler(this.cmdCodeArticle_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(1347, 6);
            this.label14.Margin = new System.Windows.Forms.Padding(1);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(96, 19);
            this.label14.TabIndex = 391;
            this.label14.Text = "Base revisee";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label14.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(1347, 36);
            this.label13.Margin = new System.Windows.Forms.Padding(1);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(120, 19);
            this.label13.TabIndex = 390;
            this.label13.Text = "Date de revision";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label13.Visible = false;
            // 
            // txtBaseRevisee
            // 
            this.txtBaseRevisee.AccAcceptNumbersOnly = false;
            this.txtBaseRevisee.AccAllowComma = false;
            this.txtBaseRevisee.AccBackgroundColor = System.Drawing.Color.White;
            this.txtBaseRevisee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtBaseRevisee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtBaseRevisee.AccHidenValue = "";
            this.txtBaseRevisee.AccNotAllowedChars = null;
            this.txtBaseRevisee.AccReadOnly = false;
            this.txtBaseRevisee.AccReadOnlyAllowDelete = false;
            this.txtBaseRevisee.AccRequired = false;
            this.txtBaseRevisee.BackColor = System.Drawing.Color.White;
            this.txtBaseRevisee.CustomBackColor = System.Drawing.Color.White;
            this.txtBaseRevisee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBaseRevisee.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtBaseRevisee.ForeColor = System.Drawing.Color.Black;
            this.txtBaseRevisee.Location = new System.Drawing.Point(1586, 6);
            this.txtBaseRevisee.Margin = new System.Windows.Forms.Padding(1);
            this.txtBaseRevisee.MaxLength = 32767;
            this.txtBaseRevisee.Multiline = false;
            this.txtBaseRevisee.Name = "txtBaseRevisee";
            this.txtBaseRevisee.ReadOnly = false;
            this.txtBaseRevisee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtBaseRevisee.Size = new System.Drawing.Size(261, 27);
            this.txtBaseRevisee.TabIndex = 5;
            this.txtBaseRevisee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtBaseRevisee.UseSystemPasswordChar = false;
            this.txtBaseRevisee.Visible = false;
            this.txtBaseRevisee.Leave += new System.EventHandler(this.txtBaseRevisee_Leave);
            // 
            // txtDateRevision
            // 
            this.txtDateRevision.AccAcceptNumbersOnly = false;
            this.txtDateRevision.AccAllowComma = false;
            this.txtDateRevision.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateRevision.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateRevision.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateRevision.AccHidenValue = "";
            this.txtDateRevision.AccNotAllowedChars = null;
            this.txtDateRevision.AccReadOnly = false;
            this.txtDateRevision.AccReadOnlyAllowDelete = false;
            this.txtDateRevision.AccRequired = false;
            this.txtDateRevision.BackColor = System.Drawing.Color.White;
            this.txtDateRevision.CustomBackColor = System.Drawing.Color.White;
            this.txtDateRevision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateRevision.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateRevision.ForeColor = System.Drawing.Color.Black;
            this.txtDateRevision.Location = new System.Drawing.Point(1586, 36);
            this.txtDateRevision.Margin = new System.Windows.Forms.Padding(1);
            this.txtDateRevision.MaxLength = 32767;
            this.txtDateRevision.Multiline = false;
            this.txtDateRevision.Name = "txtDateRevision";
            this.txtDateRevision.ReadOnly = false;
            this.txtDateRevision.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateRevision.Size = new System.Drawing.Size(261, 27);
            this.txtDateRevision.TabIndex = 6;
            this.txtDateRevision.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateRevision.UseSystemPasswordChar = false;
            this.txtDateRevision.Visible = false;
            this.txtDateRevision.Leave += new System.EventHandler(this.txtDateRevision_Leave);
            // 
            // txtDesignation2
            // 
            this.txtDesignation2.AccAcceptNumbersOnly = false;
            this.txtDesignation2.AccAllowComma = false;
            this.txtDesignation2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDesignation2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDesignation2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDesignation2.AccHidenValue = "";
            this.txtDesignation2.AccNotAllowedChars = null;
            this.txtDesignation2.AccReadOnly = false;
            this.txtDesignation2.AccReadOnlyAllowDelete = false;
            this.txtDesignation2.AccRequired = false;
            this.txtDesignation2.BackColor = System.Drawing.Color.White;
            this.txtDesignation2.CustomBackColor = System.Drawing.Color.White;
            this.txtDesignation2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDesignation2.ForeColor = System.Drawing.Color.Black;
            this.txtDesignation2.Location = new System.Drawing.Point(1347, 66);
            this.txtDesignation2.Margin = new System.Windows.Forms.Padding(1);
            this.txtDesignation2.MaxLength = 32767;
            this.txtDesignation2.Multiline = false;
            this.txtDesignation2.Name = "txtDesignation2";
            this.txtDesignation2.ReadOnly = false;
            this.txtDesignation2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDesignation2.Size = new System.Drawing.Size(96, 27);
            this.txtDesignation2.TabIndex = 511;
            this.txtDesignation2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDesignation2.UseSystemPasswordChar = false;
            this.txtDesignation2.Visible = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tableLayoutPanel11);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(358, 96);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(281, 558);
            this.groupBox3.TabIndex = 512;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Type de révision";
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.txtCON_Coef, 0, 6);
            this.tableLayoutPanel11.Controls.Add(this.optRevCoefFixe, 0, 5);
            this.tableLayoutPanel11.Controls.Add(this.optSansRev, 0, 3);
            this.tableLayoutPanel11.Controls.Add(this.optRevFormule, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.txtCON_TypeContrat, 0, 2);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 8;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(275, 534);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // txtCON_Coef
            // 
            this.txtCON_Coef.AccAcceptNumbersOnly = false;
            this.txtCON_Coef.AccAllowComma = false;
            this.txtCON_Coef.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Coef.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Coef.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Coef.AccHidenValue = "";
            this.txtCON_Coef.AccNotAllowedChars = null;
            this.txtCON_Coef.AccReadOnly = false;
            this.txtCON_Coef.AccReadOnlyAllowDelete = false;
            this.txtCON_Coef.AccRequired = false;
            this.txtCON_Coef.BackColor = System.Drawing.Color.White;
            this.txtCON_Coef.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Coef.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Coef.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Coef.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Coef.Location = new System.Drawing.Point(2, 203);
            this.txtCON_Coef.Margin = new System.Windows.Forms.Padding(2);
            this.txtCON_Coef.MaxLength = 32767;
            this.txtCON_Coef.Multiline = false;
            this.txtCON_Coef.Name = "txtCON_Coef";
            this.txtCON_Coef.ReadOnly = false;
            this.txtCON_Coef.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Coef.Size = new System.Drawing.Size(271, 27);
            this.txtCON_Coef.TabIndex = 3;
            this.txtCON_Coef.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Coef.UseSystemPasswordChar = false;
            this.txtCON_Coef.Visible = false;
            this.txtCON_Coef.Leave += new System.EventHandler(this.txtCON_Coef_Leave);
            // 
            // optRevCoefFixe
            // 
            this.optRevCoefFixe.AutoSize = true;
            this.optRevCoefFixe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optRevCoefFixe.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optRevCoefFixe.Location = new System.Drawing.Point(3, 155);
            this.optRevCoefFixe.Name = "optRevCoefFixe";
            this.optRevCoefFixe.Size = new System.Drawing.Size(269, 43);
            this.optRevCoefFixe.TabIndex = 2;
            this.optRevCoefFixe.TabStop = true;
            this.optRevCoefFixe.Text = "révision avec coéfficient fixe";
            this.optRevCoefFixe.UseVisualStyleBackColor = true;
            this.optRevCoefFixe.CheckedChanged += new System.EventHandler(this.optRevCoefFixe_CheckedChanged);
            // 
            // optSansRev
            // 
            this.optSansRev.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.optSansRev.AutoSize = true;
            this.optSansRev.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optSansRev.Location = new System.Drawing.Point(3, 99);
            this.optSansRev.Name = "optSansRev";
            this.optSansRev.Size = new System.Drawing.Size(269, 23);
            this.optSansRev.TabIndex = 539;
            this.optSansRev.TabStop = true;
            this.optSansRev.Text = "Sans révision";
            this.optSansRev.UseVisualStyleBackColor = true;
            this.optSansRev.CheckedChanged += new System.EventHandler(this.optSansRev_CheckedChanged);
            // 
            // optRevFormule
            // 
            this.optRevFormule.AutoSize = true;
            this.optRevFormule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optRevFormule.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optRevFormule.Location = new System.Drawing.Point(3, 26);
            this.optRevFormule.Name = "optRevFormule";
            this.optRevFormule.Size = new System.Drawing.Size(269, 31);
            this.optRevFormule.TabIndex = 0;
            this.optRevFormule.TabStop = true;
            this.optRevFormule.Text = "Révision avec formule";
            this.optRevFormule.UseVisualStyleBackColor = true;
            this.optRevFormule.CheckedChanged += new System.EventHandler(this.optRevFormule_CheckedChanged);
            // 
            // txtCON_TypeContrat
            // 
            this.txtCON_TypeContrat.AccAcceptNumbersOnly = false;
            this.txtCON_TypeContrat.AccAllowComma = false;
            this.txtCON_TypeContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_TypeContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_TypeContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_TypeContrat.AccHidenValue = "";
            this.txtCON_TypeContrat.AccNotAllowedChars = null;
            this.txtCON_TypeContrat.AccReadOnly = false;
            this.txtCON_TypeContrat.AccReadOnlyAllowDelete = false;
            this.txtCON_TypeContrat.AccRequired = false;
            this.txtCON_TypeContrat.BackColor = System.Drawing.Color.White;
            this.txtCON_TypeContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_TypeContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_TypeContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_TypeContrat.ForeColor = System.Drawing.Color.Black;
            this.txtCON_TypeContrat.Location = new System.Drawing.Point(2, 62);
            this.txtCON_TypeContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCON_TypeContrat.MaxLength = 32767;
            this.txtCON_TypeContrat.Multiline = false;
            this.txtCON_TypeContrat.Name = "txtCON_TypeContrat";
            this.txtCON_TypeContrat.ReadOnly = false;
            this.txtCON_TypeContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_TypeContrat.Size = new System.Drawing.Size(271, 27);
            this.txtCON_TypeContrat.TabIndex = 1;
            this.txtCON_TypeContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_TypeContrat.UseSystemPasswordChar = false;
            this.txtCON_TypeContrat.Visible = false;
            // 
            // fraFormule
            // 
            this.fraFormule.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel10.SetColumnSpan(this.fraFormule, 5);
            this.fraFormule.Controls.Add(this.tableLayoutPanel12);
            this.fraFormule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fraFormule.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.fraFormule.Location = new System.Drawing.Point(641, 96);
            this.fraFormule.Margin = new System.Windows.Forms.Padding(1);
            this.fraFormule.Name = "fraFormule";
            this.fraFormule.Size = new System.Drawing.Size(1206, 558);
            this.fraFormule.TabIndex = 513;
            this.fraFormule.TabStop = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 5;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 27.21065F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 21.93012F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.02982F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.82941F));
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel10, 2, 8);
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel9, 2, 7);
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel8, 2, 6);
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel7, 2, 5);
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel5, 2, 4);
            this.tableLayoutPanel12.Controls.Add(this.label15, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label16, 0, 2);
            this.tableLayoutPanel12.Controls.Add(this.label17, 1, 2);
            this.tableLayoutPanel12.Controls.Add(this.label36, 3, 2);
            this.tableLayoutPanel12.Controls.Add(this.label37, 4, 2);
            this.tableLayoutPanel12.Controls.Add(this.txtTypeFormule, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice1, 1, 3);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice3, 1, 5);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice2, 1, 4);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice4, 1, 6);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice6, 1, 8);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice1Valeur, 3, 3);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice2Valeur, 3, 4);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice3Valeur, 3, 5);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice4Valeur, 3, 6);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice5, 1, 7);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice5Valeur, 3, 7);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice6Valeur, 3, 8);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice1Date, 4, 3);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice2Date, 4, 4);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice3Date, 4, 5);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice4Date, 4, 6);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice5Date, 4, 7);
            this.tableLayoutPanel12.Controls.Add(this.txtIndice6Date, 4, 8);
            this.tableLayoutPanel12.Controls.Add(this.groupBox5, 0, 3);
            this.tableLayoutPanel12.Controls.Add(this.flowLayoutPanel4, 2, 3);
            this.tableLayoutPanel12.Controls.Add(this.Command2, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtFormule, 3, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 9;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1200, 537);
            this.tableLayoutPanel12.TabIndex = 0;
            // 
            // flowLayoutPanel10
            // 
            this.flowLayoutPanel10.Controls.Add(this.cmdIndice6);
            this.flowLayoutPanel10.Controls.Add(this.label43);
            this.flowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(561, 241);
            this.flowLayoutPanel10.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(58, 295);
            this.flowLayoutPanel10.TabIndex = 576;
            // 
            // cmdIndice6
            // 
            this.cmdIndice6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice6.FlatAppearance.BorderSize = 0;
            this.cmdIndice6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice6.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice6.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice6.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice6.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice6.Name = "cmdIndice6";
            this.cmdIndice6.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice6.TabIndex = 366;
            this.cmdIndice6.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice6.UseVisualStyleBackColor = false;
            this.cmdIndice6.Click += new System.EventHandler(this.cmdIndice6_Click);
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(29, 2);
            this.label43.Margin = new System.Windows.Forms.Padding(2);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(17, 19);
            this.label43.TabIndex = 384;
            this.label43.Text = "=";
            this.label43.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // flowLayoutPanel9
            // 
            this.flowLayoutPanel9.Controls.Add(this.cmdIndice5);
            this.flowLayoutPanel9.Controls.Add(this.label42);
            this.flowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(561, 211);
            this.flowLayoutPanel9.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(58, 28);
            this.flowLayoutPanel9.TabIndex = 575;
            // 
            // cmdIndice5
            // 
            this.cmdIndice5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice5.FlatAppearance.BorderSize = 0;
            this.cmdIndice5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice5.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice5.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice5.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice5.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice5.Name = "cmdIndice5";
            this.cmdIndice5.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice5.TabIndex = 366;
            this.cmdIndice5.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice5.UseVisualStyleBackColor = false;
            this.cmdIndice5.Click += new System.EventHandler(this.cmdIndice5_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(29, 2);
            this.label42.Margin = new System.Windows.Forms.Padding(2);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(17, 19);
            this.label42.TabIndex = 384;
            this.label42.Text = "=";
            this.label42.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // flowLayoutPanel8
            // 
            this.flowLayoutPanel8.Controls.Add(this.cmdIndice4);
            this.flowLayoutPanel8.Controls.Add(this.label41);
            this.flowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(561, 181);
            this.flowLayoutPanel8.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.Size = new System.Drawing.Size(58, 28);
            this.flowLayoutPanel8.TabIndex = 574;
            // 
            // cmdIndice4
            // 
            this.cmdIndice4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice4.FlatAppearance.BorderSize = 0;
            this.cmdIndice4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice4.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice4.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice4.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice4.Name = "cmdIndice4";
            this.cmdIndice4.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice4.TabIndex = 366;
            this.cmdIndice4.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice4.UseVisualStyleBackColor = false;
            this.cmdIndice4.Click += new System.EventHandler(this.cmdIndice4_Click);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(29, 2);
            this.label41.Margin = new System.Windows.Forms.Padding(2);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(17, 19);
            this.label41.TabIndex = 384;
            this.label41.Text = "=";
            this.label41.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.cmdIndice3);
            this.flowLayoutPanel7.Controls.Add(this.label40);
            this.flowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(561, 151);
            this.flowLayoutPanel7.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(58, 28);
            this.flowLayoutPanel7.TabIndex = 573;
            // 
            // cmdIndice3
            // 
            this.cmdIndice3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice3.FlatAppearance.BorderSize = 0;
            this.cmdIndice3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice3.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice3.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice3.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice3.Name = "cmdIndice3";
            this.cmdIndice3.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice3.TabIndex = 366;
            this.cmdIndice3.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice3.UseVisualStyleBackColor = false;
            this.cmdIndice3.Click += new System.EventHandler(this.cmdIndice3_Click);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(29, 2);
            this.label40.Margin = new System.Windows.Forms.Padding(2);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(17, 19);
            this.label40.TabIndex = 384;
            this.label40.Text = "=";
            this.label40.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Controls.Add(this.cmdIndice2);
            this.flowLayoutPanel5.Controls.Add(this.label39);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(561, 121);
            this.flowLayoutPanel5.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(58, 28);
            this.flowLayoutPanel5.TabIndex = 572;
            // 
            // cmdIndice2
            // 
            this.cmdIndice2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice2.FlatAppearance.BorderSize = 0;
            this.cmdIndice2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice2.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice2.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice2.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice2.Name = "cmdIndice2";
            this.cmdIndice2.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice2.TabIndex = 366;
            this.cmdIndice2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice2.UseVisualStyleBackColor = false;
            this.cmdIndice2.Click += new System.EventHandler(this.cmdIndice2_Click);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(29, 2);
            this.label39.Margin = new System.Windows.Forms.Padding(2);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(17, 19);
            this.label39.TabIndex = 0;
            this.label39.Text = "=";
            this.label39.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(1, 1);
            this.label15.Margin = new System.Windows.Forms.Padding(1);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(308, 28);
            this.label15.TabIndex = 384;
            this.label15.Text = "Formule de revision";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1, 61);
            this.label16.Margin = new System.Windows.Forms.Padding(1);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(308, 28);
            this.label16.TabIndex = 385;
            this.label16.Text = "Indices de Base";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(311, 61);
            this.label17.Margin = new System.Windows.Forms.Padding(1);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(248, 28);
            this.label17.TabIndex = 386;
            this.label17.Text = "Indice";
            this.label17.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(621, 61);
            this.label36.Margin = new System.Windows.Forms.Padding(1);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(271, 28);
            this.label36.TabIndex = 387;
            this.label36.Text = "Valeur";
            this.label36.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(894, 61);
            this.label37.Margin = new System.Windows.Forms.Padding(1);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(305, 28);
            this.label37.TabIndex = 388;
            this.label37.Text = "Date";
            this.label37.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtTypeFormule
            // 
            this.txtTypeFormule.AccAcceptNumbersOnly = false;
            this.txtTypeFormule.AccAllowComma = false;
            this.txtTypeFormule.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTypeFormule.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTypeFormule.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTypeFormule.AccHidenValue = "";
            this.txtTypeFormule.AccNotAllowedChars = null;
            this.txtTypeFormule.AccReadOnly = false;
            this.txtTypeFormule.AccReadOnlyAllowDelete = false;
            this.txtTypeFormule.AccRequired = false;
            this.txtTypeFormule.BackColor = System.Drawing.Color.White;
            this.txtTypeFormule.CustomBackColor = System.Drawing.Color.White;
            this.txtTypeFormule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTypeFormule.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTypeFormule.ForeColor = System.Drawing.Color.Black;
            this.txtTypeFormule.Location = new System.Drawing.Point(311, 1);
            this.txtTypeFormule.Margin = new System.Windows.Forms.Padding(1);
            this.txtTypeFormule.MaxLength = 32767;
            this.txtTypeFormule.Multiline = false;
            this.txtTypeFormule.Name = "txtTypeFormule";
            this.txtTypeFormule.ReadOnly = false;
            this.txtTypeFormule.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTypeFormule.Size = new System.Drawing.Size(248, 27);
            this.txtTypeFormule.TabIndex = 0;
            this.txtTypeFormule.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTypeFormule.UseSystemPasswordChar = false;
            this.txtTypeFormule.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTypeFormule_KeyPress);
            // 
            // txtIndice1
            // 
            this.txtIndice1.AccAcceptNumbersOnly = false;
            this.txtIndice1.AccAllowComma = false;
            this.txtIndice1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice1.AccHidenValue = "";
            this.txtIndice1.AccNotAllowedChars = null;
            this.txtIndice1.AccReadOnly = false;
            this.txtIndice1.AccReadOnlyAllowDelete = false;
            this.txtIndice1.AccRequired = false;
            this.txtIndice1.BackColor = System.Drawing.Color.White;
            this.txtIndice1.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice1.ForeColor = System.Drawing.Color.Black;
            this.txtIndice1.Location = new System.Drawing.Point(311, 91);
            this.txtIndice1.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice1.MaxLength = 32767;
            this.txtIndice1.Multiline = false;
            this.txtIndice1.Name = "txtIndice1";
            this.txtIndice1.ReadOnly = false;
            this.txtIndice1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice1.Size = new System.Drawing.Size(248, 27);
            this.txtIndice1.TabIndex = 2;
            this.txtIndice1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice1.UseSystemPasswordChar = false;
            // 
            // txtIndice3
            // 
            this.txtIndice3.AccAcceptNumbersOnly = false;
            this.txtIndice3.AccAllowComma = false;
            this.txtIndice3.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice3.AccHidenValue = "";
            this.txtIndice3.AccNotAllowedChars = null;
            this.txtIndice3.AccReadOnly = false;
            this.txtIndice3.AccReadOnlyAllowDelete = false;
            this.txtIndice3.AccRequired = false;
            this.txtIndice3.BackColor = System.Drawing.Color.White;
            this.txtIndice3.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice3.ForeColor = System.Drawing.Color.Black;
            this.txtIndice3.Location = new System.Drawing.Point(311, 151);
            this.txtIndice3.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice3.MaxLength = 32767;
            this.txtIndice3.Multiline = false;
            this.txtIndice3.Name = "txtIndice3";
            this.txtIndice3.ReadOnly = false;
            this.txtIndice3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice3.Size = new System.Drawing.Size(248, 27);
            this.txtIndice3.TabIndex = 8;
            this.txtIndice3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice3.UseSystemPasswordChar = false;
            // 
            // txtIndice2
            // 
            this.txtIndice2.AccAcceptNumbersOnly = false;
            this.txtIndice2.AccAllowComma = false;
            this.txtIndice2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice2.AccHidenValue = "";
            this.txtIndice2.AccNotAllowedChars = null;
            this.txtIndice2.AccReadOnly = false;
            this.txtIndice2.AccReadOnlyAllowDelete = false;
            this.txtIndice2.AccRequired = false;
            this.txtIndice2.BackColor = System.Drawing.Color.White;
            this.txtIndice2.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice2.ForeColor = System.Drawing.Color.Black;
            this.txtIndice2.Location = new System.Drawing.Point(311, 121);
            this.txtIndice2.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice2.MaxLength = 32767;
            this.txtIndice2.Multiline = false;
            this.txtIndice2.Name = "txtIndice2";
            this.txtIndice2.ReadOnly = false;
            this.txtIndice2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice2.Size = new System.Drawing.Size(248, 27);
            this.txtIndice2.TabIndex = 5;
            this.txtIndice2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice2.UseSystemPasswordChar = false;
            // 
            // txtIndice4
            // 
            this.txtIndice4.AccAcceptNumbersOnly = false;
            this.txtIndice4.AccAllowComma = false;
            this.txtIndice4.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice4.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice4.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice4.AccHidenValue = "";
            this.txtIndice4.AccNotAllowedChars = null;
            this.txtIndice4.AccReadOnly = false;
            this.txtIndice4.AccReadOnlyAllowDelete = false;
            this.txtIndice4.AccRequired = false;
            this.txtIndice4.BackColor = System.Drawing.Color.White;
            this.txtIndice4.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice4.ForeColor = System.Drawing.Color.Black;
            this.txtIndice4.Location = new System.Drawing.Point(311, 181);
            this.txtIndice4.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice4.MaxLength = 32767;
            this.txtIndice4.Multiline = false;
            this.txtIndice4.Name = "txtIndice4";
            this.txtIndice4.ReadOnly = false;
            this.txtIndice4.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice4.Size = new System.Drawing.Size(248, 27);
            this.txtIndice4.TabIndex = 11;
            this.txtIndice4.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice4.UseSystemPasswordChar = false;
            // 
            // txtIndice6
            // 
            this.txtIndice6.AccAcceptNumbersOnly = false;
            this.txtIndice6.AccAllowComma = false;
            this.txtIndice6.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice6.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice6.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice6.AccHidenValue = "";
            this.txtIndice6.AccNotAllowedChars = null;
            this.txtIndice6.AccReadOnly = false;
            this.txtIndice6.AccReadOnlyAllowDelete = false;
            this.txtIndice6.AccRequired = false;
            this.txtIndice6.BackColor = System.Drawing.Color.White;
            this.txtIndice6.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice6.ForeColor = System.Drawing.Color.Black;
            this.txtIndice6.Location = new System.Drawing.Point(311, 241);
            this.txtIndice6.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice6.MaxLength = 32767;
            this.txtIndice6.Multiline = false;
            this.txtIndice6.Name = "txtIndice6";
            this.txtIndice6.ReadOnly = false;
            this.txtIndice6.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice6.Size = new System.Drawing.Size(248, 27);
            this.txtIndice6.TabIndex = 17;
            this.txtIndice6.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice6.UseSystemPasswordChar = false;
            // 
            // txtIndice1Valeur
            // 
            this.txtIndice1Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice1Valeur.AccAllowComma = false;
            this.txtIndice1Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice1Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice1Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice1Valeur.AccHidenValue = "";
            this.txtIndice1Valeur.AccNotAllowedChars = null;
            this.txtIndice1Valeur.AccReadOnly = false;
            this.txtIndice1Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice1Valeur.AccRequired = false;
            this.txtIndice1Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice1Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice1Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice1Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice1Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice1Valeur.Location = new System.Drawing.Point(622, 92);
            this.txtIndice1Valeur.Margin = new System.Windows.Forms.Padding(2);
            this.txtIndice1Valeur.MaxLength = 32767;
            this.txtIndice1Valeur.Multiline = false;
            this.txtIndice1Valeur.Name = "txtIndice1Valeur";
            this.txtIndice1Valeur.ReadOnly = false;
            this.txtIndice1Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice1Valeur.Size = new System.Drawing.Size(269, 27);
            this.txtIndice1Valeur.TabIndex = 3;
            this.txtIndice1Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice1Valeur.UseSystemPasswordChar = false;
            this.txtIndice1Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice1Valeur_Validating);
            // 
            // txtIndice2Valeur
            // 
            this.txtIndice2Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice2Valeur.AccAllowComma = false;
            this.txtIndice2Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice2Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice2Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice2Valeur.AccHidenValue = "";
            this.txtIndice2Valeur.AccNotAllowedChars = null;
            this.txtIndice2Valeur.AccReadOnly = false;
            this.txtIndice2Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice2Valeur.AccRequired = false;
            this.txtIndice2Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice2Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice2Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice2Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice2Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice2Valeur.Location = new System.Drawing.Point(621, 121);
            this.txtIndice2Valeur.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice2Valeur.MaxLength = 32767;
            this.txtIndice2Valeur.Multiline = false;
            this.txtIndice2Valeur.Name = "txtIndice2Valeur";
            this.txtIndice2Valeur.ReadOnly = false;
            this.txtIndice2Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice2Valeur.Size = new System.Drawing.Size(271, 27);
            this.txtIndice2Valeur.TabIndex = 6;
            this.txtIndice2Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice2Valeur.UseSystemPasswordChar = false;
            this.txtIndice2Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice2Valeur_Validating);
            // 
            // txtIndice3Valeur
            // 
            this.txtIndice3Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice3Valeur.AccAllowComma = false;
            this.txtIndice3Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice3Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice3Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice3Valeur.AccHidenValue = "";
            this.txtIndice3Valeur.AccNotAllowedChars = null;
            this.txtIndice3Valeur.AccReadOnly = false;
            this.txtIndice3Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice3Valeur.AccRequired = false;
            this.txtIndice3Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice3Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice3Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice3Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice3Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice3Valeur.Location = new System.Drawing.Point(622, 152);
            this.txtIndice3Valeur.Margin = new System.Windows.Forms.Padding(2);
            this.txtIndice3Valeur.MaxLength = 32767;
            this.txtIndice3Valeur.Multiline = false;
            this.txtIndice3Valeur.Name = "txtIndice3Valeur";
            this.txtIndice3Valeur.ReadOnly = false;
            this.txtIndice3Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice3Valeur.Size = new System.Drawing.Size(269, 27);
            this.txtIndice3Valeur.TabIndex = 9;
            this.txtIndice3Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice3Valeur.UseSystemPasswordChar = false;
            this.txtIndice3Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice3Valeur_Validating);
            // 
            // txtIndice4Valeur
            // 
            this.txtIndice4Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice4Valeur.AccAllowComma = false;
            this.txtIndice4Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice4Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice4Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice4Valeur.AccHidenValue = "";
            this.txtIndice4Valeur.AccNotAllowedChars = null;
            this.txtIndice4Valeur.AccReadOnly = false;
            this.txtIndice4Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice4Valeur.AccRequired = false;
            this.txtIndice4Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice4Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice4Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice4Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice4Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice4Valeur.Location = new System.Drawing.Point(621, 181);
            this.txtIndice4Valeur.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice4Valeur.MaxLength = 32767;
            this.txtIndice4Valeur.Multiline = false;
            this.txtIndice4Valeur.Name = "txtIndice4Valeur";
            this.txtIndice4Valeur.ReadOnly = false;
            this.txtIndice4Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice4Valeur.Size = new System.Drawing.Size(271, 27);
            this.txtIndice4Valeur.TabIndex = 12;
            this.txtIndice4Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice4Valeur.UseSystemPasswordChar = false;
            this.txtIndice4Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice4Valeur_Validating);
            // 
            // txtIndice5
            // 
            this.txtIndice5.AccAcceptNumbersOnly = false;
            this.txtIndice5.AccAllowComma = false;
            this.txtIndice5.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice5.AccHidenValue = "";
            this.txtIndice5.AccNotAllowedChars = null;
            this.txtIndice5.AccReadOnly = false;
            this.txtIndice5.AccReadOnlyAllowDelete = false;
            this.txtIndice5.AccRequired = false;
            this.txtIndice5.BackColor = System.Drawing.Color.White;
            this.txtIndice5.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice5.ForeColor = System.Drawing.Color.Black;
            this.txtIndice5.Location = new System.Drawing.Point(311, 211);
            this.txtIndice5.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice5.MaxLength = 32767;
            this.txtIndice5.Multiline = false;
            this.txtIndice5.Name = "txtIndice5";
            this.txtIndice5.ReadOnly = false;
            this.txtIndice5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice5.Size = new System.Drawing.Size(248, 27);
            this.txtIndice5.TabIndex = 14;
            this.txtIndice5.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice5.UseSystemPasswordChar = false;
            // 
            // txtIndice5Valeur
            // 
            this.txtIndice5Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice5Valeur.AccAllowComma = false;
            this.txtIndice5Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice5Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice5Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice5Valeur.AccHidenValue = "";
            this.txtIndice5Valeur.AccNotAllowedChars = null;
            this.txtIndice5Valeur.AccReadOnly = false;
            this.txtIndice5Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice5Valeur.AccRequired = false;
            this.txtIndice5Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice5Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice5Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice5Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice5Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice5Valeur.Location = new System.Drawing.Point(621, 211);
            this.txtIndice5Valeur.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice5Valeur.MaxLength = 32767;
            this.txtIndice5Valeur.Multiline = false;
            this.txtIndice5Valeur.Name = "txtIndice5Valeur";
            this.txtIndice5Valeur.ReadOnly = false;
            this.txtIndice5Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice5Valeur.Size = new System.Drawing.Size(271, 27);
            this.txtIndice5Valeur.TabIndex = 15;
            this.txtIndice5Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice5Valeur.UseSystemPasswordChar = false;
            this.txtIndice5Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice5Valeur_Validating);
            // 
            // txtIndice6Valeur
            // 
            this.txtIndice6Valeur.AccAcceptNumbersOnly = false;
            this.txtIndice6Valeur.AccAllowComma = false;
            this.txtIndice6Valeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice6Valeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice6Valeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice6Valeur.AccHidenValue = "";
            this.txtIndice6Valeur.AccNotAllowedChars = null;
            this.txtIndice6Valeur.AccReadOnly = false;
            this.txtIndice6Valeur.AccReadOnlyAllowDelete = false;
            this.txtIndice6Valeur.AccRequired = false;
            this.txtIndice6Valeur.BackColor = System.Drawing.Color.White;
            this.txtIndice6Valeur.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice6Valeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice6Valeur.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice6Valeur.ForeColor = System.Drawing.Color.Black;
            this.txtIndice6Valeur.Location = new System.Drawing.Point(621, 241);
            this.txtIndice6Valeur.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice6Valeur.MaxLength = 32767;
            this.txtIndice6Valeur.Multiline = false;
            this.txtIndice6Valeur.Name = "txtIndice6Valeur";
            this.txtIndice6Valeur.ReadOnly = false;
            this.txtIndice6Valeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice6Valeur.Size = new System.Drawing.Size(271, 27);
            this.txtIndice6Valeur.TabIndex = 18;
            this.txtIndice6Valeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice6Valeur.UseSystemPasswordChar = false;
            this.txtIndice6Valeur.Validating += new System.ComponentModel.CancelEventHandler(this.txtIndice6Valeur_Validating);
            // 
            // txtIndice1Date
            // 
            this.txtIndice1Date.AccAcceptNumbersOnly = false;
            this.txtIndice1Date.AccAllowComma = false;
            this.txtIndice1Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice1Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice1Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice1Date.AccHidenValue = "";
            this.txtIndice1Date.AccNotAllowedChars = null;
            this.txtIndice1Date.AccReadOnly = false;
            this.txtIndice1Date.AccReadOnlyAllowDelete = false;
            this.txtIndice1Date.AccRequired = false;
            this.txtIndice1Date.BackColor = System.Drawing.Color.White;
            this.txtIndice1Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice1Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice1Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice1Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice1Date.Location = new System.Drawing.Point(894, 91);
            this.txtIndice1Date.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice1Date.MaxLength = 32767;
            this.txtIndice1Date.Multiline = false;
            this.txtIndice1Date.Name = "txtIndice1Date";
            this.txtIndice1Date.ReadOnly = false;
            this.txtIndice1Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice1Date.Size = new System.Drawing.Size(305, 27);
            this.txtIndice1Date.TabIndex = 4;
            this.txtIndice1Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice1Date.UseSystemPasswordChar = false;
            this.txtIndice1Date.Leave += new System.EventHandler(this.txtIndice1Date_Leave);
            // 
            // txtIndice2Date
            // 
            this.txtIndice2Date.AccAcceptNumbersOnly = false;
            this.txtIndice2Date.AccAllowComma = false;
            this.txtIndice2Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice2Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice2Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice2Date.AccHidenValue = "";
            this.txtIndice2Date.AccNotAllowedChars = null;
            this.txtIndice2Date.AccReadOnly = false;
            this.txtIndice2Date.AccReadOnlyAllowDelete = false;
            this.txtIndice2Date.AccRequired = false;
            this.txtIndice2Date.BackColor = System.Drawing.Color.White;
            this.txtIndice2Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice2Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice2Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice2Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice2Date.Location = new System.Drawing.Point(894, 121);
            this.txtIndice2Date.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice2Date.MaxLength = 32767;
            this.txtIndice2Date.Multiline = false;
            this.txtIndice2Date.Name = "txtIndice2Date";
            this.txtIndice2Date.ReadOnly = false;
            this.txtIndice2Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice2Date.Size = new System.Drawing.Size(305, 27);
            this.txtIndice2Date.TabIndex = 7;
            this.txtIndice2Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice2Date.UseSystemPasswordChar = false;
            this.txtIndice2Date.Leave += new System.EventHandler(this.txtIndice2Date_Leave);
            // 
            // txtIndice3Date
            // 
            this.txtIndice3Date.AccAcceptNumbersOnly = false;
            this.txtIndice3Date.AccAllowComma = false;
            this.txtIndice3Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice3Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice3Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice3Date.AccHidenValue = "";
            this.txtIndice3Date.AccNotAllowedChars = null;
            this.txtIndice3Date.AccReadOnly = false;
            this.txtIndice3Date.AccReadOnlyAllowDelete = false;
            this.txtIndice3Date.AccRequired = false;
            this.txtIndice3Date.BackColor = System.Drawing.Color.White;
            this.txtIndice3Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice3Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice3Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice3Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice3Date.Location = new System.Drawing.Point(894, 151);
            this.txtIndice3Date.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice3Date.MaxLength = 32767;
            this.txtIndice3Date.Multiline = false;
            this.txtIndice3Date.Name = "txtIndice3Date";
            this.txtIndice3Date.ReadOnly = false;
            this.txtIndice3Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice3Date.Size = new System.Drawing.Size(305, 27);
            this.txtIndice3Date.TabIndex = 10;
            this.txtIndice3Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice3Date.UseSystemPasswordChar = false;
            this.txtIndice3Date.Leave += new System.EventHandler(this.txtIndice3Date_Leave);
            // 
            // txtIndice4Date
            // 
            this.txtIndice4Date.AccAcceptNumbersOnly = false;
            this.txtIndice4Date.AccAllowComma = false;
            this.txtIndice4Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice4Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice4Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice4Date.AccHidenValue = "";
            this.txtIndice4Date.AccNotAllowedChars = null;
            this.txtIndice4Date.AccReadOnly = false;
            this.txtIndice4Date.AccReadOnlyAllowDelete = false;
            this.txtIndice4Date.AccRequired = false;
            this.txtIndice4Date.BackColor = System.Drawing.Color.White;
            this.txtIndice4Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice4Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice4Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice4Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice4Date.Location = new System.Drawing.Point(895, 182);
            this.txtIndice4Date.Margin = new System.Windows.Forms.Padding(2);
            this.txtIndice4Date.MaxLength = 32767;
            this.txtIndice4Date.Multiline = false;
            this.txtIndice4Date.Name = "txtIndice4Date";
            this.txtIndice4Date.ReadOnly = false;
            this.txtIndice4Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice4Date.Size = new System.Drawing.Size(303, 27);
            this.txtIndice4Date.TabIndex = 13;
            this.txtIndice4Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice4Date.UseSystemPasswordChar = false;
            this.txtIndice4Date.Leave += new System.EventHandler(this.txtIndice4Date_Leave);
            // 
            // txtIndice5Date
            // 
            this.txtIndice5Date.AccAcceptNumbersOnly = false;
            this.txtIndice5Date.AccAllowComma = false;
            this.txtIndice5Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice5Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice5Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice5Date.AccHidenValue = "";
            this.txtIndice5Date.AccNotAllowedChars = null;
            this.txtIndice5Date.AccReadOnly = false;
            this.txtIndice5Date.AccReadOnlyAllowDelete = false;
            this.txtIndice5Date.AccRequired = false;
            this.txtIndice5Date.BackColor = System.Drawing.Color.White;
            this.txtIndice5Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice5Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice5Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice5Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice5Date.Location = new System.Drawing.Point(894, 211);
            this.txtIndice5Date.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice5Date.MaxLength = 32767;
            this.txtIndice5Date.Multiline = false;
            this.txtIndice5Date.Name = "txtIndice5Date";
            this.txtIndice5Date.ReadOnly = false;
            this.txtIndice5Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice5Date.Size = new System.Drawing.Size(305, 27);
            this.txtIndice5Date.TabIndex = 16;
            this.txtIndice5Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice5Date.UseSystemPasswordChar = false;
            this.txtIndice5Date.Leave += new System.EventHandler(this.txtIndice5Date_Leave);
            // 
            // txtIndice6Date
            // 
            this.txtIndice6Date.AccAcceptNumbersOnly = false;
            this.txtIndice6Date.AccAllowComma = false;
            this.txtIndice6Date.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIndice6Date.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIndice6Date.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIndice6Date.AccHidenValue = "";
            this.txtIndice6Date.AccNotAllowedChars = null;
            this.txtIndice6Date.AccReadOnly = false;
            this.txtIndice6Date.AccReadOnlyAllowDelete = false;
            this.txtIndice6Date.AccRequired = false;
            this.txtIndice6Date.BackColor = System.Drawing.Color.White;
            this.txtIndice6Date.CustomBackColor = System.Drawing.Color.White;
            this.txtIndice6Date.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIndice6Date.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIndice6Date.ForeColor = System.Drawing.Color.Black;
            this.txtIndice6Date.Location = new System.Drawing.Point(894, 241);
            this.txtIndice6Date.Margin = new System.Windows.Forms.Padding(1);
            this.txtIndice6Date.MaxLength = 32767;
            this.txtIndice6Date.Multiline = false;
            this.txtIndice6Date.Name = "txtIndice6Date";
            this.txtIndice6Date.ReadOnly = false;
            this.txtIndice6Date.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIndice6Date.Size = new System.Drawing.Size(305, 27);
            this.txtIndice6Date.TabIndex = 19;
            this.txtIndice6Date.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIndice6Date.UseSystemPasswordChar = false;
            this.txtIndice6Date.Leave += new System.EventHandler(this.txtIndice6Date_Leave);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.Check2);
            this.groupBox5.Controls.Add(this.Check1);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox5.Location = new System.Drawing.Point(1, 91);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel12.SetRowSpan(this.groupBox5, 3);
            this.groupBox5.Size = new System.Drawing.Size(308, 88);
            this.groupBox5.TabIndex = 570;
            this.groupBox5.TabStop = false;
            // 
            // Check2
            // 
            this.Check2.AutoSize = true;
            this.Check2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check2.Location = new System.Drawing.Point(1, 42);
            this.Check2.Name = "Check2";
            this.Check2.Padding = new System.Windows.Forms.Padding(4);
            this.Check2.Size = new System.Drawing.Size(82, 31);
            this.Check2.TabIndex = 571;
            this.Check2.Text = "Connu";
            this.Check2.UseVisualStyleBackColor = true;
            this.Check2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Check2_MouseUp);
            // 
            // Check1
            // 
            this.Check1.AutoSize = true;
            this.Check1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check1.Location = new System.Drawing.Point(1, 15);
            this.Check1.Name = "Check1";
            this.Check1.Padding = new System.Windows.Forms.Padding(4);
            this.Check1.Size = new System.Drawing.Size(66, 31);
            this.Check1.TabIndex = 570;
            this.Check1.Text = "Réel";
            this.Check1.UseVisualStyleBackColor = true;
            this.Check1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.Check1_MouseUp);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.cmdIndice1);
            this.flowLayoutPanel4.Controls.Add(this.label38);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(561, 91);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(58, 28);
            this.flowLayoutPanel4.TabIndex = 571;
            // 
            // cmdIndice1
            // 
            this.cmdIndice1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIndice1.FlatAppearance.BorderSize = 0;
            this.cmdIndice1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIndice1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIndice1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIndice1.Location = new System.Drawing.Point(1, 1);
            this.cmdIndice1.Margin = new System.Windows.Forms.Padding(1);
            this.cmdIndice1.Name = "cmdIndice1";
            this.cmdIndice1.Size = new System.Drawing.Size(25, 20);
            this.cmdIndice1.TabIndex = 366;
            this.cmdIndice1.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdIndice1.UseVisualStyleBackColor = false;
            this.cmdIndice1.Click += new System.EventHandler(this.cmdIndice1_Click);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(29, 2);
            this.label38.Margin = new System.Windows.Forms.Padding(2);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(17, 19);
            this.label38.TabIndex = 384;
            this.label38.Text = "=";
            this.label38.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command2.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command2.Location = new System.Drawing.Point(563, 3);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(25, 19);
            this.Command2.TabIndex = 577;
            this.Command2.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // txtFormule
            // 
            this.txtFormule.AccAcceptNumbersOnly = false;
            this.txtFormule.AccAllowComma = false;
            this.txtFormule.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFormule.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFormule.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFormule.AccHidenValue = "";
            this.txtFormule.AccNotAllowedChars = null;
            this.txtFormule.AccReadOnly = false;
            this.txtFormule.AccReadOnlyAllowDelete = false;
            this.txtFormule.AccRequired = false;
            this.txtFormule.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel12.SetColumnSpan(this.txtFormule, 2);
            this.txtFormule.CustomBackColor = System.Drawing.Color.White;
            this.txtFormule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFormule.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFormule.ForeColor = System.Drawing.Color.Blue;
            this.txtFormule.Location = new System.Drawing.Point(622, 2);
            this.txtFormule.Margin = new System.Windows.Forms.Padding(2);
            this.txtFormule.MaxLength = 32767;
            this.txtFormule.Multiline = true;
            this.txtFormule.Name = "txtFormule";
            this.txtFormule.ReadOnly = true;
            this.tableLayoutPanel12.SetRowSpan(this.txtFormule, 2);
            this.txtFormule.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFormule.Size = new System.Drawing.Size(576, 56);
            this.txtFormule.TabIndex = 1;
            this.txtFormule.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFormule.UseSystemPasswordChar = false;
            this.txtFormule.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFormule_KeyPress);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(3, 655);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(116, 16);
            this.label44.TabIndex = 514;
            this.label44.Text = "Analytique activité";
            this.label44.Visible = false;
            // 
            // txtCGINITULE
            // 
            this.txtCGINITULE.AccAcceptNumbersOnly = false;
            this.txtCGINITULE.AccAllowComma = false;
            this.txtCGINITULE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCGINITULE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCGINITULE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCGINITULE.AccHidenValue = "";
            this.txtCGINITULE.AccNotAllowedChars = null;
            this.txtCGINITULE.AccReadOnly = false;
            this.txtCGINITULE.AccReadOnlyAllowDelete = false;
            this.txtCGINITULE.AccRequired = false;
            this.txtCGINITULE.BackColor = System.Drawing.Color.White;
            this.txtCGINITULE.CustomBackColor = System.Drawing.Color.White;
            this.txtCGINITULE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCGINITULE.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCGINITULE.ForeColor = System.Drawing.Color.Black;
            this.txtCGINITULE.Location = new System.Drawing.Point(1039, 657);
            this.txtCGINITULE.Margin = new System.Windows.Forms.Padding(2);
            this.txtCGINITULE.MaxLength = 32767;
            this.txtCGINITULE.Multiline = false;
            this.txtCGINITULE.Name = "txtCGINITULE";
            this.txtCGINITULE.ReadOnly = false;
            this.txtCGINITULE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCGINITULE.Size = new System.Drawing.Size(305, 27);
            this.txtCGINITULE.TabIndex = 517;
            this.txtCGINITULE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCGINITULE.UseSystemPasswordChar = false;
            this.txtCGINITULE.Visible = false;
            // 
            // txtCGNUM
            // 
            this.txtCGNUM.AccAcceptNumbersOnly = false;
            this.txtCGNUM.AccAllowComma = false;
            this.txtCGNUM.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCGNUM.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCGNUM.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCGNUM.AccHidenValue = "";
            this.txtCGNUM.AccNotAllowedChars = null;
            this.txtCGNUM.AccReadOnly = false;
            this.txtCGNUM.AccReadOnlyAllowDelete = false;
            this.txtCGNUM.AccRequired = false;
            this.txtCGNUM.BackColor = System.Drawing.Color.White;
            this.txtCGNUM.CustomBackColor = System.Drawing.Color.White;
            this.txtCGNUM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCGNUM.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCGNUM.ForeColor = System.Drawing.Color.Black;
            this.txtCGNUM.Location = new System.Drawing.Point(1348, 657);
            this.txtCGNUM.Margin = new System.Windows.Forms.Padding(2);
            this.txtCGNUM.MaxLength = 32767;
            this.txtCGNUM.Multiline = false;
            this.txtCGNUM.Name = "txtCGNUM";
            this.txtCGNUM.ReadOnly = false;
            this.txtCGNUM.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCGNUM.Size = new System.Drawing.Size(235, 27);
            this.txtCGNUM.TabIndex = 518;
            this.txtCGNUM.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCGNUM.UseSystemPasswordChar = false;
            this.txtCGNUM.Visible = false;
            // 
            // txtLibelleActivite
            // 
            this.txtLibelleActivite.AccAcceptNumbersOnly = false;
            this.txtLibelleActivite.AccAllowComma = false;
            this.txtLibelleActivite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleActivite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleActivite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleActivite.AccHidenValue = "";
            this.txtLibelleActivite.AccNotAllowedChars = null;
            this.txtLibelleActivite.AccReadOnly = false;
            this.txtLibelleActivite.AccReadOnlyAllowDelete = false;
            this.txtLibelleActivite.AccRequired = false;
            this.txtLibelleActivite.BackColor = System.Drawing.Color.White;
            this.txtLibelleActivite.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleActivite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleActivite.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibelleActivite.ForeColor = System.Drawing.Color.Black;
            this.txtLibelleActivite.Location = new System.Drawing.Point(1587, 657);
            this.txtLibelleActivite.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleActivite.MaxLength = 32767;
            this.txtLibelleActivite.Multiline = false;
            this.txtLibelleActivite.Name = "txtLibelleActivite";
            this.txtLibelleActivite.ReadOnly = false;
            this.txtLibelleActivite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleActivite.Size = new System.Drawing.Size(259, 27);
            this.txtLibelleActivite.TabIndex = 519;
            this.txtLibelleActivite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleActivite.UseSystemPasswordChar = false;
            this.txtLibelleActivite.Visible = false;
            // 
            // Frame14
            // 
            this.Frame14.BackColor = System.Drawing.Color.Transparent;
            this.Frame14.Controls.Add(this.tableLayoutPanel13);
            this.Frame14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame14.Location = new System.Drawing.Point(1, 96);
            this.Frame14.Margin = new System.Windows.Forms.Padding(1);
            this.Frame14.Name = "Frame14";
            this.Frame14.Size = new System.Drawing.Size(355, 558);
            this.Frame14.TabIndex = 520;
            this.Frame14.TabStop = false;
            this.Frame14.Text = "Analytique";
            this.Frame14.Visible = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 3;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel13.Controls.Add(this.label46, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.label47, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.txtActivite, 1, 0);
            this.tableLayoutPanel13.Controls.Add(this.txtAnalytique, 1, 2);
            this.tableLayoutPanel13.Controls.Add(this.txtCON_AnaModif, 1, 3);
            this.tableLayoutPanel13.Controls.Add(this.cmdActivite, 2, 0);
            this.tableLayoutPanel13.Controls.Add(this.cmdSecteur, 2, 2);
            this.tableLayoutPanel13.Controls.Add(this.ckkCON_AnaModif, 1, 1);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 7;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(349, 534);
            this.tableLayoutPanel13.TabIndex = 0;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(3, 0);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(151, 30);
            this.label46.TabIndex = 384;
            this.label46.Text = "Activité";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(3, 60);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(151, 30);
            this.label47.TabIndex = 385;
            this.label47.Text = "Secteur";
            // 
            // txtActivite
            // 
            this.txtActivite.AccAcceptNumbersOnly = false;
            this.txtActivite.AccAllowComma = false;
            this.txtActivite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtActivite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtActivite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtActivite.AccHidenValue = "";
            this.txtActivite.AccNotAllowedChars = null;
            this.txtActivite.AccReadOnly = false;
            this.txtActivite.AccReadOnlyAllowDelete = false;
            this.txtActivite.AccRequired = false;
            this.txtActivite.BackColor = System.Drawing.Color.White;
            this.txtActivite.CustomBackColor = System.Drawing.Color.White;
            this.txtActivite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtActivite.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtActivite.ForeColor = System.Drawing.Color.Black;
            this.txtActivite.Location = new System.Drawing.Point(159, 2);
            this.txtActivite.Margin = new System.Windows.Forms.Padding(2);
            this.txtActivite.MaxLength = 32767;
            this.txtActivite.Multiline = false;
            this.txtActivite.Name = "txtActivite";
            this.txtActivite.ReadOnly = false;
            this.txtActivite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtActivite.Size = new System.Drawing.Size(153, 27);
            this.txtActivite.TabIndex = 502;
            this.txtActivite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtActivite.UseSystemPasswordChar = false;
            // 
            // txtAnalytique
            // 
            this.txtAnalytique.AccAcceptNumbersOnly = false;
            this.txtAnalytique.AccAllowComma = false;
            this.txtAnalytique.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAnalytique.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAnalytique.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAnalytique.AccHidenValue = "";
            this.txtAnalytique.AccNotAllowedChars = null;
            this.txtAnalytique.AccReadOnly = false;
            this.txtAnalytique.AccReadOnlyAllowDelete = false;
            this.txtAnalytique.AccRequired = false;
            this.txtAnalytique.BackColor = System.Drawing.Color.White;
            this.txtAnalytique.CustomBackColor = System.Drawing.Color.White;
            this.txtAnalytique.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnalytique.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAnalytique.ForeColor = System.Drawing.Color.Black;
            this.txtAnalytique.Location = new System.Drawing.Point(159, 62);
            this.txtAnalytique.Margin = new System.Windows.Forms.Padding(2);
            this.txtAnalytique.MaxLength = 32767;
            this.txtAnalytique.Multiline = false;
            this.txtAnalytique.Name = "txtAnalytique";
            this.txtAnalytique.ReadOnly = false;
            this.txtAnalytique.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAnalytique.Size = new System.Drawing.Size(153, 27);
            this.txtAnalytique.TabIndex = 503;
            this.txtAnalytique.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAnalytique.UseSystemPasswordChar = false;
            this.txtAnalytique.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAnalytique_KeyPress);
            // 
            // txtCON_AnaModif
            // 
            this.txtCON_AnaModif.AccAcceptNumbersOnly = false;
            this.txtCON_AnaModif.AccAllowComma = false;
            this.txtCON_AnaModif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_AnaModif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_AnaModif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_AnaModif.AccHidenValue = "";
            this.txtCON_AnaModif.AccNotAllowedChars = null;
            this.txtCON_AnaModif.AccReadOnly = false;
            this.txtCON_AnaModif.AccReadOnlyAllowDelete = false;
            this.txtCON_AnaModif.AccRequired = false;
            this.txtCON_AnaModif.BackColor = System.Drawing.Color.White;
            this.txtCON_AnaModif.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_AnaModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_AnaModif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_AnaModif.ForeColor = System.Drawing.Color.Black;
            this.txtCON_AnaModif.Location = new System.Drawing.Point(159, 92);
            this.txtCON_AnaModif.Margin = new System.Windows.Forms.Padding(2);
            this.txtCON_AnaModif.MaxLength = 32767;
            this.txtCON_AnaModif.Multiline = false;
            this.txtCON_AnaModif.Name = "txtCON_AnaModif";
            this.txtCON_AnaModif.ReadOnly = false;
            this.txtCON_AnaModif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_AnaModif.Size = new System.Drawing.Size(153, 27);
            this.txtCON_AnaModif.TabIndex = 504;
            this.txtCON_AnaModif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_AnaModif.UseSystemPasswordChar = false;
            // 
            // cmdActivite
            // 
            this.cmdActivite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdActivite.FlatAppearance.BorderSize = 0;
            this.cmdActivite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdActivite.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdActivite.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdActivite.Location = new System.Drawing.Point(317, 3);
            this.cmdActivite.Name = "cmdActivite";
            this.cmdActivite.Size = new System.Drawing.Size(25, 20);
            this.cmdActivite.TabIndex = 505;
            this.cmdActivite.UseVisualStyleBackColor = false;
            this.cmdActivite.Click += new System.EventHandler(this.cmdActivite_Click);
            // 
            // cmdSecteur
            // 
            this.cmdSecteur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSecteur.FlatAppearance.BorderSize = 0;
            this.cmdSecteur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSecteur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSecteur.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSecteur.Location = new System.Drawing.Point(317, 63);
            this.cmdSecteur.Name = "cmdSecteur";
            this.cmdSecteur.Size = new System.Drawing.Size(25, 20);
            this.cmdSecteur.TabIndex = 506;
            this.cmdSecteur.UseVisualStyleBackColor = false;
            this.cmdSecteur.Click += new System.EventHandler(this.cmdSecteur_Click);
            // 
            // ckkCON_AnaModif
            // 
            this.ckkCON_AnaModif.AutoSize = true;
            this.ckkCON_AnaModif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ckkCON_AnaModif.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ckkCON_AnaModif.Location = new System.Drawing.Point(160, 33);
            this.ckkCON_AnaModif.Name = "ckkCON_AnaModif";
            this.ckkCON_AnaModif.Size = new System.Drawing.Size(151, 24);
            this.ckkCON_AnaModif.TabIndex = 570;
            this.ckkCON_AnaModif.Text = "Autre";
            this.ckkCON_AnaModif.UseVisualStyleBackColor = true;
            this.ckkCON_AnaModif.CheckedChanged += new System.EventHandler(this.ckkCON_AnaModif_CheckedChanged);
            // 
            // Command4
            // 
            this.Command4.Location = new System.Drawing.Point(643, 658);
            this.Command4.Name = "Command4";
            this.Command4.Size = new System.Drawing.Size(22, 13);
            this.Command4.TabIndex = 521;
            this.Command4.Visible = false;
            this.Command4.Click += new System.EventHandler(this.Command4_Click);
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(712, 655);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(110, 16);
            this.label45.TabIndex = 515;
            this.label45.Text = "N°=cpte de vente";
            this.label45.Visible = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel14);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1848, 693);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 3;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 17.64706F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 29.41176F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.94118F));
            this.tableLayoutPanel14.Controls.Add(this.groupBox6, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.txtCON_EchuEchoir, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.Frame11, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.Frame4, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.Frame5, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.chkCalcEcheance, 1, 1);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 3;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1848, 693);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel14.SetColumnSpan(this.groupBox6, 3);
            this.groupBox6.Controls.Add(this.ultraCombo1);
            this.groupBox6.Controls.Add(this.SSOleDBDropDown2);
            this.groupBox6.Controls.Add(this.SSOleDBDropDown1);
            this.groupBox6.Controls.Add(this.SSOleDBGrid1);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(2, 146);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(2);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1844, 545);
            this.groupBox6.TabIndex = 572;
            this.groupBox6.TabStop = false;
            // 
            // ultraCombo1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ultraCombo1.DisplayLayout.Appearance = appearance1;
            this.ultraCombo1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCombo1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ultraCombo1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ultraCombo1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ultraCombo1.DisplayLayout.MaxColScrollRegions = 1;
            this.ultraCombo1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ultraCombo1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ultraCombo1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ultraCombo1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ultraCombo1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ultraCombo1.DisplayLayout.Override.CellAppearance = appearance8;
            this.ultraCombo1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ultraCombo1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ultraCombo1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ultraCombo1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ultraCombo1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ultraCombo1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ultraCombo1.DisplayLayout.Override.RowAppearance = appearance11;
            this.ultraCombo1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ultraCombo1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ultraCombo1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ultraCombo1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ultraCombo1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ultraCombo1.Location = new System.Drawing.Point(325, 150);
            this.ultraCombo1.Name = "ultraCombo1";
            this.ultraCombo1.Size = new System.Drawing.Size(186, 25);
            this.ultraCombo1.TabIndex = 505;
            this.ultraCombo1.Visible = false;
            // 
            // SSOleDBDropDown2
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown2.DisplayLayout.Appearance = appearance13;
            this.SSOleDBDropDown2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSOleDBDropDown2.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown2.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown2.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSOleDBDropDown2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown2.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown2.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown2.DisplayLayout.Override.RowAppearance = appearance23;
            this.SSOleDBDropDown2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown2.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSOleDBDropDown2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown2.Location = new System.Drawing.Point(548, 98);
            this.SSOleDBDropDown2.Name = "SSOleDBDropDown2";
            this.SSOleDBDropDown2.Size = new System.Drawing.Size(186, 25);
            this.SSOleDBDropDown2.TabIndex = 504;
            this.SSOleDBDropDown2.Visible = false;
            // 
            // SSOleDBDropDown1
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBDropDown1.DisplayLayout.Appearance = appearance25;
            this.SSOleDBDropDown1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBDropDown1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBDropDown1.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.SSOleDBDropDown1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBDropDown1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBDropDown1.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBDropDown1.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.SSOleDBDropDown1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBDropDown1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellAppearance = appearance32;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBDropDown1.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBDropDown1.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBDropDown1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBDropDown1.DisplayLayout.Override.RowAppearance = appearance35;
            this.SSOleDBDropDown1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBDropDown1.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.SSOleDBDropDown1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBDropDown1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBDropDown1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBDropDown1.Location = new System.Drawing.Point(322, 98);
            this.SSOleDBDropDown1.Name = "SSOleDBDropDown1";
            this.SSOleDBDropDown1.Size = new System.Drawing.Size(186, 25);
            this.SSOleDBDropDown1.TabIndex = 503;
            this.SSOleDBDropDown1.Visible = false;
            // 
            // SSOleDBGrid1
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance37;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance44;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance47;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSOleDBGrid1.Location = new System.Drawing.Point(3, 18);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(1838, 524);
            this.SSOleDBGrid1.TabIndex = 412;
            this.SSOleDBGrid1.Text = "ultraGrid1";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.AfterExitEditMode += new System.EventHandler(this.SSOleDBGrid1_AfterExitEditMode);
            this.SSOleDBGrid1.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSOleDBGrid1_AfterRowUpdate);
            this.SSOleDBGrid1.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.SSOleDBGrid1_BeforeExitEditMode);
            this.SSOleDBGrid1.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.SSOleDBGrid1_Error);
            this.SSOleDBGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid1_DoubleClickRow);
            // 
            // txtCON_EchuEchoir
            // 
            this.txtCON_EchuEchoir.AccAcceptNumbersOnly = false;
            this.txtCON_EchuEchoir.AccAllowComma = false;
            this.txtCON_EchuEchoir.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_EchuEchoir.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_EchuEchoir.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_EchuEchoir.AccHidenValue = "";
            this.txtCON_EchuEchoir.AccNotAllowedChars = null;
            this.txtCON_EchuEchoir.AccReadOnly = false;
            this.txtCON_EchuEchoir.AccReadOnlyAllowDelete = false;
            this.txtCON_EchuEchoir.AccRequired = false;
            this.txtCON_EchuEchoir.BackColor = System.Drawing.Color.White;
            this.txtCON_EchuEchoir.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_EchuEchoir.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_EchuEchoir.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_EchuEchoir.ForeColor = System.Drawing.Color.Black;
            this.txtCON_EchuEchoir.Location = new System.Drawing.Point(1, 111);
            this.txtCON_EchuEchoir.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_EchuEchoir.MaxLength = 32767;
            this.txtCON_EchuEchoir.Multiline = false;
            this.txtCON_EchuEchoir.Name = "txtCON_EchuEchoir";
            this.txtCON_EchuEchoir.ReadOnly = false;
            this.txtCON_EchuEchoir.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_EchuEchoir.Size = new System.Drawing.Size(324, 27);
            this.txtCON_EchuEchoir.TabIndex = 571;
            this.txtCON_EchuEchoir.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_EchuEchoir.UseSystemPasswordChar = false;
            this.txtCON_EchuEchoir.Visible = false;
            // 
            // Frame11
            // 
            this.Frame11.BackColor = System.Drawing.Color.Transparent;
            this.Frame11.Controls.Add(this.tableLayoutPanel17);
            this.Frame11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame11.Location = new System.Drawing.Point(870, 1);
            this.Frame11.Margin = new System.Windows.Forms.Padding(1);
            this.Frame11.Name = "Frame11";
            this.tableLayoutPanel14.SetRowSpan(this.Frame11, 2);
            this.Frame11.Size = new System.Drawing.Size(977, 142);
            this.Frame11.TabIndex = 413;
            this.Frame11.TabStop = false;
            this.Frame11.Text = "Dernier Indices de révision";
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 4;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel17.Controls.Add(this.label34, 0, 3);
            this.tableLayoutPanel17.Controls.Add(this.label29, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Indice1rev, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Indice2rev, 0, 2);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Indice3rev, 0, 3);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Valeur1rev, 2, 1);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Valeur2rev, 2, 2);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Valeur3rev, 2, 3);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Date1rev, 3, 1);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Date2rev, 3, 2);
            this.tableLayoutPanel17.Controls.Add(this.txtCON_Date3rev, 3, 3);
            this.tableLayoutPanel17.Controls.Add(this.label6, 1, 2);
            this.tableLayoutPanel17.Controls.Add(this.label30, 1, 1);
            this.tableLayoutPanel17.Controls.Add(this.label31, 2, 0);
            this.tableLayoutPanel17.Controls.Add(this.label32, 4, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 4;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(971, 121);
            this.tableLayoutPanel17.TabIndex = 0;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(315, 91);
            this.label34.Margin = new System.Windows.Forms.Padding(1);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(26, 29);
            this.label34.TabIndex = 516;
            this.label34.Text = "=";
            this.label34.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(1, 1);
            this.label29.Margin = new System.Windows.Forms.Padding(1);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(312, 28);
            this.label29.TabIndex = 512;
            this.label29.Text = "Indice";
            this.label29.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtCON_Indice1rev
            // 
            this.txtCON_Indice1rev.AccAcceptNumbersOnly = false;
            this.txtCON_Indice1rev.AccAllowComma = false;
            this.txtCON_Indice1rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Indice1rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Indice1rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Indice1rev.AccHidenValue = "";
            this.txtCON_Indice1rev.AccNotAllowedChars = null;
            this.txtCON_Indice1rev.AccReadOnly = false;
            this.txtCON_Indice1rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Indice1rev.AccRequired = false;
            this.txtCON_Indice1rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Indice1rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Indice1rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Indice1rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Indice1rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Indice1rev.Location = new System.Drawing.Point(1, 31);
            this.txtCON_Indice1rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Indice1rev.MaxLength = 32767;
            this.txtCON_Indice1rev.Multiline = false;
            this.txtCON_Indice1rev.Name = "txtCON_Indice1rev";
            this.txtCON_Indice1rev.ReadOnly = false;
            this.txtCON_Indice1rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Indice1rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Indice1rev.TabIndex = 0;
            this.txtCON_Indice1rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Indice1rev.UseSystemPasswordChar = false;
            // 
            // txtCON_Indice2rev
            // 
            this.txtCON_Indice2rev.AccAcceptNumbersOnly = false;
            this.txtCON_Indice2rev.AccAllowComma = false;
            this.txtCON_Indice2rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Indice2rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Indice2rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Indice2rev.AccHidenValue = "";
            this.txtCON_Indice2rev.AccNotAllowedChars = null;
            this.txtCON_Indice2rev.AccReadOnly = false;
            this.txtCON_Indice2rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Indice2rev.AccRequired = false;
            this.txtCON_Indice2rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Indice2rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Indice2rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Indice2rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Indice2rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Indice2rev.Location = new System.Drawing.Point(1, 61);
            this.txtCON_Indice2rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Indice2rev.MaxLength = 32767;
            this.txtCON_Indice2rev.Multiline = false;
            this.txtCON_Indice2rev.Name = "txtCON_Indice2rev";
            this.txtCON_Indice2rev.ReadOnly = false;
            this.txtCON_Indice2rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Indice2rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Indice2rev.TabIndex = 3;
            this.txtCON_Indice2rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Indice2rev.UseSystemPasswordChar = false;
            // 
            // txtCON_Indice3rev
            // 
            this.txtCON_Indice3rev.AccAcceptNumbersOnly = false;
            this.txtCON_Indice3rev.AccAllowComma = false;
            this.txtCON_Indice3rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Indice3rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Indice3rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Indice3rev.AccHidenValue = "";
            this.txtCON_Indice3rev.AccNotAllowedChars = null;
            this.txtCON_Indice3rev.AccReadOnly = false;
            this.txtCON_Indice3rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Indice3rev.AccRequired = false;
            this.txtCON_Indice3rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Indice3rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Indice3rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Indice3rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Indice3rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Indice3rev.Location = new System.Drawing.Point(1, 91);
            this.txtCON_Indice3rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Indice3rev.MaxLength = 32767;
            this.txtCON_Indice3rev.Multiline = false;
            this.txtCON_Indice3rev.Name = "txtCON_Indice3rev";
            this.txtCON_Indice3rev.ReadOnly = false;
            this.txtCON_Indice3rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Indice3rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Indice3rev.TabIndex = 6;
            this.txtCON_Indice3rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Indice3rev.UseSystemPasswordChar = false;
            // 
            // txtCON_Valeur1rev
            // 
            this.txtCON_Valeur1rev.AccAcceptNumbersOnly = false;
            this.txtCON_Valeur1rev.AccAllowComma = false;
            this.txtCON_Valeur1rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Valeur1rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Valeur1rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Valeur1rev.AccHidenValue = "";
            this.txtCON_Valeur1rev.AccNotAllowedChars = null;
            this.txtCON_Valeur1rev.AccReadOnly = false;
            this.txtCON_Valeur1rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Valeur1rev.AccRequired = false;
            this.txtCON_Valeur1rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Valeur1rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Valeur1rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Valeur1rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Valeur1rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Valeur1rev.Location = new System.Drawing.Point(343, 31);
            this.txtCON_Valeur1rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Valeur1rev.MaxLength = 32767;
            this.txtCON_Valeur1rev.Multiline = false;
            this.txtCON_Valeur1rev.Name = "txtCON_Valeur1rev";
            this.txtCON_Valeur1rev.ReadOnly = false;
            this.txtCON_Valeur1rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Valeur1rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Valeur1rev.TabIndex = 1;
            this.txtCON_Valeur1rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Valeur1rev.UseSystemPasswordChar = false;
            this.txtCON_Valeur1rev.Validating += new System.ComponentModel.CancelEventHandler(this.txtCON_Valeur1rev_Validating);
            // 
            // txtCON_Valeur2rev
            // 
            this.txtCON_Valeur2rev.AccAcceptNumbersOnly = false;
            this.txtCON_Valeur2rev.AccAllowComma = false;
            this.txtCON_Valeur2rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Valeur2rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Valeur2rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Valeur2rev.AccHidenValue = "";
            this.txtCON_Valeur2rev.AccNotAllowedChars = null;
            this.txtCON_Valeur2rev.AccReadOnly = false;
            this.txtCON_Valeur2rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Valeur2rev.AccRequired = false;
            this.txtCON_Valeur2rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Valeur2rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Valeur2rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Valeur2rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Valeur2rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Valeur2rev.Location = new System.Drawing.Point(343, 61);
            this.txtCON_Valeur2rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Valeur2rev.MaxLength = 32767;
            this.txtCON_Valeur2rev.Multiline = false;
            this.txtCON_Valeur2rev.Name = "txtCON_Valeur2rev";
            this.txtCON_Valeur2rev.ReadOnly = false;
            this.txtCON_Valeur2rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Valeur2rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Valeur2rev.TabIndex = 4;
            this.txtCON_Valeur2rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Valeur2rev.UseSystemPasswordChar = false;
            this.txtCON_Valeur2rev.Validating += new System.ComponentModel.CancelEventHandler(this.txtCON_Valeur2rev_Validating);
            // 
            // txtCON_Valeur3rev
            // 
            this.txtCON_Valeur3rev.AccAcceptNumbersOnly = false;
            this.txtCON_Valeur3rev.AccAllowComma = false;
            this.txtCON_Valeur3rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Valeur3rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Valeur3rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Valeur3rev.AccHidenValue = "";
            this.txtCON_Valeur3rev.AccNotAllowedChars = null;
            this.txtCON_Valeur3rev.AccReadOnly = false;
            this.txtCON_Valeur3rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Valeur3rev.AccRequired = false;
            this.txtCON_Valeur3rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Valeur3rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Valeur3rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Valeur3rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Valeur3rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Valeur3rev.Location = new System.Drawing.Point(343, 91);
            this.txtCON_Valeur3rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Valeur3rev.MaxLength = 32767;
            this.txtCON_Valeur3rev.Multiline = false;
            this.txtCON_Valeur3rev.Name = "txtCON_Valeur3rev";
            this.txtCON_Valeur3rev.ReadOnly = false;
            this.txtCON_Valeur3rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Valeur3rev.Size = new System.Drawing.Size(312, 27);
            this.txtCON_Valeur3rev.TabIndex = 7;
            this.txtCON_Valeur3rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Valeur3rev.UseSystemPasswordChar = false;
            this.txtCON_Valeur3rev.Validating += new System.ComponentModel.CancelEventHandler(this.txtCON_Valeur3rev_Validating);
            // 
            // txtCON_Date1rev
            // 
            this.txtCON_Date1rev.AccAcceptNumbersOnly = false;
            this.txtCON_Date1rev.AccAllowComma = false;
            this.txtCON_Date1rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Date1rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Date1rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Date1rev.AccHidenValue = "";
            this.txtCON_Date1rev.AccNotAllowedChars = null;
            this.txtCON_Date1rev.AccReadOnly = false;
            this.txtCON_Date1rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Date1rev.AccRequired = false;
            this.txtCON_Date1rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Date1rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Date1rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Date1rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Date1rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Date1rev.Location = new System.Drawing.Point(657, 31);
            this.txtCON_Date1rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Date1rev.MaxLength = 32767;
            this.txtCON_Date1rev.Multiline = false;
            this.txtCON_Date1rev.Name = "txtCON_Date1rev";
            this.txtCON_Date1rev.ReadOnly = false;
            this.txtCON_Date1rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Date1rev.Size = new System.Drawing.Size(313, 27);
            this.txtCON_Date1rev.TabIndex = 2;
            this.txtCON_Date1rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Date1rev.UseSystemPasswordChar = false;
            this.txtCON_Date1rev.Leave += new System.EventHandler(this.txtCON_Date1rev_Leave);
            // 
            // txtCON_Date2rev
            // 
            this.txtCON_Date2rev.AccAcceptNumbersOnly = false;
            this.txtCON_Date2rev.AccAllowComma = false;
            this.txtCON_Date2rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Date2rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Date2rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Date2rev.AccHidenValue = "";
            this.txtCON_Date2rev.AccNotAllowedChars = null;
            this.txtCON_Date2rev.AccReadOnly = false;
            this.txtCON_Date2rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Date2rev.AccRequired = false;
            this.txtCON_Date2rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Date2rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Date2rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Date2rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Date2rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Date2rev.Location = new System.Drawing.Point(657, 61);
            this.txtCON_Date2rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Date2rev.MaxLength = 32767;
            this.txtCON_Date2rev.Multiline = false;
            this.txtCON_Date2rev.Name = "txtCON_Date2rev";
            this.txtCON_Date2rev.ReadOnly = false;
            this.txtCON_Date2rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Date2rev.Size = new System.Drawing.Size(313, 27);
            this.txtCON_Date2rev.TabIndex = 5;
            this.txtCON_Date2rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Date2rev.UseSystemPasswordChar = false;
            this.txtCON_Date2rev.Leave += new System.EventHandler(this.txtCON_Date2rev_Leave);
            // 
            // txtCON_Date3rev
            // 
            this.txtCON_Date3rev.AccAcceptNumbersOnly = false;
            this.txtCON_Date3rev.AccAllowComma = false;
            this.txtCON_Date3rev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCON_Date3rev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCON_Date3rev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCON_Date3rev.AccHidenValue = "";
            this.txtCON_Date3rev.AccNotAllowedChars = null;
            this.txtCON_Date3rev.AccReadOnly = false;
            this.txtCON_Date3rev.AccReadOnlyAllowDelete = false;
            this.txtCON_Date3rev.AccRequired = false;
            this.txtCON_Date3rev.BackColor = System.Drawing.Color.White;
            this.txtCON_Date3rev.CustomBackColor = System.Drawing.Color.White;
            this.txtCON_Date3rev.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCON_Date3rev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCON_Date3rev.ForeColor = System.Drawing.Color.Black;
            this.txtCON_Date3rev.Location = new System.Drawing.Point(657, 91);
            this.txtCON_Date3rev.Margin = new System.Windows.Forms.Padding(1);
            this.txtCON_Date3rev.MaxLength = 32767;
            this.txtCON_Date3rev.Multiline = false;
            this.txtCON_Date3rev.Name = "txtCON_Date3rev";
            this.txtCON_Date3rev.ReadOnly = false;
            this.txtCON_Date3rev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCON_Date3rev.Size = new System.Drawing.Size(313, 27);
            this.txtCON_Date3rev.TabIndex = 8;
            this.txtCON_Date3rev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCON_Date3rev.UseSystemPasswordChar = false;
            this.txtCON_Date3rev.Leave += new System.EventHandler(this.txtCON_Date3rev_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(315, 61);
            this.label6.Margin = new System.Windows.Forms.Padding(1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(26, 28);
            this.label6.TabIndex = 511;
            this.label6.Text = "=";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(315, 31);
            this.label30.Margin = new System.Windows.Forms.Padding(1);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(26, 28);
            this.label30.TabIndex = 513;
            this.label30.Text = "=";
            this.label30.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(343, 1);
            this.label31.Margin = new System.Windows.Forms.Padding(1);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(312, 28);
            this.label31.TabIndex = 514;
            this.label31.Text = "Valeur";
            this.label31.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(657, 1);
            this.label32.Margin = new System.Windows.Forms.Padding(1);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(313, 28);
            this.label32.TabIndex = 515;
            this.label32.Text = "Date";
            this.label32.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.tableLayoutPanel16);
            this.Frame4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame4.Location = new System.Drawing.Point(329, 3);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(537, 104);
            this.Frame4.TabIndex = 412;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Terme";
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 2;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Controls.Add(this.optAutre, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.optAnnuel, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.Option6, 0, 1);
            this.tableLayoutPanel16.Controls.Add(this.Option7, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 2;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(531, 83);
            this.tableLayoutPanel16.TabIndex = 0;
            // 
            // optAutre
            // 
            this.optAutre.AutoSize = true;
            this.optAutre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optAutre.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAutre.Location = new System.Drawing.Point(5, 44);
            this.optAutre.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.optAutre.Name = "optAutre";
            this.optAutre.Size = new System.Drawing.Size(257, 36);
            this.optAutre.TabIndex = 541;
            this.optAutre.Text = "Autre";
            this.optAutre.UseVisualStyleBackColor = true;
            this.optAutre.CheckedChanged += new System.EventHandler(this.optAutre_CheckedChanged);
            // 
            // optAnnuel
            // 
            this.optAnnuel.AutoSize = true;
            this.optAnnuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optAnnuel.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optAnnuel.Location = new System.Drawing.Point(270, 3);
            this.optAnnuel.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.optAnnuel.Name = "optAnnuel";
            this.optAnnuel.Size = new System.Drawing.Size(258, 35);
            this.optAnnuel.TabIndex = 540;
            this.optAnnuel.Text = "Annuel";
            this.optAnnuel.UseVisualStyleBackColor = true;
            this.optAnnuel.CheckedChanged += new System.EventHandler(this.optAnnuel_CheckedChanged);
            // 
            // Option6
            // 
            this.Option6.AutoSize = true;
            this.Option6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option6.Location = new System.Drawing.Point(270, 44);
            this.Option6.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.Option6.Name = "Option6";
            this.Option6.Size = new System.Drawing.Size(258, 36);
            this.Option6.TabIndex = 539;
            this.Option6.Text = "Echu sur date d\'effet";
            this.Option6.UseVisualStyleBackColor = true;
            this.Option6.CheckedChanged += new System.EventHandler(this.Option6_CheckedChanged);
            // 
            // Option7
            // 
            this.Option7.AutoSize = true;
            this.Option7.Checked = true;
            this.Option7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option7.Location = new System.Drawing.Point(5, 3);
            this.Option7.Margin = new System.Windows.Forms.Padding(5, 3, 3, 3);
            this.Option7.Name = "Option7";
            this.Option7.Size = new System.Drawing.Size(257, 35);
            this.Option7.TabIndex = 538;
            this.Option7.TabStop = true;
            this.Option7.Text = "A echoir";
            this.Option7.UseVisualStyleBackColor = true;
            this.Option7.CheckedChanged += new System.EventHandler(this.Option7_CheckedChanged);
            // 
            // Frame5
            // 
            this.Frame5.BackColor = System.Drawing.Color.Transparent;
            this.Frame5.Controls.Add(this.tableLayoutPanel15);
            this.Frame5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame5.Location = new System.Drawing.Point(3, 3);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(320, 104);
            this.Frame5.TabIndex = 411;
            this.Frame5.TabStop = false;
            this.Frame5.Text = "Type de radio";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Controls.Add(this.Option9, 0, 1);
            this.tableLayoutPanel15.Controls.Add(this.Option8, 0, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 2;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(314, 83);
            this.tableLayoutPanel15.TabIndex = 0;
            // 
            // Option9
            // 
            this.Option9.AutoSize = true;
            this.Option9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option9.Location = new System.Drawing.Point(7, 44);
            this.Option9.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.Option9.Name = "Option9";
            this.Option9.Size = new System.Drawing.Size(304, 36);
            this.Option9.TabIndex = 539;
            this.Option9.Text = "%";
            this.Option9.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Option9.UseVisualStyleBackColor = true;
            // 
            // Option8
            // 
            this.Option8.AutoSize = true;
            this.Option8.Checked = true;
            this.Option8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option8.Location = new System.Drawing.Point(7, 3);
            this.Option8.Margin = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.Option8.Name = "Option8";
            this.Option8.Size = new System.Drawing.Size(304, 35);
            this.Option8.TabIndex = 538;
            this.Option8.TabStop = true;
            this.Option8.Text = "Rapport";
            this.Option8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.Option8.UseVisualStyleBackColor = true;
            // 
            // chkCalcEcheance
            // 
            this.chkCalcEcheance.AutoSize = true;
            this.chkCalcEcheance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCalcEcheance.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkCalcEcheance.Location = new System.Drawing.Point(329, 113);
            this.chkCalcEcheance.Name = "chkCalcEcheance";
            this.chkCalcEcheance.Size = new System.Drawing.Size(537, 28);
            this.chkCalcEcheance.TabIndex = 570;
            this.chkCalcEcheance.Text = "Mode de réglement lié à l\'immeuble";
            this.chkCalcEcheance.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel18);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1848, 693);
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 1;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.groupBox8, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.Frame6, 0, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(1848, 693);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.tableLayoutPanel20);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(1, 278);
            this.groupBox8.Margin = new System.Windows.Forms.Padding(1);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1846, 414);
            this.groupBox8.TabIndex = 412;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Suivi sur contrat resilies";
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.39478F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.60522F));
            this.tableLayoutPanel20.Controls.Add(this.txtANCC_DateReceptResil, 1, 1);
            this.tableLayoutPanel20.Controls.Add(this.label48, 0, 2);
            this.tableLayoutPanel20.Controls.Add(this.label49, 0, 1);
            this.tableLayoutPanel20.Controls.Add(this.txtANCC_CommRESILIE, 1, 2);
            this.tableLayoutPanel20.Controls.Add(this.flowLayoutPanel11, 1, 3);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 4;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 18F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 65.90909F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.09091F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1840, 390);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // txtANCC_DateReceptResil
            // 
            this.txtANCC_DateReceptResil.AccAcceptNumbersOnly = false;
            this.txtANCC_DateReceptResil.AccAllowComma = false;
            this.txtANCC_DateReceptResil.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCC_DateReceptResil.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCC_DateReceptResil.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCC_DateReceptResil.AccHidenValue = "";
            this.txtANCC_DateReceptResil.AccNotAllowedChars = null;
            this.txtANCC_DateReceptResil.AccReadOnly = false;
            this.txtANCC_DateReceptResil.AccReadOnlyAllowDelete = false;
            this.txtANCC_DateReceptResil.AccRequired = false;
            this.txtANCC_DateReceptResil.BackColor = System.Drawing.Color.White;
            this.txtANCC_DateReceptResil.CustomBackColor = System.Drawing.Color.White;
            this.txtANCC_DateReceptResil.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCC_DateReceptResil.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCC_DateReceptResil.ForeColor = System.Drawing.Color.Black;
            this.txtANCC_DateReceptResil.Location = new System.Drawing.Point(302, 19);
            this.txtANCC_DateReceptResil.Margin = new System.Windows.Forms.Padding(1);
            this.txtANCC_DateReceptResil.MaxLength = 32767;
            this.txtANCC_DateReceptResil.Multiline = false;
            this.txtANCC_DateReceptResil.Name = "txtANCC_DateReceptResil";
            this.txtANCC_DateReceptResil.ReadOnly = false;
            this.txtANCC_DateReceptResil.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCC_DateReceptResil.Size = new System.Drawing.Size(1537, 27);
            this.txtANCC_DateReceptResil.TabIndex = 0;
            this.txtANCC_DateReceptResil.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCC_DateReceptResil.UseSystemPasswordChar = false;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label48.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(3, 48);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(295, 225);
            this.label48.TabIndex = 384;
            this.label48.Text = "Commentaire";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label49.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(3, 18);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(295, 30);
            this.label49.TabIndex = 385;
            this.label49.Text = "Date réception réalisation";
            // 
            // txtANCC_CommRESILIE
            // 
            this.txtANCC_CommRESILIE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCC_CommRESILIE.AutoWordSelection = false;
            this.txtANCC_CommRESILIE.BackColor = System.Drawing.Color.Transparent;
            this.txtANCC_CommRESILIE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCC_CommRESILIE.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCC_CommRESILIE.ForeColor = System.Drawing.Color.Black;
            this.txtANCC_CommRESILIE.Location = new System.Drawing.Point(302, 49);
            this.txtANCC_CommRESILIE.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.txtANCC_CommRESILIE.Name = "txtANCC_CommRESILIE";
            this.txtANCC_CommRESILIE.ReadOnly = false;
            this.txtANCC_CommRESILIE.Size = new System.Drawing.Size(1537, 222);
            this.txtANCC_CommRESILIE.TabIndex = 1;
            this.txtANCC_CommRESILIE.WordWrap = true;
            // 
            // flowLayoutPanel11
            // 
            this.flowLayoutPanel11.Controls.Add(this.chkANCC_Reconduction);
            this.flowLayoutPanel11.Controls.Add(this.chkANCC_RenovPerdue);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(302, 274);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.Size = new System.Drawing.Size(1537, 115);
            this.flowLayoutPanel11.TabIndex = 570;
            // 
            // chkANCC_Reconduction
            // 
            this.chkANCC_Reconduction.AutoSize = true;
            this.chkANCC_Reconduction.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkANCC_Reconduction.Location = new System.Drawing.Point(3, 3);
            this.chkANCC_Reconduction.Name = "chkANCC_Reconduction";
            this.chkANCC_Reconduction.Size = new System.Drawing.Size(124, 23);
            this.chkANCC_Reconduction.TabIndex = 570;
            this.chkANCC_Reconduction.Text = "Reconduction";
            this.chkANCC_Reconduction.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkANCC_Reconduction.UseVisualStyleBackColor = true;
            // 
            // chkANCC_RenovPerdue
            // 
            this.chkANCC_RenovPerdue.AutoSize = true;
            this.chkANCC_RenovPerdue.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkANCC_RenovPerdue.Location = new System.Drawing.Point(133, 3);
            this.chkANCC_RenovPerdue.Name = "chkANCC_RenovPerdue";
            this.chkANCC_RenovPerdue.Size = new System.Drawing.Size(203, 23);
            this.chkANCC_RenovPerdue.TabIndex = 571;
            this.chkANCC_RenovPerdue.Text = "Cause rénovation perdue";
            this.chkANCC_RenovPerdue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.chkANCC_RenovPerdue.UseVisualStyleBackColor = true;
            // 
            // Frame6
            // 
            this.Frame6.BackColor = System.Drawing.Color.Transparent;
            this.Frame6.Controls.Add(this.tableLayoutPanel19);
            this.Frame6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame6.Location = new System.Drawing.Point(1, 1);
            this.Frame6.Margin = new System.Windows.Forms.Padding(1);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(1846, 275);
            this.Frame6.TabIndex = 411;
            this.Frame6.TabStop = false;
            this.Frame6.Text = "Suivi sur contrat acceptes";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 2;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 16.47635F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 83.52365F));
            this.tableLayoutPanel19.Controls.Add(this.txtANCC_CommACCEPTE, 0, 1);
            this.tableLayoutPanel19.Controls.Add(this.label35, 0, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(1840, 251);
            this.tableLayoutPanel19.TabIndex = 0;
            // 
            // txtANCC_CommACCEPTE
            // 
            this.txtANCC_CommACCEPTE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtANCC_CommACCEPTE.AutoWordSelection = false;
            this.txtANCC_CommACCEPTE.BackColor = System.Drawing.Color.Transparent;
            this.txtANCC_CommACCEPTE.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtANCC_CommACCEPTE.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtANCC_CommACCEPTE.ForeColor = System.Drawing.Color.Black;
            this.txtANCC_CommACCEPTE.Location = new System.Drawing.Point(304, 21);
            this.txtANCC_CommACCEPTE.Margin = new System.Windows.Forms.Padding(1, 1, 1, 2);
            this.txtANCC_CommACCEPTE.Name = "txtANCC_CommACCEPTE";
            this.txtANCC_CommACCEPTE.ReadOnly = false;
            this.txtANCC_CommACCEPTE.Size = new System.Drawing.Size(1535, 228);
            this.txtANCC_CommACCEPTE.TabIndex = 0;
            this.txtANCC_CommACCEPTE.WordWrap = true;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(3, 20);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(297, 231);
            this.label35.TabIndex = 385;
            this.label35.Text = "Commentaire";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Controls.Add(this.ultraTabPageControl3);
            this.SSTab1.Controls.Add(this.ultraTabPageControl4);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.SSTab1.Location = new System.Drawing.Point(0, 242);
            this.SSTab1.Margin = new System.Windows.Forms.Padding(0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1850, 715);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab1.TabPage = this.ultraTabPageControl1;
            ultraTab1.Text = "Identification";
            ultraTab2.TabPage = this.ultraTabPageControl2;
            ultraTab2.Text = "Parametres";
            ultraTab3.TabPage = this.ultraTabPageControl3;
            ultraTab3.Text = "Calendrier";
            ultraTab4.TabPage = this.ultraTabPageControl4;
            ultraTab4.Text = "Suivi";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2,
            ultraTab3,
            ultraTab4});
            this.toolTip1.SetToolTip(this.SSTab1, "Recherche du code article");
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            this.SSTab1.SelectedTabChanged += new Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventHandler(this.SSTab1_SelectedTabChanged);
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1848, 693);
            // 
            // cmdResilié
            // 
            this.cmdResilié.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(173)))), ((int)(((byte)(163)))));
            this.cmdResilié.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdResilié.FlatAppearance.BorderSize = 0;
            this.cmdResilié.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdResilié.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdResilié.ForeColor = System.Drawing.Color.White;
            this.cmdResilié.Image = global::Axe_interDT.Properties.Resources.Control_16X16;
            this.cmdResilié.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdResilié.Location = new System.Drawing.Point(2, 45);
            this.cmdResilié.Margin = new System.Windows.Forms.Padding(2);
            this.cmdResilié.Name = "cmdResilié";
            this.cmdResilié.Size = new System.Drawing.Size(113, 30);
            this.cmdResilié.TabIndex = 568;
            this.cmdResilié.Text = "Résiliation";
            this.cmdResilié.UseVisualStyleBackColor = false;
            this.cmdResilié.Click += new System.EventHandler(this.cmdResilié_Click);
            // 
            // Frame9
            // 
            this.Frame9.BackColor = System.Drawing.Color.Transparent;
            this.Frame9.Controls.Add(this.ChkResiliee);
            this.Frame9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame9.Location = new System.Drawing.Point(3, 3);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(111, 33);
            this.Frame9.TabIndex = 570;
            this.Frame9.TabStop = false;
            // 
            // ChkResiliee
            // 
            this.ChkResiliee.AutoSize = true;
            this.ChkResiliee.Enabled = false;
            this.ChkResiliee.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ChkResiliee.Location = new System.Drawing.Point(18, 9);
            this.ChkResiliee.Name = "ChkResiliee";
            this.ChkResiliee.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.ChkResiliee.Size = new System.Drawing.Size(80, 23);
            this.ChkResiliee.TabIndex = 570;
            this.ChkResiliee.Text = "Resiliee";
            this.ChkResiliee.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ChkResiliee.UseVisualStyleBackColor = true;
            this.ChkResiliee.CheckedChanged += new System.EventHandler(this.ChkResiliee_CheckedChanged);
            // 
            // lblChaudiereCondo
            // 
            this.lblChaudiereCondo.AutoSize = true;
            this.lblChaudiereCondo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(216)))), ((int)(((byte)(181)))));
            this.lblChaudiereCondo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblChaudiereCondo.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChaudiereCondo.ForeColor = System.Drawing.Color.Red;
            this.lblChaudiereCondo.Location = new System.Drawing.Point(153, 0);
            this.lblChaudiereCondo.Name = "lblChaudiereCondo";
            this.lblChaudiereCondo.Size = new System.Drawing.Size(994, 20);
            this.lblChaudiereCondo.TabIndex = 585;
            this.lblChaudiereCondo.Text = "Chaudière à condensation";
            this.lblChaudiereCondo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Blue;
            this.label4.Location = new System.Drawing.Point(2, 25);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 19);
            this.label4.TabIndex = 582;
            this.label4.Tag = "44";
            this.label4.Text = "Immeuble";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label4.Click += new System.EventHandler(this.label4_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Blue;
            this.label3.Location = new System.Drawing.Point(2, 54);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 19);
            this.label3.TabIndex = 581;
            this.label3.Tag = "44";
            this.label3.Text = "Gérant";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(85, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(457, 25);
            this.label2.TabIndex = 559;
            this.label2.Text = "N°";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 20);
            this.label5.TabIndex = 568;
            this.label5.Text = "Nom";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(546, 26);
            this.cmdRechercheImmeuble.Margin = new System.Windows.Forms.Padding(1);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(25, 19);
            this.cmdRechercheImmeuble.TabIndex = 366;
            this.toolTip1.SetToolTip(this.cmdRechercheImmeuble, "Recherche de l\'immeuble");
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Visible = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // lblAutoliquidation
            // 
            this.lblAutoliquidation.AutoSize = true;
            this.lblAutoliquidation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(216)))), ((int)(((byte)(181)))));
            this.lblAutoliquidation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAutoliquidation.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAutoliquidation.ForeColor = System.Drawing.Color.Red;
            this.lblAutoliquidation.Location = new System.Drawing.Point(1613, 0);
            this.lblAutoliquidation.Name = "lblAutoliquidation";
            this.lblAutoliquidation.Size = new System.Drawing.Size(228, 29);
            this.lblAutoliquidation.TabIndex = 586;
            this.lblAutoliquidation.Text = "Autoliquidation";
            this.lblAutoliquidation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblGoFacturation
            // 
            this.lblGoFacturation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGoFacturation.AutoSize = true;
            this.lblGoFacturation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoFacturation.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoFacturation.ForeColor = System.Drawing.Color.Blue;
            this.lblGoFacturation.Location = new System.Drawing.Point(2, 5);
            this.lblGoFacturation.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoFacturation.Name = "lblGoFacturation";
            this.lblGoFacturation.Size = new System.Drawing.Size(226, 19);
            this.lblGoFacturation.TabIndex = 577;
            this.lblGoFacturation.Tag = "44";
            this.lblGoFacturation.Text = "Facturation";
            this.lblGoFacturation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoFacturation.Click += new System.EventHandler(this.lblGoFacturation_Click);
            // 
            // lblGoTypeRevision
            // 
            this.lblGoTypeRevision.AutoSize = true;
            this.lblGoTypeRevision.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoTypeRevision.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoTypeRevision.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoTypeRevision.ForeColor = System.Drawing.Color.Blue;
            this.lblGoTypeRevision.Location = new System.Drawing.Point(462, 0);
            this.lblGoTypeRevision.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoTypeRevision.Name = "lblGoTypeRevision";
            this.lblGoTypeRevision.Size = new System.Drawing.Size(226, 29);
            this.lblGoTypeRevision.TabIndex = 576;
            this.lblGoTypeRevision.Tag = "44";
            this.lblGoTypeRevision.Text = "Type de Révision";
            this.lblGoTypeRevision.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoTypeRevision.Click += new System.EventHandler(this.lblGoTypeRevision_Click);
            // 
            // lblGoFormule
            // 
            this.lblGoFormule.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGoFormule.AutoSize = true;
            this.lblGoFormule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoFormule.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoFormule.ForeColor = System.Drawing.Color.Blue;
            this.lblGoFormule.Location = new System.Drawing.Point(232, 5);
            this.lblGoFormule.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoFormule.Name = "lblGoFormule";
            this.lblGoFormule.Size = new System.Drawing.Size(226, 19);
            this.lblGoFormule.TabIndex = 575;
            this.lblGoFormule.Tag = "44";
            this.lblGoFormule.Text = "Formule";
            this.lblGoFormule.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoFormule.Click += new System.EventHandler(this.lblGoFormule_Click);
            // 
            // lblGoHisto
            // 
            this.lblGoHisto.AutoSize = true;
            this.lblGoHisto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoHisto.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoHisto.ForeColor = System.Drawing.Color.Blue;
            this.lblGoHisto.Location = new System.Drawing.Point(1382, 0);
            this.lblGoHisto.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoHisto.Name = "lblGoHisto";
            this.lblGoHisto.Size = new System.Drawing.Size(226, 29);
            this.lblGoHisto.TabIndex = 574;
            this.lblGoHisto.Tag = "44";
            this.lblGoHisto.Text = "Historique";
            this.lblGoHisto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoHisto.Click += new System.EventHandler(this.lblGoHisto_Click);
            // 
            // lblGoListeContrats
            // 
            this.lblGoListeContrats.AutoSize = true;
            this.lblGoListeContrats.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoListeContrats.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoListeContrats.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoListeContrats.ForeColor = System.Drawing.Color.Blue;
            this.lblGoListeContrats.Location = new System.Drawing.Point(1152, 0);
            this.lblGoListeContrats.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoListeContrats.Name = "lblGoListeContrats";
            this.lblGoListeContrats.Size = new System.Drawing.Size(226, 29);
            this.lblGoListeContrats.TabIndex = 573;
            this.lblGoListeContrats.Tag = "44";
            this.lblGoListeContrats.Text = "Liste des contrats";
            this.lblGoListeContrats.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoListeContrats.Click += new System.EventHandler(this.lblGoListeContrats_Click);
            // 
            // Label53
            // 
            this.Label53.AutoSize = true;
            this.Label53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label53.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label53.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label53.ForeColor = System.Drawing.Color.Blue;
            this.Label53.Location = new System.Drawing.Point(922, 0);
            this.Label53.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.Label53.Name = "Label53";
            this.Label53.Size = new System.Drawing.Size(226, 29);
            this.Label53.TabIndex = 572;
            this.Label53.Tag = "44";
            this.Label53.Text = "Contrat à facturer";
            this.Label53.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label53.Click += new System.EventHandler(this.Label53_Click);
            // 
            // lblGoIndice
            // 
            this.lblGoIndice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.lblGoIndice.AutoSize = true;
            this.lblGoIndice.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoIndice.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoIndice.ForeColor = System.Drawing.Color.Blue;
            this.lblGoIndice.Location = new System.Drawing.Point(692, 5);
            this.lblGoIndice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblGoIndice.Name = "lblGoIndice";
            this.lblGoIndice.Size = new System.Drawing.Size(226, 19);
            this.lblGoIndice.TabIndex = 571;
            this.lblGoIndice.Tag = "44";
            this.lblGoIndice.Text = "Indice";
            this.lblGoIndice.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblGoIndice.Click += new System.EventHandler(this.lblGoIndice_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.tableLayoutPanel22);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(162, 0);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1425, 115);
            this.groupBox1.TabIndex = 412;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Contrat";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel22.Controls.Add(this.txtLibelleCont2, 0, 2);
            this.tableLayoutPanel22.Controls.Add(this.txtLibelleCont1, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel23, 0, 0);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 3;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 34.04255F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.91489F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1419, 94);
            this.tableLayoutPanel22.TabIndex = 414;
            // 
            // txtLibelleCont2
            // 
            this.txtLibelleCont2.AccAcceptNumbersOnly = false;
            this.txtLibelleCont2.AccAllowComma = false;
            this.txtLibelleCont2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleCont2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleCont2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleCont2.AccHidenValue = "";
            this.txtLibelleCont2.AccNotAllowedChars = null;
            this.txtLibelleCont2.AccReadOnly = false;
            this.txtLibelleCont2.AccReadOnlyAllowDelete = false;
            this.txtLibelleCont2.AccRequired = false;
            this.txtLibelleCont2.BackColor = System.Drawing.Color.White;
            this.txtLibelleCont2.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleCont2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleCont2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelleCont2.ForeColor = System.Drawing.Color.Black;
            this.txtLibelleCont2.Location = new System.Drawing.Point(2, 64);
            this.txtLibelleCont2.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleCont2.MaxLength = 32767;
            this.txtLibelleCont2.Multiline = false;
            this.txtLibelleCont2.Name = "txtLibelleCont2";
            this.txtLibelleCont2.ReadOnly = false;
            this.txtLibelleCont2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleCont2.Size = new System.Drawing.Size(1415, 27);
            this.txtLibelleCont2.TabIndex = 416;
            this.txtLibelleCont2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleCont2.UseSystemPasswordChar = false;
            this.txtLibelleCont2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibelleCont2_KeyPress);
            // 
            // txtLibelleCont1
            // 
            this.txtLibelleCont1.AccAcceptNumbersOnly = false;
            this.txtLibelleCont1.AccAllowComma = false;
            this.txtLibelleCont1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleCont1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleCont1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibelleCont1.AccHidenValue = "";
            this.txtLibelleCont1.AccNotAllowedChars = null;
            this.txtLibelleCont1.AccReadOnly = false;
            this.txtLibelleCont1.AccReadOnlyAllowDelete = false;
            this.txtLibelleCont1.AccRequired = false;
            this.txtLibelleCont1.BackColor = System.Drawing.Color.White;
            this.txtLibelleCont1.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleCont1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibelleCont1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelleCont1.ForeColor = System.Drawing.Color.Black;
            this.txtLibelleCont1.Location = new System.Drawing.Point(2, 34);
            this.txtLibelleCont1.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleCont1.MaxLength = 32767;
            this.txtLibelleCont1.Multiline = false;
            this.txtLibelleCont1.Name = "txtLibelleCont1";
            this.txtLibelleCont1.ReadOnly = false;
            this.txtLibelleCont1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleCont1.Size = new System.Drawing.Size(1415, 27);
            this.txtLibelleCont1.TabIndex = 415;
            this.txtLibelleCont1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleCont1.UseSystemPasswordChar = false;
            this.txtLibelleCont1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibelleCont1_KeyPress);
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 5;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel23.Controls.Add(this.flowLayoutPanel2, 4, 0);
            this.tableLayoutPanel23.Controls.Add(this.txtAvenant, 3, 0);
            this.tableLayoutPanel23.Controls.Add(this.label33, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.txtNumContrat, 1, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1419, 32);
            this.tableLayoutPanel23.TabIndex = 414;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.cmdPremier);
            this.flowLayoutPanel2.Controls.Add(this.cmdPrecedent);
            this.flowLayoutPanel2.Controls.Add(this.cmdSuivant);
            this.flowLayoutPanel2.Controls.Add(this.cmdDernier);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1314, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(105, 32);
            this.flowLayoutPanel2.TabIndex = 0;
            // 
            // cmdPremier
            // 
            this.cmdPremier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPremier.FlatAppearance.BorderSize = 0;
            this.cmdPremier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPremier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPremier.Image = global::Axe_interDT.Properties.Resources.first_16;
            this.cmdPremier.Location = new System.Drawing.Point(0, 0);
            this.cmdPremier.Margin = new System.Windows.Forms.Padding(0);
            this.cmdPremier.Name = "cmdPremier";
            this.cmdPremier.Size = new System.Drawing.Size(25, 23);
            this.cmdPremier.TabIndex = 2;
            this.cmdPremier.UseVisualStyleBackColor = false;
            this.cmdPremier.Click += new System.EventHandler(this.cmdPremier_Click);
            // 
            // cmdPrecedent
            // 
            this.cmdPrecedent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPrecedent.FlatAppearance.BorderSize = 0;
            this.cmdPrecedent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrecedent.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPrecedent.Image = global::Axe_interDT.Properties.Resources.previous_16;
            this.cmdPrecedent.Location = new System.Drawing.Point(25, 0);
            this.cmdPrecedent.Margin = new System.Windows.Forms.Padding(0);
            this.cmdPrecedent.Name = "cmdPrecedent";
            this.cmdPrecedent.Size = new System.Drawing.Size(25, 23);
            this.cmdPrecedent.TabIndex = 1;
            this.cmdPrecedent.UseVisualStyleBackColor = false;
            this.cmdPrecedent.Click += new System.EventHandler(this.cmdPrecedent_Click);
            // 
            // cmdSuivant
            // 
            this.cmdSuivant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSuivant.FlatAppearance.BorderSize = 0;
            this.cmdSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSuivant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSuivant.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuivant.Image")));
            this.cmdSuivant.Location = new System.Drawing.Point(50, 0);
            this.cmdSuivant.Margin = new System.Windows.Forms.Padding(0);
            this.cmdSuivant.Name = "cmdSuivant";
            this.cmdSuivant.Size = new System.Drawing.Size(25, 23);
            this.cmdSuivant.TabIndex = 2;
            this.cmdSuivant.UseVisualStyleBackColor = false;
            this.cmdSuivant.Click += new System.EventHandler(this.cmdSuivant_Click);
            // 
            // cmdDernier
            // 
            this.cmdDernier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDernier.FlatAppearance.BorderSize = 0;
            this.cmdDernier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDernier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdDernier.Image = global::Axe_interDT.Properties.Resources.last_16;
            this.cmdDernier.Location = new System.Drawing.Point(75, 0);
            this.cmdDernier.Margin = new System.Windows.Forms.Padding(0);
            this.cmdDernier.Name = "cmdDernier";
            this.cmdDernier.Size = new System.Drawing.Size(25, 23);
            this.cmdDernier.TabIndex = 0;
            this.cmdDernier.UseVisualStyleBackColor = false;
            this.cmdDernier.Click += new System.EventHandler(this.cmdDernier_Click);
            // 
            // txtAvenant
            // 
            this.txtAvenant.AccAcceptNumbersOnly = false;
            this.txtAvenant.AccAllowComma = false;
            this.txtAvenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAvenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAvenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAvenant.AccHidenValue = "";
            this.txtAvenant.AccNotAllowedChars = null;
            this.txtAvenant.AccReadOnly = false;
            this.txtAvenant.AccReadOnlyAllowDelete = false;
            this.txtAvenant.AccRequired = false;
            this.txtAvenant.BackColor = System.Drawing.Color.White;
            this.txtAvenant.CustomBackColor = System.Drawing.Color.White;
            this.txtAvenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAvenant.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAvenant.ForeColor = System.Drawing.Color.Black;
            this.txtAvenant.Location = new System.Drawing.Point(717, 2);
            this.txtAvenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtAvenant.MaxLength = 32767;
            this.txtAvenant.Multiline = false;
            this.txtAvenant.Name = "txtAvenant";
            this.txtAvenant.ReadOnly = true;
            this.txtAvenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAvenant.Size = new System.Drawing.Size(595, 27);
            this.txtAvenant.TabIndex = 4;
            this.txtAvenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAvenant.UseSystemPasswordChar = false;
            this.txtAvenant.Leave += new System.EventHandler(this.txtAvenant_Leave);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(633, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(79, 32);
            this.label33.TabIndex = 1;
            this.label33.Text = "Avenant";
            this.label33.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 32);
            this.label1.TabIndex = 2;
            this.label1.Text = "N°:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtNumContrat
            // 
            this.txtNumContrat.AccAcceptNumbersOnly = false;
            this.txtNumContrat.AccAllowComma = false;
            this.txtNumContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumContrat.AccHidenValue = "";
            this.txtNumContrat.AccNotAllowedChars = null;
            this.txtNumContrat.AccReadOnly = false;
            this.txtNumContrat.AccReadOnlyAllowDelete = false;
            this.txtNumContrat.AccRequired = false;
            this.txtNumContrat.BackColor = System.Drawing.Color.White;
            this.txtNumContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtNumContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNumContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNumContrat.ForeColor = System.Drawing.Color.Black;
            this.txtNumContrat.Location = new System.Drawing.Point(33, 2);
            this.txtNumContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumContrat.MaxLength = 32767;
            this.txtNumContrat.Multiline = false;
            this.txtNumContrat.Name = "txtNumContrat";
            this.txtNumContrat.ReadOnly = true;
            this.txtNumContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumContrat.Size = new System.Drawing.Size(595, 27);
            this.txtNumContrat.TabIndex = 3;
            this.txtNumContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumContrat.UseSystemPasswordChar = false;
            this.txtNumContrat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNumContrat_KeyPress);
            // 
            // cmdAjoutAvenant
            // 
            this.cmdAjoutAvenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdAjoutAvenant.FlatAppearance.BorderSize = 0;
            this.cmdAjoutAvenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjoutAvenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdAjoutAvenant.Location = new System.Drawing.Point(841, 316);
            this.cmdAjoutAvenant.Name = "cmdAjoutAvenant";
            this.cmdAjoutAvenant.Size = new System.Drawing.Size(19, 17);
            this.cmdAjoutAvenant.TabIndex = 509;
            this.cmdAjoutAvenant.Text = "+";
            this.cmdAjoutAvenant.UseVisualStyleBackColor = false;
            this.cmdAjoutAvenant.Visible = false;
            this.cmdAjoutAvenant.Click += new System.EventHandler(this.cmdAjoutAvenant_Click);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel3);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Margin = new System.Windows.Forms.Padding(0, 0, 2, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(158, 115);
            this.Frame1.TabIndex = 411;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Selection";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80.81395F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.18605F));
            this.tableLayoutPanel3.Controls.Add(this.lblContrat, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblSyndic, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblImmeuble, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdSyndic, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.cmdImmeuble, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdContrat, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(152, 94);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // lblContrat
            // 
            this.lblContrat.AutoSize = true;
            this.lblContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContrat.Location = new System.Drawing.Point(3, 46);
            this.lblContrat.Name = "lblContrat";
            this.lblContrat.Size = new System.Drawing.Size(116, 48);
            this.lblContrat.TabIndex = 386;
            this.lblContrat.Text = "Par contrat";
            // 
            // lblSyndic
            // 
            this.lblSyndic.AutoSize = true;
            this.lblSyndic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSyndic.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSyndic.Location = new System.Drawing.Point(3, 0);
            this.lblSyndic.Name = "lblSyndic";
            this.lblSyndic.Size = new System.Drawing.Size(116, 23);
            this.lblSyndic.TabIndex = 384;
            this.lblSyndic.Text = "Par gérant";
            // 
            // lblImmeuble
            // 
            this.lblImmeuble.AutoSize = true;
            this.lblImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblImmeuble.Location = new System.Drawing.Point(3, 23);
            this.lblImmeuble.Name = "lblImmeuble";
            this.lblImmeuble.Size = new System.Drawing.Size(116, 23);
            this.lblImmeuble.TabIndex = 385;
            this.lblImmeuble.Text = "Par immeuble";
            // 
            // cmdSyndic
            // 
            this.cmdSyndic.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSyndic.FlatAppearance.BorderSize = 0;
            this.cmdSyndic.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSyndic.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSyndic.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdSyndic.Location = new System.Drawing.Point(123, 1);
            this.cmdSyndic.Margin = new System.Windows.Forms.Padding(1);
            this.cmdSyndic.Name = "cmdSyndic";
            this.cmdSyndic.Size = new System.Drawing.Size(25, 20);
            this.cmdSyndic.TabIndex = 387;
            this.cmdSyndic.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolTip1.SetToolTip(this.cmdSyndic, "Recherche de l\'immeuble");
            this.cmdSyndic.UseVisualStyleBackColor = false;
            this.cmdSyndic.Click += new System.EventHandler(this.cmdSyndic_Click);
            // 
            // cmdImmeuble
            // 
            this.cmdImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdImmeuble.Location = new System.Drawing.Point(123, 24);
            this.cmdImmeuble.Margin = new System.Windows.Forms.Padding(1);
            this.cmdImmeuble.Name = "cmdImmeuble";
            this.cmdImmeuble.Size = new System.Drawing.Size(25, 20);
            this.cmdImmeuble.TabIndex = 388;
            this.cmdImmeuble.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolTip1.SetToolTip(this.cmdImmeuble, "Recherche de l\'immeuble");
            this.cmdImmeuble.UseVisualStyleBackColor = false;
            this.cmdImmeuble.Click += new System.EventHandler(this.cmdImmeuble_Click);
            // 
            // cmdContrat
            // 
            this.cmdContrat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdContrat.FlatAppearance.BorderSize = 0;
            this.cmdContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdContrat.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdContrat.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdContrat.Location = new System.Drawing.Point(123, 47);
            this.cmdContrat.Margin = new System.Windows.Forms.Padding(1);
            this.cmdContrat.Name = "cmdContrat";
            this.cmdContrat.Size = new System.Drawing.Size(25, 20);
            this.cmdContrat.TabIndex = 389;
            this.cmdContrat.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.toolTip1.SetToolTip(this.cmdContrat, "Recherche de l\'immeuble");
            this.cmdContrat.UseVisualStyleBackColor = false;
            this.cmdContrat.Click += new System.EventHandler(this.cmdContrat_Click);
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImprimer.Location = new System.Drawing.Point(132, 44);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdImprimer.TabIndex = 375;
            this.toolTip1.SetToolTip(this.cmdImprimer, "Imprimer");
            this.cmdImprimer.UseVisualStyleBackColor = false;
            this.cmdImprimer.Visible = false;
            this.cmdImprimer.Click += new System.EventHandler(this.cmdImprimer_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(131, 5);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 1;
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(193, 5);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 0;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(68, 5);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 361;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(4, 44);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 3;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            this.cmdSupprimer.Click += new System.EventHandler(this.cmdSupprimer_Click);
            // 
            // cmdRecherche
            // 
            this.cmdRecherche.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRecherche.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRecherche.FlatAppearance.BorderSize = 0;
            this.cmdRecherche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRecherche.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdRecherche.Location = new System.Drawing.Point(68, 44);
            this.cmdRecherche.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRecherche.Name = "cmdRecherche";
            this.cmdRecherche.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmdRecherche.Size = new System.Drawing.Size(60, 35);
            this.cmdRecherche.TabIndex = 2;
            this.cmdRecherche.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdRecherche, "Recherche");
            this.cmdRecherche.UseVisualStyleBackColor = false;
            this.cmdRecherche.Visible = false;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 3;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 261F));
            this.tableLayoutPanel21.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel21.Controls.Add(this.Frame1, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.groupBox1, 1, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(1850, 115);
            this.tableLayoutPanel21.TabIndex = 413;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdAnnuler);
            this.panel1.Controls.Add(this.cmdAjouter);
            this.panel1.Controls.Add(this.cmdMAJ);
            this.panel1.Controls.Add(this.cmdImprimer);
            this.panel1.Controls.Add(this.cmdSupprimer);
            this.panel1.Controls.Add(this.cmdRecherche);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(1592, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(255, 109);
            this.panel1.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.SSTab1, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel21, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel24, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel25, 0, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 115F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 92F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 16F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel4.TabIndex = 510;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 8;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel24.Controls.Add(this.lblAutoliquidation, 7, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoFacturation, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoFormule, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoTypeRevision, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoIndice, 3, 0);
            this.tableLayoutPanel24.Controls.Add(this.Label53, 4, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoHisto, 6, 0);
            this.tableLayoutPanel24.Controls.Add(this.lblGoListeContrats, 5, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(3, 118);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(1844, 29);
            this.tableLayoutPanel24.TabIndex = 414;
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 66.66666F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 119F));
            this.tableLayoutPanel25.Controls.Add(this.tableLayoutPanel26, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.tableLayoutPanel27, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.tableLayoutPanel28, 2, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(3, 153);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(1844, 86);
            this.tableLayoutPanel25.TabIndex = 415;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 3;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel26.Controls.Add(this.cmdRechercheImmeuble, 2, 1);
            this.tableLayoutPanel26.Controls.Add(this.txtSyndic, 1, 2);
            this.tableLayoutPanel26.Controls.Add(this.txtCodeImmeuble, 1, 1);
            this.tableLayoutPanel26.Controls.Add(this.label4, 0, 1);
            this.tableLayoutPanel26.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel26.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel26.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 3;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 29F));
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 21F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(575, 86);
            this.tableLayoutPanel26.TabIndex = 0;
            // 
            // txtSyndic
            // 
            this.txtSyndic.AccAcceptNumbersOnly = false;
            this.txtSyndic.AccAllowComma = false;
            this.txtSyndic.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSyndic.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSyndic.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSyndic.AccHidenValue = "";
            this.txtSyndic.AccNotAllowedChars = null;
            this.txtSyndic.AccReadOnly = false;
            this.txtSyndic.AccReadOnlyAllowDelete = false;
            this.txtSyndic.AccRequired = false;
            this.txtSyndic.BackColor = System.Drawing.Color.White;
            this.txtSyndic.CustomBackColor = System.Drawing.Color.White;
            this.txtSyndic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSyndic.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSyndic.ForeColor = System.Drawing.Color.Blue;
            this.txtSyndic.Location = new System.Drawing.Point(84, 56);
            this.txtSyndic.Margin = new System.Windows.Forms.Padding(2);
            this.txtSyndic.MaxLength = 32767;
            this.txtSyndic.Multiline = false;
            this.txtSyndic.Name = "txtSyndic";
            this.txtSyndic.ReadOnly = true;
            this.txtSyndic.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSyndic.Size = new System.Drawing.Size(459, 27);
            this.txtSyndic.TabIndex = 1;
            this.txtSyndic.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSyndic.UseSystemPasswordChar = false;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(84, 27);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = true;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(459, 27);
            this.txtCodeImmeuble.TabIndex = 0;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            this.txtCodeImmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeImmeuble_KeyPress);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 2;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Controls.Add(this.label5, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.txtNomImmeuble, 0, 1);
            this.tableLayoutPanel27.Controls.Add(this.txtNomSyndic, 0, 2);
            this.tableLayoutPanel27.Controls.Add(this.lblChaudiereCondo, 1, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(575, 0);
            this.tableLayoutPanel27.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 4;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(1150, 86);
            this.tableLayoutPanel27.TabIndex = 1;
            // 
            // txtNomImmeuble
            // 
            this.txtNomImmeuble.AccAcceptNumbersOnly = false;
            this.txtNomImmeuble.AccAllowComma = false;
            this.txtNomImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomImmeuble.AccHidenValue = "";
            this.txtNomImmeuble.AccNotAllowedChars = null;
            this.txtNomImmeuble.AccReadOnly = false;
            this.txtNomImmeuble.AccReadOnlyAllowDelete = false;
            this.txtNomImmeuble.AccRequired = false;
            this.txtNomImmeuble.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel27.SetColumnSpan(this.txtNomImmeuble, 2);
            this.txtNomImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtNomImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNomImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomImmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtNomImmeuble.Location = new System.Drawing.Point(2, 22);
            this.txtNomImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomImmeuble.MaxLength = 32767;
            this.txtNomImmeuble.Multiline = false;
            this.txtNomImmeuble.Name = "txtNomImmeuble";
            this.txtNomImmeuble.ReadOnly = true;
            this.txtNomImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomImmeuble.Size = new System.Drawing.Size(1146, 27);
            this.txtNomImmeuble.TabIndex = 0;
            this.txtNomImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomImmeuble.UseSystemPasswordChar = false;
            // 
            // txtNomSyndic
            // 
            this.txtNomSyndic.AccAcceptNumbersOnly = false;
            this.txtNomSyndic.AccAllowComma = false;
            this.txtNomSyndic.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomSyndic.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomSyndic.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomSyndic.AccHidenValue = "";
            this.txtNomSyndic.AccNotAllowedChars = null;
            this.txtNomSyndic.AccReadOnly = false;
            this.txtNomSyndic.AccReadOnlyAllowDelete = false;
            this.txtNomSyndic.AccRequired = false;
            this.txtNomSyndic.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel27.SetColumnSpan(this.txtNomSyndic, 2);
            this.txtNomSyndic.CustomBackColor = System.Drawing.Color.White;
            this.txtNomSyndic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNomSyndic.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomSyndic.ForeColor = System.Drawing.Color.Blue;
            this.txtNomSyndic.Location = new System.Drawing.Point(2, 55);
            this.txtNomSyndic.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomSyndic.MaxLength = 32767;
            this.txtNomSyndic.Multiline = false;
            this.txtNomSyndic.Name = "txtNomSyndic";
            this.txtNomSyndic.ReadOnly = true;
            this.txtNomSyndic.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomSyndic.Size = new System.Drawing.Size(1146, 27);
            this.txtNomSyndic.TabIndex = 1;
            this.txtNomSyndic.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomSyndic.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 1;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.Frame9, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.cmdResilié, 0, 1);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(1725, 0);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 2;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(119, 86);
            this.tableLayoutPanel28.TabIndex = 2;
            // 
            // UserDocFicheContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Controls.Add(this.cmdAjoutAvenant);
            this.Name = "UserDocFicheContrat";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche Contrat";
            this.VisibleChanged += new System.EventHandler(this.UserDocFicheContrat_VisibleChanged);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel11.PerformLayout();
            this.fraFormule.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel10.PerformLayout();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.Frame14.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBDropDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.Frame11.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.Frame4.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.Frame5.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel20.PerformLayout();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            this.Frame6.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel21.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel27.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblContrat;
        private System.Windows.Forms.Label lblSyndic;
        private System.Windows.Forms.Label lblImmeuble;
        public System.Windows.Forms.Button cmdSyndic;
        public System.Windows.Forms.Button cmdImmeuble;
        public System.Windows.Forms.Button cmdContrat;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtNumContrat;
        public iTalk.iTalk_TextBox_Small2 txtAvenant;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 txtLibelleCont1;
        public iTalk.iTalk_TextBox_Small2 txtLibelleCont2;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtNomImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtNomSyndic;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox Frame9;
        public System.Windows.Forms.CheckBox ChkResiliee;
        public System.Windows.Forms.Button cmdResilié;
        public System.Windows.Forms.Button cmdPremier;
        public System.Windows.Forms.Button cmdPrecedent;
        public System.Windows.Forms.Button cmdDernier;
        public System.Windows.Forms.Button cmdSuivant;
        private System.Windows.Forms.Label lblGoIndice;
        private System.Windows.Forms.Label lblGoFacturation;
        private System.Windows.Forms.Label lblGoTypeRevision;
        private System.Windows.Forms.Label lblGoFormule;
        private System.Windows.Forms.Label lblGoHisto;
        private System.Windows.Forms.Label lblGoListeContrats;
        private System.Windows.Forms.Label Label53;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        public iTalk.iTalk_TextBox_Small2 txtRefGerant;
        public iTalk.iTalk_TextBox_Small2 Text1;
        public iTalk.iTalk_TextBox_Small2 Text3;
        public iTalk.iTalk_TextBox_Small2 Text2;
        public iTalk.iTalk_TextBox_Small2 txtCON_CompAdresse1;
        public iTalk.iTalk_TextBox_Small2 txtCON_CompAdresse2;
        public iTalk.iTalk_TextBox_Small2 txtFactAdresse;
        public iTalk.iTalk_TextBox_Small2 txtcommentaire2;
        public iTalk.iTalk_TextBox_Small2 txtFactNomImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtLibelleFact;
        public iTalk.iTalk_TextBox_Small2 txtFactCP;
        public iTalk.iTalk_TextBox_Small2 txtFactVille;
        public iTalk.iTalk_TextBox_Small2 txtIntervAdresse;
        public iTalk.iTalk_TextBox_Small2 txtLibelleInterv1;
        public iTalk.iTalk_TextBox_Small2 txtLibelleInterv2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        public System.Windows.Forms.CheckBox Check3;
        public System.Windows.Forms.CheckBox chkFactureManuelle;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtSyndic;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button cmdImprimer;
        public System.Windows.Forms.Button cmdRecherche;
        public System.Windows.Forms.Button cmdSupprimer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtCodeArticle;
        public iTalk.iTalk_TextBox_Small2 txtBaseContractuelle;
        public iTalk.iTalk_TextBox_Small2 txtBaseActualisee;
        public iTalk.iTalk_TextBox_Small2 txtDesignation1;
        public iTalk.iTalk_TextBox_Small2 txtBase;
        public iTalk.iTalk_TextBox_Small2 txtDateActualisee;
        public System.Windows.Forms.Button cmdCodeArticle;
        public iTalk.iTalk_TextBox_Small2 txtBaseRevisee;
        public iTalk.iTalk_TextBox_Small2 txtDateRevision;
        public iTalk.iTalk_TextBox_Small2 txtDesignation2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        public iTalk.iTalk_TextBox_Small2 txtCON_TypeContrat;
        private System.Windows.Forms.RadioButton optRevCoefFixe;
        private System.Windows.Forms.RadioButton optSansRev;
        private System.Windows.Forms.RadioButton optRevFormule;
        public iTalk.iTalk_TextBox_Small2 txtCON_Coef;
        private System.Windows.Forms.GroupBox fraFormule;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        public iTalk.iTalk_TextBox_Small2 txtTypeFormule;
        public iTalk.iTalk_TextBox_Small2 txtIndice1;
        public iTalk.iTalk_TextBox_Small2 txtIndice3;
        public iTalk.iTalk_TextBox_Small2 txtIndice2;
        public iTalk.iTalk_TextBox_Small2 txtIndice4;
        public iTalk.iTalk_TextBox_Small2 txtIndice6;
        public iTalk.iTalk_TextBox_Small2 txtIndice1Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice2Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice3Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice4Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice5;
        public iTalk.iTalk_TextBox_Small2 txtIndice5Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice6Valeur;
        public iTalk.iTalk_TextBox_Small2 txtIndice1Date;
        public iTalk.iTalk_TextBox_Small2 txtIndice2Date;
        public iTalk.iTalk_TextBox_Small2 txtIndice3Date;
        public iTalk.iTalk_TextBox_Small2 txtIndice4Date;
        public iTalk.iTalk_TextBox_Small2 txtIndice5Date;
        public iTalk.iTalk_TextBox_Small2 txtIndice6Date;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.CheckBox Check2;
        private System.Windows.Forms.CheckBox Check1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        public System.Windows.Forms.Button cmdIndice1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        public System.Windows.Forms.Button cmdIndice2;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        public System.Windows.Forms.Button cmdIndice3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        public System.Windows.Forms.Button cmdIndice4;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        public System.Windows.Forms.Button cmdIndice5;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        public System.Windows.Forms.Button cmdIndice6;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        public iTalk.iTalk_TextBox_Small2 txtNomAnalytique;
        public iTalk.iTalk_TextBox_Small2 txtCGINITULE;
        public iTalk.iTalk_TextBox_Small2 txtCGNUM;
        public iTalk.iTalk_TextBox_Small2 txtLibelleActivite;
        private System.Windows.Forms.GroupBox Frame14;
        public System.Windows.Forms.Button Command4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        public iTalk.iTalk_TextBox_Small2 txtActivite;
        public iTalk.iTalk_TextBox_Small2 txtAnalytique;
        public iTalk.iTalk_TextBox_Small2 txtCON_AnaModif;
        public System.Windows.Forms.Button cmdActivite;
        public System.Windows.Forms.Button cmdSecteur;
        private System.Windows.Forms.CheckBox ckkCON_AnaModif;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        private System.Windows.Forms.GroupBox Frame11;
        private System.Windows.Forms.GroupBox Frame4;
        private System.Windows.Forms.GroupBox Frame5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.RadioButton Option9;
        private System.Windows.Forms.RadioButton Option8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.RadioButton optAutre;
        private System.Windows.Forms.RadioButton optAnnuel;
        private System.Windows.Forms.RadioButton Option6;
        private System.Windows.Forms.RadioButton Option7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        private System.Windows.Forms.CheckBox chkCalcEcheance;
        public iTalk.iTalk_TextBox_Small2 txtCON_Indice1rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Indice2rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Indice3rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Valeur1rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Valeur2rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Valeur3rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Date1rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Date2rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_Date3rev;
        public iTalk.iTalk_TextBox_Small2 txtCON_EchuEchoir;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.GroupBox Frame6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label49;
        private iTalk.iTalk_RichTextBox txtANCC_CommRESILIE;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        private System.Windows.Forms.CheckBox chkANCC_Reconduction;
        private System.Windows.Forms.CheckBox chkANCC_RenovPerdue;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private iTalk.iTalk_RichTextBox txtANCC_CommACCEPTE;
        private System.Windows.Forms.Label label35;
        public iTalk.iTalk_TextBox_Small2 txtANCC_DateReceptResil;
        public System.Windows.Forms.Button cmdAjoutAvenant;
        public System.Windows.Forms.Button Command2;
        public iTalk.iTalk_TextBox_Small2 txtcommentaire1;
        private System.Windows.Forms.GroupBox groupBox6;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        private System.Windows.Forms.Label lblChaudiereCondo;
        private System.Windows.Forms.Label lblAutoliquidation;
        private System.Windows.Forms.Label label28;
        public iTalk.iTalk_TextBox_Small2 txtIntervVille;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private iTalk.iTalk_TextBox_Small2 txtFormule;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown2;
        private Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBDropDown1;
        private System.Windows.Forms.ToolTip toolTip1;
        public iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small21;
        private Infragistics.Win.UltraWinGrid.UltraCombo ultraCombo1;
        public iTalk.iTalk_TextBox_Small2 txtIntervCP;
    }
}
