﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.Contrat.FicheContrat.Historique
{
    public partial class UserDocHistoContrat : UserControl
    {
        public UserDocHistoContrat()
        {
            InitializeComponent();
        }

        DataTable rsHisto = new DataTable();
        string sOrderBy;
        string sAscDesc;
        ModAdo ModAdo1 = new ModAdo();
        private void fc_clean()
        {

            cmbCodeimmeuble.Text = "";
            cmbCodeGerant.Text = "";
            txtDateFin.Text = "";
            txtDateEffet.Text = "";
            ChkResiliee.CheckState = CheckState.Unchecked;
            chkNonFacturable.CheckState = CheckState.Unchecked;

        }

        private void cmdClean_Click(Object eventSender, EventArgs eventArgs)
        {
            fc_clean();
        }

        private void cmdRefresh_Click(Object eventSender, EventArgs eventArgs)
        {

            fc_LoadHistoContrat();
        }

        private void cmdGerant_Click(Object eventSender, EventArgs eventArgs)
        {
            cmbCodeGerant_KeyPress(cmbCodeGerant, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

        private void cmbCodeGerant_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;
            DataTable rsGerant;
            bool blnGoRecherche = false;

            try
            {
                blnGoRecherche = false;
                if (KeyAscii == 13)
                {
                    rsGerant = new DataTable();
                    rsGerant = ModAdo1.fc_OpenRecordSet("SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 LIKE '%" + cmbCodeGerant.Text + "%'");
                    if (rsGerant.Rows.Count > 0)
                    {
                        if (rsGerant.Rows.Count > 1)
                        {
                            blnGoRecherche = true;
                        }
                    }
                    else
                    {
                        blnGoRecherche = true;
                    }

                    if (blnGoRecherche == true)
                    {


                        string req = "SELECT Code1 as \"Code Gérant\"," + " Adresse1 as \"Adresse\","
                            + " Ville as \"Ville\" , CodePostal AS \"Code Postal\" FROM Table1 ";
                        string where = "";

                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un gérant" };
                        fg.SetValues(new Dictionary<string, string> { { "Code1", cmbCodeGerant.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {

                            cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                if (fg.ugResultat.ActiveRow != null)
                                {
                                    cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose();
                                    fg.Close();
                                }
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    else
                    {
                        fc_LoadHistoContrat();
                    }
                    rsGerant = null;

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";cmbCodeGerant_KeyPress");
            }

        }
        private void cmbCodeimmeuble_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;

            try
            {
                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(ModAdo1.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + cmbCodeimmeuble.Text + "'")))
                    {


                        string req1 = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";
                        string where1 = "";

                        //.Move Screen.Width - .Width, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
                        SearchTemplate fg = new SearchTemplate(this, null, req1, where1, "") { Text = "Recherche d'un immeuble" };
                        fg.SetValues(new Dictionary<string, string> { { "Code1", cmbCodeGerant.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {

                            cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();
                    }
                    fc_LoadHistoContrat();
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";cmdrecherche_Click");
            }

        }
        private void cmdRechercheImmeuble_Click(Object eventSender, EventArgs eventArgs)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }
        //tested
        private void fc_LoadHistoContrat()
        {
            string sSQL = null;
            string sWhere = null;

            try
            {

                if (!string.IsNullOrEmpty(txtDateEffet.Text))
                {
                    if (!General.IsDate(txtDateEffet.Text) || txtDateEffet.Text.Contains("."))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtDateEffet.Focus();
                        txtDateEffet.SelectionStart = 0;
                        txtDateEffet.SelectionLength = txtDateEffet.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDateEffet.Text = String.Format(txtDateEffet.Text, "dd/MM/yyyy");
                        if (Convert.ToDateTime(txtDateEffet.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateEffet.Focus();
                            txtDateEffet.SelectionStart = 0;
                            txtDateEffet.SelectionLength = txtDateEffet.Text.Length;
                            return;
                        }
                    }
                }


                if (!string.IsNullOrEmpty(txtDateFin.Text))
                {
                    if (!General.IsDate(txtDateFin.Text) || txtDateFin.Text.Contains("."))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtDateFin.Focus();
                        txtDateFin.SelectionStart = 0;
                        txtDateFin.SelectionLength = txtDateFin.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDateFin.Text = string.Format(txtDateFin.Text, "dd/MM/yyyy");
                        if (Convert.ToDateTime(txtDateFin.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateFin.Focus();
                            txtDateFin.SelectionStart = 0;
                            txtDateFin.SelectionLength = txtDateFin.Text.Length;
                            return;
                        }
                    }
                }

                sSQL = "SELECT     Contrat.CodeImmeuble, Immeuble.Code1 AS [code client], Contrat.NumContrat, Contrat.Avenant, " + " Contrat.DateEffet, Contrat.DateFin, Contrat.Resiliee, " + " Contrat.nonFacturable , Contrat.CodeArticle, FacArticle.Designation1," + " Contrat.LibelleCont1, Contrat.LibelleCont2, Contrat.CON_Noauto" + " FROM         Contrat INNER JOIN" + " Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN " + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN " + " FacArticle ON Contrat.CodeArticle = FacArticle.CodeArticle";


                sWhere = "";

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE Contrat.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Contrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbCodeGerant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeGerant.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtDateEffet.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Contrat.DateEffet>='" + string.Format(txtDateEffet.Text, General.FormatDateSansHeureSQL) + " 00:00:00" + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Contrat.DateEffet>='" + string.Format(txtDateEffet.Text, General.FormatDateSansHeureSQL) + " 00:00:00" + "'";

                    }
                }

                if (!string.IsNullOrEmpty(txtDateFin.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Contrat.DateFin>='" +/* string.Format(*/txtDateFin.Value/*, General.FormatDateSansHeureSQL) + " 00:00:00"*/ + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Contrat.DateFin>='" +/* string.Format(*/txtDateFin.Value/*, General.FormatDateSansHeureSQL) + " 00:00:00" */+ "'";

                    }
                }

                if (ChkResiliee.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Contrat.Resiliee = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and Contrat.Resiliee = 1";

                    }
                }

                if (chkNonFacturable.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Contrat.nonFacturable = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and Contrat.nonFacturable = 1";

                    }
                }

                Cursor.Current = Cursors.WaitCursor;

                if (string.IsNullOrEmpty(sOrderBy))
                {
                    sOrderBy = " ORDER BY Immeuble.Code1, Contrat.CodeImmeuble ";
                }

                rsHisto = ModAdo1.fc_OpenRecordSet(sSQL + " " + sWhere + " " + sOrderBy);

                Cursor.Current = Cursors.Arrow;
                //ssGridHistoCtr.ReBind();
                ssGridHistoCtr.DataSource = rsHisto;
                //ssGridHistoCtr.Rows = rsHisto.Rows.Count;
                lblTotal.Text = "Total: " + rsHisto.Rows.Count;
                FC_SaveParam();
                return;

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_LoadHistoContrat");
            }
        }
        //tested

        private void FC_SaveParam()
        {
            General.saveInReg("UserDocHistoContrat", "cmbCodeimmeuble", cmbCodeimmeuble.Text);
            General.saveInReg("UserDocHistoContrat", "cmbCodeGerant", cmbCodeGerant.Text);
            General.saveInReg("UserDocHistoContrat", "txtDateEffet", (txtDateEffet.Value == null ? "" : txtDateEffet.Value.ToString()));
            General.saveInReg("UserDocHistoContrat", "txtDateFin", (txtDateFin.Value == null ? "" : txtDateFin.Value.ToString()));

            General.saveInReg("UserDocHistoContrat", "ChkResiliee", Convert.ToString(ChkResiliee.Checked));
            General.saveInReg("UserDocHistoContrat", "chkNonFacturable", Convert.ToString(chkNonFacturable.Checked));

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_GetParam()
        {

            cmbCodeimmeuble.Text = General.getFrmReg("UserDocHistoContrat", "cmbCodeimmeuble", "");
            cmbCodeGerant.Text = General.getFrmReg("UserDocHistoContrat", "cmbCodeGerant", "");
            txtDateEffet.Value = General.getFrmReg("UserDocHistoContrat", "txtDateEffet", "");
            txtDateFin.Value = General.getFrmReg("UserDocHistoContrat", "txtDateFin", "");

            ChkResiliee.Checked = General.getFrmReg("UserDocHistoContrat", "ChkResiliee").ToString() == "1" ? true : false;
            chkNonFacturable.Checked = General.getFrmReg("UserDocHistoContrat", "chkNonFacturable").ToString() == "1" ? true : false;


        }


        private void ssGridHistoCtr_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            //var _with3 = ssGridHistoCtr;
            FC_SaveParam();
            //Interaction.SaveSetting(General.cFrNomApp, this.Name, "CON_Noauto", ssGridHistoCtr.ActiveRow.Cells["CON_Noauto"].Text);

            General.saveInReg("UserDocHistoContrat", "CON_Noauto", ssGridHistoCtr.ActiveRow.Cells["CON_Noauto"].Text);
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, ssGridHistoCtr.ActiveRow.Cells["NumContrat"].Text, ssGridHistoCtr.ActiveRow.Cells["Avenant"].Text);
            //  ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGFACCONTRAT);
            View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
        }

        private void ssGridHistoCtr_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        //Tested
        private void UserDocHistoContrat_VisibleChanged(object sender, EventArgs e)
        {
            string sCON_Noauto = null;

            /*  if (ModMain.bActivate == true)
              {

                  
                  if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                  {
                      imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                      if (Information.IsNumeric(General.imgTop))
                      {
                          imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                      }
                      if (Information.IsNumeric(General.imgLeft))
                      {
                          imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                      }
                  }
               
                  if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
                  {
                      lblNomSociete.Text = General.NomSousSociete;
                      System.Windows.Forms.Application.DoEvents();
                  }

                  lblNavigation[0].Text = General.sNomLien0;
                  lblNavigation[1].Text = General.sNomLien1;
                  lblNavigation[2].Text = General.sNomLien2;*/
            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocHistoContrat");

            fc_GetParam();

            fc_LoadHistoContrat();

            sCON_Noauto = General.getFrmReg("UserDocHistoContrat", "CON_Noauto", "");


            if (!string.IsNullOrEmpty(sCON_Noauto) && General.IsNumeric(sCON_Noauto))
            {

                // cmbIntervention_Click
                if ((rsHisto != null))
                {
                    //if (rsHisto.State == ADODB.ObjectStateEnum.adStateOpen)
                    //{
                    //rsHisto.Find("CON_Noauto = " + sCON_Noauto , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                    if (rsHisto.Rows.Count > 0)
                    {
                        //ssGridHistoCtr.Bookmark = rsHisto.Bookmark;
                        //ssGridHistoCtr.SelBookmarks.RemoveAll();
                        //ssGridHistoCtr.SelBookmarks.Add((ssGridHistoCtr.GetBookmark(0)));
                        var row = ssGridHistoCtr.Rows.First(r => r.GetCellValue("CON_Noauto").ToString() == sCON_Noauto);
                        if (row != null)
                            ssGridHistoCtr.ActiveRow = row;
                    }
                    //}
                }
            }


        }
        private void txtDateEffet_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                fc_LoadHistoContrat();
                e.KeyChar = (char)0;
            }
        }

        private void ssGridHistoCtr_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for (int i = 0; i < ssGridHistoCtr.DisplayLayout.Bands[0].Columns.Count - 1; i++)
                ssGridHistoCtr.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;

            ssGridHistoCtr.DisplayLayout.Bands[0].Columns["CON_Noauto"].Hidden = true;
            ssGridHistoCtr.DisplayLayout.Bands[0].Columns["LibelleCont1"].Header.Caption = "Commentaire1";
            ssGridHistoCtr.DisplayLayout.Bands[0].Columns["LibelleCont2"].Header.Caption = "Commentaire2";
        }
    }
}
