namespace Axe_interDT.Views.Contrat.FicheContrat.Historique
{
    partial class UserDocHistoContrat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheGroupe = new System.Windows.Forms.Button();
            this.cmbCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.cmbCodeGerant = new iTalk.iTalk_TextBox_Small2();
            this.ChkResiliee = new System.Windows.Forms.CheckBox();
            this.chkNonFacturable = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDateFin = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtDateEffet = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblTotal = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGridHistoCtr = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdRefresh = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEffet)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHistoCtr)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 4;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.cmdRechercheGroupe, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbCodeimmeuble, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cmbCodeGerant, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.ChkResiliee, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.chkNonFacturable, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 45);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 62);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdRechercheGroupe
            // 
            this.cmdRechercheGroupe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheGroupe.FlatAppearance.BorderSize = 0;
            this.cmdRechercheGroupe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheGroupe.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheGroupe.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheGroupe.Location = new System.Drawing.Point(803, 3);
            this.cmdRechercheGroupe.Name = "cmdRechercheGroupe";
            this.cmdRechercheGroupe.Size = new System.Drawing.Size(24, 20);
            this.cmdRechercheGroupe.TabIndex = 505;
            this.toolTip1.SetToolTip(this.cmdRechercheGroupe, "Recherche de l\'immeuble");
            this.cmdRechercheGroupe.UseVisualStyleBackColor = false;
            this.cmdRechercheGroupe.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // cmbCodeimmeuble
            // 
            this.cmbCodeimmeuble.AccAcceptNumbersOnly = false;
            this.cmbCodeimmeuble.AccAllowComma = false;
            this.cmbCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.cmbCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.cmbCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.cmbCodeimmeuble.AccHidenValue = "";
            this.cmbCodeimmeuble.AccNotAllowedChars = null;
            this.cmbCodeimmeuble.AccReadOnly = false;
            this.cmbCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.cmbCodeimmeuble.AccRequired = false;
            this.cmbCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.cmbCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.cmbCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.cmbCodeimmeuble.Location = new System.Drawing.Point(122, 2);
            this.cmbCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCodeimmeuble.MaxLength = 32767;
            this.cmbCodeimmeuble.Multiline = false;
            this.cmbCodeimmeuble.Name = "cmbCodeimmeuble";
            this.cmbCodeimmeuble.ReadOnly = false;
            this.cmbCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.cmbCodeimmeuble.Size = new System.Drawing.Size(676, 27);
            this.cmbCodeimmeuble.TabIndex = 502;
            this.cmbCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.cmbCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 32);
            this.label2.TabIndex = 386;
            this.label2.Text = "Gérant";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(114, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Immeuble";
            // 
            // cmbCodeGerant
            // 
            this.cmbCodeGerant.AccAcceptNumbersOnly = false;
            this.cmbCodeGerant.AccAllowComma = false;
            this.cmbCodeGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.cmbCodeGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.cmbCodeGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.cmbCodeGerant.AccHidenValue = "";
            this.cmbCodeGerant.AccNotAllowedChars = null;
            this.cmbCodeGerant.AccReadOnly = false;
            this.cmbCodeGerant.AccReadOnlyAllowDelete = false;
            this.cmbCodeGerant.AccRequired = false;
            this.cmbCodeGerant.BackColor = System.Drawing.Color.White;
            this.cmbCodeGerant.CustomBackColor = System.Drawing.Color.White;
            this.cmbCodeGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeGerant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbCodeGerant.ForeColor = System.Drawing.Color.Black;
            this.cmbCodeGerant.Location = new System.Drawing.Point(122, 32);
            this.cmbCodeGerant.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCodeGerant.MaxLength = 32767;
            this.cmbCodeGerant.Multiline = false;
            this.cmbCodeGerant.Name = "cmbCodeGerant";
            this.cmbCodeGerant.ReadOnly = false;
            this.cmbCodeGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.cmbCodeGerant.Size = new System.Drawing.Size(676, 27);
            this.cmbCodeGerant.TabIndex = 503;
            this.cmbCodeGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.cmbCodeGerant.UseSystemPasswordChar = false;
            // 
            // ChkResiliee
            // 
            this.ChkResiliee.AutoSize = true;
            this.ChkResiliee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ChkResiliee.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ChkResiliee.Location = new System.Drawing.Point(833, 3);
            this.ChkResiliee.Name = "ChkResiliee";
            this.ChkResiliee.Size = new System.Drawing.Size(1014, 24);
            this.ChkResiliee.TabIndex = 570;
            this.ChkResiliee.Text = "Date de fin du";
            this.ChkResiliee.UseVisualStyleBackColor = true;
            // 
            // chkNonFacturable
            // 
            this.chkNonFacturable.AutoSize = true;
            this.chkNonFacturable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkNonFacturable.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkNonFacturable.Location = new System.Drawing.Point(833, 33);
            this.chkNonFacturable.Name = "chkNonFacturable";
            this.chkNonFacturable.Size = new System.Drawing.Size(1014, 26);
            this.chkNonFacturable.TabIndex = 571;
            this.chkNonFacturable.Text = "Contrat non facturable";
            this.chkNonFacturable.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.button1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.button1.Location = new System.Drawing.Point(803, 33);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(24, 20);
            this.button1.TabIndex = 506;
            this.toolTip1.SetToolTip(this.button1, "Recherche du gérant");
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.cmdGerant_Click);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtDateFin, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtDateEffet, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblTotal, 3, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 107);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1850, 85);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 28);
            this.label1.TabIndex = 385;
            this.label1.Text = "Date effet du";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 28);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 28);
            this.label3.TabIndex = 386;
            this.label3.Text = "Date de fin du";
            // 
            // txtDateFin
            // 
            this.txtDateFin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateFin.Location = new System.Drawing.Point(123, 31);
            this.txtDateFin.MaskInput = "{LOC}yyyy/mm/dd";
            this.txtDateFin.Name = "txtDateFin";
            this.txtDateFin.Size = new System.Drawing.Size(674, 21);
            this.txtDateFin.TabIndex = 392;
            this.txtDateFin.Value = null;
            // 
            // txtDateEffet
            // 
            this.txtDateEffet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateEffet.Location = new System.Drawing.Point(123, 3);
            this.txtDateEffet.MaskInput = "{LOC}yyyy/mm/dd";
            this.txtDateEffet.Name = "txtDateEffet";
            this.txtDateEffet.Size = new System.Drawing.Size(674, 21);
            this.txtDateEffet.TabIndex = 395;
            this.txtDateEffet.Value = null;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblTotal.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(1847, 56);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(0, 30);
            this.lblTotal.TabIndex = 389;
            this.lblTotal.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.ssGridHistoCtr, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 192);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1850, 765);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // ssGridHistoCtr
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridHistoCtr.DisplayLayout.Appearance = appearance1;
            this.ssGridHistoCtr.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridHistoCtr.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHistoCtr.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHistoCtr.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridHistoCtr.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHistoCtr.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridHistoCtr.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridHistoCtr.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridHistoCtr.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridHistoCtr.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridHistoCtr.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridHistoCtr.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHistoCtr.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHistoCtr.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridHistoCtr.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridHistoCtr.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridHistoCtr.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridHistoCtr.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridHistoCtr.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHistoCtr.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridHistoCtr.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridHistoCtr.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridHistoCtr.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridHistoCtr.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridHistoCtr.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridHistoCtr.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridHistoCtr.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridHistoCtr.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridHistoCtr.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridHistoCtr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridHistoCtr.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridHistoCtr.Location = new System.Drawing.Point(3, 3);
            this.ssGridHistoCtr.Name = "ssGridHistoCtr";
            this.ssGridHistoCtr.Size = new System.Drawing.Size(1844, 759);
            this.ssGridHistoCtr.TabIndex = 412;
            this.ssGridHistoCtr.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridHistoCtr_InitializeLayout);
            this.ssGridHistoCtr.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridHistoCtr_BeforeRowsDeleted);
            this.ssGridHistoCtr.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssGridHistoCtr_DoubleClickRow);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Controls.Add(this.cmdRefresh);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1671, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(179, 45);
            this.flowLayoutPanel1.TabIndex = 386;
            // 
            // cmdClean
            // 
            this.cmdClean.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClean.Location = new System.Drawing.Point(93, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(84, 40);
            this.cmdClean.TabIndex = 404;
            this.cmdClean.Text = "   Clean";
            this.toolTip1.SetToolTip(this.cmdClean, "Annuler");
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdRefresh
            // 
            this.cmdRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmdRefresh.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRefresh.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdRefresh.FlatAppearance.BorderSize = 0;
            this.cmdRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRefresh.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRefresh.ForeColor = System.Drawing.Color.White;
            this.cmdRefresh.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.cmdRefresh.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRefresh.Location = new System.Drawing.Point(5, 2);
            this.cmdRefresh.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRefresh.Name = "cmdRefresh";
            this.cmdRefresh.Size = new System.Drawing.Size(84, 40);
            this.cmdRefresh.TabIndex = 403;
            this.cmdRefresh.Text = "     Chercher";
            this.toolTip1.SetToolTip(this.cmdRefresh, "Rechercher");
            this.cmdRefresh.UseVisualStyleBackColor = false;
            this.cmdRefresh.Click += new System.EventHandler(this.cmdRefresh_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel4.TabIndex = 387;
            // 
            // UserDocHistoContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel4);
            this.Name = "UserDocHistoContrat";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "HISTORIQUE CONTRAT";
            this.VisibleChanged += new System.EventHandler(this.UserDocHistoContrat_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateEffet)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHistoCtr)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 cmbCodeimmeuble;
        public iTalk.iTalk_TextBox_Small2 cmbCodeGerant;
        public System.Windows.Forms.Button cmdRechercheGroupe;
        public System.Windows.Forms.Button button1;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridHistoCtr;
        public System.Windows.Forms.CheckBox chkNonFacturable;
        public System.Windows.Forms.CheckBox ChkResiliee;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button cmdRefresh;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDateFin;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDateEffet;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
    }
}
