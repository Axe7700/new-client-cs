namespace Axe_interDT.Views.Contrat.FicheContrat.ListeDesContrats
{
    partial class UserListeContrat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton3 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton4 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.txtFacture1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtFacture2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.PB1 = new Infragistics.Win.UltraWinProgressBar.UltraProgressBar();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.lblGofichecontrat = new iTalk.iTalk_LinkLabel();
            this.lblGoFacturation = new iTalk.iTalk_LinkLabel();
            this.txtSqlGrille = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lblTotAf = new iTalk.iTalk_TextBox_Small2();
            this.lblTotF = new iTalk.iTalk_TextBox_Small2();
            this.lblResiliee = new iTalk.iTalk_TextBox_Small2();
            this.lblNonFacturable = new iTalk.iTalk_TextBox_Small2();
            this.lblTot = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGridListe = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacture1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacture2)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridListe)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 51);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 170);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.Frame4, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 137F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 72F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(916, 162);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.tableLayoutPanel4);
            this.Frame4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Frame4.Location = new System.Drawing.Point(3, 4);
            this.Frame4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Frame4.Name = "Frame4";
            this.Frame4.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Frame4.Size = new System.Drawing.Size(910, 129);
            this.Frame4.TabIndex = 411;
            this.Frame4.TabStop = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.PB1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 22);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 61F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(904, 103);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel5.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtFacture1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtFacture2, 1, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 4);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 40.625F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 59.375F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(898, 53);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(135, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Periode à facturer";
            // 
            // txtFacture1
            // 
            appearance16.BackColor = System.Drawing.Color.White;
            appearance16.BackColor2 = System.Drawing.Color.White;
            appearance16.BackColorDisabled = System.Drawing.Color.White;
            appearance16.BackColorDisabled2 = System.Drawing.Color.White;
            appearance16.BorderColor = System.Drawing.Color.White;
            appearance16.BorderColor2 = System.Drawing.Color.White;
            stateEditorButton3.Appearance = appearance16;
            this.txtFacture1.ButtonsLeft.Add(stateEditorButton3);
            this.txtFacture1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFacture1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFacture1.Location = new System.Drawing.Point(3, 24);
            this.txtFacture1.Name = "txtFacture1";
            this.txtFacture1.Nullable = false;
            this.txtFacture1.Size = new System.Drawing.Size(443, 26);
            this.txtFacture1.TabIndex = 3;
            this.txtFacture1.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtFacture1_BeforeDropDown);
            this.txtFacture1.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.txtFacture1_AfterEditorButtonCheckStateChanged);
            // 
            // txtFacture2
            // 
            appearance17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txtFacture2.Appearance = appearance17;
            this.txtFacture2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            appearance18.BackColor = System.Drawing.Color.White;
            appearance18.BackColor2 = System.Drawing.Color.White;
            appearance18.BackColorDisabled = System.Drawing.Color.White;
            appearance18.BackColorDisabled2 = System.Drawing.Color.White;
            appearance18.BorderColor = System.Drawing.Color.White;
            appearance18.BorderColor2 = System.Drawing.Color.White;
            appearance18.BorderColor3DBase = System.Drawing.Color.White;
            stateEditorButton4.Appearance = appearance18;
            this.txtFacture2.ButtonsLeft.Add(stateEditorButton4);
            this.txtFacture2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFacture2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFacture2.Location = new System.Drawing.Point(452, 24);
            this.txtFacture2.Name = "txtFacture2";
            this.txtFacture2.Nullable = false;
            this.txtFacture2.Size = new System.Drawing.Size(443, 26);
            this.txtFacture2.TabIndex = 3;
            this.txtFacture2.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtFacture2_BeforeDropDown);
            this.txtFacture2.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.txtFacture2_AfterEditorButtonCheckStateChanged);
            // 
            // PB1
            // 
            this.PB1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PB1.Location = new System.Drawing.Point(3, 65);
            this.PB1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.PB1.Name = "PB1";
            this.PB1.Size = new System.Drawing.Size(898, 34);
            this.PB1.Step = 1;
            this.PB1.TabIndex = 1;
            this.PB1.Text = "[Formatted]";
            this.PB1.UseFlatMode = Infragistics.Win.DefaultableBoolean.False;
            this.PB1.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.lblGofichecontrat);
            this.flowLayoutPanel1.Controls.Add(this.lblGoFacturation);
            this.flowLayoutPanel1.Controls.Add(this.txtSqlGrille);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 141);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(910, 64);
            this.flowLayoutPanel1.TabIndex = 412;
            // 
            // lblGofichecontrat
            // 
            this.lblGofichecontrat.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGofichecontrat.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblGofichecontrat.AutoSize = true;
            this.lblGofichecontrat.BackColor = System.Drawing.Color.Transparent;
            this.lblGofichecontrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblGofichecontrat.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGofichecontrat.LinkColor = System.Drawing.Color.Blue;
            this.lblGofichecontrat.Location = new System.Drawing.Point(3, 6);
            this.lblGofichecontrat.Name = "lblGofichecontrat";
            this.lblGofichecontrat.Size = new System.Drawing.Size(103, 19);
            this.lblGofichecontrat.TabIndex = 0;
            this.lblGofichecontrat.TabStop = true;
            this.lblGofichecontrat.Text = "Fiche Contrat";
            this.lblGofichecontrat.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGofichecontrat.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGofichecontrat_LinkClicked);
            // 
            // lblGoFacturation
            // 
            this.lblGoFacturation.ActiveLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoFacturation.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblGoFacturation.AutoSize = true;
            this.lblGoFacturation.BackColor = System.Drawing.Color.Transparent;
            this.lblGoFacturation.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblGoFacturation.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.lblGoFacturation.LinkColor = System.Drawing.Color.Blue;
            this.lblGoFacturation.Location = new System.Drawing.Point(112, 6);
            this.lblGoFacturation.Name = "lblGoFacturation";
            this.lblGoFacturation.Size = new System.Drawing.Size(88, 19);
            this.lblGoFacturation.TabIndex = 1;
            this.lblGoFacturation.TabStop = true;
            this.lblGoFacturation.Text = "Facturation";
            this.lblGoFacturation.VisitedLinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(101)))), ((int)(((byte)(202)))));
            this.lblGoFacturation.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lblGoFacturation_LinkClicked);
            // 
            // txtSqlGrille
            // 
            this.txtSqlGrille.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSqlGrille.Location = new System.Drawing.Point(206, 3);
            this.txtSqlGrille.Name = "txtSqlGrille";
            this.txtSqlGrille.Size = new System.Drawing.Size(198, 25);
            this.txtSqlGrille.TabIndex = 2;
            this.txtSqlGrille.Visible = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 187F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.lblTotAf, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.lblTotF, 1, 1);
            this.tableLayoutPanel6.Controls.Add(this.lblResiliee, 1, 2);
            this.tableLayoutPanel6.Controls.Add(this.lblNonFacturable, 1, 3);
            this.tableLayoutPanel6.Controls.Add(this.lblTot, 1, 4);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(925, 4);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 6;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(916, 162);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(3, 123);
            this.label6.Margin = new System.Windows.Forms.Padding(3);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(181, 24);
            this.label6.TabIndex = 400;
            this.label6.Text = " Total";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 24);
            this.label2.TabIndex = 396;
            this.label2.Text = "Total a Facturer";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(3, 33);
            this.label3.Margin = new System.Windows.Forms.Padding(3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(181, 24);
            this.label3.TabIndex = 397;
            this.label3.Text = "Total Facture";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(3, 63);
            this.label4.Margin = new System.Windows.Forms.Padding(3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(181, 24);
            this.label4.TabIndex = 398;
            this.label4.Text = "Resiliee";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(3, 93);
            this.label5.Margin = new System.Windows.Forms.Padding(3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(181, 24);
            this.label5.TabIndex = 399;
            this.label5.Text = "Non Facturable";
            // 
            // lblTotAf
            // 
            this.lblTotAf.AccAcceptNumbersOnly = false;
            this.lblTotAf.AccAllowComma = false;
            this.lblTotAf.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTotAf.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTotAf.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTotAf.AccHidenValue = "";
            this.lblTotAf.AccNotAllowedChars = null;
            this.lblTotAf.AccReadOnly = false;
            this.lblTotAf.AccReadOnlyAllowDelete = false;
            this.lblTotAf.AccRequired = false;
            this.lblTotAf.BackColor = System.Drawing.Color.White;
            this.lblTotAf.CustomBackColor = System.Drawing.Color.White;
            this.lblTotAf.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotAf.Enabled = false;
            this.lblTotAf.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblTotAf.ForeColor = System.Drawing.Color.Blue;
            this.lblTotAf.Location = new System.Drawing.Point(189, 2);
            this.lblTotAf.Margin = new System.Windows.Forms.Padding(2);
            this.lblTotAf.MaxLength = 32767;
            this.lblTotAf.Multiline = false;
            this.lblTotAf.Name = "lblTotAf";
            this.lblTotAf.ReadOnly = true;
            this.lblTotAf.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTotAf.Size = new System.Drawing.Size(725, 27);
            this.lblTotAf.TabIndex = 502;
            this.lblTotAf.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTotAf.UseSystemPasswordChar = false;
            // 
            // lblTotF
            // 
            this.lblTotF.AccAcceptNumbersOnly = false;
            this.lblTotF.AccAllowComma = false;
            this.lblTotF.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTotF.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTotF.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTotF.AccHidenValue = "";
            this.lblTotF.AccNotAllowedChars = null;
            this.lblTotF.AccReadOnly = false;
            this.lblTotF.AccReadOnlyAllowDelete = false;
            this.lblTotF.AccRequired = false;
            this.lblTotF.BackColor = System.Drawing.Color.White;
            this.lblTotF.CustomBackColor = System.Drawing.Color.White;
            this.lblTotF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotF.Enabled = false;
            this.lblTotF.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblTotF.ForeColor = System.Drawing.Color.Blue;
            this.lblTotF.Location = new System.Drawing.Point(189, 32);
            this.lblTotF.Margin = new System.Windows.Forms.Padding(2);
            this.lblTotF.MaxLength = 32767;
            this.lblTotF.Multiline = false;
            this.lblTotF.Name = "lblTotF";
            this.lblTotF.ReadOnly = true;
            this.lblTotF.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTotF.Size = new System.Drawing.Size(725, 27);
            this.lblTotF.TabIndex = 502;
            this.lblTotF.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTotF.UseSystemPasswordChar = false;
            // 
            // lblResiliee
            // 
            this.lblResiliee.AccAcceptNumbersOnly = false;
            this.lblResiliee.AccAllowComma = false;
            this.lblResiliee.AccBackgroundColor = System.Drawing.Color.White;
            this.lblResiliee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblResiliee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblResiliee.AccHidenValue = "";
            this.lblResiliee.AccNotAllowedChars = null;
            this.lblResiliee.AccReadOnly = false;
            this.lblResiliee.AccReadOnlyAllowDelete = false;
            this.lblResiliee.AccRequired = false;
            this.lblResiliee.BackColor = System.Drawing.Color.White;
            this.lblResiliee.CustomBackColor = System.Drawing.Color.White;
            this.lblResiliee.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblResiliee.Enabled = false;
            this.lblResiliee.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblResiliee.ForeColor = System.Drawing.Color.Blue;
            this.lblResiliee.Location = new System.Drawing.Point(189, 62);
            this.lblResiliee.Margin = new System.Windows.Forms.Padding(2);
            this.lblResiliee.MaxLength = 32767;
            this.lblResiliee.Multiline = false;
            this.lblResiliee.Name = "lblResiliee";
            this.lblResiliee.ReadOnly = true;
            this.lblResiliee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblResiliee.Size = new System.Drawing.Size(725, 27);
            this.lblResiliee.TabIndex = 502;
            this.lblResiliee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblResiliee.UseSystemPasswordChar = false;
            // 
            // lblNonFacturable
            // 
            this.lblNonFacturable.AccAcceptNumbersOnly = false;
            this.lblNonFacturable.AccAllowComma = false;
            this.lblNonFacturable.AccBackgroundColor = System.Drawing.Color.White;
            this.lblNonFacturable.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblNonFacturable.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblNonFacturable.AccHidenValue = "";
            this.lblNonFacturable.AccNotAllowedChars = null;
            this.lblNonFacturable.AccReadOnly = false;
            this.lblNonFacturable.AccReadOnlyAllowDelete = false;
            this.lblNonFacturable.AccRequired = false;
            this.lblNonFacturable.BackColor = System.Drawing.Color.White;
            this.lblNonFacturable.CustomBackColor = System.Drawing.Color.White;
            this.lblNonFacturable.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNonFacturable.Enabled = false;
            this.lblNonFacturable.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNonFacturable.ForeColor = System.Drawing.Color.Blue;
            this.lblNonFacturable.Location = new System.Drawing.Point(189, 92);
            this.lblNonFacturable.Margin = new System.Windows.Forms.Padding(2);
            this.lblNonFacturable.MaxLength = 32767;
            this.lblNonFacturable.Multiline = false;
            this.lblNonFacturable.Name = "lblNonFacturable";
            this.lblNonFacturable.ReadOnly = true;
            this.lblNonFacturable.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblNonFacturable.Size = new System.Drawing.Size(725, 27);
            this.lblNonFacturable.TabIndex = 502;
            this.lblNonFacturable.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblNonFacturable.UseSystemPasswordChar = false;
            // 
            // lblTot
            // 
            this.lblTot.AccAcceptNumbersOnly = false;
            this.lblTot.AccAllowComma = false;
            this.lblTot.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTot.AccHidenValue = "";
            this.lblTot.AccNotAllowedChars = null;
            this.lblTot.AccReadOnly = false;
            this.lblTot.AccReadOnlyAllowDelete = false;
            this.lblTot.AccRequired = false;
            this.lblTot.BackColor = System.Drawing.Color.White;
            this.lblTot.CustomBackColor = System.Drawing.Color.White;
            this.lblTot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTot.Enabled = false;
            this.lblTot.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblTot.ForeColor = System.Drawing.Color.Blue;
            this.lblTot.Location = new System.Drawing.Point(189, 122);
            this.lblTot.Margin = new System.Windows.Forms.Padding(2);
            this.lblTot.MaxLength = 32767;
            this.lblTot.Multiline = false;
            this.lblTot.Name = "lblTot";
            this.lblTot.ReadOnly = true;
            this.lblTot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTot.Size = new System.Drawing.Size(725, 27);
            this.lblTot.TabIndex = 502;
            this.lblTot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTot.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ssGridListe, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 178F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ssGridListe
            // 
            appearance4.BackColor = System.Drawing.SystemColors.Window;
            appearance4.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridListe.DisplayLayout.Appearance = appearance4;
            this.ssGridListe.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssGridListe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridListe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.True;
            appearance5.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance5.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance5.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance5.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridListe.DisplayLayout.GroupByBox.Appearance = appearance5;
            appearance6.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridListe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance6;
            this.ssGridListe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance7.BackColor2 = System.Drawing.SystemColors.Control;
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance7.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridListe.DisplayLayout.GroupByBox.PromptAppearance = appearance7;
            this.ssGridListe.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridListe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance8.BackColor = System.Drawing.SystemColors.Window;
            appearance8.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridListe.DisplayLayout.Override.ActiveCellAppearance = appearance8;
            appearance9.BackColor = System.Drawing.SystemColors.Highlight;
            appearance9.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridListe.DisplayLayout.Override.ActiveRowAppearance = appearance9;
            this.ssGridListe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridListe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance10.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridListe.DisplayLayout.Override.CardAreaAppearance = appearance10;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            appearance11.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridListe.DisplayLayout.Override.CellAppearance = appearance11;
            this.ssGridListe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridListe.DisplayLayout.Override.CellPadding = 0;
            appearance12.BackColor = System.Drawing.SystemColors.Control;
            appearance12.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance12.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance12.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance12.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridListe.DisplayLayout.Override.GroupByRowAppearance = appearance12;
            appearance13.TextHAlignAsString = "Left";
            this.ssGridListe.DisplayLayout.Override.HeaderAppearance = appearance13;
            this.ssGridListe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridListe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(170)))), ((int)(((byte)(220)))));
            appearance14.BorderColor = System.Drawing.Color.Silver;
            this.ssGridListe.DisplayLayout.Override.RowAppearance = appearance14;
            this.ssGridListe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridListe.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance15.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridListe.DisplayLayout.Override.TemplateAddRowAppearance = appearance15;
            this.ssGridListe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridListe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridListe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridListe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridListe.Location = new System.Drawing.Point(3, 228);
            this.ssGridListe.Name = "ssGridListe";
            this.ssGridListe.Size = new System.Drawing.Size(1844, 726);
            this.ssGridListe.TabIndex = 576;
            this.ssGridListe.Text = "Liste Contrats";
            this.ssGridListe.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridListe_InitializeLayout);
            this.ssGridListe.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridListe_InitializeRow);
            this.ssGridListe.AfterExitEditMode += new System.EventHandler(this.ssGridListe_AfterExitEditMode);
            this.ssGridListe.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssGridListe_DoubleClickRow);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.cmdMAJ);
            this.flowLayoutPanel3.Controls.Add(this.cmbIntervention);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(1844, 41);
            this.flowLayoutPanel3.TabIndex = 575;
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(1781, 2);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(61, 35);
            this.cmdMAJ.TabIndex = 570;
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmbIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbIntervention.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbIntervention.Location = new System.Drawing.Point(1716, 2);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(61, 35);
            this.cmbIntervention.TabIndex = 569;
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbIntervention, "Rechercher");
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // UserListeContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "UserListeContrat";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "UserListeContrat";
            this.VisibleChanged += new System.EventHandler(this.UserListeContrat_VisibleChanged);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacture1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFacture2)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel1.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridListe)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.GroupBox Frame4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.Label label1;
        private Infragistics.Win.UltraWinProgressBar.UltraProgressBar PB1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private iTalk.iTalk_LinkLabel lblGofichecontrat;
        private iTalk.iTalk_LinkLabel lblGoFacturation;
        private System.Windows.Forms.TextBox txtSqlGrille;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 lblTotAf;
        public iTalk.iTalk_TextBox_Small2 lblTotF;
        public iTalk.iTalk_TextBox_Small2 lblResiliee;
        public iTalk.iTalk_TextBox_Small2 lblNonFacturable;
        public iTalk.iTalk_TextBox_Small2 lblTot;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Button cmdMAJ;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridListe;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtFacture1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtFacture2;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
