﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;
using CrystalDecisions.CrystalReports.Engine;
using Axe_interDT.Views.SharedViews;
using Infragistics.Win.UltraWinEditors;

namespace Axe_interDT.Views.Contrat.FicheContrat.ListeDesContrats
{
    public partial class UserListeContrat : UserControl
    {
        string sSQL;
        int lAf;
        int lF;
        int lRes;
        int lNF;
        float dbBar;
        int iOldCol;
        bool bTriAsc;
        public UserListeContrat()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TODO faut verifier le proccessbar car qu'on a plusieur ligne il prend beaucoup de temps
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            if (!(txtFacture1.ButtonsLeft[0] as StateEditorButton).Checked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Sélectionnez une date dans le calendrier", "Opération annulée", MessageBoxButtons.OK);
                txtFacture1.Focus();
                return;
            }
            else if (!(txtFacture2.ButtonsLeft[0] as StateEditorButton).Checked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Sélectionnez une date dans le calendrier", "Opération annulée", MessageBoxButtons.OK);
                txtFacture2.Focus();
                return;
            }
            //try
            //{
            sSQL = "SELECT Contrat.NumContrat as [NumContrat], Contrat.Avenant as [Avenant],"
                + " Contrat.CodeImmeuble, Contrat.nonFacturable as [NonFacturable], FacCalendrier.Date," 
                + " Contrat.CodeArticle, FacCalendrier.NoFacture ,contrat.Resiliee as [Resiliee]"
                + " FROM FacCalendrier INNER JOIN Contrat ON" + " (FacCalendrier.Avenant = Contrat.Avenant) AND" 
                + " (FacCalendrier.NumContrat = Contrat.NumContrat)" + " WHERE FacCalendrier.Date >=" 
                + General.fc_Fdate(txtFacture1.DateTime.ToShortDateString(), 1) + " and FacCalendrier.Date <=" 
                + General.fc_Fdate(txtFacture2.DateTime.ToShortDateString(), 1);
            txtSqlGrille.Text = sSQL;
            sSQL = sSQL + " order by Contrat.NumContrat, Contrat.avenant";
            ssGridListe.Visible = false;
            lblTotAf.Text = "";
            lblTotF.Text = "";
            lblTot.Text = "";
            lblResiliee.Text = "";
            lblNonFacturable.Text = "";
            var adodc1 = new DataTable();
            var modAdo = new ModAdo();
            adodc1 = modAdo.fc_OpenRecordSet(sSQL);
            lF = lAf = lRes = lNF = 0;

            if (adodc1.Rows.Count > 0)
            {
                PB1.Visible = true;
                if (adodc1.Rows.Count != 0)
                {
                    dbBar = 100 / adodc1.Rows.Count;
                }
                PB1.Value = 0;
                int i = 1, first = adodc1.Rows.Count / 100, all = adodc1.Rows.Count / 100, current = 1;
                foreach (DataRow dr in adodc1.Rows)
                {
                    if (dr["NoFacture"] + "" != "")
                        lF += 1;
                    else if (Convert.ToBoolean(dr["NonFacturable"]) == true)
                        lNF += 1;
                    else if (Convert.ToBoolean(dr["Resiliee"]) == true)
                        lRes += 1;
                    else
                        lAf += 1;

                    if (i == all)
                    {
                        all += first;
                        current++;
                    }
                    if (current <= 100)
                    {
                        PB1.Value = current;
                        PB1.PerformStep();
                    }
                    i++;
                    //else
                    //{
                    //    if (float.Parse(PB1.Value + "") + dbBar < 1)
                    //        PB1.Value = 1;
                    //    else
                    //        PB1.Value += Convert.ToInt32(dbBar);
                    //}
                 
                }
                //  modAdo.Dispose();
                //  modAdo.Close();
            }
            ssGridListe.DataSource = adodc1;
            ssGridListe.Visible = true;
            PB1.Visible = false;
            lblTotAf.Text = "" + lAf;
            lblTotF.Text = "" + lF;
            lblTot.Text = "" + adodc1.Rows.Count;
            lblResiliee.Text = Convert.ToString(lRes);
            lblNonFacturable.Text = Convert.ToString(lNF);
            //adodc1 = null;
            //}
            //catch (Exception ex) { }

        }

        private void ssGridListe_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.ssGridListe.DisplayLayout.Bands[0].Header.Caption = "Liste Contrats";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["NumContrat"].Header.Caption = "Num Contrat";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["Avenant"].Header.Caption = "Avenant";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "Code Immeuble";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["nonFacturable"].Header.Caption = "Non Facturable";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["Date"].Header.Caption = "Date";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["CodeArticle"].Header.Caption = "Code Article";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["NoFacture"].Header.Caption = "No Facture";
            this.ssGridListe.DisplayLayout.Bands[0].Columns["Resiliee"].Header.Caption = "Resiliee";

            foreach (var clm in ssGridListe.DisplayLayout.Bands[0].Columns)
                if (clm.Key != "nonFacturable" && clm.Key != "Resiliee")
                {
                    clm.CellActivation = Activation.NoEdit;
                }
            this.ssGridListe.DisplayLayout.Bands[0].Columns["nonFacturable"].CellActivation = Activation.AllowEdit;
            this.ssGridListe.DisplayLayout.Bands[0].Columns["Resiliee"].CellActivation = Activation.AllowEdit;
            ssGridListe.DisplayLayout.Rows.ToList().ForEach(l => l.Cells["Resiliee"].Appearance.BackColor = l.Cells["nonFacturable"].Appearance.BackColor = l.IsAlternate ? Color.FromArgb(255, 255, 192) : Color.White);
        }

        private void ssGridListe_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            //sSQL = "UPDATE FacCalendrier SET FacCalendrier.nonFacturable = " + General.fc_bln(Convert.ToBoolean(e.Row.GetCellValue("nonFacturable"))) + " WHERE FacCalendrier.NumContrat='" + e.Row.GetCellValue("NumContrat") + "'" + "  AND FacCalendrier.Avenant='" + e.Row.GetCellValue("Avenant") + "'";
            //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL);
            ////General.Execute(sSQL);
            //sSQL = "UPDATE FacCalendrier SET FacCalendrier.RESILIEE = " + General.fc_bln(Convert.ToBoolean(e.Row.GetCellValue("Resiliee"))) + " WHERE FacCalendrier.NumContrat='" + e.Row.GetCellValue("NumContrat") + "'" + "  AND FacCalendrier.Avenant='" + e.Row.GetCellValue("Avenant") + "'";
            //General.adocnn.Execute(sSQL);
        }


        private void ssGridListe_InitializeRow(object sender, InitializeRowEventArgs e)
        {
           
           /* if (e.Row.Cells["nonFacturable"].Value.ToString() != "")
                e.Row.Cells["nonFacturable"].Value = false;
            if (e.Row.Cells["Resiliee"].Value.ToString() != "")
                e.Row.Cells["Resiliee"].Value = false;*/

            if (e.Row.GetCellValue("NoFacture") + "" == "")
            {
                //foreach (var cell in e.Row.Cells)
                //    cell.Appearance.BackColor = Color.Aquamarine;
                e.Row.Appearance.BackColor = Color.Silver;
            }
        }

        private void ssGridListe_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            try
            {
                ReportDocument CrystalReport1 = new ReportDocument();
                CrystalReport1.Load(General.CHEMINEUROONLY);

                if (e.Row.GetCellValue("nofacture") + "" != "")
                {
                    string sWhere = "{FactEnTete.Nofacture}='" + e.Row.GetCellValue("nofacture") + "'";
                    CrystalReportFormView crfv = new CrystalReportFormView(CrystalReport1, sWhere);
                    crfv.Show();
                    //CrystalReport1.Action = 1;
                    //Debug.Print(CrystalReport1.Status);
                    //CrystalReport1.SelectionFormula = "";
                    //CrystalReport1.ReportFileName = "";
                }
            }
            catch (Exception ex)
            {
                //err.Clear();
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }

        //TODO.update probleme==> 2 table has the same column .the column updated on second table but the first still like before.
        private void ssGridListe_AfterExitEditMode(object sender, EventArgs e)
        {
            
                sSQL = "UPDATE FacCalendrier SET FacCalendrier.nonFacturable = "
                    + General.fc_bln(Convert.ToBoolean(ssGridListe.ActiveRow.GetCellValue("nonFacturable"))) 
                    + " WHERE FacCalendrier.NumContrat='" + ssGridListe.ActiveRow.GetCellValue("NumContrat") + "'" 
                    + "  AND FacCalendrier.Avenant='" + ssGridListe.ActiveRow.GetCellValue("Avenant") + "'";
                General.Execute(sSQL);
            
            
                sSQL = "UPDATE FacCalendrier SET FacCalendrier.RESILIEE = "
                    + General.fc_bln(Convert.ToBoolean(ssGridListe.ActiveRow.GetCellValue("Resiliee"))) 
                    + " WHERE FacCalendrier.NumContrat='" + ssGridListe.ActiveRow.GetCellValue("NumContrat") + "'" 
                    + "  AND FacCalendrier.Avenant='" + ssGridListe.ActiveRow.GetCellValue("Avenant") + "'";
                General.Execute(sSQL);
            
        }

        private void UserListeContrat_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserListeContrat");
                General.open_conn();
          
            //---rouge
            //ssGridListe.StyleSets["Facture"].BackColor = 0xffc0ff;
            //ssGridListe.StyleSets["Facture"].ForeColor = 0x0;
        }

        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            ssGridListe.UpdateData();
        }

        private void lblGofichecontrat_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
        }

        private void lblGoFacturation_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //TODO must devloped
            //View.Theme.Theme.Navigate(typeof(UserImprimeFacture));//
        }

        private void txtFacture2_AfterEditorButtonCheckStateChanged(object sender, Infragistics.Win.UltraWinEditors.EditorButtonEventArgs e)
        {
            var btn = sender as UltraDateTimeEditor;
            var dtValue = btn.Value;
            if (((btn.ButtonsLeft[0]) as StateEditorButton).Checked == false)
            {
                btn.ReadOnly = true;
                btn.Value = dtValue;
                btn.Parent.BackColor = Color.LightGray;
                return;
            }
            btn.ReadOnly = false;
            //StateEditorButton b = txtFacture2.ButtonsLeft[0] as StateEditorButton;
            //if (b.Checked)
            //    txtFacture2.Appearance.ForeColor = Color.Gray;
            //else
            //    txtFacture2.Appearance.ForeColor = Color.Black;
        }

        private void txtFacture1_AfterEditorButtonCheckStateChanged(object sender, EditorButtonEventArgs e)
        {
            var btn = sender as UltraDateTimeEditor;
            var dtValue = btn.Value;
            if (((btn.ButtonsLeft[0]) as StateEditorButton).Checked == false)
            {
                btn.ReadOnly = true;
                btn.Value = dtValue;
                btn.Parent.BackColor = Color.LightGray;
                return;
            }
            btn.ReadOnly = false;
            //StateEditorButton b = txtFacture1.ButtonsLeft[0] as StateEditorButton;
            //if (b.Checked)
            //    txtFacture1.Appearance.ForeColor = Color.Gray;
            //else
            //    txtFacture1.Appearance.ForeColor = Color.Black;
        }

        private void txtFacture2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            (txtFacture2.ButtonsLeft[0] as StateEditorButton).Checked = true;
        }

        private void txtFacture1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            (txtFacture1.ButtonsLeft[0] as StateEditorButton).Checked = true;
        }
    }
}
