﻿using Axe_interDT.Shared;
using Axe_interDT.Shared.FilterOfGrids;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Contrat.FicheContrat.Historique;
using Axe_interDT.Views.Contrat.FicheContrat.ListeDesContrats;
using Axe_interDT.Views.Facturation;
using Axe_interDT.Views.Parametrages;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat
{
    public partial class UserDocFicheContrat : UserControl
    {
        DataTable adors;
        SqlDataAdapter SDAadors;
        SqlCommandBuilder SCBadors;
        DataTable adocontrat;
        SqlDataAdapter SDAadocontrat;
        SqlCommandBuilder SCBadocontrat;
        DataTable adoCalendrier;
        SqlDataAdapter SDAadoCalendrier;
        SqlCommandBuilder SCBadoCalendrier;
        DataTable rs;
        SqlDataAdapter SDArs;
        SqlCommandBuilder SCBrs;


        string sSQL;
        string[] Tabe;
        string strCodeImmeuble;
        string SQL;
        string temporaire;
        string strFormule;
        string strNumContrat;
        string strTEMP;

        string strRetour;

        bool boolimmeuble;
        bool boolIntervention;

        System.DateTime[] dtTabeTemp;
        System.DateTime datVerif;
        System.DateTime dtTemp;

        int indi2;
        int indi1;
        int indi3;

        DataTable Adodc1;
        SqlDataAdapter SDAAdodc1;
        SqlCommandBuilder SCBAdodc1;
        DataTable Adodc2;
        SqlDataAdapter SDAAdodc2;
        SqlCommandBuilder SCBAdodc2;
        DataTable Adodc3;
        SqlDataAdapter SDAAdodc3;
        SqlCommandBuilder SCBAdodc3;
        DataTable Adodc4;
        SqlDataAdapter SDAAdodc4;
        SqlCommandBuilder SCBAdodc4;
        DataTable Adodc5;

        System.Windows.Forms.Control ControleSurFeuille;

        bool blAutorise;
        bool blAddnew;
        bool blSupp;
        bool boolTableContratVide;
        bool boolOk;
        bool boolVerifContratAv;
        bool boolValidate;
        bool boolFocVerifCtrAv;
        bool boolAutoriseMiseAJour;
        bool boolNonFacturable;
        bool boolResilie;
        public UserDocFicheContrat()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check1_MouseUp(object sender, MouseEventArgs e)
        {

            if (Check2.CheckState == CheckState.Checked)
            {
                Check1.CheckState = System.Windows.Forms.CheckState.Checked;
                Check2.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }
            if (Check1.CheckState == CheckState.Unchecked && Check2.CheckState == CheckState.Unchecked)
            {
                Check1.CheckState = System.Windows.Forms.CheckState.Checked;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check2_MouseUp(object sender, MouseEventArgs e)
        {
            if (Check1.CheckState == CheckState.Checked)
            {
                Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Check2.CheckState = System.Windows.Forms.CheckState.Checked;
            }
            if (Check1.CheckState == CheckState.Unchecked && Check2.CheckState == CheckState.Unchecked)
            {
                Check2.CheckState = System.Windows.Forms.CheckState.Checked;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkResiliee_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkResiliee.CheckState == CheckState.Checked)
            {
                Check3.CheckState = System.Windows.Forms.CheckState.Checked;
                Check3.Enabled = false;
                chkFactureManuelle.Enabled = false;
            }
            else
            {
                //===> Mondir le 06.01.2021, pour #2156
                //Check3.Enabled = true;
                //chkFactureManuelle.Enabled = true;
                //Fin Modif Mondir
            }

        }

        /// <summary>
        /// Tested - Control Is in A hidden Container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ckkCON_AnaModif_CheckedChanged(object sender, EventArgs e)
        {
            fc_AnaModif(Convert.ToString(ckkCON_AnaModif.CheckState));
            txtCON_AnaModif.Text = ckkCON_AnaModif.CheckState == CheckState.Checked ? "1" : "0";
            adocontrat.Rows[0]["CON_AnaModif"] = txtCON_AnaModif.Text;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sValue"></param>
        private void fc_AnaModif(string sValue)
        {
            switch (sValue)
            {

                case " ":
                case "0":
                    // bloqué
                    fc_EtatTextBox(txtAnalytique, 1);
                    fc_EtatTextBox(txtActivite, 1);
                    if (ckkCON_AnaModif.CheckState == CheckState.Checked)
                        ckkCON_AnaModif.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    break;
                case "1":
                    // non bloqué
                    fc_EtatTextBox(txtAnalytique);
                    fc_EtatTextBox(txtActivite);
                    if (ckkCON_AnaModif.CheckState == CheckState.Unchecked)
                        ckkCON_AnaModif.CheckState = System.Windows.Forms.CheckState.Checked;
                    break;
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="tText"></param>
        /// <param name="lBloque"></param>
        private void fc_EtatTextBox(iTalk.iTalk_TextBox_Small2 tText, int lBloque = 0)
        {
            if (lBloque == 0)
            {
                //non bloqué
                tText.ReadOnly = false;
                tText.BackColor = System.Drawing.ColorTranslator.FromOle(0xffffff);
                tText.ForeColor = Color.DimGray;
                //tText.Appearance = 1; le vérifier
            }
            else if (lBloque == 1)
            {
                //  bloqué
                tText.ReadOnly = true;
                tText.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                tText.ForeColor = System.Drawing.ColorTranslator.FromOle(0xc00000);
                // tText.Appearance = 0; le vérifier
            }

        }

        /// <summary>
        /// Tested - Controle Is In A Hidden Container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdActivite_Click(object sender, EventArgs e)
        {
            string req;
            string where = "";
            req = "SELECT ACT_Code, ACT_Libelle" + " FROM  ACT_AnaActivite order by ACT_Code";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche du contrat" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtActivite.Text = fg.ugResultat.ActiveRow.Cells["ACT_Code"].Value.ToString();
                fg.Dispose(); fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtActivite.Text = fg.ugResultat.ActiveRow.Cells["ACT_Code"].Value.ToString();
                    fg.Dispose(); fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested - Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjoutAvenant_Click(object sender, EventArgs e)
        {
            string strSql = null;
            string NumContrat = null;
            int NoAvenant = 0;
            string ContratCodeImmeuble = null;
            string titreContrat = null;
            string strCodeFormule = null;
            short intReel = 0;
            short intConnu = 0;
            string strIndice1 = null;
            string strIndice2 = null;
            string strIndice3 = null;
            double dblValeur1 = 0;
            double dblValeur2 = 0;
            double dblValeur3 = 0;
            System.DateTime dteDate1 = default(System.DateTime);
            System.DateTime dteDate2 = default(System.DateTime);
            System.DateTime dteDate3 = default(System.DateTime);
            double dblConCoef = 0;
            string strConTypeContrat = null;


            titreContrat = txtLibelleCont1.Text;
            ContratCodeImmeuble = txtCodeImmeuble.Text;
            NumContrat = txtNumContrat.Text;
            strSql = "SELECT MAX(Contrat.Avenant) as [MaxAvenant] FROM Contrat WHERE Contrat.NumContrat='" + txtNumContrat.Text + "'";
            try
            {
                if ((General.rstmp == null))
                {
                    if (General.adocnn.State == ConnectionState.Open)
                    {
                        General.rstmp.Dispose();
                    }
                }
                else
                {
                    General.rstmp = new DataTable();
                }
                var tmpAdo = new ModAdo();
                General.rstmp = tmpAdo.fc_OpenRecordSet(strSql);

                if (General.rstmp.Rows.Count > 0)
                {
                    //  General.rstmp.MoveFirst();
                    foreach (DataRow rstmpRows in General.rstmp.Rows)
                    {
                        NoAvenant = Convert.ToInt16(General.nz(rstmpRows["MaxAvenant"], "0")) + 1;
                    }
                }

                General.rstmp.Dispose();
                General.rstmp = null;



                if (adocontrat.Rows.Count > 0)
                {
                    //adocontrat.Update();
                    SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                    SDAadocontrat.Update(adocontrat);
                }


                adocontrat = new DataTable();
                SDAadocontrat = new SqlDataAdapter("", General.adocnn);
                SDAadocontrat.Fill(adocontrat);

                Adodc1 = adocontrat;
                SDAAdodc1 = SDAadocontrat;


                adocontrat.Rows.Add(adocontrat.NewRow());
                txtAvenant.Text = Convert.ToString(NoAvenant);
                txtNumContrat.Text = NumContrat;
                txtLibelleCont1.Text = titreContrat + " / Avenant : " + NoAvenant;
                txtCodeImmeuble.Text = ContratCodeImmeuble;
                RechercheParImmeuble(ContratCodeImmeuble);
                Check3.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkFactureManuelle.CheckState = System.Windows.Forms.CheckState.Unchecked;
                ChkResiliee.CheckState = System.Windows.Forms.CheckState.Unchecked;
                txtFormule.Text = "";
                Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Check2.CheckState = System.Windows.Forms.CheckState.Checked;
                chkCalcEcheance.CheckState = System.Windows.Forms.CheckState.Unchecked;
                optRevFormule.Checked = true;
                fc_TypeRevsion(Convert.ToString(2));

                SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                SDAadocontrat.Update(adocontrat);
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("" + ex, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            try
            {
                SablierOnOff(true);

                //si lutilisateur a cliqué sur
                if (boolValidate == true)
                {
                    //l'onglet Calendrier et a donc sans le savoir valider la fiche
                    //contrat, cette procedure effacera cette fiche
                    var xx = General.Execute("Delete From contrat Where Contrat.NumContrat ='" + txtNumContrat.Text + "' and Contrat.Avenant ='" + txtAvenant.Text + "'");
                    xx = General.Execute("Delete From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' and FacCalendrier.Avenant ='" + txtAvenant.Text + "'");
                    boolValidate = false;

                    // var _with2 = Adodc1;
                    //  _with2.RecordSource = "SELECT TOP 1 Contrat.* From Contrat order by NumContrat";
                    // _with2.Refresh();

                    Adodc1 = new DataTable();
                    SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat order by NumContrat", General.adocnn);
                    SDAAdodc1.Fill(Adodc1);

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;

                    if (adocontrat.Rows.Count == 0)
                        TableContratVide(this);
                    else
                        RecupererValeurs(adocontrat);
                    //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                    fc_LoadSuivi();

                }
                else
                {
                    // adocontrat.CancelUpdate();                    
                    UpdateContratDataTable();
                    SCBAdodc1 = new SqlCommandBuilder(SDAAdodc1);
                    int xx = SDAAdodc1.Update(adocontrat);

                    if (adocontrat.Rows.Count > 0)
                    {

                        //AnnuleModif();pas le mm resultat que le vb c'est pour j'ai commenter cette ligne car cette fct permet de vider tt les champs .

                        // adocontrat.Bookmark = adocontrat.Bookmark;
                        //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                        fc_LoadSuivi();

                        //With Adodc1
                        //.Recordset.AbsolutePosition = adoContrat.AbsolutePosition
                        //.Refresh
                        // Set adoContrat = Adodc1.Recordset
                        //adoContrat.Bookmark = adoContrat.Bookmark
                        // End With
                    }
                    else
                    {
                        // var _with3 = Adodc1;
                        // _with3.RecordSource = "SELECT TOP 1 Contrat.* From Contrat order by NumContrat";
                        //  _with3.Refresh();
                        // adocontrat = _with3.Recordset;
                        Adodc1 = new DataTable();
                        SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat order by NumContrat", General.adocnn);
                        SDAAdodc1.Fill(Adodc1);

                        adocontrat = Adodc1;

                        if (adocontrat.Rows.Count == 0)
                            TableContratVide(this);

                        //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                        fc_LoadSuivi();
                    }
                }

                blAddnew = false;
                //l'utilisateur annule l'addnew
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }

                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }

                SynchroCalendrier();

                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }

                //SynchroCodeFormule (txtTypeFormule)
                cmdRechercheImmeuble.Visible = false;
                txtCodeImmeuble.ReadOnly = true;
                txtCodeImmeuble.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                txtNumContrat.ReadOnly = true;
                txtNumContrat.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                txtAvenant.ReadOnly = true;
                txtAvenant.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);


                txtCodeImmeuble.ReadOnly = true;
                SSOleDBGrid1.Enabled = true;
                txtNumContrat.ReadOnly = true;
                txtAvenant.ReadOnly = true;
                if (boolTableContratVide == false)
                    cmdSupprimer.Enabled = true;
                boolVerifContratAv = false;
                //voir addnew
                //Controle pouvant être ou pas utiliséé
                Frame1.Enabled = true;
                lblSyndic.Enabled = true;
                lblImmeuble.Enabled = true;
                lblContrat.Enabled = true;
                cmdAjouter.Enabled = true;
                cmdSyndic.Enabled = true;
                cmdImmeuble.Enabled = true;
                cmdContrat.Enabled = true;
                boolOk = false;
                // If txtCodeImmeuble <> "" Then
                //   RechercheParImmeuble (txtCodeImmeuble)
                //  Else
                //  InitialiseImmeubleGerant
                //End If
                //If txtTypeFormule <> "" Then RechercheFormule (txtTypeFormule)
                SablierOnOff(false);
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("" + ex.Message, " ", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCodeArticle_Click(object sender, EventArgs e)
        {
            string requete = "SELECT " + "FacArticle.CodeArticle as \"Code Article\" , FacArticle.Designation1 as \"Designation1\", FacArticle.Designation2 as \"Designation2\" FROM FacArticle ";
            string where = " ( CodeCategorieArticle='P')  ";

            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche de la prestation" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtCodeArticle.Text = fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString();
                adocontrat.Rows[0]["CodeArticle"] = txtCodeArticle.Text;
                RechercheParCodeArticle(fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString());
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtCodeArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    adocontrat.Rows[0]["CodeArticle"] = txtCodeArticle.Text;
                    RechercheParCodeArticle(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdContrat_Click(object sender, EventArgs e)
        {

            string requete = "SELECT " + "NumContrat as \"N°Contrat:\", Avenant as \"N°Avenant:\", LibelleCont1 as \"Designation1\", LibelleCont2 as \"Designation2\" FROM Contrat ";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche du contrat" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>

            {
                ClearAllControls(this);
                SablierOnOff(true);
                if (fg.ugResultat.ActiveRow == null) return;
                string req = "SELECT Contrat.* From Contrat Where Contrat.NumContrat ='"
                + fg.ugResultat.ActiveRow.Cells[1].Value
                + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[2].Value + "'";
                Adodc1 = new DataTable();
                SDAAdodc1 = new SqlDataAdapter(req, General.adocnn);
                SDAAdodc1.Fill(Adodc1);
                SDAadocontrat = SDAAdodc1;
                RecupererValeurs(Adodc1);
                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();

                adocontrat = Adodc1;
                RecupererValeurs(adocontrat);
                //=== modif du 24 07 2012, reactive les champs.
                if (adocontrat.Rows.Count > 0)
                {
                    TableContratActive(this);
                }

                SSTab1.SelectedTab = SSTab1.Tabs[0];

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }
                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }
                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }

                SynchroCalendrier();
                SablierOnOff(false);
                if (ChkResiliee.CheckState == CheckState.Checked)
                {
                    Check3.CheckState = System.Windows.Forms.CheckState.Checked;
                    Check3.Enabled = false;
                    chkFactureManuelle.Enabled = false;
                }
                else
                {
                    //===> Mondir le 06.01.2021, pour #2156
                    //Check3.Enabled = true;
                    //chkFactureManuelle.Enabled = true;
                    //===> Fin Modir Mondir
                }

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);
                fg.Dispose(); fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>

            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    ClearAllControls(this);
                    SablierOnOff(true);
                    if (fg.ugResultat.ActiveRow == null) return;
                    string req = "SELECT Contrat.* From Contrat Where Contrat.NumContrat ='"
                    + fg.ugResultat.ActiveRow.Cells[1].Value
                    + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[2].Value + "'";
                    Adodc1 = new DataTable();
                    SDAAdodc1 = new SqlDataAdapter(req, General.adocnn);
                    SDAAdodc1.Fill(Adodc1);
                    RecupererValeurs(Adodc1);
                    //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                    fc_LoadSuivi();

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;

                    RecupererValeurs(adocontrat);
                    //=== modif du 24 07 2012, reactive les champs.
                    if (adocontrat.Rows.Count > 0)
                    {
                        TableContratActive(this);
                    }

                    SSTab1.SelectedTab = SSTab1.Tabs[0];

                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        RechercheParImmeuble(txtCodeImmeuble.Text);
                    }
                    else
                    {
                        InitialiseImmeubleGerant();
                    }
                    if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                    {
                        RechercheParCodeArticle(txtCodeArticle.Text);
                    }
                    else
                    {
                        InitialiseArticle();
                    }
                    if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                    {
                        RechercheFormule(txtTypeFormule.Text);
                    }
                    else
                    {
                        txtFormule.Text = "";
                    }

                    SynchroCalendrier();
                    SablierOnOff(false);
                    if (ChkResiliee.CheckState == CheckState.Checked)
                    {
                        Check3.CheckState = System.Windows.Forms.CheckState.Checked;
                        Check3.Enabled = false;
                        chkFactureManuelle.Enabled = false;
                    }
                    else
                    {
                        //===> Mondir le 06.01.2021, pour #2156
                        //Check3.Enabled = true;
                        //chkFactureManuelle.Enabled = true;
                        //===> Fin Modif Mondir
                    }

                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);
                    fg.Dispose(); fg.Close();
                }
            };
            blSupp = true;
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDernier_Click(object sender, EventArgs e)
        {
            string strNoAvenant = null;
            string strNoContrat = null;
            using (var tmpAdo = new ModAdo())
            {
                strNoContrat = General.nz(tmpAdo.fc_ADOlibelle("SELECT TOP 1 Contrat.NumContrat FROM Contrat WHERE Contrat.CodeImmeuble='" + txtCodeImmeuble.Text + "' ORDER BY NumContrat DESC"), txtNumContrat.Text).ToString();
                strNoAvenant = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.Avenant FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' ORDER BY NumContrat DESC, Avenant DESC"), "0").ToString();
                fc_ChargeContrat(strNoContrat, strNoAvenant);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImmeuble_Click(object sender, EventArgs e)
        {

            string requete = "SELECT " + "Contrat.CodeImmeuble as \"CodeImmeuble:\", Immeuble.Adresse as \"Adresse:\", Contrat.NumContrat as \"N°Contrat\", Contrat.Avenant as \"Avenant\" FROM Contrat INNER JOIN Immeuble ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche de l'immeuble" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                ClearAllControls(this);
                SablierOnOff(true);
                if (fg.ugResultat.ActiveRow == null) return;
                string req = "SELECT Contrat.* From Contrat Where Contrat.CodeImmeuble ='" + fg.ugResultat.ActiveRow.Cells[1].Value + "' and Contrat.NumContrat='" + fg.ugResultat.ActiveRow.Cells[3].Value
                + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[4].Value + "'";
                Adodc1 = new DataTable();
                SDAAdodc1 = new SqlDataAdapter(req, General.adocnn);
                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();
                SDAAdodc1.Fill(Adodc1);
                RecupererValeurs(Adodc1);
                adocontrat = Adodc1;

                SDAadocontrat = SDAAdodc1;
                RecupererValeurs(adocontrat);
                //=== modif du 24 07 2012, reactive les champs.
                if (adocontrat.Rows.Count > 0)
                {
                    TableContratActive(this);
                }

                SSTab1.SelectedTab = SSTab1.Tabs[0];

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }

                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }

                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }

                SynchroCalendrier();
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);
                SablierOnOff(false);
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    ClearAllControls(this);
                    SablierOnOff(true);
                    if (fg.ugResultat.ActiveRow == null) return;
                    string req = "SELECT Contrat.* From Contrat Where Contrat.CodeImmeuble ='" + fg.ugResultat.ActiveRow.Cells[1].Value + "' and Contrat.NumContrat='" + fg.ugResultat.ActiveRow.Cells[3].Value + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[4].Value + "'";

                    Adodc1 = new DataTable();
                    SDAAdodc1 = new SqlDataAdapter(req, General.adocnn);
                    //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                    fc_LoadSuivi();
                    SDAAdodc1.Fill(Adodc1);
                    RecupererValeurs(Adodc1);
                    adocontrat = Adodc1;
                    RecupererValeurs(adocontrat);
                    //=== modif du 24 07 2012, reactive les champs.
                    if (adocontrat.Rows.Count > 0)
                    {
                        TableContratActive(this);
                    }

                    SSTab1.SelectedTab = SSTab1.Tabs[0];

                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        RechercheParImmeuble(txtCodeImmeuble.Text);
                    }
                    else
                    {
                        InitialiseImmeubleGerant();
                    }

                    if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                    {
                        RechercheParCodeArticle(txtCodeArticle.Text);
                    }
                    else
                    {
                        InitialiseArticle();
                    }

                    if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                    {
                        RechercheFormule(txtTypeFormule.Text);
                    }
                    else
                    {
                        txtFormule.Text = "";
                    }

                    SynchroCalendrier();
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);
                    SablierOnOff(false);
                    fg.Dispose();
                    fg.Close();
                }
            };
            blSupp = true;
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            //    ImprimeFacture.Show
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice1_Click(object sender, EventArgs e)
        {

            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice1.Text, 1, (txtIndice1.Text.Length) - 1) } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice1Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice1Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice1Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice1Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice2_Click(object sender, EventArgs e)
        {
            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice2.Text, 1, (txtIndice2.Text.Length) - 1).ToString() } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice2Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice2Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice2Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice2Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice3_Click(object sender, EventArgs e)
        {
            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice3.Text, 1, (txtIndice3.Text.Length) - 1).ToString() } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice3Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice3Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice3Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice3Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice4_Click(object sender, EventArgs e)
        {
            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice4.Text, 1, (txtIndice4.Text.Length) - 1).ToString() } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice4Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice4Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice4Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice4Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice5_Click(object sender, EventArgs e)
        {
            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice5.Text, 1, (txtIndice5.Text.Length) - 1).ToString() } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice5Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice5Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice5Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice5Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIndice6_Click(object sender, EventArgs e)
        {

            string req = "SELECT " + "FacIndice.Libelle as \"Libelle\", FacIndice.Date as \"Date\", FacIndice.Valeur as \"Valeur\" FROM FacIndice";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de l'indice" };
            fg.SetValues(new Dictionary<string, string> { { "Libelle", General.Mid(txtIndice6.Text, 1, (txtIndice6.Text.Length) - 1).ToString() } });
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice6Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                txtIndice6Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice6Valeur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    txtIndice6Date.Text = fg.ugResultat.ActiveRow.Cells[2].Value != DBNull.Value ? Convert.ToDateTime(fg.ugResultat.ActiveRow.Cells[2].Value.ToString()).ToShortDateString() : "";
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {

            short i = 0;
            string temporaire = null;

            try
            {
                using (var tmpAdo = new ModAdo())
                {
                    SablierOnOff(true);
                    if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code immeuble est obligatoire", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodeImmeuble.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (boolVerifContratAv == true)
                    {
                        adors = new DataTable();

                        adors = tmpAdo.fc_OpenRecordSet("Select NumContrat, Avenant From Contrat where Avenant ='" + txtAvenant.Text + "' and NumContrat ='" + txtNumContrat.Text + "'");
                        var _with13 = adors;
                        if (_with13.Rows.Count > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention ce n° de contrat et d'avenant éxiste déja.Veuillez le(s) remplacer afin de poursuivre votre opération ", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            SablierOnOff(false);
                            txtNumContrat.Focus();

                            adors = null;
                            return;
                        }
                        boolVerifContratAv = false;
                        // adors.Close();

                        adors = null;
                    }

                    if (optRevFormule.Checked == true)
                    {

                        adocontrat.Rows[0]["CON_Coef"] = System.DBNull.Value;
                    }
                    else if (optSansRev.Checked == true)
                    {
                        adocontrat.Rows[0]["CodeFormule"] = "";
                        txtFormule.Text = "";
                        adocontrat.Rows[0]["indice1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["indice2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["indice3"] = System.DBNull.Value;
                        adocontrat.Rows[0]["valeur1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["valeur2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Valeur3"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date3"] = System.DBNull.Value;
                        adocontrat.Rows[0]["CON_Coef"] = System.DBNull.Value;

                    }
                    else if (optRevCoefFixe.Checked == true)
                    {
                        adocontrat.Rows[0]["CodeFormule"] = "";
                        txtFormule.Text = "";
                        adocontrat.Rows[0]["indice1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["indice2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["indice3"] = System.DBNull.Value;
                        adocontrat.Rows[0]["valeur1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["valeur2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Valeur3"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date1"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date2"] = System.DBNull.Value;
                        adocontrat.Rows[0]["Date3"] = System.DBNull.Value;

                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type de révision est obligatoire");
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        SablierOnOff(false);
                        return;

                    }

                    if (string.IsNullOrEmpty(txtNumContrat.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTCHAINEVIDE, General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.None);
                        txtNumContrat.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (string.IsNullOrEmpty(txtBaseContractuelle.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champs Base Contractuelle est obligatoire", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.None);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtBaseContractuelle.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (string.IsNullOrEmpty(txtBase.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champs Date de base est obligatoire", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtBase.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtIndice1.Text) && string.IsNullOrEmpty(txtIndice1Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice1.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice1Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtIndice1.Text) && string.IsNullOrEmpty(txtIndice1Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice1.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1]; txtIndice1Date.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    if (!string.IsNullOrEmpty(txtIndice2.Text) && string.IsNullOrEmpty(txtIndice2Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice2.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice2Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtIndice2.Text) && string.IsNullOrEmpty(txtIndice2Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice2.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice2Date.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtIndice3.Text) && string.IsNullOrEmpty(txtIndice3Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice3.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice3Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtIndice3.Text) && string.IsNullOrEmpty(txtIndice3Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice3.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice3Date.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    //== modif rachid ajout de 3 indices.
                    if (!string.IsNullOrEmpty(txtIndice4.Text) && string.IsNullOrEmpty(txtIndice4Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice4.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice4Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtIndice4.Text) && string.IsNullOrEmpty(txtIndice4Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice4.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice4Date.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    if (!string.IsNullOrEmpty(txtIndice5.Text) && string.IsNullOrEmpty(txtIndice5Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice5.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice5Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    else if (!string.IsNullOrEmpty(txtIndice5.Text) && string.IsNullOrEmpty(txtIndice5Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice5.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice5Date.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    if (!string.IsNullOrEmpty(txtIndice6.Text) && string.IsNullOrEmpty(txtIndice6Valeur.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice " + txtIndice6.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice6Valeur.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    else if (!string.IsNullOrEmpty(txtIndice6.Text) && string.IsNullOrEmpty(txtIndice6Date.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de l'indice " + txtIndice6.Text + " n'a pas été saisie", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtIndice6Date.Focus();
                        SablierOnOff(false);
                        return;
                    }



                    if (!string.IsNullOrEmpty(txtBaseRevisee.Text) && string.IsNullOrEmpty(txtDateRevision.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champs date de revision doit être fournis", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtDateRevision.Focus();
                        SablierOnOff(false);
                        return;
                    }
                    if (!string.IsNullOrEmpty(txtBaseActualisee.Text) && string.IsNullOrEmpty(txtDateActualisee.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champs date d'actualisation doit être fournis", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        txtDateActualisee.Focus();
                        SablierOnOff(false);
                        return;
                    }

                    var _with14 = this;
                    if (_with14.optRevCoefFixe.Checked == false && _with14.optRevFormule.Checked == false && optSansRev.Checked == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type de révision est obligatoire");
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        SablierOnOff(false);
                        return;
                    }
                    if (string.IsNullOrEmpty(txtCodeArticle.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type de prestation est obligatoire");
                        SSTab1.SelectedTab = SSTab1.Tabs[1];
                        cmdCodeArticle_Click(cmdCodeArticle, new System.EventArgs());
                        SablierOnOff(false);
                        return;
                    }

                    if (_with14.optRevCoefFixe.Checked == true)
                    {
                        txtCON_TypeContrat.Text = Convert.ToString(1);
                    }
                    else if (_with14.optRevFormule.Checked == true)
                    {
                        txtCON_TypeContrat.Text = Convert.ToString(2);
                    }
                    else if (optSansRev.Checked == true)
                    {
                        txtCON_TypeContrat.Text = Convert.ToString(3);
                    }

                    if (optRevCoefFixe.Checked == true)
                    {
                        if (string.IsNullOrEmpty(txtCON_Coef.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le contrat est de type revision avec coefficient fixe, vous devez saisir un coefficent", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            SSTab1.SelectedTab = SSTab1.Tabs[1];
                            txtCON_Coef.Focus();
                            return;
                        }
                    }

                    if (VerifTousLesChamps() == true)
                    {
                        SablierOnOff(false);
                        return;
                    }

                    //adocontrat.Update(); 
                    //if (boolVerifContratAv)

                    //update contrat

                    //j'ai ajouté ces lignes de code pour eviter le conflit concernat la source                        
                    string req = "select count(numcontrat) from contrat where numcontrat='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                    var tmp = new ModAdo();
                    int countC = Convert.ToInt32(General.nz(tmp.fc_ADOlibelle(req), 0));
                    if (countC > 0)
                    {
                        adocontrat = new DataTable();
                        string reqSelect = "select * from contrat where numcontrat='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                        SDAadocontrat = new SqlDataAdapter(reqSelect, General.adocnn);
                        SDAadocontrat.Fill(adocontrat);

                    }

                    UpdateContratDataTable();
                    SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                    var xxx = SDAadocontrat.Update(adocontrat);

                    boolValidate = false;

                    MiseAjourDesPeriodes();

                    //=== modif du 29 07 2013, mise a jour de l'onglet suivi.
                    fcMajSuivi();

                    //si l'utilisateur a effectué un addnew
                    if (blAddnew == true)
                    {

                        //txtNomImmeuble = ""
                        //txtNomSyndic = ""
                        //txtTypeFormule = ""
                        if (adocontrat.Rows[0]["nonFacturable"].ToString() == "1" || adocontrat.Rows[0]["nonFacturable"].ToString() == "True")
                        {
                            var xx = General.Execute("Update FacCalendrier set nonFacturable = 1 where NumContrat ='" + txtNumContrat.Text + "' and Avenant ='" + txtAvenant.Text + "'");
                        }
                        else if (adocontrat.Rows[0]["nonFacturable"].ToString() == "0" || adocontrat.Rows[0]["nonFacturable"].ToString() == "False")
                        {
                            var xx = General.Execute("Update FacCalendrier set nonFacturable = 0 where NumContrat ='" + txtNumContrat.Text + "' and Avenant ='" + txtAvenant.Text + "'");
                        }
                        cmdRechercheImmeuble.Visible = false;
                        txtCodeImmeuble.ReadOnly = true;
                        txtCodeImmeuble.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                        txtNumContrat.ReadOnly = true;
                        txtNumContrat.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                        txtAvenant.ReadOnly = true;
                        txtAvenant.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);


                        txtCodeImmeuble.ReadOnly = true;
                        txtNumContrat.ReadOnly = true;
                        txtAvenant.ReadOnly = true;
                        cmdSupprimer.Enabled = true;
                        //Contoles pouvant être ou pas utilisées
                        Frame1.Enabled = true;
                        lblSyndic.Enabled = true;
                        lblImmeuble.Enabled = true;
                        lblContrat.Enabled = true;
                        cmdSyndic.Enabled = true;
                        cmdImmeuble.Enabled = true;
                        cmdContrat.Enabled = true;
                        cmdAjouter.Enabled = true;

                        blAddnew = false;
                    }

                    if (blAddnew == false)
                    {
                        if (adocontrat.Rows[0]["nonFacturable"].ToString() == "1" || adocontrat.Rows[0]["nonFacturable"].ToString() == "True")
                        {

                            var xx = General.Execute("Update FacCalendrier set nonFacturable = " + General.fc_bln(true) + " where NumContrat ='" + txtNumContrat.Text + "' and Avenant ='" + txtAvenant.Text + "'");
                        }
                        else if (adocontrat.Rows[0]["nonFacturable"].ToString() == "0" || adocontrat.Rows[0]["nonFacturable"].ToString() == "False")
                        {

                            var xx = General.Execute("Update FacCalendrier set nonFacturable =" + General.fc_bln(false) + " where NumContrat ='" + txtNumContrat.Text + "' and Avenant ='" + txtAvenant.Text + "'");
                        }
                    }

                    //----Requete verifiant tous les contrats liées a l'immeuble
                    //----si chaque contrats avec le plus petit avenant(contrat principal)
                    //----a un code article a 999(contrat resiliée), on met a jour le champ
                    //----reiliée dans table prepintervention.
                    if (ChkResiliee.Checked == true)
                    {
                        Check3.Enabled = false;
                        Check3.CheckState = System.Windows.Forms.CheckState.Checked;
                        if (txtAvenant.Text == "0")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez résilier ce contrat et tous ses avenants.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            SQL = "UPDATE contrat set resiliee =" + General.fc_bln(true) + ", nonfacturable=" + General.fc_bln(true) + "  where numcontrat='" + txtNumContrat.Text + "'";
                            var xx = General.Execute(SQL);

                            SQL = "Update faccalendrier set resiliee =" + General.fc_bln(true) + ", nonfacturable=" + General.fc_bln(true) + "  where numcontrat ='" + txtNumContrat.Text + "'";
                            xx = General.Execute(SQL);
                        }
                        else
                        {
                            SQL = "UPDATE contrat set resiliee =" + General.fc_bln(true) + ", nonfacturable=" + General.fc_bln(true) + "  where numcontrat='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            var xx = General.Execute(SQL);

                            SQL = "Update faccalendrier set resiliee =" + General.fc_bln(true) + ", nonfacturable=" + General.fc_bln(true) + "  where numcontrat ='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            xx = General.Execute(SQL);
                        }

                    }
                    else if (ChkResiliee.CheckState == 0)
                    {
                        //===> Mondir le 06.01.2021, pour #2156
                        //Check3.Enabled = true;
                        //===> Fin Modif Mondir

                        //=================> Tested First
                        if (txtAvenant.Text == "0")
                        {
                            SQL = "UPDATE contrat set resiliee =" + General.fc_bln(false) + "  where numcontrat='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            var xx = General.Execute(SQL);

                            SQL = "Update faccalendrier set resiliee =" + General.fc_bln(false) + "  where numcontrat ='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            General.Execute(SQL);
                        }
                        else
                        {

                            SQL = "UPDATE contrat set resiliee =" + General.fc_bln(false) + "  where numcontrat='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            var xx = General.Execute(SQL);

                            SQL = "Update faccalendrier set resiliee =" + General.fc_bln(false) + " where numcontrat ='" + txtNumContrat.Text + "' AND Avenant='" + txtAvenant.Text + "'";
                            xx = General.Execute(SQL);
                        }
                    }

                    adors = new DataTable();
                    var _with15 = adors;

                    SQL = "SELECT numcontrat from contrat where codeimmeuble='" + txtCodeImmeuble.Text + "'" + " and resiliee =" + General.fc_bln(false);
                    _with15 = tmpAdo.fc_OpenRecordSet(SQL);
                    boolResilie = false;
                    if (_with15.Rows.Count == 0)
                    {
                        var xx = General.Execute("UPDATE PreparationIntervention SET Resiliee =" + General.fc_bln(true) + " " + " WHERE (((PreparationIntervention.Codeimmeuble)='" + txtCodeImmeuble.Text + "'))");
                    }
                    else
                    {
                        var xx = General.Execute("UPDATE PreparationIntervention SET Resiliee =" + General.fc_bln(false) + "" + " WHERE (((PreparationIntervention.Codeimmeuble)='" + txtCodeImmeuble.Text + "'))");
                    }
                    //_with15.Close();

                    rs = null;
                    adors = null;

                    //Set adors = New ADODB.Recordset
                    //With adors
                    //    SQL = "SELECT Min(Contrat.Avenant) AS MinDeAvenant, Contrat.NumContrat" _
                    //'    & " From Contrat" _
                    //'    & " Where (((Contrat.CodeImmeuble) ='" & txtCodeImmeuble & "'))" _
                    //'    & " GROUP BY Contrat.NumContrat"
                    //    .Open SQL, adocnn, adOpenForwardOnly, adLockReadOnly
                    //    boolResilie = True
                    //    If Not .EOF Then
                    //        Set rs = New ADODB.Recordset
                    //        Do While .EOF = False
                    //            SQL = "select codearticle from contrat where numcontrat='" & !NumContrat & "'" _
                    //'            & " and avenant='" & adors!MinDeAvenant & "'"
                    //            rs.Open SQL, adocnn, adOpenForwardOnly, adLockReadOnly
                    //            If Not rs.EOF Then
                    //                If rs!CodeArticle <> "999" Then
                    //                    boolResilie = False
                    //                    rs.Close
                    //                    Exit Do
                    //                End If
                    //            End If
                    //            rs.Close
                    //            .MoveNext
                    //        Loop
                    //        If boolResilie = True Then
                    //            adocnn.Execute "UPDATE PreparationIntervention SET Resiliee = True" _
                    //'            & " WHERE (((PreparationIntervention.Codeimmeuble)='" & txtCodeImmeuble & "'))"
                    //        Else
                    //            adocnn.Execute "UPDATE PreparationIntervention SET Resiliee = false" _
                    //'            & " WHERE (((PreparationIntervention.Codeimmeuble)='" & txtCodeImmeuble & "'))"
                    //        End If
                    //    End If
                    //'    .Close
                    //    Set rs = Nothing
                    //    Set adors = Nothing
                    //End With


                    //If txtCodeImmeuble <> "" Then RechercheParImmeuble2 (txtCodeImmeuble)
                    //If txtSyndic <> "" Then RechercheParSyndic2 (txtSyndic)
                    SablierOnOff(false);
                    return;
                }
            }

            catch (Exception ex)
            {
                Program.SaveException(ex);
                //champs primaires doublons
                if (ex.HResult == -2147467259)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTPRIMAIREFAUX, General.TITREDONNEEFAUX);
                    SablierOnOff(false);
                    txtNumContrat.Focus();
                    // Adodc1.Recordset.CancelUpdate
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        void cmdAjouterRec(Control C)
        {
            if (boolTableContratVide)
            {
                foreach (Control CC in C.Controls)
                {
                    if (CC is iTalk.iTalk_TextBox_Small2)
                    {
                        if (CC.Tag != null && CC.Tag.ToString() == "valide")
                            CC.Enabled = true;
                    }
                    if (CC.HasChildren)
                        cmdAjouterRec(CC);
                }
            }


            //If boolTableContratVide = True Then
            //    For Each ControleSurFeuille In Me.Controls
            //        If TypeOf ControleSurFeuille Is TextBox Then
            //          If ControleSurFeuille.Tag = "valide" Then
            //            ControleSurFeuille.Enabled = True
            //         End If
            //        End If
            //    Next
            //    boolTableContratVide = False
            //End If
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            try
            {
                SablierOnOff(true);

                SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                if (adocontrat != null && adocontrat.Rows.Count > 0 && SDAadocontrat != null)
                {
                    SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                    var xx = SDAadocontrat.Update(adocontrat);
                }


                //adocontrat.AddNew();  

                string requete = "SELECT Contrat.* From Contrat Where Contrat.NumContrat ='0'";

                Adodc1 = new DataTable();
                adocontrat = new DataTable();
                SDAadocontrat = new SqlDataAdapter(requete, General.adocnn);
                SDAadocontrat.Fill(adocontrat);
                Adodc1 = adocontrat;
                SDAAdodc1 = SDAadocontrat;
                adocontrat.Rows.Add(adocontrat.NewRow());

                if (cmdMAJ.Enabled == false)
                {
                    cmdMAJ.Enabled = true;
                }

                if (cmdAnnuler.Enabled == false)
                {
                    cmdAnnuler.Enabled = true;
                }

                if (cmdImprimer.Enabled == false)
                {
                    cmdImprimer.Enabled = true;
                }

                cmdAjouter.Enabled = false;

                InitialiseImmeubleGerant();

                txtFormule.Text = "";
                cmdRechercheImmeuble.Visible = true;
                txtCodeImmeuble.ReadOnly = false;
                txtCodeImmeuble.BackColor = System.Drawing.Color.White;
                txtNumContrat.ReadOnly = false;
                txtNumContrat.BackColor = System.Drawing.Color.White;
                txtAvenant.ReadOnly = false;
                txtAvenant.BackColor = System.Drawing.Color.White;



                txtCodeImmeuble.ReadOnly = false;
                txtNumContrat.ReadOnly = false;
                txtAvenant.ReadOnly = false;
                cmdSupprimer.Enabled = false;
                Frame1.Enabled = false;
                lblSyndic.Enabled = false;
                lblImmeuble.Enabled = false;
                lblContrat.Enabled = false;
                cmdSyndic.Enabled = false;
                cmdImmeuble.Enabled = false;
                cmdContrat.Enabled = false;
                SSOleDBGrid1.Enabled = false;
                //SynchroCalendrier();


                blAddnew = true;
                //voir la validation
                boolOk = true;
                boolVerifContratAv = true;

                //dans ce programme se trouve deux validations
                //une dans cmdMaj, l'autre valide a l'insu de l'utili. dans
                //sstab1. Avant chaque validation une verificatoin du n°de
                //contrat et d'avenant se produit, ce boolean empeche deux verif.
                boolFocVerifCtrAv = true;

                //permet la verification du numero de
                //contrat et d'avenant dans le focus de txtAvenant
                cmdAjouterRec(this);
                if (boolTableContratVide)
                    boolTableContratVide = false;


                /* if (boolTableContratVide == true) vérifier
                 {
                     foreach (object ControleSurFeuille_loopVariable in this.Controls)
                     {
                         ControleSurFeuille = ControleSurFeuille_loopVariable;

                         if (ControleSurFeuille is System.Windows.Forms.TextBox)
                         {
                             if (ControleSurFeuille.Tag == "valide")
                             {
                                 ControleSurFeuille.Enabled = true;
                             }
                         }
                     }
                     boolTableContratVide = false;
                 }*/

                txtNumContrat.Focus();
                SSTab1.SelectedTab = SSTab1.Tabs[0];

                fc_TypeRevsion(Convert.ToString(0));
                fc_TypeEchuEchoir(0);

                ChkResiliee.CheckState = System.Windows.Forms.CheckState.Unchecked;

                fc_clearSuivi();

                Check3.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Check2.CheckState = System.Windows.Forms.CheckState.Checked;
                chkCalcEcheance.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkFactureManuelle.CheckState = System.Windows.Forms.CheckState.Unchecked;

                lblAutoliquidation.Visible = false;

                SablierOnOff(false);

                ClearAllControls(this);

                //vide le ssoledbgrid
                SynchroCalendrier();
                Check1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkCalcEcheance.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Check2.CheckState = System.Windows.Forms.CheckState.Checked;
                txtAvenant.Text = "0";
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAjouter_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void ClearAllControls(Control C)
        {
            foreach (Control CC in C.Controls)
            {
                if (CC is iTalk.iTalk_TextBox_Small2)
                    ((iTalk.iTalk_TextBox_Small2)CC).Text = "";
                else if (CC is UltraGrid)
                    ((UltraGrid)CC).DataSource = null;
                if (CC.HasChildren)
                    ClearAllControls(CC);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void MiseAjourDesPeriodes()
        {
            int lCount = 0;
            DateTime dtDu = new DateTime();
            DateTime dtAu = new DateTime();
            DateTime dtDate = new DateTime();
            DateTime dteffet;
            SablierOnOff(true);
            indi1 = 1;

            if (Adodc2 == null || Adodc2.Rows.Count == 0)
                return;

            int i = 0;

            while (true)
            {
                if (Adodc2.Rows[i]["histo"].ToString() == "Vrai" || Adodc2.Rows[i]["histo"].ToString() == "True")
                    i++;
                else
                    break;
            }

            //            'Cette boucle permet de verifier les champs Date, PcDu , Au
            //            'si des valeurs se trouvent dans les champs PcDu, Au et pas dans le champs
            //            'Date alors ceux-çi seront effacés. Cette boucle permet en méme temps de
            //            'verifier si des valeurs se trouvent dans le champs Date avec boolAutoriseMiseAJour
            //            'afin d'autoriser la procedure suivante

            while (indi1 != 14)
            {
                if (Adodc2.Rows[i]["date"].ToString() == "")
                {
                    Adodc2.Rows[i]["PCDu"] = DBNull.Value;
                    Adodc2.Rows[i]["Au"] = DBNull.Value;
                    SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                    var xx = SDAAdodc2.Update(Adodc2);
                }
                else if (Adodc2.Rows[i][1].ToString() != "")
                {
                    boolAutoriseMiseAJour = true;
                    lCount = lCount + 1;
                }
                indi1 = indi1 + 1;
                i++;
            }

            i = 0;


            while (true)
            {
                if (Adodc2.Rows[i]["histo"] != DBNull.Value && Convert.ToBoolean(Adodc2.Rows[i]["histo"]))
                    i++;
                else
                    break;
            }

            if (Option6.Checked)
            {
                if (!string.IsNullOrEmpty(Adodc2.Rows[i]["date"].ToString()) && General.IsDate(Adodc2.Rows[i]["date"]))
                {
                    if (lCount == 1)
                    {

                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                        dteffet = Convert.ToDateTime(Convert.ToDateTime(Text2.Text).Day + "/" + Convert.ToDateTime(Text2.Text).Month + "/" + dtDate.Year);
                        Adodc2.Rows[i]["Au"] = dteffet.ToShortDateString();
                        dtDate = dteffet;
                        dtDate = dtDate.AddYears(-1);
                        dtDate = dtDate.AddDays(-1);
                        Adodc2.Rows[i]["PCDu"] = dtDate.ToShortDateString();

                        SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                        var xx = SDAAdodc2.Update(Adodc2);

                    }
                    else if (lCount > 1)
                    {
                        //=== récupere la derniére date.
                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);

                        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                        //dteffet = Convert.ToDateTime(Convert.ToDateTime(Text2.Text).Day + "/" + Convert.ToDateTime(Text2.Text).Month + "/" + dtDate.Year);
                        //=== modif du 09 / 01 / 2020
                        dteffet = Convert.ToDateTime(Text2.Text.ToDate().Day + "/" + Text2.Text.ToDate().Month + "/" +
                                                     Text2.Text.ToDate().Year);
                        //===> Fin Modif Mondir

                        while (true)
                        {
                            if (Adodc2.Rows[i]["date"].ToString() != "")
                                i++;
                            else
                                break;
                        }

                        i--;

                        dtDate = dteffet;
                        dtDate = dteffet.AddYears(1);
                        dtDate = dtDate.AddDays(-1);

                        Adodc2.Rows[i]["Au"] = dtDate;

                        //                '=== se repositionne au début
                        while (true)
                        {
                            if (Adodc2.Rows[i]["histo"] != DBNull.Value && Convert.ToBoolean(Adodc2.Rows[i]["histo"]) == false && Convert.ToInt32(Adodc2.Rows[i]["echeance"]) != 1)
                                i--;
                            else
                                break;
                        }

                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                        Adodc2.Rows[i]["PCDu"] = dteffet.ToShortDateString();
                        Adodc2.Rows[i]["Au"] = dtDate.ToShortDateString();
                        SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                        var xx = SDAAdodc2.Update(Adodc2);

                        i++;


                        while (Adodc2.Rows[i]["date"].ToString() != "")
                        {
                            Adodc2.Rows[i]["PCDu"] = dtDate.AddDays(1).ToShortDateString();
                            dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                            Adodc2.Rows[i]["Au"] = dtDate.ToShortDateString();

                            SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                            xx = SDAAdodc2.Update(Adodc2);

                            i++;

                        }
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("il faut remplir la premiere ligne du calendrier", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (Option7.Checked)
            {
                if (!string.IsNullOrEmpty(Adodc2.Rows[i]["date"].ToString()) && General.IsDate(Adodc2.Rows[i]["date"]))
                {

                    //' à échoir
                    if (lCount == 1)
                    {

                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                        Adodc2.Rows[i]["PCDu"] = dtDate.ToShortDateString();
                        dtDate = dtDate.AddDays(-1);
                        Adodc2.Rows[i]["Au"] = dtDate.AddYears(1).ToShortDateString();

                        SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                        var xx = SDAAdodc2.Update(Adodc2);

                    }
                    else if (lCount > 1)
                    {

                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                        //                    ' se repositionne sur le dernier enregistrement.
                        while (true)
                        {
                            if (Adodc2.Rows[i]["date"].ToString() != "")
                                i++;
                            else
                                break;
                        }

                        i--;
                        Adodc2.Rows[i]["PCDu"] = Convert.ToDateTime(Adodc2.Rows[i]["date"]).ToShortDateString();
                        dtDate = dtDate.AddDays(-1);
                        dtDate = dtDate.AddYears(1);
                        Adodc2.Rows[i]["Au"] = dtDate.ToShortDateString();

                        SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                        var xx = SDAAdodc2.Update(Adodc2);

                        dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);

                        i--;
                        if (i == -1)
                            i = 0;

                        while (Adodc2.Rows[i]["histo"] == DBNull.Value || Convert.ToBoolean(Adodc2.Rows[i]["histo"]) == false)
                        {
                            Adodc2.Rows[i]["PCDu"] = Convert.ToDateTime(Adodc2.Rows[i]["date"]).ToShortDateString();
                            Adodc2.Rows[i]["Au"] = dtDate.AddDays(-1).ToShortDateString();
                            dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);

                            SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                            xx = SDAAdodc2.Update(Adodc2);

                            if (Convert.ToInt32(Adodc2.Rows[i]["echeance"]) == 1)
                                break;

                            i--;
                            if (i < 0)
                                break;
                        }
                        //}
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("il faut remplir la premiere ligne du calendrier", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else if (optAnnuel.Checked) //annuel sur 1 er échéance.
            {
                if (!string.IsNullOrEmpty(Adodc2.Rows[i]["date"].ToString()) && General.IsDate(Adodc2.Rows[i]["date"]))
                {

                    dtDate = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                    dtDu = Convert.ToDateTime(Adodc2.Rows[i]["date"]);
                    Adodc2.Rows[i]["PCDu"] = dtDu.ToShortDateString();
                    dtDate = dtDate.AddDays(-1);
                    dtDate = dtDate.AddYears(1);
                    Adodc2.Rows[i]["Au"] = dtDate.ToShortDateString();

                    SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                    var xx = SDAAdodc2.Update(Adodc2);

                    i++;

                    while (i < Adodc2.Rows.Count && Adodc2.Rows[i]["date"].ToString() != "")
                    {
                        Adodc2.Rows[i]["PCDu"] = dtDu.ToShortDateString();
                        Adodc2.Rows[i]["Au"] = dtDate.ToShortDateString();

                        SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                        xx = SDAAdodc2.Update(Adodc2);

                        i++;
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("il faut remplir la premiere ligne du calendrier", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }

        }


        //private int nbrRowContainsDate(DataTable dt,int i)
        //{

        //    bool find = false;
        //    while (!find && i < Adodc2.Rows.Count)
        //    {
        //        if (Adodc2.Rows[i]["date"].ToString() != "" && General.IsDate(Adodc2.Rows[i]["date"].ToString()))
        //        {
        //            find = true;
        //            break;
        //        }
        //        else
        //            i++;
        //    }
        //    if(find)
        //      return i;
        //    else
        //      return -1;
        //}
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPrecedent_Click(object sender, EventArgs e)
        {
            string strNoAvenant = null;
            string strNoContrat = null;

            //======> Mondir 18.05.2020 : Added a where condition on CodeImmeuble to fix https://groupe-dt.mantishub.io/view.php?id=1805

            using (var tmpAdo = new ModAdo())
            {
                strNoAvenant = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.Avenant FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' AND Contrat.NumContrat='" + txtNumContrat.Text + "' AND Contrat.Avenant<'" + txtAvenant.Text + "' ORDER BY Avenant DESC"), txtAvenant.Text).ToString();

                if (strNoAvenant == txtAvenant.Text)
                {
                    strNoContrat = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.NumContrat FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' AND NumContrat<'" + txtNumContrat.Text + "' ORDER BY NumContrat DESC"), txtNumContrat.Text).ToString();
                    strNoAvenant = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.Avenant FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' AND NumContrat<'" + txtNumContrat.Text + "' ORDER BY NumContrat DESC, Avenant DESC"), "0").ToString();
                    fc_ChargeContrat(strNoContrat, strNoAvenant);
                }
                else
                {
                    fc_ChargeContrat(txtNumContrat.Text, strNoAvenant);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPremier_Click(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                string strNoContrat = null;
                strNoContrat = General.nz(tmpAdo.fc_ADOlibelle("SELECT TOP 1 Contrat.NumContrat FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' ORDER BY NumContrat ASC"), txtNumContrat.Text).ToString();
                fc_ChargeContrat(strNoContrat, "0");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strNumContrat"></param>
        /// <param name="strNoAvenant"></param>
        private void fc_ChargeContrat(string strNumContrat, string strNoAvenant)
        {
            Adodc1 = new DataTable();
            string req = "SELECT TOP 1 Contrat.* From Contrat where NumContrat ='" + strNumContrat + "' and Avenant ='" + strNoAvenant + "'";
            SDAAdodc1 = new SqlDataAdapter(req, General.CHEMINBASE);
            SDAAdodc1.Fill(Adodc1);
            //===  modif du 29 07 2013,  ajout de l'onglet suivi.
            fc_LoadSuivi();
            adocontrat = Adodc1;
            SDAadocontrat = SDAAdodc1;
            RecupererValeurs(Adodc1);

            //SDAAdodc1.Fill(adocontrat);
            //Adodc1 = adocontrat;
            //SDAAdodc1 = SDAadocontrat;

            if (adocontrat.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }
                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }
            }
            else
            {
                TableContratVide(this);
            }
            //remplit la table FacCalendrier (ssOleDbgrid1)
            string sql = "SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + strNumContrat + "' And FacCalendrier.Avenant ='" + strNoAvenant + "' order by histo DESC,ECHEANCE";
            Adodc2 = new DataTable();
            SDAAdodc2 = new SqlDataAdapter(sql, General.CHEMINBASE);
            SDAAdodc2.Fill(Adodc2);
            SSOleDBGrid1.DataSource = Adodc2;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {

            string req = "SELECT " + " codeImmeuble as \"CodeImmeuble\" , Adresse as \"Adresse\" FROM immeuble ";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeubles" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells["codeImmeuble"].Value.ToString();
                RechercheParImmeuble(fg.ugResultat.ActiveRow.Cells["codeImmeuble"].Value.ToString());
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells["codeImmeuble"].Value.ToString();
                    RechercheParImmeuble(fg.ugResultat.ActiveRow.Cells["codeImmeuble"].Value.ToString());
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdResilié_Click(object sender, EventArgs e)
        {
            string sT = null;

            //===> Mondir le 12.03.2021 changer le mot de pass, demanadé oar Fréd
            //sT = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le mot de passe", "Mot de passe");

            //if (sT != "cachou")
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Mot de passe invalide", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    return;
            //}

            var aut = ModAutorisation.fc_DroitDetail("UserDocFicheContrat-cmdResilié", 2);
            if (aut == 0)
                return;
            //===> Fin Modif Mondir

            Forms.frmResiliation frmresil = new Forms.frmResiliation(this);
            frmresil.txtNumContrat.Text = txtNumContrat.Text;
            frmresil.txtAvenant.Text = txtAvenant.Text;
            frmresil.lblContrat.Text = "Contrat: " + txtNumContrat.Text + " Avenant: " + txtAvenant.Text;
            frmresil.lblImmeuble.Text = "Immeuble: " + txtCodeImmeuble.Text;
            frmresil.chknonFacturable.CheckState = Check3.CheckState;
            frmresil.chkFactureManuelle.CheckState = chkFactureManuelle.CheckState;
            frmresil.ChkResiliee.CheckState = ChkResiliee.CheckState;
            frmresil.ShowDialog();
            cmdMAJ_Click(cmdMAJ, new System.EventArgs());
            frmresil.Close();
        }

        /// <summary>
        /// Control Is In A Hidden Container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSecteur_Click(object sender, EventArgs e)
        {
            string req = "Select sec_Code, Sec_Libelle from SEC_Secteur ";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche du contrat" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtAnalytique.Text = fg.ugResultat.ActiveRow.Cells["sec_Code"].Value.ToString();
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtAnalytique.Text = fg.ugResultat.ActiveRow.Cells["sec_Code"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSuivant_Click(object sender, EventArgs e)
        {
            string strNoAvenant = null;
            string strNoContrat = null;

            //======> Mondir 18.05.2020 : Added a where condition on CodeImmeuble to fix https://groupe-dt.mantishub.io/view.php?id=1805

            using (var tmpAdo = new ModAdo())
            {

                strNoAvenant = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.Avenant FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' AND Contrat.NumContrat='" + txtNumContrat.Text + "' AND Contrat.Avenant>'" + txtAvenant.Text + "' ORDER BY Avenant ASC"), txtAvenant.Text).ToString();
                if (strNoAvenant == txtAvenant.Text)
                {
                    strNoContrat = General.nz(tmpAdo.fc_ADOlibelle("SELECT Contrat.NumContrat FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text + "' AND NumContrat>'" + txtNumContrat.Text + "' ORDER BY NumContrat ASC"), txtNumContrat.Text).ToString();
                    if (strNoContrat != txtNumContrat.Text)
                    {
                        using (var tmpModADO = new ModAdo())
                        {
                            //=============> Mondir 05.06.2020 : Changed to fix https://groupe-dt.mantishub.io/view.php?id=1830
                            //
                            //Was : fc_ChargeContrat(strNoContrat, "0");
                            //For This exemple, Contrat number "3164" dont have Avenant "0", so we need to get the first Avenant !
                            //
                            //
                            var tmpDT = tmpModADO.fc_OpenRecordSet(
                                "SELECT Contrat.NumContrat, Avenant FROM Contrat WHERE CodeImmeuble='" + txtCodeImmeuble.Text +
                                "' AND NumContrat>'" + txtNumContrat.Text + "' ORDER BY NumContrat, Avenant ASC");
                            fc_ChargeContrat(strNoContrat, tmpDT.Rows[0]["Avenant"].ToString());
                        }
                    }
                }
                else
                {
                    fc_ChargeContrat(txtNumContrat.Text, strNoAvenant);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSyndic_Click(object sender, EventArgs e)
        {
            //string req = "SELECT " 
            //    + "Table1.Nom as \"Nom\", Contrat.Code1 as \"Syndic\", Contrat.NumContrat as \"N°Contrat\", Contrat.Avenant as \"Avenant\" FROM Table1 INNER JOIN Contrat ON Table1.Code1 = Contrat.Code1";

            string req = " SELECT  Table1.Nom as \"Nom\", Table1.Code1 as \"Syndic\", Contrat.NumContrat as \"N°Contrat\", Contrat.Avenant as \"Avenant\" "
                       + "FROM  Table1 INNER JOIN "
                       + " Immeuble ON Table1.Code1 = Immeuble.Code1 INNER JOIN "
                       + " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble";

            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche du gérant" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                ClearAllControls(this);
                SablierOnOff(true);
                if (fg.ugResultat.ActiveRow == null) return;
                Adodc1 = new DataTable();
                SDAAdodc1 = new SqlDataAdapter("select contrat.* from contrat where Contrat.Code1 ='" + fg.ugResultat.ActiveRow.Cells[2].Value + "'and Contrat.NumContrat ='" + fg.ugResultat.ActiveRow.Cells[3].Value + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[4].Value + "'", General.adocnn);
                SDAAdodc1.Fill(Adodc1);

                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();

                adocontrat = Adodc1;
                SDAadocontrat = SDAAdodc1;
                //SDAAdodc1.Fill(adocontrat);
                RecupererValeurs(adocontrat);

                //=== modif du 24 07 2012, reactive les champs.
                if (adocontrat.Rows.Count > 0)
                {
                    TableContratActive(this);
                }

                SSTab1.SelectedTab = SSTab1.Tabs[0];

                SynchroCalendrier();

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }

                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }

                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);

                SablierOnOff(false);
                fg.Dispose();
                fg.Close();

            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    ClearAllControls(this);
                    SablierOnOff(true);
                    if (fg.ugResultat.ActiveRow == null) return;
                    Adodc1 = new DataTable();
                    SDAAdodc1 = new SqlDataAdapter("select contrat.* from contrat where Contrat.Code1 ='" + fg.ugResultat.ActiveRow.Cells[2].Value + "'and Contrat.NumContrat ='" + fg.ugResultat.ActiveRow.Cells[3].Value + "' and Contrat.Avenant ='" + fg.ugResultat.ActiveRow.Cells[4].Value + "'", General.adocnn);
                    SDAAdodc1.Fill(Adodc1);

                    //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                    fc_LoadSuivi();

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;

                    RecupererValeurs(adocontrat);

                    //=== modif du 24 07 2012, reactive les champs.
                    if (adocontrat.Rows.Count > 0)
                    {
                        TableContratActive(this);
                    }

                    SSTab1.SelectedTab = SSTab1.Tabs[0];

                    SynchroCalendrier();

                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        RechercheParImmeuble(txtCodeImmeuble.Text);
                    }
                    else
                    {
                        InitialiseImmeubleGerant();
                    }

                    if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                    {
                        RechercheParCodeArticle(txtCodeArticle.Text);
                    }
                    else
                    {
                        InitialiseArticle();
                    }

                    if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                    {
                        RechercheFormule(txtTypeFormule.Text);
                    }
                    else
                    {
                        txtFormule.Text = "";
                    }

                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtNumContrat.Text);

                    SablierOnOff(false);
                    fg.Dispose();
                    fg.Close();
                }

            };
            blSupp = true;
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command2_Click(object sender, EventArgs e)
        {

            if (Check1.CheckState == CheckState.Unchecked && Check2.CheckState == CheckState.Unchecked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cliquez d'abord sur réel ou connu.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtTypeFormule.Text = "";
                return;
            }

            string req = "SELECT " + " FacFormule.Libelle as \"Libelle\", FacFormule.Type as \"Type\" FROM FacFormule";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de la formule" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtIndice1.Text = "";
                txtIndice2.Text = "";
                txtIndice3.Text = "";
                txtIndice4.Text = "";
                txtIndice5.Text = "";
                txtIndice6.Text = "";
                txtFormule.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                txtTypeFormule.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                adocontrat.Rows[0]["CodeFormule"] = txtTypeFormule.Text;
                RechercheQuotient(fg.ugResultat.ActiveRow.Cells[1].Value.ToString());
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtIndice1.Text = "";
                    txtIndice2.Text = "";
                    txtIndice3.Text = "";
                    txtIndice4.Text = "";
                    txtIndice5.Text = "";
                    txtIndice6.Text = "";
                    txtFormule.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                    txtTypeFormule.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    adocontrat.Rows[0]["CodeFormule"] = txtTypeFormule.Text;
                    RechercheQuotient(fg.ugResultat.ActiveRow.Cells[0].Value.ToString());
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Control Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTSUPPRESSION, General.TITRESUPPRESSION, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dg == DialogResult.Yes)
            {
                SablierOnOff(true);
                SqlDataAdapter SDAadocontrat = new SqlDataAdapter();
                SDAadocontrat.Fill(adocontrat);
                SqlCommandBuilder SCBcontrat = new SqlCommandBuilder(SDAadocontrat);
                if (adocontrat.Rows.Count > 0)
                {
                    //adocontrat.Delete(); verifier

                }

                //If adoContrat.EOF = False Then
                SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat order by NumContrat", General.adocnn);
                SDAAdodc1.Fill(adocontrat);
                if (adocontrat.Rows.Count == 0)
                    TableContratVide(this);
                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();

                //End If

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }
                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }
                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }
                SynchroCalendrier();
                SablierOnOff(false);
            }
            return;

        }

        /// <summary>
        /// Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command4_Click(object sender, EventArgs e)
        {

            string req = "SELECT " + "codeImmeuble as \"Code Immeuble:\",Adresse  as \"Adresse:\" FROM immeuble ";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeubles" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtAnalytique.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                txtNomAnalytique.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                adocontrat.Rows[0]["AnalytiqueImmeuble"] = txtAnalytique.Text;
                adocontrat.Rows[0]["LibelleAnaImmeuble"] = txtNomAnalytique.Text;
                //RechercheAnalytique (.SSOleDBGrid1.Columns(0).Value)
                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtAnalytique.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtNomAnalytique.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    adocontrat.Rows[0]["AnalytiqueImmeuble"] = txtAnalytique.Text;
                    adocontrat.Rows[0]["LibelleAnaImmeuble"] = txtNomAnalytique.Text;
                    //RechercheAnalytique (.SSOleDBGrid1.Columns(0).Value)
                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label3_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtSyndic.Text))
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtSyndic.Text);
                View.Theme.Theme.Navigate(typeof(UserDocClient));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label4_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
            }
        }

        private void Label53_Click(object sender, EventArgs e)
        {
            //TODO : Mondir - Must Developpe
            View.Theme.Theme.Navigate(typeof(UserListeContrat));
            //  ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserListeContrat);
        }

        private void lblGoFacturation_Click(object sender, EventArgs e)
        {

            View.Theme.Theme.Navigate(typeof(UserDocImprimeFacture));
            // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGIMPRIMEFACTURE);
        }

        private void lblGoFormule_Click(object sender, EventArgs e)
        {
            //TODO : Mondir - Must Developpe
            General.ParamVisu = "Contrat Formule";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
            //fc_Navigue Me, gsCheminPackage & PROGUserDocFormule
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGPARAMETRE);todo

        }

        private void lblGoHisto_Click(object sender, EventArgs e)
        {
            var lDroit = ModAutorisation.fc_DroitDetail("UserDocHistoContrat");
            if (lDroit == 0)
                return;
            View.Theme.Theme.Navigate(typeof(UserDocHistoContrat));
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocHistoContrat);
        }

        private void lblGoIndice_Click(object sender, EventArgs e)
        {
            General.ParamVisu = "Contrat Indice";
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
            //fc_Navigue Me, gsCheminPackage & PROGUserFacIndice
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGPARAMETRE);todo

        }

        private void lblGoListeContrats_Click(object sender, EventArgs e)
        {
            View.Theme.Theme.Navigate(typeof(UserListeContrat));
            //  ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGListeContrat);todo
        }

        private void lblGoTypeRevision_Click(object sender, EventArgs e)
        {
            General.ParamVisu = "Contrat Type Ratio";
            //fc_Navigue Me, gsCheminPackage & PROGUserDocTypeRatio
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGPARAMETRE);todo
            View.Theme.Theme.Navigate(typeof(UserDocParamtre));
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optAnnuel_CheckedChanged(object sender, EventArgs e)
        {
            if (optAnnuel.Checked)
            {
                fc_TypeEchuEchoir(4);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optAutre_CheckedChanged(object sender, EventArgs e)
        {
            if (optAutre.Checked)
            {
                fc_TypeEchuEchoir(3);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option7_CheckedChanged(object sender, EventArgs e)
        {
            if (Option7.Checked)
            {
                fc_TypeEchuEchoir(2);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option6_CheckedChanged(object sender, EventArgs e)
        {
            if (Option6.Checked)
            {
                if (string.IsNullOrEmpty(Text2.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date d'effet avant de saisir le calendrier.", "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    Text2.Focus();
                    SablierOnOff(false);
                    return;
                }
                fc_TypeEchuEchoir(1);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optRevCoefFixe_CheckedChanged(object sender, EventArgs e)
        {
            if (optRevCoefFixe.Checked)
            {
                fc_TypeRevsion(Convert.ToString(1));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optRevFormule_CheckedChanged(object sender, EventArgs e)
        {
            if (optRevFormule.Checked)
            {
                fc_TypeRevsion(Convert.ToString(2));

                if (Check2.CheckState == CheckState.Unchecked && Check1.CheckState == CheckState.Unchecked)
                {
                    Check2.CheckState = System.Windows.Forms.CheckState.Checked;
                }

            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optSansRev_CheckedChanged(object sender, EventArgs e)
        {
            if (optSansRev.Checked)
            {
                fc_TypeRevsion(Convert.ToString(3));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                SablierOnOff(true);

                //If CR.ReportFileName = "" Then CR.ReportFileName = CHEMINFACTURECONTRAT
                // If CR.ReportFileName = "" Then
                // If CDate(SSOleDBGrid1.Columns("date").Value) < #6/1/2001# Then
                //     CR.ReportFileName = CHEMINFACTURECONTRAT
                // ElseIf CDate(SSOleDBGrid1.Columns("date").Value) < #8/26/2002# Then
                //      CR.ReportFileName = CHEMINNEWFACTURECONTRAT

                //  Else
                // CR.ReportFileName = General.CHEMINEUROONLY;

                ReportDocument CR = new ReportDocument();
                string SelectionFormula;
                CR.Load(General.CHEMINEUROONLY);

                // End If
                //End If
                if (!string.IsNullOrEmpty(SSOleDBGrid1.DisplayLayout.Bands[0].Columns["nofacture"].Key))
                {
                    SelectionFormula = "{FactEnTete.Nofacture}='" + SSOleDBGrid1.ActiveRow.Cells["nofacture"].Value + "'";
                    //Connect_Base

                    // CR.Action = 1;
                    // CR.SelectionFormula = "";
                    // CR.ReportFileName = "";
                    // CR.Reset();
                    CrystalReportFormView CrFrV = new CrystalReportFormView(CR, SelectionFormula);
                    CrFrV.Show();
                }
                SablierOnOff(false);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        private void UpdateContratDataTable()
        {
            adocontrat.Rows[0]["NumContrat"] = txtNumContrat.Text;
            if (txtAvenant.Text != "")
                adocontrat.Rows[0]["Avenant"] = txtAvenant.Text;
            else
                adocontrat.Rows[0]["Avenant"] = 0;
            adocontrat.Rows[0]["LibelleCont1"] = txtLibelleCont1.Text;
            adocontrat.Rows[0]["LibelleCont2"] = txtLibelleCont2.Text;
            adocontrat.Rows[0]["Resiliee"] = ChkResiliee.Checked ? 1 : 0;
            adocontrat.Rows[0]["CodeImmeuble"] = txtCodeImmeuble.Text;
            adocontrat.Rows[0]["nonFacturable"] = Check3.Checked ? 1 : 0;
            adocontrat.Rows[0]["FactureManuelle"] = chkFactureManuelle.Checked ? 1 : 0;
            adocontrat.Rows[0]["RefGerant"] = txtRefGerant.Text;
            if (General.IsDate(Text1.Text))
                adocontrat.Rows[0]["DateDeSignature"] = Convert.ToDateTime(Text1.Text);
            if (General.IsDate(Text2.Text))
                adocontrat.Rows[0]["DateEffet"] = Convert.ToDateTime(Text2.Text);
            if (General.IsDate(Text3.Text))
                adocontrat.Rows[0]["DateFin"] = Convert.ToDateTime(Text3.Text);
            else if (Text3.Text == "")
                adocontrat.Rows[0]["DateFin"] = DBNull.Value;
            adocontrat.Rows[0]["CON_CompAdresse1"] = txtCON_CompAdresse1.Text;
            adocontrat.Rows[0]["CON_CompAdresse2"] = txtCON_CompAdresse2.Text;
            adocontrat.Rows[0]["CodeArticle"] = General.nz(txtCodeArticle.Text, DBNull.Value);
            adocontrat.Rows[0]["BaseContractuelle"] = Convert.ToDouble(General.nz(txtBaseContractuelle.Text, 0));
            if (txtBaseActualisee.Text != "")
                adocontrat.Rows[0]["BaseActualisee"] = txtBaseActualisee.Text;
            else
                adocontrat.Rows[0]["BaseActualisee"] = DBNull.Value;
            if (!string.IsNullOrEmpty(txtBase.Text) && General.IsDate(txtBase.Text))
                adocontrat.Rows[0]["DateBase"] = txtBase.Text;
            if (!string.IsNullOrEmpty(txtDateActualisee.Text) && General.IsDate(txtDateActualisee.Text))
                adocontrat.Rows[0]["DateActualisee"] = txtDateActualisee.Text;
            adocontrat.Rows[0]["CON_TypeContrat"] = Convert.ToInt32(General.nz(txtCON_TypeContrat.Text, 0));
            if (txtCON_Coef.Text != "")
                adocontrat.Rows[0]["CON_Coef"] = txtCON_Coef.Text;
            else
                adocontrat.Rows[0]["CON_Coef"] = DBNull.Value;
            adocontrat.Rows[0]["CodeFormule"] = txtTypeFormule.Text;
            adocontrat.Rows[0]["Reel"] = Check1.Checked ? 1 : 0;
            adocontrat.Rows[0]["Connu"] = Check2.Checked ? 1 : 0;
            adocontrat.Rows[0]["Indice1"] = txtIndice1.Text;
            adocontrat.Rows[0]["Indice2"] = txtIndice2.Text;
            adocontrat.Rows[0]["Indice3"] = txtIndice3.Text;
            adocontrat.Rows[0]["Indice4"] = txtIndice4.Text;
            adocontrat.Rows[0]["Indice5"] = txtIndice5.Text;
            adocontrat.Rows[0]["Indice6"] = txtIndice6.Text;
            if (txtIndice1Valeur.Text != "")
                adocontrat.Rows[0]["Valeur1"] = txtIndice1Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur1"] = DBNull.Value;
            if (txtIndice2Valeur.Text != "")
                adocontrat.Rows[0]["Valeur2"] = txtIndice2Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur2"] = DBNull.Value;
            if (txtIndice3Valeur.Text != "")
                adocontrat.Rows[0]["Valeur3"] = txtIndice3Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur3"] = DBNull.Value;
            if (txtIndice4Valeur.Text != "")
                adocontrat.Rows[0]["Valeur4"] = txtIndice4Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur4"] = DBNull.Value;
            if (txtIndice5Valeur.Text != "")
                adocontrat.Rows[0]["Valeur5"] = txtIndice5Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur5"] = DBNull.Value;
            if (txtIndice6Valeur.Text != "")
                adocontrat.Rows[0]["Valeur5"] = txtIndice6Valeur.Text;
            else
                adocontrat.Rows[0]["Valeur5"] = DBNull.Value;
            if (txtIndice1Date.Text != "")
                adocontrat.Rows[0]["Date1"] = txtIndice1Date.Text;
            else
                adocontrat.Rows[0]["Date1"] = DBNull.Value;
            if (txtIndice2Date.Text != "")
                adocontrat.Rows[0]["Date2"] = txtIndice2Date.Text;
            else
                adocontrat.Rows[0]["Date2"] = DBNull.Value;
            if (txtIndice3Date.Text != "")
                adocontrat.Rows[0]["Date3"] = txtIndice3Date.Text;
            else
                adocontrat.Rows[0]["Date3"] = DBNull.Value;
            if (txtIndice4Date.Text != "")
                adocontrat.Rows[0]["Date4"] = txtIndice4Date.Text;
            else
                adocontrat.Rows[0]["Date4"] = DBNull.Value;
            if (txtIndice5Date.Text != "")
                adocontrat.Rows[0]["Date5"] = txtIndice5Date.Text;
            else
                adocontrat.Rows[0]["Date5"] = DBNull.Value;
            if (txtIndice6Date.Text != "")
                adocontrat.Rows[0]["Date6"] = txtIndice6Date.Text;
            else
                adocontrat.Rows[0]["Date6"] = DBNull.Value;
            if (txtBaseRevisee.Text != "")
                adocontrat.Rows[0]["BaseRevisee"] = txtBaseRevisee.Text;
            else
                adocontrat.Rows[0]["BaseRevisee"] = DBNull.Value;
            adocontrat.Rows[0]["AnalytiqueActivite"] = txtActivite.Text;
            adocontrat.Rows[0]["AnalytiqueImmeuble"] = txtAnalytique.Text;
            if (txtCON_AnaModif.Text != "")
                adocontrat.Rows[0]["CON_AnaModif"] = txtCON_AnaModif.Text;
            else
                adocontrat.Rows[0]["CON_AnaModif"] = DBNull.Value;
            if (txtNomAnalytique.Text != "")
                adocontrat.Rows[0]["LibelleAnaImmeuble"] = txtNomAnalytique.Text;
            else
                adocontrat.Rows[0]["LibelleAnaImmeuble"] = DBNull.Value;
            if (txtCON_Indice1rev.Text != "")
                adocontrat.Rows[0]["CON_Indice1rev"] = txtCON_Indice1rev.Text;
            else
                adocontrat.Rows[0]["CON_Indice1rev"] = DBNull.Value;
            if (txtCON_Indice2rev.Text != "")
                adocontrat.Rows[0]["CON_Indice2rev"] = txtCON_Indice2rev.Text;
            else
                adocontrat.Rows[0]["CON_Indice2rev"] = DBNull.Value;
            if (txtCON_Indice3rev.Text != "")
                adocontrat.Rows[0]["CON_Indice3rev"] = txtCON_Indice3rev.Text;
            else
                adocontrat.Rows[0]["CON_Indice3rev"] = DBNull.Value;
            if (txtCON_Valeur1rev.Text != "")
                adocontrat.Rows[0]["CON_Valeur1rev"] = txtCON_Valeur1rev.Text;
            else
                adocontrat.Rows[0]["CON_Valeur1rev"] = DBNull.Value;
            if (txtCON_Valeur2rev.Text != "")
                adocontrat.Rows[0]["CON_Valeur2rev"] = txtCON_Valeur2rev.Text;
            else
                adocontrat.Rows[0]["CON_Valeur2rev"] = DBNull.Value;
            if (txtCON_Valeur3rev.Text != "")
                adocontrat.Rows[0]["CON_Valeur3rev"] = txtCON_Valeur3rev.Text;
            else
                adocontrat.Rows[0]["CON_Valeur3rev"] = DBNull.Value;
            if (txtCON_Date1rev.Text != "")
                adocontrat.Rows[0]["CON_Date1rev"] = txtCON_Date1rev.Text;
            else
                adocontrat.Rows[0]["CON_Date1rev"] = DBNull.Value;
            if (txtCON_Date2rev.Text != "")
                adocontrat.Rows[0]["CON_Date2rev"] = txtCON_Date2rev.Text;
            else
                adocontrat.Rows[0]["CON_Date2rev"] = DBNull.Value;
            if (txtCON_Date3rev.Text != "")
                adocontrat.Rows[0]["CON_Date3rev"] = txtCON_Date3rev.Text;
            else
                adocontrat.Rows[0]["CON_Date3rev"] = DBNull.Value;
            adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;

        }
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            int i = 0;


            try
            {
                if (e.Tab == SSTab1.Tabs[2])
                {
                    if (blAddnew == true && boolOk == true)
                    {
                        SablierOnOff(true);
                        //pour boolVerifContratAv voir addnew
                        if (boolVerifContratAv == true)
                        {
                            if (string.IsNullOrEmpty(txtNumContrat.Text))
                            {

                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un numéro de contrat et un avenant (par défaut 0) pour saisir dans l'onglet Calendrier", "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtNumContrat.Focus();
                                SSTab1.SelectedTab = SSTab1.Tabs[1];
                                SablierOnOff(false);
                                return;

                            }
                            else if (string.IsNullOrEmpty(txtAvenant.Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un numéro de contrat et un avenant (par défaut 0) pour saisir dans l'onglet Calendrier", "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SSTab1.SelectedTab = SSTab1.Tabs[1];
                                txtAvenant.Focus();
                                SablierOnOff(false);
                                return;

                            }
                            else if (string.IsNullOrEmpty(Text2.Text))
                            {

                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date d'effet avant de saisir le calendrier.", "Action annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SSTab1.SelectedTab = SSTab1.Tabs[0];
                                Text2.Focus();
                                SablierOnOff(false);
                                return;

                            }

                            adors = new DataTable();
                            ModAdo modAdors = new ModAdo();
                            adors = modAdors.fc_OpenRecordSet("Select NumContrat, Avenant From Contrat where Avenant ='" + txtAvenant.Text + "' and NumContrat ='" + txtNumContrat.Text + "'");

                            var _with34 = adors;
                            if (_with34.Rows.Count > 0)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention ce n° de contrat et d'avenant éxiste déja.Veuillez le(s) remplacer afin de poursuivre votre opération ", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                adors.Dispose();

                                adors = null;
                                SablierOnOff(false);
                                txtNumContrat.Focus();
                                return;
                            }

                            adors.Dispose();

                            adors = null;
                            boolVerifContratAv = false;
                        }

                        if (VerifTousLesChamps() == true)
                        {
                            return;
                        }

                        UpdateContratDataTable();
                        //   adocontrat.Update(); verifier
                        SCBadocontrat = new SqlCommandBuilder(SDAadocontrat);
                        SDAadocontrat.Update(adocontrat);


                        boolValidate = true;
                        //voir Annuler
                        adors = new DataTable();
                        temporaire = "";
                        i = 1;
                        while (temporaire != txtNumContrat.Text)
                        {
                            var tmpAdo = new ModAdo();
                            adors = tmpAdo.fc_OpenRecordSet("Select Contrat.NumContrat from contrat where NumContrat ='" + txtNumContrat.Text + "'");
                            temporaire = adors.Rows[0]["NumContrat"] + "";
                            adors.Dispose();
                            i = i + 1;
                            if (i == 1000)
                            {
                                break;
                            }
                        }


                        adors = null;

                        RemplitContrat();

                        boolOk = false;
                        SSOleDBGrid1.Enabled = true;
                        SablierOnOff(false);

                    }
                    else
                    {
                        adors = new DataTable();
                        var tmpAdo = new ModAdo();
                        adors = tmpAdo.fc_OpenRecordSet("Select NumContrat, Avenant from FacCalendrier where NumContrat='" + txtNumContrat.Text + "' and Avenant ='" + txtAvenant.Text + "'");
                        if (adors.Rows.Count == 0)
                        {
                            RemplitContrat(tmpAdo.SDArsAdo);
                            SSOleDBGrid1.Enabled = true;
                        }

                    }
                }

                ///======> Tested
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }

                //======> Tested
                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }

                return;
            }
            //champs primaires doublons
            catch (Exception ex)
            {
                Program.SaveException(ex);
                if (ex.HResult == -2147467259)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTPRIMAIREFAUX, General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    SablierOnOff(false);
                    txtNumContrat.Focus();
                    // Adodc1.Recordset.CancelUpdate();
                    SablierOnOff(false);
                }
                else
                {
                    // MsgBox err.Description, vbCritical, err.Number

                }
            }
            // static_SSTab1_SelectedIndexChanged_PreviousTab = SSTab1.SelectedTab;verifier
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_SelectedTabChanging(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangingEventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text1_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text1.Text) && !(General.IsDate(Text1.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text1.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(Text1.Text) && Text1.Text != Convert.ToDateTime(Text1.Text).ToString())
            {
                Text1.Text = Convert.ToDateTime(Text1.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text2_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text2.Text) && !(General.IsDate(Text2.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text2.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(Text2.Text) && Text2.Text != Convert.ToDateTime(Text2.Text).ToString())
            {
                Text2.Text = Convert.ToDateTime(Text2.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text3_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Text3.Text) && !(General.IsDate(Text3.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text3.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(Text3.Text) && Text3.Text != Convert.ToDateTime(Text3.Text).ToString())
            {
                Text3.Text = Convert.ToDateTime(Text3.Text).ToShortDateString();
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAvenant_Leave(object sender, EventArgs e)
        {
            if (boolFocVerifCtrAv == true)
            {
                adors = new DataTable();
                using (var tmpAdo = new ModAdo())
                {
                    adors = tmpAdo.fc_OpenRecordSet("Select NumContrat, Avenant From Contrat where Avenant ='" + txtAvenant.Text + "' and NumContrat ='" + txtNumContrat.Text + "'");
                    var _with35 = adors;
                    if (_with35.Rows.Count > 0)
                    {
                        boolFocVerifCtrAv = false;
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention ce n° de contrat et d'avenant éxiste déja.Veuillez le(s) remplacer afin de poursuivre votre opération ", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtNumContrat.Focus();
                        // adors.Close();

                        adors = null;
                        return;
                    }

                    adors = null;
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBase_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBase.Text) && !(General.IsDate(txtBase.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBase.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtBase.Text) && txtBase.Text != Convert.ToDateTime(txtBase.Text).ToString())
            {
                txtBase.Text = Convert.ToDateTime(txtBase.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBaseActualisee_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBaseActualisee.Text) && !(General.IsNumeric(txtBaseActualisee.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base actualisée n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBaseActualisee.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBaseContractuelle_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBaseContractuelle.Text) && !(General.IsNumeric(txtBaseContractuelle.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base actualisée n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBaseContractuelle.Focus();
            }
        }

        /// <summary>
        /// Control Is Allways Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtBaseRevisee_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtBaseRevisee.Text) && !(General.IsNumeric(txtBaseRevisee.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base actualisée n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBaseRevisee.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if ((KeyAscii == 13))
            {
                RechercheParCodeArticle(txtCodeArticle.Text);
                //string CodeArticle = null;
                //using (var tmpAdo = new ModAdo())
                //     CodeArticle = tmpAdo.fc_ADOlibelle("select CodeCategorieArticle from FacArticle where CodeArticle='"+ txtCodeArticle .Text+ "'");
                //if (CodeArticle == "P")
                //{
                //RechercheParCodeArticle(txtCodeArticle.Text);
                //}
                //else
                //{
                //    txtCodeArticle.Text = "";
                //    txtDesignation1.Text = "";
                //    txtDesignation2.Text = "";                   
                //    lblAutoliquidation.Visible = false;
                //    lblChaudiereCondo.Visible = false;

                //    cmdCodeArticle_Click(cmdCodeArticle, new EventArgs());
                //}
                //enleve le bip
                KeyAscii = 0;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if ((KeyAscii == 13))
            {
                if (blAddnew == true)
                {
                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        RechercheParImmeuble(txtCodeImmeuble.Text);
                        //enleve le bip
                        KeyAscii = 0;
                    }
                    else
                    {
                        InitialiseImmeubleGerant();
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Coef_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Coef.Text) && !(General.IsNumeric(txtCON_Coef.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le Coefficient fixe doit avoir une valeur numérique.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCON_Coef.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Date1rev_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Date1rev.Text) && !(General.IsDate(txtCON_Date1rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCON_Date1rev.Focus();
                return;
            }
            if (txtCON_Date1rev.Text != Convert.ToDateTime(txtCON_Date1rev.Text).ToString() && !string.IsNullOrEmpty(txtCON_Date1rev.Text))
            {
                txtCON_Date1rev.Text = Convert.ToDateTime(txtCON_Date1rev.Text).ToShortDateString();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Date2rev_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Date2rev.Text) && !(General.IsDate(txtCON_Date2rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCON_Date2rev.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtCON_Date2rev.Text) && txtCON_Date2rev.Text != Convert.ToDateTime(txtCON_Date2rev.Text).ToString())
            {
                txtCON_Date2rev.Text = Convert.ToDateTime(txtCON_Date2rev.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Date3rev_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Date3rev.Text) && !(General.IsDate(txtCON_Date3rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtCON_Date3rev.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtCON_Date3rev.Text) && txtCON_Date3rev.Text != Convert.ToDateTime(txtCON_Date3rev.Text).ToString())
            {
                txtCON_Date3rev.Text = Convert.ToDateTime(txtCON_Date3rev.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Valeur1rev_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Valeur1rev.Text) && !(General.IsNumeric(txtCON_Valeur1rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Valeur2rev_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Valeur2rev.Text) && !(General.IsNumeric(txtCON_Valeur2rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCON_Valeur3rev_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCON_Valeur3rev.Text) && !(General.IsNumeric(txtCON_Valeur3rev.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateActualisee_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateActualisee.Text) && !(General.IsDate(txtDateActualisee.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateActualisee.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtDateActualisee.Text) && txtDateActualisee.Text != Convert.ToDateTime(txtDateActualisee.Text).ToString())
            {
                txtDateActualisee.Text = Convert.ToDateTime(txtDateActualisee.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Controle Is Allways Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDateRevision_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateRevision.Text) && !(General.IsDate(txtDateRevision.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateRevision.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtDateRevision.Text) && txtDateRevision.Text != Convert.ToDateTime(txtDateRevision.Text).ToString())
            {
                txtDateRevision.Text = Convert.ToDateTime(txtDateRevision.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFormule_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if ((KeyAscii == 13))
            {
                txtIndice1.Text = "";
                txtIndice2.Text = "";
                txtIndice3.Text = "";
                txtIndice4.Text = "";
                txtIndice5.Text = "";
                txtIndice6.Text = "";
                RechercheQuotient(txtFormule.Text);
                //enleve le bip
                KeyAscii = 0;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice1Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtDateRevision.Text) && !(General.IsDate(txtDateRevision.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDateRevision.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtDateRevision.Text) && txtDateRevision.Text != Convert.ToDateTime(txtDateRevision.Text).ToString())
            {
                txtDateRevision.Text = Convert.ToDateTime(txtDateRevision.Text).ToShortDateString();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice1Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice1Valeur.Text) && !(General.IsNumeric(txtIndice1Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Testeds
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice2Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice2Date.Text) && !(General.IsDate(txtIndice2Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIndice2Date.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtIndice2Date.Text) && txtIndice2Date.Text != Convert.ToDateTime(txtIndice2Date.Text).ToString())
            {
                txtIndice2Date.Text = Convert.ToDateTime(txtIndice2Date.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice2Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice2Valeur.Text) && !(General.IsNumeric(txtIndice2Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice3Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice3Date.Text) && !(General.IsDate(txtIndice3Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIndice3Date.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtIndice3Date.Text) && txtIndice3Date.Text != Convert.ToDateTime(txtIndice3Date.Text).ToString())
            {
                txtIndice3Date.Text = Convert.ToDateTime(txtIndice3Date.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice3Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice3Valeur.Text) && !(General.IsNumeric(txtIndice3Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice4Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice4Date.Text) && !(General.IsDate(txtIndice4Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIndice4Date.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtIndice4Date.Text) && txtIndice4Date.Text != Convert.ToDateTime(txtIndice4Date.Text).ToString())
            {
                txtIndice4Date.Text = Convert.ToDateTime(txtIndice4Date.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice4Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice4Valeur.Text) && !(General.IsNumeric(txtIndice4Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice5Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice5Date.Text) && !(General.IsDate(txtIndice5Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIndice5Date.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtIndice5Date.Text) && txtIndice5Date.Text != Convert.ToDateTime(txtIndice5Date.Text).ToString())
            {
                txtIndice5Date.Text = Convert.ToDateTime(txtIndice5Date.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice5Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice5Valeur.Text) && !(General.IsNumeric(txtIndice5Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice6Date_Leave(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice6Date.Text) && !(General.IsDate(txtIndice6Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtIndice6Date.Focus();
                return;
            }
            if (!string.IsNullOrEmpty(txtIndice6Date.Text) && txtIndice6Date.Text != Convert.ToDateTime(txtIndice6Date.Text).ToString())
            {
                txtIndice6Date.Text = Convert.ToDateTime(txtIndice6Date.Text).ToShortDateString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIndice6Valeur_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtIndice6Valeur.Text) && !(General.IsNumeric(txtIndice6Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de l'indice1 n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheAnalytique(object strParam)
        {
            //Set adors = New ADODB.Recordset
            //adors.Open "Select Immeuble.Adresse from immeuble where immeuble.CodeImmeuble = '" & strParam & "'", adocnn
            //With adors
            //If .EOF <> True Then
            // txtAnalytique = strParam & ""
            // txtNomAnalytique = !Adresse & ""
            //Else
            //MsgBox "Ce champs n'existe pas.", vbCritical, "Données érronées"
            //txtAnalytique.SetFocus
            //End If
            //End With
            //adors.Close
            //Set adors = Nothing
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibelleCont1_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (txtLibelleCont1.Text.Length >= 50 && KeyAscii != 8 && KeyAscii != 13)
            {
                KeyAscii = 0;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("vous avez dépassé le nombre de caractere autorisé", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                txtLibelleCont1.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibelleCont2_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (txtLibelleCont2.Text.Length >= 50 && KeyAscii != 8 && KeyAscii != 13)
            {
                KeyAscii = 0;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("vous avez dépassé le nombre de caractere autorisé", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Handled = true;
                txtLibelleCont2.Focus();
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNumContrat_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if ((KeyAscii == 13) && blAddnew == false)
            {
                RechercheParContrat(txtNumContrat.Text);
                //enleve le bip
                KeyAscii = 0;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTypeFormule_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if ((KeyAscii == 13))
            {
                if (Check1.CheckState == CheckState.Unchecked && Check2.CheckState == CheckState.Unchecked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cliquez d'abord sur réel ou connu.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtTypeFormule.Text = "";

                }
                RechercheFormule(txtTypeFormule.Text);
                if (!string.IsNullOrEmpty(txtFormule.Text))
                {
                    RechercheQuotient(txtFormule.Text);
                }
                //enleve le bip
                KeyAscii = 0;
                if (!string.IsNullOrEmpty(txtIndice1.Text))
                    txtIndice1Valeur.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheParImmeuble(object strParam)
        {
            adors = new DataTable();
            //delostal
            //adors.Open "SELECT Immeuble.Adresse, Table1.Code1, Table1.Nom, Table1.Adresse1, Table1.CodePostal, Table1.Ville, Immeuble.CodePostal, Immeuble.Ville FROM Table1 INNER JOIN Immeuble ON Table1.Code1 = Immeuble.Code1 Where Immeuble.CodeImmeuble ='" & strParam & "'", adocnn
            // gesten

            SQL = "SELECT Immeuble.Adresse, immeuble.Code1, Table1.Nom, Immeuble.CodePostal," + " Immeuble.Ville, AdresseFact1_IMM, AdresseFact2_IMM, AdresseFact3_IMM" + " , Immeuble.CodePostal_IMM, Immeuble.VilleFact_IMM , CommentaireFacture1," + " CommentaireFacture2, immeuble.RefClient " + " FROM Table1 INNER JOIN Immeuble ON Table1.Code1 = Immeuble.Code1" + " Where Immeuble.CodeImmeuble ='" + strParam + "'";
            var tmpAdo = new ModAdo();
            adors = tmpAdo.fc_OpenRecordSet(SQL);
            var _with36 = adors;
            if (_with36.Rows.Count > 0)
            {
                txtNomImmeuble.Text = _with36.Rows[0]["Adresse"] + "";
                txtSyndic.Text = _with36.Rows[0]["Code1"] + "";
                txtNomSyndic.Text = _with36.Rows[0]["Nom"] + "";
                //-- adresse de facturation.
                txtFactNomImmeuble.Text = _with36.Rows[0]["AdresseFact1_IMM"] + "";
                //txtFactAdresse = !Adresse1 & ""
                //txtFactCP = ![Table1.Codepostal] & ""
                //txtFactVille = ![Table1.Ville] & ""
                txtLibelleFact.Text = _with36.Rows[0]["AdresseFact2_IMM"] + "";
                txtFactAdresse.Text = _with36.Rows[0]["AdresseFact3_IMM"] + "";
                txtFactCP.Text = _with36.Rows[0]["CodePostal_IMM"] + "";
                txtFactVille.Text = _with36.Rows[0]["VilleFact_IMM"] + "";
                txtcommentaire1.Text = _with36.Rows[0]["CommentaireFacture1"] + "";
                txtcommentaire2.Text = _with36.Rows[0]["CommentaireFacture2"] + "";
                txtIntervAdresse.Text = _with36.Rows[0]["Adresse"] + "";
                //txtIntervCP = ![Immeuble.CodePostal] & ""
                //txtIntervVille = ![Immeuble.Ville] & ""
                txtIntervCP.Text = _with36.Rows[0]["CodePostal"] + "";
                txtIntervVille.Text = _with36.Rows[0]["Ville"] + "";
                if (string.IsNullOrEmpty(txtRefGerant.Text))
                {
                    txtRefGerant.Text = _with36.Rows[0]["RefClient"] + "";
                }
                // modif delostal
                //If blAddnew Then
                //    txtAnalytique = strParam & ""
                //    txtNomAnalytique = !Adresse & ""
                //End If
                fc_AnaModif(txtCON_AnaModif.Text);
                fc_TypeRevsion(txtCON_TypeContrat.Text);
                if (txtCON_AnaModif.Text != "1")
                    fc_Secteur(txtCodeImmeuble.Text);
                if (string.IsNullOrEmpty(txtCON_EchuEchoir.Text))
                    txtCON_EchuEchoir.Text = "0";
                fc_TypeEchuEchoir(Convert.ToInt32(txtCON_EchuEchoir.Text));

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce code immeuble n'est pas valide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtNomImmeuble.Text = "";
                txtSyndic.Text = "";
                txtNomSyndic.Text = "";
                txtFactNomImmeuble.Text = "";
                txtFactAdresse.Text = "";
                txtFactCP.Text = "";
                txtFactVille.Text = "";
                txtIntervAdresse.Text = "";
                txtIntervCP.Text = "";
                txtIntervVille.Text = "";
                //txtCodeImmeuble.SetFocus
            }
            //adors.Close();

            adors = null;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <returns></returns>
        private string fc_Secteur(string sCode)
        {
            string functionReturnValue = null;

            DataTable rsSec = default(DataTable);
            sSQL = "";
            // sSql = "SELECT Personnel.SEC_Code " _
            //& " FROM IntervenantImm_INM INNER JOIN Personnel" _
            //& " ON IntervenantImm_INM.intervenant_INM = Personnel.Matricule" _
            //& " Where (((IntervenantImm_INM.Codeimmeuble_IMM) ='" & gFr_DoublerQuote(scode) & "')" _
            //& " And ((IntervenantImm_INM.Nordre_INM) = 1))" _
            //& " ORDER BY IntervenantImm_INM.Noauto"

            sSQL = "select sec_code from immeuble where codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
            rsSec = new DataTable();
            using (ModAdo modAdorsSec = new ModAdo())
            {
                rsSec = modAdorsSec.fc_OpenRecordSet(sSQL);
                if (rsSec.Rows.Count > 0)
                {

                    txtAnalytique.Text = General.nz(rsSec.Rows[0]["SEC_Code"], "").ToString();
                    functionReturnValue = txtAnalytique.Text;
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de secteur.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

            }
            rsSec.Dispose();

            rsSec = null;
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheParContrat(string strParam)
        {
            adors = new DataTable();
            using (var tmpAdo = new ModAdo())
            {
                adors = tmpAdo.fc_OpenRecordSet("Select * from contrat where NumContrat ='" + strParam + "'");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheParCodeArticle(string strParam)
        {
            adors = new DataTable();
            using (var tmpAdo = new ModAdo())
            {
                strCodeImmeuble = "";
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    adors = tmpAdo.fc_OpenRecordSet("select codetva from immeuble where codeimmeuble ='" + txtCodeImmeuble.Text + "'");
                    if (adors.Rows.Count > 0)
                    {
                        strCodeImmeuble = adors.Rows[0]["CodeTVA"] + "";
                    }
                    //  adors.Close();
                }
                adors = tmpAdo.fc_OpenRecordSet("SELECT FacArticle.CG_Num,FacArticle.CG_Num2, FacArticle.CG_INtitule," + " FacArticle.Designation1, FacArticle.Designation2, FacArticle.Ca_Num," + " FacArticle.CA_INTITULE, ACT_Code, AutoLiquidation,  ChaudiereCondensation FROM FacArticle  Where FacArticle.CodeArticle = '" + strParam + "'");
                var _with39 = adors;
                if (_with39.Rows.Count > 0)
                {
                    txtDesignation1.Text = _with39.Rows[0]["Designation1"] + "";
                    txtDesignation2.Text = _with39.Rows[0]["Designation2"] + "";

                    if (General.nz(_with39.Rows[0]["AutoLiquidation"], "").ToString().ToUpper() == Variable.cContratAutoLiquidation.ToUpper().ToString())
                    {
                        lblAutoliquidation.Visible = true;
                    }
                    else
                    {
                        lblAutoliquidation.Visible = false;
                    }

                    if (General.nz(_with39.Rows[0]["ChaudiereCondensation"], 0).ToString() == "1")
                    {
                        lblChaudiereCondo.Visible = true;
                    }
                    else
                    {
                        lblChaudiereCondo.Visible = false;
                    }

                    //If LTrim(!CG_Num) = (!CG_Num2) Then
                    //    txtCGNUM = !CG_Num & ""
                    //ElseIf strCodeImmeuble = "TR" Then
                    //    txtCGNUM = !CG_Num & ""
                    //ElseIf strCodeImmeuble = "TP" Then
                    //    txtCGNUM = !CG_Num2 & ""
                    //End If
                    //txtCGINTITULE = !CG_INTITULE & ""
                    //txtActivite = !CA_NUM & ""
                    //txtLibelleActivite = !CA_INTITULE & ""
                    // If txtCON_AnaModif <> "1" Then
                    //     If nz(!ACT_Code, "") = "" Then
                    //         MsgBox "Attention cette prestation n'a pas de code analytique d'activité.", vbCritical, ""
                    //     Else
                    //         txtActivite = !ACT_Code & ""
                    //     End If
                    // End If
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce code article n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtDesignation1.Text = "";
                    txtDesignation2.Text = "";
                    txtCGNUM.Text = "";
                    txtCGINITULE.Text = "";
                    txtActivite.Text = "";
                    txtLibelleActivite.Text = "";
                    txtActivite.Text = "";
                    lblAutoliquidation.Visible = false;
                    //txtCodeArticle.SetFocus
                }
                //adors.Close();
                adors = null;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheQuotient(string strParam)
        {
            indi1 = 0;
            indi2 = 1;
            Tabe = new string[indi1 + 1];
            strFormule = "";
            //Decoupe la formule en enlevant les espaces du méme coup
            //===================> Tested
            while (indi2 <= strParam.Length)
            {
                //If Mid(strFormule, indi2, 1) = "=" Then

                switch (General.Mid(strParam, indi2, 1))
                {
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                    case "(":
                    case ")":
                    case "=":
                        indi1 = indi1 + 1;
                        Array.Resize(ref Tabe, indi1 + 1);
                        Tabe[indi1] = Tabe[indi1] + General.Mid(strParam, indi2, 1);
                        indi1 = indi1 + 1;
                        Array.Resize(ref Tabe, indi1 + 1);
                        break;
                    case " ":
                        break;
                    default:
                        Tabe[indi1] = Tabe[indi1] + General.Mid(strParam, indi2, 1);
                        break;
                }
                indi2 = indi2 + 1;
            }

            indi1 = 0;
            txtIndice1.Text = "";
            txtIndice1Date.Text = "";
            txtIndice1Valeur.Text = "";
            txtIndice2.Text = "";
            txtIndice2Date.Text = "";
            txtIndice2Valeur.Text = "";
            txtIndice3.Text = "";
            txtIndice3Date.Text = "";
            txtIndice3Valeur.Text = "";
            //== modif rachid du 14 septembre 2005
            txtIndice4.Text = "";
            txtIndice4Date.Text = "";
            txtIndice4Valeur.Text = "";
            txtIndice5.Text = "";
            txtIndice5Date.Text = "";
            txtIndice5Valeur.Text = "";
            txtIndice6.Text = "";
            txtIndice6Date.Text = "";
            txtIndice6Valeur.Text = "";

            for (indi2 = 0; indi2 < Tabe.Length; indi2++)
            {
                switch (Tabe[indi1])
                {
                    case "+":
                    case "-":
                    case "*":
                    case "(":
                    case ")":
                    case "=":
                        break;
                    case "/":

                        if (!string.IsNullOrEmpty(txtIndice5.Text) && string.IsNullOrEmpty(txtIndice6.Text))
                            txtIndice6.Text = Tabe[indi1 + 1];
                        if (!string.IsNullOrEmpty(txtIndice4.Text) && string.IsNullOrEmpty(txtIndice5.Text))
                            txtIndice5.Text = Tabe[indi1 + 1];
                        if (!string.IsNullOrEmpty(txtIndice3.Text) && string.IsNullOrEmpty(txtIndice4.Text))
                            txtIndice4.Text = Tabe[indi1 + 1];

                        if (!string.IsNullOrEmpty(txtIndice2.Text) && string.IsNullOrEmpty(txtIndice3.Text))
                            txtIndice3.Text = Tabe[indi1 + 1];
                        if (!string.IsNullOrEmpty(txtIndice1.Text) && string.IsNullOrEmpty(txtIndice2.Text))
                            txtIndice2.Text = Tabe[indi1 + 1];
                        ///===================> Tested
                        if (string.IsNullOrEmpty(txtIndice1.Text))
                            txtIndice1.Text = Tabe[indi1 + 1];
                        break;

                    default:
                        if (Tabe[indi1] != null && VerifieNumText(Tabe[indi1]) == false)
                        {
                            adors = new DataTable();
                            using (var tmpAdo = new ModAdo())
                            {

                                if (Check1.CheckState == CheckState.Checked)
                                {
                                    if (General.Right(Tabe[indi1], 1) == "o")
                                    {

                                        sSQL = "SELECT  FacIndice.Valeur From FacIndice " + " Where  FacIndice.Reel = " + General.fc_bln(true) + "And FacIndice.Libelle ='" + General.Left(Tabe[indi1], Tabe[indi1].Length - 1) + "'";
                                        adors = tmpAdo.fc_OpenRecordSet(sSQL);
                                        if (adors.Rows.Count > 0)
                                            Tabe[indi1] = adors.Rows[0]["valeur"].ToString();
                                    }
                                    else
                                    {

                                        sSQL = "SELECT  FacIndice.Valeur From FacIndice" + " Where  FacIndice.Reel =" + General.fc_bln(true) + " And FacIndice.Libelle ='" + Tabe[indi1] + "'";
                                        adors = tmpAdo.fc_OpenRecordSet(sSQL);
                                        if (adors.Rows.Count > 0)
                                            Tabe[indi1] = adors.Rows[0]["valeur"].ToString();
                                    }
                                }
                                else if (Check2.CheckState == CheckState.Checked)
                                {
                                    ///==========================> Tested
                                    if (General.Right(Tabe[indi1], 1) == "o")
                                    {
                                        adors = tmpAdo.fc_OpenRecordSet("SELECT  FacIndice.Valeur From FacIndice" + " Where  FacIndice.Connu =" + General.fc_bln(true) + " And FacIndice.Libelle ='" + General.Left(Tabe[indi1], Tabe[indi1].Length - 1) + "'");
                                        if (adors.Rows.Count > 0)
                                            Tabe[indi1] = adors.Rows[0]["valeur"].ToString();
                                    }
                                    else
                                    {
                                        adors = tmpAdo.fc_OpenRecordSet("SELECT  FacIndice.Valeur From FacIndice" + " Where  FacIndice.Connu =" + General.fc_bln(true) + " And FacIndice.Libelle ='" + Tabe[indi1] + "'");
                                        if (adors.Rows.Count > 0)
                                            Tabe[indi1] = adors.Rows[0]["valeur"].ToString();
                                    }
                                }
                            }
                            // adors.Close();

                        }
                        break;
                }
                strFormule = strFormule + Tabe[indi1];
                indi1 = indi1 + 1;
            }

            adors = null;
            ///===================> Tested
            if (string.IsNullOrEmpty(txtIndice1.Text))
            {
                txtIndice1.Enabled = false;
                txtIndice1Date.Enabled = false;
                txtIndice1Valeur.Enabled = false;
                cmdIndice1.Enabled = false;
            }
            else
            {
                // txtIndice1.Enabled = True
                txtIndice1Date.Enabled = true;
                txtIndice1Valeur.Enabled = true;
                cmdIndice1.Enabled = true;
            }

            ///====================> Tested
            if (string.IsNullOrEmpty(txtIndice2.Text))
            {
                txtIndice2.Enabled = false;
                txtIndice2Date.Enabled = false;
                txtIndice2Valeur.Enabled = false;
                cmdIndice2.Enabled = false;
            }
            else
            {
                //txtIndice2.Enabled = True
                txtIndice2Date.Enabled = true;
                txtIndice2Valeur.Enabled = true;
                cmdIndice2.Enabled = true;
            }

            ///===================> Tested
            if (string.IsNullOrEmpty(txtIndice3.Text))
            {
                txtIndice3.Enabled = false;
                txtIndice3Date.Enabled = false;
                txtIndice3Valeur.Enabled = false;
                cmdIndice3.Enabled = false;
            }
            else
            {
                txtIndice3Date.Enabled = true;
                txtIndice3Valeur.Enabled = true;
                cmdIndice3.Enabled = true;
            }

            ///======================> Tested
            //== modif rachid ajout de 3 indices
            if (string.IsNullOrEmpty(txtIndice4.Text))
            {
                txtIndice4.Enabled = false;
                txtIndice4Date.Enabled = false;
                txtIndice4Valeur.Enabled = false;
                cmdIndice4.Enabled = false;
            }
            else
            {
                txtIndice4Date.Enabled = true;
                txtIndice4Valeur.Enabled = true;
                cmdIndice4.Enabled = true;
            }

            ///=================> Tested
            if (string.IsNullOrEmpty(txtIndice5.Text))
            {
                txtIndice5.Enabled = false;
                txtIndice5Date.Enabled = false;
                txtIndice5Valeur.Enabled = false;
                cmdIndice5.Enabled = false;
            }
            else
            {
                txtIndice5Date.Enabled = true;
                txtIndice5Valeur.Enabled = true;
                cmdIndice5.Enabled = true;
            }

            ///==================> Testeds
            if (string.IsNullOrEmpty(txtIndice6.Text))
            {
                txtIndice6.Enabled = false;
                txtIndice6Date.Enabled = false;
                txtIndice6Valeur.Enabled = false;
                cmdIndice6.Enabled = false;
            }
            else
            {
                txtIndice6Date.Enabled = true;
                txtIndice6Valeur.Enabled = true;
                cmdIndice6.Enabled = true;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Temp"></param>
        /// <returns></returns>
        private bool VerifieNumText(string Temp)
        {
            bool functionReturnValue = false;
            indi3 = 1;
            if (General.IsNumeric(Temp))
            {
                functionReturnValue = true;
            }
            while (indi3 < Temp.Length)
            {
                if (General.Mid(Temp, indi3, 1) == "F")
                {
                    functionReturnValue = false;
                    break;
                }
                indi3 = indi3 + 1;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void SynchroCalendrier()
        {
            string sRatio = null;


            //Adodc2.RecordSource = "SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' And FacCalendrier.Avenant ='" + txtAvenant.Text + "'  order by histo desc ,Echeance"
            Adodc2 = new DataTable();
            SQL = "SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' And FacCalendrier.Avenant ='" + txtAvenant.Text + "'  order by histo desc ,Echeance";
            SDAAdodc2 = new SqlDataAdapter(SQL, General.adocnn);
            SDAAdodc2.Fill(Adodc2);
            SSOleDBGrid1.DataSource = Adodc2;
            //Adodc2.RecordSource = "SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" & txtNumContrat & "' And FacCalendrier.Avenant ='" & txtAvenant & "'"

            //  SSOleDBGrid1.CtlRefresh();
            //== le code ci dessous est une astuce pour debloquer la grille(bug)
            if (SSOleDBGrid1.Rows.Count > 0)
            {
                sRatio = SSOleDBGrid1.Rows[0].Cells["ratio"].Value.ToString();
                SSOleDBGrid1.Rows[0].Cells["ratio"].Value = "test";
                SSOleDBGrid1.UpdateData();
                SSOleDBGrid1.Rows[0].Cells["ratio"].Value = sRatio;
                SSOleDBGrid1.UpdateData();
            }
            //    Adodc2.Recordset.MoveNext
            //    Adodc2.Recordset.MovePrevious
            //    SSOleDBGrid1.MoveNext
            //    SSOleDBGrid1.MovePrevious

            // SSOleDBGrid1.CtlUpdate(); 
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_clearSuivi()
        {

            txtANCC_CommACCEPTE.Text = "";
            txtANCC_CommRESILIE.Text = "";
            chkANCC_Reconduction.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkANCC_RenovPerdue.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtANCC_DateReceptResil.Text = "";

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fcMajSuivi()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            int lCON_Noauto = 0;
            try
            {
                sSQL = "SELECT      Codeimmeuble,  NumContrat,  ANCC_CommACCEPTE,"
                    + " ANCC_CommRESILIE, ANCC_Reconduction, ANCC_RenovPerdue,ANCC_DateReceptResil "
                    + " From CONT_ContratComment" + " WHERE   NumContrat = '"
                    + StdSQLchaine.gFr_DoublerQuote(txtNumContrat.Text) + "'"
                    + " and codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

                // & " AND Codeimmeuble = '" & gFr_DoublerQuote(txtCodeImmeuble) & "'"
                using (var tmpAdo = new ModAdo())
                {
                    rs = tmpAdo.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count == 0)
                    {
                        //rs.AddNew();verifier
                        rs.Rows.Add(rs.NewRow());
                        sSQL = "SELECT CON_Noauto FROM contrat WHERE NumContrat ='" + StdSQLchaine.gFr_DoublerQuote(txtNumContrat.Text) + "' and  Avenant =" + General.nz(txtAvenant.Text, 0);
                        using (var tmp = new ModAdo())
                        {
                            sSQL = tmp.fc_ADOlibelle(sSQL);
                            lCON_Noauto = Convert.ToUInt16(General.nz(sSQL, 0));
                        }

                    }

                    rs.Rows[0]["ANCC_CommACCEPTE"] = txtANCC_CommACCEPTE.Text;
                    rs.Rows[0]["ANCC_CommRESILIE"] = txtANCC_CommRESILIE.Text;
                    rs.Rows[0]["ANCC_Reconduction"] = chkANCC_Reconduction.CheckState;
                    rs.Rows[0]["ANCC_RenovPerdue"] = chkANCC_RenovPerdue.CheckState;
                    rs.Rows[0]["CodeImmeuble"] = txtCodeImmeuble.Text;
                    rs.Rows[0]["NumContrat"] = txtNumContrat.Text;
                    rs.Rows[0]["ANCC_DateReceptResil"] = General.nz(txtANCC_DateReceptResil.Text, System.DBNull.Value);
                    //rs!Avenant = nz(txtAvenant, 0)
                    //rs!CON_Noauto = lCON_Noauto
                    int update = tmpAdo.Update();
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fcMajSuivi");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadSuivi()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);

            sSQL = "SELECT      Codeimmeuble,  NumContrat, ANCC_CommACCEPTE,"
                + " ANCC_CommRESILIE, ANCC_Reconduction, ANCC_RenovPerdue, ANCC_DateReceptResil "
                + " From CONT_ContratComment" + " WHERE   NumContrat = '"
                + StdSQLchaine.gFr_DoublerQuote(txtNumContrat.Text) + "'"
                + " and codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            // & " AND Codeimmeuble = '" & gFr_DoublerQuote(txtCodeImmeuble) & "'"
            try
            {
                using (ModAdo modadors = new ModAdo())
                {
                    rs = modadors.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count > 0)
                    {

                        txtANCC_CommACCEPTE.Text = rs.Rows[0]["ANCC_CommACCEPTE"] + "";
                        txtANCC_CommRESILIE.Text = rs.Rows[0]["ANCC_CommRESILIE"] + "";
                        chkANCC_Reconduction.Checked = General.nz(rs.Rows[0]["ANCC_Reconduction"], 0).ToString() == "1";
                        chkANCC_RenovPerdue.Checked = General.nz(rs.Rows[0]["ANCC_RenovPerdue"], 0).ToString() == "1";
                        txtANCC_DateReceptResil.Text = rs.Rows[0]["ANCC_DateReceptResil"] + "";
                    }
                    else
                    {
                        fc_clearSuivi();

                    }

                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadSuivi");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void RemplitContrat(SqlDataAdapter sda = null)
        {
            var _with48 = SSOleDBGrid1;
            indi1 = 1;
            Adodc2 = new DataTable();
            Adodc2.Columns.Add("Echeance", typeof(int));
            Adodc2.Columns.Add("Date", typeof(DateTime));
            Adodc2.Columns.Add("Facture");
            Adodc2.Columns.Add("NoFacture");
            Adodc2.Columns.Add("Ratio", typeof(string));
            Adodc2.Columns.Add("PCdu", typeof(DateTime));
            Adodc2.Columns.Add("Au", typeof(DateTime));
            Adodc2.Columns.Add("TypeCalcul");
            Adodc2.Columns.Add("TypeRevision");
            Adodc2.Columns.Add("NumContrat");
            Adodc2.Columns.Add("Avenant");
            Adodc2.Columns.Add("FacEssai");
            Adodc2.Columns.Add("DateFacture", typeof(DateTime));
            Adodc2.Columns.Add("NoFactureTemp");
            Adodc2.Columns.Add("BaseNoFacture");
            Adodc2.Columns.Add("nonFacturable");
            Adodc2.Columns.Add("BaseNoFactureTemp");
            Adodc2.Columns.Add("histo");
            Adodc2.Columns.Add("Resiliee");
            Adodc2.Columns.Add("coef");
            Adodc2.Columns.Add("Fac_SansRevision", typeof(int));
            Adodc2.Columns.Add("Fac_MoisIndice", typeof(int));
            Adodc2.Columns.Add("AEffacerNofacture");

            while (indi1 != 14)
            {
                var newRow = Adodc2.NewRow();
                newRow["NumContrat"] = txtNumContrat.Text;
                if (string.IsNullOrEmpty(txtAvenant.Text))
                {
                    newRow[10] = "0";
                }
                else
                {
                    newRow[10] = txtAvenant.Text;
                }
                newRow[0] = indi1;

                Adodc2.Rows.Add(newRow.ItemArray);

                SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
                var xx = SDAAdodc2.Update(Adodc2);
                indi1++;
            }
            Adodc2 = new DataTable();
            SDAAdodc2 = new SqlDataAdapter("SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' And FacCalendrier.Avenant ='" + txtAvenant.Text + "' order by histo, FacEssai", General.adocnn);
            SDAAdodc2.Fill(Adodc2);
            SSOleDBGrid1.DataSource = Adodc2;
            /*   while (indi1 != 14) 
               {
                   _with48.AddNew();
                   _with48.Columns(9).value = txtNumContrat;
                   if (string.IsNullOrEmpty(txtAvenant.Text))
                   {
                       _with48.Columns(10).value = "0";
                   }
                   else
                   {
                       _with48.Columns(10).value = txtAvenant;
                       //'c''
                   }
                   _with48.Columns(0).value = indi1;

                   _with48.CtlUpdate();
                   indi1 = indi1 + 1;
               }

               Adodc2.RecordSource = "SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' And FacCalendrier.Avenant ='" + txtAvenant.Text + "' order by histo, FacEssai";
               Adodc2.Refresh();

       */
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void TableContratVide(Control C)
        {
            cmdAnnuler.Enabled = false;
            cmdSupprimer.Enabled = false;
            cmdMAJ.Enabled = false;
            SSOleDBGrid1.Enabled = false;
            cmdImprimer.Enabled = false;

            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    TableContratVide(CC);

            if (C is iTalk.iTalk_TextBox_Small2)
            {
                C.Text = "";
                C.Enabled = false;
            }


            //foreach (Control ControleSurFeuille_loopVariable in this.Controls)
            //{
            //   ControleSurFeuille = ControleSurFeuille_loopVariable;

            //    if (ControleSurFeuille is System.Windows.Forms.TextBox)
            //    {
            //        ControleSurFeuille.Text = "";
            //        ControleSurFeuille.Enabled = false;
            //    }
            //}
            //boolTableContratVide = true;

            fc_clearSuivi();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void TableContratActive(Control C)
        {

            cmdAnnuler.Enabled = true;
            cmdSupprimer.Enabled = true;
            cmdMAJ.Enabled = true;
            SSOleDBGrid1.Enabled = true;
            cmdImprimer.Enabled = true;

            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    TableContratActive(CC);
            if (C is iTalk.iTalk_TextBox_Small2)
            {
                // C.Text = "";
                C.Enabled = true;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Sablier"></param>
        public void SablierOnOff(bool Sablier)
        {
            /// transforme le pointeur en sabier si fleche et inversement
            System.Windows.Forms.Application.DoEvents();
            if (Sablier)
            {
                //FrmMain.MousePointer = 11
                this.Cursor = System.Windows.Forms.Cursors.WaitCursor;
            }
            else
            {
                //FrmMain.MousePointer = 1
                this.Cursor = System.Windows.Forms.Cursors.Arrow;
            }
            System.Windows.Forms.Application.DoEvents();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strParam"></param>
        private void RechercheFormule(string strParam)
        {
            adors = new DataTable();
            using (var tmpAdo = new ModAdo())
            {
                adors = tmpAdo.fc_OpenRecordSet("Select FacFormule.Libelle from FacFormule where FacFormule.Type = '" + strParam + "'");
                var _with49 = adors;
                ///========> Tested First
                if (_with49.Rows.Count > 0)
                {
                    txtFormule.Text = adors.Rows[0]["Libelle"] + "";
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce code de formule n'est pas valide", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTypeFormule.Focus();
                    txtFormule.Text = "";
                    if (!string.IsNullOrEmpty(txtIndice1.Text))
                    {
                        txtIndice1.Text = "";
                        txtIndice1Valeur.Text = "";
                        txtIndice1Date.Text = "";
                    }
                    if (!string.IsNullOrEmpty(txtIndice2.Text))
                    {
                        txtIndice2.Text = "";
                        txtIndice2Valeur.Text = "";
                        txtIndice2Date.Text = "";
                    }
                    if (!string.IsNullOrEmpty(txtIndice3.Text))
                    {
                        txtIndice3.Text = "";
                        txtIndice3Valeur.Text = "";
                        txtIndice3Date.Text = "";
                    }
                    if (!string.IsNullOrEmpty(txtIndice4.Text))
                    {
                        txtIndice4.Text = "";
                        txtIndice4Valeur.Text = "";
                        txtIndice4Date.Text = "";
                    }
                    if (!string.IsNullOrEmpty(txtIndice5.Text))
                    {
                        txtIndice5.Text = "";
                        txtIndice5Valeur.Text = "";
                        txtIndice5Date.Text = "";
                    }
                    if (!string.IsNullOrEmpty(txtIndice6.Text))
                    {
                        txtIndice6.Text = "";
                        txtIndice6Valeur.Text = "";
                        txtIndice6Date.Text = "";
                    }

                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool VerifTousLesChamps()
        {
            bool functionReturnValue = false;

            if (!string.IsNullOrEmpty(Text1.Text) && !(General.IsDate(Text1.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text1.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(Text1.Text) && Text1.Text != Convert.ToDateTime(Text1.Text).ToString())
            {
                Text1.Text = Convert.ToDateTime(Text1.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(Text2.Text) && !(General.IsDate(Text2.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text2.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(Text2.Text) && Text2.Text != Convert.ToDateTime(Text2.Text).ToString())
            {
                Text2.Text = Convert.ToDateTime(Text2.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(Text3.Text) && !(General.IsDate(Text3.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text3.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(Text3.Text) && Text3.Text != Convert.ToDateTime(Text3.Text).ToString())
            {
                Text3.Text = Convert.ToDateTime(Text3.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtBaseContractuelle.Text) && !(General.IsNumeric(txtBaseContractuelle.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base contractuelle n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);

                functionReturnValue = true;
                txtBaseContractuelle.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtBaseRevisee.Text) && !(General.IsNumeric(txtBaseRevisee.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base contractuelle n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtBaseRevisee.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtBaseActualisee.Text) && !(General.IsNumeric(txtBaseActualisee.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de la base contractuelle n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtBaseActualisee.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtBase.Text) && !(General.IsDate(txtBase.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBase.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtBase.Text) && txtBase.Text != Convert.ToDateTime(txtBase.Text).ToString())
            {
                txtBase.Text = Convert.ToDateTime(txtBase.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtDateRevision.Text) && !(General.IsDate(txtDateRevision.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBase.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }


            if (!string.IsNullOrEmpty(txtDateRevision.Text) && txtDateRevision.Text != Convert.ToDateTime(txtDateRevision.Text).ToString())
            {
                txtDateRevision.Text = Convert.ToDateTime(txtDateRevision.Text).ToShortDateString();
            }

            if (!(General.IsDate(txtDateActualisee.Text)) && !string.IsNullOrEmpty(txtDateActualisee.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtBase.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtDateActualisee.Text) && txtDateActualisee.Text != Convert.ToDateTime(txtDateActualisee.Text).ToString())
            {
                txtDateActualisee.Text = Convert.ToDateTime(txtDateActualisee.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtIndice1Valeur.Text) && !(General.IsNumeric(txtIndice1Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice1  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice1Valeur.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtIndice3Valeur.Text) && !(General.IsNumeric(txtIndice2Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice2  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice2Valeur.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtIndice3Valeur.Text) && !(General.IsNumeric(txtIndice3Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice3  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice3Valeur.Focus();
                return functionReturnValue;
            }

            //== modif rachid du 14 septembre 2005
            if (!string.IsNullOrEmpty(txtIndice4Valeur.Text) && !(General.IsNumeric(txtIndice4Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice4  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice4Valeur.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtIndice5Valeur.Text) && !(General.IsNumeric(txtIndice5Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice5  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice5Valeur.Focus();
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtIndice6Valeur.Text) && !(General.IsNumeric(txtIndice6Valeur.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur de  de l'indice6  n'est pas une valeur numerique.Saisisez un point ou une virgule selon vos paramétres régionaux sous windows.", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice6Valeur.Focus();
                return functionReturnValue;
            }



            if (!string.IsNullOrEmpty(txtIndice1Date.Text) && !(General.IsDate(txtIndice1Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice1Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice1Date.Text) && txtIndice1Date.Text != Convert.ToDateTime(txtIndice1Date.Text).ToString())
            {
                txtIndice1Date.Text = Convert.ToDateTime(txtIndice1Date.Text).ToShortDateString();
            }
            if (!string.IsNullOrEmpty(txtIndice2Date.Text) && !(General.IsDate(txtIndice2Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice2Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice2Date.Text) && txtIndice2Date.Text != Convert.ToDateTime(txtIndice2Date.Text).ToString())
            {
                txtIndice2Date.Text = Convert.ToDateTime(txtIndice2Date.Text).ToShortDateString();
            }
            if (!string.IsNullOrEmpty(txtIndice3Date.Text) && !(General.IsDate(txtIndice3Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice3Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice3Date.Text) && txtIndice3Date.Text != Convert.ToDateTime(txtIndice3Date.Text).ToString())
            {
                txtIndice3Date.Text = Convert.ToDateTime(txtIndice3Date.Text).ToShortDateString();
            }
            //== modif rachid du 14 sep 2005
            if (!string.IsNullOrEmpty(txtIndice4Date.Text) && !(General.IsDate(txtIndice4Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice4Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice4Date.Text) && txtIndice4Date.Text != Convert.ToDateTime(txtIndice4Date.Text).ToString())
            {
                txtIndice4Date.Text = Convert.ToDateTime(txtIndice4Date.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtIndice5Date.Text) && !(General.IsDate(txtIndice5Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice5Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice5Date.Text) && txtIndice5Date.Text != Convert.ToDateTime(txtIndice5Date.Text).ToString())
            {
                txtIndice5Date.Text = Convert.ToDateTime(txtIndice5Date.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtIndice6Date.Text) && !(General.IsDate(txtIndice6Date.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                functionReturnValue = true;
                txtIndice6Date.Focus();
                return functionReturnValue;
            }
            if (!string.IsNullOrEmpty(txtIndice6Date.Text) && txtIndice6Date.Text != Convert.ToDateTime(txtIndice6Date.Text).ToString())
            {
                txtIndice6Date.Text = Convert.ToDateTime(txtIndice6Date.Text).ToShortDateString();
            }

            if (!string.IsNullOrEmpty(txtANCC_DateReceptResil.Text) && !(General.IsDate(txtANCC_DateReceptResil.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("la date réception résiliation est invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                SSTab1.SelectedTab = SSTab1.Tabs[3];
                txtANCC_DateReceptResil.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }
            return functionReturnValue;

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void InitialiseImmeubleGerant()
        {
            //si le code immeuble n'existe pas
            txtNomImmeuble.Text = "";
            txtSyndic.Text = "";
            txtNomSyndic.Text = "";
            txtFactNomImmeuble.Text = "";
            txtLibelleFact.Text = "";
            txtFactAdresse.Text = "";
            txtFactCP.Text = "";
            txtFactVille.Text = "";
            txtIntervAdresse.Text = "";
            txtLibelleInterv1.Text = "";
            txtLibelleInterv2.Text = "";
            txtIntervCP.Text = "";
            txtIntervVille.Text = "";
            if (blAddnew == true)
            {
                txtAnalytique.Text = "";
                txtNomAnalytique.Text = "";
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void InitialiseArticle()
        {
            txtDesignation1.Text = "";
            txtDesignation2.Text = "";
            txtCGNUM.Text = "";
            txtCGINITULE.Text = "";
            txtActivite.Text = "";
            txtLibelleActivite.Text = "";
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void AnnuleModif()
        {
            //verifier 
            txtNumContrat.Text = "";
            txtNumContrat.Text = "";
            txtAvenant.Text = "";
            txtLibelleCont1.Text = "";
            txtLibelleCont2.Text = "";
            txtCodeImmeuble.Text = "";
            Text1.Text = "";
            Text2.Text = "";
            Text3.Text = "";
            txtCodeArticle.Text = "";
            txtBaseContractuelle.Text = "";
            txtBase.Text = "";
            txtBaseRevisee.Text = "";
            txtDateRevision.Text = "";
            txtBaseActualisee.Text = "";
            txtDateActualisee.Text = "";
            txtTypeFormule.Text = "";
            txtIndice1.Text = "";
            txtIndice2.Text = "";
            txtIndice3.Text = "";
            //== modif rachid ajout de 3 indices.
            txtIndice4.Text = "";
            txtIndice5.Text = "";
            txtIndice6.Text = "";
            txtIndice1Valeur.Text = "";
            txtIndice2Valeur.Text = "";
            txtIndice3Valeur.Text = "";
            //== modif rachid ajout de 3 indices.
            txtIndice4Valeur.Text = "";
            txtIndice5Valeur.Text = "";
            txtIndice6Valeur.Text = "";
            txtIndice1Date.Text = "";
            txtIndice2Date.Text = "";
            txtIndice3Date.Text = "";
            //== modif rachid ajout de 3 indices.
            txtIndice4Date.Text = "";
            txtIndice5Date.Text = "";
            txtIndice6Date.Text = "";
            txtAnalytique.Text = "";
            txtNomAnalytique.Text = "";
            /*  txtNumContrat.DataChanged = false;
              txtAvenant.DataChanged = false;
              txtLibelleCont1.DataChanged = false;
              txtLibelleCont2.DataChanged = false;
              txtCodeImmeuble.DataChanged = false;
              Text1.DataChanged = false;
              Text2.DataChanged = false;
              Text3.DataChanged = false;
              txtCodeArticle.DataChanged = false;
              txtBaseContractuelle.DataChanged = false;
              txtBase.DataChanged = false;
              txtBaseRevisee.DataChanged = false;
              txtDateRevision.DataChanged = false;
              txtBaseActualisee.DataChanged = false;
              txtDateActualisee.DataChanged = false;
              txtTypeFormule.DataChanged = false;
              txtIndice1.DataChanged = false;
              txtIndice2.DataChanged = false;
              txtIndice3.DataChanged = false;
              //== modif rachid ajout de 3 indices.
              txtIndice4.DataChanged = false;
              txtIndice5.DataChanged = false;
              txtIndice6.DataChanged = false;
              txtIndice1Valeur.DataChanged = false;
              txtIndice2Valeur.DataChanged = false;
              txtIndice3Valeur.DataChanged = false;
              //== modif rachid ajout de 3 indices.
              txtIndice4Valeur.DataChanged = false;
              txtIndice5Valeur.DataChanged = false;
              txtIndice6Valeur.DataChanged = false;
              txtIndice1Date.DataChanged = false;
              txtIndice2Date.DataChanged = false;
              txtIndice3Date.DataChanged = false;
              //== modif rachid ajout de 3 indices.
              txtIndice4Date.DataChanged = false;
              txtIndice5Date.DataChanged = false;
              txtIndice6Date.DataChanged = false;
              txtAnalytique.DataChanged = false;
              txtNomAnalytique.DataChanged = false;*/
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lType"></param>
        private void fc_TypeRevsion(string lType)
        {
            var _with51 = this;
            txtCON_Coef.Visible = false;
            fraFormule.Visible = false;

            switch (lType)
            {
                case "0":
                    _with51.optRevCoefFixe.Checked = false;
                    _with51.optRevFormule.Checked = false;
                    optSansRev.Checked = false;

                    adocontrat.Rows[0]["CON_TypeContrat"] = General.nz(txtCON_TypeContrat.Text, System.DBNull.Value);
                    break;
                case "1":
                    txtCON_Coef.Visible = true;
                    txtCON_TypeContrat.Text = "1";
                    adocontrat.Rows[0]["CON_TypeContrat"] = txtCON_TypeContrat.Text;
                    if (_with51.optRevCoefFixe.Checked == false)
                        _with51.optRevCoefFixe.Checked = true;
                    break;
                case "2":
                    ////========> Tested
                    txtCON_TypeContrat.Text = "2";
                    adocontrat.Rows[0]["CON_TypeContrat"] = txtCON_TypeContrat.Text;
                    fraFormule.Visible = true;
                    if (_with51.optRevFormule.Checked == false)
                        _with51.optRevFormule.Checked = true;
                    break;
                case "3":
                    txtCON_TypeContrat.Text = "3";
                    adocontrat.Rows[0]["CON_TypeContrat"] = txtCON_TypeContrat.Text;
                    if (optSansRev.Checked == false)
                        optSansRev.Checked = true;
                    break;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lType"></param>
        private void fc_TypeEchuEchoir(int lType)
        {
            if (SSOleDBGrid1.DataSource != null)
            {
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCDu"].CellActivation = Activation.ActivateOnly;
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellActivation = Activation.ActivateOnly;
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCDu"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
            }
            switch (lType)
            {
                case 0:
                    Option6.Checked = false;
                    Option7.Checked = false;
                    txtCON_EchuEchoir.Text = Convert.ToString(0);
                    adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;
                    break;
                case 1:
                    // echu

                    txtCON_EchuEchoir.Text = Convert.ToString(1);
                    adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;
                    Option6.Checked = true;
                    break;
                case 2:
                    ////===============> Tested
                    // echoir
                    txtCON_EchuEchoir.Text = Convert.ToString(2);
                    adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;
                    Option7.Checked = true;
                    break;
                case 3:
                    // autre
                    txtCON_EchuEchoir.Text = Convert.ToString(3);
                    adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;
                    optAutre.Checked = true;

                    if (SSOleDBGrid1.DataSource != null)
                    {
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCDu"].CellActivation = Activation.AllowEdit;
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellActivation = Activation.AllowEdit;
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCDu"].CellAppearance.BackColor = ColorTranslator.FromOle(0xffffff);
                        SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellAppearance.BackColor = ColorTranslator.FromOle(0xffffff);
                    }
                    break;
                case 4:
                    txtCON_EchuEchoir.Text = Convert.ToString(4);
                    adocontrat.Rows[0]["CON_EchuEchoir"] = txtCON_EchuEchoir.Text;
                    optAnnuel.Checked = true;
                    break;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocFicheContrat_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocClient");

            string sAvenantCtr = null;
            SSTab1.SelectedTab = SSTab1.Tabs[0];
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            SSTab1.SelectedTab = SSTab1.Tabs[0];
            General.open_conn();

            var _with40 = this;
            //=== positionne sur le dernier enregistrement.
            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                Adodc1 = new DataTable();
                var _with41 = Adodc1;
                sAvenantCtr = General.getFrmReg(Variable.cUserDocFicheContrat, "Avenant", "");

                ///======> Tested First
                if (!string.IsNullOrEmpty(sAvenantCtr))
                {
                    SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat where NumContrat ='" + ModParametre.sNewVar + "' and Avenant ='" + sAvenantCtr + "'", General.CHEMINBASE);
                    SDAAdodc1.Fill(Adodc1);//le verifier

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;

                    RecupererValeurs(Adodc1);
                }
                else
                {
                    SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat where NumContrat ='" + ModParametre.sNewVar + "' and Avenant ='0'", General.CHEMINBASE);
                    SDAAdodc1.Fill(Adodc1);//le verifier

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;

                    RecupererValeurs(Adodc1);
                }
                //                    If Not Adodc1.Recordset.EOF And Not Adodc1.Recordset.BOF Then
                //                        Adodc1.Recordset.MoveFirst
                //                    End If
                // _with41.Refresh();

                //=== rustine du 25 juillet 2012, certains contrats n'ont pas d'avenant à 0 (ce qui est anormal),
                //=== aussi la requete ci dessus ne retourne aucun enregistrement alors que ce contrat existe
                //=== en avenant 1 (voir 2). la rustine ci dessous recherche si il existe un contrat en avenant 1
                //=== ou 2 (max).
                if (Adodc1.Rows.Count == 0)
                {
                    SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat where NumContrat ='" + ModParametre.sNewVar + "' and Avenant ='1'", General.CHEMINBASE);
                    //_with41.Refresh();
                    SDAAdodc1.Fill(Adodc1);
                    RecupererValeurs(Adodc1);

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;
                }

                if (Adodc1.Rows.Count == 0)
                {
                    SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat where NumContrat ='" + ModParametre.sNewVar + "' and Avenant ='2'", General.CHEMINBASE);
                    // _with41.Refresh();
                    SDAAdodc1.Fill(Adodc1);
                    RecupererValeurs(Adodc1);

                    adocontrat = Adodc1;
                    SDAadocontrat = SDAAdodc1;
                }

                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();


            }
            else
            {

                var _with42 = Adodc1;
                Adodc1 = new DataTable();
                // _with42.ConnectionString = General.CHEMINBASE;
                SDAAdodc1 = new SqlDataAdapter("SELECT TOP 1 Contrat.* From Contrat order by NumContrat", General.CHEMINBASE);
                // _with42.Refresh();
                SDAAdodc1.Fill(Adodc1);

                adocontrat = Adodc1;
                SDAadocontrat = SDAAdodc1;

                RecupererValeurs(Adodc1);
                //===  modif du 29 07 2013,  ajout de l'onglet suivi.
                fc_LoadSuivi();
            }

            adocontrat = Adodc1;
            SDAadocontrat = SDAAdodc1;
            //SDAAdodc1.Fill(adocontrat);
            RecupererValeurs(adocontrat);
            if (adocontrat.Rows.Count > 0)
            {

                if (!string.IsNullOrEmpty(txtTypeFormule.Text))
                {
                    RechercheFormule(txtTypeFormule.Text);
                }
                else
                {
                    txtFormule.Text = "";
                }

                ////========> Tested
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    RechercheParImmeuble(txtCodeImmeuble.Text);
                }
                else
                {
                    InitialiseImmeubleGerant();
                }

                ////========> Tested
                if (!string.IsNullOrEmpty(txtCodeArticle.Text))
                {
                    RechercheParCodeArticle(txtCodeArticle.Text);
                }
                else
                {
                    InitialiseArticle();
                }

                //=== modif du 24 07 2012, reactive les champs.
                ////========> Tested
                if (adocontrat.Rows.Count > 0)
                {
                    TableContratActive(this);
                }


            }
            else
            {
                TableContratVide(this);
            }

            //remplit la table FacCalendrier (ssOleDbgrid1)

            var _with43 = Adodc2;
            Adodc2 = new DataTable();
            SDAAdodc2 = new SqlDataAdapter("SELECT * From FacCalendrier Where FacCalendrier.NumContrat ='" + txtNumContrat.Text + "' And FacCalendrier.Avenant ='" + txtAvenant.Text + "' order by histo DESC,Ratio DESC,Date", General.CHEMINBASE);
            SDAAdodc2.Fill(Adodc2);
            SSOleDBGrid1.DataSource = Adodc2;
            //_with43.Refresh();

            blAutorise = true;
            var _with44 = Adodc3;
            Adodc3 = new DataTable();
            SDAAdodc3 = new SqlDataAdapter("SELECT Code,Libelle From TypeFacture", General.CHEMINBASE);
            SDAAdodc3.Fill(Adodc3);
            SSOleDBDropDown1.DataSource = Adodc3;
            //_with44.Refresh();
            var _with45 = Adodc4;
            Adodc4 = new DataTable();
            SDAAdodc4 = new SqlDataAdapter("SELECT Code,Libelle  From TypeRatio", General.CHEMINBASE);
            SDAAdodc4.Fill(Adodc4);
            SSOleDBDropDown2.DataSource = Adodc4;
            // _with45.Refresh();
            Adodc5 = new DataTable();
            Adodc5.Columns.Add("Mois");
            Adodc5.Rows.Add("1");
            Adodc5.Rows.Add("2");
            Adodc5.Rows.Add("3");
            Adodc5.Rows.Add("4");
            Adodc5.Rows.Add("5");
            Adodc5.Rows.Add("6");
            Adodc5.Rows.Add("7");
            Adodc5.Rows.Add("8");
            Adodc5.Rows.Add("9");
            Adodc5.Rows.Add("10");
            Adodc5.Rows.Add("11");
            Adodc5.Rows.Add("12");
            ultraCombo1.DataSource = Adodc5;

            // ModMain.bActivate = false;


        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="adodc1"></param>
        private void RecupererValeurs(DataTable adodc1)
        {
            if (adodc1.Rows.Count == 0) return;
            //les valeurs des champs de l'entete de la fiche  
            txtNumContrat.Text = adodc1.Rows[0]["NumContrat"].ToString();
            txtAvenant.Text = adodc1.Rows[0]["Avenant"].ToString();
            txtLibelleCont1.Text = adodc1.Rows[0]["LibelleCont1"].ToString();
            txtLibelleCont2.Text = adodc1.Rows[0]["LibelleCont2"].ToString();
            txtCodeImmeuble.Text = adodc1.Rows[0]["CodeImmeuble"].ToString();
            if (adodc1.Rows[0]["Resiliee"].ToString() == "True")
            {
                ChkResiliee.Checked = true;
            }
            else
            {
                ChkResiliee.Checked = false;
            }
            //les valeurs de Tab Identification
            txtRefGerant.Text = adodc1.Rows[0]["RefGerant"].ToString();
            Text1.Text = adodc1.Rows[0]["DateDeSignature"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["DateDeSignature"].ToString()).ToShortDateString() : "";
            Text2.Text = adodc1.Rows[0]["DateEffet"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["DateEffet"].ToString()).ToShortDateString() : "";
            Text3.Text = adodc1.Rows[0]["DateFin"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["DateFin"].ToString()).ToShortDateString() : "";
            txtCON_CompAdresse1.Text = adodc1.Rows[0]["CON_CompAdresse1"].ToString();
            txtCON_CompAdresse2.Text = adodc1.Rows[0]["CON_CompAdresse2"].ToString();
            if (adodc1.Rows[0]["nonFacturable"].ToString() == "True")
            {
                Check3.Checked = true;
            }
            else
            {
                Check3.Checked = false;
            }
            if (adodc1.Rows[0]["FactureManuelle"].ToString() == "True")
            {
                chkFactureManuelle.Checked = true;
            }
            else
            {
                chkFactureManuelle.Checked = false;
            }

            //les valeurs de Tab Parametre 
            txtCodeArticle.Text = adodc1.Rows[0]["CodeArticle"].ToString();
            txtBaseContractuelle.Text = adodc1.Rows[0]["BaseContractuelle"].ToString();
            txtBaseActualisee.Text = adodc1.Rows[0]["BaseActualisee"].ToString();
            txtBase.Text = adodc1.Rows[0]["DateBase"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["DateBase"].ToString()).ToShortDateString() : "";
            txtDateActualisee.Text = adodc1.Rows[0]["DateActualisee"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["DateActualisee"].ToString()).ToShortDateString() : "";
            txtBaseRevisee.Text = adodc1.Rows[0]["BaseRevisee"].ToString();
            txtCON_TypeContrat.Text = adodc1.Rows[0]["CON_TypeContrat"].ToString();
            txtTypeFormule.Text = adodc1.Rows[0]["CodeFormule"].ToString();
            txtCON_Coef.Text = adodc1.Rows[0]["CON_Coef"].ToString();
            txtIndice1.Text = adodc1.Rows[0]["Indice1"].ToString();
            txtIndice2.Text = adodc1.Rows[0]["Indice2"].ToString();
            txtIndice3.Text = adodc1.Rows[0]["Indice3"].ToString();
            txtIndice4.Text = adodc1.Rows[0]["Indice4"].ToString();
            txtIndice5.Text = adodc1.Rows[0]["Indice5"].ToString();
            txtIndice6.Text = adodc1.Rows[0]["Indice6"].ToString();
            txtIndice1Valeur.Text = adodc1.Rows[0]["Valeur1"].ToString();
            txtIndice2Valeur.Text = adodc1.Rows[0]["Valeur2"].ToString();
            txtIndice3Valeur.Text = adodc1.Rows[0]["Valeur3"].ToString();
            txtIndice4Valeur.Text = adodc1.Rows[0]["Valeur4"].ToString();
            txtIndice5Valeur.Text = adodc1.Rows[0]["Valeur5"].ToString();
            txtIndice6Valeur.Text = adodc1.Rows[0]["Valeur6"].ToString();
            txtIndice1Date.Text = adodc1.Rows[0]["Date1"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date1"].ToString()).ToShortDateString() : "";
            txtIndice2Date.Text = adodc1.Rows[0]["Date2"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date2"].ToString()).ToShortDateString() : "";
            txtIndice3Date.Text = adodc1.Rows[0]["Date3"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date3"].ToString()).ToShortDateString() : "";
            txtIndice4Date.Text = adodc1.Rows[0]["Date4"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date4"].ToString()).ToShortDateString() : "";
            txtIndice5Date.Text = adodc1.Rows[0]["Date5"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date5"].ToString()).ToShortDateString() : "";
            txtIndice6Date.Text = adodc1.Rows[0]["Date6"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["Date6"].ToString()).ToShortDateString() : "";
            if (adodc1.Rows[0]["Reel"].ToString() == "True")
            {
                Check1.Checked = true;
            }
            else
            {
                Check1.Checked = false;
            }
            if (adodc1.Rows[0]["Connu"].ToString() == "True")
            {
                Check2.Checked = true;
            }
            else
            {
                Check2.Checked = false;
            }

            txtActivite.Text = adodc1.Rows[0]["AnalytiqueActivite"].ToString();
            txtAnalytique.Text = adodc1.Rows[0]["AnalytiqueImmeuble"].ToString();
            txtCON_AnaModif.Text = adodc1.Rows[0]["CON_AnaModif"].ToString();
            txtNomAnalytique.Text = adodc1.Rows[0]["LibelleAnaImmeuble"].ToString();
            txtCGINITULE.Text = adodc1.Rows[0]["CON_AnaModif"].ToString();

            //les valeurs de Tab Calandrier
            txtCON_Indice1rev.Text = adodc1.Rows[0]["CON_Indice1rev"].ToString();
            txtCON_Indice2rev.Text = adodc1.Rows[0]["CON_Indice2rev"].ToString();
            txtCON_Indice3rev.Text = adodc1.Rows[0]["CON_Indice3rev"].ToString();
            txtCON_Valeur1rev.Text = adodc1.Rows[0]["CON_Valeur1rev"].ToString();
            txtCON_Valeur2rev.Text = adodc1.Rows[0]["CON_Valeur2rev"].ToString();
            txtCON_Valeur3rev.Text = adodc1.Rows[0]["CON_Valeur3rev"].ToString();
            txtCON_Date1rev.Text = adodc1.Rows[0]["CON_Date1rev"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["CON_Date1rev"].ToString()).ToShortDateString() : "";
            txtCON_Date2rev.Text = adodc1.Rows[0]["CON_Date2rev"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["CON_Date2rev"].ToString()).ToShortDateString() : "";
            txtCON_Date3rev.Text = adodc1.Rows[0]["CON_Date3rev"] != DBNull.Value ? Convert.ToDateTime(adodc1.Rows[0]["CON_Date3rev"].ToString()).ToShortDateString() : "";
            txtCON_EchuEchoir.Text = adodc1.Rows[0]["CON_EchuEchoir"].ToString();
            if (adodc1.Rows[0]["CalcEcheanceImm"].ToString() == "True")
            {
                chkCalcEcheance.Checked = true;
            }
            else
            {
                chkCalcEcheance.Checked = false;
            }

            txtDateRevision.Text = adodc1.Rows[0]["DateRevisee"].ToString();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_MoisIndice"].ValueList = ultraCombo1;
            this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns[7].ValueList = SSOleDBDropDown1;
            this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns[8].ValueList = SSOleDBDropDown2;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumContrat"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Avenant"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["histo"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NoFactureTemp"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["DateFacture"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["nonFacturable"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_SansRevision"].Header.Caption = "Sans Revision";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_MoisIndice"].Header.Caption = "MoisIndice";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["AEffacerNofacture"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["BaseNoFactureTemp"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["BaseNoFacture"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["FacEssai"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["coef"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Resiliee"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Facture"].Editor.DataFilter = new BooleanColumnDataFilter();
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_SansRevision"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_SansRevision"].Editor.DataFilter = new BooleanColumnDataFilter();
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCdu"].CellActivation = Activation.ActivateOnly;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellActivation = Activation.ActivateOnly;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Echeance"].CellActivation = Activation.ActivateOnly;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Facture"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Facture"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Facture"].Editor.DataFilter = new BooleanColumnDataFilter();
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NoFacture"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PCdu"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].CellAppearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterExitEditMode(object sender, EventArgs e)
        {
            if (SSOleDBGrid1.ActiveCell.DataChanged)
            {
                if (SSOleDBGrid1.ActiveCell.Column.Index == 4)
                {
                    if (Option9.Checked)
                    {
                        indi1 = 1;
                        strTEMP = SSOleDBGrid1.ActiveRow.Cells[4].Value.ToString();
                        while (indi1 <= strTEMP.Length)
                        {
                            if (General.Mid(strTEMP, indi1, 1) != "%" && indi1 == strTEMP.Length)
                            {
                                SSOleDBGrid1.ActiveRow.Cells[4].Value = SSOleDBGrid1.ActiveRow.Cells[4].Value + "%";
                            }
                            indi1++;
                        }
                    }
                    if (Option8.Checked)
                    {
                        indi1 = 1;
                        strTEMP = SSOleDBGrid1.ActiveRow.Cells[4].Value.ToString();
                        if (SSOleDBGrid1.ActiveRow.Cells[4].DataChanged)
                            while (indi1 <= strTEMP.Length)
                            {
                                if (General.Mid(strTEMP, indi1, 1) != "/" && indi1 == strTEMP.Length)
                                {
                                    SSOleDBGrid1.ActiveRow.Cells[4].Value = "1/" + SSOleDBGrid1.ActiveRow.Cells[4].Value;
                                }
                                indi1++;
                            }
                    }
                }
                else if (SSOleDBGrid1.ActiveCell.Column.Index == 1)
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[1].Value.ToString()) && !General.IsDate(SSOleDBGrid1.ActiveRow.Cells[1].Value.ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide", General.TITREDONNEEFAUX, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        SSOleDBGrid1.ActiveRow.Cells[1].Value = " ";
                    }
                    else if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells[1].Value.ToString()))
                    {
                        datVerif = Convert.ToDateTime(SSOleDBGrid1.ActiveRow.Cells[1].Value);
                        SSOleDBGrid1.ActiveRow.Cells[1].Value = datVerif;
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //verifier

            try
            {
                if (SSOleDBGrid1.ActiveCell == null || SSOleDBGrid1.ActiveRow == null) return;

                //if (SSOleDBGrid1.ActiveCell.Column.Index == 4)
                //{

                //    if (Option9.Checked)
                //    {
                //        indi1 = 1;
                //        strTEMP = SSOleDBGrid1.ActiveRow.Cells[4].Text.ToString();
                //        if (strTEMP != SSOleDBGrid1.ActiveRow.Cells[4].Value.ToString())
                //            while (indi1 <= strTEMP.Length)
                //            {
                //                if (General.Mid(strTEMP, indi1, 1) != "%" && indi1 == strTEMP.Length)
                //                {
                //                    SSOleDBGrid1.ActiveRow.Cells[4].Value = SSOleDBGrid1.ActiveRow.Cells[4].Text + "%";
                //                }
                //                indi1++;
                //            }
                //    }
                //    if (Option8.Checked)
                //    {
                //        indi1 = 1;
                //        strTEMP = SSOleDBGrid1.ActiveRow.Cells[4].Text.ToString();
                //        if (strTEMP != SSOleDBGrid1.ActiveRow.Cells[4].Value.ToString())
                //            while (indi1 <= strTEMP.Length)
                //            {
                //                if (General.Mid(strTEMP, indi1, 1) != "/" && indi1 == strTEMP.Length)
                //                {
                //                    SSOleDBGrid1.ActiveRow.Cells[4].Value = "1/" + SSOleDBGrid1.ActiveRow.Cells[4].Text;
                //                }
                //                indi1++;
                //            }
                //    }


                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PcDu"].Index)
                {

                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["PcDu"].Text))
                    {
                        if (!General.IsDate(SSOleDBGrid1.ActiveRow.Cells["PcDu"].Text))
                        {
                            CustomMessageBox.Show("Date invalide");
                            e.Cancel = true;
                        }
                    }
                }

                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Au"].Index)
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["Au"].Text))
                    {
                        if (!General.IsDate(SSOleDBGrid1.ActiveRow.Cells["Au"].Text))
                        {
                            CustomMessageBox.Show("Date invalide");
                            e.Cancel = true;
                        }
                    }
                }
                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TypeCalcul"].Index)
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["TypeCalcul"].Text))
                    {
                        var row = SSOleDBDropDown1.Rows.Cast<UltraGridRow>().Where(l => l.Cells["Code"].Value.ToString() == SSOleDBGrid1.ActiveRow.Cells["TypeCalcul"].Text).FirstOrDefault();
                        if (row == null)
                        {
                            CustomMessageBox.Show("Cette valeur est incorrecte, " + "\n" + "Choisisez une valeur dans la liste déroulante.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                        }

                    }

                }
                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TypeRevision"].Index)
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["TypeRevision"].Text))
                    {
                        var row = SSOleDBDropDown2.Rows.Cast<UltraGridRow>().Where(l => l.Cells["Code"].Value.ToString() == SSOleDBGrid1.ActiveRow.Cells["TypeRevision"].Text).FirstOrDefault();
                        if (row == null)
                        {
                            CustomMessageBox.Show("Cette valeur est incorrecte, " + "\n" + "Choisisez une valeur dans la liste déroulante.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                        }
                    }
                }
                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fac_MoisIndice"].Index)
                {
                    if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["Fac_MoisIndice"].Text))
                    {
                        var row = ultraCombo1.Rows.Cast<UltraGridRow>().Where(l => l.Cells["Mois"].Value.ToString() == SSOleDBGrid1.ActiveRow.Cells["Fac_MoisIndice"].Text).FirstOrDefault();
                        if (row == null)
                        {
                            CustomMessageBox.Show("Cette valeur est incorrecte, " + "\n" + "Choisisez une valeur dans la liste déroulante.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                        }
                    }
                }

                //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
                if (SSOleDBGrid1.ActiveCell.Column.Index == SSOleDBGrid1.DisplayLayout.Bands[0].Columns["date"].Index)
                {
                    if (!Text2.Text.IsDate())
                    {
                        CustomMessageBox.Show("Vous devez d'abord saisir une date d'effet.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }

                    if (SSOleDBGrid1.ActiveRow.Cells["date"].Text.IsDate())
                    {
                        if (SSOleDBGrid1.ActiveRow != null &&
                            SSOleDBGrid1.ActiveRow.Cells["date"].Text.ToDate() < Text2.Text.ToDate())
                        {
                            CustomMessageBox.Show("Vous devez saisir une date supérieure à la date d'effet.",
                                "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            e.Cancel = true;
                            return;
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }

        }

        /// <summary>
        /// Control Is In A Hidden Container
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAnalytique_KeyPress(object sender, KeyPressEventArgs e)
        {

            if ((int)e.KeyChar == 13)
            {
                RechercheAnalytique((txtAnalytique.Text));
            }
        }

        private void txtRefGerant_TextChanged(object sender, EventArgs e)
        {

        }

        private void Text3_TextChanged(object sender, EventArgs e)
        {

        }

        private void Text2_TextChanged(object sender, EventArgs e)
        {

        }

        private void Text1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCON_CompAdresse1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtCON_CompAdresse2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtcommentaire1_TextChanged(object sender, EventArgs e)
        {

        }

        private void SSOleDBGrid1_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrecte dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption);
                e.Cancel = true;
            }
        }

        private void SSOleDBGrid1_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //SCBAdodc2 = new SqlCommandBuilder(SDAAdodc2);
            //var xx = SDAAdodc2.Update(Adodc2);
        }


    }


}