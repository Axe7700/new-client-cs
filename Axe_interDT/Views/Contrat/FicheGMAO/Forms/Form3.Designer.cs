﻿namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.optContrat = new System.Windows.Forms.RadioButton();
            this.optDemandeContrat = new System.Windows.Forms.RadioButton();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // optContrat
            // 
            this.optContrat.AutoSize = true;
            this.optContrat.Checked = true;
            this.optContrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optContrat.Location = new System.Drawing.Point(30, 23);
            this.optContrat.Name = "optContrat";
            this.optContrat.Size = new System.Drawing.Size(81, 23);
            this.optContrat.TabIndex = 538;
            this.optContrat.TabStop = true;
            this.optContrat.Text = "Contrat";
            this.optContrat.UseVisualStyleBackColor = true;
            // 
            // optDemandeContrat
            // 
            this.optDemandeContrat.AutoSize = true;
            this.optDemandeContrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optDemandeContrat.Location = new System.Drawing.Point(30, 64);
            this.optDemandeContrat.Name = "optDemandeContrat";
            this.optDemandeContrat.Size = new System.Drawing.Size(259, 23);
            this.optDemandeContrat.TabIndex = 539;
            this.optDemandeContrat.TabStop = true;
            this.optDemandeContrat.Text = "Origine de la demande de contrat";
            this.optDemandeContrat.UseVisualStyleBackColor = true;
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(159)))), ((int)(((byte)(118)))));
            this.cmdVisu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdVisu.ForeColor = System.Drawing.Color.White;
            this.cmdVisu.Image = global::Axe_interDT.Properties.Resources.Eye_16x16;
            this.cmdVisu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisu.Location = new System.Drawing.Point(199, 9);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.MaximumSize = new System.Drawing.Size(85, 35);
            this.cmdVisu.MinimumSize = new System.Drawing.Size(85, 35);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(85, 35);
            this.cmdVisu.TabIndex = 571;
            this.cmdVisu.Text = "     Visu";
            this.cmdVisu.UseVisualStyleBackColor = false;
            this.cmdVisu.Click += new System.EventHandler(this.cmdVisu_Click);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(295, 134);
            this.Controls.Add(this.cmdVisu);
            this.Controls.Add(this.optDemandeContrat);
            this.Controls.Add(this.optContrat);
            this.Name = "Form3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form3";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton optContrat;
        private System.Windows.Forms.RadioButton optDemandeContrat;
        public System.Windows.Forms.Button cmdVisu;
    }
}