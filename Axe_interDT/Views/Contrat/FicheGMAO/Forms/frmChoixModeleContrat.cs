﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class frmChoixModeleContrat : Form
    {
        public frmChoixModeleContrat()
        {
            InitializeComponent();
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            General.lChoixModele = 1;
            this.Close();
        }

        private void Command2_Click(object sender, EventArgs e)
        {
            General.lChoixModele = 2;
            this.Close();
        }

        private void Command3_Click(object sender, EventArgs e)
        {
            General.lChoixModele = 0;
            this.Close();
        }
    }
}
