﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void cmdVisu_Click(object sender, EventArgs e)
        {
            if (optContrat.Checked)
            {
                General.lEditionCtr = 1;
            }
            else if (optDemandeContrat.Checked)
            {
                General.lEditionCtr = 2;
            }
        }
    }
}
