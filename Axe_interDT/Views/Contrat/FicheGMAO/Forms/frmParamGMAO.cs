﻿using Axe_interDT.Shared;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class frmParamGMAO : Form
    {
        private const int cColWidth = 500;
        List<UltraCombo> cmdMoisList = new List<UltraCombo>();

        public frmParamGMAO()
        {
            InitializeComponent();

            cmdMoisList.Add(cmdMois_1);
            cmdMoisList.Add(cmdMois_2);
            cmdMoisList.Add(cmdMois_3);
            cmdMoisList.Add(cmdMois_4);
            cmdMoisList.Add(cmdMois_5);
            cmdMoisList.Add(cmdMois_6);
            cmdMoisList.Add(cmdMois_7);
            cmdMoisList.Add(cmdMois_8);
            cmdMoisList.Add(cmdMois_9);
            cmdMoisList.Add(cmdMois_10);
            cmdMoisList.Add(cmdMois_11);
            cmdMoisList.Add(cmdMois_12);
        }

        private void fc_LoadCombo()
        {
            int i = 0;

            try
            {
                for (i = 0; i < 12; i++)
                {
                    var dt = new DataTable();
                    dt.Columns.Add("Mois");

                    dt.Columns.Add(ModP2v2.cIDAnnuel);
                    dt.Columns.Add(ModP2v2.cIDBiMensuel);
                    dt.Columns.Add(ModP2v2.cIDCycleAnnuel);
                    dt.Columns.Add(ModP2v2.cIDCycleJournee);
                    dt.Columns.Add(ModP2v2.cIDCycleMensuel);
                    dt.Columns.Add(ModP2v2.cIDHebdo);
                    dt.Columns.Add(ModP2v2.cIDMensuel);
                    dt.Columns.Add(ModP2v2.cIDSemestriel);
                    dt.Columns.Add(ModP2v2.cIDBiMestriel);
                    dt.Columns.Add(ModP2v2.cIDQuotidien);
                    dt.Columns.Add(ModP2v2.cIDTriMestriel);

                    cmdMoisList[i].DataSource = dt;
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void fc_AddPeriode()
        {
            string stemp = "";
            DataTable rsAdd = null;
            ModAdo rsAddModAdo = new ModAdo();
            string sSQL = "";
            string sCodeimmeuble = "";
            int lEQM_NoAuto = 0;
            string sTPP_Code = "";
            int lPEI_JD = 0;
            int lCop_NoAuto = 0;
            int i = 0;
            int Y = 0;

            try
            {
                sCodeimmeuble = txtCodeimmeuble.Text;
                lEQM_NoAuto = txtEQM_Noauto.Text.ToInt();
                lPEI_JD = 1;
                lCop_NoAuto = General.nz(txtCOP_Noauto.Text, 0).ToInt();

                sSQL = "Delete FROM PEI_PeriodeGammeImm where EQM_NoAuto =" + lEQM_NoAuto;
                General.Execute(sSQL);

                sSQL =
                    "SELECT        CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code, PEI_JD, PEI_MD, PEI_AnD, PEI_JF, PEI_MF, PEI_AnF,"
                    + " SEM_Code, PEI_Duree, PEI_Visite, PEI_Tech, PEI_Cout, PEI_ForceVisite, COP_NoContrat, PEI_TotalHT, "
                    + " PEI_DureeReel, PEI_Intervenant, PEI_APlanifier, PEI_Calcul, PEI_IntAutre, COP_NoAuto, PEI_AvisPassage,"
                    + " PEI_AvisNbJour, PEI_KFO, PEI_KMO, PEI_Heure, PEI_ForceJournee, PEI_MtAchat, PEI_Cycle, PEI_DateDebut, "
                    + " PEI_DateFin , PEI_DureeVisu, PEI_Samedi, PEI_Dimanche, PEI_Pentecote, PEI_Jourferiee, PEI_NbAn, PEI_CompteurObli,"
                    + " TempDate, PEI_PassageObli, ChampsTempAeffacer "
                    + " From PEI_PeriodeGammeImm "
                    + " WHERE        (PEI_NoAuto = 0)";

                rsAdd = rsAddModAdo.fc_OpenRecordSet(sSQL);

                for (Y = 0; Y < GridGMAO.Rows.Count; Y++)
                {
                    for (i = 1; i <= 12; i++)
                    {
                        stemp = GridGMAO.Rows[Y].Cells["" + i + ""].Value?.ToString();

                        switch (stemp)
                        {
                            case ModP2v2.cIDAnnuel:
                                sTPP_Code = modP2.cAnnuel;
                                break;
                            case ModP2v2.cIDBiMensuel:
                                sTPP_Code = modP2.cBiMensuel;
                                break;
                            case ModP2v2.cIDCycleAnnuel:
                                sTPP_Code = modP2.cCycleAnnuel;
                                break;
                            case ModP2v2.cIDCycleJournee:
                                sTPP_Code = modP2.cCycleJournee;
                                break;
                            case ModP2v2.cIDCycleMensuel:
                                sTPP_Code = modP2.cCycleMensuel;
                                break;
                            case ModP2v2.cIDHebdo:
                                sTPP_Code = modP2.cHebdo;
                                break;
                            case ModP2v2.cIDMensuel:
                                sTPP_Code = modP2.cSemestriel;
                                break;
                            case ModP2v2.cIDSemestriel:
                                sTPP_Code = modP2.cBiMestriel;
                                break;
                            case ModP2v2.cIDBiMestriel:
                                sTPP_Code = modP2.cBiMestriel;
                                break;
                            case ModP2v2.cIDQuotidien:
                                sTPP_Code = modP2.cQuotidien;
                                break;
                            case ModP2v2.cIDTriMestriel:
                                sTPP_Code = modP2.cTriMestriel;
                                break;
                            default:
                                sTPP_Code = "";
                                break;
                        }

                        int lPEI_MD;
                        int lPEI_JF;
                        int lPEI_MF;

                        if (!sTPP_Code.IsNullOrEmpty())
                        {
                            var newRow = rsAdd.NewRow();

                            newRow["CodeImmeuble"] = sCodeimmeuble;
                            newRow["EQM_NoAuto"] = lEQM_NoAuto;
                            newRow["TPP_Code"] = sTPP_Code;
                            newRow["PEI_JD"] = lPEI_JD;
                            newRow["PEI_MD"] = i;

                            if (i == 2)
                            {
                                newRow["PEI_JF"] = 29;
                            }
                            else if (i == 1 || i == 3 || i == 5 || i == 7 || i == 8 || i == 10 || i == 12)
                            {
                                newRow["PEI_JF"] = 31;
                            }
                            else if (i == 4 || i == 6 || i == 9 || i == 11)
                            {
                                newRow["PEI_JF"] = 30;
                            }

                            newRow["PEI_MF"] = i;
                            newRow["SEM_Code"] = "";
                            //===  avoir avec Xavier
                            newRow["PEI_Duree"] = 1;
                            newRow["PEI_Visite"] = 1;
                            newRow["PEI_tech"] = DBNull.Value;
                            newRow["PEI_Cout"] = DBNull.Value;
                            newRow["PEI_ForceVisite"] = "A";
                            newRow["COP_NoContrat"] = "";
                            newRow["PEI_TotalHT"] = DBNull.Value;
                            newRow["PEI_DureeReel"] = 1;
                            newRow["PEI_Intervenant"] = "9999";
                            newRow["PEI_Calcul"] = DBNull.Value;
                            newRow["PEI_IntAutre"] = DBNull.Value;
                            newRow["COP_NoAuto"] = lCop_NoAuto;
                            newRow["PEI_AvisPassage"] = DBNull.Value;
                            newRow["PEI_AvisNbJour"] = DBNull.Value;
                            newRow["PEI_KFO"] = DBNull.Value;
                            newRow["PEI_KMO"] = DBNull.Value;
                            newRow["PEI_Heure"] = DBNull.Value;
                            newRow["PEI_ForceJournee"] = DBNull.Value;
                            newRow["PEI_MtAchat"] = DBNull.Value;
                            newRow["PEI_Cycle"] = DBNull.Value;
                            newRow["PEI_DateDebut"] = DBNull.Value;
                            newRow["PEI_DateFin"] = DBNull.Value;
                            newRow["PEI_DureeVisu"] = DBNull.Value;
                            newRow["PEI_Samedi"] = 1;
                            newRow["PEI_Dimanche"] = 1;
                            newRow["PEI_Pentecote"] = 1;
                            newRow["PEI_Jourferiee"] = 1;
                            newRow["PEI_NbAn"] = DBNull.Value;
                            newRow["PEI_CompteurObli"] = DBNull.Value;
                            newRow["TempDate"] = DBNull.Value;
                            newRow["PEI_PassageObli"] = DBNull.Value;
                            newRow["ChampsTempAeffacer"] = DBNull.Value;
                            newRow["PEI_APlanifier"] = DBNull.Value;
                            rsAdd.Rows.Add(newRow);

                            rsAddModAdo.Update();
                        }
                    }
                }
                //Dim sM1     As String
                //Dim sM2     As String
                //Dim sM3     As String
                //Dim sM4     As String
                //Dim sM5     As String
                //Dim sM6     As String
                //Dim sM7     As String
                //Dim sM8     As String
                //Dim sM9     As String
                //Dim sM10     As String
                //Dim sM11     As String
                //Dim sM12     As String
                //Dim i        As Long
                //
                //sM1 = Combo(1)
                //sM2 = Combo(2)
                //sM3 = Combo(3)
                //sM4 = Combo(4)
                //sM5 = Combo(5)
                //sM6 = Combo(6)
                //sM7 = Combo(7)
                //sM8 = Combo(8)
                //sM9 = Combo(9)
                //sM10 = Combo(10)
                //sM11 = Combo(11)
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void fc_Save()
        {
            DataTable rs = null;
            ModAdo rsModAdo = new ModAdo();
            int lCop_NoAuto;
            string sCodeimmeuble;
            int lPDAB_Noauto;

            try
            {
                sCodeimmeuble = txtCodeimmeuble.Text;
                lPDAB_Noauto = General.nz(txtPDAB_Noauto.Text, 0).ToInt();

                //// === en registre la gamme.
                //sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier, COP_Noauto,GAI_Compteur,GAI_CompteurObli, GAI_NoLigne, CodeImmeuble " _
                //    & " From GAI_GammeImm " _
                //    & " where COP_NoAuto = 0"
                //
                //If sPriseEnCompteLocalP2 = "1" Then
                //    sSQL = sSQL & " AND PDAB_Noauto =" & txtPDAB_Noauto & ""
                //End If
                //
                //
                //
                //rs.AddNew
                //
                //rs!GAi_ID =
                //rs!GAM_Code =
                //rs!GAI_LIbelle =
                //rs!PDAB_Noauto =
                //rs!GAI_Select =
                //rs!GAI_NePasPlanifier =
                //rs!COP_Noauto =
                //rs!GAI_Compteur =
                //rs!GAI_CompteurObli =
                //rs!GAI_NoLigne =
                //rs!CodeImmeuble = sCodeImmeuble
                //
                //rs.Update
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_AddPeriode();
        }

        private void Combo_0_BeforeDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            var Index = (sender as UltraCombo).Tag.ToInt();
            string sSQL = "";

            try
            {
                switch (Index)
                {
                    case 0: //===> Tested
                        //=== controle.
                        sSQL = "SELECT     CTR_Libelle as Contrôle, CTR_ID"
                               + " From CTR_ControleGmao "
                               + " ORDER BY CTR_Libelle ";

                        sheridan.InitialiseCombo(Combo_0, sSQL, "Contrôle");

                        Combo_0.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
                        Combo_0.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                        break;
                    case 1: //===> Tested
                        //=== moyen.
                        sSQL = "SELECT     MOY_Libelle as Moyen, MOY_ID From MOY_MoyenGmao "
                               + " ORDER BY MOY_Libelle";

                        sheridan.InitialiseCombo(Combo_1, sSQL, "Moyen");

                        Combo_1.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
                        Combo_1.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                        break;
                    case 2: //===> Tested
                        //=== Anomalie.
                        sSQL = "SELECT     ANO_Libelle as Anomalie , ANO_ID From ANO_AnomalieGmao " +
                               " ORDER BY ANO_Libelle";

                        sheridan.InitialiseCombo(Combo_2, sSQL, "Anomalie");

                        Combo_2.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
                        Combo_2.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                        break;
                    case 3: //===> Tested
                        //=== Opération.
                        sSQL = "SELECT     OPE_Libelle as Opération, OPE_ID From OPE_OperationGmao "
                               + " ORDER BY OPE_Libelle ";

                        sheridan.InitialiseCombo(Combo_3, sSQL, "Opération");

                        Combo_3.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
                        Combo_3.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                        break;
                    case 4: //===> Tested
                        //=== localisation.
                        sSQL = "SELECT     PDAB_Noauto, PDAB_Libelle as Localisation "
                               + " From PDAB_BadgePDA"
                               + " WHERE Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                        sheridan.InitialiseCombo(Combo_4, sSQL, "Localisation");
                        break;
                    case 5: //===> Tested
                        //=== equipements.
                        sSQL =
                            "SELECT     GAM_ID, GAM_Code as [Code], GAM_Libelle as [Libellé], GAM_Compteur, GAM_CompteurObli "
                            + " FROM         GAM_Gamme" + " ORDER BY GAM_Code ";

                        sheridan.InitialiseCombo(Combo_5, sSQL, "Code");
                        break;
                    case 6: //===> Tested
                        //=== composant.
                        sSQL = "SELECT GAM_ID FROM GAM_Gamme where GAM_Code ='" +
                               General.nz(Combo_5.Text, "XXXXX@@X2233") + "'";

                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);


                        sSQL = "SELECT CAI_Code AS [Code], CAI_Libelle as [Libellé], CAI_NoAuto "
                               + " FROM CAI_CategoriInterv"
                               + " WHERE GAM_ID =" + General.nz(sSQL, 0);

                        sheridan.InitialiseCombo(Combo_6, sSQL, "Code");
                        break;
                }
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                throw;
            }
        }

        private void frmParamGMAO_Load(object sender, EventArgs e)
        {
            //fc_LoadParamGMAO " WHERE PEI_PeriodeGammeImm.PEI_Noauto =" & nz(txtPEI_NoAuto, 0)
            fc_LoadCombo();
            fc_LoadGRID(General.nz(txtEQM_Noauto.Text, 0).ToInt());
            //txtLocalisation.Locked = Tru
            fc_DropDown();
        }

        private void fc_DropDown()
        {
            string sSQL = "";

            try
            {
                sSQL = "SELECT       TP2_Code as Code, TP2_Libelle as Libelle "
                       + " From TP2_TypePeriode "
                + " ORDER BY TP2_Code";

                sheridan.InitialiseCombo(ssDropMois, sSQL, "Code");
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void GridGMAO_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["1"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["2"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["3"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["4"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["5"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["6"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["7"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["8"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["9"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["10"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["11"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["12"].ValueList = ssDropMois;
            e.Layout.Bands[0].Columns["Autre"].Style = ColumnStyle.CheckBox;
        }

        private void fc_LoadGRID(int lEQM_NoAuto)
        {
            var dt = new DataTable();
            dt.Columns.Add("Prestation");
            dt.Columns.Add("complément");
            dt.Columns.Add("Localisation");
            dt.Columns.Add("intervenant");
            dt.Columns.Add("Autre");
            dt.Columns.Add("Doubler");
            dt.Columns.Add("Nb Visite");
            dt.Columns.Add("Duree");
            dt.Columns.Add("répartition");
            dt.Columns.Add("Compteurs");
            dt.Columns.Add("1");
            dt.Columns.Add("2");
            dt.Columns.Add("3");
            dt.Columns.Add("4");
            dt.Columns.Add("5");
            dt.Columns.Add("6");
            dt.Columns.Add("7");
            dt.Columns.Add("8");
            dt.Columns.Add("9");
            dt.Columns.Add("10");
            dt.Columns.Add("11");
            dt.Columns.Add("12");
            dt.Columns.Add("contrat");
            dt.Columns.Add("Avt");
            dt.Columns.Add("Résilié");
            dt.Columns.Add("Jour");
            dt.Columns.Add("Semaine");
            dt.Columns.Add("Heure");
            dt.Columns.Add("Hors WE");

            ModP2v2.ParamGMAOx[] tGmao = null;
            ModP2v2.ParamGMAOw[] tpGoupPrest = null;
            int i;
            string sWhere = "";

            try
            {
                sWhere = " WHERE Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                //sWhere = sWhere & " AND (Contrat.DateFin is null  or Contrat.DateFin <" & Date & ")"

                sWhere = sWhere + " AND (PEI_PeriodeGammeImm.EQM_Noauto = " + lEQM_NoAuto + ") ";

                tGmao = ModP2v2.fc_LoadParamGMAO(sWhere, 4, lblAvancement);
                tpGoupPrest = ModP2v2.fc_GroupPrestation(tGmao, lblAvancement, true);

                i = tpGoupPrest.Length;

                for (i = 0; i < tpGoupPrest.Length; i++)
                {
                    var newRow = dt.NewRow();
                    newRow["Prestation"] = tpGoupPrest[i].sPrestation;
                    newRow["complément"] = tpGoupPrest[i].sComplement;
                    newRow["Localisation"] = tpGoupPrest[i].sLocalisation;
                    newRow["intervenant"] = tpGoupPrest[i].sInterv;
                    newRow["Autre"] = tpGoupPrest[i].lIntervAutre;
                    newRow["Nb Visite"] = tpGoupPrest[i].lnbVisite;
                    newRow["Duree"] = tpGoupPrest[i].dbDuree;
                    newRow["répartition"] = tpGoupPrest[i].sRepartition;
                    newRow["Compteurs"] = tpGoupPrest[i].sCompteur;
                    newRow["1"] = tpGoupPrest[i].sJanvier;
                    newRow["2"] = tpGoupPrest[i].sFevrier;
                    newRow["3"] = tpGoupPrest[i].sMars;
                    newRow["4"] = tpGoupPrest[i].sAvril;
                    newRow["5"] = tpGoupPrest[i].sMai;
                    newRow["6"] = tpGoupPrest[i].sJuin;
                    newRow["7"] = tpGoupPrest[i].sJuillet;
                    newRow["8"] = tpGoupPrest[i].sAout;
                    newRow["9"] = tpGoupPrest[i].sSeptembre;
                    newRow["10"] = tpGoupPrest[i].sOctobre;
                    newRow["11"] = tpGoupPrest[i].sNovembre;
                    newRow["12"] = tpGoupPrest[i].sDecembre;
                    newRow["contrat"] = tpGoupPrest[i].sNoContrat;
                    newRow["Avt"] = tpGoupPrest[i].sAvt;
                    newRow["résilié"] = tpGoupPrest[i].sResilie;
                    newRow["Jour"] = tpGoupPrest[i].sJour;
                    newRow["semaine"] = tpGoupPrest[i].sSemaine;
                    newRow["heure"] = tpGoupPrest[i].sHeure;
                    newRow["hors WE"] = tpGoupPrest[i].sHorsWe;
                    dt.Rows.Add(newRow);
                }

                GridGMAO.DataSource = dt;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
