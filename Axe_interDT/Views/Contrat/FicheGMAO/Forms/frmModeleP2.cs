﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class frmModeleP2 : Form
    {
        DataTable rsRecap;
        string keyNodeActive;
        const string cNiv1 = "A";
        const string cNiv2 = "B";

        const string cStyleCompteurObl = "StyleCompteurObl";
        DataTable ssGridModeleSource = new DataTable();

        public frmModeleP2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAfficheModéle_Click(object sender, EventArgs e)
        {
            try
            {
                fc_Loadtreeview();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAfficheModéle_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Loadtreeview()
        {

            int i = 0;

            UltraTreeNode Node = new UltraTreeNode();
            UltraTreeNode Node1 = new UltraTreeNode();
            bool bselect = false;
            string sWhere = null;
            bool bFirst = false;

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            try
            {
                //==reinitialise le treview.
                TreeView1.Nodes.Clear();
                ssGridRecap.Visible = false;

                General.sSQL = "SELECT MVP_ID, MVP_Nom From MVP_ModeleVisiteP2";

                sWhere = "";

                if (!string.IsNullOrEmpty(txtCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  MVP_ModeleVisiteP2.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND MVP_ModeleVisiteP2.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtMVP_Nom.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  MVP_ModeleVisiteP2.MVP_Nom ='" + StdSQLchaine.gFr_DoublerQuote(txtMVP_Nom.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND MVP_ModeleVisiteP2.MVP_Nom  ='" + StdSQLchaine.gFr_DoublerQuote(txtMVP_Nom.Text) + "'";
                    }
                }

                //If txtCOP_NoAuto <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " WHERE  MVP_ModeleVisiteP2.COP_NoAuto = " & txtcopCOP_NoAuto
                //        Else
                //            sWhere = sWhere & " AND MVP_ModeleVisiteP2.COP_NoAuto  " & gFr_DoublerQuote(txtMVP_Nom) & "'"
                //        End If
                //End If

                if (!string.IsNullOrEmpty(txtMVP_CreePar.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  MVP_ModeleVisiteP2.MVP_CreePar ='" + StdSQLchaine.gFr_DoublerQuote(txtMVP_CreePar.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND MVP_ModeleVisiteP2.MVP_CreePar ='" + StdSQLchaine.gFr_DoublerQuote(txtMVP_CreePar.Text) + "'";
                    }
                }

                General.sSQL = General.sSQL + sWhere + " ORDER BY MVP_Nom";
                var tmpAdo = new ModAdo();
                General.rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a aucun modèle(s) crée(s) pour ces critères  de recherche", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    General.rstmp.Dispose();
                    General.rstmp = null;
                    return;
                }


                bFirst = true;
                if (General.rstmp.Rows.Count > 0)
                {
                    foreach (DataRow rstmpRow in General.rstmp.Rows)
                    {
                        //==Ajout du 1er niveau de hiérarchie.
                        if (bFirst == true)
                        {
                            Node = TreeView1.Nodes.Add(cNiv1 + "1", "Modèle");
                            Node.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                            bFirst = false;
                        }
                        //==Ajout du 2eme niveau de hiérarchie.
                        if (rstmpRow["MVP_Nom"] != null)
                        {
                            Node1 = Node.Nodes.Add(cNiv2 + rstmpRow["MVP_ID"], General.nz(rstmpRow["MVP_Nom"], "").ToString());
                            Node1.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                        }
                        //_with1.MoveNext();
                    }
                }
                General.rstmp.Dispose();


                General.rstmp = null;

                bselect = false;

                foreach (UltraTreeNode oNode in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode oNode_N1 in oNode.Nodes)
                    {
                        if (oNode_N1.Selected == true)
                        {
                            bselect = true;
                        }
                    }

                }

                TreeView1.Nodes[cNiv1 + "1"].Expanded = true;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Loadtreeview;");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindModele_Click(object sender, EventArgs e)
        {
            txtMVP_Nom_KeyPress(txtMVP_Nom, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            txtCodeimmeuble_KeyPress(txtCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            try
            {
                ssGridModele.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdSauver_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdInsertModele_Click(object sender, EventArgs e)
        {
            int i = 0;
            //UltraGridRow  book = null;
            ssGridModele.UpdateData();
            var _with2 = ssGridModele;
            try
            {

                //_with2.MoveFirst();
                for (i = 0; i < ssGridModele.Rows.Count; i++)
                {

                    //----ici dbdate1 ne contient pas de date
                    //book = _with2.GetBookmark(i);
                    if (Convert.ToBoolean(ssGridModele.Rows[i].Cells["Selection"].Value.ToString()) == true)
                    {
                        Array.Resize(ref ModVisiteP2.tpModInterP2, i + 1);
                        ModVisiteP2.tpModInterP2[i].lCop_NoAuto = Convert.ToInt32(ssGridModele.Rows[i].Cells["COP_NoAuto"].Value);
                        ModVisiteP2.tpModInterP2[i].lMVP_ID = Convert.ToInt32(ssGridModele.Rows[i].Cells["MVP_ID"].Value);
                        ModVisiteP2.tpModInterP2[i].lPDAB_Noauto = Convert.ToInt32(ssGridModele.Rows[i].Cells["PDAB_Noauto"].Value);
                        ModVisiteP2.tpModInterP2[i].sCodeImmeuble = ssGridModele.Rows[i].Cells["CodeImmeuble"].Value.ToString();
                        ModVisiteP2.bInsertModeleP2 = true;
                    }
                }

                this.Close();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdInsertModele_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSelectModele_Click(object sender, EventArgs e)
        {
            string sID = null;
            string sSQL = null;

            DataTable rs = default(DataTable);
            ssGridModele.UpdateData();

            try
            {
                if (fc_CtrlLocTreev(ref sID) == true)
                {
                    sSQL = "SELECT COP_NoAuto, CodeImmeuble, PDAB_Noauto From MVP_ModeleVisiteP2 WHERE MVP_ID = " + General.nz(sID, 0);
                    var tmpAdo = new ModAdo();
                    rs = tmpAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow rsRow in rs.Rows)
                        {
                            var sAddItem = ssGridModeleSource.NewRow();

                            sAddItem["Selection"] = true;
                            sAddItem["Nom"] = keyNodeActive;// TreeView1.Nodes[cNiv2 + sID].Text ;
                            sAddItem["COP_NoAuto"] = rsRow["COP_NoAuto"];
                            sAddItem["MVP_ID"] = sID;
                            sAddItem["CodeImmeuble"] = rsRow["CodeImmeuble"];
                            sAddItem["PDAB_Noauto"] = rsRow["PDAB_Noauto"];

                            ssGridModeleSource.Rows.Add(sAddItem);
                            ssGridModele.DataSource = ssGridModeleSource;
                            // ssGridModele.AddItem(sAddItem);

                            // rs.MoveNext();
                        }
                        ssGridModele.DataSource = ssGridModeleSource;
                    }
                    rs.Dispose();
                    rs = null;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSelectModele_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        private bool fc_CtrlLocTreev(ref string sID)
        {
            bool functionReturnValue = false;

            bool bselect = false;
            //=== stocke dans sid le nuémro de localisation, retourne true si la focntion s'est bien déroulé.
            functionReturnValue = false;
            bselect = false;
            try
            {

                foreach (UltraTreeNode oNode in TreeView1.Nodes)
                {
                    foreach (UltraTreeNode oNode_N1 in oNode.Nodes)
                    {

                        if (oNode_N1.Selected == true)
                        {

                            if (General.Left(oNode_N1.Key, 1).ToString().ToUpper() == cNiv2.ToString().ToUpper())
                            {

                                sID = General.Mid(oNode_N1.Key, 2, oNode_N1.Key.Length - 1);
                                bselect = true;
                                keyNodeActive = oNode_N1.Text;
                                //fc_CtrlLocTreev = sid

                            }
                        }
                    }
                }
                //===
                if (TreeView1.Nodes.Count == 0)//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un modèle pour l'ajouter dans la grille 'Modèle(s) sélectionné(s)'", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }
                else if (bselect == false)//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un modèle pour l'ajouter dans la grille 'Modèle(s) sélectionné(s)'.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlLocTreev");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            txtMVP_CreePar_KeyPress(txtMVP_CreePar, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
        }

        private void Command3_Click(object sender, EventArgs e)
        {
            //ssGridModele.RemoveAll();
            this.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lCop_NoAuto"></param>
        /// <param name="lPda_Noauto"></param>
        private void fc_RecapPrest(int lCop_NoAuto, int lPda_Noauto)
        {
            try
            {
                string sSQL = null;

                sSQL = "SELECT   PDAB_BadgePDA.PDAB_Libelle,  GAI_GammeImm.GAI_LIbelle,  ICC_IntervCategorieContrat.CAI_Libelle, ICC_IntervCategorieContrat.COP_Noauto, " + " EQM_EquipementP2Imm.EQM_Code,";

                sSQL = sSQL + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle, ";

                sSQL = sSQL + " EQM_EquipementP2Imm.CompteurObli , EQM_EquipementP2Imm.EQU_P1Compteur, "
                    + " PEI_PeriodeGammeImm.TPP_Code, PEI_PeriodeGammeImm.PEI_DateDebut, PEI_PeriodeGammeImm.PEI_DateFin, PEI_PeriodeGammeImm.PEI_JD, "
                    + " PEI_PeriodeGammeImm.PEI_MD, PEI_PeriodeGammeImm.PEI_JF, PEI_PeriodeGammeImm.PEI_MF, PEI_PeriodeGammeImm.PEI_IntAutre, "
                    + " PEI_PeriodeGammeImm.PEI_Intervenant, PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel, "
                    + " PEI_PeriodeGammeImm.PEI_Visite, '' AS Total ";
                sSQL = sSQL + " FROM         GAI_GammeImm INNER JOIN "
                    + " PDAB_BadgePDA ON GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto INNER JOIN " + " PEI_PeriodeGammeImm INNER JOIN "
                    + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                    + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                    + " GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID";

                sSQL = sSQL + " WHERE ICC_IntervCategorieContrat.cop_NoAuto="
                    + lCop_NoAuto + " and PDAB_BadgePDA.PDAB_Noauto = "
                    + lPda_Noauto + " ORDER BY PDAB_BadgePDA.PDAB_Libelle,GAI_LIbelle,  ICC_IntervCategorieContrat.CAI_Libelle, EQM_EquipementP2Imm.EQM_CODE ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                using (var tmpAdo = new ModAdo())
                    rsRecap = tmpAdo.fc_OpenRecordSet(sSQL);
                this.ssGridRecap.DataSource = rsRecap;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_RecapPrest");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridRecap_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {

            double dbTotTemp = 0;
            //If nz(.Columns("PEI_IntAutre").Text, "0") = "0" Then
            //        .Columns("PEI_Intervenant").Value = FC_intervenantImm(txtCodeImmeuble)
            //End If

            //=== rendez vous à prendre.
            //If nz(.Columns("CompteurObli").Text, "0") = "1" Or nz(.Columns("EQU_P1Compteur").Text, "0") = "1" Then
            //    For i = 0 To .Columns.Count - 1
            //        .Columns(i).CellStyleSet cStyleCompteurObl, .Row
            //    Next i
            //End If
            if (e.Row.Cells["CompteurObli"].Value == DBNull.Value)
            {
                e.Row.Cells["CompteurObli"].Value = false;
            }
            if (e.Row.Cells["EQU_P1Compteur"].Value == DBNull.Value)
            {
                e.Row.Cells["EQU_P1Compteur"].Value = false;
            }

            dbTotTemp = fc_CalculDuree(Convert.ToDouble(General.nz(e.Row.Cells["PEI_Duree"].Text, 0)), Convert.ToDouble(General.nz(e.Row.Cells["PEI_Visite"].Text, 0)));
            e.Row.Cells["total"].Value = dbTotTemp;
            ssGridRecap.UpdateData();
        }
        private double fc_CalculDuree(double dbDuree, double dbNbVisite)
        {

            double functionReturnValue = 0;
            double dbTot = 0;
            try
            {
                dbTot = dbDuree * dbNbVisite;
                dbTot = General.FncArrondir(dbTot, 2);

                functionReturnValue = dbTot;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CalculDuree");
                return functionReturnValue;
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;

            try
            {
                if (KeyAscii == 13)
                {
                    var tmpAdo = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmpAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + txtCodeimmeuble.Text + "'"), "").ToString()))
                    {
                        string req = "SELECT CodeImmeuble as \"Code Immeuble\","
                                 + " Adresse as \"Adresse\", Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";

                        req = "SELECT     Immeuble.CodeImmeuble AS \"Modèle Immeuble\", Immeuble.Adresse AS adressse, Immeuble.Ville AS ville,"
                        + " MVP_ModeleVisiteP2.MVP_CreePar AS \"Crée Par\", MVP_ModeleVisiteP2.MVP_CreeLe AS \"Crée Le\" "
                        + " FROM         Immeuble INNER JOIN "
                        + " MVP_ModeleVisiteP2 ON Immeuble.CodeImmeuble = MVP_ModeleVisiteP2.CodeImmeuble";

                        string where = "";
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un immeuble" };
                        fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeimmeuble.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }

                    cmdAfficheModéle_Click(cmdAfficheModéle, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeimmeuble_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMVP_CreePar_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {

                if (KeyAscii == 13)
                {
                    var tmpAdo = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmpAdo.fc_ADOlibelle("SELECT USR_Nt From USR_Users WHERE USR_Nt = '" + StdSQLchaine.gFr_DoublerQuote(txtMVP_CreePar.Text) + "'"), "").ToString()))
                    {
                        string req = " SELECT     USR_Nt AS \"log\", USR_Name AS \"Nom\"  From USR_Users ";
                        string where = "";
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un utilisateur" };
                        fg.SetValues(new Dictionary<string, string> { { " USR_Nt", txtMVP_CreePar.Text + "*" } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtMVP_CreePar.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtMVP_CreePar.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();
                    }
                    cmdAfficheModéle_Click(cmdAfficheModéle, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtMVP_CreePar_KeyPress");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMVP_Nom_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    var tmpAdo = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmpAdo.fc_ADOlibelle("SELECT  MVP_Nom From MVP_ModeleVisiteP2 WHERE  MVP_Nom = '"
                        + StdSQLchaine.gFr_DoublerQuote(txtMVP_Nom.Text) + "'"), "").ToString()))
                    {
                        string req = "SELECT MVP_Nom AS \"Nom du modèle\", CodeImmeuble AS \"Code immeuble\", "
                            + " COP_NoAuto AS \"N°Fiche GMAO\", MVP_CreePar AS \"Crée par\"  FROM   MVP_ModeleVisiteP2 ";
                        string where = "";
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un modèle" };
                        fg.SetValues(new Dictionary<string, string> { { "MVP_Nom", txtMVP_Nom.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtMVP_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                         {
                             if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                             {
                                 txtMVP_Nom.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                 fg.Dispose(); fg.Close();
                             }
                         };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();
                    }

                }

                cmdAfficheModéle_Click(cmdAfficheModéle, new System.EventArgs());
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtMVP_Nom_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            try
            {


                string sID;
                string sSQL;
                int lPda_Noauto;
                DataTable rs = new DataTable();
                ModAdo rsAdo = new ModAdo();

                UltraTreeNode Node = TreeView1.ActiveNode;

                sID = General.Mid(Node.Key, 2, General.Len(Node.Key) - 1);
                if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv2))
                {
                    sSQL = "SELECT     COP_NoAuto, PDAB_Noauto From MVP_ModeleVisiteP2 WHERE MVP_ID = " + sID;
                    rs = rsAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        sID = rs.Rows[0]["COP_NoAuto"] + "";
                        lPda_Noauto = Convert.ToInt32(General.nz(rs.Rows[0]["PDAB_Noauto"], 0));
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun modèle trouvé.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        rs.Dispose();
                        rs = null;
                        return;
                    }
                    rs.Dispose();
                    rs = null;
                    fc_RecapPrest(Convert.ToInt32(General.nz(sID, 0)), lPda_Noauto);
                    ssGridRecap.Visible = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";TreeView1_AfterSelect");
            }
        }

        private void ssGridRecap_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            int i = 0;

            ssGridRecap.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Header.Caption = "Localisation";
            ssGridRecap.DisplayLayout.Bands[0].Columns["GAI_LIbelle"].Header.Caption = modP2.clibNiv0;
            ssGridRecap.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = modP2.clibNiv1;
            ssGridRecap.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle";
            ssGridRecap.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen";
            ssGridRecap.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssGridRecap.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridRecap.DisplayLayout.Bands[0].Columns["CompteurObli"].Header.Caption = "Compteur Obligatoire";
            ssGridRecap.DisplayLayout.Bands[0].Columns["CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridRecap.DisplayLayout.Bands[0].Columns["EQU_P1Compteur"].Header.Caption = "P1 Compteur";
            ssGridRecap.DisplayLayout.Bands[0].Columns["EQU_P1Compteur"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridRecap.DisplayLayout.Bands[0].Columns["TPP_Code"].Header.Caption = "Périodicité";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_JD"].Header.Caption = "Jour début";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_MD"].Header.Caption = "Mois début";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_JF"].Header.Caption = "Jour Fin";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_MF"].Header.Caption = "Mois Fin";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Header.Caption = "Autre";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].Header.Caption = "Intervenant";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_Duree"].Header.Caption = "Durée";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_Visite"].Header.Caption = "Nbre Visites";

            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DateFin"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DateDebut"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["cop_NoAuto"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["EQM_Code"].Header.Caption = modP2.cLIbNiv2;
            ssGridRecap.DisplayLayout.Bands[0].Columns["EQM_Code"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DureeReel"].Hidden = true;

            for (i = 0; i < ssGridRecap.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridRecap.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }

        }

        private void ssGridModele_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            ssGridModele.DisplayLayout.Bands[0].Columns["PDAB_Noauto"].Hidden = true;
            ssGridModele.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssGridModele.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssGridModele.DisplayLayout.Bands[0].Columns["MVP_ID"].Hidden = true;

            ssGridModele.DisplayLayout.Bands[0].Columns["Selection"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }

        private void frmModeleP2_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            ssGridModeleSource.Columns.Add("Selection");
            ssGridModeleSource.Columns.Add("Nom");
            ssGridModeleSource.Columns.Add("COP_NoAuto");
            ssGridModeleSource.Columns.Add("CodeImmeuble");
            ssGridModeleSource.Columns.Add("MVP_ID");
            ssGridModeleSource.Columns.Add("PDAB_Noauto");
            ssGridModele.DataSource = ssGridModeleSource;

        }
    }
}
