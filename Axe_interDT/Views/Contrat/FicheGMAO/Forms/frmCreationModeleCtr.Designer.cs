﻿namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    partial class frmCreationModeleCtr
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtCOP_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtLocalisation = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtMVP_Nom = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtMVP_CreePar = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMVP_CreeLe = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.cmdClean = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.txtPDAB_Noauto = new iTalk.iTalk_TextBox_Small2();
            this.iTalk_TextBox_Small21 = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(258, 57);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(290, 27);
            this.txtCodeImmeuble.TabIndex = 0;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(12, 57);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(203, 19);
            this.label33.TabIndex = 503;
            this.label33.Text = "Modèle basé sur l\'immeuble";
            // 
            // txtCOP_NoAuto
            // 
            this.txtCOP_NoAuto.AccAcceptNumbersOnly = false;
            this.txtCOP_NoAuto.AccAllowComma = false;
            this.txtCOP_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoAuto.AccHidenValue = "";
            this.txtCOP_NoAuto.AccNotAllowedChars = null;
            this.txtCOP_NoAuto.AccReadOnly = false;
            this.txtCOP_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoAuto.AccRequired = false;
            this.txtCOP_NoAuto.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoAuto.Location = new System.Drawing.Point(258, 89);
            this.txtCOP_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoAuto.MaxLength = 32767;
            this.txtCOP_NoAuto.Multiline = false;
            this.txtCOP_NoAuto.Name = "txtCOP_NoAuto";
            this.txtCOP_NoAuto.ReadOnly = false;
            this.txtCOP_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoAuto.Size = new System.Drawing.Size(290, 27);
            this.txtCOP_NoAuto.TabIndex = 1;
            this.txtCOP_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoAuto.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 92);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(241, 19);
            this.label1.TabIndex = 503;
            this.label1.Text = "Modèle basé sur le fiche GMAO n°";
            // 
            // txtLocalisation
            // 
            this.txtLocalisation.AccAcceptNumbersOnly = false;
            this.txtLocalisation.AccAllowComma = false;
            this.txtLocalisation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLocalisation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLocalisation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLocalisation.AccHidenValue = "";
            this.txtLocalisation.AccNotAllowedChars = null;
            this.txtLocalisation.AccReadOnly = false;
            this.txtLocalisation.AccReadOnlyAllowDelete = false;
            this.txtLocalisation.AccRequired = false;
            this.txtLocalisation.BackColor = System.Drawing.Color.White;
            this.txtLocalisation.CustomBackColor = System.Drawing.Color.White;
            this.txtLocalisation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtLocalisation.ForeColor = System.Drawing.Color.Black;
            this.txtLocalisation.Location = new System.Drawing.Point(258, 120);
            this.txtLocalisation.Margin = new System.Windows.Forms.Padding(2);
            this.txtLocalisation.MaxLength = 32767;
            this.txtLocalisation.Multiline = false;
            this.txtLocalisation.Name = "txtLocalisation";
            this.txtLocalisation.ReadOnly = false;
            this.txtLocalisation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLocalisation.Size = new System.Drawing.Size(290, 27);
            this.txtLocalisation.TabIndex = 2;
            this.txtLocalisation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLocalisation.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 120);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 19);
            this.label2.TabIndex = 503;
            this.label2.Text = "Localisation";
            // 
            // txtMVP_Nom
            // 
            this.txtMVP_Nom.AccAcceptNumbersOnly = false;
            this.txtMVP_Nom.AccAllowComma = false;
            this.txtMVP_Nom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMVP_Nom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMVP_Nom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMVP_Nom.AccHidenValue = "";
            this.txtMVP_Nom.AccNotAllowedChars = null;
            this.txtMVP_Nom.AccReadOnly = false;
            this.txtMVP_Nom.AccReadOnlyAllowDelete = false;
            this.txtMVP_Nom.AccRequired = false;
            this.txtMVP_Nom.BackColor = System.Drawing.Color.White;
            this.txtMVP_Nom.CustomBackColor = System.Drawing.Color.White;
            this.txtMVP_Nom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMVP_Nom.ForeColor = System.Drawing.Color.Black;
            this.txtMVP_Nom.Location = new System.Drawing.Point(258, 152);
            this.txtMVP_Nom.Margin = new System.Windows.Forms.Padding(2);
            this.txtMVP_Nom.MaxLength = 32767;
            this.txtMVP_Nom.Multiline = false;
            this.txtMVP_Nom.Name = "txtMVP_Nom";
            this.txtMVP_Nom.ReadOnly = false;
            this.txtMVP_Nom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMVP_Nom.Size = new System.Drawing.Size(290, 27);
            this.txtMVP_Nom.TabIndex = 3;
            this.txtMVP_Nom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMVP_Nom.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 152);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(119, 19);
            this.label3.TabIndex = 503;
            this.label3.Text = "Nom du modèle";
            // 
            // txtMVP_CreePar
            // 
            this.txtMVP_CreePar.AccAcceptNumbersOnly = false;
            this.txtMVP_CreePar.AccAllowComma = false;
            this.txtMVP_CreePar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMVP_CreePar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMVP_CreePar.AccHidenValue = "";
            this.txtMVP_CreePar.AccNotAllowedChars = null;
            this.txtMVP_CreePar.AccReadOnly = false;
            this.txtMVP_CreePar.AccReadOnlyAllowDelete = false;
            this.txtMVP_CreePar.AccRequired = false;
            this.txtMVP_CreePar.BackColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.CustomBackColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMVP_CreePar.ForeColor = System.Drawing.Color.Black;
            this.txtMVP_CreePar.Location = new System.Drawing.Point(258, 183);
            this.txtMVP_CreePar.Margin = new System.Windows.Forms.Padding(2);
            this.txtMVP_CreePar.MaxLength = 32767;
            this.txtMVP_CreePar.Multiline = false;
            this.txtMVP_CreePar.Name = "txtMVP_CreePar";
            this.txtMVP_CreePar.ReadOnly = false;
            this.txtMVP_CreePar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMVP_CreePar.Size = new System.Drawing.Size(290, 27);
            this.txtMVP_CreePar.TabIndex = 4;
            this.txtMVP_CreePar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMVP_CreePar.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 183);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(69, 19);
            this.label4.TabIndex = 503;
            this.label4.Text = "Crée par";
            // 
            // txtMVP_CreeLe
            // 
            this.txtMVP_CreeLe.AccAcceptNumbersOnly = false;
            this.txtMVP_CreeLe.AccAllowComma = false;
            this.txtMVP_CreeLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMVP_CreeLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMVP_CreeLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMVP_CreeLe.AccHidenValue = "";
            this.txtMVP_CreeLe.AccNotAllowedChars = null;
            this.txtMVP_CreeLe.AccReadOnly = false;
            this.txtMVP_CreeLe.AccReadOnlyAllowDelete = false;
            this.txtMVP_CreeLe.AccRequired = false;
            this.txtMVP_CreeLe.BackColor = System.Drawing.Color.White;
            this.txtMVP_CreeLe.CustomBackColor = System.Drawing.Color.White;
            this.txtMVP_CreeLe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMVP_CreeLe.ForeColor = System.Drawing.Color.Black;
            this.txtMVP_CreeLe.Location = new System.Drawing.Point(258, 214);
            this.txtMVP_CreeLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtMVP_CreeLe.MaxLength = 32767;
            this.txtMVP_CreeLe.Multiline = false;
            this.txtMVP_CreeLe.Name = "txtMVP_CreeLe";
            this.txtMVP_CreeLe.ReadOnly = false;
            this.txtMVP_CreeLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMVP_CreeLe.Size = new System.Drawing.Size(290, 27);
            this.txtMVP_CreeLe.TabIndex = 5;
            this.txtMVP_CreeLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMVP_CreeLe.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 214);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(59, 19);
            this.label5.TabIndex = 503;
            this.label5.Text = "Crée le";
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdClean.Location = new System.Drawing.Point(404, 8);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(60, 35);
            this.cmdClean.TabIndex = 504;
            this.cmdClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdClean.UseVisualStyleBackColor = false;
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(480, 8);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 505;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // txtPDAB_Noauto
            // 
            this.txtPDAB_Noauto.AccAcceptNumbersOnly = false;
            this.txtPDAB_Noauto.AccAllowComma = false;
            this.txtPDAB_Noauto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPDAB_Noauto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPDAB_Noauto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPDAB_Noauto.AccHidenValue = "";
            this.txtPDAB_Noauto.AccNotAllowedChars = null;
            this.txtPDAB_Noauto.AccReadOnly = false;
            this.txtPDAB_Noauto.AccReadOnlyAllowDelete = false;
            this.txtPDAB_Noauto.AccRequired = false;
            this.txtPDAB_Noauto.BackColor = System.Drawing.Color.White;
            this.txtPDAB_Noauto.CustomBackColor = System.Drawing.Color.White;
            this.txtPDAB_Noauto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtPDAB_Noauto.ForeColor = System.Drawing.Color.Black;
            this.txtPDAB_Noauto.Location = new System.Drawing.Point(11, 237);
            this.txtPDAB_Noauto.Margin = new System.Windows.Forms.Padding(2);
            this.txtPDAB_Noauto.MaxLength = 32767;
            this.txtPDAB_Noauto.Multiline = false;
            this.txtPDAB_Noauto.Name = "txtPDAB_Noauto";
            this.txtPDAB_Noauto.ReadOnly = false;
            this.txtPDAB_Noauto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPDAB_Noauto.Size = new System.Drawing.Size(135, 27);
            this.txtPDAB_Noauto.TabIndex = 502;
            this.txtPDAB_Noauto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPDAB_Noauto.UseSystemPasswordChar = false;
            this.txtPDAB_Noauto.Visible = false;
            // 
            // iTalk_TextBox_Small21
            // 
            this.iTalk_TextBox_Small21.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small21.AccAllowComma = false;
            this.iTalk_TextBox_Small21.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small21.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small21.AccHidenValue = "";
            this.iTalk_TextBox_Small21.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small21.AccReadOnly = false;
            this.iTalk_TextBox_Small21.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small21.AccRequired = false;
            this.iTalk_TextBox_Small21.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.iTalk_TextBox_Small21.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small21.Location = new System.Drawing.Point(258, 121);
            this.iTalk_TextBox_Small21.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small21.MaxLength = 32767;
            this.iTalk_TextBox_Small21.Multiline = false;
            this.iTalk_TextBox_Small21.Name = "iTalk_TextBox_Small21";
            this.iTalk_TextBox_Small21.ReadOnly = false;
            this.iTalk_TextBox_Small21.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small21.Size = new System.Drawing.Size(290, 27);
            this.iTalk_TextBox_Small21.TabIndex = 2;
            this.iTalk_TextBox_Small21.TextAlignment = Infragistics.Win.HAlign.Left;
            this.iTalk_TextBox_Small21.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(12, 121);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 19);
            this.label6.TabIndex = 503;
            this.label6.Text = "Localisation";
            // 
            // frmCreationModeleCtr
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(551, 276);
            this.Controls.Add(this.CmdSauver);
            this.Controls.Add(this.cmdClean);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtPDAB_Noauto);
            this.Controls.Add(this.txtMVP_CreeLe);
            this.Controls.Add(this.txtMVP_CreePar);
            this.Controls.Add(this.txtMVP_Nom);
            this.Controls.Add(this.iTalk_TextBox_Small21);
            this.Controls.Add(this.txtLocalisation);
            this.Controls.Add(this.txtCOP_NoAuto);
            this.Controls.Add(this.txtCodeImmeuble);
            this.Name = "frmCreationModeleCtr";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmCreationModeleCtr";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoAuto;
        public System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtLocalisation;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtMVP_Nom;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtMVP_CreePar;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtMVP_CreeLe;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button CmdSauver;
        public iTalk.iTalk_TextBox_Small2 txtPDAB_Noauto;
        public iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small21;
        public System.Windows.Forms.Label label6;
    }
}