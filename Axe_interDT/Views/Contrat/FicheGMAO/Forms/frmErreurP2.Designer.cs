﻿namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    partial class frmErreurP2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblInsertion = new System.Windows.Forms.Label();
            this.txtErreur = new iTalk.iTalk_RichTextBox();
            this.SuspendLayout();
            // 
            // lblInsertion
            // 
            this.lblInsertion.AutoSize = true;
            this.lblInsertion.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInsertion.Location = new System.Drawing.Point(70, 22);
            this.lblInsertion.Name = "lblInsertion";
            this.lblInsertion.Size = new System.Drawing.Size(304, 19);
            this.lblInsertion.TabIndex = 587;
            this.lblInsertion.Text = "Erreurs décelées dans la planification";
            this.lblInsertion.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtErreur
            // 
            this.txtErreur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtErreur.AutoWordSelection = false;
            this.txtErreur.BackColor = System.Drawing.Color.Transparent;
            this.txtErreur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtErreur.ForeColor = System.Drawing.Color.Black;
            this.txtErreur.Location = new System.Drawing.Point(12, 56);
            this.txtErreur.Name = "txtErreur";
            this.txtErreur.ReadOnly = false;
            this.txtErreur.Size = new System.Drawing.Size(408, 276);
            this.txtErreur.TabIndex = 588;
            this.txtErreur.WordWrap = true;
            // 
            // frmErreurP2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(431, 341);
            this.Controls.Add(this.txtErreur);
            this.Controls.Add(this.lblInsertion);
            this.Name = "frmErreurP2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmErreurP2";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblInsertion;
        public iTalk.iTalk_RichTextBox txtErreur;
    }
}