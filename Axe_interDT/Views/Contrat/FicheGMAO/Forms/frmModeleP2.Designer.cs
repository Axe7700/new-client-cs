﻿namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    partial class frmModeleP2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.txtCOP_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.ssGridModele = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Command3 = new System.Windows.Forms.Button();
            this.cmdInsertModele = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Command1 = new System.Windows.Forms.Button();
            this.txtMVP_CreePar = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdFindModele = new System.Windows.Forms.Button();
            this.txtMVP_Nom = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGridRecap = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdSelectModele = new System.Windows.Forms.Button();
            this.TreeView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.cmdAfficheModéle = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.Frame2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridModele)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridRecap)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 35.98726F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 64.01274F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(794, 627);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox6, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 219F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(788, 219);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.tableLayoutPanel7);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox6.Location = new System.Drawing.Point(397, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(388, 213);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Modèle(s) sélectionné(s)";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.Frame2, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.ssGridModele, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.Command3, 1, 2);
            this.tableLayoutPanel7.Controls.Add(this.cmdInsertModele, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.CmdSauver, 1, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 49F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(382, 190);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.Frame2.Controls.Add(this.txtCOP_NoAuto);
            this.Frame2.Controls.Add(this.label4);
            this.Frame2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame2.Location = new System.Drawing.Point(3, 104);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(185, 34);
            this.Frame2.TabIndex = 413;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "controle invisbile";
            this.Frame2.Visible = false;
            // 
            // txtCOP_NoAuto
            // 
            this.txtCOP_NoAuto.AccAcceptNumbersOnly = false;
            this.txtCOP_NoAuto.AccAllowComma = false;
            this.txtCOP_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCOP_NoAuto.AccHidenValue = "";
            this.txtCOP_NoAuto.AccNotAllowedChars = null;
            this.txtCOP_NoAuto.AccReadOnly = false;
            this.txtCOP_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoAuto.AccRequired = false;
            this.txtCOP_NoAuto.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoAuto.Location = new System.Drawing.Point(34, 7);
            this.txtCOP_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoAuto.MaxLength = 32767;
            this.txtCOP_NoAuto.Multiline = false;
            this.txtCOP_NoAuto.Name = "txtCOP_NoAuto";
            this.txtCOP_NoAuto.ReadOnly = false;
            this.txtCOP_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoAuto.Size = new System.Drawing.Size(135, 27);
            this.txtCOP_NoAuto.TabIndex = 502;
            this.txtCOP_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoAuto.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(45, 5);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(140, 19);
            this.label4.TabIndex = 384;
            this.label4.Text = "Basé sur le modele";
            // 
            // ssGridModele
            // 
            this.tableLayoutPanel7.SetColumnSpan(this.ssGridModele, 2);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridModele.DisplayLayout.Appearance = appearance1;
            this.ssGridModele.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridModele.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridModele.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridModele.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridModele.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridModele.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridModele.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridModele.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridModele.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridModele.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridModele.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridModele.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridModele.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridModele.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridModele.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridModele.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridModele.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridModele.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridModele.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridModele.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridModele.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridModele.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridModele.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridModele.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridModele.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridModele.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridModele.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridModele.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridModele.Location = new System.Drawing.Point(3, 3);
            this.ssGridModele.Name = "ssGridModele";
            this.ssGridModele.Size = new System.Drawing.Size(376, 95);
            this.ssGridModele.TabIndex = 412;
            this.ssGridModele.Text = "ultraGrid1";
            this.ssGridModele.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridModele_InitializeLayout);
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.Command3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command3.Location = new System.Drawing.Point(194, 144);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(185, 43);
            this.Command3.TabIndex = 404;
            this.Command3.Text = "Annuler et Fermer";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // cmdInsertModele
            // 
            this.cmdInsertModele.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdInsertModele.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdInsertModele.FlatAppearance.BorderSize = 0;
            this.cmdInsertModele.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdInsertModele.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdInsertModele.ForeColor = System.Drawing.Color.White;
            this.cmdInsertModele.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdInsertModele.Location = new System.Drawing.Point(2, 143);
            this.cmdInsertModele.Margin = new System.Windows.Forms.Padding(2);
            this.cmdInsertModele.Name = "cmdInsertModele";
            this.cmdInsertModele.Size = new System.Drawing.Size(187, 45);
            this.cmdInsertModele.TabIndex = 403;
            this.cmdInsertModele.Text = "Insérer le(s) modèle(s) dans le contrat";
            this.cmdInsertModele.UseVisualStyleBackColor = false;
            this.cmdInsertModele.Click += new System.EventHandler(this.cmdInsertModele_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.CmdSauver.ForeColor = System.Drawing.Color.White;
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.CmdSauver.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdSauver.Location = new System.Drawing.Point(194, 104);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(185, 34);
            this.CmdSauver.TabIndex = 406;
            this.CmdSauver.Text = "      Sauver";
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.38292F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.61708F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel5, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel6, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 8F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(388, 213);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.42714F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.57286F));
            this.tableLayoutPanel5.Controls.Add(this.Command1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtMVP_CreePar, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(184, 139);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(203, 31);
            this.tableLayoutPanel5.TabIndex = 505;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command1.Location = new System.Drawing.Point(176, 3);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(24, 24);
            this.Command1.TabIndex = 503;
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // txtMVP_CreePar
            // 
            this.txtMVP_CreePar.AccAcceptNumbersOnly = false;
            this.txtMVP_CreePar.AccAllowComma = false;
            this.txtMVP_CreePar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMVP_CreePar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMVP_CreePar.AccHidenValue = "";
            this.txtMVP_CreePar.AccNotAllowedChars = null;
            this.txtMVP_CreePar.AccReadOnly = false;
            this.txtMVP_CreePar.AccReadOnlyAllowDelete = false;
            this.txtMVP_CreePar.AccRequired = false;
            this.txtMVP_CreePar.BackColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.CustomBackColor = System.Drawing.Color.White;
            this.txtMVP_CreePar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMVP_CreePar.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMVP_CreePar.ForeColor = System.Drawing.Color.Black;
            this.txtMVP_CreePar.Location = new System.Drawing.Point(2, 2);
            this.txtMVP_CreePar.Margin = new System.Windows.Forms.Padding(2);
            this.txtMVP_CreePar.MaxLength = 32767;
            this.txtMVP_CreePar.Multiline = false;
            this.txtMVP_CreePar.Name = "txtMVP_CreePar";
            this.txtMVP_CreePar.ReadOnly = false;
            this.txtMVP_CreePar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMVP_CreePar.Size = new System.Drawing.Size(169, 27);
            this.txtMVP_CreePar.TabIndex = 0;
            this.txtMVP_CreePar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMVP_CreePar.UseSystemPasswordChar = false;
            this.txtMVP_CreePar.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMVP_CreePar_KeyPress);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.42714F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.57286F));
            this.tableLayoutPanel4.Controls.Add(this.cmdRechercheImmeuble, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.txtCodeimmeuble, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(184, 106);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(203, 31);
            this.tableLayoutPanel4.TabIndex = 504;
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(176, 3);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(24, 24);
            this.cmdRechercheImmeuble.TabIndex = 503;
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(2, 2);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = false;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(169, 27);
            this.txtCodeimmeuble.TabIndex = 0;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            this.txtCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeimmeuble_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.label33, 2);
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(2, 2);
            this.label33.Margin = new System.Windows.Forms.Padding(2);
            this.label33.Name = "label33";
            this.tableLayoutPanel3.SetRowSpan(this.label33, 2);
            this.label33.Size = new System.Drawing.Size(384, 68);
            this.label33.TabIndex = 384;
            this.label33.Text = "Veuillez rechercher et sélectionner le(s) modèle(s) à insérer dans le contrat.";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 72);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(177, 33);
            this.label1.TabIndex = 385;
            this.label1.Text = "Nom du modèle";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(177, 33);
            this.label2.TabIndex = 386;
            this.label2.Text = "Basé sur le modèle de l\'immeuble";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 138);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(177, 33);
            this.label3.TabIndex = 387;
            this.label3.Text = "Crée par";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.42714F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.57286F));
            this.tableLayoutPanel6.Controls.Add(this.cmdFindModele, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtMVP_Nom, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(184, 73);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(203, 31);
            this.tableLayoutPanel6.TabIndex = 503;
            // 
            // cmdFindModele
            // 
            this.cmdFindModele.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindModele.FlatAppearance.BorderSize = 0;
            this.cmdFindModele.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindModele.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindModele.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFindModele.Location = new System.Drawing.Point(176, 3);
            this.cmdFindModele.Name = "cmdFindModele";
            this.cmdFindModele.Size = new System.Drawing.Size(24, 24);
            this.cmdFindModele.TabIndex = 503;
            this.cmdFindModele.UseVisualStyleBackColor = false;
            this.cmdFindModele.Click += new System.EventHandler(this.cmdFindModele_Click);
            // 
            // txtMVP_Nom
            // 
            this.txtMVP_Nom.AccAcceptNumbersOnly = false;
            this.txtMVP_Nom.AccAllowComma = false;
            this.txtMVP_Nom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMVP_Nom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMVP_Nom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMVP_Nom.AccHidenValue = "";
            this.txtMVP_Nom.AccNotAllowedChars = null;
            this.txtMVP_Nom.AccReadOnly = false;
            this.txtMVP_Nom.AccReadOnlyAllowDelete = false;
            this.txtMVP_Nom.AccRequired = false;
            this.txtMVP_Nom.BackColor = System.Drawing.Color.White;
            this.txtMVP_Nom.CustomBackColor = System.Drawing.Color.White;
            this.txtMVP_Nom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMVP_Nom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMVP_Nom.ForeColor = System.Drawing.Color.Black;
            this.txtMVP_Nom.Location = new System.Drawing.Point(2, 2);
            this.txtMVP_Nom.Margin = new System.Windows.Forms.Padding(2);
            this.txtMVP_Nom.MaxLength = 32767;
            this.txtMVP_Nom.Multiline = false;
            this.txtMVP_Nom.Name = "txtMVP_Nom";
            this.txtMVP_Nom.ReadOnly = false;
            this.txtMVP_Nom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMVP_Nom.Size = new System.Drawing.Size(169, 27);
            this.txtMVP_Nom.TabIndex = 0;
            this.txtMVP_Nom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMVP_Nom.UseSystemPasswordChar = false;
            this.txtMVP_Nom.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMVP_Nom_KeyPress);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.74112F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.25888F));
            this.tableLayoutPanel8.Controls.Add(this.ssGridRecap, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.cmdSelectModele, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.TreeView1, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.cmdAfficheModéle, 1, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 228);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(788, 396);
            this.tableLayoutPanel8.TabIndex = 1;
            // 
            // ssGridRecap
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridRecap.DisplayLayout.Appearance = appearance13;
            this.ssGridRecap.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridRecap.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridRecap.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridRecap.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGridRecap.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridRecap.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGridRecap.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridRecap.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridRecap.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridRecap.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGridRecap.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridRecap.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridRecap.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridRecap.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridRecap.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridRecap.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridRecap.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGridRecap.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridRecap.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridRecap.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGridRecap.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGridRecap.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridRecap.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGridRecap.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGridRecap.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridRecap.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridRecap.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGridRecap.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridRecap.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridRecap.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridRecap.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridRecap.Location = new System.Drawing.Point(261, 53);
            this.ssGridRecap.Name = "ssGridRecap";
            this.ssGridRecap.Size = new System.Drawing.Size(524, 340);
            this.ssGridRecap.TabIndex = 412;
            this.ssGridRecap.Text = "ultraGrid1";
            this.ssGridRecap.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridRecap_InitializeLayout);
            this.ssGridRecap.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridRecap_InitializeRow);
            // 
            // cmdSelectModele
            // 
            this.cmdSelectModele.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSelectModele.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSelectModele.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdSelectModele.FlatAppearance.BorderSize = 0;
            this.cmdSelectModele.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSelectModele.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdSelectModele.ForeColor = System.Drawing.Color.White;
            this.cmdSelectModele.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSelectModele.Location = new System.Drawing.Point(2, 2);
            this.cmdSelectModele.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSelectModele.Name = "cmdSelectModele";
            this.cmdSelectModele.Size = new System.Drawing.Size(254, 46);
            this.cmdSelectModele.TabIndex = 506;
            this.cmdSelectModele.Text = "Sélectionnez et Ajouter un modèle";
            this.cmdSelectModele.UseVisualStyleBackColor = false;
            this.cmdSelectModele.Click += new System.EventHandler(this.cmdSelectModele_Click);
            // 
            // TreeView1
            // 
            this.TreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TreeView1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.TreeView1.Location = new System.Drawing.Point(3, 53);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.Size = new System.Drawing.Size(252, 340);
            this.TreeView1.TabIndex = 507;
            this.TreeView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeView1_AfterSelect);
            // 
            // cmdAfficheModéle
            // 
            this.cmdAfficheModéle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAfficheModéle.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAfficheModéle.FlatAppearance.BorderSize = 0;
            this.cmdAfficheModéle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAfficheModéle.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAfficheModéle.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdAfficheModéle.Location = new System.Drawing.Point(260, 2);
            this.cmdAfficheModéle.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAfficheModéle.Name = "cmdAfficheModéle";
            this.cmdAfficheModéle.Size = new System.Drawing.Size(60, 46);
            this.cmdAfficheModéle.TabIndex = 507;
            this.cmdAfficheModéle.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdAfficheModéle.UseVisualStyleBackColor = false;
            this.cmdAfficheModéle.Click += new System.EventHandler(this.cmdAfficheModéle_Click);
            // 
            // frmModeleP2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(794, 627);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximumSize = new System.Drawing.Size(810, 666);
            this.MinimumSize = new System.Drawing.Size(810, 666);
            this.Name = "frmModeleP2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmP2";
            this.Load += new System.EventHandler(this.frmModeleP2_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridModele)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridRecap)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Button cmdSelectModele;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.Button Command1;
        public iTalk.iTalk_TextBox_Small2 txtMVP_CreePar;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.Button cmdFindModele;
        public iTalk.iTalk_TextBox_Small2 txtMVP_Nom;
        public System.Windows.Forms.Button cmdAfficheModéle;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.Button cmdInsertModele;
        public System.Windows.Forms.Button Command3;
        public System.Windows.Forms.Button CmdSauver;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridModele;
        private System.Windows.Forms.GroupBox Frame2;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoAuto;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridRecap;
        private Infragistics.Win.UltraWinTree.UltraTree TreeView1;
    }
}