﻿namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    partial class frmChoixModeleContrat
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.Command1 = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.Command3 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(203, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(226, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Cliquez sur un des boutons.";
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Location = new System.Drawing.Point(11, 50);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(215, 53);
            this.Command1.TabIndex = 580;
            this.Command1.Text = "Rechercher et insérer dans ce contrat un modèle existant";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Location = new System.Drawing.Point(230, 50);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(215, 53);
            this.Command2.TabIndex = 581;
            this.Command2.Text = "Créer un modèle à partir de contrat";
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.Location = new System.Drawing.Point(449, 50);
            this.Command3.Margin = new System.Windows.Forms.Padding(2);
            this.Command3.MaximumSize = new System.Drawing.Size(144, 53);
            this.Command3.MinimumSize = new System.Drawing.Size(144, 53);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(144, 53);
            this.Command3.TabIndex = 582;
            this.Command3.Text = "Annuler";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // frmChoixModeleContrat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(604, 138);
            this.Controls.Add(this.Command3);
            this.Controls.Add(this.Command2);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.label33);
            this.Name = "frmChoixModeleContrat";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmChoixModeleContrat";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button Command2;
        public System.Windows.Forms.Button Command3;
    }
}