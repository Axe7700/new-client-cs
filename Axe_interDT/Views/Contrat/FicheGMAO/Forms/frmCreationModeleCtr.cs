﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class frmCreationModeleCtr : Form
    {
        public frmCreationModeleCtr()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            string sSQl;
            DataTable rs = default(DataTable);
            try
            {
                if (string.IsNullOrEmpty(txtMVP_Nom.Text))//Tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le nom est obligatoire ", "opération annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }
                sSQl = "SELECT     MVP_ID, MVP_Nom, COP_NoAuto, CodeImmeuble, MVP_CreePar, MVP_CreeLe, PDAB_Noauto From MVP_ModeleVisiteP2  WHERE     (MVP_ID = 0)";
                var ModAdo=new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQl);
                if (rs.Rows.Count == 0)
                {
                    var dr = rs.NewRow();
                    dr["MVP_Nom"] = txtMVP_Nom.Text.Trim();
                    dr["COP_NoAuto"] = General.nz(txtCOP_NoAuto.Text, 0);
                    dr["CodeImmeuble"] = txtCodeImmeuble.Text;
                    dr["MVP_CreePar"] = txtMVP_CreePar.Text;
                    dr["MVP_CreeLe"] = txtMVP_CreeLe.Text;
                    dr["PDAB_Noauto"] = txtPDAB_Noauto.Text;
                    rs.Rows.Add(dr);
                    ModAdo.Update();
                }
                ModAdo.Close();
                rs = null;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Modèle enregistré.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name +";CmdSauver_Click");
            }
        }
    }
}
